/*
 * Log.h
 *
 *  Created on: Nov 2, 2015
 *      Author: rupandreev
 */
#ifndef LOG_H
#define LOG_H

#include <stdarg.h>
#include <dlog.h>
#include "Common.h"

class Log
{
public:
    static void info(const char *msg, ...);
    static void debug(const char *msg, ...);
    static void warning(const char *msg, ...);
    static void error(const char *msg, ...);
private:
    static void write( log_priority prio, va_list list, const char *msg );

    static const char *TAG;
};

#endif // LOG_H
