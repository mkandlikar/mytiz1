#ifndef FACEBOOKAPPROTOCOL_H_
#define FACEBOOKAPPROTOCOL_H_

#include "DataServiceProtocolCommon.h"
#include "StorageDataCleaner.h"


class FileDownloader;

/**
 * @brief Interface for class which will implement start/stop service functionality.
 */
class IAppControl {
public:
    virtual ~IAppControl(){}
    virtual void HandleShutdownCommand() = 0;
};

#ifdef DATA_SERVICE_TEST
class FacebookAppProtocol;

/**
 * @brief This a demo class for test purposes only.
 */
class AppControl : public IAppControl {
public:
    ~AppControl(){}
    virtual void HandleShutdownCommand();
};
#endif

/**
 * @brief This is an concrete protocol for communication with the Facebook application.
 * @details This protocol can be constructed and used by clients.
 * This protocol isn't fully completed yet and it will be extended later.
 */
class FacebookAppProtocol : public ProtocolBase {
public:
    FacebookAppProtocol();
    virtual ~FacebookAppProtocol();

    void DownloadImageCompleted(int error, MessageId requestId, const char* fileName);

    void SetAppControl(IAppControl * appControl);

private:
    virtual void DoHandleReceivedData(MessageType messageType, bundle* data) override;
    virtual void CancelRequest(MessageId requestId, MessageType requestType) override;

    void DownloadImage(MessageId requestId, bundle *request);
    void CancelDownloadImage(MessageId requestId);

    void ShutDown();
    void ConnectToPush();

private:
    FileDownloader *mFileDownloader;
    IAppControl *mAppControl;
    StorageDataCleaner * mDataCleaner;
};

#endif /* FACEBOOKAPPROTOCOL_H_ */
