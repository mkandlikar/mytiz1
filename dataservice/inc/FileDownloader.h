#ifndef FILEDOWNLOADER_H_
#define FILEDOWNLOADER_H_

#include "FacebookAppProtocol.h"

class AsyncHTTPRequest;
class FileDownloader {
public:
    FileDownloader(FacebookAppProtocol * protocol) : mFacebookAppProtocol(protocol) {}
    ~FileDownloader();
    void DownloadImage(MessageId requestId, const char* url);
    void CancelDownloadImage(MessageId requestId);
private:
    AsyncHTTPRequest* FindRequest(MessageId requestId) const;
    //It may look redundant to pass both requestId and pointer to request as parameters. But we need both of them.
    //If we pass only requestId then we need to find request in the map, but we already know it. So, let's pass the both.
    void RemoveRequest(AsyncHTTPRequest* request, MessageId requestId);

    static void on_get_download_image_completed(void* data, int curlCode, const char* respData, int resplen,  const char* fileName);
    static void on_cancelation_completed(void* data);

    FacebookAppProtocol *mFacebookAppProtocol;
    std::map <MessageId, AsyncHTTPRequest*> mRequests;
};


#endif /* FILEDOWNLOADER_H_ */
