#ifndef UTILS_H_
#define UTILS_H_

#include <cassert>
#include <Elementary.h>
#include <string>

namespace Utils {

/**
 * Replace searchStr to replaceStr in str
 */
void ReplaceString(std::string &str, const std::string &searchStr, const std::string &replaceStr);

/**
 * Getting icon name from URL.
 * return icon name.
 */
char* GetIconNameFromUrl(const char* url);

/**
 * @brief This function gets the user-agent header to be set in the requests that are sent to server
 * @return[out] Char string representing the UserAgent
 */
const char *GetHTTPUserAgent();

/**
 * @brief This function gets the mcc, mnc
 * @return[out] Success/Failure to retreive mcc, mnc
 */
bool GetMCC_MNC(char **mcc, char **mnc);
int Memcpy_s(void *dst, size_t sizeDst, const void *src, size_t sizeSrc);

int Snprintf_s(char *buffer, size_t count, const char *format, ...);

long int  GetUnixTime_inSec();
}
#endif /* UTILS_H_ */
