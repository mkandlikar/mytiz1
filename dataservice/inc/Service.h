#ifndef SERVICE_H_
#define SERVICE_H_

#include <app_control.h>
#include <app_common.h>

#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "dataservice"

class FacebookAppProtocol;

class Service {
public:
    Service();
    virtual ~Service();

    int Run(int argc, char *argv[]);

private:
    virtual void Init();

//callbacks
    static bool service_app_create(void *data);
    static void service_app_terminate(void *data);
    static void service_app_control(app_control_h app_control, void *data);
    static void service_app_lang_changed(app_event_info_h event_info, void *user_data);
    static void service_app_orient_changed(app_event_info_h event_info, void *user_data);
    static void service_app_region_changed(app_event_info_h event_info, void *user_data);
    static void service_app_low_battery(app_event_info_h event_info, void *user_data);
    static void service_app_low_memory(app_event_info_h event_info, void *user_data);
private:
    FacebookAppProtocol* mFbAppPr;
};


#endif /* SERVICE_H_ */
