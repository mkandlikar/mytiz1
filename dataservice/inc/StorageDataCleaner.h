#ifndef STORAGEDATACLEANER_H_
#define STORAGEDATACLEANER_H_

#include <Ecore.h>
#include <sys/stat.h>

class StorageDataCleaner {

    enum ViewMode {
        EDeleteAllFiles,
        EDeleteOldFiles,
        EFindFiles
    };

public:
    StorageDataCleaner();
    virtual ~StorageDataCleaner();
    void RemoveUserDataFromStorage();
    void StartClearTimer();
    void CheckSizeOfUserData();
private:
    int ViewDirectory(const char * path, ViewMode Mode);
    long int GetLastClearTime();
    bool IsOldFile(long int accessTime);
    long int GetSizeOfUserData();
    void RemoveOldUserDataFromStorage();
    void NormalizeSizeOfUserData();
    static int sort_cb(const void *d1, const void *d2);
    static Eina_Bool time_to_delete_user_data_cb(void *data);
    Ecore_Timer * mTimer;
    Eina_List * mFiles;

    const static int LifetimeOfUserData;
    const static int CleaningPeriod;
    const static long int MaxSizeUserData;
    const static long int NormalSizeUserData;
    const static char * PreferenceField;
};

#endif /* STORAGEDATACLEANER_H_ */
