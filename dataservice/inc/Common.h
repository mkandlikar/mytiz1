#ifndef COMMON_H_
#define COMMON_H_

//#define _RELEASE_NO_PRINTS
#ifdef _RELEASE_NO_PRINTS
#define dlog_print(...)
#define dlog_vprint(...)
#endif

#ifndef FB_PACKAGE
#define FB_PACKAGE "com.facebook.tizen"
#endif

#define SAFE_STRDUP(str) (str ? strdup(str) : nullptr)

// Literals for user-agent string
static const char* defPlatformVersion = "0.0.0.0";
static const char* defAppVersion = "0.0.0";
static const char* defModel = "Z3(Default)";
static const char* defMenufacturerName = "Samsung(Default)";
static const char* defPlatform = "Tizen(Default)";
static const char* defLocale ="en(Default)";
static const char* defMCC = "000";
static const char* defMNC = "000";

// Notification constants
static const char* APP_CONTROL_CUSTOM_OPERATION_NOTIF_RCVD_TO_PROCESS = "NotificationReceivedToProcess";
static const char* PUSH_NOTIF_JSON_DATA = "NOTIFICATION_OBJECT_ID";

//User-agent buffer length
static const unsigned int MAX_UA_LENGTH = 512;  /* actually UA strings for Z1,Z2,Z3,Z4 do not exceed 300 bytes */

#endif /* COMMON_H_ */
