#ifndef CURLWRAPPER_H_
#define CURLWRAPPER_H_

#include <curl/curl.h>


class AsyncHTTPRequest;

class CurlWrapper {
public:
    CurlWrapper(AsyncHTTPRequest *request);
    ~CurlWrapper();

    CURLcode SendHttpGet(const char *request);

private:
    static size_t write_data(void *contents, size_t size, size_t nmemb, void *userp);
    static int progress_callback(void *clientp, curl_off_t dltotal, curl_off_t dlnow, curl_off_t ultotal, curl_off_t ulnow);

public:
    char *mMemory;
    size_t mSize;

private:
    size_t mMemoryBufferSize;
    AsyncHTTPRequest *mRequest;
};

#endif /* CURLWRAPPER_H_ */
