#ifndef PUSH_SERVICE_REGISTRATION_DS_H
#define PUSH_SERVICE_REGISTRATION_DS_H

#include <push-service.h>

class PushService
{
public:
    static void connect();
    static void disconnect();

    static push_service_connection_h pushConn;

private:
    static void _state_cb(push_service_state_e state, const char *err, void *user_data);
    static void _on_state_registered(void *user_data);
    static void _noti_cb(push_service_notification_h noti, void *user_data);

    enum Toggle_Modes {
        Off, On
    };

    static Toggle_Modes toggle;
    const static char *FacebookName;
};
#endif
