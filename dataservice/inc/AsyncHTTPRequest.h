#ifndef ASYNCHTTPREQUEST_H_
#define ASYNCHTTPREQUEST_H_

#include "Ecore.h"


class CurlWrapper;
/**
 * This class intended for sending async requests to facebook
 */

typedef void (*RequestCallback)(void* data, int curlCode, const char* respData, int resplen, const char* fileName);
typedef void (*CanceledCallback)(void* data);

class AsyncHTTPRequest {
public:

    AsyncHTTPRequest(const char *url, RequestCallback callback, CanceledCallback canceledCB, void* userData);
    virtual ~AsyncHTTPRequest();
    /**
     * @brief This method run a request to facebook in async thread
     * @return pointer to started async thread
     */
    bool ExecuteAsync();
    bool Cancel();
    void RemoveCallbacks();
    /**
     * This function is called in a non UI thread
     */
    int OnProgress(long int dltotal, long int dlnow, long int ultotal, long int ulnow);

private:
    void ThreadCompleted();
    void ThreadCanceled();

    //internal callbacks for multithreading
    /**
     * @brief  - This method runs in async thread
     * @param data[in] - user data passed to thread
     * @param thread[in] - thread
     */
    static void thread_run_cb(void *data, Ecore_Thread *thread);

    /**
     * @brief - This callback is called when async thread is finished
     * @param data[in] -user data
     * @param thread[in] - thread
     */
    static void thread_end_cb(void *data, Ecore_Thread *thread);

    /**
     * @brief - This callback is called when async thread should be cancelled
     * @param data[in] - user data
     * @param thread[in] - thread
     */
    static void thread_cancel_cb(void *data, Ecore_Thread *thread);

private:
    const char*          mUrl;
    const char*          mFileName;
    RequestCallback      mCallback;
    CanceledCallback     mCanceledCB;
    void *               mUserData;

    CurlWrapper* mCurlWrapper;
    //According to the Tizen docs there are 2 thread handlers. 1st used in the main thread and second one used from the worker thread.
    //As I can see from my experiments these 2 handlers are the same thing, but I created 2 handlers just in case.
    Ecore_Thread *mMainLoopThread;
    Ecore_Thread *mWorkerThread;
    int                  mCurlCode;
};

#endif /* ASYNCHTTPREQUEST_H_ */
