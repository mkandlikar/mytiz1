#ifndef PROTOCOLBASE_H_
#define PROTOCOLBASE_H_

#include "MessagePortTransport.h"
#include "map"

typedef unsigned int MessageId;
typedef unsigned int MessageType;
typedef unsigned int ErrorCode;

enum BaseProtocolRequestType {
    EBPCancel,
    EBPRequestTypeLast
};

enum BaseProtocolError{
    EBPErrNone,
    EBPErrMessagePortNotFound,
    EBPErrMessagePortError,
    EBPErrUnknown,
    EBPErrLast
};


/**
 * @brief This is an interface for protocol clients.
 * @details Concrete protocols shall provide its own interfaces derived from this interface.
 * Then these interfaces should be implemented by classes that intended to be protocol's clients.
 */
class ProtocolBase;

class IProtocolClient {
    friend class ProtocolBase;
public:
    IProtocolClient();
    virtual ~IProtocolClient(){}
    void Cancel(MessageId requestID);

private:
    void SetProtocol(ProtocolBase *protocol);

private:
    ProtocolBase *mProtocol;
};

/**
 * @brief Base class for classes which implement IProtocol interface. It implements basic Request-Response interactions.
 * @details
 * Terminology:
 * Message - any incoming or outgoing message passing through message port.
 * Request - Message which requires async response from other side.
 * Response - Message which is response on Request sent from other side.
 *
 * When couple of async requests are raised by application then it's needed to store information related to
 * each request until request is completed. Also, It's needed to associate each response
 * received when async request is completed with appropriate request. The ProtocolBase class in
 * conjunction with its helper RequestBase implements base functionality for such Request - Response support.
 * Also, this class provides helper functions which can be used by its successors.
  */
class ProtocolBase : public IProtocol {
public:
    ProtocolBase(const char *localPortName, const char *remoteAppId, const char *remotePortName);
    virtual ~ProtocolBase();
    void Cancel(MessageId requestId);

protected:
    /**
     * @brief This is a helper class. It's intended to store request info while async request is in progress.
     * @details This is a pure virtual helper class. The ProtocolBase uses classes derived from
     * the RequestBase to store information related to request while async request is in progress.
     * Each request can be identified by its Id and type. Also, it stores pointer to client
     * (IProtocolClient derived class) to call its methods when the async request is completed.
     */
    class RequestBase {
        friend class ProtocolBase;
    public:
        RequestBase(MessageType requestType, MessageId requestId, IProtocolClient *client);
        virtual ~RequestBase() = 0;

        MessageId GetId() const;
        IProtocolClient *GetClient() const;
        MessageType GetMessageType() const;
    private:
        const MessageId mId;
        IProtocolClient *mClient;
        const MessageType mMessageType;
    };

protected:
    virtual int SendMessage(bundle* abundle);

    virtual void DoHandleReceivedData(MessageType messageType, bundle* data) = 0;
    virtual void CancelRequest(MessageId requestId, MessageType requestType) = 0;

    void AddIntValueToBundle(bundle* data, const char* key, int value) const;
    int GetIntValueFromBundle(bundle* data, const char* key) const;

    bundle *CreateBundleForRequest(MessageType requestType);
    bundle *CreateBundleForCancel(RequestBase *request) const;
    bundle *CreateBundleForResponse(MessageType responseType, MessageId requestId) const;
    bundle *CreateBundle(MessageType messageType) const;
    void SetError(bundle* data, ErrorCode error) const;

    MessageType GetMessageType(bundle* data) const;
    MessageId GetMessageId(bundle* data) const;
    MessageId GetRequestId(bundle* data) const;
    MessageId GetCancelId(bundle* data) const;
    MessageId GetCancelMessageType(bundle* data) const;
    ErrorCode GetError(bundle* data) const;

    void AddRequest(RequestBase* request);
    void RemoveRequest(MessageId requestId);
    void clearAllPendingRequestOnLogout();
    RequestBase* FindRequest(MessageId requestId);
    void Cancel(RequestBase *request);

private:
    MessageId GenereateMessageId();
    virtual void HandleReceivedData(bundle* data) override;

private:
    MessageId mLastMessageId;
    std::map <MessageId, RequestBase*> mRequests;

    static const char* mKeyMessageType;
    static const char* mKeyMessageId;
    static const char* mKeyCancelId;
    static const char* mKeyCancelMessageType;
    static const char* mKeyRequestId;
    static const char* mKeyError;
    static const int mMaxRequestNumber;
};


#endif /* PROTOCOLBASE_H_ */
