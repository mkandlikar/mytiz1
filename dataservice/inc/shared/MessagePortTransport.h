#ifndef MESSAGEPORTTRANSPORT_H_
#define MESSAGEPORTTRANSPORT_H_

#include <message_port.h>

class IProtocol;

/**
 * @brief This class intended to manage Message Port operations
 * @details This class intended to manage such Message Port operations as register local port, send messages to remote port.
 * This is a C++ wrapper for Tizen message port. Trusted message port connection is used for communication.
 * Local port and remote port are configured by upper layer protocol (IProtocol).
 * So, it is not intended to be used without the upper layer protocol. It's why its constructor is private.
 */
class MessagePortTransport {
    friend class IProtocol;
public:
    ~MessagePortTransport();

    bool OpenPort(); //AAA ToDo: Move this function to private. Now it's non-private because it's called from DataServiceProtocol::SendMessage().

    int SendMessageWithLocalPort(bundle *message);
    int SendMessage(bundle *message);

private:
    MessagePortTransport(const char *localPortName, const char *remoteAppId, const char *remotePortName);
    void AttachProtocol(IProtocol* protocol);

    static void DataReceived(int localPortId, const char *remoteId, const char *remPort, bool trustedRemotePort, bundle *message, void *userData);
    bool CheckRemotePort() const;
    bool ClosePort();

private:
    int mLocalPortId;
    const char *mRemoteAppId;
    const char *mRemotePortName;
    const char *mLocalPortName;
    IProtocol* mProtocol;
};

/**
 * @brief This class represents interface for MessagePortTransport's upper layer protocol.
 * @details This is a pure virtual class represents interface for MessagePortTransport's upper layer protocol.
 * Implementing this interface derived classes are able to configure MessagePortTransport,
 * send outgoing messages and receive incoming messages.
 * This interface is tightly binded with MessagePortTransport. It creates MessagePortTransport instance
 * on construction and bind itself to the MessagePortTransport.
 * So, classes which implements this interface are intended to be used only in conjunction
 * with MessagePortTransport and can't be used separately.
 */
class IProtocol {
protected:
    IProtocol(const char *localPortName, const char *remoteAppId, const char *remotePortName);
    virtual ~IProtocol(){}

public:
    virtual void HandleReceivedData(bundle* data) = 0;

protected:
    MessagePortTransport mMPT;
};

#endif /* MESSAGEPORTTRANSPORT_H_ */
