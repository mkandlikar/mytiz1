#ifndef DATASERVICEPROTOCOLCOMMON_H_
#define DATASERVICEPROTOCOLCOMMON_H_

#include "ProtocolBase.h"

enum DSRequestType {
    EDownloadImageRequest = EBPRequestTypeLast + 1,
    EDownloadImageResponse,
    EShutdownRequest,
    EConnectToPushRequest,
    EDeleteUserData,
    EDeleteOldUserData,
    EAppGoToBG
};


enum DataServiceProtocolError{
    EDSErrNetworkNotAvailable = EBPErrLast + 1,
    EDSErrServiceNotReady,
    EDSErrUrlNotFound,
    EDSErrFileDownloadError,
    EDSErrFileWriteError,
    EDSErrBadParameter,
    EDSErrUnknown
};

const char* const KUrl = "url";
const char* const KFileName = "filename";

/*
 * Macro for test purposes. Uncomment if you need to test message port with fake commands. It should be removed when real functionality will be implemented.
 */
//#define DATA_SERVICE_TEST

#endif /* DATASERVICEPROTOCOLCOMMON_H_ */
