#include "Common.h"
#include "Utils.h"

#include <app.h>
#include <dlog.h>
#include <stdio.h>
#include <stdlib.h>
#include <system_info.h>
#include <system_settings.h>
#include <telephony.h>

char* Utils::GetIconNameFromUrl(const char* url){
    char *result = NULL;
    if (url != NULL) {
        std::string input = url;
        std::string img_ext;

        unsigned int start_img_pos = 0;
        unsigned int end_img_pos = input.length();
        unsigned int pos = 0;
        int MAX_FILE_NAME_LENGTH = 57;

        int hash = eina_hash_superfast (url, end_img_pos);

        if ((input.find("%")!= std::string::npos)){
            Utils::ReplaceString(input, "%2F", "/");
            Utils::ReplaceString(input, "%3A", ":");
            Utils::ReplaceString(input, "%3D", "=");
            Utils::ReplaceString(input, "%3F", "?");
            Utils::ReplaceString(input, "%2C", ",");
            Utils::ReplaceString(input, "%26", "&");
            Utils::ReplaceString(input, "%23", "#");
        }

        if ((input.find(".jpg")!= std::string::npos)){
            img_ext = ".jpg";
        } else if (input.find(".png")!= std::string::npos){
            img_ext = ".png";
        } else if (input.find(".jpeg")!= std::string::npos){
            img_ext = ".jpeg";
        } else if (input.find(".mp4")!= std::string::npos) {
            img_ext = ".mp4";
        } else {
            img_ext = ".gif";
        }

        if (input.find(img_ext) == std::string::npos){
            img_ext = "#";
        }

        pos = input.rfind("/");
        input.erase(0,pos+1);

        if((input.find(img_ext) + img_ext.length()) != end_img_pos){
            end_img_pos = input.find(img_ext) + img_ext.length();
        }

        char buffer[33];
        Utils::Snprintf_s(buffer, 32, "%x_", hash);
        buffer[32] = 0;

        input = buffer + input.substr(start_img_pos,end_img_pos);

        if(input.length()>MAX_FILE_NAME_LENGTH){
            input = input.substr(0,MAX_FILE_NAME_LENGTH);
            input = input + ".jpg";
        };
        result = strdup(input.c_str());
        dlog_print(DLOG_DEBUG, LOG_TAG, "Utils::GetIconNameFromUrl, result = %s", input.c_str());
    }
    return result;
}

void Utils::ReplaceString(std::string &str, const std::string &searchStr, const std::string &replaceStr) {
    if (str.empty() || searchStr.empty() || replaceStr.empty()) {
        return;
    }
    unsigned int pos = 0;
    while ((pos = str.find(searchStr, pos)) != std::string::npos) {
        str.replace(pos, searchStr.length(), replaceStr);
        pos += replaceStr.length();
    }
    return;
}

// This function set the user-agent header to be set in the requests that are sent to server
// returns User-agent string;
const char *InitHTTPUserAgent() {
    //Set the User Agent to be sent in the HTTP header. The format is as follows
    //{Browser UA} [FBAN/{App Name};FBAV/{App Version};FBBV/{App Build Number};FBMF/{Device Manufacturer};
    //FBDV/{Device Model};FBSN/{OS Name};FBSV/{OS Version};FBCR/{Carrier};FBLC/{Locale};FBDM/{Display Metrics}]

    //Retreive the information necessary to build the HTTP UA header using system info API calls
    static char user_agent_string[MAX_UA_LENGTH] = {0};
    int ret,tret, dpi, height, width;

    //Get the Platform Version
    char * platform_version;
    ret = system_info_get_platform_string("http://tizen.org/feature/platform.version", &platform_version);
    if(ret == SYSTEM_INFO_ERROR_NONE) {
        dlog_print(DLOG_DEBUG, LOG_TAG, "Platform version =%s", platform_version);
    } else {
        platform_version = strdup(defPlatformVersion);
    }

    //Get the DPI
    ret = system_info_get_platform_int("http://tizen.org/feature/screen.dpi", &dpi);
    if(ret == SYSTEM_INFO_ERROR_NONE) {
        dlog_print(DLOG_DEBUG, LOG_TAG, "Screen DPI =%d", dpi);
    } else {
        dpi = 0;
    }

    //get the Height
    ret = system_info_get_platform_int("http://tizen.org/feature/screen.height", &height);
    if(ret == SYSTEM_INFO_ERROR_NONE) {
        dlog_print(DLOG_DEBUG, LOG_TAG, "Screen Height =%d", height);
    } else {
        height = 0;
    }

    //get the width
    ret = system_info_get_platform_int("http://tizen.org/feature/screen.width", &width);
    if(ret == SYSTEM_INFO_ERROR_NONE) {
        dlog_print(DLOG_DEBUG, LOG_TAG, "Screen Width =%d", width);
    } else {
        width = 0;
    }

    //Get the Model name
    char *model_name;
    ret = system_info_get_platform_string("http://tizen.org/system/model_name", &model_name);
    if(ret == SYSTEM_INFO_ERROR_NONE) {
        dlog_print(DLOG_DEBUG, LOG_TAG, "Model Name =%s", model_name);
    } else {
        model_name = strdup(defModel);
    }

    //Get the Platform Name
    char * platform_name;
    ret = system_info_get_platform_string("http://tizen.org/system/platform.name", &platform_name);
    if(ret == SYSTEM_INFO_ERROR_NONE) {
        dlog_print(DLOG_DEBUG, LOG_TAG, "Platform Name =%s", platform_name);
    } else {
        platform_name = strdup(defPlatform);
    }

    //Get the manufacturer Name
    char * manuf_name;
    ret = system_info_get_platform_string("http://tizen.org/system/manufacturer", &manuf_name);
    if(ret == SYSTEM_INFO_ERROR_NONE) {
        dlog_print(DLOG_DEBUG, LOG_TAG, "Manufacturer =%s", manuf_name);
    } else {
        manuf_name = strdup(defMenufacturerName);
    }

    //Get the Locale
    char * locale;
    ret = system_settings_get_value_string (SYSTEM_SETTINGS_KEY_LOCALE_LANGUAGE, &locale);
    if(ret == SYSTEM_SETTINGS_ERROR_NONE ) {
        dlog_print(DLOG_DEBUG, LOG_TAG, "Locale =%s", locale);
    } else {
        locale = strdup(defLocale);
    }

    //Application version detials
    char * app_version;
    tret =  app_get_version (&app_version);
    if(tret != APP_ERROR_NONE) {
        //set the APP version as 0.0.0
        app_version = strdup(defAppVersion);
    }

    //Carrier
    char *mcc, *mnc;
    Utils::GetMCC_MNC(&mcc,&mnc);

    // An example of user-ageent string is given below:
    //Mozilla/5.0 (Linux; Tizen 2.3; SAMSUNG SM-Z130H) AppleWebKit/537.3 (KHTML, like Gecko) Version/2.3
    //Mobile Safari/537.3 [FBAN/FB4T;FBAV/1.0;FBBV/15;FBMF/Samsung;FBDV/Z3;FBSN/Tizen;FBSV/2.3;
    //FBCR/310-012;FBLC/en_US;FBCM/{density=3.0,width=720,height=1080};]

    char ua_agent_part1[MAX_UA_LENGTH/2] = {0};
    char ua_agent_part2[MAX_UA_LENGTH/2] = {0};
    char ua_agent_part3[MAX_UA_LENGTH/2] = {0};

    Utils::Snprintf_s(ua_agent_part1, MAX_UA_LENGTH/2, "Mozilla/5.0 (Linux; Tizen %s; %s %s) AppleWebKit/537.3 (KHTML, like Gecko) Version/%s Mobile Safari/537.3", platform_version, manuf_name, model_name, platform_version);
    Utils::Snprintf_s(ua_agent_part2, MAX_UA_LENGTH/2, "FBAN/FB4T;FBAV/%s;FBBV/%s;FBMF/%s;FBDV/%s;FBSN/Tizen", app_version, app_version, manuf_name, model_name);
    Utils::Snprintf_s(ua_agent_part3, MAX_UA_LENGTH/2, "FBSV/%s;FBCR/%s-%s;FBLC/%s;FBCM/{density=%d,width=%d,height=%d}", platform_version, mcc, mnc, locale, dpi, width, height);
    assert(strlen(ua_agent_part1) + strlen(ua_agent_part2) + strlen(ua_agent_part3) + 5 < MAX_UA_LENGTH);  /* 5 characters for " [;;]" */

    Utils::Snprintf_s(user_agent_string, MAX_UA_LENGTH, "%s [%s;%s;]", ua_agent_part1, ua_agent_part2, ua_agent_part3);
    dlog_print(DLOG_DEBUG, LOG_TAG, "UserAgent='%s'", user_agent_string);

    //Free the allocated strings
    free(platform_version);
    free(model_name);
    free(platform_name);
    free(manuf_name);
    free(locale);
    free(app_version);
    free(mcc);
    free(mnc);
    return user_agent_string;
}

const char * Utils::GetHTTPUserAgent() {
    static const char *dataS_http_user_agent = InitHTTPUserAgent();
    return dataS_http_user_agent;
}

bool Utils::GetMCC_MNC(char **mcc, char **mnc) {
    telephony_handle_list_s handle_list;

    int ret;
    int l_sim_num;

    *mcc = NULL;
    *mnc = NULL;
    bool result = false;

    ret = telephony_init(&handle_list);// In case of single SIM, we get only one handle

    if (ret == TELEPHONY_ERROR_NONE) {
        for(l_sim_num=0;l_sim_num<handle_list.count;l_sim_num++) {
            if(handle_list.handle[l_sim_num] != NULL)
                break;
        }
        if(l_sim_num != handle_list.count ) {
            //Fetch the MCC and MNC of the first found SIM
            //Get the SIM state
            telephony_sim_state_e l_state;
            ret = telephony_sim_get_state(handle_list.handle[l_sim_num], &l_state);
            if(ret == TELEPHONY_ERROR_NONE) {
                //If the SIM state is active get the mcc and mnc details
                if(l_state == TELEPHONY_SIM_STATE_AVAILABLE) {
                	result = true;
                    ret = telephony_network_get_mcc(handle_list.handle[l_sim_num], mcc);
                    if(ret != TELEPHONY_ERROR_NONE) {
                        *mcc = strdup(defMCC);
                    }
                    ret = telephony_network_get_mnc(handle_list.handle[l_sim_num], mnc);
                    if(ret != TELEPHONY_ERROR_NONE) {
                        *mnc = strdup(defMNC);
                    }
                }
            }
        }
    }//Get MCC and MNC details

    if(!result){
        //Set the MCC and MNC as 0
        if(!*mcc) *mcc = strdup(defMCC);
        if(!*mnc) *mnc = strdup(defMNC);
    }
    return result;
}

long int Utils::GetUnixTime_inSec() {
    long int timer;
    if (time(&timer) == -1) {
        return 0;
    }
    return timer;
}

int Utils::Memcpy_s(void *dst, size_t sizeDst, const void *src, size_t sizeSrc) {
    if(sizeDst >= sizeSrc && dst != NULL && src != NULL) {
        memcpy(dst, src, sizeSrc);
        return 1;
    } else {
        return 0;
    }
}

int Utils::Snprintf_s(char *buffer, size_t count, const char *format, ...) {

    va_list argptr;
    va_start(argptr, format);
    int res = vsnprintf(buffer, count, format, argptr);
    va_end(argptr);

    if (res > count)
        res = 0;

    assert(res);
    return res;
}
