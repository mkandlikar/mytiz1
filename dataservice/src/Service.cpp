#include "FacebookAppProtocol.h"
#include "Log.h"
#include "Service.h"
#include "Utils.h"

#include <assert.h>
#include <curl/curl.h>
#include <notification.h>
#include <service_app.h>


Service::Service() : mFbAppPr(NULL) {
    Log::info("Service::Service");
    Init();
}

Service::~Service() {
    Log::info("Service::~Service");
}

void Service::Init() {
    mFbAppPr = NULL;
    app_event_handler_h handlers[5] = {NULL, };

    service_app_add_event_handler(&handlers[APP_EVENT_LOW_BATTERY], APP_EVENT_LOW_BATTERY, service_app_low_battery, this);
    service_app_add_event_handler(&handlers[APP_EVENT_LOW_MEMORY], APP_EVENT_LOW_MEMORY, service_app_low_memory, this);
    service_app_add_event_handler(&handlers[APP_EVENT_DEVICE_ORIENTATION_CHANGED], APP_EVENT_DEVICE_ORIENTATION_CHANGED, service_app_orient_changed, this);
    service_app_add_event_handler(&handlers[APP_EVENT_LANGUAGE_CHANGED], APP_EVENT_LANGUAGE_CHANGED, service_app_lang_changed, this);
    service_app_add_event_handler(&handlers[APP_EVENT_REGION_FORMAT_CHANGED], APP_EVENT_REGION_FORMAT_CHANGED, service_app_region_changed, this);
}

int Service::Run(int argc, char *argv[]) {
service_app_lifecycle_callback_s event_callback = {0,};

    event_callback.create = service_app_create;
    event_callback.terminate = service_app_terminate;
    event_callback.app_control = service_app_control;

    return service_app_main(argc, argv, &event_callback, this);
}

bool Service::service_app_create(void *data)
{
    Log::info("Service::service_app_create()");
    /* Initialize libcurl */
    curl_global_init(CURL_GLOBAL_ALL);

    Service *service = static_cast<Service*>(data);

    if(service) {
        service->mFbAppPr = new FacebookAppProtocol();
    }

    //Set the HTTPUSER AGENT to be used in subsequent requests
    Utils::GetHTTPUserAgent();

#ifdef DATA_SERVICE_TEST
    Log::info("FB DATASERVICE");
// MEMORY LEAK: delete isn't never called for pointers below because it's just test code.
    AppControl* ac = new AppControl();
    fap->SetAppControl(ac);
#endif
    ecore_thread_max_set(4);
    return true;
}

void Service::service_app_terminate(void *data)
{
    Log::info("Service::service_app_terminate()");

    //Package is uninstalled, so clean notifications owned by dataservice
    notification_delete_all(NOTIFICATION_TYPE_NOTI);

    Service * me = (Service*) data;
    assert(me);
    delete me->mFbAppPr;
    me->mFbAppPr = NULL;

    /* we're done with libcurl, so clean it up */
    curl_global_cleanup();

    return;
}

void Service::service_app_control(app_control_h app_control, void *data)
{
    Log::info("Service::service_app_control()");
    // Todo: add your code here.
    return;
}

void
Service::service_app_lang_changed(app_event_info_h event_info, void *user_data)
{
    /*APP_EVENT_LANGUAGE_CHANGED*/
    return;
}

void
Service::service_app_orient_changed(app_event_info_h event_info, void *user_data)
{
    /*APP_EVENT_DEVICE_ORIENTATION_CHANGED*/
    return;
}

void
Service::service_app_region_changed(app_event_info_h event_info, void *user_data)
{
    /*APP_EVENT_REGION_FORMAT_CHANGED*/
}

void
Service::service_app_low_battery(app_event_info_h event_info, void *user_data)
{
    /*APP_EVENT_LOW_BATTERY*/
}

void
Service::service_app_low_memory(app_event_info_h event_info, void *user_data)
{
    /*APP_EVENT_LOW_MEMORY*/
}
