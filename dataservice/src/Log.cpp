/*
 * Log.cpp
 *
 *  Created on: Nov 2, 2015
 *      Author: rupandreev
 */

#include "Log.h"

const char* Log::TAG = "dataservice";


void Log::info(const char *msg...)
{
#ifndef _RELEASE_NO_PRINTS
    va_list list;
    va_start(list, msg);
    write( DLOG_INFO, list, msg );
    va_end(list);
#endif
}

void Log::debug(const char *msg...)
{
#ifndef _RELEASE_NO_PRINTS
    va_list list;
    va_start(list, msg);
    write( DLOG_DEBUG, list, msg );
    va_end(list);
#endif
}

void Log::warning(const char *msg...)
{
#ifndef _RELEASE_NO_PRINTS
    va_list list;
    va_start(list, msg);
    write( DLOG_WARN, list, msg );
    va_end(list);
#endif
}

void Log::error(const char *msg...)
{
#ifndef _RELEASE_NO_PRINTS
    va_list list;
    va_start(list, msg);
    write( DLOG_ERROR, list, msg );
    va_end(list);
#endif
}

void Log::write( log_priority prio, va_list list, const char *msg )
{
	dlog_vprint( prio, TAG, msg, list );
}
