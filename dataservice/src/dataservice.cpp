#include <tizen.h>
#include <service_app.h>
#include <dlog.h>

#include "Common.h"
#include "dataservice.h"
#include "Service.h"

int
main(int argc, char *argv[])
{
    Service srv;
    int ret = 0;

    ret = srv.Run(argc, argv);
    if (ret != APP_ERROR_NONE) {
        dlog_print(DLOG_ERROR, LOG_TAG, "service_app_main() is failed. err = %d", ret);
    }

    return ret;
}
