#include "AsyncHTTPRequest.h"
#include "FileDownloader.h"
#include "Log.h"

#include <app_common.h>
#include <cassert>
#include <curl/curl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include "Utils.h"

#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "Facebook_FileDownloader"
#define DIR_IMAGES "Images"
#define DIR_VIDEOS "Videos"
#define VIDEO_EXT_MP4 ".mp4"

typedef struct download_data_struct {
    FileDownloader* me;
    MessageId requestId;
} download_data;

void FileDownloader::DownloadImage(MessageId requestId, const char* url) {
    download_data *data = (download_data *) calloc(1, sizeof(download_data));
    data->requestId = requestId;
    data->me = this;
    AsyncHTTPRequest *request = new AsyncHTTPRequest(url, on_get_download_image_completed, on_cancelation_completed, data);
    if (request->ExecuteAsync()) {
        mRequests[requestId] = request;
    } else {
        //AAA ToDo: we have to notify client from here immediately if an error occurs.
        delete request;
    }
}
FileDownloader::~FileDownloader() {
    for(auto iterator = mRequests.begin(); iterator != mRequests.end(); ++iterator) {
        iterator->second->RemoveCallbacks();
        iterator->second->Cancel();
    }
}

void FileDownloader::CancelDownloadImage(MessageId requestId) {
    AsyncHTTPRequest* request = FindRequest(requestId);
    if (request && request->Cancel()) {
        RemoveRequest(request, requestId);
    }
}

void FileDownloader::on_get_download_image_completed(void* data, int curlCode, const char* respData, int respLen, const char* fileName) {
    download_data *dData = static_cast<download_data*> (data);
    assert(dData);
    FileDownloader *me = dData->me;
    assert(me);
    char *fullPath = nullptr;
    int err = EBPErrNone;

    if (curlCode == CURLE_OK) {
        //Log::info("File download operation is completed successfully.");

        //Prior to saving a file, examine the 'fileName' and strip out directories if any, leave only a file name itself.
        const char *onlyFileName = strrchr(fileName, '/');    //look for a path separator
        if (onlyFileName) {
            onlyFileName++;
        } else {
            onlyFileName = fileName;
        }

        char *dirName = nullptr;
        char *sharedPath = app_get_shared_trusted_path();
        int fullPathLength = strlen(sharedPath) + 1;    //one byte for terminating '\0'
        if (strstr(onlyFileName, VIDEO_EXT_MP4)) {
            fullPathLength += strlen(DIR_VIDEOS);
            dirName = (char*)malloc(fullPathLength);
            Utils::Snprintf_s(dirName, fullPathLength, "%s%s", sharedPath, DIR_VIDEOS);
        } else {
            fullPathLength += strlen(DIR_IMAGES);
            dirName = (char*)malloc(fullPathLength);
            Utils::Snprintf_s(dirName, fullPathLength, "%s%s", sharedPath, DIR_IMAGES);
        }
        free(sharedPath);

        struct stat st = {0};
        if (stat(dirName, &st) == -1) {
            mkdir(dirName, 0740);
        }
        fullPathLength += 1 + strlen(onlyFileName);    //one byte for '/' separator
        fullPath = (char*)malloc(fullPathLength);
        Utils::Snprintf_s(fullPath, fullPathLength, "%s/%s", dirName, onlyFileName);
        free(dirName);

        FILE *file = fopen(fullPath, "r");// check that the file is already exist
        if (!file) {
            char *fullPathTmp = (char*)malloc(fullPathLength + 4);    //four bytes for ".tmp" suffix
            eina_strlcpy(fullPathTmp, fullPath, fullPathLength + 4);
            eina_strlcat(fullPathTmp, ".tmp", fullPathLength + 4);

            file = fopen(fullPathTmp, "w+");
            if (file) {
               if (respData) {
                   fwrite(respData, 1, respLen, file);
                }
                fclose(file);
                rename(fullPathTmp, fullPath);
                Log::info("FileDownloader::on_get_download_image_completed->create file = %s", fullPath);
            } else {
                err = EDSErrFileWriteError;
            }
            free(fullPathTmp);
        } else {
            Log::info("FileDownloader::on_get_download_image_completed->file is already created = %s", fullPath);
        }
    } else {
        Log::error("File download operation is completed with error: curlCode = %d", curlCode);
        err = EDSErrFileDownloadError;
    }

    me->mFacebookAppProtocol->DownloadImageCompleted(err, dData->requestId, (err == EBPErrNone ? fullPath : ""));
    free(fullPath);

    AsyncHTTPRequest* request = me->FindRequest(dData->requestId);
    if (request) {
        me->RemoveRequest(request, dData->requestId);
    }

    free(dData);
}

void FileDownloader::on_cancelation_completed(void* data) {
    download_data *dData = static_cast<download_data*> (data);
    assert(dData);
    FileDownloader *me = dData->me;
    assert(me);
    AsyncHTTPRequest* request = me->FindRequest(dData->requestId);
    if (request) {
        me->RemoveRequest(request, dData->requestId);
    }
    free(dData);
}


AsyncHTTPRequest* FileDownloader::FindRequest(MessageId requestId) const {
    auto it = mRequests.find(requestId);
    return (it != mRequests.end() ? it->second : nullptr);
}

void FileDownloader::RemoveRequest(AsyncHTTPRequest* request, MessageId requestId) {
    assert(request);
    mRequests.erase(requestId);
    delete request;
}
