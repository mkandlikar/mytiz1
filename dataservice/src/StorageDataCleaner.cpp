#include <app_common.h>
#include <app_preference.h>
#include <dirent.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <unistd.h>

#include "StorageDataCleaner.h"
#include "Utils.h"


const int StorageDataCleaner::LifetimeOfUserData = 604800;//7 day
const int StorageDataCleaner::CleaningPeriod = 259200;//3 day
const long int StorageDataCleaner::MaxSizeUserData = 209715200;//200 Mb
const long int StorageDataCleaner::NormalSizeUserData = 104857600;//100 Mb
const char * StorageDataCleaner::PreferenceField = "last_clear_old_user_data";

StorageDataCleaner::StorageDataCleaner() : mTimer(nullptr), mFiles(nullptr) {}

StorageDataCleaner::~StorageDataCleaner() {
    if (mTimer)
        ecore_timer_del(mTimer);

    void * data;
    EINA_LIST_FREE(mFiles,data){
        free(data);
    }
}

int StorageDataCleaner::ViewDirectory(const char * path, ViewMode Mode) {
    DIR *d = opendir(path);
    int res = -1, res2 = -1;
    if (d) {
        size_t pathLen = strlen(path);
        struct dirent *p;
        res = 0;
        while (!res && (p = readdir(d))) {
            if (!strcmp(p->d_name, ".") || !strcmp(p->d_name, "..")) {
                continue;
            }
            size_t len = pathLen + strlen(p->d_name) + 2;
            char *buf = (char*) malloc(len);
            if (buf) {
                struct stat statBuf;
                Utils::Snprintf_s(buf, len, "%s/%s", path, p->d_name);
                if (!stat(buf, &statBuf)) {
                    if (S_ISDIR(statBuf.st_mode)) {
                        res2 = ViewDirectory(buf, Mode);
                    } else if (Mode == EDeleteAllFiles || (Mode == EDeleteOldFiles && IsOldFile(statBuf.st_atim.tv_sec))) {
                        res2 = unlink(buf);
                    } else if (Mode == EFindFiles) {
                        mFiles = eina_list_append(mFiles, strdup(buf));
                    }
                }
                free(buf);
            }
            res = res2;
        }
        closedir(d);
    }
    if (!res && Mode == EDeleteAllFiles) {
        res = rmdir(path);
    }
    return res;
}

void StorageDataCleaner::RemoveUserDataFromStorage() {
    ViewDirectory(app_get_shared_trusted_path(), EDeleteAllFiles);
}

void StorageDataCleaner::RemoveOldUserDataFromStorage() {
    preference_set_double(PreferenceField, Utils::GetUnixTime_inSec());
    ViewDirectory(app_get_shared_trusted_path(), EDeleteOldFiles);
}

void StorageDataCleaner::StartClearTimer() {
    long int diff = Utils::GetUnixTime_inSec() - GetLastClearTime();
    if (mTimer) {
        ecore_timer_del(mTimer);
    }

    mTimer = ecore_timer_add(diff > CleaningPeriod || diff < 0 ? 1 : CleaningPeriod - diff, time_to_delete_user_data_cb, this);
}

Eina_Bool StorageDataCleaner::time_to_delete_user_data_cb(void *data) {
    StorageDataCleaner* cleaner = static_cast<StorageDataCleaner*>(data);
    cleaner->RemoveOldUserDataFromStorage();
    cleaner->StartClearTimer();
    return EINA_FALSE;
}

long int StorageDataCleaner::GetLastClearTime() {
    double time = 0;
    bool isExist = false;
    if ((preference_is_existing(PreferenceField, &isExist) == 0) && isExist) {
        preference_get_double(PreferenceField, &time);
    } else {
        preference_set_double(PreferenceField, time);
    }
    return time;
}

bool StorageDataCleaner::IsOldFile(long int accessTime) {
    return Utils::GetUnixTime_inSec() - accessTime > LifetimeOfUserData;
}

long int StorageDataCleaner::GetSizeOfUserData(){
    struct stat statBuf;
    return !stat(app_get_shared_trusted_path(), &statBuf) ? statBuf.st_size : -1;
}

void StorageDataCleaner::CheckSizeOfUserData() {
    if (GetSizeOfUserData() > MaxSizeUserData)
        NormalizeSizeOfUserData();
}

void StorageDataCleaner::NormalizeSizeOfUserData(){
    Eina_List *l;
    void *element = NULL;
    long int DataSize = GetSizeOfUserData();
    struct stat statBuf;

    ViewDirectory(app_get_shared_trusted_path(),EFindFiles);
    mFiles = eina_list_sort(mFiles, 0, sort_cb);
    EINA_LIST_REVERSE_FOREACH(mFiles, l, element) {
        const char * path = static_cast<const char *>(element);
        stat(path, &statBuf);
        DataSize -= statBuf.st_size;
        if (unlink(path) != 0 || DataSize <= NormalSizeUserData)
            break;
    }
}

int StorageDataCleaner::sort_cb(const void *d1, const void *d2){

    const char * path1 = static_cast<const char *>(d1);
    const char * path2 = static_cast<const char *>(d2);
    struct stat statBuf1;
    struct stat statBuf2;
    stat(path1, &statBuf1);
    stat(path2, &statBuf2);

    return statBuf1.st_atim.tv_sec > statBuf2.st_atim.tv_sec;
}
