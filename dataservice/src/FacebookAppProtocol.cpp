#include "FacebookAppProtocol.h"
#include "FileDownloader.h"
#include "Log.h"
#include "PushService.h"

#include <notification.h>
#include <service_app.h>

#define remote_app_id "com.facebook.tizen"
#define remote_port "UIPort"
#define local_port "servicePort"

#define LOG_PREFIX  "FbAppProtocol:"


FacebookAppProtocol::FacebookAppProtocol() : ProtocolBase(local_port, remote_app_id, remote_port) {
    mFileDownloader = new FileDownloader(this);
    mAppControl = NULL;
    mDataCleaner = new StorageDataCleaner();
}

FacebookAppProtocol::~FacebookAppProtocol() {
    delete mFileDownloader;
    delete mDataCleaner;
    mAppControl = NULL;
}

void FacebookAppProtocol::SetAppControl(IAppControl * appControl) {
    mAppControl = appControl;
}

/**
* @brief             Implements callback function from the ProtocolBase.
*/
void FacebookAppProtocol::DoHandleReceivedData(MessageType messageType, bundle* data) {
    Log::info(LOG_PREFIX "DoHandleReceivedData(): Received message of type=%d", messageType);
    switch (messageType) {
    case EDownloadImageRequest: {
        MessageId messageId = GetMessageId(data);
        Log::info(LOG_PREFIX "DoHandleReceivedData(): Received EDownloadImageRequest with request id=%d", messageId);
        DownloadImage(messageId, data);
        break;
    }
    case EShutdownRequest:
        ShutDown();
        break;
    case EConnectToPushRequest:  //the UI-application has got forcibly closed, need to take over push control from it
        ConnectToPush();
        break;
    case EDeleteUserData:
        //UI-app logs out, so clean notifications owned by dataservice
        notification_delete_all(NOTIFICATION_TYPE_NOTI);
        mDataCleaner->RemoveUserDataFromStorage();
        break;
    case EDeleteOldUserData:
        mDataCleaner->StartClearTimer();
        break;
    case EAppGoToBG:
        mDataCleaner->CheckSizeOfUserData();
        break;
    default:
        Log::error(LOG_PREFIX "DoHandleReceivedData(): Received message of unknown type=%d", messageType);
        break;
    }
}

/**
* @brief             Called when EDownloadImageRequest is received.
*/
void FacebookAppProtocol::DownloadImage(MessageId requestId, bundle *request) {
    char *url = nullptr;
    bundle_get_str(request, KUrl, &url);
    Log::info(LOG_PREFIX "DownloadImage(%s)", url);
    if (url && *url != '\0') {
        if (mFileDownloader) {
            mFileDownloader->DownloadImage(requestId, url);
        } else {
            DownloadImageCompleted(EDSErrServiceNotReady, requestId, "");
        }
    } else {
        Log::error(LOG_PREFIX "Url is empty!");
        DownloadImageCompleted(EDSErrBadParameter, requestId, "");
    }
}

/**
* @brief             Called when request to download image is completed.
*/
void FacebookAppProtocol::DownloadImageCompleted(int error, MessageId requestId, const char* fileName) {
    bundle *b = CreateBundleForResponse(EDownloadImageResponse, requestId);
    bundle_add_str (b, KFileName, fileName);
    SetError(b, error);
    SendMessage(b);
    bundle_free (b);
}

void FacebookAppProtocol::CancelRequest(MessageId requestId, MessageType requestType) {
    switch (requestType) {
    case EDownloadImageRequest:
    {
        CancelDownloadImage(requestId);
        break;
    }
    default:
        break;
    }
}

void FacebookAppProtocol::CancelDownloadImage(MessageId requestId) {
    Log::info("Cancel DownloadImage received for requestId:%d", requestId);
    if (mFileDownloader) {
        mFileDownloader->CancelDownloadImage(requestId);
    }
}

 /**
 * @brief             Called when EShutdownRequest is received.
 */
void FacebookAppProtocol::ShutDown() {
    Log::info("ShutDown command received");
    if (mAppControl != NULL) {
        mAppControl->HandleShutdownCommand();
    }
}

/**
* @brief             Called when EConnectToPushRequest is received.
*/
void FacebookAppProtocol::ConnectToPush() {
    Log::info(LOG_PREFIX "ConnectToPush command received");
    PushService::connect();  //re-connect with the Service-application's callbacks
}


#ifdef DATA_SERVICE_TEST
    void AppControl::HandleShutdownCommand() {
        service_app_exit();
    }
#endif
