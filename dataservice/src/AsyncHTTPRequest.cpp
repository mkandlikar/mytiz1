#include "AsyncHTTPRequest.h"
#include "CurlWrapper.h"
#include "Log.h"
#include "Utils.h"

#include "cassert"

#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "AsyncHTTPRequest"


AsyncHTTPRequest::AsyncHTTPRequest(const char *url, RequestCallback callback, CanceledCallback canceledCB, void* userData) {
    mFileName = nullptr;
    assert(url);
    mUrl = strdup(url);
    mCurlWrapper = nullptr;
    mCallback = callback;
    mCanceledCB = canceledCB;
    mUserData = userData;
    mMainLoopThread = nullptr;
    mWorkerThread = nullptr;
    mCurlCode = -1;
}

AsyncHTTPRequest::~AsyncHTTPRequest() {
    free((void*) mUrl);
    free((void*) mFileName);
    delete mCurlWrapper;
}

/**
 * @brief This method run a request to facebook in async thread
 * @return pointer to started async thread
 */
bool AsyncHTTPRequest::ExecuteAsync() {
    //Run a thread
    mMainLoopThread = ecore_thread_run(thread_run_cb, thread_end_cb, thread_cancel_cb, this);
    return mMainLoopThread;
}

bool AsyncHTTPRequest::Cancel() {
    bool res = ecore_thread_cancel(mMainLoopThread);
    if (res) { //It seems that res is never true, but we handle this case anyway.
        mMainLoopThread = nullptr;
        mWorkerThread = nullptr;
    }
    return res;
}

void AsyncHTTPRequest::RemoveCallbacks() {
    mCallback = nullptr;
    mCanceledCB = nullptr;
}

//Note: this function is always called from worker thread.
int AsyncHTTPRequest::OnProgress(long int dltotal, long int dlnow, long int ultotal, long int ulnow) {
    assert(mWorkerThread);
    int ret = ecore_thread_check(mWorkerThread) ? 1 : 0;
    return ret;
}

/**
 * @brief  - This method runs in async thread
 * @param data[in] - user data passed to thread
 * @param thread[in] - thread
 */
void AsyncHTTPRequest::thread_run_cb(void *data, Ecore_Thread *thread)
{
    AsyncHTTPRequest *me = static_cast<AsyncHTTPRequest *> (data);
    assert(me);
    me->mWorkerThread = thread;
    me->mCurlWrapper = new CurlWrapper(me);
    me->mCurlCode = me->mCurlWrapper->SendHttpGet(me->mUrl);
    me->mWorkerThread = nullptr;
}

/**
 * @brief - This callback is called when async thread is finished
 * @param data[in] -user data
 * @param thread[in] - thread
 */
void AsyncHTTPRequest::thread_end_cb(void *data, Ecore_Thread *thread)
{
    //Since thread_end_cb() is called from main loop thread,
    //we can access EAPI without considering any syncronization.
    AsyncHTTPRequest *me = static_cast<AsyncHTTPRequest *> (data);
    assert(me);
    me->ThreadCompleted();
}

/**
 * @brief This callback is called when async thread should be cancelled
 * @param data[in] - user data
 * @param thread[in] - thread
 */
void AsyncHTTPRequest::thread_cancel_cb(void *data, Ecore_Thread *thread)
{
    AsyncHTTPRequest *me = static_cast<AsyncHTTPRequest *> (data);
    assert(me);
    me->ThreadCanceled();
}

void AsyncHTTPRequest::ThreadCompleted() {
    if (mCurlCode == CURLE_OK) {
        Log::info("Buffer length:%d", mCurlWrapper->mSize);
        if (mCallback) {
            mFileName = Utils::GetIconNameFromUrl(mUrl);
            mCallback(mUserData, mCurlCode, mCurlWrapper->mMemory, mCurlWrapper->mSize, mFileName);
        }
    } else {
        Log::error("Download Error: curlCode = %d", mCurlCode);
        if (mCallback) {
            mCallback(mUserData, mCurlCode, NULL, 0, NULL);
        }
    }
    mMainLoopThread = nullptr;
}

void AsyncHTTPRequest::ThreadCanceled() {
    if (mCanceledCB) {
        mCanceledCB(mUserData);
    }
    mMainLoopThread = nullptr;
}
