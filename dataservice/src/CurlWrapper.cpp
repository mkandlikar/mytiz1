#include "AsyncHTTPRequest.h"
#include "CurlWrapper.h"
#include "Log.h"
#include "Utils.h"

#include <cassert>
#include <stdlib.h>

#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "CurlWrapper"

#define CURLWRAPPER_BUFFER_INIT_SIZE 50*1024


CurlWrapper::CurlWrapper(AsyncHTTPRequest *request) {
    assert(request);
    mRequest = request;
    mMemory = (char*) malloc(CURLWRAPPER_BUFFER_INIT_SIZE);  /* will be grown as needed by the realloc below */
    mMemoryBufferSize = CURLWRAPPER_BUFFER_INIT_SIZE;
    mSize = 0;  /* no data at this point */
}

CurlWrapper::~CurlWrapper() {
    free(mMemory);
}

/**
 * @brief This method should be used for sending HTTP GET requests via CURL library
 * @param[in] request - The full http request url
 * @param[in] respondBuffer - Memory buffer, where HTTP respond will be placed
 * @return - The code result of execution HTTP GET request
 */
CURLcode CurlWrapper::SendHttpGet(const char * request)
{
    Log::debug("SendHttpGet:%s", request);

    CURL *curl;
    CURLcode res;

    curl = curl_easy_init();
    curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
    curl_easy_setopt(curl, CURLOPT_URL, request);
    curl_easy_setopt(curl, CURLOPT_HTTPGET, 1);
    curl_easy_setopt(curl, CURLOPT_USERAGENT,Utils::GetHTTPUserAgent());
    /* send all data to this function  */
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &write_data);

    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0);
    curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, &progress_callback);
    curl_easy_setopt(curl, CURLOPT_XFERINFODATA, (void * )this);

    /* we pass our 'chunk' struct to the callback function */
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)this);

    /* get it! */
    res = curl_easy_perform( curl );
    if(CURLE_OK != res && CURLE_ABORTED_BY_CALLBACK != res) {
        Log::error("curl_easy_perform() failed: %s", curl_easy_strerror(res));
    }

    Log::debug("SendHttpGet: Curl result with code = %d", res);

    /* cleanup curl stuff */
    curl_easy_cleanup(curl);
    return res;
}

/**
 * @brief This is a callback passed to CURL library which will be called to store the output
 * @param[in] contents - content
 * @param[in] size - block size
 * @param[in] nmemb - blocks cound
 * @param[in] userp - user data passed to callback
 * @return
 */
size_t CurlWrapper::write_data(void *contents, size_t size, size_t nmemb, void *userp)
{
  size_t realsize = size * nmemb;
  CurlWrapper *me = static_cast<CurlWrapper *> (userp);
  assert(me);

  if(me->mMemoryBufferSize < (me->mSize + realsize + 1)) {
      me->mMemory = (char*) realloc(me->mMemory, me->mSize + realsize + 1);
      me->mMemoryBufferSize = me->mSize + realsize + 1;

      if(me->mMemory == NULL) {
        /* out of memory! */
        Log::error("not enough memory (realloc returned NULL).");
        return 0;
      }
  }

  int res = Utils::Memcpy_s(&(me->mMemory[me->mSize]), me->mSize + realsize + 1, contents, realsize);
  assert(res);
  me->mSize += realsize;
  me->mMemory[me->mSize] = 0;

  return realsize;
}

int CurlWrapper::progress_callback(void *clientp, curl_off_t dltotal, curl_off_t dlnow, curl_off_t ultotal, curl_off_t ulnow) {
    CurlWrapper *me = static_cast<CurlWrapper *> (clientp);
    assert(me);
    return me->mRequest->OnProgress(dltotal, dlnow, ultotal, ulnow);
}

