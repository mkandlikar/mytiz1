#include "Common.h"
#include "Log.h"
#include "PushService.h"
#include "Utils.h"

#include <app_preference.h>
#include <notification.h>

#define PUSH_APP_ID "b17700c2a232dc8f"  //OFFICIAL APPID
#define LOG_PREFIX  "dPushService:"

const char * PushService::FacebookName = "Facebook";


//Flag to be used to send Deregister before Registering. This would ensure that whenever the FB APP starts, the Tizen
//Push Server and application have refreshed Registration ID.
PushService::Toggle_Modes PushService::toggle = Off;

//Variable to save the Push Connection Details. This shall contain the information required to communicate with
//Tizen Push service
push_service_connection_h PushService::pushConn = NULL;


//The following method is used to Connect to the Tizen Push Platform service. This is a Tizen Platform service
//which connects further to the Tizen Push server.
void  PushService::connect()
{
    // Connect to the push service when the application is launched
    int ret = push_service_connect(PUSH_APP_ID, _state_cb, _noti_cb, NULL, &PushService::pushConn);
    if (PUSH_SERVICE_ERROR_NONE == ret) {
        Log::info(LOG_PREFIX " connect() returned Success");
    } else {
        Log::error(LOG_PREFIX " connect() returned error=%d", ret);
    }
}

//The Following Method is used to Disconnect from the Push Notification service. This would be called when
//the user does not want to receive any notification. The destructor for the PushService would be typically
//called after this.
void PushService::disconnect()
{
    push_service_disconnect(PushService::pushConn);
    //Reset the connection details
    PushService::pushConn = NULL;
    Log::info(LOG_PREFIX " disconnect() completed");
}

//The following function is a call back which is triggered when the Registration request is approved by server.
//This callback is set in push_service_register. This shall not be called when there is failure in even
//calling the registration funtion.
static void _reg_result_cb(push_service_result_e result, const char *msg, void *user_data)
{
    if (result == PUSH_SERVICE_RESULT_SUCCESS) {
        Log::info(LOG_PREFIX " Registration request is APPROVED (msg=%s)", msg);
    } else {
        Log::error(LOG_PREFIX " Registration ERROR=%d (msg=%s)", result, msg);
    }
}

//The following function is a call back which is triggered when the De-Registration request is approved by server.
//This callback is set in push_service_deregister.
static void _dereg_result_cb(push_service_result_e result, const char *msg, void *user_data)
{
    if (result == PUSH_SERVICE_RESULT_SUCCESS) {
        Log::debug(LOG_PREFIX " De-Registration SUCCESS (msg=%s)", msg);
    } else {
        Log::debug(LOG_PREFIX " De-Registration ERROR=%d (msg=%s)", result, msg);
    }
}

//The following function is a call back which is triggered when the user is in unregistered state
// In this case the mobile needs to register to the Tizen Notification service. Only after a successful
//Registration would it receive notifications.
static void _on_state_unregistered(void *user_data)
{
    Log::info(LOG_PREFIX " Push Service STATE UNREGISTERED");
    // Send a registration request to the push service
    int ret = push_service_register(PushService::pushConn, _reg_result_cb, NULL);
    if (ret == PUSH_SERVICE_ERROR_NONE) {
        Log::info(LOG_PREFIX " push_service_register() returned SUCCESS");
    } else {
        Log::error(LOG_PREFIX " push_service_register() returned error=%d", ret);
    }
}

//The following function is a call back which is called if the user is registered succesfully for Notification services.
//There may be few pending notifications as well, which need to be locally read when the user enters this state.
void PushService::_on_state_registered(void *user_data)
{
    Log::info(LOG_PREFIX " Push Service STATE REGISTERED");

    //Check to see any unread notification
    int ret = push_service_request_unread_notification(PushService::pushConn);
    Log::warning(LOG_PREFIX " push_service_request_unread_notification() returned %d" , ret);

    // Get the registration ID
    char *reg_id = NULL;
    ret = push_service_get_registration_id(PushService::pushConn, &reg_id);
    if (ret == PUSH_SERVICE_ERROR_NONE) {
        Log::debug(LOG_PREFIX " SUCCESS: push_service_get_registration_id() = %s", reg_id);
        free(reg_id);
    } else {
        Log::warning(LOG_PREFIX " push_service_get_registration_id() returned error=%d", ret);
    }
}

static void _on_state_error(const char *err , void *user_data)
{
    Log::error(LOG_PREFIX " Push Service STATE ERROR (%s)", err);
}

//The following function is triggered whenever the state of the registration changes.
void PushService::_state_cb(push_service_state_e state, const char *err, void *user_data)
{
    switch (state) {
    case PUSH_SERVICE_STATE_UNREGISTERED:
        Log::info(LOG_PREFIX " Arrived at STATE_UNREGISTERED");
        //The following function shall send the request for registration.
        _on_state_unregistered(user_data);
        //Since we do not want to de register again.
        PushService::toggle = On;
        break;
    case PUSH_SERVICE_STATE_REGISTERED:
        if (PushService::toggle == Off) {
            //Do an Explicit De-registration. This would typically happen when the application is restarted.
            //We also need to handle the case when a new user signs up.
            //Need to check if this is really required. Registration id's may be stored by the platform service
            //in persistent storage. In that case this may not be required. In testing we have seen that at times
            // we do not receive notification even though the application on starting comes into Registered state which
            //seems to indicate that the reg id is stored locally in persistent DB by platform service.
            int ret = push_service_deregister(PushService::pushConn, _dereg_result_cb, user_data);
            Log::info(LOG_PREFIX " Sent  De-register [%d]", ret);
            PushService::toggle = On;
        } else {
            Log::info(LOG_PREFIX " Arrived at STATE_REGISTERED");
            _on_state_registered(user_data);
            PushService::toggle = Off;
        }
        break;
    case PUSH_SERVICE_STATE_ERROR:
        Log::error(LOG_PREFIX " Arrived at STATE_ERROR");
        _on_state_error(err, user_data);
        break;
    default:
        Log::warning(LOG_PREFIX " Facebook_Notification", "Unknown State");
        break;
    }
}

//The following method is for receiving the notifications from the Tizen Push server. This method would
//receive the individual notifcation and store them in a list. This list shall be displayed in the notification screen.
void PushService::_noti_cb(push_service_notification_h noti, void *user_data)
{
    Log::info(LOG_PREFIX " Notification received in background from Tizen Server");
    // Extract important fields out of the notification.

    char *data = NULL;  // App data loaded on the notification
    if ((push_service_get_notification_data(noti, &data) == PUSH_SERVICE_ERROR_NONE) &&
        data && *data) {
        // AppData contains the JSON sent by FB server.
        Log::debug(LOG_PREFIX "   AppData = %s", data);
        char *message = strdup(data);
        char *notiText = strstr(message, "\"alert\":\"");
        if (notiText) {
            notiText += sizeof("\"alert\":\"") - 1;
            char *notiTextEnd = strchr(notiText, '\"');
            if (notiTextEnd) {
                *notiTextEnd = '\0';
            }
        }

        if (notiText) {
            Log::debug(LOG_PREFIX "   Message to display: %s", notiText);
            notification_h noti_notification = notification_create(NOTIFICATION_TYPE_NOTI);
            if (noti_notification) {
                app_control_h app_control = NULL;
                int ret = app_control_create(&app_control);
                if ((APP_CONTROL_ERROR_NONE == ret) && app_control) {
                    notification_set_text(noti_notification, NOTIFICATION_TEXT_TYPE_TITLE, FacebookName, nullptr, NOTIFICATION_VARIABLE_TYPE_NONE);
                    notification_set_text(noti_notification, NOTIFICATION_TEXT_TYPE_CONTENT, notiText, nullptr, NOTIFICATION_VARIABLE_TYPE_NONE);
                    notification_set_display_applist(noti_notification, NOTIFICATION_DISPLAY_APP_NOTIFICATION_TRAY | NOTIFICATION_DISPLAY_APP_TICKER);

                    app_control_set_app_id(app_control, FB_PACKAGE);
                    ret = app_control_set_operation(app_control, APP_CONTROL_CUSTOM_OPERATION_NOTIF_RCVD_TO_PROCESS);
                    if (APP_CONTROL_ERROR_NONE == ret) {
                        ret = app_control_add_extra_data(app_control, PUSH_NOTIF_JSON_DATA, SAFE_STRDUP(data));
                        Log::debug(LOG_PREFIX "app_control_add_extra_data() %s Notification data.",
                                (APP_CONTROL_ERROR_NONE == ret) ? "successfully added" : "failed to add");
                    }
                    notification_set_launch_option(noti_notification, NOTIFICATION_LAUNCH_OPTION_APP_CONTROL, (void *)app_control);
                    notification_post(noti_notification);
                }
                app_control_destroy(app_control);
            }
        }
        free(message);
    } else {
        Log::error(LOG_PREFIX "   Cannot get AppData!");
        free(data);
        return;
    }
    free(data);

    char *msg = NULL;   // Noti message
    if (push_service_get_notification_message(noti, &msg) == PUSH_SERVICE_ERROR_NONE) {
        Log::debug(LOG_PREFIX "   Message = %s", msg);
    }
    free(msg);

    long long int time_stamp; // Time when the noti is generated
    if (push_service_get_notification_time(noti, &time_stamp) == PUSH_SERVICE_ERROR_NONE) {
        Log::debug(LOG_PREFIX "   Time = %llds", time_stamp/1000);
    }

    //Wake up the UI-application for a moment to increase the Badge counter.
    if (PREFERENCE_ERROR_NONE == preference_set_string("fbapp_ui_mode", "without_ui")) {
        app_control_h app_control = NULL;
        int ret = app_control_create(&app_control);
        if ((APP_CONTROL_ERROR_NONE == ret) && app_control) {
            app_control_set_app_id(app_control, FB_PACKAGE);
            app_control_set_operation(app_control, "dummy");
            app_control_send_launch_request(app_control, NULL, NULL);
        }
        app_control_destroy(app_control);
    }
}
