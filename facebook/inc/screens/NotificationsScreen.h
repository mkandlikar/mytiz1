#ifndef NOTIFICATIONSSCREEN_H_
#define NOTIFICATIONSSCREEN_H_

#include "GraphObjectDownloader.h"
#include "ScreenBase.h"
#include "TrackItemsProxyAdapter.h"

class ActionBase;
class NotificationsScreen: public ScreenBase, public TrackItemsProxyAdapter, public IGraphObjectDownloaderObserver
{
public:
    NotificationsScreen(ScreenBase *parentScreen);
    virtual ~NotificationsScreen();

    void Update(AppEventId eventId, void *data) override;
    void FetchNewNotifications();

protected:
    void* CreateItem(void* dataForItem, bool isAddToEnd) override;
    void SetDataAvailable(bool isAvailable) override;
    void RebuildAllItems(void) override;

private:
    Evas_Object* CreateNotificationItem(ActionAndData *actionData, Evas_Object *parent, bool isAddToEnd);
    static Evas_Object* NoResultItem(Evas_Object* parent, bool isAddToEnd);
    void FetchEdgeItems() override;
    void UpdateNotificationBadge();
    void DeleteNoResultItem();

    void ProgressBarShow() override;
    void StartDownloader(const char *objectId);
    void GraphObjectDownloaderProgress(IGraphObjectDownloaderObserver::Result result) override;
    void DownloaderProgress(Result result, GraphObjectDownloader *downloader) override {};

    static void on_item_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void try_link_in_webview(char *link);

    static void on_notification_mark_as_read_completed(void* object, char* respond, int code);
    static void on_notification_mark_as_seen_completed(void* object, char* respond, int code);

    ActionBase* mAction;

    static GraphRequest *mNotificationMarkAsRead;
    GraphRequest *mNotificationSeen;
    GraphObjectDownloader *mObjectDownloader;
    char *mClickedNotiLink;

    Evas_Object *mNoNotifications;

    static const unsigned int DEFAULT_WND_SIZE;
    bool mPostponedLanguageChange;
};

#endif /* NOTIFICATIONSSCREEN_H_ */
