#ifndef PHOTOGALLERYUPLOADPHOTOSTRATEGY_H_
#define PHOTOGALLERYUPLOADPHOTOSTRATEGY_H_

#include "OwnProfileScreen.h"
#include "PhotoGalleryScreenBase.h"
#include "Photo.h"


class Photo;

class PhotoGalleryUploadPhotoStrategy: public PhotoGalleryStrategyBase
{

public:
    PhotoGalleryUploadPhotoStrategy(UploadPhotoType uploadType);

private:
    virtual ~PhotoGalleryUploadPhotoStrategy();
    virtual void PhotoSelected(Photo* photo);

private:
    UploadPhotoType mUploadType;
};

#endif /* PHOTOGALLERYUPLOADPHOTOSTRATEGY_H_ */
