/*
 *  UserProfilePlacesMore.h
 *
 *  Created on: 17th July 2015
 *      Author: Manjunath Raja
 */

#ifndef USER_PROFILE_PLACES_MORE_H_
#define USER_PROFILE_PLACES_MORE_H_

#include "FbResponseAboutPlaces.h"
#include "ScreenBase.h"
#include "TrackItemsProxyAdapter.h"

class ActionBase;
class UserProfilePlacesMore: public ScreenBase, public TrackItemsProxyAdapter
{
private:
    ActionBase* mAction;

    Evas_Object* CreateWrapper(Evas_Object* parent, bool isAddToEnd);
    void CreatePlaceItem(Evas_Object* parent, FbResponseAboutPlaces::PlacesList *places_data);
    static Evas_Object *PlacesGet(void *data, Evas_Object *obj, bool isAddToEnd, void* self_data);
    static void hide_btns_cb(void *data, Evas_Object *obj, void *event_info);
    static void show_btns_cb(void *data, Evas_Object *obj, void *event_info);

protected:
    virtual void* CreateItem(void* dataForItem, bool isAddToEnd);

public:
    UserProfilePlacesMore(ScreenBase *parentScreen, Evas_Object *container, const char *profileId);
    UserProfilePlacesMore(ScreenBase *parentScreen, Evas_Object *container, const char *profileId, AbstractDataProvider * provider);
    virtual ~UserProfilePlacesMore();

};

#endif //USER_PROFILE_PLACES_MORE_H_
