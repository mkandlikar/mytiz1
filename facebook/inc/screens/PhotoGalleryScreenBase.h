#ifndef PHOTOGALLERYSCREENBASE_H_
#define PHOTOGALLERYSCREENBASE_H_

#include "Photo.h"
#include "ScreenBase.h"
#include "TrackItemsProxy.h"

class ActionBase;
class ActionAndData;
class PhotoGalleryProvider;
class DataUploader;

class GalleryItem {
public:
    GalleryItem();
    ~GalleryItem();
    void AppendActionAndData(ActionAndData *actionAndData);
    Eina_List *GetActionAndDates() const;
    Evas_Object *GetLayout() const;
    void SetLayout(Evas_Object *layout);

private:
    Eina_List *mActionAndDates;
    Evas_Object *mLayout;
};


class PhotoGalleryScreenBase;

class PhotoGalleryStrategyBase {
public:
    virtual ~PhotoGalleryStrategyBase() {}
    void SetScreen(PhotoGalleryScreenBase *screen);
    virtual void PhotoSelected(Photo *photo) = 0;

protected:
    PhotoGalleryScreenBase *mScreen;
};


class PhotoGalleryScreenBase: public ScreenBase,
                              public TrackItemsProxy
{
protected:
    class AsyncImageUpdatersManager;

    class AsyncImageLayoutUpdater : public ImageFileDownloader
    {
    public:
        AsyncImageLayoutUpdater(AsyncImageUpdatersManager &manager, ImageFile &image, Evas_Object *layout);
        ~AsyncImageLayoutUpdater() {}
        inline Evas_Object *GetLayout() {return mLayout;}

    private:
        //from ImageFileObserver
        virtual void ImageDownloaded(bool result);

     private:
        AsyncImageUpdatersManager &mManager;
        Evas_Object *mLayout;
    };

    class AsyncImageUpdatersManager {
    public:
        AsyncImageUpdatersManager();
        ~AsyncImageUpdatersManager();
        void UpdateLayout(ImageFile *image, Evas_Object *layout);
        inline void Pause() {mIsPaused = true;};
        void Resume();
        void LayoutUpdated(AsyncImageLayoutUpdater *updater);

    private:
        void Continue();

    private:
        Eina_List *mUpdaters;
        bool mIsPaused;
        static const unsigned int MAX_ACTIVE_IMAGE_DOWNLOADS;
    };

protected:
    void SetTopListLayout(Evas_Object *topListLayout);
    void SetBottomListLayout(Evas_Object *bottomListLayout);

    virtual void* CreateItem( void* dataForItem, bool isAddToEnd );
    virtual void DeleteItem( void* item );
    virtual ItemSize* GetItemSize( void* item );
    virtual void HideItem( void *item );
    virtual void ShowItem( void *item );
    virtual void DeleteAll_Prepare();
    virtual bool IsItemsEqual( void *itemToCompare_data, void *itemForCompare_ui ) {
        return false;
    }
    virtual void SetDataAvailable(bool isAvailable);
    virtual void* GetDataForItem( void *item );
    virtual ItemSize* SaveSize( void *item );
    virtual ItemSize* GetSize( void *item );

    const char *SelectBestLayoutForItem(GalleryItem *item);

public:
    PhotoGalleryScreenBase(ScreenBase *parentScreen, PhotoGalleryProvider* provider, PhotoGalleryStrategyBase *mStrategy);
    virtual ~PhotoGalleryScreenBase() = 0;
    static void ArrangeItems(const Eina_List *photos, Eina_Array *items, void *userData);

private:
    static void on_photo_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    static bool go_item_comparator(void *itemToCompare, void *itemForCompare);
    static void on_download_completed(void* data);

protected:
    DataUploader *mDataDownloader;
    static const unsigned int DEFAULT_ITEMS_WND_SIZE;
    static const unsigned int DEFAULT_ITEMS_STEP_SIZE;
    PhotoGalleryStrategyBase *mStrategy;
    AsyncImageUpdatersManager mImageUpdatersManager;

private:
    ActionBase *mAction;
    Evas_Object *mTopListLayout;
    bool mIsTopListLayoutSet;
    Evas_Object *mBottomListLayout;
};

#endif /* PHOTOGALLERYSCREENBASE_H_ */
