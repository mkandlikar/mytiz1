#ifndef WEBVIEWSCREEN_H_
#define WEBVIEWSCREEN_H_

#include "ActionBase.h"
#include "AppEvents.h"
#include "LoadRequestScreen.h"

/*
 *
 */
class WebViewScreen : public LoadRequestScreen, public Subscriber
{
private:
    /**
     * @brief Construct WebViewScreen object
     * @param originalUrl - original url to be loaded in this screen (without credentials etc)
     * @param url - url to be loaded in this screen
     * @param action_data - external ActionaAndData object to be used in this screen
     */
    WebViewScreen(const char *originalUrl, const char * url, bool enableMenu, ActionAndData * action_data = NULL);

public:

    /**
     * @brief Destruction
     */
    virtual ~WebViewScreen();

    /**
     * @brief Overloaded Push method
     */
    virtual void Push();

    /**
     * @brief This callback is called when webview title is changed
     * @param data - user data passed into callback
     * @param obj - object which generated event
     * @param event_info - event information
     */
    static void on_title_changed(void *data, Evas_Object *obj, void *event_info);

    /**
     * @brief This callback is called to report load progress
     * @param data - user data passed into callback
     * @param obj - object which generated event
     * @param event_info - progress rate
     */
    static void on_load_progress(void *data, Evas_Object *obj, void *event_info);

    /**
     * @brief This callback is called when loading is started
     * @param data - user data passed into callback
     * @param obj - object which generated event
     * @param event_info - event information
     */
    static void on_load_started(void *data, Evas_Object *obj, void *event_info);

    /**
     * @brief This callback is called when loading is finished
     * @param data - user data passed into callback
     * @param obj - object which generated event
     * @param event_info - event information
     */
    static void on_load_finished(void *data, Evas_Object *obj, void *event_info);

    /**
     * @brief This callback is called when user pressed on close button
     * @param data - user data passed into callback
     * @param obj - object which generated event
     * @param emission - signal description
     * @param source - who emitted the signal
     */
    static void on_close_button_clicked (void *data, Evas_Object *obj, const char *emission, const char *source);

    /**
     * @brief This callback is called when user pressed on menu button
     * @param data - user data passed into callback
     * @param obj - object which generated event
     * @param emission - signal description
     * @param source - who emitted the signal
     */
    static void on_menu_button_clicked (void *data, Evas_Object *obj, const char *emission, const char *source);

    /**
     * @brief This callback is called when user pressed on back navigation button
     * @param data - user data passed into callback
     * @param obj - object which generated event
     * @param emission - signal description
     * @param source - who emitted the signal
     */
    static void on_back_button_clicked (void *data, Evas_Object *obj, const char *emission, const char *source);

    /**
     * @brief This callback is called when user pressed on forward navigation button
     * @param data - user data passed into callback
     * @param obj - object which generated event
     * @param emission - signal description
     * @param source - who emitted the signal
     */
    static void on_forward_button_clicked (void *data, Evas_Object *obj, const char *emission, const char *source);

    /**
     * @brief Close popup menu
     * @param data
     * @param obj
     * @param event_info
     */
    static void on_menu_dismiss_cb(void *data, Evas_Object *obj, void *event_info);

    /**
     * @brief This is genlist callback to get text of context menu item
     * @param data The data passed in the item creation function
     * @param obj The base widget object
     * @param part The part name of the swallow
     * @return The allocated (NOT stringshared) string to set as the text
     */
    static char* menu_item_text_get_cb(void *data, Evas_Object *obj, const char *part);

    /**
     * @brief This is genlist callback to get icon of context menu item
     * @param data The data passed in the item creation function
     * @param obj The base widget object
     * @param part The part name of the swallow
     * @return The image to set as the icon
     */
    static Evas_Object* menu_item_icon_get_cb(void *data, Evas_Object *obj, const char *part);

    /**
     * @brief This callback is called when a popup menu item is selected
     * @param data [in] - user data passed in callback
     * @param obj [in] - object which generates click event
     * @param event_info [in] - event information
     */
    static void menu_item_select_cb(void *data, Evas_Object *obj, void *event_info);

    // Begin ----- Common webview launch routine area
    static void privacy_shortcuts(void* data, Evas_Object* obj, void* event_info);

    static void new_window_policy_cb(void* data, Evas_Object *obj, void *event_info);
    static void navigation_policy_cb(void* data, Evas_Object *obj, void *event_info);
    static void on_url_changed(void* data, Evas_Object *obj, void *event_info);
    static void response_policy_cb(void* data, Evas_Object *obj, void *event_info);

    static void Launch(const char* url, bool enableDeeplink=false, bool enableMenu=false, ActionAndData *action_data = NULL);
    static const char* encode_url(const char* rawUrl);

    // End------

    /**
     * @brief Shows the ProgressBar
     */
    virtual void ShowProgressBar();

    /**
     * @brief Hides the ProgressBar
     */
    virtual void HideProgressBar();

    /**
     * @brief Sends request to facebook
     */
    virtual GraphRequest * DoRequest();

    /**
     * @brief Parse json response
     */
    virtual FbRespondBase* ParseResponse(char* response);

    /**
     * @brief - Update screen with received data
     * @param response[in] - the received data
     */
    virtual void SetData(FbRespondBase * response);

    virtual bool HandleBackButton();

    virtual void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = NULL);

private:
    const char * mUrl;
    const char * mOriginalUrl;
    Evas_Object * mProgressBar;
    Evas_Object * mWebView;
    Evas_Object * mPopup;
    Evas_Object * mUrlCopyLabel;
    ActionAndData *mWebViewActionNData;
    bool mMenuEnabled{false};
    Ecore_Thread *mThread;
    bool mIsUrlValid;
    bool mIsFullScreen;
    /**
     * @brief - Closes menu popup
     */
    void ClosePopup();

    /**
     * @brief - Opens PostComposer Screen to share link
     */
    void ShareLink();

    /**
     * @brief - Copies url to clipboard
     */
    void CopyToClipboard();

    void InitData();
    void InitScreen();
    void InitMenu (bool enableMenu);
    void InitUrl (const char* url);
    void StartUrl();

    static void thread_start_cb(void *data, Ecore_Thread *thread);
    static void thread_end_cb(void *data, Ecore_Thread *thread);
    static void thread_cancel_cb(void *data, Ecore_Thread *thread);

    static bool check_url_for_deep_link(const char *url);
    static bool check_url_for_group_file(const char *url);
    static std::string get_contained_link(const char* url, bool isDeepLinkUrl);
    static std::string get_contained_link_to_file(const char* url);
    static void download_document_file(const char* url);
    static bool send_launch_request(const char* uri, const char* operation_id);

    static void on_enter_fullscreen(void *data, Evas_Object *obj, void *event_info);
    static void on_exit_fullscreen(void *data, Evas_Object *obj, void *event_info);
};

#endif /* WEBVIEWSCREEN_H_ */
