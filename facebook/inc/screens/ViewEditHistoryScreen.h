/*
 * ViewEditHistoryScreen.h
 *
 *  Created on: May 11, 2016
 *      Author: ruibezruch
 */

#ifndef VIEWEDITHISTORYSCREEN_H_
#define VIEWEDITHISTORYSCREEN_H_

#include "FeedAction.h"
#include "ScreenBase.h"
#include "TrackItemsProxyAdapter.h"

class ViewEditHistoryScreen:  public ScreenBase, public TrackItemsProxyAdapter
{
public:
    ViewEditHistoryScreen(const char *id);
    virtual ~ViewEditHistoryScreen();
    virtual void Push();

private:
    const char *mId;

    FeedAction *mAction;

    virtual void* CreateItem(void* dataForItem, bool isAddToEnd);

    Evas_Object *CreatePostHistoryItem(Evas_Object *parent, ActionAndData *actionData, bool isAddToEnd);

};

#endif /* VIEWEDITHISTORYSCREEN_H_ */
