#ifndef FRIENDSTAB_H_
#define FRIENDSTAB_H_

#include "FriendsAction.h"
#include "ScreenBase.h"
#include "TrackItemsProxyAdapter.h"

#include <Elementary.h>
#include <Evas.h>

class FriendsTab: public ScreenBase,
                  public TrackItemsProxyAdapter
{
public:
    FriendsTab(ScreenBase *parentScreen);
    virtual ~FriendsTab();
    void Update( AppEventId eventId = eUNDEFINED_EVENT, void *data = NULL ) override;
    bool DeleteConfirmationDialog();
protected:
    void* CreateItem(void* dataForItem, bool isAddToEnd) override;
    void* UpdateItem(void* dataForItem, bool isAddToEnd) override;
private:
    FriendsAction* mAction;

    Evas_Object *CreateFriendItem(Evas_Object* parent, ActionAndData *action_data, bool isAddToEnd);
    void FetchEdgeItems() override;
    void RebuildAllItems() override;

    static void on_friends_button_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_item_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
};
#endif /* FRIENDSTAB_H_ */
