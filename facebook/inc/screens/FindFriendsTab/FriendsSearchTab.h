#ifndef FRIENDSSEARCHTAB_H_
#define FRIENDSSEARCHTAB_H_

#include "Application.h"
#include "FriendsAction.h"
#include "ScreenBase.h"
#include "TrackItemsProxyAdapter.h"

#include <Elementary.h>
#include <Evas.h>

class FriendsSearchTab: public ScreenBase, public TrackItemsProxyAdapter
{

public:
    FriendsSearchTab(ScreenBase *parentScreen, bool isSelected);
    virtual ~FriendsSearchTab();

    void DeleteItemById(char *id);

    Evas_Object* mSearchEntry;

protected:
    void* CreateItem(void* dataForItem, bool isAddToEnd) override;
    void SetDataAvailable( bool isAvailable ) override;
    void SetNoDataViewVisibility(bool isVisible) override;

private:
    void ShowAddBtn(Evas_Object* actionObject);
    void ShowCancelBtn(Evas_Object*  actionObject);
    void Update(AppEventId eventId = eUNDEFINED_EVENT, void *data = nullptr) override;
    FriendsAction* mAction;

    void CreateSearchItem(Evas_Object* parent, ActionAndData *action_data);
    static Evas_Object *SearchGet(void *data, Evas_Object *obj, bool isAddToEnd);

    static void show_input_text_panel(void *data, Evas_Object *obj, void *event_info);
    static void on_search_entry_clicked_cb(void *data, Evas_Object *obj, void *event_info);
    static void on_item_clicked_cb(void *user_data, Evas_Object *obj, const char *emisson, const char *source);

    /**
     * @brief Object for getting ui constants.
     */
    static UIRes *R;
};
#endif /* FRIENDSSEARCHTAB_H_ */
