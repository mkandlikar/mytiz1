#ifndef FRIENDSCONTACTSTAB_H_
#define FRIENDSCONTACTSTAB_H_

#include "ScreenBase.h"

class FriendsContactsTab: public ScreenBase
{
public:
    FriendsContactsTab(ScreenBase *parentScreen);
    virtual ~FriendsContactsTab();
};

#endif /* FRIENDSCONTACTSTAB_H_ */
