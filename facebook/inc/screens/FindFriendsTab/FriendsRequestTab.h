#ifndef FRIENDSREQUESTTAB_H_
#define FRIENDSREQUESTTAB_H_

#include "Application.h"
#include "ActionBase.h"
#include "Common.h"
#include <Evas.h>
#include <Elementary.h>

#include "FindFriendsScreen.h"
#include "FriendsAction.h"
#include "MainScreen.h"
#include "ScreenBase.h"
#include "TrackItemsProxyAdapter.h"

class FriendsRequestTab: public ScreenBase, public TrackItemsProxyAdapter
{
public:
    FriendsRequestTab(ScreenBase *parentScreen, bool isFindFriends);
    void OnResume() override;
    static void scroll_to_top (void *data, Evas_Object *obj, const char *emission, const char *source);
    virtual ~FriendsRequestTab();

protected:
    void* CreateItem(void* dataForItem, bool isAddToEnd) override;
    void FetchEdgeItems() override;

private:
    FriendsAction* mAction;
    Evas_Object *mFriendsSeparatorItem;
    FindFriendsScreen *mFfScreen;

    void CreateRequestItem(Evas_Object* parent, ActionAndData *action_data);
    void CreatePYMKItem(Evas_Object* parent, ActionAndData *action_data);
    Evas_Object *FriendRequestsGet(void *data, bool isAddToEnd);
    Evas_Object* NoResultItem(bool isAddToEnd);

    void RefreshPeopleItem(ActionAndData *action_data);
    void RefreshRequestItem(ActionAndData *action_data);

    void StartGetFriendRequestsCountRequest();
    void RequestForErase();
    void UpdateWithDefaultEvent();
    void DestroyFriendRequest(User *user);

    void OnAppResume();
    void OnAppPause();
    void OnMainScreenTabChanged(MainScreen::Tab tab);

    void StartBadgeUpdateByTimer();
    void StopBadgeUpdateByTimer();

    static Eina_Bool on_badge_update_timer_cb(void *data);

    static void hide_ff_btn_cb(void *data, Evas_Object *obj, void *event_info);
    static void show_ff_btn_cb(void *data, Evas_Object *obj, void *event_info);
    static void on_find_friends_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);

    static void on_item_click(ActionAndData *action_data, bool isPYMKMode);
    static void on_pymk_item_click(void *data, Evas_Object *obj, const char *emisson, const char *source);
    static void on_request_item_click(void *data, Evas_Object *obj, const char *emisson, const char *source);
    void Update(AppEventId eventId, void *data) override;

    void UpdateFriendsRequests(int unreadCount, int totalCount);
    void SetFriendRequestsBadge();
    void PostSeenFriendRequests();
    static void on_friend_requests_count_completed(void* object, char* response, int code);
    static void redraw_ui(void *data);
    static void update_ui_with_default_event(void *data);

    static const unsigned int DEFAULT_FRIEND_WND_SIZE;
    static const unsigned int DEFAULT_FRIEND_WND_STEP;

    Ecore_Timer *mBadgeUpdateTimer;
    bool mIsScreenActive;

    static const double BADGE_UPDATE_TIMEOUT;
    GraphRequest * mRequest;
    bool mPostponedPostFriendRequestsWasSeen;

    Ecore_Job* mRedrawJob;
    Ecore_Job* mUpdateWithDefaultEvenetJob;
};
#endif /* FRIENDSREQUESTTAB_H_ */
