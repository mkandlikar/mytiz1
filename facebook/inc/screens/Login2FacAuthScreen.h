#ifndef SCREENS_LOGIN2FACAUTHSCREEN_H_
#define SCREENS_LOGIN2FACAUTHSCREEN_H_

#include "ScreenBase.h"

class LoginRequest;
class SignUpInProgressScreen;
class InitializationDataUploader;

class Login2FacAuthScreen: public ScreenBase, public Subscriber {
private:
    SignUpInProgressScreen *mScreenInProgress;
    InitializationDataUploader *mInitDataUploader;
    LoginRequest *mLoginRequest;
    Evas_Object *mEvasLoginCodeInput;
    ConfirmationDialog *mConfirmationDialog;

    const char *mFirstFactor;
    const char *mUserName;
    const char *mPswdAcc;

private:
    void LoadEdjLayout();
    void CreateView();
    Evas_Object* CreateInput(Elm_Input_Panel_Layout entryType, const char *placeHolderTxt);
    void DisplayCurlErrorMsg(int curlCode);

    void UpdateLoginButton();
    void EnableLoginButton(bool enable);
    void ShowAccountDisabledPopup();
    bool DeleteConfirmationDialog();
    void ShowAlertPopup(const char *message, const char *title = NULL);
    static void confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event);

public:
    Login2FacAuthScreen(ScreenBase *parentScreen, const char *username, const char *password, const char *firstFactor);
    virtual ~Login2FacAuthScreen();
    virtual void Push();
    virtual bool HandleBackButton();
    virtual void Update(AppEventId eventId, void *data);

    void StartDownloadInitData();

protected:
    static void login_btn_cb(void *data, Evas_Object *obj, void *eventInfo);
    static void login_btn_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void resend_code_btn_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void need_help_btn_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void OnLoginCompleted(void *object, char *respond, int code);
    static void OnResendCodeCompleted(void *object, char *respond, int code);
    static void entry_changed_cb(void *data, Evas_Object *obj, void *eventInfo);
    static void provider_sequence_init_done_cb();
};

#endif /* SCREENS_LOGIN2FACAUTHSCREEN_H_ */
