#ifndef IMAGESCAROUSEL_H_
#define IMAGESCAROUSEL_H_

#include "Photo.h"

#include <Elementary.h>


//#define _IMAGE_CAROUSEL_DEBUG

/*
* AAA It's strange, but evas_object_del() execution takes a valuable amount of time.
* This a simple EvasObjectsKiller was created in order to kill list of evas objects with minimum
* impact on the UI thread.
* Now it's not used, but let's keep is for possible usages in the future.
 */
#ifdef _IMAGE_CAROUSEL_DEBUG
/*
 * Class EvasObjectsKiller
 */
class EvasObjectsKiller {
public:
    static void Kill(Eina_List *objects) {
        Eina_List *l;
        void *data;
        EINA_LIST_FOREACH(objects, l, data) {
            Evas_Object *obj = static_cast<Evas_Object *> (data);
            evas_object_hide(obj);
        }
        mObjects = eina_list_merge(mObjects, objects);
        ecore_timer_add(0.5f, do_kill, NULL);
    }
private:
    static Eina_Bool do_kill(void *) {
        int i = 0;
        Eina_List *l;
        Eina_List *l_next;
        void *data;
        EINA_LIST_FOREACH_SAFE(mObjects, l, l_next, data) {
            if(i < MAX_KILLED_PER_ONCE) {
                mObjects = eina_list_remove(mObjects, data);
                Evas_Object *obj = static_cast<Evas_Object *> (data);
                evas_object_del(obj);
                ++i;
            } else {
                return ECORE_CALLBACK_RENEW;
            }
        }
        return ECORE_CALLBACK_CANCEL;
    }

private:
    static const int MAX_KILLED_PER_ONCE;
    static Eina_List *mObjects;
};
#endif

/*
 * Class PhotocamWidget
 */
class PhotocamWidget : private ImageFileDownloader {
public:
    PhotocamWidget(Evas_Object *parent, ImageFile &image, int id);
    ~PhotocamWidget();
    inline Evas_Object * GetLayout() {return mLayout;}
    inline int GetId() {return mId;}
    void Freeze(bool freeze);
    inline void GetImageSize(int *w, int *h) {elm_photocam_image_size_get(mPhotocam, w, h);}
    void Normalize();
    void EnableDoubleClick(bool enable);
    void EnableZoom(bool enable);
private:
    void CreateLayout();
    void CreatePhotocam();
    void CreatePlaceholder();

//from ImageFileObserver
    void ImageDownloaded(bool result) override;

//callbacks:
    static void double_clicked_cb(void *data, Evas_Object *obj, void *event_info);
    static void scroll_anim_start_cb(void *data, Evas_Object *obj, void *event_info);
    static void scroll_anim_stop_cb(void *data, Evas_Object *obj, void *event_info);
    static void photocam_scroll_cb(void *data, Evas_Object *obj, void *event_info);

    static void set_image_file(void *data);

private:
    int mId;
    Evas_Object * mLayout;
    Evas_Object * mParent;
    Evas_Object * mPhotocam;
    bool mZoomed;
    bool mIsDCEnabled;
    Ecore_Job *mSetImageFileJob;
};


/*
 * Class IImageCarouselObserver
 */
class IImageCarouselObserver {
public:
    virtual ~IImageCarouselObserver(){}
    virtual void PageChanged(int pageNumber) = 0;
    virtual void PageDownloaded(unsigned int pageNumber) = 0;
    virtual void ScrollStart() = 0;
    virtual void ScrollStop() = 0;
};


/*
 * Class ImagesCarousel
 */
class ImagesCarousel {
protected:
    class CarouselImageDownloader : public ImageFileDownloader {
    public:
        CarouselImageDownloader(ImagesCarousel &carousel, ImageFile &image, unsigned int page);

    private:
        void ImageDownloaded(bool result) override;

    private:
        ImagesCarousel &mCarousel;
        unsigned int mPage;
    };

public:
    ImagesCarousel(Evas_Object* parent, IImageCarouselObserver* observer, Eina_List *images);
    virtual ~ImagesCarousel();

    void ShowPage(int pageNumber);
    inline Evas_Object * GetLayout() {return mScroller;}
    inline int GetCurrentPage() const {return mCurrentPage;};
    ImageFile &GetCurrentImageFile() const;
    void PageDownloaded(unsigned int pageNumber);
    void StopScroll(bool stop);
    void EnableZoom(bool enable);
private:
    void CreateLayout(Evas_Object* parent);
    void EnableGesture(bool enable);
    void EnableDoubleClick(bool enable);
    void NormalizePhotocams();

    void ShiftToLeft(unsigned int shiftItems);
    void ShiftToRight(unsigned int shiftItems);

    static void resize_cb(void *data, Evas *e, Evas_Object *obj, void *event_info);
    static void scroll_start_cb(void *data, Evas_Object *scroller, void *event_info);
    static void scroll_stop_cb(void *data, Evas_Object *scroller, void *event_info);
    static void scroll_drag_start_cb(void *data, Evas_Object *scroller, void *event_info);

    static void page_changed(void *data);

    PhotocamWidget *GetPhotocamByPage(int page);

#ifdef _IMAGE_CAROUSEL_DEBUG
    void PrintPhotocams();
#endif

private:
    Evas_Object* mScroller;
    Evas_Object *mBox;
    Evas_Object *mLeftFillerBox;
    Eina_List *mPhotocams;
    Eina_List *mDownloaders;
    int mCurrentPage;
    int mShift;
    IImageCarouselObserver* mObserver;

    static const unsigned int MAX_BOX_BUFFERED_ITEMS;
    static const unsigned int MAX_ITEMS_TO_DOWNLOAD;
};


#endif /* IMAGESCAROUSEL_H_ */
