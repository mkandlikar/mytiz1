/*
 * MapViewScreen.h
 *
 *  Created on: Oct 29, 2015
 *      Author: RUINVISH
 */

#ifndef MAPVIEWSCREEN_H_
#define MAPVIEWSCREEN_H_

#include "ScreenBase.h"
#include "Post.h"
#include "ActionBase.h"

class MapViewScreen: public ScreenBase {
protected:
	Evas_Object *CreateHeader(Evas_Object *obj, void *user_data);
	static void back_btn_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);

	virtual void Push();

public:
	MapViewScreen(ActionAndData *data);

	virtual ~MapViewScreen();
};

#endif /* MAPVIEWSCREEN_H_ */
