#ifndef VIDEOPLAYERSCREEN_H_
#define VIDEOPLAYERSCREEN_H_

#include "ActionBase.h"
#include "AppEvents.h"
#include "PostComposerMediaData.h"
#include "ScreenBase.h"
#include "VideoPlayerView.h"

#define APP_EVENTS_COUNT 5

enum VideoPlayerMode {
    eVIDEO_PLAYER_FEED_MODE,
    eVIDEO_PLAYER_CAMERA_ROLL_MODE,
    eVIDEO_PLAYER_POST_COMPOSER_MODE
};

class Post;

class VideoPlayerScreen : public ScreenBase, public IVideoPlayerObserver, public Subscriber {
    friend class VideoPlayerAction;
private:
    enum State{
        EStateInitial,
        EStatePlaying,
        EStatePaused,
        EStateStopped
    };
public:
    VideoPlayerScreen(VideoPlayerMode mode, void * inputData, const char* CacheFilePath = NULL);
    Evas_Object *GetPlayerLayout();
    virtual ~VideoPlayerScreen();
    void Push() override;
    void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = NULL) override;

    bool isCorrectVideoPlayerView(VideoPlayerView *videoPlayerView);

    static void Launch(void* Data);
    static void DeleteWaitDownloadTimer();

private:
    void MainLayoutAdd(Evas_Object *parent);
    void SetVideoView();
    void PlaybackControlAdd(Evas_Object *parent);
    void HeaderAdd(Evas_Object *parent);
    void FooterAdd(Evas_Object *parent);
    void ProgressBarAdd(Evas_Object *parent);
    void UpdateFooter(ActionAndData *action_data);
    void SetPreviousPlayerPos();

    void WaitProgressBarShow();
    void MoveWaitProgress(int width, int height);
    void WaitProgressBarHide();

    bool StartPlayer();
    bool PausePlayer();
    bool StopPlayer();

    void UpdatePosition();

    void MsToMinSec(char *str, int ms);

    void UpdatePanelsVisibility();
    void HidePanels(bool hide);

    void RegisterAppCbs();
    void UnregisterAppCbs();
    void SetRotation();
    void CalcVideoResolution(int *width, int *height);

    void ScreenClicked();
    void SetToInitialPosition();

    void PlayerReady() override;
    void PlayerCompleted() override;
    void PlayerInterrupted() override;

//CallBacks:
    static Eina_Bool OnTimerUpdCb(void *data);
    static Eina_Bool OnExitTimerCb(void *data);
    static Eina_Bool OnDownloadTimerCb(void *data);
    static void OnRotateCb(app_event_info_h event_info, void *user_data);
    static void OnSliderDragStartCb(void *data, Evas_Object *obj, void *event_info);
    static void OnSliderDragStopCb(void *data, Evas_Object *obj, void *event_info);
    static void OnSliderChangedCb(void *data, Evas_Object *obj, void *event_info);
    static void OnPlayerScreenClicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void auto_rotation_setting_cb(system_settings_key_e key, void *user_data);
    /**
     * @brief Reinitialize Confirm Callback to Confirm changes.
     */
    void ConfirmCallback(void *data, Evas_Object *obj, const char *emission, const char *source) override;

    app_device_orientation_e mRotation;

    Evas_Object *mPlaybackControl;
    Evas_Object *mProgressBar;
    Evas_Object *mWaitProgressBar;
    Evas_Object *mFooter;

    Ecore_Timer *mTimer;
    Ecore_Timer *mExitTimer;
    static Ecore_Timer *mWaitDownloadTimer;

    int mPlayingTimeCounter;
    app_event_handler_h mEvent_handlers[APP_EVENTS_COUNT];
    VideoPlayerView *mVideoPlayerView;
    Post *mPost;
    State mState;
    bool mPausedWhileUpdating;

    static const char *mLastVideoId;
    static int mLastVideoPosition;
    int mScreenHeight;
    int mScreenWidth;
    bool mAutoRotationEnabled;

    VideoPlayerMode mMode;
    PostComposerMediaData * mMediaData;
    ActionAndData *mVideoActionData;

    bool mIsPlayedFirstTime;
};

#endif /* VIDEOPLAYERSCREEN_H_ */
