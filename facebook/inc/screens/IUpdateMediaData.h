#ifndef IUPDATEMEDIADATA_H_
#define IUPDATEMEDIADATA_H_

class IUpdateMediaData {
public:
    virtual ~IUpdateMediaData() {}
    virtual void UpdateMedia() = 0;
};

#endif /* IUPDATEMEDIADATA_H_ */
