#ifndef USERPHOTOSSCREEN_H_
#define USERPHOTOSSCREEN_H_

#include "ScreenBase.h"
#include "OwnProfileScreen.h"

class PhotosOfYouScreen;
class UploadsScreen;
class AlbumsScreen;

class UserPhotosScreen: public ScreenBase, public Subscriber {
private:
    enum ScreenTabs {
        PHOTOS_OF_YOU_TAB,
        UPLOADS_TAB,
        ALBUMS_TAB
    };

    class TabbarItem {
    public:
        TabbarItem(Evas_Object *parent, ScreenBase *screen, int tabId, const char* text, const char *textName);
        virtual ~TabbarItem(){}
        void SetActive(bool isActive);
        void SetLast(bool isLast);
        ScreenBase * GetScreen();
        int GetTabId();

    private:
        ScreenBase *mScreen;
        int mTabId;
        Evas_Object *mItem;
    };

public:
    UserPhotosScreen(ScreenBase* parentScreen = nullptr, const char* userProfileId = nullptr, const char* userProfileName = NULL, UploadPhotoType targetUploadType = UPLOAD_NONE, void *calBakObj4UsrProfilScr = nullptr);
    virtual ~UserPhotosScreen();
    virtual void Push() override;
    virtual void OnViewResize(Evas_Coord w, Evas_Coord h) override;
    virtual void Update(AppEventId EventId, void* Data) override;
    static void on_screen_delete_cb(void *data, Evas *e, Evas_Object *obj, void *event_info);
    static void on_get_user_profile_name_completed(void* object, char* respond, int code);
    bool HandleBackButton() override;

private:
    PhotosOfYouScreen *mPhotosOfYouScreen;
    UploadsScreen *mUploadsScreen;
    AlbumsScreen *mAlbumsScreen;

    Evas_Object *mFriendsList;

    Elm_Genlist_Item_Class *mItcSearch;
    Elm_Genlist_Item_Class *mItcRequest;
    Elm_Genlist_Item_Class *mItcFriends;

    char* mUserProfileId;
    char* mUserProfileName;
    GraphRequest *mProfileNameGrpReq;

protected :
    /* callbacks to follow scroller */
    static void anim_stop_scroll_cb(void *data, Evas_Object *obj, void *event_info);

    /* Whole view */
    Evas_Object *CreateView(Evas_Object *parent);

    Evas_Object *CreateScroller(Evas_Object *parent);
    Evas_Object *CreateTabbar(Evas_Object *parent);

    /* tabbar's items callbacks */
    static void tabbar_cb(void *data, Evas_Object *obj, const char *emission, const char *source);

    /* getting on a previous screen callback*/
    static void BackButtonClicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void AddPhotoButtonClicked(void *data, Evas_Object *obj, const char *emission, const char *source);

    void DoAnimation(unsigned int wasChoosen, unsigned int isChoosen);
    void UpdateAddPhotoBtn();

    Evas_Object *mTabbar;
    Evas_Object *mScroller;

    Evas_Object *mPhotosOfYouBox;
    Evas_Object *mUploadsBox;
    Evas_Object *mAlbumsBox;

    UploadPhotoType mTargetUploadType;
    void *mCalBakObj4UsrProfilScr;

    static const unsigned int MAX_TABS = 3;
    TabbarItem* mTabbarItems[MAX_TABS];
    unsigned int mTabbarSelectedItem;
};

#endif /* USERPHOTOSSCREEN_H_ */
