#ifndef ALBUMGALLERYSCREEN_H_
#define ALBUMGALLERYSCREEN_H_

#include "ConfirmationDialog.h"
#include "PhotoGalleryScreenBase.h"


class ActionAndData;
class Album;
class Photo;

class AlbumGalleryScreen: public PhotoGalleryScreenBase {

private:
    class AlbumGalleryEventsSubscriber : public Subscriber {
        friend class AlbumGalleryScreen;
    private:
        AlbumGalleryEventsSubscriber(AlbumGalleryScreen *screen);
        virtual ~AlbumGalleryEventsSubscriber();
        virtual void Update(AppEventId eventId, void * data);

        AlbumGalleryScreen *mScreen;
    };

public:
    AlbumGalleryScreen(Album *album, PhotoGalleryStrategyBase* strategy);
    virtual ~AlbumGalleryScreen();

    void PhotoAdded();
    void PhotosAdditionCompleted();
private:
    void ShowAlbumRenamePopup();
    void CloseAlbumRenamePopup();

    void ShowMoreMenu();
    void RenameAlbum(const char*newName);
    void DeleteAlbum();

    void ReloadScreen();
    Evas_Object *CreateGalleryCaption();
    void UpdateAlbumCaption();

    virtual void SecondActionClicked();
    virtual void Push();
    virtual bool HandleBackButton();

    static void onCancelClicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void onRenameClicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void menu_dismiss_cb(void *data, Evas_Object *obj, void *event_info);

    static void rename_selected_cb(void *data, Evas_Object *obj, void *event_info);
    static void delete_selected_cb(void *data, Evas_Object *obj, void *event_info);

    static void on_add_photos_btn_clicked (void *data, Evas_Object *obj, const char *emission, const char *source);

    static void confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event);

    static void on_rename_album_completed(void* object, char* response, int code);
    static void on_delete_album_completed(void* object, char* response, int code);

private:
    Album *mAlbum;
    Evas_Object *mGalleryDetails;
    Evas_Object *mRenameAlbumPopup;
    Evas_Object *mRenameEntry;
    GraphRequest *mRenameRequest;
    GraphRequest *mDeleteRequest;
    AlbumGalleryEventsSubscriber mSubscriber;
    ConfirmationDialog *mConfirmationDialog;
    Evas_Object *mGalleryCaption;
};

#endif /* ALBUMGALLERYSCREEN_H_ */
