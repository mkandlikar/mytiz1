#ifndef IMAGESCAROUSELSCREEN_H_
#define IMAGESCAROUSELSCREEN_H_

#include "ConfirmationDialog.h"
#include "EditPhotoTagScreen.h"
#include "FriendTagging.h"
#include "ImagesCarousel.h"
#include "Popup.h"
#include "ScreenBase.h"

class ImagePostScreen;
class ImagesCarouselAction;
class Post;
class Photo;
struct ImageRegion;

#define PHOTO_TAGGING

class ImagesCarouselScreen : public ScreenBase, public IImageCarouselObserver, public Subscriber {
public:
    enum ImagesCarouselMode {
        eACTION_AND_DATA_MODE,
        eDATA_MODE,
        ePOST_COMPOSER_MODE,
        eCAMERA_ROLL_MODE,
        eWITHOUT_UI_MODE,
        ePROFILE_COVER_MODE
    };

    ImagesCarouselScreen(Eina_List * inputDataList, int defaultPage, ImagesCarouselMode mode,
            ImagePostScreen *postScreen = NULL, ActionAndData * parentActionAndData = NULL);
    /**
     * @brief Constructor for one Photo. Created only for CommentScreen. If you need this constructor for other screen
     * please modify it carefully or create another one.
     */
    ImagesCarouselScreen(Photo * photo, ImagesCarouselMode mode);

    ImagesCarouselScreen(const char *file_path, const char *id, ImagesCarouselMode mode);

    ~ImagesCarouselScreen() override;
    void Push()  override;
    bool HandleBackButton() override;
    void UpdateUI(ActionAndData * actionAndData = NULL);
    void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = NULL) override;

    char *GetCaptionEntryText();
    void EnableSaveButton(bool enable);
    void EnableClearButton(bool enable);
    inline ConfirmationDialog *GetConfirmationDialog() { return mConfirmationDialog; }
    void ShowKeepDiscardPopup();
    void DeleteConfirmationDialog();
    inline FriendTagging *GetTagging() { return mTagging; }
    static void on_anchor_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);
    void OnPause() override;
    void OnResume() override;
    inline char *GetFromId() { return mFromId; }

private:

    static void update_ui(void* data);
    void UpdateUI(Photo * photo);
    void PageChanged(int pageNumber) override;
    void PageDownloaded(unsigned int pageNumber) override;

    void CreateLayout(int defaultPage, Photo * defaultPhoto);
    void CreateFooter(Photo * defaultPhoto);
    void CreateHeader(Evas_Object *parent, int defautPage);
    void UpdateMoreMenuVisibility();
    bool IsSavePhotoAvailable();
    bool IsEditCaptionAvailable();
    bool IsDeletePhotoAvailable();
    unsigned int CalcMoreMenuItems();
    Evas_Object *CreateGradient(const int *color1, const int *color2, bool horizontalGradient);

    void CreateTags();
    bool RemoveTags();
    void UpdateTags();
    void HideTags(bool hide);
    void EnableEditPhotoTagMode();
    void DisableEditPhotoTagMode();
    void ShowImageControls();
    void HideImageControls();
    void CalculateImageGeometry();
    void ScrollStart() override;
    void ScrollStop() override;

    /* This function creates Entry's UI if User wants to change caption of a photo */
    Evas_Object * CreateCaptionEntry();
    void RefreshPhotoText(ActionAndData * photoActionAndData);
    void RefreshLikeAndCommentsInfo(ActionAndData * actionAndData);
    void RefreshTagButton();
    inline void *GetItemAtCurrentPage() const {return eina_list_nth(mItems, mCarousel->GetCurrentPage());}
    inline ActionAndData *GetActionAndDataAtCurrentPage() const {return static_cast<ActionAndData *> (GetItemAtCurrentPage());}

    static void on_footer_button_clicked (void *data, Evas_Object *obj, const char *emission, const char *source);
    static void on_top_bar_buttons_clicked (void *data, Evas_Object *obj, const char *emission, const char *source);
    static void save_changed_caption_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void clear_caption_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void entry_changed_cb(void* data, Evas_Object* Obj, void* EventInfo);
    static void on_scroll_page_cb(void *data, Evas_Object *scroller, void *event_info);
    static void save_photo_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void edit_caption_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void delete_photo_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void deletion_confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event);
    static void on_delete_photo_completed(void *data, char* response, int code);
    static void confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event);
    static void on_selected_text_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void menu_dismiss_cb(void *data, Evas_Object *obj, void *event_info);
    static void on_tag_area_clicked(void *data, Evas *e, Evas_Object *obj, void *event_info);
    static void on_scroller_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void on_edit_tag_button_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    /**
     * @brief Reinitialize Confirm Callback to Confirm changes.
     */
    void ConfirmCallback(void *data, Evas_Object *obj, const char *emission, const char *source)  override;

    static void on_get_image_details_completed(void *data, char* str, int code);
    // Shows the option menu
    void ShowMoreMenu();
    bool IsCaptionChanged();
    void CloseEditCaption();
    void RequestOneImageDetails(const char* imageId);

    FriendTagging *mTagging;
    EditPhotoTagScreen *mEditPhotoTagScreen = nullptr;
    ImagesCarousel *mCarousel;
    Eina_List *mItems;
    ImagePostScreen *mPostScreen;
    ImagesCarouselAction *mImagesCarouselAction;
    ActionAndData *mImagesCarouselActionAndData;
    Post *mPost;
    ImagesCarouselMode mMode;
    Evas_Object * mHeader;
    ActionAndData * mParentActionAndData;
    Eina_List *mRequests;
    Evas_Object *mEditCaptionLayout{nullptr};
    Evas_Object *mEditCaptionEntry{nullptr};
    ConfirmationDialog *mConfirmationDialog{nullptr};
    char *mFromId{nullptr};
    Photo *mDefaultPhoto{nullptr};
    GraphRequest *mDeleteRequest{nullptr};
    Evas_Object *mTopBarButtons = nullptr;
    PopupMenu::PopupMenuItem *mMoreMenuItems = nullptr;
    Evas_Object *mEditCaptionPopup = nullptr;
    bool mIsSaveButtonEnabled;
    bool mIsClearButtonEnabled;
    static const int MESSAGE_LENGTH_TO_SCROLL;

    Evas_Object *mComposerFooter = nullptr;
    Eina_List *mTagList = nullptr;
    ImageRegion * mImageRegion;
    bool mIsShowImageControls = true;
    bool mIsShowTagButton;
    bool mAreTagsShown = false;

    void CreateActionAndDatesFromData(Eina_List *data);
    ActionAndData* CreateActionAndData(void* dataForItem);
};

#endif /* IMAGESCAROUSELSCREEN_H_ */
