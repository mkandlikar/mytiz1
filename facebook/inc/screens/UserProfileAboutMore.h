/*
 *  UserProfileAboutMore.h
 *
 *  Created on: 10th Aug 2015
 *      Author: Manjunath Raja
 */

#ifndef USERPROFILEABOUTMORE_H_
#define USERPROFILEABOUTMORE_H_


class ScreenBase;
class FbResponseAboutMore;
class ActionBase;
class ActionAndData;

class UserProfileAboutMore: public ScreenBase {
    private:
        FbResponseAboutMore* mParsedResponse;
        GraphRequest* mGraphRequest;
        ActionBase* mAction;
        ActionAndData* mActionAndData;
        Evas_Object* mEvasProgressBar;

        void LoadEdjLayout();
        void CreateView();
        void InitGraphRequests();
        void LoadWork();

        static void HeaderBackCb(void* Data, Evas_Object* Obj, void* EventInfo);
        static void SearchCb(void* Data, Evas_Object* Obj, void* EventInfo);
        static void AddWorkCb(void* Data, Evas_Object* Obj, void* EventInfo);
        static void WorkOptionsCb(void* Data, Evas_Object* Obj, void* EventInfo);
        static Evas_Object* NewLabel(Evas_Object* Layout, const char* Part, int WrapMax);
        static Evas_Object* CreateWorkItemView(void* Data, Evas_Object* Obj, const char* Part);
        static void GraphRequestCb(void* Object, char* Respond, int CurlCode);

    public:
        UserProfileAboutMore(ScreenBase* ParentScreen);
        virtual ~UserProfileAboutMore();
};

#endif //USERPROFILEABOUTMORE_H_
