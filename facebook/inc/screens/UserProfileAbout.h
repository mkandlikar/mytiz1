/*
 *  UserProfileAbout.h
 *
 *  Created on: 17th July 2015
 *      Author: Manjunath Raja
 */

#ifndef USERPROFILEABOUT_H_
#define USERPROFILEABOUT_H_

#include "ScreenBase.h"

class FbRespondAbout;

class UserProfileAbout: public ScreenBase {
    private:
        FbRespondAbout *mParsedResponse;
        char *mProfileId;
        Evas_Object* mProgressBar;

        Evas_Object *box;
        Evas_Object *scroller;
        Evas_Object *about_content_box_container;
        Evas_Object *about_overview_layout;
        Evas_Object *mGenList;

        void LoadEdjLayout();
        void CreateView();
        void LoadData();
        void LoadOverview();
        static Evas_Object* NewLabel(Evas_Object* Layout, const char* Part, int WrapMax);
        static void UPAboutHeaderBackCb(void* Data, Evas_Object* Obj, void* EventInfo);
        static void SearchCb(void* Data, Evas_Object* Obj, void* EventInfo);
        static void GROverviewCallBack(void* Object, char* Respond, int CurlCode);
        static Evas_Object* CreateOverviewItemView(void* Data, Evas_Object* Obj, const char* Part);
        static void MoreCb(void* Data, Evas_Object* Obj, void* EventInfo);
        static void UpdateCb(void* Data, Evas_Object* Obj, void* EventInfo);

    public:
        UserProfileAbout(ScreenBase* ParentScreen, const char *profileId);
        virtual ~UserProfileAbout();
        virtual void Push();
};

#endif //USERPROFILEABOUT_H_
