/*
 *  NearByPlacesComposer.h
 *
 *  Created on: 14 OCT 2015
 *      Author: Jeyaramakrishnan Sundar
 */

#ifndef NEAR_BY_PLACES_COMPOSER_H_
#define NEAR_BY_PLACES_COMPOSER_H_

#include "ActionBase.h"
#include "ScreenBase.h"
#include "NearByPlacesScreen.h"
#include "TrackItemsProxyAdapter.h"

class PlacesProvider;
class LocationService;

class NearByPlacesComposer: public ScreenBase, public TrackItemsProxyAdapter
{
public:
    NearByPlacesComposer(NearByPlacesScreen *parentScreen, Evas_Object *container);
    virtual ~NearByPlacesComposer();
    ConfirmationDialog *mConfirmationDialog;
protected:
    virtual void* CreateItem(void* dataForItem, bool isAddToEnd);
    static void on_places_item_click(void *data, Evas_Object *obj, const char *emisson, const char *source);
private:
    static const unsigned int DEFAULT_PLACES_WND_SIZE;
    bool HandleBackButton();

    ActionBase *mAction;
    NearByPlacesScreen *mParentScreen;

    PlacesProvider *mPlacesProvider;
    LocationService *mLocationService;

    Evas_Object* CreateWrapper(Evas_Object* parent, bool isAddToEnd);
    void CreatePlacesItem(Evas_Object* parent, void *action_data);
    Evas_Object *PlacesGet(void *data, Evas_Object *obj, bool isAddToEnd);
    static void hide_btns_cb(void *data, Evas_Object *obj, void *event_info);
    static void show_btns_cb(void *data, Evas_Object *obj, void *event_info);
    static void on_locationServices_get_cb(void *, double latitude, double longitude, int);
};

#endif //NEAR_BY_PLACES_COMPOSER_H_
