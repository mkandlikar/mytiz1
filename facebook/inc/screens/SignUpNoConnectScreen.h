/*
 *  SignUpNoConnectScreen.h
 *
 *  Created on: 29th June 2015
 *      Author: Manjunath Raja
 */

#ifndef SIGNUPNOCONNECTSCREEN_H_
#define SIGNUPNOCONNECTSCREEN_H_

#include "ScreenBase.h"

class SignUpNoConnectScreen: public ScreenBase {
    private:
        void LoadEdjLayout();
        void CreateView();
        static void HeaderBackCb(void* Data, Evas_Object* Obj, void* EventInfo);

    public:
        SignUpNoConnectScreen(ScreenBase *parentScreen);
        virtual ~SignUpNoConnectScreen();

    protected:
        static void TryAgainBtnCb(void *data, Evas_Object *obj, void *EventInfo);
};

#endif /* SIGNUPNOCONNECTSCREEN_H_ */
