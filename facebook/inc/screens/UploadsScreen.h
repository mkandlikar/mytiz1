/*
 * UploadsScreen.h
 *
 *  Created on: Sep 1, 2015
 *      Author: inbperumal
 */

#ifndef UPLOADSSCREEN_H_
#define UPLOADSSCREEN_H_

#include "PhotoGalleryScreenBase.h"


class UploadsScreen: public PhotoGalleryScreenBase {
public:
    UploadsScreen(ScreenBase *parentScreen, const char* userProfileId, PhotoGalleryStrategyBase* strategy);
    virtual ~UploadsScreen();

private:
    void CreateNoPhotosTextLayout();

    virtual void SetDataAvailable(bool isAvailable);

    Evas_Object *mNoPhotosLayout;
};

#endif /* UPLOADSSCREEN_H_ */
