/*
 *  SignUpGenderScreen.h
 *
 *  Created on: 10th June 2015
 *      Author: Manjunath Raja
 */

#ifndef SIGNUPGENDERSCREEN_H_
#define SIGNUPGENDERSCREEN_H_

#include "ScreenBase.h"

class SignUpGenderScreen: public ScreenBase {
    private:
        enum ERadioValue {
            EGenderMale,
            EGenderFemale
        };

        void LoadEdjLayout();
        void CreateView();
        static void HeaderBackCb(void* Data, Evas_Object* Obj, void* EventInfo);

    public:
        SignUpGenderScreen(ScreenBase *parentScreen);
        virtual ~SignUpGenderScreen();

    protected:
        static void ContinueBtnCb(void *data, Evas_Object *obj, void *EventInfo);
        static void GenderChangeCb(void *data, Evas_Object *obj, void *EventInfo);
};

#endif /* SIGNUPGENDERSCREEN_H_ */
