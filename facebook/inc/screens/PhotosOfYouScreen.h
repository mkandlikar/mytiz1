/*
 * PhotosOfYouScreen.h
 *
 *  Created on: Sep 1, 2015
 *      Author: inbperumal
 */

#ifndef PHOTOSOFYOUSCREEN_H_
#define PHOTOSOFYOUSCREEN_H_

#include "PhotoGalleryScreenBase.h"

class PhotosOfYouScreen: public PhotoGalleryScreenBase
{
public:
    PhotosOfYouScreen(ScreenBase *parentScreen, const char* userProfileId, PhotoGalleryStrategyBase* strategy);
    virtual ~PhotosOfYouScreen();

private:
    Evas_Object *mPOYProgressBar;
    Ecore_Timer *mPOYProgBarTimer;
    Evas_Object *mNoPhotosLayout;

    static Eina_Bool poy_prog_bar_timer_cb(void *data);

    void POYDisplayProgressBar();
    void POYHideProgressBar();

    void CreateNoPhotosTextLayout();

    virtual void SetDataAvailable( bool isAvailable );
};

#endif /* PHOTOSOFYOUSCREEN_H_ */
