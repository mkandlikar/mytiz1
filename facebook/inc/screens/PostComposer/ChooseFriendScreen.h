/*
 * ChooseFriendScreen.h
 *
 *  Created on: Sep 9, 2015
 *      Author: ruibezruch
 */

#ifndef CHOOSEFRIENDSCREEN_H_
#define CHOOSEFRIENDSCREEN_H_

#include <Evas.h>
#include <Elementary.h>

#include "ActionBase.h"
#include "PostComposerScreen.h"
#include "ScreenBase.h"
#include "TrackItemsProxyAdapter.h"

class ChooseFriendScreen: public ScreenBase,
                          public TrackItemsProxyAdapter
{
public:
    ChooseFriendScreen(PostComposerScreen *postComposer);
    ~ChooseFriendScreen() override;

private:
    ActionBase* mAction;
    PostComposerScreen *mPostComposerScreen;
    Evas_Object *mSearchEntry;

    void Push() override;

    void* CreateItem(void* dataForItem, bool isAddToEnd) override;
    void SetDataAvailable(bool isAvailable) override;
    void SetNoDataViewVisibility(bool isVisible) override;

    Evas_Object *CreateFriendItem(ActionAndData *action_data, Evas_Object* parent, bool isAddToEnd);

    static void on_item_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_search_keypad_btn_clicked_cb(void *data, Evas_Object *obj, void *event_info);

    static void on_search_btn_clicked_cb(void *data, Evas_Object *obj, void *event_info);
    static void on_search_btn_clicked_signal(void *data, Evas_Object *obj, const char *emission, const char *source);
    void OnSearchBtnClicked();

    static void on_clear_btn_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);

    static const int DEFAULT_INIT_ITEMS_SIZE;
    static const int DEFAULT_MAX_ITEMS_SIZE;
};

#endif /* CHOOSEFRIENDSCREEN_H_ */
