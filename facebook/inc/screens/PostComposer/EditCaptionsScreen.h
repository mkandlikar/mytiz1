#ifndef SCREENS_EDITCAPTIONSSCREEN_H_
#define SCREENS_EDITCAPTIONSSCREEN_H_

#include "ComposerPhotoWidget.h"

class EditCaptionsScreen: public ScreenBase, public IPhotoWidgetEvents {

public:
    EditCaptionsScreen(ScreenBase *screen);
    virtual ~EditCaptionsScreen();

private:
    ComposerPhotoWidget *mPhotoWidget;
    ScreenBase *mScreen;
    Evas_Object *mHeader;
    ConfirmationDialog *mConfirmationDialog;
    Evas_Object *mScrollerGesture;
    Evas_Object *mScrollerBox;
    Evas_Object *mScroller;
    Evas_Object *mMediaBox;
    Eina_List *mMediaListToDelete;

    virtual void Push() override;
    virtual void ConfirmCallback(void *data, Evas_Object *obj, const char *emission, const char *source) override;
    virtual bool HandleBackButton() override;
    virtual void CancelCallback(void *data, Evas_Object *obj, void *event_info) override;

    void CancelChanges();

    void ShowKeepDiscardPopup();
    static void confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event);

    // PhotoWidget callbacks
    virtual void CaptionTextChanged(PostComposerMediaData *mediaData) override {};
    virtual void CropImageButtonClick(PostComposerMediaData *mediaData) override {};
    virtual void DeleteMediaButtonClick(PostComposerMediaData *mediaData, Evas_Object *photoWidgetLayout) override;
};

#endif /* SCREENS_EDITCAPTIONSSCREEN_H_ */
