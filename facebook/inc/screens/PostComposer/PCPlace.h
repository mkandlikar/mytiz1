#ifndef PCPLACE_H_
#define PCPLACE_H_

#include "Place.h"
#include "Post.h"

#include <string>

struct PCPlace {
    PCPlace() {}
    ~PCPlace() {}
    PCPlace(const Place& place);
    PCPlace(const Post::Place& place);
    PCPlace(const PCPlace& place);
    Post::Place *GetPlace();

    std::string mId;
    std::string mName;
    std::string mCity;
};

#endif /* PCPLACE_H_ */
