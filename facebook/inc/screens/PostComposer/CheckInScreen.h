#ifndef CHECKINSCREEN_H_
#define CHECKINSCREEN_H_

#include "ScreenBase.h"
#include "TrackItemsProxyAdapter.h"

class Place;
struct PCPlace;
class PostComposerScreen;
class ActionBase;
class EventCreatePage;
class NearByPlacesScreen;

class CheckInScreen : public ScreenBase, public TrackItemsProxyAdapter {
public:
    enum CheckinScreenMode {
        EFromPostComposer,
        EFromEventScreen,
        ENotFromPostComposer,
        EFromNearByPlacesScreen
    };

public:
    static void OpenScreen(ScreenBase *screen, CheckinScreenMode mode, PCPlace *selectedPlace = NULL);
    virtual ~CheckInScreen();
    void PlaceSelected(Place *place);

private:
    CheckInScreen(ScreenBase *screen, CheckinScreenMode mode, PCPlace *selectedPlace = NULL);
    virtual void Push();
    virtual void ConfirmCallback (void *data, Evas_Object *obj, const char *emission, const char *source);

    //from TrackItemsProxyAdapter
    virtual void* CreateItem(void* dataForItem, bool isAddToEnd);
    virtual void DeleteItem(void* itemData);
    virtual ItemSize* GetItemSize(void* itemData);

    void UpdateClearButton();

    static Evas_Object* ItemContentGetCb(void *data, Evas_Object *obj, const char *part);

    static void onItemClicked (void *data, Evas_Object *obj, const char *emission, const char *source);
    static void onRemoverClicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void onClearClicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void FilterChangedCb(void *data, Evas_Object *obj, void *event_info);
    static void on_done_btn_clicked_cb(void *data, Evas_Object *obj, void *event_info);

private:
    Evas_Object *mSearchFieldGroup;
    Evas_Object *mSearchField;
    PostComposerScreen *mPostComposer;
    EventCreatePage *mEventCreatePage;
    NearByPlacesScreen *mNearByPlacesScreen;
    CheckinScreenMode mMode;
    PCPlace *mSelectedPlace;
    ActionBase *mAction;
    double mLatitude;
    double mLongitude;

    static const unsigned int DEFAULT_ITEMS_WINDOW_SIZE;
    static const unsigned int DEFAULT_ITEMS_WINDOW_STEP;
    static const unsigned int DEFAULT_ITEMS_INITIAL_WINDOW_SIZE;
};

#endif /* CHECKINSCREEN_H_ */
