#ifndef ADDTEXTSCREEN_H_
#define ADDTEXTSCREEN_H_

#include "PostComposerMediaData.h"
#include "ScreenBase.h"

#ifdef FEATURE_ADD_TEXT

enum AddTextMode
{
    eADD_TEXT_NEW,
    eADD_TEXT_VIEW
};

struct AddTextScreenAndTextData;

class AddTextScreen : public ScreenBase {
private:
    PostComposerMediaData * mMediaData;
    Evas_Object * mPhoto;
    Evas_Object * mClipper;
    Evas_Object * mLayoutGesture;
    Evas_Object * mTextBorder;
    Evas_Object * mSelectedTextImageObject;
    int mPressedXDelta;
    int mPressedYDelta;
    bool mOnDeleteArea;
	// Keeps gesture list for text widgets. Uses to delete callbacks while applying changes.
    Eina_List * mTextGestureList;

    // For checking the boundary conditions
    int mBoundaryLeft;
    int mBoundaryRight;
    int mBoundaryTop;
    int mBoundaryBottom;

    bool mIsMoveStarted;

    void CreateHeader();
    void PlacePhoto();
    void CreateText(AddTextMode mode);
    void CreateClipper();
    void CreateTextBorder();
    AddTextScreenAndTextData * CreateTextImage(PostComposerMediaText * mediaText, Eina_List * textList);
    void LayoutGestureTurnOff();
    void TextGestureTurnOff(bool isEditMode);

    /**
     * @brief Reinitialize Cancel Callback to Confirm when we canceling
     */
    virtual void CancelCallback(void *data, Evas_Object *obj, void *event_info);

    /**
     * @brief Reinitialize Confirm Callback to Confirm changes. Seriously.
     */
    virtual void ConfirmCallback(void *data, Evas_Object *obj, const char *emission, const char *source);

    /**
     * @brief need this to draw text after pressing back key.
     */
    virtual bool HandleBackButton();

    void ApplyChanges();

    static void on_text_delete_cb(void *data, Evas *e, Evas_Object *obj, void *event_info);
    static void on_add_text_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void on_mouse_up_cb(void *data, Evas *e, Evas_Object *obj, void *event_info);

    static Evas_Event_Flags on_text_line_start(void *data, void *event_info);
    static Evas_Event_Flags on_text_line_move(void *data, void *event_info);

    static Evas_Event_Flags on_zoom_move(void *data, void *event_info);
    static Evas_Event_Flags on_zoom_end(void *data, void *event_info);
    static Evas_Event_Flags on_zoom_abort(void *data, void *event_info);

    static Evas_Event_Flags on_rotate_move(void *data, void *event_info);
    static Evas_Event_Flags on_rotate_end(void *data, void *event_info);
    static Evas_Event_Flags on_rotate_abort(void *data, void *event_info);

    static Evas_Event_Flags on_layout_tap_end(void *data, void *event_info);

    static Evas_Event_Flags on_text_tap_end(void *data, void *event_info);

    static bool set_new_thumbnail(media_info_h media, void *user_data);

public:
    AddTextScreen(PostComposerMediaData * mediaData, AddTextMode mode);
    virtual ~AddTextScreen();
    virtual void Push();
    virtual void InsertBefore(ScreenBase * parentScreen);
};

struct AddTextScreenAndTextData {
    AddTextScreenAndTextData(AddTextScreen * screen, Evas_Object * imageTextBackground, Evas_Object * imageTextObject,
            Eina_List * textList, PostComposerMediaText * textData);
    virtual ~AddTextScreenAndTextData();

    AddTextScreen * mScreen;
    Evas_Object * mTextImageBackground;
    Evas_Object * mTextImageObject;
    Eina_List * mTextList;
    PostComposerMediaText * mTextData;

    /**
     * @brief Uses for keeping zoom factor during zoom operation. It needs for limitation of zoom factor.
     */
    double mObjectZoom;
};

#endif  /* FEATURE_ADD_TEXT */

#endif /* ADDTEXTSCREEN_H_ */
