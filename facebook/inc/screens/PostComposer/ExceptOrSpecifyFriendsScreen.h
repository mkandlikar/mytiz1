#ifndef EXCEPT_SPECIFY_FRIENDS_SCREEN_H_
#define EXCEPT_SPECIFY_FRIENDS_SCREEN_H_

#include "DataServiceProtocol.h"
#include "PostComposerScreen.h"


class ExceptOrSpecifyFriendsScreen : public ScreenBase, public IPCPrivacyObserver {
    class ListGroup : public GenListGroupItemBase {
    public:
        ListGroup(const char *groupName, ExceptOrSpecifyFriendsScreen *screen);
        ~ListGroup(){delete []mGroupName;};
        Eina_List *GetList() {return mItems;}

    private:
        virtual Evas_Object* DoCreateLayout(Evas_Object *parent);

//        virtual bool Filter(Elm_Object_Item *insertAfter);
//        virtual int Compare(GenListItemBase *otherItem);

    private:
         char *mGroupName;
         Evas_Object *mLayout;
         ExceptOrSpecifyFriendsScreen *mScreen;
    };


    class ListItem : public GenListItemBase, public IDataServiceProtocolClient {
    public:
        ListItem(ExceptOrSpecifyFriendsScreen *screen);
        ~ListItem();

        void Toggle();
    public:
         Friend *mFriend;
         Evas_Object *mLayout;
         ExceptOrSpecifyFriendsScreen *mScreen;
         bool mIsSelected;
         MessageId mRequestId;

    private:
// from IDataServiceProtocolClient
         virtual void HandleDownloadImageResponse(ErrorCode error, MessageId requestID, const char *url, const char *fileName);
// from GenListItemBase
         virtual Evas_Object* DoCreateLayout(Evas_Object *parent);
         virtual int Compare(GenListItemBase *otherItem);
         virtual bool Filter(Elm_Object_Item *insertAfter);
         void CreateImage(Evas_Object *parent);

    private:
         Evas_Object *mImage;
    };

public:
    enum SelectionMode {
        EExceptedFriendsMode,
        ESpecificFriendsMode
    };

    ExceptOrSpecifyFriendsScreen(PCPrivacyModel *privacyModel, const char *caption, SelectionMode selectionMode);
    virtual ~ExceptOrSpecifyFriendsScreen();
    const char *GetFilterText() {return mFilterText;}

private:
    void Push();
    bool HandleBackButton();

    void FillList(Eina_List *friends);
    void UpdateFields();
    void BackspacePressed();
    ExceptOrSpecifyFriendsScreen::ListItem* GetLastSelectedItem();

    static void key_down_cb(void *data, Evas *e, Evas_Object *obj, void *eventInfo);
    static void on_get_friends_completed(void *object, char *respond, int code);
    static Evas_Object* GroupItemContentGetCb(void *data, Evas_Object *parent, const char *part);

    static void onItemClicked (void *data, Evas_Object *obj, const char *emission, const char *source);
    static void filter_changed_cb(void *data, Evas_Object *obj, void *event_info);
    static void cursor_changed_cb(void *data, Evas_Object *obj, void *event_info);

    virtual void PrivacyChanged(IPCPrivacyObserver::PrivacyEvent privacyEvent);

private:
    Evas_Object *mToField;
    Evas_Object *mSelectField;
    PCPrivacyModel *mPrivacyModel;
    GenList *mGenList;
    ListGroup *mLGPeople;
    int mMinCurPos;
    char *mGeneratedText;
    const char *mFilterText;
    SelectionMode mSelectionMode;
};

#endif /* EXCEPT_SPECIFY_FRIENDS_SCREEN_H_ */
