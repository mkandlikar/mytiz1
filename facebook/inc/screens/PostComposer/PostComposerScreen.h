#ifndef POSTCOMPOSERSCREEN_H_
#define POSTCOMPOSERSCREEN_H_

// Comment the following macros after Samsung provide fix
// for "Unexpected scrolling to top" issue
#define _SCROLLER_WORKAROUND_ENABLED_

#include "ActionBase.h"
#include "Event.h"
#include "CameraRollScreen.h"
#include "FriendTagging.h"
#include "Group.h"
#include "FacebookSession.h"
#include "IUpdateMediaData.h"
#include "PostComposerMediaData.h"
#include "ScreenBase.h"
#include "PrivacyOptionsData.h"
#include "AppEvents.h"
#include "FbRespondGetGroups.h"
#include "Operation.h"
#include "SelectFriendsScreen.h"
#include "PCPrivacyModel.h"
#include "ConfirmationDialog.h"
#include "ComposerPhotoWidget.h"

class ShareWithScreen;
class Post;
class SelectAlbumScreen;
class LinkPreview;
class ComposerPhotoWidget;

enum PostComposerMode {
    eDEFAULT,
    eSHARE,
    eSHARE_EVENT,
    eEDIT,
    eADD_GROUP_POST,
    eCREATEALBUM,
    eCREATEALBUMANDNOTIFY,
    eSHAREDAPPCONTROL,
    eADDPHOTOSTOALBUM,
    eSHARELINKWEB,
    eFRIENDTIMELINE,
    eADD_EVENT_POST,
    eDEFAULTAPPTEXTSHARE,
    eSHARETEXTPICTURES,
    eSHAREGROUP
};

class DataUploader;

class PostComposerScreen: public ScreenBase, public IPCPrivacyObserver, public Subscriber, public ISelectFriendsScreenClient, public IPhotoWidgetEvents {
public:
    enum ChangeType {
        EText,
        ETagged,
        EMedia,
        EPrivacy,
        EPlace,
        ENothing
    };

    enum AlbumButtonMode {
        EActive,
        EShown,
        EHidden
    };

public:
    PostComposerScreen(ScreenBase *screen, PostComposerMode mode = eDEFAULT, GraphObject *obj = nullptr, Event *event = nullptr, const char *id = nullptr, Album* album = nullptr, bool isDestinationSelectorEnabled = true, char * notifMsgToShowAtPopScr = nullptr);
    /**
     * Use for eEDIT mode
     */
    PostComposerScreen(ScreenBase *screen, PostComposerMode mode, ActionAndData *action_data,
            Operation::OperationDestination opDest, const char * targetId, const char * targetName, const char * targetPrivacy);
    /**
     * Use for eFRIENDTIMELINE, eADD_GROUP_POST or eADD_EVENT_POST
     */
    PostComposerScreen(ScreenBase *screen, PostComposerMode mode, const char * targetId, const char * targetName, const char * targetPrivacy);
    virtual ~PostComposerScreen();
    virtual void Push() override;
    virtual void InsertBefore(ScreenBase * parentScreen) override;
    virtual bool HandleBackButton() override;
    virtual bool HandleKeyPadHide() override;
    virtual void CancelCallback(void *data, Evas_Object *obj, void *event_info) override;
    virtual void OnResume() override;
    virtual void OnPause() override;

    void PostChanged(ChangeType type);

    static Evas_Object *CreateCaptionText(Evas_Object *parent, const char* text);

    PCPrivacyModel &GetPrivacy() {return *mPrivacyModel;}
    void SetSelectedFriendsTimeline(const char*id, const char *name);
    void SetSelectedGroupTimeline(Group *group);
    void SetPhotoAlbumButtonMode(AlbumButtonMode newMode);

    void SetGeneratedText();
    void SetGeneratedEditText();
    static char *GenPrivacy(PCPrivacyModel *privacyModel);
    void AlbumSelected(Album* album, bool isNewCreation);
    void PlaceSelected(Place* place);
    void DeleteMediaList();
    void SetEntryText(char *text);

    static char *GenExceptedFriendsWhoAreTagged(const Eina_List *list);

    void SetMessageEntryGuide();
    /**
     * Create box for all media content in scroller and "Add Photo" button
     */
    void CreateSelectedMedia();
    void SetMediaListToDelete(Eina_List *mediaListToDelete);

private:
    enum PreviewWidgetMode{
        ENone,
        EProgress,
        EPreview,
        EPlaceholder
    };

    class PreviewWidgetData {
    public:
        PreviewWidgetData();
        PreviewWidgetData(JsonObject * object);
        ~PreviewWidgetData();

         inline const char *GetName() const {return mName;}
         inline const char *GetCaption() const {return mCaption;}
         inline const char *GetDescription() const {return mDescription;}
         inline const char *GetImage() const {return mImage;}

         void SetName(const char *name);
         void SetCaption(const char *caption);
         void SetDescription(const char *description);
         void SetImage(const char *image);

    private:
        char *mHref;
        char *mName;
        char *mCaption;
        char *mDescription;
        char *mImage;
    };

    //methods
    void InitData();
    void Init(Post * post, Event *event);
    void CreateHeader();
    void CreateDestinationSelector();
    void CreateFooterButtons();

    void LaunchTagFriendsScreen();
    void LaunchCheckInScreen();

    void CreatePrivacy(Evas_Object * parent);
    void SetPrivacyContent();

    void ManageText();
    void CreateText();
    void RemoveText();

    void SetDestinationData(const char *id, const char *name, const char *privacy, Operation::OperationDestination destination);
    void DownloadGroupMembers(const char *id);

    /**
     * @brief Removes temp directory which uses for adding text to the photo or crop photo.
     */
    void DeletePostComposerTempDirectory();

    /**
     * Create "Add Photo" button at the end of media list
     */
    void CreateAddPhotoButton(Evas_Object * parent);

    Evas_Object *CreateSharedItemContent(Evas_Object *parent);
    void ShowPreviewWidget(PreviewWidgetMode mode, const PreviewWidgetData *preview);
    void RemovePreviewWidget();
    void DiscardSharedLink();
    void DeleteSharedLink();
    Evas_Object *CreateSharedEventItemContent(Evas_Object *parent);
    void PrepareEditContent();
    void ShowEditContent(Eina_List *mediaDataList);
    Evas_Object *CreateCreateAlbumContent(Evas_Object *parent);

    static Evas_Event_Flags on_video_clicked(void *data, void *event_info);

    Friend *GetFriendById(const char*id);
    bool NothingToPost();
    bool IsSomethingToLose();
    bool IsModeAllowsEmpty();
    bool IsPostEmpty();
    bool IsPostEdited(bool includeCaptionChanges = true, bool includeMediaListChanges = true);
    bool IsTextChanged();
    bool IsPlaceChanged();
    bool IsEditGroupPost();

    inline bool IsOnFriendTimelinePost() {return (mToId && mToName && mPostDestinationType == Operation::OD_Friend_Timeline);}
    inline bool IsEditOnFriendTimelinePost() {return (mMode == eEDIT && IsOnFriendTimelinePost());}
    bool IsAudienceChanged();
    char *GetEntryText(Evas_Object *entry);
    void DisablePostButton(bool disable);
    void UpdateDestinationSelector();
    void UpdatePrivacyField();
    void EnablePrivacyField(bool enable);
    void ShowPrivacyIsLoading();
    void UpdateGroupMembersList();
    void RefreshScreen();

    char* GenTaggedFriends();
    char* GenPlace();

    void SendingStarted();
    void SendingCompleted();

    void DeleteEntryCallbacks();
    void SetEntryCallbacks();

    void LockScreen() override;

    void SwitchFromDefaultToAddPhotosToAlbumMode();
    void SwitchFromAddPhotosToAlbumToDefaultMode();

    const char *GenTaggedList();

    virtual void PrivacyChanged(IPCPrivacyObserver::PrivacyEvent privacyEvent) override;
    virtual void Update(AppEventId eventId, void *data) override;

//from ISelectFriendsScreenClient
    virtual void FriendsSelectionCompleted(Eina_List *friends, bool isCanceled) override;

    void CheckForLink();
    bool IsDiscardedLink(const char *link);

    void UpdateGenText(const char *text);
    void UpdateGenTextField();

    //This means that mPrivacyLayout must be created.
    bool IsPrivacyFieldRequired(Post *post);
    //This means that privacy field is availbale and can be selected/changed.
    bool IsPrivacyEditable();

    // PhotoWidget callbacks
    virtual void CaptionTextChanged(PostComposerMediaData *mediaData) override;
    virtual void CropImageButtonClick(PostComposerMediaData *mediaData) override;
    virtual void DeleteMediaButtonClick(PostComposerMediaData *mediaData, Evas_Object *photoWidgetLayout) override;

    static void on_create_album_completed(void* object, char* respond, int code);
    static void on_get_album_details_completed(void* object, char* respond, int code);
    static void on_tag_friend_button_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void on_checkin_button_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void add_media_button_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void add_select_album_button_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void entry_anchor_clicked_cb(void *data, Evas_Object *obj, void *event_info);
    static void on_get_groups_request_completed(void* object, char* respond, int code);
    static void on_set_default_privacy_completed(void* object, char* response, int code);

    static void key_down_cb(void *data, Evas *e, Evas_Object *obj, void *event_info);

    static void album_name_key_down_cb(void *data, Evas *e, Evas_Object *obj, void *event_info);

    static void on_remove_preview_widget_cb(void *data, Evas_Object *obj, const char *emission, const char *source);

    static void message_entry_focus_in(void *data, Evas_Object *obj, void *event_info);
    static void message_entry_focus_out(void *data, Evas_Object *obj, void *event_info);

    static Evas_Event_Flags on_image_clicked_cb(void *data, void *event_info);

    static Eina_Bool update_gen_text_field(void *data);

    /**
     * @brief Starts GetPrivacyOptions request to facebook
     */
    void StartGetPrivacyOptionsRequest();

    /**
     * @brief This callback is called when PostPhoto request is completed
     * @param object - callback object
     * @param respond - http respond
     * @param code - curl code
     */
    static void on_photo_upload_completed(void* object, char* respond, int code);
    static void on_dest_selector_clicked (void *data, Evas_Object *obj, const char *emission, const char *source);
    static void on_to_selector_clicked (void *data, Evas_Object *obj, const char *emission, const char *source);
    static void onfriendstimeline_item_select_cb(void *data, Evas_Object *obj, void *event_info);
    static void ingroup_item_select_cb(void *data, Evas_Object *obj, void *event_info);
#ifdef _SCROLLER_WORKAROUND_ENABLED_
    static void unexpected_scroll_cb(void *data, Evas_Object *obj, void *event_info);
    static void mouse_down_cb(void *data, Evas *e, Evas_Object *obj, void *event_info);
#endif

    static void on_link_preview_completed(void* object, char* response, int code);

    virtual void ConfirmCallback (void *data, Evas_Object *obj, const char *emission, const char *source) override;

    bundle* CreatePostBundle();
    void PostStatus();
    /**
     * @brief Uploading post with photos/video to FB server
     * @return - true if process has been launched, false in other cases
     */
    virtual bool SendPostWithPhoto();

    /**
     * @brief This method checks if video id attached
     * @return if post contains video attachment result will be 'true',
     *          otherwise 'false'.
     */
    bool IsVideoMediaPresented();

    void ShowKeepDiscardPopup();
    void ShowConfirmAccessForExceptedToPopup(const Eina_List *list);

    static void on_get_suggested_friends_completed(void* data);
    static void on_get_group_members_completed(void* data);

    static void posttofacebook_item_select_cb(void *data, Evas_Object *obj, void *event_info);

    static void confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event);
    static void confirmation_access_for_excepted_cb(void *user_data, ConfirmationDialog::CDEventType event);

    static void select_album_screen_dismiss_cb(void *data, Evas_Object *obj, void *event_info);

    static void TextChangedCb(void *data, Evas_Object *obj, void *event_info);

    static void on_image_details_received(void *data, char* str, int code);

    void CloseSelectAlbumPopup();

    void SetToId(const char *id);
    const char *GetToId();

    void SetToName(const char *name);
    const char *GetToName();

    void SetToPrivacy(const char *privacy);
    const char *GetToPrivacy();

    void EnableKeypad(Eina_Bool enabled);
    void RemoveEntryFocus();

    static Evas_Event_Flags  show_input_text_panel(void *data, void *event_info);

    Eina_List *GetFriendsListForSelection();
    void UpdateTaggedFriends(Eina_List *friends);

    Eina_List *GetFriendInfos();

    inline bool IsMediaListChanged() { return eina_list_count(mMediaListToDelete) > 0; }

    void RefreshPhotoCaption(PostComposerMediaData *media);
    void RequestImageData();

#ifdef _SCROLLER_WORKAROUND_ENABLED_
    void StoreScrollPosition();
    void RestoreScrollPosition();
#endif

    Privacy *GetEditedPostPrivacy();

private:
    //members
    FriendTagging *mTagging;
    ScreenBase *mScreen;
    PostComposerMode mMode;
    const char * mId;
    PrivacyTypes mDestPrivacy;
    Eina_List * mDestDeny;
    Eina_List * mDestAllow;
    GraphRequest * mRequest;
    const char * mshare_appcontrolpath;
    Evas_Object *mScroller;
    Eina_List *mImageDetailsRequests;
#ifdef _SCROLLER_WORKAROUND_ENABLED_
    Evas_Coord mScrollPosX;
    Evas_Coord mScrollPosY;
    Evas_Coord mScrollWidth;
    Evas_Coord mScrollHeight;
#endif

    /**
     * Media box contains all media widgets and "Add Photo" button
     */
    Evas_Object * mMediaBox;

    Evas_Object *mSharedItemBox;
    /**
     * This is the entry for creating post message
     */
    Evas_Object * mMessageEntry;
    Evas_Object * mAlbumNameEntry;
    Evas_Object * mAlbumDescriptionEntry;
    /**
     * Layout contains footer buttons
     */
    Evas_Object * mFooterLayout;

    Evas_Object *mPrivacyLayout;
    Evas_Object *mToSelectorBox;
    Evas_Object* mPrivacyField;
    Evas_Object* mScrollerBox;
    Evas_Object *mTextLayout;

    Evas_Object *mGenTextLayout;
    Ecore_Timer *mGenTextUpdateTimer;

    char *mToId;
    char *mToName;
    char *mToPrivacy;
    Operation::OperationDestination mPostDestinationType;
    Eina_List *mGroupMembers;
    PCPrivacyModel *mPrivacyModel;
    ShareWithScreen* mShareWithScreen;
    SelectFriendsScreen* mSelectFriendsScreen;
    Post *mSharedPost;
    Post *mPostToEdit;
    Event *mSharedEvent;
    Album *mAlbumToAddPhotos;
    PCPlace *mPlace;
    Evas_Object* mDestinationSelector;
    bool mIsPrivacyFieldEnabled;
    bool mIsSendingPost;
    bool mIsVideoMedia;
    Evas_Object *mSelectAlbumPopup;
    AlbumButtonMode mAlbumButtonMode;
    ActionAndData* mActionAndData;
    ActionAndData* mParentActionNData;

    Evas_Object * mHeader;

    Evas_Object * mAddPhotoButtonLayout;
    Evas_Object * mScrollerGesture;

    SelectAlbumScreen *mSelectAlbumScreen;
    DataUploader *mSuggestedFriendsUploader;
    ConfirmationDialog *mConfirmationDialog;

    DataUploader *mGroupMembersUploader;

    char *mSharedLink;
    Group *mSharedGroup;
    char *mNotifMsgToShowAtPopScr;  // Contains the Message like "Your photo will be uploaded. Check progress in the notifications window." during the closing of the post composer screen.
    Evas_Object *mPreviewWidgetLayout;
    bool mIsPasteRequested;
    Eina_List *mDiscardedLinks;
    GraphRequest *mReqLinkPreview;

    //Flag showing that the entry text came from a sharing source, with optional pictures.
    //When the flag is set, new pictures are not allowed to be added to the Post (workaround
    //  due to non-standard paths to images shared by Memo app).
    bool mShareTextPictures;

    bool mIsAudienceChanged;
    static const int MAX_POST_MESSAGE_LENGTH;
    static const unsigned int MAX_ALBUM_TITLE_LENGTH;

    bool mIsDestinationSelectorEnabled;

    Eina_List *mMediaListToDelete;

    ComposerPhotoWidget *mPhotoWidget;
};

#endif /* POSTCOMPOSERSCREEN_H_ */
