/*
 * ChooseGroupScreen.h
 *
 *  Created on: Sep 9, 2015
 *      Author: ruibezruch
 */

#ifndef CHOOSEGROUPSCREEN_H_
#define CHOOSEGROUPSCREEN_H_

#include <Evas.h>
#include <Elementary.h>

#include "ActionBase.h"
#include "PostComposerScreen.h"
#include "ScreenBase.h"
#include "TrackItemsProxyAdapter.h"

class ChooseGroupScreen: public ScreenBase,
                         public TrackItemsProxyAdapter
{
public:
    ChooseGroupScreen(PostComposerScreen *postComposer);
    virtual void Push();
    virtual ~ChooseGroupScreen();
private:
    ActionBase* mAction;
    PostComposerScreen *mPostComposerScreen;
    Evas_Object *mSearchEntry;

    virtual void* CreateItem( void* dataForItem, bool isAddToEnd );
    virtual void SetDataAvailable(bool isAvailable);
    virtual void SetNoDataViewVisibility(bool isVisible);

    Evas_Object *CreateGroupItem(ActionAndData *action_data, Evas_Object* parent, bool isAddToEnd);

    static void on_item_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_search_keypad_btn_clicked_cb(void *data, Evas_Object *obj, void *event_info);

    static void on_search_btn_clicked_cb(void *data, Evas_Object *obj, void *event_info);
    static void on_search_btn_clicked_signal(void *data, Evas_Object *obj, const char *emission, const char *source);
    void OnSearchBtnClicked();

    static void on_clear_btn_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);

    static const int DEFAULT_INIT_ITEMS_SIZE;
    static const int DEFAULT_MAX_ITEMS_SIZE;
};

#endif /* CHOOSEGROUPSCREEN_H_ */
