#ifndef SHAREWITHSCREEN_H_
#define SHAREWITHSCREEN_H_

#include "PostComposerScreen.h"
#include "ScreenBase.h"


class ShareWithScreen: public ScreenBase, public IPCPrivacyObserver {
private:
    enum RadioValue {
        EPublic,
        EFriends,
        EMore,
        EFriendsExcept,
        EFriendsSpecific,
        EOnlyMe,
        ENextRadioValue
    };
    class ListItem {
    public:
        ListItem(int radioValue, PCPrivacyOption *privacyOption, const char *bottomText, const char *secondImage, PCPrivacyValue privacyValue);
        ListItem(RadioValue radioValue, const char *firstImage, const char *topText, const char *bottomText, const char *secondImage, PCPrivacyValue privacyValue);
        ~ListItem();

        void AddLayout(Evas_Object *parent, int width);
        void RemoveLayout(Evas_Object *parent);
        void UpdateLayout();

        void SetBottomText(const char *bottomText);

    private:
        Evas_Object* CreateImage(const char *file);

    public:
        Evas_Object *mItemLayout;
        int mRadioValue;
        const char *mFirstImage;
        const char *mTopText;
        const char *mBottomText;
        const char *mSecondImage;
        PCPrivacyValue mPrivacyValue;
        PCPrivacyOption *mPrivacyOption;
    };
public:
    ShareWithScreen(ScreenBase *screen, PCPrivacyModel *privacyModel, Post *post);
    virtual ~ShareWithScreen();

private:
    void Push() override;
    bool HandleBackButton() override;
    void ConfirmCallback (void *data, Evas_Object *obj, const char *emission, const char *source) override;
    void CancelCallback(void *data, Evas_Object *obj, void *event_info) override;
    virtual void OnResume() override;

    void ShowConfirmAccessForExceptedToPopup(const Eina_List *list);
    void ShowKeepDiscardPopup();

    void CreateOptionsList();
    void SetActivePrivacyOption();
    void RefreshList();

    void DisablePostButton(bool disable);
    ListItem *GetListItemByPrivacyOption(PCPrivacyOption* po);
    ListItem *GetListItemByLayout(Evas_Object *layout);
    ListItem *GetListItemByDescription(const char *text);
    ListItem *GetListItemById(int Id);
    ListItem *GetSelectedListItem();

    void ChangePrivacy();

    static void onItemMoreClicked (void *data, Evas_Object *obj, const char *emission, const char *source);
    static void onItemClicked (void *data, Evas_Object *obj, const char *emission, const char *source);

    static void confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event);
    static void confirmation_access_for_excepted_cb(void *user_data, ConfirmationDialog::CDEventType event);

    void PrivacyChanged(IPCPrivacyObserver::PrivacyEvent privacyEvent) override;

private:
    ScreenBase *mScreen;
    Evas_Object *mBox;
    Evas_Object *mRadioGroup;

    Eina_List *mListItems;
    PCPrivacyModel *mPrivacyModel;
    Post *mPost;
    ConfirmationDialog *mConfirmationDialog;
    bool mStartedFriendsSelection{false};
};

#endif /* SHAREWITHSCREEN_H_ */
