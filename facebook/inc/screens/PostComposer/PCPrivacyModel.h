#ifndef PCPRIVACYMODEL_H_
#define PCPRIVACYMODEL_H_

#include "Friend.h"
#include "Post.h"
#include "PrivacyOptionsData.h"


class IPCPrivacyObserver {
public:
    enum PrivacyEvent {
        EPrivacyOptionManuallyChanged,
        EPrivacyOptionChangedByProgram,
        EExceptedFriendsChanged,
        ESpecificFriendsChanged,
        EExceptedFriendsLimitExceeded,
        ESpecificFriendsLimitExceeded,
        EPrivacyOptionsRefreshed //This event is sent twice. Once when data is read from cache and next time when data is updated from the FB server.
    };
    virtual ~IPCPrivacyObserver(){};
    virtual void PrivacyChanged(PrivacyEvent privacyEvent) = 0;
};

enum PCPrivacyValue {
    EVUnknown,
    EVEveryone,
    EVAllFriends,
    EVFriendsOfFriends,
    EVSelf,
    EVCustomExcepted,
    EVCustomSpecific
};

enum PCPrivacyFriends {
    EFNone,
    EFAllFriends,
    EFFriendsOfFriends,
    EFSomeFriends
};

class PCPrivacyOption : public PrivacyOption {
public:
    PCPrivacyOption(const char *id, const char *description, const char *type, const char *icon, bool isCurrentlySelected) :
        PrivacyOption(id, description, type, icon, isCurrentlySelected) {}

    ~PCPrivacyOption(){};
    const char *GenIconPath(bool isSmall) override;
    void Update(const char *id, const char *description, bool isCurrentlySelected);


};


class PCPrivacyModel : public IPrivacyOptionsObserver, public Subscriber {
    enum State {
        ENotReady,
        ECachedData,
        EUpdatedData
    };
public:
    PCPrivacyModel();
    ~PCPrivacyModel();

    void AddObserver(IPCPrivacyObserver* observer) {mObservers = eina_list_append(mObservers, observer);}
    void RemoveObserver(IPCPrivacyObserver* observer) {mObservers = eina_list_remove(mObservers, observer);}

    char *GenDeniedFriends();
    char *GenAllowedFriends();

    const char *GenDescriptionForCustomDenied();
    const char *GenDescriptionForCustomAllowed();

    Eina_List *GetOptionsList(){return mOptions;}
    void UpdateOptions(Eina_List *newOptions);
    PCPrivacyOption *GetStandardOptionByType(const char *type);
    PCPrivacyOption *GetStandardOption(PCPrivacyValue privacyValue);
    PCPrivacyOption *GetActiveOption();
    PCPrivacyOption *SetOptionFromPostPrivacy(Privacy *postPrivacy);
    bool IsPostPrivacyChanged(Privacy *postPrivacy);

    void PrivacyOptionSelected(PCPrivacyOption *privacyOption);
    bool DeniedFriendSelected(Friend *fr, bool manual);
    bool AllowedFriendSelected(Friend *fr, bool manual);
    void DeniedFriendUnselected(Friend *fr);
    void AllowedFriendUnselected(Friend *fr);

    Eina_List *GetDeniedFriends() const;
    Eina_List *GetAllowedFriends() const;
    bool IsDeniedFriend(Friend *fr) {return (eina_list_data_find(mDeniedFriends, fr) != NULL);}
    bool IsAllowedFriend(Friend *fr) {return (eina_list_data_find(mAllowedFriends, fr) != NULL);}
    void SetPrivacyValue(PCPrivacyValue privacyValue) {mValue = privacyValue;}
    PCPrivacyOption *GetPrivacyOption() {return mPrivacyOption;}

    void UpdateDefaultPrivacyOptionOnFB();

    bool AreDeniedFriends() const;
    bool AreAllowedFriends() const;

    char *GenPrivacyString();

    char *GenCustomPrivacy();

    void EnableRefresh(bool enable);
    inline bool IsModelReady() {return mState != ENotReady;}
    inline bool IsCached() {return mState != EUpdatedData;}

    Eina_List *GetFriendsList() {return mFriends;} //AAA make it const
    Friend *GetFriendById(const char *id) const;

    inline Eina_List *GetTaggedFriends() const {return mTaggedFriends;}

    Eina_List * FindExceptedFriendsWhoAreTagged();
    bool AreThereTaggedFriends();
    bool AreTaggedFriendsChanged(const Eina_List *taggedFriends);
    void SetTaggedFriendsFromPost(Post * post);
    bool ReplaceTaggedFriend(Friend* fr);

    void AppendTaggedFriend(Friend* fr);
    void RemoveTaggedFriend(Friend* fr);
    void ClearTaggedFriends();
    static int sort_cb(const void *d1, const void *d2);

private:
    void AppendOption(PCPrivacyOption *option, PrivacyOption *newOp = nullptr);
    void SetPrivacyOption(PCPrivacyOption *privacyOption, bool manual);
    bool AddDeniedFriend(Friend *fr);
    bool AddAllowedFriend(Friend *fr);
    void RemoveDeniedFriend(Friend *fr);
    void RemoveAllowedFriend(Friend *fr);
    void SetDeniedFriendsFromStr(const char *friends);
    void SetAllowedFriendsFromStr(const char *friends);
    char *GenFriendsList(Eina_List* friends, bool names);
    void NotifyObservers(IPCPrivacyObserver::PrivacyEvent event);

    void StartGetPrivacyOptionsRequest();

    Friend *FindTaggedFriend(const Friend *friendToFind);

//from IPrivacyOptionsObserver
    void UpdatePrivacyOptions(Eina_List *privacyOptions) override;
    void UpdateFriendsList();
    static void start_request_cb(void *data);

//from Subscriber
    void Update(AppEventId eventId, void *data) override;

private:
    Eina_List *mOptions;
    const char *mPrivacyId;
    PCPrivacyValue mValue;
    Eina_List *mDeniedFriends;
    Eina_List *mAllowedFriends;
    PCPrivacyOption *mPrivacyOption;
    PCPrivacyOption *mLastActivePrivacyOption;
    Eina_List *mObservers;
    Eina_List *mTaggedFriends;
    bool mIsRefreshEnabled;
    Ecore_Job *mAsyncJob;
    Eina_List *mFriends;
    State mState;

    static const unsigned int MAX_EXCEPTED_FRIENDS;
    static const unsigned int MAX_SPECIFIC_FRIENDS;
};

#endif //PCPRIVACYMODEL_H_
