#ifndef ADDTEXTINPUTSCREEN_H_
#define ADDTEXTINPUTSCREEN_H_

#include "PostComposerMediaData.h"
#include "ScreenBase.h"

#ifdef FEATURE_ADD_TEXT

class AddTextInputScreen : public ScreenBase {
private:
    PostComposerMediaData * mMediaData;
    Evas_Object * mColorPicker;
    Evas_Object * mColorPickerRect;
    Evas_Object* mEntry;
    int mSpectrumSourceHeight;

    /**
     * @brief List of edited text
     */
    Eina_List * mTextList;

    /**
     * @brief Creates header of the screen
     */
    void CreateHeader();

    /**
     * @brief Creates background blurred image
     */
    void CreateBackground();

    /**
     * @brief Creates entry for inputing text and color panel for choosing color of the text
     */
    void CreateEntry();

    /**
     * @brief Creates color picker with rectangle of chosen color
     */
    void CreateColorPicker();

    /**
     * @brief Creates color spectrum for choosing the color of text
     */
    void CreateSpectrumImage();

    /**
     * @brief Calls when user move finger on the color spectrum
     */
    static void on_spectrum_move_cb(void *data, Evas_Object *obj, const char *emission, const char *source);

    /**
     * @brief Hides color picker when user unpress the color spectrum
     */
    static void on_spectrum_unpressed_cb(void *data, Evas_Object *obj, const char *emission, const char *source);

    /**
     * @brief Call Confirm Callback when virtual keyboard is off
     */
    static void on_virtualkeyboard_off_cb(void *data, Evas_Object *obj, void *event_info);

    /**
     * @brief Reinitialize Cancel Callback to Confirm when we canceling
     */
    virtual void CancelCallback(void *data, Evas_Object *obj, void *event_info);

    /**
     * @brief Reinitialize Confirm Callback to Confirm changes. Seriously.
     */
    virtual void ConfirmCallback(void *data, Evas_Object *obj, const char *emission, const char *source);

    virtual bool HandleBackButton();

public:
    AddTextInputScreen(PostComposerMediaData * mediaData, Eina_List * textList = NULL);
    virtual ~AddTextInputScreen();
    virtual void Push();
    virtual void InsertBefore(ScreenBase * parentScreen);
};

#endif  /* FEATURE_ADD_TEXT */

#endif /* ADDTEXTINPUTSCREEN_H_ */
