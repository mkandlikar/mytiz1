#ifndef SELECTALBUMSCREEN_H_
#define SELECTALBUMSCREEN_H_

#include "PostComposerScreen.h"
#include "DataServiceProtocol.h"
#include "TrackItemsProxyAdapter.h"

#include "ActionBase.h"
class SelectAlbumAction;

class SelectAlbumScreen : public TrackItemsProxyAdapter {

public:
    SelectAlbumScreen(PostComposerScreen *postComposer, const char* userProfileId, const char* selectedAlbumId);
    virtual ~SelectAlbumScreen();
    Evas_Object *GetLayout() {return mLayout;}

private:
    virtual void* CreateItem(void* dataForItem, bool isAddToEnd);
    virtual void DeleteItem(void* itemData);
    virtual ItemSize* GetItemSize(void* itemData);

    static void on_album_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void on_create_album_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);
private:
    Evas_Object *mLayout;
    SelectAlbumAction *mAction;
    char* mSelectedAlbumId;
    ActionAndData *mSelectedActionAndData;
    PostComposerScreen *mPostComposer;
    Eina_List *mCoverImagesList;
};

class SelectAlbumAction : public ActionBase {
public:
    SelectAlbumAction(SelectAlbumScreen *screen) {
        mScreen = reinterpret_cast<ScreenBase*>(screen);
    }
};

class DownloadCover : public ImageFileDownloader {
    virtual void ImageDownloaded( bool result );
    Evas_Object *mCoverArea;
    int mWidth;
    int mHeight;
public:
    DownloadCover( ImageFile &albumFile, Evas_Object * coverArea ,int width, int height );
};

#endif /* SELECTALBUMSCREEN_H_ */
