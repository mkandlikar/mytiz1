#ifndef ADDCROPSCREEN_H_
#define ADDCROPSCREEN_H_

#include "IUpdateMediaData.h"
#include "PostComposerMediaData.h"
#include "PostComposerScreen.h"
#include "ScreenBase.h"

#include <Ecore_Legacy.h>
#include <image_util.h>

#define CROP_MARKERS_AMOUNT 8
#define LINE_WIDTH 2
#define TIMEOUT 1.0

class AddCropScreen : public ScreenBase {
public:
    AddCropScreen(PostComposerMediaData *mediaData, PostComposerScreen *screen);
    ~AddCropScreen();
    virtual void Push();

private:
    PostComposerScreen *mScreen;
    PostComposerMediaData *mMediaData;
    Evas_Object *mPhoto;
    Evas_Object *mMoveGesture;
    Evas_Object *mResizeGesture[CROP_MARKERS_AMOUNT];

    struct CropRect {
        int x;
        int y;
        int width;
        int height;
        Evas_Object *cropRect;
        Evas_Object *resizePoint[CROP_MARKERS_AMOUNT];
        Evas_Object *borderLine[CROP_MARKERS_AMOUNT * LINE_WIDTH];
    } mCropRect;

    void SetCropRect(int x, int y, int w, int h) {
        mCropRect.x = x;
        mCropRect.y = y;
        mCropRect.width = w;
        mCropRect.height = h;
    }

    struct ImageRegion
    {
        int x1;
        int y1;
        int w;
        int h;
        int x2;
        int y2;
    } mImgRegion;

    void SetImageRegion(int x, int y, int w, int h) {
        mImgRegion.x1 = x;
        mImgRegion.y1 = y;
        mImgRegion.w = w;
        mImgRegion.h = h;
        mImgRegion.x2 = x + w;
        mImgRegion.y2 = y + h;
    }

    enum MarkerIndex {
        MI_NO_MARKER = -1,    //negative value for non-marker cases
        MI_FIRST_MARKER = 0,  //valid marker indices start from zero
        MI_TOP_LEFT = MI_FIRST_MARKER,
        MI_TOP,
        MI_TOP_RIGHT,
        MI_RIGHT,
        MI_BOTTOM_RIGHT,
        MI_BOTTOM,
        MI_BOTTOM_LEFT,
        MI_LEFT,
        MI_LAST_MARKER = MI_LEFT
    };

    void DrawLayout();

    void DeleteCropRectObjs();
    void RedrawCropRect(int index);

    void DrawCrop(bool restoreCropParams);
    void TransformImage();
    void CalculateCropCoords();
    void SimpleCrop(unsigned char *src_image_buffer, int width, int cropped_width, int cropped_height);
    void SimpleCropRotateJPG(unsigned char *src_image_buffer, int width, int height, int cropped_width, int cropped_height);
    void ApplyNewPicture(unsigned char *ptr);

    bool IsImageChanged();

    int mPrevMomentumX2;
    int mPrevMomentumY2;
    int mPrevMarkerIndex;
    int mRealImageW;
    int mRealImageH;
    int mCroppedImageX1 = 0;
    int mCroppedImageY1 = 0;
    int mCroppedImageX2 = 0;
    int mCroppedImageY2 = 0;

    bool mDisableVerticalCrop;
    bool mDisableHorizontalCrop;

    void RegisterGestureCBs();
    void UnegisterGestureCBs();

    int mElmRotationAngle;
    int mRealRotationAngle;
    int mExifNeededAngle;

    //--------------------
    /**
     * @brief Reinitialize Confirm Callback to Confirm changes. Seriously.
     */
    virtual void ConfirmCallback(void *data, Evas_Object *obj, const char *emission, const char *source);
    /**
     * @brief need this to draw text after pressing back key. Return false to do default behavior after this function.
     */
    virtual bool HandleBackButton();

    static Eina_Bool ecore_timer_cb(void *data);
    static void on_rotate_btn_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static bool set_new_thumbnail_callback(media_info_h media, void *user_data);

    static Evas_Event_Flags momentum_start(void *data, void *event_info);
    static Evas_Event_Flags momentum_end(void *data, void *event_info);
    static Evas_Event_Flags momentum_abort(void *data, void *event_info);
    static Evas_Event_Flags momentum_move_cropRect(void *data, void *event_info);
    static Evas_Event_Flags momentum_move_resizePoint0_TL(void *data, void *event_info);
    static Evas_Event_Flags momentum_move_resizePoint1_T(void *data, void *event_info);
    static Evas_Event_Flags momentum_move_resizePoint2_TR(void *data, void *event_info);
    static Evas_Event_Flags momentum_move_resizePoint3_R(void *data, void *event_info);
    static Evas_Event_Flags momentum_move_resizePoint4_BR(void *data, void *event_info);
    static Evas_Event_Flags momentum_move_resizePoint5_B(void *data, void *event_info);
    static Evas_Event_Flags momentum_move_resizePoint6_BL(void *data, void *event_info);
    static Evas_Event_Flags momentum_move_resizePoint7_L(void *data, void *event_info);

    Ecore_Timer *mTimer;
};

#endif /* ADDCROPSCREEN_H_ */
