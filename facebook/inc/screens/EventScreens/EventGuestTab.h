/*
 * EventGuestTab.h
 *
 *  Created on: Nov 2, 2015
 *      Author: ruibezruch
 */

#ifndef EVENTGUESTTAB_H_
#define EVENTGUESTTAB_H_

#include "EventActions.h"
#include "EventGuestList.h"
#include "ScreenBase.h"
#include "TrackItemsProxyAdapter.h"

#ifdef EVENT_NATIVE_VIEW

class EventGuestTab: public ScreenBase, public TrackItemsProxyAdapter
{
public:
    EventGuestTab(ScreenBase *parentScreen, EventGuestListTabs tab);
    virtual ~EventGuestTab();

    virtual void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = NULL);
    virtual void SetDataAvailable(bool isAvailable);
protected:
    virtual void* CreateItem(void* dataForItem, bool isAddToEnd);
private:
    EventActions* mAction;
    EventGuestListTabs mTab;
    Evas_Object * CreateGuestItem(Evas_Object* parent, ActionAndData *action_data);
    static Evas_Object *GuestsGet(void *data, Evas_Object *obj, bool isAddToEnd);
    void SetGuestFriendshipStatus(Evas_Object * layout, User::FriendshipStatus status);

    virtual void SetNoDataViewVisibility(bool isVisible);

    static void on_item_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);

    /**
     * @brief Object for getting ui constants.
     */
    static UIRes *R;
};

#endif

#endif /* EVENTGUESTTAB_H_ */
