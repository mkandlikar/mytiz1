/*
 * EventCreatePage.h
 *
 *  Created on: Aug 26, 2015
 *      Author: ruibezruch
 */

#ifndef EVENTCREATEPAGE_H_
#define EVENTCREATEPAGE_H_

#include "FriendsAction.h"
#include "ScreenBase.h"

#ifdef EVENT_NATIVE_VIEW

struct PCPlace;

class EventCreatePage: public ScreenBase
{
public:
    EventCreatePage(Event *event, bool isFromProfile);
    virtual ~EventCreatePage();
    void PlaceSelected(Place* place);

private:
    void InitUi();

    virtual void Push();
    virtual bool HandleBackButton();

    void CreateHeadLayout(Evas_Object *parent);
    void CreateStartTimeLayout(Evas_Object *parent);
    void CreateEndTimeLayout(Evas_Object *parent);
    void CreateOptionalLayout(Evas_Object *parent);
    void CreateDetailsEntry(Evas_Object *parent);
    void CreateFriendsPrivacyItem(Evas_Object *parent);
    void CreatePrivacyInfoItem(Evas_Object *parent, bool isCreate);

    Evas_Object* CreateDetailsWrapper(Evas_Object* parent);

    bool CloseTimeSelector();
    bool CloseDateSelector();

    void EnableKeypad(bool isEnabled);
    void HideKeypad();

    FriendsAction* mAction;

    Evas_Object* mDataArea;
    Evas_Object* mStartTime;
    Evas_Object* mEndTime;
    Evas_Object* mDateSelectorPopup;
    Evas_Object* mDatePicker;
    Evas_Object *mTimeSelectorPopup;
    Evas_Object *mTimePicker;
    Evas_Object *mKeepDiscardPopup;
    Evas_Object *mLocationObj;

    struct tm mStartSavedTimeDate;
    struct tm mEndSavedTimeDate;

    Evas_Object *mNameEntry;
    Evas_Object *mDetailsEntry;
    Evas_Object *mDetailsLayout;

    Evas_Object *mPrivacyItem;
    Evas_Object *mPrivacyInfo;

    bool mIsCreate;
    bool mGuestsCanInvite;
    bool mIsPrivate;

    bool mIsFromProfile;

    GraphRequest* mCreateRequest;

    GraphRequest *mRequestData;
    static void on_get_event_profile_completed(void* object, char* respond, int code);

    const char *mId;

    PCPlace *mPlace;

    bool mIsHeaderCallbacksActivated;

    void AddDateCallbacks();
    void RemoveDateCallbacks();
    void AddTimeCallbacks();
    void RemoveTimeCallbacks();

    void UpdateTMStruct (tm *time) const;
    void GetDateTimeValue (Evas_Object *obj, tm *time) const;
    void AdjustEndTime (const tm *newEndTime, bool IsDateChanged);
    int CompareDateTimesWithoutSecs (tm firstTime, tm secondTime) const;
    int CompareTimesWithoutSecs (const tm *firstTime, const tm *secondTime) const;

    //Dismiss dialogue
    void ShowKeepDiscardPopup();
    void CloseKeepDiscardPopup();
    static void onKeepClicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void onDiscardClicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void menu_dismiss_cb(void *data, Evas_Object *obj, void *event_info);
    bool isEventChanged();

    //Select nearby places
    void LaunchCheckInScreen();
    static void on_checkin_button_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);

    /* Time Selector Callbacks */
    static void on_start_date_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void on_end_date_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void cancel_date_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void clear_date_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void set_start_date_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void set_end_date_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);


    void OnDateClicked(bool isStartDate);
    void DateTimeSelected(bool isStartDateTime, bool isDateChanged);

    void UpdateDateTimeFields();

    static void on_start_time_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void on_end_time_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void cancel_time_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void clear_time_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void set_start_time_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void set_end_time_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);

    static void date_changed_cb (void *data, Evas_Object *obj, void *event_info);
    static void time_changed_cb (void *data, Evas_Object *obj, void *event_info);

    void OnTimeClicked(bool isStartTime);

    bool IsDateSet(const tm *dateTime) const;
    void ClearEndDateTime();

    /* Header Callbacks */
    static void on_done_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_edit_done_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_back_cb(void *user_data, Evas_Object *obj, const char *emisson, const char *source);
    static void on_selector_cb(void *user_data, Evas_Object *obj, const char *emisson, const char *source);
    static void on_done_completed(void* object, char* respond, int code);
    static void on_edit_done_completed(void* object, char* respond, int code);

    /* Privacy Callbacks */
    static void on_private_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void on_public_cb(void *user_data, Evas_Object *obj, void *event_info);

    static void set_gcif_true_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void set_gcif_false_cb(void *user_data, Evas_Object *obj, void *event_info);

    void SetEventPrivate();
    void SetEventPublic();

    void ActivateHeaderCallbacks();
    void DeactivateHeaderCallbacks();
};

#endif
#endif /* EVENTCREATEPAGE_H_ */
