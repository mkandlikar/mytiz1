/*
 * EventWidgetFactory.h
 *
 *  Created on: Aug 10, 2015
 *      Author: ruibezruch
 */

#ifndef EVENTWIDGETFACTORY_H_
#define EVENTWIDGETFACTORY_H_

#include "ActionBase.h"

#ifdef EVENT_NATIVE_VIEW

class EventWidgetFactory
{
public:
    EventWidgetFactory() {};
    virtual ~EventWidgetFactory() {};

    static Evas_Object* CreateWrapper(Evas_Object* parent, bool isAddToEnd);

    static Evas_Object *CreateContentSelectorItem(Evas_Object *parent);
    static Evas_Object* CreateInvitedBtns(Evas_Object *parent, ActionAndData *action_data);
    static void CreateSelectedEventItem(Evas_Object *parent, ActionAndData *action_data);
    static void RefreshEventInfoBox(ActionAndData *action_data);
    static Evas_Object *CreateBdayItem(Evas_Object *parent, ActionAndData * action_data);
    static void CreateViewAllItem(Evas_Object *parent);

    static void on_manage_event_clicked(void *user_data, Evas_Object *obj, const char *emission, const char *source);

    static void on_event_status_popup_clicked(void *user_data, Evas_Object *obj, const char *emission, const char *source);

    static void on_edit_event_btn_clicked(void *user_data, Evas_Object *obj, void *event_info);

    static void on_bday_btn_clicked(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_selected_item_clicked(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_selected_birthday_item_clicked(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_view_all_btn_clicked(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_delete_event_btn_clicked(void *data, Evas_Object *obj, void *user_data);
    static void popup_approved_delete_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void popup_cancel_delete_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void popup_signal_cancel_delete_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void on_invite_friends_btn_clicked(void *user_data, Evas_Object *obj, void *event_info);

    static bool CloseEventPopup();

    static void CreateEventJoinStatusPopup(ActionAndData *actionData, Evas_Object *obj);
private:
    /**
     * @brief Object for getting ui constants.
     */
    static UIRes *R;
};

#endif
#endif /* EVENTWIDGETFACTORY_H_ */
