#ifndef EVENTALLPOSTSSCREEN_H_
#define EVENTALLPOSTSSCREEN_H_

#include "FeedAction.h"
#include "ScreenBase.h"
#include "TrackItemsProxyAdapter.h"
#include "FeedProvider.h"

#ifdef EVENT_NATIVE_VIEW

class EventAllPostsScreen: public ScreenBase, public TrackItemsProxyAdapter
{
public:
    EventAllPostsScreen(const char *id);
    virtual void Push();
    virtual bool HandleBackButton();
    virtual ~EventAllPostsScreen();
private:
    FeedAction* mAction;

    const char *mId;

    virtual void* CreateItem( void* dataForItem, bool isAddToEnd );
    virtual void DeletePost( ActionAndData *postData );
    FeedProvider *mFeedProvider;
};

#endif
#endif /* EVENTALLPOSTSSCREEN_H_ */

