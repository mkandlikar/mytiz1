/*
 * EventGuestList.h
 *
 *  Created on: Nov 2, 2015
 *      Author: ruibezruch
 */

#ifndef EVENTGUESTLIST_H_
#define EVENTGUESTLIST_H_

#include "ScreenBase.h"

#ifdef EVENT_NATIVE_VIEW

enum EventGuestListTabs {
    EVENT_GOING_TAB,
    EVENT_MAYBE_TAB,
    EVENT_INVITED_TAB,
    EVENT_DECLINED
};

class EventGuestList: public ScreenBase
{
public:
    EventGuestList(EventGuestListTabs tab);
    virtual ~EventGuestList();
    virtual void Push();
    virtual void OnViewResize(Evas_Coord w, Evas_Coord h);

private:
    short mAttemptCounter;
    Ecore_Timer *mBringInScrollerTimer;

    EventGuestListTabs mStartTab;

    Evas_Object *mFirstViewRect;
    Evas_Object *mSecondViewRect;
    Evas_Object *mThirdViewRect;

    Evas_Object *mTabbar;
    Evas_Object *mScroller;

    Evas_Object *mGoingTab;
    Evas_Object *mMaybeTab;
    Evas_Object *mInvitedTab;

    bool mGoingSelected;
    bool mMaybeSelected;
    bool mInvitedSelected;

    EventGuestListTabs mLowSelectedId;
    EventGuestListTabs mTopSelectedId;

    ScreenBase *mGoingScreen;
    ScreenBase *mMaybeScreen;
    ScreenBase *mInvitedScreen;

    Evas_Object *CreateView(Evas_Object *parent);

    Evas_Object *CreateScroller(Evas_Object *parent);
    Evas_Object *CreateTabbar(Evas_Object *parent);

    Evas_Object *CreateTabbarItem(Evas_Object *parent, const char *text, bool isSelected, EventGuestListTabs tab);

    /* getting on a previous screen callback*/
    static void BackButtonClicked(void *data, Evas_Object *obj, void *event_info);

    static void tabbar_cb(void *data, Evas_Object *obj, void * event_info);
    void TabbarCb(EventGuestListTabs tab);

    static void anim_stop_scroll_cb(void *data, Evas_Object *obj, void *event_info);

    void DoAnimation(EventGuestListTabs wasChoosen, EventGuestListTabs isChoosen);

    short AttempBringInScroll();

    static Eina_Bool bring_in_cb( void *data );

    static const short MAX_BRING_IN_ATTEMPTS;
    static const double BRING_IN_TIME;
};

struct EventGuestListToTab {
    EventGuestList *ffScreen;
    EventGuestListTabs mTab;
    EventGuestListToTab();
    ~EventGuestListToTab();
};

#endif
#endif /* EVENTGUESTLIST_H_ */
