#ifndef EVENTPROFILEPAGE_H_
#define EVENTPROFILEPAGE_H_

#include "ActionBase.h"
#include "AppEvents.h"
#include "FeedAction.h"
#include "EventActions.h"
#include "ScreenBase.h"
#include "SelectFriendsScreen.h"
#include "User.h"

#ifdef EVENT_NATIVE_VIEW

class DataUploader;

class EventProfilePage: public ScreenBase, public Subscriber, public ISelectFriendsScreenClient
{
public:
    EventProfilePage(const char *id, const char * name, Event *event);
    virtual void Push();
    virtual bool HandleBackButton();
    virtual void OnErrorWidgetRetryClickedCb();
    virtual ~EventProfilePage();
    virtual void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = NULL);

    void InviteFriends();

    const char *mId;
private:
    Evas_Object *mScroller;
    Evas_Object *mDataArea;
    Evas_Object *mLoadingPost;
    Evas_Object *mPostWidget;
    Evas_Object *mViewAllWidget;
    GraphRequest *mRequestData;
    GraphRequest *mRequestMyAvatar;
    GraphRequest *mRequestLastPost;
    GraphRequest *mRequestInviteFriends;

    EventActions* mAction;
    FeedAction* mFeedAction;
    ActionAndData *mActionAndData;
    DataUploader *m_GoingUsersDataUploader;
    DataUploader *m_MaybeUsersDataUploader;
    DataUploader *m_InvitedUsersDataUploader;
    DataUploader *m_DeclinedUsersDataUploader;
    Evas_Object *mStatsBox;
    Evas_Object *mEventStatisticWidget;
    Evas_Object *mProfileFriends;
    Evas_Object *mUsersPhotosBox;
    Evas_Object *mMyPhotoWidget;
    Evas_Object *mTheLastPhotoWidget;
    bool mIsEventProfileUploadCompleted;
    ActionAndData * mActionAndDataForAvatarsDownload;
    SelectFriendsScreen *mSelectFriendsScreen;
    DataUploader *mSuggestedFriendsUploader;

    static int NUMBER_OF_FRIENDS_TO_SHOW;

    bool isUsingSavedData;
    bool isReRequest;
    bool isAfterEdit;
    /* comment everything connected with rerequest bar due to G2441 just in case we'll may need this bar in future */
//    Evas_Object *mRerequestBar;

    User * mMe;
    int mFriendsPhotosCreated;

    bool mIsUICreated;

    void SetData(ActionAndData *action_data);

    void RequestData();
    void RequestLastPost();

    void CreateEventProfileFriends();
    int AddEventProfileFriend(const Eina_List * friendsList, int addedCount = 0, bool isMaybeState = false);
    Evas_Object * CreateUserPhoto(User * user, bool isMaybeState, bool isAddToEnd);
    void ShowTheLastPhoto();
    void HideTheLastPhoto();
    void ShowMyPhoto();
    void HideMyPhoto();
    void UpdateStatsBox();
    void CreateInviteFriendsItem();
    char *GenerateFriendsText(const Eina_List * friendsList, bool isMaybeState = false);

//from ISelectFriendsScreenClient
    virtual void FriendsSelectionCompleted(Eina_List *friends, bool isCanceled);

    Eina_List *GetFriendInfos();

    void SendInviteFriendsRequest(Eina_List *friends);

    static void scroll_cb(void *data, Evas_Object *obj, void *event_info);
    void ScrollCb();
    void DoTinyPadding();

    void ActionAndDataCreate(Event *event);

    bool IsUploadCompleted();
    void SetEntityId(const char * id);

    static void on_get_event_profile_completed(void* object, char* respond, int code);
    static void on_get_last_post_completed(void* object, char* respond, int code);

    static void friends_has_been_received_cb(void *data);

    static void on_profile_friends_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void on_invite_friends_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);

    static void invite_friends_request_completed (void* object, char* response, int code);
    static void on_get_suggested_friends_completed(void* data);
    static void on_get_my_avatar_completed(void* object, char* respond, int code);
};

#endif
#endif /* EVENTPROFILEPAGE_H_ */
