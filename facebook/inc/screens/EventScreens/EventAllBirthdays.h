/*
 * EventAllBirthdays.h

*/

#ifndef EVENTALLBIRTHDAYS_H_
#define EVENTALLBIRTHDAYS_H_

#include "ScreenBase.h"
#include "TrackItemsProxyAdapter.h"

#ifdef EVENT_NATIVE_VIEW

class EventAllBirthdays: public ScreenBase, public TrackItemsProxyAdapter
{
public:
    EventAllBirthdays();
    virtual ~EventAllBirthdays() {};
protected:
    virtual void Push();
private:
    virtual void* CreateItem(void* dataForItem, bool isAddToEnd);

    Evas_Object *BdaysGet(void *user_data, Evas_Object *obj, bool isAddToEnd);
};

#endif
#endif /* EVENTALLBIRTHDAYS_H_ */
