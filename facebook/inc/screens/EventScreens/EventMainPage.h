/*
 * EventMainPage.h
 *
 *  Created on: Aug 10, 2015
 *      Author: ruibezruch
 */

#ifndef EVENTMAINPAGE_H_
#define EVENTMAINPAGE_H_

#include "DataUploader.h"
#include "EventActions.h"
#include "ScreenBase.h"
#include "SelectFriendsScreen.h"
#include "TrackItemsProxyAdapter.h"

#ifdef EVENT_NATIVE_VIEW

class EventMainPage: public ScreenBase,
                     public TrackItemsProxyAdapter,
                     public ISelectFriendsScreenClient
{
public:
    EventMainPage();
    virtual void Push();
    virtual ~EventMainPage();
    virtual bool HandleBackButton();

    void InviteFriends(const char* eventId);

protected:
    virtual void* CreateItem(void* dataFotItem, bool isAddToEnd);
    virtual void* UpdateItem(void* dataForItem, bool isAddToEnd);
    virtual void SetDataAvailable(bool isAvailable);

private:
    ActionAndData *mActionData;
    EventActions* mAction;

    DataUploader *mBdayUploader;

    Evas_Object *mNoEvents;
    Evas_Object *mSelectorText;
    Evas_Object *mContextMenu;
    Evas_Object *mFirstBox;
    Evas_Object *mBirthDayBox;
    SelectFriendsScreen *mSelectFriendsScreen;
    GraphRequest *mRequestInviteFriends;
    DataUploader *m_GoingUsersDataUploader;
    DataUploader *m_MaybeUsersDataUploader;
    DataUploader *m_InvitedUsersDataUploader;
    DataUploader *m_DeclinedUsersDataUploader;
    DataUploader *mSuggestedFriendsUploader;

    const char *mId;
    bool mIsEventProfileUploadCompleted;

    Evas_Object *CreateSelector(Evas_Object *parent);
    Evas_Object *ShowNoAvailableEvents(Evas_Object *parent, const char * selectorId);
    Evas_Object *CreateBDay(Evas_Object *parent, bool isAddToEnd);
    static void on_bday_item_delete_cb(void *data, Evas *e, Evas_Object *obj, void *event_info);

    Evas_Object *GetBirthdaysList(bool isAddToEnd);

    static Evas_Object *EventsGet(void *data, Evas_Object *obj, bool isAddToEnd);
    void CreateNearestEvent(Evas_Object *parent);

    static void on_selector_clicked(void *user_data, Evas *e, Evas_Object *obj, void *event_info);
    static void on_create_clicked(void *user_data, Evas *e, Evas_Object *obj, void *event_info);

    static void ctxmenu_dismissed_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void on_upcoming_clicked(void *user_data, Evas_Object *obj, void *event_info);
    static void on_invites_clicked(void *user_data, Evas_Object *obj, void *event_info);
    static void on_saved_clicked(void *user_data, Evas_Object *obj, void *event_info);
    static void on_hosting_clicked(void *user_data, Evas_Object *obj, void *event_info);
    static void on_past_events(void *user_data, Evas_Object *obj, void *event_info);
    static void on_nearby_events(void *user_data, Evas_Object *obj, void *event_info);
    static void invite_friends_request_completed (void* object, char* response, int code);
    static void on_get_suggested_friends_completed(void* data);
    static void friends_has_been_received_cb(void *data);
    virtual void Update(AppEventId eventId, void *data);


    static void on_get_bdays_completed(void* data);

    //from ISelectFriendsScreenClient
    virtual void FriendsSelectionCompleted(Eina_List *friends, bool isCanceled);
    Eina_List *GetFriendInfos();

    void SendInviteFriendsRequest(Eina_List *friends);
    void SetEntityId(const char * id);
    bool IsUploadCompleted();
};

#endif
#endif /* EVENTMAINPAGE_H_ */
