#ifndef LOADREQUESTSCREEN_H_
#define LOADREQUESTSCREEN_H_

#include "FbRespondBase.h"
#include "ScreenBase.h"

/*
 *
 */
class LoadRequestScreen: public ScreenBase
{
public:
    LoadRequestScreen(ScreenBase *parentScreen);
    virtual ~LoadRequestScreen();

    /**
     * @brief - put ProgressBar on the screen and initiate an http request to facebook
     * @param id[in] - id of the object for which data is requested
     */
    void StartRequest();

    /**
     *  @brief - Place the ProgressBar on the screen
     */
    virtual void ShowProgressBar();

    /**
     * @brief Hides the ProgressBar
     */
    virtual void HideProgressBar();

    /**
     *@brief Perform request to facebook
     */
    virtual GraphRequest *  DoRequest() = 0;

    /**
     * @brief Parses the faceebook json response to objects
     * @param response - json object
     * @return parsed object
     */
    virtual FbRespondBase* ParseResponse(char *response) = 0;

    /**
     * @brief - Update screen with received data
     * @param response[in] - the received data
     */
    virtual void SetData(FbRespondBase *response) = 0;

    /**
     * @brief Callback which is called when http request is completed
     * @param object - callback object
     * @param response - http response
     * @param code - curl code
     */
    static void on_request_completed(void *object, char *response, int code);

    //members
    /**
     * @brief - Async thread for implementing http request
     */
     GraphRequest * mRequest;

    /**
     * @brief - the result of http request
     */
    FbRespondBase * mResponse;
};

#endif /* LOADREQUESTSCREEN_H_ */
