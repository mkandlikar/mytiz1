#ifndef SIGNUPINPROGRESSSCREEN_H_
#define SIGNUPINPROGRESSSCREEN_H_

#include "ScreenBase.h"

class LogoutRequest;

typedef enum Progress_Screen_Request_Types_Enum_
{
    UndefinedReq,
    LoginReq,
    LogOutReq
} Progress_Screen_Request_Types_Enum;

class SignUpInProgressScreen: public ScreenBase {
    private:
        void LoadEdjLayout();
        void CreateView();
        void CancelGraphRequest();
        LogoutRequest * mLogOutReq;
    public:
        void* mGraphRequest;
        bool mInProgress;
        Progress_Screen_Request_Types_Enum mRequestType;

        SignUpInProgressScreen(ScreenBase *parentScreen);
        virtual ~SignUpInProgressScreen();
        bool HandleBackButton() override;
        void Push() override;
        static void on_logout_completed(void* object, char* respond, int code);
};

#endif /* SIGNUPINPROGRESSSCREEN_H_ */
