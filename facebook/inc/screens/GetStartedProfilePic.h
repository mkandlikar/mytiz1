/* GetStartedProfilePic.h
 *
 * Created on: 10th July 2015
 * Author: Jeyaramakrishnan Sundar
 *
 */

#ifndef GET_STARTED_PRO_PIC_H
#define GET_STARTED_PRO_PIC_H

#include "ScreenBase.h"
#include "GraphRequest.h"


class GetStartedProfilePic:public ScreenBase
{
    public:
        GetStartedProfilePic(const char *path);
        virtual ~GetStartedProfilePic();
        static GraphRequest * mUploadRequest;

    private:
        void setProfilePic(const char *path);
        void upload_profile_picture(const char *image_file_path);
        void create_entry(Evas_Object*);

    protected:
        static void skip_btn_cb(void *data , Evas_Object *obj, const char *emission, const char *source);
        static void get_started_btn_cb(void *data , Evas_Object *obj, const char *emission, const char *source);
        static void anchor_clicked_cb(void *data, Evas_Object *obj, void *event_info);
        static void upload_profile_picture_completed_cb(void* object, char* respond, int code);
};
#endif
