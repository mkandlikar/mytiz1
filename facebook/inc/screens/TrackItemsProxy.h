#ifndef TRACKITEMSPROXY_H_
#define TRACKITEMSPROXY_H_

#include "AppEvents.h"
#include "ItemsBuilderAssistent.h"

#include <eina_lock.h>
#include <eina_types.h>
#include <Evas.h>
#include <Ecore.h>

class GraphRequest;
class AbstractDataProvider;

/**
 * @brief  This is a structure for the common size of a UI element.
 * @param  'key'    The unique identifier of an element/item.
 * @param  'height' The height of an element/item.
 * @param  'weight' The weight of an element/item.
 */
typedef struct ItemSize_ {
    ItemSize_( const char *k, unsigned int h, unsigned int w ) {
        key = k;
        height = h;
        weight = w;
    }
    const char *key;
    unsigned int height;
    unsigned int weight;
} ItemSize;

/**
 * @brief This class implements a 'lightweight' and up-to-date scrolling with its own cache.
 * It means that if a widget/scroll has many items (which we need to show to a user) these
 * items will be created during scrolling up or down and the old items (which a user scrolled
 * for long time ago) will be deleted. When a user scrolls back (to the deleted items), these
 * items will be re-created. In the result, the will be a window with 'alive' items. Not
 * alive items will be cached and used when it needed.
 */

class TrackItemsProxy;

class ItemsCache
{
    friend class TrackItemsProxy;
    /**
     * @param 'parent' A pointer to an instance of a parent class, which will be responsible
     *             for some delegated actions (e.g. create and delete items).
     *
     * @param 'provider' The reference to data provider for retrieving the data.
     */
public:
    ItemsCache( TrackItemsProxy* parent, AbstractDataProvider *provider );
    virtual ~ItemsCache();

protected:
    /**
     * @brief This method normalizes the ItemsWindow when the incoming data was added to the
     *        begin of the existing item's sequence.
     *
     * @param 'shiftValue' This value indicates how many items was added to the begin of the
     *        existing item's sequence.
     */
    void NormalizeWindowIndex( bool isDescendigOrder, int shiftValue );

private:
    void SetSettings( unsigned int wndSize, unsigned int wndStep, unsigned int initWndSize );
    /**
     * @brief This method decrease a lower index by 1. Such action required, when the manual item
     *        deletion hasb been performed.
     */
    void DecreaseIndex();

    void EraseWindowData();
    /**
     * @brief This method redraws the current items window.
     */
    void RedrawWindow();

    void UpdateOneItem(void *data);
    /**
     * @brief This method updates UI representation of item. It re-creates a widget without
     *        deletion all widgets internal data (e.g. it does not delete ActionAndData,
     *        GraphObject data).
     */
    void UpdateItemsWindow();

    void UpdateDataForItems();
    /**
     * @brief This method returns true if window do not contains items.
     */
    bool isEmpty();
    /**
     * @brief  This method sets the manual value for items index
     * @note   Use this method ONLY if you definitely knows what you do!
     */
    void SetIndex( int upperBound, int lowerBound );
    inline int GetUpperIndex( ) const { return m_ItemsWindow.upperIndex; }
    inline int GetLowerIndex( ) const { return m_ItemsWindow.lowerIndex; }

    Utils::DoubleSidedStack* GetItemsStack();

    void ResetCurrentWindowSize();

    void DeleteFromTop();
    void DeleteFromBottom( unsigned int limit );

protected:
    /**
     * @brief This method need to be called when a user scrolls 'Down' and the bottom edge has
     *        been reached. Window of items will be shifted down according to 'Max Window Size'
     *        and 'Items Sep'. This functionality will be use cached items data. If there is not
     *        enough cached data, the appropriate value will be return (@see retval).
     *
     * @retval If cache has enough data (items count) to make a full shift, 'true' is going to
     *         be returned, otherwise 'false'.
     */
    virtual bool ShiftDown( bool isPreCreate = false );
    /**
     * @brief This method need to be called when a user scrolls 'Up' and the upper edge has been
     *        reached. Window of items will be shifted up according to 'Max Window Size' and
     *        'Items Step'. This functionality will be use cached items data. If there is not
     *        enough cached data, the appropriate value will be return (@see retval).
     *
     * @retval If cache has enough data (items count) to make a full shift, 'true' is going
     *         to be returned, otherwise 'false'.
     */
    virtual bool ShiftUp();

    virtual void CheckProviderSizeChanged( bool disableNormalization ){};

    unsigned int GetCurrentWindowSize() const { return m_CurrentWindowSize; };

    typedef struct ItemsWindow {
        int upperIndex;
        int lowerIndex;
    } ItemsWindow;
    // This 'window' stores indices (which relates to position if 'item's cache') of lower bound
    // and upper bound
    ItemsWindow m_ItemsWindow;
    // Cache of all items
    Utils::DoubleSidedStack *m_ItemsWindowStack;

    unsigned int m_RemainedItems;
    // How many items need to add during one shift ( down/up ).
    // This value must not be greater then m_MaxWindowSize.
    unsigned int m_ItemsStep;
    // How many items might be displayed (maximum).
    unsigned int m_CurrentWindowSize;
    unsigned int m_MaxWindowSize;
    // The data provider, which will be used to retrieve data/items
    AbstractDataProvider *m_Provider;
    TrackItemsProxy* m_Parent;

    static const int INVALID_WINDOW_INDEX;
    unsigned int m_AddedHeight;
};


class TrackItemsProxy : public ItemsBuilderAssistent,
                        public Subscriber
{
    friend class ItemsCache;

public:

    typedef struct MouseMovement_ {
        int yCoordinate_scroller;
        int xCoordinate_scroller;
        int xCoordinate_Point;
        int yCoordinate_Point;
    } MouseMovement;

    /**
     * @brief This class specifies the base settings as for TrackItemsProxy class, as
     *        for its internal objects.
     * @param 'windowSize' This value is responsible for max items, which might be visible
     *                 in a 'item's window'. It means that when a use scrolls end/begin, the 'alive'
     *                 items will be adjusted up to 'max window size' (some items will be deleted
     *                 from begin/end' edge and added to an opposite side ( e.g. one delete from
     *                 begin and one added to end; or one deleted from end and one added to begin).
     * @param 'windowStep' How many items need to add during one shift ( end/begin ).
     * @param 'direction'  The scroll direction.
     */
    class ProxySettings
    {
        public:
            ProxySettings( unsigned int windowSize, unsigned int windowStep, unsigned int initWndSize = INITIALIZATION_CACHE_WINDOW_SIZE ) :
                        initialWndSize( initWndSize ), wndSize( windowSize ), wndStep( windowStep ) {}
            unsigned int initialWndSize;
            unsigned int wndSize;
            unsigned int wndStep;
    };
    /**
     * @param 'parentLayout'  A pointer to a parent layout.
     * @param 'provider' A pointer to a data provider.
     */
    TrackItemsProxy( Evas_Object* parentLayout, AbstractDataProvider *provider, ItemsComparator updateItemsWndComp, int headerSize = SWITCH_OFF_HEDER_BUFFERIZATION, ItemsCache* itemsCache = nullptr);

    virtual ~TrackItemsProxy();

    /**
     * @brief  This method creates the scroller based on the given parent layout. A user of this class might
     *         to specify settings and data area.
     * @param 'parentLayout'      The parent layout which will be using to create a scroller.
     * @param 'isDataAreaRequired'If this value is 'true' the data area for a scroller will be created, otherwise
     *                            will not created. If a user of this method will not prefere to auto create
     *                            a data area, it could be specified later by using method SetDataArea. It is very
     *                            important, that the scroller can not work without the data area.
     * @param 'settings'          The settings for TrackItemsProxy, which will be used to create a scroller and some
     *                            other extra params.
     */
    void ApplyScroller( Evas_Object *parentLayout, bool isDataAreaRequired = true, ProxySettings *settings = NULL );


    /*
     * @brief This method allows or declines showing progressbar in screens (test method)
     */
    void ApplyProgressBar( bool applied = true );

    /**
     * @brief  This method sets a header area.
     */
    void SetHeaderArea( Evas_Object *headerArea );

    /**
     * @brief  This method sets a data area (but not sets a context for a scroller; so a user must manual set a context).
     */
    void SetDataArea( Evas_Object *dataArea );

    /**
     * @brief  This method erases all data from the scroller.
     */
    void EraseWindowData();
    /**
     * @brief  This method returns a reference to 'Scroller' a object.
     *
     * @retval A reference to a scroller object.
     */
    Evas_Object* GetScroller() const;

    /**
     * @brief This method returns a reference to 'Data Area Layout'. Such layout stores items.
     *
     * @retval A reference to 'Data Area'
     */
    Evas_Object* GetDataArea() const;
    /**
     * @brief This method returns a reference to 'Progress Bar'. Progress Bar will be create when
     *        an application sends a data request to Facebook server and waits for receiving it.
     *
     * @retval If data request has been send, the reference of Progress Bar will be returned,
     *         otherwise NULL.
     */
    Evas_Object* GetProgressBar() const;

    /**
     * @brief This method returns current proxy settings.
     */
    ProxySettings* GetProxySettings() const;

    void AnimatedTop();

    /**
     * @brief This method sets proxy settings.
     * @note  It applies the settings only for ItemsCache class.
     */
    void SetProxySettings( ProxySettings *proxySettings );

    void RequestData( bool isDescendingOrder = true, bool isMuteRequest = false );
    /**
     * @brief This is a 'bridge' to @method ItemsCache::ShiftDown(). See to origin source for
     *        description and retvals.
     */
    bool ShiftDown( bool isPreCreate = false );
    /**
     * @brief This is a 'bridge' to @method ItemsCache::ShiftUp(). See to origin source for
     *        description and retvals.
     */
    bool ShiftUp();
    /**
     * @brief When new data will be received from the Data Provider, this method will be called.
     *        So, this method implements the updating necessary data when it will be received.
     */
    virtual void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = NULL);

    virtual void UpdateErase();
    /**
     * @brief This method redraws the current items window.
     * @note  Use @method UpdateItemsWindow instead of this (in all applicable cases).
     */
    void Redraw();

    void UpdateItemsWindow();

    void UpdateHistoryCount();
    /**
     * @brief This method removes given item from its position in the scroller and
     *        adjusts all scroller window's indices appropriately. Use this method
     *        to manually delete an item from a scroller.
     */
    bool DestroyItem( void *item, ItemsComparator comparator, bool updateHistoryCount = true );

    /**
     * @brief This method finds the target item using its data value, then acts like DestroyItem().
     */
    bool DestroyItemByData( void *data, ItemsComparator comparator, bool updateHistoryCount = true );

    /**
     * @brief This method does mostly the same as DestroyItemByData() but does not delete
     *        the ActionAndData instance associated with the item.
     */
    void * DestroyItemByDataKeepAnD( void *data, ItemsComparator comparator);

    bool IsPresentItem( void *item, ItemsComparator comparator );


    MouseMovement* GetMouseMovement();

    /**
     * @brief Each time when the data will be arrived from Data Provider, this method will be called.
     *        Reimplement in in the user-derived class if you need to specify some screen for "No Items"
     *        purpose.
     */
    virtual void SetDataAvailable( bool isAvailable ){};

    bool IsWaitingForData() const;

    void SetMuteTimerEnabled( bool isEnabled );

    AbstractDataProvider *GetProvider() { return m_Provider; };

    Eina_Array* GetItemsIds();
    void * GetItemById( const char * itemId );
    void CustomUpdateItem( AppEventId eventId, const char *itemId );

    void SetOperationsComparator( ItemsComparator comparator );

    void SetUpdateCacheEnable( bool isEnable );

    void UpdateCachedData();

    virtual ItemSize* SaveSize( void *item ) = 0;
    virtual ItemSize* GetSize( void *item ) = 0;
    virtual void* GetDataForItem( void *item ) = 0;
    virtual void* GetWgtForItem( void *item ) { return nullptr; };
    virtual void UpdateDataForItem(void *item) {};

    static void ShowConnectionErrorBig( std::string layoutName,  Evas_Object* connectionErrorLayout );
    inline void SetScrollerRegionTopOffset(int offset) { m_TopOffset = offset;}

    virtual void CheckOffStageUnfollowItems() {};

protected:
    void SetIdentifier(const char *id);

    virtual char* GetItemId( void *item ){
        return NULL;
    }
    virtual bool FindItemId(void *item, const char *id);

    virtual void IdleStateHandler() {}
    /**
     * @brief This method change Y coordinate for scrolling.
     *
     * @param 'padding' The value which must be added to the begin of Y coordinate.
     */
    virtual void ChangeYCoordinate( int padding, bool isFromTop );
    /**
     * @brief  Implement this method to calculate the height of a screen specific item.
     *
     * @retval The item height.
     *
     * @remark This method is required for adjustment scroll during deletion and
     *         creation new items, to avoid scroll jumping.
     */
    virtual ItemSize* GetItemSize( void* item ) = 0;
    /**
     * @brief Create widget to present no connection.
     * @param name of layout
     */
    void SetConnectionErrorLayoutName(const char *layoutName);
    /**
     * @brief Create progress bar widget.
     * @param name of layout
     */
    void SetProgressBarLayoutName(const char *layoutName);
    /**
     * @brief  This is a callback for the event when a top or left edge (depending on the scroll direction)
     *         has been reached during a scrolling action.
     * @param 'object' User data passed to callback
     */
    static void begin_edge_of_scroll_reached( void *data, Evas_Object *obj, void *event_info );
    /**
     * @brief  This is a callback for the event when a bottom or right edge (depending on the scroll direction)
     *         has been reached during a scrolling action.
     */
    static void end_edge_of_scroll_reached( void *data, Evas_Object *obj, void *event_info );

    static Eina_Bool async_data_area_resize(void *data);
    static void scroller_resize_cb(void *data, Evas *e, Evas_Object *obj, void *event_info);// works if virtual keypad is shown
    /**
     * @brief  This is a callback for the event when user press "No connection" widget
     */
    static void on_connectionerror_widget_handler(void *data, Evas_Object *obj, const char  *emission, const char  *source);

    static Eina_Bool hide_progress_bar_by_timer_cb( void *data );

    static Eina_Bool auto_fetch_top_edge_items_cb( void *data );

    static const int TINY_SCROLL_PADDING;

    void ShowProgressBarWithTimer( bool isToEnd );

    void HideProgressBar();
    virtual void ScrollerReachedTop(bool playSound) {};
    virtual void FetchEdgeItems() {};

    bool isReadyToRequest();

    void EnableRequestData( bool isEnabled );

    void PreFetchTail();

    /**
     * @brief Like New Stories on Home Screen. So, this method will be called, when new items
     *        received from the top edge.
     */
    virtual void NewItemsAvailable() {}
    void SetShowConnectionErrorEnabled( bool isEnabled );
    void EnableBigConnectionErrorWidget( bool isEnabled );
    bool AllDataWasUploaded ();
    void RePackConnectionError( bool packOnTop = false );
    void ShowConnectionError( bool isToEnd );
    void HideConnectionError();
    bool IsConnectionError();
    bool IsEmpty();
    void* FindItem(void *data, ItemsComparator comparator);
    /**
     * @brief This method is responsible for deletion of @struct ItemSize object.
     */
    static void delete_cached_item_size( void *itemSize );

    void RefreshRectangles( bool use_exact_algorithm = false, int offset = 0 );
    void RemoveRectangle( bool isFromTop );
    virtual void ActivateRectangleCb();
    virtual void DeactivateRectangleCb();
    virtual void ActivatePeriodicalRedraw();
    virtual void DeactivatePeriodicalRedraw();
    virtual void OnScrollerResizeActions(){};

    unsigned int GetMaxWindowSize() const { return m_ItemsCache->m_MaxWindowSize; };

    void DisableMouseUpDownEvents(bool disableNow){ m_DisableMouseUpDown = disableNow; };

    // these virtual methods are overridden in CommentsScreenAdapter
    virtual void AddDataAreaResizeCB(){};
    virtual void AddAnimationCBs();
    virtual void RemoveAnimationCBs();
    virtual void AddPresenter(){};
    virtual void RemovePresenter(void *item){};

    // This is a cache for items size
    Eina_Hash *m_ItemSizeCache;
    ItemsCache* m_ItemsCache;

    // Should be overridden in HomeScreen
    virtual bool IsPseudoPostVisible() { return false; };
    virtual void ShowUpToDatePopup() {};
    virtual void ResetUpdatesCount() {};

    inline bool IsServiceWidgetOnBottom() const { return m_ShowServiceWidgetOnBottom; };

    virtual void RebuildAllItems(void){};
    virtual void DataErasedSignal(){};
    bool IsRealTopEdgeOfScroller() { return ( NoOffstageItemsAbove() && IsScrollerRegionOnTop() ); }

    void SetShowSinglePresenter();
    void SetDisableShiftUpForPresenter() { m_DisableShiftUpForPresenter = true; }
    virtual Evas_Coord DeleteItemDirectly ( void* item ){ return 0;};
    virtual Evas_Coord GetWidgetHeight ( void* item ){ return 0;};
    Evas_Coord DeleteAssociatedWithItemRect(Evas_Object * toCompare);
    inline void RequestToRebuildAllItems() { m_RequestedToRebuildAllItems = true; }
    inline bool IsRequestedToRebuildAllItems() const { return m_RequestedToRebuildAllItems; };
    void SetDataAreaHint(bool expand);

    virtual bool FindDuplicateItem(void * item){ return false; }

    void UnpackAssociatedWithItemRect(Evas_Object * toCompare);

    virtual void HideHeaderWidget() {};
    virtual void ShowHeaderWidget() {};
    void CreateTopItemsAfterPTR();

private:

#ifdef MONITOR_SCROLL_CORRUPTION
    static Eina_Bool state_timer_cb( void *data );
    Ecore_Timer *m_StateTimer;
#endif

    static bool IsAlive(TrackItemsProxy *pointer) { return eina_list_search_unsorted_list(mAliveTrackItemsProxies, track_items_proxy_comparator, pointer); }
    static int track_items_proxy_comparator(const void *data1, const void *data2);

    static void fetch_edge_data(void *data );
    static void request_data_ASC(void *data );
    static void request_data_DSC(void *data );
    static Evas_Object *add_rectangle_object( Evas_Coord w, Evas_Coord h, const Evas_Common_Interface *box );
    static void off_stage_items_scroll_cb( void *data, Evas_Object *obj, void *event_info );

    static void mouse_down_cb(void *data, Evas *e, Evas_Object *obj, void *event_info);
    static void mouse_up_cb(void *data, Evas *e, Evas_Object *obj, void *event_info);

    static void animation_started_cb( void *data, Evas_Object *obj, void *event_info );
    static void animation_stopped_cb( void *data, Evas_Object *obj, void *event_info );

    static Eina_Bool bring_in_region_cb( void *data );
    static Eina_Bool periodical_rectangle_redraw_cb( void *data );
    static Eina_Bool user_idle_state_cb( void *data );
    static Eina_Bool delayed_registration_of_edged_cb( void *data );
    static void scroller_page_changed( void *data, Evas_Object *obj, void *event_info );

    /**
     * @retval Return a number: how many operations were removed.
     */
    unsigned int ClearOperations();
    void UpdateMuteTimer();
    void AppendMuteItems();

    void ShowOffStagedItems();

    void PendingErase();

    void ShowProgressBar( bool isToEnd );

    int GetBringPage() const;

    void CleanRequest();

    bool IsServiceItem( const char *itemType );

    Evas_Object *GetFirstRectangle(bool isFromTop);
    bool IsItemOfType(Evas_Object *item, const char *type);

    void ValidateScrollerAndPreloadItems();

    bool IsScrollerRegionOnTop();
    inline bool NoOffstageItemsAbove() const { return (m_ItemsCache->m_ItemsWindow.upperIndex <= 0); };

    void AutomaticallyShowJustAddedItems();
    bool DataIsFiltered();
    void DeleteTopEdgeCB();

    typedef struct LastUploadHistory_ {
        bool m_IsDescendingOrder;
        int m_DataCount;
        int m_UploadResult;
    } LastUploadHistory;

    ProxySettings *m_ProxySettings;
    Evas_Object* m_Scroller;
    bool m_ProgressBarAboveHeader;
    Evas_Object* m_HeaderArea;
    Evas_Object* m_DataArea;
    AbstractDataProvider *m_Provider;
    Ecore_Job* m_RequestJob;
    Ecore_Job* m_RedrawJob;
    LastUploadHistory m_UploadHistory;
    MouseMovement m_MouseMovement;
    bool m_IsSwipeUp_FromTop;
    bool m_IsMuteTimerEnabled;
    bool m_IsMuteRequested;
    bool m_IsNeedToErase;
    bool m_IsRectangleCb;
    bool m_IsPeriodicalRedraw;
    bool m_IsUpdateCacheEnabled;
    long long m_LastRectangleProcessingTime;
    int m_HeaderSize;
    Ecore_Timer *m_MutedDataRequestTimer;
    Ecore_Timer *m_ManualIdleTimer;
    Ecore_Timer *m_PeriodicalTimer;
    Ecore_Timer *m_BringInRegTimer;
    //
    const char *m_Identifier;

    Evas_Object* m_ProgressBar;
    Evas_Object* m_ProgressBarLayout;
    Evas_Object* m_ConnectionErrorLayout;
    bool m_ShowServiceWidgetOnBottom;
    ItemsComparator m_OperationsComparator;

    char *m_ProgressBarLayoutName;
    char *m_ConnectionErrorLayoutName;

    GraphRequest* m_DataRequest;

    Eina_Lock m_Mutex;

    static const char *EDGE_TOP;
    static const char *EDGE_BOTTOM;

    static const char *SCROLL;
    static const char *SCROLL_ANIM_START;
    static const char *SCROLL_ANIM_STOP;

    static const unsigned int PIXELS_HEIGHT_THRESHOLD;

    static const unsigned int DELETE_RECTANGLES_REFRESH_OFFSET;

    static const double HIDE_EARLY_PROGRESS_BAR_TIMEOUT;
    static const double HIDE_REALLY_PROGRESS_BAR_TIMEOUT;
    static const unsigned int AUTO_FETCH_TOP_EDGE_ITEMS_TIMEOUT;

    static const unsigned int DEFAULT_CACHE_WINDOW_SIZE;
    static const unsigned int DEFAULT_CACHE_WINDOW_STEP;

    static const unsigned int INITIALIZATION_CACHE_WINDOW_SIZE;
    static const unsigned int MAX_DATA_PROVIDER_ITEMS_REST;

    static const double REAL_IDLE_STATE_TIMEOUT;
    static const double DELAYED_RECT_REDRAW_TIMEOUT;
    static const unsigned long long TIME_FROM_LAST_USER_ACTIVITY_LIMIT;

    Ecore_Timer *mProgressBarTimer;
    Ecore_Timer *m_DelayedRegEdgesCbTimer;

    bool m_EnableToShowConnectionError;
    bool m_EnableBigConnectionErrorWidget;
    bool m_ShowSingleItem;
    ItemsComparator m_UpdateItemsWndComp;

    bool mIsBeginEdgeCbAdded;
    bool mIsEndEdgeCbAdded;

    bool m_DisableMouseUpDown;
    bool m_ProgressBarApplied;

    bool m_IsInScrollAnimation;

    bool m_AllowAutoAddNewItems;
    bool m_AllowNewStoriesBtn;

    Ecore_Timer *mAsyncResizeTimer = nullptr;

    bool m_DisableShiftUpForPresenter;
    bool m_RequestedToRebuildAllItems;
    bool m_NewItemsWereAddedButNotShown;
    int m_TopOffset;
    bool m_CheckForDuplicatesShiftUp;
    bool m_CheckForDuplicatesShiftDown;
    unsigned int m_AddedNewItems;

    clock_t mTimeStamp;
    static Eina_List *mAliveTrackItemsProxies;

protected:
    bool m_PostponedTopItemsDelete;
    bool m_PostponedBottomItemsDelete;
};

#endif /* TRACKITEMSPROXY_H_ */
