#ifndef SCREENS_EDITPHOTOTAGSCREEN_H_
#define SCREENS_EDITPHOTOTAGSCREEN_H_

#include "Photo.h"
#include "PhotoTagging.h"
#include "ScreenBase.h"

class EditPhotoTagScreen: public ScreenBase, PTInterface {
    Evas_Object * mHeader;
    Evas_Object * mGestureLayer;
    Evas_Object * mPhotoEvasObject;
    Evas_Object *mClipper;
    Evas_Object *mTapToTagLabel;
    Photo *mPhoto;
    ImageRegion mImageRegion;
    ScreenSize mScreenSize;
    Eina_List *mTagList;
    Evas_Object *mMouseDownRect;

    static Evas_Event_Flags zoom_move(void *data, void *event_info);
    static Evas_Event_Flags zoom_start(void *data, void *event_info);
    static Evas_Event_Flags zoom_abort(void *data, void *event_info);
    static Evas_Event_Flags zoom_end(void *data, void *event_info);
    static Evas_Event_Flags momentum_move(void *data, void *event_info);
    static Evas_Event_Flags momentum_start(void *data, void *event_info);
    static Evas_Event_Flags momentum_abort(void *data, void *event_info);
    static Evas_Event_Flags momentum_end(void *data, void *event_info);
    static Evas_Event_Flags tap_end(void *data, void *event_info);
    static void on_tag_area_clicked(void *data, Evas *e, Evas_Object *obj, void *event_info);
    static void on_image_resize_cb(void *data, Evas *e, Evas_Object *obj, void *event_info);
    static void MoveImage(void * data, int offsetX, int offsetY);
    static void ZoomImage(void * data, int xTouch, int yTouch, double zoom);
    static void CalculateImageSize(void * data, bool fitToBox = false);
    static void menu_dismiss_cb(void *data, Evas *evas, Evas_Object *o, void *einfo);

    void ClosePopup(void *data);
    void RegisterMomentumListeners();
    void UnRegisterMomentumListeners();
    void RegisterZoomListeners();
    void UnRegisterZoomListeners();
    void RegisterTapsListeners();
    void UnRegisterTapsListeners();

    bool RemoveTags();
    void UpdateTagsPosition();
    void ApplyChanges();
    void ConfirmCallback(void *data, Evas_Object *obj, const char *emission, const char *source) override;
    bool HandleBackButton() override;
    void Push() override;

public:
    EditPhotoTagScreen(Photo *item);
    virtual ~EditPhotoTagScreen();
    void CreateTags();
    bool FitToScreen() override;
    void FitToUserScreen() override;
    void RemoveTag(void * data) override;
    void EnableDismissCallback(bool enable) override;
};

#endif /* SCREENS_EDITPHOTOTAGSCREEN_H_ */
