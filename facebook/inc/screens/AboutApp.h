/*
 *  AboutApp.h
 *
 *  Created on: 10th Oct 2015
 *      Author: Manjunath Raja
 */

#ifndef ABOUTAPP_H_
#define ABOUTAPP_H_

#include "ScreenBase.h"

class AboutApp: public ScreenBase {
    private:
        void CreateView();
        Evas_Object* NewLabel();

        static void header_back_cb(void* Data, Evas_Object* Obj, void* EventInfo);
        static void search_cb(void* Data, Evas_Object* Obj, void* EventInfo);
        static void terms_cb(void* Data, Evas_Object* Obj, void* EventInfo);
        static void data_policy_cb(void* Data, Evas_Object* Obj, void* EventInfo);

    public:
        AboutApp(ScreenBase *parentScreen);
        virtual ~AboutApp();
        virtual void Push();
};

#endif //ABOUTAPP_H_
