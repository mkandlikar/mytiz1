#ifndef FINDFRIENDSSCREEN_H_
#define FINDFRIENDSSCREEN_H_

#include "FriendsSearchTab.h"
#include "ScreenBase.h"

enum FindFriendsScreenTabs {
    FRIENDS_SEARCH_TAB,
    FRIENDS_REQUEST_TAB,
    FRIENDS_FRIENDS_TAB
};

class FriendsRequestTab;
class FriendsTab;

class FindFriendsScreen: public ScreenBase, public Subscriber {

public:
    FindFriendsScreen(bool is_ff);
    virtual ~FindFriendsScreen();
    void RefreshBadgeCount(int badgeCount);

private:
    void Push() override;
    bool HandleBackButton() override;
    void OnResume() override;

    static void resize_cb(void *data, Evas *e, Evas_Object *obj, void *event_info);
    /* callbacks to follow scroller */
    static void anim_stop_scroll_cb(void *data, Evas_Object *obj, void *event_info);
    static void on_tabbar_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    /* getting on a previous screen callback*/
    static void on_back_button_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void show_new_page_cb(void *data);

    /* Whole view */
    Evas_Object *CreateView(Evas_Object *parent);

    Evas_Object *CreateScroller(Evas_Object *parent);
    Evas_Object *CreateTabbar(Evas_Object *parent);
    void SwitchTab(FindFriendsScreenTabs tab);

    void SetSearchEntryFocus();
    void RemoveSearchEntryFocus();

    /* we need a badge only on requests tab (yet) */
    Evas_Object *CreateTabbarItem(Evas_Object *parent, const char *text, bool isSelected, FindFriendsScreenTabs tab);

    void Update(AppEventId eventId, void *data) override;

    FriendsRequestTab *mFriendsRequest;
    FriendsTab *mFriends;

    Evas_Object *mTabbar;

    Evas_Object *mSearchTab;
    Evas_Object *mRequestTab;
    Evas_Object *mFriendsTab;

    FindFriendsScreenTabs mActiveTab;

    FriendsSearchTab* mSearchFriendsTab;

    Evas_Object *mScroller;

    bool mIsFF;
    Eina_List *mTabs;
};

struct FindFriendsToTab {
    FindFriendsScreen *ffScreen;
    FindFriendsScreenTabs mTab;
    FindFriendsToTab();
};
#endif /* FINDFRIENDSSCREEN_H_ */
