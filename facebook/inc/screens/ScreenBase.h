#ifndef SCREENBASE_H_
#define SCREENBASE_H_

#include "Application.h"
#include "Post.h"
#include "UIRes.h"

#include <Elementary.h>
#include <Evas.h>

class ActionAndData;

class ScreenBase {
public:
    enum ScreenIdEnum {
         SID_UNKNOWN
        ,SID_PEOPLE_YOU_MAY_KNOW_CAROUSEL
        ,SID_USER_PHOTOS
        ,SID_USER_PROFILE
        ,SID_USER_PROFILE_ABOUT
        ,SID_COMMENTS //5
        ,SID_REPLY
        ,SID_SEARCH
        ,SID_WEB_VIEW
        ,SID_WHO_LIKED
        ,SID_POST //10
        ,SID_INPROGRESS
        ,SID_MAIN_SCREEN
        ,SID_SHARE_WITH
        ,SID_CHOOSE_FRIEND
        ,SID_CHOOSE_GROUP //15
        ,SID_CAMERA_ROLL
        ,SID_SELECT_FRIENDS
        ,SID_IMAGE_CAROUSEL
        ,SID_VIDEO_PLAYER
        ,SID_ADD_TEXT //20
        ,SID_ADD_TEXT_INPUT
        ,SID_GROUP_PROFILE_PAGE
        ,SID_HOME_SCREEN
        ,SID_NOTIFICATION_SCREEN
        ,SID_FIND_FRIENDS //25
        ,SID_FRIENDS_SEARCH_TAB
        ,SID_EDIT_COMMENT
        ,SID_MAP_VIEW
        ,SID_CHECKIN
        ,SID_EVENT_CREATE_PAGE //30
        ,SID_EVENT_GUEST
        ,SID_EVENT_PROFILE_PAGE
        ,SID_EVENT_MAIN_PAGE
        ,SID_FRIENDS_REQUESTS
        ,SID_FRIENDS_TAB //35
        ,SID_POST_COMPOSER_SCREEN
        ,SID_EVENT_ALL_POSTS_SCREEN
        ,SID_IMAGE_POST
        ,SID_ALLBDAYS_SCREEN
        ,SID_ADD_CROP_SCREEN //40
        ,SID_USER_PROFILE_FRIENDS_TAB
        ,SID_VIEW_EDIT_HISTORY_SCREEN
        ,SID_ALBUM_GALLERY
        ,SID_SELECT_USER_SCREEN
        ,SID_OWN_PROFILE //45
        ,SID_EDIT_CAPTIONS_SCREEN
        ,SID_EXCEPT_FRIENDS
        ,SID_SPECIFY_FRIENDS
    };

    enum ErrorWidgetType {
        eCONNECTION_ERROR,
        eSOMETHING_WRONG_ERROR,
        eNO_DATA_AVAILABLE
    };

protected:
    ScreenIdEnum mScreenId;
    Evas_Object *mLayout;
    Elm_Object_Item *mNfItem;
    Evas_Object *mNestedScreenContainer;
    Eina_Array * mScreenArray;
    char *mTitleLabel;
    Evas_Object *mTitle;
    Evas_Object *mLockScreen;

    // Pointer to header widget and cancel button. Used in cancel_cb and confirm_cb to delete callbacks after invoking.
    Evas_Object * mHeaderWidget;
    Evas_Object * mHeaderCancelBtnWidget;

    // Time since the screen is visible. Used in back button callback to avoid back callbacks while 0.5 second is not passed.
    double mVisibleTime;

    Evas_Object * mProgressBar;
    bool mIsRightButtonEnabled;
    bool mIs1stActionBarActionEnabled;
    bool mIs2ndActionBarActionEnabled;

    Evas_Object * mErrorWidget;
    ErrorWidgetType mErrorWidgetType;

    /**
     * @brief Object for getting ui constants.
     */
    static UIRes *R;
public:
    ScreenBase(ScreenBase *parentScreen);
    virtual ~ScreenBase();
    virtual void Show();
    virtual void Hide();

    virtual void Push();
    virtual void Pop();

    /**
     * Insert current screen before parent in naviframe.
     */
    virtual void InsertBefore(ScreenBase * parentScreen);

    virtual void OnViewResize(Evas_Coord w, Evas_Coord h);
    virtual void AddChildScreen(ScreenBase *screen);
    virtual void RemoveAllScreens();

    /**
     * @brief Override this function to catch pressing of Back Button.
     *
     * @return false - to perform default behavior
     *         true - to do nothing
     */
    virtual bool HandleBackButton() {return false;}

    virtual bool HandleKeyPadHide() {return false;}
    virtual bool HandleKeyPadShow() {return false;}

    /**
     * @brief Override this function in order to Update/Refresh the screen UI/data,
     *        when the screen gets visible/unvisible in the viewport.
     */
    virtual void OnPause();
    virtual void OnResume();

    virtual void DeletePost( ActionAndData *postData ) {}
    virtual void UpdateComment () {}

    static void BackButtonClicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void SearchButtonClicked(void *data, Evas_Object *obj, const char *emission, const char *source);

    virtual void SetNoDataViewVisibility(bool isVisible);

    virtual void LockScreen();
    virtual void UnlockScreen();

    Evas_Object *getMainLayout();
    Evas_Object *getParentMainLayout();
    Evas_Object *getNestedScreenLayout();
    Evas_Object *getNf();
    Evas_Object *MinSet(Evas_Object *obj, Evas_Object *box, Evas_Coord w, Evas_Coord h);

    /**
     * @brief Callback for cancel/back button of screen. It calls virtual CancelCallback function that can be redefined
     */
    static void cancel_cb(void *data, Evas_Object *obj, void *event_info);
    static void on_back_btn_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);

    /**
     * @brief Calls from cancel_cb. By default it calls Pop function
     */
    virtual void CancelCallback(void *data, Evas_Object *obj, void *event_info);

    /**
     * @brief Callback for confirm/done button of screen. It calls virtual ConfirmCallback function that can be redefined
     */
    static void confirm_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void on_actionbar_1st_action_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void on_actionbar_2nd_action_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void remove_main_layout_from_naviframe(void *data, Evas *e, Evas_Object *obj, void *event_info);
    /**
     * @brief Calls from confirm_cb. By default it calls Pop function
     */
    virtual void ConfirmCallback(void *data, Evas_Object *obj, const char *emission, const char *source);
    virtual void FirstActionClicked(){}
    virtual void SecondActionClicked(){}

    inline void SetHeaderWidget(Evas_Object * header) {mIsRightButtonEnabled = false; mHeaderWidget = header;}
    inline void SetHeaderCancelBtnWidget(Evas_Object * cancelBtn) { mHeaderCancelBtnWidget = cancelBtn; }

    inline void SetVisibleTime(double time) { mVisibleTime = time; }
    inline double GetVisibleTime() { return mVisibleTime; }
    Elm_Object_Item *GetNfItem() { return mNfItem; }

    /**
     * @brief Shows the progress bar
     */
    virtual void ProgressBarShow();

    /**
     * @brief Hides the progress bar
     */
    virtual void ProgressBarHide();
    void EnableRightButton(bool enable);

    void Enable1stActionBarAction(bool enable);
    void Enable2ndActionBarAction(bool enable);

    ScreenIdEnum GetScreenId() const { return mScreenId;}
    ScreenBase* GetParent() { return mParent;}

    /**
     * @brief Calls from on_connection_error_retry_clicked_cb if network is connected.
     */
    virtual void OnErrorWidgetRetryClickedCb() { }

    inline void SetLaunchedByAppControl() {mIsLaunchedByAppControl = true;}
    inline bool IsLaunchedByAppControl() const {return mIsLaunchedByAppControl;}

protected:
    void EnableCancelButton(bool enable);
    void ShowErrorWidget(ErrorWidgetType type);
    void HideErrorWidget();

    bool mIsLaunchedByAppControl;

private:
    ScreenBase *mParent;
    static void on_error_widget_retry_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
};

#endif /* SCREENBASE_H_ */
