/* SignUpWelcomeScreen.h
 *
 * Created on: 10th June 2015
 * Author: Jeyaramakrishnan Sundar
 *
 */

#ifndef SIGNUP_SCREEN_H
#define SIGNUP_SCREEN_H

#include "ScreenBase.h"
#include "WebViewScreen.h"

typedef struct _Browser_Window {
    Evas_Object *elm_window;
    Evas_Object *ewk_view;
    Evas_Object *conform;
    Evas_Object *nav_fm;

} Browser_Window;

class SignUpWelcomeScreen:public ScreenBase
{
	public:
		SignUpWelcomeScreen(ScreenBase *parentScreen);
		virtual ~SignUpWelcomeScreen();
		WebViewScreen *webview;

	private:
		void create_next_button(Evas_Object*);
		void create_entry(Evas_Object*);
		static void HeaderBackCb(void* Data, Evas_Object* Obj, void* EventInfo);

	protected:
		static void next_btn_clicked_cb(void *data , Evas_Object *obj, void *event_info);
		static void anchor_clicked_cb(void *data, Evas_Object *obj, void *event_info);
};
#endif
