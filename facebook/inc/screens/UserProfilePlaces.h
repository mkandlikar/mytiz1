/*
 *  UserProfilePlaces.h
 *
 *  Created on: 01 SEP 2015
 *      Author: Jeyaramakrishnan Sundar
 */

#ifndef USERPROFILEPLACES_H_
#define USERPROFILEPLACES_H_

#include "ScreenBase.h"
#include "FriendsAction.h"

class FbResponseAboutPlaces;

class UserProfilePlaces{
    private:
		FbResponseAboutPlaces * placesResponse;
        char *mProfileId;
        Evas_Object *mRequestList;
        Evas_Object *mRequestLayout;
        Evas_Object* mProgressBar;
        GraphRequest *mPlaceRequestData;

        void LoadData();
        void setPlacesData();
        static void places_response_cb(void* Object, char* Respond, int CurlCode);
        static Evas_Object *get_places_content(void *data, Evas_Object *obj, const char *part);
        static void places_more_btn_clicked_cb(void *data , Evas_Object *obj, const char *emission, const char *source);

    public:
        UserProfilePlaces(Evas_Object *main_layout, ScreenBase * parent, const char *profileId);
        Evas_Object *CreateScroller(Evas_Object *parent);
        virtual ~UserProfilePlaces();

};

#endif //USERPROFILEPLACES_H_
