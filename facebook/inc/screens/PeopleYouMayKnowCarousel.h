#ifndef PEOPLEYOUMAYKNOWCAROUSEL_H_
#define PEOPLEYOUMAYKNOWCAROUSEL_H_

#include "ConfirmationDialog.h"
#include "ScreenBase.h"
#include "UsersDetailInfoProvider.h"
#include <vector>

class AbstractDataProvider;

class PeopleYouMayKnowCarousel: public ScreenBase,
                                public Subscriber
{

    class PageScroller{
    public:
        PageScroller(PeopleYouMayKnowCarousel *pymk, Evas_Object *parentLayout);
        ~PageScroller();
        inline Evas_Object* GetScroller() {return mScroller;};
        inline Evas_Object* GetDataArea() {return mDataArea;}
        bool IsFirstPage();
        bool IsLastPage();
        void SetPreviousPage();
        void ResetCurPageForRTL();
        void AddPageChangedListener();
        void RemovePageChangedListener();
        void ShowErrorPage(bool packToEnd = true);
        void HideErrorPage(bool isPackedToEnd = true);
        void ShowProgressBarPage(bool packToEnd = true);
        void HideProgressBarPage(bool isPackedToEnd = true);
        void DeleteProgressBarPulseLayout(bool isPackedToEnd = true);
        Evas_Object* UnpackServiceWidget(bool isPackedToEnd);
        void PackServiceWidget(Evas_Object* serviceWidget,bool packToEnd);
    private:
        void BringInToFirstPage();
        void BringInToLastPage();
        static void scroller_page_changed( void *data, Evas_Object *obj, void *event_info );
        static void mouse_down_cb(void *data, Evas *e, Evas_Object *obj, void *event_info);
        static void mouse_up_cb(void *data, Evas *e, Evas_Object *obj, void *event_info);

        Evas_Object* mScroller = nullptr;
        Evas_Object* mDataArea = nullptr;
        Evas_Object* mPymkConnectionErrorLayout[2] = {nullptr,nullptr};
        Evas_Object* mPymkProgressBarLayout[2] = {nullptr,nullptr};
        Evas_Object* mPymkProgressBar[2] = {nullptr,nullptr};
        PeopleYouMayKnowCarousel * mPymk = nullptr;
        bool mIsHeadData = true;
        bool mIsTailData = true;

    public:
        int mIndex;
        int mPrevIndex;
        int mPrevScrollerPage;
    };

    class Followers : public Subscriber
    {
    public:
        Followers(AbstractDataProvider *provider);
        virtual ~Followers();
        void RequestData();

    private:
        const char * mId;
        const char * mName;
        AbstractDataProvider *m_Provider;

        void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = NULL) override;
        void Redraw();
     };

public:
    PeopleYouMayKnowCarousel(const char *user_id);
    virtual ~PeopleYouMayKnowCarousel();

    void Push() override;
    void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = NULL) override;
    bool HandleBackButton() override;
    inline bool ProviderIsEmpty() {return mCarouselItemsCount == 0;}
    inline bool CarouselIsEmpty() {return mActionsAndDates.size() == 0;}
    inline int GetLastIndex() {return mActionsAndDates.size()-1;}

protected:
    void CreatePymkItem(void *dataForItem, bool addToEnd);
    void UpdatePymkItem(const char *id, FriendRequestStatus friendRequestStatus = eUSER_REQUEST_UNKNOWN);
    void AddItemsToCarousel();

private:
    Evas_Object *mContent = nullptr;
    FriendsAction* mAction = nullptr;
    Evas_Object *item = nullptr;
    void CreateMiniProfile(ActionAndData *action_data);
    void DeleteMiniProfile(ActionAndData *action_data);
    void UpdateScrollerPages();
    void RemoveMiniProfileFromCarousel(void *data);
    ActionAndData* FindPymkItem(const char *id, int &index);
    void SetScrollerPageManually(int pageNumber);

    GraphRequest *mGetPeopleYouMayKnowRequest = nullptr;
    //Followers * m_Followers;
    PageScroller *mPymkScroller = nullptr;
    ConfirmationDialog *mConfirmationDialog = nullptr;
    UsersDetailInfoProvider *mProvider = nullptr;
    std::vector<ActionAndData*> mActionsAndDates;
    int mCarouselItemsCount;

    const char * GetIconName(const int count);
    const char * GetStoryName(const int count);
    const char * GetEventName(const int line_number);

    void RefreshBtns(ActionAndData *actionData);
    void ShowCancelBlockPopup(ActionAndData *action_data, Evas_Object *layout);

    static void on_get_people_you_may_know_completed(void* object, char* respond, int code);

    static void on_cover_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);

    static void on_message_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_ctxmenu_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_ctxmenu_respond_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_block_user_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event);

    static void on_friends_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_about_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void close_popup(void *user_data, Evas_Object *obj, void *event_info);
    static void remove_this_screen(void *data);
};
#endif /* PEOPLEYOUMAYKNOWCAROUSEL_H_ */
