/*
 * ItemsBuilderAssistent.h
 *
 *  Created on: Sep 17, 2015
 *      Author: ruindmsh
 */

#ifndef ITEMSDESTROYER_H_
#define ITEMSDESTROYER_H_

#include "DoubleSidedStack.h"

class ItemsBuilderAssistent
{
public:
    ItemsBuilderAssistent( unsigned int headerSize );
    virtual ~ItemsBuilderAssistent();

    static const unsigned int SWITCH_OFF_HEDER_BUFFERIZATION;

protected:
    void AddToDelete( void *item );
    void* AddToCreate( void *item, bool isAddToEnd = true );
    /**
     * @brief Implement this method for deleting a UI representation of an item. This method will
     *        be called automatically when 'Window Shifting' will be occured.
     *
     * @param 'item' Data for item to delete. Cast this pointer to a screen specific type.
     */
    virtual void DeleteItem( void* item ) = 0;
    virtual void DeleteAll_Prepare() { };
    virtual void DeleteAll_ItemDelete(void* item ){ DeleteItem(item);};
    /**
     * @brief Implement this method for creating a UI representation of an item. This method
     *        will be called automatically when 'Window Shifting' will be occured.
     *
     * @param 'dataForItem' Data for item, which must be created and added to UI.
     *
     * @param 'isAddToEnd'  If this value is equal to 'true', the new item will be added to
     *                      the and of a parent box (in the normal case to the Lower edge),
     *                      otherwise (in the case of 'false') it will be added to the begin
     *                      (in the normal case to the Upper edge).
     *
     * @retval A reference to item specific data, which was created. Use this retval to delete
     *         an item by passing this value to @method DeleteItem.
     */
    virtual void* CreateItem( void* dataForItem, bool isAddToEnd ) = 0;
    virtual void* UpdateItem( void* dataForItem, bool isAddToEnd );
    virtual void* UpdateSingleItem( void* dataForItem, void* prevItem ) { return nullptr; };
    virtual void HideItem( void *item ) = 0;
    virtual void ShowItem( void *item ) = 0;

    virtual bool IsItemsEqual( void *itemToCompare_data, void *itemForCompare_ui ) = 0;

    void ShowHeader( Utils::DoubleSidedStack *parentItemsStackToCopy );
    unsigned int GetHeaderSize() const;
    void EraseHeader();
    void DeleteLast();
    void PushToBegin( void *item );
private:
    Utils::DoubleSidedStack *m_HeaderItems;
    unsigned int m_HeaderSize;
};

#endif /* ITEMSDESTROYER_H_ */
