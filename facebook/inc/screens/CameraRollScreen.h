#ifndef CAMERAROLLSCREEN_H_
#define CAMERAROLLSCREEN_H_

#include <media_content.h>

#include "AppEvents.h"
#include "IUpdateMediaData.h"
#include "Group.h"
#include "PostComposerMediaData.h"
#include "ScreenBase.h"
#include "Utils.h"

enum CameraRollMode {
    eCAMERA_ROLL_POST_COMPOSER_MODE,
    eCAMERA_ROLL_POST_COMPOSER_MODE_FRIEND_TIMELINE,
    eCAMERA_ROLL_COMMENT_MODE,
    eCAMERA_ROLL_POST_FROM_GROUP_MODE,
    eCAMERA_ROLL_POST_TO_FRIEND_TIMELINE_MODE,
    eCAMERA_ROLL_ADD_PHOTO_TO_ALBUM,
    eCAMERA_ROLL_ADD_PHOTO_FROM_USER_PHOTO_SCREEN,
    eCAMERA_ROLL_CREATE_PHOTO_POST_MODE
};

class CameraRollScreen: public ScreenBase, public Subscriber {
private:
    static const unsigned int MIN_VIDEO_HEIGHT;
    CameraRollMode mMode;
    Eina_List * mMediaList;
    Eina_List *mCapturedMediaList;
    IUpdateMediaData * mIUpdate;
    Evas_Object * mHeader;

    bool mIsLongPressed;
    Album *mAlbum;

    Group *mUserGroup;
    char *mToUserId;
    char *mToUserName;
    Evas_Object *mGengrid;
    Elm_Gengrid_Item_Class *mGIC;

    /**
     * Invokes on each found media
     */
    static bool gallery_media_item_cb(media_info_h media, void *user_data);
    static Evas_Object* gengrid_content_get_cb(void *data, Evas_Object *obj, const char *part);
    static void gengrid_content_selected_cb(void *data, Evas_Object *obj, void *event_info);
    static void gengrid_content_unselected_cb(void *data, Evas_Object *obj, void *event_info);
    static void gengrid_content_longpressed_cb(void *data, Evas_Object *obj, void *event_info);
    static void gengrid_content_realized_cb(void *data, Evas_Object *obj, void *event_info);
    static void gengrid_refresh_selected_indexes_from(Evas_Object *gengrid, int index);
    static void on_make_new_photo_button_clicked_cb(void *data, Evas_Object *obj, void *event_info);
    static void on_make_new_video_button_clicked_cb(void *data, Evas_Object *obj, void *event_info);
    static void app_control_reply_cb(app_control_h request, app_control_h reply, app_control_result_e result,
            void *user_data);
    static bool app_control_reply_data_cb(app_control_h app_control, const char *key, void *user_data);
    static void media_content_update_cb (
                                        media_content_error_e error,
                                        int pid,
                                        media_content_db_update_item_type_e update_item,
                                        media_content_db_update_type_e update_type,
                                        media_content_type_e media_type,
                                        char *uuid,
                                        char *path,
                                        char *mime_type,
                                        void *user_data);

    void Init();
    void CreateHeader();
    void UpdateHeaderButtons();
    void CreateGengrid();
    bool IsMultiSelectMode(CameraRollMode mode);

    void ClearMediaDataList(Eina_List *mediaDataList);
    void PopulateMediaList();
    void FillGengrid();

    virtual bool HandleBackButton();

    void HandleLongPressedState(Elm_Object_Item *gengridItem);
    void SetLongPressedState(bool enable);

    /**
     * @brief Reinitialize Cancel Callback to Confirm when we canceling.
     */
    virtual void CancelCallback(void *data = nullptr, Evas_Object *obj = nullptr, void *event_info = nullptr);

    /**
     * @brief Reinitialize Confirm Callback to Confirm changes.
     */
    virtual void ConfirmCallback(void *data = nullptr, Evas_Object *obj = nullptr, const char *emission = nullptr, const char *source = nullptr);

public:
    CameraRollScreen(CameraRollMode mode, IUpdateMediaData * iUpdate = NULL, Album *album = NULL);
    CameraRollScreen(CameraRollMode mode, Group *userGroup);
    CameraRollScreen(CameraRollMode mode, const char* userId, const char* userName);

    virtual ~CameraRollScreen();
    virtual void Push();
    virtual void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = NULL);
    virtual void OnResume();
};

#endif /* CAMERAROLLSCREEN_H_ */
