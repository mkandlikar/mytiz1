/*
 *  LoginScreen.h
 *
 *  Created on: May 7, 2015
 *      Author: ikhan03
 */

#ifndef LOGINSCREEN_H_
#define LOGINSCREEN_H_

#include "ScreenBase.h"

class Authentication2FScreen;
class LoginRequest;
class SignUpInProgressScreen;
class InitializationDataUploader;

class LoginScreen: public ScreenBase, public Subscriber {
    private:
        SignUpInProgressScreen* mScreenInProgress;
        InitializationDataUploader *mInitDataUploader;
        LoginRequest* mLoginRequest;
        Evas_Object *mEvasUidInput;
        Evas_Object *mEvasPswdInput;
        ConfirmationDialog *mConfirmationDialog;

        const char *mFirstFactor;
        const char *mPswdAcc;
        const char *mUserName;

    private:
        void LoadEdjLayout();
        void CreateView();
        Evas_Object* CreateInput(Elm_Input_Panel_Layout  EntryType, const char *PlaceHolderTxt);
        void DisplayCurlErrorMsg(int CurlCode);
        static void NeedHelpPopupCb(void *Data, Evas_Object *Obj, void *EventInfo);
        static void on_screen_keyb_on_cb(void *Data, Evas_Object *Obj, void *EventInfo);
        static void on_screen_keyb_off_cb(void *Data, Evas_Object *Obj, void *EventInfo);

        void UpdateLoginButton();
        void EnableLoginButton(bool enable);
        void ShowAccountDisabledPopup();
        bool DeleteConfirmationDialog();
        void ShowAlertPopup(const char* message, const char* title = NULL);
        static void confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event);

        void ParseFirstFactorFromResponse(const char *errorData);

    public:
        LoginScreen(ScreenBase *parentScreen);
        virtual ~LoginScreen();
        virtual void Push();
        virtual bool HandleBackButton();
        virtual void Update(AppEventId EventId, void* Data);

        void StartDownloadInitData();

        inline void set_login_field(const char *loginData) {
            elm_entry_entry_set(mEvasUidInput, loginData);
            elm_object_focus_set(mEvasPswdInput, EINA_TRUE);
        }

    protected:
        inline static void PopUpBtnCb(void *data, Evas_Object *obj, void *EventInfo) {
            evas_object_del(static_cast<Evas_Object *>(data));
        }
        static void LoginBtnCb(void *data, Evas_Object *obj, void *EventInfo);
        static void LoginBtnCb(void *data, Evas_Object *obj, const char *emission, const char *source);
        static void SignUpBtnCb(void *data, Evas_Object *obj, const char *emission, const char *source);
        static void NeedHelpBtnCb(void *data, Evas_Object *obj, const char *emission, const char *source);
        static void OnLoginCompleted(void *object, char *respond, int code);
        static void entry_activated_cb(void* Data, Evas_Object* Obj, void* EventInfo);
        static void entry_changed_cb(void* Data, Evas_Object* Obj, void* EventInfo);
        static void provider_sequence_init_done_cb();
};

#endif /* LOGINSCREEN_H_ */
