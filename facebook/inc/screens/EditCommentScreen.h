#ifndef EDITCOMMENTSCREEN_H_
#define EDITCOMMENTSCREEN_H_

#include "ActionBase.h"
#include "ScreenBase.h"
#include "Comment.h"
#include "CommentScreen.h"
#include "Operation.h"

class FriendTagging;

class EditCommentScreen: public ScreenBase, public Subscriber
{
public:
    EditCommentScreen(ActionAndData *data, const Evas_Object *dataArea, ActionAndData* selectedComment = nullptr);
    virtual ~EditCommentScreen();
    void Push() override;
    /**
     * @brief Implement this function. It's calling when event comes.
     */
    void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = NULL) override;

    bool HandleKeyPadHide() override;

private:
    const char *CommentTextGet();
    void CreateEntry(void *data);

    static void back_btn_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void entry_text_changed(void *data, Evas_Object *obj, void *event_info);
    static void update_cmnt_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    void ConfirmCallback (void *data, Evas_Object *obj, const char *emission, const char *source) override;
    bundle* CreateCommentBundle();
    void ReplaceEditedCommentWithPresenter(Sptr<Operation> operation);

    Comment *mComment;
    const Evas_Object *mDataArea;

    Evas_Object *mEditEntry;
    ActionBase *mCommentAction;
    ActionAndData * mInActionAndData;
    Sptr<Operation> mOperation;

    FriendTagging *mTagging;
    ActionAndData* mRootCommentForReply;

    const char *mOriginalMessage;
};
#endif /* EDITCOMMENTSCREEN_H_ */
