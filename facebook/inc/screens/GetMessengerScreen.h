#ifndef GETMESSENGERSCREEN_H_
#define GETMESSENGERSCREEN_H_

#include "ScreenBase.h"

class GetMessengerScreen: public ScreenBase
{
public:
    GetMessengerScreen();
    virtual ~GetMessengerScreen(){};
    virtual void Push();
    static void BackButtonClicked(void *data, Evas_Object *obj, void *event_info);
    static void GetMessengerClicked(void *data, Evas_Object *obj, void *event_info);
};

#endif /* GETMESSENGERSCREEN_H_ */
