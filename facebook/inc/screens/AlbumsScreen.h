#ifndef ALBUMSSCREEN_H_
#define ALBUMSSCREEN_H_

#include "ScreenBase.h"
#include "TrackItemsProxyAdapter.h"

class ActionBase;
class ActionAndData;
class Album;

class AlbumsScreenItem : public BaseObject {
public:
    AlbumsScreenItem();
    ~AlbumsScreenItem();
    void AppendActionAndData(ActionAndData *actionAndData);
    Eina_List *GetActionAndDates() const;
    ActionAndData *GetActionAndDataById(const char *id) const;
    Evas_Object *GetLayout() const;
    void SetLayout(Evas_Object *layout);
    void Update();

private:
    Eina_List *mActionAndDates;
    Evas_Object *mLayout;
};

class AlbumsScreen: public ScreenBase,
                    public TrackItemsProxy,
                    public IDataServiceProtocolClient {
private:
    class AlbumsEventsSubscriber : public Subscriber {
        friend class AlbumsScreen;
    private:
        AlbumsEventsSubscriber(AlbumsScreen *screen);
        virtual ~AlbumsEventsSubscriber();
        virtual void Update(AppEventId eventId, void * data);

        AlbumsScreen *mScreen;
    };

public:
    void AlbumRenamed(ActionAndData *actionAndData);
    void AlbumDeleted();
    void AlbumCreated();
    AlbumsScreen(ScreenBase *parentScreen, const char* userProfileId, bool isOpenFrmUplodProfPhoto, void *calBakObj4UsrProfilScr);
    virtual ~AlbumsScreen();
    static void ArrangeItems(Eina_List *photos, Eina_Array *items, const char* userId, ActionBase* action);
    static void on_album_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    static Album *GetAlbumById(const char*id);

protected:
    const char *SelectBestLayoutForItem(AlbumsScreenItem *item);

    bool mIsOpenFrmUplodProfPhoto;
    void *mCalBakObj4UsrProfilScr;

private:
//from ItemsBuilderAssistent
    virtual void* CreateItem( void* dataForItem, bool isAddToEnd );
    virtual void DeleteItem( void* item );
    virtual void HideItem( void *item );
    virtual void ShowItem( void *item );
    virtual void DeleteAll_Prepare();
    virtual void DeleteAll_ItemDelete(void* item );
    virtual bool IsItemsEqual( void *itemToCompare_data, void *itemForCompare_ui ) {
        return false;
    }
//from TrackItemProxy
    virtual ItemSize* GetItemSize( void* item );
    virtual void* GetDataForItem(void *item);
    virtual ItemSize* SaveSize(void *item);
    virtual ItemSize* GetSize(void *item);
    virtual bool FindItemId(void *item, const char *id);

    virtual void HandleDownloadImageResponse(ErrorCode error, MessageId requestId, const char* url, const char* fileName);

    void ReloadScreen();

    void UpdateItems();

    static bool go_item_comparator(void *itemToCompare, void *itemForCompare);

private:
    ActionBase *mAction;
    AlbumsEventsSubscriber mSubscriber;
    const char *mUserId;
    Eina_List *mCoverImagesList;
};

typedef struct OnAlbumClickedDataStruct_
{
    ActionAndData *smActionNData;
    bool smIsOpenFrmUplodProfPhoto;
    void *smCalBakObj4UsrProfilScr;
} OnAlbumClickedDataStruct;

typedef struct downloadCoverPhotoStruct_
{
    MessageId requestId;
    bool isDownloadCompleted;
    Evas_Object *album_image;
} downloadCoverPhotoStruct;

#endif /* ALBUMSSCREEN_H_ */
