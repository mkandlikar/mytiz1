#ifndef ACCOUNT_INFORMATION_SCREEN_H
#define ACCOUNT_INFORMATION_SCREEN_H

#include "ScreenBase.h"
#include "LogoutRequest.h"
#include "ConfirmationDialog.h"

class AccountInformationScreen : public ScreenBase
{
    public:
        AccountInformationScreen();
        virtual ~AccountInformationScreen();
        void Push() override;
        bool HandleBackButton() override;

    private:
        Evas_Object * mMoreOptionPopup;
        ConfirmationDialog * mRemoveAccountDialog;

        void CreateAccountScreen(Evas_Object *parent);
        LogoutRequest * mLogOutReq;

        bool CloseMorePopup();
        bool HidePopUp();
        void CloseScreenWindow();

        static void show_popup_cb(void *data, Evas_Object *obj, void *event_info);
        static void back_btn_cb(void *data , Evas_Object *obj, const char *emission, const char *source);
        static void more_btn_cb(void *data , Evas_Object *obj, const char *emission, const char *source);
        static void ctxpopup_dismissed_cb(void *data, Evas_Object *obj, void *event_info);
#ifdef FEATURE_SYNC_FB_CONTACTS
        static void sync_now_cb(void *data, Evas_Object *obj, void *event_info);
        static bool account_contact_sync_cb(account_h account, void* user_data);
#endif
        static bool account_remove_cb(account_h account, void* user_data);
        static void on_logout_completed(void* object, char* respond, int code);
        static void confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event);
};

#endif
