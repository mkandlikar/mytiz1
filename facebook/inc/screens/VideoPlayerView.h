#ifndef VIDEOPLAYERVIEW_H_
#define VIDEOPLAYERVIEW_H_

#include <Evas.h>
#include <Ecore.h>
#include <player.h>

class IVideoPlayerObserver {
public:
    virtual ~IVideoPlayerObserver(){};
    virtual void PlayerReady() = 0;
    virtual void PlayerCompleted() = 0;
    virtual void PlayerInterrupted() = 0;
};


class VideoPlayerView {
public:
    VideoPlayerView(Evas_Object *parent, IVideoPlayerObserver *observer, const char *path);
    ~VideoPlayerView();
    bool IsPlayerReady();
    bool StartPlayer();
    bool StopPlayer();
    bool PausePlayer();
    inline int GetVideoDuration() {return mVideoDuration;}
    inline Evas_Object *GetPlayerLayout() { return mVideoRect; }
    inline int GetVideoWidth() {return mVideoWidth;}
    inline int GetVideoHeight() {return mVideoHeight;}
    inline int GetPosition() {int ms = 0; player_get_play_position(mPlayer, &ms); return ms;}
    inline void SetPosition(int ms) {player_set_play_position(mPlayer, ms, true, NULL, NULL);}

    // if video's in portrait, returns true
    inline bool IsVideoPortrait() { return mVideoSceneModePortrait; }

private:
    void CreatePlayer();
    void DestroyPlayer();

    void MainLayoutAdd(Evas_Object *parent);
    void VideoRectAdd(Evas_Object *parent);
    void OpenMediaRes();
    void PreparePlayer();
    void HandlePrepare();
    player_state_e GetPlayerState();
    void CheckMediaSessionOptions();

//CallBacks:
    static void on_player_async_prepared_cb(void *data);
    static void notify_player_ready( void *data );
    static void notify_player_complete( void *data );
    static void notify_player_interrupted( void *data );
    static void on_player_completed_cb( void *data );
    static void on_player_interrupted_cb( player_interrupted_code_e code, void *data );
    static void on_player_seek_completed_cb( void *data );

private:
    player_h mPlayer;
    Evas_Object *mVideoRect;

    const char *mPath;

    bool mVideoSceneModePortrait{false};
    bool mWithoutAudioV3{false};  //this flag marks videos without an audiotrack - they have to be handled specially on Tizen_3.0
    int mVideoDuration;
    int mVideoWidth, mVideoHeight;

    IVideoPlayerObserver *mObserver;
    Ecore_Job *mNotifyPlayerReadyJob;
    Ecore_Job *mNotifyPlayerCompleteJob;
    Ecore_Job *mNotifyPlayerInterruptedJob;
    int mInterruptedPlayerPosition;
};

#endif // VIDEOPLAYERVIEW_H_

