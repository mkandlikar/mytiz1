#ifndef MESSENGER_H_
#define MESSENGER_H_

#include "ScreenBase.h"

class Messenger: public ScreenBase {
    private:
        bool mIsMessengerInstalled;
        std::string mIconPath;
        GraphRequest* mGraphRequest;

        void CreateView();
        void InstallMessenger();

        static void LearnMoreCb(void* Data, Evas_Object* Obj, void* EventInfo);
        static void GetAppCb(void* Data, Evas_Object* Obj, void* EventInfo);
        static void GraphRequestCb(void* Object, char* Respond, int CurlCode);

    public:
        Messenger(ScreenBase *parentScreen);
        virtual ~Messenger();
        virtual void Push();

        static bool IsMessengerInstalled();
        static void LaunchMessenger();
};

#endif /* Messenger_H_ */
