/*
 *  SignUpBDateScreen.h
 *
 *  Created on: 9th June 2015
 *      Author: Manjunath Raja
 */

#ifndef SIGNUPBDATESCREEN_H_
#define SIGNUPBDATESCREEN_H_

#include "ScreenBase.h"

class SignUpBDateScreen: public ScreenBase {
    private:
        struct tm mTmSelectedBDate;
        Evas_Object *mEvasInfo;
        Evas_Object *mEvasBDate;
        Evas_Object *mEvasSelectBDateBtn;
        Evas_Object *mEvasSelectBDateBtnOverlay;

    private:
        void LoadEdjLayout();
        void CreateView();
        void SetInfo(bool NoText);
        static void HeaderBackCb(void* Data, Evas_Object* Obj, void* EventInfo);
        static void AnchorCb(void* Data, Evas_Object* Obj, void* EventInfo);

    public:
        SignUpBDateScreen(ScreenBase *parentScreen);
        virtual ~SignUpBDateScreen();

    protected:
        static void SelectBDateBtnCb(void *data, Evas_Object *obj, void *EventInfo);
        static void ContinueBtnCb(void *data, Evas_Object *obj, void *EventInfo);
};

#endif /* SIGNUPBDATESCREEN_H_ */
