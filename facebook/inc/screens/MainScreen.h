#ifndef MAINSCREEN_H_
#define MAINSCREEN_H_

#include "ScreenBase.h"
#include "UserProfileDataUploader.h"

enum Panel {
LOGIN,
FRIENDS,
FRIENDS_FROM_DB,
FEEDS_FROM_DB,
USER_PROFILE,
USER_PROFILE_FROM_DB
};

class HomeScreen;
class NotificationsScreen;
class FriendsRequestTab;
class MessagerScreen;
class SettingsScreen;

class MainScreen:public ScreenBase {
public:
    enum Tab {
        eFeedTab,
        eFriendRequestsTab,
        eNotificationsTab,
        eSettingsTab
    };

    MainScreen(ScreenBase *parentScreen);
    virtual ~MainScreen();

    void Push() override;
    void OnViewResize(Evas_Coord w, Evas_Coord h) override;
    bool HandleBackButton() override;
    void SetTabbarBadge(int index, int num);
    int GetTabIndex() const;
    void SetMessengerIcon();
    static ScreenBase * getMainScreen() { return my_instance;};
    void AllowNewsFeedBtnClick(bool allow);

private:
    void OnResume() override;
    Evas_Object *CreateMainView(Evas_Object *parent);

    Evas_Object *CreateDrawerLayout(Evas_Object *parent);
    Evas_Object *CreateBg(Evas_Object *parent);

    Evas_Object *CreatePanel(Evas_Object *parent);
    static void panel_scroll_cb(void *data, Evas_Object *obj, void *event_info);

    void CreateSearchField(Evas_Object *parent);
    Evas_Object *CreateTabbar(Evas_Object *parent);
    Evas_Object *CreateScroller(Evas_Object *parent);

#if 0
    Evas_Object *CreateDrawersBtn(Evas_Object *parent, Evas_Smart_Cb func, void *data);

    void ToggleDrawerPanel(Eina_Bool hidden);

    static void drawer_back_cb(void *data, Evas_Object *obj, void *event_info);
    static void on_drawer_button_cb(void *data, Evas_Object *obj, void *event_info);
#endif

    // Tabbar callback handler
    static void on_tabbar_handler(void *data, Evas_Object *obj, const char  *emission, const char  *source);

    // Tabbar scroller animation stop handler
    static void anim_stop_cb(void *data, Evas_Object *obj, void *event_info);

    // Drawer menu item selection handler
    static void on_login_item_selected(void *data, Evas_Object *obj, void *event_info);
    static void on_friends_item_selected(void *data, Evas_Object *obj, void *event_info);
    static void on_friends_from_db_item_selected(void *data, Evas_Object *obj, void *event_info);
    static void on_feed_from_db_item_selected(void *data, Evas_Object *obj, void *event_info);

    // Search field handlers
    static void searchfield_clicked_cb(void *data, Evas_Object *obj, void *event_info);
    static void scroll_to_top(void *data, Evas_Object *obj, const char *emission, const char *source);

    static void MessengerCb(void* Data, Evas_Object* Obj, void* EventInfo);

    // Store drawer panel
    Evas_Object *mDrawer;
    Evas_Object *mMainLayout;

    // Store entry for search field
    Evas_Object *mSearchField;

    Evas_Object *mScroller;
    Evas_Object *mTabbar;

    Evas_Object *mFirstViewRect;
    Evas_Object *mSecondViewRect;
    Evas_Object *mThirdViewRect;
    Evas_Object *mFourthViewRect;
    Evas_Object *mFifthViewRect;
    FriendsRequestTab *mFriendsScreen;
    HomeScreen *mHomeScreen;
    NotificationsScreen *mNotificationScreen;
    MessagerScreen *mMessagerScreen;
    SettingsScreen *mSettingsScreen;
    Evas_Object* mEvasMessengerIcon;
    static ScreenBase * my_instance;

    UserProfileDataUploader *mOwnProfileDataUploader;

};

#endif /* MAINSCREEN_H_ */
