#ifndef SETTINGSSCREEN_H_
#define SETTINGSSCREEN_H_

#include "ActionBase.h"
#include "Common.h"
#include "ScreenBase.h"

// Forward declarations
class ProfileBasicData;
class FbRespondGetGroups;
class SignUpInProgressScreen;
class LogoutRequest;

struct Field {
    char *mText;
    char *mIconPath;
    BaseObject *mData;

    Field(const char *text, const char *icon, BaseObject *data = NULL);
    ~Field();
};

class GroupDescription: public BaseObject {
    public:
        GroupDescription(const char *id, const char *name);
        virtual ~GroupDescription();
        char *mId;
        char *mName;
};

enum  SettingsScreenItemsEnum
{
    PROFILETEST,            // 0
    Favorites,              // 1
    FindFriends,            // 2
    Events,                 // 3
    Apps,                   // 4
    Chat,                   // 5
    LikePages,              // 6
    Saved,                  // 7
    Friends,                // 8
    NearbyPlaces,           // 9
    Photos,                 //10
    Pokes,                  //11
    Groups,                 //12
    GroupCategory,          //13
    Pages,                  //14
    CreatePage,             //15
    Feeds,                  //16
    MostRecent,             //17
    CloseFriends,           //18
    Family,                 //19
    HelpNSettings,          //20
    AppSettings,            //21
    NewsFeedPreferences,    //22
    AccountSettings,        //23
    HelpCenter,             //24
    PrivacyShortcuts,       //25
    TermsNPolicies,         //26
    ReportAProblem,         //27
    About,                  //28
    MobileData,             //29
    LogOut                  //30
};

class SettingsScreen: public ScreenBase,
                      public Subscriber
{

public:
    SettingsScreen(ScreenBase *parentScreen);
    virtual ~SettingsScreen();
    static bool clearPopup();
    virtual void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = NULL);

    static void cb_get_groups_list();
    static void on_logout_completed(void* object, char* respond, int code);

protected:
    static void gl_FindFriendsScreenSel(void *data, Evas_Object *obj, void *event_info);
    static void pokes_cb(void* Data, Evas_Object* Object, void* EventInfo);
    static void gl_PhotosSel(void *data, Evas_Object *obj, void *event_info);
    static void gl_EventScreenSel(void *data, Evas_Object *obj, void *event_info);
    static void gl_SavedFolderSel(void *data, Evas_Object *obj, void *event_info);
    static void gl_sel(void *data, Evas_Object *obj, void *event_info);
    static char *gl_text_get(void *data, Evas_Object *obj, const char *part);
    static Evas_Object *gl_categories_data_get(void *data, Evas_Object *obj, const char *part);
    static Evas_Object *gl_content_get(void *data, Evas_Object *obj, const char *part);
    static Evas_Object *gl_groups_data_get(void *data, Evas_Object *obj, const char *part);
    static Evas_Object *gl_see_all_data_get(void *data, Evas_Object *obj, const char *part);
    // Loggedin user Item callbacks
    static Evas_Object *gl_user_data_get(void *user_data, Evas_Object *obj, const char *part);
    static void gl_user_sel(void *data, Evas_Object *obj, void *event_info);
    static void gl_GroupScreenSel(void *data, Evas_Object *obj, void *event_info);
    static void gl_SeeAllGroupsSel(void *data, Evas_Object *obj, void *event_info);
    static void gl_SuggestedGroupsSel(void *data, Evas_Object *obj, void *event_info);

    static Evas_Object CreateTabbar(Evas_Object *parent);
    static Evas_Object *gl_titles_data_get(void *data, Evas_Object *obj, const char *part);
    static char *gl_group_index_text_get(void *data, Evas_Object *obj, const char *part);
    static char *title_text_get(void *data, Evas_Object *obj, const char *part, int *i);
    static char *GetSettingsScreenIcon(void *data);
    static void account_settings_sel_cb(void* data, Evas_Object* obj, void* event_info);
    static void help_center_sel_cb(void* data, Evas_Object* obj, void* event_info);
    static void privacy_shortcuts_sel_cb(void* data, Evas_Object* obj, void* event_info);
    static void terms_n_policies_sel_cb(void* data, Evas_Object* obj, void* event_info);
    static void about_sel_cb(void* data, Evas_Object* obj, void* event_info);
    static void log_out_sel_cb(void *data, Evas_Object *obj, void *event_info);
    static void Reload_Profile_Data_cb(void *data, void *thisScreen);
    static void pop_up_hide_cb(void* data, Evas_Object* obj, void* event_info);
    static void pop_up_cancel_btn_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void pop_up_log_out_btn_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void nearBy_places_sel_cb(void *data, Evas_Object *obj, void *EventInfo);

private:
    static Field mArray[];
    GraphRequest *mGroupRequest;
    Elm_Object_Item *mAfterItem;
    Elm_Object_Item *mBeforeItem;
    Elm_Object_Item *mGroupItem;
    Eina_Array * mGroupItems;
    SignUpInProgressScreen* mScreenInProgress;
    LogoutRequest *mLogOutReq;
    static ActionAndData* mActionAndData;

    static const int mLoggedinUserItemWitdth;
    static const int mLoggedinUserItemHeight;
    static const int mMaxShownGroups;

    Evas_Object *mOwnAvatar;
    Evas_Object *mOwnName;

    void RefreshOwnData();
    void GetGroupsList();
    void ClearGetGroupsRequest();
    void OnGetGroupsRequestComplete(char* respond, int code);
    void SetGroupRequestsData(FbRespondGetGroups * respond);
    void RemoveGroupsList();
    void CreateLogOutPopUp();
    void SetPathAndUploadAvatar(const char *filePath);
    void ClearAvatarPath();

    static void on_get_groups_request_completed(void* object, char* respond, int code);
    static SettingsScreen *settings_screen_instance;
    static void settings_screen_instance_set(SettingsScreen *);
    static SettingsScreen * settings_screen_instance_get();
    static void on_item_delete_cb(void *data, Evas_Object *obj);

    char *mAvatarPath;
};

#endif /* SETTINGSSCREEN_H_ */

