#ifndef HOMESCREEN_H_
#define HOMESCREEN_H_

#include "AppEvents.h"
#include "DataServiceProtocol.h"
#include "FbRespondGetFeed.h"
#include "GraphObjectDownloader.h"
#include "ScreenBase.h"
#include "FeedAction.h"
#include "TrackItemsProxyAdapter.h"

class DataUploader;
class MainScreen;
class OperationPresenter;

class HomeScreen: public ScreenBase,
                  public TrackItemsProxyAdapter,
                  public IGraphObjectDownloaderObserver
{
    class BriefUpdater
    {
        public:
            BriefUpdater( HomeScreen *parent );
            virtual ~BriefUpdater();
            void Start();
        private:
            static void brief_uploading_done_cb( void *parent );
            static Eina_Bool brief_auto_update_cb( void *data );

            DataUploader *m_Uploader;
            HomeScreen *m_Parent;
            Ecore_Timer *m_BriefTimer;
    };

    static const unsigned int DEFAUL_HOME_SCR_ITEMS_WINDOW_SIZE;
    static const unsigned int DEFAUL_HOME_SCR_ITEMS_WINDOW_STEP;
    static const unsigned int REFRESH_TIMER_LIMIT;
    static const double BRIEF_POSTS_AUTO_UPDATE_TIME;
    static const unsigned int ATTEMPTS_TO_UPDATE_LIMIT;
    BriefUpdater *mBriefUpdater;
    FeedAction* mAction;
    Evas_Object *mCustomItem;
    bool mIsNewStoriesBt;
    ActionAndData* mEmptyStateCallbackData;
    Evas_Object* mEmptyStateItem;

    void CheckForEmptyState();
    unsigned int m_AttemptsToUpdateCount;
    Eina_List * mHiddenSharedItems;
    void UpdatePost( ActionAndData* &data, void* postPtr, bool isAddToEnd = false, Evas_Object *prevItem = nullptr );
    void UpdatePresenter( ActionAndData* &data, OperationPresenter *operation, bool isAddToEnd );
    void DeleteUnfollowPost(ActionAndData *postData);

protected:
    void* CreateItem( void* dataForItem, bool isAddToEnd ) override;
    void* UpdateItem( void* dataForItem, bool isAddToEnd ) override;
    void* UpdateSingleItem( void* dataForItem, void* prevItem ) override;
    bool IsPseudoPostVisible() override { return mEmptyStateItem; };
    void ShowUpToDatePopup() override;
    void ResetUpdatesCount() override;
    void FetchEdgeItems() override;
    void DataErasedSignal() override {
        DeleteFindFriendsItem();
    };
    bool NoVisualData();
    void DeleteFindFriendsItem();

    void StartDownloader(const char *id, IGraphObjectDownloaderObserver *observer, ActionAndData *data);
    void DownloaderProgress(IGraphObjectDownloaderObserver::Result result, GraphObjectDownloader *downloader) override;
    void GraphObjectDownloaderProgress(Result result) override {};

public:
    HomeScreen( ScreenBase *parentScreen );
    virtual ~HomeScreen();

    void AllowNewStoriesBtnClick(bool allow);
    void NotifyUpdate();
    static void hide_btns_cb(void *data, Evas_Object *obj, void *event_info);
    static void on_status_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void on_photo_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void on_checkin_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void new_stories_click_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    void ScrollerReachedTop(bool playSound) override;
    void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = NULL) override;
    void NewItemsAvailable() override;
    void ShowHiddenInProgressItems();

    void CheckOffStageUnfollowItems() override;

    bool mIsBtnShown;
    MainScreen * mMainScreen;

    void FollowUser(ActionAndData *actionData);

    static void undo_unfollow_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);

    void CreateUnfollowWidget(ActionAndData *actionData);
    void CancelUnfollowWidget(ActionAndData *actionData);
    Eina_List *mUnfollowActionDataList;
    Eina_List *mObjectDownloaders;
};

#endif /* HOMESCREEN_H_ */

