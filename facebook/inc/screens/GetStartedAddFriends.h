/* GetStartedAddFriends.h
 *
 * Created on: 17th July 2015
 * Author: Jeyaramakrishnan Sundar
 *
 */

#ifndef GET_STARTED_ADD_FRIENDS_H
#define GET_STARTED_ADD_FRIENDS_H

#include "ScreenBase.h"
#include <contacts.h>
#include "vector"
#include "Common.h"
#include "FriendsAction.h"
#include "ScreenBase.h"
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>


using namespace std;

typedef struct
{
    int number_id;
    char number[20];
}local_contacts;

typedef struct
{
    char * uId;
    char * name;
    char * userName;
    int isFriends;
    char * photo;
}struct_addFriends;

typedef std::vector<local_contacts> st_local_contacts;


class GetStartedAddFriends:public ScreenBase
{
    public:
        class UserFriends : public GraphObject
        {
            public:
                UserFriends(struct_addFriends);
                virtual ~UserFriends();

                //members
                const char * mId;
                const char * mName;
                const char * mUserName;
                int mIsFriends;
                const char * mPicSquareWithLogo;
        };
        GetStartedAddFriends();
        virtual ~GetStartedAddFriends();


    public:
        char **phone_number;
        st_local_contacts lc_st;

        Eina_List * mPeople;
        FriendsAction* mAction;

    private:
        Evas_Object *mRequestList;
        Evas_Object *mRequestLayout;
        void get_phone_numbers(contacts_list_h associated_contacts);
        void get_contact_list();
        void xml_parser(char *response);
        void parseContact (xmlDocPtr doc, xmlNodePtr cur);
        void SetAddFriendsData();

        Ecore_Thread * ExecuteAsync();
        static void thread_start_cb(void *data, Ecore_Thread *thread);
        static void thread_end_cb(void *data, Ecore_Thread *thread);
        static void thread_cancel_cb(void *data, Ecore_Thread *thread);
        static void response_cb(void* object, char* respond, int code);
        static void parse_response_xml(char*);

        static Evas_Object *gl_ppl_u_may_know_get(void *data, Evas_Object *obj, const char *part);
        static void on_add_friend_button_clicked_cb(void *data, Evas *e, Evas_Object *obj, const char *emisson, void *source);
        static void on_add_friend_completed(void* object, char* respond, int code);
        static void on_item_delete_cb(void *data, Evas_Object *obj);
        static void on_cancel_friend_button_clicked_cb(void *data, Evas *e, Evas_Object *obj, const char *emisson, void *source);
        static void on_cancel_friend_completed(void* object, char* respond, int code);

    protected:
        Evas_Object *CreateScroller(Evas_Object *parent);
        Evas_Object *mTabbar;
        Evas_Object *mScroller;
        Evas_Object *mNestedBoxContainer;

        Evas_Object *mSearchBox;
        Evas_Object *mReqsBox;
        Evas_Object *mContactsBox;
        Evas_Object *mFriendsBox;
        Evas_Object *CreateAddFriendsPage(Evas_Object *parent);
};

struct FrObjAndData {
    ActionAndData *mEData;
    Evas_Object *mEObject;
    FrObjAndData();
    ~FrObjAndData();
};

#endif
