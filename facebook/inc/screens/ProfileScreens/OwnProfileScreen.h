#ifndef OWNPROFILESCREEN_H_
#define OWNPROFILESCREEN_H_

#include "BaseProfileScreen.h"
#include "IUpdateMediaData.h"

enum UploadPhotoType {
    UPLOAD_COVER,
    UPLOAD_AVATAR,

    UPLOAD_NONE
};

class OwnProfileScreen: public BaseProfileScreen, public IUpdateMediaData
{
public:
    OwnProfileScreen();
    virtual ~OwnProfileScreen();

    void CreateProfileWidget() override;

    void OnPostTextClicked() override;
    void OnPostPhotoClicked() override;

    void OnCoverClicked(Evas_Object *obj) override;
    void OnAvatarClicked(Evas_Object *obj) override;

    bool HandleBackButton() override;
    void UpdateMedia() override;

    UploadPhotoType mTargetUploadType;
private:
    /**
     * Opens CameraRollScreen to choose photo from upload
     */
    static void UploadPhotoClicked(void *user_data, Evas_Object *obj = nullptr, void *event_info = nullptr);
    /**
     * Opens UserPhotosScreen to choose photo from albums
     */
    static void ChooseFromPhotoClicked(void *user_data, Evas_Object *obj = nullptr, void *event_info = nullptr);
    /**
     * Opens web-view when Update Info button clicked
     */
    static void on_update_profile_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    /**
     * Opens popup with more options
     */
    static void on_more_profile_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);

    static void change_cover_from_more_popup_cb(void *data, Evas_Object *obj, void *event_info);
    static void change_avatar_from_more_popup_cb(void *data, Evas_Object *obj, void *event_info);
    static void on_edit_privacy_clicked(void *data, Evas_Object *obj, void *event_info);

    void CreatePhotoPopup( Evas_Object *obj );
    void CreateNoPhotoPopup( Evas_Object *obj );

    Evas_Object *CreateActionBtns(Evas_Object *parent, ActionAndData *actionData) override;
    void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = nullptr) override;
};

#endif /* OWNPROFILESCREEN_H_ */
