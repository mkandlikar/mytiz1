#ifndef USERFRIENDSSCREEN_H_
#define USERFRIENDSSCREEN_H_


#include "ScreenBase.h"
#include "TrackItemsProxyAdapter.h"

class FriendsAction;


class UserFriendsScreen: public ScreenBase, public TrackItemsProxyAdapter
{
public:
    UserFriendsScreen(const char *id, const char *name, bool showOnlyMutualFriends = false);
    virtual ~UserFriendsScreen();

private:
    void Push() override;

    void FetchEdgeItems() override;
    void RebuildAllItems() override;

    void* CreateItem(void* dataForItem, bool isAddToEnd) override;
    void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = nullptr) override;
    void* UpdateItem(void* dataForItem, bool isAddToEnd) override;

    void SetDataAvailable(bool isAvailable) override;
    void SetNoDataViewVisibility(bool isVisible) override;
    bool HandleBackButton() override;

    void CreateSearchEntry();
    void OnSearchBtnClicked();
    void RefreshButton(Evas_Object *list_item, Person::FriendshipStatus status);

    Evas_Object *CreateFriendItem(Evas_Object* parent, ActionAndData *action_data, bool isAddToEnd);

    static void on_friends_button_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_item_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_search_keypad_btn_clicked_cb(void *data, Evas_Object *obj, void *event_info);
    static void on_clear_btn_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void on_changed_cb(void *data, Evas_Object *obj, void *event_info);

    bool mOwnFriendsList;

    FriendsAction* mAction;

    Evas_Object *mSearchLayout;
    Evas_Object *mSearchEntry;

    std::string mId;

    static const unsigned int DEFAULT_ITEMS_WND_SIZE;
};

#endif /* USERFRIENDSSCREEN_H_ */
