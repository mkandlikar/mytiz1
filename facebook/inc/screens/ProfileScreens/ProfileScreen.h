#ifndef PROFILESCREEN_H_
#define PROFILESCREEN_H_

#include "ConfirmationDialog.h"
#include "BaseProfileScreen.h"

class ProfileScreen: public BaseProfileScreen
{
public:
    ProfileScreen(const char *id);
    virtual ~ProfileScreen();

protected:
    void Update(AppEventId eventId, void* data) override;

private:
    bool HandleBackButton() override;

    void OnCoverClicked(Evas_Object *obj) override;
    void OnAvatarClicked(Evas_Object *obj) override;

    void OnPostTextClicked() override;
    void OnPostPhotoClicked() override;

    Evas_Object *CreateActionBtns(Evas_Object *parent, ActionAndData *actionData) override;

    void RefreshActionBtns() override;

    Evas_Object *CreateTwoButtons();
    Evas_Object *CreateThreeButtons();
    Evas_Object *CreateFourButtons();

    static void on_friend_btn_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    void FriendClicked(Evas_Object *obj);
    void RespondClicked(Evas_Object *obj);

    static void on_confirm_friend_request_btn_clicked(void *data, Evas_Object *obj, void *event_info = NULL);
    static void on_delete_friend_request_btn_clicked(void *data, Evas_Object *obj, void *event_info = NULL);
    void ConfirmFriendRequest();
    void DeleteFriendRequest();

    static void on_follow_btn_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void on_popup_follow_btn_clicked(void *user_data, Evas_Object *obj = NULL, void *event_info = NULL);
    void FollowClicked();
    static void on_follow_completed(void* object, char* respond, int code);

    static void on_add_cancel_friend_btn_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    void AddCancelFriendClicked();
    static void on_add_cancel_friend_completed(void* object, char* respond, int code);

    static void on_popup_unfriend_btn_clicked(void *user_data, Evas_Object *obj = NULL, void *event_info = NULL);
    void UnfriendClicked();
    void DoUnfriend();
    void OnUnfriendError();

    static void on_message_btn_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);

    static void on_more_btn_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    void OnMoreClicked(Evas_Object *obj);

    static void on_poke_btn_clicked(void *user_data, Evas_Object *obj, void *event_info = NULL);
    static void on_poke_user_completed(void* object, char* respond, int code);

    void BlockUser();
    static void on_block_btn_clicked(void *user_data, Evas_Object *obj, void *event_info = NULL);
    void ShowCancelBlockPopup();

    static void confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event);

    static void remove_this_screen(void *data);

    static void confirmation_dialog_cb_unfriend(void *user_data, ConfirmationDialog::CDEventType event);
    bool DeleteConfirmationDialog();

    ConfirmationDialog *mConfirmationDialog;

    GraphRequest *mFollowUnfollowRequest;
    GraphRequest *mPokeRequest;
};

#endif /* PROFILESCREEN_H_ */
