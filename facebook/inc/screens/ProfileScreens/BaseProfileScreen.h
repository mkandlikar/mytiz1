#ifndef BASEPROFILESCREEN_H_
#define BASEPROFILESCREEN_H_

#include "FeedAction.h"
#include "FeedProvider.h"
#include "GraphObjectDownloader.h"
#include "ScreenBase.h"
#include "TrackItemsProxyAdapter.h"
#include "UserProfileData.h"
#include "UserProfileDataUploader.h"

class OperationPresenter;

class BaseProfileScreen: public ScreenBase, public TrackItemsProxyAdapter, public IGraphObjectDownloaderObserver
{
public:
    enum NotificationType {
        EAvatarUploadSuccess,
        ECoverUploadSuccess,
        ENetworkError,
        EUploadError,
    };

    BaseProfileScreen(const char *id = NULL);
    void Push() override;
    void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = NULL) override;
    virtual ~BaseProfileScreen();

    /**
     * Starts data uploader process
     */
    void UpdateProfileData();

    /**
     * Function called when all data has been received
     */
    static void on_user_profile_info_received_cb(void *data, int code);

    /**
     * Creates or updates mUserActionData
     */
    void UpdateProfileWidget(UserProfileData *userData);

    /**
     * Create base UI
     */
    virtual void CreateProfileWidget();

    /*
     * Creates cover item
     */
    Evas_Object *CreateCover(Evas_Object *parent, ActionAndData *actionData);
    /**
     * Set data in cover item
     */
    void RefreshCover(Evas_Object *layout, ActionAndData *actionData);

    /**
     * Action when cover item was clicked
     */
    virtual void OnCoverClicked(Evas_Object *obj);
    /**
     * Action when avatar item was clicked
     */
    virtual void OnAvatarClicked(Evas_Object *obj);

    /**
     * Opens ImageCarouselScreen to view cover
     */
    static void ViewCover(void *user_data, Evas_Object *obj = NULL, void *event_info = NULL);
    /**
     * Opens ImageCarouselScreen to view avatar
     */
    static void ViewAvatar(void *user_data, Evas_Object *obj = NULL, void *event_info = NULL);

    /**
     * Create post and photo buttons
     */
    Evas_Object *CreatePostAndPhotoItem(Evas_Object *parent, ActionAndData *actionData);

    /**
     * Action on post button clicked on post & photo widget
     */
    virtual void OnPostTextClicked();
    /**
     * Action on photo button clicked on post & photo widget
     */
    virtual void OnPostPhotoClicked();

    /**
     * Creates profile info item (About, Photos, Friends)
     */
    virtual Evas_Object *CreateProfileInfoItems(Evas_Object *parent, ActionAndData *actionData);
    /**
     * Sets preview for About, Photos and Friends buttons in profile info item
     */
    void RefreshProfileInfo(Evas_Object *profileInfo, ActionAndData *actionData);

    /**
     * Shows notification as toast
     */
    void ShowNotification(NotificationType status);

    Evas_Object *mCover;
    Evas_Object *mPostAndPhotoBtn;
    Evas_Object *mProfileInfo;

    ActionAndData *mUserActionData;
    FeedAction *mAction;

    char *mName;

    /**
     * Uploader to create or update data about user
     */
    UserProfileDataUploader *mProfileDataUploader;

    /**
     * Flag when there is no already uploaded ActionAndData for current user
     */
    bool mProfileWidgetExist;

    inline std::string GetId() { return mId; }

protected:
    std::string mId;

    Evas_Object *mProfileLayout;
    Evas_Object *mFeedWidget;

    void FetchEdgeItems() override;

    virtual Evas_Object* CreateActionBtns(Evas_Object *parent, ActionAndData *actionData){ return nullptr;};
    virtual void RefreshActionBtns(){};

    Evas_Object *mProfileBtns;

    Eina_List *mObjectDownloaders;

private:
    void UpdatePost(ActionAndData* &data, void* postPtr, bool isAddToEnd, Evas_Object* prevItem = nullptr);
    void UpdatePresenter(ActionAndData* &data, OperationPresenter *operation, bool isAddToEnd);

    static void on_profile_photo_clicked(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_profile_post_data_clicked(void *user_data, Evas_Object *obj, const char *emission, const char *source);

    static void on_about_profile_clicked(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_photo_profile_clicked(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_friends_profile_clicked(void *user_data, Evas_Object *obj, const char *emission, const char *source);

    void* UpdateItem(void* dataForItem, bool isAddToEnd) override;
    void* CreateItem(void* dataForItem, bool isAddToEnd) override;
    void* UpdateSingleItem( void* dataForItem, void* prevItem ) override;

    void StartDownloader(const char *id, IGraphObjectDownloaderObserver *observer, ActionAndData *data);
    void DownloaderProgress(IGraphObjectDownloaderObserver::Result result, GraphObjectDownloader *downloader) override;
    void GraphObjectDownloaderProgress(Result result) override {};

    void Init();
    void CreateBaseUI();
    bool CheckDestination(const char * destination, int type);

    void HideHeaderWidget() override;
    void ShowHeaderWidget() override;
    inline bool IsUserProfileWidgetShown();

    std::map<NotificationType, const char*> mNotificationType;

    static const unsigned int REFRESH_TIMER_LIMIT;
    FeedProvider* mFeedProvider;
};

#endif /* BASEPROFILESCREEN_H_ */
