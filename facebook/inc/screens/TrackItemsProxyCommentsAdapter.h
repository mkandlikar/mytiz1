#ifndef TRACKITEMSPROXYCOMMENTSADAPTER_H_
#define TRACKITEMSPROXYCOMMENTSADAPTER_H_

#include "TrackItemsProxyAdapter.h"
#include "TrackItemsProxy.h"

class CommentsItemsCache;

class TrackItemsProxyCommentsAdapter: public TrackItemsProxyAdapter
{
    friend CommentsItemsCache;

public:
    TrackItemsProxyCommentsAdapter( Evas_Object* parentLayout, AbstractDataProvider *provider, unsigned int headerSize = ItemsBuilderAssistent::SWITCH_OFF_HEDER_BUFFERIZATION );
    virtual ~TrackItemsProxyCommentsAdapter();

protected:
    inline CommentsItemsCache* GetItemsCache();

    void AddAnimationCBs() override {};
    void RemoveAnimationCBs() override {};

    void AddDataAreaResizeCB() override;
    void RemoveDataAreaResizeCB();

    void ChangeYCoordinate( int padding, bool isFromTop ) override;

    void AddPresenter() override;
    void RemovePresenter(void *item) override;
    void UpdateCommentsWindowItems();
    void OnScrollerResizeActions() override;

    void ScrollToEnd();
    void CheckOffStageItemsBelow();
    void RestartUnblockShiftsTimer();
    bool IsOffStageItemsBelow();

private:
    static void change_y_coordinate_cb(void *data);
    static void delete_items_from_top_cb(void *data);
    static void data_area_resize_cb(void *data, Evas_Object *obj, void *event_info);
    static Eina_Bool unblock_shifts(void *data);

    bool m_SetNewBottomYCoordinate;
    Evas_Coord m_CurrrenScrollerSize;
    Evas_Coord m_PreviousScrollerSize;

    Ecore_Job* m_EcoreJob_ChangeY;
    Ecore_Job* m_EcoreJob_DelFromTop;

    bool m_RequestToDeleteAllItems;
    bool m_IsOffStageItemsBelow;
    Ecore_Timer *m_UnblockShiftsTimer;
};

class CommentsItemsCache : public ItemsCache
{
    friend TrackItemsProxyCommentsAdapter;

public:
    CommentsItemsCache(TrackItemsProxy* parent, AbstractDataProvider *provider);
    virtual ~CommentsItemsCache(){};

protected:
    inline TrackItemsProxyCommentsAdapter* GetParent();

    bool ShiftDown( bool isPreCreate ) override;
    bool ShiftUp() override;
    void DecrementWindowSize();
    void CheckProviderSizeChanged( bool disableNormalization ) override;


private:
    void DeleteAllItemsFromTop();
    void DeleteFromTop( unsigned int limit );
    void DeleteFromBottom( unsigned int limit );

    void NormalizeCurrentWindowSize( unsigned int itemsToAdd );
    void SetNewIndexesAndWindowSize();
    bool SetNewProviderSize();
    unsigned int m_PrevProviderSize;
    unsigned int m_CurProviderSize;
};
#endif /* TRACKITEMSPROXYCOMMENTSADAPTER_H_ */
