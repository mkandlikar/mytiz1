/*
 *  CommonScreen.h
 *
 *  Created on: June 26, 2015
 *      Author: Manjunath Raja
 */

#ifndef COMMONSCREEN_H_
#define COMMONSCREEN_H_


namespace CommonScreen {
    void LaunchSignIn();
    void StartProgressScreen();
    void StartSigningUp();
    bool IsValidNumber(const char *Number);
}


#endif /* COMMONSCREEN_H_ */
