#ifndef SEARCHSCREENTABBED_H_
#define SEARCHSCREENTABBED_H_

#include "ScreenBase.h"
#include "SearchScreen.h"

enum SearchScreenTabs {
    SEARCH_ALL_TAB = 0,
    SEARCH_PEOPLE_TAB,
    SEARCH_PAGES_TAB,
    SEARCH_GROUP_TAB,
    //SEARCH_APPS_TAB
};

struct SearchToTab;
class FbRespondGetSearchResults;
/*
 *
 */
class SearchScreenTabbed : public ScreenBase
{
public:
    SearchScreenTabbed(const char * searchStr, SearchScreen * previousScreen);
    virtual ~SearchScreenTabbed();
    void Push() override;
    void OnViewResize(Evas_Coord w, Evas_Coord h) override;
private:
    SearchScreen * mPreviousScreen;
    // Store entry for search field
    Evas_Object *mSearchField;
    char *mSearchString;

    Evas_Object *mTabbar;

    Evas_Object *mAllTab;
    Evas_Object *mPeopleTab;
    Evas_Object *mPagesTab;
    Evas_Object *mGroupsTab;

    SearchScreenTabs mActiveTab;

    Evas_Object *mScroller;
    Evas_Object *mNestedBoxContainer;

    Evas_Object *mAllLayout;
    Evas_Object *mPeopleLayout;
    Evas_Object *mPageLayout;
    Evas_Object *mGroupLayout;

    Evas_Object *mAllInnerLayout;
    Evas_Object *mPeopleInnerLayout ;
    Evas_Object *mPageInnerLayout;
    Evas_Object *mGroupInnerLayout;


    GraphRequest *mSearchAllRequest;
    GraphRequest *mSearchPeopleRequest;
    GraphRequest *mSearchPageRequest;
    GraphRequest *mSearchGroupRequest;

    SearchToTab *mSearchToTab[4];

    void RegisterCbs();
    void UnregisterCbs();

    Evas_Object *CreateView(Evas_Object *parent);
    Evas_Object *CreateTabbar(Evas_Object *parent);
    Evas_Object *CreateScroller(Evas_Object *parent);
    void CreateAllPage(Evas_Object *parent);
    void CreatePeoplePage(Evas_Object *parent);
    void CreatePagePage(Evas_Object *parent);
    void CreateGroupPage(Evas_Object *parent);

    void CreateSearchField(Evas_Object *parent);
    Evas_Object *CreateTabbarItem(Evas_Object *parent, const char *text, bool isSelected, SearchScreenTabs id);
    static void searchfield_pressed_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void searchfield_clear_button_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void back_button_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void retry_connect_cb(void *data, Evas_Object *obj, const char *emission, const char *source);

    static void tabbar_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    void SwitchTabbar(SearchScreenTabs tab, bool changeScrollerPage = false);

    static void anim_stop_scroll_cb(void *data, Evas_Object *obj, void *event_info);

    void ShowProgressBar(Evas_Object *layout);
    void StartSearchRequest();
        
    //search callbacks
    static void on_search_all_completed(void* object, char* respond, int code);
    static void on_search_people_completed(void* object, char* respond, int code);
    static void on_search_page_completed(void* object, char* respond, int code);
    static void on_search_group_completed(void* object, char* respond, int code);

    static void on_search_completed(void* object, char* respond, int code, GraphRequest ** request, Evas_Object *layout);

    //list data methods
    void SetSearchResults(FbRespondGetSearchResults * searchRespond, Evas_Object* layout);
    static Evas_Object *gl_list_item_get(void *user_data, Evas_Object *obj, const char *part);
    static void on_item_delete_cb(void *data, Evas_Object *obj);
    static void on_item_clicked_cb(void *data, Evas_Object *obj, const char *emisson, const char *source);
    bool HandleBackButton() override;
};

struct SearchToTab {
    SearchScreenTabbed *stScreen;
    SearchScreenTabs mTab;
    SearchToTab();
    ~SearchToTab();
};
#endif /* SEARCHSCREENTABBED_H_ */
