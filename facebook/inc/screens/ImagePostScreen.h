#ifndef IMAGEPOSTSCREEN_H_
#define IMAGEPOSTSCREEN_H_

#include "ActionBase.h"
#include "AppEvents.h"
#include "ImagePostAction.h"
#include "Photo.h"
#include "PresenterHelpers.h"
#include "ScreenBase.h"

class ImagesCarouselScreen;
class FbResponseImageDetails;
class Photo;

class ImagePostScreen: public ScreenBase, public Subscriber {
    friend class ImagePostAction;
private:
    class ImageWidget : public Subscriber {
        friend class ImagePostScreen;
    private:
        ImageWidget(ImagePostScreen *screen, ActionAndData *actionAndData, int order);
        ~ImageWidget();

        int GetWidgetHeight();
        inline Evas_Object* GetLayout() { return mLayout; }
        static void on_image_widget_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
        void RefreshImageWidget(ActionAndData * photoActionAndData);
        void RefreshPhotoText(ActionAndData * photoActionAndData);
    public:
        void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = NULL) override;

    private:
        Evas_Object* mLayout;
        int mImageHeight;
        int mOrder;
        ActionAndData * mActionAndData;
        ImagePostScreen *mScreen;

    };
    void Push();
    void CreatePostScreenLayout();
    void CreatePostWidget(Evas_Object * parent);
    void RefreshPostWidget();
    void ShowCarousel(int imageNumber);
    void CalculateWidgetGeometry(int index, int *x, int *y, int *w, int *h);
    static void ImageDetailsReceived(void *data, char* str, int code);
    static Eina_Bool timer_cb( void *data);
    ImageWidget *GetImageWidgetByPhoto(Photo *photo);
    void EnableUpdateParentCache( bool isEnabled );
    Evas_Object* mVScroller;

    /**
     * @brief Scroller's box
     */
    Evas_Object *mBox;
    ImagesCarouselScreen* mCarouselScreen;
    int mCurrentImage;
    ImagePostAction * mImagePostAction;
    ActionAndData * mImagePostActionAndData;//pointer to whole post action and data
    Eina_List *mImagePhotoActionAndDataList;
    Eina_List *mImageWidgets;
    Eina_List *mRequests;
    ActionBase * mImagePhotoAction;
    ActionAndData * mParentActionAndData;
    Ecore_Timer *mTimer;
    ScreenBase *mParent;

public:
    ImagePostScreen(ActionAndData *parentActionAndData, int currentImage);
    virtual ~ImagePostScreen();
    bool HandleBackButton() override;
    void ShowWidget(int index);
    void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = NULL) override;
};
#endif /* IMAGEPOSTSCREEN_H_ */
