/*
 * MessagerScreen.h
 *
 *  Created on: Apr 6, 2015
 *      Author: RUINDMVA
 */

#ifndef MESSAGERSCREEN_H_
#define MESSAGERSCREEN_H_
#include "ScreenBase.h"

class MessagerScreen:public ScreenBase  {
public:
    MessagerScreen(ScreenBase *parentScreen);
    virtual ~MessagerScreen();
    static void cb_check_Messenger_inst_status();

protected:
    void Create_invoker_screen();
    void Create_installer_screen();

    static void trigger_msg_install(void *data, Evas_Object *obj, void *EventInfo);
    static void trigger_msg_invoke(void *data, Evas_Object *obj, void *EventInfo);
    static int check_Messenger_inst_status();

private:
    bool messager_inst;
    Evas_Object *mBtn;
    Evas_Object *mTitle;
    Evas_Object *mImage;
    Evas_Object *mInfoString;

    //Data Member for displayed screen
    static const int max_string_length;
    static MessagerScreen *msg_screen_instance;
    static void msg_screen_instance_set(MessagerScreen *);
    static MessagerScreen * msg_screen_instance_get();
};

#endif /* MESSAGERSCREEN_H_ */
