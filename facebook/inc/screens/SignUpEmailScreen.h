/*
 *  SignUpEmailScreen.h
 *
 *  Created on: 4th June 2015
 *      Author: Manjunath Raja
 */

#ifndef SIGNUPEMAILSCREEN_H_
#define SIGNUPEMAILSCREEN_H_

#include "ScreenBase.h"

class SignUpEmailScreen: public ScreenBase {
    private:
        unsigned short int mCountryCode;
        bool mUsingMobile;
        bool mChangeEmail;
        bool mConfirmMobile;
        Evas_Object *mEvasEmailLayout;
        Evas_Object *mEvasEmailInput;
        Evas_Object *mEvasToggleButton;
        Evas_Object *mEvasCountryDropDown;
        Evas_Object *mEvasMobileLayout;
        Evas_Object *mEvasMobileInput;
        Evas_Object *mEvasCtxpopup;

    private:
        void LoadEdjLayout();
        void CreateView();
        static void HeaderBackCb(void* Data, Evas_Object* Obj, void* EventInfo);

    public:
        SignUpEmailScreen(ScreenBase *parentScreen, bool ChangeEmail, bool ConfirmMobile);
        virtual ~SignUpEmailScreen();

    protected:
        static void CreateInput(Evas_Object *InputLayout,
                Evas_Object *Input, Elm_Input_Panel_Layout  EntryType);
        static void CreateEmailInput(SignUpEmailScreen *data);
        static void CreateMobileInput(SignUpEmailScreen *data);
        static void SetScreenInfo(SignUpEmailScreen *data, bool error);
        static const char *GetCountryCode(char *selectedCountry);
        static bool IsValidEmail(const char *EmailAddress);
        static void CountryDropDownCb(void *data, Evas_Object *obj, void *EventInfo);
        static void ContinueBtnCb(void *data, Evas_Object *obj, void *EventInfo);
        static void ToggleButtonCb(void *data, Evas_Object *obj, void *EventInfo);
        static void InputClearButtonCb(void *data, Evas_Object *obj, void *EventInfo);
        static void InputChangedCb(void *data, Evas_Object *obj, void *EventInfo);
        static void InputFocusedCb(void *data, Evas_Object *obj, void *EventInfo);
        static void InputUnFocusedCb(void *data, Evas_Object *obj, void *EventInfo);
        static void DropDownDismissedCb(void *data, Evas_Object *obj, void *EventInfo);
        static void DropDownSelectedItemCb(void *data, Evas_Object *obj, void *EventInfo);
        static void on_screen_keyb_on(void *data, Evas_Object *obj, void *event_info);
        static void on_screen_keyb_off(void *data, Evas_Object *obj, void *event_info);
};

#endif /* SIGNUPEMAILSCREEN_H_ */
