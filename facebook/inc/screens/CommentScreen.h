
#ifndef COMMENTSCREEN_H_
#define COMMENTSCREEN_H_

#include "GraphObject.h"
#include "IUpdateMediaData.h"
#include "ScreenBase.h"
#include "TrackItemsProxyCommentsAdapter.h"
#include "UIRes.h"

class ActionAndData;
class CommentsAction;
class FriendTagging;
class CommentsProvider;

class CommentScreen: public ScreenBase,
                     public IUpdateMediaData,
                     public TrackItemsProxyCommentsAdapter
{
public:
    CommentScreen(ScreenBase *previousScreeen, ActionAndData *data, ScreenIdEnum screenId = ScreenBase::SID_COMMENTS);
    ~CommentScreen() override;

    const char* GetId() const;
    Evas_Object* GetCommentEntry() const;
    Evas_Object* GetAttachedPhoto() const;

    void UpdateMedia() override;
    void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = NULL) override;

    // TrackItemsProxyAdapter
    void DeleteItem( void* item ) override;

    void SetSelectedActionNData(ActionAndData *selectedActionNData);
    ActionAndData *GetSelectedActionNData();

    CommentsProvider *GetCommentsProvider() const {return mCommentsProvider;}
    inline bool GetParentItemType() const {return mParentItemIsPost;}
    inline ScreenBase* GetPreviousScreen() const {return mPreviousScreeen;}

protected:
    void* UpdateItem(void* dataForItem, bool isAddToEnd) override;
    void* CreateItem(void* dataForItem, bool isAddToEnd) override;
    void SetDataAvailable(bool isAvailable) override;

private:
    void InitCommentScreen();

    static void back_btn_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void send_comment_cb(void *data, Evas_Object *obj, const char *emisson, const char *source);
    static void on_add_photo_button_clicked_cb(void *data, Evas_Object *obj, const char *emisson, const char *source);
    static void text_changed_cb(void *data, Evas_Object *obj, void *event_info);
    static void show_input_text_panel(void *data, Evas_Object *obj = NULL, void *event_info = NULL);
    static void on_unattach_btn_clicked_cb(void *data, Evas_Object *obj, const char *emisson, const char *source);
    static void on_is_post_exists_completed(void* object, char* respond, int code);

    bool HandleBackButton() override;
    bool HandleKeyPadHide() override;
    void OnErrorWidgetRetryClickedCb() override;
    void OnResume() override;
    void EnableSendButton();

    void CreateUI();
    void CreateLayout();

    void RemoveMediaPath();
    /**
     * @brief Create request to add comments with message from entry field and selected media data
     */
    void PostComment();

    /**
     * @brief Remove widget that is related to selected picture
     */
    void DeleteMedia();
    void Push() override;

    void EnableUpdateParentCache( bool isEnable );

    bool mIsReadyForPost;
    // The id of the object, for which comments screen is displayed
    char *mId;
    char *mMediaDataPath;
    char *mMediaDataThumbnailPath;
    CommentsAction *mCommentsAction;

    Evas_Object *mCommentLayout;
    Evas_Object *mCommentEntry;
    Evas_Object *mAttachedPhoto;

    FriendTagging *mTagging;

    /**
     * @bried ActionAndData of the object for which the screen is created
     */
    ActionAndData * mInActionNData;

    /**
     * @bried ActionAndData of the object for which was selected in the list
     */
    ActionAndData * mSelectedActionNData;

    GraphRequest * mIsPostExistsRequest;

    Evas_Object *mTopLayout;
    Evas_Object *mBotLayout;

    bool mIsDataDownloaded;

    static const unsigned int DEFAULT_ITEMS_WND_SIZE;
    static const unsigned int DEFAULT_INIT_ITEMS_SIZE;
    static const unsigned int DEFAULT_ITEMS_STEP_SIZE;

    CommentsProvider *mCommentsProvider;

    bool mPostExist;

    bool mParentItemIsPost;
    ScreenBase *mPreviousScreeen;
};
#endif /* COMMENTSCREEN_H_ */

