/* UserProfilePlacesScreen.h
 *
 * Created on: 11th August 2015
 * Author: Jeyaramakrishnan Sundar
 *
 */

#ifndef USER_PROFILE_FRIENDS_SCRREN_H
#define USER_PROFILE_FRIENDS_SCRREN_H

#include "ScreenBase.h"


class UserProfilePlacesScreen:public ScreenBase
{
    public:
        UserProfilePlacesScreen(const char *profileId);
        virtual ~UserProfilePlacesScreen();
        virtual void Push();
        char *mProfileId;

    protected:
        static UIRes *R;

    private:
        Evas_Object * mViewRect1;
        Evas_Object *mScroller;

        Evas_Object *CreatePlacesPage(Evas_Object *parent);
        Evas_Object *CreateScroller(Evas_Object *parent);

        virtual void OnViewResize(Evas_Coord w, Evas_Coord h);
        static void back_btn_cb(void *data , Evas_Object *obj, const char *emission, const char *source);
};

#endif
