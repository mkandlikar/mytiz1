#ifndef GROUPPROFILEPAGE_H_
#define GROUPPROFILEPAGE_H_

#include "FeedAction.h"
#include "FeedProvider.h"
#include "TrackItemsProxyAdapter.h"
#include "ScreenBase.h"

class OperationPresenter;

class GroupProfilePage: public ScreenBase,
                        public TrackItemsProxyAdapter
{
public:
    GroupProfilePage(const char *id, const char *name);
    void Push() override;
    virtual ~GroupProfilePage();
    bool HandleBackButton() override;
    void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = NULL) override;

    const char* GetProfileId();

protected:
    void FetchEdgeItems() override;

private:
    const char *mId;
    const char *mName;
    ActionAndData *mGroupActionAndData;
    FeedAction *mAction;
    GraphRequest *mDetailedData;

    Evas_Object *mGroupHeaderWidget;
    Evas_Object *mGroupProfileLayout;
    Evas_Object *mGroupFeedWidget;

    void UpdatePost(ActionAndData* &data, void* postPtr, bool isAddToEnd, Evas_Object* prevItem = nullptr);
    void UpdatePresenter(ActionAndData* &data, OperationPresenter *operation, bool isAddToEnd);

    void* UpdateItem( void* dataForItem, bool isAddToEnd ) override;
    void* CreateItem( void* dataForItem, bool isAddToEnd ) override;
    void* UpdateSingleItem( void* dataForItem, void* prevItem ) override;

    void RequestDetailedData();
    void CreateBaseUI();
    void SetData(ActionAndData *actionData);

    static void on_get_group_details_completed(void* object, char* respond, int code);

    FeedProvider * mFeedProvider;

    bool mProfileWidgetExist;
    bool mAllowFetchData;

    void HideHeaderWidget() override;
    void ShowHeaderWidget() override;
    inline bool IsGroupProfileWidgetShown();
};

#endif /* GROUPPROFILEPAGE_H_ */
