/*
 * GroupSeeAllScreen.h
 *
 *  Created on: Sep 8, 2015
 *      Author: ruibezruch
 */

#ifndef GROUPSEEALLSCREEN_H_
#define GROUPSEEALLSCREEN_H_

#include "FeedAction.h"
#include "ScreenBase.h"
#include "TrackItemsProxyAdapter.h"

class GroupSeeAllScreen: public ScreenBase,
                         public TrackItemsProxyAdapter
{
public:
    GroupSeeAllScreen();
    virtual void Push();
    virtual ~GroupSeeAllScreen();
private:
    FeedAction* mAction;

    virtual void* CreateItem( void* dataForItem, bool isAddToEnd );
    virtual void SetDataAvailable(bool isAvailable);
    virtual void SetNoDataViewVisibility(bool isVisible);

    static Evas_Object *GroupsGet(void *user_data, Evas_Object *obj, bool isAddToEnd);
    static Evas_Object *CreateSuggestedItem(void *user_data, Evas_Object *obj, bool isAddToEnd);
    void CreateGroupItem(Evas_Object* parent, ActionAndData *action_data);

    static void on_item_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_suggested_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);

    static const unsigned int DEFAULT_ITEMS_WND_SIZE;
};

#endif /* GROUPSEEALLSCREEN_H_ */
