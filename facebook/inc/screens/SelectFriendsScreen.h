#ifndef SELECTFRIENDSCREEN_H_
#define SELECTFRIENDSCREEN_H_

#include "GenListBase.h"
#include "DataServiceProtocol.h"
#include "ScreenBase.h"

class GraphObject;
class Friend;

class ISelectFriendsScreenClient {
public:
    virtual ~ISelectFriendsScreenClient(){};
    virtual void FriendsSelectionCompleted(Eina_List *friends, bool isCanceled) = 0;
};

class DataUploader;

class SelectFriendsScreen : public ScreenBase {
public:
    class FriendInfo {
    public:
        FriendInfo();
        ~FriendInfo();

        void SetFriend(Friend *fr);
        inline Friend *GetFriend() {
            return mFriend;
        }

        void *mClientFriend;
        bool mIsSelected;

    private:
        Friend *mFriend;
    };

private:
    class ListGroup : public GenListGroupItemBase {
        friend class SelectFriendsScreen;
        enum Type {
            ESuggested,
            EMore,
            EAllFriends
        };
    public:
        ListGroup(const char*groupName, Type type, SelectFriendsScreen *screen) : mType(type), mScreen(screen) {
            if (groupName) {
                mGroupName = new char[strlen(groupName) + 1];
                eina_strlcpy(mGroupName, groupName, strlen(groupName) + 1);
            } else {
                mGroupName = NULL;
            }
        }
        ~ListGroup(){delete []mGroupName;};
        Eina_List *GetList() {return mItems;}

    private:
        virtual Evas_Object* DoCreateLayout(Evas_Object* parent);

        virtual bool Filter(Elm_Object_Item *insertAfter);
        virtual int Compare(GenListItemBase* otherItem);

    private:
         char* mGroupName;
         Type mType;
         SelectFriendsScreen *mScreen;
    };

    class ListItem : public GenListItemBase, public IDataServiceProtocolClient {
        friend class SelectFriendsScreen;

        enum UpdateType {
            EImage,
            ECheck,
            EAll
        };

        class ListItemSharedData {
        public:
            ListItemSharedData() : mFriendInfo(NULL), mImagePath(NULL), mSuggestionOrder(-1) {}
            ~ListItemSharedData();
            FriendInfo *mFriendInfo;
            const char *mImagePath;
            int mSuggestionOrder;
        };

    public:
        ListItem(SelectFriendsScreen *screen, ListItemSharedData *data);
        ~ListItem();

    private:
         Evas_Object* ItemContentGet(Evas_Object *parent);
         Evas_Object* ItemCreateImage(Evas_Object *parent);
         virtual Evas_Object* DoCreateLayout(Evas_Object* parent);
         virtual int Compare(GenListItemBase* otherItem);
         virtual bool Filter(Elm_Object_Item *insertAfter);

         virtual void HandleDownloadImageResponse(ErrorCode error, MessageId requestID, const char* url, const char* fileName);
         void UpdateAllItems(UpdateType uType);

    public:
         ListItemSharedData *mData;

    protected:
         bool mIsDataOwner;
         MessageId mRequestId;
         SelectFriendsScreen *mScreen;
    };

    class SuggestedListItem : public ListItem {
        friend class SelectFriendsScreen;

    public:
        SuggestedListItem(SelectFriendsScreen *screen, ListItemSharedData *data) :
            ListItem(screen, data) {}

    private:
        virtual int Compare(GenListItemBase* otherItem);
    };

public:
    SelectFriendsScreen(ISelectFriendsScreenClient *client, const char *caption);
    virtual ~SelectFriendsScreen();

    void SetFriendInfos(Eina_List *friendInfos);

    const char *GetFilterText() {return mFilterText;}

    void UnregisterClient();

private:
    virtual void Push();
    virtual void ConfirmCallback (void *data, Evas_Object *obj, const char *emission, const char *source);
    virtual bool HandleBackButton();
    virtual void CancelCallback(void *data, Evas_Object *obj, void *event_info);

    void UpdateSuggestedFriends(const Eina_Array *suggested);
    ListItem *GetListItemForFriend(GraphObject *fr);

    void FillAllFriendsGroup(Eina_List *friendInfos);
    void FillSuggestedGroup();
    void ExtendSuggestedGroup();

    static Evas_Object* ItemCreateImage(Evas_Object *parent, SelectFriendsScreen::ListItem *id);
    static char *GItemTextGet(void *data, Evas_Object *obj, const char *part);

    static Eina_Bool set_friend_infos_async(void* data);

    void UpdateAllItemsById(const char* id, ListItem::UpdateType uType);

    void UpdateClearButton();
    void CreateSelectedFriendsField();
    void UpdateSelectedFriendsField();
    int GetSelectedFriendsFieldLinesCount();
    const char* GenSelectedFriendsField();
    bool AreThereSelectedFriends();
    int SelectedFriendsCount();
    void UpdateList();

    char* GenReplacedStr(const char*str, const char*orig, const char*rep);

    ListItem *GetLastSelectedItem();

    void BackspacePressed();
    void UpdateSelectedList(ListItem *item);
    void ItemSelected(ListItem *item);

    Evas_Object* CreateSelectedTextItem(ListItem *li);
    Evas_Object* CreateSelectedTextItemLayout(const char *text);
    int CalculateSelectedTextItemWidth(const char *text, const char *part);

    static void onSearchPlaceholderClicked (void *data, Evas_Object *obj, const char *emission, const char *source);
    static void onItemClicked (void *data, Evas_Object *obj, const char *emission, const char *source);
    static void onClearClicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void onMoreClicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    static Evas_Object *item_provider(void *data, Evas_Object *entry, const char *item);
    static void CursorChangedCb(void *data, Evas_Object *obj, void *event_info);
    static void FilterChangedCb(void *data, Evas_Object *obj, void *event_info);
    static void update_field(void *data);

    static void key_down_cb(void *data, Evas *e, Evas_Object *obj, void *event_info);
    static void anchor_clicked_cb(void *data, Evas_Object *obj, void *event);

    static void on_get_suggested_friends_completed(void* data);
    static void text_item_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);

private:
    Evas_Object *mSelectedFriendsGroup;
    Evas_Object* mSelectedFriendsField;
    Evas_Object* mSearchPlaceholder;
    ISelectFriendsScreenClient *mClient;
    GenList *mGenList;
    ListGroup *mSuggestedFriendsGroup;
    ListGroup *mAllFriendsGroup;
    ListGroup *mMoreGroup;
    Eina_List *mSelectedList;
    int mMinCurPos;
    const char *mTagsText;
    const char *mFilterText;
    Ecore_Job *mAsyncJob;
    Ecore_Timer *mUpdateTimer;
    DataUploader *mSuggestedFriendsUploader;
    ListItem *mHighlightedFriend;
    bool mIsDataSet;
    Eina_List *mFriendInfos;
};

#endif /* SELECTFRIENDSCREEN_H_ */
