/*
 *  SignUpPswdScreen.h
 *
 *  Created on: 9th June 2015
 *      Author: Manjunath Raja
 */

#ifndef SIGNUPPSWDSCREEN_H_
#define SIGNUPPSWDSCREEN_H_

#include "ScreenBase.h"

class SignUpPswdScreen: public ScreenBase {
    private:
        Evas_Object *mEvasPasswordInput;

        void LoadEdjLayout();
        void CreateView();
        Evas_Object *CreatePasswordInput();
        static void HeaderBackCb(void* Data, Evas_Object* Obj, void* EventInfo);

    public:
        SignUpPswdScreen(ScreenBase *parentScreen);
        virtual ~SignUpPswdScreen();

    protected:
        static void ContinueBtnCb(void *data, Evas_Object *obj, void *EventInfo);
        static void InputClearButtonCb(void *data, Evas_Object *obj, void *EventInfo);
        static void InputChangedCb(void *data, Evas_Object *obj, void *EventInfo);
        static void InputFocusedCb(void *data, Evas_Object *obj, void *EventInfo);
        static void InputUnFocusedCb(void *data, Evas_Object *obj, void *EventInfo);
        static void on_screen_keyb_on(void *data, Evas_Object *obj, void *event_info);
        static void on_screen_keyb_off(void *data, Evas_Object *obj, void *event_info);
};

#endif /* SIGNUPPSWDSCREEN_H_ */
