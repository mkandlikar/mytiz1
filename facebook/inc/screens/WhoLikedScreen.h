#ifndef WHOLIKEDSCREEN_H_
#define WHOLIKEDSCREEN_H_

#include "FriendsAction.h"
#include "ScreenBase.h"
#include "TrackItemsProxyAdapter.h"

class PeopleWhoLikedProvider;

class WhoLikedScreen: public ScreenBase,
                      public TrackItemsProxyAdapter
{
public:
    WhoLikedScreen(const char * id);
    virtual ~WhoLikedScreen();

protected:
    void* CreateItem(void* dataForItem, bool isAddToEnd) override;

private:
    enum ButtonState {
        ADD_FRIEND,
        CANCEL_FRIEND,
        ARE_FRIENDS,
        HIDE_BUTTON
    };

    FriendsAction *mAction;
    char *mId;
    Eina_List * mUserDetailsRequestList;

    static void back_btn_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_item_click(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void on_friends_button_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void user_details_cb(void* object, char* respond, int code);

    Evas_Object* CreateUserItem(Evas_Object* parent, ActionAndData *action_data, bool isAddToEnd);
    void* UpdateItem(void *dataForItem, bool isAddToEnd) override;
    void Push() override;
    void Update(AppEventId eventId, void *data) override;

    void UpdateFriendshipStatus(ActionAndData *data);

    void DeleteAllUserDetailsRequests();
    void FetchEdgeItems() override;
    void RebuildAllItems() override;

    static const unsigned int DEFAULT_ITEMS_WND_SIZE;
    static const unsigned int DEFAULT_INIT_ITEMS_SIZE;
    static const unsigned int DEFAULT_ITEMS_TEP_SIZE;

    static void RefreshButton(Evas_Object *list_item, ButtonState buttonState);
    PeopleWhoLikedProvider *mProvider;
};

#endif /* WHOLIKEDSCREEN_H_ */
