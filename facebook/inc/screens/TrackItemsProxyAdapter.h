#ifndef TRACKITEMSPROXYADAPTER_H_
#define TRACKITEMSPROXYADAPTER_H_

#include "TrackItemsProxy.h"

class TrackItemsProxyAdapter: public TrackItemsProxy
{
public:
    TrackItemsProxyAdapter( Evas_Object* parentLayout, AbstractDataProvider *provider, unsigned int headerSize = ItemsBuilderAssistent::SWITCH_OFF_HEDER_BUFFERIZATION, ItemsCache* itemsCache = nullptr);
    virtual ~TrackItemsProxyAdapter();
    void DestroyUIItem(void *data);

protected:
    virtual void DeleteUIItem( void* item );

    void IdleStateHandler() override;
    void DeleteItem( void* item ) override;
    void DeleteAll_Prepare() override;
    void DeleteAll_ItemDelete(void* item ) override;
    ItemSize* GetItemSize( void* item ) override;
    void HideItem( void *item ) override;
    void ShowItem( void *item ) override;
    bool IsItemsEqual( void *itemToCompare, void *itemForCompare ) override;
    char* GetItemId( void *item ) override;
    void* GetDataForItem( void *item ) override;
    void* GetWgtForItem(void *item) override;
    void UpdateDataForItem(void *item) override;
    ItemSize* SaveSize( void *item ) override;
    ItemSize* GetSize( void *item ) override;
    void RemovePresenter(void *item) override;
    bool FindDuplicateItem(void *item) override;

    bool IsItemInDataArea(void* item);

    void SetPostponedItemDeletion(bool value) { m_PostponedItemDeletion = value; };
    bool GetPostponedItemDeletion() { return m_PostponedItemDeletion; };

    static bool items_comparator( void *itemToCompare, void *itemForCompare );
    static bool items_comparator_by_ptr( void *itemToCompare, void *itemForCompare );
    static bool items_comparator_Obj_vs_ActionAndData( void *itemToCompare, void *itemForCompare );
    static bool operation_comparator( void *itemToCompare, void *itemForCompare );
    static bool go_item_comparator( void *itemToCompare, void *itemForCompare );

private:
    void DeleteItemFromGarbageManager();
    Evas_Coord DeleteItemDirectly( void* item ) override;
    Evas_Coord GetWidgetHeight(void *item) override;

    bool m_PostponedItemDeletion;
    Utils::DoubleSidedStack *m_PendingItemsToDelete;

    static const unsigned int MAX_STORED_ITEMS;
    static const unsigned int PORTION_SIZE;
};

#endif /* TRACKITEMSPROXYADAPTER_H_ */
