/* NearByPlacesScreen.h
 *
 * Created on: 14 OCT 2015
 * Author: Jeyaramakrishnan Sundar
 *
 */

#ifndef NEAR_BY_PLACES_SCRREN_H
#define NEAR_BY_PLACES_SCRREN_H

#include "ScreenBase.h"
#include "PCPrivacyModel.h"
#include "ConfirmationDialog.h"

struct PCPlace;

class NearByPlacesScreen:public ScreenBase
{
    public:
        NearByPlacesScreen();
        virtual ~NearByPlacesScreen();
        virtual void Push();
        void setMapPosition(double latitude , double longitude);
        void setPlacesMarker(const char *filename , double latitude, double longitude, const char *link);
        void add_icon(Elm_Map_Overlay *parent, const char *filename , double latitude, double longitude, const char *link, int size);
        void ShowHideMsg(bool);
        void PlaceSelected(Place* place);

    private:
        Evas_Object *mMapObj;
        Elm_Map_Overlay *places_ovl_class;
        Elm_Map_Overlay *my_loc_ovl_class;
        Evas_Object *playout;
        PCPlace *mPlace;
        double mLat;
        double mLong;

        Evas_Object *CreatePlacesPage(Evas_Object *parent);
        void create_map_area(Evas_Object *parent);
        void LaunchCheckInScreen();

        static void back_btn_cb(void *data , Evas_Object *obj, const char *emission, const char *source);
        static void bringIn_btn_cb(void *data , Evas_Object *obj, const char *emission, const char *source);
        static void on_search_button_clicked(void *data, Evas_Object *obj, const char *emission, const char * source);
        static void on_nearby_button_clicked(void *data, Evas_Object *obj, const char *emission, const char * source);
        static void on_places_item_click(void *data, Evas_Object *obj, void *event_info);
};

#endif
