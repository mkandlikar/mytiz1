#ifndef SELECTUSERSCREEN_H_
#define SELECTUSERSCREEN_H_

#include "ScreenBase.h"
#include "TrackItemsProxy.h"

class FriendsAction;

class SelectUserScreen: public ScreenBase,
                  public TrackItemsProxy
{
public:
    SelectUserScreen(ScreenBase *parentScreen, Eina_List *ids);
    virtual ~SelectUserScreen();

    static bool go_item_comparator(void *itemToCompare, void *itemForCompare);

private:
    virtual void Update( AppEventId eventId = eUNDEFINED_EVENT, void *data = NULL );
    virtual void* CreateItem(void* dataForItem, bool isAddToEnd);
    virtual void* UpdateItem(void* dataForItem, bool isAddToEnd);
    virtual void Push();
    virtual bool HandleBackButton();
    virtual void DeleteItem(void* item);
    void DeleteUIItem(void* item);
    virtual void HideItem(void *item);
    virtual void ShowItem(void *item);
    virtual ItemSize* GetItemSize(void* item);
    virtual char* GetItemId(void *item);
    virtual void DeleteAll_Prepare();
    virtual void DeleteAll_ItemDelete(void* item );
    virtual bool IsItemsEqual( void *itemToCompare_data, void *itemForCompare_ui );
    virtual void* GetDataForItem( void *item );
    virtual ItemSize* SaveSize( void *item );
    virtual ItemSize* GetSize( void *item );

    void PackToDataArea(Evas_Object *layout, bool isAddToEnd);

private:
    FriendsAction* mAction;
};
#endif /* SELECTUSERSCREEN_H_ */
