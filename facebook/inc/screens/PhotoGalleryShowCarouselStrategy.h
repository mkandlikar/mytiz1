#ifndef PHOTOGALLERYSHOWCAROUSELSTRATEGY_H_
#define PHOTOGALLERYSHOWCAROUSELSTRATEGY_H_

#include "PhotoGalleryScreenBase.h"


class Photo;

class PhotoGalleryShowCarouselStrategy : public PhotoGalleryStrategyBase
{
public:
    PhotoGalleryShowCarouselStrategy();

protected:

private:
    virtual ~PhotoGalleryShowCarouselStrategy();
    virtual void PhotoSelected(Photo* photo);
};

#endif /* PHOTOGALLERYSHOWCAROUSELSTRATEGY_H_ */
