/* GetStartedAddPhoto.h
 *
 * Created on: 9th July 2015
 * Author: Jeyaramakrishnan Sundar
 *
 */

#ifndef GETSTARTED_ADD_PHOTO_H
#define GETSTARTED_ADD_PHOTO_H

#include "ScreenBase.h"


class GetStartedAddPhoto:public ScreenBase
{
    public:
        GetStartedAddPhoto();
        virtual ~GetStartedAddPhoto();

    private:

        static void skip_btn_cb(void *data , Evas_Object *obj, const char *emission, const char *source);
        static void take_photo_btn_cb(void *data , Evas_Object *obj, const char *emission, const char *source);
        static void camera_result_cb(app_control_h request, app_control_h reply, app_control_result_e result, void *data);
        static void choose_from_gallery_btn_cb(void *data , Evas_Object *obj, const char *emission, const char *source);
        static void file_select_result_cb(app_control_h request, app_control_h reply, app_control_result_e result, void *user_data);
        static bool foreach_extra_data_cb(app_control_h app_control, const char *key, void *data);
        void get_last_file_path_from_camera(char* *file_path);
        void set_profile_picture(void *data, const char *path);
};
#endif
