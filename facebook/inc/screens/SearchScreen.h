#ifndef SEARCHSCREEN_H_
#define SEARCHSCREEN_H_

#include "ScreenBase.h"
#include "TrackItemsProxyAdapter.h"

class ActionAndData;
class FbRespondGetSearchResults;

/*
 *
 */
class SearchScreen : public ScreenBase,
                     public TrackItemsProxyAdapter
{
public:
    SearchScreen(const char * searchFieldString = NULL);
    virtual ~SearchScreen();
    void SetSearchFieldString(const char * searchFieldString);

    /**
     * @brief Overloaded Push metod
     */
    void Push() override;
    void OnPause() override;

    void ShowInputPanel();

protected:
    void* CreateItem(void* dataForItem, bool isAddToEnd) override;

private:
    // Store entry for search field
    Evas_Object *mSearchField;
    char *mSearchString;

    void CreateSearchField(Evas_Object *parent);

    // Search field handlers
    static void searchfield_changed_cb(void *data, Evas_Object *obj, void *event_info);
    static void searchfield_activated_cb(void *data, Evas_Object *obj, void *event_info);
    static void searchfield_clear_button_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void searchfield_back_button_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);

    void StartQuickSearchRequest(const char * searchString);
    static void on_get_quick_search_results_completed(void* object, char* respond, int code);
    void SetSearchResults(FbRespondGetSearchResults * response);

    void CreateUserItem(Evas_Object* parent, ActionAndData *action_data);
    Evas_Object *ItemGet(void *user_data, Evas_Object *obj, bool isAddToEnd);

    static void on_item_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);

    bool HandleBackButton() override;

};

#endif /* SEARCHSCREEN_H_ */
