/*
 *  SignUpConfirmScreen.h
 *
 *  Created on: 22nd June 2015
 *      Author: Manjunath Raja
 */

#ifndef SIGNUPCONFIRMSCREEN_H_
#define SIGNUPCONFIRMSCREEN_H_

#include "ScreenBase.h"

class SignUpConfirmScreen: public ScreenBase {
    private:
        Evas_Object *mCodeLayout;
        Evas_Object *mCodeInput;

    private:
        Evas_Object *LoadEdcLayout(Evas_Object *parent);
        void CreateView(Evas_Object *parent);

    public:
        SignUpConfirmScreen(ScreenBase *parentScreen);
        virtual ~SignUpConfirmScreen();

    protected:
        static Evas_Object *CreateInput(Evas_Object *parent, Evas_Object *Input,
                Evas_Object *entry, Elm_Input_Panel_Layout  EntryType);
        static void CreateConfirmInput(SignUpConfirmScreen *data);
        static void CreateCodeInput(SignUpConfirmScreen *data);
        static void SetScreenInfo(SignUpConfirmScreen *data, bool error);
        static void ContinueBtnCb(void *data, Evas_Object *obj, void *EventInfo);
        static void SendCodeAgainBtnCb(void *data, Evas_Object *obj, void *EventInfo);
        static void ChangeEmailBtnCb(void *data, Evas_Object *obj, void *EventInfo);
        static void ConfirmByPhoneBtnCb(void *data, Evas_Object *obj, void *EventInfo);
        static void InputClearButtonCb(void *data, Evas_Object *obj, void *EventInfo);
        static void InputChangedCb(void *data, Evas_Object *obj, void *EventInfo);
        static void InputFocusedCb(void *data, Evas_Object *obj, void *EventInfo);
        static void InputUnFocusedCb(void *data, Evas_Object *obj, void *EventInfo);
};

#endif /* SIGNUPCONFIRMSCREEN_H_ */
