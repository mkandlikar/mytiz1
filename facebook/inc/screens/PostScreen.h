#ifndef POSTSCREEN_H_
#define POSTSCREEN_H_

#include "GraphObjectDownloader.h"
#include "IUpdateMediaData.h"
#include "ScreenBase.h"
#include "TrackItemsProxyCommentsAdapter.h"

class FriendTagging;
class CommentsAction;
class CommentsProvider;
class Photo;
class PostScreenAction;

class PostScreen: public ScreenBase,
                  public IUpdateMediaData,
                  public TrackItemsProxyCommentsAdapter,
                  public IGraphObjectDownloaderObserver

{
public:
    PostScreen(ActionAndData *parentActionAndData, const char *postId = nullptr, GraphObject *object = nullptr);
    ~PostScreen() override;
    void Push() override;

    void UpdateMedia() override;

    CommentsAction* mCommentsAction;

    Evas_Object* GetCommentEntry() const { return mCommentEntry; }
    Evas_Object* GetAttachedPhoto() const { return mAttachedPhoto; }

    void* CreateItem( void* dataForItem, bool isAddToEnd ) override;
    void* UpdateItem( void* dataForItem, bool isAddToEnd ) override;
    void DeleteItem( void* item ) override;

    void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = NULL) override;
    void SetSelectedActionNData(ActionAndData *selectedActionNData);
    ActionAndData *GetSelectedActionNData();

    CommentsProvider *GetCommentsProvider() const {return mCommentsProvider;}
    void UpdateItems() {
        TrackItemsProxy::UpdateItemsWindow();
    };

private:

    bool HandleBackButton() override;
    bool HandleKeyPadHide() override;
    void OnResume() override;

    void SetScrollerUi();
    void SetEntryUi();

    void SetBasicData();

    void CreatePhotoHeader();
    void CreatePhotoItem();

    void GraphObjectDownloaderProgress(IGraphObjectDownloaderObserver::Result result) override;
    void DownloaderProgress(Result result, GraphObjectDownloader *downloader) override {};

    bool mIsReadyForPost;
    FriendTagging *mTagging;

    PostScreenAction *mPostScreenAction;
    /**
     * @brief ActionAndData of the object for which the screen is created
     */
    ActionAndData * mInActionNData;
    ActionAndData * mSharedSourceNData;

    /**
     * @brief ActionAndData of the object for which was selected in the list
     */
    ActionAndData * mSelectedActionNData;

    Evas_Object *mCommentEntry;
    Evas_Object* mHeaderWidget;
    Evas_Object *mCommentEntryLayout;
    Evas_Object* mContainer;

    Evas_Object *mAttachedPhoto;
    Evas_Object *mUnattachBtn;

    char *mMediaDataPath;
    char *mMediaDataThumbnailPath;

    Evas_Object *mPostText;

    void SetData();

    static void on_add_photo_button_clicked_cb(void *data, Evas_Object *obj, const char *emisson, const char *source);
    static void send_comment_cb(void *data, Evas_Object *obj, const char *emisson, const char *source);

    static void enable_send_btn(void *data, Evas_Object *obj = NULL, void *event_info = NULL);
    static void show_input_text_panel(void *data, Evas_Object *obj, void *event_info);

    static void on_delete_button_clicked_cb(void *data, Evas_Object *obj, const char *emisson, const char *source);
    static void on_anchor_clicked_cb(void *data, Evas_Object *obj, void *eventInfo);

    void DeleteMedia();
    void RemoveMediaPath();

    void PostComment();

    void RefreshLikeText();

    void CreateLikeText();
    void GenPostLikes(int likeCount, bool hasLiked, const char *likeFromId, const char *likeFromName, const char *id);

    static const unsigned int DEFAULT_ITEMS_WND_SIZE;
    static const unsigned int DEFAULT_INIT_ITEMS_SIZE;
    static const unsigned int DEFAULT_ITEMS_STEP_SIZE;

    CommentsProvider *mCommentsProvider;

    GraphObjectDownloader *mObjectDownloader;
    GraphObjectDownloader *mPostInPostDownloader;
    Evas_Object *mPostInPostWrapper;
};

#endif /* POSTSCREEN_H_ */
