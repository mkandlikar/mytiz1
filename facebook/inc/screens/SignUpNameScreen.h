/*
 *  SignUpNameScreen.h
 *
 *  Created on: 9th June 2015
 *      Author: Manjunath Raja
 */

#ifndef SIGNUPNAMESCREEN_H_
#define SIGNUPNAMESCREEN_H_

#include "ScreenBase.h"

class SignUpNameScreen: public ScreenBase {
    private:
        Evas_Object *mEvasFNameInput;
        Evas_Object *mEvasLNameInput;

        void CreateLayout(Evas_Object *Parent);
        void CreateView();
        void CreateInput(Evas_Object *Parent, Evas_Object *InputLayout, Evas_Object *Input);
        static void HeaderBackCb(void* Data, Evas_Object* Obj, void* EventInfo);

    public:
        SignUpNameScreen(ScreenBase *parentScreen);
        virtual ~SignUpNameScreen();

    protected:
        static void ContinueBtnCb(void *data, Evas_Object *obj, void *EventInfo);
        static void InputClearButtonCb(void *data, Evas_Object *obj, void *EventInfo);
        static void InputChangedCb(void *data, Evas_Object *obj, void *EventInfo);
        static void InputFocusedCb(void *data, Evas_Object *obj, void *EventInfo);
        static void InputUnFocusedCb(void *data, Evas_Object *obj, void *EventInfo);
        static void on_screen_keyb_on(void *data, Evas_Object *obj, void *event_info);
        static void on_screen_keyb_off(void *data, Evas_Object *obj, void *event_info);
};

#endif /* SIGNUPNAMESCREEN_H_ */
