#ifndef POSTCOMPOSERMEDIADATA_H_
#define POSTCOMPOSERMEDIADATA_H_

#include <Elementary.h>
#include <media_content.h>

#include "FriendTagging.h"
#include "Photo.h"
#include "Utils.h"

class Album;

enum DECODE_STATUS {
    DECODE_STATUS_UNKNOWN,
    UNABLE_TO_DECODE,
    ABLE_DECODE_TO_JPEG,
    ABLE_DECODE_TO_PNG
};

class PostComposerMediaData : public Photo {
private:
    virtual ~PostComposerMediaData();

    /**
     * @brief Path to original media. Used for photo editing
     */
    const char * mOriginalPath;

    /**
     * @brief Path to thumbnail of media.
     */
    const char * mThumbnailPath;

    /**
     * @brief Path to original thumbnail of media. Used in camera roll
     */
    const char * mOriginalThumbnailPath;

    /**
     * @brief Media type
     */
    media_content_type_e mType;

    /**
     * @brief Index of chosen media. It's only sets now. And never used.
     *        Maybe it will be useful in UI. If not then remove.
     */
    int mIndex;

    /**
     * @brief Photo caption, initially it equals to mOriginalCaption value.
     *        However the actual value depends on the user input.
     */
    const char * mCaption;

    /**
     * @brief Original Photo caption, it equals to the server value.
     */
    const char * mOriginalCaption;

    /**
     * @brief Layout which includes caption of current media.
     */
    Evas_Object * mCaptionLayout;

    /**
     * @brief List of texts which was created for this photo.
     */
    Eina_List * mTextList;

    /**
     * @brief Elm image object for media data in PostComposerScreen.
     *        Used for change picture in PostComposerScreen from AddTextScreen.
     */
    Evas_Object * mImageObject;

    /**
     * @brief Gengrid item of media data in CameraRollScreen. Use only in CameraRollScreen.
     */
    Elm_Object_Item * mGengridItem;

    /**
     * @brief Tag ID for media
     */
    const char * mMediaId;

    /**
     * @brief Media file size.
     */
    unsigned long long mFileSize;

    /**
     * @brief ID on FB server when photo is uploaded
     */
    const char * mPhotoId;

    /**
     * *@brief If the media has been formed as a result of app control operation
     */
    bool mMediaData_AppCtrl;
    /**
     * *@brief To do not decode every time
     * */
    int mAbleToDecode;

    /**
     * @brief Width of a photo or video
     */
    int mWidth;

    /**
     * @brief Height of a photo or video
     */
    int mHeight;

    char *mImageUrl;

    Evas_Object *mImageLayout;

    char *mTextToCaption;

    struct cropParams{
        int x,y,w,h;
        int orientation;
        bool saved;
    } mCropParams;

    /**
     * @brief Path to temp cropped media. Used for photo editing
     */
    const char * mTempCropImagePath;
    FriendTagging *mTagging;

public:
    PostComposerMediaData(const char * path, const char * thumbnailPath, media_content_type_e type, const char * mediaId,
            unsigned long long fileSize, bool AppCtrl = false, const char *imageUrl = NULL);

    /**
     * @brief Copy constructor
     */
    PostComposerMediaData(const PostComposerMediaData & other);

    /**
     * @brief Returns true when media ids are equal
     */
    static int media_data_comparator(const void *data1, const void *data2);

    /**
     * @brief Set the width of a photo or video
     */
    inline void SetWidth( int width ) { mWidth = width; }

    /**
     * @brief Set the height of a photo or video
     */
    inline void SetHeight( int height ) { mHeight = height; }

    /**
     * @brief Get original photo path
     */
    inline const char * GetOriginalPath() const { return mOriginalPath; }

    inline const char * GetTempCropImagePath() { return mTempCropImagePath; }

    inline void SetTempCropImagePath(const char * path) { mTempCropImagePath = path; }

    /**
     * @brief Get index number in selected list
     */
    inline int GetIndex() { return mIndex; }

    /**
     * @brief Set index number is selected list
     */
    inline void SetIndex(int index) { mIndex = index; }

    /**
     * @brief Check is chosen photo or not
     */
    inline bool IsSelected() { return mIndex > 0; }

    /**
     * @brief Get media type enum
     */
    inline media_content_type_e GetType() { return mType; }

    /**
     * @brief Get the width of a photo or video
     */
    inline int GetWidth() const { return mWidth; }

    /**
     * @brief Get the height of a photo or video
     */
    inline int GetHeight() const { return mHeight; }

    /**
     * @brief Set thumbnail path of media
     */
    inline void SetThumbnailPath(const char * path) { mThumbnailPath = path; }

    /**
     * @brief Get thumbnail path of media
     */
    inline const char * GetThumbnailPath() { return mThumbnailPath; }

    /**
     * @brief Get thumbnail path of media
     */
    inline const char * GetOriginalThumbnailPath() { return mOriginalThumbnailPath; }

    /**
     * @brief Set original thumbnail path of media
     */
    inline void SetOriginalThumbnailPath(const char * path) { mOriginalThumbnailPath = path; }

    /**
     * @brief Get media layout
     */
    inline Evas_Object * GetCaptionLayout() { return mCaptionLayout; }

    /**
     * @brief Set media layout
     */
    inline void SetCaptionLayout(Evas_Object * layout) { mCaptionLayout = layout; }

    /**
     * @brief Get caption text
     */
    inline const char * GetCaption() { return mCaption; }

    /**
     * @brief Set caption text
     */
    inline void SetCaption(const char * caption) { mCaption = SAFE_STRDUP(caption); }

    inline const char * GetOriginalCaption() { return mOriginalCaption; }

    inline void SetOriginalCaption(const char *origCaption) { free((void *)mOriginalCaption); mOriginalCaption = SAFE_STRDUP(origCaption); }

    inline Eina_List * GetTextList() { return mTextList; }

    inline void TextListAppend(const void * data) { mTextList = eina_list_append(mTextList, data); }

    inline void TextListRemoveList(Eina_List * list) { mTextList = eina_list_remove_list(mTextList, list); }

    inline void TextListDemoteList(Eina_List * list) { mTextList = eina_list_demote_list(mTextList, list); }

    inline void SetImageObject(Evas_Object * object) { mImageObject = object; }

    inline Evas_Object * GetImageObject() { return mImageObject; }

    inline void SetGengridItem(Elm_Object_Item * item) { mGengridItem = item; }

    inline Elm_Object_Item * GetGengridItem() { return mGengridItem; }

    inline void SetMediaId(char * mediaId) { mMediaId = mediaId; }

    inline const char * GetMediaId() const { return mMediaId; }

    inline void SetFileSize(unsigned long long fileSize) { mFileSize = fileSize; }

    inline unsigned long long GetFileSize() { return mFileSize; }

    inline void SetPhotoId(const char *photoId) { free((void *)mPhotoId); mPhotoId = photoId ? strdup(photoId) : NULL; }
    inline const char * GetPhotoId() { return mPhotoId; }

    inline void SetDecodeStatus(DECODE_STATUS status) { mAbleToDecode = status; }
    inline int  GetDecodeStatus() { return mAbleToDecode; }

    inline bool IsMediaDataAppCtrl() { return mMediaData_AppCtrl; }

    inline bool AbleToRestoreCropParams() { return mCropParams.saved; }

    inline const char *GetSrcUrl() {return mImageUrl;}

    inline void SetImageLayout(Evas_Object * object) { mImageLayout = object; }
    inline Evas_Object * GetImageLayout() { return mImageLayout; }

    inline void SetTextToCaption(const char *text) { mTextToCaption = SAFE_STRDUP(text); };
    inline char *GetTextToCaption() { return mTextToCaption; };

    inline void SetCropParams(int x,int y,int w,int h,int orientation) {
    	mCropParams.x = x;
    	mCropParams.y = y;
    	mCropParams.w = w;
    	mCropParams.h = h;
    	mCropParams.orientation = orientation;
    	mCropParams.saved = true;
    }
    inline void GetCropParams(int &x,int &y,int &w,int &h,int &orientation) {
    	x = mCropParams.x;
    	y = mCropParams.y;
    	w = mCropParams.w;
    	h = mCropParams.h;
    	orientation = mCropParams.orientation;
    }
    void RemoveCaptionLayout();
    void UpdateCaption();
    bool IsCaptionChanged();
    inline FriendTagging *GetTagging() { return mTagging; }
    void CreateTagging();
    void DeleteTagging();
};

/**
 * @brief This class keeps all information about text of photo that was created by user.
 */
class PostComposerMediaText {
private:
    char * mText;
    int mColorR;
    int mColorG;
    int mColorB;
    int mPositionX;
    int mPositionY;
    int mWidth;
    int mHeight;
    /**
     * @brief Uses for calculating zoom factor of text
     */
    int mOriginalWidth;
    char * mImagePath;
    double mAngle;

public:
    PostComposerMediaText(char * text, int colorR, int colorG, int colorB);
    virtual ~PostComposerMediaText();

    /**
     * @brief Set text
     */
    inline void SetText(char * text) { mText = text; }

    /**
     * @brief Get text
     */
    inline char * GetText() { return mText; }

    /**
     * @brief Set text object position
     *
     * @param[in] 'x' x position
     * @param[in] 'y' y position
     */
    inline void SetPosition(int x, int y) { mPositionX = x; mPositionY = y; }

    void GetPosition(int *x, int *y);

    /**
     * @brief Set text object size
     *
     * @param[in] 'w' text object width
     * @param[in] 'h' text object height
     */
    void SetSize(int w, int h);

    void GetSize(int *w, int *h);

    /**
     * @brief Calculate zoom factor for text
     *
     * @return Zoom factor
     */
    double GetZoomFactor();

    /**
     * @brief Get text object geometry
     *
     * @param[out] '*x' x position
     * @param[out] '*y' y position
     * @param[out] '*w' text object width
     * @param[out] '*h' text object height
     */
    void GetGeometry(int *x, int *y, int *w, int *h);

    /**
     * @brief Set text color
     *
     * @param[in] 'r' red component of text color
     * @param[in] 'g' green component of text color
     * @param[in] 'b' blue component of text color
     */
    inline void SetColor(int r, int g, int b) { mColorR = r; mColorG = g; mColorB = b; }
    /**
     * @brief Get text color
     *
     * @param[out] '*r' red component of text color
     * @param[out] '*g' green component of text color
     * @param[out] '*b' blue component of text color
     */
    void GetColor(int *r, int *g, int *b);

    /**
     * Path to text image png file
     */
    inline void SetImagePath(char * path) { mImagePath = path; }
    inline char * GetImagePath() { return mImagePath; }

    /**
     * @brief Sets angle of the text
     * @param[in] 'angle' angle of the text
     */
    inline void SetAngle(double angle) { mAngle = angle; }
    inline double GetAngle() { return mAngle; }
};

#endif /* POSTCOMPOSERMEDIADATA_H_ */
