#ifndef REQUESTUSERDATA_H_
#define REQUESTUSERDATA_H_

#include "Common.h"
#include "bundle.h"
#include "GraphRequest.h"

class GraphRequest;

class RequestUserData
{
public:

    RequestUserData(const char *RequestUrl, const char *MultipartFileUploadName
            ,bundle *ParamsBundle, bundle *BodyParamsBundle, GraphRequest::HttpMethod requestHttpMethod
            ,Sptr<GraphRequest> Request, bool IsHeaderReq, const char *EtagsHeader);
    RequestUserData(const char *RequestUrl, const char *RequestPayload, RequestUserData_CB callbackFunc, void *callbackData);
    virtual ~RequestUserData();

    //in
    const char *mRequestUrl;
    GraphRequest::HttpMethod   httpMethod;
    bool        mIsCancelled;
    const char *mMultipartFileUploadName;
    struct MemoryStruct *mRequestBuffer; // post parameters
    bundle     *mBodyParamsBundle;
    bool        mIsHeaderReq;
    const char *mThrdEtagsHeader;

    //out
    struct MemoryStruct* mResponseBuffer;
    CURLcode         curlCode;

    RequestUserData_CB callback;
    void       *callbackObject;

    Sptr<GraphRequest> mParentRequest;

protected:
    void Init();
};

#endif /* REQUESTUSERDATA_H_ */
