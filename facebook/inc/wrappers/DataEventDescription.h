#ifndef DATAEVENTDESCRIPTION_H_
#define DATAEVENTDESCRIPTION_H_

#include "Operation.h"

class AbstractDataProvider;

class DataEventDescription {
public:
    enum DataEventEnum {
        eDE_ITEM_ADDED,
        eDE_ITEM_REMOVED,
        eDE_POST_UPDATED,
        eDE_COMMENT_EDITED
    };
    DataEventDescription(AbstractDataProvider* Provider, DataEventEnum event, const char *itemId, bool addedToEnd = false, Operation::OperationType = Operation::OT_None);
    ~DataEventDescription();

    AbstractDataProvider* GetProvider() { return mProvider; }
    DataEventEnum GetDataEvent() { return mEvent; }
    const char* GetItemId() { return mItemId; }
    int GetAddedToEnd() { return mAddedToEnd; }
    inline Operation::OperationType GetOperationType() const { return mOperationType; }
private:
    AbstractDataProvider*  mProvider;
    DataEventEnum mEvent;
    char *mItemId;
    bool mAddedToEnd;
    Operation::OperationType mOperationType;
};

#endif /* DATAEVENTDESCRIPTION_H_ */
