/*
 * RequestContinueEvent.h
 *
 *  Created on: Dec 7, 2015
 *      Author: dvasin
 */

#ifndef REQUESTCONTINUEEVENT_H_
#define REQUESTCONTINUEEVENT_H_

#include "bundle.h"
#include "BaseObject.h"

class RequestContinueEvent : public BaseObject {
public:
    RequestContinueEvent(const char *request, bool isDescendingOrder, bundle *param = NULL);
    virtual ~RequestContinueEvent();

    char *mRequest;
    bool mIsDescendingOrder;
    bundle *mParam;
};



#endif /* REQUESTCONTINUEEVENT_H_ */
