/*
 * RequestCompleteEvent.h
 *
 */

#ifndef REQUESTCOMPLETEEVENT_H_
#define REQUESTCOMPLETEEVENT_H_

#include "BaseObject.h"

class RequestCompleteEvent:public BaseObject {
public:
    RequestCompleteEvent();
    int mCurlCode;
    int mResponseError;
};

#endif //REQUESTCOMPLETEEVENT_H_
