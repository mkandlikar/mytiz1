/*
 * ContactsDataBaseRecord.h
 *
 *  Created on: Jun 27, 2016
 *      Author: ruebasargi
 */

#ifndef CONTACTSDATABASERECORD_H_
#define CONTACTSDATABASERECORD_H_

#include <contacts.h>
#include <string>

namespace Contacts {
    /**
     * @class Record
     *
     * @exception DataBaseException
     *
     * @brief This class represents a record in Tizen contact data base.
     *
     * @details Record is an OOP-style wrapper around record related functions. It keeps record handle
     * and takes care of its life time. This wrapper provides limited abilities in comparison with low-level C API,
     * so it can't be considered as full replacement of native API, but it provides enough functionality for
     * Facebook application needs. In case if it would be not enough, don't shy to extend wrappers functionality.
     * Instances of Record class should work together with DataBase as all the changes performed over the records
     * should be applied by data base instance.
     */
    class Record {
        friend class DataBase;
    public:
        Record();
        Record(const char* viewUri);
        Record(const Record& record);
        Record(const contacts_record_h& recordHandle, const char* viewUri);
        ~Record();
        void operator=(const Record& record);
        void setFieldValue(unsigned int propertyId, int value);
        void setFieldValue(unsigned int propertyId, const std::string& value);
        int getIntegerValue(unsigned int propertyId);
        std::string getStringValue(unsigned int propertyId);
        void addChildRecord(unsigned int propertyId, const Record& childRecord);
    private:
        contacts_record_h mRecordHandle;
        std::string mViewUri;
    };
}

#endif /* CONTACTSDATABASERECORD_H_ */
