/*
 * ContactsDataBaseFilter.h
 *
 *  Created on: Jun 27, 2016
 *      Author: ruebasargi
 */

#ifndef CONTACTSDATABASEFILTER_H_
#define CONTACTSDATABASEFILTER_H_

#include <string>
#include <contacts.h>

namespace Contacts {
    /**
     * @class Filter
     *
     * @exception DataBaseException
     *
     * @brief This class represents a filter for Tizen contact data base.
     *
     * @details Filter is an OOP-style wrapper around filter related functions. It keeps filter handle
     * and takes care of its life time. This wrapper provides limited abilities in comparison with low-level C API,
     * so it can't be considered as full replacement of native API, but it provides enough functionality for
     * Facebook application needs. In case if it would be not enough, don't shy to extend wrappers' functionality.
     * Instances of Filter class should work together with DataBase as it input data for data base queries.
     */
    class Filter {
        friend class DataBase;
    public:
        Filter(const char* viewUri);
        ~Filter();
        void addStringValue(unsigned int propertyId, contacts_match_str_flag_e matchFlag, const char* matchValue);
        void addIntegerValue(unsigned int propertyId, contacts_match_int_flag_e matchFlag, int matchValue);
        void addOperator(contacts_filter_operator_e filterOperator);
    private:
        contacts_filter_h mFilterHandle;
        std::string mViewUri;
    };
}

#endif /* CONTACTSDATABASEFILTER_H_ */
