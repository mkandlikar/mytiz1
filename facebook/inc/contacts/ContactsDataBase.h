/*
 * ContactsDataBase.h
 *
 *  Created on: Jun 27, 2016
 *      Author: ruebasargi
 */

#ifndef CONTACTSDATABASE_H_
#define CONTACTSDATABASE_H_

#include <vector>
#include <contacts.h>
#include "ContactsDataBaseRecord.h"
#include "ContactsDataBaseFilter.h"
#include "ContactsException.h"
#include "Mutex.h"

namespace Contacts {
    typedef std::vector<Record> Records;
    /**
     * @class DataBase
     *
     * @exception DataBaseException
     *
     * @brief This class provides interface for Tizen contact data base.
     *
     * @details DataBase is an OOP-style wrapper around data base related functions. It keeps connection to the
     * contacts data base open while one or more instances of DataBase class exist. It closes the connection with destruction
     * of the last instance. The class is thread safe, more over it's recommended to use it in a service thread as it performs I/O operations.
     * This wrapper provides limited abilities in comparison with low-level C API, so it can't be considered as full replacement of native API,
     * but it provides enough functionality for Facebook application needs. In case if it would be not enough, don't shy to extend
     * wrappers' functionality.
     */
    class DataBase {
    public:
        DataBase();
        ~DataBase();
        int insertRecord(Record& record);
        Record getRecordById(const char* viewUri, int recordId);
        Record getRecordByField(const char* viewUri, int fieldId, int fieldValue);
        Record getRecordByField(const char* viewUri, int fieldId, const char* fieldValue);
        Records getRecordsByFilter(const Filter& filter);
        void updateRecord(const Record& record);
        void removeRecord(const Record& record, int recordId);
    private:
        static unsigned int mDbInstanceCount;
        static Mutex mMutex;
    };
}
#endif /* CONTACTSDATABASE_H_ */
