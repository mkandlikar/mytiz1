#ifndef FACEBOOKCONTACTSWRITER_H_
#define FACEBOOKCONTACTSWRITER_H_

#include <string>

class Friend;

namespace Contacts {
    /**
     * @class FacebookContactsWriter
     *
     * @brief FacebookContactsWriter is a high level interface for Tizen contact data base adapted to Facebook application needs.
     *
     * @details FacebookContactsWriter can be initialized in 2 ways:
     *  - with accoundId and addressBookName (that will cause re-initialization of the address book)
     *  - without any parameters (that will give opportunity to update existing records in the address book).
     * FacebookContactsWriter gives ability to perform 2 actions:
     *  - fulfill re-initialized address book with new contacts
     *  - update existing contacts with images that were downloaded later
     * FacebookContactsWriter works with I/O, so it would be a good idea to work with it in a service thread.
     *
     * @exception DataBaseException
     * FacebookContactsWriter throws DataBaseException exceptions in case of any error reported by contacts data base low level operation.
     */
    class FacebookContactsWriter {
    public:
        FacebookContactsWriter(int accountId, const std::string& addressBookName);
        FacebookContactsWriter();
        int writeFacebookContact(const Friend& contact);
        void updateFacebookContactWithImage(int contactId, const std::string& imagePath, const std::string& uid);
        void clearTheAddressBook();

    private:
        int getTheAddressBookId();
        int createTheAddressBook();
        int getBirthdayDateForDb(const std::string& fbBirthdayDate);
        std::string generateExtraDataForProfileRecord(const std::string& uid);

        int mAccountId;
        int mAddressBookId;
        std::string mAddressBookName;
    };
}

#endif /* FACEBOOKCONTACTSWRITER_H_ */
