/*
 * ContactsException.h
 *
 *  Created on: Jul 4, 2016
 *      Author: ruebasargi
 */

#ifndef CONTACTSEXCEPTION_H_
#define CONTACTSEXCEPTION_H_

#include <stdexcept>
#include <string>
#include <map>
#include "ErrorHandling.h"
#include "contacts.h"

namespace Contacts {
    /**
     * @class DataBaseException
     *
     * @extends std::exception
     *
     * @brief DataBaseException represents data base error object.
     *
     * @details DataBaseException is thrown by classes working with Tizen contact data base in of any error reported by low level api functions.
     * DataBaseException contains error message combined from operation description that lead to error, error code returned by the operation,
     * file name and line, where the error occurred.
     */
    class DataBaseException: public std::exception{
    public:
        DataBaseException(const std::string& operation, int errorCode, const std::string& fileName, int lineNumber);
        DataBaseException(const DataBaseException& exception) throw(): mErrorDescription(exception.mErrorDescription) {}
        virtual ~DataBaseException() throw (){}
        virtual const char* what() const throw();
        DataBaseException& operator=(const DataBaseException& exception) throw() {
            mErrorDescription = exception.mErrorDescription;
            return *this;
        }
    private:
        std::string getErrorString(int code);
        typedef std::map<int, std::string> ErrorMapping;
        static ErrorMapping initErrorCodeTable();
        std::string mErrorDescription;
    };
}

/**
 * @def CHECK_CONTACTS_OPERATION
 *
 * @brief Macro for checking Tizen contacts data base operation results and throwing DataBaseException exception.
 *
 * @exception DataBaseException
 *
 * @details CHECK_CONTACTS_OPERATION macro checks that contact data base operation returned CONTACTS_ERROR_NONE error code,
 * if the error code differs it creates new instance of DataBaseException, passes to it checked operation code as a string,
 * error code returned by checked operation, file name and file line where check failed.
 *
 */
#define CHECK_CONTACTS_OPERATION(OPERATION) {int operationResult = OPERATION; CHECK_THROW(operationResult == CONTACTS_ERROR_NONE, Contacts::DataBaseException(#OPERATION, operationResult, __FILE__, __LINE__));}

#endif /* CONTACTSEXCEPTION_H_ */
