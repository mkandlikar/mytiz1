/*
 * jsonutilities.h
 *
 *  Created on: Apr 16, 2015
 *      Author: RUINMKOL
 */

#ifndef JSONUTILITIES_H_
#define JSONUTILITIES_H_

#include <json-glib/json-glib.h>

#ifdef __cplusplus
extern "C"

{
#endif
    JsonParser* openJsonParser(const char * data, JsonObject ** object);
    char *GetStringFromJson(JsonObject *object, const char *member);
    gint64 GetGint64FromJson(JsonObject *object, const char *member);
#ifdef __cplusplus
}
#endif



#endif /* JSONUTILITIES_H_ */
