/*
 * DoubleSidedStack.h
 *
 *  Created on: Jul 6, 2015
 *      Author: Dmitry.Shcherbakov@SymphonyTeleca.com
 */

#ifndef DOUBLESIDEDSTACK_H_
#define DOUBLESIDEDSTACK_H_

#include "Common.h"

namespace Utils {

class DoubleSidedStack
{
public:
    typedef struct Node_ {
        Node_( void *item ) {
            prev = 0;
            next = 0;
            data = item;
        }
        struct Node_ *prev;
        struct Node_ *next;
        void *data;
    } Node;

    using ConstIterator = const Node*;
    using Iterator = Node*;

    DoubleSidedStack();
    virtual ~DoubleSidedStack();

    void PushToBegin( void *item);
    void PushToEnd( void *item );

    void* PopFromBegin();
    void* PopFromEnd();

    bool RemoveItem( void *itemToDelete, ItemsComparator comparator );
    bool IsPresentItem( void *itemToFind, ItemsComparator comparator );

    void Clear();

    void * GetItem( void *itemToFind, ItemsComparator comparator );
    /**
     * @brief Returns item from stack by index(from [0] to [Size - 1])
     */
    void * GetItemByIndex(int index);

    inline unsigned int GetSize() const {
        return m_Size;
    }

    inline ConstIterator Begin() const {
        return m_Head;
    }
    inline Iterator Begin() {
        return m_Head;
    }
    inline ConstIterator End() const {
        return m_Tail;
    }
    inline Iterator End() {
        return m_Tail;
    }
    inline ConstIterator Next(ConstIterator iter) const {
        return iter ? iter->next : nullptr;
    }
    inline Iterator Next(Iterator iter) {
        return iter ? iter->next : nullptr;
    }
private:
    Node *m_Head;
    Node *m_Tail;
    unsigned int m_Size;
};

} /* namespace Utils */

#endif /* DOUBLESIDEDSTACK_H_ */
