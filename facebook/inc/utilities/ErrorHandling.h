#ifndef ERRORHANDLING_H_
#define ERRORHANDLING_H_

#include "Log.h"
#include <cassert>

/**
 * @def CHECK_DO(EXPR, DO)
 *
 * @brief Base for CHECK macros
 *
 * @details CHECK_DO macro checks if EXPR equals true, in case it is false, it performs DO.
 * If you are not writing new CHECK macro you won't need it. Don't use CHECK macros for normal flow,
 * only for handling of errors!
 */
#define CHECK_DO(EXPR, DO) do{if(!(EXPR)){DO;}}while(false)

/**
 * @def LOG(EXPR, METHOD)
 *
 * @brief LOG macro adds entry in log of the application with specified Log class method.
 *
 * @details LOG macro adds entry in log of the application with specified Log class method when CHECK_RET or ASSERT macro detects error.
 * As parameter it accepts text of the expression that can't pass the check and Log class method name to use for print. The macro adds file name and line
 * number of the erroneous expression to the message.
 */
#define LOG(EXPR, METHOD) Log::METHOD("Expression `%s` didn't pass check in file `%s`, line %d", (EXPR), __FILE__, __LINE__)

/**
 * @def LOG_ERROR(EXPR)
 *
 * @brief LOG_ERROR macro adds entry in error log of the application.
 *
 * @details LOG_ERROR macro adds entry in error log of the application when CHECK_RET macro detects error.
 * As parameter it accepts text of the expression that can't pass the check. The macro adds file name and line
 * number of the erroneous expression to the message.
 */
#define LOG_ERROR(EXPR) LOG((EXPR), error)

/**
 * @def CHECK_DO_LOG(EXPR, DO)
 *
 * @brief Checks EXPR, in case of failure adds error log entry and performs DO.
 *
 * @details CHECK_DO_LOG macro checks if EXPR equals true, in case it is false, it adds entry in
 * the error log, containing information about expression that hasn't pass the check, file name and
 * line number to be able to identify where it occurred, then it performs DO.
 * If you are not writing new CHECK macro you won't need it. Don't use CHECK macros for normal flow,
 * only for handling of errors.
 */
#define CHECK_DO_LOG(EXPR, DO) CHECK_DO(EXPR, {LOG_ERROR(#EXPR); DO;})

/**
 * @def CHECK_RET(EXPR, RET)
 *
 * @brief Checks EXPR, in case of failure adds error log entry and returns RET value.
 *
 * @details CHECK_RET macro checks if EXPR equals true, in case it is false, it adds entry in
 * the error log, containing information about expression that hasn't pass the check, file name and
 * line number to be able to identify where it occurred, then it returns RET value.
 * If you are not writing new CHECK macro you won't need it. Don't use CHECK macros for normal flow,
 * only for handling of errors.
 */
#define CHECK_RET(EXPR, RET) CHECK_DO_LOG((EXPR), return RET)

/**
 * @def CHECK_RET_NRV(EXPR)
 *
 * @brief Checks EXPR, in case of failure adds error log entry and returns.
 *
 * @details CHECK_RET_NRV macro checks if EXPR equals true, in case it is false, it adds entry in
 * the error log, containing information about expression that hasn't pass the check, file name and
 * line number to be able to identify where it occurred, then it returns.
 * If you are not writing new CHECK macro you won't need it. Don't use CHECK macros for normal flow,
 * only for handling of errors.
 */
#define CHECK_RET_NRV(EXPR) CHECK_DO_LOG((EXPR), return)

/**
 * @def CHECK_THROW(EXPR, EXCEPT)
 *
 * @brief Checks EXPR, in case of failure it throws EXCEPT.
 *
 * @details CHECK_THROW macro checks if EXPR equals true, in case it is false, it adds entry in
 * the error log, containing information about expression that hasn't pass the check, file name and
 * line number to be able to identify where it occurred, then it throws EXCEPT.
 * If you are not writing new CHECK macro you won't need it. Don't use CHECK macros for normal flow,
 * only for handling of errors.
 */
#define CHECK_THROW(EXPR, EXCEPT) CHECK_DO((EXPR), throw EXCEPT;)

/**
 * @def CHECK_CONT(EXPR)
 *
 * @brief Checks EXPR, in case of failure it performs continue action.
 *
 * @details CHECK_CONT macro checks if EXPR equals true, in case it is false, it adds entry in
 * the error log, containing information about expression that hasn't pass the check, file name and
 * line number to be able to identify where it occurred, then it performs continue action.
 * If you are not writing new CHECK macro you won't need it. Don't use CHECK macros for normal flow,
 * only for handling of errors.
 */
#define CHECK_CONT(EXPR) if(!(EXPR)){LOG_ERROR(#EXPR); continue;}

/**
 * @def LOG_FATAL(EXPR)
 *
 * @brief LOG_FATAL macro adds entry in fatal error log of the application.
 *
 * @details LOG_ERROR macro adds entry in fatal error log of the application when ASSERT macro detects error.
 * As parameter it accepts text of the expression that can't pass the check. The macro adds file name and line
 * number of the erroneous expression to the message.
 */
#define LOG_FATAL(EXPR) LOG((EXPR), fatal)

/**
 * @def ASSERT_LOG
 *
 * @brief Debug assert
 *
 * @details This is debug assert which in case of error logs the place where error occurred and adds any text you want to the message.
 */
#define ASSERT_LOG(EXPR, LOG_ENTRY) do{bool result = (EXPR); if(!result){LOG_FATAL(#EXPR);Log::fatal((LOG_ENTRY));} assert(result);}while(false)

/**
 * @def ASSERT_LOG
 *
 * @brief Debug assert
 *
 * @details This is debug assert which in case of error logs the place where error occurred.
 */
#define ASSERT(EXPR) do{bool result = (EXPR); if(!result){LOG_FATAL(#EXPR);} assert(result);}while(false)

#endif /* ERRORHANDLING_H_ */
