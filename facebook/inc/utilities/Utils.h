#ifndef UTILS_H_
#define UTILS_H_

#include "Common.h"
#include "Location.h"
#include "Post.h"

#include <account.h>
#include <cairo.h>
#include <Elementary.h>
#include <libexif/exif-data.h>
#include <media_content.h>
#include <regex.h>
#include <storage.h>
#include <account.h>
#include <string>
#include <vector>
#include <utils_i18n.h>

namespace Utils {

enum BirthdayMonth {
    EJanuary,
    EFebruary,
    EMarch,
    EApril,
    EMay,
    EJune,
    EJuly,
    EAugust,
    ESeptember,
    EOctober,
    ENovember,
    EDecember
};

enum WeekDays {
    ESunday,
    EMonday,
    ETuesday,
    EWednesday,
    EThursday,
    EFriday,
    ESaturday,
};

    /**
     * @brief Gets current timestamp in milliseconds
     */
    double GetCurrentTime();

    long long GetCurrentTimeMicroSecs();
    /**
     * @brief Gets unix timestamp string
     */
    const char* GetUnixTime();
    time_t GetUnixTime_inSec();

    /**
     * Convert time from milliseconds to display extended format
     */
    char * PresentTimeFromMilliseconds(double time);

    /**
      * Convert time from milliseconds to display general format
      */
     char * PresentTimeFromMillisecondsGeneral(double time, bool needTimeShift);

     /**
       * Convert time from milliseconds to display in full format (e.g. 2015-05-14T21:00:00+0300)
       */
     char * PresentTimeFromMillisecondsFullFormat(double time, bool needTimeShift);

    /**
     * Gets turning info based on birthday and current date
     */
    char * GetTurningInfo(const char * birthday);

    /**
     * Gets birthday info in MM DD, YY format based on birthday and current date
     */
    char * GetNearestEventInfo(const char * event, bool isBirthday);

    char * GetNearestBdayInfo(const char *event, bool isFutureYear);

    int WeekDayForDate(int day, int month, int year);

    /**
     * Gets birthday info in YYMMDD format based on birthday and current date for date comparer
     */
    char * GetNearestBDay(const char * birthday);

    /**
     * Gets event day info in (Week day) MM DD, YY format based on current date
     */
    char * GetEventDay(const struct tm eventTime, bool showDay);

    /**
     * Gets Picked Event date and time from tm struct
     */
    const char * GetPickedTime(struct tm *currtime);

    /**
     * IGets Event's time
     */
    char * GetEventTime(const struct tm eventTime);

    /**
     * Gets event month name
     */
    char * GetEventMonth(unsigned int month);

    /**
     * Gets event week day name
     */
    const char * GetEventWeekDay(unsigned int day, bool isFullFormat);

    /**
     * Gets current Date and time in struct tm format
     */
    struct tm GetCurrentDateTime();

    char * GetEventDurationTime(const char * eventStartTime, const char * eventEndTime,
                                int * starts_in_hr);

    char * GetEventDetailedDurationTime(const char * eventStartTime, const char * eventEndTime);

    /**
     * Replace searchStr to replaceStr in str
     */
    std::string ReplaceString(std::string str, const std::string& searchStr, const std::string& replaceStr, unsigned int pos = 0, bool firstReplace = false);

    /**
     * Split string by substring
     */
    std::vector<std::string> SplitString(const std::string& splittedString, const std::string& splitter);

    /**
     * @brief    Indicates whether the 24-hour clock is used.
     * @retval  'false' in case of 12-hour, otherwise 'true'
     */
    bool IsTimeFormat24Hour();

    std::string ReplaceQuotation(std::string str);
    /**
     * Parse time from facebook response format to milliseconds
     * If timeString == NULL then get current time in milliseconds
     */
    double ParseTimeToMilliseconds(char *timeString);

    /**
     * Check if string contains "NULL" then return NULL. Else return pointer to this string.
     */
    char *GetStringOrNull(const char *string);

    /**
     * @brief - to get MD5 has of the string
     * @param str[in] - string for which MD5 hash to be calculated
     * @param len[in] - len of the string
     *
     * @return - MD5 hash string of str
     */
    char* str2md5(const char* str, int len);

    /**
     * Find Hashtag in text using regexp
     */
    int FindHashTagsInText(const char *text);


    /**
     * Find tagged friend in "@[1000009449351076:2048:Johnn Smith]" format in text using regexp
     */
    int FindTaggedFriendsInText(const char *text);

    /**
     * Find URL in text using regexp
     */
    int FindUrlInText(const char *text);

    char *FindUrlInText(const char *text, int occurance, bool needCut = false);

    /**
     * Find regExpText in text using regexp
     */
    int FindRegExpInText(const char *regExpText, const char *text);

    /**
     * Execute regular expression
     */
    int DoRegExp(regex_t *regex, const char *text);

    /**
     * Get string by start offset and end offset
     * return string from startOffset to endOffset-1
     */
    char * GetStringByOffset(const char * text, int startOffset, int endOffset);

    /**
     * Getting icon name from URL.
     * return icon name.
     */
    char* GetIconNameFromUrl(const char* url);
    char* GetImagePathFromUrl(const char* url);
    /**
     * Get static regmatch to find mathes after Find%HashTags/Url%InText or DoRegExp function calling
     */
    regmatch_t * GetRegmatch();

    /**
     * Make blur on the input evas_object_image
     */
    void ImageBlur(Evas_Object *evasObjectImage);

    /**
     * @brief Gets image color RGB be the x y position.
     *
     * @param[in] 'evasObjectImage' evas object of image
     * @param[in] 'x' x position
     * @param[in] 'y' y position
     * @param[out] 'r' red component of color at given position
     * @param[out] 'g' green component of color at given position
     * @param[out] 'b' blue component of color at given position
     *
     * @return false is x or y is more than width and height of source image. Else return true.
     */
    bool GetImageColorByXY(Evas_Object * evasObjectImage, int x, int y, int *r, int *g, int *b);

    /**
     * @brief Writes given surface to png. See cairo documentation.
     *
     * @param[in] 'surface' surface for writing to png.
     * @param[in] 'saveDirPath' path to directory to saving the png.
     *
     * @return Path to created png file
     *         NEED TO FREE RETURNED STRING!
     */
    char * SurfaceWriteToPng(cairo_surface_t * surface, const char * saveDirPath);

    /**
     * @brief Draws given text with color
     *
     * @param[in] 'text' Text to draw
     * @param[in] 'scaleFactor' Scale factor. Uses if not 0.0
     * @param[in] 'r' Red part of color
     * @param[in] 'g' Green part of color
     * @param[in] 'b' Blue part of color
     * @param[in] 'saveDirPath' path to directory to saving the png.
     *
     * @return Path to created png file
     *         NEED TO FREE RETURNED STRING!
     */
    char * DrawText(const char * text, double scaleFactor, int w, int h, int r, int g, int b, const char * saveDirPath);

    /**
     * @brief Draws given text with color on the given image
     *
     * @param[in] 'evasObjectImage' Evas object of image
     * @param[in] 'imageScaleFactor' Scale factor for image. Uses if not 0.0
     * @param[in] 'text' Text to draw
     * @param[in] 'textZoomFactor' Zoom factor for text.
     * @param[in] 'textPositionX' Text X position relation to the image
     * @param[in] 'textPositionY' Text Y position relation to the image
     * @param[in] 'r' Red part of color
     * @param[in] 'g' Green part of color
     * @param[in] 'b' Blue part of color
     * @param[in] 'saveDirPath' path to directory to saving the png.
     *
     * @return Path to created png file
     *         NEED TO FREE RETURNED STRING!
     */
    char * DrawTextOnImage(Evas_Object *evasObjectImage, double imageScaleFactor, const char * text, double textZoomFactor,
            int textPositionX, int textPositionY, int r, int g, int b, const char * saveDirPath);

    const char * GetLocale();
    const char * GetLocaleCode();
    void CropImageToFill(Evas_Object *evasObjectImage, int targetW, int targetH, int imageW, int imageH);

    /**
     * @brief prints the success or failure log message with the function name.
     */
    void AcctFuncRetValLog(const char *funcName, int ret);

    /**
     * @brief Selects media info from the media database.
     *
     * @param[in] filterCondition The media info selection filter
     * @param[in] callback  The callback function to be invoked
     * @param[in] user_data The user data to be passed to the callback function
     * @param[in] order The selection order
     * @param[in] collation The media content collation
     * @param[in] orderType The order type
     *
     * @return @c 0 on success,
     *         otherwise a negative error value
     *
     * @retval #MEDIA_CONTENT_ERROR_NONE Successful
     * @retval #MEDIA_CONTENT_ERROR_INVALID_PARAMETER Invalid parameter
     * @retval #MEDIA_CONTENT_ERROR_PERMISSION_DENIED Permission denied
     */
    int SelectMediaFilesFromMediaDB(const char * filterCondition, media_info_cb callback, void * user_data,
            const char * order = NULL, media_content_collation_e * collation = NULL,
            media_content_order_e * orderType = NULL);

    void SelectMediaFileFromMediaDBByPath(char *imagePath, media_info_cb callback, void * user_data);

    /**
     * @brief Inserts a media file into the media database.
     *
     * @param[in] path The path to the media file
     * @param[in] callback  The callback function to be invoked
     * @param[in] user_data The user data to be passed to the callback function
     *
     * @return @c 0 on success,
     *         otherwise a negative error value
     *
     * @retval #MEDIA_CONTENT_ERROR_NONE Successful
     * @retval #MEDIA_CONTENT_ERROR_INVALID_PARAMETER Invalid parameter
     * @retval #MEDIA_CONTENT_ERROR_PERMISSION_DENIED Permission denied
     */
    int InsertMediaFileToMediaDB(const char *path, media_info_cb callback, void * user_data);

    /**
     * @brief Deletes a media file from the media database.
     *
     * @param[in] mediaId The ID to the media file
     *
     * @return @c 0 on success,
     *         otherwise a negative error value
     *
     * @retval #MEDIA_CONTENT_ERROR_NONE Successful
     * @retval #MEDIA_CONTENT_ERROR_INVALID_PARAMETER Invalid parameter
     * @retval #MEDIA_CONTENT_ERROR_PERMISSION_DENIED Permission denied
     */
    int DeleteMediaFileFromDB(const char *mediaId);

    /**
     * @brief Gets the absolute path to the directory of the storage.
     *
     * @param[in] dirType The directory type
     *
     * @return The absolute path to the directory type
     * You must release path using free().
     */
    char * GetInternalStorageDirectory(storage_directory_e dirType);

    /**
     * @brief Gets the internal memory size in bytes.
     *
     * @return internal memory size
     */
    unsigned long long GetAvailableInternalMemorySize();

    /**
     * @brief Shows toast at short time period.
     *
     * @param[in] layout Layout on which toast will be show
     * @param[in] message Toast text
     */
    void ShowToast(Evas_Object * layout, const char * message);

    Evas_Object *ShowUpdatingToast(Evas_Object * layout, const char * message);
    void HideUpdatingToast(Evas_Object *obj);

    /**
     * @brief This method searches for the given element (see @ param object), within the given array (see @ param array)
     *
     * @param[in] object An object to be search for within the given array.
     * @param[in] array An array for search in.
     * @param[in] comparator A comparator for compare a couple of the given elements (an element to search and an element for search (within the given array)).
     *
     * @return index of element if object is in the array and -1 otherwise
     */
    int FindElement(void * object, Eina_Array * array, bool (*comparator) (const void *, const void *));

    /**
     * @brief This method compares given user id with id of current logged in user
     * @param[in] userId - the id of thr object that should be compared with current logged in user
     * @return true if given object id is equal to current logged in user id
     */
    bool IsMe(const char * userId);

    /**
     * @brief Quick sort algorithm
     *
     * @param[in] array What should be sorted
     * @param[in] low The first index of the array
     * @param[in] high The last index of the array
     * @param[in] descending The order of sorting
     * @param[in] user_strcmp The rule how to compare two objects during sorting
     *
     */
    void QuickSort(Eina_Array * array, int low, int high, bool descending, int (*user_strcmp) (const char *, const char *));

    /**
     * @brief Numeric compare method of positive values
     *
     * @param[in] a Element to be compared with b
     * @param[in] b Element to be compared with a
     *
     * @return int value depends on numbers length and returns of strcmp() function
     */
    int digits_strcmp(const char *a, const char *b);

    /**
     * @brief This function check if string is empty or contains only spaces
     * @return[in] true if string is NULL or empty or contains only spaces
     */
    bool isEmptyString(const char *st);

    /**
     * @brief This function set format
     * @return[in] format Pattern
     * @return[in] content
     *
     * @return[out] formatted char
     */
    char* WrapByFormatGenlist(const char *formatPattern, const char *content);

    /**
     * @brief This function set format
     * @return[in] format Pattern
     * @return[in] contentPart1
     * @return[in] contentPart2
     *
     * @return[out] formatted char
     */
    char* WrapByFormat2Genlist(const char *formatPattern, const char *contentPart1,
            const char *contentPart2);

    /**
     * @brief This function gets the user-agent header to be set in the requests that are sent to server
     * @return[out] Char string representing the UserAgent
     */
    const char *GetHTTPUserAgent();

    /**
     * @brief This function gets the mcc, mnc
     * @return[out] Success/Failure to retreive mcc, mnc
     */
    bool GetMCC_MNC(char **mcc, char **mnc);

    char* getCopy(const char* st);

    void FoundAndReplaceText(std::string &composer, const char *regExp, const char *repText);

    void ReplaceFoundTextInText(std::string &composer, int matchCount, const char *text);

    char *escape(const char* str);
    char *unescape(const char* str);

    std::string ExtractEtag(const char *header);

    bool IsHeader_Etag(const char *header);
    bool IsHeader_304NotModified(const char *header);

    int RemoveFolderRecursively(const char *path);

    i18n_uchar *UDup(const char*str);
    int UStrLen (const char*str);

    int UCompare(const i18n_uchar *uStr1, const i18n_uchar *uStr2, bool caseSensitive);
    int UCompare(const char*str1, const char*str2, bool caseSensitive);

    int UCompareN (const i18n_uchar *uStr1, const i18n_uchar *uStr2, int len, bool caseSensitive);
    int UCompareN (const char*str1, const char*str2, int len, bool caseSensitive);

    bool UIsSubstringOfAnyWord (const char*text, const char*substring, bool caseSensitive);
    bool UAreAllWordsSubstringsOfAnyWord(const char*text, const char*substrings, bool caseSensitive);
    bool IsSpace(char ch);
    char *Trim(const char*str);

    void GetThumbNailPathforUsingPathFilter(char *imagePath, char** thumbNailPath);

    /**
     * @brief Returns the number of %symbol% in %string%
     */
    int GetSymbolCount(char symbol, const char * string);

    /**
     * @brief Return length of n-line text
     *
     * @param message[in] text
     * @param nline[in] index of the finding line
     *
     * @return Length of n-line text if the count of message lines more than nline and 0 otherwise
     */
    unsigned int GetNLineLength(const std::string& message, unsigned int nline);

    int GetEXIFOrientation(const char *file_path);

    bool FileExists(const char *filePath);

    void SetKeyboardFocus(Evas_Object* EvasObject, Eina_Bool EnableFocus);

    const char* TextCutter(const char *text, int cutLength);

    enum EXIF_ORIENTATION
    {
        ORIENTATION_UNKNOWN=0,
        ORIENTATION_NORMAL,
        ORIENTATION_FLIP_HORIZONTAL,
        ORIENTATION_ROTATE_180,
        ORIENTATION_FLIP_VERTICAL,
        ORIENTATION_TRANSPOSE,
        ORIENTATION_ROTATE_90,
        ORIENTATION_TRANSVERSE,
        ORIENTATION_ROTATE_270
    };

    /**
     * @brief Searches the GraphObject in Providers by id.
     *
     * @param id - Id of object
     * @param object - founded object will set into this variable.
     *
     * @return object if found, otherwise returns NULL
     */
    GraphObject * FindGraphObjectInProvidersById(const char * id);

    /**
     * @brief This function creates evas_object_image and sets it into "part" of "layout". Then calls CropImageToFill.
     */
    void SetImageToEvasAndFill(Evas_Object * layout, const char * part, const char * fileName);
    void IncreaseTime(tm* time, int days, int hours, int minutes, int seconds);

    bool IsSameWeek(const tm *firstTime, const tm *secondTime);
    long int DiffInDays(const tm *firstTime, const tm *secondTime);

    char* UTF8TextCutter(const char *text, int cutLength, bool withDots = true);
    const char* GetUTF8TextSubstring(const char *str, int offsetLength, int cutLength);

    /* free allocated memory after using GetTimeShift() & GetCurrentTimeShift() */
    char* GetTimeShift(char* timeString);
    char* GetCurrentTimeShift();
    double ConvertTimeToGMT(char *gotString, bool needTimeShift);

    /*This function sets elm.guide part for obj entry with chosen formats
     * @param entry - entry object in which you want to set the elm.guide text
     * @param formatPattern - pattern for text, e.g. including font, font_size, etc.
     * @param contentPart1 - font size, which is passed to formatPattern
     * @param contentPart2 - the text itself, which is going to be displayed in entry as elm.guide
     * Note: elm.guide - is an entry text part (built-in) which displayed when entry is empty
     *  and disappears when user types smthn
     * */
    void SetEntryGuideText(Evas_Object *entry, const char *formatPattern, const char *contentPart1, const char *contentPart2);

    /* Create time with shift
     * @param time - Time without shift
     * @param needShortFormat - If we need time in base format = false
     * For short format (ex. "1 hr ago", "Just Now" = true
     *
     * @return Time with shift in const char*
     * */

    int GetLinesNumber(Evas_Object *entry);

    bool IsTimeShiftNeeded(char *time);
    const char *CreateTimeWithShift(char *time, bool needShortFormat = false);

    void OpenProfileScreen(std::string id);

    /* This one takes last characters from file_name, like .mkv
     * and compares them with FB supported formats
     * returns true if video file is supported and post composer should be opened */
    bool IsVideoFormat(const char *filePath);

    bool HasEnding(std::string &fullString, std::string &ending);
    bool HasEnding(const char *fullString, const char *ending);

    template <typename StringType >
    bool EndsWith(const StringType& str, const StringType& pattern)
    {
        typename StringType::const_reverse_iterator strIter = str.rbegin();
        for (typename StringType::const_reverse_iterator patternIter = pattern.rbegin(); patternIter != pattern.rend(); ++patternIter ) {
            if (strIter == str.rend()) {
                return false;
            }
            if (*patternIter == *strIter) {
                strIter++;
                continue;
            }
            else return false;
        }
        return true;
    }

    std::string FormatBigInteger(int count);
    bool CheckAccess(const char * path);
    bool IsPhoneNumber(char *number);
    char *CleanPhoneNumber(char *number);

    int Memcpy_s(void *dst, size_t sizeDst, const void *src, size_t sizeSrc);

    int Snprintf_s(char *buffer, size_t count, const char *format, ...);

    /**
     * @brief This function is for getting number
     * of pictures in post for edit in
     * format +<remaning number of pictures>
     * @return[out] char value
     */
    char * GetRemaningPicturesNumber(unsigned int count);

    void RedrawEvasObject(Evas_Object* objectToRedraw);

    char* ComputeHash(const char* data);
}
#endif /* UTILS_H_ */
