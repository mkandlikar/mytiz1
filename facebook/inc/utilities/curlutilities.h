/*
 * curlutilities.h
 *
 *  Created on: Apr 14, 2015
 *      Author: RUINMKOL
 */

#ifndef CURLUTILITIES_H_
#define CURLUTILITIES_H_

#include <curl/curl.h>
#include "stdbool.h"
#include "bundle.h"
#include "RequestUserData.h"

struct MemoryStruct
{
  char *memory; //Member to store the response received
  char *header;//Member to store the header received
  size_t memory_buffer_size;
  size_t size;
  size_t header_size;
};

#ifdef __cplusplus
extern "C"

{
#endif
    CURLcode SendHttpGet(RequestUserData *contextData);
    CURLcode SendHttpPost(RequestUserData *contextData);
    CURLcode SendHttpPostMultiPart(RequestUserData *contextData);
    CURLcode SendHttpDelete(RequestUserData *contextData);
    size_t write_data(void *contents, size_t size, size_t nmemb, void *userp);
    size_t header_callback(char *buffer,   size_t size,   size_t nitems,   void *userdata);
    int progress_callback(void *clientp,   curl_off_t dltotal,   curl_off_t dlnow,   curl_off_t ultotal,   curl_off_t ulnow);
#ifdef __cplusplus
}
#endif



#endif /* CURLUTILITIES_H_ */
