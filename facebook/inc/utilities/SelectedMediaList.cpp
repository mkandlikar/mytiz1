#include "SelectedMediaList.h"
#include "Log.h"

namespace SelectedMediaList {
    static const char* LogTag = "SelectedMediaList";
    static Eina_List *selectedMediaList = NULL;

    void RefreshIndexesFrom(int index) {
        Eina_List *list;
        void *selectedData;
        PostComposerMediaData *mediaData;
        EINA_LIST_FOREACH(eina_list_nth_list(selectedMediaList, index - 1), list, selectedData) {
            mediaData = static_cast<PostComposerMediaData *>(selectedData);
            mediaData->SetIndex(index);
            ++index;
        }
    }

    Eina_List* Get() {
        return selectedMediaList;
    }

    void AddItem(PostComposerMediaData *mediaData) {
        mediaData->SetIndex(eina_list_count(selectedMediaList) + 1);
        selectedMediaList = eina_list_append(selectedMediaList, mediaData);
    }

    unsigned int Count() {
        return eina_list_count(selectedMediaList);
    }

    bool IsAnyCaptionEdited() {
        bool isCaptionEdited = false;
        Eina_List *list;
        void *listData;
        PostComposerMediaData *media;

        EINA_LIST_FOREACH(Get(), list, listData) {
            media = static_cast<PostComposerMediaData *>(listData);

            if (media->IsCaptionChanged()) {
                isCaptionEdited = true;
                break;
            }
        }
        return isCaptionEdited;
    }

    void Clear() {
        void *listData;
        EINA_LIST_FREE(selectedMediaList, listData) {
            (static_cast<PostComposerMediaData *>(listData))->Release();
        }
    }

    void RemoveItem(PostComposerMediaData *mediaData) {
        int lastItemsCount = Count();
        // mediaData becomes invalid after release, so let's pick its index first
        int index = mediaData->GetIndex();

        selectedMediaList = eina_list_remove(selectedMediaList, mediaData);

        if (lastItemsCount > Count()) {
            mediaData->Release();
            RefreshIndexesFrom(index);
        }
    }

    void Print() {
        Log::debug_tag(LogTag, "Print");
        Eina_List *listItem;
        void *listData;
        EINA_LIST_FOREACH(selectedMediaList, listItem, listData) {
            PostComposerMediaData * mediaData = static_cast<PostComposerMediaData *>(listData);

            Log::debug_tag(LogTag, "index: %d, path: %s, selected: %s",
                    mediaData->GetIndex(), mediaData->GetFilePath(), mediaData->IsSelected() ? "true" : "false");
        }
    }
}
