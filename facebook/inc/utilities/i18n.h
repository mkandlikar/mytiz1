#ifndef I18N_H_
#define I18N_H_

#include "ErrorHandling.h"
#include <utils_i18n_types.h>
#include <string>

namespace i18n {

    using u16string = std::basic_string<i18n_uchar>;

    u16string StdStringToU16String(const std::string& src);

    static const char PATTERN[] = "%s";
    std::string Localize(const char* msgId);
    template <typename... Args>
    std::string Localize(const char* msgId, const std::string& arg, Args... args);
    template <typename... Args>
    std::string Localize(const char* msgId, const char* arg, Args... args);
    template <typename Arithmetic, typename... Args>
    std::string Localize(const char* msgId, Arithmetic arg, Args... args);


    template <typename... Args>
    std::string Localize(const char* msgId, const std::string& arg, Args... args) {
        ASSERT_LOG(msgId, "You've passed nullptr as message id for localization");
        auto msg(Localize(msgId, args...));
        auto position(msg.rfind(PATTERN));
        if(position != std::string::npos) {
            msg.replace(position, strlen(PATTERN), arg);
        }
        return msg;
    }

    template <typename... Args>
    std::string Localize(const char* msgId, const char* arg, Args... args) {
        ASSERT_LOG(msgId, "You've passed nullptr as message id for localization");
        auto msg(Localize(msgId, args...));
        auto position(msg.rfind(PATTERN));
        if(position != std::string::npos) {
            msg.replace(position, strlen(PATTERN), arg ? arg : "nullptr");
        }
        return msg;
    }

    template <typename Arithmetic, typename... Args>
    std::string Localize(const char* msgId, Arithmetic arg, Args... args) {
        ASSERT_LOG(msgId, "You've passed nullptr as message id for localization");
        auto msg(Localize(msgId, args...));
        auto position(msg.rfind(PATTERN));
        if(position != std::string::npos) {
            msg.replace(position, strlen(PATTERN), std::to_string(arg));
        }
        return msg;
    }

}

#endif /* I18N_H_ */
