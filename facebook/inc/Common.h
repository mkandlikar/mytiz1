#ifndef COMMON_H_
#define COMMON_H_

#include <stdbool.h>
#include <string.h>

#define UNUSED( val ) (void*)(val)

//#define _RELEASE_NO_PRINTS
#ifdef _RELEASE_NO_PRINTS
#define dlog_print(...)
#endif

// return true is items are equal
typedef bool (*ItemsComparator)( void *itemToCompare, void *itemForCompare );

typedef void (*RequestUserData_CB)(void *callback_object, char *respond, int code);

enum ProfileType {
    EProfileTypeUndecided,
    EProfileTypeInvalid,
    EProfileTypeNoContent,
    EProfileTypeNoInternet,
    EProfileTypeMe,
    EProfileTypeFriend,
    EProfileTypeNonFriendableFollowable,
    EProfileTypeNonFriendableNonFollowable,
    EProfileTypeOther
};

enum FriendRequestStatus {
    eUSER_REQUEST_UNKNOWN,
    eUSER_REQUEST_CONFIRMED,
    eUSER_REQUEST_DELETED,
    eUSER_REQUEST_SENT,
    eUSER_REQUEST_CANCELLED
};

//
// static definitions
static const unsigned int REGMATCH_ARRAY_SIZE = 1000;
static const unsigned int NUM_OF_ITEMS = 20;
static const unsigned int MAX_FILE_NAME_LENGTH = 57;
static const unsigned int MAX_FULL_PATH_NAME_LENGTH = 4096;
static const unsigned int CAMERA_ROLL_MAX_SELECTED_PHOTOS = 30;
static const unsigned int MAX_UA_LENGTH = 512;  /* actually UA strings for Z1,Z2,Z3,Z4 do not exceed 300 bytes */
static const unsigned int MAX_REQ_WEBURL_LEN = 1024;
static const int NO_ITEMS_FOUND = -1;
static const int MAX_COMMENT_MESSAGE_LENGTH = 8000;

//#define FEATURE_ADD_TEXT

static const char*  STR_APP_ID = "app_id=";
static const char*  STR_SESSION_KEY = "session_key=";
static const char*  STR_SIGNATURE = "sig=";
static const char*  STR_TIME = "t=";
static const char*  STR_USER_ID = "uid=";

static const char*  PRIVACY_EVERYONE = "EVERYONE";
static const char*  PRIVACY_ALL_FRIENDS = "ALL_FRIENDS";
static const char*  PRIVACY_SOME_FRIENDS = "SOME_FRIENDS";
static const char*  PRIVACY_FRIENDS_OF_FRIENDS = "FRIENDS_OF_FRIENDS";
static const char*  PRIVACY_SELF = "SELF";
static const char*  PRIVACY_CUSTOM = "CUSTOM";
static const char*  PRIVACY_CUSTOM_MANUAL_DENIED = "CUSTOM_MANUAL_DENIED";
static const char*  PRIVACY_CUSTOM_MANUAL_ALLOWED = "CUSTOM_MANUAL_ALLOWED";
static const char*  PRIVACY_FRIENDLIST = "FRIENDLIST";
static const char*  PRIVACY_FRIENDS_EXCEPT_ACQUAINTANCES = "FRIENDS_EXCEPT_ACQUAINTANCES";
static const char*  PRIVACY_GROUP_OPEN = "Open";
static const char*  PRIVACY_GROUP_CLOSED = "Closed";
static const char*  PRIVACY_GROUP_SECRET = "Secret";

static const char* PUSH_NOTIF_JSON_DATA = "NOTIFICATION_OBJECT_ID";
static const char* APP_CONTROL_CUSTOM_OPERATION_NOTIF_RCVD_TO_PROCESS = "NotificationReceivedToProcess";
static const char* APP_CONTROL_CUSTOM_OPERATION_PHOTO_VIDEO_UPLOAD = "photo_or_video_uploaded";
static const char* NOTIF_POST_ID = "NOTIFICATION_POST_ID";

// Literals for user-agent string
static const char* defPlatformVersion = "0.0.0.0";
static const char* defAppVersion = "0.0.0";
static const char* defModel = "Z3(Default)";
static const char* defMenufacturerName = "Samsung(Default)";
static const char* defPlatform = "Tizen(Default)";
static const char* defLocale ="en(Default)";
static const char* defMCC = "000";
static const char* defMNC = "000";

static const char* MODEL_NAME_Z1 = "SM-Z130H";
static const char* MODEL_NAME_Z2 = "SM-Z200F";
static const char* MODEL_NAME_Z3 = "SM-Z300H";

static const int CONTACT_SYNC_PERIODICITY = 86400;
static const int CONTACT_SYNC_TRIGGER_TIME = 60;

#define SAFE_STRDUP(str) ( str ? strdup(str) : nullptr )

#ifndef PACKAGE
#define PACKAGE "com.facebook.tizen"
#endif

#define DATASERVICE_NAME "com.facebook.dataservice"
#define APP_DIR "/opt/usr/apps/com.facebook.tizen"
#define ICON_DIR APP_DIR "/res/images"
#define SOUND_DIR APP_DIR "/res/sounds"
#define DIR_IMAGES_NAME "Images"
#define DIR_VIDEOS "Videos"
#define ICON_COMMON_DIR ICON_DIR "/Common"
#define ICON_SETTINGS_DIR ICON_DIR "/settingsscreenicons"
#define ICON_COMMENTS_DIR ICON_DIR "/CommentScreen"
#define ICON_FEED_PRIVACY_DIR ICON_DIR "/FeedScreen/Privacy"
#define ICON_EVENT_DIR ICON_DIR "/Events"

#define FACEBOOK_DEEP_BLUE_COLOR 59, 89, 152

#define POST_COMPOSER_ICON_DIR ICON_DIR "/PostComposer"
#define POST_COMPOSER_TEMP_DIR "PostComposerTemp"

#define LOG_TAG_FACEBOOK "Facebook"
#define LOG_TAG_FACEBOOK_FRIENDS "Facebook::FriendsAction"
#define LOG_TAG_FACEBOOK_EVENT "Facebook::Event"
#define LOG_TAG_ENTERING "Entering"

#define APP_EDJ_480P_FILE "edje/fbapp_480p.edj"
#define APP_EDJ_720P_FILE "edje/fbapp_720p.edj"
#define APP_EDJ_480P_THEMES_FILE "edje/themes_480.edj"
#define APP_EDJ_720P_THEMES_FILE "edje/themes_720.edj"

#define TRANSFER_PROTOCOL_PLAIN "http://"
#define TRANSFER_PROTOCOL_SECURE "https://"
#define TRANSFER_PROTOCOL_DEFAULT TRANSFER_PROTOCOL_SECURE

// Webview URLs
#define URL_ACCOUNT_SETTINGS "https://m.facebook.com/settings"
#define URL_PRIVACY_SHORTCUTS "https://m.facebook.com/privacy"
#define URL_HELP_CENTER "https://m.facebook.com/help"
#define URL_TERMS_N_POLICIES "https://m.facebook.com/policies"
#define URL_TARGET "https://m.facebook.com/auth.php?"
#define URL_SUGGESTED_GROUPS "https://m.facebook.com/groups/?category=top"
#define URL_EVENT_MAIN_PAGE "https://m.facebook.com/events"
#define URL_SAVED_FOLDER "https://m.facebook.com/saved"
#define URL_POKES_PAGE "https://m.facebook.com/pokes"
#define URL_EVENT_PROFILE_PAGE "https://m.facebook.com/events/%s"
#define URL_GROUP_LEAVE "https://m.facebook.com/group/leave/?group_id=%s"
#define URL_GROUP_INVITE "https://m.facebook.com/groups/members/search/?group_id=%s"
#define URL_GROUP_VIEWINFO "https://m.facebook.com/groups/%s?view=info"
#define URL_GROUP_MEMBER_REQUEST "https://m.facebook.com/groups/%s/madminpanel/requests/"
#define URL_HASHTAG_PAGE "https://m.facebook.com/hashtag/%s"
#define URL_NEARBY_EVENTS "https://m.facebook.com/events/nearby"

/* Common formats */
#define SANS_REGULAR_9298A4_FORMAT "<font_size=%s color=#9298a4>%s</>"
#define SANS_REGULAR_588FFF_FORMAT "<font_size=%s color=#588fff>%s</>"
#define SANS_REGULAR_5890ff_FORMAT "<font_size=%s color=#5890ff>%s</>"
#define SANS_REGULAR_000_FORMAT "<font_size=%s color=#000>%s</>"
#define SANS_REGULAR_BEC1C9_FORMAT "<font_size=%s color=#bec1c9>%s</>"
#define SANS_REGULAR_9fa2a6_FORMAT "<font_size=%s color=#9fa2a6>%s</>"
#define SANS_REGULAR_586c95_FORMAT "<font_size=%s color=#586c95>%s</>"
#define SANS_REGULAR_4e5665_FORMAT "<ellipsis=1.0 font_size=%s color=#4e5665>%s</>"
#define SANS_REGULAR_a0a3a7_FORMAT "<font_size=%s color=#a0a3a7>%s</>"
#define SANS_REGULAR_5890FE_FORMAT "<font_size=%s color=#5890fe>%s</>"
#define SANS_REGULAR_ffffff_FORMAT "<font_size=%s color=#ffffff>%s</>"

#define SANS_MEDIUM_3b5998_FORMAT "<font_size=%s color=#3b5998 align=center>%s%s</>"
#define SANS_MEDIUM_000_FORMAT "<font_size=%s color=#000>%s</>"
#define SANS_MEDIUM_000_FORMAT_BOLD "<b><font_size=%s color=#000>%s</></b>"
#define SANS_MEDIUM_fff_FORMAT "<font_size=%s color=#fff>%s</>"
#define SANS_MEDIUM_9197a3_FORMAT "<font_size=%s color=#9197a3>%s</>"
#define SANS_MEDIUM_666_FORMAT "<font_size=%s color=#666>%s</>"
#define SANS_MEDIUM_9298A4_FORMAT "<font_size=%s color=#9298a4>%s</>"
#define SANS_MEDIUM_BDC1C9_FORMAT "<font_size=%s color=#BDC1C9>%s</>"
#define SANS_MEDIUM_CENTER_9197a3_FORMAT "<font_size=%s color=#9197a3 align=center>%s</>"
#define SANS_MEDIUM_5f5f5f_FORMAT "<font_size=%s color=#5f5f5f>%s</>"
#define SANS_MEDIUM_CENTER_4e5665_FORMAT "<font_size=%s color=#4e5665 align=center>%s</>"
#define NO_MARGIN_FORMAT "<left_margin=0 right_margin=0>%s</>"
#define SANS_SHADOW_444444_FORMAT "<font_size=%s color=#fff style=shadow shadow_color=#444444>%s</>"
#define SANS_SHADOW_2f3f5c_FORMAT "<font_size=%s color=#6788c5 style=shadow shadow_color=#2f3f5c>%s</>"
#define SANS_9197a3_FORMAT "<font_size=%s color=#9197a3>%s %s</>"

/* Common styles */
#define SANS_REGULAR_BLACK_STYLE "DEFAULT='font_size=%s color=#000'"

#define GLOBAL_SEARCH_STYLE "DEFAULT='ellipsis=1.0 font_size=28 color=#fff'"
#define GLOBAL_SEARCH_GUIDE_FORMAT "<font_size=28 color=#aaa>%s</font>"

#define ADD_TEXT_TEXT_STYLE "DEFAULT='font_size=27 color=#fff'"

#define COMPOSER_ITEM_TAG_USER "<a rel=@[%s color=#5890ff>%s</a>"

#define FEED_SCREEN_BG_COLOR 219, 221, 226

#define FEED_ITEM_TITLE_FONT_OPEN "<font_size=21 color=#000 wrap=mixed>"
#define FEED_ITEM_TITLE_FONT_CLOSE "</font_size>"

#define FEED_ITEM_TITLE_FONT_2_OPEN "<font_size=28 color=#1e2330  wrap=mixed>"
#define FEED_ITEM_TITLE_FONT_2_CLOSE "</font_size>"

#define FEED_ITEM_TITLE_FONT_3_OPEN "<font_size=28 color=#000 wrap=mixed>"
#define FEED_ITEM_TITLE_FONT_3_CLOSE "</font_size>"

#define FEED_TITLE_ARROW "<font_size=21 color=#78839a wrap=mixed>%s</font_size>"

#define FEED_ITEM_TITLE_FONT_CLOSE "</font_size>"

#define FEED_ITEM_TITLE_NAME_FORMAT_2 \
        FEED_ITEM_TAG_PAGE\

#define COMMENT_FROM_LIKE \
        FEED_ITEM_TITLE_FONT_3_OPEN\
        FEED_ITEM_TAG_PAGE" "\
        FEED_ITEM_TITLE_FONT_3_CLOSE

#define FEED_ITEM_TITLE_LIKE_FORMAT "<font_size=27 color=#000 align=left wrap=mixed>%s liked this.</font_size>"
//#define FEED_ITEM_TITLE_LOCATION_FORMAT_3 "<font=TizenSans font_style=Medium font_size=21 color=#000 align=left wrap=mixed>%s <font=TizenSans font_style=Regular font_size=21 color=#1e2330>with</font_size> %s <font=TizenSans font_style=Regular font_size=21 color=#1e2330>in</font_size> %s </font_size>"
#define FEED_ITEM_TITLE_LOCATION_FORMAT_4 \
        FEED_ITEM_TITLE_FONT_3_OPEN\
        "%s "\
        FEED_ITEM_TITLE_FONT_2_OPEN "%s" FEED_ITEM_TITLE_FONT_2_CLOSE\
        " " FEED_ITEM_TAG_PAGE\
        FEED_ITEM_TITLE_FONT_3_CLOSE

#define FEED_ITEM_TITLE_LOCATION_FORMAT_5 \
        FEED_ITEM_TITLE_FONT_OPEN\
        "%s "\
          FEED_ITEM_TITLE_FONT_2_OPEN "%s" FEED_ITEM_TITLE_FONT_2_CLOSE\
          " %s "\
          FEED_ITEM_TITLE_FONT_2_OPEN "%s" FEED_ITEM_TITLE_FONT_2_CLOSE\
          " %s"\
        FEED_ITEM_TITLE_FONT_CLOSE

#define FEED_ITEM_TITLE_VIA \
        FEED_ITEM_TITLE_FONT_3_OPEN\
        FEED_ITEM_TAG_PAGE" "\
        FEED_ITEM_TITLE_FONT_3_CLOSE\
        FEED_ITEM_TITLE_FONT_2_OPEN "%s" FEED_ITEM_TITLE_FONT_2_CLOSE\
        " " FEED_ITEM_TITLE_FONT_3_OPEN\
        FEED_ITEM_TAG_PAGE\
        FEED_ITEM_TITLE_FONT_3_CLOSE

#define FEED_ITEM_TITLE_TO_FRIEND_FEED \
        FEED_ITEM_TITLE_FONT_3_OPEN\
        FEED_ITEM_TAG_PAGE\
        FEED_ITEM_TITLE_FONT_3_CLOSE\
        FEED_TITLE_ARROW\
        FEED_ITEM_TITLE_FONT_3_OPEN\
        FEED_ITEM_TAG_PAGE\
        FEED_ITEM_TITLE_FONT_3_CLOSE

#define COMMENT_AUTHOR_NAME \
        FEED_ITEM_TITLE_FONT_3_OPEN\
        FEED_ITEM_TAG_USER\
        FEED_ITEM_TITLE_FONT_3_CLOSE

#define FEED_ITEM_TAG_FORMAT_2 "<b><a href=%s>%s</a></b>"
#define FEED_ITEM_TAG_FORMAT_3 "<b><a href=%s_%s>%s</a></b>"
#define FEED_ITEM_TAG_PAGE "<b><a href=p_%s>%s</a></b>"
#define FEED_ITEM_TAG_USER "<b><a href=u_%s>%s</a></b>"
#define FEED_ITEM_TAG_USER_SRV "@[%s:%s]"

#define FEED_ITEM_DATE_FORMAT "<font_size=24 color=#BDC1C9 align=left ellipsis=1.0 wrap=none>%s</font_size>"
#define FEED_ITEM_TEXT_FORMAT "<font_size=21 color=#000>%s</font_size>"

#define FEED_ITEM_NEW_FRIEND_CAREER_FORMAT_1 "<font_size=18 align=left>%s</font_size>"
#define FEED_ITEM_NEW_FRIENDS_FORMAT_4 "%s <font_size=18 align=left>%s</font_size> %s <font_size=18 align=left>%s</font_size> %s"
#define FEED_ITEM_SHARED_POST_FORMAT_4 "%s <font_size=18 align=left>%s</font_size> %s <font_size=18 align=left>%s</font_size>"
#define FEED_ITEM_SONG_NAME_FORMAT_1 "<b>%s</b>"
#define FEED_ITEM_LIKED_SONG_FORMAT_3 "%s %s %s"
#define FEED_ITEM_SONG_ARTIST_FORMAT_1 "<font_size=18 align=left>%s</font_size>"
#define FEED_ITEM_CUT_MESSAGE_FORMAT_1  "<font_size=28 color=#bdc1c9><a href=read_more> %s </a></font>"
#define FEED_ITEM_COMMUNITY_NAME_FORMAT_2 "<b><a href=p_%s>%s</a></b>"
#define FEED_ITEM_COMMUNITY_TYPE_FORMAT_1 "<font_size=18><align=left>%s</align></font_size>"
#define FEED_ITEM_COMMUNITY_SUBSCRIBERS_FORMAT_1 "<font_size=18><align=left>%s</align></font_size>"
#define FEED_ITEM_MULTIPLE_LIKE_FORMAT_2 "%s %s"
#define FEED_ITEM_NAME_ANS_LIKED_FORMAT_2 "%s %s"
#define FEED_ITEM_COMMENT_TEXT_FORMAT_1 "<font_size=22>%s</font_size>"
#define FEED_ITEM_LOCATION_TYPE_FORMAT_1 "%s"
#define FEED_ITEM_LOCATION_VISITORS_FORMAT_1 "<font_size=22>%s</font_size>"
#define FEED_ITEM_ADDED_STHG_FORMAT_3 "%s %s <font_size=22>%s</font_size>"
#define FEED_ITEM_UNFOLLOW_SUBMENU_ITEM_FORMAT_2 "%s %s"

#define COMMENT_BAR_TIME_TEXT_FORMAT "<font_size=18 color=#9fa2a6>%s</font_size>"
#define COMMENT_BAR_BTNS_TEXT_FORMAT "<font_size=18 color=#586c95>%s</font_size>"
#define COMMENT_ITEM_WRITE_FORMAT "<font_size=24 color=#9298a4>%s</font_size>"
#define COMMENT_TEXT_STYLE "DEFAULT='font_size=24 color=#141823'"

//Note: the fix for b2373. <font> start tag, </font_size> end tag and a space between </b> and <font=TizenSans... > are a solution.
#define COMMENT_ITEM_REPLY_ITEM_FORMAT_5 "<b><font=TizenSans font_style=Medium font_size=%s color=#000><a href=u_%s>%s</a></font_size></b> <font=TizenSans font_style=Medium font_size=%s color=#666> %s</font_size>"

#define REPLY_TEXT_STYLE "DEFAULT='font=TizenSans:style=Regular font_size=24 color=#141823'"

#define FEED_ITEM_DESCRIPTION_TEXT_STYLE "DEFAULT='font_size=24 color=#9197a3'"
#define FEED_ITEM_DESCRIPTION_FORMAT_APP "<font_size=24 color=#9197a3 align=left> %s %s</font_size>"
#define FEED_ITEM_DESCRIPTION_FORMAT "<font_size=24 color=#9197a3 align=left>%s</font_size>"
#define FEED_ITEM_NAME_FORMAT "<font_size=24 color=#000 align=left>%s</font_size>"

#define FIND_FRIENDS_NAME_FORMAT "<font_size=21 color=#141823 align=left>%s</font_size>"
#define FIND_FRIENDS_MUTUALS_FORMAT "<font_size=18 color=#9298a4 align=left>%s</font_size>"
#define FIND_FRIENDS_SEARCH_FORMAT "<font_size=%s color=#9298a4>%s</font>"

#define MESSENGER_SCRN_FORMAT "<font_size=%s color=#141823FF align=center><b>%s</b></font>"
#define MESSENGER_APPLICATION_ID "com.facebook.Messenger"

#define CREATE_ALBUM_TEXT_FORMAT "<font_size=%s color=#9197a3>%s</>"

#define COMPOSER_SEARCH_TEXT_STYLE "DEFAULT='font_size=36 color=#000'"

#define PLACE_INFO_ITEM_FORMAT_3 "%s, %s %s"

#define POPUP_TITLE_FORMAT "<font_size=%s align=left color=#000>%s</>"
#define POPUP_MESSAGE_FORMAT "<font_size=%s align=left color=#000>%s</>"
#define POPUP_BTN1_FORMAT "<font_size=%s color=#000>%s</>"

#define REQ_APP_ID "app_id="
#define REQ_QUERY_STRING_DLIM "?"
#define REQ_MORE_PARAM_DLIM "&"
#define REQ_LOCALE "locale="
#define REQ_SESSION_KEY "session_key="
#define REQ_SIGNATURE "sig="
#define REQ_ENC "enc"
#define REQ_TIME "t="
#define REQ_USER_ID "uid="
#define REQ_URL "url="
#define REQ_AUTH_URL "https://m.facebook.com/auth.php?"
#define REQ_UPDATE_INFO_URI "https://m.facebook.com/profile.php?v=info"
#define REQ_MAX_LEN 1024

#define URL_FB_MWEB "https://m.facebook.com/"
#define URL_TERMS "terms.php"
#define URL_DATAPOLICY "policy.php"

#define FB_EVAS_HINT_DEFAULT 0.0
#define FB_BUF_SIZE 1024

#define TEN_MINUTES 600
#define AUTO_ROTATION_SETTING SYSTEM_SETTINGS_KEY_DISPLAY_SCREEN_ROTATION_AUTO
#define KEY_USER_LOGIN "user_login"

#define ANCHOR_FORMAT1 "<a href=%s>%s</a>"
#define ANCHOR_FORMAT2 "<a href=%s>%s %s</a>"
#define ANCHOR_FORMAT3 "<a href=%s>%s %s %s</a>"
#define ANCHOR_FORMAT4 "<a href=%s>%s %s %s %s</a>"

#endif /* COMMON_H_ */
