#ifndef APPLICATION_H_
#define APPLICATION_H_

#include "DataServiceProtocol.h"
#include "WidgetFactory.h"

#include <app.h>
#include <stdlib.h>
#include <system_settings.h>
#include <Elementary.h>
#include <efl_extension.h>
#include <efl_util.h>
#include <dlog.h>
#include <tizen.h>
#include <account.h>
#include <notification.h>

#include <package_manager.h>

class ScreenBase;
class PrivacyOptionsData;
class LocationManager;
class LogoutRequest;

//#define EVENT_NATIVE_VIEW


#define FRMTD_TRNSLTD_TXT_SET1(OBJ, FORMAT, TEXT1) Application::GetInstance()->TranslatedTextSet(OBJ, FORMAT, TEXT1)
#define FRMTD_TRNSLTD_TXT_SET2(OBJ, FORMAT, TEXT1, TEXT2) Application::GetInstance()->TranslatedTextSet(OBJ, FORMAT, TEXT1, TEXT2)
#define FRMTD_TRNSLTD_TXT_SET3(OBJ, FORMAT, TEXT1, TEXT2, TEXT3) Application::GetInstance()->TranslatedTextSet(OBJ, FORMAT, TEXT1, TEXT2, TEXT3)
#define FRMTD_TRNSLTD_TXT_SET4(OBJ, FORMAT, TEXT1, TEXT2, TEXT3, TEXT4) Application::GetInstance()->TranslatedTextSet(OBJ, FORMAT, TEXT1, TEXT2, TEXT3, TEXT4)
#define FRMTD_TRNSLTD_TXT_SET5(OBJ, FORMAT, TEXT1, TEXT2, TEXT3, TEXT4, TEXT5) Application::GetInstance()->TranslatedTextSet(OBJ, FORMAT, TEXT1, TEXT2, TEXT3, TEXT4, TEXT5)

#define FRMTD_TRNSLTD_PART_TXT_SET1(OBJ, PART, FORMAT, TEXT1) Application::GetInstance()->TranslatedPartTextSet(OBJ, PART, FORMAT, TEXT1)
#define FRMTD_TRNSLTD_PART_TXT_SET2(OBJ, PART, FORMAT, TEXT1, TEXT2) Application::GetInstance()->TranslatedPartTextSet(OBJ, PART, FORMAT, TEXT1, TEXT2)
#define FRMTD_TRNSLTD_PART_TXT_SET3(OBJ, PART, FORMAT, TEXT1, TEXT2, TEXT3) Application::GetInstance()->TranslatedPartTextSet(OBJ, PART, FORMAT, TEXT1, TEXT2, TEXT3)
#define FRMTD_TRNSLTD_PART_TXT_SET4(OBJ, PART, FORMAT, TEXT1, TEXT2, TEXT3, TEXT4) Application::GetInstance()->TranslatedPartTextSet(OBJ, PART, FORMAT, TEXT1, TEXT2, TEXT3, TEXT4)
#define FRMTD_TRNSLTD_PART_TXT_SET5(OBJ, PART, FORMAT, TEXT1, TEXT2, TEXT3, TEXT4, TEXT5) Application::GetInstance()->TranslatedPartTextSet(OBJ, PART, FORMAT, TEXT1, TEXT2, TEXT3, TEXT4, TEXT5)

#define HRSTC_TRNSLTD_TEXT_SET(OBJ, TEXT) Application::GetInstance()->HeuristicTranslatedTextSet(OBJ, TEXT)

#define HRSTC_TRNSLTD_PART_TEXT_SET(OBJ, PART, TEXT) Application::GetInstance()->HeuristicTranslatedPartTextSet(OBJ, PART, TEXT)

#define ELM_IMAGE_FILE_SET(OBJ, LTRPATH, RTLPATH) Application::GetInstance()->SetElmImageFile(OBJ, LTRPATH, RTLPATH)

#define ELM_BUTTON_STYLE_SET1(OBJ, LTRSTYLE) Application::GetInstance()->SetElmButtonStyle(OBJ, LTRSTYLE, NULL)
#define ELM_BUTTON_STYLE_SET2(OBJ, LTRSTYLE, RTLSTYLE) Application::GetInstance()->SetElmButtonStyle(OBJ, LTRSTYLE, RTLSTYLE)

#define DATE_TRNSLTD_PART_TEXT_SET(OBJ, PART, FORMAT, TIME) Application::GetInstance()->DateTranslatedTextSet(OBJ, PART, FORMAT, TIME)

#define TO_STORY_TRNSLTD_TEXT_SET(OBJ, TYPE, POST) Application::GetInstance()->ToStoryTranslatedTextSet(OBJ, TYPE, POST)

#define FRMTD_TRNSLTD_ENTRY_TXT(OBJ, FORMATLTR, FORMATRTL) Application::GetInstance()->FormattedTranslatedEntry(OBJ, FORMATLTR, FORMATRTL)

#define DLT_TRNSLTD_OBJ(OBJ) Application::GetInstance()->DeleteTranslatableObject(OBJ)

#ifdef DATA_SERVICE_TEST
class Application : public IDataServiceProtocolClient{
#else
    class Application {
#endif
Application();

class TranslatableObject {
public:
    TranslatableObject(Evas_Object *obj);
    virtual ~TranslatableObject() {};
    virtual void Update() = 0;
    virtual void UpdatePart(const char *name) {};
    inline Evas_Object *GetObject() {return mObject;}

protected:
    Evas_Object *mObject;
};

class TranslatableEntry: public TranslatableObject  {
public:
    TranslatableEntry(Evas_Object *obj, const char *formatLTR, const char *formatRTL);
    virtual ~TranslatableEntry();
    void Update() override;

private:
    char *mFormatLTR;
    char *mFormatRTL;
};

class TranslatableTextField : public TranslatableObject {
    class TranslatorBase {
    public:
        virtual ~TranslatorBase(){};
        virtual char *Translate() = 0;
    };

    class FormatTranslator : public TranslatorBase {
    public:
        FormatTranslator(const char *format, const char *text1, const char *text2, const char *text3, const char *text4, const char *text5);
        virtual ~FormatTranslator();

    private:
        char *Translate() override;

    private:
        char *mFormat;
        char *mText1;
        char *mText2;
        char *mText3;
        char *mText4;
        char *mText5;
    };


    class HeuristicTranslator : public TranslatorBase {
    public:
        HeuristicTranslator(const char *originalText);
        virtual ~HeuristicTranslator();

    private:
        char *mOriginalText;

    protected:
        void SetOriginalText(const char *originalText);
        char *Translate() override;
    };

    class DateTranslator: public HeuristicTranslator {

        const char *mFormat;
        double mTimeInMillisec;

        virtual ~DateTranslator();
        char *Translate() override;

    public:
        DateTranslator(const char *format, double timeInMillisec);
    };

    class ToStoryTranslator : public HeuristicTranslator {
        ~ToStoryTranslator() override;
        char * Translate() override;
        PresentableAsPost & mPost;
        PostItemTitleType mType;
    public :
        ToStoryTranslator(PostItemTitleType Type, PresentableAsPost &Post);
    };

    class Part {
    public:
        Part(Evas_Object &object, const char *name, TranslatorBase *translator);
        ~Part();
        inline const char *GetName() {return mName;}
        void SetTranslator(TranslatorBase *translator);
        void Update();

    private:
        Evas_Object &mObject;
        char *mName;
        TranslatorBase *mTranslator;
    };

public:
    TranslatableTextField(Evas_Object *obj);
    virtual ~TranslatableTextField();

    void SetText(const char *part, const char *format, const char *text1, const char *text2, const char *text3, const char *text4, const char *text5);
    void SetText(const char *part, const char *originalText);
    void SetText(const char *part, const char *format, double time);
    void SetText(PostItemTitleType Type, PresentableAsPost &Post);

    void Update() override;
    void UpdatePart(const char *name) override;

private:
    Part *GetPartByName(const char *name);
    void SetTranslator(const char *part, TranslatorBase *tr);

private:
    Eina_List *mParts;
};


class TranslatableImage : public TranslatableObject {
public:
    TranslatableImage(Evas_Object *obj);
    virtual ~TranslatableImage();

    void SetElmImageFile(const char *LTRPath, const char *RTLPath);
    void Update() override;

private:
    char *mRTLPath;
    char *mLTRPath;
};

class TranslatableButton : public TranslatableObject {
public:
    TranslatableButton(Evas_Object *obj);
    virtual ~TranslatableButton();

    void SetElmButtonStyle(const char *LTRStyle, const char *RTLStyle);
    void Update() override;

private:
    char *mLTRStyle;
    char *mRTLStyle;
};


public:
    static Application * GetInstance();
    virtual ~Application();

    virtual void Init();

    int Run(int argc, char *argv[]);

    void CreateBaseGUI();
    Evas_Object *CreateConform(Evas_Object *parent);

    void OnViewResize();
    void AddScreen(ScreenBase *screen);
    int GetTopScreenId();
    ScreenBase *GetTopScreen();
    /**
     * Replace current screen to target screen
     */
    void ReplaceScreen(ScreenBase * targetScreen);
    void RemoveTopScreen();// in fact removes top naviframe
    void MakeThisScreenTop(ScreenBase *screen);
    void RemoveAllScreensAndNfItemsFromNaviframe();
    void EraseAllProviderData();
    void DeleteAllData();

    void ReStartDataService();
    void StartDataService();

    LocationManager *GetLocationManager();
    void DeleteLocationManager();
    void RemoveOldPostComposerScreen();
    void RemovePostSimplePickerScreen();
    void SetScreenOrientationPortrait();
    void SetScreenOrientationBoth();
    void SetManualRotation();
    void EnableRotationForVideoScreen();
    void NotifyResumeForVideoScreen();

    bool HandleSuspendedAppControl();
    void GoToBackground();
    void ConfigureSoundManager();

    void TranslatedTextSet(Evas_Object *obj, const char *format, const char *text1 = nullptr, const char *text2 = nullptr, const char *text3 = nullptr, const char *text4 = nullptr, const char *text5 = nullptr);
    void TranslatedPartTextSet(Evas_Object *obj, const char *part, const char *format, const char *text1 = nullptr, const char *text2 = nullptr, const char *text3 = nullptr, const char *text4 = nullptr, const char *text5 = nullptr);

    void HeuristicTranslatedTextSet(Evas_Object *obj, const char *originalText);
    void HeuristicTranslatedPartTextSet(Evas_Object *obj, const char *part, const char *originalText);
    void DateTranslatedTextSet(Evas_Object *obj, const char *part, const char *format, double time);
    void ToStoryTranslatedTextSet(Evas_Object *obj, PostItemTitleType Type, PresentableAsPost &Post);
    void FormattedTranslatedEntry(Evas_Object *obj, const char *formatLTR, const char *formatRTL);

    void SetElmImageFile(Evas_Object *obj, const char *LTRPath, const char *RTLPath = nullptr);

    void SetElmButtonStyle(Evas_Object *obj, const char *LTRPath, const char *RTLPath = nullptr);

    inline const char *GetModelName() {return mModelName;}
    inline int GetPlatformVersion() {return mPlatformVersion;}

    static bool IsRTLLanguage();

    void DeleteTranslatableObject(Evas_Object *obj);
    ScreenBase *GetParentFeedScreen();
    void EnableHWBack();

    static bool app_create(void *data);
    static void app_control(app_control_h app_control, void *data);
    static void app_pause(void *data);
    static void app_resume(void *data);
    static void app_terminate(void *user_data);

    static void ui_app_lang_changed(app_event_info_h event_info, void *user_data);
    static void ui_app_orient_changed(app_event_info_h event_info, void *user_data);
    static void ui_app_region_changed(app_event_info_h event_info, void *user_data);
    static void ui_app_low_battery(app_event_info_h event_info, void *user_data);
    static void ui_app_low_memory(app_event_info_h event_info, void *user_data);

    static void win_delete_request_cb(void *data , Evas_Object *obj , void *event_info);
    static void win_back_cb(void *data, Evas_Object *obj, void *event_info);

    static void v_keypad_is_on(void *data, Evas_Object *obj, void *event_info);
    static void v_keypad_is_off(void *data, Evas_Object *obj, void *event_info);

    static void btn_cb(void *data, Evas_Object *obj, void *event_info);
    static bool account_read_cb(account_h account, void* user_data);

    static void view_size_reset(void *data, Evas *e, Evas_Object *obj, void *event_info);
    static void view_focused(void *data, Evas *e, void *event_info);
    static void view_unfocused(void *data, Evas *e, void *event_info);

    static void remove_main_layout_from_naviframe(void *data, Evas *e, Evas_Object *obj, void *event_info);

    void SetScreenMode(efl_util_screen_mode_e mode);
    static bool account_remove_cb(account_h account, void* data);
    static char mEdjPath[PATH_MAX];
    static char mThemeEdjPath[PATH_MAX];

    static int device_width;
    static int device_height;

    Evas_Object *mWin;
    Evas_Object *mConform;
    Evas_Object *mNf;
    DataServiceProtocol* mDataService;
    PrivacyOptionsData *mPrivacyOptionsData;
    LocationManager *mLocationManager;
    app_control_h mSuspendedAppControl;

private:
    void HandleAppControl(app_control_h app_control);
    bool DoHandleAppControl(app_control_h app_control);
    class ContactSynchronizer* mContactSynchronizer;
    static void run_messenger_app_cb( void *data );

    bool mNoUIMode;
    Ecore_Timer *mScreenModeTimer;
    static Eina_Bool set_default_mode_cb( void *data );

    Eina_Array * mScreenArray;
    Eina_List * mPackageManagerList;
    void AddPackageManagerListener(void);
    static void on_uninstall_me( const char *type, const char *package,
                                 package_manager_event_type_e event_type,
                                 package_manager_event_state_e event_state,
                                 int progress, package_manager_error_e error,
                                 void *user_data );

    static void on_clear_data( const char *type, const char *package,
                               package_manager_event_type_e event_type,
                               package_manager_event_state_e event_state,
                               int progress, package_manager_error_e error,
                               void *user_data );
    void RemovePackageManagerListener(void);
    void UpdateTranslations();
    TranslatableObject *GetTranslatableByObject(Evas_Object *obj);

#ifdef DATA_SERVICE_TEST
    virtual void HandleDownloadImageResponse(ErrorCode error, MessageId, const char* url, const char* fileName) const {return;}
#endif
    static void on_translatable_object_deleted(void *data, Evas *e, Evas_Object *obj, void *event_info);
    static Eina_Bool naviframe_pop_cb(void *data, Elm_Object_Item *it);
    void DoEmergencyLogout();

    const char *mModelName;
    int mPlatformVersion;
    Eina_List *mTranslatableFields;
    bool mIsHWBackCBAdded = false;
    LogoutRequest *mEmergencyLogoutRequest = nullptr;
};

#endif /* APPLICATION_H_ */
