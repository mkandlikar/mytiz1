#ifndef DATACONFIG_H_
#define DATACONFIG_H_

#include <string>

namespace Tables {
    namespace Settings {
        static std::string TABLE_NAME = "settings";

        static int COLUMNS_NUMBER = 3;
        namespace Columns {
            static std::string ID = "id";
            static std::string NAME = "name";
            static std::string VALUE = "value";
        }

        static std::string CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
            + "(" + Columns::ID + " INTEGER PRIMARY KEY AUTOINCREMENT"
            + "," + Columns::NAME + " TEXT NOT NULL"
            + "," + Columns::VALUE + " INTEGER"
            + ",UNIQUE(" + Columns::NAME + ") ON CONFLICT REPLACE" + ");";

        static std::string DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";


        namespace ValueName {
            static std::string DB_VERSION = "db_version";
            static std::string LAST_PRIVACY_OPTIONS_JSON = "last_privacy_options_response";
        }

    }

    namespace OutgoingFriendRequests {
        static std::string TABLE_NAME = "friendrequestsout";

        static int COLUMNS_NUMBER = 2;
        namespace Columns {
            static std::string ID = "id";
            static std::string NAME = "name";
        }
        static std::string CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
            + "(" + Columns::ID + " TEXT NOT NULL"
            + "," + Columns::NAME + " TEXT NOT NULL"
            + ",UNIQUE(" + Columns::ID + ") ON CONFLICT REPLACE" + ");";

        static std::string DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";
    }

    namespace FriendableUser {
        static std::string TABLE_NAME = "friendable_user";

        static int COLUMNS_NUMBER = 5;
        namespace Columns {
            static std::string ID = "id";
            static std::string NAME = "name";
            static std::string USER_NAME = "username";
            static std::string MUTUAL_FRIENDS = "mutual_friends";
            static std::string PIC_SQUARE_WITH_LOGO = "pic_square_with_logo";
        }

        static std::string CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
            + "(" + Columns::ID + " TEXT NOT NULL"
            + "," + Columns::NAME + " TEXT NOT NULL"
            + "," + Columns::USER_NAME + " TEXT NOT NULL"
            + "," + Columns::MUTUAL_FRIENDS + " INTEGER"
            + "," + Columns::PIC_SQUARE_WITH_LOGO + " TEXT"
            + ",UNIQUE(" + Columns::ID + ") ON CONFLICT REPLACE" + ");";

        static std::string DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";
    }

    namespace Friends {
        static std::string TABLE_NAME = "friends";

        static int COLUMNS_NUMBER = 9;
        namespace Columns {
            static std::string ID = "id";
            static std::string NAME = "name";
            static std::string MUTUAL_FRIENDS = "mutual_friends";
            static std::string PICTURE_PATH = "picture_path";
            static std::string FIRST_NAME = "first_name";
            static std::string LAST_NAME = "last_name";
            static std::string BIRTHDAY = "birthday";
            static std::string EMAIL = "email";
            static std::string MOBILE_PHONE = "mobile_phone";
        }

        static std::string CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
            + "(" + Columns::ID + " TEXT NOT NULL"
            + "," + Columns::NAME + " TEXT NOT NULL"
            + "," + Columns::MUTUAL_FRIENDS + " INTEGER"
            + "," + Columns::PICTURE_PATH + " TEXT"
            + "," + Columns::FIRST_NAME + " TEXT"
            + "," + Columns::LAST_NAME + " TEXT"
            + ",UNIQUE(" + Columns::ID + ") ON CONFLICT REPLACE" + ");";

        static std::string DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";
    }

    namespace Posts {
        static std::string TABLE_NAME = "posts";

        static int COLUMNS_NUMBER = 91;
        namespace Columns {
            static std::string AUTO_ID = "auto_id";
            static std::string ID = "id";
            static std::string CREATED_TIME = "created_time";
            static std::string UPDATED_TIME = "updated_time";
            static std::string CAPTION = "caption";
            static std::string DESCRIPTION = "description";
            static std::string FROM_NAME = "from_name";
            static std::string FROM_ID = "from_id";
            static std::string FROM_PICTURE = "from_picture";
            static std::string FULL_PICTURE = "full_picture";
            static std::string LINK = "link";
            static std::string MESSAGE = "message";
            static std::string MESSAGE_TAGS = "message_tags";
            static std::string OBJECT_ID = "object_id";
            static std::string PARENT_ID = "parent_id";
            static std::string PLACE_ID = "place_id";
            static std::string PLACE_NAME = "place_name";
            static std::string PLACE_CITY = "place_city";
            static std::string PLACE_COUNTRY = "place_country";
            static std::string PLACE_LATITUDE = "place_latitude";
            static std::string PLACE_LONGITUDE = "place_longitude";
            static std::string PLACE_STREET = "place_street";
            static std::string PLACE_CHECKINS = "checkins";
            static std::string PRIVACY_VALUE = "privacy_value";
            static std::string PRIVACY_DESCRIPTION = "privacy_description";
            static std::string PRIVACY_FRIENDS = "privacy_friends";
            static std::string PRIVACY_ALLOW = "privacy_allow";
            static std::string PRIVACY_DENY = "privacy_deny";
            static std::string SOURCE = "source";
            static std::string STATUS_TYPE = "status_type";
            static std::string STORY = "story";
            static std::string STORY_TAGS = "story_tags";
            static std::string TYPE = "type";
            static std::string LIKES_COUNT = "likes_count";
            static std::string COMMENTS_COUNT = "comments_count";
            static std::string PHOTO_COUNT = "photo_count";
            static std::string ADDED_STHG = "added_sthg";
            static std::string CHILD_ID = "child_id";
            static std::string COMMENT = "comment";
            static std::string COMMUTITY_AVATAR = "community_avatar";
            static std::string COMMUTITY_NAME = "community_name";
            static std::string COMMUTITY_SUBSCRIBERS = "community_subscribers";
            static std::string COMMUTITY_TYPE = "community_type";
            static std::string COMMUTITY_WALLPAPER = "community_wallpaper";
            static std::string EVENT_COVER = "event_cover";
            static std::string EVENT_INFO = "event_info";
            static std::string FROM_PICTURE_PATH = "from_picture_path";
            static std::string LOCATION_AVATAR_PATH = "location_avatar_path";
            static std::string PLACE_TYPE = "place_type";
            static std::string PLACE_VISITORS = "place_visitors";
            static std::string NAME = "name";
            static std::string PLACE_PICTURE = "place_picture";
            static std::string SONG_ARTIST = "song_artist";
            static std::string SONG_NAME = "song_name";
            static std::string NEW_FRIEND_ID = "new_friend_id";
            static std::string NEW_FRIEND_AVATAR = "new_friend_avatar";
            static std::string NEW_FRIEND_CAREER = "new_friend_career";
            static std::string NEW_FRIEND_MUTAL_FRIENDS = "new_friend_mutal_friends";
            static std::string NEW_FRIEND_WORK = "new_friend_work";
            static std::string NEW_FRIEND_EDUCATION = "new_friend_education";
            static std::string NEW_FRIEND_COVER = "new_friend_cover";
            static std::string CAN_LIKE = "can_like";
            static std::string HAS_LIKED = "has_liked";
            static std::string CAN_COMMENT = "can_comment";
            static std::string CAN_SHARE = "can_share";
            static std::string TO_NAME = "to_name";
            static std::string TO_ID = "to_id";
            static std::string TO_PROFILE_TYPE = "to_profile_type";
            static std::string VIA_ID = "via_id";
            static std::string VIA_NAME = "via_name";
            static std::string GROUP_NAME = "group_name";
            static std::string GROUP_STATUS = "group_status";
            static std::string GROUP_ID = "group_id";
            static std::string LIKE_FROM_NAME = "like_from_name";
            static std::string LIKE_FROM_ID = "like_from_id";
            static std::string ATTACHMENT_DESCRIPTION = "attachment_description";
            static std::string ATTACHMENT_SRC = "attachment_src";
            static std::string ATTACHMENT_HEIGHT = "attachment_height";
            static std::string ATTACHMENT_WIDTH = "attachment_width";
            static std::string ATTACHMENT_TARGET_ID = "attachment_target_id";
            static std::string ATTACHMENT_TARGET_TITLE = "attachment_target_title";
            static std::string ATTACHMENT_TARGET_URL = "attachment_target_url";
            static std::string WITH_TAGS_COUNT = "with_tags_count";
            static std::string WITH_TAGS_NAME = "with_tags_name";
            static std::string WITH_TAGS_ID = "with_tags_id";
            static std::string IMPLICIT_PLACE_ID = "imp_place_id";
            static std::string IMPLICIT_PLACE_NAME = "imp_place_name";
            static std::string IMPLICIT_PLACE_CITY = "imp_place_city";
            static std::string IMPLICIT_PLACE_COUNTRY = "imp_place_country";
            static std::string IMPLICIT_PLACE_LATITUDE = "imp_place_latitude";
            static std::string IMPLICIT_PLACE_LONGITUDE = "imp_place_longitude";
            static std::string IMPLICIT_PLACE_STREET = "imp_place_street";

        }
        static std::string TABLE_FIELDS = Columns::AUTO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT"
            + "," + Columns::ID + " TEXT NOT NULL"
            + "," + Columns::CREATED_TIME + " TEXT"
            + "," + Columns::UPDATED_TIME + " TEXT"
            + "," + Columns::CAPTION + " TEXT"
            + "," + Columns::DESCRIPTION + " TEXT"
            + "," + Columns::FROM_NAME + " TEXT"
            + "," + Columns::FROM_ID + " TEXT"
            + "," + Columns::FROM_PICTURE + " TEXT"
            + "," + Columns::FULL_PICTURE + " TEXT"
            + "," + Columns::LINK + " TEXT"
            + "," + Columns::MESSAGE + " TEXT"
            + "," + Columns::MESSAGE_TAGS + " TEXT"
            + "," + Columns::OBJECT_ID + " TEXT"
            + "," + Columns::PARENT_ID + " TEXT"
            + "," + Columns::PLACE_ID + " TEXT"
            + "," + Columns::PLACE_NAME + " TEXT"
            + "," + Columns::PLACE_CITY + " TEXT"
            + "," + Columns::PLACE_COUNTRY + " TEXT"
            + "," + Columns::PLACE_LATITUDE + " REAL"
            + "," + Columns::PLACE_LONGITUDE + " REAL"
            + "," + Columns::PLACE_STREET + " TEXT"
            + "," + Columns::PLACE_CHECKINS + " INTEGER"
            + "," + Columns::PRIVACY_VALUE + " TEXT"
            + "," + Columns::PRIVACY_DESCRIPTION + " TEXT"
            + "," + Columns::PRIVACY_FRIENDS + " TEXT"
            + "," + Columns::PRIVACY_ALLOW + " TEXT"
            + "," + Columns::PRIVACY_DENY + " TEXT"
            + "," + Columns::SOURCE + " TEXT"
            + "," + Columns::STATUS_TYPE + " TEXT"
            + "," + Columns::STORY + " TEXT"
            + "," + Columns::STORY_TAGS + " TEXT"
            + "," + Columns::TYPE + " TEXT"
            + "," + Columns::LIKES_COUNT + " INTEGER"
            + "," + Columns::COMMENTS_COUNT + " INTEGER"
            + "," + Columns::PHOTO_COUNT + " INTEGER"
            + "," + Columns::ADDED_STHG + " TEXT"
            + "," + Columns::CHILD_ID + " TEXT"
            + "," + Columns::COMMENT + " TEXT"
            + "," + Columns::COMMUTITY_AVATAR + " TEXT"
            + "," + Columns::COMMUTITY_NAME + " TEXT"
            + "," + Columns::COMMUTITY_SUBSCRIBERS + " TEXT"
            + "," + Columns::COMMUTITY_TYPE + " TEXT"
            + "," + Columns::COMMUTITY_WALLPAPER + " TEXT"
            + "," + Columns::EVENT_COVER + " TEXT"
            + "," + Columns::EVENT_INFO + " TEXT"
            + "," + Columns::FROM_PICTURE_PATH + " TEXT"
            + "," + Columns::LOCATION_AVATAR_PATH + " TEXT"
            + "," + Columns::PLACE_TYPE + " TEXT"
            + "," + Columns::PLACE_VISITORS + " TEXT"
            + "," + Columns::NAME + " TEXT"
            + "," + Columns::PLACE_PICTURE + " TEXT"
            + "," + Columns::SONG_ARTIST + " TEXT"
            + "," + Columns::SONG_NAME + " TEXT"
            + "," + Columns::NEW_FRIEND_ID  + " TEXT"
            + "," + Columns::NEW_FRIEND_AVATAR  + " TEXT"
            + "," + Columns::NEW_FRIEND_CAREER  + " TEXT"
            + "," + Columns::NEW_FRIEND_MUTAL_FRIENDS  + " TEXT"
            + "," + Columns::NEW_FRIEND_WORK  + " TEXT"
            + "," + Columns::NEW_FRIEND_EDUCATION  + " TEXT"
            + "," + Columns::NEW_FRIEND_COVER + " TEXT"
            + "," + Columns::CAN_LIKE  + " INTEGER"
            + "," + Columns::HAS_LIKED + " INTEGER"
            + "," + Columns::CAN_COMMENT + " INTEGER"
            + "," + Columns::CAN_SHARE + " INTEGER"
            + "," + Columns::TO_NAME + " TEXT "
            + "," + Columns::TO_ID + " TEXT "
            + "," + Columns::VIA_ID + " TEXT "
            + "," + Columns::VIA_NAME + " TEXT "
            + "," + Columns::GROUP_NAME + " TEXT "
            + "," + Columns::GROUP_STATUS + " TEXT "
            + "," + Columns::GROUP_ID + " TEXT "
            + "," + Columns::LIKE_FROM_NAME + " TEXT "
            + "," + Columns::LIKE_FROM_ID + " TEXT "
            + "," + Columns::ATTACHMENT_DESCRIPTION + " TEXT "
            + "," + Columns::ATTACHMENT_SRC + " TEXT "
            + "," + Columns::ATTACHMENT_HEIGHT + " INTEGER "
            + "," + Columns::ATTACHMENT_WIDTH + " INTEGER "
            + "," + Columns::ATTACHMENT_TARGET_ID + " TEXT "
            + "," + Columns::ATTACHMENT_TARGET_TITLE + " TEXT "
            + "," + Columns::ATTACHMENT_TARGET_URL + " TEXT "
            + "," + Columns::WITH_TAGS_COUNT + " INTEGER "
            + "," + Columns::WITH_TAGS_NAME + " TEXT "
            + "," + Columns::WITH_TAGS_ID + " TEXT"
            + "," + Columns::IMPLICIT_PLACE_ID + " TEXT"
            + "," + Columns::IMPLICIT_PLACE_NAME + " TEXT"
            + "," + Columns::IMPLICIT_PLACE_CITY + " TEXT"
            + "," + Columns::IMPLICIT_PLACE_COUNTRY + " TEXT"
            + "," + Columns::IMPLICIT_PLACE_LATITUDE + " REAL"
            + "," + Columns::IMPLICIT_PLACE_LONGITUDE + " REAL"
            + "," + Columns::IMPLICIT_PLACE_STREET + " TEXT"
            + ",UNIQUE(" + Columns::ID + ") ON CONFLICT REPLACE";

        static std::string CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME +
               + "(" + TABLE_FIELDS + ");";

        static std::string DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";
    }

    namespace Photos {
        static std::string TABLE_NAME = "photos";

        static int COLUMNS_NUMBER = 6;
        namespace Columns {
            static std::string POST_ID = "post_id";
            static std::string SOURCE = "source";
            static std::string PHOTO_NAME = "photo_name";
            static std::string WIDTH = "width";
            static std::string HEIGHT = "height";
            static std::string POST_AUTO_ID = "post_auto_id";

        }
        static std::string CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
            + "(" + Columns::POST_ID + " TEXT NOT NULL"
            + "," + Columns::SOURCE + " TEXT NOT NULL"
            + "," + Columns::PHOTO_NAME + " TEXT NOT NULL"
            + "," + Columns::WIDTH + " INTEGER"
            + "," + Columns::HEIGHT + " INTEGER"
            + "," + Columns::POST_AUTO_ID + " INTEGER NOT NULL"
            + "," + " FOREIGN KEY(" + Columns::POST_AUTO_ID + ") REFERENCES posts( auto_id )" + ");";
        static std::string DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";
    }

    namespace Sessions {
        static std::string TABLE_NAME = "sessions";

        static int COLUMNS_NUMBER = 3;
        namespace Columns {

            static std::string SESSION_AUTO_ID = "session_auto_id";
            static std::string SESSION_START_DATE = "start_date";
            static std::string SESSION_END_DATE = "end_date";

        }
        static std::string CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
            + "(" + Columns::SESSION_AUTO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT"
            + "," + Columns::SESSION_START_DATE + " TEXT NOT NULL"
            + "," + Columns::SESSION_END_DATE + " TEXT" + ");";
        static std::string DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";
    }

    namespace PostOrder {
        static std::string TABLE_NAME = "post_order";

        static int COLUMNS_NUMBER = 3;
        namespace Columns {

            static std::string POSITION_ID = "position_id";
            static std::string POST_AUTO_ID = "post_auto_id";
            static std::string SESSION_AUTO_ID = "session_auto_id";

        }
        static std::string CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
            + "(" + Columns::POSITION_ID + " INTEGER NOT NULL"
            + "," + Columns::POST_AUTO_ID + " INTEGER NOT NULL"
            + "," + Columns::SESSION_AUTO_ID + " INTEGER NOT NULL"
            + "," + " FOREIGN KEY(" + Columns::POST_AUTO_ID + ") REFERENCES posts( auto_id )"
            + "," + " FOREIGN KEY(" + Columns::SESSION_AUTO_ID + ") REFERENCES sessions( session_auto_id )" + ");";
        static std::string DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";
    }

    namespace Accounts {
        static std::string TABLE_NAME = "accounts";

        static int COLUMNS_NUMBER = 5;
        namespace Columns {
            static std::string USER_NAME = "user_name";
            static std::string USER_ID = "user_id";
            static std::string ACCESS_TOKEN = "access_token";
            static std::string SESSION_KEY = "session_key";
            static std::string SESSION_SECRET = "session_secret";
        }

        static std::string CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
            + "(" + Columns::USER_NAME + " TEXT NOT NULL"
            + "," + Columns::USER_ID + " TEXT NOT NULL"
            + "," + Columns::ACCESS_TOKEN + " TEXT NOT NULL"
            + "," + Columns::SESSION_KEY + " TEXT NOT NULL"
            + "," + Columns::SESSION_SECRET + " TEXT NOT NULL"
            + ",UNIQUE(" + Columns::USER_NAME + ") ON CONFLICT REPLACE" + ");";

        static std::string DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";
    }

    namespace Notifications {
        static std::string TABLE_NAME = "notifications";

        static int COLUMNS_NUMBER = 7;
        namespace Columns {
            static std::string NOTIFICATION_ID = "notification_id";
            static std::string NOTIFICATION_MSG = "notification_msg";
            static std::string USER_NAME = "user_name";
            static std::string IMAGE_PATH = "image_path";
            static std::string NOTIF_RCVD_TIME = "notif_rcvd_time";
            static std::string NOTIFICATION_TYPE = "notification_type";
            static std::string NOTIF_GRAPH_OBJECT = "notif_graph_object";
        }

        static std::string CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
            + "(" + Columns::NOTIFICATION_ID + " TEXT NOT NULL"
            + "," + Columns::NOTIFICATION_MSG + " TEXT NOT NULL"
            + "," + Columns::USER_NAME + " TEXT NOT NULL"
            + "," + Columns::IMAGE_PATH + " TEXT NOT NULL"
            + "," + Columns::NOTIF_RCVD_TIME + " TEXT NOT NULL"
            + "," + Columns::NOTIFICATION_TYPE + " TEXT NOT NULL"
            + "," + Columns::NOTIF_GRAPH_OBJECT + " TEXT NOT NULL"
            + ",UNIQUE(" + Columns::NOTIFICATION_ID + ") ON CONFLICT REPLACE" + ");";

        static std::string DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";
    }

    namespace LoggedInUser {
        static std::string TABLE_NAME = "loggedinuser";

        static int COLUMNS_NUMBER = 1;
        namespace Columns {
            static std::string USER_ID = "user_id";
        }

        static std::string CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
            + "(" + Columns::USER_ID + " TEXT NOT NULL" + ");";

        static std::string DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";
    }

    namespace ProfileFeed {
        static std::string TABLE_NAME = "profilefeedposts";

        static std::string CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME +
               + "(" + Posts::TABLE_FIELDS + ");";

        static std::string DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";
    }

}

#endif /* DATACONFIG_H_ */
