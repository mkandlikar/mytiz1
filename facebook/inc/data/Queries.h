#ifndef QUERIES_H_
#define QUERIES_H_

#include <string>

namespace Queries {
    namespace SelectFriendsRequestOut {
        static std::string QUERY_TEXT = "SELECT * FROM friendrequestsout WHERE id = \'%s\';";
        static std::string DELETE_ROW = "DELETE FROM friendrequestsout WHERE id = \'%s\';";

        static int ID = 0;
        static int NAME = 1;
    }


    namespace SelectFriendableUser {
        static std::string QUERY_TEXT = "SELECT * FROM friendable_user LIMIT %d;";

        static int ID = 0;
        static int NAME = 1;
        static int USER_NAME = 2;
        static int MUTUAL_FRIENDS = 3;
        static int PIC_SQUARE_WITH_LOGO = 4;
    }

    namespace DeleteAllFriendableUser {
        static std::string QUERY_TEXT = "DELETE FROM friendable_user;";
    }

    namespace SelectNFriends {
        static std::string QUERY_TEXT = "SELECT * FROM friends;";
        static std::string DELETE_FRIEND = "DELETE FROM friends WHERE id = \'%s\';";

        static int ID = 0;
        static int NAME = 1;
        static int MUTUAL_FRIENDS = 2;
        static int PICTURE_PATH = 3;
        static int FIRST_NAME = 4;
        static int LAST_NAME = 5;
        static int BIRTHDAY = 6;
        static int EMAIL = 7;
        static int MOBILE_PHONE = 8;
    }

    namespace SelectNPosts {
        static std::string QUERY_TEXT = "SELECT * FROM posts LIMIT %d;";

        static int AUTO_ID = 0;
        static int ID = 1;
        static int CREATED_TIME = 2;
        static int UPDATED_TIME = 3;
        static int CAPTION = 4;
        static int DESCRIPTION = 5;
        static int FROM_NAME = 6;
        static int FROM_ID = 7;
        static int FROM_PICTURE = 8;
        static int FULL_PICTURE = 9;
        static int LINK = 10;
        static int MESSAGE = 11;
        static int MESSAGE_TAGS = 12;
        static int OBJECT_ID = 13;
        static int PARENT_ID = 14;
        static int PLACE_ID = 15;
        static int PLACE_NAME = 16;
        static int PLACE_CITY = 17;
        static int PLACE_COUNTRY = 18;
        static int PLACE_LATITUDE = 19;
        static int PLACE_LONGITUDE = 20;
        static int PLACE_STREET = 21;
        static int PLACE_CHECKINS = 22;
        static int PRIVACY_VALUE = 23;
        static int PRIVACY_DESCRIPTION = 24;
        static int PRIVACY_FRIENDS = 25;
        static int PRIVACY_ALLOW = 26;
        static int PRIVACY_DENY = 27;
        static int SOURCE = 28;
        static int STATUS_TYPE = 29;
        static int STORY = 30;
        static int STORY_TAGS = 31;
        static int TYPE = 32;
        static int LIKES_COUNT = 33;
        static int COMMENTS_COUNT = 34;
        static int PHOTO_COUNT = 35;
        static int ADDED_STHG = 36;
        static int CHILD_ID = 37;
        static int COMMENT = 38;
        static int COMMUTITY_AVATAR = 39;
        static int COMMUTITY_NAME = 40;
        static int COMMUTITY_SUBSCRIBERS = 41;
        static int COMMUTITY_TYPE = 42;
        static int COMMUTITY_WALLPAPER = 43;
        static int EVENT_COVER = 44;
        static int EVENT_INFO = 45;
        static int FROM_PICTURE_PATH = 46;
        static int LOCATION_AVATAR_PATH = 47;
        static int PLACE_TYPE = 48;
        static int PLACE_VISITORS = 49;
        static int NAME = 50;
        static int PLACE_PICTURE = 51;
        static int SONG_ARTIST = 52;
        static int SONG_NAME = 53;
        static int NEW_FRIEND_ID = 54;
        static int NEW_FRIEND_AVATAR = 55;
        static int NEW_FRIEND_CAREER = 56;
        static int NEW_FRIEND_MUTAL_FRIENDS = 57;
        static int NEW_FRIEND_WORK = 58;
        static int NEW_FRIEND_EDUCATION = 59;
        static int NEW_FRIEND_COVER = 60;
        static int CAN_LIKE = 61;
        static int HAS_LIKED = 62;
        static int CAN_COMMENT = 63;
        static int CAN_SHARE = 64;
        static int TO_NAME = 65;
        static int TO_ID = 66;
        static int VIA_ID = 67;
        static int VIA_NAME = 68;
        static int GROUP_NAME = 69;
        static int GROUP_STATUS = 70;
        static int GROUP_ID = 71;
        static int LIKE_FROM_NAME = 72;
        static int LIKE_FROM_ID = 73;
        static int ATTACHMENT_DESCRIPTION = 74;
        static int ATTACHMENT_SRC = 75;
        static int ATTACHMENT_HEIGHT = 76;
        static int ATTACHMENT_WIDTH = 77;
        static int ATTACHMENT_TARGET_ID = 78;
        static int ATTACHMENT_TARGET_TITLE = 79;
        static int ATTACHMENT_TARGET_URL = 80;
        static int WITH_TAGS_COUNT = 81;
        static int WITH_TAGS_NAME = 82;
        static int WITH_TAGS_ID = 83;
        static int IMPLICIT_PLACE_ID = 84;
        static int IMPLICIT_PLACE_NAME = 85;
        static int IMPLICIT_PLACE_CITY = 86;
        static int IMPLICIT_PLACE_COUNTRY = 87;
        static int IMPLICIT_PLACE_LATITUDE = 88;
        static int IMPLICIT_PLACE_LONGITUDE = 89;
        static int IMPLICIT_PLACE_STREET = 90;
        static int TO_PROFILE_TYPE = 91;
    }

    namespace SelectPhotosById {
        static std::string QUERY_TEXT = "SELECT * FROM photos WHERE post_id IN (\'%s\') ORDER BY post_id;";

        static int POST_ID = 0;
        static int SOURCE = 1;
        static int PHOTO_NAME = 2;
        static int WIDTH = 3;
        static int HEIGHT = 4;
        static int POST_AUTO_ID = 5;
    }

    namespace SelectPostById {
        static std::string QUERY_TEXT = "SELECT * FROM posts WHERE id = \'%s\' ORDER BY auto_id;";
        static std::string BY_AUTO_ID_QUERY_TEXT = "SELECT * FROM posts WHERE auto_id = %d ORDER BY id;";

        static int AUTO_ID = 0;
        static int ID = 1;
        static int CREATED_TIME = 2;
        static int UPDATED_TIME = 3;
        static int CAPTION = 4;
        static int DESCRIPTION = 5;
        static int FROM_NAME = 6;
        static int FROM_ID = 7;
        static int FROM_PICTURE = 8;
        static int FULL_PICTURE = 9;
        static int LINK = 10;
        static int MESSAGE = 11;
        static int MESSAGE_TAGS = 12;
        static int OBJECT_ID = 13;
        static int PARENT_ID = 14;
        static int PLACE_ID = 15;
        static int PLACE_NAME = 16;
        static int PLACE_CITY = 17;
        static int PLACE_COUNTRY = 18;
        static int PLACE_LATITUDE = 19;
        static int PLACE_LONGITUDE = 20;
        static int PLACE_STREET = 21;
        static int PLACE_CHECKINS = 22;

        static int PRIVACY_VALUE = 23;
        static int PRIVACY_DESCRIPTION = 24;
        static int PRIVACY_FRIENDS = 25;
        static int PRIVACY_ALLOW = 26;
        static int PRIVACY_DENY = 27;
        static int SOURCE = 28;
        static int STATUS_TYPE = 29;
        static int STORY = 30;
        static int STORY_TAGS = 31;
        static int TYPE = 32;
        static int LIKES_COUNT = 33;
        static int COMMENTS_COUNT = 34;
        static int PHOTO_COUNT = 35;
        static int ADDED_STHG = 36;
        static int CHILD_ID = 37;
        static int COMMENT = 38;
        static int COMMUTITY_AVATAR = 39;
        static int COMMUTITY_NAME = 40;
        static int COMMUTITY_SUBSCRIBERS = 41;
        static int COMMUTITY_TYPE = 42;
        static int COMMUTITY_WALLPAPER = 43;
        static int EVENT_COVER = 44;
        static int EVENT_INFO = 45;
        static int FROM_PICTURE_PATH = 46;
        static int LOCATION_AVATAR_PATH = 47;
        static int PLACE_TYPE = 48;
        static int PLACE_VISITORS = 49;
        static int NAME = 50;
        static int PLACE_PICTURE = 51;
        static int SONG_ARTIST = 52;
        static int SONG_NAME = 53;
        static int NEW_FRIEND_ID = 54;
        static int NEW_FRIEND_AVATAR = 55;
        static int NEW_FRIEND_CAREER = 56;
        static int NEW_FRIEND_MUTAL_FRIENDS = 57;
        static int NEW_FRIEND_WORK = 58;
        static int NEW_FRIEND_EDUCATION = 59;
        static int NEW_FRIEND_COVER = 60;
        static int CAN_LIKE = 61;
        static int HAS_LIKED = 62;
        static int CAN_COMMENT = 63;
        static int CAN_SHARE = 64;
        static int TO_NAME = 65;
        static int TO_ID = 66;
        static int VIA_ID = 67;
        static int VIA_NAME = 68;
        static int GROUP_NAME = 69;
        static int GROUP_STATUS = 70;
        static int GROUP_ID = 71;
        static int LIKE_FROM_NAME = 72;
        static int LIKE_FROM_ID = 73;
        static int ATTACHMENT_DESCRIPTION = 74;
        static int ATTACHMENT_SRC = 75;
        static int ATTACHMENT_HEIGHT = 76;
        static int ATTACHMENT_WIDTH = 77;
        static int ATTACHMENT_TARGET_ID = 78;
        static int ATTACHMENT_TARGET_TITLE = 79;
        static int ATTACHMENT_TARGET_URL = 80;
        static int WITH_TAGS_COUNT = 81;
        static int WITH_TAGS_NAME = 82;
        static int WITH_TAGS_ID = 83;
        static int IMPLICIT_PLACE_ID = 84;
        static int IMPLICIT_PLACE_NAME = 85;
        static int IMPLICIT_PLACE_CITY = 86;
        static int IMPLICIT_PLACE_COUNTRY = 87;
        static int IMPLICIT_PLACE_LATITUDE = 88;
        static int IMPLICIT_PLACE_LONGITUDE = 89;
        static int IMPLICIT_PLACE_STREET = 90;
        static int TO_PROFILE_TYPE = 91;
    }

    namespace DeletePostById {
        static std::string QUERY_TEXT = "DELETE FROM posts WHERE id = \'%s\';";
    }

    namespace DeletePhotosById {
        static std::string QUERY_TEXT = "DELETE FROM photos WHERE post_id = \'%s\';";
    }

    namespace SelectUpdatedTimeByPostId {
        static std::string QUERY_TEXT = "SELECT updated_time FROM posts WHERE id = \'%s\';";
    }

    namespace SelectPostIdByPostAutoId {
        static std::string QUERY_TEXT = "SELECT id FROM posts WHERE auto_id = %d;";
    }

    namespace SelectPostAutoIdByPostId {
        static std::string QUERY_TEXT = "SELECT auto_id FROM posts WHERE id = \'%s\';";
    }

    namespace SelectPostOrder{
        static std::string QUERY_TEXT = "SELECT * FROM post_order WHERE position_id > %d and position_id < %d ORDER BY position_id;";

        static int POSITION_ID = 0;
        static int POST_AUTO_ID = 1;
        static int SESSION_AUTO_ID = 2;
    }

    namespace SelectPostOrderBySessionId{
        static std::string QUERY_TEXT = "SELECT * FROM post_order WHERE session_auto_id = %d ;";

        static int POSITION_ID = 0;
        static int POST_AUTO_ID = 1;
    }

    namespace DeletePostOrders {
        static std::string QUERY_TEXT = "DELETE FROM post_order;";
    }

    namespace SelectSessionMaxAutoId {
        static std::string QUERY_TEXT = "SELECT MAX(session_auto_id) FROM sessions;";
    }

    namespace SelectAccounts {
        static std::string QUERY_TEXT = "SELECT * FROM accounts WHERE user_name=\'%s\';";

        static int USER_NAME = 0;
        static int USER_ID = 1;
        static int ACCESS_TOKEN = 2;
        static int SESSION_KEY = 3;
        static int SESSION_SECRET = 4;
    }

    namespace DeleteAccounts {
        static std::string QUERY_TEXT = "DELETE FROM accounts WHERE user_name=\'%s\';";
    }

    namespace SelectNNotifications {
        static std::string QUERY_TEXT = "SELECT * FROM notifications LIMIT %d;";

        const int NOTIFICATION_ID = 0;
        const int NOTIFICATION_MSG = 1;
        const int USER_NAME = 2;
        const int IMAGE_PATH = 3;
        const int NOTIF_RCVD_TIME = 4;
        const int NOTIFICATION_TYPE = 5;
        const int NOTIF_GRAPH_OBJECT = 6;
    }

    namespace DeleteNotifications {
        static std::string QUERY_TEXT = "DELETE FROM notifications;";
    }

    namespace SelectLoggedInUser {
        static std::string QUERY_TEXT = "SELECT * FROM loggedinuser LIMIT 1;";

        const int USER_ID = 0;
    }

    namespace DeleteLoggedInUser {
        static std::string QUERY_TEXT = "DELETE FROM loggedinuser;";
    }

    namespace GetSettingsValue {
        static std::string QUERY_TEXT = "SELECT value FROM settings WHERE name = \'%s\';";
    }
}

#endif /* QUERIES_H_ */
