/*
 * PostLoader.h
 *
 *  Created on: Oct 12, 2015
 *      Author: dvasin
 */

#ifndef POSTLOADER_H_
#define POSTLOADER_H_
#include "FacebookSession.h"
#include "Post.h"

class PostLoader;

class PostLoaderSubscriber {
public:
    PostLoaderSubscriber(){}
    virtual ~PostLoaderSubscriber(){}
    virtual void LoadComplete(PostLoader *loader, int errorCode) = 0;
};

class PostLoader{
public:
    PostLoader(const char *id, const char *parent_id);
    virtual ~PostLoader();
    bool startLoad(PostLoaderSubscriber* subscriber);
    Post *getPost();
    void SetProcessed(bool value) { mProcessed = value; };
    bool GetProcessed() { return mProcessed; };
    const char *getPostId() { return mId; };
    const char *getParentPostId() { return mParentId; };

protected:
    static void on_post_request_completed(void* object, char* respond, int code);
private:
    char *mId;
    char *mParentId;
    Post *mPost;
    GraphRequest * mPostRequest;
    PostLoaderSubscriber *mSubscriber;
    bool mProcessed;
};



#endif /* POSTLOADER_H_ */
