#ifndef GRAPH_OBJECT_DOWNLOADER_H_
#define GRAPH_OBJECT_DOWNLOADER_H_

#include "ActionBase.h"
#include "AppEvents.h"

class GraphObject;
class GraphObjectDownloader;
class GraphRequest;

class IGraphObjectDownloaderObserver {
public:
    enum Result {
        ENothingDownloaded,
        EPostDownloaded,
        EPhotoDownloaded,
        ENetworkErrorToBeResumed,
        EDownloadingStarted,//it is also sent each time when downloading is resumed
        EAlbumDownloaded
    };
    virtual ~IGraphObjectDownloaderObserver() {};
    virtual void GraphObjectDownloaderProgress(Result result) = 0;
    virtual void DownloaderProgress(Result result, GraphObjectDownloader *downloader) = 0;
};

class GraphObjectDownloader : public Subscriber {
public:
    GraphObjectDownloader(const char *id, IGraphObjectDownloaderObserver *observer);
    GraphObjectDownloader(const char *id, IGraphObjectDownloaderObserver *observer, ActionAndData *data); //we use this when we know the graph object and we need to download it.
    virtual ~GraphObjectDownloader();

    void StartDownloading();
    inline GraphObject *GetGraphObject() {return mObject;}
    inline ActionAndData *GetGraphObjectData() {return mActionAndData;}

private:
    void Update(AppEventId eventId, void *data) override;

    static void on_post_request_completed(void* object, char* respond, int code);
    static void on_photo_request_completed(void* object, char* respond, int code);
    static void on_get_object_id_details_completed(void *data, char* str, int code);

    enum State {
        ENotStarted,
        EPostDownloaded,
        EPhotoDownloaded,
        EAlbumDownloaded
    };

private:
    GraphObject *mObject;
    IGraphObjectDownloaderObserver *mObserver;
    char *mId;
    GraphRequest *mRequest;
    State mState;
    ActionAndData *mActionAndData;
};


#endif /* GRAPH_OBJECT_DOWNLOADER_H_ */


