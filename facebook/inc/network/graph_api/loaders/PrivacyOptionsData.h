#ifndef PRIVACYOPTIONSDATA_H_
#define PRIVACYOPTIONSDATA_H_

#include <Elementary.h>

class  GraphRequest;

class PrivacyOption {
public:
    PrivacyOption(const char *id, const char *description, const char *type, const char *icon, bool isCurrentlySelected);
    virtual ~PrivacyOption();

    static const char *KBigIconPostfix;
    static const char *KSmallIconPostfix;

    const char *GetId() {return mId;}

    const char *GetDescription() {return mDescription;}

    const char *GetType() {return mType;}

    virtual const char *GenIconPath(bool isSmall) {return mIcon;}

    bool GetIsSelected() {return mIsCurrentlySelected;}

protected:
    char* mId;
    char* mDescription;
    char* mType;
    char* mIcon;
    bool mIsCurrentlySelected;
};

class IPrivacyOptionsObserver {
public:
    virtual ~IPrivacyOptionsObserver(){}
    virtual void UpdatePrivacyOptions(Eina_List *privacyOptions) = 0;
};

class PrivacyOptionsData {
public:
    PrivacyOptionsData();
    ~PrivacyOptionsData();

    void GetPrivacyOptionsFromFB(IPrivacyOptionsObserver *observer);
    void RemoveObserver(IPrivacyOptionsObserver *observer);
    Eina_List *GetPrivacyOptions(){return mOptions;}

private:
    void GetPrivacyOptionsFromDB();
    void StartGetPrivacyOptionsRequest();
    void UpdateOptions(Eina_List *newOptions);

    static void on_get_privacy_options_completed(void* object, char* respond, int code);
    void ParsePrivacyOptionsFromJson(const char* json);
private:
    Eina_List *mOptions;
    Eina_List *mObservers;
    GraphRequest * mGetPrivacyOptionsRequest;
};

#endif /* PRIVACYOPTIONSDATA_H_ */
