#ifndef USERPROFILEDATAUPLOADER_H_
#define USERPROFILEDATAUPLOADER_H_

#include "GraphRequest.h"

typedef void (*profile_data_uploaded)(void *parent, int code);

class UserProfileData;

class UserProfileDataUploader
{
public:
    UserProfileDataUploader(const char *id, profile_data_uploaded done_cb, void *parent);
    virtual ~UserProfileDataUploader();

    void StartUploading();

    GraphRequest *mBasicDataUploader;
    GraphRequest *mFirstPhotoUploader;
    GraphRequest *mFirstFriendUploader;
    GraphRequest *mAvatarIdUploader;

    profile_data_uploaded mUploadingDone;
    UserProfileData *mUserData;
private:
    static void on_user_profile_info_received_cb(void* object, char* respond, int code);
    static void on_first_photo_received_cb(void* object, char* respond, int code);
    static void on_first_friend_received_cb(void* object, char* respond, int code);
    static void on_avatar_id_received_cb(void* object, char* respond, int code);

    void *mParent;
    const char *mId;
};



#endif /* USERPROFILEDATAUPLOADER_H_ */
