#ifndef NOTIFICATIONPROVIDER_H_
#define NOTIFICATIONPROVIDER_H_

#include "AbstractDataProvider.h"
#include "NotificationObject.h"

class NotificationProvider: public AbstractDataProvider
{
    NotificationProvider();
public:
    static NotificationProvider* GetInstance();
    virtual ~NotificationProvider();
    void DeleteItem(void* item) override;
    UPLOAD_DATA_RESULT UploadData(bool isDescendingOrder = true) override;

    int GetUnreadBadgeNum() { return mUnreadBadgeNum; }
    void SetUnreadBadgeNum(int num);

    void DeleteNotificationsByPostId(const char *id);

private:
    unsigned int ParseData(char *data, bool isDescendingOrder, Ecore_Thread *thread = NULL) override;
    void ReleaseItem(void* item) override;
    bool IsNotificationNew(NotificationObject *notification);

    int mUnreadBadgeNum;
    bool mEarlierSeparatorCreated;
};

#endif /* NOTIFICATIONPROVIDER_H_ */
