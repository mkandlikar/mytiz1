#ifndef PEOPLEWHOLIKEDPROVIDER_H_
#define PEOPLEWHOLIKEDPROVIDER_H_

#include "AbstractDataProvider.h"

class PeopleWhoLikedProvider: public AbstractDataProvider
{
public:
    PeopleWhoLikedProvider();
    virtual ~PeopleWhoLikedProvider();

    void SetEntityId( const char *id );
    User *GetUserById(const char *id);

    void DeleteItem( void* item ) override;
    UPLOAD_DATA_RESULT UploadData( bool isDescendingOrder = true ) override;

    bool ReplaceUser(GraphObject *user);
private:
    unsigned int ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread = NULL ) override;
    void ReleaseItem( void* item ) override;

    const char *m_EntityId;
};

#endif /* PEOPLEWHOLIKEDPROVIDER_H_ */
