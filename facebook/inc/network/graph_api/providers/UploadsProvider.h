#ifndef UPLOADSPROVIDER_H_
#define UPLOADSPROVIDER_H_

#include "PhotoGalleryProvider.h"

class UploadsProvider: public PhotoGalleryProvider
{
    UploadsProvider(){}
public:
    static UploadsProvider* GetInstance();
    virtual ~UploadsProvider();
};

#endif /* UPLOADSPROVIDER_H_ */
