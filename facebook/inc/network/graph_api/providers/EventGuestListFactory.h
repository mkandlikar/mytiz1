/*
 * EventGuestListFactory.h
 *
 *  Created on: Nov 24, 2015
 *      Author: ruykurin
 */

#ifndef EVENTGUESTLISTFACTORY_H_
#define EVENTGUESTLISTFACTORY_H_

#include "EventGuestList.h"
#include "EventGuestListProvider.h"
#include "Event.h"

#ifdef EVENT_NATIVE_VIEW
class EventGuestListFactory {
    EventGuestListFactory() { }
public:
    static EventGuestListProvider * GetProvider(EventGuestListTabs type);
    static GraphObject * GetGraphObjectById(const char * id);
    virtual ~EventGuestListFactory() { }
    static Event::EventRsvpStatus GetLoggedInUserStatus();
};

#endif
#endif /* EVENTGUESTLISTFACTORY_H_ */
