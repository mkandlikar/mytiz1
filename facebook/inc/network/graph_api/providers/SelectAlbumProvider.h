#ifndef SELECTALBUMPROVIDER_H_
#define SELECTALBUMPROVIDER_H_

#include "AlbumsProviderBase.h"

class SelectAlbumProvider: public AlbumsProviderBase
{
    SelectAlbumProvider(){}
public:
    static SelectAlbumProvider* GetInstance();
    virtual ~SelectAlbumProvider();
    void SetUserId(const char* userId);
private:
    virtual unsigned int ParseData(char *data, bool isDescendingOrder, Ecore_Thread *thread = NULL);
};

#endif /* SELECTALBUMPROVIDER_H_ */
