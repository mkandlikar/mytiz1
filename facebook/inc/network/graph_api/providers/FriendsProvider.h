#ifndef FRIENDSPROVIDER_H_
#define FRIENDSPROVIDER_H_

#include "AbstractDataProvider.h"

/**
 * @brief  This class implements the Data Provider for Friends request data.
 *         This kind of data is the friends within the user (me) in the contact
 *         list.
 */
class User;
class FriendsProvider: public AbstractDataProvider
{
protected:
    unsigned int m_MutualFriendsLimit;
    static const char *FRIENDS_REQUEST_PART;
    bool mMissCacheRetrieve;

public:
    bool IsFriendInFriendsArray(const char* userId);
    FriendsProvider(const char *id = nullptr);
    ~FriendsProvider() override;
    void AddItem(User *item);
    void FilterData(bool appendToFilteredDataArray = false);
    void SortData();
    void SetMutualFriendsLimit(unsigned int limit);
    void SetAlphabeticalOrder(bool isAlphabetical);
    Eina_Array * SortFriendsArray(Eina_Array * friendsArray);

    void SetFindText(const char *text);
    void ResetFilter();
    const char *GetFindText() {return mTextToFind;}
    Friend *GetFriendById(const char *id);
    void AppendToFilteredDataArray();

    void MissCacheRetrieve( bool miss );

    Eina_Array* GetData() const override;
    unsigned int GetDataCount() const override;
    void DeleteItem(void* item) override;

private:
    unsigned int ParseData(char *data, bool isDescendingOrder, Ecore_Thread *thread = nullptr) override;
    void ReleaseItem(void* item) override;
    UPLOAD_DATA_RESULT UploadData(bool isDescendingOrder = true) override;
    void CustomErase() override;

    static Eina_Bool friends_array_comparator(const void *container, void *data, void *fdata);

    static const unsigned int REQUEST_FRIENDS_LIMIT;
    static const unsigned int DEFAULT_MUTUAL_FRIENDS_LIMIT;
    char *mId;
    char *mTextToFind;
    bool mAlphabeticalOrder;
    Eina_Array *mFilteredData;
    Eina_Array *mSortedData;
    unsigned int mLastIndex;
};

#endif /* FRIENDSPROVIDER_H_ */
