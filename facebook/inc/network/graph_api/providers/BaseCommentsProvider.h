/*
 * BaseCommentsProvider.h
 *
 * Created: Jul 28, 2015
 * Author: Dmitry.Shcherbakov@SymphonyTeleca.com
 */

#ifndef BASECOMMENTSPROVIDER_H_
#define BASECOMMENTSPROVIDER_H_

#include "AbstractDataProvider.h"

class BaseCommentsProvider: public AbstractDataProvider
{
protected:
    BaseCommentsProvider();
    void SetReplyCount( unsigned int count );
    const char *m_EntityId;
public:
    virtual ~BaseCommentsProvider();

    void SetEntityId( const char *id );
    void SetSummaryCount( unsigned int count );

    virtual UPLOAD_DATA_RESULT UploadData( bool isDescendingOrder = true );
    virtual void DeleteItem( void* item );

private:
    virtual unsigned int ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread = NULL );
    virtual void ReleaseItem( void* item );

    unsigned int m_SummaryCount;

    unsigned int m_RepliesLimit;
    unsigned int m_RepliesSummaryCount;

    static const unsigned int DEFAULT_SUMMARY_COUNT;
    static const unsigned int DEFAULT_REPLAY_LIMIT;
    static const unsigned int DEFAULT_REPLAY_SUMMARY_COUNT;
    static const unsigned int REQUEST_COMMENT_LIMIT;
};

#endif /* BASECOMMENTSPROVIDER_H_ */
