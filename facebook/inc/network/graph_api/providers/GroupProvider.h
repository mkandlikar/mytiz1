#ifndef GROUPPROVIDER_H_
#define GROUPPROVIDER_H_

#include "AbstractDataProvider.h"

class GroupProvider: public AbstractDataProvider
{
public:
    static GroupProvider* GetInstance();
    ~GroupProvider() override;
    void SetFindText(const char *text);
    UPLOAD_DATA_RESULT UploadData(bool isDescendingOrder = true) override;
    void FilterData();
    inline bool GroupsDataRequestSuccessful() const {return mGroupsDataRequestSuccessful;}
    unsigned int GetDataCount() const override;

    Eina_Array* GetIds() const;
    bool IsGroup(const char * object);

private:
    GroupProvider();

    Eina_Array* GetData() const override;
    unsigned int ParseData(char *data, bool isDescendingOrder, Ecore_Thread *thread = NULL) override;
    void DeleteItem(void* item) override;
    void ReleaseItem( void* item ) override;
    void CustomErase() override;

    void ClearIds();

    char *mTextToFind;
    Eina_Array *mFilteredData;

    Eina_Array *mIds;

    bool mIsNeedSeparator;
    bool mGroupsDataRequestSuccessful;
};

#endif /* GROUPPROVIDER_H_ */
