#ifndef DATA_MUTUALFRIENDSPROVIDER_H_
#define DATA_MUTUALFRIENDSPROVIDER_H_

#include "FriendsProvider.h"

class MutualFriendsProvider: public FriendsProvider {
public:
    UPLOAD_DATA_RESULT UploadData( bool isDescendingOrder );
    MutualFriendsProvider(const char *id);
private:
    virtual ~MutualFriendsProvider(){};
    virtual unsigned int ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread = NULL );
    const char *mId;
};

#endif /* DATA_MUTUALFRIENDSPROVIDER_H_ */
