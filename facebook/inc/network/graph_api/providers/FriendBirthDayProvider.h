/*
 * FriendBirthDayProvider.h
 *
 *  Created on: Oct 30, 2015
 *      Author: ruibezruch
 */

#ifndef FRIENDBIRTHDAYPROVIDER_H_
#define FRIENDBIRTHDAYPROVIDER_H_

#include "AbstractDataProvider.h"

class FriendBirthDayProvider: public AbstractDataProvider
{
public:
    static FriendBirthDayProvider* GetInstance();
    virtual ~FriendBirthDayProvider();

    virtual void DeleteItem(void* item);
    virtual UPLOAD_DATA_RESULT UploadData(bool isDescendingOrder = true);
private:
    FriendBirthDayProvider();
    virtual unsigned int ParseData(char *data, bool isDescendingOrder, Ecore_Thread *thread = NULL);
    virtual void ReleaseItem(void* item);
    void CreateBDayData(bool isDescendingOrder);
    static int sort_cb(const void *d1, const void *d2);
};

#endif /* FRIENDBIRTHDAYPROVIDER_H_ */
