/*
 * PlacesProvider.h
 *
 * Created: 18 SEP 2015
 * Author: Jeyaramakrishnan Sundar
 */

#ifndef PLACESPROVIDER_H_
#define PLACESPROVIDER_H_

#include "AbstractDataProvider.h"

class PlacesProvider: public AbstractDataProvider
{
    PlacesProvider();
public:
    static PlacesProvider* GetInstance();
    virtual ~PlacesProvider();
    virtual UPLOAD_DATA_RESULT UploadData( bool isDescendingOrder = true );
    virtual void DeleteItem( void* item );
    void ClearIds();
    void SetEntityId( const char *id );
    void setLatitudeLongitude(double latitude , double longitude){mLatitude = latitude; mLongitude = longitude;}
    Eina_Array* GetIds() const;

private:
    Eina_Array *m_Ids;
    const char *m_EntityId;
    double mLatitude;
    double mLongitude;
    static const double LAT_LONG_TUDE;

    virtual unsigned int ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread = NULL );
    virtual void ReleaseItem( void* item );
    virtual void CustomErase();

    static const char *RADIUS;
    static const char *PLACES_REQUEST_PART;

    const char * getLatitudeStr();
    const char * getLongitudeStr();
};

#endif /* PLACESPROVIDER_H_ */
