#ifndef ALBUMSPROVIDER_H_
#define ALBUMSPROVIDER_H_

#include "AlbumsProviderBase.h"

/**
 * @brief  This class implements the Data Provider for Albums.
 */
class AlbumsProvider : public AlbumsProviderBase {

public:
    static AlbumsProvider* GetInstance();
    virtual ~AlbumsProvider();
    void SetUserId(const char* userId, class ActionBase* action);
protected:
    AlbumsProvider();
    virtual unsigned int ParseData(char* data, bool isDescendingOrder, Ecore_Thread* thread = nullptr);
    virtual void CustomErase();
private:
    class ActionBase* mAction;
};

#endif /* ALBUMSPROVIDER_H_ */
