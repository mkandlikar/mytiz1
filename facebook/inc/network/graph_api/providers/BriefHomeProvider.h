/*
 * BriefHomeProvider.h
 *
 * Created: Nov 18, 2015
 * Author: Dmitry.Shcherbakov@SymphonyTeleca.com
 */

#ifndef BRIEFHOMEPROVIDER_H_
#define BRIEFHOMEPROVIDER_H_

#include "AbstractDataProvider.h"

class BriefPostInfo;
class Post;

class BriefHomeProvider: public AbstractDataProvider
{
    BriefHomeProvider();
public:
    virtual ~BriefHomeProvider();
    static BriefHomeProvider* GetInstance();

    /**
     * @retval  returns 'false' if for some reason the given ids array cannot be
     *          used for request data. It could be due:
     *          1. Empty array has been given
     *          2. The given array is the same as it was before
     */
    bool SetIds( const Eina_Array *ids );
    void DeleteItem( void* item );
    virtual void FillManualPagination( bool isDescendigOrder = true );

    virtual UPLOAD_DATA_RESULT UploadData( bool isDescendingOrder = true );

protected:
    virtual bool GetPagingAttributes( std::stringstream &finalRequest, bool isDescendingOrder, bool isExtraParams );

private:
    virtual unsigned int ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread = NULL );
    std::string PrepareIds( bool isDescendingOrder = true );
    virtual void ReleaseItem( void* item );
    virtual void CustomErase();
    bool IsPostChanged( Post *post, BriefPostInfo *info );

    Eina_Array *m_Ids;
    unsigned int m_IdLastIndex;
    unsigned int m_HandledItems;

    static unsigned short MAX_REQUEST_ITEMS;
    static unsigned short IDS_CONTAINER_SIZE_STEP;
};

#endif /* BRIEFHOMEPROVIDER_H_ */
