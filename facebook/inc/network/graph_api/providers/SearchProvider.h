/*
 * SearchProvider.h
 *
 *  Created on: Aug 6, 2015
 *      Author: RUINMKOL
 */

#ifndef SEARCHPROVIDER_H_
#define SEARCHPROVIDER_H_

#include "AbstractDataProvider.h"
/*
 *
 */
class SearchProvider: public AbstractDataProvider
{
public:

    static SearchProvider* GetInstance();
    virtual ~SearchProvider();
    virtual UPLOAD_DATA_RESULT UploadData( bool isDescendingOrder = true );
    virtual void DeleteItem( void* item );
    void SetSearchString(const char * searhString);

private:
    SearchProvider();
    virtual unsigned int ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread = NULL );
    virtual void CustomErase();
    virtual void ReleaseItem( void* item );

    const char * mSearchString;

    static const char * SEARCH_REQUEST_MAIN_PART;
    static const char * SEARCH_FILTER;

    static const unsigned int SEARCH_REQUEST_LIMIT;
};

#endif /* SEARCHPROVIDER_H_ */
