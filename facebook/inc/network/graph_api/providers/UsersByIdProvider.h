#ifndef USERSBYIDPROVIDER_H_
#define USERSBYIDPROVIDER_H_

#include "AbstractDataProvider.h"

class UsersByIdProvider: public AbstractDataProvider {
public:
    virtual ~UsersByIdProvider();
    static UsersByIdProvider* GetInstance();

    virtual UPLOAD_DATA_RESULT UploadData( bool isDescendingOrder = true );
    bool SetIds(Eina_List *ids);

private:
    UsersByIdProvider();

    virtual bool GetPagingAttributes( std::stringstream &finalRequest, bool isDescendingOrder, bool isExtraParams );
    virtual void DeleteItem(void* item);
    virtual void FillManualPagination( bool isDescendigOrder = true );
    virtual unsigned int ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread = NULL );
    std::string PrepareIds( bool isDescendingOrder = true );
    virtual void ReleaseItem( void* item );
    virtual void CustomErase();

    Eina_Array *m_Ids;
    unsigned int m_IdLastIndex;
    unsigned int m_HandledItems;

    static unsigned short MAX_REQUEST_ITEMS;
    static unsigned short IDS_CONTAINER_SIZE_STEP;
};

#endif /* USERSBYIDPROVIDER_H_ */
