/*
 * FeedExtProvider.h
 *
 *  Created on: Oct 13, 2015
 *      Author: dvasin
 */

#ifndef FEEDEXTPROVIDER_H_
#define FEEDEXTPROVIDER_H_

#include "FeedProvider.h"

class FeedExtProvider : public FeedProvider
{
public:
    FeedExtProvider();

};

#endif /* FEEDEXTPROVIDER_H_ */
