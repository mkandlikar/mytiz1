/*
 * FriendRequestsProvider.h
 *
 * Created on: Jul 23, 2015
 * Author: Dmitry.Shcherbakov@SymphonyTeleca.com
 */

#ifndef FRIENDREQUESTSPROVIDER_H_
#define FRIENDREQUESTSPROVIDER_H_

#include "AbstractDataProvider.h"

/**
 * @brief  This class implements the Data Provider for Friends Requests
 *         data. Such data is the incoming friends requests (friends which
 *         is awaiting for confirmation to add their to a contact list.
 */
class FriendRequestsProvider: public AbstractDataProvider
{
    FriendRequestsProvider();
public:
    static FriendRequestsProvider* GetInstance();
    virtual ~FriendRequestsProvider();

    virtual void DeleteItem( void* item ){}
    virtual UPLOAD_DATA_RESULT UploadData( bool isDescendingOrder = true );
    bool IsRequest(const void * object);
private:
    virtual void CustomErase();
    virtual unsigned int ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread = NULL );
    virtual void ReleaseItem( void* item );

    void ClearIds();
    Eina_Array* GetIds() const;

    Eina_Array *m_Ids;
    static const char *FRIEND_REQUESTS_MAIN_REQUEST_PART;
};

#endif /* FRIENDREQUESTSPROVIDER_H_ */
