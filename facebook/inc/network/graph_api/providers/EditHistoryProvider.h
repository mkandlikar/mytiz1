/*
 * EditHistoryProvider.h
 *
 *  Created on: May 11, 2016
 *      Author: ruibezruch
 */

#ifndef EDITHISTORYPROVIDER_H_
#define EDITHISTORYPROVIDER_H_

#include "AbstractDataProvider.h"

class EditHistoryProvider: public AbstractDataProvider
{
    EditHistoryProvider();
public:
    static EditHistoryProvider* GetInstance();
    virtual ~EditHistoryProvider();

    void SetId(const char *id);

private:
    virtual void CustomErase();
    virtual unsigned int ParseData(char *data, bool isDescendingOrder, Ecore_Thread *thread = NULL);
    virtual void ReleaseItem(void* item);
    virtual void DeleteItem(void* item);
    virtual UPLOAD_DATA_RESULT UploadData(bool isDescendingOrder = true);

    const char *mId;

    static const char *REQUEST_MAIN_PART;
};

#endif /* EDITHISTORYPROVIDER_H_ */
