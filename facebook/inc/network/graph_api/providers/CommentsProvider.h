#ifndef COMMENTSPROVIDER_H_
#define COMMENTSPROVIDER_H_

#include "BaseCommentsProvider.h"

class Comment;

class CommentsProvider: public BaseCommentsProvider
{
public:
    CommentsProvider();

    virtual ~CommentsProvider();

    virtual void Update(AppEventId eventId, void * data);

    bool ReplaceCommentObject(Comment *comment);

    static const unsigned int COMMENTS_DEFAULT_REPLY_LIMIT;
};

#endif /* COMMENTSPROVIDER_H_ */
