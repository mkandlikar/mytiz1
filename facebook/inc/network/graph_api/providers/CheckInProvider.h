#ifndef CHECKINPROVIDER_H_
#define CHECKINPROVIDER_H_

#include "AbstractDataProvider.h"

class CheckInProvider: public AbstractDataProvider {
public:
    static CheckInProvider* GetInstance();
    virtual ~CheckInProvider();

    void SetSearchCriteria(double latitude, double longitude, const char *string);

protected:
    CheckInProvider();

private:
    virtual UPLOAD_DATA_RESULT UploadData( bool isDescendingOrder = true );
    virtual void DeleteItem( void* item );
    virtual unsigned int ParseData(char *data, bool isDescendingOrder, Ecore_Thread *thread);
    virtual void CustomErase();
    virtual void ReleaseItem( void* item );

    static const char *CHECKIN_MAIN_REQUEST_PART;
    static const char *CHECKIN_MAIN_REQUEST_PART_WITH_STR;
    static const unsigned int CHECKIN_REQUEST_LIMIT;
    static const unsigned int CHECKIN_REQUEST_RADUIS;

private:
    double mLatitude;
    double mLongitude;
    char*mSearchString;
};

#endif /* CHECKINPROVIDER_H_ */
