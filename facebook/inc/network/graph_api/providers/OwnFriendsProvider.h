#ifndef OWNFRIENDSPROVIDER_H_
#define OWNFRIENDSPROVIDER_H_

#include "FriendsProvider.h"
#include "Mutex.h"
#include <list>
#include <string>
#include <vector>

class OwnFriendsProvider: public FriendsProvider
{
public:
    OwnFriendsProvider();
    static OwnFriendsProvider* GetInstance();
    virtual ~OwnFriendsProvider();

    virtual UPLOAD_DATA_RESULT UploadData(bool isDescendingOrder = true);

    bool IsFriend(const void * object);
    std::list<class Friend*> GetDataCopy();
    void DeleteItemById(const char * id);
private:
    void ClearIds();
    virtual unsigned int ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread = NULL );

    bool RetrieveCachedData();
    bool IsCachedDataShouldBeSet();

    Mutex m_IdsMutex;
    std::vector<std::string> m_Ids;
};



#endif /* OWNFRIENDSPROVIDER_H_ */
