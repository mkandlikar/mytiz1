/*
 * SubscribersProvider.h
 *
 * Created: Aug 06, 2015
 * Author: ruinreku
 */

#ifndef SUBSCRIBERSPROVIDER_H_
#define SUBSCRIBERSPROVIDER_H_

#include "AbstractDataProvider.h"

/**
 * @brief  This class implements the Data Provider for subscribers request data.
 *         This kind of data is the my subscribers
 */
class SubscribersProvider: public AbstractDataProvider
{
	SubscribersProvider();
public:
    static SubscribersProvider* GetInstance();
    virtual ~SubscribersProvider(){ EraseData(); }

    virtual UPLOAD_DATA_RESULT UploadData( bool isDescendingOrder = true );

    virtual void DeleteItem( void* item );

    Eina_Array* GetIds() const;

    static bool idsComparator(const void * object, const void * id);

    static bool comparator( void *forCompare, void *toCompare );

private:
    virtual unsigned int ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread = NULL );
    virtual void CustomErase();
    virtual void ReleaseItem( void* item );

    Eina_Array *m_Ids;

    static const unsigned int DEFAULT_MUTUAL_FRIENDS_LIMIT;
    static const char *SUBSCRIBERS_REQUEST_PART;
};

#endif /* SUBSCRIBERSPROVIDER_H_ */
