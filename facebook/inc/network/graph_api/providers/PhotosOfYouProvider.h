#ifndef PHOTOSOFYOUPROVIDER_H_
#define PHOTOSOFYOUPROVIDER_H_

#include "PhotoGalleryProvider.h"

class PhotosOfYouProvider: public PhotoGalleryProvider
{
    PhotosOfYouProvider(){}
public:
    static PhotosOfYouProvider* GetInstance();
    virtual ~PhotosOfYouProvider(){}
};

#endif /* PHOTOSOFYOUPROVIDER_H_ */
