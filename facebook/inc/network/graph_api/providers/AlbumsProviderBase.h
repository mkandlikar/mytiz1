#ifndef ALBUMSPROVIDERBASE_H_
#define ALBUMSPROVIDERBASE_H_

#include "AbstractDataProvider.h"
#include <string>

class AlbumsProviderBase: public AbstractDataProvider {
protected:
    AlbumsProviderBase();
    virtual ~AlbumsProviderBase();
    virtual UPLOAD_DATA_RESULT UploadData(bool isDescendingOrder = true);
    virtual void DeleteItem( void* item );
    virtual void CustomErase();
    virtual void ReleaseItem(void* item);

    static const char *ALBUMS_MAIN_REQUEST_PART;
    static const char *ALBUMS_FIELDS_LIST;

    std::string mUserId;
};

#endif /* ALBUMSPROVIDERBASE_H_ */
