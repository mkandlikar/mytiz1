/*
 * NotificationAbstractDataProvider.h
 *
 *  Created on: Aug 28, 2015
 *      Author: ingsharm02
 */

#ifndef NOTIFICATIONABSTRACTDATAPROVIDER_H_
#define NOTIFICATIONABSTRACTDATAPROVIDER_H_

#include "AbstractDataProvider.h"

class NotificationAbstractDataProvider : public AbstractDataProvider
{
private:
    char *mData;
    char *mHeader;
    int mRetCode;
    NotificationAbstractDataProvider();
    virtual void IncomingData( char* data, char *header, int curlCode, Ecore_Thread *thread = NULL );
    virtual unsigned int ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread = NULL );
    virtual void ReleaseItem( void* item );

public:
    static NotificationAbstractDataProvider* GetInstance();
    virtual ~NotificationAbstractDataProvider();
    virtual UPLOAD_DATA_RESULT UploadData( bool isDescendingOrder );
    virtual void DeleteItem( void* item );
    GraphRequest * GetOldNotification( const char *request, bundle *param );

    int getRetCode() const { return mRetCode; }
    char* getData() const { return mData; }
};


#endif /* NOTIFICATIONABSTRACTDATAPROVIDER_H_ */
