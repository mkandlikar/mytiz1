#ifndef FRIENDREQUESTSBATCHPROVIDER_H_
#define FRIENDREQUESTSBATCHPROVIDER_H_

#include "AbstractDataProvider.h"
#include "FbRespondGetPeopleYouMayKnow.h"
#include "User.h"

class Separator;

class FriendRequestsBatchProvider: public AbstractDataProvider
{
    FriendRequestsBatchProvider();
public:

    typedef enum BATCH_REQUEST_ {
        FRIENDS_REQUEST = 0,
        PEOPLE_YOU_MAY_KNOW_REQUEST,
    } BATCH_REQUEST;

    static FriendRequestsBatchProvider* GetInstance();
    virtual ~FriendRequestsBatchProvider();

    void DeleteItem( void* item ) override;
    UPLOAD_DATA_RESULT UploadData( bool isDescendingOrder ) override;

    Eina_Array* GetPYMK_Ids() const;
    Eina_Array* GetFR_Ids() const;

    FbRespondGetPeopleYouMayKnow::FriendableUser *GetPymkDataById(const void *userId);
    User *GetFrDataById(const void *userId);

    void RequestCancelSuccessSet(const void *object);
    void RequestSendSuccessSet(const void *object);

    void FriendConfirmedSuccessSet(const void *object);
    void FriendDeletedSuccessSet(const void *object);
    void AppendNewFriendRequestsHeaderItems();

    static const char *BATCH_REQUEST_FRIENDS_REQUEST;
    static const char *REST_PEOPLE_YOU_MAY_KNOW_REQUEST;

    int mUnreadBadgeNum;
    inline int GetRequestsTotalCount() { return mRequestsTotalCount; }
    void SetRequestsTotalCount(int count);
    inline void SetMissCacheRetrive() { mMissCacheRetrive = true; }
    inline void SetDeleteInvalidFRs() { mDeleteInvalidFRs = true; }

private:
    void CustomErase() override;
    unsigned int ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread = nullptr ) override;
    void ReleaseItem( void* item ) override;

    unsigned int RetrieveCachedData();
    Eina_List * SetPeopleYouMayKnow(Eina_List *peopleList, bool isDescendingOrder);
    void LoadPeopleYouMayKnowFromCache();
    void SetFriendsRequest();
    void SetPeopleYouMayKnowRequest();

    void ClearAllIds();
    void ClearFRIds();
    void ClearPYMKIds();

    void UpdateFriendRequestsHeader();
    void UpdatePeopleYouMayKnowHeader();
    GraphObject* GetGraphObjectById(const void *userId);

    Separator *mFriendRequests_Separator = nullptr;
    Separator *mNoNewFriends_Separator = nullptr;
    Separator *mPeopleYouMayKnow_Separator = nullptr;
    bool mBothRequestsWereDone;
    bool mMissCacheRetrive = false;
    // with mMissCacheRetrive==true only new friend requests will be added, otherwise all data will be refreshed

    BATCH_REQUEST m_CurrentRequest;

    Eina_Array *mPYMK_Ids;
    Eina_Array *mFR_Ids;
    Eina_List  *mParsedItemsList;
    Eina_Array *mFriendRequestsArray;
    Eina_Array *mPYMKArray;
    Eina_Array *mFR_Header;
    Eina_Array *mPYMK_Header;
    Eina_Lock m_IdsMutex;
    int mRequestsTotalCount;

    bool mDeleteInvalidFRs;
};

#endif /* FRIENDREQUESTSBATCHPROVIDER_H_ */
