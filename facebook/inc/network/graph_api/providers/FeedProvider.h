#ifndef FEEDPROVIDER_H_
#define FEEDPROVIDER_H_

#include "PostProvider.h"

// Forward declarations
class Post;

/**
 * @brief  This class implements the Data Provider for Feed request data.
 */
class FeedProvider : public PostProvider
{
public:
    FeedProvider();
    virtual ~FeedProvider();
    void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = nullptr) override;
    Post* GetPostByIdFromCache(const char* id) override;
    void SetEntityId( const char *id );

private:
    std::string GetMainRequestPart() override;
    unsigned int RetrieveCachedData() override;
    bool IsNeedToHandleOperation(Sptr<Operation> oper) override;
    unsigned int ParseData(char *data, bool isDescendingOrder, Ecore_Thread *thread = nullptr) override;

    const char *mEntityId;
    GraphObject *mGraphObj;

    static const unsigned int LIMIT_BUFFER_SIZE;
    static const unsigned int REQUEST_FEEDS_LIMIT;
    static const unsigned int REQUEST_FEED_COMMENTS_LIMIT;
    static const char *FEED_MAIN_REQUEST_PART;
    static const char *FEED_DEFAULT_ID;
};

#endif /* FEEDPROVIDER_H_ */
