/*
 * EventBatchProvider.h
 *
 *  Created on: Aug 19, 2015
 *      Author: ruibezruch
 */

#ifndef EVENTBATCHPROVIDER_H_
#define EVENTBATCHPROVIDER_H_

#include "AbstractDataProvider.h"
#include "Event.h"

class EventBatchProvider: public AbstractDataProvider
{
    EventBatchProvider();
public:
    static EventBatchProvider* GetInstance();
    virtual ~EventBatchProvider();

    void SetSelectorId(const char *text);
    const char * GetSelectorId();

    virtual void DeleteItem(void *item);
    virtual UPLOAD_DATA_RESULT UploadData(bool isDescendingOrder = true);

    void EventStatusSet(const void *object, Event::EventRsvpStatus status);
    void ReSetDataByEvent(Event *event_updated);

    static const char *SELECTOR_UPCOMING;
    static const char *SELECTOR_INVITES;
    static const char *SELECTOR_SAVED;
    static const char *SELECTOR_PAST;
    static const char *SELECTOR_HOSTED;

private:
    virtual unsigned int ParseData(char *data, bool isDescendingOrder, Ecore_Thread *thread = NULL);
    virtual void ReleaseItem( void* item );

    const char *mSelectorId;
    bool mIsNeedSeparator;
};

#endif /* EVENTBATCHPROVIDER_H_ */
