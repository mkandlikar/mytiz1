/*
 * ChooseFriendProvider.h
 *
 *  Created on: Sep 16, 2015
 *      Author: ruibezruch
 */

#ifndef CHOOSEFRIENDPROVIDER_H_
#define CHOOSEFRIENDPROVIDER_H_

#include "AbstractDataProvider.h"

class ChooseFriendProvider: public AbstractDataProvider
{
public:
    static ChooseFriendProvider* GetInstance();
    virtual ~ChooseFriendProvider();
    virtual void DeleteItem(void* item);
    void SetFindText(const char *text);
    virtual UPLOAD_DATA_RESULT UploadData(bool isDescendingOrder = true);
    virtual Eina_Array* GetData() const;
    void FilterData();
    virtual void CustomErase();
    virtual unsigned int GetDataCount() const;
private:
    ChooseFriendProvider();
    virtual unsigned int ParseData(char *data, bool isDescendingOrder, Ecore_Thread *thread = NULL);
    virtual void ReleaseItem(void* item) {};
    char *GetFirstNameSymbol(const char *name);
    void CreateFriendsData(bool isDescendingOrder);

    const char *mTextToFind;
    Eina_Array *mFilteredData;
};

#endif /* CHOOSEFRIENDPROVIDER_H_ */
