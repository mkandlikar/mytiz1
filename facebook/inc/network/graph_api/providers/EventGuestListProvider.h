/*
 * EventGuestListProvider.h
 *
 *  Created on: Nov 2, 2015
 *      Author: ruibezruch
 */

#ifndef EVENTGUESTLISTPROVIDER_H_
#define EVENTGUESTLISTPROVIDER_H_

#include "EventGuestList.h"
#include "AbstractDataProvider.h"
class User;

class EventGuestListProvider: public AbstractDataProvider
{
protected:
    EventGuestListProvider(const char * requestEdge);
public:
    virtual ~EventGuestListProvider();

    void SetEntityId( const char *id );
    virtual void DeleteItem( void* item );
    virtual UPLOAD_DATA_RESULT UploadData( bool isDescendingOrder = true );
    const Eina_List *GetFriendsList() { return mFriendsList; }
    bool isLoggedInUserHere;
private:
    virtual unsigned int ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread = NULL );
    virtual void ReleaseItem( void* item );
    virtual void CustomErase();

    void SortListByFriends();

    const char *m_EntityId;
    Eina_List *mListToSort;
    Eina_List *mFriendsList;
    const char *mRequestEdge;
};

class EventGoingListProvider: public EventGuestListProvider
{
    friend class EventGuestListFactory;
    EventGoingListProvider();
    static EventGoingListProvider * GetInstance();
public:
    virtual ~EventGoingListProvider();
};

class EventMaybeListProvider: public EventGuestListProvider
{
    friend class EventGuestListFactory;
    EventMaybeListProvider();
    static EventMaybeListProvider * GetInstance();
public:
    virtual ~EventMaybeListProvider();
};

class EventNoReplyListProvider: public EventGuestListProvider
{
    friend class EventGuestListFactory;
    EventNoReplyListProvider();
    static EventNoReplyListProvider * GetInstance();
public:
    virtual ~EventNoReplyListProvider();
};

class EventDeclinedListProvider: public EventGuestListProvider
{
    friend class EventGuestListFactory;
    EventDeclinedListProvider();
    static EventDeclinedListProvider * GetInstance();
public:
    virtual ~EventDeclinedListProvider();
};

#endif /* EVENTGUESTLISTPROVIDER_H_ */
