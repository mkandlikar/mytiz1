#ifndef HOMEPROVIDER_H_
#define HOMEPROVIDER_H_

#include "PostProvider.h"
#include <list>

struct Order;
class Post;

/**
 * @brief  This class implements the Data Provider for Home request data.
 */
class HomeProvider: public PostProvider
{
    HomeProvider();
public:
    static HomeProvider* GetInstance();
    virtual ~HomeProvider();
    virtual void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = NULL);
    void MissCacheRetrieve( bool miss );

protected:
    virtual bool UpdateCacheData(FbRespondGetFeed * feedResponse);

private:
    virtual void CachePostsSequence( Eina_Array *data, bool isDescendingOrder );
    virtual unsigned int RetrieveCachedData();
    virtual std::list<Order*> RetrievePostIds();
    virtual std::string GetMainRequestPart();
    virtual bool IsNeedToHandleOperation(Sptr<Operation> oper);

    static const unsigned int LIMIT_BUFFER_SIZE;
    static const unsigned int REQUEST_HOME_ITEMS_LIMIT;
    static const unsigned int REQUEST_HOME_COMMENTS_LIMIT;
    static const char *HOME_MAIN_REQUEST_PART;
};

#endif /* HOMEPROVIDER_H_ */
