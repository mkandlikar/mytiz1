#ifndef PHOTOGALLERYPROVIDER_H_
#define PHOTOGALLERYPROVIDER_H_

#include "AbstractDataProvider.h"

/**
 * @brief  This class implements the Data Provider for Photo galleries.
 */
class PhotoGalleryProvider : public AbstractDataProvider {
public:
    enum UserPhotosType {
        EUndefinedType,
        ETagged,
        EUploaded
    };

    static PhotoGalleryProvider* GetInstance();
    virtual ~PhotoGalleryProvider();

    void SetAlbumId(const char *id, void* userData);
    void SetUserId(const char *id, UserPhotosType photosType, void* userData);

protected:
    PhotoGalleryProvider();

private:
    enum ProviderMode {
        EUndefinedMode,
        EAlbum,
        EUserPhotos
    };

private:
    virtual UPLOAD_DATA_RESULT UploadData( bool isDescendingOrder = true );
    virtual void DeleteItem( void* item );
    virtual unsigned int ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread = NULL );
    virtual void ReleaseItem( void* item );

    virtual void CustomErase();
    static const char *PHOTOS_MAIN_REQUEST_PART;
    static const char *PHOTOS_FIELDS_LIST;
    static const char *UPLOADED;
    static const char *TAGGED;

private:
    ProviderMode mMode;
    UserPhotosType mUserPhotosType;
    char *mId;
    void *mUserData;
};

#endif /* PHOTOGALLERYPROVIDER_H_ */
