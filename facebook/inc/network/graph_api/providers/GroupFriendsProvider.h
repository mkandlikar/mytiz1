#ifndef GROUPFRIENDSPROVIDER_H_
#define GROUPFRIENDSPROVIDER_H_

#include "AbstractDataProvider.h"

class GroupFriendsProvider: public AbstractDataProvider {
public:
    GroupFriendsProvider();
    virtual ~GroupFriendsProvider();
    static GroupFriendsProvider* GetInstance();
    void SetGroupId(const char *groupId);

private:
    virtual void DeleteItem(void* item);
    virtual unsigned int ParseData(char *data, bool isDescendingOrder, Ecore_Thread *thread = NULL);
    virtual void ReleaseItem( void* item );
    virtual UPLOAD_DATA_RESULT UploadData(bool isDescendingOrder = true);
    virtual void CustomErase();

    const char *mGroupId;
    static const char *REQUEST_MAIN_PART;
};

#endif /* GROUPFRIENDSPROVIDER_H_ */
