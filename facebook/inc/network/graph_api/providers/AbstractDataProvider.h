#ifndef ABSTRACTDATAPROVIDER_H_
#define ABSTRACTDATAPROVIDER_H_

#include "AppEvents.h"
#include "GraphObject.h"
#include "GraphRequest.h"
#include <json-glib/json-glib.h>

#include <list>
#include <sstream>

#include "BaseObject.h"
#include "Operation.h"
#include <eina_lock.h>

class Subscriber;

typedef enum UPLOAD_DATA_RESULT_ {
    NONE_DATA_COULD_BE_RESULT,

    //
    // TOO_EARLY_TO_REQUEST_RESULT means that the requests could not
    // be send to often. And the timer from last request was not expired.
    // So, wait a will and try again later.
    TOO_EARLY_TO_REQUEST_RESULT,
    ONLY_REQUEST_SENT_RESULT,
    ONLY_CACHED_DATA_RESULT
} UPLOAD_DATA_RESULT;

enum EThreadMsgType {
    eInValidAccessToken,
    eNotify,
    eDoJob,
    eUnKnown
};

typedef enum PROVIDER_TYPE_ {
    UNDEFINED_TYPE =-1,
    HOME_PROVIDER,
    FEED_PROVIDER,
    NOTIFICATIONS_PROVIDER,
    COMMENTS_PROVIDER,
    FRIENDS_REQUESTS_BATCH_PROVIDER,
    FRIENDS_PROVIDER,
    WHO_LIKED_PROVIDER
} PROVIDER_TYPE;

class ThreadMessage:public BaseObject {
public:
    ThreadMessage(EThreadMsgType type = eUnKnown, void* data = NULL);
    EThreadMsgType mType;
    void* mData;
};

/**
 * @brief  This class implements the base methods to monitor, save and retrieve the data
 *         as from within the fb servers as from the Cache Manager. To use this class think
 *         in the term of Publisher/Subscriber pattern. So, all users of this class will be
 *         notified about the new data. If user do not want to be notified about the new data,
 *         he/she might be unsubscribed.
 */

class AbstractDataProvider : public BaseObject, public Subscriber
{
    /**
     * @brief  This is a pagination information. Use #NEXT and #PREV constants from #PAGING
     *         JSON level to store this values. This values need to be handled ONLY to determine
     *         is it the last/first page or not. To go through the pages use ONLY cursors attributes.
     *
     * @remark The top edge of pagination is stored within the 'head' reference, the bottom edge is
     *         stored within the 'after' reference. @see @method ParsePagination(..) for pagination
     *         usage.
     */
    typedef struct Pagination_
    {
        char *head;
        char *tail;
        bool isPagingEnabled;
        bool isHeadRequired;
    } Pagination;

    /**
     * @brief  This is a short history of the 2 last requests (one for head edge and one for
     *         bottom edge).
     */
    typedef struct LastRequests_
    {
        //
        // Due to DataProvider might be used several data users, the request attributes
        // might be different for all request edges. So, save requests for each request edge.
        char *headRequest;
        char *tailRequest;
        bool isLastOrderDescending;
        bool isBatchRequest;
        bool isEmptyResponce;
        bool isSkipHeaderPaging;
        // the time, when the last request has been send
        int lastRequestTime_secs;
    } LastRequests;

    /**
     * @brief  This is an enumeration for fb error codes. Use #non-RESPONSE_OK to handle an
     *         appropriate scenario. E.g, if #DEPRICATED_API has been received, we need to
     *         decrease the API version to the older.
     */
    typedef enum FB_RESPONSE_ERROR_ {
        RESPONSE_OK,
        DEPRICATED_API = 12,
        FB_ERROR_INVALID_ACCESS_TOKEN = 190,
        INCORRECT_FB_REQUEST_API = 2500 // Unknown path components
    } FB_RESPONSE_ERROR;

public:
    virtual ~AbstractDataProvider();

    /**
     * @brief  Call this method to subscribe for the incoming data. When the data will be received,
     *         @method Notify() will be called to inform all subscribers.
     */
    void Subscribe( const Subscriber *subscriber );

    /**
     * @brief  Call this method to unsubscribe to be inform about receiving new data. This is a
     *         usual case, when the data will be received only once and the user is already processed
     *         the new portion of data. After that, he/she does not want received more data for some
     *         time.
     */
    void Unsubscribe( const Subscriber *subscriber );

    /**
     * @brief  Check that subscriber was added.
     */
    bool IsSubscribed( const Subscriber *subscriber ) const;

    /**
     * @retval The value for max items for the fb response. The value '0'
     *         must be treated that there is no limitation. As a result the 'limit'
     *         field MUST NOT be included to the request string.
     */
    unsigned int GetLimit() const;
    /**
     * @retval Some of response from fb might to return "total_count" tag. It means
     *         how many items fb contains. If for a particular request 'total_count"
     *         is absent, this method returns #UNDEF_TOTAL_COUNT value, otherwise it
     *         returns the real items count on the fb side.
     */
    int GetItemsTotalCount() const;
    /**
     * @brief  This method sets the state for 'Reverse Chronological' items order.
     *
     * @param  'isReverseChronological' A new state for 'Reverse Chronological' items order.
     */
    void SetReverseChronological( bool isReverseChronological );
    bool IsReverseChronological() const;
    /**
     * @todo   Need to move it to AbstactFeedProvider, when it will be implemented.
     *
     * @brief  This method sets the limit for comments, which might be retrieved.
     *
     * @param  'limit' The value for comment limit. The default value is #UNDEFINED_VALUE
     */
    void SetCommentsLimit( int limit );
    /**
     * @todo   Need to move it to AbstactFeedProvider, when it will be implemented.
     *
     * @brief  This method returns the value for comments limit.
     *
     * @retval The comment's limit will be return. If the comments limit was not specified,
     *         #UNDEFINED_VALUE will be returned.
     */
    int GetCommentsLimit() const;

    /**
     * @todo   Need to move it to AbstactFeedProvider, when it will be implemented.
     *
     * @brief  This method sets the condition for retrieving the summary for comments.
     *
     * @param  'isSummary' If this value is equal to 'true' the summary for comments
     *         will be requested, otherwise will not be requested.
     */
    void SetCommentsSummary( bool isSummary );
    /**
     * @todo   Need to move it to AbstactFeedProvider, when it will be implemented.
     *
     * @brief  This method returns the condition for comment's summary
     *
     * @retval If comment's summary condition is requested, 'true' will be returned,
     *         otherwise 'false'.
     */
    bool IsCommentsSummary() const;

    /**
     * @todo   Need to move it to AbstactFeedProvider, when it will be implemented.
     *
     * @brief  This method sets the limit for likes, which might be retrieved.
     *
     * @param  'limit' The value for likes limit. The default value is #UNDEFINED_VALUE
     */
    void SetLikesLimit( int limit );
    /**
     * @todo   Need to move it to AbstactFeedProvider, when it will be implemented.
     *
     * @brief  This method returns the value for likes limit.
     *
     * @retval The likes's limit will be return. If the comments limit was not specified,
     *         #UNDEFINED_VALUE will be returned.
     */
    int GetLikesLimit() const;

    /**
     * @todo   Need to move it to AbstactFeedProvider, when it will be implemented.
     *
     * @brief  This method sets the condition for retrieving the summary for likes.
     *
     * @param  'isSummary' If this value is equal to 'true' the summary for likes
     *         will be requested, otherwise will not be requested.
     */
    void SetLikesSummary( bool isSummary );
    /**
     * @todo   Need to move it to AbstactFeedProvider, when it will be implemented.
     *
     * @brief  This method returns the condition for likes's summary
     *
     * @retval If likes's summary condition is requested, 'true' will be returned,
     *         otherwise 'false'.
     */
    bool IsLikesSummary() const;

    /**
     * @brief  This method is responsible to process incoming data from fb servers. All
     *         appropriate parsing method (e.g. summary parsing, pagination extraction,
     *         user specific data parsing and so on) will be called within this method. This
     *         is a main method to handle incoming data and notify all subscribers.
     *
     * @param  'data' The pointer to the incoming JSON data. The memory for 'data' will be
     *         deallocated inside this method.
     *
     * @param  'curlCode' The curl specific result code.
     */
    virtual void IncomingData( char* data, char* header, int curlCode, Ecore_Thread *thread );

    /**
     * @brief  This method returns the reference to an array of the specific items (e.g.
     *         posts, friends and so on). Each element of this element is specified by parse
     *         algorithm within the overrided @method  ParseData(..). If there is no data,
     *         the reference to empty array will be return.
     *
     * @ramark If a user of this interface decided that there is not enough data,
     *         call @method UploadData.
     */
    virtual Eina_Array* GetData() const;
    /**
     * @brief This method returns the number of items within the 'm_Data' container ( the
     *        container which could be given by @method GetData().
     *
     * @retval The number of items within the 'm_Data' container.
     */
    virtual unsigned int GetDataCount() const;

    /**
     * @brief  If 'Data Provider' has not contain enough items, call this method to retrieve
     *         more data. When this request will be processed, call @method GetData() to get
     *         the data (there will be old data and new incoming if it possible). This method
     *         implemented to use pagination algorithm.
     *
     * @param  'isDescendingOrder' if this value will be equal to 'true', the next older part
     *         of items will be requested. If this value is equal to 'false' the newest part of
     *         items will be requested. Next and previous parts are handled based of the last
     *         result of pagination information.
     *
     * @todo   When the base implementation of 'Data Provider' will be done, we need to implement
     *         the data retrieving from within the data base cache (e.g. sqlite). If cache
     *         contains up-to-data information, we must not send the data request to fb server.
     *         In any case the are some exceptions (e.g. we must send request to fb server all
     *         time for posts/feeds, because of we need to retrieve the sequence of such items;
     *         we do not know such parameter/sort).
     *
     * @retval If send request was success, 'true' will be return, otherwise 'false'. 'false' value
     *         might be retrieved for a set of cases:
     *         - the 'total value' is equal to size of internal buffer. It means that we downloaded
     *         all information. But, 'total value' is applicable not to all fb requests.
     *         - Too long request to fb server. The request limit is equal to
     *         DataProvider::MAX_REQUEST_LENGTH value.
     *         - It could be pagination problem.
     */
    virtual UPLOAD_DATA_RESULT UploadData( bool isDescendingOrder ) = 0;

    /**
     * @brief This method erases all received data and cleans pagination information.
     */
    void EraseData();

    virtual void DeleteItem( void* item ) = 0;

    virtual Eina_List* SearchItem(const char *condition);

    /**
     * @brief Implemention of Suscriber interface.
     */
    virtual void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = NULL);

    virtual void UpdateInternal(AppEventId eventId, void * data);

    void SwitchContinueRequest();

    /**
     * @beief  This method sets the limit for Items which will be requested from fb server
     *         or from CacheManager. The default value is #DEFAULT_LIMITS_LIMIT. If items
     *         limit is equal to zero, the 'limit' attribute will be not included to the
     *         outgoing request. The 'limit' is used for each request. So, if a user sets up
     *         limit to 10, and he decided to send 3 request, the result items number could be:
     *         3 * 10 = 30 items.
     *
     * @param  'itemsLimit' The value for items limit.
     */
    void SetLimit( unsigned int itemsLimit );

    bool IsDataCached() const;

    void ResetRequestTimer();
    void UpdateTimerToCurrent();

    void SetRequestTimerEnabled( bool isEnabled );
    bool IsRequestTimerEnabled() const;

    bool IsRequestAlive() const;

    void ResetNotifyJob();
    void SetNotifyJob(Ecore_Job *notifyJob);
    void ResetNotifySelfJob();
    void SetNotifySelfJob(Ecore_Job *notifyJob);

    /**
     * @brief  This method remove operation from 'mOperations', remove operation presenter
     * from 'mOperationPresenters', remove it from 'm_Data'
     *
     * @param  'operation' - Operation object.
     */
    void RemoveOperation(Sptr<Operation> operation);

    /**
     * @brief  This method remove all operations and operation presenters objects
     */
    void DeleteAllOperations();

    bool IsContainItem( void *forCompare, Eina_Array *arrayForSearch = NULL );

    static const unsigned int BASE_CONTAINER_SIZE;
    static const unsigned int RESPONSE_CONTAINER_SIZE;
    static const unsigned int NONE_ITEMS;
    static const int UNDEFINED_VALUE;
    /**
     * This is a list of attributes which are used within the request building.
     */
    static const char *FIELDS;
    static const char *ID;
    static const char *URL;
    static const char *CREATED_TIME;
    static const char *ACTIONS;
    static const char *CAPTION;
    static const char *DESCRIPTION;
    static const char *DESCRIPTION_TAGS;
    static const char *FULL_PICTURE;
    static const char *LINK;
    static const char *MESSAGE;
    static const char *MESSAGE_TAGS;
    static const char *DATA;
    static const char *NAME;
    static const char *FIRST_NAME;
    static const char *LAST_NAME;
    static const char *OBJECT_ID;
    static const char *PARENT_ID;
    static const char *PRIVACY;
    static const char *SOURCE;
    static const char *STATUS_TYPE;
    static const char *STORY;
    static const char *STORY_TAGS;
    static const char *TYPE;
    static const char *UPDATED_TIME;
    static const char *PICTURE;
    static const char *COVER;
    static const char *RELATIONSHIP_STATUS;
    static const char *WORK;
    static const char *EDUCATION;
    static const char *SCHOOL;
    static const char *BIRTHDAY;
    static const char *EMPLOYER;
    static const char *POSITION;
    static const char *FROM;
    static const char *TO;
    static const char *TO_PROFILE_TYPE;
    static const char *PLACE;
    static const char *LOCATION;
    static const char *CATEGORY_LIST;
    static const char *CHECKINS;
    static const char *LIKES;
    static const char *LIMIT;
    static const char *SUMMARY;
    static const char *COMMENTS;
    static const char *ATTACHMENT;
    static const char *ATTACHMENTS;
    static const char *SUBATTACHMENTS;
    static const char *MEDIA;
    static const char *TARGET;
    static const char *TITLE;
    static const char *CONTEXT;
    static const char *MUTUAL_FRIENDS;
    static const char *APPLICATION;
    static const char *APPLINK;
    static const char *APPNAME;
    static const char *APPID;
    static const char *VIA;
    static const char *ORDER;
    static const char *REVERSE_CHRONOLOGICAL;
    static const char *FILTER;
    static const char *TOPLEVEL;
    static const char *CAN_COMMENT;
    static const char *CAN_LIKE;
    static const char *CAN_REMOVE;
    static const char *USER_LIKES;
    static const char *COMMENT_COUNT;
    static const char *LIKE_COUNT;
    static const char *IS_HIDDEN;
    static const char *EDIT_ACTIONS;
    static const char *TEXT;
    static const char *EDIT_TIME;
    static const char *QUERY;
    static const char *LOCALE;
    static const char *ATTACHMENTS_LIMIT;
    static const char *CITY;
    static const char *COUNTRY;
    static const char *WITH_TAGS;
    static const char *IMPLICIT_PLACE;


    void SetData(Eina_Array * tmp_Data);
    void SetTotalDataCount(int i);

    /**
     * @brief Searches GraphObject by id.
     * !!!ATTENTION!!! Use this function only if your provider keeps data of GraphObject type.
     */
    GraphObject * GetGraphObjectById(const char * id);
    /**
     * @brief  This method returns list of operation presenters
     */
    Eina_List *GetOperationPresenters() { return mOperationPresenters; }
    void CleanRequest();

    /**
     * @brief  This method appends parsed items to local buffer (to 'm_Data' container). Use this
     *         method when @method ParseData(..) will finish parsing.
     *
     * @param  'array' The items array, which was parsed and must be added to the existing items.
     *
     * @param  'isToEnd' If this condition is equal to 'true', the new items will be added to the
     *         end to the existing items container/sequence, otherwise to the begin.
     */
    void AppendData( const Eina_Array *array, bool isToEnd );

    /**
     * @brief  This method appends items to local buffer (to 'm_Data' container)
     *
     * @param  'list' The items list.
     *
     * @param  'isToEnd' If this condition is equal to 'true', the new items will be added to the
     *         end to the existing items container/sequence, otherwise to the begin.
     */
    void AppendData( Eina_List *list, bool isToEnd );

    /**
     * @brief  This method appends the item to local buffer (to 'm_Data' container)
     *
     * @param  'item' The item.
     *
     * @param  'isToEnd' If this condition is equal to 'true', the new item will be added to the
     *         end to the existing items container/sequence, otherwise to the begin.
     */
    void AppendData( void *item, bool isToEnd );

    PROVIDER_TYPE GetProviderType() { return mProviderType; }

    unsigned int GetPresentersCount() const;

protected:
    AbstractDataProvider();

    typedef enum API_TYPE_ {
        GRAPH_API,
        REST_API
    } API_TYPE;

    /**
     * @brief  The enumeration for Graph API versions. The most of fb request use
     *         the last API version (or might support the latest version). In any
     *         case, there is still some requests (like me/friendrequests) which
     *         does not support the latest. For such cases use API_VER_1_0 version.
     *         The appropriate version will be translated to the string representation
     *         when the request will be created.
     */
    typedef enum FB_API_VERSION_ {
        API_VER_UNVERSIONED, // This version has not string.

        API_VER_1_0,
        //
        // versions: 2.0, 2.1, 2.2 are unused.
        //
        API_VER_2_3,
        API_VER_2_4,

        API_VER_LATEST = API_VER_2_4
    } FB_API_VERSION;

    typedef enum PAGIN_ALGORITHM_ {
        CURSOR_ALGORITHM,
        ITERATORS_ALGORITHM, // use of next/previous fields
        MANUAL_ALGORITHM,

        PAGING_SIZE
    } PAGIN_ALGORITHM;

    void SetPagingAlgorithm( PAGIN_ALGORITHM algorithm );
    /**
     * @brief  This method sets the given version for a custom fb request. The default
     *         value for all requests is API_VER_2_3.
     *
     * @param  'version' An identifier for a specific Graph API version.
     */
    void SetAPIVersion( FB_API_VERSION version );

    /**
     * @brief  This method specifies the request type to FB. The default value for this is
     *         GRAPH_API type.
     *
     * @param  'type' The new value for request type.
     */
    void SetAPIType( API_TYPE type );
    /**
     * @brief  This method finalize forming request to fb server and sends it. Call this method
     *         within the inherited class (for a specific Data Provider) to send a request within
     *         the scope of appropriate pagination order (this condition determined by @param
     *         isDescendingOrder). So, this method must be used to request the sequence of data
     *         (e.g. if the purpose of a request to retrieve the minor data like status, we must
     *         not use this method).
     *
     * @param  'request' The pointer to a request specific data. It must be very specific information
     *          for a particular request (it must be constructed within the specific Data Provider,
     *          like HomeProvider, FeedProvider and so on). This request will be extended to common
     *          attributes within this method (like pagination, limit and so on).
     *
     * @param  'isDescendingOrder' The condition of items order. @see more information for this param
     *          to @method UploadData(..)
     *
     * @retval  If request processed success, it will be send to fb server and result will be 'true',
     *          otherwise 'false'.
     */
    UPLOAD_DATA_RESULT RequestData( const char *request, bool isDescendingOrder, bundle *param = NULL );
    /**
     * @brief  This method remove items from local buffer (to 'm_Data' container)
     *
     * @param  'list' The items list that should be removed (not deleted).
     */
    void RemoveData( Eina_List *list );

    static Eina_Bool RemoveComparator( void *forCompare, void *toCompare );
    /**
     * @brief  This method sets the 'HHTP method' to use in within the outgoing request to fb server.
     *         @see GraphRequest::HttpMethod declaration for a set of possible values. The default value
     *         for all requests is GET method type.
     */
    void SetHttpRequestType( GraphRequest::HttpMethod httpRequestType );
    /**
     * @brief  Implement this method within the scope of inherited class. This method must parse a
     *         specific JSON response from the fb server to build an array of parsed items. When
     *         parsing will be done, call @method AppendData(..) to append new items according the
     *         specified order (@param isDescendingOrder).
     *
     * @param  'object' A reference to a JsonObject of a received data.
     *
     * @param  'isDescendingOrder' The condition of items order. If this value equal to 'true'
     *         these items was received in the Descending Order, otherwise in the ascending order.
     */
    virtual unsigned int ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread = NULL ) = 0;

    virtual void CustomErase(){};

    void SetBatchRequest( bool isBatchRequest );

    virtual bool GetPagingAttributes( std::stringstream &finalRequest, bool isDescendingOrder, bool isExtraParams );

    void SetDataCached( bool isCached, Ecore_Thread *thread = NULL );

    void CleanPaging();
    bool IsItLastPage( JsonObject *object ) const;
    bool IsItFirstPage( JsonObject *object ) const;

    virtual void FillManualPagination(bool isDescendigOrder){};

    virtual void ReleaseItem( void* item ) = 0;
    GraphRequest* SendRequest( const char *request, bundle *param );
    void Notify(AppEventId eventId, BaseObject * data, Ecore_Thread *thread );

    static Eina_Bool KeepItemByPointer(void *forCompare, void *toCompare );
    static Eina_Bool KeepItemById(void *forCompare, void *toCompare );
    static bool idsComparator(const void * object, const void * id);

    void SaveLastRequest( const char *request, bool isDescendingOrder );

    void SetEtag(const char *etag);

    void SetSkipHeaderPagingEnabled(bool isEnable);

    void NotifyErase( Ecore_Thread *thread );

    static void handleThreadMessage_cb(void *data, Ecore_Thread *thread, void* ThreadMsgObj);

    /**
     * @brief  This method appends operation to 'mOperations', create operation presenter
     * and store it in mOperationPresenters, insert to 'm_Data' at the begin
     *
     * @param  'operation' - Operation object.
     */
    void AddOperation(Sptr<Operation> operation);
    void CleanData();

    bool ReplaceGraphObjectById(const char *id, GraphObject *newObject, Eina_Array *dataArray);

    Pagination m_Pagination;

    Eina_Lock m_Mutex;

    PROVIDER_TYPE mProviderType;

private:
    int GetResponseError( JsonObject *object ) const;
    void ParsePagination( JsonObject *object, bool isDescendigOrder );
    void ParseSummary( JsonObject *object );
    void AppendLocale(std::stringstream &request);
    static void notifySelf( void *notif );
    static void notifyUISubscribers( void *notif );
    static void notifyUIErase( void *notif );

    const char *m_PageIdettifier_Prev[PAGING_SIZE];
    const char *m_PageIdettifier_Next[PAGING_SIZE];

    /**
     * @brief m_SkipHeaderPagingEnabled - enable functionality when top is rerequested without cursor.
     * it should be used for post fetching because data updated constantly
     */
    bool m_SkipHeaderPagingEnabled;
    /**
     * @brief m_SkipHeaderPaging - indicates that cursor should not be used
     *  when we try to get data from head
     */
    bool m_SkipHeaderPaging;
    LastRequests m_LastRequests;
    GraphRequest::HttpMethod m_HttpRequestType;
    int m_ItemsTotalCount;
    unsigned int m_ItemsLimit;
    unsigned int m_ItemsResponseCounter;
    GraphRequest *m_Request;
    FB_API_VERSION m_APIVersion;
    API_TYPE m_APIType;
    PAGIN_ALGORITHM m_PagingAlgorithm;
    Eina_List *m_Subscribers;
    Eina_Array *m_Data;
    int m_CommentsLimit;
    int m_LikesLimit;
    bool m_IsLikesSummary;
    bool m_IsCommentsSummary;
    bool m_IsReverseChronological;
    bool m_IsDataCached;
    bool m_IsContinueRequest;
    bool m_IsRequestTimerEnabled;
    char *m_Etag;
    ThreadMessage* mThreadMsgObj;

    Ecore_Job *mNotifyJob;
    Ecore_Job *mNotifySelfJob;

    /**
     * @brief mOperations - store operations that belong to this provider
     */
    std::list<Sptr<Operation>> mOperations;

    /**
     * @brief mOperationPresenters - store operation presenters that used as wrapper for operation to
     * present them like a GraphObject
     */
    Eina_List *mOperationPresenters;

    static const char *API_GRAPH_ADDRESS;
    static const char *API_REST_ADDRESS;
    static const char *ACCESS_TOKEN;
    static const char *LIMIT_FIELD;

    static const char *NEXT;
    static const char *PREV;

    static const char *BEFORE;
    static const char *AFTER;

    static const char *IDS;

    static const char *ERROR;
    static const char *PAGING;
    static const char *CURSORS;
    static const char *CODE;

    static const char *GRAPH_API_VERSION_2_4;
    static const char *GRAPH_API_VERSION_2_3;
    static const char *GRAPH_API_VERSION_1_0;

    static const char *TOTAL_COUNT;

    static const unsigned int DEFAULT_LIMITS_LIMIT;
    static const unsigned int LIMIT_BUFFER_SIZE;
    static const unsigned int MAX_REQUEST_LENGTH;
    static const unsigned int MAX_REQUEST_DELAY;
};

#endif /* ABSTRACTDATAPROVIDER_H_ */
