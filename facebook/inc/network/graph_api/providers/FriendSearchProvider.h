#ifndef FRIENDSEARCHPROVIDER_H_
#define FRIENDSEARCHPROVIDER_H_

#include "AbstractDataProvider.h"

class FoundItem;

class FriendSearchProvider: public AbstractDataProvider
{
    FriendSearchProvider();
public:
    static const int MAX_URL_LEN = 2048;
    static FriendSearchProvider* GetInstance();
    virtual ~FriendSearchProvider();

    void SetFindText(const char *text);
    virtual void DeleteItem(void* item);
    virtual UPLOAD_DATA_RESULT UploadData(bool isDescendingOrder = true);

    FoundItem *GetItemById(const char *id);
private:
    virtual unsigned int ParseData(char *data, bool isDescendingOrder, Ecore_Thread *thread = NULL);
    virtual void ReleaseItem( void* item );

    char *mTextToFind;
};

#endif /* FRIENDSEARCHPROVIDER_H_ */
