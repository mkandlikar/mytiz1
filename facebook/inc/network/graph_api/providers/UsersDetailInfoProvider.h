#ifndef USERSDETAILINFOPROVIDER_H_
#define USERSDETAILINFOPROVIDER_H_

#include "AbstractDataProvider.h"

/**
 * @brief  This class implements the Data Provider for Users detail request data.
 *         This kind of data is the users detail info
 */
class UserDetailInfo;

class UsersDetailInfoProvider: public AbstractDataProvider
{
    UsersDetailInfoProvider();
public:
    static UsersDetailInfoProvider* GetInstance();
    virtual ~UsersDetailInfoProvider();

    bool CheckPaginationInfo( bool isDescendingOrder );
    virtual UPLOAD_DATA_RESULT UploadData( bool isDescendingOrder );

    void SetMutualFriendsLimit( unsigned int limit );

    virtual void DeleteItem( void* item );

    Eina_Array* GetIds() const;

    void SetPositionById(const char *firstId);

    std::string PrepareIdsPage(bool isDescendigOrder);

    void SetIds(const Eina_Array * ids);
    inline int GetIdsSize() { return m_Ids ? eina_array_count(m_Ids) : 0; };

    static bool idsComparator(const void * object, const void * id);

    inline bool GetIsLastRequestInDescendingOrder() { return mLastRequestInDescendingOrder; };
    inline bool GetIsFriendRequestsMode() const { return !mIsPymkMode; };
    inline void SetCarouselMode(bool isPymkMode) { mIsPymkMode = isPymkMode; };
    UserDetailInfo* GetUsersDetailInfoObjectById(const char *userId);

private:
    virtual void FillManualPagination(bool isDescendigOrder);
    virtual unsigned int ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread = NULL );
    virtual void CustomErase();
    virtual void ReleaseItem( void* item );
    void ClearIds();

    int m_IdsTailIndex;
    int m_IdsHeadIndex;
    unsigned int m_MutualFriendsLimit;
    /**
     * @brief required ids which should be taken by clients
     */
    Eina_Array *m_Ids;

    static const unsigned int REQUEST_USERS_DETAIL_INFO_LIMIT;
    static const unsigned int DEFAULT_MUTUAL_FRIENDS_LIMIT;
    bool mLastRequestInDescendingOrder;
    bool mIsPymkMode = false;
};

#endif /* USERSDETAILINFOPROVIDER_H_ */
