#ifndef SUGGESTEDFRIENDSPROVIDER_H_
#define SUGGESTEDFRIENDSPROVIDER_H_

#include "AbstractDataProvider.h"

class SuggestedFriendsProvider: public AbstractDataProvider {
    SuggestedFriendsProvider();
public:
    static SuggestedFriendsProvider* GetInstance();
    virtual ~SuggestedFriendsProvider();

    virtual UPLOAD_DATA_RESULT UploadData(bool isDescendingOrder = true);
private:
    virtual unsigned int ParseData(char *data, bool isDescendingOrder, Ecore_Thread *thread = NULL);
    virtual void ReleaseItem(void* item);
    virtual void DeleteItem(void* item);
};

#endif /* SUGGESTEDFRIENDSPROVIDER_H_ */
