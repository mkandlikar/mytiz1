#ifndef POSTPROVIDER_H_
#define POSTPROVIDER_H_

#include <list>
#include "AbstractDataProvider.h"
#include "FbRespondGetFeed.h"
#include "Operation.h"
#include "PostLoader.h"

struct Order;
class Post;

/**
 * @brief  This class implements the Data Provider for Home request data.
 */
class PostProvider: public AbstractDataProvider, PostLoaderSubscriber
{
public:
    virtual ~PostProvider();

    virtual void DeleteItem( void* item );

    /**
     * @brief Generates fields for GET post request.
     */
    void GenPostRequestFields(std::stringstream &request, int likesLimit = 0, bool isLikesSummary = true,
            int commentsLimit = 0, bool isCommentsSummary = true);

    virtual UPLOAD_DATA_RESULT UploadData( bool isDescendingOrder = true );

    virtual void CustomErase();

    void Update(AppEventId eventId, void * data);

    Post* GetPostById(const char* id);
    Eina_List* GetPostsByAlbumId(const char* albumId);

protected:
    PostProvider();
    virtual bool UpdateCacheData(FbRespondGetFeed * feedResponse);
    virtual unsigned int ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread = NULL );
    virtual void ReleaseItem( void* item );
    virtual void CachePostsSequence( Eina_Array *data, bool isDescendingOrder );
    virtual unsigned int RetrieveCachedData();
    virtual std::list<Order*> RetrievePostIds();
    virtual std::string GetMainRequestPart();
    virtual Post* GetPostByIdFromCache(const char* id);
    virtual void RemoveCompletedPostLoader();
    virtual void LoadComplete(PostLoader *loader, int errorCode);

    /**
     * @brief The GetPostById search post by id in the postsList list
     * @param[in] id - id value
     * @param[in] postsList - list of posts
     * @return post object if post is found, NULL - otherwise
     */
    Post* GetPostById(const char* id, Eina_List * postsList);
    Post* GetPostById(const char* id, Eina_Array * postsArray);
    Eina_List* GetPostsByAlbumId(const char* albumId, Eina_Array * postsArray);
    void UpdateAllChildrenPostsByParent(Post *post);

    static bool isStdListContainOrder( std::list<Order*> &list, Order *element );

    Eina_List * mPostLoaderList;

    unsigned int m_PostLowerBound;
    unsigned int m_PostUpperBound;

    bool mMissCacheRetrieve;

private:
    virtual bool IsNeedToHandleOperation(Sptr<Operation> oper);
    bool IsCachedDataShouldBeSet();
    void UpdateAvatarForAllMyPosts(const char* path);

    static const unsigned int LIMIT_BUFFER_SIZE;
    static const unsigned int REQUEST_HOME_ITEMS_LIMIT;
    static const unsigned int REQUEST_HOME_LIKES_LIMIT;
    static const unsigned int REQUEST_HOME_COMMENTS_LIMIT;
    static const char *MAIN_REQUEST_PART;

    Eina_Array * mSourcePosts;
};

#endif /* POSTPROVIDER_H_ */
