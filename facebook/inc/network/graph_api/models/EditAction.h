#ifndef EDITACTION_H_
#define EDITACTION_H_

#include "PresentableAsPost.h"

class EditAction: public PresentableAsPost {
public:
    EditAction(struct _JsonObject* object);
    EditAction(const EditAction& other);
    ~EditAction();

    bool IsInteractive() override {return false;}
    //From
    std::string GetFromId() override {return "";}
    std::string GetFromName() override {return "";}
    std::string GetFromPicture() override {return "";}
    //To
    const char *GetToId() override {return nullptr;}
    const char *GetToName() override {return nullptr;}
    //Via
    std::string GetViaId() override {return "";}
    std::string GetViaName() override {return "";}
    //With
    std::string GetWithTagsId() override {return "";}
    std::string GetWithTagsName() override {return "";}
    unsigned int GetWithTagsCount() override {return 0u;}
    //Location
    std::string GetLocationId() override {return "";}
    std::string GetLocationName() override {return "";}
    std::string GetLocationType() override {return "";}
    unsigned int GetCheckinsNumber() override {return 0u;}
    std::string GetImplicitLocationName() override {return "";}
    //Content
    std::string GetName() override {return "";}
    std::string GetStory() override {return "";}
    std::list<Tag> GetStoryTagsList() override {return {};}
    const char *GetMessage() override;
    std::list<Tag> GetMessageTagsList() override {return {};}
    std::string GetCaption() override {return "";}
    unsigned int GetLikesCount() override {return 0u;}
    unsigned int GetCommentsCount() override {return 0u;}
    std::string GetApplicationName() override {return "";}
    double GetCreationTime() override {return 0.0;}
    std::list<Friend> GetPostFriendsList()  override {return {};}
    bool IsEdited()  override {return false;}
    std::string GetPrivacyIcon()  override {return "";}
    //Attachment
    std::string GetAttachmentTitle()  override {return "";}
    std::string GetAttachmentTargetUrl()  override {return "";}
    std::string GetAttachmentDescription()  override {return "";}
    std::list<Tag> GetSubattachmentList()  override {return {};}
    bool IsPhotoPost() override {return false;}
    bool IsPhotoAddedToAlbum() override {return false;}
    const char* GetAlbumName() override {return nullptr;}

    const char* mText;
    const char* mEditTime;
};

#endif /* EDITACTION_H_ */
