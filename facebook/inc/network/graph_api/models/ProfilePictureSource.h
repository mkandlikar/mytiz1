/*
 * ProfilePictureSource.h
 *
 *  Created on: May 20, 2015
 *      Author: RUINMKOL
 */

#ifndef PROFILEPICTURESOURCE_H_
#define PROFILEPICTURESOURCE_H_

#include <json-glib/json-glib.h>
/*
 *
 */
class ProfilePictureSource
{
public:
    /**
     * @brief Construct Paging object from JsonObject
     * @param object - Json Object, from which Friend will be constructed
     */
    ProfilePictureSource(JsonObject * object);

    /**
     * @brief Copy constructor
     */
    ProfilePictureSource(const ProfilePictureSource & other);

    /**
     * @brief destruction
     */
    virtual ~ProfilePictureSource();

    //members
    int mHeight;
    bool mIsSilhouette;
    char * mUrl;
    int mWidth;

};

#endif /* PROFILEPICTURESOURCE_H_ */
