#ifndef FRIENDSLIST_H_
#define FRIENDSLIST_H_

#include <Eina.h>

/**
 * @brief This class represents data for FriendsList object received from facebook
 */
class FriendsList {
public:
    FriendsList(JsonObject *object);
    ~FriendsList();
    inline Eina_List *GetList(){return mList;}

private:
    Eina_List *mList;
};
#endif /* FRIENDSLIST_H_ */
