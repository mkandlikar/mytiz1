/*
 * Album.h
 *
 *  Created on: Jun 27, 2015
 *      Author: RUINMKOL
 */

#ifndef ALBUM_H_
#define ALBUM_H_

#include "GraphObject.h"
#include "jsonutilities.h"

class Event;
class User;
class Place;

class Picture: public GraphObject
{
public:
    Picture(JsonObject * object);

    /**
     * @brief Copy constructor
     */
    Picture(const Picture & other);

    virtual ~Picture();

    bool mIsSilhouette;
    const char *mUrl;
};

/**
 * @brief Represents a photo album.
 */
class Album: public GraphObject
{
public:
    /**
     * @brief Construct Album object from JsonObject
     * @param[in] object - Json Object, from which Album will be constructed
     */
    Album(JsonObject * object);

    /**
     * @brief Copy constructor
     */
    Album(const Album & other);

    /**
     * @brief Whether the viewer can upload photos to this album.
     */
    bool mCanUpload;

    /**
     * @brief Number of photos in this album.
     */
    int mCount;

    /**
     * @brief The ID of the album's cover photo.
     */
    const char * mCoverPhoto;

    /**
     * @brief The time the album was initially created.
     */
    const char * mCreatedTime;

    /**
     * @brief The description of the album.
     */
    const char * mDescription;

    /**
     * @brief The event associated with this album.
     */
    Event * mEvent;

    /**
     * @brief  The profile that created the album.
     */
    User * mFrom;

    /**
     * @brief A link to this album on Facebook.
     */
    const char * mLink;

    /**
     * @brief The textual location of the album.
     */
    const char * mLocation;

    /**
     * @brief The title of the album.
     */
    const char * mName;

    /**
     * @brief The place associated with this album.
     */
    Place * mPlace;

    /**
     * @brief The privacy settings for the album.
     */
    const char * mPrivacy;

    /**
     * @brief The type of the album.
     * enum{app, cover, profile, mobile, wall, normal, album}
     */
    const char *mType;

    /**
     * @brief The last time the album was updated.
     */
    const char * mUpdatedTime;

    Picture *mPicture;

private:
    /**
     * @brief Destruction
     */
    virtual ~Album();

};

#endif /* ALBUM_H_ */
