#ifndef TIMER_H_
#define TIMER_H_

#include "Log.h"

#include <chrono>
#include <functional>

#include <Ecore.h>

/**
 * @class Timer
 *
 * @exception All methods of Timer class are marked as noexcept
 *
 * @brief Timer class represents timer which can call callback function in specified time period specified quantity of times.
 *
 * @details Timer class is an OOP-style wrapper around Ecore_Timer. It provides all the same capabilities but doesn't require manual memory
 * management. Also it provides compatibility with std::chrono time types and accepts callback in std::function format, so clients can pass
 * member functions as callbacks.
 */

class Timer final {
    static constexpr unsigned multiplier{1000u};
public:
    using TimerPeriod = std::chrono::duration<unsigned long long, std::ratio<1, multiplier>>;

    template <typename DurationType>
    Timer(DurationType duration, std::function<void(void)> callback) noexcept:
        mNativeTimer(nullptr),
        mCallback(callback),
        mRepeats(0u) {
        mPeriod = std::chrono::duration_cast<TimerPeriod>(duration);
        Log::verbose_tag(TimerLogTag, "Timer::Timer period: `%d`", mPeriod.count());
    }
    Timer(const Timer&) = delete;
    Timer(Timer&& other) noexcept;
    ~Timer() noexcept;
    template <typename DurationType>
    void SetInterval(DurationType duration) noexcept {
        mPeriod = std::chrono::duration_cast<TimerPeriod>(duration);
        if(mNativeTimer) ecore_timer_interval_set(mNativeTimer, mPeriod.count() / multiplier);
    }
    template <typename DurationType = TimerPeriod>
    DurationType GetInterval() noexcept {
        return std::chrono::duration_cast<DurationType>(mPeriod);
    }
    void Start(unsigned int repeats = 1) noexcept;
    void Stop() noexcept;
    void Freeze() noexcept;
    void Thaw() noexcept;
    void Reset() noexcept;
    bool IsSet() const noexcept;
    unsigned int GetLeftRepeats() const noexcept;
    template <typename DurationType = TimerPeriod>
    DurationType GetLeftTime() const noexcept {
        if(mNativeTimer) {
            TimerPeriod leftTime((unsigned long long)(ecore_timer_pending_get(mNativeTimer) * multiplier));
            return std::chrono::duration_cast<DurationType>(leftTime);
        } else {
            return 0ull;
        }
    }
private:
    static const char TimerLogTag[];
    static Eina_Bool NativeTimerCallback(void*) noexcept;
    TimerPeriod mPeriod;
    Ecore_Timer* mNativeTimer;
    std::function<void(void)> mCallback;
    unsigned int mRepeats;
};

#endif /* TIMER_H_ */
