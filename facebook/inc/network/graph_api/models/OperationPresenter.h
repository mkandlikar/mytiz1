#ifndef OPERATIONPRESENTER_H_
#define OPERATIONPRESENTER_H_

#include "Operation.h"
#include "PresentableAsPost.h"
#include "Comment.h"

class OperationPresenter: public PresentableAsPost
{
public:
    OperationPresenter(Sptr<Operation> oper, Comment* comment = nullptr);
    ~OperationPresenter() override;
    inline Sptr<Operation> GetOperation() { return mOperation; }

    bool IsInteractive() override;
    //From
    std::string GetFromId() override;
    std::string GetFromName() override;
    std::string GetFromPicture() override;
    //To
    const char *GetToId() override {return nullptr;}
    const char *GetToName() override {return nullptr;}
    //Via
    std::string GetViaId() override;
    std::string GetViaName() override;
    //With
    std::string GetWithTagsId() override;
    std::string GetWithTagsName() override;
    unsigned int GetWithTagsCount() override;
    //Location
    std::string GetLocationId() override;
    std::string GetLocationName() override;
    const char* GetAlbumName() override;
    std::string GetLocationType() override;
    unsigned int GetCheckinsNumber() override;
    std::string GetImplicitLocationName() override;
    //Content
    std::string GetName() override;
    std::string GetStory() override;
    std::list<Tag> GetStoryTagsList() override;
    const char *GetMessage() override;
    std::list<Tag> GetMessageTagsList() override;
    std::string GetCaption() override;
    unsigned int GetLikesCount() override;
    unsigned int GetCommentsCount() override;
    std::string GetApplicationName() override;
    double GetCreationTime() override;
    std::list<Friend> GetPostFriendsList() override;
    bool IsEdited() override;
    std::string GetPrivacyIcon() override;
    //Attachment
    std::string GetAttachmentTitle() override;
    std::string GetAttachmentTargetUrl() override;
    std::string GetAttachmentDescription() override;
    std::list<Tag> GetSubattachmentList() override;

    bool IsPhotoPost() override;
    bool IsPhotoAddedToAlbum() override {return false;}

    inline Comment* GetEditedComment() {return mEditedComment;}

private:
    struct TagPlace;
    TagPlace GetTagPlace(const std::string& message, const std::string& tagName);
    std::map<std::string, std::string> mPrivacyIconMapping;
    Sptr<Operation> mOperation;
    Comment* mEditedComment;//To Create Replies List for Comments presenter
};

#endif /* OPERATIONPRESENTER_H_ */
