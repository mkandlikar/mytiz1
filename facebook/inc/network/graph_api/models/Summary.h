/*
 * Summary.h
 *
 *  Created on: Apr 14, 2015
 *      Author: RUINMKOL
 */

#ifndef SUMMARY_H_
#define SUMMARY_H_

#include <json-glib/json-glib.h>

/**
 * @brief This class contains data received from Facebook
 */
class Summary
{
public:
    /**
     * @brief Construct Summary object from JsonObject
     * @param object - Json Object, from which Summary will be constructed
     */
    Summary(JsonObject * object);

    /**
     * @brief Simple constructor. Sets default values for variables.
     */
    Summary();

    /**
     * @brief Destructor
     */
    virtual ~Summary();

    /**
     * @brief The total count of likes
     */
    int mTotalCount;
};

/**
 * @brief This class contains Likes Summary data received from Facebook
 */
class LikesSummary : public Summary
{
public:
    /**
     * @brief Construct LikesSummary object from JsonObject
     * @param object - Json Object, from which LikesSummary will be constructed
     */
    LikesSummary(JsonObject * object);

    /**
     * @brief Simple constructor. Sets default values for variables.
     */
    LikesSummary();

    /**
     * @brief Copy constructor
     */
    LikesSummary(const LikesSummary & other);

    /**
     * @brief Destructor
     */
    virtual ~LikesSummary();

    /**
     * @brief Indicates whether you can like facebook object
     */
    bool mCanLike;

    /**
     * @brief Indicates whether you already liked facebook object
     */
    bool mHasLiked;

};

#endif /* SUMMARY_H_ */
