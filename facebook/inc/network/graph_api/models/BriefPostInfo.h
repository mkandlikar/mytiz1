/*
 * BriefPostInfo.h
 *
 * Created: Nov 20, 2015
 * Author: Dmitry.Shcherbakov@SymphonyTeleca.com
 */

#ifndef BRIEFPOSTINFO_H_
#define BRIEFPOSTINFO_H_

#include "GraphObject.h"
#include "jsonutilities.h"

class BriefPostInfo : public GraphObject
{
public:
    BriefPostInfo( JsonObject *object );
    virtual ~BriefPostInfo();

    unsigned int GetCommentsCount() const;
    unsigned int GetLikesCount() const;
    double GetUpdatedTime() const;
    bool GetHasLiked() const;
    const char * GetLikeFromId() const;
    const char * GetLikeFromName() const;

private:
    unsigned int m_CommentsCount;
    unsigned int m_LikesCount;
    bool m_HasLiked;
    double m_UpdatedTime;
    const char * m_LikeFromId;
    const char * m_LikeFromName;
};

#endif /* BRIEFPOSTINFO_H_ */
