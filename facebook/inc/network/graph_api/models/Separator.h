#ifndef SEPARATOR_H_
#define SEPARATOR_H_

#include <GraphObject.h>

class Separator: public GraphObject
{
public:

    typedef enum SeparatorType_ {
        FRIENDS_SEPARATOR,
        PEOPLE_YOU_MAY_KNOW_SEPARATOR,
        BDAY_ITEMS,
        FRIENDS_NOREQUEST_SEPARATOR,
        NEW_NOTI_SEPARATOR,
        EARLIER_NOTI_SEPARATOR,
        OTHERS_SEPARATOR
    } SeparatorType;

    Separator( SeparatorType type, const char *custom_text = NULL);
    SeparatorType GetSeparatorType() const;
    char *mCustomText;

private:
    virtual ~Separator();
    SeparatorType m_SeparatorType;
};

#endif /* SEPARATOR_H_ */
