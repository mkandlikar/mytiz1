/*
 * Place.h
 *
 *  Created on: Jun 29, 2015
 *      Author: RUINMKOL
 */

#ifndef PLACE_H_
#define PLACE_H_

#include "GraphObject.h"
#include "Location.h"

/**
 * @brief Represents place Facebook objects.
 */
class Place: public GraphObject
{
public:
    /**
     * @brief Construct Place object from JsonObject
     * @param object - Json Object, from which Place will be constructed
     */
    Place(JsonObject * object);

    /**
     * @brief Copy constructor
     */
    Place(const Place & other);

    /**
     * @brief Destruction
     */
    virtual ~Place();

    //members
    /**
     * @brief Location of Place
     */
    Location * mLocation;

    /**
     * @brief Name
     */
    const char * mName;
    /**
     * @brief Category
     */
    const char * mCategory;
};

#endif /* PLACE_H_ */
