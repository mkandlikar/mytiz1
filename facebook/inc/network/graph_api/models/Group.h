/*
 * Group.h
 *
 *  Created on: Jun 27, 2015
 *      Author: RUINMKOL
 */

#ifndef GROUP_H_
#define GROUP_H_

#include "CoverPhoto.h"
#include "GraphObject.h"


/**
 * @brief Represents a Facebook group.
 */

class Group: public GraphObject
{
public:
    enum MembershipState {
        eNOT_AVAILABLE,
        eMEMBER,
        eCAN_REQUEST,
        eREQUESTED,
        eOWNER
    };

    enum Privacy {
        eCLOSED,
        eOPEN,
        eSECRET
    };
    /**
     * @brief Construct Group object from JsonObject
     * @param[in] object - Json Object, from which Group will be constructed
     */
    Group(JsonObject * object);

    /**
     * @brief Copy constructor
     */
    Group(const Group & other);

    virtual void SetStringForDataType(const char *str, int dataType, int index);

    /**
     * @brief Destruction
     */
    virtual ~Group();

    const char * GetPrivacyAsStr() const;

    /**
     * @brief Information about the group's cover photo.
     */
    CoverPhoto * mCover;
    char * mPictureUrl;

    /**
     * @brief A brief description of the group.
     */
    const char * mDescription;

    /**
     * @brief The URL for the group's icon.
     */
    const char * mIcon;

    /**
     * @brief The state of member request. Can be "MEMBER" or "CAN_REQUEST" etc. Is used for creating buttons layout after redirecting to group profile page
     */
    MembershipState mMembershipState;

    /**
     * @brief The name of the group.
     */
    const char * mName;

    /**
     * @brief The privacy setting of the group.
     */
    Privacy mPrivacy;

    /**
    * @brief Group's members count
    */
    int mMembersCount;

    int mMembersRequestCount;

    GraphObject *mOwner;
};

#endif /* GROUP_H_ */
