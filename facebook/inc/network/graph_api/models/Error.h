#ifndef ERROR_H_
#define ERROR_H_

#include <json-glib/json-glib.h>

/**
 * This class contains data received from Facebook
 */
class Error
{
public:
    typedef enum ErrorType_ {
        eUnknown
        ,eOAuthException
        ,eGraphMethodException
    } ErrorType;

    /**
     * @brief Construct Error object from JsonObject
     * @param[in] object - Json Object, from which Error will be constructed
     */

    Error(JsonObject * object);
    virtual ~Error();

    const char * mMessage;
    ErrorType mType;
    int mCode;
    int mErrorSubcode;
};



#endif /* ERROR_H_ */
