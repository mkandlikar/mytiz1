#ifndef BASEOBJECT_H_
#define BASEOBJECT_H_

#include <utility>

/**
 * @class BaseObject
 *
 * @brief Class BaseObject is a base class adding intrusive reference counting capabilities to its' descendants.
 *
 * @details Class BaseObject is a base class adding intrusive reference counting capabilities to its' descendants.
 * It's highly recommended to use it in pair with Sptr smart pointer which provided automation of reference counter
 * increment and decrement. Also it's highly recommended to create new instances of BaseObject descendants via
 * MakeSptr() function.
 */
class BaseObject {
public:
    BaseObject();
    virtual unsigned int AddRef();
    virtual unsigned int Release();
    virtual unsigned int GetRefCount() { return mRefCount;};

protected:
    virtual ~BaseObject();

private:
    unsigned int mRefCount;
};

/**
 * @class Sptr
 *
 * @exception All methods of Sptr are marked as noexcept.
 *
 * @brief Class Sptr is a smart pointer providing reference counting automation for BaseObject descendants.
 *
 * @details Sptr is a shared pointer, which increases reference count of owned object in its constructor and
 * decreases it in its destructor. It behaves fully as a usual pointer. It's highly recomended to pass Sptr
 * as a function parameter by value, not by reference to let it count real quantity of references.
 */
template<typename BaseObjectDescendant>
class Sptr {
public:
    Sptr(BaseObject* obj) noexcept : mObject(obj) {
        if(mObject) mObject->AddRef();
    }
    Sptr(const Sptr<BaseObjectDescendant>& other) noexcept : mObject(other.mObject) {
        if(mObject) mObject->AddRef();
    }
    template<typename BaseObjectDescendant2>
    Sptr(const Sptr<BaseObjectDescendant2>& other) noexcept: mObject(const_cast<BaseObjectDescendant*>(static_cast<const BaseObjectDescendant*>(other.Get()))) {
        if(mObject) mObject->AddRef();
    }
    Sptr(Sptr<BaseObjectDescendant>&& other) noexcept : mObject(other.mObject) {
        other.mObject = nullptr;
    }
    void operator=(const std::nullptr_t&) {
        if(mObject) {
            mObject->Release();
            mObject = nullptr;
        }
    }
    void operator=(const Sptr<BaseObjectDescendant>& other) noexcept {
        if(mObject) mObject->Release();
        mObject = other.mObject;
        if(mObject) mObject->AddRef();
    }
    template<typename BaseObjectDescendant2>
    void operator=(const Sptr<BaseObjectDescendant2>& other) noexcept {
        if(mObject) mObject->Release();
        mObject = const_cast<BaseObjectDescendant*>(static_cast<const BaseObjectDescendant*>(other.Get()));
        if(mObject) mObject->AddRef();
    }
    void operator=(Sptr<BaseObjectDescendant>&& other) noexcept {
        if(mObject) mObject->Release();
        mObject = other.mObject;
        other.mObject = nullptr;
    }
    ~Sptr() noexcept {
        if(mObject) mObject->Release();
    }
    BaseObjectDescendant* operator->() noexcept {
        return static_cast<BaseObjectDescendant*>(mObject);
    }
    const BaseObjectDescendant* operator->() const noexcept {
        return static_cast<const BaseObjectDescendant*>(mObject);
    }
    BaseObjectDescendant& operator*() noexcept {
        return *(static_cast<BaseObjectDescendant*>(mObject));
    }
    const BaseObjectDescendant& operator*() const noexcept {
        return *(static_cast<const BaseObjectDescendant*>(mObject));
    }
    BaseObjectDescendant* Get() noexcept {
        return static_cast<BaseObjectDescendant*>(mObject);
    }
    const BaseObjectDescendant* Get() const noexcept {
        return static_cast<const BaseObjectDescendant*>(mObject);
    }
    operator bool() const noexcept {
        return mObject != nullptr;
    }
protected:
    BaseObject* mObject;
};

// equality
template<typename BaseObjectDescendant>
bool operator==(const Sptr<BaseObjectDescendant>& sptr, const BaseObjectDescendant* rawPtr) noexcept {
    return sptr.Get() == rawPtr;
}

template<typename BaseObjectDescendant, typename BaseObjectDescendant2>
bool operator==(const Sptr<BaseObjectDescendant>& sptr, const BaseObjectDescendant2* rawPtr) noexcept {
    return sptr.Get() == static_cast<const BaseObjectDescendant*>(rawPtr);
}

template<typename BaseObjectDescendant>
bool operator==(const Sptr<BaseObjectDescendant>& first, const Sptr<BaseObjectDescendant>& second) noexcept {
    return first.Get() == second.Get();
}

template<typename BaseObjectDescendant, typename BaseObjectDescendant2>
bool operator==(const Sptr<BaseObjectDescendant>& first, const Sptr<BaseObjectDescendant2>& second) noexcept {
    return first.Get() == static_cast<const BaseObjectDescendant*>(second.Get());
}

template<typename BaseObjectDescendant>
bool operator==(const BaseObjectDescendant* rawPtr, const Sptr<BaseObjectDescendant>& sptr) noexcept {
    return rawPtr == sptr.Get();
}

template<typename BaseObjectDescendant, typename BaseObjectDescendant2>
bool operator==(const BaseObjectDescendant* rawPtr, const Sptr<BaseObjectDescendant2>& sptr) noexcept {
    return rawPtr == static_cast<const BaseObjectDescendant*>(sptr.Get());
}

// non-equality
template<typename BaseObjectDescendant>
bool operator!=(const Sptr<BaseObjectDescendant>& sptr, const BaseObjectDescendant* rawPtr) noexcept {
    return sptr.Get() != rawPtr;
}

template<typename BaseObjectDescendant, typename BaseObjectDescendant2>
bool operator!=(const Sptr<BaseObjectDescendant>& sptr, const BaseObjectDescendant2* rawPtr) noexcept {
    return sptr.Get() != static_cast<const BaseObjectDescendant*>(rawPtr);
}

template<typename BaseObjectDescendant>
bool operator!=(const Sptr<BaseObjectDescendant>& first, const Sptr<BaseObjectDescendant>& second) noexcept {
    return first.Get() != second.Get();
}

template<typename BaseObjectDescendant, typename BaseObjectDescendant2>
bool operator!=(const Sptr<BaseObjectDescendant>& first, const Sptr<BaseObjectDescendant2>& second) noexcept {
    return first.Get() != static_cast<const BaseObjectDescendant*>(second.Get());
}

template<typename BaseObjectDescendant>
bool operator!=(const BaseObjectDescendant* rawPtr, const Sptr<BaseObjectDescendant>& sptr) noexcept {
    return rawPtr != sptr.Get();
}

template<typename BaseObjectDescendant, typename BaseObjectDescendant2>
bool operator!=(const BaseObjectDescendant* rawPtr, const Sptr<BaseObjectDescendant2>& sptr) noexcept {
    return rawPtr != static_cast<const BaseObjectDescendant*>(sptr.Get());
}

// less
template<typename BaseObjectDescendant>
bool operator<(const Sptr<BaseObjectDescendant>& first, const Sptr<BaseObjectDescendant>& second) noexcept {
    return first.Get() < second.Get();
}

template<typename BaseObjectDescendant>
bool operator<(const Sptr<BaseObjectDescendant>& sptr, const BaseObjectDescendant* rawPtr) noexcept {
    return sptr.Get() < rawPtr;
}

template<typename BaseObjectDescendant>
bool operator<(const BaseObjectDescendant* rawPtr,  const Sptr<BaseObjectDescendant>& sptr) noexcept {
    return rawPtr < sptr.Get();
}

// less or equal
template<typename BaseObjectDescendant>
bool operator<=(const Sptr<BaseObjectDescendant>& first, const Sptr<BaseObjectDescendant>& second) noexcept {
    return first.Get() <= second.Get();
}

template<typename BaseObjectDescendant>
bool operator<=(const Sptr<BaseObjectDescendant>& sptr, const BaseObjectDescendant* rawPtr) noexcept {
    return sptr.Get() <= rawPtr;
}

template<typename BaseObjectDescendant>
bool operator<=(const BaseObjectDescendant* rawPtr,  const Sptr<BaseObjectDescendant>& sptr) noexcept {
    return rawPtr <= sptr.Get();
}

// greater
template<typename BaseObjectDescendant>
bool operator>(const Sptr<BaseObjectDescendant>& first, const Sptr<BaseObjectDescendant>& second) noexcept {
    return first.Get() > second.Get();
}

template<typename BaseObjectDescendant>
bool operator>(const Sptr<BaseObjectDescendant>& sptr, const BaseObjectDescendant* rawPtr) noexcept {
    return sptr.Get() > rawPtr;
}

template<typename BaseObjectDescendant>
bool operator>(const BaseObjectDescendant* rawPtr,  const Sptr<BaseObjectDescendant>& sptr) noexcept {
    return rawPtr > sptr.Get();
}

// greater or equal
template<typename BaseObjectDescendant>
bool operator>=(const Sptr<BaseObjectDescendant>& first, const Sptr<BaseObjectDescendant>& second) noexcept {
    return first.Get() >= second.Get();
}

template<typename BaseObjectDescendant>
bool operator>=(const Sptr<BaseObjectDescendant>& sptr, const BaseObjectDescendant* rawPtr) noexcept {
    return sptr.Get() >= rawPtr;
}

template<typename BaseObjectDescendant>
bool operator>=(const BaseObjectDescendant* rawPtr,  const Sptr<BaseObjectDescendant>& sptr) noexcept {
    return rawPtr >= sptr.Get();
}

/**
 * @function MakeSptr
 *
 * @brief Function MakeSptr creates new instance of BaseObject descendant and returns Sptr owning newly created object.
 *
 * @details Function MakeSptr creates new instance of BaseObject descendant and returns Sptr owning newly created object.
 * It's highly recommended to use this function for creation of base object descendants instances, as it helps to avoid
 * leaks some case when exception is thrown.
 */
template<typename BaseObjectDescendant, typename ... Args>
Sptr<BaseObjectDescendant> MakeSptr(Args&& ... args) {
    Sptr<BaseObjectDescendant> ptr(new BaseObjectDescendant(std::forward<Args>(args)...));
    ptr->Release();
    return ptr;
}

#endif
