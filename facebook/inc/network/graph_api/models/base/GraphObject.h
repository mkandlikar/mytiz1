#ifndef GRAPHOBJECT_H_
#define GRAPHOBJECT_H_

#include "BaseObject.h"
#include "GraphRequest.h"

#include <Elementary.h>

typedef void (*AsyncRequestCallback)(void* data, char* respData, int code);
typedef void (*AsyncClientCallback)(void* data, const char *result);


class GraphObject:public BaseObject {
public:
    typedef enum GraphObjectTypeEnum_ {
         GOT_UNKNOWN
        ,GOT_POST
        ,GOT_PHOTO
        ,GOT_COMMENT
        ,GOT_ALBUM
        ,GOT_FRIEND_REQUEST
        ,GOT_PEOPLE_YOU_MAY_KNOW
        ,GOT_EVENT
        ,GOT_GROUP
        ,GOT_SEPARATOR
        ,GOT_FRIEND
        ,GOT_FOUNDITEM
        ,GOT_USERPROFILEDETAIL
        ,GOT_OPERATION_PRESENTER
        ,GOT_USER
        ,GOT_EDIT_ACTION
        ,GOT_NOTIFICATION
    } GraphObjectTypeEnum;

    GraphObject( GraphObjectTypeEnum type = GOT_SEPARATOR );
    virtual ~GraphObject();

    virtual bool operator==(const GraphObject& otherObject) const;

    virtual int GetGraphObjectType() const;
    virtual void SetGraphObjectType( GraphObjectTypeEnum type );

    virtual bool isDataReady(int dataType, int index = -1) {return true;}
    bool GetAsync(int dataType, void* data, AsyncClientCallback callBack);
    virtual const char *GetStringForDataType(int dataType, int index = -1) const {return NULL;}
    virtual void SetStringForDataType(const char *str, int dataType, int index = -1) {return;}
    void CancelAsyncReq(void *clientData);
    int GetHash() const;
    const char* GetId() const;
    void SetId( char *id );

protected:
    virtual int GetAsyncRequestTypeForData(int dataType){return -1;}
    virtual bool ParseAsyncResponse(int reqType, char*, int code) {return false;}
    virtual GraphRequest* SendAsyncRequest(int reqType, void* data, AsyncRequestCallback callback) {return NULL;}

protected:
    int mGraphObjectType;

private:
    struct AsyncClientData {
        AsyncClientCallback mCallBack;
        void *mData;
        int mDataType;
    };

    class AsyncRequestData {
        friend class GraphObject;
    public:
        AsyncRequestData(int reqType, GraphObject* me);
        ~AsyncRequestData();
        void SetGraphRequest(GraphRequest *graphRequest);
        GraphRequest *GetGraphRequest();
    private:
        void AddAsyncClient(int dataType, AsyncClientCallback callBack, void* data);
        void NotifyClients(bool result);
        bool Cancel(void *clientData);
    private:
        GraphRequest *mGraphRequest;
        int mAsyncRequestType;
        Eina_List *mAsyncClients;
        GraphObject *mGraphObject;
    };

private:
    AsyncRequestData* GetActiveAsyncRequest(int reqType);
    static void AsyncRequestCb(void* data, char* respData, int code);

private:
    Eina_List *mActiveAsyncRequests;
    const char *mId;
    int mHash;
};

#endif /* GRAPHOBJECT_H_ */
