#ifndef PRESENTABLEASPOST_H_
#define PRESENTABLEASPOST_H_
#include "GraphObject.h"
#include "Tag.h"
#include "Friend.h"
#include <string>
#include <list>

class PresentableAsPost : public GraphObject{
public:
    virtual ~PresentableAsPost() {}

    virtual bool IsInteractive() = 0;
    //From
    virtual std::string GetFromId() = 0;
    virtual std::string GetFromName() = 0;
    virtual std::string GetFromPicture() = 0;
    //To
    virtual const char *GetToId() = 0;
    virtual const char *GetToName() = 0;
    //Via
    virtual std::string GetViaId() = 0;
    virtual std::string GetViaName() = 0;
    //With
    virtual std::string GetWithTagsId() = 0;
    virtual std::string GetWithTagsName() = 0;
    virtual unsigned int GetWithTagsCount() = 0;
    //Location
    virtual std::string GetLocationId() = 0;
    virtual std::string GetLocationName() = 0;
    virtual std::string GetLocationType() = 0;
    virtual unsigned int GetCheckinsNumber() = 0;
    virtual std::string GetImplicitLocationName() = 0;
    //Content
    virtual std::string GetName() = 0;
    virtual std::string GetStory() = 0;
    virtual std::list<Tag> GetStoryTagsList() = 0;
    virtual const char *GetMessage() = 0;
    virtual std::list<Tag> GetMessageTagsList() = 0;
    virtual std::string GetCaption() = 0;
    virtual unsigned int GetLikesCount() = 0;
    virtual unsigned int GetCommentsCount() = 0;
    virtual std::string GetApplicationName() = 0;
    virtual double GetCreationTime() = 0;
    virtual std::list<Friend> GetPostFriendsList() = 0;
    virtual bool IsEdited() = 0;
    virtual std::string GetPrivacyIcon() = 0;
    //Attachment
    virtual std::string GetAttachmentTitle() = 0;
    virtual std::string GetAttachmentTargetUrl() = 0;
    virtual std::string GetAttachmentDescription() = 0;
    virtual std::list<Tag> GetSubattachmentList() = 0;
    virtual bool IsPhotoPost() = 0;
    virtual bool IsPhotoAddedToAlbum() = 0;
    virtual const char *GetAlbumName() = 0;
};

#endif /* PRESENTABLEASPOST_H_ */
