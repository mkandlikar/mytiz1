#ifndef TAG_H_
#define TAG_H_

#include "Friend.h"
#include <string>

struct Tag {
    std::string mId;
    std::string mName;
    std::string mType;
    int mOffset;
    int mLength;

    Tag();
    Tag(const std::string& id, const std::string& name, const std::string& type, int offset, int length);
    Tag(const Tag& other);
    Tag(Tag&& other);
    Tag(Friend& fr, int offset = 0, int length = 0);
    ~Tag();
    void operator=(const Tag& other);
    bool operator<(const Tag& other);
    static void parse_each_json_object_cb(JsonObject *object, const gchar *memberName, JsonNode *memberNode, gpointer userData);
    static void tags_json_array_to_eina_list(JsonArray* memberArray, Eina_List** list);
};

#endif /* TAG_H_ */
