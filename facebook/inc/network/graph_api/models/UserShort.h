/*
 * UserShort.h
 *
 *  Created on: Aug 5, 2015
 *      Author: RUINREKU
 */

#ifndef USERSHORT_H_
#define USERSHORT_H_

#include <json-glib/json-glib.h>
#include "GraphObject.h"
/*
 *
 */
class UserShort : public GraphObject
{
public:
	UserShort(JsonObject * object);
    virtual ~UserShort();

    void SetName(const char *name) { mName = strdup(name); }
    inline char* GetName() { return mName; }

    void SetPictureUrl(const char *pictureUrl) { mPictureUrl = strdup(pictureUrl); }
    inline char* GetPictureUrl() { return mPictureUrl; }

    inline bool operator == (const UserShort &user) const
    {
        return ( (user.mHash == mHash) &&
                 (!strcmp(user.mName, mName)) );
    }

private:
    int mHash;
    char * mName;
    char * mPictureUrl;
};

#endif /* USERSHORT_H_ */
