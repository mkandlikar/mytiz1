/*
 * StoryAttachment.h
 *
 *  Created on: May 24, 2015
 *      Author: RUINMKOL
 */

#ifndef STORYATTACHMENT_H_
#define STORYATTACHMENT_H_

#include <json-glib/json-glib.h>

#include "Photo.h"

/**
 * @brief Object that the attachment links to
 */
class StoryAttachmentTarget
{
public:
    /**
     * @brief Construction
     * @param object - json object with data
     */
    StoryAttachmentTarget(JsonObject * object);

    /**
     * @brief Copy constructor
     */
    StoryAttachmentTarget(const StoryAttachmentTarget & other);

    /**
     * @brief destruction
     */
    virtual ~StoryAttachmentTarget();

    /**
     * @brief Object id
     */
    const char * mId;

    /**
     * @brief URI of the image
     */
    const char * mUrl;
};

/**
 * @brief  Image source
 * Parent - Photo for full view implementation (ImagesCarouselScreen)
 */
class ImageSource : public Photo
{
public:
    /**
     * @brief Construction
     * @param object - json object with data
     */
    ImageSource(JsonObject * object);

    /**
     * @brief Copy constructor
     */
    ImageSource(const ImageSource & other);

    /**
     * @brief destruction
     */
    virtual ~ImageSource();

    //members
    /**
     * @brief Height of the image
     */
    int mHeight;

    /**
     * @brief Width of the image
     */
    int mWidth;

    inline int GetHeight() { return mHeight; }
    inline int GetWidth() { return mWidth; }
};

/**
 * @brief  Media object (photo, link etc.) contained in the attachment
 */
class StoryAttachmentMedia
{
public:
    /**
     * @brief Construction
     * @param object - json object with data
     */
    StoryAttachmentMedia(JsonObject * object);

    /**
     * @brief Copy constructor
     */
    StoryAttachmentMedia(const StoryAttachmentMedia & other);

    /**
     * @brief destruction
     */
    virtual ~StoryAttachmentMedia();

    //members
    /**
     * @brief An image in the attachment
     */
    ImageSource * mImage;
};


/**
 * @brief This class contains media content associated with a story or comment.
 */
class StoryAttachment
{
public:
    /**
     * @brief Construction
     * @param object - json object with data
     */
    StoryAttachment(JsonObject * object);

    /**
     * @brief Copy constructor
     */
    StoryAttachment(const StoryAttachment & other);

    /**
     * @brief destruction
     */
    virtual ~StoryAttachment();

    //members
    /**
     * @brief Text accompanying the attachment
     */
    const char * mDescription;

    //TODO: implement later if needed
    //    description_tags
    //    Profiles tagged in the text accompanying the attachment
    //    Profile[]

    /**
     * @brief Media object (photo, link etc.) contained in the attachment
     */
    StoryAttachmentMedia * mMedia;

    /**
     * @brief Object that the attachment links to
     */
    StoryAttachmentTarget * mTarget;

    /**
     * @brief Title of the attachment
     */
    const char * mTitle;

    /**
     * @brief Type of the attachment
     */
    const char * mType;

    /**
     * @brief URL of the attachment
     */
    const char * mUrl;
};

#endif /* STORYATTACHMENT_H_ */
