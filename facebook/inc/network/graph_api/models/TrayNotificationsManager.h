#ifndef TRAYNOTIFICATIONSMANAGER_H_
#define TRAYNOTIFICATIONSMANAGER_H_

#include <Ecore.h>
#include <notification.h>

class TrayNotificationsManager {
private:
    TrayNotificationsManager();
public:
    virtual ~TrayNotificationsManager();
    static TrayNotificationsManager* GetInstance();

    notification_h CreateOngoingNotification();
    void DeleteOngoingNotification(notification_h &ongoing_notification);
    void UpdateOngoingNotification(notification_h &ongoing_notification, double progress);

    void CreateNotiNotification(const char *text, void *app_control);

    void DeleteAllNotiNotifications();
    void DeleteAllOngoingNotifications();

    static Eina_Bool on_playnoti_timeout(void*);

    static Ecore_Timer *mPlayNotiTimer;

private:
    void CreateTickerUtilityNotification();

    const static char *FacebookName;
};

#endif /* TRAYNOTIFICATIONSMANAGER_H_*/
