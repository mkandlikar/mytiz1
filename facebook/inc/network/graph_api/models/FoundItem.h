#ifndef FOUNDITEM_H_
#define FOUNDITEM_H_

#include "Person.h"

#include <json-glib/json-glib.h>
/**
 * @brief This class contains user data received from Facebook for FindFriend request
 */
class FoundItem : public Person
{
public:
    /**
     * @brief Construct FoundItem object from JsonObject
     * @param object - Json Object, from which FoundItem will be constructed
     */
    FoundItem(JsonObject * object);

    /**
     * @brief Destructor
     */
    virtual ~FoundItem();

    //members
    /**
     * @brief Type
     */
    const char * mType;

    /**
     * @brief Facebook path
     */
    const char * mPath;

    /**
     * @brief User name
     */
    const char * mText;

    /**
     * @brief Mutual friends count text
     */
    const char * mSubText;

    /**
     * @brief link to photo
     */
    const char * mPhoto;

    /**
     * @brief Category
     */
    const char * mCategory;
};

#endif /* FOUNDITEM_H_ */
