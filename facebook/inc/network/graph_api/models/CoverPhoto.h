#ifndef COVERPHOTO_H_
#define COVERPHOTO_H_

#include <json-glib/json-glib.h>
#include "GraphObject.h"

/**
 * @brief A cover photo for objects in the Graph API. Cover photos are used with Events, Groups, Pages and People.
 */
class CoverPhoto : public GraphObject
{
public:
    /**
     * @brief Construct CoverPhoto object from JsonObject
     * @param[in] object - Json Object, from which CoverPhoto will be constructed
     */
    CoverPhoto(JsonObject * object);

    /**
     * @brief Copy constructor
     */
    CoverPhoto(const CoverPhoto & other);

    /**
     * @brief Destruction
     */
    virtual ~CoverPhoto();

    //members
    /**
     * @brief The ID of the cover photo
     */
    //depricated
    //cover_id

    /**
     * @brief  When non-zero, the cover image overflows horizontally. The value indicates the offset percentage of the total image width from the left [0-100]
     */
    double mOffsetX;

    /**
     * @brief When non-zero, the cover photo overflows vertically. The value indicates the offset percentage of the total image height from the top [0-100]
     */
    double mOffsetY;

    /**
     * @brief Direct URL for the person's cover photo image
     */
    const char * mSource;

    const char * mPhotoPath;
};

#endif /* COVERPHOTO_H_ */
