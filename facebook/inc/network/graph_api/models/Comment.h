/*
 * Comment.h
 *
 *  Created on: May 24, 2015
 *      Author: RUINMKOL
 */

#ifndef COMMENT_H_
#define COMMENT_H_

#include <json-glib/json-glib.h>

#include "GraphObject.h"
#include "ProfilePictureSource.h"
#include "StoryAttachment.h"
#include "Tag.h"
#include "User.h"
#include <list>

/**
 * @brief This class contains data received from facebook for particular comment
 */
class Comment: public GraphObject
{
public:
    enum AsyncDataType {
            EPhotoPath,
            EProfileAvatar,
        };

    /**
     * @brief Construction
     * @param object - json object with data
     */
    Comment(JsonObject * object);

    /**
     * @brief Copy constructor
     */
    Comment(const Comment & other);

    /**
     * @brief destruction
     */
    virtual ~Comment();

    void copy(const Comment & other);

    //members
    /**
     * @brief Link or photo attached to the comment
     */
    StoryAttachment * mAttachment;

    /**
     * @brief Whether the viewer can reply to this comment
     */
    bool mCanComment;

    /**
     * @brief Whether the viewer can remove this comment
     */
    bool mCanRemove;

    /**
     * @brief Whether the viewer can hide this comment
     */
    bool mCanHide;

    /**
     * @brief Whether the viewer can like this comment
     */
    bool mCanLike;

    /**
     * @brief  Number of replies to this comment
     */
    int mCommentCount;

    /**
     * @brief  The time this comment was made
     */
    double mCreatedTime;

    /**
     * @brief The person that made this comment
     */
    User * mFrom;

    /**
     * @brief Number of times this comment was liked
     */
    int mLikeCount;

    /**
     * @brief The comment text
     */
    const char * mMessage;

    /**
     * @brief Whether the viewer has liked this comment
     */
    bool mUserLikes;

    /**
     * @brief list of comment's replies
     */
    Eina_List * mCommentsList;

    std::list<Tag> mMessageTagsList;

    inline const char *GetMessage() { return mMessage; }
    inline int GetRepliesCount() { return mCommentCount; }
    inline int GetLikesCount() { return mLikeCount; }
    inline const char *GetName() { return mFrom ? mFrom->mName : nullptr; }
    inline const char *GetAvatar() { return (mFrom && mFrom->mPicture) ? mFrom->mPicture->mUrl: nullptr; }
    inline User* GetFrom() { return mFrom;}
    std::list<Tag> GetMessageTagsList();
    inline Eina_List *GetCommentsList() { return mCommentsList; }
    void FreeCommentsList();
    void FreeMessageTagsList();
    void SetHasLikedTrue();
    void SetHasLikedFalse();
    void SetMessage(const char *message);
    void SetAttachement(const char *attachement);

    virtual void SetStringForDataType(const char *str, int dataType, int index = -1);

    //empty stubs for valid LikeOperation usage...
    inline void ToggleLike() {};
    inline void SetLikesCount(int count) {};
    inline void SetCanLike(bool canLike) {};
    inline void SetHasLiked(bool hasLiked) {};
    inline void SetLikeFromId(const char* id) {};
    inline void SetLikeFromName(const char* id) {};

};

#endif /* COMMENT_H_ */
