#ifndef POST_H_
#define POST_H_

#include <Elementary.h>
#include <json-glib/json-glib.h>

#include <string>

#include "Album.h"
#include "ErrorHandling.h"
#include "Friend.h"
#include "GraphObject.h"
#include "PresentableAsPost.h"
#include "Tag.h"


class PrivacyOption;

class Privacy {
public:
    Privacy();
    ~Privacy();

    void Copy(Privacy *src);

    void SetValue(const char *str);
    void SetDescription(const char *str);
    void SetFriends(const char *str);
    void SetAllow(const char *str);
    void SetDeny(const char *str);

    inline const char* GetValue() {return mValue;}
    inline const char* GetDescription() {return mDescription;}
    inline const char* GetFriends() {return mFriends;}
    inline const char* GetAllow() {return mAllow;}
    inline const char* GetDeny() {return mDeny;}

    bool CompareWithOption(PrivacyOption* option);

private:
    char *mValue;
    char *mDescription;
    char *mFriends;
    char *mAllow;
    char *mDeny;
};



struct NewFriend {
    NewFriend();
    ~NewFriend();

    const char *mId;
    const char *mName;
    const char *mAvatar;
    const char *mCareer;
    const char *mMutualFriends;
    const char *mWork;
    const char *mEducation;
    const char *mCover;
};



struct ApplicationField {
    const char *mAppLink;
    const char *mAppName;
    const char *mAppId;

    ApplicationField();
    ~ApplicationField();
};

struct Via {
    const char *mName;
    const char *mId;

    Via();
    ~Via();
};

// Below Enum is Used to Identify the Post Related to Notifacation / Group Actions.
enum PostRelatedTo
{
     EUnKnown
    ,EEvents
    ,EGroups
};

class Post : public PresentableAsPost {
public:
    enum AsyncDataType {
        EFriendWork,
        EFriendEducation,
        EFriendCover,
        EFriendPicture,
        EEventInfo,
        EEventCover,
        EPhotoPath,
        EFromPicturePath,
        EFullPicturePath,
        EGroupCover,
        EProfileCover,
        EProfileAvatar,
        EProfileFirstPhoto,
        EProfileFirstFriend,
        EProfileAboutInfo
    };

    struct Place {
        char *mId;
        char *mName;
        char *mCity;
        char *mCountry;
        double mLatitude;
        double mLongitude;
        char *mStreet;
        unsigned int mCheckins;

        Place(const Place& other);
        Place();
        ~Place();
    };

private:
    enum AsyncReqType {
        EUnknownType = -1,
        EFriendDetails,
        EEventDetails,
        EGroupDetails
    };
private:
    void Init();

    int GetAsyncRequestTypeForData(int dataType) override;
    bool isDataReady(int dataType, int index = -1) override;
    GraphRequest* SendAsyncRequest( int reqType, void* data, AsyncRequestCallback callback) override;
    bool ParseAsyncResponse(int reqType, char*, int code) override;
    const char *GetStringForDataType(int dataType, int index = -1) const override;
    void SetStringForDataType(const char *str, int dataType, int index = -1) override;
    void ImagesListUpdate(const char *url);

    bool ParseFriendDetailsResponse (char* data, int code);
    bool ParseEventDetailsResponse (char* data, int code);
    bool ParseGroupDetailsResponse( char* data, int code);
    void CleanTaggedFriends();

private:
    const char *mChildId;   // it is a link to child post
    Post *mParentPost;

    ApplicationField *mApplication;
    const char *mCaption;
    const char *mDescription;
    const char *mFromName;
    const char *mFromId;
    const char *mToName;
    const char *mToId;
    const char *mToProfileType;
    const char *mFromPictureLink;
    const char *mFromPicturePath;
    int mWithTagsCount;
    const char *mWithTagsName;
    const char *mWithTagsId;
    const char *mPlacePictureLink;
    const char *mFullPicture;
    const char *mFullPicturePath;
    const char *mLink;
    const char *mMessage;
    // A list of Tag objects
    Eina_List *mMessageTagsList;
    const char *mName;
    const char *mObjectId;
    const char *mParentId;
    Place *mPlace;
    Place *mImplicitPlace;
    Privacy *mPrivacy;
    // Source of video or link or picture
    const char *mSource;
    const char *mStatusType;
    const char *mStory;
    const char *mProfilePictureStory;
    // A list of Tag objects
    Eina_List *mStoryTagsList;
    const char *mType;
    double mCreatedTime;
    double mUpdatedTime;
    double mLongitude;
    double mLatitude;
    unsigned int mCheckinsNumber;
    int mLikesCount;
    bool mEdited;
    const char *mEditText;
    const char *mEditTime;
    bool mCanLike;
    bool mHasLiked;
    int mCommentsCount;
    bool mCanComment;
    bool mCanShare;
    bool mIsSharedPost;
    int mPhotoCount;
    int mImagesCount;
    int mAutoId;
    Eina_List *mPhotoList;
    Eina_List *mImagesList;
    Eina_List *mTaggedFriendsList;
    Eina_List *mSubattachmentList;
    const char *mPrivacyIcon;
    const char *mLocationAvatarPath;

    bool mIsFriendInfoRequested;
    bool mIsEventInfoRequested;
    NewFriend *mNewFriend;

    bool mIsGroupInfoRequested;

    const char *mSongName;
    const char *mSongArtist;

    const char *mCommunityName;
    const char *mCommunityAvatar;
    const char *mCommunityType;
    const char *mCommunitySubscribers;
    const char *mCommunityWallpaper;

    const char *mComment;

    const char *mLocationPlace;
    const char *mLocationType;
    const char *mLocationVisitors;

    const char *mEventCover;
    const char *mEventInfo;

    const char *mGroupCover;

    const char *mAddedSthg;

    const char *mGroupName;
    const char *mGroupId;
    bool mIsGroupPost;

    const char *mLikeFromName;
    const char *mLikeFromId;

    Via *mVia;

    const char *mAttachmentDescription;
    const char *mAttachmentSrc;
    int mAttachmentHeight;
    int mAttachmentWidth;
    const char *mAttachTargetId;
    const char *mAttachTargetUrl;
    const char *mAttachmentTitle;
    const char *mAttachmentType;
    int mActionsAmount;

    Album *mAlbum;

    PostRelatedTo mPostRelatedTo; // This member is used to segregate the code flow to Display the Event/Group Name beside the From Object name.

    char * GetPrivacyIcon(Privacy *privacy);

public:
    Post(JsonObject *object);
    Post(char **name, char **value);
    virtual ~Post();
    void copy(Post &srcPost);

    void SetPhotoList(Eina_List *photoList) { mPhotoList = photoList; }
    void SetImageList(Eina_List *imageList) { mImagesList = imageList; }
    void SetFromPicturePath(const char * path) { mFromPicturePath = SAFE_STRDUP(path); }
    void SetChildId(const char *id);
    void SetParent(Post *post);
    void ToggleLike();
    void SetMessage(const char *message);
    void SetMessageTagsList(std::list<Tag> messageTagsList);
    inline void SetLikesCount(int count) { mLikesCount = count; }
    inline void SetCanLike(bool canLike) { mCanLike = canLike; }
    inline void SetHasLiked(bool hasLiked) { mHasLiked = hasLiked; }
    inline void SetCommentsCount(int count) { mCommentsCount = count; }
    inline void SetCanComment(bool canComment) { mCanComment = canComment; }
    inline void SetIsSharedPost(bool flag) { mIsSharedPost = flag; }
    inline void SetCanShare(bool canShare) { mCanShare = canShare; }

    inline const char *GetChildId() { return mChildId; }
    inline Post *GetParent() const { return mParentPost; }
    inline int GetAutoId() { return mAutoId; }
    inline ApplicationField *GetApplication() { return mApplication; }
    inline const double GetCreatedTime() { return mCreatedTime; }
    inline const char *GetDescription() { return mDescription; }
    inline const char *GetFromPictureLink() { return mFromPictureLink; }
    inline const char *GetPlacePictureLink() { return mPlacePictureLink; }
    inline const char *GetFullPicture() { return mFullPicture; }
    inline const char *GetFullPicturePath() { return mFullPicturePath; }
    inline const char *GetLink() { return mLink; }
    inline const char *GetObjectId() { return mObjectId; }
    inline const char *GetParentId() { return mParentId; }
    inline Place *GetPlace() { return mPlace; }
    inline Privacy *GetPrivacy() { return mPrivacy; }
    inline const char *GetSource() { return mSource; }
    inline const char *GetStatusType() const { return mStatusType; }
    inline const char *GetType() const { return mType; }
    inline const double GetUpdatedTime() { return mUpdatedTime; }
    inline Via *GetVia() { return mVia; }
    inline bool GetCanLike() { return mCanLike; }
    inline bool GetHasLiked() { return mHasLiked; }
    inline bool GetCanComment() { return mCanComment; }
    inline bool GetCanShare() { return mCanShare; }
    inline bool GetIsSharedPost() { return mIsSharedPost; }
    inline bool GetIsGroupPost() {return mIsGroupPost;}
    bool IsTrulyGroupPost();
    inline const char *GetToProfileType() const {return mToProfileType;}
    inline int GetPhotoCount() { return mPhotoCount; }
    inline int GetImagesCount() { return mImagesCount; }
    inline Eina_List *GetPhotoList() { return mPhotoList; }
    inline int GetPhotoListCount() {return eina_list_count(mPhotoList);}
    inline Eina_List *GetImageList() { return mImagesList; }
    void SetPhotoPath(int index, const char* path);
    const char* GetPhotoPath(int index) const;

    inline const char* GetFromPicturePath() { return mFromPicturePath; }
    inline double GetLongitude() { return mLongitude; }
    inline double GetLatitude() { return mLatitude; }
    void SetLikeFromName(const char * likeFromName);
    inline const char *GetLikeFromName() { return mLikeFromName; }
    void SetLikeFromId(const char * likeFromId);
    inline const char *GetLikeFromId() { return mLikeFromId; }

    inline const char *GetAttachmentSrc() { return mAttachmentSrc; }
    inline int GetAttachmentHeight() { return mAttachmentHeight; }
    inline int GetAttachmentWidth() { return mAttachmentWidth; }
    inline const char *GetAttachmentTargetId() { return mAttachTargetId; }

    inline Place *GetImplicitPlace() { return mImplicitPlace; }

    const char* GetNewFriendId() const;

    const char* GetLocationAvatar() { return mLocationAvatarPath; }

    const char* GetSongName() { return mSongName; }
    const char* GetSongArtist() { return mSongArtist; }

    const char* GetCommunityName() { return mCommunityName; }
    const char* GetCommunityType() { return mCommunityType; }
    const char* GetCommunitySubscribers() { return mCommunitySubscribers; }
    const char* GetCommunityAvatar() { return mCommunityAvatar; }
    const char* GetCommunityWallpaper() { return mCommunityWallpaper; }
    const char* GetCommentText() { return mComment; }
    const char* GetLocationVisitors(){ return mLocationVisitors; }
    const char* GetAddedSthgCount() { return mAddedSthg; }
    const char* GetGroupName() { return mGroupName; }
    const char* GetEventInfoCover() const;
    const char* GetGroupId() { return mGroupId; }

    inline void SetAlbum(Album *album) {mAlbum = album; if (mAlbum) {mAlbum->AddRef();}}
    inline Album *GetAlbum() {return mAlbum;}
    inline const char* GetAlbumPostType() { return mAlbum ? mAlbum->mType : nullptr; }

    Place *ParsePlace(JsonObject *object);
    const char* ParcePlacePicture(JsonObject *object);
    void AppendTaggedFriendsToList(Eina_List *tags);
    inline const Eina_List *GetTaggedFriends() {return mTaggedFriendsList;}

    const char* GetNewFriendMutualFriends() const;
    const char* GetNewFriendAvatar() const;
    const char* GetNewFriendCareer() const;
    const char* GetNewFriendWork() const;
    const char* GetNewFriendEducation() const;
    const char* GetNewFriendCover() const;
    const char* GetEventInfo() const;
    const char* GetEventCover() const;
    void BriefUpdate( double updatedTime, int commentsCount, int likesCount, bool hasLiked, const char * likeFromId, const char * likeFromName );

    bool IsPostWithTarget();
    bool IsCoverPhotoPost();
    bool IsProfilePhotoPost();

    inline void setToId(const char* toId) { mToId = SAFE_STRDUP(toId); }
    inline void setToName(const char* toName) { mToName =  SAFE_STRDUP (toName); }
    inline PostRelatedTo getPostRelatedTo() const { return mPostRelatedTo; }
    inline void setPostRelatedTo(PostRelatedTo postRelatedTo) { mPostRelatedTo = postRelatedTo; }

    inline int GetActionsAmount() { return mActionsAmount; }
    void CopyLikesSummary(Post &sourcePost);

    void SetProfilePictureStory(const char *story);
    void SetStoryTagsList(std::list<Tag> storyTags);

    //PresentableAsPost
    bool IsInteractive() override;
    //From
    std::string GetFromId() override;
    std::string GetFromName() override;
    std::string GetFromPicture() override;
    //To
    const char *GetToId()  override {return mToId;}
    const char *GetToName()  override {return mToName;}
    //Via
    std::string GetViaId() override;
    std::string GetViaName() override;
    //With
    std::string GetWithTagsId() override;
    std::string GetWithTagsName() override;
    unsigned int GetWithTagsCount() override;
    //Location
    std::string GetLocationId() override;
    std::string GetLocationName() override;
    std::string GetLocationType() override;
    unsigned int GetCheckinsNumber() override;
    std::string GetImplicitLocationName() override;
    //Content
    std::string GetName() override;
    std::string GetStory() override;
    std::list<Tag> GetStoryTagsList() override;
    const char *GetMessage() override;
    std::list<Tag> GetMessageTagsList() override;
    std::string GetCaption() override;
    unsigned int GetLikesCount() override;
    unsigned int GetCommentsCount() override;
    std::string GetApplicationName() override;
    double GetCreationTime() override;
    std::list<Friend> GetPostFriendsList() override;
    bool IsEdited() override;
    std::string GetPrivacyIcon() override;
    //Attachment
    std::string GetAttachmentTitle() override;
    std::string GetAttachmentType();
    std::string GetAttachmentTargetUrl() override;
    std::string GetAttachmentDescription() override;
    std::list<Tag> GetSubattachmentList() override;
    bool IsPhotoPost() override;
    bool IsPostEditable();
    bool IsPhotoAddedToAlbum() override;
    const char* GetAlbumName() override {return mAlbum ? mAlbum->mName : nullptr;}
    bool IsCreatedGroupPost() {return mIsGroupPost && GetStory().find("created the group") != std::string::npos;}
    bool IsSharedLink() const;
    bool IsSharedGroup() const;
};

#endif /* POST_H_ */
