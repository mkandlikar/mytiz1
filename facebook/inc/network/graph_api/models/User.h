#ifndef USER_H_
#define USER_H_

#include <json-glib/json-glib.h>
#include "Person.h"
#include "ProfilePictureSource.h"
/*
 *
 */
class User : public Person
{
public:
    User(JsonObject * object);

    /**
     * @brief Copy constructor
     */
    User(const User & other);

    virtual ~User();

    //members
    const char * mFirstName;
    const char * mLastName;
    const char * mName;
    ProfilePictureSource * mPicture;
    int mMutualFriendsCount;
};

#endif /* USER_H_ */
