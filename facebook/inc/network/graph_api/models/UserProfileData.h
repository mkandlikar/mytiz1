#ifndef USERPROFILEDATA_H_
#define USERPROFILEDATA_H_

#include "CoverPhoto.h"
#include "Person.h"
#include "ProfilePictureSource.h"

class UserProfileData: public Person
{
public:
    enum SubscribeStatus {
        CANNOT_SUBSCRIBE,
        IS_SUBSCRIBED,
        CAN_SUBSCRIBE,

        SUBSCRIBE_UNKNOWN
    };
    UserProfileData(JsonObject *object);
    UserProfileData(const UserProfileData &other);

    virtual void SetStringForDataType(const char *str, int dataType, int index);
    virtual ~UserProfileData();

    void CopyProfileData(const UserProfileData &other);
    void SetProfileDataVariables(const UserProfileData &other);

    void SetFirstPhotoData(JsonObject *object);
    void SetFirstFriendData(JsonObject *object);

    void SetUserAvatarData(JsonObject *object);
    void SetUserAvatarUrl(JsonObject *object);

    void SetAboutPhotoUrl(JsonObject *object);

    CoverPhoto * mCover;
    const char * mName;
    const char * mFirstName;
    const char * mLastName;
    ProfilePictureSource * mPicture;

    SubscribeStatus mSubscribeStatus;

    const char * mFirstPhotoUrl;
    const char * mFirstPhotoPath;

    const char * mFirstFriendUrl;
    const char * mFirstFriendPath;

    const char * mUserAvatarId;
    const char * mUserAvatarPath;

    const char * mAboutPhotoUrl;
    const char * mAboutPhotoPath;
};


#endif /* USERPROFILEDATA_H_ */
