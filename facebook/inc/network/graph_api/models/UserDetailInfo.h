#ifndef USERDETAILINFO_H_
#define USERDETAILINFO_H_

#include "Person.h"

/**
 * @brief This class contains data received from Facebook
 */
class UserDetailInfo : public Person
{
public:
    /**
     * @brief Construct SuggestedFriend object from JsonObject
     * @param object - Json Object, from which SuggestedFriend will be constructed
     */
    UserDetailInfo(JsonObject * object);
    virtual ~UserDetailInfo();
    void Init();

    void SetName(const char *name) { mName = name ? strdup(name) : NULL; }
    inline char* GetName() { return mName; }

    void SetCoverUrl(const char *coverUrl) { mCoverUrl = coverUrl ? strdup(coverUrl) : NULL; }
    inline char* GetCoverUrl() { return mCoverUrl; }

    void SetPictureUrl(const char *pictureUrl) { mPictureUrl = pictureUrl ? strdup(pictureUrl) : NULL; }
    inline char* GetPictureUrl() { return mPictureUrl; }

    void SetBirthday(const char *birthday) { mBirthday = birthday ? strdup(birthday) : NULL; }
    inline char* GetBirthday() { return mBirthday; }

    void SetEducation(const char *education) { mEducation = education ? strdup(education) : NULL; }
    inline char* GetEducation() { return mEducation; }
    
    void SetEducationUrl(const char *url) { mEducationIconUrl = url ? strdup(url) : NULL; }
    inline char* GetEducationUrl() { return mEducationIconUrl; }

    void SetEmployer(const char *employer) { mEmployer = employer ? strdup(employer) : NULL; }
    inline char* GetEmployer() { return mEmployer; }

    void SetWorkIconUrl(const char *url) { mWorkIconUrl = url ? strdup(url) : NULL; }
    inline char* GetWorkIconUrl() { return mWorkIconUrl; }

    void SetLocation(const char *location) { mLocation = location ? strdup(location) : NULL; }
    inline char* GetLocation() { return mLocation; }

    void SetLocationIconUrl(const char *url) { mLocationIconUrl = url ? strdup(url) : NULL; }
    inline char* GetLocationIconUrl() { return mLocationIconUrl; }

    void SetPosition(const char *position) { mPosition = position ? strdup(position) : NULL; }
    inline char* GetPosition() { return mPosition; }

    void SetRelationshipStatus(const char *status) { mRelationshipStatus = status ? strdup(status) : NULL; }
    inline char* GetRelationshipStatus() { return mRelationshipStatus; }

    void SetMutualFriendAvatar(const char *avatar) { mMutualFriendAvatar = avatar ? strdup(avatar) : NULL; }
    inline char* GetMutualFriendAvatar() { return mMutualFriendAvatar; }

    void SetFirstMutualFriendName(const char *name) { mFirstMutualFriendName = name ? strdup(name) : NULL; }
    inline char* GetFirstMutualFriendName() { return mFirstMutualFriendName; }

    void SetSecondMutualFriendName(const char *name) { mSecondMutualFriendName = name ? strdup(name) : NULL; }
    inline char* GetSecondMutualFriendName() { return mSecondMutualFriendName; }

    void SetMutualFriendsCount(int count) { mMutualFriendsCount = count; }
    inline int GetMutualFriendsCount() { return mMutualFriendsCount; }

    bool isClickable;

private:
    char * mName;
    char * mCoverUrl;
    char * mPictureUrl;
    char * mBirthday;
    char * mEducation;
    char * mEducationIconUrl;
    char * mEmployer;
    char * mWorkIconUrl;
    char * mLocation;
    char * mLocationIconUrl;
    char * mPosition;
    char * mRelationshipStatus;
    char * mMutualFriendAvatar;
    char * mFirstMutualFriendName;
    char * mSecondMutualFriendName;
    int mMutualFriendsCount;
};

#endif /* USERDETAILINFO_H_ */
