#ifndef LOCATION_H_
#define LOCATION_H_

#include <memory>
#include <json-glib/json-glib.h>
/**
 * @brief Represents a Facebook location.
 */
class Location
{
public:
    /**
     * @brief Construct Location object from JsonObject
     * @param[in] object - Json Object, from which Location will be constructed
     */
    Location(JsonObject * object);

    /**
     * @brief Copy constructor
     */
    Location(const Location & other);

    /**
     * @brief Destruction
     */
    virtual ~Location();

    std::unique_ptr<char[]> GetAddress();

    //members
    /**
     * @brief City
     */
    const char * mCity;

    /**
     * @brief Country
     */
    const char * mCountry;

    /**
     * @brief Latitude
     */
    double mLatitude;

    /**
     * @brief The parent location if this location is located within another location
     *
     */
    const char * mLocatedIn;

    /**
     * @brief Longitude
     */
    double mLongitude;

    /**
     * @brief Name
     */
    const char * mName;

    /**
     * @brief Region
     */
    const char * mRegion;

    /**
     * @brief State
     */
    const char * mState;

    /**
     * @brief Street
     */
    const char * mStreet;

    /**
     * @brief Zip code
     */
    const char * mZip;
};

#endif /* LOCATION_H_ */
