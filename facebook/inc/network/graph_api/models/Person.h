#ifndef PERSON_H_
#define PERSON_H_

#include "GraphObject.h"
#include "GraphRequest.h"

#include <json-glib/json-glib.h>

class Person: public GraphObject {

private:

    class GraphRequestTracker {
    public:
        GraphRequestTracker(Person *parent);
        ~GraphRequestTracker();

        void RestartRequestTimer();

    private:
        void SendFriendRequest();
        void CancelOutgoingFriendRequest();
        void ReleaseRequests();

        static Eina_Bool on_request_timer_cb(void *data);
        void RepeatAddCancelRequest();

        GraphRequest *mAddFriendRequest = nullptr;
        GraphRequest *mCancelFriendRequest = nullptr;

        Ecore_Timer *mRequestTimer = nullptr;
        Person *mPerson = nullptr;
        bool mUseBigDelay = false;
    };

    GraphRequestTracker *mGraphRequestTracker = nullptr;

protected:
    Person();
    Person(JsonObject *object);
    virtual ~Person();

public:
    enum FriendshipStatus {
        eNOT_AVAILABLE,
        eARE_FRIENDS,
        eCAN_REQUEST,
        eCANNOT_REQUEST,
        eOUTGOING_REQUEST,
        eINCOMING_REQUEST
    };

    FriendshipStatus mFriendshipStatus = eNOT_AVAILABLE;
    FriendRequestStatus mFriendRequestStatus = eUSER_REQUEST_UNKNOWN;

    GraphRequestTracker *GetTracker() { return mGraphRequestTracker; };
};

#endif //PERSON_H_
