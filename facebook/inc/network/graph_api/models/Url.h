/*
 * Url.h
 *
 *  Created on: Jun 18, 2015
 *      Author: RUINMKOL
 */

#ifndef URL_H_
#define URL_H_

#include <json-glib/json-glib.h>
#include "GraphObject.h"

class Share
{
public:
    Share(JsonObject * object);
    virtual ~Share() {};
    int mCommentCount;
    int mShareCount;
};

/*
 *
 */
class Url : public GraphObject
{
public:
    Url(JsonObject * object);
    virtual ~Url();

    Share * mShare;
};

#endif /* URL_H_ */


