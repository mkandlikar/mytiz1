#ifndef EVENT_H_
#define EVENT_H_

#include <json-glib/json-glib.h>

#include "CoverPhoto.h"
#include "GraphObject.h"
#include "Group.h"
#include "Place.h"

/**
 * @brief Represents a Facebook event.
 */
class Event : public GraphObject
{
public:
    enum EventRsvpStatus {
        EVENT_RSVP_ATTENDING,
        EVENT_RSVP_MAYBE,
        EVENT_RSVP_NOT_REPLIED,
        EVENT_RSVP_DECLINED,
        EVENT_RSVP_NONE
    };
    /**
     * @brief Construct Event object from JsonObject
     * @param[in] object - Json Object, from which Event will be constructed
     */
    Event(JsonObject * object);

    /**
     * @brief Copy constructor
     */
    Event(const Event & other);

    virtual void SetStringForDataType(const char *str, int dataType, int index);

    /**
     * @brief Destruction
     */
    virtual ~Event();

    void CopyEvent(const Event &other);

    void SetEventVariables(const Event &other);

    /**
     * @brief The event's cover photo.
     */
    CoverPhoto * mCover;

    /**
     * @brief Long-form description of the event.
     */
    const char * mDescription;

    /**
     * @brief End time of the event, if one has been set.
     */
    double mEndTime;

    const char * mEndTimeString;
    /**
     * @brief Indicates whether the event only has a date specified, but no time.
     */
    bool mIsDateOnly;

    bool mCanGuestsInvite;

    bool mGuestListsEnabled;

    /**
     * @brief The name of the event.
     */
    const char * mName;

    /**
     * @brief The profile that created the event.
     * User|Page|Group
     */
    GraphObject * mOwner;

    /**
     * @brief The group the event belongs to, if any.
     */
    Group * mParentGroup;

    /**
     * @brief Location associated with the event, if any
     */
    Place * mPlace;

    /**
     * @brief Who can see the event.
     */
    const char * mPrivacyStatus;

    const char * mPrivacy;

    /**
     * @brief Start time of the event.
     */
    double mStartTime;

    const char * mStartTimeString;

    /**
     * @brief The link users can visit to buy a ticket to this event. Can only be added to events created by Pages. A user access token is needed for the returned uri to be valid.
     */
    const char * mTicketUri;

    /**
     * @brief Any timezone that the event has.
     */
    const char * mTimezone;

    /**
     * @brief Last time that the event was updated.
     */
    const char * mUpdatedTime;

    EventRsvpStatus mRsvpStatus;

    const char * GetStartTime();
    const char * mStartTimePresenter;

    const char * GetEndTime();
    const char * mEndTimePresenter;

    const char * CutDescription();
    char *mCuttedDescription;

    int mAttendingCount;
    int mMaybeCount;
    int mInvitedCount;
    int mDeclinedCount;

    bool isInvited;

    const char *SetLink();

    const char *mLinkPresenter;
    const char* GetLink();

    bool mNeedTimeShift;

    inline const char *GetTicketURI() { return mTicketUri; }
};

#endif /* EVENT_H_ */
