/*
 * FriendRequest.h
 *
 *  Created on: Jun 22, 2015
 *      Author: RUINMKOL
 */

#ifndef FRIENDREQUEST_H_
#define FRIENDREQUEST_H_

#include "User.h"

/**
 * @brief This class contains data accociated with FriendRequest received from Facebook
 */
class FriendRequest
{
public:
    /**
     * @brief Construct FriendRequest object from JsonObject
     * @param object - Json Object, from which FriendRequest will be constructed
     */
    FriendRequest(JsonObject * object);

    /**
     * @brief Destructor
     */
    virtual ~FriendRequest();

    /**
     * @brief The person who sent the friend request
     */
    User * mFrom;

    /**
     * @brief The person to whom the friend request was sent. This will always be the current user.
     */
    User * mTo;

    /**
     * @brief Time at which the friend request was created
     */
    const char * mCreatedTime;

    /**
     * @brief Message provided by the sender when they sent the request
     */
    const char * mMessage;

    /**
     * @brief Whether the user has read the friend request or not.
     */
    bool mUnread;

};

#endif /* FRIENDREQUEST_H_ */
