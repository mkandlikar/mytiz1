#ifndef NOTIFICATION_H_
#define NOTIFICATION_H_

#include "GraphObject.h"

#include <string>

class NotificationObject: public GraphObject
{
public:
    enum NoitificationType {
        NOTIF_NONE,
        NOTIF_UNKNOWN,
        NOTIF_GROUP,
        NOTIF_FRIEND,
        NOTIF_GAMES,
        NOTIF_VIDEO,
        NOTIF_FEED_COMMENTS,
        NOTIF_WALL,
        NOTIF_LIKES,
        NOTIF_GIFTS,
        NOTIF_PHOTOS,
        NOTIF_FB4T,
        NOTIF_LINKS,
        NOTIF_PAGES
    };
    NotificationObject(JsonObject *object);
    NotificationObject(const NotificationObject & other);

    virtual ~NotificationObject();

    void Init();

    const char *mApplicationCategory;
    const char *mApplicationLink;
    const char *mApplicationName;
    const char *mApplicationId;

    NoitificationType mNotificationType;

    char *mCreatedTime;

    const char *mFromName;
    const char *mFromId;
    const char *mFromPictureUrl;

    const char *mLink;

    const char *mObjectCreatedTime;
    const char *mObjectStory;
    const char *mObjectId;
    const char *mObjectName;

    const char *mTitle;
    std::string mTaggedTitle;

    bool mUnread;

    char *mUpdatedTime;

    /*
     * mCreatedTimeString, mUpdatedTimeString - time of notifications with shift
     */
    const char *mUpdatedTimeString;
    int mUpdatedTimeInSec;
};

#endif /* NOTIFICATION_H_ */
