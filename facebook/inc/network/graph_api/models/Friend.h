#ifndef FRIEND_H_
#define FRIEND_H_

#include "Person.h"
#include "ProtocolBase.h"
#include "User.h"

#include <json-glib/json-glib.h>
#include <string>

/**
 * @brief This class contains data received from Facebook
 */
class Friend : public Person
{
public:
    Friend();
    Friend(Friend&& friendObj);
    Friend(const Friend& friendObj);
    Friend(JsonObject* object);
    Friend(char** name, char** value);
    virtual ~Friend();
    inline std::string GetName() { return mName; }
    inline int GetMutualFriendsCount() { return mMutualFriendsCount; }
    inline std::string GetPicturePath() { return mPicturePath; }

    bool isBdayNextYear;
    std::string mName;
    int mMutualFriendsCount;
    std::string mPicturePath;
    std::string mBirthday;
    std::string mTurningInfo;
    std::string mFirstName;
    std::string mLastName;
    std::string mEmail;
    std::string mPhone;
    MessageId mDownloadImageReqId;
};

#endif /* FRIEND_H_ */
