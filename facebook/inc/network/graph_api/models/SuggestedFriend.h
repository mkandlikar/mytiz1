/*
 * SuggestedFriend.h
 *
 *  Created on: Jul 21, 2015
 *      Author: RUINREKU
 */

#ifndef SUGGESTEDFRIEND_H_
#define SUGGESTEDFRIEND_H_

#include <json-glib/json-glib.h>
#include "GraphObject.h"

/**
 * @brief This class contains data received from Facebook
 */
class SuggestedFriend : public GraphObject
{
public:
    char *mName;

    /**
     * @brief Construct SuggestedFriend object from JsonObject
     * @param object - Json Object, from which SuggestedFriend will be constructed
     */
    SuggestedFriend(JsonObject * object);
    virtual ~SuggestedFriend();

    void SetId(char *id) { GraphObject::SetId( strdup(id) ); }

    void SetName(char *name) { mName = strdup(name); }

    inline char* GetName() { return mName; }
};

#endif /* SUGGESTEDFRIEND_H_ */
