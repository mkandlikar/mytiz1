#ifndef PHOTO_H_
#define PHOTO_H_


#include <json-glib/json-glib.h>
#include "Album.h"
#include "DataServiceProtocol.h"
#include "GraphObject.h"
#include "Place.h"
#include "Post.h"
#include "Summary.h"
#include "Utils.h"


class ImageFileDownloader;

/*
 * Class PhotoTag
 */
class PhotoTag : public BaseObject {
    ~PhotoTag();
public:
    PhotoTag(JsonObject * object);
    PhotoTag(double x, double y);
    // There may be "with friends", but they are not really tagged on the photo.
    bool mIsPhotoTag;
    std::string mTaggedUserName;
    std::string mTaggedUserId;
    double mX;
    double mY;
    std::string mCreatedTime;
    std::string mTagAuthorName;
    std::string mTagAuthorId;
};

/*
 * Class ImageFile
 */
class ImageFile : public BaseObject, private IDataServiceProtocolClient {
public:
    enum Status {
        ENotDownloaded,
        EDownloading,
        EDownloaded
    };

    ImageFile(const char *url, const char *path, unsigned int height = 0, unsigned int width = 0);

    Status GetStatus();

    inline const char *GetPath() const {return mPath;}
    inline const char *GetUrl() const {return mUrl;}
    inline unsigned int GetHeight() const {return mHeight;}
    inline unsigned int GetWidth() const {return mWidth;}

    inline void SetHeight(unsigned int height) {mHeight = height;}
    inline void SetWidth(unsigned int width) {mWidth = width;}
    void SetPath(const char* path);
    void SetUrl(const char* url);

    void DownloadComplete(bool result);

protected:
    ~ImageFile();

    bool IsImageDownloaded();
    friend ImageFileDownloader;
    void Download();
    void StopDownload();
    void AddDownloader(ImageFileDownloader *downloader);
    void RemoveDownloader(ImageFileDownloader *downloader);

private:
    virtual void HandleDownloadImageResponse(ErrorCode error, MessageId requestID, const char* url, const char* fileName);

private:
    const char *mUrl;
    const char *mPath;
    unsigned int mHeight;
    unsigned int mWidth;
    MessageId mRequestID;
    Eina_List *mDownloaders;
};


/*
 * Class ImageFileObserver
 */
class ImageFileDownloader {
public:
    ImageFileDownloader(ImageFile &image);
    virtual ~ImageFileDownloader();

    inline ImageFile &GetImage() const {return mImage;}
    inline void Download() {mImage.Download();}

    virtual void ImageDownloaded(bool result){};

protected:
    inline void Register() {mImage.AddDownloader(this);}
    inline void Unregister() {mImage.RemoveDownloader(this);}

protected:
    ImageFile &mImage;
};


/**
 * @brief Represents an individual photo on Facebook.
 */
class Photo : public GraphObject
{
    //members
    /**
     * @brief The album this photo is in
     */
    Album * mAlbum;

    double mCreateTimeInMilliseconds;

    /**
     * @brief The profile (user or page) that uploaded this photo
     */
    GraphObject * mFrom;

    /**
     * @brief The height of this photo in pixels
     */
    int mHeight;

    /**
     * @brief The icon that Facebook displays when photos are published to News Feed
     */
    const char * mIcon;

    /**
     * @brief The different stored representations of the photo. Can vary in number based upon the size of the original photo.
     *     list<PlatformImageSource>
     */
    Eina_List * mImages;

    /**
     * @brief A link to the photo on Facebook
     */
    const char * mLink;

    /**
     * @brief  The user-provided caption given to this photo. Corresponds to caption when creating photos
     */
    const char * mName;

    /**
     * @brief A list containing an array of objects mentioned in the name field which contain the id, name, and type of each object as well as the offset and length which can be used to match it up with its corresponding string in the name field
     */

    Eina_List *mPhotoMessageTagsList;

    /**
     * @brief ID of the page story this corresponds to. May not be on all photos. Applies only to published photos

     */
    const char * mPageStoryId;

    /**
     * @brief The last time the photo was updated
     */
    const char * mUpdatedTime;

    /**
     * @brief The width of this photo in pixels
     */
    int mWidth;

    /**
     * @brief If this object has a place, the event associated with the place
     */
    Event * mEvent;

    /**
     * @brief Location associated with the photo, if any
     */
    Place * mPlace;

    /**
     * @brief A user-specified time for when this object was created
     */
    const char * mBackdatedTime;

    /**
     * @brief How accurate the backdated time is
     *    // enum
     */
    int mBackdatedTimeGranularity;

    int mCommentsCount;
    bool mCanComment;

    ImageFile *mPostImage;
    /**
     * @brief The number of likes of the Photo.
     */
    int mLikesCount;

    /**
     * @brief Indicates whether you can like the Photo
     */
    bool mCanLike;

    /**
     * @brief Indicates whether you already liked the Photo
     */
    bool mHasLiked;

    /**
     * @brief Id of the first person who liked this photo. Used in Comment screen when likes count is 1
     */
    const char * mLikeFromId;

    /**
     * @brief Name of the first person who liked this photo. Used in Comment screen when likes count is 1
     */
    const char * mLikeFromName;

    const char *mFromId;

    Eina_List * mPhotoTags;

    bool mCanTag;

    Post *mPhotoPost;
protected:
    /**
     * @brief Destruction
     */
    virtual ~Photo();

    void Init();

public:
    /**
     * @brief Construct Photo object from JsonObject
     * @param object - Json Object, from which Photo will be constructed
     */
    Photo(JsonObject * object);

    /**
     * @brief Simple constructor. Sets default values for variables.
     */
    Photo();

    /**
     * @brief Copy constructor
     */
    Photo(const Photo & other);

    void SetPhotoPost(Post *photoPost);

    /**
     * @brief Updates caption, likes info and comments info
     */
    void UpdatePhotoDetails(Photo * other);

    /**
     * @brief Refresh likes count by has liked value.
     */
    void ToggleLike();

    inline Album * GetAlbum() { return mAlbum; }
    inline void SetAlbum(Album * album) { mAlbum = album; }

    inline double GetCreatedTimeInMilliseconds() { return mCreateTimeInMilliseconds; }
    inline void SetCreatedTimeInMilliseconds(double  createdTimeInMillisecs) { mCreateTimeInMilliseconds = createdTimeInMillisecs; }

    inline GraphObject * GetFrom() { return mFrom; }
    inline void SetFrom(GraphObject * from) { mFrom = from; }

    inline int GetHeight() { return mHeight; }
    inline void SetHeight(int height) { mHeight = height; }

    inline const char * GetIcon() { return mIcon; }
    inline void SetIcon(const char * icon) { mIcon = icon; }

    inline Eina_List * GetImages() { return mImages; }
    inline void SetImages(Eina_List * images) { mImages = images; }

    inline const char * GetLink() { return mLink; }

    inline const char * GetName() { return mName; }
    void SetName(const char * name);

    inline const char * GetPageStoryId() { return mPageStoryId; }
    inline void SetPageStoryId(const char * pageStoryId) { mPageStoryId = pageStoryId; }

    inline const char * GetUpdatedTime() { return mUpdatedTime; }
    inline void SetUpdatedTime(const char * updatedTime) { mUpdatedTime = updatedTime; }

    inline int GetWidth() { return mWidth; }
    inline void SetWidth(int width) { mWidth = width; }

    inline Event * GetEvent() { return mEvent; }
    inline void SetEvent(Event * event) { mEvent = event; }

    inline Place * GetPlace() { return mPlace; }
    inline void SetPlace(Place * place) { mPlace = place; }

    inline const char * GetBackdatedTime() { return mBackdatedTime; }
    inline void SetBackdatedTime(const char * backdatedTime) { mBackdatedTime = backdatedTime; }

    inline int GetBackdatedTimeGranularity() { return mBackdatedTimeGranularity; }
    inline void SetBackdatedTimeGranularity(int backdatedTimeGranularity) { mBackdatedTimeGranularity = backdatedTimeGranularity; }

    inline bool GetCanLike() {return mPhotoPost ? mPhotoPost->GetCanLike() : mCanLike;}
    inline void SetCanLike(bool canLike) {if(mPhotoPost) {mPhotoPost->SetCanLike(canLike);} else {mCanLike = canLike;}}

    inline bool GetHasLiked() {return mPhotoPost ? mPhotoPost->GetHasLiked() : mHasLiked;}
    inline void SetHasLiked(bool hasLiked) {if(mPhotoPost) {mPhotoPost->SetHasLiked(hasLiked);} else {mHasLiked = hasLiked;}}

    inline int GetLikesCount() {return mPhotoPost ? mPhotoPost->GetLikesCount() : mLikesCount;}
    inline void SetLikesCount(int likesCount) {if(mPhotoPost) {mPhotoPost->SetLikesCount(likesCount);} else {mLikesCount = likesCount;}}

    inline int GetCommentsCount() {return mPhotoPost ? mPhotoPost->GetCommentsCount() : mCommentsCount;}
    inline void SetCommentsCount(int commentsCount) {if(mPhotoPost) {mPhotoPost->SetCommentsCount(commentsCount);} else {mCommentsCount = commentsCount;}}

    inline bool GetCanComment() {return mPhotoPost ? mPhotoPost->GetCanComment() : mCanComment; }
    inline void SetCanComment(bool canComment) {if(mPhotoPost) {mPhotoPost->SetCanComment(canComment);} else {mCanComment = canComment;}}

    inline const char *GetFilePath() const {return mPostImage ? mPostImage->GetPath() : NULL;}
    void SetFilePath(const char * filePath);

    inline const char *GetSrcUrl() const {return mPostImage ? mPostImage->GetUrl() : NULL;}
    void SetSrcUrl(const char *src);

    ImageFile *GetOptimumResolutionPicture(int width, int height);
    ImageFile *GetMaxResDownloadedImageFile();

    inline const char *GetFileName() const {return Utils::GetIconNameFromUrl(GetSrcUrl());};
    inline const char * GetLikeFromId() {return mPhotoPost ? mPhotoPost->GetLikeFromId() : mLikeFromId;}
    void SetLikeFromId(const char * likeFromId);

    inline const char *GetLikeFromName() {return mPhotoPost ? mPhotoPost->GetLikeFromName() : mLikeFromName;}
    void SetLikeFromName(const char * likeFromName);
    inline const char *GetFromId() { return mFromId; }

    std::list<Tag> GetPhotoMessageTagsList();
    void SetPhotoMessageTagsList(std::list<Tag> photoMessageTagsList);
    void SetPhotoMessageTagsList(Eina_List *photoMessageTagsList);

    inline Eina_List * GetPhotoTagList(){ return mPhotoTags; }
    inline void SetPhotoTagList(Eina_List * list) { mPhotoTags = list; }
    inline bool GetCanTag() { return mCanTag; }
    inline void SetCanTag(bool * canTag) { mCanTag = canTag; }
};

#endif /* PHOTO_H_ */
