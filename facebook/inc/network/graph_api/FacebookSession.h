#ifndef FACEBOOKSESSION_H_
#define FACEBOOKSESSION_H_

#include "curlutilities.h"
#include "Ecore.h"
#include "Evas.h"
#include "FbRespondGetFriends.h"
#include "GraphRequest.h"
#include "PhotoPostOperation.h"


#include <functional>
#include <string>

class ScreenBase;

enum PrivacyTypes
{
    EVERYONE,
    ALL_FRIENDS,
    FRIENDS_OF_FRIENDS,
    CUSTOM,
    SELF
};

/**
 * @brief This class intended to manage facebook session
 */
class FacebookSession {
public:
    enum FacebookSessionError {
        FBE_NONE,
        FBE_OBJECT_ALREADY_EXISTS,
        FBE_OBJECT_IS_NULL,
        FBE_OBJECT_NOT_FOUND,
    };

    /**
     * @brief Deletes all GraphRequest objects handled by FacebookSession
     */
    void DeleteAllGraphRequests();

private:

    Eina_List *mGraphRequestList;

    //Constructions
    /**
     * @brief Construction
     */
    FacebookSession();

    /**
     * @brief Prints all GraphRequest objects handled by FacebookSession
     */
    void PrintAllGraphRequests();

public:
    enum LoginStatus
    {
        LoginInProgress,
        LoginOk,
        LoginFailed
    };

    //Constants
    static const int MAX_URL_LEN = 2048;
    static const int SHORT_BUFFER_SIZE = 128;
    static const int REQUEST_HOME_ITEMS_LIMIT = 35;

    //Constructions
    /**
     * @brief Returns instance of FacebookSession object
     * @return FacebookSession object
     */
    static FacebookSession * GetInstance();

    //Destructor
    /**
     * @brief Destruction
     */
    virtual ~FacebookSession();

    /**
     * @brief Add GraphRequest object to internal list
     * @param obj - GraphRequest object
     * @return error code FBE_NONE if operation done without error, otherwise error code
     */
    FacebookSessionError AddGraphRequest(GraphRequest *obj);

    /**
     * @brief Return GraphRequest object from internal list
     * @param obj - GraphRequest object
     * @return GraphRequest object or NULL if it is not found
     */
    GraphRequest * GetGraphRequestById(const GraphRequest *objId);

    /**
     * @brief Remove GraphRequest object from internal list
     * @param obj - GraphRequest object
     * @return error code FBE_NONE if operation done without error, otherwise error code
     */
    static FacebookSessionError RemoveGraphRequest(const GraphRequest *obj);

    /**
     * @brief Destroy GraphRequest object
     * @param[in] waitForComplete - indicate that request should complete
     * @return FBE_NONE if object is destroyed without error, otherwise error code
     */
    static FacebookSessionError ReleaseGraphRequest(GraphRequest* &obj, bool waitForComplete = false);

    //API
    /**
     * @brief Build privacy value string from enum
     * @param privacy - privacy enum
     * @return built value
     */
    static char * BuildPrivacyValue(PrivacyTypes privacy, Eina_List *allowList, Eina_List *denyList);

    /**
     * @brief This method should be invoked for login to facebook.
     * It initates redirect to facebook login page, and subscribe for login floe redirections
     * @param[in] ewk_view_login - web view for displaying facebook login page
     * @param[in] login_cb - callback to notify client about login statuses
     * @param[in] cb_object - The user data to be passed to the callback function
     */
    void Login(Evas_Object *ewk_view_login, void (*login_cb)(ScreenBase *, LoginStatus), ScreenBase * cb_object);


    /**
     * @brief This method should be invoked to request friends from facebook
     * @param[in] callback - callback to notify client when request os completed
     * @param[in] callback_object - user data to pass in callback
     * @return[in] a pointer to async thread
     */
    GraphRequest * GetFriends(void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to post feed to facebook
     * @param params - params bundle
     * @param[in] callback - callback to notify client when request os completed
     * @param[in] callback_object - user data to pass in callback
     * @return - started request
     */
    GraphRequest * PostFeed(bundle * params, void (*callback) (void *, char*, int), void * callback_object, const char *userId = "me" );
    GraphRequest * UpdatePost(bundle * params, void (*callback) (void *, char*, int), void * callback_object, const char* id );
    GraphRequest * UpdateComment(bundle * params, void (*callback) (void *, char*, int), void * callback_object, const char* id);

    GraphRequest * ReloadComment(const char* id, void (*callback) (void *, char*, int), void * callback_object);
    GraphRequest * GetActionsAvailable(const char * object_id, void (*callback) (void *, char*, int), void * callback_object);


    GraphRequest * SetDefaultPrivacyOption(const char *privacyOptionId, void (*callback) (void *, char*, int), void * callback_object);
    GraphRequest * SetCustomPrivacyOptionAsDefault(const char *privacyOption, void (*callback) (void *, char*, int), void * callback_object);

    /**
         * @brief This method should be invoked to request userprofilepicture from facebook
         * @param[in] callback - callback to notify client when request os completed
         * @param[in] callback_object - user data to pass in callback
         * @return[in] a pointer to async thread
         */
    GraphRequest* GetUserProfileAndCoverPic(const char * object_id, void (*callback) (void *, char*, int), void * callback_object);
    GraphRequest* GetUserFullPicture(const char* object_id, void (*callback) (void*, char*, int), void* callback_object);
    GraphRequest* GetUserProfileName(void (*callback) (void*, char*, int), void* callback_object);
    GraphRequest* GetNotificationPhotoDetails(const char* photoObjId, void (*callback) (void *, char*, int), void * callback_object);
    GraphRequest* GetProfileActivityFeed(const char *object_id, void (*callback) (void *, char*, int), void * callback_object);
    GraphRequest* GetAnyUserProfilepic(void (*callback) (void *, char*, int), void * callback_object, const char * Req);
    GraphRequest* GetUserFirstPhoto(const char * object_id, void (*callback) (void *, char*, int), void * callback_object);
    GraphRequest* GetUserFriendPic(const char * object_id, void (*callback) (void *, char*, int), void * callback_object);
    GraphRequest* GetUserProfileAboutPic(void (*CallBack)(void*, char*, int), void* CallBackObject);
    GraphRequest* GetUPAboutOverview(const char *object_id, void (*CallBack)(void*, char*, int), void* CallBackObject);
    GraphRequest* GetUPPlacesOverview(const char *object_id, void (*CallBack)(void*, char*, int), void* CallBackObject);
    GraphRequest* GetUPPhotos(const char* userProfileId, void (*CallBack)(void *, char*, int), void* CallBackObject);
    GraphRequest* GetUPPictureId(const char* userProfileId, void (*CallBack)(void *, char*, int), void* CallBackObject);
    GraphRequest* GetUPAboutMore(const char* UserId, void (*CallBack)(void*, char*, int), void* CallBackObject);

    /**
     * @brief This method should be invoked to set like/unlike for facebook object
     * @param object_id - object to publish like/unlike
     * @param liked - true if like should be set, false if like should be removed
     * @param callback - callback to notify client when request is completed
     * @return pointer on GraphRequest
     */
    GraphRequest* Like(const std::string& objectId, bool liked, std::function<void(const std::string&, CURLcode)> callback);

    /**
     * @brief This method should be invoked to fetch likes summary data for facebook object
     * @param object_id - object to fetch likes
     * @param callback - callback to notify client when request is completed
     * @return pointer on GraphRequest
     */
    GraphRequest * GetLikesSummary(const std::string& objectId, std::function<void(const std::string&, CURLcode)> callback);

    /**
     * @brief This method should be invoked to post comment for facebook object
     * @param object_id - object to publish comment
     * @param params -bundle which contains parameters for posted comment
     * @param callback - callback to notify client when request is completed
     * @param callback_object -user data to pass in callback
     * @param photoFileName - the name of photo file to be attached; NULL if no photo should be attached
     * @return
     */
    GraphRequest * PostComment(const char * object_id, bundle * params, void (*callback) (void *, char*, int), void * callback_object, const char * photoFileName = NULL);

    /**
     * @brief This method should be invoked to get comments for facebook object
     * @param object_id - object to publish comment
     * @param callback - callback to notify client when request is completed
     * @param callback_object -user data to pass in callback
     * @return
     */
    GraphRequest * GetComments(const char * object_id, void (*callback) (void *, char*, int), void * callback_object);

    GraphRequest * GetFriendDetails(const char * object_id, void (*callback) (void *, char*, int), void * callback_object);

    GraphRequest * GetEventDetails(const char * object_id, GraphRequest::HttpMethod httpMethod, void (*callback) (void *, char*, int), void * callback_object);

    Eina_List *GetImageDetails(Eina_List *imageIds, void (*callback) (void *, char*, int), void * callback_object);
    /**
     * @brief This method should be invoked to share some facebook object
     * @param params - parameters bundle
     * @param[in] callback - callback to notify client when request is completed
     * @param[in] callback_object - user data to pass in callback
     * @return
     */
    GraphRequest * Share(bundle * params, void (*callback) (void *, char*, int), void * callback_object);


    /**
     * @brief This method should be invoked to get info about some facebook object
     * @param id - object id or web url
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * GetInfo(const char * id, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to get list of icoming friend requests
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * GetIncomingFriendRequestsCounts(void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to send friend request to facebook user
     * @param id - user id to which friend request is sent
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * SendFriendRequest(const char * object_id, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to cancel friend request we sent before
     * @param id - user id to which friend request should be cancelled
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * CancelOutgoingFriendRequest(const char * object_id, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to unfriend with some facebook user
     * @param id - user id to unfriend with
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * Unfriend(const char * object_id, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to get list of friendlists
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * GetFriendlists(void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to edit friendlist
     * @param list_id - friend list to me modified
     * @param member_id - user id to be added/deleted
     * @param httpMethod - the type of http sending method. POST - to add member, DELETE - to delete member
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * EditFriendlistMembers(const char * list_id, const char * member_id, GraphRequest::HttpMethod httpMethod, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to get photos
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * GetPhotos(void (*callback) (void *, char*, int), void * callback_object);

    /**
    * @brief This method should be invoked to post new photo
    * @param imageFileName - the name of image file that should be uploaded
    * @param params - parameters bundle
    * @param callback - callback to notify client when request is completed
    * @param callback_object- user data to pass in callback
    * @param id- user id or album id.
    * @return started request
    */
   GraphRequest * PostPhoto(const char * imageFileName, bundle * params, void (*callback) (void *, char*, int), void * callback_object, const char *id = "me" );

   /**
   * @brief This method should be invoked to post new video
   * @param imageFileName - the name of image file that should be uploaded
   * @param params - parameters bundle
   * @param callback - callback to notify client when request is completed
   * @param callback_object- user data to pass in callback
   * @param id- user id or album id.
   * @return started request
   */
   GraphRequest * PostVideo(const char * imageFileName, bundle * params, void (*callback) (void *, char*, int), void * callback_object, const char *id = "me");
   /**
   * @brief This method should be invoked to post new photo to cover
   * @param imageFileName - the name of image file that should be uploaded
   * @param params - parameters bundle
   * @param callback - callback to notify client when request is completed
   * @param callback_object- user data to pass in callback
   * @return started request
   */
   GraphRequest * PostCoverPhoto(const char * imageFileName, bundle * params, void (*callback) (void *, char*, int), void * callback_object, const char *userId = "me" );
   GraphRequest * PostProfilePicture(const char * imageFileName, bundle * params, void (*callback) (void *, char*, int), void * callback_object, const char *userId = "me" );
   GraphRequest * PostFeedWithPhotos(bundle * params, Eina_List * mediaList, void (*callback) (void *, char*, int), void * callback_object, const char *userId = "me" );
   GraphRequest *AddPhotosToPost(bundle *params, Eina_List *photos, void (*callback) (void *, char*, int), void * callback_object, const char *userId, const char *PhostId);
   char *CreateJSONArrayPhotosForPost(Eina_List *list, const char *postId, bool withDependencies);
   char *CreateJSONPhotoForPost(PhotoPostOperation::PhotoPostDescription *photoDecription, const char *postId, unsigned int number);

   // @brief This method is invoked when we see requests in Friends Screen right after clicking/scrolling onto this page
   GraphRequest * PostSeenRequests();

    /**
     * @brief This method should be invoked to confirm or reject friend request
     * @param userId - the user id who sent request
     * @param isConfirm - indicates whether you want confirm or reject friend request
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * ConfirmFriendRequest(const char * userId, bool isConfirm, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to get list of people you may know
     * @param flowType - application context, for which people are requested.Following types are supported:
     *      NEWS_FEED_MEGAPHONE,
     *      FRIENDS_TAB,
     *      FRIEND_BROWSER,
     *      NEW_ACCOUNT_NUX,
     *      INTERSTITIAL,
     *      BOOKMARKS,
     *      SEARCH_BOX,
     *      FRIENDS_CENTER,
     *      CONTINUOUS_SYNC,
     *      FRIEND_REQUEST_TAB,
     *      ACCEPT_FRIEND_REQUEST,
     *      FIND_FRIENDS,
     *      NOTIFICATION,
     *      NUX
     * @param picSize - the size of user picture
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * GetPeopleYouMayKnow(const char * flowType, int picSize, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to get list of privacy options for PostComposerScreen
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * GetPrivacyOptions(void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to get events
     * @param time - date and time value (e.g. 2015-05-14T12:51:02Z)
     * @param since - true in case of events posted since the passed time (false in case of old events)
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * GetEvents(const char * object_id, bool since, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to get events saves
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * GetEventsSavesRequestBatch(void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to get invites events
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * GetInvitesEventsRequestBatch(void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to get hosting events
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * GetHostingEventsRequestBatch(void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to post a new event to facebook
     * @param params - params bundle
     * e.g.
     *      bundle* paramsBundle;
     *      paramsBundle = bundle_create();
     *      bundle_add_str(paramsBundle, "name", "NewEvent10072015-10-22");
     *      bundle_add_str(paramsBundle, "start_time", "2016-01-02T10:00:00Z");
     *      bundle_add_str(paramsBundle, "end_time", "2016-01-02T11:00:00Z");
     *      //bundle_add_str(paramsBundle, "location", "Kremlin");//used if no location_id found for the event
     *      bundle_add_str(paramsBundle, "location_id", "213636511990328");
     *      bundle_add_str(paramsBundle, "country", "Russia");
     *      bundle_add_str(paramsBundle, "description", "First successful event");
     *      bundle_add_str(paramsBundle, "type", "public");//value should be clarified, this is privacy_type field
     *      bundle_add_str(paramsBundle, "can_guests_invite_friends", "true");
     * @param[in] callback - callback to notify client when request is completed
     * @param[in] callback_object - user data to pass in callback
     * @return started request
     */
    GraphRequest * PostCreateNewEvent(bundle * params, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to edit a facebook event
     * @param object_id - object - id of event
     * @param params - params bundle
     * e.g.
     *      bundle_add_str(paramsBundle, "name", "NewEvent10072015-11-31");
     *      bundle_add_str(paramsBundle, "id", "1471636316463640");
     * @param[in] callback - callback to notify client when request os completed
     * @param[in] callback_object - user data to pass in callback
     * @return started request
     */
    GraphRequest * PostEditEvent(const char * object_id, bundle * params, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to invite friends to event
     * @param object_id - object - id of event
     * @param params - params bundle
     * e.g.
     *      bundle_add_str(paramsBundle, "users", "100000392273904, 100003295217975");
     * @param[in] callback - callback to notify client when request os completed
     * @param[in] callback_object - user data to pass in callback
     * @return started request
     */
    GraphRequest * PostInviteFriendsToEvent(const char * object_id, bundle * params, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to delete an event
     * @param object_id - object - id of event
     * @param[in] callback - callback to notify client when request os completed
     * @param[in] callback_object - user data to pass in callback
     * @return started request
     */
    GraphRequest * DeleteEvent(const char * object_id, void (*callback) (void *, char*, int), void * callback_object);

    GraphRequest * GetLastFeedPost(const char * object_id, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to get event containing the search string
     * @param searchString - string which should be part of event's name
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * SearchEvents(const char * search_string, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to get birthday events
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * GetBirthdayEvents(void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to get event's actions count (attending, maybe, invited)
     * @param eventid - id of event
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * GetEventActionsCount(const char * eventid, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to change a status of event on which you are invited (not own event)
     * @param object_id - event object to be modified
     * @param status - the new status of event
     *     declined
     *     attending
     *     maybe
     * @param[in] callback - callback to notify client when request is completed
     * @param[in] callback_object - user data to pass in callback
     * @return started request
     */
    GraphRequest * ChangeEventStatus(const char * object_id, const char * status, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to get the status count of the event
     * @param object_id - event id
     * @param type - the required status count of the event
     *     declined_count
     *     attending_count
     *     maybe_count
     *     noreply_count
     * @param[in] callback - callback to notify client when request is completed
     * @param[in] callback_object - user data to pass in callback
     * @return started request
     */
    GraphRequest * GetStatusCountOfEvent(const char * object_id, const char * statusCount, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to get groups
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * GetGroups(void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to get groups list
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * GetGroupsList(void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to get group details information
     * @param group_id - id of group
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * GetGroupDetails(const char * group_id, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to join to a group
     * @param group_id - id of group
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * JoinToGroup(const char * group_id, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to cancel join request or leave a group
     * @param group_id - id of group
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * CancelGroup(const char * group_id, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to get albums
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * GetAlbums(void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to delete an album
     * @param object_id - object - id of album
     * @param[in] callback - callback to notify client when request is completed
     * @param[in] callback_object - user data to pass in callback
     * @return started request
     */
    GraphRequest * DeleteAlbum(const char * album_id, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to rename album
     * @param params - params bundle
     * e.g.
     *      bundle* paramsBundle;
     *      paramsBundle = bundle_create();
     *      bundle_add_str(paramsBundle, "name", "NewAlbumName");
     * @param[in] callback - callback to notify client when request is completed
     * @param[in] callback_object - user data to pass in callback
     * @return started request
     */
    GraphRequest * PostRenameAlbum(const char * album_id, bundle * params, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to create an album
     * @param params - params bundle
     * e.g.
     *      bundle* paramsBundle;
     *      paramsBundle = bundle_create();
     *      bundle_add_str(paramsBundle, "name", "NewAlbumName");
     * @param[in] callback - callback to notify client when request is completed
     * @param[in] callback_object - user data to pass in callback
     * @return started request
     */
    GraphRequest * PostCreateAlbum(bundle * params, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to get album details
     * @param albumId - id of album object
     * @param callback - callback to notify client when request is completed
     * @param callback_object -user data to pass in callback
     * @return
     */
    GraphRequest *GetAlbumDetails(const char *albumId, void (*callback) (void *, char*, int), void * callback_object);
    GraphRequest *GetAlbumDetailsForPhoto(const char *photoId, void (*callback) (void *, char*, int), void * callback_object);
    /**
     * @brief This method should be invoked to add a photo to the album
     * @param params - params bundle
     * e.g.
     *      bundle* paramsBundle;
     *      paramsBundle = bundle_create();
     *      bundle_add_str(paramsBundle, "url", "http://moisad.ua/images/stories/virtuemart/product/aspirin%C2%AE-rose3.jpg");
     *      bundle_add_str(paramsBundle, "caption", "Flowers_in_garden");
     * @param[in] callback - callback to notify client when request is completed
     * @param[in] callback_object - user data to pass in callback
     * @return started request
     */
    GraphRequest * PostAddPhotoToAlbum(const char * album_id, bundle * params, void (*callback) (void *, char*, int), void * callback_object);

    /** CURRENTLY THIS METHOD DOESN'T WORK EVEN THE RESPONSE RETURNS TRUE
     * @brief This method should be invoked to edit photo caption
     * @param params - params bundle
     * e.g.
     *      bundle* paramsBundle;
     *      paramsBundle = bundle_create();
     *      bundle_add_str(paramsBundle, "name", "NewCaption");//tried with caption field also
     * @param[in] callback - callback to notify client when request is completed
     * @param[in] callback_object - user data to pass in callback
     * @return started request
     */
    GraphRequest * PostEditPhotoCaption(const char * photo_id, bundle * params, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to delete a photo
     * @param photo_id - object - id of photo
     * @param[in] callback - callback to notify client when request is completed
     * @param[in] callback_object - user data to pass in callback
     * @return started request
     */
    GraphRequest * DeletePhoto(const char * photo_id, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to delete own post
     * @param post_id - id of post
     * @param[in] callback - callback to notify client when request is completed
     * @param[in] callback_object - user data to pass in callback
     * @return started request
     */
    GraphRequest * DeletePost(const char * post_id, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to delete own comment
     * @param post_id - id of post
     * @param[in] callback - callback to notify client when request is completed
     * @param[in] callback_object - user data to pass in callback
     * @return started request
     */
    GraphRequest * DeleteComment(const char * comment_id, void (*callback) (void *, char*, int), void * callback_object);

    /**
        * @brief This method should be invoked to perform Mark Notifications as read on FB server
        * @param httpMethod - the type of http sending method
        * @param[in] callback - callback to notify client when request is completed
        * @param[in] callback_object - user data to pass in callback
        * @return started request
        */

    GraphRequest * MarkNotificationRead(void (*callback) (void *, char*, int), void * callback_object, const char * ntf_id);

    GraphRequest * MarkNotificationAsRead(const char * object_id, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to get send batch request
     * @param batch - the json array with batch requests
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * DoBatchRequest(const char * batch, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to get friends requests via batch request
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * GetFriendRequestsBatch(void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to get events requests via batch request
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * GetEventsRequestsBatch(const char * time, bool since, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to get quick search results
     * @param searchString - string to be found
     * @param filter.Following filters are supported:
     *            ['user','group','page', 'app']
     *            ['group']
     *            ['user']
     *            ['page']
     *            ['app']
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * GetSearchResults(const char * searchString, const char * filter, void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to get mobile zero campain info
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * GetMobileZeroCampain(void (*callback) (void *, char*, int), void * callback_object);

    /**
     * @brief This method should be invoked to perform friend operations
     * @param object_id - object for friend operation
     * @param httpMethod - the type of http sending method
     * @param[in] callback - callback to notify client when request is completed
     * @param[in] callback_object - user data to pass in callback
     * @return started request
     */
    GraphRequest * DoFriends(const char * object_id, GraphRequest::HttpMethod httpMethod, void (*callback) (void *, char*, int), void * callback_object);
    GraphRequest * FollowUser(const char * object_id, void (*callback) (void *, char*, int), void * callback_object);
    GraphRequest * UnFollowUser(const char * object_id, void (*callback) (void *, char*, int), void * callback_object);

    GraphRequest * BlockUser(const char * object_id, void (*callback) (void *, char*, int), void * callback_object);

    GraphRequest * UnblockUser(const char * object_id, void (*callback) (void *, char*, int), void * callback_object);
    GraphRequest * PokeUser(const char * object_id, void (*callback) (void *, char*, int), void * callback_object);
    GraphRequest * GetUnseenNotificationsCount(void (*callback) (void *, char*, int), void * callback_object);
    GraphRequest * PostUnseenNotificationsCount(void (*callback) (void *, char*, int), void * callback_object);

    //Callbacks
    /**
     * @brief This is internal callback which is called during login procedure when url redirection event happens
     * @param[in] data - The user data to be passed to the callback function
     * @param[in] obj - object which generates event
     * @param[in] event_info - event information
     */
    static void on_url_changed(void *data, Evas_Object *obj, void *event_info);

    GraphRequest * GetEventProfileInfo(const char * id, void (*callback) (void *, char*, int), void * callback_object);

    GraphRequest * UpdatePostWithPhotos(bundle * params, Eina_List * mediaList, void (*callback) (void *, char*, int), void * callback_object, const char *userId );

    GraphRequest * GetFeatureState(const char* gkNames, void (*callback) (void*, char*, int), void* callback_object);

private:
    //Private Members
    void (*mLoginCallback)(ScreenBase * callbackObject, LoginStatus status);
    ScreenBase * mLoginCallbackObject;

    static Eina_Lock m_Mutex;
    static bool mIsSessionClosed;

    //Private Methods
    /**
     * @brief This method examines redirection url for access_token presence
     * If token is found, saves it to mAccessToken
     * @param[in] url - redirection url
     * @return - login status (LoginOk if access_token is found)
     */
    LoginStatus ProceedUrl(const char * url);

    /**
     * @brief This methods send HHTP GET request to facebook via CURL library
     * @param[in] request - graph path
     * @param[in] respondBuffer - the buffer, where HTTP GET respond should be placed
     * @return CURL library return code
     */
    CURLcode SendRequest(const char * request, MemoryStruct * respond);

    /**
     * @brief This method should be invoked to send likes requests to facebook
     * @param object_id - object to which like request is sent
     * @param httpMethod - the type of http sending method
     * @param[in] callback - callback to notify client when request is completed
     * @param[in] callback_object - user data to pass in callback
     * @return started request
     */
    GraphRequest * DoLike(const char * object_id, GraphRequest::HttpMethod httpMethod, void (*callback) (void *, char*, int), void * callback_object);
    GraphRequest * DoLike(const std::string& objectId, GraphRequest::HttpMethod httpMethod, std::function<void(const std::string&, CURLcode)> callback);
public:
    /**
        * @brief This method should be invoked to get specific story
        * @param id - id of the specific story
        * @param httpMethod - the type of http sending method
        * @param[in] callback - callback to notify client when request is completed
        * @param[in] callback_object - user data to pass in callback
        * @return started request
        */

    GraphRequest * GetSpecificStory(void (*callback) (void *, char*, int), void * callback_object, const char * id);

    /**
           * @brief This method should be invoked to get specific object type
           * @param id - id of the object
           * @param httpMethod - the type of http sending method
           * @param[in] callback - callback to notify client when request is completed
           * @param[in] callback_object - user data to pass in callback
           * @return started request
           */
    GraphRequest * GetObjectType(void (*callback) (void *, char*, int), void * callback_object, const char * id, bool isLinkNeeded = false);

    /**
           * @brief This method should be invoked to get post detailed information
           * @param id - Post id
           * @param httpMethod - the type of http sending method
           * @param[in] callback - callback to notify client when request is completed
           * @param[in] callback_object - user data to pass in callback
           * @return started request
           */
    GraphRequest * GetPostDetails(const char *postId, void (*callback) (void *, char*, int), void * callback_object);
    GraphRequest * GetPhotoDetails(const char *photoId, void (*callback) (void *, char*, int), void * callback_object);

    GraphRequest * GetUserDetails(const char *userId, void (*callback) (void *, char*, int), void * callback_object);
    /**
     * @brief This method should be invoked to get Posts details
     * @param callback - callback to notify client when request is completed
     * @param callback_object- user data to pass in callback
     * @return started request
     */
    GraphRequest * GetPostsDetailsBatch(void (*callback) (void *, char*, int), void * callback_object);

    GraphRequest *PostSetLastLocation(double latitude, double longitude, time_t timestamp, int accuracy, void (*callback) (void *, char*, int), void * callback_object);
    GraphRequest *PostLinkPreview(const char *link, void (*callback) (void *, char*, int), void * callback_object);

    GraphRequest * GetUserProfileBaseInfo(const char *id, void (*callback) (void *, char*, int), void * callback_object);
    GraphRequest * GetMyAvatar(int width, int height, void (*callback) (void *, char*, int), void * callback_object);

    GraphRequest * IsObjectExists(const char *id, void (*callback) (void *, char*, int), void * callback_object);

    GraphRequest * GetPageDetails(const char *id, void (*callback) (void *, char*, int), void * callback_object);
    GraphRequest * CreatePhotoTags(const char * id, Eina_List * tagList, void (*callback) (void *, char*, int), void * callback_object);
    GraphRequest * DeletePhotoTags(const char * id, Eina_List * tagList, void (*callback) (void *, char*, int), void * callback_object);

    static const char *REQUEST_USER_BASE_INFO;
};

#endif /* FACEBOOKSESSION_H_ */
