/*
 * FbRespondGetAlbums.h
 *
 *  Created on: Jul 15, 2015
 *      Author: RUINREKU
 */
#ifndef FBRESPONDGETALBUMS_H_
#define FBRESPONDGETALBUMS_H_

#include "FbRespondBase.h"
/**
 * This class contains data received for /me/albums facebook request
 */
class FbRespondGetAlbums: public FbRespondBase
{
public:
    /**
     * @brief Construct FbRespondGetAlbums from JsonObject
     * @param[in] object - Json Object, from which FbRespondGetAlbums will be constructed
     */
	FbRespondGetAlbums(JsonObject* object);

    /**
     * @brief Destruction
     */
    virtual ~FbRespondGetAlbums();

    /**
     * @brief Creates FbRespondGetAlbums object fron JSON formatted text
     * @param data - JSON formatted text
     * @return - created FbRespondGetAlbums object
     */
    static FbRespondGetAlbums* createFromJson(char* data);

    /**
     * @brief albums array
     */
    Eina_List * mAlbumsList;
};

#endif /* FBRESPONDGETALBUMS_H_ */
