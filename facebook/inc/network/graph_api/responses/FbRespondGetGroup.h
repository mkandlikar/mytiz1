/*
 * FbRespondGetGroups.h
 *
 *  Created on: Jul 15, 2015
 *      Author: RUINREKU
 */

#ifndef FBRESPONDGETGROUPS_H_
#define FBRESPONDGETGROUPS_H_

#include "eina_list.h"
#include "FbRespondBase.h"
/**
 * This class contains data received for /me/groups facebook request
 */
class FbRespondGetGroup: public FbRespondBase
{
public:
    /**
     * @brief Construct FbRespondGetGroups from JsonObject
     * @param[in] object - Json Object, from which FbRespondGetGroups will be constructed
     */
	FbRespondGetGroup(JsonObject* object);

    /**
     * @brief Destruction
     */
    virtual ~FbRespondGetGroup();

    Eina_List* RemoveDataFromList(void *data);

    /**
     * @brief Creates FbRespondGetGroups object from JSON formatted text
     * @param data - JSON formatted text
     * @return - created FbRespondGetGroups object
     */
    static FbRespondGetGroup* createFromJson(char* data);

    /**
     * @brief groups array
     */
    Eina_List * mGroupsList;
};

#endif /* FBRESPONDGETGROUPS_H_ */
