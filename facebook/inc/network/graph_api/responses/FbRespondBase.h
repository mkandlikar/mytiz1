#ifndef FBRESPONDBASE_H_
#define FBRESPONDBASE_H_

#include <json-glib/json-glib.h>

#include "Error.h"

/**
 * @brief This base class for data received for all facebook requests
 */
class FbRespondBase
{
public:
    /**
     * @brief Construct FbRespondBase object from JsonObject
     * @param object - Json Object, from which FbRespondBase will be constructed
     */
    FbRespondBase(JsonObject * object);

    /**
     * @brief Construct FbRespondBase object from JsonNode
     * @param object - JsonNode, from which FbRespondBase will be constructed
     */
    FbRespondBase(JsonNode* rootNode);

    /**
     * @brief Inits elements of FbRespondBase object
     * @param object - Json Object, from which elements will be inited
     */
    void Init(JsonObject * object);

    /**
     * @brief Destructor
     */
    virtual ~FbRespondBase();

    /**
     * @brief Error
     */
    Error * mError;

    /**
     * @brief Request result
     */
    bool mSuccess;

    /**
     * @brief - creates an instance of FbRespondBase object from char buffer
     * @param data - char buffer
     * @return - returns created object
     */
    static FbRespondBase* createFromJson(const char* data);

    static const char *DATA;
};


#endif /* FBRESPONDBASE_H_ */
