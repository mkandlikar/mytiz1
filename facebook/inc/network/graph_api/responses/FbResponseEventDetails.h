#ifndef FBRESPONSEEVENTDETAILS_H_
#define FBRESPONSEEVENTDETAILS_H_

#include "FbRespondBase.h"
/**
 *@brief This class intended for data received from facebook for POST http requests
 */
class FbResponseEventDetails: public FbRespondBase
{
public:
    /**
     * @brief Construct FbRespondBase object from JsonObject
     * @param object - Json Object, from which FbRespondBase will be constructed
     */
    FbResponseEventDetails(JsonObject * object);

    /**
     * @brief Destructor
     */
    virtual ~FbResponseEventDetails();

    /**
     * @brief - an id of created object
     */
    double mStartTime;
    const char * mId;
    const char * mCoverUrl;
    const char * mLocation;
    const char * mOwnerId;
    bool mNeedTimeShift;
    /**
     * @brief - creates an instance of FbRespondPost object from char buffer
     * @param data - char buffer
     * @return - returns created object
     */
    static FbResponseEventDetails* createFromJson(char* data);
};

#endif /* FBRESPONSEEVENTDETAILS_H_ */
