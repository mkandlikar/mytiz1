#ifndef GRAPH_DATA_FBRESPONDPOSTOBJECTID_H_
#define GRAPH_DATA_FBRESPONDPOSTOBJECTID_H_

#include "Album.h"
#include "FbRespondBase.h"

#include <json-glib/json-glib.h>

class FbRespondPostObjectId: public FbRespondBase
{
public:
    FbRespondPostObjectId(JsonObject * object);
    virtual ~FbRespondPostObjectId();

    Album *mAlbum;

    static FbRespondPostObjectId* createFromJson(const char* data);
};

#endif /* GRAPH_DATA_FBRESPONDPOSTOBJECTID_H_ */
