#ifndef NETWORK_GRAPH_API_RESPONSES_FBRESPONDGETFEATURESTATE_H_
#define NETWORK_GRAPH_API_RESPONSES_FBRESPONDGETFEATURESTATE_H_

#include <json-glib/json-glib.h>
#include "FbRespondBaseRestApi.h"

/**
 * @brief Response wrapper for GateKeeper data
 */
class FbRespondGetFeatureState: public FbRespondBaseRestApi
{
public:
    /**
     * @brief Constructs FbRespondGetFeatureState from JsonNode
     * @param[in] object - incoming Json object to parse out
     */
    FbRespondGetFeatureState(JsonNode* object);

    /**
     * @brief Destructor
     */
    virtual ~FbRespondGetFeatureState();

    /**
     * @brief Creates FbRespondGetFeatureState from passed Json
     * @param data - JSON formatted string
     * @return - created FbRespondGetFeatureState object
     */
    static FbRespondGetFeatureState* createFromJson(const char* data);

    const char* getConfig() { return mConfig; }

private:
    /**
     * @brief holds parsed gk config
     */
    const char* mConfig;
};

#endif /* NETWORK_GRAPH_API_RESPONSES_FBRESPONDGETFEATURESTATE_H_ */
