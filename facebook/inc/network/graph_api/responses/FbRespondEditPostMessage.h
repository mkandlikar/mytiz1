#ifndef FBRESPONDEDITPOSTMESSAGE_H_
#define FBRESPONDEDITPOSTMESSAGE_H_

#include <json-glib/json-glib.h>

/**
 *@brief This class intended for data received from facebook for POST http requests
 */
class FbRespondEditPostMessage: public FbRespondBase
{
public:
    /**
     * @brief Construct FbRespondBase object from JsonObject
     * @param object - Json Object, from which FbRespondBase will be constructed
     */
    FbRespondEditPostMessage(JsonObject * object);

    /**
     * @brief Destructor
     */
    virtual ~FbRespondEditPostMessage();

    /**
     * @brief - an id of created object
     */
    bool mIsSuccess;

    bool mCanShare;

    /**
     * @brief - creates an instance of FbRespondEditPostMessage object from char buffer
     * @param data - char buffer
     * @return - returns created object
     */
    static FbRespondEditPostMessage* createFromJson(char* data);
};

#endif /* FBRESPONDEDITPOSTMESSAGE_H_ */
