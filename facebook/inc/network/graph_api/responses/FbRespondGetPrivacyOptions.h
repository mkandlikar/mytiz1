/*
 * FbRespondGetPrivacyOptions.h
 *
 *  Created on: Jul 7, 2015
 *      Author: RUINMKOL
 */

#ifndef FBRESPONDGETPRIVACYOPTIONS_H_
#define FBRESPONDGETPRIVACYOPTIONS_H_

#include "FbRespondBase.h"
/**
 * This class contains data received for /me/privacy_options facebook request
 */
class FbRespondGetPrivacyOptions : public FbRespondBase
{
public:
    class PrivacyOption
    {
    public:
        PrivacyOption(JsonObject * object);
        virtual ~PrivacyOption();

        //members
        const char * mId;
        const char * mDescription;
        const char * mType;
        const char * mIconSrc;
        bool mIsCurrentlySelected;
     };

    /**
     * @brief Construct FbRespondGetPrivacyOptions from JsonObject
     * @param[in] object - Json Object, from which FbRespondGetPrivacyOptions will be constructed
     */
    FbRespondGetPrivacyOptions(JsonObject* object);

    /**
     * @brief Destruction
     */
    virtual ~FbRespondGetPrivacyOptions();

    /**
     * @brief Creates FbRespondGetPrivacyOptions object fron JSON formatted text
     * @param data - JSON formatted text
     * @return - created FbRespondGetPrivacyOptions object
     */
    static FbRespondGetPrivacyOptions* createFromJson(const char* data);

    /**
     * @brief privacy options array
     */
    Eina_List * mPrivacyOptionsList;
};

#endif /* FBRESPONDGETPRIVACYOPTIONS_H_ */
