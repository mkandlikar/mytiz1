/*
 * FbRespondPostPhoto.h
 *
 */

#ifndef FBRESPONDPOSTPHOTO_H_
#define FBRESPONDPOSTPHOTO_H_

#include "FbRespondBase.h"
/**
 * This class contains data received for /me/photos facebook request
 */
class FbRespondPostPhoto: public FbRespondBase
{
public:
    /**
     * @brief Construct FbRespondPostPhoto from JsonObject
     * @param[in] object - Json Object, from which FbRespondPostPhoto will be constructed
     */
    FbRespondPostPhoto(JsonObject* object);

    /**
     * @brief Destruction
     */
    virtual ~FbRespondPostPhoto();

    /**
     * @brief Creates FbRespondPostPhoto object fron JSON formatted text
     * @param data - JSON formatted text
     * @return - created FbRespondPostPhoto object
     */
    static FbRespondPostPhoto* createFromJson(char* data);

    /**
     * @brief data in response from FB
     */
    char* mId;
    char* mPostId;
};

#endif /* FBRESPONDPOSTPHOTO_H_ */
