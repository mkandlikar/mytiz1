/*
 * FbRespondGetFindFriend.h
 *
 *  Created on: Jun 29, 2015
 *      Author: RUINMKOL
 */

#ifndef FBRESPONDGETFINDFRIEND_H_
#define FBRESPONDGETFINDFRIEND_H_

#include <efl_extension.h>
#include <json-glib/json-glib.h>
#include "FbRespondBaseRestApi.h"
/**
 * @brief This class contains data received from Facebook for FindFriends request
 */
class FbRespondGetSearchResults: public FbRespondBaseRestApi
{
public:
    /**
     * @brief Construct FbRespondGetFindFriend from JsonNode
     * @param[in] object - Json Object, from which FbRespondGetFindFriend will be constructed
     */
    FbRespondGetSearchResults(JsonNode* object);

    /**
     * @brief Destructor
     */
    virtual ~FbRespondGetSearchResults();

    /**
     * @brief Creates FbRespondGetFindFriend object fron JSON formatted text
     * @param data - JSON formatted text
     * @return - created FbRespondGetFindFriend object
     */
    static FbRespondGetSearchResults* createFromJson(const char* data);

    //members
    /**
     * @brief list of found items
     */
    Eina_List * mFoundItems;

    Eina_List* RemoveDataFromList(void *data);
};

#endif /* FBRESPONDGETFINDFRIEND_H_ */
