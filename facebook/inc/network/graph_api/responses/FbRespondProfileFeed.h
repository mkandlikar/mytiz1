#ifndef FBRESPONDPROFILEFEED_H_
#define FBRESPONDPROFILEFEED_H_

#include "FbRespondBase.h"

class FbRespondProfileFeed: public FbRespondBase {
public:
    FbRespondProfileFeed(JsonObject* object);
    virtual ~FbRespondProfileFeed();

    static FbRespondProfileFeed* createFromJson(char* data);
    Eina_List *mFeedList;
};

#endif /* FBRESPONDPROFILEFEED_H_ */
