/*
 * FbRespondGetEvents.h
 *
 *  Created on: Jul 02, 2015
 *      Author: RUINREKU
 */

#ifndef FBRESPONDGETEVENTS_H_
#define FBRESPONDGETEVENTS_H_

#include <eina_list.h>
#include "FbRespondBase.h"

/**
 * This class contains data received for /me/events facebook request
 */
class FbRespondGetEvents: public FbRespondBase
{
public:
    /**
     * @brief Construct FbRespondGetEvents from JsonObject
     * @param[in] object - Json Object, from which FbRespondGetEvents will be constructed
     */
    FbRespondGetEvents(JsonObject* object);

    /**
     * @brief Destruction
     */
    virtual ~FbRespondGetEvents();

    /**
     * @brief Creates FbRespondGetEvents object from JSON formatted text
     * @param data - JSON formatted text
     * @return - created FbRespondGetEvents object
     */
    static FbRespondGetEvents* createFromJson(char* data);

    /**
     * @brief events array
     */
    Eina_List * mEventsList;

    Eina_List* RemoveDataFromList(void *data);
};

#endif /* FBRESPONDGETEVENTS_H_ */
