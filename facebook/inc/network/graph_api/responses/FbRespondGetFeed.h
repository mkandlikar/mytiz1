#ifndef FBRESPONDGETFEED_H_
#define FBRESPONDGETFEED_H_

#include "FbRespondBase.h"

/**
 * This class contains data received for /me/home facebook request
 */
class FbRespondGetFeed : public FbRespondBase
{
public:
    /**
     * @brief Construct FbRespondGetFeed from JsonObject
     * @param[in] object - Json Object, from which FbRespondGetFeed will be constructed
     */
    FbRespondGetFeed(JsonObject* object);

    /**
     * @brief Destructor
     */
    virtual ~FbRespondGetFeed();

    /**
     * @brief Creates FbRespondGetFeed object from JSON formatted text
     * @param data - JSON formatted text
     * @return - created FbRespondGetFeed object
     */
    static FbRespondGetFeed* createFromJson(const char* data);

    Eina_List* GetPostsList() const;

    Eina_List* RemoveDataFromList(void *data);

private:

    /**
     * @brief An array of Post objects
     */
    Eina_List * mPostsList;
};

#endif /* FBRESPONDGETFEED_H_ */
