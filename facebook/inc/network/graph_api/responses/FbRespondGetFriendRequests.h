/*
 * FbRespondGetFriendRequests.h
 *
 *  Created on: Jun 22, 2015
 *      Author: RUINMKOL
 */

#ifndef FBRESPONDGETFRIENDREQUESTS_H_
#define FBRESPONDGETFRIENDREQUESTS_H_

#include <efl_extension.h>
#include "FbRespondBase.h"

/**
 * @brief This class contains data received from Facebook for GetIcomingFriendRequests request
 */
class FbRespondGetFriendRequests : public FbRespondBase
{
    /**
     * @brief This class contains summary data for GetFriendRequests request
     */
    class Summary
    {
    public:
        /**
         * @brief Construct Summary object from JsonObject
         * @param object - Json Object, from which Summary will be constructed
         */
        Summary(JsonObject * object);

        /**
         * @brief Destructor
         */
        virtual ~Summary();

        /**
         * @brief The count of friend requests
         */
        int mTotalCount;

        /**
         * @brief The count of unread friend requests
         */
        int mUnreadCount;

        /**
         * @brief The last update time
         */
        const char * mUpdatedTime;
    };

public:
    /**
     * @brief Construct FbRespondGetFriendRequests from JsonObject
     * @param[in] object - Json Object, from which FbRespondGetFriendRequests will be constructed
     */
    FbRespondGetFriendRequests(JsonObject * object);

    /**
     * @brief Destructor
     */
    virtual ~FbRespondGetFriendRequests();

    /**
     * @brief Creates FbRespondGetFriendRequests object fron JSON formatted text
     * @param data - JSON formatted text
     * @return - created FbRespondGetFriendRequests object
     */
    static FbRespondGetFriendRequests* createFromJson(char* data);

    Eina_List* RemoveDataFromList(void *data);

    /**
     * @brief Friends array
     */
    Eina_List * mRequestsList;

    /**
     * @brief Summary
     */
    Summary * mSummary;
};

#endif /* FBRESPONDGETFRIENDREQUESTS_H_ */
