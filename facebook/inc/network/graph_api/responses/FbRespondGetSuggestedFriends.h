/*
 * FbRespondGetSuggestedFriends.h
 *
 *  Created on: Jul 21, 2015
 *      Author: RUINREKU
 */

#ifndef FBRESPONDGETSUGGESTEDFRIENDS_H_
#define FBRESPONDGETSUGGESTEDFRIENDS_H_

#include "FbRespondBase.h"
/**
 * This class contains data received for /me/orderedfriends facebook request
 */
class FbRespondGetSuggestedFriends: public FbRespondBase
{
public:
    /**
     * @brief Construct FbRespondGetSuggestedFriends from JsonObject
     * @param[in] object - Json Object, from which FbRespondGetSuggestedFriends will be constructed
     */
	FbRespondGetSuggestedFriends(JsonObject* object);

    /**
     * @brief Destruction
     */
    virtual ~FbRespondGetSuggestedFriends();

    /**
     * @brief Creates FbRespondGetSuggestedFriends object from JSON formatted text
     * @param data - JSON formatted text
     * @return - created FbRespondGetSuggestedFriends object
     */
    static FbRespondGetSuggestedFriends* createFromJson(char* data);

    /**
     * @brief suggested friends array
     */
    Eina_List * mSuggestedFriendsList;
};

#endif /* FBRESPONDGETSUGGESTEDFRIENDS_H_ */
