/*
 * FbRespondGetSubscribers.h
 *
 *  Created on: Aug 06, 2015
 *      Author: RUINREKU
 */

#ifndef FBRESPONDGETSUBSCRIBERS_H_
#define FBRESPONDGETSUBSCRIBERS_H_

#include "FbRespondBase.h"

/**
 * This class contains data received for /me/friends facebook request
 */
class FbRespondGetSubscribers : public FbRespondBase
{
public:
    /**
     * @brief Construct FbRespondGetFriends from JsonObject
     * @param[in] object - Json Object, from which FbRespondGetFriends will be constructed
     */
	FbRespondGetSubscribers(JsonObject* object);

    /**
     * @brief Destructor
     */
    virtual ~FbRespondGetSubscribers();

    /**
     * @brief Creates FbRespondGetFriends object fron JSON formatted text
     * @param data - JSON formatted text
     * @return - created FbRespondGetFriends object
     */
    static FbRespondGetSubscribers* createFromJson(char* data);

    /**
     * @brief Subscribers array
     */
    Eina_List * mSubscribersList;
};

#endif /* FBRESPONDGETSUBSCRIBERS_H_ */
