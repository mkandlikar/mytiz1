/*
 * FbRespondGetPostsDetailsBatch.h
 */

#ifndef FBRESPONDGETPOSTSDETAILSBATCH_H_
#define FBRESPONDGETPOSTSDETAILSBATCH_H_

#include "FbRespondGetPostDetails.h"

/**
 * @brief This class contains data received from Facebook for GetPostsDetailsBatch batch request
 */
class FbRespondGetPostsDetailsBatch : public FbRespondBase
{
public:
    /**
     * @brief Construct FbRespondGetPostsDetailsBatch from JsonObject
     * @param[in] object - Json Object, from which FbRespondGetPostsDetailsBatch will be constructed
     */
	FbRespondGetPostsDetailsBatch(JsonNode* object);

    /**
     * @brief Destructor
     */
    virtual ~FbRespondGetPostsDetailsBatch();

    /**
     * @brief Creates FbRespondGetPostsDetailsBatch object fron JSON formatted text
     * @param data - JSON formatted text
     * @return - created FbRespondGetEventsRequests object
     */
    static FbRespondGetPostsDetailsBatch* createFromJson(char* data);

    /**
     * @brief respond for batch request-1
     */
    FbRespondGetPostDetails * mRespondGetPostsDetailsRequests;

    /**
     * @brief User list from batch request-2
     */
    Eina_List * mPostsList;

    Eina_List* RemoveDataFromList(void *data);
};

#endif /* FBRESPONDGETPOSTSDETAILSBATCH_H_ */
