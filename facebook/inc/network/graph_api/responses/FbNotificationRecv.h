/*
 * FbNotificationRecv.h
 *
 *  Created on: Jul 7, 2015
 *      Author: ingsharm02
 */

#ifndef FBNOTIFICATIONRECV_H_
#define FBNOTIFICATIONRECV_H_

//The following class is used to store and parse the received notification. Though the received notifications are asynchronous and not graph
//but they are handled in similar manner so as to have a common logic with other graph API structures.

#include "FbRespondBase.h"

class FbNotificationRecv: public FbRespondBase {
public:
    enum Notification_Types {
        NotSupported,   // 0
        Poked,          // 1
        Stream,         // 2
        FriendRequest,  // 3
        Events,         // 4
        Groups          // 5
    };
    /**
     * @brief Construct FbNotificationRecv from JsonObject
     * @param[in] object - Json Object, from which FbNotificationRecv will be constructed
     * @param[in] notifType - Notification Type : Sync or Async Notification
     */
    FbNotificationRecv(JsonObject * object);

    /**
     * @brief Destructor
     */
    virtual ~FbNotificationRecv();

    /**
     * @brief Creates FbNotificationRecv object fron JSON formatted text received from Push Service
     * @param data - JSON formatted text
     * @return - created FbNotificationRecv object
     */
    static FbNotificationRecv* createFromJson(const char* data);

    bool mMessengerNotif;
    const char *mAuthorId;
    const char *mObjectId;
    const char *mSessionId;
    const char *mNtfType;
    const char *mNtfDisplayMessage;
    const char *mNtfId;
    const char *mUserId;
    const char *mWebLinkToOpen;
};

#endif /* FBNOTIFICATIONRECV_H_ */
