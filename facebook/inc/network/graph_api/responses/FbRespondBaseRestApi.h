/*
 * FbRespondBaseRestApi.h
 *
 *  Created on: Jul 1, 2015
 *      Author: RUINMKOL
 */

#ifndef FBRESPONDBASERESTAPI_H_
#define FBRESPONDBASERESTAPI_H_

#include <json-glib/json-glib.h>
/**
 * @brief This is base class for data received for rest api facebook requests
 */
class FbRespondBaseRestApi
{
public:
    /**
     * @brief Construct FbRespondBaseRestApi object from JsonNode
     * @param rootNode - JsonNode, from which FbRespondBaseRestApi will be constructed
     */
    FbRespondBaseRestApi(JsonNode* rootNode);

    /**
     * @brief Destructor
     */
    virtual ~FbRespondBaseRestApi();

    /**
     * @brief error code
     */
    int mErrorCode;

    /**
     * @brief error message
     */
    const char *mErrorMessage;

    /**
     * @brief - creates an instance of FbRespondBaseRestApi object from char buffer
     * @param data - char buffer
     * @return - returns created object
     */
    static FbRespondBaseRestApi* createFromJson(const char* data);
};

#endif /* FBRESPONDBASERESTAPI_H_ */
