/*
 * FbRespondGetUserList.h
 *
 *  Created on: May 20, 2015
 *      Author: RUINMKOL
 */

#ifndef FBRESPONDGETUSERLIST_H_
#define FBRESPONDGETUSERLIST_H_

#include "FbRespondBase.h"
#include "Friend.h"
#include "Summary.h"
#include <stdlib.h>

class FbRespondGetUserList: public FbRespondBase
{
public:
    /**
     * @brief Construct FbRespondGetUserList from JsonObject
     * @param[in] object - Json Object, from which FbRespondGetFriends will be constructed
     */
    FbRespondGetUserList(JsonNode* node);
    FbRespondGetUserList(JsonObject* object);
    virtual ~FbRespondGetUserList();

    /**
     * @brief Creates FbRespondGetUserList object fron JSON formatted text
     * @param data - JSON formatted text
     * @return - created FbRespondGetFriends object
     */
    static FbRespondGetUserList* createFromJson(char* data);

    /**
     * @brief Users array
     */
    Eina_List * mUserList;

    Eina_List* RemoveDataFromList(void *data);
};

#endif /* FBRESPONDGETUSERLIST_H_ */
