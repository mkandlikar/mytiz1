/*
 * FbRespondGetEditActions.h
 *
 *  Created on: May 11, 2016
 *      Author: ruibezruch
 */

#ifndef FBRESPONDGETEDITACTIONS_H_
#define FBRESPONDGETEDITACTIONS_H_

#include <eina_list.h>

#include "FbRespondBase.h"

class FbRespondGetEditActions: public FbRespondBase
{
public:
    FbRespondGetEditActions(JsonObject *object);
    virtual ~FbRespondGetEditActions();

    static FbRespondGetEditActions* createFromJson(char* data);

    Eina_List * mEditActionsList;
    Eina_List* RemoveDataFromList(void *data);
};

#endif /* FBRESPONDGETEDITACTIONS_H_ */
