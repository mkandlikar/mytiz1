/*
 *  FbRespondAbout.h
 *
 *  Created on: July 16, 2015
 *      Author: Manjunath Raja
 */

#ifndef FBRESPONDABOUT_H_
#define FBRESPONDABOUT_H_

#include <string>
#include <Elementary.h>

#include "FbRespondBase.h"


class FbRespondAbout : public FbRespondBase {
    public:
        Eina_List *mOverviewList;
        char* mAboutPicUrl;

        struct ListItem {
            std::string mHeading;
            std::string mInfo;
            std::string mPast;
            std::string mImage;
            bool mAt;
            bool mMore;
        };

        FbRespondAbout(JsonObject* JsonRoot);
        virtual ~FbRespondAbout();
        static FbRespondAbout* Parse(char* Respond);
};

#endif //FBRESPONDABOUT_H_
