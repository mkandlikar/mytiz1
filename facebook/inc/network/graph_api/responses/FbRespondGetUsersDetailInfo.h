/*
 * FbRespondGetUsersDetailInfo.h
 *
 *  Created on: Aug 4, 2015
 *      Author: RUINREKU
 */

#ifndef FBRESPONDGETUSERSDETAILINFO_H_
#define FBRESPONDGETUSERSDETAILINFO_H_

#include "FbRespondBase.h"
/**
 * This class contains data received for REQUEST_USERS_PROFILE facebook request
 */
class FbRespondGetUsersDetailInfo: public FbRespondBase
{
public:
    /**
     * @brief Construct FbRespondGetUsersDetailInfo from JsonObject
     * @param[in] object - Json Object, from which FbRespondGetUsersDetailInfo will be constructed
     */
	FbRespondGetUsersDetailInfo(JsonNode* rootNode);

    /**
     * @brief Destruction
     */
    virtual ~FbRespondGetUsersDetailInfo();

    /**
     * @brief Creates FbRespondGetUsersDetailInfo object from JSON formatted text
     * @param data - JSON formatted text
     * @return - created FbRespondGetUsersDetailInfo object
     */
    static FbRespondGetUsersDetailInfo* createFromJson(char* data);

    Eina_List* RemoveDataFromList(void *data);

    /**
     * @brief users detail info
     */
    Eina_List * mUsersDetailInfoList;
};

#endif /* FBRESPONDGETUSERSDETAILINFO_H_ */
