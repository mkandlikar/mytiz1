#ifndef FBRESPONDGETPOSTDETAILS_H_
#define FBRESPONDGETPOSTDETAILS_H_

#include "FbRespondBase.h"
#include "FbRespondGetPostDetails.h"

#include "Post.h"

class FbRespondGetPostDetails: public FbRespondBase
{
public:
    FbRespondGetPostDetails(JsonObject* object);
    virtual ~FbRespondGetPostDetails();

    /**
     * @brief - creates an instance of FbRespondPost object from char buffer
     * @param data - char buffer
     * @return - returns created object
     */
    static FbRespondGetPostDetails* createFromJson(char* data);

    Post * mPost;
};

#endif /* FBRESPONDGETPOSTDETAILS_H_ */
