/*
 * FbRespondGetMobileZeroCampain.h
 *
 *  Created on: Aug 20, 2015
 *      Author: ruinmkol
 */

#ifndef FBRESPONDGETMOBILEZEROCAMPAIN_H_
#define FBRESPONDGETMOBILEZEROCAMPAIN_H_

#include "FbRespondBaseRestApi.h"
#include <eina_list.h>

class FbRespondGetMobileZeroCampain: public FbRespondBaseRestApi
{
public:
    class RewriteRules
    {
    public:
        RewriteRules(JsonObject * object);
        virtual ~RewriteRules();

        //members
        const char * mMatcher;
        const char * mReplacer;
        const char * mMode;
     };
    /**
     * @brief Construct FbRespondGetMobileZeroCampain from JsonNode
     * @param[in] object - Json Node, from which FbRespondGetMobileZeroCampain will be constructed
     */
    FbRespondGetMobileZeroCampain(JsonNode* object);
    virtual ~FbRespondGetMobileZeroCampain();

    /**
     * @brief Creates FbRespondGetMobileZeroCampain object fron JSON formatted text
     * @param data - JSON formatted text
     * @return - created FbRespondGetMobileZeroCampain object
     */
    static FbRespondGetMobileZeroCampain* createFromJson(const char* data);

    /**
     * @brief campaign id (integer)
     */
    int mId;

    /**
     * @brief campaign status ('enabled', 'disabled', 'unknown')
     */
    const char * mStatus;

    /**
     * @brief campaign refresh interval (integer)
     */
    int mTtl;

    /**
     * @brief ip pool ('z-a', 'z-1')
     */
    const char * mIpPool;

    /**
     * @brief the rewrite rules for http requests (array)
     */
    Eina_List * mRewriteRules;

    /**
     * @brief a list of enabled ui features (array of strings)
     */
    Eina_List *mEnabledUiFeatures;

    /**
     * @brief carrier name (string)
     */
    const char * mCarrierName;

};

#endif /* FBRESPONDGETMOBILEZEROCAMPAIN_H_ */
