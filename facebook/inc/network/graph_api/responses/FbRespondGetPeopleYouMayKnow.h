#ifndef FBRESPONDGETPEOPLEYOUMAYKNOW_H_
#define FBRESPONDGETPEOPLEYOUMAYKNOW_H_

#include "FbRespondBaseRestApi.h"
#include "Person.h"

/**
 * @brief This class contains data received from Facebook for GetPeopleYouMayKnow request
 */
class FbRespondGetPeopleYouMayKnow: public FbRespondBaseRestApi
{
public:
    class FriendableUser : public Person
    {
    public:
        FriendableUser(JsonObject * object);
        FriendableUser(char **name, char **value);
        virtual ~FriendableUser();

        const char *GetName() { return mName; }
        const char *GetUserName() { return mUserName; }
        int GetMutualFriendsCount() { return mMutualFriends; }
        const char *GetPicSquareWithLogo() { return mPicSquareWithLogo; }

    private:
        //members
        const char * mName;
        const char * mUserName;
        int mMutualFriends;
        const char * mPicSquareWithLogo;
     };
    /**
     * @brief Construct FbRespondGetPeopleYouMayKnow from JsonNode
     * @param[in] object - Json Object, from which FbRespondGetPeopleYouMayKnow will be constructed
     */
    FbRespondGetPeopleYouMayKnow(JsonNode* object);

    /**
     * @brief Destructor
     */
    virtual ~FbRespondGetPeopleYouMayKnow();

    /**
     * @brief Creates FbRespondGetPeopleYouMayKnow object fron JSON formatted text
     * @param data - JSON formatted text
     * @return - created FbRespondGetPeopleYouMayKnow object
     */
    static FbRespondGetPeopleYouMayKnow* createFromJson(const char* data);

    //members
    /**
     * @brief list of found users
     */
    Eina_List * mPeople;

    Eina_List* RemoveDataFromList(void *data);
};

#endif /* FBRESPONDGETPEOPLEYOUMAYKNOW_H_ */
