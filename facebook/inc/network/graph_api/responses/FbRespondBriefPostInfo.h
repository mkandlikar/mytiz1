/*
 * FbRespondBriefPostInfo.h
 *
 * Created: Nov 20, 2015
 * Author: Dmitry.Shcherbakov@SymphonyTeleca.com
 */

#ifndef FBRESPONDBRIEFPOSTINFO_H_
#define FBRESPONDBRIEFPOSTINFO_H_

#include "FbRespondBase.h"

class FbRespondBriefPostInfo: public FbRespondBase
{
public:
    FbRespondBriefPostInfo( JsonNode* object );
    virtual ~FbRespondBriefPostInfo();

    Eina_List* RemoveDataFromList( void *data );

    static FbRespondBriefPostInfo* createFromJson(char* data);

    Eina_List* GetBriefPostInfoList();
private:
    Eina_List *m_BriefPostInfoList;
};

#endif /* FBRESPONDBRIEFPOSTINFO_H_ */
