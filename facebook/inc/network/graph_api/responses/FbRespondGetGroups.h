/*
 * FbRespondGetGroups.h
 *
 *  Created on: Jul 15, 2015
 *      Author: RUINREKU
 */

#ifndef FBRESPONDGETGROUPS_H_
#define FBRESPONDGETGROUPS_H_

#include "eina_list.h"
#include "FbRespondBase.h"
/**
 * This class contains data received for /me/groups facebook request
 */
class FbRespondGetGroups: public FbRespondBase
{
public:
    /**
     * @brief Construct FbRespondGetGroups from JsonObject
     * @param[in] object - Json Object, from which FbRespondGetGroups will be constructed
     */
	FbRespondGetGroups(JsonObject* object);

    /**
     * @brief Destruction
     */
    virtual ~FbRespondGetGroups();

    /**
     * @brief Creates FbRespondGetGroups object from JSON formatted text
     * @param data - JSON formatted text
     * @return - created FbRespondGetGroups object
     */
    static FbRespondGetGroups* createFromJson(char* data);

    /**
     * @brief groups array
     */
    Eina_List * mGroupsList;

    Eina_List* RemoveDataFromList(void *data);
};

#endif /* FBRESPONDGETGROUPS_H_ */
