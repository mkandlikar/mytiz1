#ifndef FBRESPONDGETFRIENDREQUESTSBATCH_H_
#define FBRESPONDGETFRIENDREQUESTSBATCH_H_

#include "FbRespondGetFriendRequests.h"

/**
 * @brief This class contains data received from Facebook for GetIcomingFriendRequests batch request
 */
class FbRespondGetFriendRequestsBatch : public FbRespondBase
{
public:
    /**
     * @brief Construct FbRespondGetFriendRequestsBatch from JsonObject
     * @param[in] object - Json Object, from which FbRespondGetFriendRequestsBatch will be constructed
     */
    FbRespondGetFriendRequestsBatch(JsonNode* object);

    /**
     * @brief Destructor
     */
    virtual ~FbRespondGetFriendRequestsBatch();

    /**
     * @brief Creates FbRespondGetFriendRequests object fron JSON formatted text
     * @param data - JSON formatted text
     * @return - created FbRespondGetFriendRequests object
     */
    static FbRespondGetFriendRequestsBatch* createFromJson(char* data);

    /**
     * @brief respond for batch request-1
     */
    FbRespondGetFriendRequests * mRespondGetFriendRequests;

    /**
     * @brief User list from batch request-2
     */
    Eina_List * mUsersList;

    Eina_List* RemoveDataFromList(void *data);

};


#endif /* FBRESPONDGETFRIENDREQUESTSBATCH_H_ */
