#ifndef FBRESPONDPOST_H_
#define FBRESPONDPOST_H_

#include "FbRespondBase.h"

#include <Eina.h>
#include <json-glib/json-glib.h>

/**
 *@brief This class intended for data received from facebook for POST http requests
 */
class FbRespondPost: public FbRespondBase
{
public:
    /**
     * @brief Construct FbRespondBase object from JsonObject
     * @param object - Json Object, from which FbRespondBase will be constructed
     */
    FbRespondPost(JsonObject * object);

    /**
     * @brief Destructor
     */
    virtual ~FbRespondPost();

    /**
     * @brief - an id of created object
     */
    const char * mId;

    /**
     * @brief - creates an instance of FbRespondPost object from char buffer
     * @param data - char buffer
     * @return - returns created object
     */
    static FbRespondPost* createFromJson(const char* data);
};


class FbResponsePhotoPost : public FbRespondPost {
public:
    static FbResponsePhotoPost* createFromJson(const char* data);
    inline Eina_List *GetNotSentPhotos() {return mNotSentPhotos;}
    ~FbResponsePhotoPost();

protected:
    FbResponsePhotoPost(JsonObject * object);

    /**
     * Number of photos that are not sent for this post during a batch request.
     */
    Eina_List *mNotSentPhotos;
};


#endif /* FBRESPONDPOST_H_ */
