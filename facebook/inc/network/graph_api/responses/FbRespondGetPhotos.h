/*
 * FbRespondGetPhotos.h
 *
 *  Created on: Jun 29, 2015
 *      Author: RUINMKOL
 */

#ifndef FBRESPONDGETPHOTOS_H_
#define FBRESPONDGETPHOTOS_H_

#include "FbRespondBase.h"
/**
 * This class contains data received for /me/photos facebook request
 */
class FbRespondGetPhotos: public FbRespondBase
{
public:
    /**
     * @brief Construct FbRespondGetPhotos from JsonObject
     * @param[in] object - Json Object, from which FbRespondGetPhotos will be constructed
     */
    FbRespondGetPhotos(JsonObject* object);

    /**
     * @brief Destruction
     */
    virtual ~FbRespondGetPhotos();

    /**
     * @brief Creates FbRespondGetPhotos object fron JSON formatted text
     * @param data - JSON formatted text
     * @return - created FbRespondGetFriends object
     */
    static FbRespondGetPhotos* createFromJson(char* data);

    inline const Eina_List *GetPhotosList() const {return mPhotosList;}

private:
    /**
     * @brief photos array
     */
    Eina_List *mPhotosList;
};

#endif /* FBRESPONDGETPHOTOS_H_ */
