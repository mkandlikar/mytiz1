#ifndef FBRESPONSESEARCHPLACES_H_
#define FBRESPONSESEARCHPLACES_H_

#include "FbRespondBase.h"
/**
 * This class contains data received for /search?type=place facebook request
 */
class FbResponseSearchPlaces: public FbRespondBase {
public:
    /**
     * @brief Construct FbResponseSearchPlaces from JsonObject
     * @param[in] object - Json Object, from which FbResponseSearchPlaces will be constructed
     */
    FbResponseSearchPlaces(JsonObject* object);

    /**
     * @brief Destruction
     */
    virtual ~FbResponseSearchPlaces();

    /**
     * @brief Creates FbResponseSearchPlaces object fron JSON formatted text
     * @param data - JSON formatted text
     * @return - created FbResponseSearchPlaces object
     */
    static FbResponseSearchPlaces* createFromJson(char* data);

    /**
     * @brief albums array
     */
    Eina_List * mPlacesList;
};

#endif /* FBRESPONSESEARCHPLACES_H_ */
