#ifndef FBRESPONDEDITCMNTMESSAGE_H_
#define FBRESPONDEDITCMNTMESSAGE_H_


#include "FbRespondBase.h"

#include <json-glib/json-glib.h>

/**
 *@brief This class intended for data received from facebook for POST http requests
 */
class FbRespondEditCommentMessage: public FbRespondBase
{
public:
    /**
     * @brief Construct FbRespondBase object from JsonObject
     * @param object - Json Object, from which FbRespondBase will be constructed
     */
    FbRespondEditCommentMessage(JsonObject * object);

    /**
     * @brief Destructor
     */
    virtual ~FbRespondEditCommentMessage();

    /**
     * @brief - an id of created object
     */
    bool mIsSuccess;

    /**
     * @brief - creates an instance of FbRespondEditPostMessage object from char buffer
     * @param data - char buffer
     * @return - returns created object
     */
    static FbRespondEditCommentMessage* createFromJson(char* data);
};

#endif /* FBRESPONDEDITPOSTMESSAGE_H_ */
