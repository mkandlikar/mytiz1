#ifndef FBRESPONSEIMAGEDETAILS_H_
#define FBRESPONSEIMAGEDETAILS_H_

#include "FbRespondBase.h"

class Photo;
/**
 *@brief This class intended for data received from facebook for POST http requests
 */
class FbResponseImageDetails: public FbRespondBase
{
public:
    /**
     * @brief Construct FbRespondBase object from JsonObject
     * @param object - Json Object, from which FbRespondBase will be constructed
     */
    FbResponseImageDetails(JsonObject * object);

    /**
     * @brief Destructor
     */
    virtual ~FbResponseImageDetails();

    /**
     * @brief - an id of created object
     */
    Photo *mPhoto;

    /**
     * @brief - creates an instance of FbRespondPost object from char buffer
     * @param data - char buffer
     * @return - returns created object
     */
    static FbResponseImageDetails* createFromJson(char* data);
};

#endif /* FBRESPONSEIMAGEDETAILS_H_ */
