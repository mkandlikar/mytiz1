#ifndef GRAPH_DATA_FBRESPONSELIKESSUMMARY_H_
#define GRAPH_DATA_FBRESPONSELIKESSUMMARY_H_

#include "FbRespondBase.h"

class LikesSummary;

class FbResponseLikesSummary: public FbRespondBase
{
public:
    FbResponseLikesSummary(JsonObject * object);

    virtual ~FbResponseLikesSummary();

    /**
     * @brief - an id of created object
     */
    char * mId;

    /**
     * @brief - parsed likes Summary info
     */
    LikesSummary * mLikesSummaryObj;

    /**
     * @brief - an id of other user who liked this post
     */
    char * mOtherWhoLikedId;

    /**
     * @brief - a name of other user who liked this post
     */
    char * mOtherWhoLikedName;

    /**
     * @brief - creates an instance of FbResposeLikesSummary object from char buffer
     * @param data - char buffer
     * @return - returns created object
     */
    static FbResponseLikesSummary* createFromJson(const char* data);
};

#endif /* GRAPH_DATA_FBRESPONSELIKESSUMMARY_H_ */
