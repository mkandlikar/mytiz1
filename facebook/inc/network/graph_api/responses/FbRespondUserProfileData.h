/*
 * FbRespondUserProfileData.h
 *
 *  Created on: Jul 13, 2016
 *      Author: ruibezruch
 */

#ifndef FBRESPONDUSERPROFILEDATA_H_
#define FBRESPONDUSERPROFILEDATA_H_

#include <eina_list.h>
#include "FbRespondBase.h"

class FbRespondUserProfileData: public FbRespondBase
{
public:
    FbRespondUserProfileData(JsonObject* object);
    virtual ~FbRespondUserProfileData();

    static FbRespondUserProfileData* createFromJson(char* data);

    Eina_List* RemoveDataFromList(void *data);
    Eina_List* mDataList;
};

#endif /* FBRESPONDUSERPROFILEDATA_H_ */
