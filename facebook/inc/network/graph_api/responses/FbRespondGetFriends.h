/*
 * GetFriendsListRespond.h
 *
 *  Created on: Apr 13, 2015
 *      Author: RUINMKOL
 */

#ifndef GETFRIENDSLISTRESPOND_H_
#define GETFRIENDSLISTRESPOND_H_

#include "FbRespondBase.h"
#include "Friend.h"

class Summary;
/**
 * This class contains data received for /me/friends facebook request
 */
class FbRespondGetFriends : public FbRespondBase
{
public:
    /**
     * @brief Construct FbRespondGetFriends from JsonObject
     * @param[in] object - Json Object, from which FbRespondGetFriends will be constructed
     */
    FbRespondGetFriends(JsonObject* object);

    /**
     * @brief Destructor
     */
    virtual ~FbRespondGetFriends();

    /**
     * @brief Creates FbRespondGetFriends object fron JSON formatted text
     * @param data - JSON formatted text
     * @return - created FbRespondGetFriends object
     */
    static FbRespondGetFriends* createFromJson(char* data);

    /**
     * @brief Friends array
     */
    Eina_List * mFriendsList;

    Eina_List* RemoveDataFromList(void *data);

    /**
     * @brief Summary
     */
    Summary * mSummary;
};

#endif /* GETFRIENDSLISTRESPOND_H_ */
