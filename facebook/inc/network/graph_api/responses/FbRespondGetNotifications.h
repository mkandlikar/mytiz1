#ifndef FBRESPONDGETNOTIFICATIONS_H_
#define FBRESPONDGETNOTIFICATIONS_H_

#include "FbRespondBase.h"

#include <Elementary.h>


class FbRespondGetNotifications: public FbRespondBase
{
public:
    FbRespondGetNotifications(JsonObject *object);
    virtual ~FbRespondGetNotifications();

    Eina_List* RemoveDataFromList(void *data);

    Eina_List * mNotificationList;

    int mUnseenCount;
private:
    static FbRespondGetNotifications* createFromJson(char* data);
};

#endif /* FBRESPONDGETNOTIFICATIONS_H_ */
