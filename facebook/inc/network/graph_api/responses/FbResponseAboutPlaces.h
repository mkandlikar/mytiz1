/*
 * FbResponseAboutPlaces.h
 *
 *  Created on: SEP 17, 2015
 *      Author: Jeyaramakrishnan Sundar
 */

#ifndef FB_RESPONSE_ABOUT_PLACES_H
#define FB_RESPONSE_ABOUT_PLACES_H

#include <string>
#include "FbRespondBase.h"
#include "GraphObject.h"

class FbResponseAboutPlaces: public FbRespondBase
{
public:
    class PlacesList : public GraphObject
    {
    public:
        PlacesList(JsonObject* object);
        virtual ~PlacesList();

        const char* place_name;
        const char* created_time;
        const char* city_name;
        const char* country;
        const char* mPicSquareWithLogo;
        const char* mWebLinkToOpen;
        double mLatitude;
        double mLongitude;

     };
    FbResponseAboutPlaces(JsonObject* object);
    char *mNextpage;
    virtual ~FbResponseAboutPlaces();
    Eina_List* RemoveDataFromList(void *data);
    static FbResponseAboutPlaces* createFromJson(const char* data);

    Eina_List * mListofPlaces;
};

#endif /* FB_RESPONSE_ABOUT_PLACES_H */
