/*
 * FbResponseGroupDetails.h
 *
 *  Created on: Nov 19, 2015
 *      Author: rupandreev
 */

#ifndef FBRESPONSEGROUPDETAILS_H_
#define FBRESPONSEGROUPDETAILS_H_

#include "FbRespondBase.h"
/**
 *@brief This class intended for data received from facebook for POST http requests
 */
class FbResponseGroupDetails: public FbRespondBase
{
public:
    /**
     * @brief Construct FbRespondBase object from JsonObject
     * @param object - Json Object, from which FbRespondBase will be constructed
     */
    FbResponseGroupDetails(JsonObject * object);

    /**
     * @brief Destructor
     */
    virtual ~FbResponseGroupDetails();

    const char * mCoverUrl;
    /**
     * @brief - creates an instance of FbRespondPost object from char buffer
     * @param data - char buffer
     * @return - returns created object
     */
    static FbResponseGroupDetails* createFromJson(char* data);
};

#endif /* FBRESPONSEGROUPDETAILS_H_ */
