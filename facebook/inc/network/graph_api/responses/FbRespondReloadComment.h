/*
 * FbRespondReloadComment.h
 *
 *  Created on: Jan 12, 2016
 *      Author: dvasin
 */

#ifndef FBRESPONDRELOADCOMMENT_H_
#define FBRESPONDRELOADCOMMENT_H_


#include <json-glib/json-glib.h>

#include "Comment.h"

/**
 *@brief This class intended for data received from facebook for POST http requests
 */
class FbRespondReloadComment: public FbRespondBase
{
public:
    /**
     * @brief Construct FbRespondBase object from JsonObject
     * @param object - Json Object, from which FbRespondBase will be constructed
     */
    FbRespondReloadComment(JsonObject * object);

    /**
     * @brief Destructor
     */
    virtual ~FbRespondReloadComment();

    Comment* mComment;

    /**
     * @brief - creates an instance of FbRespondEditPostMessage object from char buffer
     * @param data - char buffer
     * @return - returns created object
     */
    static FbRespondReloadComment* createFromJson(char* data);
};




#endif /* FBRESPONDRELOADCOMMENT_H_ */
