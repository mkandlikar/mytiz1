#ifndef FBRESPONSEFRIENDDETAILS_H_
#define FBRESPONSEFRIENDDETAILS_H_

#include "FbRespondBase.h"

/**
 *@brief This class intended for data received from facebook for POST http requests
 */
class FbResponseFriendDetails: public FbRespondBase
{
public:
    /**
     * @brief Construct FbRespondBase object from JsonObject
     * @param object - Json Object, from which FbRespondBase will be constructed
     */
    FbResponseFriendDetails(JsonObject * object);

    /**
     * @brief Destructor
     */
    virtual ~FbResponseFriendDetails();

    /**
     * @brief - an id of created object
     */
    char * mId;
    const char * mCoverUrl;
    const char * mPictureUrl;
    const char * mEducation;
    const char * mEmployer;
    const char * mLocation;
    const char * mPosition;

    /**
     * @brief - creates an instance of FbRespondPost object from char buffer
     * @param data - char buffer
     * @return - returns created object
     */
    static FbResponseFriendDetails* createFromJson(char* data);
};

#endif /* FBRESPONSEFRIENDDETAILS_H_ */
