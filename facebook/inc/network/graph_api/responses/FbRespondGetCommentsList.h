/*
 * FbRespondGetCommentsList.h
 *
 *  Created on: May 24, 2015
 *      Author: RUINMKOL
 */

#ifndef FBRESPONDGETCOMMENTSLIST_H_
#define FBRESPONDGETCOMMENTSLIST_H_

#include "FbRespondBase.h"

/**
 * @brief This class contains facebook respond for GetCommentsList request
 */
class FbRespondGetCommentsList: public FbRespondBase
{
public:
    /**
     * @brief This class contains summary data for GetCommentsList request
     */
    class Summary
    {
    public:
        /**
         * @brief Construct Summary object from JsonObject
         * @param object - Json Object, from which Summary will be constructed
         */
        Summary(JsonObject * object);

        /**
         * @brief Destructor
         */
        virtual ~Summary();

        /**
         * @brief The count of comments on this node.
         * It is important to note that this value is changed depending on the filter modifier being used
         *  (where comment replies are available):
         */
        int mTotalCount;

        /**
         * @brief Order in which comments were returned.
         *  - ranked indicates the most interesting comments are sorted first.
         *  - chronological indicates comments are sorted by the oldest comments first.
         */
        const char * mOrder;
    };

    /**
     * @brief Construct FbRespondGetCommentsList from JsonObject
     * @param[in] object - Json Object, from which FbRespondGetCommentsList will be constructed
     */
    FbRespondGetCommentsList(JsonObject* object);

    /**
     * @brief Destructor
     */
    virtual ~FbRespondGetCommentsList();

    /**
     * @brief Creates FbRespondGetCommentsList object fron JSON formatted text
     * @param data - JSON formatted text
     * @return - created FbRespondGetCommentsList object
     */
    static FbRespondGetCommentsList* createFromJson(char* data);

    Eina_List* GetCommentsList();

    Eina_List* RemoveDataFromList(void *data);

private:
    /**
     * @brief Friends array
     */
    Eina_List * mCommentsList;

    /**
     * @brief Summary
     */
    Summary * mSummary;

};

#endif /* FBRESPONDGETCOMMENTSLIST_H_ */
