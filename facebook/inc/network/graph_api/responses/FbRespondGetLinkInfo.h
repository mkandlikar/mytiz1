#ifndef FBRESPONDGETLINKINFO_H_
#define FBRESPONDGETLINKINFO_H_

#include "FbRespondBase.h"
#include "Url.h"
/*
 *
 */
class FbRespondGetLinkInfo: public FbRespondBase
{
public:
    FbRespondGetLinkInfo(JsonObject* object);
    virtual ~FbRespondGetLinkInfo();

    /**
     * @brief Creates FbRespondGetLinkInfo object fron JSON formatted text
     * @param data - JSON formatted text
     * @return - created FbRespondGetFriends object
     */
    static FbRespondGetLinkInfo* createFromJson(char* data);

    /**
     * @brief Url object
     */
    Url * mUrl;
};

#endif /* FBRESPONDGETLINKINFO_H_ */
