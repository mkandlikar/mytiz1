#ifndef FBRESPONSEALBUMDETAILS_H_
#define FBRESPONSEALBUMDETAILS_H_

#include "FbRespondBase.h"

class Album;
/**
 *@brief This class intended for data received from facebook for POST http requests
 */
class FbResponseAlbumDetails: public FbRespondBase
{
public:
    /**
     * @brief Construct FbRespondBase object from JsonObject
     * @param object - Json Object, from which FbRespondBase will be constructed
     */
    FbResponseAlbumDetails(JsonObject * object);

    /**
     * @brief Destructor
     */
    virtual ~FbResponseAlbumDetails();

    /**
     * @brief - creates an instance of FbRespondPost object from char buffer
     * @param data - char buffer
     * @return - returns created object
     */
    static FbResponseAlbumDetails* createFromJson(char* data);

    inline Album *GetAlbum() {return mAlbum;}

private:
    /**
     * @brief - an id of created object
     */
    Album *mAlbum;
};

#endif /* FBRESPONSEALBUMDETAILS_H_ */
