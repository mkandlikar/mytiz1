/*
 *  FbResponseAboutMore.h
 *
 *  Created on: Aug 20, 2015
 *      Author: Manjunath Raja
 */

#ifndef FBRESPONDABOUTMORE_H_
#define FBRESPONDABOUTMORE_H_

#include <string>
#include <Elementary.h>

#include "FbRespondBase.h"
#include "ActionBase.h"

class FbResponseAboutMore : public FbRespondBase {
    public:
        std::string mUserName;
        Eina_List* mWorkList;

        struct ListItemWork {
            ActionAndData* mAction;
            std::string mHeading;
            std::string mAddWork;
            std::string mImage;
            std::string mName;
            std::string mDesignation;
            std::string mDuration;
            std::string mLocation;
        };

        FbResponseAboutMore(JsonObject* JsonRoot);
        virtual ~FbResponseAboutMore();
        static FbResponseAboutMore* Parse(char* Response);
};

#endif //FBRESPONDABOUTMORE_H_
