#ifndef COMMENTPRESENTER_H_
#define COMMENTPRESENTER_H_

#include "Comment.h"
#include <string>

class CommentPresenter {
public:
    CommentPresenter(Comment& comment);
    ~CommentPresenter();

    std::string GetCommentMessage();
    std::string GetUserWithReply();
    std::string GetRepliesCount();
private:
    Comment& mComment;
};

#endif /* COMMENTPRESENTER_H_ */
