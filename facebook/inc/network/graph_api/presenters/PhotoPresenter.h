#ifndef PHOTOPRESENTER_H_
#define PHOTOPRESENTER_H_

#include "Photo.h"
#include "PresenterHelpers.h"
#include <string>

class PhotoPresenter {
public:
    PhotoPresenter(Photo& photo);
    ~PhotoPresenter();

    std::string GetName(PresenterHelpers::Mode mode = PresenterHelpers::eFeed);
    std::string GetLikesAndCommentsCount();
private:
    std::string GetLikesCount();
    std::string GetCommentsCount();
    std::string GenerateTextWithTags(const std::string& originalText);

    Photo& mPhoto;
};

#endif /* PHOTOPRESENTER_H_ */
