#ifndef POSTPRESENTER_H_
#define POSTPRESENTER_H_

#include "PresentableAsPost.h"

class PostPresenter {
public:
    PostPresenter(PresentableAsPost& post);
    ~PostPresenter();

    std::string GetApplicationName();
    std::string GetFromName();
    std::string GetCaption();
    std::string GetCommentsCount();
    std::string PrepareFriendTag(const class Friend& friendDesc);
    std::string GetLikesCount();
    std::string GetImageLikes();
    std::string GetLikesAndCommentsStats();
    std::string GetImageLikesAndComments();
    std::string GetLocationBox();
    std::string GetLocationPlace();
    std::string GetLocationType();
    std::string GetMessage(bool createContinueReading = false);
    std::string GetMultipleLikeBox();
    std::string GetStory();
    std::string CutMessage(bool createContinueReading = false);
    std::string ComposeHeaderFromNameTagsAndLocation();
    std::string ComposeFullTextMessageWithTagsAndLocation(bool createContinueReading = false);
    std::string GetLinkName();
    std::string GetPollOption();
    std::string GetViaName();
    std::string GetToStory();
    std::string GetPageDescription();
private:
    std::string GetFriendLocationList();
    std::string GetImageComments();
    void ComposeWithTagsAndLocationSubstr(std::stringstream& composedSubstr);
    void AppendContinueReading(std::string& message);

    PresentableAsPost& mPost;
};

#endif /* POSTPRESENTER_H_ */
