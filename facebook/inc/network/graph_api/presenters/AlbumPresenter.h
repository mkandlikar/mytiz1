#ifndef ALBUMPRESENTER_H_
#define ALBUMPRESENTER_H_

#include "Album.h"
#include <string>

class AlbumPresenter {
public:
    AlbumPresenter(Album& album);
    ~AlbumPresenter();

    std::string GenPhotosNumber();
private:
    Album& mAlbum;
};

#endif /* ALBUMPRESENTER_H_ */
