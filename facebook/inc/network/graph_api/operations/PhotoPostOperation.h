#ifndef PHOTOPOSTOPERATION_H_
#define PHOTOPOSTOPERATION_H_

#include <notification.h>
#include "bundle.h"
#include "Operation.h"
#include "GraphRequest.h"

class PhotoPostOperation: public Operation {
public:
    class PhotoPostDescription {
    public:
        PhotoPostDescription(const char * imageFileName, const char * id, const char * caption, bundle * paramsBundle, const char *thumbnailPath, const char * originalPath, PhotoPostOperation *operation);
        ~PhotoPostDescription();

        void SetPhotoId(const char *photoId);
        const char *GetPhotoId() { return mPhotoId;};
        const char *GetCaption() { return mCaption;};
        const char* GetFilePath() { return mImageFileName;};
        const char* GetThumbnailPath() { return mThumbnailPath; };
        const char * GetOriginalPath() { return mOriginalPath; };
        bundle * GetBundle() { return mParams; }
        inline PhotoPostOperation *GetOperation() const {return mOperation;}
        inline void SetRequest(GraphRequest *request) {mRequest = request;}
        inline GraphRequest *GetRequest() const {return mRequest;}

    private:
        char * mImageFileName;
        char * mId;
        bundle * mParams;
        char * mPhotoId;
        char * mCaption;
        char * mThumbnailPath;
        char * mOriginalPath;
        PhotoPostOperation *mOperation;
        GraphRequest *mRequest;
};

    PhotoPostOperation(OperationType opType,
                       const std::string& userId,
                       bundle* paramsBundle,
                       Eina_List *selectedMediaList,
                       const char* msgUTF8,
                       const char* privacy,
                       const std::list<Friend>& taggedFriends,
                       const std::string& privacyType,
                       const PCPlace* place,
                       OperationDestination opDestination = OD_None);
    ~PhotoPostOperation() override;

    void CurrentStepComplete() override;
    bool DoNextStep() override;

    bundle* GetBundle() override;
    void Update(AppEventId eventId, void* data) override;

    Eina_List* GetPhotos();
    const char* GetDestinationId() override;
    void PauseOperation() override;
    void ResetOperation() override;
    bool IsFeedOperation() override;
    void PrepareToBeRemoved() override;
    void PutInErrorState() override;
protected:
    bool CurrentStepExecute() override;

    static void on_photo_upload_request_completed(void* object, char* responseData, int code);
    static void on_post_feed_request_completed(void* object, char* responseData, int code);

    PhotoPostOperation::PhotoPostDescription *GetPhotoDescriptionByIndex(unsigned int index);
    void UploadNextPhoto();

    void UpdateProgress(bool isForced);

    void OnOperationComplete() override;
    bool Cancel() override;

    static void update_ongoing_notification(void * data);

    bundle* mParamsBundle;
    Sptr<GraphRequest> mRequest;
    Eina_List *mPhotoDescriptionList;
    double mPrevProgress;
    notification_h mOngoingNotification;
    char *mUploadedPostId;
    bool mIsSubscribedToProgress;
    int mSentPhotos;
    Ecore_Job *mOngoingNotificationJob;

private:
    bool ReStart() override;
};

#endif /* PHOTOPOSTOPERATION_H_ */
