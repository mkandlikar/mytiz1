#ifndef UTILITIES_PHOTOTAGOPERATION_H_
#define UTILITIES_PHOTOTAGOPERATION_H_

#include "Operation.h"

class PhotoTagOperation : public Operation {
public:
    PhotoTagOperation(OperationType opType, const char *photoId, Eina_List * tags);
    ~PhotoTagOperation();

private:
    static void on_create_tag_completed(void* object, char* response, int code);
    static void on_delete_tag_completed(void* object, char* response, int code);

    static Eina_List *mPhotoTagOperationsList;
    Eina_List * mPhotoTagList;
    const char * mPhotoId;
protected:
    bool CurrentStepExecute() override;
    OperationType mOpType;
    GraphRequest *mRequest;
};

#endif /* UTILITIES_PHOTOTAGOPERATION_H_ */
