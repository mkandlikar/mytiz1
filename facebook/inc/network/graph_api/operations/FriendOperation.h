#ifndef FRIEND_OPERATION_H_
#define FRIEND_OPERATION_H_

#include "Operation.h"

class FriendOperation : public Operation {
public:
    FriendOperation(OperationType opType, const char *userId, const char *postId = nullptr);
    ~FriendOperation();

    static bool IsOperationStarted(const char *userId);

private:
    void SendPostponedEvent();
    static int friend_operations_comparator(const void *data1, const void *data2);
    static void on_unfriend_friend_completed(void* object, char* response, int code);
    static void on_block_completed(void* object, char* response, int code);
    static void on_confirm_friend_completed(void* object, char* response, int code);
    static void on_unfollow_completed(void* object, char* response, int code);
    static void on_follow_completed(void* object, char* respond, int code);

    static Eina_List *mFriendOperationsList;

    bool mFriendRequestConfirmed;
    bool mUpdateUnfriendedUserStatus;
    std::string mPostId;

protected:
    bool CurrentStepExecute() override;

    OperationType mOpType;
    GraphRequest *mRequest;
};

#endif /* FRIEND_OPERATION_H_ */

