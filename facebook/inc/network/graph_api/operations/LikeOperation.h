#ifndef LIKEOPERATION_H_
#define LIKEOPERATION_H_

#include "Operation.h"
#include "GraphRequest.h"

#include "AppEvents.h"
#include "Application.h"
#include "CSmartPtr.h"
#include "ErrorHandling.h"
#include "FacebookSession.h"
#include "FbResponseLikesSummary.h"
#include "Log.h"
#include "ScreenBase.h"
#include "Utils.h"
#include "WidgetFactory.h"

#include <functional>
#include <memory>
#include <string>

static constexpr char LIKE_OPERATION_TAG[] = "Facebook::LikeOperation";

template <typename LikeTarget = Post> class LikeOperation:
    public Operation {

public:
    LikeOperation(const std::string& objectId, OperationType opType, ActionAndData *data = nullptr):
        Operation({}, ""),
        mObjectId(objectId),
        mOpType(opType),
        mRequest(nullptr),
        mLikeTarget(nullptr),
        mLikeActNData(data) {
        Log::debug_tag(LIKE_OPERATION_TAG, "LikeOperation::LikeOperation(): objectId: `%s`", mObjectId.c_str());
        Log::debug_tag(LIKE_OPERATION_TAG, "LikeOperation::LikeOperation(): operationType: `%d`", opType);

        if ( std::is_same<LikeTarget, Post>::value || std::is_same<LikeTarget, Photo>::value) {
            //use like target in case of Post or Photo
            assert(data);
            assert(data->mData);
            mLikeTarget = data->mData;
            Log::debug_tag(LIKE_OPERATION_TAG, "LikeOperation::LikeOperation(): likeTarget: `%p`", mLikeTarget.Get());
        }
        mStepCount = 1;
    }
    ~LikeOperation() override {
        Log::debug_tag(LIKE_OPERATION_TAG, "LikeOperation::~LikeOperation()");
    }

protected:
    bool CurrentStepExecute() override {
        using namespace std::placeholders;
        Log::debug_tag(LIKE_OPERATION_TAG, "LikeOperation::CurrentStepExecute()");
        switch(mOpType) {
            case OT_Like:
                mRequest = FacebookSession::GetInstance()->Like(mObjectId, true, std::bind(std::mem_fn(&LikeOperation<LikeTarget>::DoLikeCB), this, _1, _2));
                break;
            case OT_Unlike:
                mRequest = FacebookSession::GetInstance()->Like(mObjectId, false, std::bind(std::mem_fn(&LikeOperation<LikeTarget>::DoLikeCB), this, _1, _2));
                break;
            case OT_GetLikesSummary:
                mRequest = FacebookSession::GetInstance()->GetLikesSummary(mObjectId, std::bind(std::mem_fn(&LikeOperation<LikeTarget>::GetLikesCountCB), this, _1, _2));
                break;
            default:
                break;
        }
        Log::debug_tag(LIKE_OPERATION_TAG, "LikeOperation::CurrentStepExecute() submitted new Graph request");
        return mRequest;
    }

    bool IsErrorNotificationRequired() const override {return false;}

    void RollbackLike() {// works for post/photos only
        Log::debug_tag(LIKE_OPERATION_TAG, "LikeOperation::RollbackLike()");
        if ( ActionAndData::IsAlive(mLikeActNData) ) {
            mLikeTarget->ToggleLike();
            AppEvents::Get().Notify(eLIKE_COMMENT_EVENT, const_cast<char*>(mLikeTarget->GetId()));
        } else {
            Log::error( "LikeOperation::RollbackLike->related ActionAndData has been destroyed by someone");
        }

    }

    void GetLikesCountCB(const std::string& response, CURLcode curlcode) {
        Log::debug_tag(LIKE_OPERATION_TAG, "LikeOperation::GetLikesCountCB()");
        if (mRequest) {
            GraphRequest* requestRawPtr = mRequest.Get();
            FacebookSession::GetInstance()->ReleaseGraphRequest(requestRawPtr);
            mRequest = nullptr;
        }
        mErrorCode = curlcode;

        if(curlcode == CURLE_OK) {
            Log::debug_tag(LIKE_OPERATION_TAG, "LikeOperation::GetLikesCountCB()->curlcode is OK");
            Log::debug_tag(LIKE_OPERATION_TAG, "LikeOperation::GetLikesCountCB() response: `%s`", response.c_str());
            std::unique_ptr<FbResponseLikesSummary> likesSummaryResponse(FbResponseLikesSummary::createFromJson(response.c_str()));
            if (!likesSummaryResponse) {
                Log::error( "LikeOperation::GetLikesCountCB()->can't parse response");
            } else if (likesSummaryResponse->mError) {
                    Log::error("LikeOperation::GetLikesCountCB(), error = %s", likesSummaryResponse->mError->mMessage);
            } else {
                if ( ActionAndData::IsAlive(mLikeActNData) ) {
                    if(likesSummaryResponse->mId) {
                        if( strcmp(mLikeTarget->GetId(),likesSummaryResponse->mId) == 0) {
                            mLikeTarget->SetLikesCount(likesSummaryResponse->mLikesSummaryObj->mTotalCount);
                            mLikeTarget->SetCanLike(likesSummaryResponse->mLikesSummaryObj->mCanLike);
                            mLikeTarget->SetHasLiked(likesSummaryResponse->mLikesSummaryObj->mHasLiked);
                            mLikeTarget->SetLikeFromId(SAFE_STRDUP(likesSummaryResponse->mOtherWhoLikedId));
                            mLikeTarget->SetLikeFromName(SAFE_STRDUP(likesSummaryResponse->mOtherWhoLikedName));
                            if (std::is_same<LikeTarget, Post>::value) {
                                Log::info("LikeOperation::GetLikesCountCB->update posts with id = %s", mLikeTarget->GetId());
                                AppEvents::Get().Notify(eUPDATE_POST_LIKES_SUMMARY, mLikeTarget.Get());// to update post provider or single post screen
                            }// else should be implemented, if we'll support refresh summary for Photo object type
                            AppEvents::Get().Notify(eLIKE_COMMENT_EVENT, const_cast<char*>(mLikeTarget->GetId())); // update UI
                        }
                    }
                } else {
                    Log::error( "LikeOperation::GetLikesCountCB()->related ActionAndData has been destroyed by someone");
                }
            }
            CurrentStepComplete();
        } else {
            PutInErrorState();
        }
    }

    void DoLikeCB(const std::string& response, CURLcode curlcode) {
        Log::debug_tag(LIKE_OPERATION_TAG, "LikeOperation::DoLikeCB() curl code: `%d`", curlcode);
        mErrorCode = curlcode;
        if (mRequest) {
            GraphRequest* requestRawPtr = mRequest.Get();
            FacebookSession::GetInstance()->ReleaseGraphRequest(requestRawPtr);
            mRequest = nullptr;
        }

        if (curlcode == CURLE_OK) {

            if (!mLikeTarget) {//in case of comments LikeTarget is nullptr
                Log::error( "LikeOperation::DoLikeCB()->CB should be interrupted for comment");
                CurrentStepComplete();
                return;
            }

            Log::debug_tag(LIKE_OPERATION_TAG, "LikeOperation::DoLikeCB() curlcode is OK");
            Log::debug_tag(LIKE_OPERATION_TAG, "LikeOperation::DoLikeCB() response: `%s`", response.c_str());
            std::unique_ptr<FbRespondBase> baseResponse(FbRespondBase::createFromJson(response.c_str()));
            //graph api was changed, now "obj_id/likes?fields=picture,name" does not return {"success":true} response
            //but returns {"id":"obj_id"}, so we rollback 'like' if response invalid or "error" field is presented in response
            //or we could edit REQUEST_LIKES to "obj_id/likes" to get {"success":true} like before
            if (!baseResponse)  {
                Log::error( "LikeOperation::DoLikeCB() can't parse response");
                RollbackLike();
            }
            else if (baseResponse->mError)
            {
                if( baseResponse->mError->mType == Error::eOAuthException && baseResponse->mError->mCode == 100) {
                    Utils::ShowToast(Application::GetInstance()->GetTopScreen()->getMainLayout(), i18n_get_text("IDS_POST_IS_NOT_AVAILABLE"));
                }
                Log::error( "LikeOperation::DoLikeCB() response with error");
                RollbackLike();
            }
            // all related posts will be updated
            if (std::is_same<LikeTarget, Post>::value) {// update likes data only for Posts!
                AppEvents::Get().Notify(eUPDATE_POST_LIKES_SUMMARY, mLikeTarget.Get());
                AppEvents::Get().Notify(eLIKE_COMMENT_EVENT, const_cast<char*>(mLikeTarget->GetId()));
                Log::info("LikeOperation::DoLikeCB->update posts with id = %s",mLikeTarget->GetId());
            }
            CurrentStepComplete();
        } else {
            PutInErrorState();
        }
    }

    std::string mObjectId;
    OperationType mOpType;
    Sptr<GraphRequest> mRequest;
    Sptr<LikeTarget> mLikeTarget;//contains pointer to Post, Photo or nothing in case of Comment object
    ActionAndData* mLikeActNData;// ActionAndData is needed for checking is UI object alive or not
};

#endif /* LIKEOPERATION_H_ */
