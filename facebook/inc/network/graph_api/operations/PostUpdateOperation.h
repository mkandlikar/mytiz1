#ifndef POSTUPDATEOPERATION_H_
#define POSTUPDATEOPERATION_H_

#include "Operation.h"

class PostUpdateOperation: public Operation {
public:
    PostUpdateOperation(OperationType opType,
                                  const std::string& postId,
                                  bundle* paramsBundle,
                                  const std::list<Friend>& taggedFriends,
                                  const std::string& privacyType,
                                  const PCPlace* place,
                                  const char *coverOrProfilePhotoId = nullptr,
                                  Eina_List *mediaList = nullptr,
                                  Eina_List *mediaListToDelete = nullptr,
                                  Post *post = nullptr);
    ~PostUpdateOperation() override;

private:
    bool UpdatePostMsgAndPhotoCaptions();
    void DeleteNextPhoto();

    bool mIsPostReloadRequired;

protected:
    bool CurrentStepExecute() override;
    void OnOperationComplete() override;

    static void on_post_with_photos_update_request_completed(void *object, char *response, int code);
    static void on_photo_delete_request_completed(void *object, char *response, int code);

    bundle* mParamsBundle;
    GraphRequest *mRequest;
    char *mCoverOrProfilePhotoId;
    Eina_List *mMediaList;
    Eina_List *mMediaListToDelete;
    std::string mPostId;
    Post *mPost;
};

#endif
