#ifndef OPERATION_H_
#define OPERATION_H_

#include "AppEvents.h"
#include "BaseObject.h"
#include "Error.h"
#include "Friend.h"
#include "PCPlace.h"

#include <bundle.h>
#include <curl/curl.h>
#include <Ecore.h>

#include <list>
#include <set>
#include <string>

class Operation: public BaseObject, public Subscriber {
public:

    enum OperationType {
        OT_None,
        OT_Post_Create,
        OT_Share_Post_Create,
        OT_Post_Update,
        OT_Album_Create,
        OT_Photo_Post_Create,
        OT_Album_Add_New_Photos,
        OT_Video_Post_Create,
        OT_Comment_Create,
        OT_Delete_Post,
        OT_Reload_Post,
        OT_Add_Comment,
        OT_Edit_CommentReply,
        OT_Delete_Comment,
        OT_Add_Reply,
        OT_Delete_Reply,
        OT_Set_Privacy_Option_As_Default,
        OT_Set_Custom_Privacy_Option_As_Default,
        OT_Share_WEB_Link,
        OT_Upload_Profile_Photo,
        OT_Unfriend,
        OT_Block,
        OT_ConfirmFriend,
        OT_NotConfirmFriend,
        OT_DeletePhotoTag,
        OT_CreatePhotoTag,
        OT_Unfollow,
        OT_Follow,
        OT_Like,
        OT_Unlike,
        OT_GetLikesSummary,
        OT_Reload_Comment,
        OT_Get_App_Config
    };

    enum OperationDestination {
        OD_None,
        OD_Home,
        OD_Event,
        OD_Group,
        OD_Friend_Timeline,
        OD_Page
    };

    enum OperationState {
        OS_NotStarted,
        OS_InProgress,
        OS_Paused,
        OS_Completed,
        OS_Canceled
    };

    enum OperationResult {
        OR_Unknown,
        OR_Success,
        OR_Error
    };

    Operation(const std::list<class Friend>& taggedFriends, const std::string& privacyType, const PCPlace* place = nullptr);
    virtual ~Operation();

    /**
     * @brief Returns operation type
     * @return - OperationType
     */
    Operation::OperationType GetType();

    /**
     * @brief Returns operation destination
     * @return - OperationDestination
     */
    Operation::OperationDestination GetDestination();

    /**
     * @brief Returns current operation state
     * @return - OperationState
     */
    Operation::OperationState GetState();

    /**
     * @brief Returns current operation state
     * @return - OperationResult
     */
    Operation::OperationResult GetOperationResult();

    /**
     * @brief Subscribe on operation events
     * @param 'subscriber' - pointer on Subscriber object
     */
    void Subscribe(const Subscriber *subscriber);

    /**
     * @brief Unsubscribe from operation events
     * @param 'subscriber' - pointer on Subscriber object
     */
    void UnSubscribe(const Subscriber *subscriber);

    /**
     * @brief Start execution
     * @return - true if operation is started otherwise false
     */
    virtual bool Start();

    virtual bool ReStart();
    /**
     * @brief Pause execution of operation
     * @return - true if operation is suspended otherwise false
     */
    virtual bool Suspend();

    /**
     * @brief Resume execution of operation
     * @return - true if operation is resumed otherwise false
     */
    virtual bool Resume();

    /**
     * @brief Cancel execution of operation
     * @return - true if operation is canceled otherwise false
     */
    virtual bool Cancel();

    /**
     * @brief Indicate that operation was executed
     * @return - true if operation is completed
     */
    virtual bool isCompleted();

    /**
      * @brief Indicate that operation was canceled
      * @return - true if operation was canceled
      */

    virtual bool isCanceled();

    /**
     * @brief Start current step of operation execution
     */
    virtual bool DoStep();

    /**
     * @brief Start next step of operation execution
     */
    virtual bool DoNextStep();

    /**
     * @brief This method is called when current step is completed
     */
    virtual void CurrentStepComplete();

    /**
     * @brief This method stores all data in bundle
     * @return - true if operation is saved otherwise false
     */
    virtual bool Save(bundle *param);

    /**
     * @brief This method restores all data from bundle
     * @return - true if operation is loaded otherwise false
     */
    virtual bool Load(bundle *param);

    /**
     * @brief This method indicates whether or not upload error notification must be shown.
     * @return - true if upload error notification should be shown otherwise false
     */
    virtual bool IsErrorNotificationRequired() const {return true;}

    /**
     * @brief This method returns number of current step
     * @return - number of current step
     */
    int GetCurrentStep();

    /**
     * @brief This method returns number of all steps
     * @return - steps count
     */
    int GetStepCount();

    /**
     * @brief Implemention of Suscriber interface.
     */
    virtual void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = NULL);


    virtual bundle *GetBundle();

    virtual const char * GetDestinationId();

    std::string GetUserId() { return mUserId; }

    virtual void ResetOperation();
    virtual void PauseOperation();

    virtual bool IsFeedOperation() { return false; };

    std::list<class Friend> GetTaggedFriends() { return mTaggedFriends; }

    std::string GetPrivacyType() { return mPrivacyType; }

    const PCPlace& GetPlace() { return mPlace; }

    virtual const char *GetMessage();

    inline int GetErrorCode() { return mErrorCode; }

    virtual void PrepareToBeRemoved() {}

    virtual void PutInErrorState();

    bool IsFailedBecauseOfGoingOffline();
    inline bool IsArtificiallyFailed() {return (mErrorCode == mArtificialErrorCode);}

protected:
    /**
     * @brief Implement this method for realactions
     */
    virtual bool CurrentStepExecute() = 0;

    /**
     * @brief This method is called when operation is complete
     * it could be override for special actions
     */
    virtual void OnOperationComplete();

    /**
     * @brief This method notifies subscribers
     */
    void Notify(AppEventId eventId, void * data = NULL);

    /**
     * @brief It is helper method for launching next step via job
     */
    static void step_complete_cb( void *data );

    /**
     * @brief This method shows error popup
     */
    static void ShowErrorPopup(Error *error);

    std::list<class Friend> mTaggedFriends;
    std::string mPrivacyType;
    PCPlace mPlace;
    OperationType mOpType;
    OperationState mOpState;
    OperationDestination mOpDestination;
    OperationResult mOperationResult;
    int mCurrentStep;
    int mStepCount;
    Eina_List *mSubscribers;
    Ecore_Job *mStepCompleteJob;

    bundle *mBundle;

    CURLcode mErrorCode;
    bool mStepError;
    std::string mUserId;
    bool mRequestError;
    static const std::set<CURLcode> mOfflineErrorCodes;
    static const CURLcode mArtificialErrorCode;
};

#endif /* OPERATION_H_ */
