#ifndef UPLOADPHOTOOPERATION_H_
#define UPLOADPHOTOOPERATION_H_

#include "SimplePostOperation.h"

class ImageFile;


class UploadPhotoOperation : public SimplePostOperation {
private:
    class ImageDownloader : public ImageFileDownloader
    {
    public:
        virtual ~ImageDownloader() {}
        ImageDownloader(UploadPhotoOperation &operation, ImageFile &image);

    private:
        //from ImageFileObserver
        void ImageDownloaded(bool result) override;

    private:
        UploadPhotoOperation &mOperation;
    };

public:
    enum PhotoType {
        EUserPhoto,
        ECoverPhoto
    };

    UploadPhotoOperation(PhotoType type, ImageFile &image, bool showNotification = false);
    ~UploadPhotoOperation();

    void ImageReady();

private:
    bool DoCurrentStepExecute() override;
    bool ParseUploadPhotoResponse(char *response);

    static void on_upload_photo_completed(void* object, char* response, int code);

private:
    PhotoType mType;
    ImageFile &mImageFile;
    ImageDownloader *mImageDownloader;
    bool mShowNotification;
};

#endif /* UPLOADPHOTOOPERATION_H_ */
