#ifndef SIMPLECOMMENTOPERATION_H_
#define SIMPLECOMMENTOPERATION_H_

#include "bundle.h"
#include "Comment.h"
#include "Operation.h"
#include "GraphRequest.h"

class SimpleCommentOperation: public Operation {
    void Init();

public:
    SimpleCommentOperation(OperationType opType, const char* parentObjectId, bool isPost, bundle* paramsBundle, const char* commentId);
    SimpleCommentOperation(OperationType opType, const char* parentObjectId, bool isPost, bundle* paramsBundle, const char* photoFileName, const char * photoFileThumbnailPath );
    virtual ~SimpleCommentOperation();

    bundle* GetBundle() override;

    const char* GetPhotoFileName();
    const char* GetPhotoFileThumbnailPath();
    const char* GetParentId();
    const char* GetCommentId();
    Comment * GetComment();

protected:
    bool CurrentStepExecute() override;
    void OnOperationComplete() override;
    bool Cancel() override;
    void PutInErrorState() override;
    void ResetOperation() override;

    static void on_comment_add_request_completed(void* object, char* respond, int code);
    static void on_comment_edit_request_completed(void* object, char* respond, int code);
    static void on_comment_reload_request_completed(void* object, char* respond, int code);
    static void on_comment_delete_request_completed(void* object, char* respond, int code);

    /*
     * The id of the Comment or Reply
     */
    char* mCommentId;

    /*
     * The id of the parent's object. Post is parent for Comment; Comment is parent for Reply.
     */
    char* mParentId;
    char* mPhotoFileName;
    char* mPhotoFileThumbnailPath;
    bundle* mParamsBundle;
    GraphRequest * mRequest;

    /*
     * Keeps Comment/Reply object at Add_Comment/Reply or Reload_Comment/Reply operations.
     */
    Comment* mComment;

private:
    bool IsReloadError();
    void OnNetworkErrorAfterAdded();

    bool mIsReloadInProgress;
    bool mParentObjectIsPost = false;
};

#endif /* SIMPLECOMMENTOPERATION_H_ */
