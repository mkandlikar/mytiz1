#ifndef SIMPLEPOSTOPERATION_H_
#define SIMPLEPOSTOPERATION_H_

#include "GraphRequest.h"
#include "Operation.h"

#include <bundle.h>

class ActionAndData;
class FbRespondGetPostDetails;

class SimplePostOperation: public Operation {
public:
    SimplePostOperation(OperationType opType,
                        const std::string& userId,
                        bundle* paramsBundle,
                        const std::list<Friend>& taggedFriends,
                        const std::string& privacyType,
                        const PCPlace* place,
                        OperationDestination opDestination = OD_None,
                        Post * sharedPost = nullptr,
                        const char *coverOrProfilePhotoId = nullptr);

    ~SimplePostOperation() override;

    bundle* GetBundle() override;
    const char * GetDestinationId() override;
    void ResetOperation() override;
    bool IsFeedOperation() override;
    void PutInErrorState() override;
    ActionAndData * GetSharedActionAndPost();

protected:
    virtual bool DoCurrentStepExecute() {return false;}
    bool CurrentStepExecute() override;
    void OnOperationComplete() override;
    bool Start() override;
    void Update(AppEventId eventId, void * data) override;
    bool Cancel() override;

    static void on_post_feed_request_completed(void* object, char* respond, int code);
    static void on_post_edit_request_completed(void* object, char* respond, int code);
    static void on_post_delete_request_completed(void* object, char* respond, int code);
    static void on_post_reload_request_completed(void* object, char* respond, int code);
    static void on_set_default_privacy_completed(void* object, char* response, int code);

    bool mIsSubscribedToRequestProgress;
    bundle* mParamsBundle;
    Sptr<GraphRequest> mRequest;
    bool mIsShareWebLink;
    double mPrevProgress;
    ActionAndData * mSharedActionAndPost;
    FbRespondGetPostDetails *mPostDetailsResponse;
    char *mCoverOrProfilePhotoId;
};


#endif /* SIMPLEPOSTOPERATION_H_ */
