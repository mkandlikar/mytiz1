#ifndef NETWORK_GRAPH_API_OPERATIONS_GKCONFIGOPERATION_H_
#define NETWORK_GRAPH_API_OPERATIONS_GKCONFIGOPERATION_H_

#include "Operation.h"

class GkConfigOperation : public Operation {
public:
    GkConfigOperation(OperationType opType);
    ~GkConfigOperation();

private:
    static void on_get_app_config_completed(void* object, char* response, int code);

protected:
    bool CurrentStepExecute() override;

    OperationType mOpType;
    GraphRequest *mRequest;
};

#endif /* NETWORK_GRAPH_API_OPERATIONS_GKCONFIGOPERATION_H_ */
