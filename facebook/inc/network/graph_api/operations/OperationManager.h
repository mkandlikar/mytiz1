#ifndef OPERATIONMANAGER_H_
#define OPERATIONMANAGER_H_

#include "AppEvents.h"
#include "Operation.h"
#include "Timer.h"
#include <list>

class OperationManager: Subscriber {
    friend class SimplePostOperation;
    friend class PhotoPostOperation;
public:
    OperationManager();
    virtual ~OperationManager();

    static OperationManager* GetInstance();

    /**
     * @brief Subscribe on operation manager events
     * @param 'subscriber' - pointer on Subscriber object
     */
    void Subscribe(const Subscriber *subscriber);

    /**
     * @brief Unsubscribe from operation manager events
     * @param 'subscriber' - pointer on Subscriber object
     */
    void UnSubscribe(const Subscriber *subscriber);

    /**
     * @brief Implemention of Suscriber interface.
     */
    virtual void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = NULL);

    /**
     * @brief  This method adds operation to manager and start execution of operation queue
     * @param  'operation' - Operation object.
     */
    void AddOperation(Sptr<Operation> operation);

    /**
     * @brief  This method removes operation from operation manager
     * @param  'operation' - Operation object.
     */
    void RemoveOperation(Sptr<Operation> operation);

    /**
     * @brief  This method adds operation to manager and start execution but without event
     * to update appropriate provider, because of operation is already there
     * @param  'operation' - Operation object.
     */
    void RestartOperationAfterError(Sptr<Operation> operation);

    /**
     * @brief  This method returns executed operation
     * @return - Operation object.
     */
    Sptr<Operation> GetCurrentOperation();

    /**
     * @brief Start execution
     * @return - true if operation is started otherwise false
     */
    bool Start();
    bool Restart();

    /**
     * @brief Pause execution
     * @return - true if operation is suspended otherwise false
     */
    bool Suspend();

    /**
     * @brief Resume execution
     * @return - true if operation is resumed otherwise false
     */
    bool Resume();

    /**
     * @brief Start execution of next operation
     */
    bool DoNextOperation();

    /**
     * @brief Cancel execution of all operations
     */
    void CancelAll();

    /**
     * @brief Delete all operations
     */
    void DeleteAll();

    /**
     * @brief This method stores all data
     * @return - true if operation manager is saved otherwise false
     */
    bool Save();

    /**
     * @brief This method load data
     * @return - true if operation manager is loaded otherwise false
     */
    bool Load();

    Operation *GetActiveOperationById(const char *id);

private:
    void OnTimer();
    /**
     * @brief This method notifies subscribers
     */
    void Notify(AppEventId eventId, void * data = NULL);
    void ShowUploadErrorNotification() const;

    void PutAllOperationsInErrorState();
    void ResetAllOperations();

    Operation *GetOperationByUserId(const char *id);

    Eina_List *mSubscribers;
    std::list<Sptr<Operation>> mOperations;

    Sptr<Operation> mCurrentOperation;
    bool mOperationIsWaitingForConnection;
    Timer mTimer;
};


#endif /* OPERATIONMANAGER_H_ */
