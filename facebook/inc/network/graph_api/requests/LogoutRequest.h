#ifndef LOGOUTREQUEST_H_
#define LOGOUTREQUEST_H_

#include "PushService.h"
#include "RequestUserData.h"

#include <account.h>
#include <Ecore.h>

struct user_data_struct;

class LogoutRequest {
public:
LogoutRequest(void (*callback)(void *, char*, int), void * callback_object);
virtual ~LogoutRequest();

/**
 * @brief This method run a request to facebook in async thread
 * @return pointer to started async thread
 */
Ecore_Thread * ExecuteAsync();

//internal callbacks for multithreading
/**
 * @brief  - This method runs in async thread
 * @param data[in] - user data passed to thread
 * @param thread[in] - thread
 */
static void thread_run_cb(void *data, Ecore_Thread *thread);

/**
 * @brief - This callback is called when async thread is finished
 * @param data[in] -user data
 * @param thread[in] - thread
 */
static void thread_end_cb(void *data, Ecore_Thread *thread);

/**
 * @brief - This callback is called when async thread should be cancelled
 * @param data[in] - user data
 * @param thread[in] - thread
 */
static void thread_cancel_cb(void *data, Ecore_Thread *thread);

static void RedirectToLogin(void* ThreadData);
static void PushServiceCb(push_service_result_e Result, const char *Message, void *Data);
static void ClearAllUserData();

void DoLogout();
void Cancel();

private:
static bool account_remove_cb(account_h account, void* data);

void (*mCallback)(void * callback_object, char* respond, int code);
void *mCallbackObject;

Ecore_Thread* mThread;
RequestUserData* mThreadData;

};

#endif /* LOGOUTREQUEST_H_ */
