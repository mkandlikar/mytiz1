#ifndef GRAPHREQUEST_H_
#define GRAPHREQUEST_H_

#include "AppEvents.h"
#include "BaseObject.h"
#include "Common.h"

#include <bundle.h>
#include <curl/curl.h>
#include <Ecore.h>
#include <functional>
#include <stdlib.h>

class AbstractDataProvider;
class RequestUserData;

/**
 * This class intended for sending async requests to facebook
 */
class GraphRequest: public BaseObject
{
    friend class FacebookSession;
    friend class RequestUserData;

public:
    typedef enum HttpMethod_
    {
        GET,
        POST,
        DELETE,
        POST_MULTIPART,
    } HttpMethod;

    typedef enum ServerType_ {
        GRAPH,
        GRAPH_VIDEO,
        REST,
    } ServerType;

    enum GraphRequestStatus {
        GRS_NOT_STARTED,
        GRS_IN_PROGRESS,
        GRS_COMPLETED,
        GRS_CANCELED,
    };
    /**
     * @brief Construct an instance of GraphRequest object
     * @param graphPath[in] - This is facebook graph request
     * @param parameters[in] - Parameters for facebook graph request
     * @param httpMethod[in] - The type of Http request
     * @param callback[in] - This is callback to notify client when request is completed
     * @param callback_object[in] - data passed to callback
     */
    GraphRequest(const char * graphPath, bundle * parameters, HttpMethod httpMethod,
            void (*callback) (void *, char*, int), void* callback_object,
            const char * graphVersion = NULL, ServerType serverType = GRAPH,
            bundle * bodyParamsBundle = NULL,
            const char * multipartFileUploadName = NULL, const char * etags_header = NULL);

    GraphRequest(const std::string& graphPath,
                 bundle* parameters,
                 HttpMethod httpMethod,
                 std::function<void(const std::string&, CURLcode)> callback,
                 const std::string& graphVersion = "",
                 ServerType serverType = GRAPH,
                 bundle* bodyParamsBundle = nullptr,
                 const std::string& multipartFileUploadName = "",
                 const std::string& eTagsHeader = "");

    /**
     * @brief This constructor is used for Data Provider usage.
     */
    GraphRequest(const char * graphPath, GraphRequest::HttpMethod httpMethod, AbstractDataProvider *provider, bundle *parameters = NULL, const char *eTagsHeader = NULL );



    /**
     * @brief This method run a request to facebook in async thread
     */
    void ExecuteAsync();
    /**
     * @brief This method is used for Data Provider. Purpose of this method is similar with @method ExecuteAsync()
     */
    void ExecuteAsyncEx( void (*handleNofify_cb) (void *data, Ecore_Thread *thread, void *notif ) );

    /**
       * @brief This method is used for setting the header required to fetch ETags
       */

    void ExecuteAsyncExHeader( void (*handleNofify_cb) (void *data, Ecore_Thread *thread, void *notif ) );

    /**
     * @brief Invokes when request is completed
     * @param[in] response - body of HTTP response
     * @param[in] curlCode - error code from Curl library
     */
    void OnComplete(char* response, CURLcode curlCode, Ecore_Thread *thread);

    /**
        * @brief Invokes when request is completed
        * @param[in] response - body of HTTP response
        * @param[in] response - body of HTTP header Etags
        * @param[in] curlCode - error code from Curl library
        */
    void OnCompletewithHeader(char* response,char *header, int curlCode, Ecore_Thread *thread);
    /**
     * @brief Returns request status
     * @return object status
     */
    GraphRequestStatus GetStatus() const;

    /**
     * @brief Remove all callbacks linked with the object
     */
    void RemoveCallbacks();

    /**
     * @brief Returns flag that indicate that object invokes callback functions
     */
    bool GetIsCallbackInProgress() const;

    /**
     * @brief Returns request graph path
     */
    const char* GetGraphPath() const;

    /**
     * @brief GraphRequest's hash for fast seek/comparison
     */
    void SetHash(int hash) { mHash = hash; }
    int GetHash() { return mHash; }

    /**
     * @brief Subscribe on operation events
     * @param 'subscriber' - pointer on Subscriber object
     */
    void Subscribe(const Subscriber *subscriber);

    /**
     * @brief Unsubscribe from operation events
     * @param 'subscriber' - pointer on Subscriber object
     */
    void UnSubscribe(const Subscriber *subscriber);

    void UnSubscribeAll();

    /**
     * @brief This method notifies subscribers
     */
    void Notify(AppEventId eventId, void * data = NULL);

    /**
     *
     * This function is called in a non UI thread
     */
    void OnProgress(long int dltotal, long int dlnow, long int ultotal, long int ulnow);

    double GetProgress();
    bool IsProgressFunctionUsed();

    virtual unsigned int AddRef();
    virtual unsigned int Release();

    static char* PrepareParamsFromBundle(bundle * params);
    static struct MemoryStruct* PrepareMemoryStructFromBundle(bundle * params);

    /**
     * @brief Cancel request execution
     */
    void Cancel();
protected:

    void SetStatus(GraphRequestStatus status);

    //internal callbacks for multithreading
    /**
     * @brief  - This method runs in async thread
     * @param data[in] - user data passed to thread
     * @param thread[in] - thread
     */
    static void thread_run_cb(void *data, Ecore_Thread *thread);
    static void thread_run_ex_cb(void *data, Ecore_Thread *thread);

    /**
     * @brief - This callback is called when async thread is finished
     * @param data[in] -user data
     * @param thread[in] - thread
     */
    static void thread_end_cb(void *data, Ecore_Thread *thread);
    static void thread_end_ex_cb(void *data, Ecore_Thread *thread);
    /**
     * @brief This method is used for Data Provider. Purpose of this method is similar with @method thread_end_cb()
     */
    static void thread_end_cbEx(void *data, Ecore_Thread *thread);

    /**
     * @brief - This callback is called when async thread should be cancelled
     * @param data[in] - user data
     * @param thread[in] - thread
     */
    static void thread_cancel_cb(void *data, Ecore_Thread *thread);

    /**
     * @brief This is callback for bundle_foreach function. It append bundle item to parameters list
     * @param key - bundle item key
     * @param type - bundle item typr
     * @param kv - bundle item value
     * @param user_data - user data passed to callback
     */
    static void iterate_bundle_foreach(const char *key, const int type, const bundle_keyval_t *kv, void *user_data);

    static void handleThreadProgress_cb( void *data, Ecore_Thread *thread, void *ThreadMsgObj );


private:
    virtual ~GraphRequest();

    const char * mGraphPath;
    int mHash{0};
    bundle  *mParamsBundle;
    bundle  *mBodyParamsBundle;
    HttpMethod mHttpMethod;
    char *mEtagsHeader;
    RequestUserData_CB mCallback;
    void *mCallbackObject;
    std::function<void(const std::string&, CURLcode)> mCallbackFunctionObject;
    Ecore_Thread* mThread{nullptr};
    RequestUserData* mThreadData{nullptr};
    bool mIsThreadCompleted{false};
    const char * mGraphVersion;
    ServerType mServerType;
    const char * mMultipartFileUploadName;
    AbstractDataProvider *m_Provider;
    GraphRequestStatus mStatus;
    bool mIsCallbackInProgress;

    long int mDltotal{0L};
    long int mDlnow{0L};
    long int mUltotal{0L};
    long int mUlnow{0L};

    Eina_List *mSubscribers{nullptr};

    Eina_Lock m_Mutex;
    bool mIsProgressFunctionUsed{false};
};

#endif /* GRAPHREQUEST_H_ */
