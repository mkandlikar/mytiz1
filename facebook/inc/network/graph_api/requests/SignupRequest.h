#ifndef SIGNUPREQUEST_H_
#define SIGNUPREQUEST_H_

#include <string>

class SignupRequest {
    private:
        std::string mEmailAddress;
        std::string mMobileNumber;
        std::string mFirstName;
        std::string mLastName;
        std::string mPassword;
        std::string mBirthDay;
        std::string mConfirmCode;
        bool mIsGenderFemale;
        bool mIsConfirmPending;

    private:
        SignupRequest();
        SignupRequest(SignupRequest const&);
        SignupRequest& operator=(SignupRequest const&);
        virtual ~SignupRequest();

    public:
        enum EDataType {
            EMAILADDRESS,
            MOBILENUMBER,
            FIRSTNAME,
            LASTNAME,
            PASSWORD,
            BIRTHDAY,
            ISGENDERFEMALE,
            CONFIRMCODE
        };

        enum ERequestType {
            SIGNUP,
            CONFIRM,
            SENDCODE
        };

        static SignupRequest& Singleton() {
            //TBD
            static SignupRequest instance;
            return instance;
        }

        void SetData(EDataType DataType, std::string Data);
        std::string GetData(EDataType DataType);
        bool IsConfirmPending();
        Ecore_Thread *InitAsync(ERequestType RequestType);
        void ProcessResponse(ERequestType RequestType, const char* Response);
        static void SignUpCallback(void *Data, char *Response, int ReponseCode);
        static void ConfirmCallback(void *Data, char *Response, int ReponseCode);
        static void SendCodeCallback(void* Data, char* Response, int ReponseCode);

        static void ThreadRunCallback(void *data, Ecore_Thread *thread);
        static void ThreadEndCallback(void *data, Ecore_Thread *thread);
        static void ThreadCancelCallback(void *data, Ecore_Thread *thread);
        static void CleanupThreadData(void *data);
};



#endif /* SIGNUPREQUEST_H_ */
