#ifndef LOGINREQUEST_H_
#define LOGINREQUEST_H_

#include "Ecore.h"
#include "RequestUserData.h"

class LoginRequest {

public:
    LoginRequest(void (*callback)(void *, char*, int), void * callback_object,
            const char *username, const char *password, const char *firstFactor = nullptr, const char *secondFactor = nullptr);
    virtual ~LoginRequest();

    /**
     * @brief This method run a request to facebook in async thread
     * @return pointer to started async thread
     */
    Ecore_Thread * ExecuteAsync();

    /**
     * @brief This method runs a request to server in async thread to resend Login Code SMS for F2Authentication
     * @return pointer to started async thread
     */
    Ecore_Thread * ResendLoginCodeExecuteAsync();

    //internal callbacks for multithreading
    /**
     * @brief  - This method runs in async thread
     * @param data[in] - user data passed to thread
     * @param thread[in] - thread
     */
    static void thread_run_cb(void *data, Ecore_Thread *thread);

    /**
     * @brief - This callback is called when async thread is finished
     * @param data[in] -user data
     * @param thread[in] - thread
     */
    static void thread_end_cb(void *data, Ecore_Thread *thread);

    /**
     * @brief - This callback is called when async thread should be cancelled
     * @param data[in] - user data
     * @param thread[in] - thread
     */
    static void thread_cancel_cb(void *data, Ecore_Thread *thread);

    void DoLogin();
    void Cancel();

private:
    char *mUsername;
    char *mPassword;
    char *mFirstFactorCode;
    char *mSecondFactorCode;

    void (*mCallback)(void * callback_object, char* respond, int code);
    void *mCallbackObject;

    Ecore_Thread *mThread;
    RequestUserData *mThreadData;
};

#endif /* LOGINREQUEST_H_ */
