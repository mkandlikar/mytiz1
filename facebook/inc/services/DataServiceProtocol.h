#ifndef DATASERVICEPROTOCOL_H_
#define DATASERVICEPROTOCOL_H_

#include "DataServiceProtocolCommon.h"

/**
 * @brief This is an interface for protocol clients.
 * @details This interface should be implemented by classes that intended to be protocol's clients
 * for image downloading .
 */
class IDataServiceProtocolClient : public IProtocolClient {
public:
    virtual void HandleDownloadImageResponse(ErrorCode error, MessageId requestID, const char* url, const char* fileName) = 0;
    virtual ~IDataServiceProtocolClient() {};
};

/**
 * @brief This is an concrete protocol for communication with the Data Service.
 * @details This protocol can be constructed and used by clients.
 * This protocol isn't fully completed yet and it will be extended later.
 */
class DataServiceProtocol : public ProtocolBase {
public:
    DataServiceProtocol();
    virtual ~DataServiceProtocol();

    bool DownloadImage(IProtocolClient* client, const char* url, MessageId &requestID);
    void StopDataService();
    void NotifyDataService();
    void ClearUserData();
    void ClearOldUserData();
    void SendAppBGMessage();
    void setLoginState(bool state);
    inline bool getLoginState() {return mLoginState;}
    void clearDownloadRequest();
protected:
    virtual int SendMessage(bundle* abundle) override;

private:
    class DownloadImageRequest : public ProtocolBase::RequestBase {
    friend class DataServiceProtocol;
    public:
        DownloadImageRequest(MessageType requestType, MessageId requestId, IProtocolClient *client, const char* url);
        ~DownloadImageRequest();
    private:
        const char* mUrl;
    };

private:
    virtual void DoHandleReceivedData(MessageType messageType, bundle* data) override;
    virtual void CancelRequest(MessageId requestId, MessageType requestType) override;

private:
    bool mLoginState;
};


#endif /* DATASERVICEPROTOCOL_H_ */
