#ifndef PUSH_SERVICE_REGISTRATION_H
#define PUSH_SERVICE_REGISTRATION_H

#include <Ecore.h>
#include <push-service.h>

class ActionAndData;
class FacebookSession;

class PushService
{
public:
    PushService();
    ~PushService() { };
    void connect();
    static void disconnect();

    static push_service_connection_h pushConn;

private:
    static Ecore_Thread * ExecuteAsync(const char *);
    static void send_reg_id_if_necessary(const char *reg_id);
    static void thread_start_cb(void *data, Ecore_Thread *thread);
    static void thread_end_cb(void *data, Ecore_Thread *thread);
    static void thread_cancel_cb(void *data, Ecore_Thread *thread);
    static void _state_cb(push_service_state_e state, const char *err, void *user_data);
    static void _on_state_registered(void *user_data);
    static void _noti_cb(push_service_notification_h noti, void *user_data);
    static void response_cb(void* object, char* respond, int code);

    enum Toggle_Modes {
        Off, On
    };

    static Toggle_Modes toggle;
};
#endif
