#ifndef LOCATION_SERVICE_H
#define LOCATION_SERVICE_H

#include <locations.h>
#include "ConfirmationDialog.h"

class LocationService {
    static double mLatitude;
    static double mLongitude;
    static time_t mTimestamp;
    location_manager_h manager;
    int mManagerState;

    void start();
    void get_location();
    bool HidePopUp();
    void location_manager_init();
    void show_popup(int result);
    void setLatitudeLongitude(double latitude, double longitude, time_t timestamp, int result);
    void (*mCallback)(void * callback_object, double, double, int);
    void *mCallbackObject;


    static void state_changed_cb(location_service_state_e state, void *user_data);
    static void setting_changed_cb(location_method_e method, bool enable, void *user_data);
    static void confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event);

public:
    LocationService(void (*callback)(void *, double, double, int), void* callback_object);
    ~LocationService();

    void stop();

    static double getLat(){ return mLatitude; }
    static double getLong(){ return mLongitude; }
    static bool is_latest_location();
    static const char* get_distance_in_str(double dest_lat, double dest_long);
};

#endif
