#ifndef __fbapp_H__
#define __fbapp_H__

#include <app.h>
#include <Elementary.h>
#include <system_settings.h>
#include <efl_extension.h>
#include <dlog.h>

#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "Facebook"

#define EDJ_FILE "edje/fbapp.edj"
#define GRP_MAIN "main"

#endif /* __fbapp_H__ */
