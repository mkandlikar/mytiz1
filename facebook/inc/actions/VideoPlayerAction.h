#ifndef VIDEOPLAYERACTION_H_
#define VIDEOPLAYERACTION_H_

#include "ActionBase.h"

class VideoPlayerAction: public ActionBase {
public:
    VideoPlayerAction(ScreenBase *screen);
    virtual ~VideoPlayerAction() { }

    void TextClicked(ActionAndData *userData) override;
    void CommentBtnClicked(ActionAndData *user_data) override;
    void ShareWithPostClicked(GraphObject *graphObject) override;
};



#endif /* VIDEOPLAYERACTION_H_ */
