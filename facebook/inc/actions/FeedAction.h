#ifndef FEEDACTION_H_
#define FEEDACTION_H_

#include "ActionBase.h"
#include "GraphObject.h"
#include "Post.h"
#include "TrackItemsProxyAdapter.h"

class FeedAction: public ActionBase {
public:
    FeedAction(ScreenBase *screen);
    virtual ~FeedAction();

    Post* getParentPost(Post *basePost);
    Post* getPostById(const char* id);

    virtual void CancelPostingOperation(ActionAndData *actionAndData);
    virtual void RetryPostSend(ActionAndData *actionAndData);
    virtual void OperationMoreInfoBtnClicked(ActionAndData *actionAndData);
    virtual void AvatarClicked(GraphObject *graphObject);

    virtual void NewUserFriendsClicked(GraphObject *graphObject);

    virtual void LinkClicked(GraphObject *graphObject);
    virtual void PageClicked(GraphObject *graphObject);

    virtual void ShareNowClicked(GraphObject *graphObject);

    virtual void ShareWithPostClicked(GraphObject *graphObject);

    virtual void StatsClicked(GraphObject *graphObject);
    virtual void PhotoClicked(GraphObject *graphObject);
    virtual void AddFriendClicked(GraphObject *graphObject);
    static void on_add_friend_completed(void* object, char* respond, int code);

    virtual void CtxEditPrivacyOptionClicked(ActionAndData *actionData);
    virtual void PostVideoClicked(ActionAndData * user_data);
    virtual void CtxApprovedDeletePostClicked(ActionAndData *actionData);
    virtual void EventInfoClicked(GraphObject *graphObject);

    virtual void UnfollowUser(ActionAndData *actionData);
};

#endif /* FEEDACTION_H_ */
