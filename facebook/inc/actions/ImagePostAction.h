#ifndef IMAGEPOSTACTION_H_
#define IMAGEPOSTACTION_H_

#include "ActionBase.h"

class ImagePostAction: public ActionBase {
public:
    ImagePostAction(ScreenBase *screen);
    virtual ~ImagePostAction() { }

    virtual void ShareNowClicked(GraphObject *graphObject);
    virtual void ShareWithPostClicked(GraphObject *graphObject);
};

#endif /* IMAGEPOSTACTION_H_ */
