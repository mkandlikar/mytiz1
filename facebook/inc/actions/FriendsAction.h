#ifndef FRIENDSACTION_H_
#define FRIENDSACTION_H_

#include "ActionBase.h"

class FriendsAction: public ActionBase {
public:

    FriendsAction(ScreenBase *screen);
    virtual ~FriendsAction();

    void PymkBtnClicked(ActionAndData *actionData) override;

    void FriendRequestCarouselBtnClicked(ActionAndData *actionData) override;
    static void on_fr_friend_request_completed(void* object, char* respond, int code);

    void AddFriendBtnClicked(ActionAndData *actionData) override;
    static void on_add_friend_completed(void* object, char* respond, int code);

    void CancelFriendBtnClicked(ActionAndData *actionData) override;
    static void on_cancel_friend_completed(void* object, char* respond, int code);

    void ConfirmOrDeleteFriendRequest(ActionAndData *actionData, bool isConfirmed) override;

    void UnfriendFriendBtnClicked(ActionAndData *actionData) override;
    void BlockFriendBtnClicked(ActionAndData *actionData) override;

    static void ShowErrorDialog(int error_code);
private:
    /**
     * @brief Object for getting ui constants.
     */
    GraphRequest *mAddFriendRequest;
    GraphRequest *mCancelFriendRequest;

    static void confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event);
    void HideFriendsListItem(GraphObject *data, ScreenBase *screen);
    void DoUnfriend(ActionAndData *actionData) override;
    void DoBlock(ActionAndData *actionData) override;
};

#endif /* FRIENDSACTION_H_ */
