#ifndef ACTIONBASE_H_
#define ACTIONBASE_H_

#include <Evas.h>
#include <Elementary.h>
#include <notification.h>

#include "AppEvents.h"
#include "ConfirmationDialog.h"
#include "DataServiceProtocol.h"
#include "GraphObject.h"

class ScreenBase;
class ActionAndData;

class ActionBase {
public:
    ActionBase();
    ActionBase(ScreenBase *screen);
    virtual ~ActionBase();

    static void on_operation_more_info_btn_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);

    static void on_avatar_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void on_like_btn_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);

    static void on_text_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_comment_btn_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);

    static void on_share_now_clicked(void *user_data, Evas_Object *obj, void *event_info);
    static void on_share_with_post_clicked(void *user_data, Evas_Object *obj, void *event_info);

    static void on_stats_footer_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);

    static void on_link_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void on_page_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);

    static void on_new_user_friends_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);

    static void on_photo_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void on_video_post_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void on_photo_box_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);

    static void on_likes_count_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);

    static void on_event_info_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);

    static void on_anchor_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);

    static void on_ctxmenu_notification_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void on_ctxmenu_hide_this_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void on_ctxmenu_unfollow_user_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void on_ctxmenu_edit_post_option_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void on_ctxmenu_approved_detete_post_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_ctxmenu_edit_privacy_option_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void on_ctxmenu_approved_delete_cmnt_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);

    void OnCtxMenuApprovedDeleteEventClicked(void *user_data);

    static void on_unfollow_user_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);

    static void on_object_type_details_recv_cb(void * data, char* response, int status);
    static void on_map_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);

    static void WebViewLaunch(void * data, char* response, int status);

    static void run_messanger_cb(void *data, Evas_Object *obj, const char *emission, const char *source);

    /* Friend Requests */
    static void on_pymk_btn_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    virtual void PymkBtnClicked(ActionAndData *actionData);

    static void on_fr_carousel_btn_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    virtual void FriendRequestCarouselBtnClicked(ActionAndData *actionData);

    static void on_add_friend_btn_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    virtual void AddFriendBtnClicked(ActionAndData *actionData);
    static void on_cancel_friend_btn_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    virtual void CancelFriendBtnClicked(ActionAndData *actionData);

    static void on_confirm_friend_rq_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void on_delete_friend_rq_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);
    virtual void ConfirmOrDeleteFriendRequest(ActionAndData *actionData, bool isConfirmed);

    static void on_unfriend_friend_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);
    virtual void UnfriendFriendBtnClicked(ActionAndData *actionData);
    virtual void DoUnfriend(ActionAndData *actionData);

    static void on_block_friend_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);
    virtual void BlockFriendBtnClicked(ActionAndData *actionData);
    virtual void DoBlock(ActionAndData *actionData);

    /* Event actions */
    static void on_event_going_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void elm_on_event_going_btn_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    virtual void EventGoingBtnClicked(ActionAndData *actionData);

    static void on_event_maybe_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void elm_on_event_maybe_btn_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    virtual void EventMaybeBtnClicked(ActionAndData *actionData);

    static void on_event_not_going_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void elm_on_event_not_going_btn_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    virtual void EventNotGoingBtnClicked(ActionAndData *actionData);

    virtual void OperationMoreInfoBtnClicked(ActionAndData *actionAndData);
    virtual void CancelPostingOperation(ActionAndData *actionAndData);

    static void retry_post_send_cb(void *user_data, Evas_Object *obj, void *event_info);
    virtual void RetryPostSend(ActionAndData *actionAndData);

    virtual void AvatarClicked(GraphObject *graphObject);
    virtual void PostClicked(GraphObject *graphObject);

    virtual void LikeBtnClicked(ActionAndData *user_data);

    virtual void TextClicked(ActionAndData *userData);
    virtual void CommentBtnClicked(ActionAndData *userData);

    virtual void ShareNowClicked(GraphObject *graphObject);
    virtual void ShareWithPostClicked(GraphObject *graphObject);

    virtual void LinkClicked(GraphObject *graphObject);
    virtual void MapClicked(ActionAndData * user_data);
    virtual void PageClicked(GraphObject *graphObject);

    virtual void NewUserFriendsClicked(GraphObject *graphObject);

    virtual void StatsClicked(GraphObject *graphObject);
    virtual void ContextMenuClicked(GraphObject *graphObject);
    virtual void PhotoClicked(GraphObject *graphObject);
    virtual void PostVideoClicked(ActionAndData * user_data);

    virtual void SendCommentClicked(GraphObject *graphObject);
    virtual void LikesCountClicked(GraphObject *graphObject);

    virtual void CommunityNameClicked(GraphObject *graphObject);
    virtual void CommunityLikeClicked(GraphObject *graphObject);

    virtual void LocationMapClicked(GraphObject *graphObject);
    virtual void LocationAddBtnClicked(GraphObject *graphObject);
    virtual void LikePageBtnClicked(GraphObject *graphObject);
    virtual void LocationInfoClicked(GraphObject *graphObject);
    virtual void EventInfoClicked(GraphObject *graphObject);

    virtual void AnchorClicked(ActionAndData *data, const char *anchorName);
    virtual void StatusBtnClicked(GraphObject *graphObject);
    virtual void PhotoAddBtnClicked(GraphObject *graphObject);
    virtual void CheckInBtnClicked(GraphObject *graphObject);

    virtual void CtxMenuNotificationClicked(GraphObject *graphObject);
    virtual void CtxMenuHideThisClicked(GraphObject *graphObject);
    virtual void CtxMenuUnfollowUserClicked(GraphObject *graphObject);
    virtual void CtxEditPostOptionClicked(ActionAndData *actionData);
    virtual void CtxEditPrivacyOptionClicked(ActionAndData *actionData);
    virtual void CtxApprovedDeletePostClicked(ActionAndData *actionData);
    virtual void CtxApprovedDeleteCmntClicked(ActionAndData *actionData);
    virtual void CtxApprovedDeleteEvent(ActionAndData *actionData);

    virtual void BlockUser(ActionAndData *actionData);
    virtual void UnblockUser(ActionAndData *actionData);

    virtual void UnfollowUser(ActionAndData *actionData);

    bool IsNestedProfile(const char* ProfileId);

    typedef enum CNF_DIALOG_TYPE_ {
        CDT_GENERIC,
        CDT_CANCEL_CONFIRM,
        CDT_CANCEL_BLOCK
    } CNF_DIALOG_TYPE;
    CNF_DIALOG_TYPE mCnfDialogType{CDT_GENERIC};

    bool DeleteConfirmationDialog();
    void CreateConfirmationDialog(Evas_Object *parent, const char* caption, const char* description, const char* yesText, const char* noText,
            ConfirmationDialog::ConfirmationDialogCB callBack, void *userData);
    void CreateConfirmationDialog(Evas_Object *parent, const char* caption, const char* description, CNF_DIALOG_TYPE cnfDialogType,
            ConfirmationDialog::ConfirmationDialogCB callBack, void *userData);
    ScreenBase *getScreen();
    static void OpenImageFromPost(ActionAndData *actionAndData, int imageNumber);
protected:
    GraphRequest *LoadObjectDetails(void *data, const char *id, bool isLinkNeeded);

private:
    Eina_List *ParseMultiAnchor(const char*anchor);
    void OpenCommentScreen(ActionAndData *userData);

    ConfirmationDialog * mConfirmationDialog;

protected:
    ScreenBase *mScreen;
    Elm_Entry_Anchor_Info *event_name;
    GraphRequest *mGrpReqObjectType;
    GraphRequest *mLikeRequest;

};


class ActionAndData: public Subscriber {
public:
    enum LayoutType {
        EImage,
        EEvasImage,
        EVideo,
        EText
    };

public:
    ActionAndData(GraphObject *data, ActionBase *action, ActionAndData *parent = NULL, const char * entityId = NULL);
    ActionAndData(const ActionAndData& CopyMe);

    ~ActionAndData();

    static bool IsAlive(ActionAndData *pointer) {return eina_list_search_unsorted_list(mAliveActionAndDates, action_and_data_comparator, pointer);}

    void UpdateLayoutAsync(LayoutType type, Evas_Object *layout, int dataType);
    void UpdateImageLayoutAsync(const char* url, Evas_Object *layout, LayoutType type = EImage, int dataType = -1, int index = -1);
    void DownloadVideo(const char* url);
    void DeleteAsyncRequests();
    void ReSetData(GraphObject *newData);

    /**
     * @brief Implemention of Suscriber interface.
     */
    virtual void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = NULL);

    /**
     * @brief Checks if needed to draw an attachment of post
     */
    bool IsNeedToDrawAttachment(const char * attachmentId);

    bool getIsLinkMetaDataFieldRequired() const { return mIsLinkMetaDataFieldRequired; }
    void setIsLinkMetaDataFieldRequired(bool isLinkMetaDataRequired) { mIsLinkMetaDataFieldRequired = isLinkMetaDataRequired; }

public:
    GraphObject *mData;
    ActionBase *mAction;
    Evas_Object *mParentWidget;

    /**
     * @brief Widget for post which shows information about the number of likes and comments.
     */
    Evas_Object * mLikesCommentsWidget;

    /**
     * @brief Pointer to btn footer layout which contains button(s) Like or Comment and Share or all of them.
     */
    Evas_Object *mPostActionsBtns;

    /**
     * @brief Widget for post which shows information about the date and privacy icon.
     */
    Evas_Object * mInfoBoxWidget;

    /**
     * @brief Widget for post which shows information about the text.
     */
    Evas_Object * mPostText;

    Evas_Object * mActionObj;
    Evas_Object * mChangeableLabel;
    Evas_Object * mEventObjLabel;

    Evas_Object * mEventNameLabel;
    Evas_Object * mEventStartTimeLabel;
    Evas_Object * mEventPlaceLabel;

    Evas_Object * mEventInviteBtnsLayout;

    /*
     * @brief Widget for reply item in CommentScreen
     */
    Evas_Object *mReplyActionObject;

    /**
     * @brief Widget for comment or reply item which we are going to do something with
     */
    Evas_Object * mObjectWidget;

    // TODO: remove this field from the class
    ScreenBase *mFriendsRequestTab;
    bool mIsLinkMetaDataFieldRequired; // Nodes Like Event does not contain the Link Info. This field helps in differentiation purpose.

    /**
     * @brief Widget that hold operation presentation layout.
     */
    Evas_Object * mOperationProgress;

    notification_h mOperationNotification;

    /**
     * @brief An entity id. Used in some specific screens such as GroupProfile and EventProfile.
     * Needed to avoid unnecessary post attachments
     */
    const char * mEntityId;

    Evas_Object * mHeader;

private:

    class AsyncLayoutUpdater : public IDataServiceProtocolClient {
        friend class ActionAndData;
    public:
        AsyncLayoutUpdater(LayoutType type, Evas_Object *layout, ActionAndData *actionData, int dataType = -1, int index = -1);
        virtual ~AsyncLayoutUpdater();
        bool DownloadImage(const char* url);
        bool LoadImage(const char* fileName);
        bool DownloadGraphData(int dataType);
        bool DownloadVideo(const char* url);
        static void AsyncCallback(void* data, const char *result);

    private:
        enum State {
            ENone,
            EImageDownloading,
            ETextDownloading,
            EImageLoading,
            EVideoDownloading
        };

    private:
        virtual void HandleDownloadImageResponse(ErrorCode error, MessageId requestID, const char* url, const char* fileName);
        virtual void HandleLoadImageResponse(const char* fileName);
        inline GraphObject *GetGraphObj() {return mActionData->mData;}
        inline ActionBase *GetAction() {return mActionData->mAction;}
    private:
        LayoutType mType;
        Evas_Object *mLayout;
        const char* mImagePath;
        MessageId mRequestId;
        ActionAndData *mActionData;
        State mState;
        int mDataType;
        int mIndex;

        std::map<int, std::string> mDataTypeMap;
    };

private:
    static int action_and_data_comparator(const void *data1, const void *data2);

private:
    ActionAndData *mParent;
    ActionAndData *mChild;

    Eina_List *mLayouts;
    clock_t mTimeStamp;
    static Eina_List *mAliveActionAndDates;
};

#endif /* ACTIONBASE_H_ */
