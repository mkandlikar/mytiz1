#ifndef COMMENTSACTION_H_
#define COMMENTSACTION_H_

#include "ActionBase.h"
#include "ScreenBase.h"

class CommentsAction: public ActionBase {
public:
    CommentsAction(ScreenBase *screen);
    virtual ~CommentsAction() { free(mParentEntityId); };

    void AvatarClicked(GraphObject *graphObject) override;
    void LinkClicked(GraphObject *graphObject) override;
    void CtxApprovedDeleteCmntClicked(ActionAndData *actionData) override;
    void PhotoClicked(GraphObject *graphObject) override;
    void CancelPostingOperation(ActionAndData *actionAndData) override;
    void RetryPostSend(ActionAndData *actionAndData) override;
    void SetParentId(const char *id);
    char* mParentEntityId;
};

#endif /* COMMENTSACTION_H_ */
