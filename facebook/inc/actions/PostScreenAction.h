#ifndef POSTSCREENACTION_H_
#define POSTSCREENACTION_H_

#include "FeedAction.h"

class PostScreenAction: public FeedAction {
public:
    PostScreenAction(ScreenBase *screen) : FeedAction(screen){}

private:
    void TextClicked(ActionAndData *userData) override {};
};

#endif /* POSTSCREENACTION_H_ */
