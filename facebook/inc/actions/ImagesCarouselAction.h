#ifndef IMAGESCAROUSELACTION_H_
#define IMAGESCAROUSELACTION_H_

#include "ActionBase.h"

class ImagesCarouselAction: public ActionBase {
public:
    ImagesCarouselAction(ScreenBase *screen);
    virtual ~ImagesCarouselAction() { }

    virtual void SaveBtnClicked(ActionAndData * user_data);
    virtual void SaveCaptionClicked(ActionAndData * user_data);
};

#endif /* IMAGESCAROUSELACTION_H_ */
