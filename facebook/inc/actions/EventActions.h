/*
 * EventActions.h
 *
 *  Created on: Nov 6, 2015
 *      Author: ruibezruch
 */

#ifndef EVENTACTIONS_H_
#define EVENTACTIONS_H_

#include "ActionBase.h"

#ifdef EVENT_NATIVE_VIEW
class EventActions: public ActionBase
{
public:
    EventActions(ScreenBase *screen);
    virtual ~EventActions();

    virtual void EventGoingBtnClicked(ActionAndData *actionData);
    static void on_event_going_completed(void *object, char *respond, int code);

    virtual void EventMaybeBtnClicked(ActionAndData *actionData);
    static void on_event_maybe_completed(void *object, char *respond, int code);

    virtual void EventNotGoingBtnClicked(ActionAndData *actionData);
    static void on_event_not_going_completed(void *object, char *respond, int code);

    virtual void CtxApprovedDeleteEvent(ActionAndData *actionData);
    static void on_event_deleting_completed(void *object, char *respond, int code);

    virtual void AddFriendBtnClicked(ActionAndData *actionData);
    static void on_add_friend_completed(void* object, char* respond, int code);
    static void on_cancel_friend_request_completed(void* object, char* respond, int code);

    inline Evas_Object *GetUpdatingToast() { return mUpdatingToast; }

private:
    GraphRequest *mGoingRequest;
    GraphRequest *mMaybeRequest;
    GraphRequest *mNotGoingRequest;
    GraphRequest *mDeleteRequest;

    Evas_Object *mParentBox;
    Evas_Object *mLoadingBtns;
    Evas_Object *mUpdatingToast;

    /**
     * @brief Object for getting ui constants.
     */
    static UIRes *R;
};

#endif
#endif /* EVENTACTIONS_H_ */
