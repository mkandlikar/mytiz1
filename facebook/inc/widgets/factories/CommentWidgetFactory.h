#ifndef COMMENTWIDGETFACTORY_H_
#define COMMENTWIDGETFACTORY_H_

#include <Elementary.h>
#include <Evas.h>

#include "ActionBase.h"
#include "Comment.h"
#include "CommentScreen.h"
#include "ConfirmationDialog.h"
#include "UIRes.h"

class CommentWidgetFactory {

public:
    static Evas_Object* CreateCommentWrapper(Evas_Object* parent, Evas_Object** wrapWidget, bool isAddToEnd );
    static Evas_Object* CreateReplyWrapper(Evas_Object *parent, Evas_Object** wrapWidget, bool isAddToEnd);
    static Evas_Object *CreateDot(Evas_Object* parent);
    static Evas_Object* CommentsGet(void *data, Evas_Object *parent, bool isAddToEnd = true );
    static Evas_Object *CreateCommentAttachmentWrapper(Evas_Object* parent);
    static Evas_Object *CreateCommentPhotoAttachmentWrapper(Evas_Object* parent);
    static void CallUpdateImageLayoutAsync(ActionAndData *action_data, Comment *comment, Evas_Object *picture);
    static void AttachmentWithoutPictureItem(Evas_Object* parent, ActionAndData *action_data);
    static void LinkAttachmentItem(Evas_Object* parent, ActionAndData *action_data);
    static void PageAttachmentItem(Evas_Object* parent, ActionAndData *action_data);
    static void PhotoAttachmentItem(Evas_Object* parent, ActionAndData *action_data);
    static void VideoAttachmentItem(Evas_Object* parent, ActionAndData *action_data);
    static void CommentItem(Evas_Object* parent, ActionAndData *action_data);
    static void ActionBar(Evas_Object* parent, ActionAndData *action_data);
    static void ReplyItem(Evas_Object* parent, ActionAndData *action_data, Comment *rootComment = nullptr);

    static void who_liked_comment_clicked(void *data, Evas_Object *obj, void *event_info);

    static void unlike_btn_clicked(void *data, Evas_Object *obj, void *event_info);
    static void like_btn_clicked(void *data, Evas_Object *obj, void *event_info);
    static void on_action_bar_deleted(void *data, Evas *e, Evas_Object *obj, void *event_info);

    static void reply_item_cb(void *data, Evas_Object *obj, void *event_info);
    static void reply_btn_cb(void *data, Evas_Object *obj, void *event_info);
    static void reply_signal_cb(void *data, Evas_Object *obj, const char *emission, const char *source);

    static Eina_Bool longpressed_cmnt_cb(void *data);
    static Evas_Event_Flags OnPhotoGestureTap(void *data, void *event_info);
    static void OnAnchorTap(void *data, Evas_Object *obj, void *event_info);
    static Evas_Event_Flags OnLinkGestureTap(void *data, void *event_info);
    static void mouse_down(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void mouse_up(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void add_scroller_events(Evas_Object *scroller);
    static void remove_scroller_events(Evas_Object *scroller);
    static void scroll(void *data, Evas_Object *obj, void *event_info);
    static void StopLongpressTimer();

    static void ShowContextMenu(ActionAndData *action_data, Evas_Object *obj, bool useOldPosition);
    static void on_edit_click(void *data, Evas_Object *obj, void *user_data);
    static void on_delete_click(void *data, Evas_Object *obj, void *user_data);
    static void on_copy_click(void *data, Evas_Object *obj, void *user_data);
    static void on_cancel_click(void *data, Evas_Object *obj, void *user_data);
    static void popup_approved_delete_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void on_delete_unposted_click(void *data, Evas_Object *obj, void *event_info);
    static void on_try_again_click(void *data, Evas_Object *obj, void *event_info);
    static bool CloseCommentPopup();
    static void confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event);

    static void ErrorPopupCb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void ShowErrorPopup(ActionAndData *action_data, Evas_Object *obj, bool useOldPosition);
    static void SetUploadErrorLayout(ActionAndData *action_data);
    static Evas_Object *CreateCommentOperation(Evas_Object *parent, void *user_data, bool isAddToEnd, Evas_Object* insertAfter = nullptr);
    static void UpdateCommentOperation(ActionAndData *action_data);
    static void AddLazyCommentItem(Evas_Object* parent, ActionAndData *action_data);
    static void LazyPhotoAttachmentItem(Evas_Object* parent, ActionAndData *action_data);

    static Evas_Object *CreateBaseLayout(Evas_Object *parent);

    static Evas_Object *CreateTopLikeLayout(Evas_Object *parent);
    static void RefreshTopLikeLayout(ActionAndData *actionData, Evas_Object *layout);

    static Evas_Object *CreateTopBackLayout(Evas_Object *parent);

    static Evas_Object *CreateEntry(Evas_Object *parent, int screenId);

    static Evas_Object *CreateBotLayout(Evas_Object *parent);
    static void RefreshBotLayout(Evas_Object *parentLayout, Evas_Object *layout, bool isTextEntered, bool isPhotoAttached);

    static void on_who_liked_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);

    /**
     * @brief Object for getting ui constants.
     */
    static UIRes *R;
    static ConfirmationDialog *mConfirmationDialog;
    static Ecore_Timer *mLongPressedTimer;
};

struct CObjAndData {
    ActionAndData *mCData;
    Evas_Object *mCLikeLabel;
    Evas_Object *mCLikeIcon;
    Evas_Object *mCLikeCount;
    Evas_Object *mCLikeLabelDot;
    Evas_Object *mCReplyLabelDot;
    Evas_Object *mCActionBar;
    CObjAndData();
    ~CObjAndData();
};

#endif /* COMMENTWIDGETFACTORY_H_ */
