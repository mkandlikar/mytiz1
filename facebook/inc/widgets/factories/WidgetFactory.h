#ifndef WIDGETFACTORY_H_
#define WIDGETFACTORY_H_
#include <Evas.h>
#include <Elementary.h>

#include "ConfirmationDialog.h"
#include "Popup.h"
#include "UIRes.h"

enum PostItemTitleType
{
    eNAME,
    eSTORY,
    eLOCATION,
    eVIA,
    eTo,
    eCOMPOSED_HEADER,
};

enum RenderType
{
    eNORMAL_POST_RENDER,
    eSTATUSUPDATE_POST_RENDER,
    eSHARED_POST_RENDER,
};

class ScreenBase;
class ActionAndData;
class PresentableAsPost;


class WidgetFactory {

public:
    static Evas_Object *CreateBaseScreenLayout(ScreenBase *screen, Evas_Object *parent, const char *header_text, bool withBackButton, bool withSearchButton, bool isWhiteBg, bool hasBotBar = false, bool hasSearchBar = false);
    static void RefreshBaseScreenLayout(Evas_Object *layout, const char *header_text);

    static Evas_Object *CreateSimpleWrapper(Evas_Object* parent);

    static Evas_Object *CreateChooseScreenLayout(ScreenBase *screen, Evas_Object *parent, const char *header_text);
    static Evas_Object *CreateChooseScreenEntry(Evas_Object *parent);
    static Evas_Object *CreateChooseScreenItem(Evas_Object *parent, ActionAndData *actionData);

    //Post wrappers
    //Wraps every post
    static Evas_Object* CreatePostItemWrapper(Evas_Object* parent, Evas_Object** wrapWidget, RenderType renderType = eNORMAL_POST_RENDER);
    //Wraps post that was liked, shared etc
    static Evas_Object* CreatePostInPostItemWrapper(Evas_Object* parent);
    //Wraps info
    static Evas_Object* CreateGreyWrapper(Evas_Object* parent);
    //Wraps comment box
    static Evas_Object* CreateCommentWrapper(Evas_Object* parent);

    //Customizing header's logic
    static void PostHeaderCustomization(Evas_Object* parent, ActionAndData *action_data, bool addContextMenu = true);
    //Customizing content's logic
    static void PostContentCustomization(Evas_Object* parent, ActionAndData *action_data);

    static void PostInPostHeaderCustomization(Evas_Object *content, ActionAndData *action_data);

    //Post basic modules
    //Simple post header just with name
    static void AddPostItemHeader(Evas_Object* parent, ActionAndData *action_data, PostItemTitleType titleContent = eNAME, bool addContextMenu = true);

    //Simple shared, commented etc post header
    static void AddPostInPostItemHeader(Evas_Object* parent, ActionAndData *action_data, PostItemTitleType titleContent = eNAME, bool addContextMenu = true);
    // Bumper
    static void AddPostItemBumpHeader(Evas_Object* parent, ActionAndData *action_data, PostItemTitleType titleContent = eSTORY);
    //Every post footer - statistic, buttons and friend comments
    static void AddPostStatsItemFooter(Evas_Object* parent, ActionAndData *action_data);
    static void RefreshPostStatsItemFooter(ActionAndData *action_data);
    static void AddPostBtnsItemFooter(Evas_Object* parent, ActionAndData *action_data);
    static void RefreshPostBtnsItemFooter(ActionAndData *action_data);
    static void RefreshPostItemFooter(ActionAndData *action_data);
    static void CommentItem(Evas_Object* parent, ActionAndData *action_data);
    static void AddSharedGroupCover(Evas_Object* parent, ActionAndData *action_data);

    static std::string CreateTitle(PostItemTitleType titleContent, PresentableAsPost& user_data);
    //Post content modules
    //Creating post text for post or post that was shared,commented etc
    static void CreatePostText(Evas_Object *parent, ActionAndData *action_data, const std::string& text);
    static void CreatePostFullText(Evas_Object *parent, ActionAndData *action_data);

    //Update post text value
    static void RefreshPostText(ActionAndData *action_data, const std::string& text);
    //Creating post in post text
    static void CreatePostInPostText(Evas_Object *parent, void *user_data);
    //Creating different types of photos for post
    static void CreatePhotoItem(Evas_Object *parent, void *user_data);
    static void CreateVideoItem(Evas_Object *parent, void *user_data);
    //Creating Post in Post item
    static void CreatePostInPostItem(Evas_Object *parent, ActionAndData *actionData, Evas_Object *fullTextPostInPostWrapper = nullptr);
    //Creating post box
    static Evas_Object *CreatePostBox(Evas_Object *parent, Evas_Object ** wrapWidget, void *user_data, RenderType renderType = eNORMAL_POST_RENDER);

    static void CreateLocationItem(Evas_Object *parent, void *user_data);
    static void CreateLocationItemFullView(Evas_Object *parent, void *user_data);
    static void CreateLocationInfoItem(Evas_Object *parent, void *user_data);
    static void CreateEventItemCover(Evas_Object *parent, void *user_data);\
    static void CreateEventItemInfo(Evas_Object *parent, void *user_data);
    static void CreateSharedPostItem(Evas_Object *content, ActionAndData *actionData);
    static void CreateLinkItem(Evas_Object *parent, void *user_data);
    static void CreatePageItem(Evas_Object *parent, void *user_data);

    // Lazy posting
    static void AddLazyPostItemHeader(Evas_Object *parent, ActionAndData *action_data);
    static void CreateLazyPhotoItem(Evas_Object *parent, ActionAndData *action_data);
    static void CreateLazyVideoItem(Evas_Object *parent, ActionAndData *action_data);
    static void DeletePost(void *data, Evas_Object *obj, void *event_info);
    static void CreateLazySharedItem(Evas_Object *parent, ActionAndData *action_data);

    //Composed posts
    //Creating simple post or post with location
    static Evas_Object *CreateStatusPost(Evas_Object *parent, void *user_data, RenderType renderType = eNORMAL_POST_RENDER);
    //Creating liked, shared, commented post type
    static Evas_Object *CreateAffectedPostBox(Evas_Object *parent, ActionAndData *actionData, RenderType renderType = eNORMAL_POST_RENDER);

    static void SetPostActive(ActionAndData *actionAndData, bool active);

    // Post Info Box and its elements (from date/time to privacy icon)
    static Evas_Object *CreatePostInfoBox(Evas_Object* parent);
    static void RefreshPostInfoBox(PresentableAsPost& post, Evas_Object* infoBoxWidget, int pxAvailableWidth);
    static Evas_Object *CreatePostInfoDate(Evas_Object* parent, double time);
    static Evas_Object *CreateDot(Evas_Object* parent, Evas_Object *afterWhat);
    static Evas_Object *CreateEditedLabel(Evas_Object *parent, Evas_Object *afterWhat);
    static Evas_Object *CreateAppLabel(Evas_Object* parent, PresentableAsPost& post, Evas_Object *afterWhat, int pxAvailableWidth);
    static Evas_Object *CreateImplicitPlaceLabel(Evas_Object* parent, PresentableAsPost& post, Evas_Object *afterWhat, int pxAvailableWidth);
    static Evas_Object *CreatePrivacyImage(Evas_Object* parent, PresentableAsPost& post);
    static Evas_Object *CreatePrivacySpacer(Evas_Object* parent);

    static int MeasureLabelWidth(Evas_Object *label);
    static void CreateCtxPopup(void *data, Evas_Object *obj);

    //Context menu
    static void ShowContextMenu(ActionAndData *action_data, Evas_Object *obj, bool useOldPosition);
    static void SetMenuItem(PopupMenu::PopupMenuItem *items, PopupMenu::PopupMenuItem &item, int index, int id, bool isLast, ActionAndData *data);
    static bool close_popup_menu();
    static void on_ctxmenu_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void on_ctxmenu_back_item_cb(void *user_data, Evas_Object *obj, void *event_info);

    static void on_context_menu_cb(void *user_data, Evas_Object *obj, void *event_info);

    static void ctxmenu_more_options_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void ctxmenu_more_options_owner_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void ctxmenu_dismissed_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void ctxmenu_item_select_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void ctxmenu_item_select_edit_post_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void ctxmenu_notifications_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void ctxmenu_edit_post_option_clicked_cb(void *data, Evas_Object *obj, void *event_info);
    static void ctxmenu_show_edit_history_clicked_cb(void *data, Evas_Object *obj, void *event_info);
    static void ctxmenu_edit_privacy_option_clicked_cb(void *data, Evas_Object *obj, void *event_info);
    static void on_share_btn_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_share_now_clicked(void *data, Evas_Object *obj, void *event_info);
    static void on_share_with_post_clicked(void *data, Evas_Object *obj, void *event_info);
    static void show_cancel_posting_popup_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void show_retry_delete_post_popup_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void show_error_popup_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);

    // Popup button actions
    static void popup_hide_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void popup_delete_post_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event);
    static void popup_ok_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void popup_approved_delete_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_approved_delete_clicked(void* object, char* respond, int code);
    static bool CloseConfirmDialogue();
    static void ShowAlertPopup(const char* Message, const char* Title = NULL, Evas_Object* parent = NULL);

    //Wrapping text
    static char* WrapByFormat(const char *formatPattern, const char *content);
    static char* WrapByFormat2(const char *formatPattern, const char *contentPart1, const char *contentPart2);
    static char* WrapByFormat2(const char *formatPattern, int contentPart1, int contentPart2);
    static char* WrapByFormat3(const char *formatPattern, const char *contentPart1, const char *contentPart2, const char *contentPart3);
    static char* WrapByFormat4(const char *formatPattern, const char *contentPart1,
            const char *contentPart2, const char *contentPart3, const char *contentPart4);
    static char* WrapByFormat5(const char *formatPattern, const char *contentPart1,
            const char *contentPart2, const char *contentPart3, const char *contentPart4, const char *contentPart5);

    /**
     * @brief Object for getting ui constants.
     */
    static UIRes *R;

    static Evas_Object *mErrorPopup;

    static Evas_Object *mEntryEmulator;

    static ConfirmationDialog * mConfirmationDialog;

    /**
     * Create wrapper by wrapper name.
     * return created wrapper box.
     *
     * @param parent parent object
     * @param edjePath path to edc file
     * @param wrapperName wrapper name from edc
     */
    static Evas_Object * CreateWrapperByName(Evas_Object * parent, const char * wrapperName);

    /**
     * Create layout by group name and add it at the end of parent box.
     * return created layout.
     *
     * @param parent parent object
     * @param edjePath path to edc file
     * @param groupName group name from edc
     */
    static Evas_Object * CreateLayoutByGroupAtParentBoxEnd(Evas_Object * parent, const char * groupName);

    /**
     * Create layout by group name.
     * return created layout.
     *
     * @param parent parent object
     * @param edjePath path to edc file
     * @param groupName group name from edc
     */
    static Evas_Object * CreateLayoutByGroup(Evas_Object * parent, const char * groupName);

    /**
     * Creates gray header with logo back button, right (confirm) text button and title
     *
     * @param parent parent object
     * @param textObject object which need to put in swallow part for the header title
     * @param rightButtonText text for right button
     * @param screen pointer to the screen needs for invoking buttons callbacks
     */
    static Evas_Object * CreateGrayHeaderWithLogo(Evas_Object * parent, Evas_Object * textObject,
            const char * rightButtonText, ScreenBase * screen);

    /**
     * Creates gray header with logo back button, right (confirm) text button and title
     *
     * @param parent parent object
     * @param headerText text for the header title
     * @param rightButtonText text for right button
     * @param screen pointer to the screen needs for invoking buttons callbacks
     */
    static Evas_Object * CreateGrayHeaderWithLogo(Evas_Object * parent, const char * headerText,
                const char * rightButtonText, ScreenBase * screen);

    static Evas_Object * CreateGrayHeaderWithGroupLogo(Evas_Object * parent, const char * headerText,
                const char * rightButtonText, ScreenBase * screen);
    /**
     * Creates black header with logo back button, right (confirm) text button and title
     *
     * @param parent parent object
     * @param textObject object which need to put in swallow part for the header title
     * @param rightButtonText text for right button
     * @param screen pointer to the screen needs for invoking buttons callbacks
     */
    static Evas_Object * CreateBlackHeaderWithLogo(Evas_Object *parent, Evas_Object *textObject,
            const char *rightButtonText, ScreenBase *screen);

    /**
     * Creates black header with logo back button, right (confirm) text button and title
     *
     * @param parent parent object
     * @param headerText text for the header title
     * @param rightButtonText text for right button
     * @param screen pointer to the screen needs for invoking buttons callbacks
     */
    static Evas_Object * CreateBlackHeaderWithLogo(Evas_Object *parent, const char * headerText,
                const char *rightButtonText, ScreenBase *screen);

    /**
     * Creates header with back button, right (confirm) text button and title
     *
     * @param parent parent object
     * @param isGrayHeader true for gray header and false for black
     * @param backButtonStyle LTR style for back button
     * @param headerText text for the header title
     * @param rightButtonText text for right button
     * @param screen pointer to the screen needs for invoking buttons callbacks
     */
    static Evas_Object * CreateHeaderWithBackBtnTextRightBtn(Evas_Object *parent, bool isGrayHeader,
            const char * backButtonStyle, const char * headerText, const char * rightButtonText, ScreenBase *screen);

    /**
     * Creates header with back button, right (confirm) text button and title
     *
     * @param parent parent object
     * @param isGrayHeader true for gray header and false for black
     * @param backButtonStyle LTR style for back button
     * @param textObject object which need to put in swallow part for the header title
     * @param rightButtonText text for right button
     * @param screen pointer to the screen needs for invoking buttons callbacks
     */
    static Evas_Object * CreateHeaderWithBackBtnTextRightBtn(Evas_Object *parent, bool isGrayHeader,
            const char * backButtonStyle, Evas_Object *textObject, const char * rightButtonText, ScreenBase *screen);
    /**
     * @brief Sets a text for a header if it was created without specific evas object for a text area.
     */
    static void SetHeaderText(Evas_Object * header, const char * text);

    static Evas_Object * CreateActionBarWithBackBtnText2Actions(Evas_Object *parent,
            const char *text, const char *firstActionImageFile, const char *secondActionImageFile, ScreenBase *screen);

    static void SelectBestGroupName(Eina_List *photos, char *edjeGroupName);

    //location error dialogue
    static void ShowErrorDialog(Evas_Object * layout, const char * message);
    static void hide_error_dialog_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void hide_error_dialog_cb(void *user_data, Evas_Object *obj, void *event_info);

    static void CreateSelectorItemWithIcon(Evas_Object *parent, bool isSelected, const char *icon_path, const char *text, const char *subtext, bool isLastItem, void (*callback) (void *, Evas_Object *, void *), void * callback_object);
    /**
     * Creates widget to render post operation
     *
     * @param parent parent object - it should be box
     * @param user_data pointer to ActionAndData object that should contain OperationPresenter
     * @param isAddToEnd indicates position in parent object (start/end) which the widget be added
     */
    static Evas_Object *CreatePostOperation(Evas_Object *parent, void *user_data, bool isAddToEnd);

    /**
     * Update post operation widget
     *
     * @param parent parent object - it should be box
     * @param action_data - it ActionAndData object that should contain OperationPresenter
     * @param stepProgress indicates progress (0.0 - 1.0) of current step in operation
     */
    static void UpdatePostOperation(ActionAndData *action_data, double *progress);

    static void RefreshItemHeader(bool gotError, Evas_Object *object, void *data);
    static void UpdateProgressBar(Evas_Object *object, double value);

    /**
     * @brief Closes error dialogue
     * @return true if dialogue is closed
     */
    static bool CloseErrorDialogue();
    static bool CloseAlertPopup();

    /**
     * Creates widget for GraphObject::Separator type
     */
    static Evas_Object* CreateSeparatorItem(Evas_Object* parent, bool isAddToEnd, const char * text, bool translatable = false);

    static Evas_Object* CreateConnectivityLostLayout(Evas_Object *parent, const char *part);

    static Evas_Object* CreateErrorWidget(Evas_Object *parent, const char * title, const char * description, Edje_Signal_Cb func, void *user_data);

    static Evas_Object *CreateFriendItem(Evas_Object *parent, ActionAndData *actionData, bool isAddToEnd);

    static void CreateFriendItemEmulator(Evas_Object *parent, const char* format, int emulatorWidth);

    static int GetEntryEmulatorLines(std::string &text, int itemWidth);

    static void DeleteFriendItemEmulator();

    static Evas_Object* CreateNoDataWidget(Evas_Object *parent, const char * title, const char * description);

    static void ShowCtxtMenu(ActionAndData *action_data, Evas_Object *obj, bool useOldPosition);

    private:
        static void refresh_item_footer(void* data);

        static PopupMenu::PopupMenuItem mMenuItemDelete;
        static PopupMenu::PopupMenuItem mMenuItemEdit;
        static PopupMenu::PopupMenuItem mMenuItemMore;
        static PopupMenu::PopupMenuItem mMenuItemEditPrivacy;
        static PopupMenu::PopupMenuItem mMenuItemBack;
        static PopupMenu::PopupMenuItem mMenuItemViewEditHistory;
};
#endif
