#ifndef PROFILEWIDGETFACTORY_H_
#define PROFILEWIDGETFACTORY_H_

#include <Evas.h>
#include <Elementary.h>

#include "ActionBase.h"
#include "Event.h"
#include "UIRes.h"

class ProfileWidgetFactory
{
public:

    enum InfoFieldID {
        FIELD_UNKNOWN,
        FIELD_DATE,
        FIELD_PLACE,
        FIELD_TICKETS,
        FIELD_INVITED,
        FIELD_MEMBER_REQUEST
    };

    ProfileWidgetFactory();
    virtual ~ProfileWidgetFactory();
    static Evas_Object* CreateProfileItemWrapper(Evas_Object *parent);
    static Evas_Object* CreateLoadingItemWrapper(Evas_Object *parent);
    static Evas_Object* CreateLoadingItem(Evas_Object *parent, bool isWhiteBg, bool isAddToEnd);

#ifdef EVENT_NATIVE_VIEW
    static Evas_Object *CreateSimpleProfileStatistic(Evas_Object *box, ActionAndData * actionData);
    static void UpdateSimpleProfileStatistic(Evas_Object *stats_item, Event *event);
    static void CreateAboutProfileItem(Evas_Object *parent, ActionAndData *action_data);
#endif

    static void CreateSingleInfoField(Evas_Object *parent, InfoFieldID fieldId, const char *icon_path, const char *data_1, const char *data_2, ActionAndData *action_data);
    static void CreateCoverItem(Evas_Object *parent, ActionAndData *action_data);
    static Evas_Object * CreateViewAllPostsItem(Evas_Object *parent, const char *id);
    static void Create3ActionBtsItem(Evas_Object *parent, ActionAndData *action_data, bool isUsingSavedData = false);
    static void Create4ActionBtsItem(Evas_Object *parent, ActionAndData *action_data, bool isUsingSavedData = false);
    static void CreatePostAndPhotoItem(Evas_Object *parent, ActionAndData *action_data);

#ifdef EVENT_NATIVE_VIEW
    static void SetEvent4Actions(Evas_Object *obj, ActionAndData *action_data, bool isRefresh);
    static bool CloseEventPopup();
#endif

    static void TransparentWidget(ActionAndData *action_data);
    static char *gen_members_number_text(unsigned int number);
private:
    /**
     * @brief Object for getting ui constants.
     */
    static UIRes *R;

#ifdef EVENT_NATIVE_VIEW
    static void on_about_text_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);
#endif

    static void on_post_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void on_photo_post_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);

    GraphRequest *mJoinGroupRequest;
    GraphRequest *mLeaveGroupRequest;
    static bool mAboutIsShown;


    static void SetGroup3Actions(Evas_Object *obj, ActionAndData *action_data);
    static void SetGroup4Actions(Evas_Object *obj, ActionAndData *action_data);

    static void on_join_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void on_invite_clicked_cb(void *data, Evas_Object *obj, void *event_info);
    static void on_viewinfo_clicked_cb(void *user_data, Evas_Object *obj, void *event_info);
    static void on_group_member_requests_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_share_clicked_cb(void *data, Evas_Object *obj, void *event_info);
    static void on_group_ctxmenu_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);

#ifdef EVENT_NATIVE_VIEW
    static void SetEvent3Actions(Evas_Object *obj, ActionAndData *action_data);
    static void SetEventInviteActions(Evas_Object *obj, ActionAndData *action_data);

    static void on_part_one_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void on_part_two_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void on_part_three_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void on_event_ctxmenu_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void on_copy_click(void *data, Evas_Object *obj, void *event_info);
    static void on_create_event_click(void *data, Evas_Object *obj, void *event_info);

    static void on_edit_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_event_invite_ctxmenu_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_delete_click(void *data, Evas_Object *obj, void *event_info);

    static void popup_approved_delete_cb(void *data, Evas_Object *obj, void *event_info);
    static void popup_cancel_delete_cb(void *data, Evas_Object *obj, void *event_info);
    static void on_view_all_posts_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
#endif

    static void on_cover_clicked_cb(void *data, Evas_Object *obj, const char *source, const char *emission);
    static void on_event_place_ctxmenu_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void on_view_page_selected_cb(void *data, Evas_Object *obj, void *event_info);
    static void on_open_maps_selected_cb(void *data, Evas_Object *obj, void *event_info);
    static void on_copy_location_selected_cb(void *data, Evas_Object *obj, void *event_info);
    static void on_find_tickets_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
};

#endif /* PROFILEWIDGETFACTORY_H_ */
