#ifndef GENLISTBASE_H_
#define GENLISTBASE_H_

#include <Elementary.h>

/*
 * Class GenListItemBase
 */
class GenListItemBase {
    friend class GenListGroupItemBase;
public:
    GenListItemBase(Elm_Genlist_Item_Class *itemClass = NULL);
    virtual ~GenListItemBase() {}

    static Evas_Object *CreateLayout(void *data, Evas_Object *obj, const char *part) {
        GenListItemBase *li = (GenListItemBase*) data;
        return li->DoCreateLayout(obj);
    }

    virtual int Compare(GenListItemBase* otherItem) {return 1;}
    virtual bool Filter(Elm_Object_Item *insertAfter) {return false;}
    virtual void Print() {} // may be provided for debug purposes
    virtual Elm_Object_Item *GetLastItem() {return mItem;}

    void ShowItem();

protected:
    void SetGenList(Evas_Object *genList) {mGenList = genList;}

private:
    void InsertAfter(Elm_Object_Item *after) {
        mItem = elm_genlist_item_insert_after(mGenList, mItemClass, this, NULL, after, ELM_GENLIST_ITEM_NONE, NULL, NULL);
    }
    void InsertBefore(Elm_Object_Item *before) {
        mItem = elm_genlist_item_insert_after(mGenList, mItemClass, this, NULL, before, ELM_GENLIST_ITEM_NONE, NULL, NULL);
    }
    void Prepend () {
        mItem = elm_genlist_item_prepend(mGenList, mItemClass, this, NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);
    }
    void Append () {
        mItem = elm_genlist_item_append(mGenList, mItemClass, this, NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);
    }

    void Remove() {
        elm_object_item_del(mItem);
        mItem = NULL;
    }
    virtual Evas_Object *DoCreateLayout(Evas_Object *parent) {
        Evas_Object *layout = elm_layout_add(parent);
        evas_object_size_hint_min_set(layout, 0, 0);
        evas_object_size_hint_max_set(layout, 0, 0);
        return layout;
    }

    void SetSelectable(bool selectable) {mIsSelectable = selectable;}

public:
     Elm_Object_Item *mItem;

protected:
     Evas_Object *mGenList;
     bool mIsSelectable;

private:
     Elm_Genlist_Item_Class *mItemClass;
};

/*
 * Class GenListGroupItemBase
 */
class GenListGroupItemBase : public GenListItemBase {
public:
    GenListGroupItemBase(Elm_Genlist_Item_Class *itemClass = NULL) : GenListItemBase(itemClass), mItems(NULL), mIsFilteredIfEmpty(true), mIsRoot(false) {}
    virtual ~GenListGroupItemBase();
    void SetFilteredIfEmpty(bool filteredIfEmpty) {mIsFilteredIfEmpty = filteredIfEmpty;}

    static int compare(const void *data1, const void *data2);

    inline Eina_List *GetList(){return mItems;}
    Elm_Object_Item *AddItem(GenListItemBase* item);
    void RemoveItem(GenListItemBase* item);

    void ApplyFilter();

    virtual bool Filter(Elm_Object_Item *insertAfter);
    bool IsGroupFiltered();

private:
    Elm_Object_Item *AddToGenList(GenListItemBase* item);

    virtual Elm_Object_Item *GetLastItem();

protected:
    Eina_List *mItems;
    bool mIsFilteredIfEmpty;
    bool mIsRoot;
};

/*
 * Class GenList
 */
class GenList : public GenListGroupItemBase {
public:
    GenList(Evas_Object *parent);
    Evas_Object *GetGenList();
    virtual ~GenList();
};

#endif //GENLISTBASE_H_
