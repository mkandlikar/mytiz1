#ifndef CONFIRMATIONDIALOG_H_
#define CONFIRMATIONDIALOG_H_

#include <Evas.h>


class ConfirmationDialog {
public:
    enum CDEventType {
        ECDYesPressed,
        ECDNoPressed,
        ECDDismiss
    };

    typedef void (*ConfirmationDialogCB)(void *user_data, CDEventType event);

    ~ConfirmationDialog();

    static ConfirmationDialog *CreateAndShow(Evas_Object *parent, const char*caption, const char*description,
            const char*yesText, const char*noText, ConfirmationDialogCB callBack, void *userData);

private:
    ConfirmationDialog(Evas_Object *parent, const char* caption, const char* description,
            const char* yesText, const char* noText, ConfirmationDialogCB callBack, void *userData);
    bool Close();

    static void on_yes_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void on_no_clicked(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void dismiss_cb(void *data, Evas_Object *obj, void *event_info);


    Evas_Object *mConfirmationPopup;
    ConfirmationDialogCB mConfirmationDialogCb;
    void *mCbData;
};

#endif //CONFIRMATIONDIALOG_H_
