#ifndef USERITEMWIDGET_H_
#define USERITEMWIDGET_H_

#include <Evas.h>

class ActionAndData;


class ItemWidgetBase {
public:
    virtual ~ItemWidgetBase(){}
    virtual Evas_Object *CreateLayout(Evas_Object* parent) = 0;
    inline Evas_Object *GetLayout(){return mLayout;}
    void DestroyLayout();

protected:
    Evas_Object *mLayout;
};

class UserItemWidget : public ItemWidgetBase {
public:
    UserItemWidget(ActionAndData *actionAndData);
    ~UserItemWidget();

    virtual Evas_Object *CreateLayout(Evas_Object* parent);
    inline ActionAndData *GetActionAndData() {return mAnD;}

private:
    void RefreshButton(Person::FriendshipStatus status);

    static void on_item_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_friends_button_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_add_friend_btn_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_cancel_friend_btn_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source);

private:
    ActionAndData *mAnD;

    static UIRes *R;
};

#endif //USERITEMWIDGET_H_
