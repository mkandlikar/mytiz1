#ifndef POPUPBASE_H_
#define POPUPBASE_H_

#include "UIRes.h"

class PopupBase {
public:
    static void Move(Evas_Coord y);
    static bool Close();
    static bool IsShown();

protected:
    PopupBase();
    virtual ~PopupBase();

    void Show(Evas_Object *parent, Evas_Coord y);
    void SetContent(Evas_Object *);
    Evas_Object *GetPopupLayout();

    static void dismiss_cb(void *data, Evas_Object *obj, const char *emission, const char *source);

private:
    virtual Evas_Object *CreateContent() = 0;

protected:
    static PopupBase *mPopup;
    static UIRes *R;

private:
    Evas_Object *mPopupLayout;
    Evas_Object *mSpacer;
};

#endif /* POPUPBASE_H_ */
