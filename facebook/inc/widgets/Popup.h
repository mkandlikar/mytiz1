/*
 * Popup.h
 *
 *  Created on: Jun 20, 2015
 *      Author: RUINMKOL
 */

#ifndef POPUP_H_
#define POPUP_H_

#include <Evas.h>
#include "BaseObject.h"
#include "UIRes.h"

/*
 *
 */
class PopupMenu
{
public:
    struct PopupMenuItem
    {
        const char * itemIcon;
        const char * itemText;
        const char * itemSubText;
        void (*callback) (void *, Evas_Object *, void *);
        void * data;
        int itemId;
        bool itemLastItem;
        bool itemSelected;
    };

    static void Show(int itemNum, PopupMenuItem * items, Evas_Object * parent, bool useOldPosition,
            Evas_Coord y, BaseObject *contextObject = NULL);
    static void ShowMore(int itemNum, PopupMenuItem * items, Evas_Object * parent);
    static bool Close();
    static bool IsPopupShown();

    static void menu_dismiss_cb(void *data = NULL, Evas_Object *obj = NULL, void *event_info = NULL);
    static void menu_item_sel_cb(void *data, Evas_Object *obj, void *event_info);
private:
    static Evas_Object * mPopup;
    static int mItemNum;
    static int mY;
    static bool mIsTopOffset;
    static PopupMenuItem * mItems;

    /**
     * @brief Object for getting ui constants.
     */
    static UIRes *R;
};

#endif /* POPUP_H_ */
