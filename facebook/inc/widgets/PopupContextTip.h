#ifndef POPUPCONTEXTTIP_H_
#define POPUPCONTEXTTIP_H_

#include "PopupBase.h"

class PopupContextTip : public PopupBase {
public:
    static void Open(Evas_Object *parent, Evas_Coord y, const char*text);
    static void Open(Evas_Object *parent, const char*text);

private:
    PopupContextTip(const char*text);
    ~PopupContextTip();

    virtual Evas_Object *CreateContent();

private:
    const char *mText;
    Evas_Object *mContextTip;
};

#endif /* POPUPCONTEXTTIP_H_ */
