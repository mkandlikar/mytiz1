#ifndef FRIENDTAGGING_H_
#define FRIENDTAGGING_H_


#include "PopupBase.h"
#include "Post.h"

#include <vector>

class Friend;


class FriendTagging
{
private:
    enum SearchType {
        SEARCH_TYPE_NONE,
        SEARCH_TYPE_INSTANT,
        SEARCH_TYPE_POSTPONED
    };

    class TaggedFriend {
    public:
        TaggedFriend(Friend *fr) : mFriend(fr), mBeginOffset(0), mLength(0), mIdOffest(0), mIdLen(0), mNameOffset(0), mNameLen(0),
           mId(nullptr), mName(nullptr) {
            mFriend->AddRef();
        }

        ~TaggedFriend() {
            mFriend->Release();
            free (mId);
            free (mName);
        }

        Friend *mFriend;
        int mBeginOffset;
        int mLength;
        int mIdOffest;
        int mIdLen;
        int mNameOffset;
        int mNameLen;
        char *mId;
        char *mName;
    };

    class TaggedFriendsParser {
    public:
        enum Mode{
            eComposer,
            eFeed
        };
        TaggedFriendsParser();
        ~TaggedFriendsParser();

        void SetText(const char*text, bool isPreeditActive);
        inline const char *GetText() {return mText;}
        inline bool IsTextChanged() {return mIsTextChanged;}

        void SetMode(Mode mode);
        void Parse();
        static char *GenVisibleName(Friend &fr);

    public:
        Eina_List *mTaggedFriends;

    private:
        TaggedFriend *ParseTaggedFriend(int beginOffset);
        const char *FindTagEnd(const char *begin);
        void RemoveTag(TaggedFriend *tag);
        void RemoveLeadingSymbolsFromTag(TaggedFriend *tag);
        void RecalcTagOffsets(int offset, int delta);
        const char *Addr(int offset);
        int Offset(const char *address);
        void CleanList();

    private:
        char* mText;
        bool mIsTextChanged;
        bool mIsPreeditActive;

        const char *mBeforeId;
        const char *mAfterId;
        const char *mAfterName;

        static const char *KBbeforeId;
        static const char *KAfterId;
        static const char *KAfterName;

        static const char *KBoldBeforeId;
        static const char *KBoldAfterId;
        static const char *KBoldAfterName;
    };

    struct PCData {
    public:
        PCData(const char* textToShow);
        ~PCData();

        inline void SetFriend(Friend *fr) {mFriend = fr;}
        inline Friend *GetFriend() {return mFriend;}

    public:
        FriendTagging *mScreen;
        char *mTextToShow;

    private:
        Friend *mFriend;
    };

    class TaggingPopup : public PopupBase {
    public:
        TaggingPopup(FriendTagging *mTagging, std::vector<Friend*> const &friends, bool upsideDown);
        ~TaggingPopup();

        static void Open(Evas_Object *parent, FriendTagging *tagging, std::vector<Friend*> const &friends, bool upsideDown);
        static Evas_Coord GetHeight() {return static_cast<TaggingPopup *> (mPopup)->mHeight;}
        static bool IsShown() {return mIsShown;}

    private:
        Evas_Object *WrapWithSize(Evas_Object *parent, Evas_Object *obj, Evas_Coord w, Evas_Coord h);
        Evas_Object *CreateContent() override;
        Evas_Object *CreatePopupItem(PCData *data, Evas_Object *parent);

        static void on_popup_item_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
        static void on_popup_item_delete_cb(void *data, Evas *e, Evas_Object *obj, void *event_info);

    private:
        std::vector<Friend*> const mFriends;
        FriendTagging *mTagging;
        bool mUpsideDown;
        Evas_Coord mHeight;
        static const unsigned int mMaxItemsNum;
        static bool mIsShown;
    };

public:
    FriendTagging(Evas_Object *parent, Evas_Object *entry);
    virtual ~FriendTagging();

    std::list<Tag> GetMessageTagsList();
    std::list<Tag> GetMessageTagsList(const char* text);

    char *GetStringToPost(bool originMsg);
    inline void SetUpsideDownList(bool uppend) {mUpsideDownList = uppend;}

    int GetBytePos(const char *text, int start, int end);

    bool HideFriendsPopup();
    bool IsLongText(int maxLength);

private:
    void SearchUtility();
    char* GetTextByRange(const char* text, int begin, int end);
    void TextChanged();
    void CheckExistentTags();
    void LookForKeyword();

    void CreatePopupItem(PCData *data);
    /* Move popup
       max num of items in popup = 4, min = 1 according to FB4A
       when count = 1, popup should be moved down (in case of we're on CommentScreen/ReplyScreen)
       2 etc - move it up on itemSize height - by increasing "tag_popup" rel1's offset in "comment_screen.edc"
       in other words, we make different states for one SWALLOW edc part, depending on items' num (count)
    */
    void OnPopupItemCb(Friend *friends);
    void RemoveTag(TaggedFriend *tag);
    void RemoveLeadingSymbolsFromTag(TaggedFriend *tag);
    void ShowPopup(std::vector<Friend*> const &friendsVector);
    bool IsFriendFound(const char *firstName, const char *lastName, const char *tocompare);
    static Friend *CreateLoggedUser();

    void AddEntryCallbacks();
    void DeleteEntryCallbacks();

    void DoSearch();
    void UpdateBufferString();
    const char *GetTextAfterDelim(const char *text);
    bool IsPreeditActive();
    char *PreeditText();
    char *GetTextByRange(int begin, int end, bool IsPreeditRequired);
    char *GetTextBeforeCursor(bool IsPreeditRequired);

    unsigned int GetPositionsNumber(const char *text);

    void TagFriend(Friend *fr);

    static void text_changed_cb(void *data, Evas_Object *obj, void *event_info);

private:
    Evas_Object *mParentScreenLayout;
    Evas_Object *mMessageEntry;

    SearchType mSearchType;
    bool mSearchAllowed;
    bool mUpsideDownList;
    std::string mBufferString;
    Evas_Object *mEntryEmulator;
    TaggedFriendsParser mParser;
};

#endif /* FRIENDTAGGING_H_ */
