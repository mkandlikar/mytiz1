
#ifndef UTILITIES_PTINTERFACE_H_
#define UTILITIES_PTINTERFACE_H_

class PTInterface {
public:
    virtual ~PTInterface() { }
    virtual void RemoveTag(void * data) = 0;
    virtual bool FitToScreen() = 0;
    virtual void FitToUserScreen() = 0;
    virtual void EnableDismissCallback(bool enable) = 0;
};

#endif /* UTILITIES_PTINTERFACE_H_ */
