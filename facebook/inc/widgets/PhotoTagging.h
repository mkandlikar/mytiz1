#include "ActionBase.h"
#include "Photo.h"
#include "PopupBase.h"
#include "PTInterface.h"

typedef struct ImageRegion {
    int x;
    int y;
    int width;
    int height;
    int momentumX;
    int momentumY;
    int offsetX;
    int offsetY;
    double prevZoom;
} imageRegion;
typedef struct ScreenSize {
    int x;
    int y;
    int width;
    int height;
} screenSize;
class PhotoTagging: private PTInterface {
public:
    struct SearchItem {
    public:
        SearchItem(const char* picturePath, const char * id, const char * name);
        ~SearchItem();
        char * mPicturePath;
        char * mId;
        char * mName;
    };
    struct PTData {
    public:
        PTData(PhotoTagging *pt, SearchItem *item);
        ~PTData();
        inline SearchItem *GetItem() {return mItem;}
        inline PhotoTagging *GetPT() {return mPT;}
    private:
        PhotoTagging *mPT;
        SearchItem *mItem;
    };
    class PhotoTaggingPopup: public PopupBase {
    public:
        enum PopupItemType{
            MIDDLE,
            LAST,
            TEXT
        };
        PhotoTaggingPopup(PhotoTagging *mTagging, bool upsideDown, Eina_List * items);
        ~PhotoTaggingPopup();
        static void Open(Evas_Object *parent, PhotoTagging *tagging, bool upsideDown, Eina_List * items);
        Evas_Object *WrapWithSize(Evas_Object *parent, Evas_Object *obj, Evas_Coord w, Evas_Coord h);
        Evas_Object *CreateContent() override;
        Evas_Object *CreatePopupItem(PTData *data, Evas_Object *parent, PopupItemType type);
        static void on_popup_item_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
        static void on_popup_item_delete_cb(void *data, Evas *e, Evas_Object *obj, void *event_info);
        Eina_List * mItems;
        PhotoTagging *mPTagging;
        Evas_Coord mHeight;
        static const unsigned int mMaxItemsNum = 4;
    };

    Evas_Coord mX;
    Evas_Coord mY;
    Evas_Coord mW;
    Evas_Coord mH;
    Evas_Object * mParentLayout;
    Evas_Object * mTagLayout;
    Evas_Object * mTagText;
    Evas_Object * mEntryItem;
    Evas_Object * mEntry;
    Evas_Object * mEntryEmulator;
    Evas_Object * mTextBlockEmulator;
    ImageRegion *mImageRegion;
    bool mIsEditMode;
    bool mIsNeedDelete = false;
    bool mIsNeedCreate = false;
    PTInterface * mParent;
    PhotoTag * mPhotoTag;
    std::string mBufferString;
    ActionAndData * mActionAndData;
    GraphRequest * mRequest;
    Evas_Object *mClipper;
    Evas_Object *mAboveLayout;
    static UIRes *R;

    static void on_delete_button_clicked(void *user_data, Evas_Object *obj, const char *emission, const char *source);
    static void on_get_tagged_user_details_completed(void *data, char* str, int code);
    static void entry_changed_cb(void* data, Evas_Object* obj, void* event_info);
    static void menu_dismiss_cb(void *data, Evas_Object *obj, const char *emission, const char *source);
    static void entry_key_down_cb(void *data, Evas *e, Evas_Object *obj, void *event_info);

    ~PhotoTagging();
    PhotoTagging(ActionAndData * actionAndData, Evas_Object * layout, PhotoTag * photoTag, ImageRegion * imageRegion, bool isEditView = false, Evas_Object *clipper = nullptr, Evas_Object *above = nullptr);
    PhotoTagging(Evas_Object * layout, double xTap, double yTap, ImageRegion * imageRegion, PTInterface *parent, Evas_Object *clipper = nullptr, Evas_Object *above = nullptr);
    inline void Hide(){ evas_object_hide(mTagLayout); }
    inline void Show(){ evas_object_show(mTagLayout); }
    bool CreateTag( const char * tagName, const char * tagId );
    void CreateEntryEmulator();
    void GetX();
    void GetY();
    void UpdateImageGeometry(ImageRegion *newImageRegion);
    bool HidePTPopup();
    void OnPopupItemCb(SearchItem *item);
    void StartCreatingTag(const char * tagName, const char * tagId);
    void ShowPopup(Eina_List * items);
    void DoSearch();
    bool IsItemFound(const char *firstName, const char *lastName, const char *tocompare);
    void CalculateTagLocation();
    Evas_Object * CreateSearchEntry();
    virtual void RemoveTag(void * data){};
    virtual bool FitToScreen() {return true;};
    virtual void FitToUserScreen() {};
    virtual void EnableDismissCallback(bool enable) {};
public:
    bool ClosePopUp();
};

