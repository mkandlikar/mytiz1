#ifndef COMPOSERPHOTOWIDGET_H_
#define COMPOSERPHOTOWIDGET_H_

#include "ActionBase.h"
#include "CameraRollScreen.h"
#include "Post.h"

enum ScreenMode {
    ePOST_COMPOSE,
    ePOST_EDIT,
};

class IPhotoWidgetEvents {
public:
    virtual ~IPhotoWidgetEvents() {}
    virtual void CaptionTextChanged(PostComposerMediaData *mediaData) = 0;
    virtual void CropImageButtonClick(PostComposerMediaData *mediaData) = 0;
    virtual void DeleteMediaButtonClick(PostComposerMediaData *mediaData, Evas_Object *photoWidgetLayout) = 0;
};

class ComposerPhotoWidget {

public:
    ComposerPhotoWidget(ScreenMode mode = ePOST_COMPOSE, Evas_Object *layout = NULL, Evas_Object *boxLayout = NULL,
            IPhotoWidgetEvents *photoWidgetCb = NULL);

    virtual ~ComposerPhotoWidget();

    bool IsDataValid(Eina_List *mediaList);

private:
    Evas_Object *mLayout;
    ActionAndData *mActionAndData;
    Evas_Object *mMediaBox;
    ScreenMode mMode;
    IPhotoWidgetEvents *mIPhotoWidgetCb;

    void CreateSelectedMedia();

    void CreateMediaContent(Evas_Object * parent, PostComposerMediaData *mediaData);

    void CreatePhotoCaption(Evas_Object * parent, PostComposerMediaData * mediaData);

    static void on_caption_text_changed(void *data, Evas_Object *obj, void *event_info);
#ifdef FEATURE_ADD_TEXT
    static void on_add_text_button_clicked(void *data, Evas_Object *obj, void *event_info);
#endif
    static void on_crop_button_clicked(void *data, Evas_Object *obj, void *event_info);
    static void on_delete_media_button_clicked(void *data, Evas_Object *obj, void *event_info);
    static void on_media_layout_deleted(void *data, Evas *e, Evas_Object *obj, void *event_info);

    static Evas_Event_Flags on_photo_clicked(void *data, void *event_info);
    static Evas_Event_Flags on_video_clicked(void *data, void *event_info);

    bool CheckIsAvailableToDecode(PostComposerMediaData* mediaData);
    Eina_List* GetMediaItems();
};

struct PhotoWidgetUserData {
    IPhotoWidgetEvents *mPhotoWidgetEventsCb;
    PostComposerMediaData *mMediaData;

    PhotoWidgetUserData(IPhotoWidgetEvents *photoWidgetEventsCb, PostComposerMediaData *mediaData);
};

#endif /* COMPOSERPHOTOWIDGET_H_ */
