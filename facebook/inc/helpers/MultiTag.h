#ifndef MULTITAG_H_
#define MULTITAG_H_

#include <list>
#include <string>

class MultiTag{
   public:
       MultiTag(const std::string& id, const std::string& name, const std::string& type, int offset, int length);
       MultiTag(const MultiTag& other);
       MultiTag(MultiTag&& other);
       ~MultiTag();

       inline bool IsSameTag(int offset, int length) const { return (mOffset == offset && mLength == length); }
       void AddItem(const std::string& id, const std::string& type);
       std::string GenTag();
       inline std::string GetName(){return mName;}
       inline std::string GetId() {return mId;}
       inline int GetOffset(){return mOffset;}
       inline int GetLength(){return mLength;}
   private:
       class TagItem {
       public:
           TagItem(const std::string& id, const std::string& type): mId(id), mType(type) {
           }
           TagItem(const TagItem& other): mId(other.mId), mType(other.mType) {
           }
           TagItem(TagItem&& other): mId(std::move(other.mId)), mType(std::move(other.mType)) {
           }
           ~TagItem() {
           }
           std::string mId;
           std::string mType;
       };
   private:
       std::string mName;
       std::string mId;
       int mOffset;
       int mLength;
       std::list<TagItem> mItems;
};

#endif /* MULTITAG_H_ */
