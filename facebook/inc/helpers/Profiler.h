/*
 * Profiler.h
 *
 * Created on: Sep 16, 2015
 * Author: Dmitry.Shcherbakov@SymphonyTeleca.com
 */

#ifndef PROFILER_H_
#define PROFILER_H_

#include <stdarg.h>

typedef unsigned int id_t;
typedef long long micro_secs_t;

class Profiler {
    Profiler();
public:
    static Profiler* GetInstance();
    virtual ~Profiler();

    micro_secs_t Start( id_t id, const char *format, ... );
    void Stop( id_t id, micro_secs_t startTime, const char *format, ... );

    id_t GenerateId();
private:
    typedef enum TRACE_STATE_ {
        START_TRACE,
        STOP_TRACE
    } TRACE_STATE;

    void Trace( TRACE_STATE state, id_t id, micro_secs_t time, const char *msg, va_list &list );
    void Flush();

    static const char *OUT_FILE_NAME;
    static const char *DLOG_PROFILING_TAG;
    static const int TRACE_BUFFER_LINE_SIZE;
};

#endif /* PROFILER_H_ */
