#ifndef UTILITIES_SELECTEDMEDIALIST_H_
#define UTILITIES_SELECTEDMEDIALIST_H_

#include "PostComposerMediaData.h"

namespace SelectedMediaList {
    Eina_List* Get();
    void AddItem(PostComposerMediaData * share_image);
    void RemoveItem(PostComposerMediaData *mediaData);
    void Clear();
    unsigned int Count();
    void Print();
    bool IsAnyCaptionEdited();
}

#endif /* UTILITIES_SELECTEDMEDIALIST_H_ */
