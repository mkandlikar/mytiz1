#ifndef SOUNDNOTIFICATIONPLAYER_H_
#define SOUNDNOTIFICATIONPLAYER_H_

#include <map>
#include <string>

/**
 * @class SoundNotificationPlayer
 *
 * @brief SoundNotificationPlayer - service for playing predefined sounds.
 *
 * @details SoundNotificationPlayer plays predefined notification sounds with respect to system settings.

 */

class SoundNotificationPlayer {
public:
    enum NotificationSound {
        eSoundPostMain,
        eSoundComment,
        eSoundShare,
        eSoundLikeMain,
        eSoundPullToRefreshFast
    };
    static void PlayNotification(NotificationSound sound);
private:
    static const char* GetNotificationSoundPath(NotificationSound soundId);
    static bool IsDeviceMuted();
    //AAA (B2430):If jack is connected then we use SOUND_SESSION_TYPE_MEDIA. But it seems that we need to use the same value when it's disconnected.
    // The function below isn't used now, but let's keep it commented out for a while. Probably some use cases are lost.
    //static bool IsJackConnected();

    typedef std::map<NotificationSound, std::string> NotificationPathIdMap;
    typedef std::pair<NotificationSound, std::string> NotificationPathIdPair;

    static NotificationPathIdMap SoundPathMap;
};

#endif /* SOUNDNOTIFICATIONPLAYER_H_ */
