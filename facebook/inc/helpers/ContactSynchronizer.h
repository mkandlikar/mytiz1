#ifndef CONTACTSYNCHRONIZER_H_
#define CONTACTSYNCHRONIZER_H_

#include "AppEvents.h"
#include "DataUploader.h"
#include "DataServiceProtocol.h"
#include "Mutex.h"
#include <map>
#include <set>
#include <memory>
#include <string>

/*
 * @class ContactSynchronizer
 *
 * @extends Subscriber
 * @implements IDataServiceProtocolClient
 *
 * @brief Object controlling synchronization of contacts between Facebook and Tizen address book.
 *
 * @details ContactSynchronizer performs synchronization beetween Facebook account friend list and Tizen address book.
 * This class is design to be used only with single instance, so pointer on it should be held by application instance.
 * It performs synchronization in 2 cases:
 *  - Facebook account is changed (the class is listening to eACCOUNT_CHANGED event).
 *  - Every 24 hours since account log in.
 * After receiving eACCOUNT_CHANGED ContactSynchronizer waits for 1 minute and only then starts synchronization.
 * It was done to give ability the feed to download all required data at maximum speed in case of narrow internet channel.
 * All contact synchronization activities are performed in service thread and they don't influence GUI performance.
 */

class ContactSynchronizer: public Subscriber, public IDataServiceProtocolClient {
public:
    ContactSynchronizer();
    virtual ~ContactSynchronizer();
    virtual void Update(AppEventId eventId = eUNDEFINED_EVENT, void* data = NULL);
    virtual void HandleDownloadImageResponse(ErrorCode error, MessageId requestID, const char* url, const char* fileName);

private:
    static Eina_Bool start_contact_data_downloading(void* data);
    static void on_contact_data_downloading_finished(void* data);

    static void delete_contacts_async(void* data, Ecore_Thread* thread);
    static void write_contacts_async(void* data, Ecore_Thread* thread);
    static void on_delete_contact_async_finished(void* data, Ecore_Thread* thread);
    static void on_write_contact_async_finished(void* data, Ecore_Thread* thread);
    static void update_contact_with_image_async(void* data, Ecore_Thread* thread);
    static void on_update_contact_with_image_async_finished(void* data, Ecore_Thread* thread);

    struct ContactImageUpdateData;

    std::unique_ptr<DataUploader> mContactsDataDownloader;

    int mAccoundId;
    std::map<MessageId, std::pair<int, std::string> > mImageRequestIdToContactId;
    Mutex mMutex;

    Ecore_Timer* mRegularSynchronizationTimer;

    bool mIsInProcessOfSynchronization;
    bool mPerformSynchronizationOnConnection;

    static Ecore_Thread* mContactWritingThread;
    static std::set<Ecore_Thread*> mContactUpdateThreads;
#ifdef _DEBUG
    static unsigned int mInstanceCounter;
#endif
};

#endif /* CONTACTSYNCHRONIZER_H_ */
