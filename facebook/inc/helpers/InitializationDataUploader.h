#ifndef INITIALIZATIONDATAUPLOADER_H_
#define INITIALIZATIONDATAUPLOADER_H_

#include "AppEvents.h"
#include "GraphRequest.h"

class DataUploader;
class UserProfileDataUploader;

typedef void ( *data_initialization_done_cb )( void );

class InitializationDataUploader
{
public:
    InitializationDataUploader( data_initialization_done_cb done_cb );
    virtual ~InitializationDataUploader();

    void StartUploading();
protected:
    void StartUploadHomeData();

private:
    static void on_user_profile_info_received_cb ( void *parent, int code );
    static void friends_has_been_received_cb( void *parent );

    class HomeDataAssistant: public Subscriber
    {
    public:
        HomeDataAssistant(data_initialization_done_cb done_cb);
        virtual ~HomeDataAssistant();

        virtual void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = NULL);
        virtual void UpdateErase() {
        }
    private:
        data_initialization_done_cb m_HomeDataRecevived_cb;
    };
    HomeDataAssistant *m_HomeDataAssistant;
    DataUploader *m_FriendsDataUploader;
    UserProfileDataUploader *mOwnProfileData;
    data_initialization_done_cb m_UploadingDone;
};

#endif /* INITIALIZATIONDATAUPLOADER_H_ */
