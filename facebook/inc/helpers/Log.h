#ifndef LOG_H
#define LOG_H

#include <stdarg.h>
#include <dlog.h>

typedef enum {
    LOG_FACEBOOK,
    LOG_FACEBOOK_EVENT,
    LOG_FACEBOOK_GROUP,
    LOG_FACEBOOK_TAGGING,
    LOG_FACEBOOK_FRIENDS_ACTION,
    LOG_FACEBOOK_OPERATION,
    LOG_FACEBOOK_NOTIFICATION,
    LOG_FACEBOOK_COMMENTING,
    LOG_CACHEMANAGER,
    LOG_CONTACT_SYNC,
    LOG_FACEBOOK_USER,
    LOG_FACEBOOK_DATASERVICEPROTOCOL,

    LOG_LAST
} LogTags;

extern const char *LOG_TAGS[LOG_LAST];

class Log
{
public:
    static void verbose(const char *msg, ...);
    static void verbose(LogTags tag, const char *msg, ...);
    static void verbose_tag(const char *tag, const char *msg, ...);
    static void info(const char *msg, ...);
    static void info(LogTags tag, const char *msg, ...);
    static void info_tag(const char *tag, const char *msg, ...);
    static void debug(const char *msg, ...);
    static void debug(LogTags tag, const char *msg, ...);
    static void debug_tag(const char *tag, const char *msg, ...);
    static void warning(const char *msg, ...);
    static void warning(LogTags tag, const char *msg, ...);
    static void warning_tag(const char *tag, const char *msg, ...);
    static void error(const char *msg, ...);
    static void error(LogTags tag, const char *msg, ...);
    static void error_tag(const char *tag, const char *msg, ...);
    static void fatal(const char *msg, ...);
    static void fatal(LogTags tag, const char *msg, ...);
    static void fatal_tag(const char *tag, const char *msg, ...);
private:
    static void write( log_priority prio, va_list list, const char *msg, const char *tag = LOG_TAGS[ LOG_FACEBOOK ]);
    static const char *getTag(LogTags tag);
};

#endif // LOG_H
