/*
 * Mutex.h
 *
 *  Created on: Jul 4, 2016
 *      Author: ruebasargi
 */

#ifndef MUTEX_H_
#define MUTEX_H_

#include <memory>

/*
 * @class Mutex
 *
 * @brief Classic thread synchronization primitive.
 *
 * @details Mutex class is an OOP-style wrapper around low level platform API.
 * It keeps inside Eina_Lock instance and takes care about its lifetime. It's recommended
 * to use it in pair with MutexLocker.
 */

class Mutex {
public:
    Mutex();
    ~Mutex();
    void take();
    void release();
private:
    std::unique_ptr<struct _Eina_Lock> mMutex;
};

#endif /* MUTEX_H_ */
