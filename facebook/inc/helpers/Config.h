#ifndef CONFIG_H_
#define CONFIG_H_

#include <storage.h>
#include <string>

class Config {
private:
    std::string mUserId;
    std::string mAccessToken;
    std::string mSessionKey;
    std::string mSessionSecret;
    std::string mUserName;
    std::string mFBMediaPath;

    Config();
    Config(const Config&);
    Config& operator=(Config&);
    virtual ~Config();

    static char * pack_64bit(unsigned int num1, unsigned int num2, unsigned int num3);

public:
    static Config& GetInstance()
    {
        static Config instance;
        return instance;
    }

    void SetUserId(const char *userId);
    const std::string& GetUserId();

    void SetAccessToken(const char *accessToken);
    const std::string& GetAccessToken();

    void SetSessionKey(const char *sessionKey);
    const std::string& GetSessionKey();

    void SetUserName(const char *username);
    const std::string& GetUserName();

    void SetSessionSecret(const char* sessionSecret);
    const std::string& GetSessionSecret();

    void SetFBMediaPath(const char* path);
    const std::string& GetFBMediaPath();

    static const char * GetApplicationId();
    static const char * GetClientToken();

    static bool storage_cb(int storage_id, storage_type_e type, storage_state_e state, const char *path, void *user_data);

    bool IsLogin();
};

#endif /* CONFIG_H_ */
