/*
 * LoggedInUserData.h
 *
 *  Created on: Nov 11, 2015
 *      Author: ruibezruch
 */

#ifndef LOGGEDINUSERDATA_H_
#define LOGGEDINUSERDATA_H_

#include "UserProfileData.h"

class CoverPhoto;
class ProfilePictureSource;

class LoggedInUserData
{
private:
    LoggedInUserData();
    LoggedInUserData(const LoggedInUserData&);
    LoggedInUserData& operator=(LoggedInUserData&);
    virtual ~LoggedInUserData();
public:
    static LoggedInUserData& GetInstance()
    {
        static LoggedInUserData instance;
        return instance;
    }

    void SetData(const UserProfileData &object);
    void FreeData();

    const char *mId;
    const char *mName;
    const char *mFirstName;
    const char *mLastName;
    CoverPhoto *mCover;
    ProfilePictureSource *mPicture;

    UserProfileData *mUserProfileData;
};

#endif /* LOGGEDINUSERDATA_H_ */
