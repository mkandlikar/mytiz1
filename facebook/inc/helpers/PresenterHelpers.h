#ifndef PRESENTERHELPERS_H_
#define PRESENTERHELPERS_H_

#include "MultiTag.h"
#include "Tag.h"
#include <string>
#include <list>

namespace PresenterHelpers {
    enum Mode{
        eComposer,
        eFeed,
        eServer
    };

    /**
     * Gets string with tagged friends.
     * tagList contains tag offsets for UTF8 text. To get correct markup string
     * convert operation is executed part by part with adding tagged friend
     */
    std::string AddFriendTags(std::string text, std::list<Tag> tagList, Mode mode = eFeed);

    /**
     * Gets string with tagged links, emotions and hashtags
     */
    void AddExtraTags(std::string& composer);
    void ConvertEmojis(std::string& composer);

    /**
     * Gets string without "Continue reading..." anchor
     */
    std::string CuttedText(std::string& text);
    void RemoveTaggedFriends(std::string& composer);
    void PutFoundAnchorsInText(std::string& composer, int matchCount, char prefix, const std::string& color);
    std::string PutAnchorInText(const std::string &text, char key, const std::string& anchor, const std::string& color, unsigned int pos, bool firstReplace);
    std::list<MultiTag> GroupTags(const std::list<Tag>& tagList, const std::string& text);

    std::string CreateMutualFriendsFormat(int count, const char *textSize);
    std::string CreateFormatString(const char * format, const char *textSize);

} // namespace PresenterHelpers

#endif /* PRESENTERHELPERS_H_ */
