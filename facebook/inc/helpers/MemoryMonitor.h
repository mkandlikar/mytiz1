/*
 * MemoryMonitor.h
 *
 *  Created on: Mar 15, 2016
 *      Author: Dmitry.Shcherbakov@harman.com
 */

#ifndef MEMORYMONITOR_H_
#define MEMORYMONITOR_H_

class MemoryMonitor
{
    class FileWrapper
    {
        void Init( const char *path, unsigned long long sz, unsigned long long lastAccessedTime );
    public:
        FileWrapper( const char *path, unsigned long long sz, unsigned long long lastAccessedTime );
        FileWrapper( const FileWrapper &wrapper );
        ~FileWrapper();

        void DeleteFile();

        virtual bool operator < ( const FileWrapper &wripper ) const;
        virtual FileWrapper& operator = ( const FileWrapper &wrapper );
    private:
        const char *m_Path;
        unsigned long long m_FileSize;
        unsigned long long m_LastAccessedTime;
        friend class MemoryMonitor;
    };

    MemoryMonitor();

    static const unsigned int MEMORY_UPPER_BOUND_MBS;
    static const unsigned int MEMORY_TO_DELETE_MBS;

public:

    typedef enum {
        IMAGE_PATH,
        VIDEO_PATH,

        LAST_PATH
    } TrustedPathTypes;

    static MemoryMonitor* GetInstance();
    virtual ~MemoryMonitor();

    void UtilizeLocalMemory();
};

#endif /* MEMORYMONITOR_H_ */
