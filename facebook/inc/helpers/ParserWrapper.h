#ifndef PARSERWRAPPER_H_
#define PARSERWRAPPER_H_

#include "jsonutilities.h"

class ParserWrapper {
public:
    ParserWrapper( JsonParser *parser );
    ~ParserWrapper();
private:
    JsonParser *m_Parser;
};

#endif /* PARSERWRAPPER_H_ */
