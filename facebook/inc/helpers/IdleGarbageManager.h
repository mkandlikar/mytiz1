#ifndef IDLEGARBAGEMANAGER_H_
#define IDLEGARBAGEMANAGER_H_

#include "TrackItemsProxy.h"

class IdleGarbageManager {
    IdleGarbageManager();
public:

    typedef enum {
        ACTION_AND_DATA_ITEM,
        EVAS_ITEM
    } ItemType;

    typedef struct {
        ItemType type;
        void *data;
    } Item;

    virtual ~IdleGarbageManager();
    static IdleGarbageManager* GetInstance();

    void AppendItemsStack( Utils::DoubleSidedStack *itemsStack );
    unsigned int WaitingForDelete() const;

    static Eina_Bool idle_cb( void *data );
private:
    bool m_IsIdleInProgress;
    Utils::DoubleSidedStack *m_ItemsStack;
};

#endif /* IDLEGARBAGEMANAGER_H_ */
