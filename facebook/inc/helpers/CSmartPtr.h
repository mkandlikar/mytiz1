#ifndef CSMARTPTR_H_
#define CSMARTPTR_H_

#include <memory>
#include <stdlib.h>

class CObjectDeleter {
public:
    CObjectDeleter() {
    }
    CObjectDeleter(const CObjectDeleter& other){
    }
    ~CObjectDeleter() {
    }
    void operator=(const CObjectDeleter& other){
    }
    void operator()(void* CObject) {
        free(CObject);
    }
};

template<typename T>
class c_unique_ptr: public std::unique_ptr<T, CObjectDeleter> {
public:
    c_unique_ptr(T* ptr): std::unique_ptr<T, CObjectDeleter>(ptr, CObjectDeleter()){}
    c_unique_ptr(c_unique_ptr<T>&& other): std::unique_ptr<T, CObjectDeleter>(other){}
};

template<typename T>
class c_shared_ptr: public std::shared_ptr<T> {
public:
    c_shared_ptr(T* ptr): std::shared_ptr<T>(ptr, CObjectDeleter()) {}
    c_shared_ptr(const c_shared_ptr<T>& other): std::shared_ptr<T>(other) {}
    c_shared_ptr(c_shared_ptr<T>&& other): std::shared_ptr<T>(other) {}
    c_shared_ptr(c_unique_ptr<T>&& other): std::shared_ptr<T>(other) {}
    c_shared_ptr(const std::weak_ptr<T>&& other): std::shared_ptr<T>(other) {}
};

#endif /* CSMARTPTR_H_ */
