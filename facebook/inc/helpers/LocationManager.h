#ifndef LOCATIONMANAGER_H_
#define LOCATIONMANAGER_H_

#include <locations.h>
#include <Ecore.h>

class GraphRequest;

class LocationManager {
public:
    enum LocationManagerState{
        EStateNone,
        EStateSettingOff,
        EStateStopped,
        EStateStarting,
        EStateStarted,
        EStateStopping
    };
    enum LocationManagerError{
        EErrorNone,
        EErrorNotReady,
        EErrorNoDataAvailable,
        EErrorSettingOff
    };

    LocationManager();
    virtual ~LocationManager();
    void Start();
    void Stop();

    LocationManagerError GetLocation(double *latitude, double *longitude);
private:
    static void state_changed_cb(location_service_state_e state, void *user_data);

    static void on_set_last_location_completed(void* object, char* response, int code);
    void SendLastLocation();

private:
    location_manager_h mManager;
    LocationManagerState mState;
    GraphRequest *mReqSendLastLocation;
};


#endif /*LOCATIONMANAGER_H_*/
