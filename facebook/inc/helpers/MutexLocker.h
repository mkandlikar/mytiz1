/*
 * MutexLocker.h
 *
 * Created on: Aug 24, 2015
 * Author: Dmitry.Shcherbakov@SymphonyTeleca.com
 */

#ifndef MUTEXLOCKER_H_
#define MUTEXLOCKER_H_

/**
 * @class MutexLocker
 *
 * @brief MutexLocker is an object that captures lock in the beginning of it's lifetime and releases it in the end.
 *
 * @details MutexLocker guarantees that capturing and releasing calls over mutex will be paired. It supports both
 * instances of Mutex class and low level Eina_Lock mutex handles.
 */

class MutexLocker
{
public:
    MutexLocker(struct _Eina_Lock* mutex);
    MutexLocker(class Mutex* mutex);
    virtual ~MutexLocker();
private:
    struct _Eina_Lock* mMutexHandle;
    class Mutex* mMutexObject;
};

#endif /* MUTEXLOCKER_H_ */
