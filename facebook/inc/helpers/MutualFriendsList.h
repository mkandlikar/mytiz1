#ifndef DATA_MUTUALFRIENDSLIST_H_
#define DATA_MUTUALFRIENDSLIST_H_

#include <Eina.h>

class MutualFriendsList {
public:
    static MutualFriendsList * GetInstance();
    ~MutualFriendsList();

    void AddUsersMutualFriends(const char *userId, int mutualFriendsCount);
    int GetUsersMutualFriends(const char *userId);
    void DeleteUsersMutualFriends(const char *userId);
    void DeleteAllUsersMutualFriends();

private:
    MutualFriendsList();

    class UsersMutualFriends {
    public:
        UsersMutualFriends(const char *userId, int mutualFriendsCount);
        ~UsersMutualFriends();

        inline char *GetUserId() { return mUserId; }
        inline int GetMutualFriendsCount() { return mMutualFriendsCount; }
        inline void SetMutualFriendsCount(int mutualFriendsCount) { mMutualFriendsCount = mutualFriendsCount; }
    private:
        char *mUserId;
        int mMutualFriendsCount;
    };

    Eina_List *mUsersMutualFriendsList;
};

#endif /* DATA_MUTUALFRIENDSLIST_H_ */
