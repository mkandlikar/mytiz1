#ifndef CONNECTIVITYMANAGER_H_
#define CONNECTIVITYMANAGER_H_


#include <string>
#include <net_connection.h>

/*
 * A singleton class to monitor Wifi/Cellular network connectivity.
 */
class ConnectivityManager {
    private:
        connection_h mConnectionHandle;
        connection_type_e mSystemConnectivity;
        std::string mClassName;
        bool mInitialized;
        bool mIsWifiConnected;
        bool mIsCellularConnected;
        bool mIsBTConnected;

    private:
        ConnectivityManager();
        ConnectivityManager(ConnectivityManager const&);
        ConnectivityManager& operator=(ConnectivityManager const&);
        virtual ~ConnectivityManager();

        void UpdateConnectivity();
        void UpdateWifiConnectivity();
        void UpdateCellularConnectivity();
        void UpdateBTConnectivity();

    public:
        static ConnectivityManager& Singleton() {
            static ConnectivityManager instance;
            return instance;
        }

        void Init();
        bool IsConnected();

    protected:
        static void ConnectionChangedCb(connection_type_e ConnectionType, void *Data);
};

#endif /* CONNECTIVITYMANAGER_H_ */
