#ifndef APPEVENTS_H_
#define APPEVENTS_H_

#include <functional>
#include <map>
#include <memory>
#include <tizen_type.h>

/**
 * @enum AppEventId
 * @brief AppEventId defines notifications that can be sent via AppEvents
 */
enum AppEventId {
#ifdef _DEBUG
    eEVENT_FIRST, // actually not an event, first element for sanity checks
#endif
     eUNDEFINED_EVENT
    ,eLIKE_COMMENT_EVENT
    ,eDATA_IS_READY_EVENT
    ,eREQUEST_COMPLETED_EVENT
    ,eREQUEST_CONTINUE_EVENT
    // This event is a good approach in case when you need to inform about several
    // actions: erase and update data. Only this one event MUST be used from the not main
    // thread, instead of manual use of Erase() and Update() method separately.
    ,eREQUEST_FOR_ERASE_EVENT
    ,ePAUSE_EVENT
    ,eRESUME_EVENT
    ,eFOCUSED_EVENT
    ,eUNFOCUSED_EVENT
    ,eALBUM_CREATED_EVENT
    ,eALBUM_DELETED_EVENT
    ,eALBUM_RENAMED_EVENT
    ,eALBUM_NEW_PHOTO_ADDED_EVENT
    ,eALBUM_NEW_PHOTOS_ADDITION_COMPLETED_EVENT
    ,eDATA_CHANGES_EVENT
    ,ePOST_MESSAGE_EVENT
    ,eOPERATION_ADDED_EVENT
    ,eOPERATION_REMOVED_EVENT
    ,eOPERATION_START_EVENT
    ,eOPERATION_COMPLETE_EVENT
    ,eOPERATION_CANCELED_EVENT
    ,eOPERATION_STEP_PROGRESS_EVENT
    ,eOPERATION_STEP_COMPLETE_EVENT
    ,eOPERATION_RETRY_SEND_POST_EVENT
    ,eREDRAW_EVENT
    ,eGRAPHREQUEST_PROGRESS
    ,ePYMK_ITEM_DELETE_EVENT
    ,eFR_STATUS_CHANGE_EVENT
    ,eFR_ITEM_DELETE_EVENT    /* event data is <User*> */
    ,eFR_ITEM_UPDATE_EVENT
    ,eFRIEND_REQUEST_CONFIRM_DELETE_ERROR
    ,ePHOTO_POST_UPLOAD_ERROR_EVENT
    ,eVIDEO_POST_UPLOAD_ERROR_EVENT
    ,ePOST_RELOAD_EVENT
    ,eEVENT_PROFILE_UPDATE_STATS_BOX
    ,eINTERNET_CONNECTION_CHANGED
    ,eCAMERA_ROLL_SELECT_EVENT
    ,ePOST_SENDING_PHOTO_EVENT
    ,eUPDATED_COVER_PICTURE
    ,eUPDATED_PROFILE_PICTURE
    ,eDELETED_USER_PICTURE
    ,eUSER_PROFILE_ID_CHANGED
    ,eNOTIFICATION_REDRAW
    ,eNOTIFICATION_UPDATE_BADGE
    ,eACCOUNT_CHANGED
    ,eOWN_PROFILE_DATA_UPDATED
    ,eDATA_IS_READY
    ,eACCOUNT_DATA_IS_READY
    ,eMAINSCREEN_TAB_CHANGED
    ,eVIDEOPLAYER_PAUSE_EVENT
    ,eVIDEOPLAYER_RESUME_EVENT
    // events for "Unexpected scroll to top" issue
    ,eCONFIRMATION_DIALOG_SHOW_EVENT
    ,eERROR_DIALOG_SHOW_EVENT
    ,eSELECT_ALBUM_SCREEN_SHOW_EVENT
    ,eLANGUAGE_CHANGED
    ,eOPERATION_IN_ERROR_STATE_EVENT
    ,eFR_REQUEST_FOR_USER_IN_FRIENDS
    ,eFRIEND_REQUEST_CONFIRMED
    ,eDELETE_BLOCKED_FRIEND_FROM_MY_LIST    /* event data is <Friend*> */
    ,eDELETE_BLOCKED_FRIEND_FROM_OTHER_LIST
    ,eBLOCK_FRIEND_ERROR
    ,eREMOVE_TOP_BLOCKED_SCREEN
    ,eFRIEND_UNFRIENDED
    ,eUPDATE_UNFRIENDED_USER_STATUS
    ,eUNFRIEND_FRIEND_ERROR
    ,ePOST_UPDATE_ABORTED
    ,ePOST_NOT_AVAILABLE
    ,eUNFOLLOW_COMPLETED
    ,eFOLLOW_COMPLETED
    ,eADD_NEW_FRIEND_REQUESTS
    ,eUPDATE_FIND_FRIENDS_REQUESTS_COUNT
    ,eUPDATE_POST_LIKES_SUMMARY
    ,eREPLACE_GRAPH_OBJ_FOR_EDITED_COMMENT
    ,eREPLIES_LIST_WAS_MODIFIED
    ,eCOMMENT_WAS_DELETED
    ,eCOMMENT_WAS_RELOAD
    ,ePHOTO_COMMENTS_LIST_WAS_MODIFIED
    ,ePHOTO_CAPTION_WAS_CHANGED
    ,ePOST_DELETED
    ,ePOST_DELETION_CONFIRMED
    ,eADD_FRIEND_BTN_CLICKED
    ,eCANCEL_FRIEND_REQUEST_BTN_CLICKED
    ,eDELETE_FRIEND_REQUEST_BTN_CLICKED
    ,eCONFIRM_FRIEND_REQUEST_BTN_CLICKED
    ,eCONFIG_UPDATE_COMPLETED_EVENT
    ,eCONFIG_UPDATE_ERROR_EVENT

#ifdef _DEBUG
    ,eEVENT_LAST // actually not an event, last element for sanity checks
#endif
};

#define IS_OPERATION_EVENT(evt) ((evt >= eOPERATION_ADDED_EVENT) && (evt <= eOPERATION_STEP_COMPLETE_EVENT))

/**
 * @class Subscriber
 * @brief Base for classes which receive notifications from AppEvents
 * @details Inherit this class and override update() method to be able to receive notifiations from AppEvents
 */

class Subscriber {
public:
    Subscriber(){}
    virtual ~Subscriber();
    virtual void Update(AppEventId eventId = eUNDEFINED_EVENT, void * data = NULL) = 0;
    virtual void UpdateErase() {}
};

/**
 * @class AppEvents
 * @brief AppEvents is a notification router implemented as singleton.
 * @details AppEvents routes notifications and delivers them to classes extending Subscriber class.
 * Use methods subscribe/unsubscribe to start/stop receiving notifications. Use method notify to send notification.
 */
class AppEvents {
    AppEvents();
    ~AppEvents();
    void StopSendingNotifications();
public:
    static AppEvents& Get();
    void Subscribe(AppEventId eventId, Subscriber* subscriber);
    void Unsubscribe(AppEventId eventId, Subscriber* subscriber );
    void UnsubscribeFromAll(Subscriber* subscriber);
    void Notify(AppEventId eventId, void* data = nullptr);
private:
    static void DeleteAppEvents(AppEvents* appEvents);
    typedef std::multimap<AppEventId, Subscriber*> SubscriptionMapping;
    typedef std::pair<AppEventId, Subscriber*> Subscription;
    typedef std::pair<SubscriptionMapping::iterator, SubscriptionMapping::iterator> Subscriptions;
    SubscriptionMapping mSubscriptions;
    friend class std::unique_ptr<AppEvents, std::function<void(AppEvents*)>>;
    friend class Application;

private:
    bool mSendNotifications;
};

#endif /* APPEVENTS_H_ */
