/*
 * DataUploader.h
 *
 *  Created on: September 30, 2015
 *      Author: Dmitry.Shcherbakov@SymphonyTeleca.com
 */


#ifndef DATAUPLOADER_H_
#define DATAUPLOADER_H_

#include "AppEvents.h"

class AbstractDataProvider;

typedef void ( *uploading_done_cb )( void *parent );
typedef void ( *uploading_error_cb )( void *parent, int curlErrorCode );

class DataUploader : public Subscriber
{
public:
    DataUploader( AbstractDataProvider *provider, uploading_done_cb done_cb, void *parent );
    virtual ~DataUploader();

    void SetErrorHandler( uploading_error_cb error_cb );
    void StartUploading();
    void Uploaded();

    virtual void Update( AppEventId eventId = eUNDEFINED_EVENT, void * data = NULL );
    virtual void UpdateErase();

    inline bool IsUploadCompleted() { return m_IsUploadCompleted; }

private:
    void RequestData();

    AbstractDataProvider *m_Provider;
    void *m_Parent;
    unsigned int m_ItemsCount;
    unsigned int m_DataProviderItemsLimit;
    unsigned int m_RequestedPage;
    uploading_done_cb m_UploadingDone_cb;
    uploading_error_cb m_ErrorHandler;
    bool m_IsUploadCompleted;
};

#endif /* DATAUPLOADER_H_ */
