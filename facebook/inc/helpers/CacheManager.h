#ifndef CACHEMANAGER_H_
#define CACHEMANAGER_H_

#include <app.h>
#include <sqlite3.h>
#include <Elementary.h>

#include "FbRespondGetFriends.h"
#include "FbRespondGetFeed.h"
#include "FbRespondGetPeopleYouMayKnow.h"
#include "Post.h"

#define BUFFEER 512

class FriendsScreen;
class Friend;

typedef struct account_data_struct {
    //out
    char * userId;
    char * accessToken;
    char * sessionKey;
    char * sessionSecret;
} account_data;

class ActionAndData;
typedef struct notification_data_struct {
    //out
    char * ntfId;
    char * ntfMsg;
    char * ntfUserDisplayName;
    char * ntfImagePath;
    double ntfRcvdTime;
    char * ntfType;
    char * ntfGraphObj;
    ActionAndData *ntfActNData;
} notification_data;

typedef struct loggedin_user_data_struct {
    //out
    int rowCount;
    char *prevUserId;
} loggedin_user_data;

enum EDBVersion {
   EOldVersion,
   Ev_005,
   Ev_006,

   ELastVersion
};

class CacheManager {
private:
    CacheManager();
    CacheManager(const CacheManager&);
    CacheManager& operator=(CacheManager&);
    virtual ~CacheManager();

    const std::string *getPostTableFields();
    std::string getPostValues(Post *postData);

    std::string tagsToJSON(const std::list<Tag>& tagsList);
    static Eina_List *AppendPhotoToList(Eina_List *photoList, char **columnName);
    char* mDBValue;
    bool mIsDataUpdated;
    bool UpgradeDB(unsigned int currentVersion);
public:
    static CacheManager& GetInstance()
    {
        static CacheManager instance;
        return instance;
    }
    unsigned int GetEDBVersionFromString(std::string strVer);
    char* GetPostIdByPostAutoId( int postAutoId );
    int GetPostAutoIdByPostId( const char *postId );
    int GetSession();
    bool Init();
    std::string GetSettingsValue(const char *name);
    bool SetSettingsValue(const char *name, const char *value);
    std::string GetCurrentDBVersion();
    void DoMigration();
    bool InitDatabase();
    void CloseDatabase();
    int  CreateTable(const std::string query);
    bool CreateTables();
    void DeleteDatabase();
    void DeletePost(const char* post_id);
    bool isPostUpdated(const char* post_id, const char* time);
    bool UpdateFeedDataById(const char* post_id);
    bool UpdateFeedDataByPost(Post* postData);
    bool UpdateCommentsCountOfPostById(const char* post_id, int commentsCount);
    void SetDBValue(const char * value);
    inline const char* GetDBValue(){return mDBValue;};
    int CreateOrOpenDB(const char * path);

    static int log_callback(void *pArg, int argc, char **argv,
            char **columnName);

    /**
     * Finds if a friend request has been sent to the profile ID
     */
    bool SelectFromFriendsReqTable( const char *id);

    /**
     * Stores the profile id to whome a friend request has been sent
     */
    bool InsertToFriendsReqTable(const char* id, const char* name);

    /**
     * Delete a outgoing friend request
     */
    bool DeletFromFriendsReqTable( const char *id);

    bool AddFriend(Friend *friendData);
    bool DeleteFriend( const char *id);

    /**
     * Set Friend data to database
     * @param data Pointer to Friend respond
     */
    bool SetFriendsData(FbRespondGetFriends *friendsResponse);

    /**
     * Select data for Friends screen from database
     * @param recordsNumber The number of records
     */
    Eina_List* SelectFriendsData();

    Eina_List* SelectPostOrderData(int begin, int end);
    Eina_List* SelectPostOrderDataBySessionId(int session);
    /**
     * Transfer data to screen by calling setData() function of given screen object.
     * @param pArg Pointer to friends screen
     * @param argc The number of columns in the query result
     * @param argv An array of pointers to strings where each string is a single column of the result for that record.
     * @param columnName An array of pointers to strings of column names.
     */
    static int select_friends_callback(void *pArg, int argc, char **argv, char **columnName);

    static int select_post_order_callback(void *pArg, int argc, char **argv, char **columnName);

    static int select_post_order_by_session_callback(void *pArg, int argc, char **argv, char **columnName);

    bool SetFriendableUserData(FbRespondGetPeopleYouMayKnow *friendsResponse);
    Eina_List* SelectFriendableUserData(int recordsNumber);
    static int select_friendableuser_callback(void *pArg, int argc, char **argv, char **columnName);

    /**
     * Set Friend data to database
     * @param data Pointer to Feed response
     */
    bool SetFeedData(FbRespondGetFeed * feedResponse);

    /**
     * Select data for Feed screen from database
     * @param recordsNumber The number of records
     */
    Eina_List* SelectFeedData(int recordsNumber);

    /**
     * Select data for Feed screen from database
     * @param post_id The id of records
     */
    Post* SelectFeedDataById(const char* post_id);

    Post* SelectFeedDataByAutoId(int autoId);

    Post* SelectFeedDataByQuery(const char *queryPost);

    /**
     * Transfer data to screen by calling setData() function of given screen object.
     * @param pArg Pointer to feed screen
     * @param argc The number of columns in the query result
     * @param argv An array of pointers to strings where each string is a single column of the result for that record.
     * @param columnName An array of pointers to strings of column names.
     */

    /**
     * Store profile post data to database
     * @param data Pointer to Feed response
     */
    bool SetProfileFeedData(FbRespondGetFeed * feedResponse);

    static int select_feed_callback(void *pArg, int argc, char **argv,
            char **columnName);
    static int select_feed_callback_by_id(void *pArg, int argc, char **argv,
            char **columnName);
    bool SetPhotoTable(Eina_List *photoList, const char *id, int autoPostId);
    bool CreateSessionData(const char * create_date);
    bool UpdateSessionData(const char * end_date, int session);
    bool CreatePostPositons(int position, int autoPostId, int autoSessionId);
    static int select_photo_callback(void *pArg, int argc, char **argv,
                char **columnName);
    static int select_images_callback(void *pArg, int argc, char **argv,
                char **columnName);
    static int select_get_data_callback(void *pArg, int argc, char **argv, char **columnName);

    /**
     * Set Account data to database
     * @param userName userName used to signIn
     * @param accessToken access token received through Login response
     * @param sessionKey session key received through Login response
     * @param sessionSecret session Secret received through Login response
     */
    bool UpdateAccountData();

    /**
     * Select data from Account database
     * @param in  userName userName used to fetch the record
     * @param out accData account data structure
     */
    bool SelectAccountData(const char* userName, account_data * accData);

    /**
     * Callback for each row of the Accounts Table
     * @param pArg Pointer to Account data structure
     * @param argc The number of columns in the query result
     * @param argv An array of pointers to strings where each string is a single column of the result for that record.
     * @param columnName An array of pointers to strings of column names.
     */
    static int select_account_callback(void *pArg, int argc, char **argv,
            char **columnName);

    /**
     * Delete Account data from database
     * @param in userName userName used to signIn
     */
    bool DeleteAccountData(const char* userName);

    /**
     * Set LoggedIn User data to database
     * @param in userId userId received through FB Login Response
     */
    bool SetLoggedInUserData(const char * userId);

    /**
     * Select data from LoggedInUser database
     * @param out prevUserData previous loggedin user data structure
     */
    bool SelectLoggedInUser(loggedin_user_data *prevUserData);

    /**
     * Callback for each row of the LoggedInUser Table
     * @param pArg Pointer to current user Id logged in the app
     * @param argc The number of columns in the query result
     * @param argv An array of pointers to strings where each string is a single column of the result for that record.
     * @param columnName An array of pointers to strings of column names.
     */
    static int select_loggedinuser_callback(void *pArg, int argc, char **argv,
            char **columnName);

    /**
     * Delete LoggedInUser data from database
     */
    bool DeleteLoggedInUserData();

protected:
    void CascadeDeletePhotos(const char* post_id);
    void DeletePhotosbyPostId(const char* post_id);
    static int sort_cb(const void *data1, const void *data2);

private:
    sqlite3 *mDB;

    const static double MIN_POST_UPDATED_TIME_DELTA;
};

struct PostsPhotoData {
    Eina_List *mPostsList;
    Post *mLastPost;
    Eina_List *mLastPhotoList;

    PostsPhotoData() { mPostsList = NULL; mLastPost = NULL; mLastPhotoList = NULL; }
    ~PostsPhotoData() {  }
};

struct Order {
    int mPosition;
    int mPostAutoId;
    int mSessionAutoId;

    Order() {mPosition = 0; mPostAutoId = 0; mSessionAutoId = 0;};
    ~Order();
};

#endif /* CACHEMANAGER_H_ */
