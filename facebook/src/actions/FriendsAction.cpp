#include "CacheManager.h"
#include "Common.h"
#include "ConnectivityManager.h"
#include "FacebookSession.h"
#include "FbRespondBase.h"
#include "FbRespondGetPeopleYouMayKnow.h"
#include "FoundItem.h"
#include "FriendOperation.h"
#include "FriendsAction.h"
#include "FriendsTab.h"
#include "FriendRequestsBatchProvider.h"
#include "Log.h"
#include "OperationManager.h"
#include "OwnFriendsProvider.h"
#include "PeopleYouMayKnowCarousel.h"
#include "SelectUserScreen.h"
#include "TrackItemsProxyAdapter.h"
#include "UserDetailInfo.h"
#include "UserFriendsScreen.h"
#include "Utils.h"
#include "WidgetFactory.h"

FriendsAction::FriendsAction(ScreenBase *screen)
{
    // TODO Auto-generated constructor stub
    mScreen = screen;

    mAddFriendRequest = NULL;
    mCancelFriendRequest = NULL;
}

FriendsAction::~FriendsAction()
{
    FacebookSession::GetInstance()->ReleaseGraphRequest(mAddFriendRequest);
    FacebookSession::GetInstance()->ReleaseGraphRequest(mCancelFriendRequest);
}

void FriendsAction::PymkBtnClicked(ActionAndData *actionData)
{
    assert(actionData);
    Log::info(LOG_FACEBOOK_FRIENDS_ACTION, "FriendsAction::PymkBtnClicked");
    if (!ConnectivityManager::Singleton().IsConnected()) {
        assert(actionData->mAction);
        assert(actionData->mAction->getScreen());
        Utils::ShowToast(actionData->mAction->getScreen()->getMainLayout(), i18n_get_text("IDS_GEN_NETWORK_ERROR"));
    } else {

        Person *person = dynamic_cast<Person*>(actionData->mData);
        if(!person || !person->GetTracker()) {
            Log::error(LOG_FACEBOOK_FRIENDS_ACTION, "FriendsAction::PymkBtnClicked->Wrong data type");
            ShowErrorDialog(532);
            return;
        }

        FbRespondGetPeopleYouMayKnow::FriendableUser *user = nullptr;
        if (person->GetGraphObjectType() == GraphObject::GOT_USERPROFILEDETAIL) {
            // we are in pymk carousel and should update pymk list below
            user = FriendRequestsBatchProvider::GetInstance()->GetPymkDataById(person->GetId());
        }

        if (person->mFriendRequestStatus == eUSER_REQUEST_SENT) {
            person->mFriendRequestStatus = eUSER_REQUEST_CANCELLED;
            person->mFriendshipStatus = Person::eCAN_REQUEST;
            if (user) {
                user->mFriendRequestStatus = person->mFriendRequestStatus;
                user->mFriendshipStatus = person->mFriendshipStatus;
            }// additional copy of statuses if we are in carousel , we should update pymk list below
            AppEvents::Get().Notify(eCANCEL_FRIEND_REQUEST_BTN_CLICKED, const_cast<char*>(person->GetId()));// update UI
        } else if (person->mFriendRequestStatus == eUSER_REQUEST_UNKNOWN ||
                   person->mFriendRequestStatus == eUSER_REQUEST_CANCELLED ) { // these checks looks logical
            person->mFriendRequestStatus = eUSER_REQUEST_SENT;
            person->mFriendshipStatus = Person::eOUTGOING_REQUEST;
            if (user) {
                user->mFriendRequestStatus = person->mFriendRequestStatus;
                user->mFriendshipStatus = person->mFriendshipStatus;
            }
            AppEvents::Get().Notify(eADD_FRIEND_BTN_CLICKED, const_cast<char*>(person->GetId()));// update UI
        }
        person->GetTracker()->RestartRequestTimer();
    }
}

void FriendsAction::AddFriendBtnClicked(ActionAndData *actionData)
{
    if (!ConnectivityManager::Singleton().IsConnected()) {
        Utils::ShowToast(actionData->mAction->getScreen()->getMainLayout(), i18n_get_text("IDS_GEN_NETWORK_ERROR"));
    } else {
        assert(actionData);
        Person *person = dynamic_cast<Person*>(actionData->mData);
        if(!person || !person->GetTracker()) {
            Log::error(LOG_FACEBOOK_FRIENDS_ACTION, "FriendsAction::AddFriendBtnClicked->Wrong data type");
            return;
        }
        AppEvents::Get().Notify(eADD_FRIEND_BTN_CLICKED, const_cast<char*>(person->GetId()));// update UI
        person->GetTracker()->RestartRequestTimer();
    }
}

void FriendsAction::on_add_friend_completed(void* object, char* respond, int code)
{
    Log::debug(LOG_FACEBOOK_FRIENDS_ACTION, "FriendsAction::on_add_friend_completed");
    Person *person = static_cast<Person*>(object);
    if(!person) {
        Log::error(LOG_FACEBOOK_FRIENDS_ACTION, "FriendsAction::on_add_friend_completed->Wrong data type");
        return;
    }

    if (code == CURLE_OK) {
        Log::info(LOG_FACEBOOK_FRIENDS_ACTION, "FriendsAction::on_add_friend_completed->response: %s", respond);
        FbRespondBase * confirmRespond = FbRespondBase::createFromJson(respond);
        if (!confirmRespond) {
            Log::debug(LOG_FACEBOOK_FRIENDS_ACTION, "FriendsAction::on_add_friend_completed->Error parsing http respond");
        } else {
            if (confirmRespond->mError) {
                Log::debug("FriendsAction::on_add_friend_completed->Error = %s", confirmRespond->mError->mMessage);
                if(confirmRespond->mError->mCode == 522) {
                    AppEvents::Get().Notify(eFRIEND_REQUEST_CONFIRMED, const_cast<char*>(person->GetId()));
                } else if(confirmRespond->mError->mCode == 520) {
                    Log::debug("FriendsAction::on_add_friend_completed->There is already a pending request");
                } else if(confirmRespond->mError->mCode == 528) {
                    AppEvents::Get().Notify(eCANCEL_FRIEND_REQUEST_BTN_CLICKED, const_cast<char*>(person->GetId()));
                    ShowErrorDialog(528);
                } else if (confirmRespond->mSuccess) {
                    Log::debug("FriendsAction::on_add_friend_completed->Successfully Added");
                }
            }
        }
        delete confirmRespond;
    }
    free(respond);
}

void FriendsAction::CancelFriendBtnClicked(ActionAndData *actionData)
{
    if (!ConnectivityManager::Singleton().IsConnected()) {
        Utils::ShowToast(actionData->mAction->getScreen()->getMainLayout(), i18n_get_text("IDS_GEN_NETWORK_ERROR"));
    } else {
        assert(actionData);
        Person *person = dynamic_cast<Person*>(actionData->mData);
        if(!person || !person->GetTracker()) {
            Log::error(LOG_FACEBOOK_FRIENDS_ACTION, "FriendsAction::CancelFriendBtnClicked->Wrong data type");
            return;
        }
        AppEvents::Get().Notify(eCANCEL_FRIEND_REQUEST_BTN_CLICKED, const_cast<char*>(person->GetId()));// update UI
        person->GetTracker()->RestartRequestTimer();
    }
}

void FriendsAction::on_cancel_friend_completed(void* object, char* respond, int code)
{
    Log::debug(LOG_FACEBOOK_FRIENDS_ACTION, "FriendsAction::on_cancel_friend_completed");
    Person *person = static_cast<Person*>(object);
    if(!person) {
        Log::error(LOG_FACEBOOK_FRIENDS_ACTION, "FriendsAction::on_cancel_friend_completed->Wrong data type");
        return;
    }

    if (code == CURLE_OK) {
        FbRespondBaseRestApi * cancelRespond = FbRespondBaseRestApi::createFromJson(respond);
        if (!cancelRespond) {
            Log::debug(LOG_FACEBOOK_FRIENDS_ACTION, "FriendsAction::on_cancel_friend_completed->Error parsing http respond");
        } else {
            Log::info(LOG_FACEBOOK_FRIENDS_ACTION, "FriendsAction::on_cancel_friend_completed->response: %s", respond);
            if (cancelRespond->mErrorMessage) {
                Log::debug("FriendsAction::on_cancel_friend_completed->Error = %s", cancelRespond->mErrorMessage);
                if (cancelRespond->mErrorCode == 100) {//  It seems that 100 means that the request is already confirmed and can't be canceled.
                    AppEvents::Get().Notify(eFRIEND_REQUEST_CONFIRMED, const_cast<char*>(person->GetId()));
                } else if (cancelRespond->mErrorCode == 536) { //usually 536 comes if pending request has been cancelled already
                    Log::debug("FriendsAction::on_cancel_friend_completed->Error 536 considered as success cancel");
                } else if (cancelRespond->mErrorCode == 2) {
                    Log::debug("FriendsAction::on_cancel_friend_completed->Error 2 considered as success cancel");
                }
            }
        }
        delete cancelRespond;
    }
}

void FriendsAction::ConfirmOrDeleteFriendRequest(ActionAndData *actionData, bool isConfirmed)
{
    Log::info(LOG_FACEBOOK_FRIENDS_ACTION, "FriendsAction::ConfirmOrDeleteFriendRequest");
    assert(actionData);
    if (!ConnectivityManager::Singleton().IsConnected()) {
        assert(actionData->mAction);
        assert(actionData->mAction->getScreen());
        Utils::ShowToast(actionData->mAction->getScreen()->getMainLayout(), i18n_get_text("IDS_GEN_NETWORK_ERROR"));
    } else {
        assert(actionData->mData);

        switch (actionData->mData->GetGraphObjectType()) {
        case GraphObject::GOT_USERPROFILEDETAIL: {//update data for FR item in carousel
                Log::info("FriendsAction::ConfirmOrDeleteFriendRequest->try to confirm/delete request from FR Carousel");
                UserDetailInfo* userDetailInfo = dynamic_cast<UserDetailInfo*>(actionData->mData);
                if (userDetailInfo) {
                    userDetailInfo->mFriendRequestStatus = isConfirmed ? eUSER_REQUEST_CONFIRMED : eUSER_REQUEST_DELETED;
                    AppEvents::Get().Notify(eFR_STATUS_CHANGE_EVENT, userDetailInfo);
                }
            }
            break;
        case GraphObject::GOT_FRIEND_REQUEST:
            Log::info("FriendsAction::ConfirmOrDeleteFriendRequest->try to confirm/delete request from FR Tab");
            break;
        default:
            ShowErrorDialog(0);
            return;
        }

        User *user = FriendRequestsBatchProvider::GetInstance()->GetFrDataById(actionData->mData->GetId());
        if (user && user->mFriendRequestStatus == eUSER_REQUEST_UNKNOWN) {
            //update data for FR item in MainMenu->FriendsRequests tab
            Log::info("FriendsAction::ConfirmOrDeleteFriendRequest->update item in FriendsReqest tab");
            user->mFriendRequestStatus = isConfirmed ? eUSER_REQUEST_CONFIRMED : eUSER_REQUEST_DELETED;
            AppEvents::Get().Notify(eFR_STATUS_CHANGE_EVENT, user);
        }
        Operation::OperationType opType = isConfirmed ? Operation::OT_ConfirmFriend : Operation::OT_NotConfirmFriend;
        OperationManager::GetInstance()->AddOperation(MakeSptr<FriendOperation>(opType, actionData->mData->GetId()));
    }
}

//is not used now, but approach could be useful in future
void FriendsAction::HideFriendsListItem(GraphObject *data, ScreenBase *screen)
{
    assert(screen);

    int screenId = screen->GetScreenId();
    if (screenId == ScreenBase::SID_FRIENDS_TAB ||
        screenId == ScreenBase::SID_USER_PROFILE_FRIENDS_TAB) {
        Friend *friends = static_cast<Friend*>(data);
        TrackItemsProxyAdapter *tipAdapter = dynamic_cast<TrackItemsProxyAdapter*>(screen);
        if (tipAdapter) {
            tipAdapter->DestroyUIItem(friends);
        }
    } else {
        User *user = static_cast<User*>(data);
        if (screenId == ScreenBase::SID_WHO_LIKED) {
            TrackItemsProxyAdapter *tipAdapter = dynamic_cast<TrackItemsProxyAdapter*>(screen);
            if (tipAdapter) {
                tipAdapter->DestroyUIItem(user);
            }
        } else if (screenId == ScreenBase::SID_SELECT_USER_SCREEN) {
            TrackItemsProxy *tip = dynamic_cast<TrackItemsProxy*>(screen);
            if (tip) {
                tip->DestroyItem(user, SelectUserScreen::go_item_comparator);
                tip->UpdateItemsWindow();
            }
        }
    }
}

void FriendsAction::DoUnfriend(ActionAndData *actionData)
{
    Log::info(LOG_FACEBOOK_FRIENDS_ACTION, "FriendsAction::DoUnfriend");
    assert(actionData);
    if (!ConnectivityManager::Singleton().IsConnected()) {
        assert(actionData->mAction);
        assert(actionData->mAction->getScreen());
        Utils::ShowToast(actionData->mAction->getScreen()->getMainLayout(), i18n_get_text("IDS_GEN_NETWORK_ERROR"));
    } else {
        assert(actionData->mData);
        FriendRequestsBatchProvider::GetInstance()->ResetRequestTimer();
        switch (actionData->mData->GetGraphObjectType()) {
        case GraphObject::GOT_FRIEND: {
            Friend *friends = static_cast<Friend*>(actionData->mData);
            if (friends) {
                OperationManager::GetInstance()->AddOperation(MakeSptr<FriendOperation>(Operation::OT_Unfriend, friends->GetId()));

                if (friends->mFriendshipStatus != Person::eARE_FRIENDS) {
                    elm_object_signal_emit(actionData->mActionObj, "hide.set", "friend");
                    elm_object_signal_emit(actionData->mActionObj, "show.set", "add");
                }
            }
            break;
        }
        case GraphObject::GOT_USER: {
            User *user = static_cast<User*>(actionData->mData);
            if (user) {
                OperationManager::GetInstance()->AddOperation(MakeSptr<FriendOperation>(Operation::OT_Unfriend, user->GetId()));

                if (user->mFriendshipStatus != Person::eARE_FRIENDS) {
                    elm_object_signal_emit(actionData->mActionObj, "hide.set", "friend");
                    elm_object_signal_emit(actionData->mActionObj, "show.set", "add");
                }
            }
            break;
        }
        case GraphObject::GOT_USERPROFILEDETAIL: {
            UserDetailInfo *userDetailInfo = static_cast<UserDetailInfo *>(actionData->mData);
            OperationManager::GetInstance()->AddOperation(MakeSptr<FriendOperation>(Operation::OT_Unfriend, userDetailInfo->GetId()));
            userDetailInfo->mFriendRequestStatus = eUSER_REQUEST_CANCELLED;//Unfriend or Delete???
            assert(actionData->mActionObj);
            elm_object_signal_emit(actionData->mActionObj, "hide.set", "respond");
            elm_object_signal_emit(actionData->mActionObj, Application::IsRTLLanguage() ? "show.set.rtl" : "show.set.ltr", "add");
            elm_object_signal_emit(actionData->mActionObj, "add.set", "add");
            elm_object_translatable_part_text_set(actionData->mActionObj, "add_friend_btn_text", "IDS_PROFILE_OTHER_ADDFRIEND");
            break;
        }
        default:
            break;
        }
    }
}

void FriendsAction::UnfriendFriendBtnClicked(ActionAndData *actionData)
{
    char* title = nullptr;
    char* message = nullptr;

    if (actionData->mData->GetGraphObjectType() == GraphObject::GOT_FRIEND) {
        Friend * fr = static_cast<Friend *>(actionData->mData);
        if (fr) {
            title = WidgetFactory::WrapByFormat(i18n_get_text("IDS_UNFRIEND_CD_TITLE"), fr->mName.c_str());
            message = WidgetFactory::WrapByFormat(i18n_get_text("IDS_UNFRIEND_CD_MESSAGE"), fr->mName.c_str());
        }
    } else if (actionData->mData->GetGraphObjectType() == GraphObject::GOT_USER) {
        User * user = static_cast<User *>(actionData->mData);
        if (user) {
            title = WidgetFactory::WrapByFormat(i18n_get_text("IDS_UNFRIEND_CD_TITLE"), user->mName);
            message = WidgetFactory::WrapByFormat(i18n_get_text("IDS_UNFRIEND_CD_MESSAGE"), user->mName);
        }
    }

    actionData->mAction->CreateConfirmationDialog(actionData->mAction->getScreen()->getMainLayout(), title, message, CDT_CANCEL_CONFIRM, confirmation_dialog_cb, actionData);
    delete [] title;
    delete [] message;
}

void FriendsAction::DoBlock(ActionAndData *actionData)
{
    Log::info("FriendsAction::DoBlock");
    assert(actionData);
    if (!ConnectivityManager::Singleton().IsConnected()) {
        assert(actionData->mAction);
        assert(actionData->mAction->getScreen());
        Utils::ShowToast(actionData->mAction->getScreen()->getMainLayout(), i18n_get_text("IDS_GEN_NETWORK_ERROR"));
    } else {
        assert(actionData->mData);
        FriendRequestsBatchProvider::GetInstance()->ResetRequestTimer();
        switch (actionData->mData->GetGraphObjectType()) {
        case GraphObject::GOT_FRIEND: {
            Friend *friends = dynamic_cast<Friend*>(actionData->mData);
            if (friends) {
                OperationManager::GetInstance()->AddOperation(MakeSptr<FriendOperation>(Operation::OT_Block, friends->GetId()));
            } else {
                Log::error("FriendsAction::DoBlock->blocked object type is not Friend");
            }
            break;
        }
        case GraphObject::GOT_USER: {
            User *user = dynamic_cast<User*>(actionData->mData);
            if (user) {
                OperationManager::GetInstance()->AddOperation(MakeSptr<FriendOperation>(Operation::OT_Block, user->GetId()));
            } else {
                Log::error("FriendsAction::DoBlock->blocked object type is not User");
            }
            break;
        }
        case GraphObject::GOT_USERPROFILEDETAIL: {
        	UserDetailInfo *userDetailInfo = dynamic_cast<UserDetailInfo*>(actionData->mData);
            if (userDetailInfo) {
                OperationManager::GetInstance()->AddOperation(MakeSptr<FriendOperation>(Operation::OT_Block, userDetailInfo->GetId()));
            } else {
                Log::error("FriendsAction::DoBlock->blocked object type is not UserDetailInfo");
            }
            break;
        }
        default:
            break;
        }
    }
}

void FriendsAction::BlockFriendBtnClicked(ActionAndData *actionData)
{
    char* title = nullptr;
    char* message = nullptr;

    if (actionData->mData->GetGraphObjectType() == GraphObject::GOT_FRIEND) {
        Friend * fr = static_cast<Friend *>(actionData->mData);
        if (fr) {
            title = WidgetFactory::WrapByFormat(i18n_get_text("IDS_PROFILE_POPUP_BLOCK_TITLE"), fr->mName.c_str());
            message = WidgetFactory::WrapByFormat(i18n_get_text("IDS_PROFILE_POPUP_BLOCK_MESSAGE"), fr->mName.c_str());
        }
    } else if (actionData->mData->GetGraphObjectType() == GraphObject::GOT_USER) {
        User * user = static_cast<User *>(actionData->mData);
        if (user) {
            title = WidgetFactory::WrapByFormat(i18n_get_text("IDS_PROFILE_POPUP_BLOCK_TITLE"), user->mName);
            message = WidgetFactory::WrapByFormat(i18n_get_text("IDS_PROFILE_POPUP_BLOCK_MESSAGE"), user->mName);
        }
    }

    actionData->mAction->CreateConfirmationDialog(actionData->mAction->getScreen()->getMainLayout(), title, message, CDT_CANCEL_BLOCK, confirmation_dialog_cb, actionData);
    delete [] title;
    delete [] message;
}

void FriendsAction::FriendRequestCarouselBtnClicked(ActionAndData *actionData)//
{
    assert (actionData);
    Log::info(LOG_FACEBOOK_FRIENDS_ACTION,"FriendsAction::FriendRequestCarouselBtnClicked");

    if (!ConnectivityManager::Singleton().IsConnected()) {
        Utils::ShowToast(actionData->mAction->getScreen()->getMainLayout(), i18n_get_text("IDS_GEN_NETWORK_ERROR"));
    } else {

        UserDetailInfo *userDetailInfo = dynamic_cast<UserDetailInfo *>(actionData->mData);

        if (userDetailInfo) {
            Log::info(LOG_FACEBOOK_FRIENDS_ACTION, "FriendsAction::FriendRequestCarouselBtnClicked->FrBtnClicked = success");
            User *user = FriendRequestsBatchProvider::GetInstance()->GetFrDataById(userDetailInfo->GetId());
            // Get userId from MainMenu->FriendRequest: (tab2:FriendRequests list)...
            // Friend Request Could be already deleted from list, but if it is presented we should update it's own status!

            if (userDetailInfo->mFriendRequestStatus == eUSER_REQUEST_SENT) {
                Log::info(LOG_FACEBOOK_FRIENDS_ACTION, "FriendsAction::FriendRequestCarouselBtnClicked->CancelOutgoingFriendRequest");
                FacebookSession::GetInstance()->ReleaseGraphRequest(mCancelFriendRequest, true);
                mCancelFriendRequest = FacebookSession::GetInstance()->CancelOutgoingFriendRequest(userDetailInfo->GetId(), on_fr_friend_request_completed, actionData);
                userDetailInfo->mFriendRequestStatus = eUSER_REQUEST_CANCELLED;
            } else if (userDetailInfo->mFriendRequestStatus == eUSER_REQUEST_CANCELLED) {
                Log::info(LOG_FACEBOOK_FRIENDS_ACTION, "FriendsAction::FriendRequestCarouselBtnClicked->SendFriendRequest");
                FacebookSession::GetInstance()->ReleaseGraphRequest(mAddFriendRequest, true);
                mAddFriendRequest = FacebookSession::GetInstance()->SendFriendRequest(userDetailInfo->GetId(), on_fr_friend_request_completed, actionData);
                userDetailInfo->mFriendRequestStatus = eUSER_REQUEST_SENT;
            } else if (userDetailInfo->mFriendRequestStatus == eUSER_REQUEST_CONFIRMED) {
                Log::info(LOG_FACEBOOK_FRIENDS_ACTION, "FriendsAction::FriendRequestCarouselBtnClicked->Unfriend");
                char* title = nullptr;
                char* message = nullptr;
                assert(actionData->mAction);
                assert(actionData->mAction->getScreen());
                title = WidgetFactory::WrapByFormat(i18n_get_text("IDS_UNFRIEND_CD_TITLE"),userDetailInfo->GetName());
                message = WidgetFactory::WrapByFormat(i18n_get_text("IDS_UNFRIEND_CD_MESSAGE"), userDetailInfo->GetName());
                actionData->mAction->CreateConfirmationDialog(actionData->mAction->getScreen()->getMainLayout(), title, message, CDT_CANCEL_CONFIRM, confirmation_dialog_cb, actionData);
                delete [] title;
                delete [] message;
            } else {
                Log::info(LOG_FACEBOOK_FRIENDS_ACTION, "FriendsAction::FriendRequestCarouselBtnClicked->SendFriendRequest (else)");
                FacebookSession::GetInstance()->ReleaseGraphRequest(mAddFriendRequest, true);
                mAddFriendRequest = FacebookSession::GetInstance()->SendFriendRequest(userDetailInfo->GetId(), on_fr_friend_request_completed, actionData);
                userDetailInfo->mFriendRequestStatus = eUSER_REQUEST_SENT;
            }
            AppEvents::Get().Notify(eFR_STATUS_CHANGE_EVENT, userDetailInfo);

            if (user) {
                user->mFriendRequestStatus = userDetailInfo->mFriendRequestStatus;
                AppEvents::Get().Notify(eFR_STATUS_CHANGE_EVENT, user);
            }

        } else {
            Log::error(LOG_FACEBOOK_FRIENDS_ACTION, "FriendsAction::FriendRequestCarouselBtnClicked->Graph object type is not GOT_USERPROFILEDETAIL");
            ShowErrorDialog(0);
        }
    }
}

void FriendsAction::on_fr_friend_request_completed(void* object, char* respond, int code)
{
    Log::info(LOG_FACEBOOK_FRIENDS_ACTION, "FriendsAction::on_fr_friend_request_completed");

    ActionAndData *actionData = static_cast<ActionAndData*>(object);


    assert(actionData);
    assert(actionData->mData);

    if (code == CURLE_OK) {
        Log::debug(LOG_FACEBOOK_FRIENDS_ACTION, "FriendsAction::on_fr_friend_request_completed->response: %s", respond);
        FbRespondBaseRestApi *confirmRespond = FbRespondBaseRestApi::createFromJson(respond);
        if (!confirmRespond) {
            Log::error(LOG_FACEBOOK_FRIENDS_ACTION, "Error parsing http respond");
        } else {
            int error_code = confirmRespond->mErrorCode;
            Log::warning(LOG_FACEBOOK_FRIENDS_ACTION, "FriendsAction::on_fr_friend_request_completed->error_code is %d", error_code);
            UserDetailInfo *userDetailInfo = dynamic_cast<UserDetailInfo*>(actionData->mData);
            User *user = FriendRequestsBatchProvider::GetInstance()->GetFrDataById(userDetailInfo->GetId());
            if (userDetailInfo) {
                if (confirmRespond->mErrorMessage) {
                    if (error_code == 520) {
                        userDetailInfo->mFriendRequestStatus = eUSER_REQUEST_SENT;
                    } else if (error_code == 522 || error_code == 100) {
                        AppEvents::Get().Notify(eFRIEND_REQUEST_CONFIRMED, const_cast<char*>(userDetailInfo->GetId()));
                        if(user) {
                            FriendRequestsBatchProvider::GetInstance()->DeleteItem(user);
                        }
                    } else if (error_code == 532) {
                        userDetailInfo->mFriendRequestStatus = eUSER_REQUEST_UNKNOWN;
                    } else if (error_code == 536) {
                        userDetailInfo->mFriendRequestStatus = eUSER_REQUEST_CANCELLED;
                    } else if (error_code == 0) {
                        Log::warning(LOG_FACEBOOK_FRIENDS_ACTION, "success");
                    } else {
                        userDetailInfo->mFriendRequestStatus = eUSER_REQUEST_CANCELLED;
                    }
                    if (error_code != 100 && error_code != 522) {
                        ShowErrorDialog(error_code);
                    }
                }
                if(user) {
                    AppEvents::Get().Notify(eFR_STATUS_CHANGE_EVENT, user);
                }
            }
        }
        delete confirmRespond;
    }
    free(respond);
}

void FriendsAction::ShowErrorDialog(int error_code)
{
    if (error_code == 520) {
         WidgetFactory::ShowErrorDialog(Application::GetInstance()->GetTopScreen()->getMainLayout(), "IDS_FRIENDS_ERROR_520");
    } else if (error_code == 522) {
         WidgetFactory::ShowErrorDialog(Application::GetInstance()->GetTopScreen()->getMainLayout(), "IDS_FRIENDS_ERROR_522");
    } else if (error_code == 532) {
         WidgetFactory::ShowErrorDialog(Application::GetInstance()->GetTopScreen()->getMainLayout(), "IDS_YOU_CANT_ADD_THIS_FRIEND");
    } else if (error_code == 528) {
         WidgetFactory::ShowErrorDialog(Application::GetInstance()->GetTopScreen()->getMainLayout(), "IDS_FRIENDS_ERROR_528");
    } else if (error_code == 536) {
         WidgetFactory::ShowErrorDialog(Application::GetInstance()->GetTopScreen()->getMainLayout(), "IDS_FRIENDS_ERROR_536");
    } else {
         WidgetFactory::ShowErrorDialog(Application::GetInstance()->GetTopScreen()->getMainLayout(), "IDS_FRIENDS_ERROR_UNKNOWN");
    }
}

void FriendsAction::confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event) {
    ActionAndData *actionData = static_cast<ActionAndData *>(user_data);
    assert(actionData);

    CNF_DIALOG_TYPE cnfDialogType = actionData->mAction->mCnfDialogType;
    actionData->mAction->DeleteConfirmationDialog();

    if (event == ConfirmationDialog::ECDNoPressed) {
        if (CDT_CANCEL_CONFIRM == cnfDialogType) {
            actionData->mAction->DoUnfriend(actionData);
        } else if (CDT_CANCEL_BLOCK == cnfDialogType) {
            actionData->mAction->DoBlock(actionData);
        } else {
            Log::error(LOG_FACEBOOK_FRIENDS_ACTION, "FriendsAction::confirmation_dialog_cb() - dialog type=%d is not supported", cnfDialogType);
        }
    }
}
