/*
 * EventActions.cpp
 *
 *  Created on: Nov 6, 2015
 *      Author: ruibezruch
 */
#include <app_i18n.h>
#include <dlog.h>
#include <memory>

#include "Common.h"
#include "Event.h"
#include "EventGuestListFactory.h"
#include "EventProfilePage.h"
#include "EventBatchProvider.h"
#include "EventMainPage.h"
#include "FbRespondBaseRestApi.h"
#include "FacebookSession.h"
#include "ScreenBase.h"
#include "EventActions.h"
#include "ProfileWidgetFactory.h"
#include "WidgetFactory.h"
#include "Utils.h"

#ifdef EVENT_NATIVE_VIEW

UIRes * EventActions::R = UIRes::GetInstance();

EventActions::EventActions(ScreenBase *screen)
{
    mScreen = screen;

    mGoingRequest = NULL;
    mMaybeRequest = NULL;
    mNotGoingRequest = NULL;
    mDeleteRequest = NULL;

    mParentBox = NULL;
    mLoadingBtns = NULL;
    mUpdatingToast = NULL;
}

EventActions::~EventActions()
{
    FacebookSession::GetInstance()->ReleaseGraphRequest(mGoingRequest);
    FacebookSession::GetInstance()->ReleaseGraphRequest(mMaybeRequest);
    FacebookSession::GetInstance()->ReleaseGraphRequest(mNotGoingRequest);
    FacebookSession::GetInstance()->ReleaseGraphRequest(mDeleteRequest);
}

void EventActions::EventGoingBtnClicked(ActionAndData *actionData)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "EventActions::EventGoingBtnClicked");

    Event *event = static_cast<Event*>(actionData->mData);

    mGoingRequest = FacebookSession::GetInstance()->ChangeEventStatus(event->GetId(), "attending", on_event_going_completed, actionData);

    switch (event->mRsvpStatus) {
    case Event::EVENT_RSVP_MAYBE:
        --event->mMaybeCount;
        break;
    case Event::EVENT_RSVP_NOT_REPLIED:
        --event->mInvitedCount;
        break;
    default:
        break;
    }
    ++event->mAttendingCount;
    event->mRsvpStatus = Event::EVENT_RSVP_ATTENDING;
    EventBatchProvider::GetInstance()->EventStatusSet(event->GetId(), Event::EVENT_RSVP_ATTENDING);
    AppEvents::GetInstance()->Notify(eEVENT_PROFILE_UPDATE_STATS_BOX, event);
    if (Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_EVENT_PROFILE_PAGE) {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "EventActions::SID_EVENT_PROFILE_PAGE");
        EventProfilePage *me = static_cast<EventProfilePage*>(actionData->mAction->getScreen());
        mUpdatingToast = Utils::ShowUpdatingToast(me->getParentMainLayout(), "Updating...");
        if (event->isInvited && actionData->mActionObj) {
            mParentBox = elm_object_parent_widget_get(actionData->mActionObj);
            evas_object_del(actionData->mActionObj);
            actionData->mActionObj = NULL;
            mLoadingBtns = ProfileWidgetFactory::CreateLoadingItem(mParentBox, true, true);
        }
        AppEvents::GetInstance()->Notify(eEVENT_ITEM_CHANGE_EVENT, event);
    } else if (Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_EVENT_MAIN_PAGE) {
        AppEvents::GetInstance()->Notify(eEVENT_ITEM_CHANGE_EVENT, event);
        if (actionData->mEventInviteBtnsLayout) {
            evas_object_del(actionData->mEventInviteBtnsLayout); actionData->mEventInviteBtnsLayout = NULL;
        }
    }
}
void EventActions::on_event_going_completed(void* object, char* respond, int code)
{
    ActionAndData *actionData = static_cast<ActionAndData*>(object);
    Event *event = static_cast<Event*>(actionData->mData);
    EventActions *actions = static_cast<EventActions*>(actionData->mAction);

    FacebookSession::GetInstance()->ReleaseGraphRequest(actions->mGoingRequest);

    if (code == CURLE_OK) {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "response: %s", respond);
        FbRespondBase * goingRespond = FbRespondBase::createFromJson(respond);
        if (goingRespond == NULL) {
            dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "Error parsing http respond");
        } else {
            if (goingRespond->mError != NULL) {
                dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "Error sending going request, error = %s", goingRespond->mError);
            } else if (goingRespond->mSuccess) {
                event->mRsvpStatus = Event::EVENT_RSVP_ATTENDING;
                if (Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_EVENT_PROFILE_PAGE) {
                    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "EventActions::SID_EVENT_PROFILE_PAGE");
                    Utils::HideUpdatingToast(actions->mUpdatingToast);
                    actions->mUpdatingToast = NULL;
                    if (event->isInvited) {
                        evas_object_del(actions->mLoadingBtns); actions->mLoadingBtns = NULL;

                        if (event->mRsvpStatus == Event::EVENT_RSVP_NOT_REPLIED) {
                            ProfileWidgetFactory::Create4ActionBtsItem(actions->mParentBox, actionData);
                            event->isInvited = true;
                        } else if (event->mCanGuestsInvite) {
                            ProfileWidgetFactory::Create4ActionBtsItem(actions->mParentBox, actionData);
                        } else {
                            ProfileWidgetFactory::Create3ActionBtsItem(actions->mParentBox, actionData);
                        }
                    } else {
                        ProfileWidgetFactory::SetEvent4Actions(actionData->mActionObj, actionData, true);
                    }
                    AppEvents::GetInstance()->Notify(eEVENT_ITEM_CHANGE_EVENT, event);
                } else if (Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_EVENT_MAIN_PAGE) {
                    //
                }
                if (event->isInvited) {
                    EventBatchProvider::GetInstance()->DeleteItem(event);
                    AppEvents::GetInstance()->Notify(eREDRAW_EVENT, actionData);
                }
                event->isInvited = false;
            }
        }
        delete goingRespond;
    } else {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "Error sending http request");
    }
    if (respond != NULL) {
        //Attention!! client should take care to free respond buffer
        free(respond);
    }
}

void EventActions::EventMaybeBtnClicked(ActionAndData *actionData)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "EventActions::EventMaybeBtnClicked");

    Event *event = static_cast<Event*>(actionData->mData);

    mMaybeRequest = FacebookSession::GetInstance()->ChangeEventStatus(event->GetId(), "maybe", on_event_maybe_completed, actionData);

    switch (event->mRsvpStatus) {
    case Event::EVENT_RSVP_ATTENDING:
        --event->mAttendingCount;
        break;
    case Event::EVENT_RSVP_NOT_REPLIED:
        --event->mInvitedCount;
        break;
    default:
        break;
    }
    ++event->mMaybeCount;
    event->mRsvpStatus = Event::EVENT_RSVP_MAYBE;
    EventBatchProvider::GetInstance()->EventStatusSet(event->GetId(), Event::EVENT_RSVP_MAYBE);
    AppEvents::GetInstance()->Notify(eEVENT_PROFILE_UPDATE_STATS_BOX, event);
    if (Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_EVENT_PROFILE_PAGE) {
        EventProfilePage *me = static_cast<EventProfilePage*>(actionData->mAction->getScreen());
        mUpdatingToast = Utils::ShowUpdatingToast(me->getParentMainLayout(), "Updating...");
        if (event->isInvited) {
            mParentBox = elm_object_parent_widget_get(actionData->mActionObj);
            evas_object_del(actionData->mActionObj);
            actionData->mActionObj = NULL;
            mLoadingBtns = ProfileWidgetFactory::CreateLoadingItem(mParentBox, true, true);
        }
        AppEvents::GetInstance()->Notify(eEVENT_ITEM_CHANGE_EVENT, event);
    } else if (Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_EVENT_MAIN_PAGE) {
        AppEvents::GetInstance()->Notify(eEVENT_ITEM_CHANGE_EVENT, event);
        if (actionData->mEventInviteBtnsLayout) {
            evas_object_del(actionData->mEventInviteBtnsLayout); actionData->mEventInviteBtnsLayout = NULL;
        }
    }
}

void EventActions::on_event_maybe_completed(void* object, char* respond, int code)
{
    ActionAndData *actionData = static_cast<ActionAndData*>(object);
    Event *event = static_cast<Event*>(actionData->mData);
    EventActions *actions = static_cast<EventActions*>(actionData->mAction);

    FacebookSession::GetInstance()->ReleaseGraphRequest(actions->mMaybeRequest);

    if (code == CURLE_OK) {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "response: %s", respond);
        FbRespondBase * maybeRespond = FbRespondBase::createFromJson(respond);
        if (maybeRespond == NULL) {
            dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "Error parsing http respond");
        } else {
            if (maybeRespond->mError != NULL) {
                dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "Error sending going request, error = %s", maybeRespond->mError);
            } else if (maybeRespond->mSuccess) {
                event->mRsvpStatus = Event::EVENT_RSVP_MAYBE;
                if (Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_EVENT_PROFILE_PAGE) {
                    Utils::HideUpdatingToast(actions->mUpdatingToast);
                    actions->mUpdatingToast = NULL;
                    if (event->isInvited) {
                        evas_object_del(actions->mLoadingBtns); actions->mLoadingBtns = NULL;
                        if (event->mRsvpStatus == Event::EVENT_RSVP_NOT_REPLIED) {
                            ProfileWidgetFactory::Create4ActionBtsItem(actions->mParentBox, actionData);
                            event->isInvited = true;
                        } else if (event->mCanGuestsInvite) {
                            ProfileWidgetFactory::Create4ActionBtsItem(actions->mParentBox, actionData);
                        } else {
                            ProfileWidgetFactory::Create3ActionBtsItem(actions->mParentBox, actionData);
                        }
                    } else {
                        ProfileWidgetFactory::SetEvent4Actions(actionData->mActionObj, actionData, true);
                    }
                } else if (Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_EVENT_MAIN_PAGE) {
                    //
                }
                if (event->isInvited) {
                    EventBatchProvider::GetInstance()->DeleteItem(event);
                    AppEvents::GetInstance()->Notify(eREDRAW_EVENT, actionData);
                }
                event->isInvited = false;
            }
        }
        delete maybeRespond;
    } else {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "Error sending http request");
    }
    if (respond != NULL) {
        //Attention!! client should take care to free respond buffer
        free(respond);
    }
}

void EventActions::EventNotGoingBtnClicked(ActionAndData *actionData)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "EventActions::EventNotGoingBtnClicked");

    Event *event = static_cast<Event*>(actionData->mData);

    switch (event->mRsvpStatus) {
    case Event::EVENT_RSVP_ATTENDING:
        --event->mAttendingCount;
        break;
    case Event::EVENT_RSVP_MAYBE:
        --event->mMaybeCount;
        break;
    case Event::EVENT_RSVP_NOT_REPLIED:
        --event->mInvitedCount;
        break;
    default:
        break;
    }
    mNotGoingRequest = FacebookSession::GetInstance()->ChangeEventStatus(event->GetId(), "declined", on_event_not_going_completed, actionData);
    event->mRsvpStatus = Event::EVENT_RSVP_DECLINED;
    EventBatchProvider::GetInstance()->EventStatusSet(event->GetId(), Event::EVENT_RSVP_DECLINED);
    AppEvents::GetInstance()->Notify(eEVENT_PROFILE_UPDATE_STATS_BOX, event);
    if (Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_EVENT_PROFILE_PAGE) {
        EventProfilePage *me = static_cast<EventProfilePage*>(actionData->mAction->getScreen());
        mUpdatingToast = Utils::ShowUpdatingToast(me->getParentMainLayout(), "Updating...");
        if (event->isInvited) {
            mParentBox = elm_object_parent_widget_get(actionData->mActionObj);
            evas_object_del(actionData->mActionObj);
            actionData->mActionObj = NULL;
            mLoadingBtns = ProfileWidgetFactory::CreateLoadingItem(mParentBox, true, true);
        }
        AppEvents::GetInstance()->Notify(eEVENT_ITEM_CHANGE_EVENT, event);
    } else if (Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_EVENT_MAIN_PAGE) {
        AppEvents::GetInstance()->Notify(eEVENT_ITEM_CHANGE_EVENT, event);
        if (actionData->mEventInviteBtnsLayout) {
            evas_object_del(actionData->mEventInviteBtnsLayout); actionData->mEventInviteBtnsLayout = NULL;
        }
    }
}

void EventActions::on_event_not_going_completed(void* object, char* respond, int code)
{
    ActionAndData *actionData = static_cast<ActionAndData*>(object);
    Event *event = static_cast<Event*>(actionData->mData);
    EventActions *actions = static_cast<EventActions*>(actionData->mAction);

    FacebookSession::GetInstance()->ReleaseGraphRequest(actions->mNotGoingRequest);

    if (code == CURLE_OK) {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "response: %s", respond);
        FbRespondBase * notGoingRespond = FbRespondBase::createFromJson(respond);
        if (notGoingRespond == NULL) {
            dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "Error parsing http respond");
        } else {
            if (notGoingRespond->mError != NULL) {
                dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "Error sending notGoingRespond request, error = %s", notGoingRespond->mError);
            } else if (notGoingRespond->mSuccess) {
                if (Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_EVENT_PROFILE_PAGE) {
                    Utils::HideUpdatingToast(actions->mUpdatingToast);
                    actions->mUpdatingToast = NULL;
                    if (event->isInvited) {
                        evas_object_del(actions->mLoadingBtns); actions->mLoadingBtns = NULL;
                        if (event->mRsvpStatus == Event::EVENT_RSVP_NOT_REPLIED) {
                            ProfileWidgetFactory::Create4ActionBtsItem(actions->mParentBox, actionData);
                            event->isInvited = true;
                        } else if (event->mCanGuestsInvite) {
                            ProfileWidgetFactory::Create4ActionBtsItem(actions->mParentBox, actionData);
                        } else {
                            ProfileWidgetFactory::Create3ActionBtsItem(actions->mParentBox, actionData);
                        }
                    } else {
                        ProfileWidgetFactory::SetEvent4Actions(actionData->mActionObj, actionData, true);
                    }
                    AppEvents::GetInstance()->Notify(eEVENT_ITEM_CHANGE_EVENT, event);
                } else if (Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_EVENT_MAIN_PAGE) {
                    //
                }
                if (event->isInvited) {
                    EventBatchProvider::GetInstance()->DeleteItem(event);
                    AppEvents::GetInstance()->Notify(eREDRAW_EVENT, actionData);
                }
                event->isInvited = false;
            }
        }
        delete notGoingRespond;
    } else {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "Error sending http request");
    }
    if (respond != NULL) {
        //Attention!! client should take care to free respond buffer
        free(respond);
    }
}

void EventActions::CtxApprovedDeleteEvent(ActionAndData *actionData)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "EventActions::CtxApprovedDeleteEvent");

    Event *event = static_cast<Event*>(actionData->mData);
    mDeleteRequest = FacebookSession::GetInstance()->DeleteEvent(event->GetId(), on_event_deleting_completed, actionData);
    mUpdatingToast = Utils::ShowUpdatingToast(actionData->mAction->getScreen()->getParentMainLayout(), i18n_get_text("IDS_DELETING_EVENT_PROCESS"));
}

void EventActions::on_event_deleting_completed(void* object, char* respond, int code)
{
    ActionAndData *actionData = static_cast<ActionAndData *>(object);
    EventActions *actions = static_cast<EventActions *>(actionData->mAction);

    FacebookSession::GetInstance()->ReleaseGraphRequest(actions->mDeleteRequest);

    if ( code == CURLE_OK ) {
        FbRespondBase * deleteEventRespond = FbRespondBase::createFromJson(respond);
        if ( deleteEventRespond == NULL ) {
            dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "Error parsing http respond, on_event_deleting_completed");
        } else {
            if ( deleteEventRespond->mError != NULL ) {
                dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "Error sending notGoingRespond request, error = %s", deleteEventRespond->mError);
            } else if ( deleteEventRespond->mSuccess ) {
                Utils::HideUpdatingToast(actions->mUpdatingToast);
                actions->mUpdatingToast = NULL;
                Event *event = static_cast<Event*>(actionData->mData);
                if (event) {
                    EventBatchProvider::GetInstance()->DeleteItem(event);
                }
                AppEvents::GetInstance()->Notify( eREDRAW_EVENT, actionData);
                if (Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_EVENT_PROFILE_PAGE) {
                    Application::GetInstance()->GetTopScreen()->Pop();
                }
            }
        }
        delete deleteEventRespond;
    } else {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "on_event_deleting_completed, Error sending http request");
    }
    if (respond != NULL) {
        //Attention!! client should take care to free respond buffer
        free(respond);
    }
}

void EventActions::AddFriendBtnClicked(ActionAndData *actionData)
{
    if (!actionData || !actionData->mData) {
        return;
    }

    User * user = static_cast<User *>(actionData->mData);
    switch (user->mFriendshipStatus) {
    case User::eNOT_AVAILABLE:
    case User::eARE_FRIENDS:
    case User::eCANNOT_REQUEST:
        WidgetFactory::ShowErrorDialog(actionData->mAction->getScreen()->getMainLayout(), "IDS_YOU_CANT_ADD_THIS_FRIEND_UPDATE");
        break;
    case User::eCAN_REQUEST: {
        FacebookSession::GetInstance()->SendFriendRequest(user->GetId(), on_add_friend_completed, strdup(user->GetId()));
        user->mFriendshipStatus = User::eOUTGOING_REQUEST;
        AppEvents::GetInstance()->Notify(eEVENT_FRIENDSHIP_STATUS_CHANGED, const_cast<char *>(user->GetId()));
    }
        break;
    case User::eOUTGOING_REQUEST: {
        FacebookSession::GetInstance()->CancelOutgoingFriendRequest(user->GetId(), on_cancel_friend_request_completed, strdup(user->GetId()));
        user->mFriendshipStatus = User::eCAN_REQUEST;
        AppEvents::GetInstance()->Notify(eEVENT_FRIENDSHIP_STATUS_CHANGED, const_cast<char *>(user->GetId()));
    }
        break;
    default:
        break;
    }
}

void EventActions::on_add_friend_completed(void* object, char* respond, int code)
{
    char * userId = static_cast<char *>(object);

    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "EventActions::on_add_friend_completed");

    if (code == CURLE_OK) {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "response: %s", respond);
        FbRespondBase * response = FbRespondBase::createFromJson(respond);
        if (response == NULL) {
            dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "Error parsing http respond");
        }
        else if (response->mError) {
            dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "error_code is %d", response->mError->mCode);

            GraphObject * graphObject = EventGuestListFactory::GetGraphObjectById(userId);
            if (graphObject) {
                User * user = static_cast<User *>(graphObject);
                // TODO Add error code handling
                if (user->mFriendshipStatus == User::eOUTGOING_REQUEST) {
                    user->mFriendshipStatus = User::eCAN_REQUEST;
                }
                else if (user->mFriendshipStatus == User::eCAN_REQUEST) {
                    user->mFriendshipStatus = User::eOUTGOING_REQUEST;
                }
                AppEvents::GetInstance()->Notify(eEVENT_FRIENDSHIP_STATUS_CHANGED, const_cast<char *>(user->GetId()));
            }
        }
        delete response;
    }
    else {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "Error sending http request");
    }

    free(userId);
    //Attention!! client should take care to free respond buffer
    free(respond);
}

void EventActions::on_cancel_friend_request_completed(void* object, char* respond, int code)
{
    char * userId = static_cast<char *>(object);

    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "EventActions::on_cancel_friend_request_completed");

    if (code == CURLE_OK) {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "response: %s", respond);
        FbRespondBaseRestApi * response = FbRespondBaseRestApi::createFromJson(respond);
        if (response == NULL) {
            dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "Error parsing http respond");
        }
        else if (response->mErrorCode) {
            dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "error_code is %d", response->mErrorCode);

            GraphObject * graphObject = EventGuestListFactory::GetGraphObjectById(userId);
            if (graphObject) {
                User * user = static_cast<User *>(graphObject);
                // TODO Add error code handling
                if (user->mFriendshipStatus == User::eOUTGOING_REQUEST) {
                    user->mFriendshipStatus = User::eCAN_REQUEST;
                }
                else if (user->mFriendshipStatus == User::eCAN_REQUEST) {
                    user->mFriendshipStatus = User::eOUTGOING_REQUEST;
                }
                AppEvents::GetInstance()->Notify(eEVENT_FRIENDSHIP_STATUS_CHANGED, const_cast<char *>(user->GetId()));
            }
        }
        delete response;
    }
    else {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "Error sending http request");
    }

    free(userId);
    //Attention!! client should take care to free respond buffer
    free(respond);
}

#endif
