#include "CommentScreen.h"
#include "Common.h"
#include "Log.h"
#include "PostComposerScreen.h"
#include "VideoPlayerAction.h"
#include "VideoPlayerScreen.h"

VideoPlayerAction::VideoPlayerAction(ScreenBase *screen)
{
    mScreen = screen;
}

void VideoPlayerAction::TextClicked(ActionAndData *userData) {
    //the same action as for CommentBtnClicked().
    Log::info("VideoPlayerAction::TextClicked");
    CommentBtnClicked(userData);
}

void VideoPlayerAction::CommentBtnClicked(ActionAndData *user_data)
{
    Log::info("VideoPlayerAction::CommentBtnClicked");
    if (mScreen) {
        VideoPlayerScreen *screen = static_cast<VideoPlayerScreen *>(mScreen);
        screen->WaitProgressBarHide();
    }
    AppEvents::Get().Notify(eVIDEOPLAYER_PAUSE_EVENT);
    ActionBase::CommentBtnClicked(user_data);
}

void VideoPlayerAction::ShareWithPostClicked(GraphObject *graphObject)
{
    Log::info("VideoPlayerAction::ShareWithPostClicked");
    if (mScreen) {
        VideoPlayerScreen *screen = static_cast<VideoPlayerScreen *>(mScreen);
        screen->WaitProgressBarHide();
    }
    AppEvents::Get().Notify(eVIDEOPLAYER_PAUSE_EVENT);
    if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_POST_COMPOSER_SCREEN) {
        PostComposerScreen* postComposer = new PostComposerScreen(mScreen, eSHARE, static_cast<Post *>(graphObject));
        Application::GetInstance()->AddScreen(postComposer);
    }
}
