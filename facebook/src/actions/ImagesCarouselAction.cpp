#include "Common.h"
#include "Config.h"
#include "ImagesCarouselAction.h"
#include "ImagesCarouselScreen.h"
#include "Log.h"
#include "OperationManager.h"
#include "Photo.h"
#include "PostUpdateOperation.h"
#include "Utils.h"

#include <chrono>


#define FM_COPY_BUF_SIZE 16384

static bool file_copy(const char *src, const char *dst)
{
    FILE *source_file = NULL;
    FILE *destintation_file = NULL;
    char buf[FM_COPY_BUF_SIZE] = { 0 };
    char src_realpath[PATH_MAX] = { '\0' };
    char dst_realpath[PATH_MAX] = { '\0' };
    size_t num = 0;

    if (!realpath(src, src_realpath)) {
        return false;
    }
    if (realpath(dst, dst_realpath) && !strcmp(src_realpath, dst_realpath)) {
        return false;
    }

    source_file = fopen(src, "rb");
    if (!source_file) {
        return false;
    }

    destintation_file = fopen(dst, "wb");
    if (!destintation_file) {
        fclose(source_file);
        return false;
    }

    while ((num = fread(buf, 1, sizeof(buf), source_file)) > 0) {
        if (fwrite(buf, 1, num, destintation_file) != num) {
            fclose(source_file);
            fclose(destintation_file);
            return false;
        }
    }

    fclose(source_file);
    fclose(destintation_file);

    return true;
}

ImagesCarouselAction::ImagesCarouselAction(ScreenBase *screen)
{
    mScreen = screen;
}

void ImagesCarouselAction::SaveBtnClicked(ActionAndData * user_data)
{
    Log::debug("ImagesCarouselAction::SaveBtnClicked");
    assert(user_data);
    Photo *photo = static_cast<Photo *>(user_data->mData);
    Log::debug("Facebook media path: %s", Config::GetInstance().GetFBMediaPath().c_str());
    assert(photo);
    bool isFileSaved = false;
    if (!Config::GetInstance().GetFBMediaPath().empty()) {
        ImageFile *imageFile = photo->GetMaxResDownloadedImageFile();
        if (imageFile) {
            char destFilePath[PATH_MAX] = { 0 };

            const char *ext = strrchr(imageFile->GetPath(), '.');
            using namespace std::chrono;
            milliseconds ms = duration_cast< milliseconds >(system_clock::now().time_since_epoch());
            Utils::Snprintf_s(destFilePath, PATH_MAX, "%s/FB_IMG_%012llX%s", Config::GetInstance().GetFBMediaPath().c_str(), ms, ext ? ext : "");

            if (file_copy(imageFile->GetPath(), destFilePath)) {
                isFileSaved = true;
                media_content_connect();
                media_content_scan_folder(Config::GetInstance().GetFBMediaPath().c_str(), true, NULL, NULL); // Update the content db
                media_content_disconnect();
            }
        }
    }
    notification_status_message_post(i18n_get_text(isFileSaved ? "IDS_PHOTO_SAVE_SUCCESS" : "IDS_PHOTO_SAVE_ERROR"));
}

void ImagesCarouselAction::SaveCaptionClicked(ActionAndData * action_data)
{
    Log::info("ImagesCarouselAction::SaveCaptionClicked");
    CHECK_RET_NRV(action_data);
    CHECK_RET_NRV(action_data->mData);
    CHECK_RET_NRV(mScreen);
    ImagesCarouselScreen *screen = static_cast<ImagesCarouselScreen *>(mScreen);
    Photo *photo = static_cast<Photo*>(action_data->mData);
    CHECK_RET_NRV(photo);
    bundle* paramsBundle;
    paramsBundle = bundle_create();
    if (screen->GetTagging()) {
        char *originTextToPost = screen->GetCaptionEntryText();
        char *textToPost = screen->GetTagging()->GetStringToPost(false);
        char *escape = Utils::escape(textToPost);

        free(textToPost);
        bundle_add_str(paramsBundle, "name", escape);
        free(escape);

        const char* photoId = photo->GetId();
        OperationManager::GetInstance()->AddOperation(MakeSptr<PostUpdateOperation>(PostUpdateOperation::OT_Post_Update, photoId ? photoId : "", paramsBundle, std::list<Friend>(), "", nullptr));
        bundle_free(paramsBundle);

        photo->SetName(originTextToPost);
        photo->SetPhotoMessageTagsList(screen->GetTagging()->GetMessageTagsList());
        free(originTextToPost);
    }
    AppEvents::Get().Notify(ePHOTO_CAPTION_WAS_CHANGED, const_cast<char *>(action_data->mData->GetId()));
    screen->UpdateUI(action_data);
}
