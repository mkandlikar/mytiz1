#include "ActionBase.h"
#include "CommentsAction.h"
#include "CommentScreen.h"
#include "CommentsProvider.h"
#include "Common.h"
#include "FacebookSession.h"
#include "ImagesCarouselScreen.h"
#include "Log.h"
#include "OperationManager.h"
#include "OperationPresenter.h"
#include "PostScreen.h"
#include "SimpleCommentOperation.h"
#include "WebViewScreen.h"
#include "WidgetFactory.h"

CommentsAction::CommentsAction(ScreenBase *screen)
{
    mScreen = screen;
    mParentEntityId = nullptr;
}

void CommentsAction::SetParentId(const char *id){
    mParentEntityId = SAFE_STRDUP(id);
}

void CommentsAction::AvatarClicked(GraphObject *graphObject)
{
    if(graphObject) {
        Comment *comment = static_cast<Comment*>(graphObject);
        if(comment->GetFrom()) {
            if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_USER_PROFILE && !IsNestedProfile(comment->mFrom->GetId())) {
                mGrpReqObjectType = LoadObjectDetails(this, comment->GetFrom()->GetId(), true);
            }
        }
    }
}

void CommentsAction::CtxApprovedDeleteCmntClicked(ActionAndData *actionData)
{
    if (actionData) {
        Comment *comment = dynamic_cast<Comment*>(actionData->mData);
        if (comment) {
            Log::info("CommentsAction::CtxApprovedDeleteCmntClicked()->ParentId is %s", mParentEntityId ? mParentEntityId : "missed" );
            Operation::OperationType operationType =
                mScreen->GetScreenId() == ScreenBase::SID_REPLY ?
                Operation::OT_Delete_Reply :
                Operation::OT_Delete_Comment ;

            assert(mScreen);

            bool isPost = false;
            CommentScreen* commentScreen = dynamic_cast<CommentScreen*>(mScreen);
            if (commentScreen) {
                 isPost =  commentScreen->GetParentItemType();
            }
            const char* commentId = comment->GetId();
            OperationManager::GetInstance()->AddOperation(MakeSptr<SimpleCommentOperation>(operationType, mParentEntityId, isPost , nullptr, commentId ? commentId : ""));
        }
    }
}

void CommentsAction::PhotoClicked(GraphObject *graphObject)
{
    Comment * comment = static_cast<Comment *>(graphObject);
    if (comment && comment->mAttachment->mMedia->mImage->GetFilePath()) {
        ImagesCarouselScreen * screen = new ImagesCarouselScreen(comment->mAttachment->mMedia->mImage,
                ImagesCarouselScreen::eWITHOUT_UI_MODE);
        Application::GetInstance()->AddScreen(screen);
    }
}

void CommentsAction::LinkClicked(GraphObject *graphObject)
{
    Log::info(LOG_FACEBOOK_COMMENTING, "CommentsAction::LinkClicked");
    Comment* comment = static_cast<Comment*>(graphObject);
    if (comment) {
        const char *url = nullptr;
        bool enableDeeplink = false;
        StoryAttachment *att = comment->mAttachment;
        if (att) {
            if (att->mUrl) {
                url = att->mUrl;
            } else if (att->mTarget && att->mTarget->mUrl) {
                url = att->mTarget->mUrl;
                enableDeeplink = true;
            }
        }
        if (url) {
            Log::debug(LOG_FACEBOOK_COMMENTING, "CommentsAction, url=\n %s", url);
            if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_WEB_VIEW) {
                WebViewScreen::Launch(url, enableDeeplink, true);
            }
        }
    }
}


void CommentsAction::CancelPostingOperation(ActionAndData *actionAndData)
{
    Log::info(LOG_FACEBOOK_COMMENTING, "CommentsAction::CancelPostingOperation");
    assert(actionAndData);
    OperationPresenter* op = static_cast<OperationPresenter*>(actionAndData->mData);
    assert(op);
    Sptr<Operation> operation(op->GetOperation());
    if(operation) {
        operation->Cancel();
    }
    assert(actionAndData->mAction);
    CommentScreen *screen = static_cast<CommentScreen *>(actionAndData->mAction->getScreen());
    assert(screen);
    if(screen->GetProvider()) {
        screen->GetProvider()->RemoveOperation(operation);
    }
}

void CommentsAction::RetryPostSend(ActionAndData *actionAndData)
{
    Log::info( LOG_FACEBOOK_OPERATION, "CommentsAction::RetryCommentSend");
    assert(actionAndData);
    OperationPresenter* op = static_cast<OperationPresenter*>(actionAndData->mData);
    assert(op);
    Sptr<Operation> operation(op->GetOperation());
    if(operation && mScreen){
        OperationManager::GetInstance()->RestartOperationAfterError(operation);
    }
}
