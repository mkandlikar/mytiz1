#include "ActionBase.h"
#include "AlbumGalleryScreen.h"
#include "Application.h"
#include "Comment.h"
#include "CommentWidgetFactory.h"
#include "Config.h"
#include "ConnectivityManager.h"
#include "Event.h"
#include "EventProfilePage.h"
#include "GroupProfilePage.h"
#include "ImagePostScreen.h"
#include "ImagesCarouselScreen.h"
#include "LikeOperation.h"
#include "Log.h"
#include "MapViewScreen.h"
#include "Messenger.h"
#include "OperationManager.h"
#include "OperationPresenter.h"
#include "OwnProfileScreen.h"
#include "PhotoGalleryShowCarouselStrategy.h"
#include "Post.h"
#include "PostComposerScreen.h"
#include "PostScreen.h"
#include "ProfileScreen.h"
#include "SelectUserScreen.h"
#include "SignUpConfirmScreen.h"
#include "SoundNotificationPlayer.h"
#include "Utils.h"
#include "WebViewScreen.h"
#include "WhoLikedScreen.h"
#include "WidgetFactory.h"

#include <cassert>
#include <efl_extension.h>
#include <stdio.h>

Eina_List *ActionAndData::mAliveActionAndDates = nullptr;


ActionAndData::ActionAndData(GraphObject *data, ActionBase *action, ActionAndData *parent, const char * entityId) : mLayouts(nullptr) {
    mParent = parent;
    if(mParent) {
        mParent->mChild = this;
    }
    mChild = nullptr;
    mData = data;
    if(mData) {
        mData->AddRef();
        if(mData->GetGraphObjectType() == GraphObject::GOT_OPERATION_PRESENTER ) {
            Sptr<Operation> op(static_cast<OperationPresenter*>(mData)->GetOperation());
            if(op) {
                op->Subscribe(this);
            }
        }
    }
    mAction = action;
    mParentWidget = nullptr;
    mLikesCommentsWidget = nullptr;
    mInfoBoxWidget = nullptr;
    mPostText = nullptr;
    mActionObj = nullptr;
    mEventNameLabel = nullptr;
    mEventStartTimeLabel = nullptr;
    mEventPlaceLabel = nullptr;
    mEventInviteBtnsLayout = nullptr;
    mChangeableLabel = nullptr;
    mFriendsRequestTab = nullptr;
    mObjectWidget = nullptr;
    mOperationProgress = nullptr;
    mOperationNotification = nullptr;
    mEventObjLabel = nullptr;
    mReplyActionObject = nullptr;
    mEntityId = SAFE_STRDUP(entityId);
    mIsLinkMetaDataFieldRequired = true;
    mPostActionsBtns = nullptr;
    mHeader = nullptr;
    mTimeStamp = clock();
    mAliveActionAndDates = eina_list_append(mAliveActionAndDates, this);
}

ActionAndData::~ActionAndData() {
    // first of all we have to cancelled requests
    DeleteAsyncRequests();
    if(mData) {
        if(mData->GetGraphObjectType() == GraphObject::GOT_OPERATION_PRESENTER ) {
            Sptr<Operation> op(static_cast<OperationPresenter*>(mData)->GetOperation());
            if(op) {
                op->UnSubscribe(this);
            }
        }
        mData->Release();
    }

    if(mParent) {
        mParent->mChild = nullptr;
    }

    delete mChild;

    if (mOperationNotification) {
        notification_delete(mOperationNotification);
    }

    free((void *)mEntityId);
    mAliveActionAndDates = eina_list_remove(mAliveActionAndDates, this);
}

ActionAndData::ActionAndData(const ActionAndData& CopyMe) {
    mChild = CopyMe.mChild;
    mParentWidget = CopyMe.mParentWidget;
    mLikesCommentsWidget = CopyMe.mLikesCommentsWidget;
    mInfoBoxWidget = CopyMe.mInfoBoxWidget;
    mPostText = CopyMe.mPostText;
    mActionObj = CopyMe.mActionObj;
    mChangeableLabel = CopyMe.mChangeableLabel;
    mEventObjLabel = CopyMe.mEventObjLabel;
    mEventNameLabel = CopyMe.mEventNameLabel;
    mEventPlaceLabel = CopyMe.mEventPlaceLabel;
    mEventStartTimeLabel = CopyMe.mEventStartTimeLabel;
    mEventInviteBtnsLayout = CopyMe.mEventInviteBtnsLayout;
    mReplyActionObject = CopyMe.mReplyActionObject;
    mObjectWidget = CopyMe.mObjectWidget;
    mFriendsRequestTab = CopyMe.mFriendsRequestTab;
    mIsLinkMetaDataFieldRequired = CopyMe.mIsLinkMetaDataFieldRequired;
    mOperationProgress = CopyMe.mOperationProgress;
    mOperationNotification = CopyMe.mOperationNotification;
    mHeader = CopyMe.mHeader;
    mLayouts = nullptr;
    mParent = CopyMe.mParent;
    mData = CopyMe.mData;
    if(mData) {
        mData->AddRef();
        if(mData->GetGraphObjectType() == GraphObject::GOT_OPERATION_PRESENTER ) {
            Sptr<Operation> op(static_cast<OperationPresenter*>(mData)->GetOperation());
            if(op)
                op->Subscribe(this);
        }
    }
    mAction = CopyMe.mAction;
    mEntityId = SAFE_STRDUP(CopyMe.mEntityId);
    mPostActionsBtns = CopyMe.mPostActionsBtns;
    mTimeStamp = clock();
    mAliveActionAndDates = eina_list_append(mAliveActionAndDates, this);
}

void ActionAndData::DeleteAsyncRequests() {
    void *data;
    EINA_LIST_FREE(mLayouts, data) {
        AsyncLayoutUpdater * layoutUpdater = static_cast<AsyncLayoutUpdater *>(data);
        assert(layoutUpdater);
        layoutUpdater->Cancel(layoutUpdater->mRequestId);
        delete layoutUpdater;
    }
}

void ActionAndData::UpdateLayoutAsync(LayoutType type, Evas_Object *layout, int dataType) {
    AsyncLayoutUpdater *updater = new AsyncLayoutUpdater(type, layout, this, dataType);
    if (updater->DownloadGraphData(dataType)) {
        mLayouts = eina_list_append(mLayouts, updater);
    } else {
        delete updater;
    }
}

/**
 * If we already have Image on file system we just load it
 * else we request async request to download Image by url.
 */
void ActionAndData::UpdateImageLayoutAsync(const char* url, Evas_Object *layout, LayoutType type, int dataType, int index) {
    const char *fileName = Utils::GetIconNameFromUrl(url);
    if(fileName) {
        AsyncLayoutUpdater *updater = new AsyncLayoutUpdater(type, layout, this, dataType, index);
        if (updater->LoadImage(fileName)) {
                Log::debug("AsyncLayoutUpdater::UpdateImageLayoutAsync");
                delete updater;
                updater = nullptr;
        } else {
            if (updater->DownloadImage(url)) {
                mLayouts = eina_list_append(mLayouts, updater);
            } else {
                delete updater;
            }
        }
    }
    free((char *)fileName);
}

void ActionAndData::Update(AppEventId eventId, void * data) {
    int screenId = ScreenBase::SID_UNKNOWN;
    if ( mAction && mAction->getScreen() && mAction->getScreen()->GetScreenId() ) {
        screenId = mAction->getScreen()->GetScreenId();
    }
    switch(eventId) {
    case eOPERATION_STEP_PROGRESS_EVENT: {
            if(mData && (mData->GetGraphObjectType() == GraphObject::GOT_OPERATION_PRESENTER)) {
                double stepProgress = 0;
                if(data) {
                    stepProgress = *static_cast<double *>(data);
                }
                if (!(screenId == ScreenBase::SID_COMMENTS || screenId == ScreenBase::SID_REPLY || screenId == ScreenBase::SID_POST)) {
                    WidgetFactory::UpdatePostOperation(this, &stepProgress);
                }
            }
        }
        break;
    case eOPERATION_COMPLETE_EVENT:{
            if(mData && (mData->GetGraphObjectType() == GraphObject::GOT_OPERATION_PRESENTER)) {
                if (screenId == ScreenBase::SID_COMMENTS || screenId == ScreenBase::SID_REPLY || screenId == ScreenBase::SID_POST) {
                    CommentWidgetFactory::UpdateCommentOperation(this);
                } else {
                    WidgetFactory::UpdatePostOperation(this, nullptr);
                }
            }
        }
        break;
    case eOPERATION_STEP_COMPLETE_EVENT: {
            if(mData && (mData->GetGraphObjectType() == GraphObject::GOT_OPERATION_PRESENTER)) {
                if (screenId == ScreenBase::SID_COMMENTS || screenId == ScreenBase::SID_REPLY || screenId == ScreenBase::SID_POST) {
                    CommentWidgetFactory::UpdateCommentOperation(this);
                } else {
                    WidgetFactory::UpdatePostOperation(this, nullptr);
                }
            }
        }
        break;
    case eOPERATION_RETRY_SEND_POST_EVENT: {
            if(mData && (mData->GetGraphObjectType() == GraphObject::GOT_OPERATION_PRESENTER)) {
                if (screenId == ScreenBase::SID_GROUP_PROFILE_PAGE || screenId == ScreenBase::SID_HOME_SCREEN ||
                    screenId == ScreenBase::SID_OWN_PROFILE || screenId == ScreenBase::SID_USER_PROFILE) {
                    WidgetFactory::CloseErrorDialogue();
                    WidgetFactory::UpdatePostOperation(this, nullptr);
                } else {
                    CommentWidgetFactory::UpdateCommentOperation(this);
                }
            }
        }
        break;
    default:
        break;
    }
}

void ActionAndData::ReSetData(GraphObject *newData)
{
    if (mData) {
        mData->Release();
    }

    mData = newData;

    if (mData) {
        mData->AddRef();
    }
}

bool ActionAndData::IsNeedToDrawAttachment(const char * attachmentId)
{
    if (mAction && mAction->getScreen()) {
        int screenId = mAction->getScreen()->GetScreenId();
        if (screenId != ScreenBase::SID_HOME_SCREEN &&
            screenId != ScreenBase::SID_USER_PROFILE &&
            screenId != ScreenBase::SID_EVENT_PROFILE_PAGE &&
            screenId != ScreenBase::SID_EVENT_ALL_POSTS_SCREEN &&
            screenId != ScreenBase::SID_NOTIFICATION_SCREEN &&
            attachmentId && mEntityId && !strcmp(attachmentId, mEntityId)) {
            return false;
        }
    }

    return true;
}

//class AsyncLayoutUpdater
ActionAndData::AsyncLayoutUpdater::AsyncLayoutUpdater(LayoutType type, Evas_Object *layout, ActionAndData *actionData, int dataType, int index) : mType(type),
    mLayout(layout), mImagePath(nullptr), mRequestId(0), mActionData(actionData), mState(ENone), mDataType(dataType), mIndex(index)
{
    mDataTypeMap = {
            {Post::EGroupCover, "group_cover"},
            {Post::EProfileCover, "cover_photo"},
            {Post::EProfileAvatar, "avatar"},
            {Post::EProfileFirstPhoto, "photo_preview"},
            {Post::EProfileFirstFriend, "friend_preview"},
            {Post::EProfileAboutInfo, "about_preview"},
            {Comment::EProfileAvatar, "avatar"},
    };
}

ActionAndData::AsyncLayoutUpdater::~AsyncLayoutUpdater() {
    mLayout = nullptr;
    if(mRequestId) {
        Cancel(mRequestId);
        mRequestId = 0;
    }
    if((mState != ENone) && GetGraphObj()) {
        GetGraphObj()->CancelAsyncReq(this);
    }
    free((char *)mImagePath); mImagePath = nullptr;
    mActionData = nullptr;
}

void ActionAndData::AsyncLayoutUpdater::HandleDownloadImageResponse(ErrorCode error, MessageId requestID, const char* url, const char* fileName) {
    mState = ENone;
    if (error == EBPErrNone && fileName) {
        if (mLayout) {
            //            elm_widget_type_check(mLayout, "elm_image", "");
            if (mType == EImage) {
                std::string source;
                auto it = mDataTypeMap.find(mDataType);
                if (it != mDataTypeMap.end()) {
                    source = it->second;
                    Utils::SetImageToEvasAndFill(mLayout, source.c_str(), fileName);
                } else {
                    elm_image_file_set(mLayout, fileName, nullptr);
                }
            } else if (mType == EEvasImage) {
                evas_object_image_file_set(mLayout, fileName, nullptr);
            }
        }
        if ((mDataType != -1) && GetGraphObj()) {
            GetGraphObj()->SetStringForDataType(fileName, mDataType, mIndex);
        }
    } else {
        //ToDo
    }
}

void ActionAndData::AsyncLayoutUpdater::HandleLoadImageResponse(const char* fileName) {
    mState = ENone;
    Log::debug("AsyncLayoutUpdater::HandleLoadImageResponse, fileName = %s", fileName);

        if (mLayout) {
            if (mType == EImage) {
                std::string source;
                auto it = mDataTypeMap.find(mDataType);
                if (it != mDataTypeMap.end()) {
                    source = it->second;
                    Utils::SetImageToEvasAndFill(mLayout, source.c_str(), fileName);
                } else {
                    elm_image_file_set(mLayout, fileName, nullptr);
                }
            } else if (mType == EEvasImage) {
                evas_object_image_file_set(mLayout, fileName, nullptr);
            }
        }
        if (mDataType != -1) {
            GetGraphObj()->SetStringForDataType(fileName, mDataType, mIndex);
        }

}

void ActionAndData::AsyncLayoutUpdater::AsyncCallback(void* data, const char *result) {
    if (!result) {
        return;
    }
    ActionAndData::AsyncLayoutUpdater *me = static_cast<ActionAndData::AsyncLayoutUpdater *>(data);
    if(me) {
        me->mState = ENone;
        Log::debug("AsyncLayoutUpdater::AsyncCallback:%s", result);
        switch (me->mType) {
        case EText:
            if (result) {
                if(me->mLayout) {
                    if (me->mDataType == Post::EEventInfo) {
                        result = WidgetFactory::WrapByFormat2(SANS_REGULAR_9298A4_FORMAT, UIRes::GetInstance()->NF_LOC_INFO_TYPE_TEXT_SIZE, result );
                        elm_object_text_set(me->mLayout, result);
                        delete[] result;
                    } else {
                        elm_object_text_set(me->mLayout, result);
                    }
                }
            }
            break;
        case EImage:
            if (result) {
                if (me->mLayout && me->mDataType == Post::EGroupCover) {
                    elm_object_signal_emit(me->mLayout, "show", "cover");
                }
                if (!me->LoadImage(result)){
                    me->DownloadImage(result);
                }
            }
            break;
        case EVideo:
            break;
        default:
            break;
        }
    }
}

bool ActionAndData::AsyncLayoutUpdater::DownloadImage(const char* url) {
    bool res = false;
    res = Application::GetInstance()->mDataService->DownloadImage(this, url, mRequestId);
    if (res) {
        mState = EImageDownloading;
    }
    return res;
}

bool ActionAndData::AsyncLayoutUpdater::DownloadVideo(const char* url) {
    bool ReturnValue = false;
    ReturnValue = Application::GetInstance()->mDataService->DownloadImage(this, url, mRequestId);
    if (ReturnValue)
        mState = EVideoDownloading;
    return ReturnValue;
}

void ActionAndData::DownloadVideo(const char* url) {
    AsyncLayoutUpdater* AsyncUpdater = new AsyncLayoutUpdater(EVideo, nullptr, this);
    if (AsyncUpdater->DownloadVideo(url)) {
        mLayouts = eina_list_append(mLayouts, AsyncUpdater);
    } else {
        delete AsyncUpdater;
    }
}

bool ActionAndData::AsyncLayoutUpdater::LoadImage(const char* fileName) {
    bool res = false;
    char fullFileName [MAX_FULL_PATH_NAME_LENGTH];

    char * sharedPath = app_get_shared_trusted_path();
    Utils::Snprintf_s(fullFileName, MAX_FULL_PATH_NAME_LENGTH, "%s%s/%s", sharedPath, DIR_IMAGES_NAME, fileName);
    free(sharedPath);
    FILE *file = fopen(fullFileName, "r");

    if (file) {
        fclose (file);
        mImagePath = strdup(fullFileName);
        mState = EImageLoading;
        res = true;
        HandleLoadImageResponse(mImagePath);
    }
    return res;
}

bool ActionAndData::AsyncLayoutUpdater::DownloadGraphData(int dataType) {
    bool r = false;
    if(GetGraphObj()) {
        if (GetGraphObj()->isDataReady(dataType)) {
            if(mLayout) {
                const char *text = GetGraphObj()->GetStringForDataType(dataType);
                if (text) {
                    if (dataType == Post::EEventInfo) {
                        text = WidgetFactory::WrapByFormat2(SANS_REGULAR_9298A4_FORMAT, UIRes::GetInstance()->NF_LOC_INFO_TYPE_TEXT_SIZE, text);
                        elm_object_text_set(mLayout, text);
                        delete[] text;
                    } else {
                        elm_object_text_set(mLayout, text);
                    }
                }
            }
        } else {
            r = GetGraphObj()->GetAsync(dataType, this, AsyncCallback);
            if (r) mState = ETextDownloading;
        }
    }
    return r;
}

int ActionAndData::action_and_data_comparator(const void *data1, const void *data2) {
    const ActionAndData *and1 = static_cast<const ActionAndData *> (data1);
    const ActionAndData *and2 = static_cast<const ActionAndData *> (data2);
    if (and1 && and2 && (and1 == and2) && (and1->mTimeStamp == and2->mTimeStamp)) {
        return 0;
    } else {
        return -1;
    }
}


//ActionBase
ActionBase::ActionBase()
{
    mScreen = nullptr;
    event_name = nullptr;
    mGrpReqObjectType = nullptr;
    mLikeRequest = nullptr;
    mConfirmationDialog = nullptr;
}

ActionBase::ActionBase(ScreenBase *screen) : mScreen(screen) {
    event_name = nullptr;
    mGrpReqObjectType = nullptr;
    mLikeRequest = nullptr;
    mConfirmationDialog = nullptr;
}

ActionBase::~ActionBase()
{
    if(mGrpReqObjectType)
    {
        FacebookSession::GetInstance()->ReleaseGraphRequest(mGrpReqObjectType);
        mGrpReqObjectType = nullptr;
    }

    if (mLikeRequest) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(mLikeRequest);
    }
    DeleteConfirmationDialog();
}

ScreenBase *ActionBase::getScreen() {
    return mScreen;
}

void ActionBase::on_operation_more_info_btn_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    if(actionAndData) {
        actionAndData->mAction->OperationMoreInfoBtnClicked(actionAndData);
    }
}

void ActionBase::on_avatar_clicked_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    actionAndData->mAction->AvatarClicked(actionAndData->mData);
}

void ActionBase::on_text_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    if(actionAndData && actionAndData->mAction) {
        actionAndData->mAction->TextClicked(actionAndData);
    }
}

void ActionBase::on_comment_btn_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::debug("ActionBase::on_comment_btn_clicked_cb");
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    if(actionAndData && actionAndData->mAction) {
        if ( Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_IMAGE_POST &&
             Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_IMAGE_CAROUSEL )
        {
            SoundNotificationPlayer::PlayNotification(SoundNotificationPlayer::eSoundComment);
        }
        actionAndData->mAction->CommentBtnClicked(actionAndData);
    }
}

void ActionBase::on_like_btn_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::info("ActionBase::on_like_btn_clicked_cb");
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    if (actionAndData && actionAndData->mAction && actionAndData->mData) {
        actionAndData->mAction->LikeBtnClicked(actionAndData);
    }
}

void ActionBase::on_photo_box_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source) {
    Log::debug("ActionBase::on_photo_box_clicked_cb, source:%s", source);
    if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_IMAGE_POST) {
        ActionAndData *actionAndData = static_cast<ActionAndData*>(data);
        char *end;
        int index = strtol((source + strlen(source) - 1), &end , 10);
        index = (index > 4 || index < 0) ? 0 : index;
        Log::debug("ActionBase::on_photo_box_clicked_cb, index: %d", index);
        OpenImageFromPost(actionAndData, index);
    }
}
/**
 * @brief - This callback is called when Share Now action is clicked
 * @param[in] user_data - user data passed in callback
 * @param[in] obj - object which generates event
 * @param[in] event_info - event information
 */
void ActionBase::on_share_now_clicked(void *user_data, Evas_Object *obj,
        void *event_info)
{
    Log::debug("ActionBase::on_share_now_clicked");
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    if (ActionAndData::IsAlive(actionAndData)) {
        actionAndData->mAction->ShareNowClicked(actionAndData->mData);
    } else {
        Utils::ShowToast(Application::GetInstance()->mNf, i18n_get_text("IDS_LOADING_DATA"));
    }
}

/**
 * @brief - This callback is called when Share With Post action is clicked
 * @param[in] user_data - user data passed in callback
 * @param[in] obj - object which generates event
 * @param[in] event_info - event information
 */
void ActionBase::on_share_with_post_clicked(void *user_data, Evas_Object *obj, void *event_info)
{
    Log::debug("ActionBase::on_share_with_post_clicked");
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    SoundNotificationPlayer::PlayNotification(SoundNotificationPlayer::eSoundShare);
    actionAndData->mAction->ShareWithPostClicked(actionAndData->mData);
}
/**
 * @brief - This callback is called when Link Item is clicked
 */
void ActionBase::on_link_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::debug("ActionBase::on_link_clicked");
    ActionAndData *actionAndData = static_cast<ActionAndData*>(data);
    actionAndData->mAction->LinkClicked(actionAndData->mData);
}

/**
 * @brief - This callback is called when Map Item is clicked
 */
void ActionBase::on_map_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::debug("ActionBase::on_map_clicked");
    ActionAndData *actionAndData = static_cast<ActionAndData*>(data);
    actionAndData->mAction->MapClicked(actionAndData);
}

void ActionBase::on_page_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::debug("ActionBase::on_page_clicked");
    ActionAndData *actionAndData = static_cast<ActionAndData*>(data);
    actionAndData->mAction->PageClicked(actionAndData->mData);
}

void ActionBase::on_new_user_friends_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::debug("ActionBase::on_link_clicked");
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    actionAndData->mAction->NewUserFriendsClicked(actionAndData->mData);
}

void ActionBase::on_photo_clicked_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    ActionAndData *actionAndData = (ActionAndData*) user_data;
    actionAndData->mAction->PhotoClicked(actionAndData->mData);
}

void ActionBase::on_video_post_clicked_cb(void *user_data, Evas_Object *obj,
        void *event_info) {
    ActionAndData *actionAndData = (ActionAndData*) user_data;
    actionAndData->mAction->PostVideoClicked(static_cast<ActionAndData*>(user_data));
}

void ActionBase::on_event_info_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::info("on_event_info_clicked_cb");
    ActionAndData *actionAndData = static_cast<ActionAndData*>(data);
    actionAndData->mAction->EventInfoClicked(actionAndData->mData);
}

void ActionBase::on_anchor_clicked_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    Log::info("on_anchor_clicked_cb");
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    if (actionAndData) {
        assert(actionAndData->mAction);
        Elm_Label_Anchor_Info *event = static_cast<Elm_Label_Anchor_Info*>(event_info);
        if (event) {
            Log::info("Elm_Label_Anchor_Info, Name: %s", event->name);
            if (event->name && event->name[0] == 'p') {
                const char *id = event->name + 2;
                if (!actionAndData->mAction->IsNestedProfile(id))
                    actionAndData->mAction->mGrpReqObjectType = actionAndData->mAction->LoadObjectDetails(actionAndData->mAction, id, false);
            } else {
                actionAndData->mAction->AnchorClicked(actionAndData, event->name);
            }
        }
    }
    elm_layout_signal_emit(elm_object_parent_widget_get(obj), "delete_cb", "");
}

void ActionBase::on_ctxmenu_notification_clicked_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    ActionAndData *actionAndData = (ActionAndData*) user_data;
    actionAndData->mAction->CtxMenuNotificationClicked(actionAndData->mData);
}

void ActionBase::on_ctxmenu_hide_this_clicked_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    ActionAndData *actionAndData = (ActionAndData*) user_data;
    actionAndData->mAction->CtxMenuHideThisClicked(actionAndData->mData);
}

void ActionBase::on_ctxmenu_unfollow_user_clicked_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    ActionAndData *actionAndData = (ActionAndData*) user_data;
    actionAndData->mAction->CtxMenuUnfollowUserClicked(actionAndData->mData);
}

void ActionBase::on_ctxmenu_edit_post_option_clicked_cb(void *user_data, Evas_Object *obj,
        void *event_info)
{
    Log::debug("ActionBase::on_ctxmenu_edit_post_option_clicked_cb");
    ActionAndData *actionAndData = (ActionAndData*) user_data;
    actionAndData->mAction->CtxEditPostOptionClicked(actionAndData);
}

void ActionBase::on_ctxmenu_approved_detete_post_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::debug("ActionBase::on_ctxmenu_approved_detete_post_clicked_cb");
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    actionAndData->mAction->CtxApprovedDeletePostClicked(actionAndData);
}

void ActionBase::on_ctxmenu_approved_delete_cmnt_clicked_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    Log::debug("ActionBase::on_ctxmenu_approved_detete_post_clicked_cb");
    ActionAndData *actionAndData = (ActionAndData*) user_data;
    actionAndData->mAction->CtxApprovedDeleteCmntClicked(actionAndData);
}

void ActionBase::on_ctxmenu_edit_privacy_option_clicked_cb(void *user_data, Evas_Object *obj,
        void *event_info)
{
    Log::debug("ActionBase::on_ctxmenu_edit_privacy_option_clicked_cb");
    ActionAndData *actionAndData = (ActionAndData*) user_data;
    actionAndData->mAction->CtxEditPrivacyOptionClicked(actionAndData);
}

void ActionBase::on_stats_footer_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    assert(user_data);
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    assert(actionAndData->mData);
    Post *post = static_cast<Post *>(actionAndData->mData);

    if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_IMAGE_POST &&
        Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_IMAGE_CAROUSEL)
    {
        SoundNotificationPlayer::PlayNotification(SoundNotificationPlayer::eSoundComment);
    }

    if ( post->GetCommentsCount() != 0 ) {
        actionAndData->mAction->CommentBtnClicked(actionAndData);
    } else {
        actionAndData->mAction->LikesCountClicked(actionAndData->mData);
        OperationManager::GetInstance()->AddOperation(MakeSptr<LikeOperation<Post>>(post->GetId() ? post->GetId() : "",
                                                                                    Operation::OT_GetLikesSummary,
                                                                                    actionAndData));
    }

}

void ActionBase::on_likes_count_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::debug("ActionBase::on_likes_count_clicked_cb");
    ActionAndData *actionAndData = (ActionAndData*) user_data;
    actionAndData->mAction->LikesCountClicked(actionAndData->mData);
}

void ActionBase::on_unfollow_user_clicked_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    actionAndData->mAction->UnfollowUser(actionAndData);
}

/* Friend Requests */

void ActionBase::on_pymk_btn_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    actionAndData->mAction->PymkBtnClicked(actionAndData);
}

void ActionBase::on_fr_carousel_btn_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    actionAndData->mAction->FriendRequestCarouselBtnClicked(actionAndData);
}

void ActionBase::on_add_friend_btn_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    actionAndData->mAction->AddFriendBtnClicked(actionAndData);
}

void ActionBase::on_cancel_friend_btn_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    actionAndData->mAction->CancelFriendBtnClicked(actionAndData);
}

void ActionBase::on_confirm_friend_rq_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    actionAndData->mAction->ConfirmOrDeleteFriendRequest(actionAndData, true);
}

void ActionBase::on_delete_friend_rq_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    actionAndData->mAction->ConfirmOrDeleteFriendRequest(actionAndData, false);
}

void ActionBase::on_unfriend_friend_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    actionAndData->mAction->UnfriendFriendBtnClicked(actionAndData);
}

void ActionBase::on_block_friend_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    actionAndData->mAction->BlockFriendBtnClicked(actionAndData);
}

void ActionBase::on_event_going_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    actionAndData->mAction->EventGoingBtnClicked(actionAndData);
}

void ActionBase::elm_on_event_going_btn_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    actionAndData->mAction->EventGoingBtnClicked(actionAndData);
}

void ActionBase::on_event_maybe_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    actionAndData->mAction->EventMaybeBtnClicked(actionAndData);
}

void ActionBase::elm_on_event_maybe_btn_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    actionAndData->mAction->EventMaybeBtnClicked(actionAndData);
}

void ActionBase::on_event_not_going_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    actionAndData->mAction->EventNotGoingBtnClicked(actionAndData);
}

void ActionBase::elm_on_event_not_going_btn_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    actionAndData->mAction->EventNotGoingBtnClicked(actionAndData);
}

void ActionBase::run_messanger_cb(void *data, Evas_Object *obj, const char *emission, const char *source) {
    if (Messenger::IsMessengerInstalled()) {
        Messenger::LaunchMessenger();
    } else {
        Messenger* NewMessengerScreen = new Messenger(nullptr);
        Application::GetInstance()->AddScreen(NewMessengerScreen);
    }
}

void ActionBase::OnCtxMenuApprovedDeleteEventClicked(void *user_data)
{
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    actionAndData->mAction->CtxApprovedDeleteEvent(actionAndData);
}

void ActionBase::retry_post_send_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    ActionAndData *actionAndData = static_cast<ActionAndData*>(user_data);
    actionAndData->mAction->RetryPostSend(actionAndData);
}

void ActionBase::OperationMoreInfoBtnClicked(ActionAndData *actionAndData)
{

}

void ActionBase::CancelPostingOperation(ActionAndData *actionAndData)
{

}

void ActionBase::RetryPostSend(ActionAndData *actionAndData) {

}

void ActionBase::AvatarClicked(GraphObject *graphObject)
{
    //Nothing here
}

void ActionBase::PostClicked(GraphObject *graphObject)
{
    //Nothing here
}

void ActionBase::LikeBtnClicked(ActionAndData *user_data)
{
    if (!user_data) {
        return;
    }

    assert(user_data->mData);

    // true - do like; false - do unlike
    bool doLike = true;

    switch (user_data->mData->GetGraphObjectType()) {
    case GraphObject::GOT_POST: {
        Post *post = static_cast<Post *>(user_data->mData);
        if (post->GetParentId() && Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_IMAGE_POST) {
            post = post->GetParent();
        }
        doLike = !post->GetHasLiked();
        post->ToggleLike();
        if (doLike) {
            if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_IMAGE_POST &&
                Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_IMAGE_CAROUSEL)
            {
                SoundNotificationPlayer::PlayNotification(SoundNotificationPlayer::eSoundLikeMain);
            }
        }
        const char* objectId = post->GetId();
        AppEvents::Get().Notify(eLIKE_COMMENT_EVENT, const_cast<char *>(objectId));
        OperationManager::GetInstance()->AddOperation(MakeSptr<LikeOperation<Post>>(objectId ? objectId : "",
                                                                                    doLike? Operation::OT_Like: Operation::OT_Unlike,
                                                                                    user_data));
    }
        break;
    case GraphObject::GOT_PHOTO: {
        Photo *photo = static_cast<Photo *>(user_data->mData);
        doLike = !photo->GetHasLiked();
        photo->ToggleLike();
        const char* objectId = photo->GetId();
        AppEvents::Get().Notify(eLIKE_COMMENT_EVENT, const_cast<char *>(objectId));
        OperationManager::GetInstance()->AddOperation(MakeSptr<LikeOperation<Photo>>(objectId ? objectId : "",
                                                                                     doLike? Operation::OT_Like: Operation::OT_Unlike,
                                                                                     user_data));
    }
        break;
    default:
        return;
    }
    WidgetFactory::RefreshPostItemFooter(user_data);

}

void ActionBase::TextClicked(ActionAndData *userData) {
    SoundNotificationPlayer::PlayNotification(SoundNotificationPlayer::eSoundComment);
    OpenCommentScreen(userData);
}


void ActionBase::CommentBtnClicked(ActionAndData *userData) {
    OpenCommentScreen(userData);
}

void ActionBase::OpenCommentScreen(ActionAndData *userData) {
    if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_COMMENTS) {
        CommentScreen* commentScreen = new CommentScreen(mScreen, userData);
        Application::GetInstance()->AddScreen(commentScreen);
    }
}

/**
 * @brief - This method is called when Share Now action is clicked
 * @param[in] graphObject - object to share
 */
void ActionBase::ShareNowClicked(GraphObject *graphObject)
{
    //ShareBtnClicked
}

/**
 * @brief - This method is called when Share With Post  action is clicked
 * @param[in] graphObject - object to share
 */
void ActionBase::ShareWithPostClicked(GraphObject *graphObject)
{

}

void ActionBase::LinkClicked(GraphObject *graphObject)
{

}

void ActionBase::MapClicked(ActionAndData * user_data)
{
    Log::info("ActionBase::MapClicked, id = %s", user_data->mData->GetId());

    if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_MAP_VIEW) {
        Application::GetInstance()->AddScreen(new MapViewScreen(user_data));
    }
}

void ActionBase::PageClicked(GraphObject *graphObject)
{

}

void ActionBase::NewUserFriendsClicked(GraphObject *graphObject)
{

}

void ActionBase::StatsClicked(GraphObject *graphObject)
{
    //Nothing here
}

void ActionBase::ContextMenuClicked(GraphObject *graphObject)
{
    //Nothing here
}

void ActionBase::PhotoClicked(GraphObject *graphObject)
{
    //Nothing here
}


void ActionBase::PostVideoClicked(ActionAndData * user_data)
{
    // Nothing here
}

void ActionBase::CommunityNameClicked(GraphObject *graphObject)
{
    //Nothing here
}

void ActionBase::CommunityLikeClicked(GraphObject *graphObject)
{
    //Nothing here
}

void ActionBase::LocationMapClicked(GraphObject *graphObject)
{
    //Nothing here
}

void ActionBase::LocationAddBtnClicked(GraphObject *graphObject)
{
    //Nothing here
}

void ActionBase::LikePageBtnClicked(GraphObject *graphObject)
{
    //Nothing here
}

void ActionBase::LocationInfoClicked(GraphObject *graphObject)
{
    //Nothing here
}

void ActionBase::EventInfoClicked(GraphObject *graphObject)
{
    //Nothing here
}

Eina_List *ActionBase::ParseMultiAnchor(const char*anchor) {
    char *pch;
    Eina_List *ret = nullptr;
    char *ids = Utils::getCopy(anchor + 2); //2 for "m:"
    pch = strtok (ids, ",");
    while (pch) {
        ret = eina_list_append(ret, Utils::getCopy(pch));
        pch = strtok (nullptr, ",");
    }
    delete []ids;
    return ret;
 }

void ActionBase::AnchorClicked(ActionAndData *data, const char *anchorName)
{
    if (data) {
        if (anchorName && strlen(anchorName) > 2) {
            // anchorName has format "<kind>_<id>"
            // <kind> indicates type of anchor:
            //  c - community - profile screen should be opened
            //  u - user - profile screen should be opened
            //  p - page - profile screen should be opened
            //  g - group - profile screen should be opened
            //  s - search query for screen screen
            //  l - url - web view should be opened
            //  d - url - web view with deep linking
            //  r - read_more
            // <id> - object id
            // i - image (= photo)
            // v - video
            // a - album
            // m: - multiple. When the anchor contains more than one Id. E.g. "m:u_100009449351076,u_100009370306236".
            char *id = strdup((const char *)(anchorName+2));
            if(id && !data->mAction->IsNestedProfile(id)) {
                ScreenBase *scr = nullptr;
                char anchorType = anchorName[0];
                if (anchorType == 'c' || anchorType == 'u') {
                    Utils::OpenProfileScreen(id);
                } else if (anchorType == 'g') {
                    scr = new GroupProfilePage(id, nullptr);
                } else if (anchorType == 'l') {
                    if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_WEB_VIEW) {
                        if(Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_COMMENTS){
                            WebViewScreen::Launch(id, false, true, data);
                        } else {
                            WebViewScreen::Launch(id, false, true);
                        }
                    }
                } else if (anchorType == 'd') {
                    WebViewScreen::Launch(id, true, true);
                } else if (anchorType == 'p') {
                    scr = new ProfileScreen(id);
                } else if (anchorType == 's') {
                    if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_WEB_VIEW) {
                        char targetUrl[MAX_REQ_WEBURL_LEN] = { 0, };
                        char cuttedId[MAX_REQ_WEBURL_LEN];
                        int j = 0;
                        for (int i = 0; i < strlen(id); i++) {
                            if (i != 0) {
                                cuttedId[j] = id[i];
                                j++;
                            }
                        }
                        cuttedId[j] = 0;
                        Log::debug("Cutted id is %s", cuttedId);
                        Utils::Snprintf_s(targetUrl, MAX_REQ_WEBURL_LEN, URL_HASHTAG_PAGE, cuttedId);
                        WebViewScreen::Launch(targetUrl, false, true);
                    }
                } else if (anchorType == 'm') {
                    Eina_List *ids = ParseMultiAnchor(anchorName);
                    scr = new SelectUserScreen(nullptr, ids);
                    void *listData;
                    EINA_LIST_FREE(ids, listData) {
                        delete [] listData;
                    }

                } else if (anchorType == 'v' || !strcmp(anchorName, "read_more")) {
                    scr = new PostScreen(data);
                } else if (anchorType == 'a') {
                    Post *post = dynamic_cast<Post *> (data->mData);
                    scr = new AlbumGalleryScreen(post->GetAlbum(), static_cast<PhotoGalleryStrategyBase *> (new PhotoGalleryShowCarouselStrategy()));
                } else if (anchorType == 'e') {
#ifdef EVENT_NATIVE_VIEW
                    scr = new EventProfilePage(id, nullptr, nullptr);
#else
                    char targetUrl[MAX_REQ_WEBURL_LEN] = { 0, };
                    Utils::Snprintf_s(targetUrl, MAX_REQ_WEBURL_LEN, URL_EVENT_PROFILE_PAGE, id);
                    WebViewScreen::Launch(targetUrl, true);
#endif
                } else if (anchorType == 'i') {
                    if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_IMAGE_POST) {
                        OpenImageFromPost(data, 0);
                    }
                }
                if (scr) {
                    Application::GetInstance()->AddScreen(scr);
                }
                free(id);
            }
        }
    }
}

void ActionBase::StatusBtnClicked(GraphObject *graphObject)
{
    //Nothing here
}

void ActionBase::PhotoAddBtnClicked(GraphObject *graphObject)
{
    //Nothing here
}

void ActionBase::CheckInBtnClicked(GraphObject *graphObject)
{
    //Nothing here
}

void ActionBase::CtxMenuNotificationClicked(GraphObject *graphObject)
{
    //
}

void ActionBase::CtxMenuHideThisClicked(GraphObject *graphObject)
{
    //
}

void ActionBase::CtxMenuUnfollowUserClicked(GraphObject *graphObject)
{
    //
}

void ActionBase::CtxEditPostOptionClicked(ActionAndData *actionData)
{
    Operation::OperationDestination opDest = Operation::OD_None;
    const char * targetId = nullptr;
    const char * targetName = nullptr;
    const char * targetPrivacy = nullptr;
    Post * post = static_cast<Post *>(actionData->mData);
    if (post->IsPostWithTarget()) {
        targetId = post->GetToId();
        targetName = post->GetToName();
        GraphObject * targetObject = Utils::FindGraphObjectInProvidersById(targetId);
        if (targetObject) {
            switch (targetObject->GetGraphObjectType()) {
            case GraphObject::GOT_FRIEND:
                opDest = Operation::OD_Friend_Timeline;
                break;
            case GraphObject::GOT_GROUP: {
                opDest = Operation::OD_Group;
                Group * group = static_cast<Group *>(targetObject);
                targetPrivacy = group->GetPrivacyAsStr();
            }
                break;
            case GraphObject::GOT_EVENT: {
                opDest = Operation::OD_Event;
                Event * event = static_cast<Event *>(targetObject);
                targetPrivacy = event->mPrivacyStatus ? event->mPrivacyStatus : "";
            }
                break;
            default:
                break;
            }
        } else {
            // TODO do 2 additional requests. 1) get type of object. 2) get privacy fields
        }
    } else {
        opDest = Operation::OD_Home;
    }

    PostComposerScreen* postComposer = new PostComposerScreen(mScreen, eEDIT, actionData, opDest, targetId, targetName,
            targetPrivacy);
    Application::GetInstance()->AddScreen(postComposer);
}

void ActionBase::CtxEditPrivacyOptionClicked(ActionAndData *actionData)
{
    //
}

void ActionBase::CtxApprovedDeletePostClicked(ActionAndData *actionData){
	//
}

void ActionBase::CtxApprovedDeleteCmntClicked(ActionAndData *actionData){
    //
}

void ActionBase::CtxApprovedDeleteEvent(ActionAndData *actionData)
{
    //
}

void ActionBase::SendCommentClicked(GraphObject *graphObject)
{
    //
}

void ActionBase::BlockUser(ActionAndData *actionData)
{
    //
}

void ActionBase::UnblockUser(ActionAndData *actionData)
{
    //
}


void ActionBase::UnfollowUser(ActionAndData *actionData)
{
    //
}

void ActionBase::PymkBtnClicked(ActionAndData *actionData)
{
    //
}

void ActionBase::FriendRequestCarouselBtnClicked(ActionAndData *actionData)
{
    //
}

void ActionBase::AddFriendBtnClicked(ActionAndData *actionData)
{
    //
}

void ActionBase::CancelFriendBtnClicked(ActionAndData *actionData)
{
    //
}

void ActionBase::ConfirmOrDeleteFriendRequest(ActionAndData *actionData, bool isConfirmed)
{
    //
}

void ActionBase::UnfriendFriendBtnClicked(ActionAndData *actionData)
{
    //
}

void ActionBase::DoUnfriend(ActionAndData *actionData)
{
    //
}

void ActionBase::BlockFriendBtnClicked(ActionAndData *actionData)
{
    //
}

void ActionBase::DoBlock(ActionAndData *actionData)
{
    //
}

void ActionBase::LikesCountClicked(GraphObject *graphObject) {
    Log::debug("ActionBase::LikesCountClicked");
    if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_WHO_LIKED) {
        WhoLikedScreen* whoLikedScreen = new WhoLikedScreen(graphObject->GetId());
        Application::GetInstance()->AddScreen(whoLikedScreen);
    }
}

GraphRequest *ActionBase::LoadObjectDetails(void *data, const char *id, bool isLinkNeeded) {
    if (ConnectivityManager::Singleton().IsConnected()) {
        mScreen->LockScreen();
        return FacebookSession::GetInstance()->GetObjectType(on_object_type_details_recv_cb, data, id, isLinkNeeded);
    } else {
        notification_status_message_post(i18n_get_text("IDS_GEN_NETWORK_ERROR"));
        return nullptr;
    }
}

void ActionBase::on_object_type_details_recv_cb(void * data, char* response, int status)
{
    Log::debug("ActionBase::on_object_type_details_recv_cb()");
    ActionBase *self = static_cast<ActionBase *>(data);
    self->mScreen->UnlockScreen();
    FacebookSession::GetInstance()->ReleaseGraphRequest(self->mGrpReqObjectType);
    self->mGrpReqObjectType = nullptr;
    //The char * obj contains the response received and the status is the curl status
    if( CURLE_OK == status ) {
        //Create a Post Item and add it to the screen
        JsonObject* rootObject = nullptr;
        JsonParser* parser = openJsonParser(response, &rootObject);
        if( rootObject && parser ) {
             JsonObject *metaDataObj = json_object_get_object_member(rootObject, "metadata");
             if( metaDataObj ) {
                 char *obj_type = GetStringFromJson(metaDataObj, "type");
                 Log::debug("Object type is %s", obj_type);
                 const char* Id = json_object_get_string_member(rootObject, "id");
                 if (!obj_type || !Id || self->IsNestedProfile(Id))
                     return;

                 if(!strcmp(obj_type,"user")) {
                     char *userId =  GetStringFromJson(rootObject, "id");
                     Utils::OpenProfileScreen(userId);
                 } else if (!strcmp(obj_type,"group")) {
                     //get the Group name
                     char * grp_name =  GetStringFromJson(rootObject, "name");
                     char * grp_id =  GetStringFromJson(rootObject, "id");
                     Log::debug("Group name and id %s %s", grp_name, grp_id);

                     //Open the group profile
                     GroupProfilePage *tmp = new GroupProfilePage(grp_id,grp_name);
                     Application::GetInstance()->AddScreen(tmp);
                 } else if (!strcmp(obj_type,"event")) {
                     //get the Event name
                     char * name =  GetStringFromJson(rootObject, "name");
                     char * id =  GetStringFromJson(rootObject, "id");
                     Log::debug("Event name and id %s %s", name, id);
#ifdef EVENT_NATIVE_VIEW
                     //Open the event profile
                     EventProfilePage *tmp = new EventProfilePage(id, name, nullptr);
                     Application::Get()->AddScreen(tmp);
#else
                     char targetUrl[MAX_REQ_WEBURL_LEN] = { 0, };
                     Utils::Snprintf_s(targetUrl, MAX_REQ_WEBURL_LEN, URL_EVENT_PROFILE_PAGE, id);
                     WebViewScreen::Launch(targetUrl, true);
#endif
                 } else {
                     char * id =  GetStringFromJson(rootObject, "id");
                     self->mGrpReqObjectType = FacebookSession::GetInstance()->GetObjectType(WebViewLaunch, data, id, true);
                 }
            }
        }
        g_object_unref(parser);
    } else {
        //There was error response, hence not able to get the object type, ignore the request
        Log::error("Error in getting object type");
    }

    free(response);
}

void ActionBase::WebViewLaunch(void * data, char* response, int status)
{
    if (CURLE_OK == status) {
        JsonObject* JsonRootObj = nullptr;
        JsonParser* JsonParser = openJsonParser(response, &JsonRootObj);
        if (JsonRootObj) {
            const char* Link = json_object_get_string_member(JsonRootObj, "link");
            if (Link)
                WebViewScreen::Launch(Link, true);
        }

        if (JsonParser)
            g_object_unref(JsonParser);
    } else {
        Utils::ShowToast(Application::GetInstance()->mNf, i18n_get_text("IDS_TRYAGAIN"));
    }
}

void ActionBase::EventGoingBtnClicked(ActionAndData *actionData)
{
    //
}

void ActionBase::EventMaybeBtnClicked(ActionAndData *actionData)
{
    //
}

void ActionBase::EventNotGoingBtnClicked(ActionAndData *actionData)
{
    //
}

bool ActionBase::IsNestedProfile(const char* ProfileId) {
    if (ProfileId && mScreen) {
        const char* parentProfileId = nullptr;

        ScreenBase *screen = Application::GetInstance()->GetParentFeedScreen();
        int screenId = screen->GetScreenId();

        if (screenId == ScreenBase::SID_USER_PROFILE)
            parentProfileId = static_cast<ProfileScreen*>(screen)->GetId().c_str();
        else if (screenId == ScreenBase::SID_OWN_PROFILE)
            parentProfileId = static_cast<OwnProfileScreen*>(screen)->GetId().c_str();
        else if (screenId == ScreenBase::SID_GROUP_PROFILE_PAGE)
            parentProfileId = static_cast<GroupProfilePage*>(screen)->GetProfileId();

        if (parentProfileId)
            return (!strcmp(ProfileId, parentProfileId));
    }
    return false;
}

bool ActionBase::DeleteConfirmationDialog() {
    if (mConfirmationDialog) {
        delete mConfirmationDialog;
        mConfirmationDialog = nullptr;
        mCnfDialogType = CDT_GENERIC;
        return true;
    }
    return false;
}

void ActionBase::CreateConfirmationDialog(Evas_Object *parent, const char* caption, const char* description, const char* yesText, const char* noText,
        ConfirmationDialog::ConfirmationDialogCB callBack, void *userData) {
    mConfirmationDialog = ConfirmationDialog::CreateAndShow(parent, caption, description, yesText, noText, callBack, userData);
}

void ActionBase::CreateConfirmationDialog(Evas_Object *parent, const char* caption, const char* description, CNF_DIALOG_TYPE cnfDialogType,
        ConfirmationDialog::ConfirmationDialogCB callBack, void *userData) {
    mCnfDialogType = cnfDialogType;
    if (CDT_CANCEL_CONFIRM == mCnfDialogType) {
        mConfirmationDialog = ConfirmationDialog::CreateAndShow(parent, caption, description, "IDS_CANCEL", "IDS_CONFIRM", callBack, userData);
    } else if (CDT_CANCEL_BLOCK == mCnfDialogType) {
        mConfirmationDialog = ConfirmationDialog::CreateAndShow(parent, caption, description, "IDS_CANCEL", "IDS_BLOCK", callBack, userData);
    } else {
      Log::error("ActionBase::CreateConfirmationDialog() - dialog type=%d is not supported", mCnfDialogType);
    }
}

void ActionBase::OpenImageFromPost(ActionAndData *actionAndData, int imageNumber) {
    if (actionAndData) {
        ScreenBase *screen = nullptr;
        Post *post = static_cast<Post *>(actionAndData->mData);
        if (post && eina_list_count(post->GetPhotoList()) == 1) {
            Photo* photo = static_cast<Photo*>(eina_list_data_get(post->GetPhotoList()));
            Eina_List *list = eina_list_append(nullptr, photo);
            screen = new ImagesCarouselScreen(list, 0, ImagesCarouselScreen::eDATA_MODE, NULL, actionAndData);
            list = eina_list_free(list);
        } else {
            screen = new ImagePostScreen(actionAndData, imageNumber);
        }
        Application::GetInstance()->AddScreen(screen);
    }
}
