#include "CommentScreen.h"
#include "Common.h"
#include "ImagePostAction.h"
#include "Log.h"
#include "OperationManager.h"
#include "PostComposerScreen.h"
#include "SimplePostOperation.h"

ImagePostAction::ImagePostAction(ScreenBase *screen)
{
    mScreen = screen;
}

/**
 * @brief This method is called when Share Now action is clicked
 * @param graphObject - object which should be shared
 */
void ImagePostAction::ShareNowClicked(GraphObject *graphObject)
{
    Log::debug("ImagePostAction::ShareNowClicked, id = %s", graphObject->GetId());

    notification_status_message_post(i18n_get_text("IDS_SHARE_POSTING"));

    bundle* paramsBundle = bundle_create();
    Post *sharedPost = static_cast<Post*> (graphObject);
    assert(sharedPost);
    bundle_add_str(paramsBundle, "id", sharedPost->GetId());
    std::list<Friend> taggedFriends;
    OperationManager::GetInstance()->AddOperation(MakeSptr<SimplePostOperation>(SimplePostOperation::OT_Share_Post_Create,
                                                                                "",
                                                                                paramsBundle,
                                                                                taggedFriends,
                                                                                "", // privacy type
                                                                                nullptr, // place,
                                                                                Operation::OD_Home , // destination,
                                                                                sharedPost));

    bundle_free(paramsBundle);
}

/**
 * @brief This method is called when Share With Post  action is clicked
 * @param graphObject - object which should be shared
 */
void ImagePostAction::ShareWithPostClicked(GraphObject *graphObject)
{
    Log::info("ImagePostAction::ShareWithPostClicked");
    if (graphObject->GetGraphObjectType() == GraphObject::GOT_POST) {
        if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_POST_COMPOSER_SCREEN) {
            PostComposerScreen* postComposer = new PostComposerScreen(mScreen, eSHARE, static_cast<Post *>(graphObject));
            Application::GetInstance()->AddScreen(postComposer);
        }
    } // else ToDo: What should we do?
}
