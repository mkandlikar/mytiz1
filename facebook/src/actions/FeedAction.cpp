#include "Application.h"
#include "CacheManager.h"
#include "CameraRollScreen.h"
#include "Comment.h"
#include "CommentScreen.h"
#include "Common.h"
#include "ConnectivityManager.h"
#include "Error.h"
#include "ErrorHandling.h"
#include "EventProfilePage.h"
#include "FacebookSession.h"
#include "FbRespondEditPostMessage.h"
#include "FbRespondGetCommentsList.h"
#include "FeedAction.h"
#include "FindFriendsScreen.h"
#include "FriendOperation.h"
#include "GroupProfilePage.h"
#include "HomeProvider.h"
#include "HomeScreen.h"
#include "Log.h"
#include "MapViewScreen.h"
#include "OperationManager.h"
#include "OperationPresenter.h"
#include "PostComposerScreen.h"
#include "PostScreen.h"
#include "SearchScreen.h"
#include "ShareWithScreen.h"
#include "SimplePostOperation.h"
#include "VideoPlayerScreen.h"
#include "WebViewScreen.h"
#include "WidgetFactory.h"

#include <assert.h>
#include <cstring>
#include <curl/curl.h>
#include <efl_extension_events.h>
#include <eina_list.h>
#include <Evas.h>
#include <malloc.h>
#include <notification.h>

FeedAction::FeedAction(ScreenBase *screen)
{
    mScreen = screen;
}

FeedAction::~FeedAction()
{
    VideoPlayerScreen::DeleteWaitDownloadTimer();
}

Post* FeedAction::getParentPost(Post *basePost)
{
    Post* res = nullptr;
    if(mScreen) {
        const char* parentId = basePost->GetParentId();
        if(parentId) {
            res = getPostById(parentId);
        }
    }
    return res;
}

Post* FeedAction::getPostById(const char* id)
{
    Post* res = nullptr;
    if( id ) {
        Eina_Array *feeds = HomeProvider::GetInstance()->GetData();
        unsigned int index;
        void* item;
        Eina_Array_Iterator iterator;
        EINA_ARRAY_ITER_NEXT( feeds, index, item, iterator ) {
            Post *post = (Post *) item;
            if(strcmp(id, post->GetId()) == 0) {
                res = post;
            }
        }
    }
    return res;
}

void FeedAction::CancelPostingOperation(ActionAndData *actionAndData)
{
    assert(actionAndData);
    OperationPresenter* op = static_cast<OperationPresenter*>(actionAndData->mData);
    assert(op);
    Sptr<Operation> operation(op->GetOperation());
    if(operation) {
        operation->Cancel();
    }
}

void FeedAction::RetryPostSend(ActionAndData *actionAndData){
    Log::info( LOG_FACEBOOK_OPERATION, "FeedAction::RetryPostSend");
    assert(actionAndData);
    OperationPresenter* op = static_cast<OperationPresenter*>(actionAndData->mData);
    assert(op);
    Sptr<Operation> operation(op->GetOperation());
    if(operation && mScreen){
        OperationManager::GetInstance()->RestartOperationAfterError(operation);
    }
}

void FeedAction::OperationMoreInfoBtnClicked(ActionAndData *actionAndData)
{
    CHECK_RET_NRV(actionAndData);
    CHECK_RET_NRV(actionAndData->mAction);
    CHECK_RET_NRV(actionAndData->mAction->getScreen());
    CHECK_RET_NRV(actionAndData->mAction->getScreen()->getMainLayout());
    WidgetFactory::ShowErrorDialog(actionAndData->mAction->getScreen()->getMainLayout(), "IDS_POST_FAILED_MORE_INFO");
}

void FeedAction::AvatarClicked(GraphObject *graphObject)
{
    CHECK_RET_NRV(graphObject);
    char *fromId = nullptr;
    if (graphObject->GetGraphObjectType() == GraphObject::GOT_POST) {
        Post *post = static_cast<Post*>(graphObject);
        fromId = strdup(post->GetFromId().c_str());
    } else if (graphObject->GetGraphObjectType() == GraphObject::GOT_PHOTO) {
        Photo *photo = static_cast<Photo*>(graphObject);
        fromId = SAFE_STRDUP(photo->GetFromId());
    } else {
        assert(false);
    }
    if (!IsNestedProfile(fromId)) {
        mGrpReqObjectType = LoadObjectDetails(this, fromId, true);
    }
    free(fromId);
}

void FeedAction::NewUserFriendsClicked(GraphObject *graphObject)
{
    Log::info("FeedAction::NewUserFriendsClicked, id = %s", graphObject ? graphObject->GetId() : "NULL");
    FindFriendsScreen* findFriendsScreen = new FindFriendsScreen(true);
    Application::GetInstance()->AddScreen(findFriendsScreen);
}

/**
 * @brief This method is called when Link item is clicked
 * @param graphObject - object which is clicked
 */
void FeedAction::LinkClicked(GraphObject *graphObject)
{
    Log::info("FeedAction::LinkClicked");
    Post* post = dynamic_cast<Post*>(graphObject);
    CHECK_RET_NRV(post);
    std::string url;
    bool EnableDeeplink = false;
    if (post->GetLink()) {
        url = post->GetLink();
    } else {
        url = post->GetAttachmentTargetUrl();
        EnableDeeplink = true;
    }
    if(!url.empty()) {
        Log::debug("FeedAction: url= %s", url.c_str());
        if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_WEB_VIEW) {
            WebViewScreen::Launch(url.c_str(), EnableDeeplink, true);
        }
    }
}

void FeedAction::PageClicked(GraphObject *graphObject)
{
    Log::info("FeedAction::PageClicked");
    Post* post = static_cast<Post*>(graphObject);
    GroupProfilePage* groupPage = new GroupProfilePage(post->GetGroupId(), post->GetGroupName());
    Application::GetInstance()->AddScreen(groupPage);
}

/**
 * @brief This method is called when Share Now action is clicked
 * @param graphObject - object which should be shared
 */
void FeedAction::ShareNowClicked(GraphObject *graphObject)
{
    assert(graphObject);
    Log::info("FeedAction::ShareNowClicked, id = %s", graphObject->GetId());

    notification_status_message_post(i18n_get_text("IDS_SHARE_POSTING"));
    bundle* paramsBundle = bundle_create();
    Post *sharedPost = static_cast<Post*> (graphObject);

    if (sharedPost->GetIsGroupPost()) {
        Log::info("FeedAction::ShareNowClicked->ShareGroupPost");
        bundle_add_str(paramsBundle, "id", sharedPost->GetId());
    } else {
        bundle_add_str(paramsBundle, "id", sharedPost->GetParentId() ? sharedPost->GetParentId() : sharedPost->GetId());
    }
    std::list<Friend> taggedFriends;
    OperationManager::GetInstance()->AddOperation(MakeSptr<SimplePostOperation>(SimplePostOperation::OT_Share_Post_Create,
                                                                                "",
                                                                                paramsBundle,
                                                                                taggedFriends,
                                                                                "", // privacy type
                                                                                nullptr, // place,
                                                                                Operation::OD_Home , // destination,
                                                                                sharedPost));

    bundle_free(paramsBundle);
}

/**
 * @brief This method is called when Share With Post  action is clicked
 * @param graphObject - object which should be shared
 */
void FeedAction::ShareWithPostClicked(GraphObject *graphObject)
{
    Log::info("FeedAction::ShareWithPostClicked");
    if (graphObject->GetGraphObjectType() == GraphObject::GOT_POST) {
        Post *post = static_cast<Post*>(graphObject);
        PostComposerScreen* postComposer = new PostComposerScreen(mScreen, eSHARE, post);
        Application::GetInstance()->AddScreen(postComposer);
    } // else ToDo: What should we do?
}

void FeedAction::StatsClicked(GraphObject *graphObject)
{
    //Nothing here
}

void FeedAction::PhotoClicked(GraphObject *graphObject)
{
    //Photo opening
}

void FeedAction::AddFriendClicked(GraphObject *graphObject)
{
    FacebookSession::GetInstance()->SendFriendRequest(graphObject->GetId(), on_add_friend_completed, this);
}

void FeedAction::on_add_friend_completed(void* object, char* respond, int code)
{
    Log::info("FeedAction::on_add_friend_completed");

    if (code == CURLE_OK) {
        Log::debug("FeedAction::response: %s", respond);
        FbRespondBase * addRespond = FbRespondBase::createFromJson(respond);
        if (!addRespond) {
            Log::error("FeedAction::Error parsing http respond");
        } else {
            if (addRespond->mError) {
                Log::error("FeedAction::Error sending add request, error = %s", addRespond->mError);
            } else if (addRespond->mSuccess) {
                //edje_object_signal_emit(elm_layout_edje_get(screen->mEObject), "hide.set", "add");
            }
        }
        delete addRespond;
    } else {
        Log::error("FeedAction::Error sending http request");
    }
    free(respond);
}

void FeedAction::CtxEditPrivacyOptionClicked(ActionAndData *actionData)
{
    if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_SHARE_WITH) {
        CHECK_RET_NRV(mScreen);
        CHECK_RET_NRV(actionData);
        CHECK_RET_NRV(actionData->mData);
        ShareWithScreen* privacy = new ShareWithScreen(mScreen, nullptr, static_cast<Post*>(actionData->mData));
        Application::GetInstance()->AddScreen(privacy);
    }
}

void FeedAction::CtxApprovedDeletePostClicked(ActionAndData *actionData)
{
    assert(actionData);
    assert(actionData->mData);
    assert(mScreen);
    GraphObject *post = actionData->mData;

    Operation::OperationDestination opDestination =  Operation::OD_Home;
    if (mScreen->GetScreenId() == ScreenBase::SID_GROUP_PROFILE_PAGE) {
        opDestination = Operation::OD_Group;
    }
#ifdef EVENT_NATIVE_VIEW
    else if (mScreen->GetScreenId() == ScreenBase::SID_EVENT_ALL_POSTS_SCREEN) {
        opDestination = Operation::OD_Event;
    }
#endif

    CacheManager::GetInstance().DeletePost(post->GetId());
    AppEvents::Get().Notify(ePOST_DELETED,const_cast<char*>(post->GetId()));

    OperationManager::GetInstance()->AddOperation(MakeSptr<SimplePostOperation>(Operation::OT_Delete_Post,
                                                                                post->GetId(),
                                                                                nullptr,
                                                                                std::list<Friend>(),
                                                                                "",
                                                                                nullptr,
                                                                                opDestination));
}

void FeedAction::PostVideoClicked(ActionAndData * user_data)
{
    VideoPlayerScreen::Launch(user_data);
}

void FeedAction::EventInfoClicked(GraphObject *graphObject)
{
    Log::info("FeedAction::EventInfoClicked");
    Post* post = static_cast<Post*>(graphObject);
    std::string attachmentTargetId(std::move(post->GetAttachmentTargetId()));
    if (!attachmentTargetId.empty()) {
        // Check for Internet Connectivity. If Net connectivity is not available, show a popup & return from this function.
        if (! ConnectivityManager::Singleton().IsConnected())
        {
            WidgetFactory::ShowAlertPopup("IDS_GEN_NETWORK_ERROR");
            return ;
        }
#ifdef EVENT_NATIVE_VIEW
        EventProfilePage *eventProfileScreen = new EventProfilePage(attachmentTargetId.c_str(), post->GetAttachmentTitle().c_str(), nullptr);
        Application::GetInstance()->AddScreen(eventProfileScreen);
#else
        char targetUrl[MAX_REQ_WEBURL_LEN] = { 0, };
        Utils::Snprintf_s(targetUrl, MAX_REQ_WEBURL_LEN, URL_EVENT_PROFILE_PAGE, attachmentTargetId.c_str());
        WebViewScreen::Launch(targetUrl, true);
#endif
    }
}

void FeedAction::UnfollowUser(ActionAndData *actionData) {
    Post *post = static_cast<Post*>(actionData->mData);

    WidgetFactory::SetPostActive(actionData, false);

    const char * userId = post->GetIsGroupPost() ? post->GetToId() : post->GetFromId().c_str();

    OperationManager::GetInstance()->AddOperation(
            MakeSptr<FriendOperation>(Operation::OT_Unfollow, userId, post->GetId()));

}

