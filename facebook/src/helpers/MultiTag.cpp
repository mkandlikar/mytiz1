#include "Common.h"
#include "MultiTag.h"
#include "WidgetFactory.h"

#include <memory>

MultiTag::MultiTag(const std::string& id, const std::string& name, const std::string& type, int offset, int length) :
    mId(id), mName(name), mOffset(offset), mLength(length)  {
    AddItem (id, type);
}

MultiTag::MultiTag(const MultiTag& other):
    mName(other.mName),
    mId(other.mId),
    mOffset(other.mOffset),
    mLength(other.mLength),
    mItems(other.mItems) {
}

MultiTag::MultiTag(MultiTag&& other):
    mName(std::move(other.mName)),
    mId(other.mId),
    mOffset(std::move(other.mOffset)),
    mLength(other.mLength),
    mItems(std::move(other.mItems)) {
}

MultiTag::~MultiTag() {
}

void MultiTag::AddItem(const std::string& id, const std::string& type) {
    mItems.push_back(TagItem(id, type));
}

std::string MultiTag::GenTag() {
    std::string items(mItems.size() == 1 ? "" : "m:");
    bool isFirst = true;
    for(auto item = mItems.begin(); item != mItems.end(); item++) {
        if(!isFirst) {
            items += ',';
        } else {
            isFirst = false;
        }
        items += item->mType + '_' + item->mId;
    }
    char *markupName = elm_entry_utf8_to_markup(mName.c_str());
    std::unique_ptr<char[]> wrappedName(WidgetFactory::WrapByFormat2(FEED_ITEM_TAG_FORMAT_2, items.c_str(), markupName));
    free(markupName);
    return wrappedName ? wrappedName.get() : "";
}
