/*
 * LoggedInUserData.cpp
 *
 *  Created on: Nov 11, 2015
 *      Author: ruibezruch
 */
#include <stdlib.h>
#include <string.h>

#include "Common.h"
#include "Log.h"
#include "LoggedInUserData.h"

LoggedInUserData::LoggedInUserData()
{
    mId = NULL;
    mName = NULL;
    mFirstName = NULL;
    mLastName = NULL;
    mCover = NULL;
    mPicture = NULL;

    mUserProfileData = NULL;
}

LoggedInUserData::~LoggedInUserData()
{
    FreeData();
}

void LoggedInUserData::SetData(const UserProfileData &object)
{
    FreeData();
    mId = SAFE_STRDUP(object.GetId());
    mName = SAFE_STRDUP(object.mName);
    mFirstName = SAFE_STRDUP(object.mFirstName);
    mLastName = SAFE_STRDUP(object.mLastName);

    if (object.mCover) {
        mCover = new CoverPhoto(*object.mCover);
    }

    if (object.mPicture) {
        mPicture = new ProfilePictureSource(*object.mPicture);
    }

    mUserProfileData = new UserProfileData(object);
}

void LoggedInUserData::FreeData()
{
    Log::info(LOG_FACEBOOK_USER, "LoggedInUserData::FreeData");

    free((void *) mId); mId = NULL;
    free((void *) mName); mName = NULL;
    free((void *) mFirstName); mFirstName = NULL;
    free((void *) mLastName); mLastName = NULL;

    if (mCover) {
        mCover->Release(); mCover = NULL;
    }

    if (mPicture) {
        delete mPicture; mPicture = NULL;
    }

    if (mUserProfileData) {
        mUserProfileData->Release(); mUserProfileData = NULL;
    }
}
