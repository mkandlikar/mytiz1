/*
 * Profiler.cpp
 *
 * Created on: Sep 16, 2015
 * Author: Dmitry.Shcherbakov@SymphonyTeleca.com
 */

#include <stdio.h>
#include "Profiler.h"

#include <dlog.h>
#include <time.h>
#include <stdlib.h>
#include "Utils.h"

const char* Profiler::OUT_FILE_NAME = "profile.data";
const char* Profiler::DLOG_PROFILING_TAG = "Facebook_Profiling";
const int Profiler::TRACE_BUFFER_LINE_SIZE = 2048;

Profiler::Profiler()
{
}

Profiler::~Profiler()
{
    Flush();
}

Profiler* Profiler::GetInstance()
{
    static Profiler profiler;
    return &profiler;
}

id_t Profiler::GenerateId()
{
    srand( time( NULL ) );
    return rand();
}

micro_secs_t Profiler::Start( id_t id, const char *format, ... )
{
    micro_secs_t time = 0;
#ifdef _DEBUG
    va_list list;
    va_start( list, format );
    time = Utils::GetCurrentTimeMicroSecs();
    Trace( START_TRACE, id, time, format, list );
    va_end( list );
#endif
    return time;
}

void Profiler::Stop( id_t id, micro_secs_t startTime, const char *format, ... )
{
#ifdef _DEBUG
    va_list list;
    va_start( list, format );
    micro_secs_t duration = Utils::GetCurrentTimeMicroSecs() - startTime;
    Trace( STOP_TRACE, id, duration, format, list );
    va_end( list );
#endif
}

void Profiler::Trace( TRACE_STATE state, id_t id, micro_secs_t time, const char *msg, va_list &list )
{
    char buffer[TRACE_BUFFER_LINE_SIZE] = {0};
    if ( state == START_TRACE ) {
        // we do not interesting for START time
        time = 0;
    }
    Utils::Snprintf_s( buffer, TRACE_BUFFER_LINE_SIZE, "[ state = %s; id = %d; duration = %lld ] ", (state == START_TRACE ? "START_TRACE" : "STOP_TRACE"), id, time );
    vsnprintf( buffer + strlen( buffer ), TRACE_BUFFER_LINE_SIZE - strlen( buffer ), msg, list );
    dlog_print( DLOG_DEBUG, DLOG_PROFILING_TAG, buffer );

}

void Profiler::Flush()
{
    // @note will be implemented later
}
