/*
 * MemoryMonitor.cpp
 *
 *  Created on: Mar 15, 2016
 *      Author: Dmitry.Shcherbakov@harman.com
 */

#include "MemoryMonitor.h"
#include "Utils.h"
#include "Log.h"
#include <dirent.h>
#include <string>
#include <vector>
#include <algorithm>
#include <app_common.h>
#include <sys/stat.h>
#include <unistd.h>

const unsigned int MemoryMonitor::MEMORY_UPPER_BOUND_MBS = 50;
const unsigned int MemoryMonitor::MEMORY_TO_DELETE_MBS = 10;

static const char *TRUSTED_PATH[ MemoryMonitor::LAST_PATH ] = {
        "Images",
        "Videos"
};

MemoryMonitor::MemoryMonitor()
{
}

MemoryMonitor::~MemoryMonitor()
{
}

void MemoryMonitor::UtilizeLocalMemory()
{
    char *trustedPath = app_get_shared_trusted_path();
    unsigned long long int totalBytes = 0;
    std::vector<MemoryMonitor::FileWrapper> files;
    for ( int category = 0; category < LAST_PATH; ++category ) {
        struct dirent *de = NULL;
        std::string path( trustedPath );
        path += TRUSTED_PATH[ category ];
        DIR *directory = opendir( path.c_str() );
        if ( directory ) {
            while ((de = readdir(directory))) {
                if ( de->d_name[ 0 ] != '.' ) {
                    std::string fName( path );
                    fName += "/";
                    fName += de->d_name;
                    struct stat st;
                    if ( stat( fName.c_str(), &st ) == 0 ) {
                        totalBytes += st.st_size;
                        files.push_back( FileWrapper( fName.c_str(), st.st_size, st.st_atim.tv_sec ) );
                    }
                }
            }
            Log::debug( "Verified total size is [%lf] mbs.", (double(totalBytes)/ (1024*1024)) );
            closedir( directory );
        }
    }
    if ( totalBytes > (MEMORY_UPPER_BOUND_MBS * 1024 * 1024) ) {
        std::sort( files.begin(), files.end() );
        Log::debug( "Start removing the oldest accessed files." );
        unsigned int balance = ( MEMORY_UPPER_BOUND_MBS - MEMORY_TO_DELETE_MBS) * 1024 * 1024;
        for( int pos = 0; (totalBytes > balance ) && pos < files.size(); ++pos ) {
            files[ pos ].DeleteFile();
            totalBytes -= files[ pos ].m_FileSize;
        }
        Log::debug( "Total size after utilization is: %lf", (double(totalBytes)/ (1024*1024)) );
    }
    free( trustedPath );
}

MemoryMonitor* MemoryMonitor::GetInstance()
{
    static MemoryMonitor monitor;
    return &monitor;
}

MemoryMonitor::FileWrapper::FileWrapper( const char *path, unsigned long long sz, unsigned long long lastAccessedTime )
{
    m_Path = NULL;
    Init( path, sz, lastAccessedTime );
}

MemoryMonitor::FileWrapper::FileWrapper( const FileWrapper &wrapper )
{
    m_Path = NULL;
    Init( wrapper.m_Path, wrapper.m_FileSize, wrapper.m_LastAccessedTime );
}

MemoryMonitor::FileWrapper::~FileWrapper()
{
    free( (void*)m_Path );
}

void MemoryMonitor::FileWrapper::Init( const char *path, unsigned long long sz, unsigned long long lastAccessedTime )
{
    if ( m_Path ) {
        free( (void*)m_Path );
    }
    m_Path = SAFE_STRDUP( path );
    m_FileSize = sz;
    m_LastAccessedTime = lastAccessedTime;
}

bool MemoryMonitor::FileWrapper::operator < ( const FileWrapper &wripper ) const
{
    return m_LastAccessedTime < wripper.m_LastAccessedTime;
}

MemoryMonitor::FileWrapper& MemoryMonitor::FileWrapper::operator = ( const FileWrapper &wrapper )
{
    Init( wrapper.m_Path, wrapper.m_FileSize, wrapper.m_LastAccessedTime );
    return *this;
}

void MemoryMonitor::FileWrapper::DeleteFile()
{
    (void)unlink( m_Path );
}
