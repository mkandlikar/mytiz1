#include "Application.h"
#include "FriendTagging.h"
#include "i18n.h"
#include "PresenterHelpers.h"
#include "Utils.h"
#include "WidgetFactory.h"

#include <app_i18n.h>
#include <memory>
#include <sstream>

namespace PresenterHelpers {
    std::string AddFriendTags(std::string text, std::list<Tag> tagList, Mode mode) {
        //Items of tag list must be sorted in ascending "offset" value order for correct convert/replacement actions
        tagList.sort();
        std::string newString;
        std::string textToMarkup;
        //"Continue reading..." anchor must not be converted to markup,
        //so it's needed to get string without this anchor
        std::string cuttedText = CuttedText(text);
        int start = 0;
        if (tagList.size() != 0) {
           std::list<MultiTag> multiTagList(GroupTags(tagList, cuttedText));
           for(auto multiIt = multiTagList.begin(); multiIt != multiTagList.end(); multiIt++) {
               textToMarkup = cuttedText.substr(start, multiIt->GetOffset() - start);
               ConvertEmojis(textToMarkup);
               char *tempSubstr = elm_entry_utf8_to_markup(textToMarkup.c_str());
               textToMarkup = tempSubstr;
               free(tempSubstr);
               AddExtraTags(textToMarkup);
               newString.append(textToMarkup);

               std::string taggedFriend;
               switch (mode) {
               case eFeed:
                   //To get friend in '<a href=*_' format
                   taggedFriend = multiIt->GenTag().c_str();
                   break;
               case eComposer: {
                   //To get friend in '<a rel=@[' format
                   const char *wrappedName = WidgetFactory::WrapByFormat2(COMPOSER_ITEM_TAG_USER, multiIt->GetId().c_str(), multiIt->GetName().c_str());
                   taggedFriend = wrappedName;
                   delete[] wrappedName;

                   break;
               }
               case eServer:
                   //To get friend in '@[:]' format
                   const char *wrappedName = WidgetFactory::WrapByFormat2(FEED_ITEM_TAG_USER_SRV, multiIt->GetId().c_str(), multiIt->GetName().c_str());
                   taggedFriend = wrappedName;
                   delete[] wrappedName;
                   break;
               }
               newString.append(taggedFriend);
               start = multiIt->GetOffset() + multiIt->GetLength();
            }
        }

        //Adding part of text after last tagged friend
        textToMarkup = cuttedText.substr(start, cuttedText.length() - start);
        ConvertEmojis(textToMarkup);
        char *tempSubstr = elm_entry_utf8_to_markup(textToMarkup.c_str());
        textToMarkup = tempSubstr;
        free(tempSubstr);
        AddExtraTags(textToMarkup);
        newString.append(textToMarkup);
        //To get markup text with correct "Continue reading..." anchor
        newString = Utils::ReplaceString(text, cuttedText, newString);
        return newString;
    }

    void ConvertEmojis(std::string& composer) {
        // Emotion icons
        Utils::FoundAndReplaceText(composer, "((3|\\}):(-)?\\))", "\U0001f608");  // 3:),}:),3:-),}:-) => ??
        Utils::FoundAndReplaceText(composer, "((0|O)(:|;)(-)?(\\)|3))", "\U0001f607");  // O:),O:-),O;),O;-),O:3,O:-3,O;3,O;-3 => ??
        Utils::FoundAndReplaceText(composer, "((:|=)(-|o|c)?(\\)|\\]|\\}))", "\U0001f604");  // :),:-) => ??
        Utils::FoundAndReplaceText(composer, "((:|=|X|x|8)(-)?D)", "\U0001f601");  // =D,=-D,XD,X-D => ??
        Utils::FoundAndReplaceText(composer, "(;(-)?(\\)|\\]|D))", "\U0001f609");  // ;-), ;),;-], ;],;-D, ;D => ??
        Utils::FoundAndReplaceText(composer, "(:(-)?\\|)", "\U0001f610");  // :|,:-| => ??
        Utils::FoundAndReplaceText(composer, "(:(-)?(\\(|\\[|c|\\{))", "\U0001f612");  // :(,:[,:c,:{,:-(,:-[,:-c,:-{ => ??
        Utils::FoundAndReplaceText(composer, "(:(-)?(P|p|b))", "\U0001f61c");  // :P,:p,:b,:-P,:-p,:-b => ??
    }

    void AddExtraTags(std::string& composer) {
        int matchCount = Utils::FindHashTagsInText(composer.c_str());
        PutFoundAnchorsInText(composer, matchCount, 's', "");

        matchCount = Utils::FindUrlInText(composer.c_str());
        PutFoundAnchorsInText(composer, matchCount, 'l', "#586C95");
    }

    std::string CuttedText(std::string& text) {
       int pos = text.find(FEED_ITEM_CUT_MESSAGE_FORMAT_1);
       if (pos != std::string::npos) {
           return text.substr(0, pos);
       }
        return text;
    }

    void RemoveTaggedFriends(std::string& composer) {
        int matchCount = Utils::FindTaggedFriendsInText(composer.c_str());
        if(!matchCount) return;
        std::string message = composer;
        std::string tag;
        std::string newTag;
        int end;
        if (!message.empty()) {
            regmatch_t* regmatch = Utils::GetRegmatch();
            int start = 0;
            for (int i = 0; i < matchCount; i++) {
                start += regmatch[i].rm_so;
                end = message.find("]", start);
                if (end != std::string::npos) {
                    tag = message.substr(start, end - start + 1);
                    newTag = tag;
                    Utils::FoundAndReplaceText(newTag, "\\@\\[[0-9]+:[0-9]+:","");
                    newTag = Utils::ReplaceString(newTag, "]", "");
                    composer = Utils::ReplaceString(composer, tag.c_str(), newTag.c_str());
                }
                start += regmatch[i].rm_eo - regmatch[i].rm_so;
            }
        }
    }

    void PutFoundAnchorsInText(std::string& composer, int matchCount, char prefix, const std::string& color) {
        if (!matchCount || !prefix) {
            return;
        }

        const char* messageBuf = elm_entry_markup_to_utf8(composer.c_str());
        const char* message = messageBuf;
        if (!Utils::isEmptyString(message)) {
            regmatch_t* regmatch = Utils::GetRegmatch();
            int pos = 0;
            for (int i = 0; i < matchCount; i++) {
                std::unique_ptr<char[]> tag(Utils::GetStringByOffset(message, regmatch[i].rm_so, regmatch[i].rm_eo));
                if(tag) {
                    int tagLength = strlen(tag.get());
                    int index = tagLength - 1;
                    while((index >=0) && tag[index] == '\n') {
                        tag[index] = 0;
                        index--;
                    }

                    bool valid_tag = true;
                    if (prefix == 's') {    //exclude pure numeric values, as Facebook does not treat them as hashtags
                        char temp_char;
                        for (valid_tag = false, index = 1; !valid_tag && index < tagLength; index++) {
                            temp_char = tag[index];
                            if (temp_char < '0' || temp_char > '9') {
                                valid_tag = true;
                            }
                        }
                    }

                    if (valid_tag) {
                        int old_length = composer.length();
                        composer = PutAnchorInText(composer, prefix, tag.get(), color, pos, true);
                        Log::debug(LOG_FACEBOOK, "prefix_tag: %c_%s ", prefix, tag.get());
                        pos += regmatch[i].rm_so + tagLength + composer.length() - old_length;
                    } else {
                        pos += regmatch[i].rm_so + tagLength;
                    }
                }
                message = message + regmatch[i].rm_eo;
            }
        }
        free((void *) messageBuf);

    }

    std::string PutAnchorInText(const std::string &text, char key, const std::string& anchor, const std::string& color, unsigned int pos, bool firstReplace) {
        if (!key || text.empty() || anchor.empty()) {
            return "";
        }
        std::stringstream href;
        href << (color.empty() ? "<b>" : "<color=" + color + ">") << "<a href=" << key << "_" << anchor << ">" << anchor << "</a>" << (color.empty() ? "</b>" : "</color>");
        return Utils::ReplaceString(text, anchor, href.str(), pos, firstReplace);
    }

    std::list<MultiTag> GroupTags(const std::list<Tag>& tagList, const std::string& text) {
        FriendTagging *friendTagging = new FriendTagging(Application::GetInstance()->mWin, nullptr);
        std::list<MultiTag> ret;
        for(auto tagIt = tagList.begin(); tagIt != tagList.end(); tagIt++) {
            //To get offsets in UTF8 text
            int offset = friendTagging->GetBytePos(text.c_str(), 0, tagIt->mOffset);
            int length = friendTagging->GetBytePos(text.c_str(), tagIt->mOffset, tagIt->mOffset + tagIt->mLength);
            char type[2] = {0};
            if(tagIt->mType.empty()) {
                if(text == "photo") {
                    type[0] = 'i';
                } else if(text == "video") {
                    type[0] = 'v';
                }
            } else {
                type[0] = tagIt->mType[0];
            }
            bool isGrouped(false);
            for(auto multiIt = ret.begin(); multiIt != ret.end(); multiIt++) {
                if(multiIt->IsSameTag(offset, length)) {
                    isGrouped = true;
                    multiIt->AddItem(tagIt->mId.c_str(), type);
                }
            }
            if(!isGrouped) {
                std::string name = text.substr(offset, length);
                if(!name.empty()) {
                    ret.push_back(std::move(MultiTag(tagIt->mId, name.c_str(), type, offset, length)));
                }
            }
        }
        delete friendTagging;
        return std::move(ret);
    }

    std::string CreateMutualFriendsFormat(int count, const char *textSize) {
        std::string MutualFriendsFotmat;
        MutualFriendsFotmat.append(SANS_MEDIUM_9298A4_FORMAT);
        int pos = MutualFriendsFotmat.find("%s");
        MutualFriendsFotmat.replace(pos, 2, textSize);
        pos = MutualFriendsFotmat.find("%s");
        MutualFriendsFotmat.insert(pos, " ");
        MutualFriendsFotmat.insert(pos,Utils::FormatBigInteger(count).c_str());
        return MutualFriendsFotmat;
    }
    std::string CreateFormatString(const char * format, const char *textSize) {
            std::string FotmatString;
            FotmatString.append(format);
            int pos = FotmatString.find("%s");
            FotmatString.replace(pos, 2, textSize);
            return FotmatString;
        }
}
