#include "Log.h"
#include "MessagePortTransport.h"

#include <stdlib.h>

#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "fbMessagePortTransport"


IProtocol::IProtocol(const char *localPortName, const char *remoteAppId, const char *remotePortName) : mMPT(localPortName, remoteAppId, remotePortName) {
    mMPT.AttachProtocol(this);
}

/**
 * @brief               Constructor. Registers trusted local message port.
 * @localPortName[in]   Local message port name.
 * @remoteAppId[in]     Remote application Id.
 * @remotePortName[in]  Remote message port name.
 */
MessagePortTransport::MessagePortTransport(const char *localPortName, const char *remoteAppId, const char *remotePortName) :
        mRemoteAppId(strdup(remoteAppId)), mRemotePortName(strdup(remotePortName)),
        mLocalPortName(strdup(localPortName)), mProtocol(NULL) {
    OpenPort();
}

MessagePortTransport::~MessagePortTransport() {
    ClosePort();
    free((char *)mRemoteAppId);
    free((char *)mRemotePortName);
    free((char *)mLocalPortName);
}

/**
 * @brief             Attaches protocol to the MessagePortTransport.
 * @details           Usually this is done by protocol internally and shell not be called explicitly.
 * @protocol[in]      Protocol to be attached.
 */
void MessagePortTransport::AttachProtocol(IProtocol* protocol) {
    if (protocol) {
        mProtocol = protocol;
    }
}

/**
 * @brief                Callback called when incoming message received via the Message Port.
 * localPortId[in]       The local message port ID returned by message_port_register_local_port()
 * remoteId[in]          The ID of the remote application that sent this message
 * remPort[in]           The name of the remote message port
 * trustedRemotePort[in] If true the remote port is a trusted port, otherwise if false it is not
 * message[in]           The message passed from the remote application
 * userData[in]          The user data passed from the register function
 */
void MessagePortTransport::DataReceived(int localPortId, const char *remoteId, const char *remPort, bool trustedRemotePort, bundle *message, void *userData) {
    MessagePortTransport *self = (MessagePortTransport*) userData;
    if (remoteId && (strcmp(remoteId, self->mRemoteAppId) == 0)) {
        Log::info("Message from %s", remoteId);
        self->mProtocol->HandleReceivedData(message);
    }
}

/**
 * @brief             Sends a message with local port information to the message port of a remote application.
 * @message[in]       Message to be passed to the remote application, the recommended message size is under 4KB.
 * @return            Result.
 */
int MessagePortTransport::SendMessageWithLocalPort(bundle *message) {
    int ret = message_port_send_trusted_message_with_local_port(mRemoteAppId, mRemotePortName, message, mLocalPortId);
    if (ret != MESSAGE_PORT_ERROR_NONE) {
        Log::error("Error sending message : %d", ret);
        if (ret == MESSAGE_PORT_ERROR_PORT_NOT_FOUND) {
            if (OpenPort()) {
                // repeat once again
                int ret = message_port_send_trusted_message_with_local_port(mRemoteAppId, mRemotePortName, message, mLocalPortId);
                if (ret != MESSAGE_PORT_ERROR_NONE) {
                    Log::error("Error sending message second time : %d", ret);
                }
            }
        }
    } else {
        Log::info("Send message done");
    }
    return ret;
}

/**
 * @brief             Sends a message to the message port of a remote application.
 * @message[in]       Message to be passed to the remote application, the recommended message size is under 4KB.
 * @return            Result.
 */
int MessagePortTransport::SendMessage(bundle *message) {
    int ret = message_port_send_trusted_message(mRemoteAppId, mRemotePortName, message);
    if (ret != MESSAGE_PORT_ERROR_NONE) {
        Log::error("Error sending message : %d", ret);
    } else {
        Log::info("Send message done");
    }
    return ret;
}

/**
 * @brief             Checks whether the trusted message port of a remote application is registered.
 * @return            If the remote message port registered.
 */
bool MessagePortTransport::CheckRemotePort() const {
    bool found = false;
    int ret = message_port_check_trusted_remote_port (mRemoteAppId, mRemotePortName, &found);
    if (ret == MESSAGE_PORT_ERROR_NONE) {
        Log::info("MessagePortTransport::CheckRemotePort() - message_port_check_remote_port(%s) returned OK", mRemotePortName);
    } else {
        Log::error("MessagePortTransport::CheckRemotePort() - message_port_check_remote_port(%s) returned error=%d", mRemotePortName, ret);
    }
    return found;
}

bool MessagePortTransport::OpenPort() {
    ClosePort();
    mLocalPortId = message_port_register_trusted_local_port(mLocalPortName, DataReceived, this);
    if (mLocalPortId < 0) {
        Log::error("Port registration error : %d", mLocalPortId);
    } else {
        Log::info("Port has been successfully registered with port_id : %d", mLocalPortId);
    }
    return (mLocalPortId >= 0);
}

bool MessagePortTransport::ClosePort() {
    int ret = MESSAGE_PORT_ERROR_NONE;
    if (mLocalPortId) {
        ret = message_port_unregister_trusted_local_port(mLocalPortId);
        mLocalPortId = NULL;
    }
    return (ret == MESSAGE_PORT_ERROR_NONE);
}
