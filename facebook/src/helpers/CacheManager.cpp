//#define TIZEN_DEBUG_ENABLE

#include "CacheManager.h"
#include "Common.h"
#include "Config.h"
#include "DataTables.h"
#include "Friend.h"
#include "HomeProvider.h"
#include "Log.h"
#include "LogoutRequest.h"
#include "Queries.h"
#include "Photo.h"
#include "Post.h"
#include "Utils.h"

#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "CacheManager"
//#define CM_LOG_CALLBACK

const std::string DATABASE_NAME = "database.db";
const char *DATABASE_VERSION[ ELastVersion ] = {
        "0.0.0",
        "0.0.5",
        "0.0.6",
};

const double CacheManager::MIN_POST_UPDATED_TIME_DELTA = 100;

CacheManager::CacheManager() {
    mDB = NULL;
    mDBValue = NULL;
    mIsDataUpdated = false;
}

CacheManager::~CacheManager()
{
    CloseDatabase();
    free(mDBValue);
}

int CacheManager::log_callback(void *pArg, int argc, char **argv,
        char **columnName)
{
#ifdef CM_LOG_CALLBACK
    int i;
    for (i = 0; i < argc; i++) {
        Log::debug_tag(LOG_TAG, "%s = %s", columnName[i],
                argv[i] ? argv[i] : "NULL");
    }
#endif
    return 0;
}

bool CacheManager::Init()
{
    bool result = true;
    mDBValue = NULL;
    sqlite3_shutdown();

    sqlite3_config(SQLITE_CONFIG_URI, 1);

    sqlite3_initialize();

    result = InitDatabase();

    if (result) {
        DoMigration();

        // create new session for posts order
        const char *time = Utils::GetUnixTime();
        if ( time ) {
            Log::debug_tag(LOG_TAG, "CacheManager::Init -- time for session is %s", time );
            (void)CreateSessionData( time );
            free( (void*)time );
        } else {
            Log::error_tag( LOG_TAG, "Cannot determine local time to create new session for post order." );
        }
    }
    return result;
}

bool CacheManager::InsertToFriendsReqTable(const char* id, const char* name)
{
    bool result = true;
    char *ErrMsg = NULL;
    std::string query = "INSERT OR REPLACE INTO " + Tables::OutgoingFriendRequests::TABLE_NAME + " VALUES"
            + "(\'"+ id +"\'," + "\'" + name + "\');";
    int res = sqlite3_exec(mDB, query.c_str(), log_callback, 0, &ErrMsg);
    if (res != SQLITE_OK) {
        Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
        sqlite3_free(ErrMsg);
        result = false;
    }
    return result;
}

bool CacheManager::SelectFromFriendsReqTable( const char *id)
{
    bool result = true;
    char query[BUFFEER];
    Utils::Snprintf_s(query, BUFFEER, Queries::SelectFriendsRequestOut::QUERY_TEXT.c_str(), id);

    char *ErrMsg = NULL;
    int res = sqlite3_exec(mDB, query, log_callback, NULL, &ErrMsg);
    if (res != SQLITE_OK) {
        Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
        sqlite3_free(ErrMsg);
        result = false;
    }
    return result;
}

bool CacheManager::DeletFromFriendsReqTable( const char *id)
{
    bool result = true;
    char query[BUFFEER];
    Utils::Snprintf_s(query, BUFFEER, Queries::SelectFriendsRequestOut::DELETE_ROW.c_str(), id);

    char *ErrMsg = NULL;
    int res = sqlite3_exec(mDB, query, log_callback, NULL, &ErrMsg);
    if (res != SQLITE_OK) {
        Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
        sqlite3_free(ErrMsg);
        result = false;
    }
    return result;
}

bool CacheManager::AddFriend(Friend *friendData)
{
    Log::info(LOG_CACHEMANAGER, "AddFriend");

    char *errMsg = NULL;
    bool result = true;

    std::string query = "INSERT OR REPLACE INTO " + Tables::Friends::TABLE_NAME + " VALUES";

    if (friendData) {
        char mutual[20];
        Utils::Snprintf_s(mutual, 20, "%d", friendData->GetMutualFriendsCount());

        query = query + "(\'" + (friendData->GetId() != NULL ? friendData->GetId() : "NULL") + "\', "
                + "\'" + (!friendData->GetName().empty() ? friendData->GetName() : "NULL") + "\', "
                + "\'" + mutual + "\', "
                + "\'" + (!friendData->GetPicturePath().empty() ? friendData->GetPicturePath() : "NULL") + "\', "
                + "\'" + (!friendData->mFirstName.empty() ? friendData->mFirstName : "NULL") + "\',"
                + "\'" + (!friendData->mLastName.empty() ? friendData->mLastName : "NULL") + "\',"
                + "\'" + (!friendData->mBirthday.empty() ? friendData->mBirthday : "NULL") + "\',"
                + "\'" + (!friendData->mEmail.empty() ? friendData->mEmail : "NULL") + "\',"
                + "\'" + (!friendData->mPhone.empty() ? friendData->mPhone : "NULL") + "\')";
        query += ";";

        Log::debug(LOG_CACHEMANAGER, "SQL query: %s", query.c_str());
        int res = sqlite3_exec(mDB, query.c_str(), log_callback, 0, &errMsg);
        if (res != SQLITE_OK) {
            Log::error(LOG_CACHEMANAGER, "SQL error: %s", errMsg);
            sqlite3_free(errMsg);
            result = false;
        }
    }
    return result;
}

bool CacheManager::DeleteFriend( const char *id)
{
    bool result = true;
    char query[BUFFEER];
    Utils::Snprintf_s(query, BUFFEER, Queries::SelectNFriends::DELETE_FRIEND.c_str(), id);

    char *ErrMsg = NULL;
    int res = sqlite3_exec(mDB, query, log_callback, NULL, &ErrMsg);
    if (res != SQLITE_OK) {
        Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
        sqlite3_free(ErrMsg);
        result = false;
    }
    return result;
}

std::string CacheManager::GetCurrentDBVersion() {
    return GetSettingsValue(Tables::Settings::ValueName::DB_VERSION.c_str());
}

std::string CacheManager::GetSettingsValue(const char *name) {
    char *ErrMsg = NULL;
    std::string value = "";

    char query[BUFFEER];
    Utils::Snprintf_s(query, BUFFEER, Queries::GetSettingsValue::QUERY_TEXT.c_str(), name);

    int res = sqlite3_exec(mDB, query, select_get_data_callback, NULL, &ErrMsg);

    if (res != SQLITE_OK) {
        Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
        sqlite3_free(ErrMsg);
    } else {
        const char* val = GetInstance().GetDBValue();
        if(val) {
            value = val;
        }
    }
    return value;
}

bool CacheManager::SetSettingsValue(const char *name, const char *value) {
    bool result = true;
    std::string fields = " (" + Tables::Settings::Columns::NAME
            + "," + Tables::Settings::Columns::VALUE + ") ";

    std::string query = "INSERT OR REPLACE INTO " + Tables::Settings::TABLE_NAME + fields + " VALUES"
                + " (\'" + name + "\', "
                +   "\'" + ((value) ? value : "NULL") + "\')"
                + ";";

    Log::debug_tag(LOG_TAG, "SQL query session: %s", query.c_str());

    char *ErrMsg = NULL;
    int res = sqlite3_exec(mDB, query.c_str(), log_callback, 0, &ErrMsg);
    if (res != SQLITE_OK) {
        Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
        sqlite3_free(ErrMsg);
        result = false;
    }
    return result;
}

void CacheManager::DoMigration() {
    for (auto ver = GetEDBVersionFromString(GetCurrentDBVersion()); ver < (unsigned int)ELastVersion; ++ver) {
        if (!UpgradeDB(ver))
            break;
    }
}

bool CacheManager::UpgradeDB(unsigned int currentVersion) {
    bool res = false;
    switch ((EDBVersion)currentVersion) {
        case EOldVersion:
            DeleteDatabase();
            if (InitDatabase())
                res = CreateTables();
            break;
        case Ev_005:
            if (SQLITE_OK == sqlite3_exec(mDB, "CREATE TABLE IF NOT EXISTS friends_ (id TEXT NOT NULL, name TEXT NOT NULL, mutual_friends INTEGER, picture_path TEXT, first_name TEXT, last_name TEXT, birthday TEXT, email  TEXT, mobile_phone TEXT, UNIQUE( id ) ON CONFLICT REPLACE );", nullptr, nullptr, nullptr)
            && SQLITE_OK == sqlite3_exec(mDB, "INSERT INTO friends_ (id, name, mutual_friends, picture_path, first_name, last_name )SELECT id, name, mutual_friends, picture_path, first_name, last_name FROM friends;", nullptr, nullptr, nullptr)
            && SQLITE_OK == sqlite3_exec(mDB, "DROP TABLE IF EXISTS friends;", nullptr, nullptr, nullptr)
            && SQLITE_OK == sqlite3_exec(mDB, "ALTER TABLE friends_ RENAME TO friends;", nullptr, nullptr, nullptr)
            && SQLITE_OK == sqlite3_exec(mDB, "ALTER TABLE posts ADD to_profile_type TEXT;", nullptr, nullptr, nullptr)
            && SetSettingsValue(Tables::Settings::ValueName::DB_VERSION.c_str(), DATABASE_VERSION[Ev_006]))
                res = true;
            break;
        default:
            break;
    }
    return res;
}

unsigned int CacheManager::GetEDBVersionFromString(std::string strVer) {
    for (auto v = 0; v < ELastVersion; ++v) {
        if (!strcmp(DATABASE_VERSION[v], strVer.c_str()))
            return v;
    }
    return 0;
}

int CacheManager::CreateTable(const std::string query) {
    bool result = true;
    char *ErrMsg = NULL;
    int res = sqlite3_exec(mDB, query.c_str(), log_callback, 0, &ErrMsg);
    if (res != SQLITE_OK) {
        Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
        sqlite3_free(ErrMsg);
        result = false;
    }
    return result;
}

bool CacheManager::InitDatabase() {
    Log::debug_tag(LOG_TAG, "InitDatabase()");
    bool result = true;
    char* sharedPath = app_get_shared_trusted_path();
    char* dataPath = app_get_data_path();
    std::string path = sharedPath + DATABASE_NAME;
    std::string path2 = dataPath + DATABASE_NAME;
    Utils::CheckAccess(dataPath);
    if (Utils::CheckAccess(sharedPath)) {
        if (Utils::CheckAccess(path.c_str())) {
            result = rename(path.c_str(), path2.c_str()) == 0;
        } else if (Utils::FileExists(path.c_str())) {
            int removed = Utils::RemoveFolderRecursively(sharedPath);
            Log::debug_tag(LOG_TAG, "Remove all data from %s : %s", sharedPath, removed ? "success" : "fail");
        }
    } else {
        Log::debug_tag(LOG_TAG, "We haven't access rights to app_get_shared_trusted_path");
        result = false;
    }
    result = CreateOrOpenDB(path2.c_str()) && result;
    free(sharedPath);
    free(dataPath);
    return result;
}

int CacheManager::CreateOrOpenDB(const char * path) {
    bool result = true;
    Log::debug_tag(LOG_TAG, "CreateOrOpenDB(%s)", path);
    int res = sqlite3_open_v2(path, &mDB, (SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE), NULL);
    Log::debug_tag(LOG_TAG, "sqlite3_open_v2 = %d", res);
    if (res != SQLITE_OK) {
        Log::error_tag( LOG_TAG, "Can't open database: %s", sqlite3_errmsg(mDB));
        CloseDatabase();
        result = false;
    } else {
        Log::debug_tag(LOG_TAG, "Database is opened successfully");
        chmod(path, 0760);
    }
    return result;
}

void CacheManager::CloseDatabase() {
    if(mDB) {
        sqlite3_close(mDB);
        mDB = NULL;
    }
}

bool CacheManager::CreateTables()
{
    Log::debug_tag(LOG_TAG, "CacheManager::CreateTables Create tables");
    bool result = true;
    bool res = true;

    // create settings table
    res = CreateTable(Tables::Settings::CREATE_TABLE);
    result = res && result;

    // create outgoing friend requests table
    res = CreateTable(Tables::OutgoingFriendRequests::CREATE_TABLE);
    result = res && result;

    // create Friends table
    res = CreateTable(Tables::FriendableUser::CREATE_TABLE);
    result = res && result;

    // create Friends table
    res = CreateTable(Tables::Friends::CREATE_TABLE);
    result = res && result;

    // create Photos table
    res = CreateTable(Tables::Photos::CREATE_TABLE);
    result = res && result;

    // create Posts table
    res = CreateTable(Tables::Posts::CREATE_TABLE);
    result = res && result;

    // create Sessions table
    res = CreateTable(Tables::Sessions::CREATE_TABLE);
    result = res && result;

    // create Post_order table
    res = CreateTable(Tables::PostOrder::CREATE_TABLE);
    result = res && result;

    // create Accounts table
    res = CreateTable(Tables::Accounts::CREATE_TABLE);
    result = res && result;

    // create Notifications table
    res = CreateTable(Tables::Notifications::CREATE_TABLE);
    res = res && result;

    // create LoggedInUser table
    res = CreateTable(Tables::LoggedInUser::CREATE_TABLE);
    result = res && result;

    // create Profile feed table
    res = CreateTable(Tables::ProfileFeed::CREATE_TABLE);
    result = res && result;

    if(result) {
        if(SetSettingsValue(Tables::Settings::ValueName::DB_VERSION.c_str(), DATABASE_VERSION[Ev_005])) {
            Log::debug_tag(LOG_TAG, "CacheManager::CreateTables successful complete, version = %s", GetCurrentDBVersion().c_str());
        };
    }
    return result;
}

void CacheManager::DeleteDatabase()
{
    Log::debug_tag(LOG_TAG, "CacheManager::DeleteDatabase()");
    CloseDatabase();

    free((void*) mDBValue);
    mDBValue = NULL;

    // Remove database.db
    char * dataPath = app_get_data_path();
    std::string path = dataPath + DATABASE_NAME;
    remove(path.c_str());
    // Remove database.db-journal
    path = path + "-journal";
    remove(path.c_str());
    free(dataPath);

    // Remove Images cache
    char * sharedPath = app_get_shared_trusted_path();
    path = sharedPath;
    path = path + DIR_IMAGES_NAME;
    free(sharedPath);
    Utils::RemoveFolderRecursively(path.c_str());
}

bool CacheManager::SetFriendsData(FbRespondGetFriends *friendsResponse)
{
    Log::debug_tag(LOG_TAG, "Insert Friends data to DB");
    bool result = true;

    char *ErrMsg = NULL;

    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(friendsResponse->mFriendsList, l, listData)
    {
        std::string query = "INSERT OR REPLACE INTO " + Tables::Friends::TABLE_NAME + " VALUES";

        Friend *friendData = static_cast<Friend*>(listData);

        if(friendData) {
            char mutual[20];
            Utils::Snprintf_s(mutual, 20, "%d", friendData->GetMutualFriendsCount());

            query = query + "(\'" + (friendData->GetId() != NULL ? friendData->GetId() : "NULL") + "\', "
                    + "\'" + (!friendData->GetName().empty() ? friendData->GetName() : "NULL") + "\', "
                    + "\'" + mutual + "\', "
                    + "\'" + (!friendData->GetPicturePath().empty() ? friendData->GetPicturePath() : "NULL") + "\', "
                    + "\'" + (!friendData->mFirstName.empty() ? friendData->mFirstName : "NULL") + "\',"
                    + "\'" + (!friendData->mLastName.empty() ? friendData->mLastName : "NULL") + "\',"
                    + "\'" + (!friendData->mBirthday.empty() ? friendData->mBirthday : "NULL") + "\',"
                    + "\'" + (!friendData->mEmail.empty() ? friendData->mEmail : "NULL") + "\',"
                    + "\'" + (!friendData->mPhone.empty() ? friendData->mPhone : "NULL") + "\')";
            query += ";";

            Log::debug_tag(LOG_TAG, "SQL query: %s", query.c_str());
            int res = sqlite3_exec(mDB, query.c_str(), log_callback, 0, &ErrMsg);
            if (res != SQLITE_OK) {
                Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
                sqlite3_free(ErrMsg);
                result = false;
            }
        }
    }
    return result;
}

Eina_List* CacheManager::SelectFriendsData() {
    Eina_List *list = NULL;

    char *ErrMsg = NULL;
    int res = sqlite3_exec(mDB, Queries::SelectNFriends::QUERY_TEXT.c_str(), select_friends_callback, &list, &ErrMsg);
    if (res != SQLITE_OK) {
        Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
        sqlite3_free(ErrMsg);
    }
    return list;
}

int CacheManager::select_friends_callback(void *pArg, int argc, char **argv, char **columnName)
{
    Eina_List **list = (Eina_List **) pArg;
    Friend *data = new Friend(columnName, argv);

    *list = eina_list_append(*list, data);

    return 0;
}

bool CacheManager::SetFriendableUserData(FbRespondGetPeopleYouMayKnow *friendsResponse) {
    Log::debug_tag(LOG_TAG, "Insert FriendableUser data to DB");
    bool result = true;

    char *ErrMsg = NULL;

    /* Delete previous records*/

    int res = sqlite3_exec(mDB, Queries::DeleteAllFriendableUser::QUERY_TEXT.c_str(), log_callback, 0, &ErrMsg);
    if (res != SQLITE_OK) {
        Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
        sqlite3_free(ErrMsg);
        result = false;
    }

    if(result) {
        Eina_List *l;
        void *listData;
        EINA_LIST_FOREACH(friendsResponse->mPeople, l, listData)
        {
            std::string query = "INSERT OR REPLACE INTO " + Tables::FriendableUser::TABLE_NAME + " VALUES";

            FbRespondGetPeopleYouMayKnow::FriendableUser *friendData =
                    static_cast<FbRespondGetPeopleYouMayKnow::FriendableUser *>(listData);

            char mutual[20];
            Utils::Snprintf_s(mutual, 20, "%d", friendData->GetMutualFriendsCount());

            query = query
                    + "(\'" + (friendData->GetId() != NULL ? friendData->GetId() : "NULL") + "\', "
                    + "\'" + (friendData->GetName() ? Utils::ReplaceQuotation(friendData->GetName()) : "NULL") + "\', "
                    + "\'" + (friendData->GetUserName() ? Utils::ReplaceQuotation(friendData->GetUserName()) : "NULL") + "\', "
                    + "\'" + mutual + "\', "
                    + "\'" + (friendData->GetPicSquareWithLogo() ? friendData->GetPicSquareWithLogo() : "NULL")  + "\');";

            Log::debug_tag(LOG_TAG, "SQL query: %s", query.c_str());
            res = sqlite3_exec(mDB, query.c_str(), log_callback, 0, &ErrMsg);
            if (res != SQLITE_OK) {
                Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
                sqlite3_free(ErrMsg);
                result = false;
            }
        }
    }
    return result;
}

Eina_List* CacheManager::SelectFriendableUserData(int recordsNumber) {
    Eina_List *list = NULL;
    if (recordsNumber > 0) {
        Log::debug_tag(LOG_TAG, "Select %d friend record%s",
                recordsNumber, recordsNumber > 1 ? "s" : "");
        char query[BUFFEER];
        Utils::Snprintf_s(query, BUFFEER, Queries::SelectFriendableUser::QUERY_TEXT.c_str(), recordsNumber);

        char *ErrMsg = NULL;
        int res = sqlite3_exec(mDB, query, select_friendableuser_callback, &list, &ErrMsg);
        if (res != SQLITE_OK) {
            Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
            sqlite3_free(ErrMsg);
        }
    }
    return list;
}

int CacheManager::select_friendableuser_callback(void *pArg, int argc, char **argv,
        char **columnName) {
    Eina_List **list = (Eina_List **) pArg;
    FbRespondGetPeopleYouMayKnow::FriendableUser *data = new FbRespondGetPeopleYouMayKnow::FriendableUser(columnName, argv);

    *list = eina_list_append(*list, data);

    return 0;
}


int CacheManager::select_post_order_callback(void *pArg, int argc, char **argv, char **columnName)
{
    Eina_List **list = (Eina_List **) pArg;
    Order *data = new Order();
    if (data){
        data->mPosition = atoi(argv[Queries::SelectPostOrder::POSITION_ID]);
        data->mPostAutoId = atoi(argv[Queries::SelectPostOrder::POST_AUTO_ID]);
        data->mSessionAutoId = atoi(argv[Queries::SelectPostOrder::SESSION_AUTO_ID]);

        *list = eina_list_append(*list, data);
    }
    return 0;
}

int CacheManager::select_post_order_by_session_callback(void *pArg, int argc, char **argv, char **columnName)
{
    Eina_List **list = (Eina_List **) pArg;
    Order *data = new Order();
    if (data){
        data->mPosition = atoi(argv[Queries::SelectPostOrder::POSITION_ID]);
        data->mPostAutoId = atoi(argv[Queries::SelectPostOrder::POST_AUTO_ID]);
        data->mSessionAutoId = atoi(argv[Queries::SelectPostOrder::SESSION_AUTO_ID]);

        *list = eina_list_append(*list, data);
    }
    return 0;
}

const std::string *CacheManager::getPostTableFields() {
    static std::string fields = " (" + Tables::Posts::Columns::ID
            + "," + Tables::Posts::Columns::CREATED_TIME
            + "," + Tables::Posts::Columns::UPDATED_TIME
            + "," + Tables::Posts::Columns::CAPTION
            + "," + Tables::Posts::Columns::DESCRIPTION
            + "," + Tables::Posts::Columns::FROM_NAME
            + "," + Tables::Posts::Columns::FROM_ID
            + "," + Tables::Posts::Columns::FROM_PICTURE
            + "," + Tables::Posts::Columns::FULL_PICTURE
            + "," + Tables::Posts::Columns::LINK
            + "," + Tables::Posts::Columns::MESSAGE
            + "," + Tables::Posts::Columns::MESSAGE_TAGS
            + "," + Tables::Posts::Columns::OBJECT_ID
            + "," + Tables::Posts::Columns::PARENT_ID
            + "," + Tables::Posts::Columns::PLACE_ID
            + "," + Tables::Posts::Columns::PLACE_NAME
            + "," + Tables::Posts::Columns::PLACE_CITY
            + "," + Tables::Posts::Columns::PLACE_COUNTRY
            + "," + Tables::Posts::Columns::PLACE_LATITUDE
            + "," + Tables::Posts::Columns::PLACE_LONGITUDE
            + "," + Tables::Posts::Columns::PLACE_STREET
            + "," + Tables::Posts::Columns::PLACE_CHECKINS
            + "," + Tables::Posts::Columns::PRIVACY_VALUE
            + "," + Tables::Posts::Columns::PRIVACY_DESCRIPTION
            + "," + Tables::Posts::Columns::PRIVACY_FRIENDS
            + "," + Tables::Posts::Columns::PRIVACY_ALLOW
            + "," + Tables::Posts::Columns::PRIVACY_DENY
            + "," + Tables::Posts::Columns::SOURCE
            + "," + Tables::Posts::Columns::STATUS_TYPE
            + "," + Tables::Posts::Columns::STORY
            + "," + Tables::Posts::Columns::STORY_TAGS
            + "," + Tables::Posts::Columns::TYPE
            + "," + Tables::Posts::Columns::LIKES_COUNT
            + "," + Tables::Posts::Columns::COMMENTS_COUNT
            + "," + Tables::Posts::Columns::PHOTO_COUNT
            + "," + Tables::Posts::Columns::ADDED_STHG
            + "," + Tables::Posts::Columns::CHILD_ID
            + "," + Tables::Posts::Columns::COMMENT
            + "," + Tables::Posts::Columns::COMMUTITY_AVATAR
            + "," + Tables::Posts::Columns::COMMUTITY_NAME
            + "," + Tables::Posts::Columns::COMMUTITY_SUBSCRIBERS
            + "," + Tables::Posts::Columns::COMMUTITY_TYPE
            + "," + Tables::Posts::Columns::COMMUTITY_WALLPAPER
            + "," + Tables::Posts::Columns::EVENT_COVER
            + "," + Tables::Posts::Columns::EVENT_INFO
            + "," + Tables::Posts::Columns::FROM_PICTURE_PATH
            + "," + Tables::Posts::Columns::LOCATION_AVATAR_PATH
            + "," + Tables::Posts::Columns::PLACE_TYPE
            + "," + Tables::Posts::Columns::PLACE_VISITORS
            + "," + Tables::Posts::Columns::NAME
            + "," + Tables::Posts::Columns::PLACE_PICTURE
            + "," + Tables::Posts::Columns::SONG_ARTIST
            + "," + Tables::Posts::Columns::SONG_NAME
            + "," + Tables::Posts::Columns::NEW_FRIEND_ID
            + "," + Tables::Posts::Columns::NEW_FRIEND_AVATAR
            + "," + Tables::Posts::Columns::NEW_FRIEND_CAREER
            + "," + Tables::Posts::Columns::NEW_FRIEND_MUTAL_FRIENDS
            + "," + Tables::Posts::Columns::NEW_FRIEND_WORK
            + "," + Tables::Posts::Columns::NEW_FRIEND_EDUCATION
            + "," + Tables::Posts::Columns::NEW_FRIEND_COVER
            + "," + Tables::Posts::Columns::CAN_LIKE
            + "," + Tables::Posts::Columns::HAS_LIKED
            + "," + Tables::Posts::Columns::CAN_COMMENT
            + "," + Tables::Posts::Columns::CAN_SHARE
            + "," + Tables::Posts::Columns::TO_NAME
            + "," + Tables::Posts::Columns::TO_ID
            + "," + Tables::Posts::Columns::VIA_ID
            + "," + Tables::Posts::Columns::VIA_NAME
            + "," + Tables::Posts::Columns::GROUP_NAME
            + "," + Tables::Posts::Columns::GROUP_STATUS
            + "," + Tables::Posts::Columns::GROUP_ID
            + "," + Tables::Posts::Columns::LIKE_FROM_NAME
            + "," + Tables::Posts::Columns::LIKE_FROM_ID
            + "," + Tables::Posts::Columns::ATTACHMENT_DESCRIPTION
            + "," + Tables::Posts::Columns::ATTACHMENT_SRC
            + "," + Tables::Posts::Columns::ATTACHMENT_HEIGHT
            + "," + Tables::Posts::Columns::ATTACHMENT_WIDTH
            + "," + Tables::Posts::Columns::ATTACHMENT_TARGET_ID
            + "," + Tables::Posts::Columns::ATTACHMENT_TARGET_TITLE
            + "," + Tables::Posts::Columns::ATTACHMENT_TARGET_URL
            + "," + Tables::Posts::Columns::WITH_TAGS_COUNT
            + "," + Tables::Posts::Columns::WITH_TAGS_NAME
            + "," + Tables::Posts::Columns::WITH_TAGS_ID
            + "," + Tables::Posts::Columns::IMPLICIT_PLACE_ID
            + "," + Tables::Posts::Columns::IMPLICIT_PLACE_NAME
            + "," + Tables::Posts::Columns::IMPLICIT_PLACE_CITY
            + "," + Tables::Posts::Columns::IMPLICIT_PLACE_COUNTRY
            + "," + Tables::Posts::Columns::IMPLICIT_PLACE_LATITUDE
            + "," + Tables::Posts::Columns::IMPLICIT_PLACE_LONGITUDE
            + "," + Tables::Posts::Columns::IMPLICIT_PLACE_STREET
            + "," + Tables::Posts::Columns::TO_PROFILE_TYPE
            + ") ";
    return &fields;
}

std::string CacheManager::getPostValues(Post *postData) {
    std::string query;

    char createdTime[20];
    Utils::Snprintf_s(createdTime, 20, "%.0f", postData->GetCreatedTime());

    char updatedTime[20];
    Utils::Snprintf_s(updatedTime, 20, "%.0f", postData->GetUpdatedTime());

    char likesCount[20];
    Utils::Snprintf_s(likesCount, 20, "%d", postData->GetLikesCount());

    char canLike[20];
    Utils::Snprintf_s(canLike, 20, "%d", postData->GetCanLike()? 1 : 0);

    char hasLiked[20];
    Utils::Snprintf_s(hasLiked, 20, "%d", postData->GetHasLiked()? 1 : 0);

    char withTagsCount[20];
    Utils::Snprintf_s(withTagsCount, 20, "%d", postData->GetWithTagsCount());

    char commentsCount[20];
    Utils::Snprintf_s(commentsCount, 20, "%d", postData->GetCommentsCount());

    char canComment[20];
    Utils::Snprintf_s(canComment, 20, "%d", postData->GetCanComment()? 1 : 0);

    char canShare[20];
    Utils::Snprintf_s(canShare, 20, "%d", postData->GetCanShare()? 1 : 0);

    char photosCount[20];
    Utils::Snprintf_s(photosCount, 20, "%d", postData->GetPhotoCount());

    char attachmentHeight[20];
    Utils::Snprintf_s(attachmentHeight, 20, "%d", postData->GetAttachmentHeight());

    char attachmentWidth[20];
    Utils::Snprintf_s(attachmentWidth, 20, "%d", postData->GetAttachmentWidth());

    query = query + "(\'" + (postData->GetId() != NULL ? postData->GetId() : "NULL") + "\', "
            + "\'" + createdTime + "\', "
            + "\'" + updatedTime + "\', "
            + "\'" + (!postData->GetCaption().empty() ? Utils::ReplaceQuotation(postData->GetCaption()) : "NULL") + "\', "
            + "\'" + (postData->GetDescription() != NULL ? Utils::ReplaceQuotation(postData->GetDescription()) : "NULL") + "\', "
            + "\'" + (!postData->GetFromName().empty() ? Utils::ReplaceQuotation(postData->GetFromName()) : "NULL") + "\', "
            + "\'" + (!postData->GetFromId().empty() ? postData->GetFromId() : "NULL") + "\', "
            + "\'" + (postData->GetFromPictureLink() != NULL ? postData->GetFromPictureLink() : "NULL") + "\', "
            + "\'" + (postData->GetFullPicture() != NULL ? postData->GetFullPicture() : "NULL") + "\', "
            + "\'" + (postData->GetLink() != NULL ? postData->GetLink() : "NULL") + "\', "
            + "\'" + (postData->GetMessage() ? Utils::ReplaceQuotation(postData->GetMessage()) : "NULL") + "\', ";

    std::list<Tag> messageTagList(std::move(postData->GetMessageTagsList()));
    if (messageTagList.size()) {
        query += "\'" + tagsToJSON(messageTagList) + "\', ";
        Log::debug_tag(LOG_TAG, "SetFeedData, query Message tags %s", query.c_str());
    }
    else {
        query += "\'NULL\', ";
    }

    query = query + "\'" + (postData->GetObjectId() != NULL ? postData->GetObjectId() : "NULL") + "\', "
            + "\'" + (postData->GetParentId() != NULL ? postData->GetParentId() : "NULL") + "\', ";

    //Place
    if (postData->GetPlace()) {
        char latitude[20];
        Utils::Snprintf_s(latitude, 20, "%.0f", postData->GetPlace()->mLatitude);

        char longitude[20];
        Utils::Snprintf_s(longitude, 20, "%.0f", postData->GetPlace()->mLongitude);

        char checkins[20];
        Utils::Snprintf_s(checkins, 20, "%d", postData->GetPlace()->mCheckins);

        query = query + "\'" + (postData->GetPlace()->mId != NULL ? postData->GetPlace()->mId : "NULL") + "\', "
                + "\'" + (postData->GetPlace()->mName != NULL ? postData->GetPlace()->mName : "NULL") + "\', "
                + "\'" + (postData->GetPlace()->mCity != NULL ? postData->GetPlace()->mCity : "NULL") + "\', "
                + "\'" + (postData->GetPlace()->mCountry != NULL ? postData->GetPlace()->mCountry : "NULL") + "\', "
                + "\'" + latitude + "\', "
                + "\'" + longitude + "\', "
                + "\'" + (postData->GetPlace()->mStreet != NULL ? Utils::ReplaceQuotation(postData->GetPlace()->mStreet) : "NULL") + "\', "
                + "\'" + checkins + "\', ";
    }
    else {
        query += "\'NULL\', \'NULL\', \'NULL\', \'NULL\', \'NULL\', \'NULL\', \'NULL\', \'NULL\', ";
    }

    //Privacy
    if (postData->GetPrivacy()) {
        query = query + "\'" + (postData->GetPrivacy()->GetValue() != NULL ? postData->GetPrivacy()->GetValue() : "NULL") + "\', "
                + "\'" + (postData->GetPrivacy()->GetDescription() != NULL ? postData->GetPrivacy()->GetDescription() : "NULL") + "\', "
                + "\'" + (postData->GetPrivacy()->GetFriends() != NULL ? postData->GetPrivacy()->GetFriends() : "NULL") + "\', "
                + "\'" + (postData->GetPrivacy()->GetAllow() != NULL ? postData->GetPrivacy()->GetAllow() : "NULL") + "\', "
                + "\'" + (postData->GetPrivacy()->GetDeny() != NULL ? postData->GetPrivacy()->GetDeny() : "NULL") + "\', ";
    }
    else {
        query += "\'NULL\', \'NULL\', \'NULL\', \'NULL\', \'NULL\', ";
    }

    query = query + "\'" + (postData->GetSource() != NULL ? postData->GetSource() : "NULL") + "\', "
            + "\'" + (postData->GetStatusType() != NULL ? postData->GetStatusType() : "NULL") + "\', "
            + "\'" + (!postData->GetStory().empty() ? Utils::ReplaceQuotation(postData->GetStory()) : "NULL") + "\', ";

    std::list<Tag> storyTagsList(std::move(postData->GetStoryTagsList()));
    if (storyTagsList.size()) {
        query += "\'" + tagsToJSON(storyTagsList) + "\', ";
    }
    else {
        query += "\'NULL\', ";
    }

    query = query + "\'" + (postData->GetType() != NULL ? postData->GetType() : "NULL") + "\', "
            + "\'" + likesCount + "\', "
            + "\'" + commentsCount + "\', "
            + "\'" + photosCount  + "\', "
            + "\'" + (postData->GetAddedSthgCount()!= NULL ? Utils::ReplaceQuotation(postData->GetAddedSthgCount()) : "NULL") + "\', "
            + "\'" + (postData->GetChildId()!= NULL ? Utils::ReplaceQuotation(postData->GetChildId()) : "NULL") + "\', "
            + "\'" + (postData->GetCommentText()!= NULL ? Utils::ReplaceQuotation(postData->GetCommentText()) : "NULL") + "\', "
            + "\'" + (postData->GetCommunityAvatar()!= NULL ? Utils::ReplaceQuotation(postData->GetCommunityAvatar()) : "NULL") + "\', "
            + "\'" + (postData->GetCommunityName()!= NULL ? Utils::ReplaceQuotation(postData->GetCommunityName()) : "NULL") + "\', "
            + "\'" + (postData->GetCommunitySubscribers()!= NULL ? Utils::ReplaceQuotation(postData->GetCommunitySubscribers()) : "NULL") + "\', "
            + "\'" + (postData->GetCommunityType()!= NULL ? Utils::ReplaceQuotation(postData->GetCommunityType()) : "NULL") + "\', "
            + "\'" + (postData->GetCommunityWallpaper()!= NULL ? Utils::ReplaceQuotation(postData->GetCommunityWallpaper()) : "NULL") + "\', "
            + "\'" + (postData->GetEventCover()!= NULL ? Utils::ReplaceQuotation(postData->GetEventCover()) : "NULL") + "\', "
            + "\'" + (postData->GetEventInfo()!= NULL ? Utils::ReplaceQuotation(postData->GetEventInfo()) : "NULL") + "\', "
            + "\'" + (postData->GetFromPicturePath()!= NULL ? Utils::ReplaceQuotation(postData->GetFromPicturePath()) : "NULL") + "\', "
            + "\'" + (postData->GetLocationAvatar()!= NULL ? Utils::ReplaceQuotation(postData->GetLocationAvatar()) : "NULL") + "\', "
            + "\'" + (!postData->GetLocationType().empty() ? Utils::ReplaceQuotation(postData->GetLocationType()) : "NULL") + "\', "
            + "\'" + (postData->GetLocationVisitors()!= NULL ? Utils::ReplaceQuotation(postData->GetLocationVisitors()) : "NULL") + "\', "
            + "\'" + (!postData->GetName().empty() ? Utils::ReplaceQuotation(postData->GetName()) : "NULL") + "\', "
            + "\'" + (postData->GetPlacePictureLink()!= NULL ? Utils::ReplaceQuotation(postData->GetPlacePictureLink()) : "NULL") + "\', "
            + "\'" + (postData->GetSongArtist()!= NULL ? Utils::ReplaceQuotation(postData->GetSongArtist()) : "NULL") + "\', "
            + "\'" + (postData->GetSongName()!= NULL ? Utils::ReplaceQuotation(postData->GetSongName()) : "NULL") + "\', "
            + "\'" + (postData->GetNewFriendId()!= NULL ? Utils::ReplaceQuotation(postData->GetNewFriendId()) : "NULL") + "\', "
            + "\'" + (postData->GetNewFriendAvatar()!= NULL ? Utils::ReplaceQuotation(postData->GetNewFriendAvatar()) : "NULL") + "\', "
            + "\'" + (postData->GetNewFriendCareer()!= NULL ? Utils::ReplaceQuotation(postData->GetNewFriendCareer()) : "NULL") + "\', "
            + "\'" + (postData->GetNewFriendMutualFriends()!= NULL ? Utils::ReplaceQuotation(postData->GetNewFriendMutualFriends()) : "NULL") + "\', "
            + "\'" + (postData->GetNewFriendWork()!= NULL ? Utils::ReplaceQuotation(postData->GetNewFriendWork()) : "NULL") + "\', "
            + "\'" + (postData->GetNewFriendEducation()!= NULL ? Utils::ReplaceQuotation(postData->GetNewFriendEducation()) : "NULL") + "\', "
            + "\'" + (postData->GetNewFriendCover()!= NULL ? Utils::ReplaceQuotation(postData->GetNewFriendCover()) : "NULL") + "\', "
            + "\'" + canLike + "\', "
            + "\'" + hasLiked + "\', "
            + "\'" + canComment + "\', "
            + "\'" + canShare + "\', "
            + "\'" + (postData->GetToName() ? Utils::ReplaceQuotation(postData->GetToName()) : "NULL") + "\', "
            + "\'" + (postData->GetToId() ? postData->GetToId() : "NULL") + "\', "
            + "\'" + ((postData->GetVia() && postData->GetVia()->mId) ? postData->GetVia()->mId : "NULL") + "\', "
            + "\'" + ((postData->GetVia() && postData->GetVia()->mName) ? Utils::ReplaceQuotation(postData->GetVia()->mName) : "NULL") +  "\', "
            + "\'" + (postData->GetGroupName() ? Utils::ReplaceQuotation(postData->GetGroupName()) : "NULL") + "\', "
            + "\'NULL\', "
            + "\'" + (postData->GetGroupId() ? postData->GetGroupId() : "NULL") + "\',"
            + "\'" + (postData->GetLikeFromName() ? postData->GetLikeFromName() : "NULL") + "\', "
            + "\'" + (postData->GetLikeFromId() ? postData->GetLikeFromId() : "NULL") + "\', "
            + "\'" + (!postData->GetAttachmentDescription().empty() ? postData->GetAttachmentDescription() : "NULL") + "\', "
            + "\'" + (postData->GetAttachmentSrc() ? postData->GetAttachmentSrc() : "NULL") + "\', "
            + "\'" + attachmentHeight + "\', "
            + "\'" + attachmentWidth + "\', "
            + "\'" + (postData->GetAttachmentTargetId() ? postData->GetAttachmentTargetId() : "NULL") + "\', "
            + "\'" + (!postData->GetAttachmentTitle().empty() ? Utils::ReplaceQuotation(postData->GetAttachmentTitle()) : "NULL") + "\', "
            + "\'" + (!postData->GetAttachmentTargetUrl().empty() ? postData->GetAttachmentTargetUrl() : "NULL") + "\', "
            + "\'" + (!postData->GetWithTagsId().empty() ? postData->GetWithTagsId() : "NULL") + "\', "
            + "\'" + withTagsCount + "\', "
            + "\'" + (!postData->GetWithTagsName().empty() ? postData->GetWithTagsName() : "NULL") + "\', ";

    //ImplicitPlace
    if (postData->GetImplicitPlace()) {
        char impLatitude[20];
        Utils::Snprintf_s(impLatitude, 20, "%.0f", postData->GetImplicitPlace()->mLatitude);

        char impLongitude[20];
        Utils::Snprintf_s(impLongitude, 20, "%.0f", postData->GetImplicitPlace()->mLongitude);

        query = query + "\'" + (postData->GetImplicitPlace()->mId != NULL ? postData->GetImplicitPlace()->mId : "NULL") + "\', "
                + "\'" + (postData->GetImplicitPlace()->mName != NULL ? postData->GetImplicitPlace()->mName : "NULL") + "\', "
                + "\'" + (postData->GetImplicitPlace()->mCity != NULL ? postData->GetImplicitPlace()->mCity : "NULL") + "\', "
                + "\'" + (postData->GetImplicitPlace()->mCountry != NULL ? postData->GetImplicitPlace()->mCountry : "NULL") + "\', "
                + "\'" + impLatitude + "\', "
                + "\'" + impLongitude + "\', "
                + "\'" + (postData->GetImplicitPlace()->mStreet != NULL ? Utils::ReplaceQuotation(postData->GetImplicitPlace()->mStreet) : "NULL") + "\', ";
    } else {
        query += "\'NULL\', \'NULL\', \'NULL\', \'NULL\', \'NULL\', \'NULL\', \'NULL\', ";
    }
    query = query + "\'" + (postData->GetToProfileType() ? postData->GetToProfileType() : "NULL") + "\');";

    return query;
}

bool CacheManager::SetFeedData(FbRespondGetFeed * feedResponse)
{
    Log::debug_tag(LOG_TAG, "Insert Feed data to DB");
    bool result = true;
    int autoId = 0;

    Eina_List *el;
    void *listData;
    int i = 0;

    EINA_LIST_FOREACH(feedResponse->GetPostsList(), el, listData) {
        Log::debug_tag(LOG_TAG, "Insert Post #%d", ++i);
        Post *postData = static_cast<Post*>(listData);

        //read data from DB
        Post *dbPostData = SelectFeedDataById(postData->GetId());

        if(dbPostData == NULL){
            std::string query = "INSERT OR REPLACE INTO "
                    + Tables::Posts::TABLE_NAME +  (*getPostTableFields()) + " VALUES"
                    + getPostValues(postData);

            Log::debug_tag(LOG_TAG, "SQL query: %s", query.c_str());

            char *ErrMsg = NULL;
            int res = sqlite3_exec(mDB, query.c_str(), log_callback, 0, &ErrMsg);
            if (res != SQLITE_OK) {
                Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
                sqlite3_free(ErrMsg);
                result = false;
            }

            //Insert related photos
            // Get auto_id from post table
            autoId = GetPostAutoIdByPostId(postData->GetId());

            Log::debug_tag(LOG_TAG, "Posts insert, autoId = %d, name is: %s",
                    autoId, ( !postData->GetName().empty() ? Utils::ReplaceQuotation(postData->GetName()).c_str() : "" ) );

            //Insert attached photos
            if (postData->GetPhotoCount()) {
                result = SetPhotoTable(postData->GetPhotoList(), postData->GetId(),autoId);
            }
        } else {
            // Update post if date updated
            // This magic number (MIN_POST_UPDATED_TIME_DELTA) is defined because Facebook server returns
            // different updated time for post if it fetched via different requests
            if((postData->GetUpdatedTime() - dbPostData->GetUpdatedTime()) > MIN_POST_UPDATED_TIME_DELTA){
                UpdateFeedDataByPost(postData);
            }
        }

    }
    return result;
}

/**
 * @brief This method should be invoked for storing the logged in user's feed posts.
 * @param[in] feedResponse - JSON data for feed request
 */
bool CacheManager::SetProfileFeedData(FbRespondGetFeed * feedResponse)
{
    Log::debug_tag(LOG_TAG, "Insert Profile Feed data to DB");
    bool result = true;

    return result;
}

std::string CacheManager::tagsToJSON(const std::list<Tag>& tagsList) {
    std::stringstream tagsQueryStream;
    tagsQueryStream << "{\"tags\":{";

    int iterator(0);
    for(auto it = tagsList.begin(); it != tagsList.end(); it++) {
        std::string name(it->mName);
        if (!name.empty()) {
            name = Utils::ReplaceString(name, "\'", "\'\'");
            name = Utils::ReplaceString(name, "\"", "\\\"");
        }
        else {
            name = "NULL";
        }
        tagsQueryStream << "\""
                        << iterator++
                        << "\":[{\"id\":\""
                        << (!it->mId.empty() ? it->mId : "NULL")
                        << "\",\"name\":\""
                        << name
                        << "\",\"type\":\""
                        << (!it->mType.empty() ? it->mType : "NULL")
                        << "\",\"offset\":"
                        << it->mOffset
                        << ",\"length\":"
                        << it->mLength
                        << "}],";
    }
    std::string tagsQuery(std::move(tagsQueryStream.str()));
    tagsQuery.erase(tagsQuery.size() - 1);
    tagsQuery += "}}";
    return std::move(tagsQuery);
}

Eina_List* CacheManager::SelectFeedData(int recordsNumber)
{
    Eina_List *list = NULL;
    if (recordsNumber > 0) {
        Log::debug_tag(LOG_TAG, "Select %d post record%s",
                recordsNumber, recordsNumber > 1 ? "s" : "");
        char query[BUFFEER];
        Utils::Snprintf_s(query, BUFFEER, Queries::SelectNPosts::QUERY_TEXT.c_str(), recordsNumber);

        char *ErrMsg = NULL;
        int res = sqlite3_exec(mDB, query, select_feed_callback, &list, &ErrMsg);
        if (res != SQLITE_OK) {
            Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
            sqlite3_free(ErrMsg);
            return NULL;
        }

        std::string postsId = "";
        bool isPhotoList = false;
        Eina_List *listEl;
        void *listData;
        EINA_LIST_FOREACH(list, listEl, listData) {
            Post *postData = (Post *) listData;
            if(postData->GetPhotoCount()){isPhotoList = true;};
            if (postData->GetPhotoCount() > 0 && postData->GetId()) {
                postsId = postsId + "\'" + postData->GetId() + "\',";
            }
        }
        if (!postsId.empty()) {
            postsId.erase(postsId.size() - 1);
            Utils::Snprintf_s(query, BUFFEER, Queries::SelectPhotosById::QUERY_TEXT.c_str(), postsId.c_str());

            PostsPhotoData *photoData = new PostsPhotoData();
            photoData->mPostsList = list;
            if (isPhotoList){
                res = sqlite3_exec(mDB, query, select_photo_callback, &photoData, &ErrMsg);
            } else {
                res = sqlite3_exec(mDB, query, select_images_callback, &photoData, &ErrMsg);
            }
            if (res != SQLITE_OK) {
                Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
                sqlite3_free(ErrMsg);
            } else {
                list = photoData->mPostsList;
            }
            delete photoData;
        }
    }
    return list;
}

Post* CacheManager::SelectFeedDataByQuery(const char *queryPost) {
    Post * post = NULL;
    char *ErrMsg = NULL;
    int res = sqlite3_exec(mDB, queryPost, select_feed_callback_by_id, &post, &ErrMsg);
    if (res != SQLITE_OK) {
        Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
        sqlite3_free(ErrMsg);
        return NULL;
    }

    if (post && post->GetId()) {
        char queryPhotos[BUFFEER];
        Utils::Snprintf_s(queryPhotos, BUFFEER, Queries::SelectPhotosById::QUERY_TEXT.c_str(), post->GetId());
        PostsPhotoData *photoData = new PostsPhotoData();
        photoData->mLastPost = post;
        res = sqlite3_exec(mDB, queryPhotos, select_photo_callback, &photoData, &ErrMsg);
        if (res != SQLITE_OK) {
            Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
            sqlite3_free(ErrMsg);
        } else {
            post = photoData->mLastPost;

            Eina_List *listEl = NULL, *newList = NULL;
            void *listData = NULL;

            Log::debug_tag(LOG_TAG_FACEBOOK, "GetImage Count:( %d ) Photo count: ( %d )", post->GetImagesCount(), post->GetPhotoCount());
            EINA_LIST_FOREACH(photoData->mLastPhotoList, listEl, listData) {
                Photo * photo = static_cast <Photo *> (listData);

                // Create the photo list that will be used by widget factory to display post from the DB
                // this can handle both single and multiple photo post .
                if(photo->GetFileName()) {
                    newList = eina_list_append(newList, photo);
                    Log::debug_tag(LOG_TAG, "CacheManager::SelectFeedDataByQuery->Selected Photo: (%d %d %s)", photo->GetWidth(), photo->GetHeight(), photo->GetFileName());
                }
                else {
                    Log::debug_tag(LOG_TAG, "CacheManager::SelectFeedDataByQuery-> no photo in list");
                }
            }
            post->SetPhotoList(newList);
        }
        delete photoData;
    }
    return post;
}

Post* CacheManager::SelectFeedDataById(const char* post_id)
{
    Post * post = NULL;
    if (post_id) {
        Log::debug_tag(LOG_TAG, "Select post record = %s",post_id);

        char query[BUFFEER];
        Utils::Snprintf_s(query, BUFFEER, Queries::SelectPostById::QUERY_TEXT.c_str(), post_id);

        post = SelectFeedDataByQuery(query);
    }
    return post;
}

Post* CacheManager::SelectFeedDataByAutoId(int autoId)
{
    Post * post = NULL;
    Log::debug_tag(LOG_TAG, "Select post record autoId = %d",autoId);

    char query[BUFFEER];
    Utils::Snprintf_s(query, BUFFEER, Queries::SelectPostById::BY_AUTO_ID_QUERY_TEXT.c_str(), autoId);

    post = SelectFeedDataByQuery(query);

    return post;
}

int CacheManager::select_feed_callback(void *pArg, int argc, char **argv,
            char **columnName)
{
    Eina_List **list = (Eina_List **) pArg;
    Post *data = new Post(columnName, argv);

    *list = eina_list_append(*list, data);
    return 0;
}

int CacheManager::select_feed_callback_by_id(void *pArg, int argc, char **argv,
            char **columnName)
{
    Post **post = (Post **) pArg;
    if(post) {
        *post = new Post(columnName, argv);
    }

    return 0;
}

bool CacheManager::SetPhotoTable(Eina_List *photoList, const char *id, int autoPostId)
{
    Log::debug_tag(LOG_TAG, "Set post\'s photo");
    bool result = true;

    Eina_List *photoListEl;
    void *postListData;
    EINA_LIST_FOREACH(photoList, photoListEl, postListData) {
        std::string photoQuery = "INSERT OR REPLACE INTO " + Tables::Photos::TABLE_NAME + " VALUES";
        Photo *photoData = static_cast<Photo *>(postListData);

        char photoWidth[20];
        Utils::Snprintf_s(photoWidth, 20, "%d", photoData->GetWidth());

        char photoHeigth[20];
        Utils::Snprintf_s(photoHeigth, 20, "%d", photoData->GetHeight());

        char postAutoId[20];
        Utils::Snprintf_s(postAutoId, 20, "%d", autoPostId);

        photoQuery = photoQuery + "(\'" + (id != NULL ? id : "NULL") + "\', "
                + "\'" + photoData->GetSrcUrl() + "\', "
                + "\'" + ((photoData->GetFileName() != NULL) ? photoData->GetFileName() : "NULL") + "\', "
                + "\'" + photoWidth + "\', "
                + "\'" + photoHeigth + "\', "
                + "\'" +  postAutoId + "\'), ";

        photoQuery.erase(photoQuery.size() - 2);
        photoQuery += ";";

        Log::debug_tag(LOG_TAG, "SQL query photo: %s", photoQuery.c_str());
        char *ErrMsg = NULL;
        int res = sqlite3_exec(mDB, photoQuery.c_str(), log_callback, 0, &ErrMsg);
        if (res != SQLITE_OK) {
            Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
            sqlite3_free(ErrMsg);
            result = false;
        }
    }
    return result;
}

int CacheManager::select_photo_callback(void *pArg, int argc, char **argv,
            char **columnName)
{
    PostsPhotoData **photoData = (PostsPhotoData **) pArg;

    if ((*photoData)->mLastPost) {
        if (!strcmp((*photoData)->mLastPost->GetId(), argv[Queries::SelectPhotosById::POST_ID])) {
            (*photoData)->mLastPhotoList = AppendPhotoToList((*photoData)->mLastPhotoList, argv);
            return 0;
        }
        else {
            (*photoData)->mLastPost->SetPhotoList((*photoData)->mLastPhotoList);
        }
    }

    Eina_List *listEl;
    void *listData;
    EINA_LIST_FOREACH((*photoData)->mPostsList, listEl, listData)
    {
        Post *postData = (Post *) listData;

        if (!strcmp(postData->GetId(), argv[Queries::SelectPhotosById::POST_ID])) {
            (*photoData)->mLastPhotoList = AppendPhotoToList(postData->GetPhotoList(), argv);
            (*photoData)->mLastPost = postData;
            postData->SetPhotoList((*photoData)->mLastPhotoList);
            break;
        }
    }
    return 0;
}

int CacheManager::select_images_callback(void *pArg, int argc, char **argv,
            char **columnName)
{
    PostsPhotoData **photoData = (PostsPhotoData **) pArg;

    if ((*photoData)->mLastPost) {
        if (!strcmp((*photoData)->mLastPost->GetId(), argv[Queries::SelectPhotosById::POST_ID])) {
            (*photoData)->mLastPhotoList = AppendPhotoToList((*photoData)->mLastPhotoList, argv);
            return 0;
        }
        else {
            (*photoData)->mLastPost->SetImageList((*photoData)->mLastPhotoList);
        }
    }

    Eina_List *listEl;
    void *listData;
    EINA_LIST_FOREACH((*photoData)->mPostsList, listEl, listData)
    {
        Post *postData = (Post *) listData;

        if (!strcmp(postData->GetId(), argv[Queries::SelectPhotosById::POST_ID])) {
            (*photoData)->mLastPhotoList = AppendPhotoToList(postData->GetImageList(), argv);
            (*photoData)->mLastPost = postData;
            postData->SetImageList((*photoData)->mLastPhotoList);
            break;
        }
    }
    return 0;
}

void CacheManager::CascadeDeletePhotos(const char* post_id) {
    Log::debug_tag(LOG_TAG, "CacheManager::CascadeDeletePost, Try to delete post_id %s",post_id);
    Post *post = SelectFeedDataById(post_id);

    if (post){
        //Delete photos from file system

        Eina_List *listPhotoEl;
        void *listPhotoData;

        EINA_LIST_FOREACH(post->GetPhotoList(), listPhotoEl, listPhotoData)
        {
            Photo *photo = static_cast<Photo *>(listPhotoData);

            if (photo && photo->GetFileName()) {
                const char *fileName = photo->GetFileName();
                char fullFileName [MAX_FULL_PATH_NAME_LENGTH];
                char * sharedPath = app_get_shared_trusted_path();
                Utils::Snprintf_s(fullFileName, MAX_FULL_PATH_NAME_LENGTH, "%s%s/%s", sharedPath, DIR_IMAGES_NAME, fileName);
                free(sharedPath);

                if( remove(fullFileName) == 0 ){
                    Log::debug_tag(LOG_TAG, "CacheManager::CascadeDeletePhotos, file %s removed", fullFileName);
                } else {
                    Log::debug_tag(LOG_TAG, "CacheManager::CascadeDeletePhotos, file %s not found", fullFileName);
                }
            }
        }
        post->Release();
    }
    //delete photos from db.
    DeletePhotosbyPostId(post_id);
}

void CacheManager::DeletePhotosbyPostId(const char* post_id) {
    //delete photo record related to post from DB, even if no photo was found -
    //post record will be removed from cache database in any case and photo record will waste
    char *ErrMsg = nullptr;
    int res = 0;
    char query_del_photo[BUFFEER];
    Utils::Snprintf_s(query_del_photo, BUFFEER, Queries::DeletePhotosById::QUERY_TEXT.c_str(), post_id);
    res = sqlite3_exec(mDB, query_del_photo, log_callback, 0, &ErrMsg);
    if (res != SQLITE_OK) {
        Log::error_tag( LOG_TAG, "CacheManager::CascadeDeletePhotos, SQL error: %s", ErrMsg);
        sqlite3_free(ErrMsg);
    }
}

void CacheManager::DeletePost(const char* post_id) {
    char *ErrMsg = NULL;
    int res = 0;

    CascadeDeletePhotos(post_id);

    //Delete post record from DB
    char query_del_post[BUFFEER];
    Utils::Snprintf_s(query_del_post, BUFFEER, Queries::DeletePostById::QUERY_TEXT.c_str(), post_id);

    res = sqlite3_exec(mDB, query_del_post, log_callback, 0, &ErrMsg);
    if (res != SQLITE_OK) {
        Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
        sqlite3_free(ErrMsg);
    }
}

Eina_List *CacheManager::AppendPhotoToList(Eina_List *photoList, char **columnName) {

    Photo *photo = new Photo();
    photo->SetHeight(atoi(columnName[Queries::SelectPhotosById::HEIGHT]));
    photo->SetWidth(atoi(columnName[Queries::SelectPhotosById::WIDTH]));
    char *src = Utils::GetStringOrNull(columnName[Queries::SelectPhotosById::SOURCE]);
    photo->SetSrcUrl(src);
    free (src);

    photoList = eina_list_append(photoList, photo);
    return photoList;
}

bool CacheManager::CreatePostPositons(int position, int autoPostId, int autoSessionId)
{
    bool result = true;

    std::string posQuery = "INSERT INTO " + Tables::PostOrder::TABLE_NAME + " VALUES";

    char pos[20];
    Utils::Snprintf_s(pos, 20, "%d", position);

    char postAutoId[20];
    Utils::Snprintf_s(postAutoId, 20, "%d", autoPostId);

    char sessionAutoId[20];
    Utils::Snprintf_s(sessionAutoId, 20, "%d", autoSessionId);

    posQuery = posQuery + "(\'" + pos + "\', "
           + "\'" + postAutoId + "\', "
           + "\'" + sessionAutoId + "\'), ";

    posQuery.erase(posQuery.size() - 2);
    posQuery += ";";

    Log::debug_tag(LOG_TAG, "SQL query photo: %s", posQuery.c_str());
    char *ErrMsg = NULL;
    int res = sqlite3_exec(mDB, posQuery.c_str(), log_callback, 0, &ErrMsg);
    if (res != SQLITE_OK) {
        Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
        sqlite3_free(ErrMsg);
        result = false;
    }
    return result;
}

bool CacheManager::CreateSessionData(const char * create_date)
{
    Log::debug_tag(LOG_TAG, "Set session\'s data");
    bool result = true;

    std::string fields = " (" + Tables::Sessions::Columns::SESSION_START_DATE
            + "," + Tables::Sessions::Columns::SESSION_END_DATE + ") ";

    std::string query = "INSERT INTO " + Tables::Sessions::TABLE_NAME + fields + " VALUES";

    query = query + " (\'" + (create_date != NULL ? create_date : "NULL") + "\', "
            + "\'" +  "NULL" + "\'), ";

    query.erase(query.size() - 2);
    query += ";";

    Log::debug_tag(LOG_TAG, "SQL query session: %s", query.c_str());

    char *ErrMsg = NULL;
    int res = sqlite3_exec(mDB, query.c_str(), log_callback, 0, &ErrMsg);
    if (res != SQLITE_OK) {
        Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
        sqlite3_free(ErrMsg);
        result = false;
    }
    return result;
}

Eina_List* CacheManager::SelectPostOrderData(int begin, int end)
{
    Eina_List *list = NULL;
    if (begin > 0 && end > 0) {

        char query[BUFFEER];
        Utils::Snprintf_s(query, BUFFEER, Queries::SelectPostOrder::QUERY_TEXT.c_str(), begin, end);

        Log::debug_tag(LOG_TAG, "SQL  = SelectPostOrderData = %s",query);

        char *ErrMsg = NULL;
        int res = sqlite3_exec(mDB, query, select_post_order_callback, &list, &ErrMsg);
        if (res != SQLITE_OK) {
            Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
            sqlite3_free(ErrMsg);
        }
    }
    return list;
}

int CacheManager::sort_cb(const void *data1, const void *data2)
{
    const Order *order1 = static_cast<const Order *>(data1);
    const Order *order2 = static_cast<const Order *>(data2);
    return (order1->mPosition > order2->mPosition ? 1 : -1);
}

Eina_List* CacheManager::SelectPostOrderDataBySessionId(int session)
{
    Eina_List *list = NULL;
    if (session > 0) {

        char query[BUFFEER];
        Utils::Snprintf_s(query, BUFFEER, Queries::SelectPostOrderBySessionId::QUERY_TEXT.c_str(), session);

        Log::debug_tag(LOG_TAG, "SQL  = SelectPostOrderDataBySesionId = %s",query);

        char *ErrMsg = NULL;
        int res = sqlite3_exec(mDB, query, select_post_order_by_session_callback, &list, &ErrMsg);
        if (res != SQLITE_OK) {
            Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
            sqlite3_free(ErrMsg);
        }
    }
    list = eina_list_sort(list, 0 ,sort_cb);
    return list;
}
/**
 * This function validate was post updated or not based on
 * updated time field value
 */
bool CacheManager::isPostUpdated(const char* post_id, const char* time){
    bool result = false;
    char *ErrMsg = NULL;

    char query[BUFFEER];
    Utils::Snprintf_s(query, BUFFEER, Queries::SelectUpdatedTimeByPostId::QUERY_TEXT.c_str(), post_id);

    int res = sqlite3_exec(mDB, query, select_get_data_callback, NULL, &ErrMsg);

    if (res != SQLITE_OK) {
        Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
        sqlite3_free(ErrMsg);
    } else {
        Log::debug_tag(LOG_TAG, "CacheManager::isPostUpdated, post: %s select data = %s and updatedTime = %s ", post_id,GetInstance().GetDBValue(), time);

        if(strcmp(GetInstance().GetDBValue(),time)){
            result = true;
        }
    }
    return result;
}

int CacheManager::GetSession()
{
    int  result = 0;
    char *ErrMsg = NULL;

    int res = sqlite3_exec(mDB, Queries::SelectSessionMaxAutoId::QUERY_TEXT.c_str(), select_get_data_callback, NULL, &ErrMsg);

    if (res != SQLITE_OK) {
        Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
        sqlite3_free(ErrMsg);
    } else {
        Log::debug_tag(LOG_TAG, "CacheManager::GetSession, sessionId: = %s ", GetInstance().GetDBValue());
        result = atoi(GetInstance().GetDBValue());
    }

    return result;
}

char* CacheManager::GetPostIdByPostAutoId( int postAutoId )
{
    char  *result = NULL;
    char *ErrMsg = NULL;

    if ( postAutoId > 0 ) {
        char query[BUFFEER];
        Utils::Snprintf_s(query, BUFFEER, Queries::SelectPostIdByPostAutoId::QUERY_TEXT.c_str(), postAutoId);

        if ( sqlite3_exec( mDB, query, select_get_data_callback, NULL, &ErrMsg ) != SQLITE_OK ) {
            Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
            sqlite3_free(ErrMsg);
        } else {
            Log::debug_tag(LOG_TAG, "CacheManager::GetPostIdByPostAutoId, id: = %s ", GetDBValue());

            result = strdup(GetDBValue());
        }
    }

    return result;
}

int CacheManager::GetPostAutoIdByPostId( const char *postId )
{
    int  result = 0;
    char *ErrMsg = NULL;

    if ( postId ) {
        char query[BUFFEER];
        Utils::Snprintf_s(query, BUFFEER, Queries::SelectPostAutoIdByPostId::QUERY_TEXT.c_str(), postId);

        if ( sqlite3_exec( mDB, query, select_get_data_callback, NULL, &ErrMsg ) != SQLITE_OK ) {
            Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
            sqlite3_free(ErrMsg);
        } else {
            Log::debug_tag(LOG_TAG, "CacheManager::GetPostAutoIdByPostId, id: = %s ", GetDBValue());

            result = atoi(GetDBValue());
        }
    }

    return result;
}

int CacheManager::select_get_data_callback(void *pArg, int argc, char **argv, char **columnName)
{
    GetInstance().SetDBValue(argv[0]);
    return 0;
}

bool CacheManager::UpdateFeedDataById(const char* post_id){
    Log::debug_tag(LOG_TAG, "Update Feed data into DB for post: %s", post_id);

    Post *postData = HomeProvider::GetInstance()->GetPostById(post_id);
    return UpdateFeedDataByPost(postData);
}

bool CacheManager::UpdateFeedDataByPost(Post* postData) {
    bool result = true;
    char *ErrMsg = NULL;
    int res = 0;

    if (postData){
        postData->AddRef();

        std::string PostId = postData->GetId();
        Log::debug_tag(LOG_TAG, "CacheManager::UpdateFeedDataByPost: %s", PostId.c_str());

        std::string query = "UPDATE "+ Tables::Posts::TABLE_NAME + " SET ";

        char createdTime[20];
        Utils::Snprintf_s(createdTime, 20, "%.0f", postData->GetCreatedTime());

        char updatedTime[20];
        Utils::Snprintf_s(updatedTime, 20, "%.0f", postData->GetUpdatedTime());

        char likesCount[20];
        Utils::Snprintf_s(likesCount, 20, "%d", postData->GetLikesCount());

        char withTagsCount[20];
        Utils::Snprintf_s(withTagsCount, 20, "%d", postData->GetWithTagsCount());

        char canLike[20];
        Utils::Snprintf_s(canLike, 20, "%d", postData->GetCanLike()? 1 : 0);

        char hasLiked[20];
        Utils::Snprintf_s(hasLiked, 20, "%d", postData->GetHasLiked()? 1 : 0);

        char commentsCount[20];
        Utils::Snprintf_s(commentsCount, 20, "%d", postData->GetCommentsCount());

        char canComment[20];
        Utils::Snprintf_s(canComment, 20, "%d", postData->GetCanComment()? 1 : 0);

        char canShare[20];
        Utils::Snprintf_s(canShare, 20, "%d", postData->GetCanShare() ? 1 : 0);

        char photosCount[20];
        Utils::Snprintf_s(photosCount, 20, "%d", postData->GetPhotoCount());

        char attachmentHeight[20];
        Utils::Snprintf_s(attachmentHeight, 20, "%d", postData->GetAttachmentHeight());

        char attachmentWidth[20];
        Utils::Snprintf_s(attachmentWidth, 20, "%d", postData->GetAttachmentWidth());

        query = query +  Tables::Posts::Columns::CREATED_TIME + " = " + "\'" + createdTime + "\', "
                + Tables::Posts::Columns::UPDATED_TIME + " = " + "\'" + updatedTime + "\', "
                + Tables::Posts::Columns::CAPTION + " = " + "\'" + (!postData->GetCaption().empty() ?
                        Utils::ReplaceString(postData->GetCaption(), "\'", "\'\'") : "NULL") + "\', "
                + Tables::Posts::Columns::DESCRIPTION + " = " +  "\'" + (postData->GetDescription() ?
                        Utils::ReplaceString(postData->GetDescription(), "\'", "\'\'") : "NULL") + "\', "
                + Tables::Posts::Columns::FROM_NAME + " = "  + "\'" + (!postData->GetFromName().empty() ?
                        Utils::ReplaceString(postData->GetFromName(), "\'", "\'\'") : "NULL") + "\', "
                + Tables::Posts::Columns::FROM_ID + " = " + "\'" + (!postData->GetFromId().empty() ? postData->GetFromId() : "NULL") + "\', "
                + Tables::Posts::Columns::FROM_PICTURE + " = " + "\'" + (postData->GetFromPictureLink() != NULL ? postData->GetFromPictureLink() : "NULL") + "\', "
                + Tables::Posts::Columns::FULL_PICTURE + " = " + "\'" + (postData->GetFromPictureLink() != NULL ? postData->GetFromPictureLink() : "NULL") + "\', "
                + Tables::Posts::Columns::LINK + " = " + "\'" + (postData->GetLink() != NULL ? postData->GetLink() : "NULL") + "\', "
                + Tables::Posts::Columns::MESSAGE + " = " + "\'" + (postData->GetMessage() ?
                        Utils::ReplaceString(postData->GetMessage(), "\'", "\'\'") : "NULL") + "\', ";

        std::list<Tag> messageTagsList(std::move(postData->GetMessageTagsList()));
        if (messageTagsList.size()) {
            query += Tables::Posts::Columns::MESSAGE_TAGS + " = " + "\'" + tagsToJSON(messageTagsList) + "\', ";
            Log::debug_tag(LOG_TAG, "SetFeedData, query Message tags %s", query.c_str());
        }
        else {
            query += Tables::Posts::Columns::MESSAGE_TAGS + " = " + "\'NULL\', ";
        }

        query = query + Tables::Posts::Columns::OBJECT_ID + " = " + "\'" + (postData->GetObjectId() != NULL ? postData->GetObjectId() : "NULL") + "\', "
                + Tables::Posts::Columns::PARENT_ID + " = " + "\'" + (postData->GetParentId() != NULL ? postData->GetParentId() : "NULL") + "\', ";

        //Place
        if (postData->GetPlace()) {
            char latitude[20];
            Utils::Snprintf_s(latitude, 20, "%.0f", postData->GetPlace()->mLatitude);

            char longitude[20];
            Utils::Snprintf_s(longitude, 20, "%.0f", postData->GetPlace()->mLongitude);

            char checkins[20];
            Utils::Snprintf_s(checkins, 20, "%d", postData->GetPlace()->mCheckins);

            query = query + Tables::Posts::Columns::PLACE_ID + " = " + "\'" + (postData->GetPlace()->mId != NULL ? postData->GetPlace()->mId : "NULL") + "\', "
                    + Tables::Posts::Columns::PLACE_NAME + " = " + "\'" + (postData->GetPlace()->mName != NULL ? postData->GetPlace()->mName : "NULL") + "\', "
                    + Tables::Posts::Columns::PLACE_CITY + " = " + "\'" + (postData->GetPlace()->mCity != NULL ? postData->GetPlace()->mCity : "NULL") + "\', "
                    + Tables::Posts::Columns::PLACE_COUNTRY + " = " + "\'" + (postData->GetPlace()->mCountry != NULL ? postData->GetPlace()->mCountry : "NULL") + "\', "
                    + Tables::Posts::Columns::PLACE_LATITUDE + " = " + "\'" + latitude + "\', "
                    + Tables::Posts::Columns::PLACE_LONGITUDE + " = " + "\'" + longitude + "\', "
                    + Tables::Posts::Columns::PLACE_STREET + " = " + "\'" + (postData->GetPlace()->mStreet != NULL ? postData->GetPlace()->mStreet : "NULL") + "\', "
                    + Tables::Posts::Columns::PLACE_CHECKINS + " = " + "\'" + checkins + "\', ";
        }
        else {
            query += Tables::Posts::Columns::PLACE_ID + " = " + "\'NULL\', "
                    + Tables::Posts::Columns::PLACE_NAME + " = " + "\'NULL\', "
                    + Tables::Posts::Columns::PLACE_CITY + " = " + "\'NULL\', "
                    + Tables::Posts::Columns::PLACE_COUNTRY + " = " + "\'NULL\', "
                    + Tables::Posts::Columns::PLACE_LATITUDE + " = " + "\'NULL\', "
                    + Tables::Posts::Columns::PLACE_LONGITUDE + " = " +"\'NULL\', "
                    + Tables::Posts::Columns::PLACE_STREET + " = " + "\'NULL\', "
                    + Tables::Posts::Columns::PLACE_CHECKINS + " = " + "\'NULL\', ";
        }



        //Privacy
        if (postData->GetPrivacy()) {
            query = query + Tables::Posts::Columns::PRIVACY_VALUE + " = " +  "\'" + (postData->GetPrivacy()->GetValue() != NULL ? postData->GetPrivacy()->GetValue() : "NULL") + "\', "
                    + Tables::Posts::Columns::PRIVACY_DESCRIPTION + " = " + "\'" + (postData->GetPrivacy()->GetDescription() != NULL ? postData->GetPrivacy()->GetDescription() : "NULL") + "\', "
                    + Tables::Posts::Columns::PRIVACY_FRIENDS + " = " +  "\'" + (postData->GetPrivacy()->GetFriends() != NULL ? postData->GetPrivacy()->GetFriends() : "NULL") + "\', "
                    + Tables::Posts::Columns::PRIVACY_ALLOW + " = " +  "\'" + (postData->GetPrivacy()->GetAllow() != NULL ? postData->GetPrivacy()->GetAllow() : "NULL") + "\', "
                    + Tables::Posts::Columns::PRIVACY_DENY + " = " + "\'" + (postData->GetPrivacy()->GetDeny() != NULL ? postData->GetPrivacy()->GetDeny() : "NULL") + "\', ";
         }
         else {
            query += Tables::Posts::Columns::PRIVACY_VALUE + " = " +  "\'NULL\', "
                    + Tables::Posts::Columns::PRIVACY_DESCRIPTION + " = " + "\'NULL\', "
                    + Tables::Posts::Columns::PRIVACY_FRIENDS + " = " + "\'NULL\', "
                    + Tables::Posts::Columns::PRIVACY_ALLOW + " = " +  "\'NULL\', "
                    + Tables::Posts::Columns::PRIVACY_DENY + " = " +  "\'NULL\', ";
         }



         query = query + Tables::Posts::Columns::SOURCE + " = " + "\'" + (postData->GetSource() != NULL ? postData->GetSource() : "NULL") + "\', "
                  + Tables::Posts::Columns::STATUS_TYPE + " = " + "\'" + (postData->GetStatusType() != NULL ? postData->GetStatusType() : "NULL") + "\', "
                  + Tables::Posts::Columns::STORY + " = " + "\'" + (!postData->GetStory().empty() ? Utils::ReplaceString(postData->GetStory(), "\'", "\'\'") : "NULL") + "\', ";


         std::list<Tag> storyTagsList(std::move(postData->GetStoryTagsList()));
         if (storyTagsList.size()) {
             query += Tables::Posts::Columns::STORY_TAGS + " = " + "\'" + tagsToJSON(storyTagsList) + "\', ";
         }
         else {
             query += Tables::Posts::Columns::STORY_TAGS + " = " + "\'NULL\', ";
         }



         query = query + Tables::Posts::Columns::TYPE + " = " + "\'" + (postData->GetType() != NULL ? postData->GetType() : "NULL") + "\', "
                  + Tables::Posts::Columns::LIKES_COUNT + " = " + "\'" + likesCount + "\', "
                  + Tables::Posts::Columns::COMMENTS_COUNT + " = " + "\'" + commentsCount + "\', "
                  + Tables::Posts::Columns::PHOTO_COUNT + " = " + "\'" + photosCount  + "\', "
                  + Tables::Posts::Columns::ADDED_STHG + " = " + "\'" + (postData->GetAddedSthgCount()!= NULL ?Utils::ReplaceString(postData->GetAddedSthgCount(), "\'", "\'\'") : "NULL") + "\', "
                  + Tables::Posts::Columns::CHILD_ID + " = " + "\'" + (postData->GetChildId()!= NULL ?Utils::ReplaceString(postData->GetChildId(), "\'", "\'\'") : "NULL") + "\', "
                  + Tables::Posts::Columns::COMMENT + " = " + "\'" + (postData->GetCommentText()!= NULL ?Utils::ReplaceString(postData->GetCommentText(), "\'", "\'\'") : "NULL") + "\', "
                  + Tables::Posts::Columns::COMMUTITY_AVATAR + " = " + "\'" + (postData->GetCommunityAvatar()!= NULL ?Utils::ReplaceString(postData->GetCommunityAvatar(), "\'", "\'\'") : "NULL") + "\', "
                  + Tables::Posts::Columns::COMMUTITY_NAME + " = " + "\'" + (postData->GetCommunityName()!= NULL ?Utils::ReplaceString(postData->GetCommunityName(), "\'", "\'\'") : "NULL") + "\', "
                  + Tables::Posts::Columns::COMMUTITY_SUBSCRIBERS + " = " + "\'" + (postData->GetCommunitySubscribers()!= NULL ?Utils::ReplaceString(postData->GetCommunitySubscribers(), "\'", "\'\'") : "NULL") + "\', "
                  + Tables::Posts::Columns::COMMUTITY_TYPE + " = " + "\'" + (postData->GetCommunityType()!= NULL ?Utils::ReplaceString(postData->GetCommunityType(), "\'", "\'\'") : "NULL") + "\', "
                  + Tables::Posts::Columns::COMMUTITY_WALLPAPER + " = " + "\'" + (postData->GetCommunityWallpaper()!= NULL ?Utils::ReplaceString(postData->GetCommunityWallpaper(), "\'", "\'\'") : "NULL") + "\', "
                  + Tables::Posts::Columns::EVENT_COVER + " = " + "\'" + (postData->GetEventCover()!= NULL ?Utils::ReplaceString(postData->GetEventCover(), "\'", "\'\'") : "NULL") + "\', "
                  + Tables::Posts::Columns::EVENT_INFO + " = " + "\'" + (postData->GetEventInfo()!= NULL ?Utils::ReplaceString(postData->GetEventInfo(), "\'", "\'\'") : "NULL") + "\', "
                  + Tables::Posts::Columns::FROM_PICTURE_PATH + " = " + "\'" + (postData->GetFromPicturePath()!= NULL ?Utils::ReplaceString(postData->GetFromPicturePath(), "\'", "\'\'") : "NULL") + "\', "
                  + Tables::Posts::Columns::LOCATION_AVATAR_PATH + " = " + "\'" + (postData->GetLocationAvatar()!= NULL ?Utils::ReplaceString(postData->GetLocationAvatar(), "\'", "\'\'") : "NULL") + "\', "
                  + Tables::Posts::Columns::PLACE_TYPE + " = " + "\'" + (!postData->GetLocationType().empty() ?Utils::ReplaceString(postData->GetLocationType(), "\'", "\'\'") : "NULL") + "\', "
                  + Tables::Posts::Columns::PLACE_VISITORS + " = " +  "\'" + (postData->GetLocationVisitors()!= NULL ?Utils::ReplaceString(postData->GetLocationVisitors(), "\'", "\'\'") : "NULL") + "\', "
                  + Tables::Posts::Columns::NAME + " = " + "\'" + (!postData->GetName().empty() ?Utils::ReplaceString(postData->GetName(), "\'", "\'\'") : "NULL") + "\', "
                  + Tables::Posts::Columns::PLACE_PICTURE + " = " + "\'" + (postData->GetPlacePictureLink()!= NULL ?Utils::ReplaceString(postData->GetPlacePictureLink(), "\'", "\'\'") : "NULL") + "\', "
                  + Tables::Posts::Columns::SONG_ARTIST + " = " + "\'" + (postData->GetSongArtist()!= NULL ?Utils::ReplaceString(postData->GetSongArtist(), "\'", "\'\'") : "NULL") + "\', "
                  + Tables::Posts::Columns::SONG_NAME + " = " + "\'" + (postData->GetSongName()!= NULL ?Utils::ReplaceString(postData->GetSongName(), "\'", "\'\'") : "NULL") + "\', "
                  + Tables::Posts::Columns::NEW_FRIEND_ID + " = " +  "\'" + (postData->GetNewFriendId()!= NULL ?Utils::ReplaceString(postData->GetNewFriendId(), "\'", "\'\'") : "NULL") + "\', "
                  + Tables::Posts::Columns::NEW_FRIEND_AVATAR + " = " + "\'" + (postData->GetNewFriendAvatar()!= NULL ?Utils::ReplaceString(postData->GetNewFriendAvatar(), "\'", "\'\'") : "NULL") + "\', "
                  + Tables::Posts::Columns::NEW_FRIEND_CAREER + " = " + "\'" + (postData->GetNewFriendCareer()!= NULL ?Utils::ReplaceString(postData->GetNewFriendCareer(), "\'", "\'\'") : "NULL") + "\', "
                  + Tables::Posts::Columns::NEW_FRIEND_MUTAL_FRIENDS + " = " + "\'" + (postData->GetNewFriendMutualFriends()!= NULL ?Utils::ReplaceString(postData->GetNewFriendMutualFriends(), "\'", "\'\'") : "NULL") + "\', "
                  + Tables::Posts::Columns::NEW_FRIEND_WORK + " = " +"\'" + (postData->GetNewFriendWork()!= NULL ?Utils::ReplaceString(postData->GetNewFriendWork(), "\'", "\'\'") : "NULL") + "\', "
                  + Tables::Posts::Columns::NEW_FRIEND_EDUCATION + " = " + "\'" + (postData->GetNewFriendEducation()!= NULL ?Utils::ReplaceString(postData->GetNewFriendEducation(), "\'", "\'\'") : "NULL") + "\', "
                  + Tables::Posts::Columns::NEW_FRIEND_COVER + " = " +"\'" + (postData->GetNewFriendCover()!= NULL ?Utils::ReplaceString(postData->GetNewFriendCover(), "\'", "\'\'") : "NULL") + "\', "
                  + Tables::Posts::Columns::CAN_LIKE + " = " + "\'" + canLike + "\', "
                  + Tables::Posts::Columns::HAS_LIKED + " = " + "\'" + hasLiked + "\', "
                  + Tables::Posts::Columns::CAN_COMMENT + " = " + "\'" + canComment + "\', "
                  + Tables::Posts::Columns::CAN_SHARE + " = " + "\'" + canShare + "\', "
                  + Tables::Posts::Columns::TO_NAME + " = "  + "\'" + (postData->GetToName() ? Utils::ReplaceString(postData->GetToName(), "\'", "\'\'") : "NULL") + "\', "
                  + Tables::Posts::Columns::TO_ID + " = " + "\'" + (postData->GetToId() ? postData->GetToId() : "NULL") + "\', "
                  + Tables::Posts::Columns::VIA_ID + " = " + "\'" + ((postData->GetVia() && postData->GetVia()->mId) ? postData->GetVia()->mId : "NULL") + "\', "
                  + Tables::Posts::Columns::VIA_NAME + " = " + "\'" + ((postData->GetVia() && postData->GetVia()->mName) ? postData->GetVia()->mName : "NULL") + "\', "
                  + Tables::Posts::Columns::GROUP_NAME + " = " + "\'" + (postData->GetGroupName() ? postData->GetGroupName() : "NULL") + "\', "
                  + Tables::Posts::Columns::GROUP_STATUS + " = " + "\'NULL\', "
                  + Tables::Posts::Columns::GROUP_ID + " = " + "\'" + (postData->GetGroupId() ? postData->GetGroupId() : "NULL") + "\', "
                  + Tables::Posts::Columns::LIKE_FROM_NAME + " = " + "\'" + (postData->GetLikeFromName() ? postData->GetLikeFromName() : "NULL") + "\', "
                  + Tables::Posts::Columns::LIKE_FROM_ID + " = " + "\'" + (postData->GetLikeFromId() ? postData->GetLikeFromId() : "NULL") + "\', "
                  + Tables::Posts::Columns::ATTACHMENT_DESCRIPTION + " = " + "\'" + (!postData->GetAttachmentDescription().empty() ? postData->GetAttachmentDescription() : "NULL") + "\', "
                  + Tables::Posts::Columns::ATTACHMENT_SRC + " = " + "\'" + (postData->GetAttachmentSrc() ? postData->GetAttachmentSrc() : "NULL") + "\', "
                  + Tables::Posts::Columns::ATTACHMENT_HEIGHT + " = " + "\'" + attachmentHeight + "\', "
                  + Tables::Posts::Columns::ATTACHMENT_WIDTH + " = " + "\'" + attachmentWidth + "\', "
                  + Tables::Posts::Columns::ATTACHMENT_TARGET_ID + " = " + "\'" + (postData->GetAttachmentTargetId() ? postData->GetAttachmentTargetId() : "NULL") + "\', "
                  + Tables::Posts::Columns::ATTACHMENT_TARGET_TITLE + " = " + "\'" + (!postData->GetAttachmentTitle().empty() ? postData->GetAttachmentTitle() : "NULL") + "\', "
                  + Tables::Posts::Columns::ATTACHMENT_TARGET_URL + " = " + "\'" + (!postData->GetAttachmentTargetUrl().empty() ? postData->GetAttachmentTargetUrl() : "NULL") + "\', "
                  + Tables::Posts::Columns::WITH_TAGS_NAME + " = " + "\'" + (!postData->GetWithTagsName().empty() ? postData->GetWithTagsName() : "NULL") + "\', "
                  + Tables::Posts::Columns::WITH_TAGS_COUNT + " = " + "\'" + withTagsCount + "\', "
                  + Tables::Posts::Columns::WITH_TAGS_ID + " = " + "\'" + (!postData->GetWithTagsId().empty() ? postData->GetWithTagsId() : "NULL") + "\', ";

         //ImplicitPlace
         if (postData->GetImplicitPlace()) {
             char impLatitude[20];
             Utils::Snprintf_s(impLatitude, 20, "%.0f", postData->GetImplicitPlace()->mLatitude);

             char impLongitude[20];
             Utils::Snprintf_s(impLongitude, 20, "%.0f", postData->GetImplicitPlace()->mLongitude);

             query = query + Tables::Posts::Columns::IMPLICIT_PLACE_ID + " = " + "\'" + (postData->GetImplicitPlace()->mId != NULL ? postData->GetImplicitPlace()->mId : "NULL") + "\', "
                     + Tables::Posts::Columns::IMPLICIT_PLACE_NAME + " = " + "\'" + (postData->GetImplicitPlace()->mName != NULL ? postData->GetImplicitPlace()->mName : "NULL") + "\', "
                     + Tables::Posts::Columns::IMPLICIT_PLACE_CITY + " = " + "\'" + (postData->GetImplicitPlace()->mCity != NULL ? postData->GetImplicitPlace()->mCity : "NULL") + "\', "
                     + Tables::Posts::Columns::IMPLICIT_PLACE_COUNTRY + " = " + "\'" + (postData->GetImplicitPlace()->mCountry != NULL ? postData->GetImplicitPlace()->mCountry : "NULL") + "\', "
                     + Tables::Posts::Columns::IMPLICIT_PLACE_LATITUDE + " = " + "\'" + impLatitude + "\', "
                     + Tables::Posts::Columns::IMPLICIT_PLACE_LONGITUDE + " = " + "\'" + impLongitude + "\', "
                     + Tables::Posts::Columns::IMPLICIT_PLACE_STREET + " = " + "\'" + (postData->GetImplicitPlace()->mStreet != NULL ? postData->GetImplicitPlace()->mStreet : "NULL") + "\', ";
         } else {
             query += Tables::Posts::Columns::IMPLICIT_PLACE_ID + " = " + "\'NULL\', "
                     + Tables::Posts::Columns::IMPLICIT_PLACE_NAME + " = " + "\'NULL\', "
                     + Tables::Posts::Columns::IMPLICIT_PLACE_CITY + " = " + "\'NULL\', "
                     + Tables::Posts::Columns::IMPLICIT_PLACE_COUNTRY + " = " + "\'NULL\', "
                     + Tables::Posts::Columns::IMPLICIT_PLACE_LATITUDE + " = " + "\'NULL\', "
                     + Tables::Posts::Columns::IMPLICIT_PLACE_LONGITUDE + " = " +"\'NULL\', "
                     + Tables::Posts::Columns::IMPLICIT_PLACE_STREET + " = " + "\'NULL\', ";
         }

         query += Tables::Posts::Columns::TO_PROFILE_TYPE + " = \'" + (postData->GetToProfileType() ? postData->GetToProfileType() : "NULL") + "\' ";

         query += " WHERE id = \'" + PostId + "\';";

         Log::debug_tag(LOG_TAG, "SQL query: %s", query.c_str());
         res = sqlite3_exec(mDB, query.c_str(), log_callback, 0, &ErrMsg);
         if (res != SQLITE_OK) {
             Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
             sqlite3_free(ErrMsg);
             result = false;
         }

         //Update related photos
         if (postData->GetId()) {
             DeletePhotosbyPostId(postData->GetId());
             if (postData->GetPhotoCount()) {
                 int auto_id = GetPostAutoIdByPostId(postData->GetId());
                 SetPhotoTable(postData->GetPhotoList(), postData->GetId(), auto_id );
             }
             // instead of wrong update should delete all photos from db and create record again
         }
         postData->Release();
    }
    return result;
}

bool CacheManager::UpdateCommentsCountOfPostById(const char* post_id, int commentsCount)
{
    Log::debug_tag(LOG_TAG, "Update comments count into DB for post: %s", post_id);

    bool result = true;

    std::string query = "UPDATE " + Tables::Posts::TABLE_NAME + " SET ";

    char commentsCountString[20];
    Utils::Snprintf_s(commentsCountString, 20, "%d", commentsCount);

    query += Tables::Posts::Columns::COMMENTS_COUNT + " = " + "\'" + commentsCountString + "\' "
            + " WHERE id = \'" + post_id + "\';";

    Log::debug_tag(LOG_TAG, "SQL query: %s", query.c_str());
    char *ErrMsg = NULL;
    int res = 0;
    res = sqlite3_exec(mDB, query.c_str(), log_callback, 0, &ErrMsg);
    if (res != SQLITE_OK) {
        Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
        sqlite3_free(ErrMsg);
        result = false;
    }

    return result;
}

void CacheManager::SetDBValue(const char * value) {
    free(mDBValue);
    mDBValue = (value) ? strdup(value) : NULL;
};

bool CacheManager::UpdateSessionData(const char * end_date, int session)
{
    Log::debug_tag(LOG_TAG, "Update session\'s data");
    bool result = true;

    std::string query = "UPDATE " + Tables::Sessions::TABLE_NAME + " SET ";

    query = query + Tables::Sessions::Columns::SESSION_END_DATE + " = " +  " \'" + (end_date != NULL ? end_date : "NULL") + "\', ";

    query.erase(query.size() - 2);

    char sessionId[20];
    Utils::Snprintf_s(sessionId, 20, "%d", session);
    std::string session_id = sessionId;

    query += " WHERE session_auto_id = " + session_id + ";";

    Log::debug_tag(LOG_TAG, "SQL query update session: %s", query.c_str());

    char *ErrMsg = NULL;
    int res = sqlite3_exec(mDB, query.c_str(), log_callback, 0, &ErrMsg);
    if (res != SQLITE_OK) {
        Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
        sqlite3_free(ErrMsg);
        result = false;
    }
return result;
}

//Config::GetInstance().GetAccessToken(), Config::GetInstance().GetSessionKey(), Config::GetInstance().GetSessionSecret()

bool CacheManager::UpdateAccountData()
{
    bool result = true;

    char *ErrMsg = NULL;
    std::string query = "INSERT OR REPLACE INTO " +
                        Tables::Accounts::TABLE_NAME +
                        " VALUES(\'"+
                        Config::GetInstance().GetUserName() +
                        "\',\'" +
                        Config::GetInstance().GetUserId() +
                        "\',\'" +
                        Config::GetInstance().GetAccessToken() +
                        "\',\'" +
                        Config::GetInstance().GetSessionKey() +
                        "\',\'" +
                        Config::GetInstance().GetSessionSecret() +
                        "\');";
    int res = sqlite3_exec(mDB, query.c_str(), log_callback, 0, &ErrMsg);
    if (res != SQLITE_OK) {
        Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
        sqlite3_free(ErrMsg);
        result = false;
    }
    return result;
}

bool CacheManager::SelectAccountData(const char* userName,
        account_data* accData)
{
    bool result = true;
    char query[BUFFEER];
    Utils::Snprintf_s(query, BUFFEER, Queries::SelectAccounts::QUERY_TEXT.c_str(), userName);

    char *ErrMsg = NULL;
    int res = sqlite3_exec(mDB, query, select_account_callback, accData, &ErrMsg);
    if (res != SQLITE_OK) {
        Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
        sqlite3_free(ErrMsg);
        result = false;
    }
    return result;
}

int CacheManager::select_account_callback(void* pArg, int argc, char** argv,
        char** columnName)
{
    account_data *accData = (account_data *)pArg;
    accData->userId = Utils::GetStringOrNull(argv[Queries::SelectAccounts::USER_ID]);
    accData->accessToken = Utils::GetStringOrNull(argv[Queries::SelectAccounts::ACCESS_TOKEN]);
    accData->sessionKey  = Utils::GetStringOrNull(argv[Queries::SelectAccounts::SESSION_KEY]);
    accData->sessionSecret  = Utils::GetStringOrNull(argv[Queries::SelectAccounts::SESSION_SECRET]);
    return 0;
}

bool CacheManager::DeleteAccountData(const char* userName)
{
    bool result = true;
    char query[BUFFEER];
    Utils::Snprintf_s(query, BUFFEER, Queries::DeleteAccounts::QUERY_TEXT.c_str() ,userName);

    char *ErrMsg = NULL;
    int res = sqlite3_exec(mDB, query, log_callback, 0, &ErrMsg);
    if (res != SQLITE_OK) {
        Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
        sqlite3_free(ErrMsg);
        result = false;
    }
    return result;
}



bool CacheManager::SetLoggedInUserData(const char * userId)
{
    bool result = true;
    char *ErrMsg = NULL;
    std::string query = "INSERT INTO " + Tables::LoggedInUser::TABLE_NAME + " VALUES"
            + "(\'"+ userId + "\');";
    int res = sqlite3_exec(mDB, query.c_str(), log_callback, 0, &ErrMsg);
    if (res != SQLITE_OK) {
        Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
        sqlite3_free(ErrMsg);
        result = false;
    }
    Log::debug_tag(LOG_TAG, "SetLoggedInUserData() %s\n", result?"Success":"Failed");
    return result;
}

bool CacheManager::SelectLoggedInUser( loggedin_user_data *prevUserData)
{
    bool result = true;
    char *ErrMsg = NULL;
    int res = sqlite3_exec(mDB, Queries::SelectLoggedInUser::QUERY_TEXT.c_str(), select_loggedinuser_callback, (void *)prevUserData, &ErrMsg);
    if (res != SQLITE_OK) {
        Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
        sqlite3_free(ErrMsg);
        result = false;
    }
    Log::debug_tag(LOG_TAG, "SelectLoggedInUser() %s. Row Count is %d.\n", result?"Success":"Failed", prevUserData->rowCount);
    return result;
}

int CacheManager::select_loggedinuser_callback(void* pArg, int argc, char** argv,
        char** columnName)
{
    Log::debug_tag(LOG_TAG, "select_loggedinuser_callback() called\n");
    loggedin_user_data* prevUserData = static_cast<loggedin_user_data *>(pArg);
    (prevUserData->rowCount)++;
    prevUserData->prevUserId  = Utils::GetStringOrNull(argv[Queries::SelectLoggedInUser::USER_ID]);
    Log::debug_tag(LOG_TAG, "SelectLoggedInUser() Previous User Id : %s\n", prevUserData->prevUserId);
    return 0;
}

bool CacheManager::DeleteLoggedInUserData()
{
    bool result = true;
    char *ErrMsg = NULL;
    int res = sqlite3_exec(mDB, Queries::DeleteLoggedInUser::QUERY_TEXT.c_str(), log_callback, 0, &ErrMsg);
    if (res != SQLITE_OK) {
        Log::error_tag( LOG_TAG, "SQL error: %s", ErrMsg);
        sqlite3_free(ErrMsg);
        result = false;
    }
    Log::debug_tag(LOG_TAG, "DeleteLoggedInUserData() %s\n", result?"Success":"Failed");
    return result;
}
