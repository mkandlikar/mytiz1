#include <wav_player.h>
#include <sound_manager.h>
#include <runtime_info.h>
#include <system_settings.h>

#include "SoundNotificationPlayer.h"
#include "ErrorHandling.h"
#include "Common.h"

SoundNotificationPlayer::NotificationPathIdMap SoundNotificationPlayer::SoundPathMap;

void SoundNotificationPlayer::PlayNotification(SoundNotificationPlayer::NotificationSound sound) {
    if(IsDeviceMuted()) return;
    const char* path = GetNotificationSoundPath(sound);
    CHECK_RET_NRV(path);
//AAA (B2430):If jack is connected then we use SOUND_SESSION_TYPE_MEDIA. But it seems that we need to use the same value when it's disconnected.
    CHECK_RET_NRV(WAV_PLAYER_ERROR_NONE == wav_player_start(path, SOUND_TYPE_NOTIFICATION, nullptr, nullptr, nullptr));
}

bool SoundNotificationPlayer::IsDeviceMuted() {
    bool isVibrationMode(false);
    bool isSilentMode(false);
    CHECK_RET(RUNTIME_INFO_ERROR_NONE == runtime_info_get_value_bool(RUNTIME_INFO_KEY_VIBRATION_ENABLED, &isVibrationMode), false);
    CHECK_RET(SYSTEM_SETTINGS_ERROR_NONE == system_settings_get_value_bool(SYSTEM_SETTINGS_KEY_SOUND_SILENT_MODE, &isSilentMode), false);
    return isVibrationMode || isSilentMode;
}

//AAA (B2430):If jack is connected then we use SOUND_SESSION_TYPE_MEDIA. But it seems that we need to use the same value when it's disconnected.
// The function below isn't used now, but let's keep it commented out for a while. Probably some use cases are lost.
//bool SoundNotificationPlayer::IsJackConnected() {
//    bool isConnected = false;
//    CHECK_RET(RUNTIME_INFO_ERROR_NONE == runtime_info_get_value_bool(RUNTIME_INFO_KEY_AUDIO_JACK_CONNECTED, &isConnected), false);
//    return isConnected;
//}

const char* SoundNotificationPlayer::GetNotificationSoundPath(NotificationSound soundId) {
    if(SoundPathMap.size() == 0) {
        SoundPathMap.insert(NotificationPathIdPair(eSoundPostMain, SOUND_DIR "/post_main.wav"));
        SoundPathMap.insert(NotificationPathIdPair(eSoundComment, SOUND_DIR "/comment.wav"));
        SoundPathMap.insert(NotificationPathIdPair(eSoundShare, SOUND_DIR "/share.wav"));
        SoundPathMap.insert(NotificationPathIdPair(eSoundLikeMain, SOUND_DIR "/like_main.wav"));
        SoundPathMap.insert(NotificationPathIdPair(eSoundPullToRefreshFast, SOUND_DIR "/comment.wav"));
    }
    NotificationPathIdMap::iterator it = SoundPathMap.find(soundId);
    CHECK_RET(it != SoundPathMap.end(), nullptr);
    return it->second.c_str();
}
