#include "ErrorHandling.h"
#include "Log.h"
#include "TrayNotificationsManager.h"

Ecore_Timer * TrayNotificationsManager::mPlayNotiTimer = nullptr;
const char * TrayNotificationsManager::FacebookName = "Facebook";


TrayNotificationsManager::TrayNotificationsManager() {
}

TrayNotificationsManager::~TrayNotificationsManager(){
    if (mPlayNotiTimer) {
        ecore_timer_del(mPlayNotiTimer);
    }
}

TrayNotificationsManager* TrayNotificationsManager::GetInstance() {
    static TrayNotificationsManager* trayNotificationsManager = new TrayNotificationsManager();
    return trayNotificationsManager;
}

notification_h TrayNotificationsManager::CreateOngoingNotification()
{
    notification_h ongoing_notification = notification_create(NOTIFICATION_TYPE_ONGOING);
    assert(ongoing_notification);
    CHECK_RET(ongoing_notification, NULL);
    CreateTickerUtilityNotification();
    notification_set_display_applist(ongoing_notification, NOTIFICATION_DISPLAY_APP_NOTIFICATION_TRAY);
    notification_set_text(ongoing_notification, NOTIFICATION_TEXT_TYPE_TITLE, i18n_get_text("IDS_PROFILE_ME_UPLOAD_PROFILE_PICTURE"), nullptr, NOTIFICATION_VARIABLE_TYPE_NONE);
    notification_set_progress(ongoing_notification, 0);
    notification_post(ongoing_notification);
    return ongoing_notification;
}

void TrayNotificationsManager::CreateTickerUtilityNotification()
{
    notification_h ticker_utility_notification = notification_create(NOTIFICATION_TYPE_NOTI);
    assert(ticker_utility_notification);
    CHECK_RET_NRV(ticker_utility_notification);
    notification_set_display_applist(ticker_utility_notification, NOTIFICATION_DISPLAY_APP_TICKER);
    notification_set_text(ticker_utility_notification, NOTIFICATION_TEXT_TYPE_TITLE, i18n_get_text("IDS_PROFILE_ME_UPLOAD_PROFILE_PICTURE"), nullptr, NOTIFICATION_VARIABLE_TYPE_NONE);
    notification_post(ticker_utility_notification);
    notification_delete(ticker_utility_notification);
    notification_free(ticker_utility_notification);
}

void TrayNotificationsManager::DeleteOngoingNotification(notification_h &ongoing_notification) {
    CHECK_RET_NRV(ongoing_notification);
    notification_delete(ongoing_notification);
    notification_free(ongoing_notification);
    ongoing_notification = nullptr;
}

void TrayNotificationsManager::UpdateOngoingNotification(notification_h &ongoing_notification, double progress) {
    if(ongoing_notification) {
        notification_set_progress (ongoing_notification, progress);
        notification_update(ongoing_notification);
    }
}

void TrayNotificationsManager::CreateNotiNotification(const char *text, void *app_control) {
    Log::debug(LOG_FACEBOOK_NOTIFICATION, "TrayNotificationsManager::CreateNotiNotification:notif msg = %s", text);
    notification_h noti_notification;
    noti_notification = notification_create(NOTIFICATION_TYPE_NOTI);
    assert(noti_notification);
    CHECK_RET_NRV(noti_notification);
    notification_set_text(noti_notification, NOTIFICATION_TEXT_TYPE_TITLE, FacebookName, nullptr, NOTIFICATION_VARIABLE_TYPE_NONE);
    notification_set_text(noti_notification, NOTIFICATION_TEXT_TYPE_CONTENT, text, nullptr, NOTIFICATION_VARIABLE_TYPE_NONE);

    if (!mPlayNotiTimer) {
        notification_set_sound(noti_notification, NOTIFICATION_SOUND_TYPE_DEFAULT, nullptr);
        mPlayNotiTimer = ecore_timer_add(1.0, on_playnoti_timeout, nullptr);
    } else {
        notification_set_sound(noti_notification, NOTIFICATION_SOUND_TYPE_NONE, nullptr);
        ecore_timer_reset(mPlayNotiTimer);
    }
    notification_set_display_applist(noti_notification, NOTIFICATION_DISPLAY_APP_NOTIFICATION_TRAY | NOTIFICATION_DISPLAY_APP_TICKER);

    if(app_control) {
        notification_set_launch_option(noti_notification, NOTIFICATION_LAUNCH_OPTION_APP_CONTROL, (void *) app_control);
    }

    notification_post(noti_notification);
}

Eina_Bool TrayNotificationsManager::on_playnoti_timeout(void*)
{
    mPlayNotiTimer = nullptr;
    return ECORE_CALLBACK_CANCEL;  //returning this shall auto-delete the timer
}

void TrayNotificationsManager::DeleteAllNotiNotifications() {
    notification_delete_all(NOTIFICATION_TYPE_NOTI);
}

void TrayNotificationsManager::DeleteAllOngoingNotifications()
{
    notification_delete_all(NOTIFICATION_TYPE_ONGOING);
}
