/*
 * Mutex.cpp
 *
 *  Created on: Jul 4, 2016
 *      Author: ruebasargi
 */

#include "Mutex.h"
#include <eina_lock.h>

Mutex::Mutex(): mMutex(new Eina_Lock()) {
    eina_lock_new(mMutex.get());
}

Mutex::~Mutex() {
    eina_lock_free(mMutex.get());
}

void Mutex::take() {
    eina_lock_take(mMutex.get());
}

void Mutex::release() {
    eina_lock_release(mMutex.get());
}
