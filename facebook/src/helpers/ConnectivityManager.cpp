#include "Common.h"
#include "Config.h"
#include "dlog.h"
#include "ConnectivityManager.h"
#include "Utils.h"


ConnectivityManager::ConnectivityManager():
    mConnectionHandle(NULL),
    mSystemConnectivity(CONNECTION_TYPE_DISCONNECTED),
    mClassName("ConnectivityManager"),
    mInitialized(false),
    mIsWifiConnected(false),
    mIsCellularConnected(false),
    mIsBTConnected(false) {
}

ConnectivityManager::~ConnectivityManager() {
    connection_unset_type_changed_cb(mConnectionHandle);
    connection_destroy(mConnectionHandle);
}

void ConnectivityManager::Init() {
    if (mInitialized)
        return;
    if (CONNECTION_ERROR_NONE != connection_create(&mConnectionHandle)) {
        Log::debug("%s: Failed to Initialize", mClassName.c_str());
        return;
    }
    connection_set_type_changed_cb(mConnectionHandle, ConnectionChangedCb, this);
    UpdateConnectivity();
    mInitialized = true;
}

void ConnectivityManager::UpdateConnectivity() {
    connection_get_type(mConnectionHandle, &mSystemConnectivity);
    switch (mSystemConnectivity) {
        case CONNECTION_TYPE_WIFI:
            UpdateWifiConnectivity();
            break;
        case CONNECTION_TYPE_CELLULAR:
            UpdateCellularConnectivity();
            break;
        case CONNECTION_TYPE_ETHERNET:
            mIsCellularConnected = true;
            break;
        case CONNECTION_TYPE_BT:
            UpdateBTConnectivity();
            break;
        case CONNECTION_TYPE_DISCONNECTED:
        default:
            if (mIsWifiConnected) {
                UpdateWifiConnectivity();
            }
            if (mIsCellularConnected) {
                UpdateCellularConnectivity();
            }
            if (mIsBTConnected){
                UpdateBTConnectivity();
            }
            break;
    }
    Log::debug("%s::UpdateConnectivity Network Connection is %s" ,
            mClassName.c_str(), IsConnected() ? "Available" : "Disconnected");
}

void ConnectivityManager::UpdateWifiConnectivity() {
    connection_wifi_state_e WifiConnectivity;
    connection_get_wifi_state(mConnectionHandle, &WifiConnectivity);
    switch (WifiConnectivity) {
        case CONNECTION_WIFI_STATE_CONNECTED:
            mIsWifiConnected = true;
            break;
        case CONNECTION_WIFI_STATE_DEACTIVATED:
        case CONNECTION_WIFI_STATE_DISCONNECTED:
        default:
            mIsWifiConnected = false;
            break;
    }
    Log::debug("%s::UpdateWifiConnectivity Wifi is %s",
            mClassName.c_str(), mIsWifiConnected ? "Connected" : "Disconnected");
}

void ConnectivityManager::UpdateCellularConnectivity() {
    connection_cellular_state_e CellularConnectivity;
    connection_get_cellular_state(mConnectionHandle, &CellularConnectivity);
    switch (CellularConnectivity) {
        case CONNECTION_CELLULAR_STATE_CONNECTED:
            mIsCellularConnected = true;
            break;
        case CONNECTION_CELLULAR_STATE_AVAILABLE:
        case CONNECTION_CELLULAR_STATE_FLIGHT_MODE:
        case CONNECTION_CELLULAR_STATE_ROAMING_OFF:
        case CONNECTION_CELLULAR_STATE_OUT_OF_SERVICE:
        case CONNECTION_CELLULAR_STATE_CALL_ONLY_AVAILABLE:
        default:
            mIsCellularConnected = false;
            break;
    }
    Log::debug("%s::UpdateCellularConnectivity Cellular is %s",
            mClassName.c_str(), mIsCellularConnected ? "Connected" : "Disconnected" );
}

void ConnectivityManager::UpdateBTConnectivity() {
    connection_bt_state_e BTConnectivity;
    connection_get_bt_state(mConnectionHandle, &BTConnectivity);
    switch (BTConnectivity) {
        case CONNECTION_BT_STATE_CONNECTED:
            mIsBTConnected = true;
            break;
        case CONNECTION_BT_STATE_DEACTIVATED:
        case CONNECTION_BT_STATE_DISCONNECTED:
        default:
            mIsBTConnected = false;
            break;
    }
    Log::debug("%s::UpdateBTConnectivity BT is %s", mClassName.c_str(),
            mIsBTConnected ? "Connected" : "Disconnected");
}

bool ConnectivityManager::IsConnected() {
    return mIsWifiConnected || mIsCellularConnected || mIsBTConnected;
}

void ConnectivityManager::ConnectionChangedCb(connection_type_e ConnectionType, void *Data) {
    if (!Data)
        return;
    ConnectivityManager *Self = (ConnectivityManager *)Data;
    Self->UpdateConnectivity();
    AppEvents::Get().Notify(eINTERNET_CONNECTION_CHANGED);
}
