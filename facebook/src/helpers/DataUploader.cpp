/*
 * DataUploader.cpp
 *
 *  Created on: September 30, 2015
 *      Author: Dmitry.Shcherbakov@SymphonyTeleca.com
 */

#include "DataUploader.h"

#include <dlog.h>
#include <assert.h>
#include <curl/curl.h>

#include "AbstractDataProvider.h"
#include "RequestCompleteEvent.h"
#include "Common.h"

DataUploader::DataUploader( AbstractDataProvider *provider, uploading_done_cb done_cb, void *parent )
{
    Log::debug( "DataUploader::DataUploader" );

    m_Provider = provider;
    m_Parent = parent;
    m_UploadingDone_cb = done_cb;
    m_ErrorHandler = NULL;
    m_RequestedPage = 0;
    m_IsUploadCompleted = false;

    m_ItemsCount = m_Provider->GetDataCount();
    m_DataProviderItemsLimit = m_Provider->GetLimit();
    //
    // @note when the limit is equal to zero, the request will be send without
    // a limit attribute. As a result, the response will contain as many items
    // as FB server can give in one response. But, in any case, we have to revert
    // the old limit value, when the uploading will finish.
    m_Provider->SetLimit( 0 );
}

DataUploader::~DataUploader()
{
    m_UploadingDone_cb = NULL;
    m_Provider->Unsubscribe( this );
    m_Provider->CleanRequest();
    m_Provider->SetLimit( m_DataProviderItemsLimit );
}

void DataUploader::StartUploading()
{
    Log::debug( "DataUploader: starting uploading data" );
    UpdateErase();
    m_Provider->Subscribe( this );
    RequestData();
}

void DataUploader::RequestData()
{
    Log::debug( "DataUploader: requesting page number [ %d ], providerType = %d", m_RequestedPage, m_Provider->GetProviderType() );
    UPLOAD_DATA_RESULT result = m_Provider->UploadData( true );

    switch ( result )
    {
        case NONE_DATA_COULD_BE_RESULT:
        {
            Log::debug( "DataUploader: NONE_DATA_COULD_BE_RESULT" );
            Uploaded();
            break;
        }
        case TOO_EARLY_TO_REQUEST_RESULT:
        {
            //
            // actually it must not happen, but add extra check for it
            Log::debug( "DataUploader: TOO_EARLY_TO_REQUEST_RESULT" );
            m_Provider->ResetRequestTimer();
            RequestData();
            break;
        }
        case ONLY_REQUEST_SENT_RESULT:
        {
            Log::debug( "DataUploader: ONLY_REQUEST_SENT_RESULT" );
            // waiting for a response
            break;
        }
        case ONLY_CACHED_DATA_RESULT:
        {
            Log::debug( "DataUploader: ONLY_CACHED_DATA_RESULT" );
            Uploaded();
            break;
        }
        default:
            assert( false );
    }
}

void DataUploader::Update( AppEventId eventId, void *data )
{
    if( eventId == eREQUEST_COMPLETED_EVENT ) {
        RequestCompleteEvent *eventData = static_cast<RequestCompleteEvent *>(data);
        if( eventData && eventData->mCurlCode != CURLE_OK ) {
            Log::debug( "DataUploader: update/curl error" );
            if ( m_ErrorHandler ) {
                m_ErrorHandler( m_Parent, eventData->mCurlCode );
            }
            m_Provider->Unsubscribe( this );
            return;
        }
    }
    unsigned int itemsCount = m_Provider->GetDataCount();
    if ( m_ItemsCount == itemsCount ) {
        // no more data could be received by using Data Provider
        m_Provider->Unsubscribe( this );
        m_Provider->SetLimit( m_DataProviderItemsLimit );
        Uploaded();
    } else {
        m_ItemsCount = itemsCount;
        ++m_RequestedPage;
        RequestData();
    }
}

void DataUploader::UpdateErase()
{
    m_ItemsCount = 0;
    m_RequestedPage = 0;
}

void DataUploader::Uploaded()
{
    Log::debug( "DataUploader: uploading has done" );
    m_IsUploadCompleted = true;
    if (m_UploadingDone_cb) {
        m_UploadingDone_cb( m_Parent );
        m_UploadingDone_cb = nullptr; // to prevent call of the callback more than once.
    }
}
