/*
 * Log.cpp
 *
 *  Created on: Nov 2, 2015
 *      Author: rupandreev
 */

#include "Common.h"
#include "Log.h"

const char *LOG_TAGS[ LOG_LAST ] = {
    "Facebook",
    "Facebook_Event",
    "Facebook_Group",
    "Facebook_Tagging",
    "Facebook_FriendsAction",
    "Facebook_Operation",
    "Facebook_Notification",
    "Facebook_Commenting",
    "CacheManager",
    "ContactSync",
    "Facebook_User",
    "Facebook_DataServiceProtocol",
};

void Log::verbose(const char *msg, ...) {
#ifndef _RELEASE_NO_PRINTS
    va_list list;
    va_start(list, msg);
    write( DLOG_VERBOSE, list, msg );
    va_end(list);
#endif
}
void Log::verbose(LogTags tag, const char *msg, ...) {
#ifndef _RELEASE_NO_PRINTS
    va_list list;
    va_start(list, msg);
    write( DLOG_VERBOSE, list, msg, getTag(tag) );
    va_end(list);
#endif
}

void Log::verbose_tag(const char *tag, const char *msg, ...) {
#ifndef _RELEASE_NO_PRINTS
    va_list list;
    va_start(list, msg);
    write(DLOG_VERBOSE, list, msg, tag);
    va_end(list);
#endif
}

void Log::info(const char *msg, ...)
{
#ifndef _RELEASE_NO_PRINTS
    va_list list;
    va_start(list, msg);
    write( DLOG_INFO, list, msg );
    va_end(list);
#endif
}

void Log::info(LogTags tag, const char *msg, ...)
{
#ifndef _RELEASE_NO_PRINTS
    va_list list;
    va_start(list, msg);
    write( DLOG_INFO, list, msg, getTag(tag) );
    va_end(list);
#endif
}

void Log::info_tag(const char *tag, const char *msg, ...) {
#ifndef _RELEASE_NO_PRINTS
    va_list list;
    va_start(list, msg);
    write(DLOG_INFO, list, msg, tag);
    va_end(list);
#endif
}


void Log::debug(const char *msg, ...)
{
#ifndef _RELEASE_NO_PRINTS
    va_list list;
    va_start(list, msg);
    write( DLOG_DEBUG, list, msg);
    va_end(list);
#endif
}

void Log::debug(LogTags tag, const char *msg, ...)
{
#ifndef _RELEASE_NO_PRINTS
    va_list list;
    va_start(list, msg);
    write( DLOG_DEBUG, list, msg, getTag(tag) );
    va_end(list);
#endif
}

void Log::debug_tag(const char *tag, const char *msg, ...)
{
#ifndef _RELEASE_NO_PRINTS
    va_list list;
    va_start(list, msg);
    write( DLOG_DEBUG, list, msg, tag);
    va_end(list);
#endif
}

void Log::warning(const char *msg, ...)
{
#ifndef _RELEASE_NO_PRINTS
    va_list list;
    va_start(list, msg);
    write( DLOG_WARN, list, msg );
    va_end(list);
#endif
}

void Log::warning(LogTags tag, const char *msg, ...)
{
#ifndef _RELEASE_NO_PRINTS
    va_list list;
    va_start(list, msg);
    write( DLOG_WARN, list, msg, getTag(tag) );
    va_end(list);
#endif
}

void Log::warning_tag(const char *tag, const char *msg, ...)
{
#ifndef _RELEASE_NO_PRINTS
    va_list list;
    va_start(list, msg);
    write( DLOG_WARN, list, msg, tag);
    va_end(list);
#endif
}

void Log::error(const char *msg, ...)
{
#ifndef _RELEASE_NO_PRINTS
    va_list list;
    va_start(list, msg);
    write( DLOG_ERROR, list, msg );
    va_end(list);
#endif
}

void Log::error(LogTags tag, const char *msg, ...)
{
#ifndef _RELEASE_NO_PRINTS
    va_list list;
    va_start(list, msg);
    write( DLOG_ERROR, list, msg, getTag(tag) );
    va_end(list);
#endif
}

void Log::error_tag(const char *tag, const char *msg, ...)
{
#ifndef _RELEASE_NO_PRINTS
    va_list list;
    va_start(list, msg);
    write( DLOG_ERROR, list, msg, tag);
    va_end(list);
#endif
}

void Log::fatal(const char *msg, ...) {
#ifndef _RELEASE_NO_PRINTS
    va_list list;
    va_start(list, msg);
    write( DLOG_FATAL, list, msg );
    va_end(list);
#endif
}

void Log::fatal(LogTags tag, const char *msg, ...) {
#ifndef _RELEASE_NO_PRINTS
    va_list list;
    va_start(list, msg);
    write( DLOG_FATAL, list, msg, getTag(tag) );
    va_end(list);
#endif
}
void Log::fatal_tag(const char *tag, const char *msg, ...) {
#ifndef _RELEASE_NO_PRINTS
    va_list list;
    va_start(list, msg);
    write( DLOG_FATAL, list, msg, tag);
    va_end(list);
#endif
}

void Log::write( log_priority prio, va_list list, const char *msg, const char *tag)
{
	dlog_vprint( prio, tag, msg, list );
}

const char *Log::getTag(LogTags tag)
{
    return LOG_TAGS[tag];
}
