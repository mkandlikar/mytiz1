#include "LocationManager.h"

#include "FacebookSession.h"

#include <dlog.h>

#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "LocationManager"

#define KDataExpirationTime 60*10 //10 min


LocationManager::LocationManager() : mState(EStateNone), mReqSendLastLocation(NULL) {
    int ret = location_manager_create(LOCATIONS_METHOD_HYBRID, &mManager);
    dlog_print(DLOG_INFO, LOG_TAG, "location_manager_create():%d", ret);
    if (LOCATIONS_ERROR_NONE == ret) {
        mState = EStateStopped;
        location_manager_set_service_state_changed_cb(mManager, LocationManager::state_changed_cb, this);
    }
}

LocationManager::~LocationManager() {
    dlog_print(DLOG_INFO, LOG_TAG, "LocationManager destruction");
    location_manager_unset_service_state_changed_cb(mManager);
    Stop();
    location_manager_destroy(mManager);
    FacebookSession::GetInstance()->ReleaseGraphRequest(mReqSendLastLocation);
}

void LocationManager::Start() {
    dlog_print(DLOG_INFO, LOG_TAG, "START");
    if (mState == EStateStarted || mState == EStateStarting) {
        SendLastLocation();
        return;
    }
    int ret = location_manager_start(mManager);
    dlog_print(DLOG_INFO, LOG_TAG, "location_manager_start():%d", ret);
    if (ret == LOCATIONS_ERROR_NONE) {
        mState = EStateStarting;
    } else if (ret == LOCATIONS_ERROR_GPS_SETTING_OFF) {
        dlog_print(DLOG_ERROR, LOG_TAG, "SETTING OFF");
        mState = EStateSettingOff;
    }
}

void LocationManager::Stop() {
    dlog_print(DLOG_INFO, LOG_TAG, "STOP");
    if (mState == EStateStopped || mState == EStateStopping) {
        return;
    }
    if (location_manager_stop(mManager) == LOCATIONS_ERROR_NONE) {
        mState = EStateStopping;
    }
}

void LocationManager::state_changed_cb(location_service_state_e state, void *user_data) {
    LocationManager* me = static_cast<LocationManager *> (user_data);

    dlog_print(DLOG_INFO, LOG_TAG, "STATE: %d", state);

    if (state == LOCATIONS_SERVICE_ENABLED) {
        me->mState = EStateStarted;
        me->SendLastLocation();
    } else {
        me->mState = EStateSettingOff;
    }
}

LocationManager::LocationManagerError LocationManager::GetLocation(double *aLatitude, double *aLongitude) {
    double altitude, latitude, longitude, climb, direction, speed, horizontal, vertical;
    location_accuracy_level_e level;
    time_t timestamp;

    LocationManagerError ret = EErrorNone;
    int res = LOCATIONS_ERROR_NONE;
    *aLatitude = 0.0f;
    *aLongitude = 0.0f;

    switch (mState) {
    case EStateNone:
        ret = EErrorNotReady;
        break;
    case EStateSettingOff:
        ret = EErrorSettingOff;
        break;
    case EStateStopped:
    case EStateStarting:
    case EStateStopping:
        res = location_manager_get_last_location(mManager, &altitude, &latitude, &longitude,
                  &climb, &direction, &speed, &level, &horizontal, &vertical, &timestamp);
        dlog_print(DLOG_INFO, LOG_TAG, "location_manager_get_last_location():%d", ret);
        break;
    case EStateStarted:
        res = location_manager_get_location(mManager, &altitude, &latitude, &longitude,
                  &climb, &direction, &speed, &level, &horizontal, &vertical, &timestamp);
        dlog_print(DLOG_INFO, LOG_TAG, "location_manager_get_location():%d", ret);
        break;
    }
    if (ret == EErrorNone && res == LOCATIONS_ERROR_NONE) {
        time_t currentTime;
        time(&currentTime);
        dlog_print(DLOG_INFO, LOG_TAG, "LAST TIME:%d", currentTime - timestamp);
        if (KDataExpirationTime > currentTime - timestamp) {
            *aLatitude = latitude;
            *aLongitude = longitude;
        } else {
            ret = EErrorNoDataAvailable;
        }
    }

    return ret;
}

void LocationManager::SendLastLocation() {
    double altitude, latitude, longitude, climb, direction, speed, horizontal, vertical;
    location_accuracy_level_e level;
    time_t timestamp;

    int res = location_manager_get_last_location(mManager, &altitude, &latitude, &longitude,
              &climb, &direction, &speed, &level, &horizontal, &vertical, &timestamp);
    if (res == LOCATIONS_ERROR_NONE) {
// Send 0 as accuracy value because we don't know how to get it. Probably will send actual value later.
        mReqSendLastLocation = FacebookSession::GetInstance()->PostSetLastLocation(latitude, longitude, timestamp, 0,
                on_set_last_location_completed, this);
    }
}

void LocationManager::on_set_last_location_completed(void* object, char* response, int code) {
    dlog_print(DLOG_INFO, LOG_TAG, "on_set_last_location_completed");

    LocationManager *me = static_cast<LocationManager *> (object);

    if (me->mReqSendLastLocation) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(me->mReqSendLastLocation);
        me->mReqSendLastLocation = NULL;
    }

    if (code == CURLE_OK) {
        if (response) {
            if (!strcmp(response, "null")) {
            //We receive 'null' response on SUCCESS
                dlog_print(DLOG_DEBUG, LOG_TAG, "SUCCESS");
            } else {
                dlog_print(DLOG_ERROR, LOG_TAG, "Response: %s", response);
            }
        } else {
            dlog_print(DLOG_ERROR, LOG_TAG, "response is NULL");
        }
    } else {
        dlog_print(DLOG_ERROR, LOG_TAG, "Error sending http request");
    }
    free(response);
}
