#include "Common.h"
#include "Config.h"
#include "Log.h"

#include <Elementary.h>
#include <stdlib.h>
#include <string.h>
#include "Utils.h"

/** Security requirements:
 *    do not define the App ID and App Secret as string values;
 *    do not assign them to string or integer variables.
 */
#define FB_APP_ID         0x1654035054824520ULL
#define CLIENT_TOKEN_HIGH 0x67591e463496f667ULL
#define CLIENT_TOKEN_LOW  0x9e1bd25f89269ed9ULL
#define PACK_64_BIT(numvalue) pack_64bit((numvalue >> 11*4), ((numvalue >> 5*4) & 0xFFFFFF), (numvalue & 0xFFFFF))

char * Config::pack_64bit(unsigned int num1, unsigned int num2, unsigned int num3) {
    char *packedString = (char*)malloc(16+1);
    Utils::Snprintf_s(packedString, 16+1, "%05x%06x%05x", num1, num2, num3);
    return packedString;
}

const char * Config::GetApplicationId() {
    static char sFbApId[16+1] = "";
    if (*sFbApId == '\0') {
        char *packedString = PACK_64_BIT(FB_APP_ID);
        eina_strlcpy(sFbApId, packedString, 16+1);
        free(packedString);
    }
    return sFbApId;
}

const char * Config::GetClientToken() {
    static char sClientToken[32+1] = "";
    if (*sClientToken == '\0') {
        char *packedString = PACK_64_BIT(CLIENT_TOKEN_HIGH);
        eina_strlcpy(&sClientToken[0], packedString, 32+1);
        free(packedString);
        packedString = PACK_64_BIT(CLIENT_TOKEN_LOW);
        eina_strlcpy(&sClientToken[16], packedString, 16+1);
        free(packedString);
    }
    return sClientToken;
}

Config::Config() {
    storage_foreach_device_supported(storage_cb, this);
}

Config::~Config() {
}

bool Config::storage_cb(int storage_id, storage_type_e type, storage_state_e state, const char *path, void *user_data) {
    Config *me = static_cast<Config *> (user_data);
    if (type == STORAGE_TYPE_INTERNAL) {
        int internal_storage_id = storage_id;
        int error;
        char *storagePath;
        error = storage_get_directory(internal_storage_id, STORAGE_DIRECTORY_IMAGES, &storagePath);

        me->SetFBMediaPath(storagePath);
        free(storagePath);
        return false; // Terminate the iteration
    }
    return true;
}

void Config::SetUserId(const char* userId) { mUserId = userId ? userId : ""; }

const std::string& Config::GetUserId() { return mUserId; }

void Config::SetAccessToken(const char* accessToken) { mAccessToken = accessToken ? accessToken : ""; }
const std::string& Config::GetAccessToken() { return mAccessToken; }

void Config::SetSessionKey(const char* sessionKey) { mSessionKey = sessionKey ? sessionKey : ""; }
const std::string& Config::GetSessionKey() { return mSessionKey; }

void Config::SetUserName(const char* username) { mUserName = username ? username : ""; }
const std::string& Config::GetUserName() { return mUserName; }

void Config::SetSessionSecret(const char* sessionSecret) { mSessionSecret = sessionSecret ? sessionSecret : ""; }
const std::string& Config::GetSessionSecret() { return mSessionSecret; }

void Config::SetFBMediaPath(const char* path) { mFBMediaPath = path ? path : ""; }
const std::string& Config::GetFBMediaPath() { return mFBMediaPath; }

bool Config::IsLogin() { return (mAccessToken != ""); }
