#include "MutualFriendsList.h"

#include <string.h>

/*
 * Class MutualFriendsList - a singleton, collects mutual friends count values for different Facebook users.
 */

MutualFriendsList::MutualFriendsList()
{
    mUsersMutualFriendsList = nullptr;
}

MutualFriendsList * MutualFriendsList::GetInstance()
{
    static MutualFriendsList *instanceMutualFriendsList = new MutualFriendsList();
    return instanceMutualFriendsList;
}

MutualFriendsList::~MutualFriendsList()
{
    DeleteAllUsersMutualFriends();
}



void MutualFriendsList::AddUsersMutualFriends(const char *userId, int mutualFriendsCount)
{
    if (userId && *userId && mutualFriendsCount) {
        Eina_List *list;
        void *data;
        EINA_LIST_FOREACH(mUsersMutualFriendsList, list, data) {
            UsersMutualFriends *item = static_cast<UsersMutualFriends*>(data);
            if (item && !strcmp(userId, item->GetUserId())) {
                item->SetMutualFriendsCount(mutualFriendsCount);
                return;
            }
        }

        UsersMutualFriends *item = new UsersMutualFriends(userId, mutualFriendsCount);
        mUsersMutualFriendsList = eina_list_append(mUsersMutualFriendsList, item);
    }
}

int MutualFriendsList::GetUsersMutualFriends(const char *userId)
{
    if (userId && *userId) {
        Eina_List *list;
        void *data;
        EINA_LIST_FOREACH(mUsersMutualFriendsList, list, data) {
            UsersMutualFriends *item = static_cast<UsersMutualFriends*>(data);
            if (item && !strcmp(userId, item->GetUserId())) {
                return item->GetMutualFriendsCount();
            }
        }
    }
    return 0;
}

void MutualFriendsList::DeleteUsersMutualFriends(const char *userId)
{
    if (mUsersMutualFriendsList && userId && *userId) {
        Eina_List *list;
        void *data;
        EINA_LIST_FOREACH(mUsersMutualFriendsList, list, data) {
            UsersMutualFriends *item = static_cast<UsersMutualFriends*>(data);
            if (item && !strcmp(userId, item->GetUserId())) {
                delete item;
                mUsersMutualFriendsList = eina_list_remove(mUsersMutualFriendsList, data);
                return;
            }
        }
    }
}

void MutualFriendsList::DeleteAllUsersMutualFriends()
{
    Eina_List *list;
    void *data;
    EINA_LIST_FOREACH(mUsersMutualFriendsList, list, data) {
        delete static_cast<UsersMutualFriends*>(data);
    }
    mUsersMutualFriendsList = eina_list_free(mUsersMutualFriendsList);
}


/*
 * Class MutualFriendsList::UsersMutualFriends stores a user ID and a mutual friends count value.
 */

MutualFriendsList::UsersMutualFriends::UsersMutualFriends(const char *userId, int mutualFriendsCount)
{
    mUserId = strdup(userId);
    mMutualFriendsCount = mutualFriendsCount;
}

MutualFriendsList::UsersMutualFriends::~UsersMutualFriends(){
    free(mUserId);
}

