#include "AppEvents.h"
#include "ErrorHandling.h"
#include "Log.h"

#include <cassert>
#include <list>
#include <memory>
#include <typeinfo>

Subscriber::~Subscriber() {
    AppEvents::Get().UnsubscribeFromAll(this);
}

AppEvents::AppEvents(): mSendNotifications(true) {
}

AppEvents::~AppEvents() {
#ifdef _DEBUG
    if(mSubscriptions.size() != 0) {
        Log::error("Following %d subscribers haven't unsubscribed:", mSubscriptions.size());
        for(SubscriptionMapping::iterator it = mSubscriptions.begin();
            it != mSubscriptions.end();
            it++) {
            Log::error(typeid(*(it->second)).name());
        }
        assert(false);
    }
#endif
}

void AppEvents::StopSendingNotifications() {
    mSendNotifications = false;
}

AppEvents& AppEvents::Get() {
    static std::unique_ptr<AppEvents, std::function<void(AppEvents*)>> appEvents(new AppEvents(), std::function<void(AppEvents*)>(AppEvents::DeleteAppEvents));
    return *appEvents.get();
}

void AppEvents::Subscribe(AppEventId eventId, Subscriber* subscriber) {
    CHECK_RET_NRV(subscriber);
#ifdef _DEBUG
    if(eventId <= eEVENT_FIRST || eventId >= eEVENT_LAST) {
        Log::error("You are trying to subscribe to some unknown event id=%d, please check available events in enum AppEventId", eventId);
        assert(false);
    }
#endif
    Subscriptions allSubscriptionsToEvent = mSubscriptions.equal_range(eventId);
    for(SubscriptionMapping::iterator it = allSubscriptionsToEvent.first;
        it != allSubscriptionsToEvent.second;
        it++) {
        if(it->second == subscriber) {
            // a duplicate subscription shall cause duplicate calls of Update(), hence avoid it:
            Log::error("Subscriber `%s` is trying to subscribe on the same event id=%d twice", typeid(*subscriber).name(), eventId);
            return;
        }
    }
    mSubscriptions.insert(Subscription(eventId, subscriber));
}

void AppEvents::Unsubscribe(AppEventId eventId, Subscriber* subscriber) {
    CHECK_RET_NRV(subscriber);
#ifdef _DEBUG
    if(eventId <= eEVENT_FIRST || eventId >= eEVENT_LAST) {
        Log::error("You are trying to unsubscribe from some unknown event id=%d, please check available events in enum AppEventId", eventId);
        assert(false);
    }
#endif
    Subscriptions allSubscriptionsToEvent = mSubscriptions.equal_range(eventId);
    for(SubscriptionMapping::iterator it = allSubscriptionsToEvent.first;
        it != allSubscriptionsToEvent.second;
        it++) {
        if(it->second == subscriber) {
            mSubscriptions.erase(it);
            return;
        }
    }
}

void AppEvents::UnsubscribeFromAll(Subscriber* subscriber) {
    std::list<AppEventId> events;
    for(auto it = mSubscriptions.begin(); it != mSubscriptions.end(); it++) {
        if(it->second == subscriber) {
            events.push_back(it->first);
        }
    }
    for(auto it = events.begin(); it != events.end(); it++) {
        Unsubscribe(*it, subscriber);
    }
}

void AppEvents::Notify(AppEventId eventId, void* data) {
#ifdef _DEBUG
    if(eventId <= eEVENT_FIRST || eventId >= eEVENT_LAST) {
        Log::error("You are trying to send notification about some unknown event id, please check available events in enum AppEventId");
        assert(false);
    }
#endif
    if(!mSendNotifications) return;
    Subscriptions allSubscriptionsToEvent = mSubscriptions.equal_range(eventId);
    for(SubscriptionMapping::iterator it = allSubscriptionsToEvent.first;
        it != allSubscriptionsToEvent.second;
        it++) {
        it->second->Update(eventId, data);
    }
}

void AppEvents::DeleteAppEvents(AppEvents* appEvents) {
    delete appEvents;
}
