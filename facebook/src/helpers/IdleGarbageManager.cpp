/*
 * IdleGarbageManager.cpp
 *
 * Created on: Oct 9, 2015
 * Author: Dmitry.Shcherbakov@SymphonyTeleca.com
 */

#include "DoubleSidedStack.h"
#include <dlog.h>
#include <eina_types.h>
#include "IdleGarbageManager.h"
#include "Log.h"

#include "Common.h"
#include "ActionBase.h"

IdleGarbageManager::IdleGarbageManager()
{
    m_ItemsStack = new Utils::DoubleSidedStack();
    m_IsIdleInProgress = false;
}

IdleGarbageManager::~IdleGarbageManager()
{
	while ( m_ItemsStack->GetSize() ) {
		Item *item = static_cast<Item*>(m_ItemsStack->PopFromBegin());
		delete item;
	}
    delete m_ItemsStack;
}

IdleGarbageManager* IdleGarbageManager::GetInstance()
{
    static IdleGarbageManager garbageManager;
    return &garbageManager;
}

void IdleGarbageManager::AppendItemsStack( Utils::DoubleSidedStack *itemsStack )
{
    bool isLunchIdle = itemsStack->GetSize() > 0;
    while( itemsStack->GetSize() ) {
        void *item = itemsStack->PopFromBegin();
        m_ItemsStack->PushToEnd( item );
    }

    if ( isLunchIdle && !m_IsIdleInProgress ) {
        ecore_idle_enterer_add( idle_cb, m_ItemsStack );
    }

}

Eina_Bool IdleGarbageManager::idle_cb( void *data )
{
    Utils::DoubleSidedStack *itemsStack = static_cast<Utils::DoubleSidedStack*>(data);
    int numberToDelete = 4;
    if ( itemsStack->GetSize() && numberToDelete > 0 ) {
        Item *item = static_cast<Item*>(itemsStack->PopFromBegin());
        switch ( item->type )
        {
            case IdleGarbageManager::ACTION_AND_DATA_ITEM:
            {
                ActionAndData *actionAndData = static_cast<ActionAndData*>(item->data);
                evas_object_del( actionAndData->mParentWidget );
                delete actionAndData;
            } break;
            case IdleGarbageManager::EVAS_ITEM:
            {
                Evas_Object *evasItem = static_cast<Evas_Object*>(item->data);
                evas_object_del( evasItem );
            } break;
            default:
            {
                Log::error( "IdleGarbageManager::idle_cb, Undetermined item type to delete." );
            }
        }

        --numberToDelete;
        delete item;
    }
    return itemsStack->GetSize() > 0 ? ECORE_CALLBACK_RENEW : ECORE_CALLBACK_CANCEL;
}

unsigned int IdleGarbageManager::WaitingForDelete() const
{
    return m_ItemsStack->GetSize();
}
