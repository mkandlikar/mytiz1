#include "Application.h"
#include "ConnectivityManager.h"
#include "ContactSynchronizer.h"
#include "ContactsException.h"
#include "DataUploader.h"
#include "ErrorHandling.h"
#include "FacebookContactsWriter.h"
#include "Log.h"
#include "MutexLocker.h"
#include "OwnFriendsProvider.h"

#include <app_preference.h>
#include <cassert>
#include <Ecore.h>

#define KEY_PREF_ADDRESS_BOOK_CLEARED "key_pref_address_book_cleared"

const static char AddressBookName[] = "FacebookAddress";
const static double RegularSynchronizationPeriod = 86400;

Ecore_Thread* ContactSynchronizer::mContactWritingThread = nullptr;
std::set<Ecore_Thread*> ContactSynchronizer::mContactUpdateThreads;
#ifdef _DEBUG
    unsigned int ContactSynchronizer::mInstanceCounter = 0;
#endif

struct ContactSynchronizer::ContactImageUpdateData
{
    ContactImageUpdateData(ContactSynchronizer* synchronizer, ErrorCode status, MessageId requestId, const char* imagePath):
        mSynchronizerInstace(synchronizer), mImageDownloadStatus(status), mImageDownloadRequestId(requestId), mDownloadedImagePath(imagePath ? imagePath : "") {}
    ContactSynchronizer* mSynchronizerInstace;
    ErrorCode mImageDownloadStatus;
    MessageId mImageDownloadRequestId;
    std::string mDownloadedImagePath;
};

ContactSynchronizer::ContactSynchronizer() :
        mContactsDataDownloader(nullptr),
        mAccoundId(0),
        mRegularSynchronizationTimer(nullptr),
        mIsInProcessOfSynchronization(false),
        mPerformSynchronizationOnConnection(false)
{
#ifdef _DEBUG //Self diagnostics, ContactSynchronizer is not designed to be used with several instances
    if (mInstanceCounter > 0) {
        Log::error(LOG_CONTACT_SYNC, "Attention! ContactSynchronizer::ContactSynchronizer(): you're trying to create more than 1 instance of "
                "ContactSynchronizer class at one time, something is wrong with your logic!");
        assert(false);
    }
#endif
    AppEvents::Get().Subscribe(eACCOUNT_CHANGED, this);

#ifdef FEATURE_SYNC_FB_CONTACTS // Remove previously synced contacts once app is created when feature is off
    AppEvents::Get().Subscribe(eDATA_IS_READY, this);
    AppEvents::Get().Subscribe(eINTERNET_CONNECTION_CHANGED, this);
#else
    AppEvents::Get().Subscribe(eACCOUNT_DATA_IS_READY, this);
#endif

#ifdef _DEBUG
    mInstanceCounter++;
#endif
}

ContactSynchronizer::~ContactSynchronizer()
{
    if (mContactWritingThread) {
        ecore_thread_cancel(mContactWritingThread);
        mContactWritingThread = nullptr;
    }
    for (std::set<Ecore_Thread*>::iterator it = mContactUpdateThreads.begin(); it != mContactUpdateThreads.end(); it++) {
        CHECK_CONT(*it);
        ecore_thread_cancel(*it);
    }
    mContactUpdateThreads.clear();
    if (mRegularSynchronizationTimer) {
        ecore_timer_del(mRegularSynchronizationTimer);
    }
#ifdef _DEBUG
    mInstanceCounter--;
#endif
}

void ContactSynchronizer::Update(AppEventId eventId, void* data)
{
    switch (eventId) {
    case eACCOUNT_CHANGED:
        mAccoundId = data ? *(static_cast<int*>(data)) : 0;
        Log::info(LOG_CONTACT_SYNC, "ContactSynchronizer::Update: eACCOUNT CHANGED - %d", mAccoundId);
        break;

    case eDATA_IS_READY:
        Log::info(LOG_CONTACT_SYNC, "ContactSynchronizer::Update: eDATA_IS_READY");
        on_contact_data_downloading_finished(this);
        if (mRegularSynchronizationTimer) {
            ecore_timer_del(mRegularSynchronizationTimer);
        }
        mRegularSynchronizationTimer = ecore_timer_add(RegularSynchronizationPeriod, ContactSynchronizer::start_contact_data_downloading, this);
        break;

    case eINTERNET_CONNECTION_CHANGED:
        if (mIsInProcessOfSynchronization && !ConnectivityManager::Singleton().IsConnected()) {
            mIsInProcessOfSynchronization = false;
            mPerformSynchronizationOnConnection = true;
        }
        break;

    case eACCOUNT_DATA_IS_READY: {
        mAccoundId = data ? *(static_cast<int*>(data)) : 0;
        Log::info(LOG_CONTACT_SYNC, "ContactSynchronizer::Update: eACCOUNT_DATA_IS_READY - %d", mAccoundId);

        bool isAddressBookCleared = false;
        int res = preference_get_boolean(KEY_PREF_ADDRESS_BOOK_CLEARED, &isAddressBookCleared);

        if ((PREFERENCE_ERROR_NONE == res && !isAddressBookCleared)
                || PREFERENCE_ERROR_NO_KEY == res) {
            if (mContactWritingThread) {
                ecore_thread_cancel(mContactWritingThread);
            }
            mContactWritingThread = ecore_thread_run(delete_contacts_async,
                    on_delete_contact_async_finished,
                    on_delete_contact_async_finished,
                    this);
        }
    }
        break;

    default:
#ifdef _DEBUG
        Log::error(LOG_CONTACT_SYNC, "ContactSynchronizer received unknown event, please check subscription or add handler for new event");
        assert(false);
#endif
        break;
    }
}

void ContactSynchronizer::HandleDownloadImageResponse(ErrorCode error, MessageId requestID, const char* url, const char* fileName) {
    CHECK_RET_NRV(fileName);
    Ecore_Thread* newThread = ecore_thread_run(ContactSynchronizer::update_contact_with_image_async,
                                               ContactSynchronizer::on_update_contact_with_image_async_finished,
                                               ContactSynchronizer::on_update_contact_with_image_async_finished,
                                               new ContactImageUpdateData(this, error, requestID, fileName));
    CHECK_RET_NRV(newThread);
    mContactUpdateThreads.insert(newThread);
}

Eina_Bool ContactSynchronizer::start_contact_data_downloading(void* data)
{
    ContactSynchronizer *me = static_cast<ContactSynchronizer*>(data);
    me->mContactsDataDownloader.reset(new DataUploader(OwnFriendsProvider::GetInstance(), &ContactSynchronizer::on_contact_data_downloading_finished, me));
    return ECORE_CALLBACK_DONE;
}

void ContactSynchronizer::on_contact_data_downloading_finished(void* data)
{
    if (mContactWritingThread)
        ecore_thread_cancel(mContactWritingThread);
    mContactWritingThread = ecore_thread_run(ContactSynchronizer::write_contacts_async, ContactSynchronizer::on_write_contact_async_finished, ContactSynchronizer::on_write_contact_async_finished, data);
}

void ContactSynchronizer::delete_contacts_async(void* data, Ecore_Thread* thread) {
    CHECK_RET_NRV(thread);

    if (ecore_thread_check(thread)) {
        return;
    }

    ContactSynchronizer* me = static_cast<ContactSynchronizer*>(data);
    CHECK_RET_NRV(me);

    if (ecore_thread_check(thread) || me->mAccoundId <= 0) {
        return;
    }

    std::unique_ptr<Contacts::FacebookContactsWriter> fbContactWriter(nullptr);
    try {
        fbContactWriter = std::unique_ptr<Contacts::FacebookContactsWriter>(new Contacts::FacebookContactsWriter(me->mAccoundId, AddressBookName));
        CHECK_RET_NRV(fbContactWriter.get());
        fbContactWriter->clearTheAddressBook();
    } catch(Contacts::DataBaseException& e) {
        Log::error(LOG_CONTACT_SYNC, e.what());
    }
}

void ContactSynchronizer::write_contacts_async(void* data, Ecore_Thread* thread)
{
    CHECK_RET_NRV(thread);
    if(ecore_thread_check(thread)) return;
    ContactSynchronizer* me = static_cast<ContactSynchronizer*>(data);
    CHECK_RET_NRV(me);

    if(ecore_thread_check(thread)) return;
    std::list<Friend*> contacts(std::move(OwnFriendsProvider::GetInstance()->GetDataCopy()));

    if(ecore_thread_check(thread)) return;
    if(me->mAccoundId <= 0) return;

    if(ecore_thread_check(thread)) return;
    std::unique_ptr<Contacts::FacebookContactsWriter> fbContactWriter(nullptr);
    try {
        fbContactWriter = std::unique_ptr<Contacts::FacebookContactsWriter>(new Contacts::FacebookContactsWriter(me->mAccoundId, AddressBookName));
        CHECK_RET_NRV(fbContactWriter.get());
        fbContactWriter->clearTheAddressBook();
    } catch(Contacts::DataBaseException& e) {
        Log::error(LOG_CONTACT_SYNC, e.what());
    }

    if(ecore_thread_check(thread)) return;
    CHECK_RET_NRV(Application::GetInstance());
    CHECK_RET_NRV(Application::GetInstance()->mDataService);
    CHECK_RET_NRV(fbContactWriter.get());

    if(ecore_thread_check(thread)) return;
    MutexLocker lock(&me->mMutex);
    for(auto item = contacts.begin(); item != contacts.end(); item++) {
        if(ecore_thread_check(thread)) return;
        Friend* contact = *item;
        CHECK_CONT(contact);
        int contactId = 0;
        try {
            contactId = fbContactWriter->writeFacebookContact(*contact);
        } catch(Contacts::DataBaseException& e) {
            Log::error(LOG_CONTACT_SYNC, e.what());
        }
        if(ecore_thread_check(thread)) return;
        Application::GetInstance()->mDataService->DownloadImage(me, contact->mPicturePath.c_str(), contact->mDownloadImageReqId);
        if(ecore_thread_check(thread)) return;
        if(contactId != 0) {
            std::pair<MessageId, std::pair<int, std::string> > mapEntry(contact->mDownloadImageReqId, std::pair<int, std::string>(contactId, contact->GetId() ? contact->GetId() : ""));
            if(ecore_thread_check(thread)) return;
            me->mImageRequestIdToContactId.insert(mapEntry);
        }
        contact->Release();
    }
}

void ContactSynchronizer::on_write_contact_async_finished(void* data, Ecore_Thread* thread) {
    if (mContactWritingThread == thread) {
        mContactWritingThread = nullptr;
        preference_set_boolean(KEY_PREF_ADDRESS_BOOK_CLEARED, false);
    }
}

void ContactSynchronizer::on_delete_contact_async_finished(void* data, Ecore_Thread* thread) {
    if (mContactWritingThread == thread) {
        mContactWritingThread = nullptr;
        preference_set_boolean(KEY_PREF_ADDRESS_BOOK_CLEARED, true);
    }
}

void ContactSynchronizer::update_contact_with_image_async(void* data, Ecore_Thread* thread)
{
    CHECK_RET_NRV(thread);
    if (ecore_thread_check(thread))
        return;
    ContactImageUpdateData* contactUpdateData = static_cast<ContactImageUpdateData*>(data);
    CHECK_RET_NRV(contactUpdateData);
    ContactSynchronizer* me = contactUpdateData->mSynchronizerInstace;
    CHECK_RET_NRV(me);

    if (ecore_thread_check(thread))
        return;
    MutexLocker lock(&me->mMutex);
    if (ecore_thread_check(thread))
        return;
    auto it = me->mImageRequestIdToContactId.find(contactUpdateData->mImageDownloadRequestId);
    if (ecore_thread_check(thread))
        return;
    if (it != me->mImageRequestIdToContactId.end()) {
        if (!contactUpdateData->mDownloadedImagePath.empty() && contactUpdateData->mImageDownloadStatus == EBPErrNone) {
            try {
                Contacts::FacebookContactsWriter writer;
                writer.updateFacebookContactWithImage(it->second.first, contactUpdateData->mDownloadedImagePath, it->second.second);
            } catch (Contacts::DataBaseException& e) {
                Log::error(LOG_CONTACT_SYNC, e.what());
            }
        }
        if (ecore_thread_check(thread))
            return;
        me->mImageRequestIdToContactId.erase(it);
    }
}

void ContactSynchronizer::on_update_contact_with_image_async_finished(void* data, Ecore_Thread* thread)
{
    auto it = mContactUpdateThreads.find(thread);
    if (it != mContactUpdateThreads.end())
        mContactUpdateThreads.erase(it);
    std::unique_ptr<ContactImageUpdateData> contactUpdateData(static_cast<ContactImageUpdateData*>(data));
    CHECK_RET_NRV(contactUpdateData.get());
    ContactSynchronizer* me = contactUpdateData->mSynchronizerInstace;
    CHECK_RET_NRV(me);
    if (me->mImageRequestIdToContactId.size() == 0) {
        me->mIsInProcessOfSynchronization = false;
        me->mPerformSynchronizationOnConnection = false;
    }
}
