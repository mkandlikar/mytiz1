#include "UIRes.h"

UIRes * UIRes::GetInstance()
{
    static UIRes r;
    return &r;
}

void UIRes::Init480()
{
    //Common
    SCREEN_SIZE_HEIGHT = 800;
    SCREEN_SIZE_WIDTH = 480;
    STATUS_BAR_SIZE_H = 27;
    HEADER_SIZE_H = 72;
    MESSAGE_READ_MORE_LIMIT = 500;
    MESSAGE_READ_MORE_LINES_LIMIT = 5;
    REPLY_READ_MORE_LIMIT = 22;

    /* AddTextScreen */
    ADD_TEXT_DELETE_AREA = 680;
    ADD_TEXT_BOUNDARY_LEFT = 30;
    ADD_TEXT_MIN_TEXT_HEIGHT = 32;
    ADD_TEXT_MAX_TEXT_HEIGHT = 350;

    /* AddTextInputScreen */
    ADD_TEXT_COLOR_PICKER_SIZE_H = 65;
    ADD_TEXT_COLOR_PICKER_OFFSET_L = 380;
    ADD_TEXT_COLOR_PICKER_RECT_SIZE_W_H = 38;
    ADD_TEXT_COLOR_PICKER_RECT_OFFSET_L = ADD_TEXT_COLOR_PICKER_OFFSET_L + 12;

    /* PostComposerScreen */
    POST_COMP_PRIVACY_TO_TEXT_FONT_SIZE = "34";
//    POST_COMP_PRIVACY_TO_TEXT_FONT_SIZE = "21"; //"21" according to redlines
    POST_COMP_PRIVACY_TO_TEXT_OFFSET_R = 15;
    POST_COMP_PRIVACY_TO_SELECTOR_FONT_SIZE = "27";
//    POST_COMP_PRIVACY_TO_SELECTOR_FONT_SIZE = "21"; //"21" according to redlines
    POST_COMP_PRIVACY_TO_IMG_SIZE = "24x24";
    POST_COMPOSER_TEXT_FONT_SIZE = "28";
//    POST_COMPOSER_TEXT_FONT_SIZE = "21"; //"21" according to redlines
    POST_COMPOSER_CREATE_ALBUM_TEXT_FONT_SIZE = "24";
    POST_COMPOSER_IMAGE_SIZE = 426;
    POST_COMP_PRIVACY_GROUP_ICON_SIZE = 24;

    /* News Feed */
    NF_PH_TITLE_WRAP_WIDTH = 320;
    NF_PH_TITLE_TEXT_SIZE = "28";
    NF_BH_TITLE_WRAP_WIDTH = 388;
    NF_PH_DATE_TEXT_SIZE = "24";
    NF_PH_DATE_TEXT_FORMAT = "<font_size=24 color=#9197a3>%s</>";
    NF_PIPH_TITLE_WRAP_WIDTH = 252;
    NF_PROGRESS_COUNTER_FONT_SIZE = "24";

    NF_PH_INFOBOX_WIDTH = 360;
    NF_PIPH_INFOBOX_WIDTH = 320;

    NF_COMMENT_MSG_WRAP_WIDTH = 344;
    NF_COMMENT_MSG_TEXT_SIZE = "28";

    NF_POST_MSG_TEXT_SIZE = "28";
    NF_POST_MSG_WRAP_WIDTH = 448;

    NF_PIP_MSG_TEXT_SIZE = "21";
    NF_PIP_MSG_WRAP_WIDTH = 416;

    NF_LINK_LORES_TITLE_TEXT_SIZE = "28";
    NF_LINK_LOREST_CAPTION_TEXT_SIZE = "24";
    NF_LINK_LORES_WRAP_WIDTH = 276;

    NF_LOC_INFO_TITLE_TEXT_SIZE = "28";
    NF_LOC_INFO_TYPE_TEXT_SIZE = "24";
    NF_LOC_INFO_WRAP_WIDTH = 271;
    NF_PRIVACY_ICON_SIZE = 20;
    NF_PRIVACY_ICON_WEIGHT_BEFORE = 0.2;
    NF_PRIVACY_ICON_WEIGHT_AFTER = 20;
    NF_LOC_INFO_WIDTH = 300;

    NF_LANDSCAPE_PHOTO_LAYOUT_OFFSET_W_L = 8;
    NF_LANDSCAPE_PHOTO_LAYOUT_OFFSET_W_R = 8;
    NF_LANDSCAPE_PHOTO_LAYOUT_SIZE_W = 480;
    NF_LANDSCAPE_PHOTO_LAYOUT_SIZE_H = 284;
    NF_SQUARE_PHOTO_LAYOUT_OFFSET_W_L = 32;
    NF_SQUARE_PHOTO_LAYOUT_OFFSET_W_R = 32;
    NF_SQUARE_PHOTO_LAYOUT_SIZE_W = 480;

    UNFOLLOW_TEXT_FONT_SIZE = "34";

    //Settings screen
    SETTINGS_LOGGEDIN_PROFILE_NAME = "33";
    SETTINGS_VIEW_YOUR_PROFILE = "28";
    SETTINGS_CATEGORIES_ITEM_FONT_SIZE = "32";
    SETTINGS_TITLES_ITEM_FONT_SIZE = "28";
    SETTINGS_CATEGORIES_LIST_ITEM_WIDTH = 480;
    SETTINGS_CATEGORIES_LIST_ITEM_HEIGHT = 84;
    SETTINGS_TITLES_LIST_ITEM_WIDTH = 480;
    SETTINGS_TITLES_LIST_ITEM_HEIGHT = 78;

    //SearchScreenTabbed
    SST_LIST_ITEM_SIZE_W = 480;
    SST_LIST_ITEM_SIZE_H = 144;
    SST_MAIN_RECT_SIZE_W = 480;
    SST_MAIN_RECT_SIZE_H = 636;

    /* Commenting */
    COMMENT_ENTRY_TEXT_SIZE = "32"; // "24" - redlines
    COMMENT_ACTIONBAR_TEXT_SIZE = "24";
    COMMENT_ACTIONBAR_WITH_TIME = "<font_size=24 color=#9fa2a6>%s</>";
    COMMENT_ACTIONBAR_LIKEICON_SIZE = 25;
    COMMENT_ACTIONBAR_LIKE_COUNT_TEXT_SIZE = "24";
    COMMENT_MSG_TEXT_SIZE = "28";
    COMMENT_MSG_WRAP_WIDTH = 360;
    COMMENT_TOP_TITLE ="28";
    COMMENT_TOP_TOOLBAR_TEXTBOX_W = 344;

    COMMENT_REPLY_ITEM_MSG_WRAP_WIDTH = 280;
    COMMENT_REPLY_ITEM_FONT_SIZE = "22";

    COMMENT_LANDSCAPE_PHOTO_LAYOUT_SIZE_W = 366;
    COMMENT_STICKER_LAYOUT_SIZE_W = 130;

    /* FindFriends */
    FF_SEARCH_BTN_TEXT_SIZE = "24";
    FF_SEARCH_RESULT_WRAP_WIDTH = 164;
    FF_ITEM_FRIEND_TEXT_STYLE = "DEFAULT='font_size=28 wrap=word align=right left_margin=0 right_margin=0 text_class=tizen'";
    FF_ITEM_FRIEND_TEXT_LTR_STYLE = "DEFAULT='font_size=28 wrap=word ellipsis=1.0 align=left left_margin=0 right_margin=0 text_class=tizen'";
    FF_ITEM_FRIEND_TEXT_RTL_STYLE = "DEFAULT='font_size=28 wrap=word ellipsis=1.0 align=right left_margin=0 right_margin=0 text_class=tizen'";
    FF_SEARCH_RESULT_NAME_TEXT_SIZE = "28";
    FF_SEARCH_RESULT_MUTUAL_TEXT_SIZE = "24";
    FF_MAIN_RECT_SIZE_W = 480;
    FF_MAIN_RECT_SIZE_H = 800-173;

    FF_CAROUSEL_SCROLLER_W = 480;
    FF_SUBTEXT_ELLIPSIS = 80;

    /* Main Screen */
    CW_MAINTABS_SIZE_W = 480;
    CW_MAINTABS_SIZE_H = 74;
    MS_MAIN_RECT_SIZE_W = 480;
    MS_MAIN_RECT_SIZE_H = 800-173;

    /* General Elements */
    GE_SEARCH_BAR_N_TAB_BAR_SIZE_H = 173;
    GE_TITLEBAR_FONT_SIZE = "36";

    /* Events */
    EVENT_SELECTOR_TEXT_SIZE = "27";
    EVENT_SELECTOR_ICON_SIZE = 32;
    EVENT_CREATE_TEXT_SIZE = "27";

    EVENT_CAROUSEL_SCROLLER_SIZE = 480;

    EVENT_NEAREST_ITEM_WRAP_WIDTH = 236;
    EVENT_NEAREST_TITLE_TEXT_SIZE = "24";
    EVENT_NEAREST_SUBCONTENT_TEXT_SIZE = "21";

    EVENT_BDAY_ITEM_WRAP_WIDTH = 240;
    EVENT_BDAY_ITEM_NAME_TEXT_SIZE = "24";
    EVENT_BDAY_ITEM_SUBCONTENT_TEXT_SIZE = "21";

    EVENT_CAROUSEL_ITEM_WRAP_WIDTH = 310;
    EVENT_CAROUSEL_ITEM_MAIN_TEXT_SIZE = "24";
    EVENT_CAROUSEL_ITEM_SUBCONTENT_TEXT_SIZE = "21";

    EVENT_GUESTLIST_TABTEXT_SIZE = "21";

    //User profile screen
    PROFILE_NAME_FONT_SIZE = "45";
    PROFILE_POSTBTN_FONT_SIZE = "24";
    PROFILE_POSTBTN_ICON_SIZE = 39;
    FRIENDS_LIST_PLACEHOLDER_TEXT_FONT_SIZE = "21";
    PROFILE_TEXT_STYLE = "DEFAULT='font_size=43 wrap=word left_margin=0 right_margin=0 text_class=tizen'";
    PROFILE_TEXT_LTR_STYLE = "DEFAULT='font_size=43 color=#fff wrap=word ellipsis=1.0 align=left left_margin=0 right_margin=0 text_class=tizen'";
    PROFILE_TEXT_RTL_STYLE = "DEFAULT='font_size=43 color=#fff wrap=word ellipsis=1.0 align=right left_margin=0 right_margin=0 text_class=tizen'";

    PROFILE_NAME_ITEM_WIDTH = 290;
    GROUP_NAME_ITEM_WIDTH = 440;

    //User Profile About
    UPA_LAYOUT_W = 450;
    UPA_LAYOUT_DEFAULT_H = 62;
    UPA_LAYOUT_MULTI_H = 110;
    UPA_OV_WRAP_MAX = 380;
    UPA_OV_MORE_HEADING_WRAP_MAX = 120;
    UPA_OV_MORE_INFO_WRAP_MAX = 220;
    UPA_OV_TEXT_FONT_SIZE = "21";
    UPA_OV_PAST_FONT_SIZE = "18";
    UPA_OV_HEADING_FONT_SIZE = "21";
    UPA_OV_INFO_FONT_SIZE = "18";

    //User Profile About More
    UPAM_WORK_LAYOUT_W = 450;
    UPAM_WORK_LAYOUT_H = 140;
    UPAM_WORK_HEADING_LAYOUT_H = 48;
    UPAM_WORK_ADD_LAYOUT_H = 100;
    UPAM_WORK_DEFAULT_FONT_SIZE = "18";
    UPAM_WORK_ADD_FONT_SIZE = "21";
    UPAM_WORK_HEADING_FONT_SIZE = "17";
    UPAM_WORK_NAME_FONT_SIZE = "21";
    UPAM_WORK_WRAP_TEXT = 240;
    UPAM_WORK_HEADING_WRAP_TEXT = 120;

    //Messenger screen font
    MESSENGER_TITLE_FONT_SIZE = "30";
    MESSENGER_MAININFO_FONT_SIZE = "20";
    MESSENGER_BTN_FONT_SIZE = "20";

    //Notification
    NT_TEXT_SIZE = 21;

    /* Profiles WidgetFactory */
    PROFILES_WF_COVER_ITEM_SUBTITLE_FONT_SIZE = "18";
    PROFILES_WF_INFOFIELD_FIRST_FONT_SIZE = "21";
    PROFILES_WF_INFOFIELD_SECOND_FONT_SIZE = "18";
    PROFILES_WF_ABOUT_WRAP_WIDTH = 408;
    PROFILES_WF_ABOUT_FONT_SIZE = "21";
    PROFILES_WF_INFOFIELD_WRAP_WIDTH = 381;

    /* Popup Widget */
    POPUP_WIDGET_MENU_ITEM_HEIGHT = 84;
    POPUP_WIDGET_MENU_MULTI_ITEM_HEIGHT = 96;
    POPUP_WIDGET_MENU_ITEM_OFFSET = 5;
    POPUP_WIDGET_MENU_ITEM_SCREEN_HEIGHT = 800;

    POPUP_POST_DELETE_ITEM_SCREEN_HEIGHT = 266;
    POPUP_POST_DELETE_ITEM_SCREEN_WIDHT = 480;
    POPUP_POST_DELETE_MOVE_X = 50;
    POPUP_POST_DELETE_MOVE_Y = 50;

    /* Create Event */
    //EVENT_CREATE_NAME_FONT_SIZE = "24"; //"24" according to redlines
    EVENT_CREATE_NAME_FONT_SIZE = "32";
    //EVENT_CREATE_DETAIL_FONT_SIZE = "24"; //"24" according to redlines
    EVENT_CREATE_DETAIL_FONT_SIZE = "32";

    //EVENT_CREATE_ENTRY_PUSH = "DEFAULT='font=TizenSans:style=Medium font_size=24 color=#000'"; //"24" according to redlines
    EVENT_CREATE_ENTRY_PUSH = "DEFAULT='font_size=32 color=#000 left_margin=0 right_margin=0'";

    /* Crop Rectangle Params*/
    CROP_RECT_SIZE = 60;
    CROP_REGION_MIN_WIDTH = 90;
    CROP_REGION_MIN_HEIGHT = 90;
    CM_OFFSET = 13;

    ACTION_BAR_SIZE_H = 72;

    RENAME_ALBUM_TEXT_STYLE = "DEFAULT='font_size=24 color=#000'";

    SEARCH_TABS_SCROLLING_WIDTH = 480;

    EXCEPT_SPECIFY_FRIENDS_TO_TEXT_FORMAT = "<font=TizenSans:style=Regular font_size=27 color=#9298a4>%s</>";
//    EXCEPT_SPECIFY_FRIENDS_TO_TEXT_FORMAT = "<font=TizenSans:style=Regular font_size=21 color=#9298a4>%s</>"; //AAA fontsize 21 is accoding to redlines
    EXCEPT_SPECIFY_FRIENDS_TEXT_FORMAT = "<font=TizenSans:style=Regular font_size=27 color=#ff2929>%s</>"; //AAA ToDo: check text color
//    EXCEPT_SPECIFY_FRIENDS_TEXT_FORMAT = "<font=TizenSans:style=Regular font_size=21 color=#ff2929>%s</>"; //AAA ToDo: check text color, fontsize 21 is accoding to redlines
    EXCEPT_SPECIFY_FRIENDS_SEARCH_TEXT_STYLE = "DEFAULT='font=TizenSans:style=Regular font_size=27 color=#000'";
//    EXCEPT_SPECIFY_FRIENDS_SEARCH_TEXT_STYLE = "DEFAULT='font=TizenSans:style=Regular font_size=21 color=#000'"; //AAA fontsize 21 is accoding to redlines
    POST_SCREEN_TITLE = "<font=TizenSans:style=Medium font_size=32 color=#FFFFFF>%s</font>";

//    SHARE_WITH_TO_TEXT_FORMAT = "<font=TizenSans:style=Regular font_size=21 color=#588fff>%s</>"; //AAA fontsize 21 is accoding to redlines
    SHARE_WITH_TO_TEXT_FORMAT = "<font=TizenSans:style=Regular font_size=27 color=#588fff>%s</>";

    /* Screen base */
    SCREEN_BASE_PROGRESS_BAR_ITEM_OBJ_RESIZE = 50;
    SCREEN_BASE_PROGRESS_BAR_ITEM_OBJ_MOVE_X = 215;
    SCREEN_BASE_PROGRESS_BAR_ITEM_OBJ_MOVE_Y = 400;

    CONNECTION_ERROR_WIDGET_MOVE_X = 240;
    CONNECTION_ERROR_WIDGET_MOVE_Y = 240;

    /* Select Friends Screen*/
    TAGGED_FRIENDS_ANCHOR_FONT_SIZE = "28";
    //TAGGED_FRIENDS_ANCHOR_FONT_SIZE = "21"; //"21" - redlines
    SELECT_FRIENDS_SELECTED_FRIEND_ITEM_HEIGHT = "36";
    TAG_FRIENDS_SEARCH_TEXT_STYLE = "DEFAULT='font_size=32 valign=0.5 color=#000000'";

    PLACE_ANCHOR_FONT_SIZE = "28";
    //PLACE_ANCHOR_FONT_SIZE = "21"; //"21" - redlines
    CHECKIN_SEARCH_FIELD_TEXT_FORMAT = "DEFAULT='font_size=27 valign=0.5 color=#000'";
    HINT_SEARCH_FOR_PLACES_TEXT_SIZE = "27";

    /*Delete dialogue              */
    ERROR_DIALOGUE_TEXT_SIZE = "32";
//    ERROR_DIALOGUE_TEXT_SIZE = "21"; //"21" - redlines
    DELETE_DIALOGUE_W = 480-33-33;

    /* NearBy Places Popup */
    NEARBY_PLACES_POPUP_TITLE_FONT_SIZE = "30";
    NEARBY_PLACES_POPUP_MSG_FONT_SIZE = "28";

    /* Image Post screen*/
    IMAGE_POST_SCREEN_IMAGE_OFFSET_HEIGHT = 13;

    NOTIFICATION_TITLE_FONT_SIZE = "28";
    NOTIFICATION_TITLE_WRAP_SIZE = 324;

    ALBUM_DETAILS_DESCRIPTION_TEXT_FORMAT = "<font=TizenSans:style=Regular font_size=27 color=#141823>%s</>"; //AAA fontsize should be 21 accoding to redlines
    ALBUM_ITEM_SIZE = 210;

    CAMERA_ROLL_GENGRID_SIZE_W = 160;
    CAMERA_ROLL_GENGRID_SIZE_H = 160;
    CAMERA_ROLL_GENGRID_SIZE_W_CROPPED = 158;
    CAMERA_ROLL_GENGRID_SIZE_H_CROPPED = 158;

    SOMETHING_WRONG_TEXT_SIZE = "36";

    PHOTOCAM_PLACEHOLDER_IMAGE_SIZE = 156;

    BIG_WIDGET_ERROR_DATA_AREA_MIN_SIZE = 280;

    /* Get Messenger screen */
    GET_MESSENGER_TEXT_W = 299;
    USER_FEED_HEADER_REGION_H = 680;

    /* Tabbar FindFriendScreen UserPhotosScreen*/
    TABBAR_ITEM_FONT_SIZE = "34";

    /* Images Carousel Screen*/
        LIKE_FONT_SIZE = "32";

    /*Edit Photo Tag Screen*/
    PT_POPUP_X_POSITION = SCREEN_SIZE_WIDTH / 2;
    PT_POPUP_Y_POSITION = 250;
    PT_POPUP_WIDTH = 320;
    PT_POPUP_ITEM_HEIGHT = 45;
    PT_POPUP_ITEM_Y_OFFSET = 28;
    PT_TAG_FONT_SIZE = "28";

    PT_IMAGE_EDGE_WIDTH = 9;
    PT_IMAGE_BODY_TEXT_OFFSET = 18;
    PT_IMAGE_BODY_DELETE_WIDTH = 18;
    PT_IMAGE_BODY_OFFSET = 30;
    PT_IMAGE_BODY_MAX_WIDTH = SCREEN_SIZE_WIDTH - 2 * PT_IMAGE_BODY_OFFSET;
    PT_TAP_PHOTO_LABEL_FONT_SIZE = "25";
    PT_TAP_PHOTO_LABEL_HEIGHT = 30;
    PT_TAP_PHOTO_LABEL_Y_POSITION = PT_TAP_PHOTO_LABEL_HEIGHT + 27;

}

void UIRes::Init720()
{
    //Common
    SCREEN_SIZE_HEIGHT = 1280;
    SCREEN_SIZE_WIDTH = 720;
    STATUS_BAR_SIZE_H = 40;
    HEADER_SIZE_H = 96;
    MESSAGE_READ_MORE_LIMIT = 750;
    MESSAGE_READ_MORE_LINES_LIMIT = 7;
    REPLY_READ_MORE_LIMIT = 30;

    /* AddTextScreen */
    ADD_TEXT_DELETE_AREA = 1120;
    ADD_TEXT_BOUNDARY_LEFT = 50;
    ADD_TEXT_MIN_TEXT_HEIGHT = 42;
    ADD_TEXT_MAX_TEXT_HEIGHT = 460;

    /* AddTextInputScreen */
    ADD_TEXT_COLOR_PICKER_SIZE_H = 65;
    ADD_TEXT_COLOR_PICKER_OFFSET_L = 557;
    ADD_TEXT_COLOR_PICKER_RECT_SIZE_W_H = 38;
    ADD_TEXT_COLOR_PICKER_RECT_OFFSET_L = ADD_TEXT_COLOR_PICKER_OFFSET_L + 12;

    /* PostComposerScreen */
    POST_COMP_PRIVACY_TO_TEXT_FONT_SIZE = "27";
    POST_COMP_PRIVACY_TO_TEXT_OFFSET_R = 20;
    POST_COMP_PRIVACY_TO_SELECTOR_FONT_SIZE = "27";
    POST_COMP_PRIVACY_TO_IMG_SIZE = "32x32";
    POST_COMPOSER_TEXT_FONT_SIZE = "28";
    POST_COMPOSER_CREATE_ALBUM_TEXT_FONT_SIZE = "32";
    POST_COMPOSER_IMAGE_SIZE = 666;
    POST_COMP_PRIVACY_GROUP_ICON_SIZE = 32;

    /* News Feed */
    NF_PH_TITLE_WRAP_WIDTH = 506;
    NF_PH_TITLE_TEXT_SIZE = "28";
    NF_BH_TITLE_WRAP_WIDTH = 582;
    NF_PH_DATE_TEXT_SIZE = "24";
    NF_PH_DATE_TEXT_FORMAT = "<font_size=24 color=#9197a3>%s</>";
    NF_PIPH_TITLE_WRAP_WIDTH = 436;
    NF_PROGRESS_COUNTER_FONT_SIZE = "24";

    NF_PH_INFOBOX_WIDTH = 556;
    NF_PIPH_INFOBOX_WIDTH = 526;

    NF_COMMENT_MSG_WRAP_WIDTH = 542;
    NF_COMMENT_MSG_TEXT_SIZE = "28";

    NF_POST_MSG_TEXT_SIZE = "28";
    NF_POST_MSG_WRAP_WIDTH = 672;

    NF_PIP_MSG_TEXT_SIZE = "28";
    NF_PIP_MSG_WRAP_WIDTH = 624;

    NF_LINK_LORES_TITLE_TEXT_SIZE = "28";
    NF_LINK_LOREST_CAPTION_TEXT_SIZE = "24";
    NF_LINK_LORES_WRAP_WIDTH = 432;

    NF_LOC_INFO_TITLE_TEXT_SIZE = "28";
    NF_LOC_INFO_TYPE_TEXT_SIZE = "24";
    NF_LOC_INFO_WRAP_WIDTH = 416;
    NF_PRIVACY_ICON_SIZE = 27;
    NF_PRIVACY_ICON_WEIGHT_BEFORE = 0.4;
    NF_PRIVACY_ICON_WEIGHT_AFTER = 27;
    NF_LOC_INFO_WIDTH = 470;

    NF_LANDSCAPE_PHOTO_LAYOUT_OFFSET_W_L = 12;
    NF_LANDSCAPE_PHOTO_LAYOUT_OFFSET_W_R = 12;
    NF_LANDSCAPE_PHOTO_LAYOUT_SIZE_W = 720;
    NF_LANDSCAPE_PHOTO_LAYOUT_SIZE_H = 375;
    NF_SQUARE_PHOTO_LAYOUT_OFFSET_W_L = 48;
    NF_SQUARE_PHOTO_LAYOUT_OFFSET_W_R = 48;
    NF_SQUARE_PHOTO_LAYOUT_SIZE_W = 720;

    UNFOLLOW_TEXT_FONT_SIZE = "30";

    //Settings screen
    SETTINGS_LOGGEDIN_PROFILE_NAME = "33";
    SETTINGS_VIEW_YOUR_PROFILE = "28";
    SETTINGS_CATEGORIES_ITEM_FONT_SIZE = "32";
    SETTINGS_TITLES_ITEM_FONT_SIZE = "28";
    SETTINGS_CATEGORIES_LIST_ITEM_WIDTH = 720;
    SETTINGS_CATEGORIES_LIST_ITEM_HEIGHT = 112;
    SETTINGS_TITLES_LIST_ITEM_WIDTH = 720;
    SETTINGS_TITLES_LIST_ITEM_HEIGHT = 104;

    //SearchScreenTabbed
    SST_LIST_ITEM_SIZE_W = 720;
    SST_LIST_ITEM_SIZE_H = 192;
    SST_MAIN_RECT_SIZE_W = 720;
    SST_MAIN_RECT_SIZE_H = 1055;

    /* Commenting */
    COMMENT_ENTRY_TEXT_SIZE = "32";
    COMMENT_ACTIONBAR_TEXT_SIZE = "24";
    COMMENT_ACTIONBAR_WITH_TIME = "<font_size=24 color=#9fa2a6>%s</>";
    COMMENT_ACTIONBAR_LIKEICON_SIZE = 34;
    COMMENT_ACTIONBAR_LIKE_COUNT_TEXT_SIZE = "26";
    COMMENT_MSG_TEXT_SIZE = "28";
    COMMENT_MSG_WRAP_WIDTH = 560;
    COMMENT_TOP_TITLE ="28";
    COMMENT_TOP_TOOLBAR_TEXTBOX_W = 508;

    COMMENT_REPLY_ITEM_MSG_WRAP_WIDTH = 440;
    COMMENT_REPLY_ITEM_FONT_SIZE = "22";

    COMMENT_LANDSCAPE_PHOTO_LAYOUT_SIZE_W = 538;
    COMMENT_STICKER_LAYOUT_SIZE_W = 200;

    /* FindFriends */
    FF_SEARCH_BTN_TEXT_SIZE = "24";
    FF_SEARCH_RESULT_WRAP_WIDTH = 300;
    FF_ITEM_FRIEND_TEXT_STYLE = "DEFAULT='font_size=28 wrap=word align=right left_margin=0 right_margin=0 text_class=tizen'";
    FF_ITEM_FRIEND_TEXT_LTR_STYLE = "DEFAULT='font_size=28 wrap=word ellipsis=1.0 align=left left_margin=0 right_margin=0 text_class=tizen'";
    FF_ITEM_FRIEND_TEXT_RTL_STYLE = "DEFAULT='font_size=28 wrap=word ellipsis=1.0 align=right left_margin=0 right_margin=0 text_class=tizen'";
    FF_SEARCH_RESULT_NAME_TEXT_SIZE = "30";
    FF_SEARCH_RESULT_MUTUAL_TEXT_SIZE = "24";

    FF_MAIN_RECT_SIZE_W = 720;
    FF_MAIN_RECT_SIZE_H = 1280-237;

    FF_CAROUSEL_SCROLLER_W = 720;
    FF_SUBTEXT_ELLIPSIS = 92;

    /* Main Tabs */
    CW_MAINTABS_SIZE_W = 720;
    CW_MAINTABS_SIZE_H = 96;
    MS_MAIN_RECT_SIZE_W = 720;
    MS_MAIN_RECT_SIZE_H = 1280-237;

    /* General Elements */
    GE_SEARCH_BAR_N_TAB_BAR_SIZE_H = 237;
    GE_TITLEBAR_FONT_SIZE = "36";

    /* Events */
    EVENT_SELECTOR_TEXT_SIZE = "36";
    EVENT_SELECTOR_ICON_SIZE = 42;
    EVENT_CREATE_TEXT_SIZE = "36";

    EVENT_CAROUSEL_SCROLLER_SIZE = 720;

    EVENT_NEAREST_ITEM_WRAP_WIDTH = 394;
    EVENT_NEAREST_TITLE_TEXT_SIZE = "32";
    EVENT_NEAREST_SUBCONTENT_TEXT_SIZE = "28";

    EVENT_BDAY_ITEM_WRAP_WIDTH = 400;
    EVENT_BDAY_ITEM_NAME_TEXT_SIZE = "32";
    EVENT_BDAY_ITEM_SUBCONTENT_TEXT_SIZE = "28";

    EVENT_CAROUSEL_ITEM_WRAP_WIDTH = 496;
    EVENT_CAROUSEL_ITEM_MAIN_TEXT_SIZE = "32";
    EVENT_CAROUSEL_ITEM_SUBCONTENT_TEXT_SIZE = "28";

    EVENT_GUESTLIST_TABTEXT_SIZE = "28";

    //User profile screen
    PROFILE_NAME_FONT_SIZE = "60";
    PROFILE_POSTBTN_FONT_SIZE = "24";
    PROFILE_POSTBTN_ICON_SIZE = 52;
    FRIENDS_LIST_PLACEHOLDER_TEXT_FONT_SIZE = "27";
    PROFILE_TEXT_STYLE = "DEFAULT='font_size=46 wrap=word left_margin=0 right_margin=0 text_class=tizen'";
    PROFILE_TEXT_LTR_STYLE = "DEFAULT='font_size=46 color=#fff wrap=word ellipsis=1.0 align=left left_margin=0 right_margin=0 text_class=tizen'";
    PROFILE_TEXT_RTL_STYLE = "DEFAULT='font_size=46 color=#fff wrap=word ellipsis=1.0 align=right left_margin=0 right_margin=0 text_class=tizen'";

    PROFILE_NAME_ITEM_WIDTH = 465;
    GROUP_NAME_ITEM_WIDTH = 665;

    //User Profile About
    UPA_LAYOUT_W = 690;
    UPA_LAYOUT_DEFAULT_H = 89;
    UPA_LAYOUT_MULTI_H = 140;
    UPA_OV_WRAP_MAX = 480;
    UPA_OV_MORE_HEADING_WRAP_MAX = 160;
    UPA_OV_MORE_INFO_WRAP_MAX = 280;
    UPA_OV_TEXT_FONT_SIZE = "27";
    UPA_OV_PAST_FONT_SIZE = "24";
    UPA_OV_HEADING_FONT_SIZE = "27";
    UPA_OV_INFO_FONT_SIZE = "24";

    //User Profile About More
    UPAM_WORK_LAYOUT_W = 450;
    UPAM_WORK_LAYOUT_H = 186;
    UPAM_WORK_HEADING_LAYOUT_H = 69;
    UPAM_WORK_ADD_LAYOUT_H = 136;
    UPAM_WORK_DEFAULT_FONT_SIZE = "24";
    UPAM_WORK_HEADING_FONT_SIZE = "24";
    UPAM_WORK_ADD_FONT_SIZE = "27";
    UPAM_WORK_NAME_FONT_SIZE = "27";
    UPAM_WORK_WRAP_TEXT = 380;
    UPAM_WORK_HEADING_WRAP_TEXT = 120;

    //Messenger screen font
    MESSENGER_TITLE_FONT_SIZE = "36";
    MESSENGER_MAININFO_FONT_SIZE = "26";
    MESSENGER_BTN_FONT_SIZE = "26";

    //Notification
    NT_TEXT_SIZE = 27;

    /* Profiles WidgetFactory */
    PROFILES_WF_COVER_ITEM_SUBTITLE_FONT_SIZE = "24";
    PROFILES_WF_INFOFIELD_FIRST_FONT_SIZE = "28";
    PROFILES_WF_INFOFIELD_SECOND_FONT_SIZE = "24";
    PROFILES_WF_ABOUT_WRAP_WIDTH = 624;
    PROFILES_WF_ABOUT_FONT_SIZE = "28";
    PROFILES_WF_INFOFIELD_WRAP_WIDTH = 588;

    /* Popup Widget */
    POPUP_WIDGET_MENU_ITEM_HEIGHT = 120;
    POPUP_WIDGET_MENU_MULTI_ITEM_HEIGHT = 132;
    POPUP_WIDGET_MENU_ITEM_OFFSET = 5;
    POPUP_WIDGET_MENU_ITEM_SCREEN_HEIGHT = 1280;

    POPUP_POST_DELETE_ITEM_SCREEN_HEIGHT = 400;
    POPUP_POST_DELETE_ITEM_SCREEN_WIDHT = 720;
    POPUP_POST_DELETE_MOVE_X = 70;
    POPUP_POST_DELETE_MOVE_Y = 70;

    /* Create Event */
    EVENT_CREATE_NAME_FONT_SIZE = "32";
    EVENT_CREATE_DETAIL_FONT_SIZE = "32";
    EVENT_CREATE_ENTRY_PUSH = "DEFAULT='font_size=32 color=#000 left_margin=0 right_margin=0'";

    /* Crop Rectangle Params*/
    CROP_RECT_SIZE = 80;
    CROP_REGION_MIN_WIDTH = 120;
    CROP_REGION_MIN_HEIGHT = 120;
    CM_OFFSET = 16;

    ACTION_BAR_SIZE_H = 96;

    RENAME_ALBUM_TEXT_STYLE = "DEFAULT='font=TizenSans:style=Regular font_size=32 color=#000'";

    SEARCH_TABS_SCROLLING_WIDTH = 720;

    EXCEPT_SPECIFY_FRIENDS_TO_TEXT_FORMAT = "<font=TizenSans:style=Regular font_size=27 color=#9298a4>%s</>";
    EXCEPT_SPECIFY_FRIENDS_TEXT_FORMAT = "<font=TizenSans:style=Regular font_size=27 color=#ff2929>%s</>"; //AAA ToDo: check text color
    EXCEPT_SPECIFY_FRIENDS_SEARCH_TEXT_STYLE = "DEFAULT='font_size=27 color=#000'";
    POST_SCREEN_TITLE = "<font=TizenSans:style=Medium font_size=36 color=#FFFFFF>%s</font>";

    SHARE_WITH_TO_TEXT_FORMAT = "<font=TizenSans:style=Regular font_size=27 color=#588fff>%s</>";

    /* Screen base */
    SCREEN_BASE_PROGRESS_BAR_ITEM_OBJ_RESIZE = 74;
    SCREEN_BASE_PROGRESS_BAR_ITEM_OBJ_MOVE_X = 322;
    SCREEN_BASE_PROGRESS_BAR_ITEM_OBJ_MOVE_Y = 600;

    CONNECTION_ERROR_WIDGET_MOVE_X = 360;
    CONNECTION_ERROR_WIDGET_MOVE_Y = 600;

    /* Select Friends Screen*/
    TAGGED_FRIENDS_ANCHOR_FONT_SIZE = "28";
    SELECT_FRIENDS_SELECTED_FRIEND_ITEM_HEIGHT = "48";
    TAG_FRIENDS_SEARCH_TEXT_STYLE = "DEFAULT='font=TizenSans:style=Regular font_size=36 valign=0.5 color=#000000'";

    PLACE_ANCHOR_FONT_SIZE = "28";
    CHECKIN_SEARCH_FIELD_TEXT_FORMAT = "DEFAULT='font_size=36 valign=0.5 color=#000'";
    HINT_SEARCH_FOR_PLACES_TEXT_SIZE = "36";

    /*Delete dialogue              */
    ERROR_DIALOGUE_TEXT_SIZE = "28";
    DELETE_DIALOGUE_W = 720-42-42;

    /* NearBy Places Popup */
    NEARBY_PLACES_POPUP_TITLE_FONT_SIZE = "34";
    NEARBY_PLACES_POPUP_MSG_FONT_SIZE = "32";

    /* Image Post screen*/
    IMAGE_POST_SCREEN_IMAGE_OFFSET_HEIGHT = 16;

    NOTIFICATION_TITLE_FONT_SIZE = "28";
    NOTIFICATION_TITLE_WRAP_SIZE = 512;

    ALBUM_DETAILS_DESCRIPTION_TEXT_FORMAT = "<font=TizenSans:style=Regular font_size=27 color=#141823>%s</>";
    ALBUM_ITEM_SIZE = 280;

    CAMERA_ROLL_GENGRID_SIZE_W = 180;
    CAMERA_ROLL_GENGRID_SIZE_H = 180;
    CAMERA_ROLL_GENGRID_SIZE_W_CROPPED = 178;
    CAMERA_ROLL_GENGRID_SIZE_H_CROPPED = 178;

    SOMETHING_WRONG_TEXT_SIZE = "45";

    PHOTOCAM_PLACEHOLDER_IMAGE_SIZE = 234;

    BIG_WIDGET_ERROR_DATA_AREA_MIN_SIZE = 390;

    /* Get Messenger screen */
    GET_MESSENGER_TEXT_W = 448;
    USER_FEED_HEADER_REGION_H = 900;

    /* Tabbar FindFriendScreen UserPhotosScreen*/
    TABBAR_ITEM_FONT_SIZE = "28";

    /* Images Carousel Screen*/
    LIKE_FONT_SIZE = "24";

    /*Edit Photo Tag Screen*/
    PT_POPUP_X_POSITION = SCREEN_SIZE_WIDTH / 2;
    PT_POPUP_Y_POSITION = 250;
    PT_POPUP_WIDTH = 320;
    PT_POPUP_ITEM_Y_OFFSET = 28;
    PT_POPUP_ITEM_HEIGHT = 45;
    PT_TAG_FONT_SIZE = "28";

    PT_IMAGE_EDGE_WIDTH = 9;
    PT_IMAGE_BODY_TEXT_OFFSET = 18;
    PT_IMAGE_BODY_DELETE_WIDTH = 18;
    PT_IMAGE_BODY_OFFSET = 45;
    PT_IMAGE_BODY_MAX_WIDTH = SCREEN_SIZE_WIDTH - 2 * PT_IMAGE_BODY_OFFSET;
    PT_TAP_PHOTO_LABEL_FONT_SIZE = "27";
    PT_TAP_PHOTO_LABEL_HEIGHT = 35;
    PT_TAP_PHOTO_LABEL_Y_POSITION = PT_TAP_PHOTO_LABEL_HEIGHT + 40;
}
