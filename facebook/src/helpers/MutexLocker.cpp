/*
 * MutexLocker.cpp
 *
 * Created on: Aug 24, 2015
 * Author: Dmitry.Shcherbakov@SymphonyTeleca.com
 */

#include <eina_lock.h>
#include "Mutex.h"
#include "MutexLocker.h"
#include "ErrorHandling.h"

MutexLocker::MutexLocker(Eina_Lock* mutex): mMutexHandle(mutex), mMutexObject(NULL) {
    CHECK_RET_NRV(mMutexHandle);
    eina_lock_take(mMutexHandle);
}

MutexLocker::MutexLocker(Mutex* mutex): mMutexHandle(NULL), mMutexObject(mutex) {
    CHECK_RET_NRV(mMutexObject);
    mMutexObject->take();
}

MutexLocker::~MutexLocker() {
    if(mMutexHandle) eina_lock_release(mMutexHandle);
    if(mMutexObject) mMutexObject->release();
}

