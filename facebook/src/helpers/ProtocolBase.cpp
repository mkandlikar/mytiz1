#include "ProtocolBase.h"

#include <cassert>
#include <stdio.h>

const char* ProtocolBase::mKeyMessageType = "messagetype";
const char* ProtocolBase::mKeyMessageId = "id";
const char* ProtocolBase::mKeyCancelId = "cancelid";
const char* ProtocolBase::mKeyCancelMessageType = "cancelmessagetype";
const char* ProtocolBase::mKeyRequestId = "requestid";
const char* ProtocolBase::mKeyError = "error";

const int ProtocolBase::mMaxRequestNumber = 100000;


ProtocolBase::RequestBase::RequestBase(MessageType requestType, MessageId requestId, IProtocolClient *client) : mId(requestId), mClient(client), mMessageType(requestType) {
}

ProtocolBase::RequestBase::~RequestBase() {
    mClient = NULL;
}

MessageId ProtocolBase::RequestBase::GetId() const {
    return mId;
}

IProtocolClient *ProtocolBase::RequestBase::GetClient() const {
    return mClient;
}

MessageType ProtocolBase::RequestBase::GetMessageType() const {
    return mMessageType;
}


ProtocolBase::ProtocolBase(const char *localPortName, const char *remoteAppId, const char *remotePortName) : IProtocol(localPortName, remoteAppId, remotePortName), mLastMessageId(0) {
}

ProtocolBase::~ProtocolBase () {
    typedef std::map<MessageId, RequestBase*>::iterator it_type;
    for(it_type iterator = mRequests.begin(); iterator != mRequests.end(); iterator++) {
        delete iterator->second;
    }
    mRequests.clear();
}

/**
 * @brief Generates unique request Id.
 */
MessageId ProtocolBase::GenereateMessageId() {
    return mLastMessageId < mMaxRequestNumber ? ++mLastMessageId : mLastMessageId = 1;
}

/**
 * @brief             Creates bundle for request.
 * @details           Creates bundle for request with filled request type and unique message Id.
 * @requestType[in]   Request type.
 * @return            Created bundle with filled request type and message Id fields.
 */
bundle *ProtocolBase::CreateBundleForRequest(MessageType requestType) {
    bundle *b = CreateBundle(requestType);
    AddIntValueToBundle (b, mKeyMessageId, GenereateMessageId());
    return b;
}

/**
 * @brief             Creates bundle for response.
 * @details           Creates bundle for response with filled response type and request Id.
 * @responseType[in]  Response type.
 * @MessageId[in]     Message id received and stored from appropriate request.
 * @return            Created bundle with filled response type and message Id fields.
 */
bundle *ProtocolBase::CreateBundleForResponse(MessageType responseType, MessageId requestId) const {
    bundle *b = CreateBundle(responseType);
    AddIntValueToBundle (b, mKeyRequestId, requestId);
    return b;
}

/**
 * @brief             Creates bundle for Cancel request.
 * @details           Creates bundle for Cancel request with filled message type and request Id.
 * @MessageId[in]     Message id for request to be canceled.
 * @return            Created bundle with filled message type and cancel Id fields.
 */
bundle *ProtocolBase::CreateBundleForCancel(RequestBase *request) const {
    assert(request);
    bundle *b = CreateBundle(EBPCancel);
    AddIntValueToBundle (b, mKeyCancelId, request->GetId());
    AddIntValueToBundle (b, mKeyCancelMessageType, request->GetMessageType());
    return b;
}

/**
 * @brief             Creates bundle for message (request or response).
 * @details           Creates bundle for message with filled request type field.
 * @messageType[in]   Message type.
 * @return            Created bundle with filled message type field.
 */
bundle *ProtocolBase::CreateBundle(MessageType messageType) const {
    bundle *b = bundle_create ();
    AddIntValueToBundle(b, mKeyMessageType, messageType);
    return b;
}

void ProtocolBase::SetError(bundle* data, ErrorCode error) const {
    AddIntValueToBundle(data, mKeyError, error);
}

MessageType ProtocolBase::GetMessageType(bundle* data) const {
    return GetIntValueFromBundle(data, mKeyMessageType);
}

MessageId ProtocolBase::GetMessageId(bundle* data) const {
    return GetIntValueFromBundle(data, mKeyMessageId);
}

MessageId ProtocolBase::GetRequestId(bundle* data) const {
    return GetIntValueFromBundle(data, mKeyRequestId);
}

MessageId ProtocolBase::GetCancelId(bundle* data) const {
    return GetIntValueFromBundle(data, mKeyCancelId);
}

MessageId ProtocolBase::GetCancelMessageType(bundle* data) const {
    return GetIntValueFromBundle(data, mKeyCancelMessageType);
}

ErrorCode ProtocolBase::GetError(bundle* data) const {
    return GetIntValueFromBundle(data, mKeyError);
}

/**
 * @brief             Sends message via the Message Port.
 * @abundle[in]       Bundle to be sent via the Message Port.
 * @return            Result.
 */
int ProtocolBase::SendMessage(bundle* abundle) {
    return mMPT.SendMessage(abundle);
}

void ProtocolBase::HandleReceivedData(bundle* data) {
    MessageType messageType = GetMessageType(data);

    switch (messageType) {
    case EBPCancel:
    {
        MessageId cancelId = GetCancelId(data);
        MessageType cancelType = GetCancelMessageType(data);
        CancelRequest(cancelId, cancelType);
    break;
    }
    default:
        DoHandleReceivedData(messageType, data);
    break;
    }
}

void ProtocolBase::Cancel(MessageId requestId) {
    RequestBase* request = FindRequest(requestId);
    Cancel(request);
}

void ProtocolBase::Cancel(RequestBase *request) {
    if (request) {
        bundle *b = CreateBundleForCancel(request);
        SendMessage(b);
        bundle_free (b);
        mRequests.erase(request->GetId());
        delete request;
    }
}

/**
 * @brief             Helper function which allows to add integer value associated with key into bundle.
 * @abundle[in]       Bundle.
 * @key[in]           Key for associated with integer value.
 * @value[in]         Integer value associated with key to be added into bundle.
 */
void ProtocolBase::AddIntValueToBundle(bundle* abundle, const char* key, int value) const {
    bundle_add_byte(abundle, key, (const void *)&value, sizeof(int));
}

/**
 * @brief             Helper function which allows to read integer value associated with key from bundle.
 * @abundle[in]       Bundle.
 * @key[in]           Key for associated with integer value.
 * @value[in]         Integer value associated with key to be read from bundle.
 */
int ProtocolBase::GetIntValueFromBundle(bundle* abundle, const char* key) const {
    int *n = NULL;
    size_t n_size;
    bundle_get_byte(abundle, key, (void**)&n, &n_size);
    return (n == NULL ? -1 : *n);
}

/**
 * @brief             Adds async request to map.
 * @details           Map stores async requests associated with Ids while request isn't completed.
 * @request[in]       Request to be stored.
 */
void ProtocolBase::AddRequest(RequestBase* request) {
    request->GetClient()->SetProtocol(this);
    mRequests[request->mId] = request;
}

/**
 * @brief             Finds request by its Is in map.
 * @requestId[in]     Id for request to be found.
 * @return            Pointer to request found.
 */
ProtocolBase::RequestBase* ProtocolBase::FindRequest(MessageId requestId) {
    RequestBase* request = NULL;
    std::map<MessageId, RequestBase*>::iterator it;
    it = mRequests.find(requestId);
    if(it != mRequests.end()) {
        request = it->second;
    }
    return request;
}

/**
 * @brief             Removes request represented by Id from map.
 * @requestId[in]     Id for request to be removed.
 */
void ProtocolBase::RemoveRequest(MessageId requestId) {
    RequestBase* request = FindRequest(requestId);
    if (request) {
        mRequests.erase(requestId);
        delete request;
    }
}

void ProtocolBase::clearAllPendingRequestOnLogout()
{
    typedef std::map<MessageId, RequestBase*>::iterator it_type;
    for(it_type iterator = mRequests.begin(); iterator != mRequests.end(); iterator++) {
        delete iterator->second;
    }
    mRequests.clear();
}

//class IProtocolClient
IProtocolClient::IProtocolClient() : mProtocol(NULL) {
}

void IProtocolClient::SetProtocol(ProtocolBase *protocol) {
    mProtocol = protocol;
}

void IProtocolClient::Cancel(MessageId requestID) {
    if (mProtocol) {
        mProtocol->Cancel(requestID);
    }
}

