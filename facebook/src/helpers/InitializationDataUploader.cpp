#include "DataUploader.h"
#include "InitializationDataUploader.h"

#include <assert.h>

#include "Common.h"
#include "Config.h"
#include "ErrorHandling.h"
#include "FacebookSession.h"
#include "HomeProvider.h"
#include "FriendRequestsBatchProvider.h"
#include "Log.h"
#include "OwnFriendsProvider.h"
#include "UserProfileDataUploader.h"


InitializationDataUploader::InitializationDataUploader( data_initialization_done_cb done_cb )
{
    m_UploadingDone = done_cb;
    // We do not care about the Friends Batch request result. Just send it.
    FriendRequestsBatchProvider::GetInstance()->UploadData(true);
    m_HomeDataAssistant = new HomeDataAssistant( done_cb );
    m_FriendsDataUploader = new DataUploader( OwnFriendsProvider::GetInstance(), friends_has_been_received_cb, this );
    mOwnProfileData = new UserProfileDataUploader(Config::GetInstance().GetUserId().c_str(), on_user_profile_info_received_cb, this);
}

InitializationDataUploader::~InitializationDataUploader()
{
    HomeProvider::GetInstance()->Unsubscribe( m_HomeDataAssistant );
    delete m_HomeDataAssistant;
    delete m_FriendsDataUploader;
    delete mOwnProfileData;
}

void InitializationDataUploader::StartUploading()
{
    m_FriendsDataUploader->StartUploading();
}

void InitializationDataUploader::StartUploadHomeData()
{
    HomeProvider::GetInstance()->Subscribe( m_HomeDataAssistant );
    // only single request is required for the minimal work
    HomeProvider::GetInstance()->MissCacheRetrieve(true);
    UPLOAD_DATA_RESULT result = HomeProvider::GetInstance()->UploadData();
    switch ( result )
    {
        case NONE_DATA_COULD_BE_RESULT:
        {
            HomeProvider::GetInstance()->Unsubscribe( m_HomeDataAssistant );
            m_UploadingDone();
            break;
        }
        case ONLY_REQUEST_SENT_RESULT:
        {
            // waiting for a response. An appropriate callback will be called
            // by m_HomeDataAssistent
            break;
        }
        case ONLY_CACHED_DATA_RESULT:
        {
            Log::error(LOG_FACEBOOK_USER, "InitializationDataUploader::StartUploadHomeData->error, we should not retrieve cached items after LogIn");
            HomeProvider::GetInstance()->Unsubscribe( m_HomeDataAssistant );
            m_UploadingDone();
            break;
        }
        default:
            assert( false );
    }
}

void InitializationDataUploader::friends_has_been_received_cb(void *parent)
{
    InitializationDataUploader *object = static_cast<InitializationDataUploader*>(parent);
    assert(object);
    object->mOwnProfileData->StartUploading();
}

void InitializationDataUploader::on_user_profile_info_received_cb(void *parent, int code)
{
    Log::info(LOG_FACEBOOK_USER, "InitializationDataUploader::on_user_profile_info_received_cb");
    InitializationDataUploader *object = static_cast<InitializationDataUploader*>(parent);
    assert(object);
    object->StartUploadHomeData();
}

/*
 * @class HomeDataAssistant
 */

InitializationDataUploader::HomeDataAssistant::HomeDataAssistant( data_initialization_done_cb done_cb )
{
    m_HomeDataRecevived_cb = done_cb;
}

InitializationDataUploader::HomeDataAssistant::~HomeDataAssistant()
{
    m_HomeDataRecevived_cb = NULL;
    HomeProvider::GetInstance()->Unsubscribe( this );
}

void InitializationDataUploader::HomeDataAssistant::Update( AppEventId eventId, void *data )
{
    // we do not care about eventId and/or data params. The only response as fact is important for us
    HomeProvider::GetInstance()->Unsubscribe( this );
    if(m_HomeDataRecevived_cb) {
        m_HomeDataRecevived_cb();
    }
}
