/*
 * ParserWrapper.cpp
 *
 * Created: Aug 6, 2015
 * Author: Dmitry.Shcherbakov@SymphonyTeleca.com
 */

#include <json-glib/json-glib.h>
#include "ParserWrapper.h"

ParserWrapper::ParserWrapper( JsonParser *parser ) : m_Parser( parser )
{
}

ParserWrapper::~ParserWrapper()
{
    g_object_unref( m_Parser );
}

