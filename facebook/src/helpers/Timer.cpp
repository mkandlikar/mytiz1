#include "Timer.h"

const char Timer::TimerLogTag[] = "Facebook::Timer";

Timer::Timer(Timer&& other) noexcept {
    mPeriod = other.mPeriod;
    other.mPeriod = TimerPeriod(0ull);
    mNativeTimer = other.mNativeTimer;
    other.mNativeTimer = nullptr;
    mRepeats = other.mRepeats;
    other.mRepeats = 0u;
}

Timer::~Timer() noexcept {
    if(mNativeTimer) ecore_timer_del(mNativeTimer);
}

void Timer::Start(unsigned int repeats) noexcept {
    if(mNativeTimer) return;
    mRepeats = repeats;
    Log::verbose_tag(TimerLogTag, "Timer::Start repeats: `%d`", mRepeats);
    mNativeTimer = ecore_timer_add(mPeriod.count() / multiplier, &Timer::NativeTimerCallback, this);
}

void Timer::Stop() noexcept {
    Log::verbose_tag(TimerLogTag, "Timer::Stop");
    if(mNativeTimer) {
        ecore_timer_del(mNativeTimer);
        mNativeTimer = nullptr;
    }
}

void Timer::Freeze() noexcept {
    Log::verbose_tag(TimerLogTag, "Timer::Freeze");
    if(mNativeTimer) ecore_timer_freeze(mNativeTimer);
}

void Timer::Thaw() noexcept {
    Log::verbose_tag(TimerLogTag, "Timer::Thaw");
    if(mNativeTimer) ecore_timer_thaw(mNativeTimer);
}

void Timer::Reset() noexcept {
    Log::verbose_tag(TimerLogTag, "Timer::Reset");
    if(mNativeTimer) ecore_timer_reset(mNativeTimer);
}

bool Timer::IsSet() const noexcept {
    Log::verbose_tag(TimerLogTag, "Timer::IsTicking `%d`", bool(mNativeTimer));
    return mNativeTimer;
}

unsigned int Timer::GetLeftRepeats() const noexcept {
    Log::verbose_tag(TimerLogTag, "Timer::GetLeftRepeats `%d`", mRepeats);
    return mRepeats;
}

Eina_Bool Timer::NativeTimerCallback(void* thisPtr) noexcept {
    Log::verbose_tag(TimerLogTag, "Timer::NativeTimerCallback");
    Timer& self = *static_cast<Timer*>(thisPtr);
    Log::verbose_tag(TimerLogTag, "Timer::NativeTimerCallback calling outer callback");
    Log::verbose_tag(TimerLogTag, "Timer::NativeTimerCallback repeats: `%d`", self.mRepeats);
    if(self.mRepeats) {
        // certain count was specified
        self.mRepeats--;
        Log::verbose_tag(TimerLogTag, "Timer::NativeTimerCallback decreased repeat count, repeats: `%d`", self.mRepeats);
        if(self.mCallback) self.mCallback();
        if(self.mRepeats) {
            return EINA_TRUE;
        } else {
            self.Stop();
            return EINA_FALSE;
        }
    } else {
        // endless loop
        Log::verbose_tag(TimerLogTag, "Timer::NativeTimerCallback endless mode, repeats: `%d`", self.mRepeats);
        if(self.mCallback) self.mCallback();
        return EINA_TRUE;
    }
}
