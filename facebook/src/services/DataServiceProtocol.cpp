#include "Application.h"
#include "Common.h"
#include "DataServiceProtocol.h"
#include "Log.h"

#include <stdlib.h>
#include <cassert>

#define remote_port "servicePort"
#define local_port "UIPort"


DataServiceProtocol::DataServiceProtocol() : ProtocolBase(local_port, DATASERVICE_NAME, remote_port), mLoginState(false) {
}

DataServiceProtocol::~DataServiceProtocol() {
}

/**
* @brief             Implements callback function from the ProtocolBase.
*/
void DataServiceProtocol::DoHandleReceivedData(MessageType messageType, bundle* data) {
    Log::info(LOG_FACEBOOK_DATASERVICEPROTOCOL, "Received message of type: %d", messageType);
    switch (messageType) {
    case EDownloadImageResponse:
    {
        MessageId requestId = GetRequestId(data);
        char *fileName = NULL;
        ErrorCode error = GetError(data);
        if (EBPErrNone == error) {
            bundle_get_str (data, KFileName, &fileName);
            Log::info(LOG_FACEBOOK_DATASERVICEPROTOCOL, "File download has completed sucessfully, file name:%s ", fileName ? fileName : "");
        } else {
            Log::error(LOG_FACEBOOK_DATASERVICEPROTOCOL, "File download has completed with error=%d, file name:%s ", error, fileName ? fileName : "");
        }
        DownloadImageRequest* request = dynamic_cast<DownloadImageRequest*>(FindRequest(requestId));
        if (request) {
            IDataServiceProtocolClient* protocolClient = dynamic_cast<IDataServiceProtocolClient*>(request->GetClient());
            if (protocolClient) {
                Log::info(LOG_FACEBOOK_DATASERVICEPROTOCOL, "Found reqId: %d, mLoginState = %d", request->GetId(), mLoginState);
                protocolClient->HandleDownloadImageResponse(error, requestId, request->mUrl, fileName);
            }
            RemoveRequest(requestId);
        } else {
            Log::info(LOG_FACEBOOK_DATASERVICEPROTOCOL, "Request with reqId: %d: was not found. Probably it was canceled. ", requestId);
        }
    }
        break;
    default:
        break;
    }
}

void DataServiceProtocol::CancelRequest(MessageId requestId, MessageType requestType) {

}

/**
 * @brief             Creates and sends request for image downloading.
 * @client[in]        Pointer to object of client class (which implements IDataServiceProtocolClient).
 * When the request is completed then callback of this object is called.
 * @url[in]           Url for image file to be downloaded.
 * @requestID[out]    Request Id for created request.
 * @return            Result.
 */
bool DataServiceProtocol::DownloadImage(IProtocolClient* client, const char* url, MessageId &requestID) {
    assert(url && *url != '\0');
    if (!url || *url == '\0') {
        Log::error(LOG_FACEBOOK_DATASERVICEPROTOCOL, "Url is empty!");
        return false;
    }
    bundle *b = CreateBundleForRequest(EDownloadImageRequest);
    requestID = GetMessageId(b);
    bundle_add_str (b, KUrl, url);

    bool ret = (SendMessage(b) == MESSAGE_PORT_ERROR_NONE);
    if (ret) {
        DownloadImageRequest *req = new DownloadImageRequest(EDownloadImageRequest, requestID, client, url);
        AddRequest(req);
        Log::info(LOG_FACEBOOK_DATASERVICEPROTOCOL, "Download image request has been successfully sent with id: %d", requestID);
    } else {
        requestID = 0;
        Log::error(LOG_FACEBOOK_DATASERVICEPROTOCOL, "Download image request has been failed!");
    }

    bundle_free (b);
    return ret;
}

/**
 * @brief             Creates and sends request for stopping the Data Service.
 */
void DataServiceProtocol::StopDataService() {
    bundle *b = CreateBundle(EShutdownRequest);
    if (SendMessage(b) == MESSAGE_PORT_ERROR_NONE) {
        Log::info(LOG_FACEBOOK_DATASERVICEPROTOCOL, "ShutDown request has been successfully sent");
    } else {
        Log::error(LOG_FACEBOOK_DATASERVICEPROTOCOL, "ShutDown request has failed!");
    }
    bundle_free (b);
}

/**
 * @brief             Creates and sends request for connecting the Data Service to push-service.
 */
void DataServiceProtocol::NotifyDataService() {
    bundle *b = CreateBundle(EConnectToPushRequest);
    int ret = SendMessage(b);
    Log::info(LOG_FACEBOOK_DATASERVICEPROTOCOL, "NotifyDataService request has %s",
               ((ret == MESSAGE_PORT_ERROR_NONE) ? "been successfully sent" : "failed!"));
    bundle_free (b);
}

void DataServiceProtocol::ClearUserData() {
    bundle *b = CreateBundle(EDeleteUserData);
    Log::info(LOG_FACEBOOK_DATASERVICEPROTOCOL, "Clear user data request has %s",
            (SendMessage(b) == MESSAGE_PORT_ERROR_NONE) ? "been successfully sent" : "failed!");
    bundle_free(b);
}

void DataServiceProtocol::ClearOldUserData(){
    bundle *b = CreateBundle(EDeleteOldUserData);
    Log::info(LOG_FACEBOOK_DATASERVICEPROTOCOL, "Clear old user data request has %s",
            (SendMessage(b) == MESSAGE_PORT_ERROR_NONE) ? "been successfully sent" : "failed!");
    bundle_free(b);
}

void DataServiceProtocol::SendAppBGMessage(){
    bundle *b = CreateBundle(EAppGoToBG);
    Log::info(LOG_FACEBOOK_DATASERVICEPROTOCOL, "Send App BG Message request has %s",
            (SendMessage(b) == MESSAGE_PORT_ERROR_NONE) ? "been successfully sent" : "failed!");
    bundle_free(b);
}

void DataServiceProtocol::setLoginState(bool state) {
    mLoginState = state;
    if (!mLoginState) {
        clearDownloadRequest();
    }
}

void DataServiceProtocol::clearDownloadRequest() {
    clearAllPendingRequestOnLogout();
}

int DataServiceProtocol::SendMessage(bundle* abundle) {
    int res = mMPT.SendMessage(abundle);
    if (res == MESSAGE_PORT_ERROR_PORT_NOT_FOUND) {
        Application::GetInstance()->ReStartDataService();
        mMPT.OpenPort();
        res = mMPT.SendMessage(abundle);
    }
    return res;
}

DataServiceProtocol::DownloadImageRequest::DownloadImageRequest(MessageType requestType, MessageId requestId, IProtocolClient *client, const char* url) :
                ProtocolBase::RequestBase(requestType, requestId, client), mUrl(strdup(url)) {
}

DataServiceProtocol::DataServiceProtocol::DownloadImageRequest::~DownloadImageRequest() {
    free((char*)mUrl);
}
