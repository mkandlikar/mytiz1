#include <app.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <app_i18n.h>

#include "Application.h"
#include "Common.h"
#include "LocationService.h"
#include "NearByPlacesComposer.h"
#include "Utils.h"

double LocationService::mLatitude = 999.0;
double LocationService::mLongitude = 999.0;
time_t LocationService::mTimestamp = 0;

LocationService::LocationService(void (*callback)(void *, double, double, int), void* callback_object) {
    manager = NULL;
    mCallback = callback;
    mCallbackObject = callback_object;
    location_manager_init();
}

LocationService::~LocationService() {
    mCallback = NULL;
    mCallbackObject = NULL;
}

void LocationService::stop() {
    location_manager_unset_service_state_changed_cb(manager);
    location_manager_unset_setting_changed_cb(LOCATIONS_METHOD_HYBRID);
    location_manager_stop(manager);
    location_manager_destroy(manager);
    HidePopUp();
}

void LocationService::location_manager_init() {
    int ret = location_manager_create(LOCATIONS_METHOD_HYBRID, &manager);

    if (ret != LOCATIONS_ERROR_NONE) {
        Log::debug( "LocationService :: Create FAILED = %d", ret);
        show_popup(ret);
    } else {
        if (ret == LOCATIONS_ERROR_NONE && (mCallback != NULL)) {
            location_manager_set_setting_changed_cb(LOCATIONS_METHOD_HYBRID, setting_changed_cb, this);
            ret = location_manager_set_service_state_changed_cb(manager, LocationService::state_changed_cb, this);
            Log::debug( "LocationService ::state change callback  register = %d", ret);
            if (ret == LOCATIONS_ERROR_NONE)
                get_location();
        } else if (ret == LOCATIONS_ERROR_SERVICE_NOT_AVAILABLE) {
            Log::debug( "LocationService :: get last location ret val = %d", ret);
            show_popup(ret);
        }
    }
}

void LocationService::get_location() {
    long cur_time, last_time, diff;
    double latitude, longitude, altitude, climb, direction, speed;
    double horizontal, vertical;
    location_accuracy_level_e level;
    time_t timestamp = 0;
    int ret = location_manager_get_last_location(manager, &altitude, &latitude, &longitude, &climb, &direction, &speed, &level, &horizontal,
            &vertical, &timestamp);

    Log::debug( "LocationService :: get last location ret val = %d", ret);
    Log::debug( "LocationService :: location_manager_get_last_location LAT = %f", latitude);
    Log::debug( "LocationService :: location_manager_get_last_location LONG = %f", longitude);
    Log::debug( "LocationService :: location_manager_get_last_location TIME = %d", timestamp);

    Log::debug( "LocationService :: UNIX TIME = %ld", Utils::GetUnixTime_inSec());

    if (ret == LOCATIONS_ERROR_NONE) {
        cur_time = static_cast<long>(Utils::GetUnixTime_inSec());
        last_time = static_cast<long>(timestamp);
        diff = cur_time - last_time;

        Log::debug( "LocationService :: DIFF TIME = %ld", diff);

        if (diff < TEN_MINUTES) {
            setLatitudeLongitude(latitude, longitude, timestamp, ret);
        } else {
            setLatitudeLongitude(mLatitude, mLongitude, mTimestamp, ret);
            start();
        }
    } else {
        Log::debug("LocationService :: get last location ret val ESLE  = %d", ret);
        setLatitudeLongitude(mLatitude, mLongitude, mTimestamp, mTimestamp ? LOCATIONS_ERROR_NONE : LOCATIONS_ERROR_NOT_SUPPORTED);
        start();
    }
}

void LocationService::start() {

    if (manager) {
        mManagerState = location_manager_start(manager);
        Log::debug( "LocationService :: START ret val = %d", mManagerState);

        if (mManagerState == LOCATIONS_ERROR_NONE) {
            Log::debug( "LocationService :: START SUCCESS");
        } else {
            Log::debug( "LocationService :: LOCATIONS_ERROR = %d", mManagerState);
            show_popup(mManagerState);
        }

    } else {
        Log::debug( "LocationService :: START FAILED");
        location_manager_destroy(manager);
        manager = NULL;
    }
}

void LocationService::state_changed_cb(location_service_state_e state, void *user_data) {
    LocationService* me = static_cast<LocationService *>(user_data);

    Log::debug( "state_changed_cb STATE: %d", state);

    if (state == LOCATIONS_SERVICE_ENABLED) {
        me->HidePopUp();
        me->get_location();
    }
}

void LocationService::setting_changed_cb(location_method_e method, bool enable, void *user_data) {
    Log::debug( "LocationService :: setting_changed_cb STATE = %d", enable);
    LocationService* me = static_cast<LocationService *>(user_data);
    if (enable) {
        me->HidePopUp();
        me->start();
    }
}

void LocationService::setLatitudeLongitude(double latitude, double longtitude, time_t timestamp, int result) {
    mLatitude = latitude;
    mLongitude = longtitude;
    mTimestamp = timestamp;
    if (mCallback != NULL) {
        if (result == LOCATIONS_ERROR_NONE)
            mCallback(mCallbackObject, mLatitude, mLongitude, LOCATIONS_ERROR_NONE);
        else
            mCallback(mCallbackObject, mLatitude, mLongitude, LOCATIONS_ERROR_NOT_SUPPORTED);
    }
}

void LocationService::show_popup(int result) {
    switch (result) {
    case LOCATIONS_ERROR_GPS_SETTING_OFF:
        static_cast<NearByPlacesComposer*>(mCallbackObject)->mConfirmationDialog =
            ConfirmationDialog::CreateAndShow(Application::GetInstance()->GetTopScreen()->getMainLayout(),
                                              "IDS_POPUP_TITLE1", "IDS_POPUP_MSG1", "IDS_POPUP_SKIP", "IDS_POPUP_SETTINGS",
                                              confirmation_dialog_cb, this);
        break;
    case LOCATIONS_ERROR_NETWORK_FAILED:
        static_cast<NearByPlacesComposer*>(mCallbackObject)->mConfirmationDialog =
            ConfirmationDialog::CreateAndShow(Application::GetInstance()->GetTopScreen()->getMainLayout(),
                                              "IDS_POPUP_TITLE2", "IDS_POPUP_MSG2", NULL, "IDS_OK",
                                              confirmation_dialog_cb, this);
        break;
    case LOCATIONS_ERROR_SERVICE_NOT_AVAILABLE:
        static_cast<NearByPlacesComposer*>(mCallbackObject)->mConfirmationDialog =
            ConfirmationDialog::CreateAndShow(Application::GetInstance()->GetTopScreen()->getMainLayout(),
                                              "IDS_POPUP_TITLE2", "IDS_POPUP_MSG3", NULL, "IDS_OK",
                                              confirmation_dialog_cb, this);
        break;
    default:
        static_cast<NearByPlacesComposer*>(mCallbackObject)->mConfirmationDialog =
            ConfirmationDialog::CreateAndShow(Application::GetInstance()->GetTopScreen()->getMainLayout(),
                                              NULL, "IDS_POPUP_TITLE2", NULL, "IDS_OK",
                                              confirmation_dialog_cb, this);
        break;
    }
}

void LocationService::confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event) {

    LocationService *me = static_cast<LocationService*>(user_data);

    switch (event) {
    case ConfirmationDialog::ECDYesPressed: {
        if (me->mCallback != NULL) {
            me->mCallback(me->mCallbackObject, mLatitude, mLongitude, mTimestamp ? LOCATIONS_ERROR_NONE : LOCATIONS_ERROR_NOT_SUPPORTED);
        }
        me->HidePopUp();
        break;
    }

    case ConfirmationDialog::ECDNoPressed: {
        if (me->mManagerState == LOCATIONS_ERROR_GPS_SETTING_OFF) {
            app_control_h app_control;

            int ret = app_control_create(&app_control);
            if (ret != APP_CONTROL_ERROR_NONE) {
                Log::debug("app_control_create() failed.");
                return;
            }

            ret = app_control_set_app_id(app_control, "org.tizen.setting-location");
            ret = app_control_set_operation(app_control, APP_CONTROL_OPERATION_VIEW);

            if (ret != APP_CONTROL_ERROR_NONE) {
                Log::debug("app_control_set_operation() failed.");
                app_control_destroy(app_control);
                return;
            }

            if (app_control_send_launch_request(app_control, NULL, NULL) == APP_CONTROL_ERROR_NONE) {
                Log::debug("Succeeded to launch location settings.");
            } else {
                Log::debug("Failed to launch location settings");
            }

            app_control_destroy(app_control);
        } else {
            me->HidePopUp();
        }
        break;
    }

    case ConfirmationDialog::ECDDismiss:
        me->HidePopUp();
        break;

    default:
        me->HidePopUp();
        break;
    }
}

bool LocationService::HidePopUp(){
    bool ret = static_cast<NearByPlacesComposer*>(mCallbackObject)->mConfirmationDialog;
    if (ret) {
        delete static_cast<NearByPlacesComposer*>(mCallbackObject)->mConfirmationDialog;
        static_cast<NearByPlacesComposer*>(mCallbackObject)->mConfirmationDialog = nullptr;
    }
    return ret;
}

bool LocationService::is_latest_location() {
    if (mTimestamp == 0)
        return false;

    long cur_time, last_time, diff;

    cur_time = static_cast<long>(Utils::GetUnixTime_inSec());
    last_time = static_cast<long>(mTimestamp);
    diff = cur_time - last_time;

    Log::debug( "LocationService :: DIFF TIME = %ld", diff);

    if (diff < TEN_MINUTES)
        return true;
    else
        return false;
}

const char * LocationService::get_distance_in_str(double dest_lat, double dest_long) {
    double distance;

    int ret = location_manager_get_distance(mLatitude, mLongitude, dest_lat, dest_long, &distance);

    if (ret != LOCATIONS_ERROR_NONE)
        return NULL;
    else {
        char *dist_str = new char[20];

        if (distance < 1000) {
            Utils::Snprintf_s(dist_str, 20, "%d m", static_cast<int>(distance));
        } else {
            Utils::Snprintf_s(dist_str, 20, "%.1lf km", distance /= 1000);
        }
        return dist_str;
    }
}
