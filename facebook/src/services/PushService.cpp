#include "Common.h"
#include "Config.h"
#include "CSmartPtr.h"
#include "curlutilities.h"
#include "jsonutilities.h"
#include "Log.h"
#include "ParserWrapper.h"
#include "PushService.h"
#include "TrayNotificationsManager.h"
#include "Utils.h"

#include <openssl/sha.h>
#include <stdlib.h>
#include <string.h>

//Hash Defs for Registering with FB server
#define PUSH_HASH_KEY "existing_push_reg_id"

#define PUSH_TOKEN "0201ea52bd891fc16e1db78eb32f537e8f6b3f36e33d2780d28a22e12614193a024dc7ffba86817c6bc46d98868fe6620513"
#define PUSH_APP_ID "b17700c2a232dc8f"  //OFFICIAL APPID
#define DEVICE_ID1 "TizenZ1"
#define LOG_PREFIX  "fPushService:"

#define URL "https://api.facebook.com/method/user.registerPushCallback"
#define FORMAT "format=JSON"
#define RETURN_ST "return_structure=1"
#define ACCESS_TOKEN "access_token="
#define IS_INITIAL_REG "true"
#define USER_AGENT "Mozilla/5.0 (Samsung; U; CPU Tizen OS 5_1 like Mac OS X; en_EN) [FBAN/Facebook;FBAV/1.0;FBBV/1700;FBDV/SamsungZ1,1;FBMF/Samsung;FBSN/Tizen OS;FBSV/2.3; FBCR/SFR;FBID/phone;FBLC/en_EN]"

#define MAX_REQUEST_LEN 2048

//Flag to be used to send Deregister before Registering. This would ensure that whenever the FB APP starts, the Tizen
//Push Server and application have refereshed Registeration ID.
PushService::Toggle_Modes PushService::toggle = Off;
//Variable to save the Push Connection Details. This shall contain the information required to communicate with
//Tizen Push service
push_service_connection_h PushService::pushConn = NULL;

static void _dereg_result_cb(push_service_result_e result, const char *msg, void *user_data)
{

   if (result == PUSH_SERVICE_RESULT_SUCCESS) {
       Log::debug(LOG_PREFIX "De-Registration success [%s]" , msg);
   } else {
       Log::debug(LOG_PREFIX "De-Registration ERROR=%d [%s]", result, msg);
   }

}

PushService::PushService()
{
    //Constructor to initialize values, This shall be called when Notification services are enabled.
    Log::info(LOG_PREFIX "Push service created");
}

//The following method is used to Connect to the Tizen Push Platform service. This is a Tizen Platform service
//which connects further to the Tizen Push server.
void  PushService::connect()
{
   // Connect to the push service when the application is launched
   int ret = push_service_connect(PUSH_APP_ID, _state_cb, _noti_cb, this, &PushService::pushConn);
   if (ret != PUSH_SERVICE_ERROR_NONE) {
       Log::error(LOG_PREFIX "PushService::connect() = Failed");
   } else {
       Log::info(LOG_PREFIX "PushService::connect() = Success");
   }
}

//The Following Method is used to Disconnect from the Push Notification service. This would be called when
//the user does not want to receive any notification. The destructor for the PushService would be typically
//called after this.
void PushService::disconnect()
{
    push_service_disconnect(PushService::pushConn);
    //Reset the connection details
    PushService::pushConn = NULL;
    Log::info(LOG_PREFIX "PushService::disconnect() = Completed.");
}

//The Following function is call back which is triggered when the Registration request is approved.
//This callback is set in push_service_register. This shall not be called when there is failure in even
//calling the registration funtion.
static void _reg_result_cb(push_service_result_e result, const char *msg, void *user_data)
{
   if (result == PUSH_SERVICE_RESULT_SUCCESS) {
       Log::info(LOG_PREFIX "Registration request is approved.");
   } else {
       Log::error(LOG_PREFIX "Registration ERROR [%s]", msg);
   }
}

//The following function is a call back which is triggered when the user is in unregistered state
// In this case the mobile needs to register to the Tizen Notification service. Only after a successful
//Registration would it receive notifications.
static void _on_state_unregistered(void *user_data)
{
    Log::info(LOG_PREFIX "Push Service STATE UNREGISTERED");
    // Send a registration request to the push service
    int ret = push_service_register(PushService::pushConn, _reg_result_cb, NULL);
    if (ret == PUSH_SERVICE_ERROR_NONE) {
        Log::info(LOG_PREFIX "push_service_register() returned SUCCESS");
    } else {
        Log::error(LOG_PREFIX "push_service_register() returned error=%d" , ret);
    }
}

//The following method shall be updated to use the standard Graph request classes. Currently this is using directly
//the implementation to send request.
Ecore_Thread * PushService::ExecuteAsync(const char *reg_id)
{
    //build request
    char requestBuf[MAX_REQUEST_LEN];
    char sigBuf[MAX_REQUEST_LEN];
    char paramBuf[MAX_REQUEST_LEN];

    memset(requestBuf, 0, sizeof(requestBuf));
    memset(sigBuf, 0, sizeof(sigBuf));
    memset(paramBuf, 0, sizeof(paramBuf));

    //Format the Request in 2 steps.
    Utils::Snprintf_s(paramBuf, MAX_REQUEST_LEN, "protocol_params={\"device_id\":\"%s\",\"is_initial_reg\":%s,\"token\":\"%s\"}",DEVICE_ID1,IS_INITIAL_REG,reg_id);

    Utils::Snprintf_s(requestBuf, MAX_REQUEST_LEN, "%s&%s&%s%s&%s", FORMAT, RETURN_ST, ACCESS_TOKEN,  Config::GetInstance().GetAccessToken().c_str(), paramBuf);
    Log::debug(LOG_PREFIX "Post Data =%s", requestBuf);

    //Initialize the data structure to send the request.
    RequestUserData *ud = new RequestUserData(URL, requestBuf, response_cb, NULL);

    //Run a thread
    Ecore_Thread * thread = ecore_thread_run(thread_start_cb, thread_end_cb,
            thread_cancel_cb, ud);
    return thread;
}

//The following method is a place holder to check if there is a requirement to send the registration request
//if necessary. The comparison would be between the saved Hashes of the 2 registration ids.
void PushService::send_reg_id_if_necessary(const char *reg_id)
{
   unsigned char md[SHA_DIGEST_LENGTH];
   char hash_string[2*SHA_DIGEST_LENGTH+1];
   char *buf_ptr = hash_string;
   int i;

   // Generate a hash string from reg_id
   SHA1((unsigned char *)reg_id, sizeof(reg_id), md);

   // Convert byte array to hex string
   for (i = 0; i < SHA_DIGEST_LENGTH; i++) {
      buf_ptr += Utils::Snprintf_s(buf_ptr, 2*SHA_DIGEST_LENGTH+1, "%02X", md[i]);
   }
   hash_string[2*SHA_DIGEST_LENGTH] = '\0';
   ExecuteAsync(reg_id);
}

//The following function is a call back which is called if the user is registered succesfully for Notification services.
//There may be few pending notifications as well, which need to be locally read when the user enters this state.
void PushService::_on_state_registered(void *user_data)
{
    PushService *self = static_cast<PushService *>(user_data);
    Log::info(LOG_PREFIX "Push Service STATE REGISTERED");
    int ret;
    char *reg_id = NULL;

    //Check to see any unread notification
    ret = push_service_request_unread_notification(PushService::pushConn);
    if (ret == PUSH_SERVICE_ERROR_NONE) {
        Log::info(LOG_PREFIX "SUCCESS: push_service_request_unread_notification()");
    } else {
        Log::error(LOG_PREFIX "ERROR [%d]: push_service_request_unread_notification()", ret);
    }

    // Get the registration ID
    ret = push_service_get_registration_id(PushService::pushConn, &reg_id);
    if (ret == PUSH_SERVICE_ERROR_NONE) {
       Log::info(LOG_PREFIX "SUCCESS: push_service_get_registration_id() = %s", reg_id);
       // Send reg_id to your application server if necessary, NOT SURE WHY IS THIS BEING CALL - TO CHECK
       self->send_reg_id_if_necessary(reg_id);

       //Free the reg_id if retrieved.
       free(reg_id);
    } else {
        Log::error(LOG_PREFIX "ERROR [%d]: push_service_get_registration_id()", ret);
    }
}


static void _on_state_error(const char *err , void *user_data)
{
    Log::error(LOG_PREFIX "Push Service STATE ERROR");
}


//The following function is triggered whenever the state of the registeration changes.
void PushService::_state_cb(push_service_state_e state, const char *err, void *user_data)
{
    switch (state) {
    case PUSH_SERVICE_STATE_UNREGISTERED:
        Log::info(LOG_PREFIX "Arrived at STATE_UNREGISTERED");
        //The following function shall send the request for registration.
        _on_state_unregistered(user_data);
        //Since we do not want to de register again.
        PushService::toggle = On;
        break;

    case PUSH_SERVICE_STATE_REGISTERED:
        if (PushService::toggle == Off) {
            //Do an Explicit De-registration. This would typically happen when the application is restarted. We also need to handle the case when a new user signs up
            //Need to check if this is really required. Registration id's may be stored by the platform service
            //in persistent storage. In that case this may not be required. In testing we have seen that at times
            // we do not receive notification even though the application on starting comes into Registered state which
            //seems to indicate that the reg id is stored locally in persistent DB by platform service.
            int ret = push_service_deregister(PushService::pushConn, _dereg_result_cb, user_data);
            Log::info(LOG_PREFIX "Sent  De-register [%d]", ret);
            PushService::toggle = On;
        } else {
            Log::info(LOG_PREFIX "Arrived at STATE_REGISTERED");
            _on_state_registered(user_data);
            PushService::toggle = Off;
        }
        break;

    case PUSH_SERVICE_STATE_ERROR:
        Log::error(LOG_PREFIX "Arrived at STATE_ERROR");
        _on_state_error(err, user_data);
        break;
    default:
        Log::warning(LOG_PREFIX "Facebook_Notification", "Unknown State");
        break;
    }
}


//The following method is for receiving the notifications from the Tizen Push server. This method would
//receive the individual notifcation and store them in a list. This list shall be displayed in the notification screen.
void PushService::_noti_cb(push_service_notification_h noti, void *user_data)
{
    Log::info(LOG_PREFIX "PushService::_noti_cb(): Notification Received from Tizen Server for user");

    // The notification shall contain fields like, appdata, message, sender details, time details etc. Parse these fields.
    // Retrieve app data from notification. AppData shall contain the JSON sent by the FB server.
    char *data = NULL;  // App data loaded on the notification
    if (push_service_get_notification_data(noti, &data) == PUSH_SERVICE_ERROR_NONE) {
        Log::debug(LOG_PREFIX "Notification AppData = %s", data);
    }

    char *msg = NULL;  // Noti message
    if (push_service_get_notification_message(noti, &msg) == PUSH_SERVICE_ERROR_NONE) {
        Log::debug(LOG_PREFIX "Notification Message = %s", msg);
    }
    free(msg);

    long long int time_stamp;  // Time when the noti is generated
    if (push_service_get_notification_time(noti, &time_stamp) == PUSH_SERVICE_ERROR_NONE) {
        Log::debug(LOG_PREFIX "Notification Time = %lld", time_stamp);
    }

    JsonParser * parser = openJsonParser(data, NULL);
    ParserWrapper autoDestroyer(parser); //This will destroy the JSON parser object, so need to call g_object_unref() explicitly
    JsonNode* rootNode = parser ? json_parser_get_root(parser) : NULL;
    JsonObject *rootobj = rootNode ? json_node_get_object(rootNode) : NULL;
    if (!rootobj) {
        Log::warning(LOG_PREFIX "PushService::_noti_cb() : Invalid Notification received, hence returning");
        free(data);
        return;
    }

    //Extract the Alert Message from Push Notification JSON data
    JsonObject * apsParam = json_object_get_object_member(rootobj, "aps");
    c_unique_ptr<char> alertStr(GetStringFromJson(apsParam, "alert"));
    if (!apsParam || !alertStr) {
        Log::warning(LOG_PREFIX "No APS Parameter or No Alert Message Present in Json Data, hence returning");
        free(data);
        return;
    }

    //Check whether the push-noti is addressed to the Messenger application:
    bool isMessengerNoti = false;
    JsonObject *param = json_object_get_object_member(rootobj, "p");
    if (param) {
        c_unique_ptr<char> paramNtfType(GetStringFromJson(param, "t"));
        if (paramNtfType && strlen(paramNtfType.get()) > 1) {
            c_unique_ptr<char> paramAuthorId(GetStringFromJson(param, "a"));
            c_unique_ptr<char> paramObjectId(GetStringFromJson(param, "o"));
            c_unique_ptr<char> paramSessionId(GetStringFromJson(param, "s"));
            c_unique_ptr<char> paramUserId(GetStringFromJson(param, "u"));
            if (paramAuthorId && paramObjectId && paramSessionId && paramUserId) {
                isMessengerNoti = true;
            }
        }
    }

    if (!isMessengerNoti) {
        AppEvents::Get().Notify(eNOTIFICATION_REDRAW);
    }

    //Show the Message in the Quick Notification Panel & Increment the Badge count, if required.
    app_control_h app_control = NULL;
    int appControlRetVal = app_control_create(&app_control);
    if ((APP_CONTROL_ERROR_NONE == appControlRetVal) && app_control) {
        appControlRetVal = app_control_set_app_id(app_control, PACKAGE);
        appControlRetVal = app_control_set_operation(app_control, APP_CONTROL_CUSTOM_OPERATION_NOTIF_RCVD_TO_PROCESS);
        if (APP_CONTROL_ERROR_NONE == appControlRetVal) {
            appControlRetVal = app_control_add_extra_data(app_control, PUSH_NOTIF_JSON_DATA, SAFE_STRDUP(data)); // This json data is being freed in app_control().
            Log::debug(LOG_PREFIX "app_control_add_extra_data() %s Notification data.", (APP_CONTROL_ERROR_NONE == appControlRetVal) ? "successfully added" : "failed to add");
        }
    }
    free(data);

    std::string input(alertStr.get());
    Log::debug(LOG_PREFIX "PushService::_noti_cb(), before replacing escape sequence char if any, the notif msg: %s", input.c_str());
    //Replace special characters:
    if (input.find("%") != std::string::npos) {
        input = Utils::ReplaceString(input, "%2F", "/");
        input = Utils::ReplaceString(input, "%3A", ":");
        input = Utils::ReplaceString(input, "%3D", "=");
        input = Utils::ReplaceString(input, "%3F", "?");
        input = Utils::ReplaceString(input, "%2C", ",");
        input = Utils::ReplaceString(input, "%26", "&");
        input = Utils::ReplaceString(input, "%23", "#");
    }

    TrayNotificationsManager::GetInstance()->CreateNotiNotification(input.c_str(),app_control);
    app_control_destroy(app_control);
}

void PushService::thread_start_cb(void *data, Ecore_Thread *thread) {
    RequestUserData *ud = static_cast<RequestUserData *> (data);
    CURLcode code;

    Log::debug(LOG_PREFIX "THREAD RUN CB with %s", ud->mRequestBuffer->memory);
    code = SendHttpPost(ud);
    ud->curlCode = code;
}

/**
 * @brief - This callback is called when async thread is finished
 * @param data[in] -user data
 * @param thread[in] - thread
 */
void PushService::thread_end_cb(void *data, Ecore_Thread *thread) {
    //Since thread_end_cb() is called from main loop thread,
    //we can access EAPI without considering any syncronization.
    RequestUserData *ud = static_cast<RequestUserData *>(data);

    int curlCode = ud->curlCode;
    char * respond = ud->mResponseBuffer->memory;
    void * object = ud->callbackObject;
    RequestUserData_CB callback = ud->callback;

    delete ud;

    if (callback != NULL) {
        callback(object, respond, curlCode);
    }
    Log::info(LOG_PREFIX "THREAD END CB");
}

/**
 * @brief This callback is called when async thread should be cancelled
 * @param data[in] - user data
 * @param thread[in] - thread
 */
void PushService::thread_cancel_cb(void *data, Ecore_Thread *thread)
{
    //TODO:
}

void PushService::response_cb(void* object, char* respond, int code)
{
    Log::debug(LOG_PREFIX "THREAD RESPONSE CB = %s", respond);

    //TODO:
}
