#include "Application.h"
#include "Common.h"
#include "Config.h"
#include "ConnectivityManager.h"
#include "ErrorHandling.h"
#include "EventBatchProvider.h"
#include "GroupProvider.h"
#include "i18n.h"
#include "Log.h"
#include "OwnFriendsProvider.h"
#include "OwnProfileScreen.h"
#include "ProfileScreen.h"
#include "Utils.h"
#include "WidgetFactory.h"

#include <app.h>
#include <app_common.h>
#include <assert.h>
#include <curl/curl.h>
#include <curl/easy.h>
#include <ftw.h>
#include <locale>
#include <notification.h>
#include <openssl/md5.h>
#include <openssl/sha.h>
#include <runtime_info.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <system_info.h>
#include <system_settings.h>
#include <telephony.h>
#include <time.h>

#define MILLISECONDS_IN_DAY 86400000
#define MILLISECONDS_IN_HOUR 3600000
#define MILLISECONDS_IN_MINUTE 60000
#define FB_I18N_UCHAR_BUF_LENGTH 1024

static const long long MICRO_SECS_PREFIX = 1000000;

#define UTF8_CHAR_LEN( byte ) (( 0xE5000000 >> (( byte >> 3 ) & 0x1e )) & 3 ) + 1

static regmatch_t regmatch[REGMATCH_ARRAY_SIZE];

regmatch_t * Utils::GetRegmatch() { return regmatch; }

double Utils::GetCurrentTime()
{
    struct timeval tp;
    gettimeofday(&tp, NULL);
    return (double) tp.tv_sec * 1000L + tp.tv_usec / 1000;
}

long long Utils::GetCurrentTimeMicroSecs()
{
    struct timeval tp;
    gettimeofday( &tp, NULL );
    return tp.tv_sec * MICRO_SECS_PREFIX + tp.tv_usec;
}

const char* Utils::GetUnixTime()
{
    time_t timer;
    if ( time( &timer ) == -1 ) {
        return NULL;
    }
    char time[16] = {0};
    Utils::Snprintf_s( time, 16, "%ld", timer );
    return strdup( time );
}

time_t Utils::GetUnixTime_inSec()
{
    time_t timer;
    if ( time( &timer ) == -1 ) {
        return 0;
    }
    return timer;
}

char * Utils::PresentTimeFromMilliseconds(double postMillisec)
{
    Log::info("Utils: PresentTimeFromMilliseconds");

    double currentTime = GetCurrentTime();

    double passedTime = currentTime - postMillisec;

    if (passedTime < MILLISECONDS_IN_MINUTE) {
        return strdup("IDS_JUST_NOW");
    }
    else if (passedTime < MILLISECONDS_IN_HOUR) {
        int passedMinutes = (int) (passedTime / MILLISECONDS_IN_MINUTE);
        if (passedMinutes == 1) {
            return strdup("IDS_1_MIN");
        }
        else {
            char result[20];
            Utils::Snprintf_s(result, 20, "%d IDS_MINS", passedMinutes);
            return strdup(result);
        }
    }
    else if (passedTime < MILLISECONDS_IN_DAY) {
        int passedMinutes = (int) (passedTime / MILLISECONDS_IN_HOUR);
        if (passedMinutes == 1) {
            return strdup("IDS_1_HOUR");
        }
        else {
            char result[20];
            Utils::Snprintf_s(result, 20, "%d IDS_HOURS", passedMinutes);
            return strdup(result);
        }
    }

    return PresentTimeFromMillisecondsGeneral(postMillisec, true);
}

char * Utils::PresentTimeFromMillisecondsGeneral(double postMillisec, bool needTimeShift) {
    Log::debug("PresentTimeFromMillisecondsGeneral");
    char *localTimeZone = NULL;
    static const char *timeZoneGMT = "GMT";
    if (needTimeShift) {
        system_settings_get_value_string(SYSTEM_SETTINGS_KEY_LOCALE_TIMEZONE, &localTimeZone);
    }

    char customFormat[64] = {0};

    if (IsTimeFormat24Hour()) {
        Utils::Snprintf_s(customFormat, 64, "d MMM, yyyy '%s' HH:mm", i18n_get_text("IDS_AT_T"));
    } else {
        Utils::Snprintf_s(customFormat, 64, "d MMM, yyyy '%s' h:mm a", i18n_get_text("IDS_AT_T"));
    }
    i18n_uchar uchCustomFormat[64];
    i18n_ustring_copy_ua(uchCustomFormat, customFormat);
    int formatLength = i18n_ustring_get_length(uchCustomFormat);

    i18n_uchar ucharTimeZoneId[64];
    if (needTimeShift) {
        i18n_ustring_copy_ua(ucharTimeZoneId, localTimeZone);
    } else {
        i18n_ustring_copy_ua(ucharTimeZoneId, timeZoneGMT);
    }
    int timeZoneLength = i18n_ustring_get_length(ucharTimeZoneId);

    if (localTimeZone) {
        free(localTimeZone);
    }

    const char *locale = NULL;
    i18n_ulocale_get_default(&locale);

    i18n_udate_format_h formatter = NULL;
    int ret = i18n_udate_create(I18N_UDATE_PATTERN, I18N_UDATE_PATTERN, locale, ucharTimeZoneId, timeZoneLength, uchCustomFormat, formatLength,
            &formatter);
    if (ret != I18N_ERROR_NONE) {
        Log::debug("i18n_udate_create is failed with error:%d", ret);
    }
    if (!formatter) {
        Log::debug("formatter is NULL");
    }

    // formats a date using i18n_udate_format
    i18n_uchar formatted[64] = { 0, };
    int formattedLength;
    char result[64] = { 0, };

    i18n_udate_format_date(formatter, (i18n_udate) postMillisec, formatted, 64, NULL, &formattedLength);
    i18n_ustring_copy_au_n(result, formatted, 64);
    std::string resultString = ReplaceString(result, "a.m.", "am");
    resultString = ReplaceString(resultString, "p.m.", "pm");

    Log::debug("Presented date : %s", resultString.c_str());
    i18n_udate_destroy(formatter);

    return strdup(resultString.c_str());
}

char * Utils::PresentTimeFromMillisecondsFullFormat(double postMillisec, bool needTimeShift)
{
    char *localTimeZone = NULL;
    static const char *timeZoneGMT = "GMT";
    if (needTimeShift) {
        system_settings_get_value_string(SYSTEM_SETTINGS_KEY_LOCALE_TIMEZONE, &localTimeZone);
    }
    const char *customFormat = "YYYY-MM-dd'T'HH:mm:ss";
    i18n_uchar uchCustomFormat[64];
    i18n_ustring_copy_ua(uchCustomFormat, customFormat);
    int formatLength = i18n_ustring_get_length(uchCustomFormat);

    i18n_uchar ucharTimeZoneId[64];
    if (needTimeShift) {
        i18n_ustring_copy_ua(ucharTimeZoneId, localTimeZone);
    } else {
        i18n_ustring_copy_ua(ucharTimeZoneId, timeZoneGMT);
    }
    int timeZoneLength = i18n_ustring_get_length(ucharTimeZoneId);

    if (localTimeZone) {
        free(localTimeZone);
    }

    const char *locale = NULL;
    i18n_ulocale_get_default(&locale);

    i18n_udate_format_h formatter = NULL;
    int ret = i18n_udate_create(I18N_UDATE_PATTERN, I18N_UDATE_PATTERN, locale, ucharTimeZoneId, timeZoneLength,
            uchCustomFormat, formatLength, &formatter);
    if (ret != I18N_ERROR_NONE) {
        Log::error("Utils::PresentTimeFromMillisecondsFullFormat: i18n_udate_create is failed with error: %d", ret);
    }
    if (!formatter) {
        Log::error("Utils::PresentTimeFromMillisecondsFullFormat: formatter is NULL");
    }

    // formats a date using i18n_udate_format
    i18n_uchar formatted[64] = {0};
    int formattedLength;
    char result[64] = {0};

    i18n_udate_format_date(formatter, (i18n_udate) postMillisec, formatted, 64, NULL, &formattedLength);
    i18n_ustring_copy_au_n(result, formatted, 64);

    Log::debug("##### Presented date : %s", result);
    i18n_udate_destroy(formatter);

    return strdup(result);
}

struct tm Utils::GetCurrentDateTime() {
    time_t local_time = time(NULL);
    struct tm *time_info = localtime(&local_time);
    return *time_info;
}

char * Utils::GetTurningInfo(const char * birthday)
{
    // If birthday string doesn't have a 2 pieces of '/' it means that string without a year information.
    if (GetSymbolCount('/', birthday) != 2) {
        return NULL;
    }

    struct tm currentTime = Utils::GetCurrentDateTime();

    struct tm btm;
    strptime(birthday, "%m/%d/%Y", &btm);

    char result[64] = {0};

    // Calculate Turning
    Utils::Snprintf_s(result, 64, i18n_get_text("IDS_EVENTS_BIRTHDAYS_YEARS"), (btm.tm_mon > currentTime.tm_mon) ?
            (currentTime.tm_year - btm.tm_year): (currentTime.tm_year - btm.tm_year) + 1);

    return strdup(result);
}

char * Utils::GetNearestEventInfo(const char * event, bool isBirthday)
{
    struct tm currentTime = Utils::GetCurrentDateTime();

    struct tm etm;
    strptime(event, isBirthday ? "%m/%d/%Y" : "%Y-%m-%dT%H:%M:%S", &etm);

    char result[64] = {0};

    const char * day = NULL;

    if (currentTime.tm_year == etm.tm_year && currentTime.tm_yday == etm.tm_yday) {
        day = "IDS_EVENT_TODAY_TEXT";
    } else if (currentTime.tm_year == etm.tm_year && currentTime.tm_yday == etm.tm_yday - 1) {
        day = "IDS_EVENT_TOMORROW_TEXT";
    } else {
        day = GetEventWeekDay(etm.tm_wday, false);
    }

    char * eventTime = GetEventTime(etm);

    if (!strcmp(day, "IDS_EVENT_TODAY_TEXT") || !strcmp(day, "IDS_EVENT_TOMORROW_TEXT")) {
        Utils::Snprintf_s(result, 64, "%s %s %s", i18n_get_text(day), i18n_get_text("IDS_AT"), eventTime);
    } else if (isBirthday) {
        Utils::Snprintf_s(result, 64, "%s, %s %d", i18n_get_text(day), GetEventMonth(etm.tm_mon), etm.tm_mday);
    } else {
        Utils::Snprintf_s(result, 64, "%s, %s %d %s %s", i18n_get_text(day), GetEventMonth(etm.tm_mon), etm.tm_mday, i18n_get_text("IDS_AT"), eventTime);
    }

    if (eventTime) {
        free((void*)eventTime);
        eventTime = NULL;
    }

    return strdup(result);
}

char * Utils::GetNearestBdayInfo(const char * event, bool isFutureYear)
{
    struct tm currentTime = Utils::GetCurrentDateTime();

    int year = 1900;

    if (isFutureYear) {
        year = 1901;
    }

    struct tm etm;
    strptime(event, "%m/%d/%Y", &etm);

    char result[64] = {0};

    const char * day = NULL;

    if (currentTime.tm_mon == etm.tm_mon && currentTime.tm_mday == etm.tm_mday) {
        day = "IDS_EVENT_TODAY_TEXT";
    } else if (currentTime.tm_mon == etm.tm_mon && currentTime.tm_mday == etm.tm_mday - 1) {
        day = "IDS_EVENT_TOMORROW_TEXT";
    } else {
        day = GetEventWeekDay(WeekDayForDate(etm.tm_mday, etm.tm_mon, currentTime.tm_year), false);
    }

    if (!strcmp(day, "IDS_EVENT_TODAY_TEXT") || !strcmp(day, "IDS_EVENT_TOMORROW_TEXT")) {
        Utils::Snprintf_s(result, 64, "%s", i18n_get_text(day));
    } else {
        Utils::Snprintf_s(result, 64, "%s, %s %d, %d", i18n_get_text(day), GetEventMonth(etm.tm_mon), etm.tm_mday, currentTime.tm_year+year);
    }

    return strdup(result);
}

int Utils::WeekDayForDate(int day, int month, int year)
{
    tm timeStruct = {};
    timeStruct.tm_year = year;
    timeStruct.tm_mon = month;
    timeStruct.tm_mday = day;
    mktime(&timeStruct);
    return timeStruct.tm_wday;
}

bool Utils::IsSameWeek(const tm *firstTime, const tm *secondTime) {
    tm lFirstTime = {0};
    lFirstTime.tm_year = firstTime->tm_year;
    lFirstTime.tm_mon = firstTime->tm_mon;
    lFirstTime.tm_mday = firstTime->tm_mday;

    time_t firstDayOfWeekT = mktime(&lFirstTime) - firstTime->tm_wday*24*3600;
    time_t lastDayOfWeekT = mktime(&lFirstTime) + (6 - firstTime->tm_wday)*24*3600;

    tm lSecondTime = {0};
    lSecondTime.tm_year = secondTime->tm_year;
    lSecondTime.tm_mon = secondTime->tm_mon;
    lSecondTime.tm_mday = secondTime->tm_mday;

    time_t secondTimeT = mktime(&lSecondTime);
    return (firstDayOfWeekT <= secondTimeT && secondTimeT <= lastDayOfWeekT);
}

long int Utils::DiffInDays(const tm *firstTime, const tm *secondTime) {
    tm lFirstTime = {0};
    lFirstTime.tm_year = firstTime->tm_year;
    lFirstTime.tm_mon = firstTime->tm_mon;
    lFirstTime.tm_mday = firstTime->tm_mday;

    time_t firstTimeT = mktime(&lFirstTime);

    tm lSecondTime = {0};
    lSecondTime.tm_year = secondTime->tm_year;
    lSecondTime.tm_mon = secondTime->tm_mon;
    lSecondTime.tm_mday = secondTime->tm_mday;

    time_t secondTimeT = mktime(&lSecondTime);
    return ((secondTimeT - firstTimeT) / 86400); //secs in day (60*60*24)
}


char * Utils::GetEventDay(const struct tm eventTime, bool showDay)
{
    //AAA ToDo: Investigate: we are forced to make this stack copy of eventTime because otherwise crash occurs.
    //Further investigation is required. Most probably root cause is localtime() finction call inside Utils::GetCurrentDateTime().
    struct tm currentTime = Utils::GetCurrentDateTime();

    char result[64] = { 0 };
    int diffInDays = DiffInDays(&currentTime, &eventTime);
    if (showDay && diffInDays >= 0 && diffInDays < 7) {
        const char *day = NULL;

        if (diffInDays == 0) {
            day = "IDS_EVENT_TODAY_TEXT";
        } else if (diffInDays == 1) {
            day = "IDS_EVENT_TOMORROW_TEXT";
        } else {
            day = GetEventWeekDay(eventTime.tm_wday, true);
        }
        if (day) {
            eina_strlcpy(result, i18n_get_text(day), 64);
        }
    } else {
        Utils::Snprintf_s(result, 64, "%s %d, %d", GetEventMonth(eventTime.tm_mon), eventTime.tm_mday, 1900 + eventTime.tm_year);
    }

    return strdup(result);
}

char * Utils::GetEventTime(const struct tm eventTime) {
//AAA ToDo: Investigate: we are forced to make this stack copy of eventTime because otherwise crash occurs.
//Further investigation is required. Most probably root cause is localtime() finction call inside Utils::GetCurrentDateTime().
    char buff_date[16] = {0};
    const char* customFormat = NULL;
    if (IsTimeFormat24Hour())
    {
        customFormat = "%H:%M";
    }
    else
    {
        customFormat = "%I:%M %p";
    }

    strftime(buff_date, 16, customFormat, &eventTime);

    return strdup(buff_date);
}

const char * Utils::GetEventWeekDay(unsigned int day, bool isFullFormat)
{
    const char * result = NULL;

    switch(day) {
        case ESunday:
            result = isFullFormat ? "IDS_EVENT_SUNDAY_TEXT" : "IDS_EVENT_SUNDAY_SHORT";
            break;
        case EMonday:
            result = isFullFormat ? "IDS_EVENT_MONDAY_TEXT" : "IDS_EVENT_MONDAY_SHORT";
            break;
        case ETuesday:
            result = isFullFormat ? "IDS_EVENT_TUESTDAY_TEXT" : "IDS_EVENT_TUESTDAY_SHORT";
            break;
        case EWednesday:
            result = isFullFormat ? "IDS_EVENT_WEDNESDAY_TEXT" : "IDS_EVENT_WEDNESDAY_SHORT";
            break;
        case EThursday:
            result = isFullFormat ? "IDS_EVENT_THURSDAY_TEXT" : "IDS_EVENT_THURSDAY_SHORT";
            break;
        case EFriday:
            result = isFullFormat ? "IDS_EVENT_FRIDAY_TEXT" : "IDS_EVENT_FRIDAY_SHORT";
            break;
        case ESaturday:
            result = isFullFormat ? "IDS_EVENT_SATURDAY_TEXT" : "IDS_EVENT_SATURDAY_SHORT";
            break;
        default:
            break;
    }

    return result;
}

char * Utils::GetEventMonth(unsigned int month)
{
    const char * result = NULL;
    switch(month) {
        case EJanuary:
            result = "IDS_EVENTS_BIRTHDAYS_JAN";
            break;
        case EFebruary:
            result = "IDS_EVENTS_BIRTHDAYS_FEB";
            break;
        case EMarch:
            result = "IDS_EVENTS_BIRTHDAYS_MAR";
            break;
        case EApril:
            result = "IDS_EVENTS_BIRTHDAYS_APR";
            break;
        case EMay:
            result = "IDS_EVENTS_BIRTHDAYS_MAY";
            break;
        case EJune:
            result = "IDS_EVENTS_BIRTHDAYS_JUNE";
            break;
        case EJuly:
            result = "IDS_EVENTS_BIRTHDAYS_JULY";
            break;
        case EAugust:
            result = "IDS_EVENTS_BIRTHDAYS_AUG";
            break;
        case ESeptember:
            result = "IDS_EVENTS_BIRTHDAYS_SEPT";
            break;
        case EOctober:
            result = "IDS_EVENTS_BIRTHDAYS_OCT";
            break;
        case ENovember:
            result = "IDS_EVENTS_BIRTHDAYS_NOV";
            break;
        case EDecember:
            result = "IDS_EVENTS_BIRTHDAYS_DEC";
            break;
        default:
            break;
    }

    return i18n_get_text(result);
}

const char * Utils::GetPickedTime(struct tm * currtime)
{
    char buff[64] = {0};
    strftime(buff, 64, "%Y-%m-%dT%H:%M:%S%z", currtime);

    return strdup(buff);
}

char * Utils::GetNearestBDay(const char * birthday)
{
    time_t local_time = time(NULL);
    struct tm *time_info = localtime(&local_time);
    struct tm currentTime;

    char buff_date[64] = {0};
    currentTime = *time_info;
    strftime(buff_date, 64, "%m/%d/%Y", &currentTime);

    struct tm btm;
    strptime(birthday, "%m/%d/%Y", &btm);

    char result[64] = {0};
    Utils::Snprintf_s(result, 64, "%d%d%d", (btm.tm_mon > currentTime.tm_mon) ?
            1900 + currentTime.tm_year : 1900 + currentTime.tm_year + 1, btm.tm_mon, btm.tm_mday);

    return strdup(result);
}

char * Utils::GetEventDurationTime(const char * eventStartTime, const char * eventEndTime,
                                   int * starts_in_hr)
{
    struct tm currentTime = Utils::GetCurrentDateTime();

    struct tm startEventTime;
    strptime(eventStartTime, "%Y-%m-%dT%H:%M:%S", &startEventTime);

    char result[64] = {0};

    const char * startDay = GetEventWeekDay(startEventTime.tm_wday, false);
    int starts_in = -1;

    if (eventEndTime) {
        struct tm endEventTime;
        strptime(eventEndTime, "%Y-%m-%dT%H:%M:%S", &endEventTime);

        Utils::Snprintf_s(result, 64, "%s, %s %d - %s, %s %d", i18n_get_text(startDay), GetEventMonth(startEventTime.tm_mon), startEventTime.tm_mday,
                        i18n_get_text(GetEventWeekDay(endEventTime.tm_wday, false)), GetEventMonth(endEventTime.tm_mon), endEventTime.tm_mday);
    } else {
        if (currentTime.tm_year == startEventTime.tm_year && currentTime.tm_yday == startEventTime.tm_yday) {
            startDay = "IDS_EVENT_TODAY_TEXT";
        } else if (currentTime.tm_year == startEventTime.tm_year && currentTime.tm_yday == startEventTime.tm_yday - 1) {
            startDay = "IDS_EVENT_TOMORROW_TEXT";
        }

        char * eventTime = GetEventTime(startEventTime);

        if (!strcmp(startDay, "IDS_EVENT_TODAY_TEXT") || !strcmp(startDay, "IDS_EVENT_TOMORROW_TEXT")) {
            Utils::Snprintf_s(result, 64, "%s %s %s", i18n_get_text(startDay), i18n_get_text("IDS_AT"), eventTime);
            starts_in = mktime(&startEventTime) - mktime(&currentTime);
            if (starts_in >= 0 && starts_in <= (12 * 3600)) {
                starts_in = (starts_in < 3600) ? 0 : (starts_in + 1800) / 3600;  //round till whole hours
            } else {
                starts_in = -1;
            }
        } else {
            Utils::Snprintf_s(result, 64, "%s, %s %d %s %s", i18n_get_text(startDay), GetEventMonth(startEventTime.tm_mon),
                                          startEventTime.tm_mday, i18n_get_text("IDS_AT"),
                                          eventTime);
        }

        if (eventTime) {
            free((void*)eventTime);
            eventTime = NULL;
        }
    }

    *starts_in_hr = starts_in;
    return strdup(result);
}

char * Utils::GetEventDetailedDurationTime(const char * eventStartTime, const char * eventEndTime) {
    struct tm startEventTime;
    strptime(eventStartTime, "%Y-%m-%dT%H:%M:%S", &startEventTime);


    char result[64] = {0};

    struct tm endEventTime;
    strptime(eventEndTime, "%Y-%m-%dT%H:%M:%S", &endEventTime);

    char * evStartTime = GetEventTime(startEventTime);
    char * evEndTime = GetEventTime(endEventTime);

    Utils::Snprintf_s(result, 64, "%s %d %s %s %s %s %d %s %s", GetEventMonth(startEventTime.tm_mon),
                                                  startEventTime.tm_mday,
                                                  strdup(i18n_get_text("IDS_AT")),
                                                  evStartTime,
                                                  strdup(i18n_get_text("IDS_EVENT_DURATION_SEPARATOR")),
                                                  GetEventMonth(endEventTime.tm_mon),
                                                  endEventTime.tm_mday,
                                                  strdup(i18n_get_text("IDS_AT")),
                                                  evEndTime);

    if (evStartTime) {
        free((void*)evStartTime);
        evStartTime = NULL;
    }

    if (evEndTime) {
        free((void*)evEndTime);
        evEndTime = NULL;
    }

    return strdup(result);
}

bool Utils::IsTimeFormat24Hour()
{
    bool isTimeFormat24Hour = false;
    system_settings_get_value_bool (SYSTEM_SETTINGS_KEY_LOCALE_TIMEFORMAT_24HOUR, &isTimeFormat24Hour);
    return isTimeFormat24Hour;
}

std::string Utils::ReplaceString(std::string str, const std::string& searchStr, const std::string& replaceStr, unsigned int pos, bool firstReplace)
{
    if(str.empty() || searchStr.empty()/* || replaceStr.empty()*/) {
        return "";
    }
    while((pos = str.find(searchStr, pos)) != std::string::npos) {
        str.replace(pos, searchStr.length(), replaceStr);
        pos += replaceStr.length();
        if(firstReplace) {
            break;
        }
    }
    return str;
}

std::vector<std::string> Utils::SplitString(const std::string& splittedString, const std::string& splitter) {
    std::vector<std::string> result;
    size_t previousPosition = 0;
    size_t currentPosition = 0;
    bool isFirstSearch = true;
    while(currentPosition != std::string::npos) {
        previousPosition = isFirstSearch ? currentPosition : currentPosition + splitter.length();
        currentPosition = splittedString.find(splitter, previousPosition);
        result.push_back(splittedString.substr(previousPosition, currentPosition == std::string::npos ? splittedString.length() : currentPosition - previousPosition));
        if(isFirstSearch) isFirstSearch = false;
    }
    return result;
}

std::string Utils::ReplaceQuotation(std::string str)
{
    static std::string searchStr = "'";
    static std::string replaceStr = "''";

    unsigned int pos = 0;
    while ((pos = str.find(searchStr, pos)) != std::string::npos) {
        str.replace(pos, searchStr.length(), replaceStr);
        pos += replaceStr.length();
    }
    return str;
}

double Utils::ParseTimeToMilliseconds(char *timeString)
{
    if (!timeString) {
        return 0.0;
    }
    static const char *timeZoneId = "GMT";
    i18n_uchar *ucharTimeZoneId;
    ucharTimeZoneId = (i18n_uchar*) calloc(strlen(timeZoneId) + 1,
            sizeof(i18n_uchar));
    if (ucharTimeZoneId == NULL) {
        return -1;
    }
    i18n_ustring_copy_ua(ucharTimeZoneId, timeZoneId);
    int timeZoneLen = i18n_ustring_get_length(ucharTimeZoneId);

    const char *locale = NULL;
    i18n_ulocale_get_default(&locale);

    i18n_ucalendar_h calendar;
    int ret = i18n_ucalendar_create(ucharTimeZoneId, timeZoneLen, locale,
            I18N_UCALENDAR_TRADITIONAL, &calendar);
    if (ret) {
        Log::error("Utils::ParseTimeToMilliseconds: i18n_ucalendar_create failed.");
        return 0;
    }

    if (timeString) {
        char *tempTimeString = strdup(timeString);
        const char *token = "-T:+";

        const char * tmp = strtok(tempTimeString, token);
        int year = tmp != NULL ? atoi(tmp) : 0;

        tmp = strtok(NULL, token);
        int month = tmp != NULL ? atoi(tmp) : 0;

        tmp = strtok(NULL, token);
        int day = tmp != NULL ? atoi(tmp) : 0;

        tmp = strtok(NULL, token);
        int hour = tmp != NULL ? atoi(tmp) : 0;

        tmp = strtok(NULL, token);
        int minute = tmp != NULL ? atoi(tmp) : 0;

        tmp = strtok(NULL, token);
        int second = tmp != NULL ? atoi(tmp) : 0;

        free(tempTimeString);
        i18n_ucalendar_set_date_time(calendar, year, month - 1, day, hour, minute,
                    second);
    }

    i18n_udate millisec;
    i18n_ucalendar_get_milliseconds(calendar, &millisec);

    i18n_ucalendar_destroy(calendar);
    free(ucharTimeZoneId);

    return (double) millisec;
}

char * Utils::GetStringOrNull(const char *string)
{
    if (!string || (*string == '\0') || !strcmp(string, "NULL")) {
        return NULL;
    }
    return strdup(string);
}


char* Utils::str2md5(const char *str, int length) {
    int n;
    MD5_CTX c;
    unsigned char digest[16];
    char *out = (char*)malloc(33);

    MD5_Init(&c);

    while (length > 0) {
        if (length > 512) {
            MD5_Update(&c, str, 512);
        } else {
            MD5_Update(&c, str, length);
        }
        length -= 512;
        str += 512;
    }

    MD5_Final(digest, &c);

    for (n = 0; n < 16; ++n) {
        Utils::Snprintf_s(&(out[n*2]), 16*2, "%02x", (unsigned int)digest[n]);
    }

    return out;
}

int Utils::FindTaggedFriendsInText(const char *text)
{
    Log::debug("Utils::FindTaggedFriendsInText");

    if (!text) {
        return 0;
    }

    static bool isCompiled = false;
    static regex_t regexFriendsTag;
    if (!isCompiled) {
        const char *regExpText = "\\@\\[[0-9]+:[0-9]+:";
        int ret = regcomp(&regexFriendsTag, regExpText, REG_EXTENDED | REG_ICASE);
        if (ret) {
            Log::error("Utils::FindTaggedFriendsInText: Could not compile Regular expression");
        }
        else {
            isCompiled = true;
        }
    }
    int ret = DoRegExp(&regexFriendsTag, text);

    return ret;
}

int Utils::FindHashTagsInText(const char *text)
{
    Log::debug("Utils::FindHashTagsInText");

    if (!text) {
        return 0;
    }

    char *utf8text = elm_entry_markup_to_utf8(text);
    static bool isCompiled = false;
    static regex_t regexHashTag;
    if (!isCompiled) {
        const char *regExpText = "\\B#\\w+";
        int ret = regcomp(&regexHashTag, regExpText, REG_EXTENDED | REG_ICASE);
        if (ret) {
            Log::debug("Utils::FindHashTagsInText: Could not compile Regular expression");
        }
        else {
            isCompiled = true;
        }
    }
    int ret = DoRegExp(&regexHashTag, utf8text);
    free(utf8text);
    return ret;
}

int Utils::FindUrlInText(const char *text)
{
    Log::debug("Utils::FindUrlInText");

    if (!text) {
        return 0;
    }

    char *utf8text = elm_entry_markup_to_utf8(text);
    static bool isCompiled = false;
    static regex_t regexUrl;
    if (!isCompiled) {
        const char *regExpText =
                "((((www)\\.|(http|https|news)+\\:\\/\\/)[_.a-z0-9-]+\\.[a-z0-9\\/_:@=.+?,##%\\&amp;~-]*[^.|\\'|\\# |!|\\(|?|,| |>|<|;|\\)])"
                "|(([_.a-z0-9-]+\\.)+(com|org|ru|su|net|edu|gov|mil|int|in|us|cn)+[a-z0-9\\/_:@=.+?,#%&;~-]*))";
        int ret = regcomp(&regexUrl, regExpText, REG_EXTENDED | REG_ICASE);
        if (ret) {
            Log::error("Utils::FindUrlInText: Could not compile Regular expression");
        }
        else {
            isCompiled = true;
        }
    }
    int ret = DoRegExp(&regexUrl, utf8text);
    free(utf8text);
    return ret;
}

char *Utils::FindUrlInText(const char *text, int occurance, bool needCut) {
    Log::debug("Utils::FindUrlInText");

    char *url = NULL;
    if (!text) {
        return url;
    }

    static bool isCompiled = false;
    static regex_t regexUrl;
    if (!isCompiled) {
        const char *regExpText = NULL;
        if (!needCut) {
            regExpText = "((((www)\\.|(http|https|news)+\\:\\/\\/)[_.a-z0-9-]+\\.[a-z0-9\\/_:@=.+?,##%\\&amp;~-]*[^.|\\'|\\# |!|\\(|?|,| |>|<|;|\\)])"
                    "|(([_.a-z0-9-]+\\.)+(com|org|ru|su|net|edu|gov|mil|int|in|us|cn)+[a-z0-9\\/_:@=.+?,#%&;~-]*))";
        } else {
            regExpText = "((www|WWW)?|[a-z0-9]*)\\.[_.a-z0-9-]+";
        }

        int ret = regcomp(&regexUrl, regExpText, REG_EXTENDED | REG_ICASE);
        if (ret) {
            Log::error("Utils::FindUrlInText: Could not compile Regular expression");
        } else {
            isCompiled = true;
        }
    }

    int matchCounter = 0;
    int ret = regexec(&regexUrl, text, REGMATCH_ARRAY_SIZE, regmatch, 0);
    while (!ret) {
        if (matchCounter == occurance) {
            int urlLen = regmatch[matchCounter].rm_eo - regmatch[matchCounter].rm_so;
            url = new char[urlLen + 1];
            eina_strlcpy(url, text + regmatch[matchCounter].rm_so, urlLen + 1);
            break;
        }
        text = text + regmatch[matchCounter].rm_eo;
        ++matchCounter;
        ret = regexec(&regexUrl, text, 1, &regmatch[matchCounter], 0);
    }

    if (ret == REG_NOMATCH) {
        Log::debug("Utils::FindUrlInText: Matches found:%d", matchCounter);
    } else {
        Log::error("Utils::FindUrlInText: Regular expression execution failed");
    }
    return url;
}

int Utils::FindRegExpInText(const char *regExpText, const char *text)
{
    Log::debug("Utils::FindRegExpInText");

    if (!text || !regExpText) {
        return REG_NOMATCH;
    }

    regex_t regexUrl;
    int ret = regcomp(&regexUrl, regExpText, REG_EXTENDED | REG_ICASE);
    if (ret == 0) {
        ret = DoRegExp(&regexUrl, text);
    } else {
        Log::error("Utils::FindRegExpInText: Could not compile Regular expression");
    }
    return ret;
}

int Utils::DoRegExp(regex_t *regex, const char *text)
{
    if (!regex || !text) {
        Log::debug("Utils::DoRegExp Invalid parameters");
        return 0;
    }

    int matchCounter = 0;
    while (matchCounter < REGMATCH_ARRAY_SIZE && !regexec(regex, text, 1, regmatch + matchCounter, 0)) {
        text = text + (regmatch + matchCounter)->rm_eo;
        matchCounter++;
    }

    Log::debug("Utils::DoRegExp Matches found: %d", matchCounter);

    return matchCounter;
}

char * Utils::GetStringByOffset(const char * text, int startOffset, int endOffset)
{
    assert(text);
    Log::debug("Utils::GetStringByOffset(%s,%d,%d)", text, startOffset, endOffset);
    assert(startOffset >= 0);
    assert(startOffset <= endOffset);
    assert(endOffset <= strlen(text));

    if (!text || startOffset >= endOffset) {
        return NULL;
    }

    int tagLen = endOffset - startOffset;
    char *tag = new char[tagLen + 1];
    eina_strlcpy(tag, text + startOffset, tagLen + 1);

    Log::debug("Utils::GetStringByOffset: Our substring:%s", tag);

    return tag;
}

char* Utils::GetIconNameFromUrl(const char* url){
    char *result = NULL;
    if (url != NULL) {
        std::string input = url;
        std::string img_ext;

        unsigned int start_img_pos = 0;
        unsigned int end_img_pos = input.length();
        unsigned int pos = 0;

        int hash = eina_hash_superfast (url, end_img_pos);

        if ((input.find("%")!= std::string::npos)){
        input = Utils::ReplaceString(input, "%2F", "/");
        input = Utils::ReplaceString(input, "%3A", ":");
        input = Utils::ReplaceString(input, "%3D", "=");
        input = Utils::ReplaceString(input, "%3F", "?");
        input = Utils::ReplaceString(input, "%2C", ",");
        input = Utils::ReplaceString(input, "%26", "&");
        input = Utils::ReplaceString(input, "%23", "#");
        }

        if ((input.find(".jpg")!= std::string::npos)){
            img_ext = ".jpg";
        } else if (input.find(".png")!= std::string::npos){
            img_ext = ".png";
        } else if (input.find(".jpeg")!= std::string::npos){
            img_ext = ".jpeg";
        } else if (input.find(".mp4")!= std::string::npos) {
            img_ext = ".mp4";
        } else {
            img_ext = ".gif";
        }

        if (input.find(img_ext) == std::string::npos){
            img_ext = "#";
        }

        pos = input.rfind("/");
        input.erase(0,pos+1);

        if((input.find(img_ext) + img_ext.length()) != end_img_pos){
            end_img_pos = input.find(img_ext) + img_ext.length();
        }

        char buffer[33];
        Utils::Snprintf_s(buffer, 32, "%x_", hash);
        buffer[32] = 0;

        input = buffer + input.substr(start_img_pos,end_img_pos);

        if(input.length()>MAX_FILE_NAME_LENGTH){
            input = input.substr(0,MAX_FILE_NAME_LENGTH);
            input = input + ".jpg";
        };
        result = strdup(input.c_str());
        Log::debug("Utils::GetIconNameFromUrl, result = %s", input.c_str());
    }
    return result;
}

char* Utils::GetImagePathFromUrl(const char* url) {
    char *imagePath = NULL;
    if(url) {
        const char *fileName = Utils::GetIconNameFromUrl(url);
        if (fileName) {
            char *trustedPath = app_get_shared_trusted_path();
            int size = strlen(trustedPath) + strlen(DIR_IMAGES_NAME) + strlen(fileName) + 2;
            imagePath = static_cast<char *> (malloc(size)); //one more for '/' symbol.
            Utils::Snprintf_s(imagePath, size, "%s%s/%s", trustedPath, DIR_IMAGES_NAME, fileName);
            free((void *)fileName);
            free(trustedPath);
        }
    }
    return imagePath;
}


void Utils::ImageBlur(Evas_Object *evasObjectImage)
{
    // TODO Change algorithm to StackBlur
    // http://www.quasimondo.com/StackBlurForCanvas/StackBlurDemo.html
    unsigned char *imagePixels = static_cast<unsigned char *>(evas_object_image_data_get(evasObjectImage, EINA_TRUE));

    int w, h;
    evas_object_image_size_get(evasObjectImage, &w, &h);
    Log::debug("Utils::ImageBlur: w = %d, h = %d", w, h);

    int borderSize = 2;
    int x, y;

    for (y = borderSize; y < h - borderSize; y++) {
        for (x = borderSize; x < w - borderSize; x++) {
            int averageColor[3] = { 0, 0, 0 };

            int idx = (y * (w << 2)) + ((x - borderSize) << 2);
            averageColor[0] += (int)(((double)imagePixels[idx + 0]) * 0.028087);
            averageColor[1] += (int)(((double)imagePixels[idx + 1]) * 0.028087);
            averageColor[2] += (int)(((double)imagePixels[idx + 2]) * 0.028087);

            idx += 4;
            averageColor[0] += (int)(((double)imagePixels[idx + 0]) * 0.23431);
            averageColor[1] += (int)(((double)imagePixels[idx + 1]) * 0.23431);
            averageColor[2] += (int)(((double)imagePixels[idx + 2]) * 0.23431);

            idx += 4;
            int currentIdx = idx;
            averageColor[0] += (int)(((double)imagePixels[idx + 0]) * 0.475207);
            averageColor[1] += (int)(((double)imagePixels[idx + 1]) * 0.475207);
            averageColor[2] += (int)(((double)imagePixels[idx + 2]) * 0.475207);

            idx += 4;
            averageColor[0] += (int)(((double)imagePixels[idx + 0]) * 0.23431);
            averageColor[1] += (int)(((double)imagePixels[idx + 1]) * 0.23431);
            averageColor[2] += (int)(((double)imagePixels[idx + 2]) * 0.23431);

            idx += 4;
            averageColor[0] += (int)(((double)imagePixels[idx + 0]) * 0.028087);
            averageColor[1] += (int)(((double)imagePixels[idx + 1]) * 0.028087);
            averageColor[2] += (int)(((double)imagePixels[idx + 2]) * 0.028087);

            imagePixels[currentIdx + 0] = averageColor[0];
            imagePixels[currentIdx + 1] = averageColor[1];
            imagePixels[currentIdx + 2] = averageColor[2];

        }
    }

    for (x = borderSize; x < w - borderSize; x++) {
        for (y = borderSize; y < h - borderSize; y++) {
            int avg_color[3] = { 0, 0, 0 };
            int rgbaWidth = w << 2;

            int idx = ((y - borderSize) * rgbaWidth) + (x << 2);
            avg_color[0] += (int)(((double)imagePixels[idx + 0]) * 0.028087);
            avg_color[1] += (int)(((double)imagePixels[idx + 1]) * 0.028087);
            avg_color[2] += (int)(((double)imagePixels[idx + 2]) * 0.028087);

            idx += rgbaWidth;
            avg_color[0] += (int)(((double)imagePixels[idx + 0]) * 0.23431);
            avg_color[1] += (int)(((double)imagePixels[idx + 1]) * 0.23431);
            avg_color[2] += (int)(((double)imagePixels[idx + 2]) * 0.23431);

            idx += rgbaWidth;
            int currentIdx = idx;
            avg_color[0] += (int)(((double)imagePixels[idx + 0]) * 0.475207);
            avg_color[1] += (int)(((double)imagePixels[idx + 1]) * 0.475207);
            avg_color[2] += (int)(((double)imagePixels[idx + 2]) * 0.475207);

            idx += rgbaWidth;
            avg_color[0] += (int)(((double)imagePixels[idx + 0]) * 0.23431);
            avg_color[1] += (int)(((double)imagePixels[idx + 1]) * 0.23431);
            avg_color[2] += (int)(((double)imagePixels[idx + 2]) * 0.23431);

            idx += rgbaWidth;
            avg_color[0] += (int)(((double)imagePixels[idx + 0]) * 0.028087);
            avg_color[1] += (int)(((double)imagePixels[idx + 1]) * 0.028087);
            avg_color[2] += (int)(((double)imagePixels[idx + 2]) * 0.028087);

            imagePixels[currentIdx + 0] = avg_color[0];
            imagePixels[currentIdx + 1] = avg_color[1];
            imagePixels[currentIdx + 2] = avg_color[2];
        }
    }
    evas_object_image_data_update_add(evasObjectImage, 0, 0, w, h);
}

bool Utils::GetImageColorByXY(Evas_Object * evasObjectImage, int x, int y, int *r, int *g, int *b)
{
    bool result = false;
    int w, h;
    evas_object_image_size_get(evasObjectImage, &w, &h);
    if (x < w && y < h) {
        unsigned char *imagePixels = static_cast<unsigned char *>(evas_object_image_data_get(evasObjectImage, EINA_FALSE));
        int idx = (y * (w << 2)) + (x << 2);
        if (r) *r = imagePixels[idx + 2];
        if (g) *g = imagePixels[idx + 1];
        if (b) *b = imagePixels[idx + 0];
        result = true;
    }
    return result;
}

char * Utils::SurfaceWriteToPng(cairo_surface_t * surface, const char * saveDirPath)
{
    struct stat st = { 0 };
    if (stat(saveDirPath, &st) == -1) {
        mkdir(saveDirPath, 0740);
    }

    char fullFileName [MAX_FULL_PATH_NAME_LENGTH];
    Utils::Snprintf_s(fullFileName, MAX_FULL_PATH_NAME_LENGTH, "%s/%.0f.png", saveDirPath, GetCurrentTime());
    cairo_surface_write_to_png(surface, fullFileName);
    return strdup(fullFileName);
}

char * Utils::DrawText(const char * text, double scaleFactor, int w, int h, int r, int g, int b, const char * saveDirPath)
{
    Log::debug("Utils::DrawText");
    cairo_surface_t * surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, w, h);
    cairo_t * cairo = cairo_create(surface);
    cairo_select_font_face(cairo, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
    double fontSize = 100.0;
    cairo_set_font_size(cairo, fontSize);
    cairo_set_source_rgba(cairo, (double) r / 255, (double) g / 255, (double) b / 255, 1.0);
    if (scaleFactor) {
        cairo_scale(cairo, scaleFactor, scaleFactor);
    }
    cairo_move_to (cairo, 10.0, fontSize);
    cairo_show_text(cairo, text);

    char * fileName = SurfaceWriteToPng(surface, saveDirPath);

    cairo_destroy(cairo);
    cairo_surface_destroy(surface);

    return fileName;
}

char * Utils::DrawTextOnImage(Evas_Object *evasObjectImage, double imageScaleFactor, const char * text, double textZoomFactor,
        int textPositionX, int textPositionY, int r, int g, int b, const char * saveDirPath)
{
    Log::debug("Utils::DrawTextOnImage");

    unsigned char *imagePixels = static_cast<unsigned char *>(evas_object_image_data_get(evasObjectImage, EINA_TRUE));

    int w, h;
    evas_object_image_size_get(evasObjectImage, &w, &h);

    Log::debug("Utils::DrawTextOnImage: image size w: %d, h: %d", w, h);

    cairo_format_t imageFormat = CAIRO_FORMAT_ARGB32;
    int rowStride = cairo_format_stride_for_width(imageFormat, w);

    cairo_surface_t * surface = cairo_image_surface_create_for_data(imagePixels, imageFormat, w, h, rowStride);
    cairo_t * cairo = cairo_create(surface);
    cairo_select_font_face(cairo, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
    double fontSize = 100.0 * textZoomFactor;
    cairo_set_font_size(cairo, fontSize);
    cairo_set_source_rgba(cairo, (double) r / 255, (double) g / 255, (double) b / 255, 1.0);
    if (imageScaleFactor) {
        cairo_scale(cairo, imageScaleFactor, imageScaleFactor);
    }
    cairo_move_to(cairo, textPositionX + 10.0 * textZoomFactor, textPositionY + fontSize);
    cairo_show_text(cairo, text);

    char * fileName = SurfaceWriteToPng(surface, saveDirPath);

    cairo_destroy(cairo);
    cairo_surface_destroy(surface);

    return fileName;
}

void Utils::CropImageToFill(Evas_Object *evasObjectImage, int targetW, int targetH, int imageW, int imageH) {
    int w, h;
    if ((double)targetW / (double)targetH >= (double)imageW / (double)imageH) { //this means that target is wider than source image. So, lets crop height
        w = targetW;
        h = (double)w * ((double)imageH / (double)imageW);
        evas_object_image_fill_set(evasObjectImage, 0, (targetH - h) / 2, w, h);
    } else {
        h = targetH;
        w = (double)h * ((double)imageW / (double)imageH);
        evas_object_image_fill_set(evasObjectImage, (targetW - w) / 2, 0, w, h);
    }
    evas_object_image_load_size_set(evasObjectImage, w, h);
}

const char * Utils::GetLocale()
{
    const char *locale;
    int ret = I18N_ERROR_NONE;

    // Gets default locale
    ret = i18n_ulocale_get_default(&locale);
    if ( ret != I18N_ERROR_NONE ) {
        Log::error("Utils::GetLocale: i18n_ulocale_get_default() is failed.");
    }
    Log::debug("Utils::GetLocale: default locale : %s", locale);
    return locale;

}

const char * Utils::GetLocaleCode()
{
    const char *locale;
    char language[64] = {0,};
    int language_capacity = 64;
    int buf_size_language;
    int ret = I18N_ERROR_NONE;

    // Gets default locale
    ret = i18n_ulocale_get_default(&locale);
    if ( ret != I18N_ERROR_NONE ) {
        Log::error("Utils::GetLocaleCode: i18n_ulocale_get_default() is failed.");
    }
    Log::debug("Utils::GetLocaleCode: default locale : %s", locale);

    // Gets the language code for the specified locale
    ret = i18n_ulocale_get_language(locale, language, language_capacity, &buf_size_language);
    if ( ret != I18N_ERROR_NONE ) {
        Log::error("Utils::GetLocaleCode: i18n_ulocale_get_language() is failed.");
    }
    Log::debug("Utils::GetLocaleCode: language code for the locale : %s", language);
    return strdup(language);
}

void Utils::AcctFuncRetValLog(const char* funcName, int ret)
{
    if (ACCOUNT_ERROR_NONE == ret ) {
        Log::verbose("Utils::AcctFuncRetValLog: %s() Success.", funcName);
    } else {
        Log::error("Utils::AcctFuncRetValLog: %s() Failed.", funcName);
    }
}

int Utils::SelectMediaFilesFromMediaDB(const char * filterCondition, media_info_cb callback, void * user_data, const char * order,
        media_content_collation_e * collation, media_content_order_e * orderType)
{
    // Retrieve media file list from system
    int ret = media_content_connect();
    if (ret != MEDIA_CONTENT_ERROR_NONE) {
        Log::error("Utils::SelectMediaFilesFromMediaDB: media_content_connect failed: %d", ret);
        return ret;
    }

    filter_h filter = NULL;
    if (filterCondition) {
        ret = media_filter_create(&filter);
        if (ret != MEDIA_CONTENT_ERROR_NONE) {
            media_content_disconnect();
            Log::error("Utils::SelectMediaFilesFromMediaDB: media_filter_create failed: %d", ret);
            return ret;
        }

        ret = media_filter_set_condition(filter, filterCondition, collation ? *collation : MEDIA_CONTENT_COLLATE_DEFAULT);
        if (ret != MEDIA_CONTENT_ERROR_NONE) {
            media_content_disconnect();
            Log::error("Utils::SelectMediaFilesFromMediaDB: media_filter_set_condition failed: %d", ret);
            media_filter_destroy(filter);
            return ret;
        }

        if (order) {
            media_filter_set_order(filter, orderType ? *orderType : MEDIA_CONTENT_ORDER_ASC, order,
                    collation ? *collation : MEDIA_CONTENT_COLLATE_DEFAULT);
            if (ret != MEDIA_CONTENT_ERROR_NONE) {
                media_content_disconnect();
                Log::error("Utils::SelectMediaFilesFromMediaDB: media_filter_set_order failed: %d", ret);
                media_filter_destroy(filter);
                return ret;
            }
        }
    }

    ret = media_info_foreach_media_from_db(filter, callback, user_data);
    if (filter) {
        media_filter_destroy(filter);
    }
    if (ret != MEDIA_CONTENT_ERROR_NONE) {
        Log::error("Utils::SelectMediaFilesFromMediaDB: media_info_foreach_media_from_db failed: %d", ret);
    }
    media_content_disconnect();
    return ret;
}

void Utils::SelectMediaFileFromMediaDBByPath(char *imagePath, media_info_cb callback, void * user_data)
{
    char filterCondition[256] = {0, };
    Utils::Snprintf_s(filterCondition, 256, MEDIA_PATH "=\"%s\"", imagePath);
    media_content_collation_e collation = MEDIA_CONTENT_COLLATE_DEFAULT;
    media_content_order_e orderType = MEDIA_CONTENT_ORDER_DESC;

    Utils::SelectMediaFilesFromMediaDB(filterCondition, callback, user_data, MEDIA_MODIFIED_TIME, &collation, &orderType);
}

int Utils::InsertMediaFileToMediaDB(const char *path, media_info_cb callback, void * user_data)
{
    int ret = media_content_connect();
    if (ret != MEDIA_CONTENT_ERROR_NONE) {
        Log::error("Utils::InsertMediaFileToMediaDB: media_content_connect failed: %d", ret);
        return ret;
    }

    media_info_h mediaInfo;
    ret = media_info_insert_to_db(path, &mediaInfo);
    if (ret != MEDIA_CONTENT_ERROR_NONE) {
        media_content_disconnect();
        Log::error("Utils::InsertMediaFileToMediaDB: media_info_insert_to_db failed: %d", ret);
        return ret;
    }

    callback(mediaInfo, user_data);
    media_info_destroy(mediaInfo);
    media_content_disconnect();
    return ret;
}

int Utils::DeleteMediaFileFromDB(const char *mediaId)
{
    int ret = media_content_connect();
    if (ret != MEDIA_CONTENT_ERROR_NONE) {
        Log::error("Utils::DeleteMediaFileFromDB: media_content_connect failed: %d", ret);
        return ret;
    }

    ret = media_info_delete_from_db(mediaId);
    if (ret != MEDIA_CONTENT_ERROR_NONE) {
        Log::error("Utils::DeleteMediaFileFromDB: media_info_delete_from_db failed: %d", ret);
    }
    media_content_disconnect();

    return ret;
}

static int internal_storage_id = -1;

static bool get_storage_id_cb(int storage_id, storage_type_e type, storage_state_e state, const char *path, void *user_data)
{
    if (type == STORAGE_TYPE_INTERNAL) {
       internal_storage_id = storage_id;
       return false;
    }
    return true;
}

char * Utils::GetInternalStorageDirectory(storage_directory_e dirType)
{
    if (internal_storage_id == -1) {
        storage_foreach_device_supported(get_storage_id_cb, NULL);
    }
    char *path = NULL;
    storage_get_directory(internal_storage_id, dirType, &path);

    return path;
}

unsigned long long Utils::GetAvailableInternalMemorySize()
{
    unsigned long long memorySize = 0.0;

    if (internal_storage_id == -1) {
        struct statvfs s;
        if (!storage_get_internal_memory_size(&s)) {
            memorySize = s.f_bsize*s.f_bavail;
        }
    }
    else {
        storage_get_available_space(internal_storage_id, &memorySize);
    }
    return memorySize;
}

static void hide_toast_cb(void *data, Evas_Object *obj, void *event_info)
{
    evas_object_del(obj);
}

void Utils::ShowToast(Evas_Object * layout, const char * message)
{
    CHECK_RET_NRV(message);
    Evas_Object * toast = elm_popup_add(layout);
    elm_object_style_set(toast, "toast");
    elm_popup_orient_set(toast, ELM_POPUP_ORIENT_BOTTOM);
    elm_popup_align_set(toast, 0.5, 1.0);
    elm_popup_allow_events_set(toast, true);
    elm_object_part_text_set(toast, "default", message);
    elm_popup_timeout_set(toast, 3.0);
    evas_object_smart_callback_add(toast, "timeout", hide_toast_cb, NULL);
    evas_object_smart_callback_add(toast, "block,clicked", hide_toast_cb, NULL);

    evas_object_show(toast);
}

Evas_Object *Utils::ShowUpdatingToast(Evas_Object * layout, const char * message)
{
    Evas_Object * toast = elm_popup_add(layout);
    elm_object_style_set(toast, "toast");
    elm_popup_orient_set(toast, ELM_POPUP_ORIENT_BOTTOM);
    elm_popup_align_set(toast, 0.5, 0.5);
    elm_object_part_text_set(toast, "default", message);

    /* I think we should add a timer and cb's here
     * as w/o internet connection the app becomes
     * STUPID
     */
    elm_popup_timeout_set(toast, 5.0);
    evas_object_smart_callback_add(toast, "timeout", hide_toast_cb, NULL);

    evas_object_show(toast);

    return toast;
}

void Utils::HideUpdatingToast(Evas_Object *obj)
{
    evas_object_del(obj);
}

int Utils::FindElement(void * object, Eina_Array * array, bool (*comparator) (const void *, const void *)) {
    if ( !object || !array ) {
        Log::error("Utils::FindElement: passed null objects!");
        return NO_ITEMS_FOUND;
    }

    int count = eina_array_count(array);

    if ( count > 0 ) {
        unsigned int index = 0;
        void* item;
        Eina_Array_Iterator iterator;
        EINA_ARRAY_ITER_NEXT( array, index, item, iterator ) {
            if (comparator) {
                if (comparator(item, object)) {
                    return index;
                }
            } else {
                if (item == object) {
                    return index;
                }
            }
        }
    }

    return NO_ITEMS_FOUND;
}

bool Utils::IsMe(const char* userId) {
    CHECK_RET(userId, false);
    return Config::GetInstance().GetUserId() == userId;
}

void swap(Eina_Array * array, const int i, const int j)
{
    const void * temp = (char *) eina_array_data_get(array, i);
    eina_array_data_set(array, i, (const void *) eina_array_data_get(array, j));
    eina_array_data_set(array, j, temp);
}

int Utils::digits_strcmp(const char *a, const char *b)
{
    int delta = strlen(a)-strlen(b);

    if ( delta == 0 ) {
        return strcmp(a,b);
    }

    return delta < 0 ? -1 : 1;
}

void Utils::QuickSort(Eina_Array * array, int first, int last, bool descending, int (*user_strcmp) (const char *, const char *) )
{
    const char * x = (char *) eina_array_data_get(array, (first + last) / 2);
    int i = first, j = last;

    do {
        if (descending) {
            while ((user_strcmp((char *)eina_array_data_get(array, i), x) > 0)) {
                i++;
            }
            while ((user_strcmp((char *)eina_array_data_get(array, j), x) < 0)) {
                j--;
            }
        } else {
            while ((user_strcmp((char *)eina_array_data_get(array, i), x) < 0)) {
                i++;
            }
            while ((user_strcmp((char *)eina_array_data_get(array, j), x) > 0)) {
                j--;
            }
        }

        if(i <= j) {
            if (descending) {
                if ((user_strcmp((char *)eina_array_data_get(array, i), (char *)eina_array_data_get(array, j)) < 0)) {
                    swap(array, i, j);
                }
            } else {
                if ((user_strcmp((char *)eina_array_data_get(array, i), (char *)eina_array_data_get(array, j)) > 0)) {
                    swap(array, i, j);
                }
            }
            i++;
            j--;
        }
    } while (i <= j);

    if (i < last) {
        QuickSort(array, i, last, descending, user_strcmp);
    }
    if (first < j) {
        QuickSort(array, first, j, descending, user_strcmp);
    }
 }

/**
 * @brief This function check if string is empty or contains only spaces
 * @return[in] true if string is NULL or empty or contains only spaces
 */
bool Utils::isEmptyString(const char *st) {
    bool res = true;
    if(st) {
        while(*st){
            if( *st != ' ' && *st != '\n' && *st != '\t' ) {
                res = false;
                break;
            }
            st++;
        }
    }
    return res;
}

char* Utils::WrapByFormatGenlist(const char *formatPattern, const char *content)
{
    char *res = NULL;
    if ((content) && (formatPattern)) {
        int len = strlen(formatPattern) + strlen(content) + 1;
        res = (char*) malloc(len);
        if (res) {
            Utils::Snprintf_s(res, len, formatPattern, content);
            res[len-1] = 0;
        }
    }
    return res;
}

char* Utils::WrapByFormat2Genlist(const char *formatPattern, const char *contentPart1,
        const char *contentPart2)
{
    char *res = NULL;
    if ((contentPart1) && (contentPart2) && (formatPattern)) {
        int len = strlen(formatPattern) + strlen(contentPart1) + strlen(contentPart2) + 1;
        res = (char*) malloc(len);
        if (res) {
            Utils::Snprintf_s(res, len, formatPattern, contentPart1, contentPart2);
            res[len-1] = 0;
        }
    }
    return res;
}

const char *InitHTTPUserAgent() {
    //Set the User Agent to be sent in the HTTP header. The format is as follows
    //{Browser UA} [FBAN/{App Name};FBAV/{App Version};FBBV/{App Build Number};FBMF/{Device Manufacturer};
    //FBDV/{Device Model};FBSN/{OS Name};FBSV/{OS Version};FBCR/{Carrier};FBLC/{Locale};FBDM/{Display Metrics}]

    //Retreive the information necessary to build the HTTP UA header using system info API calls
    static char user_agent_string[MAX_UA_LENGTH] = {0};
    int ret,tret, dpi, height, width;

    //Get the Platform Version
    char * platform_version;
    ret = system_info_get_platform_string("http://tizen.org/feature/platform.version", &platform_version);
    if(ret == SYSTEM_INFO_ERROR_NONE) {
        Log::debug("InitHTTPUserAgent->Platform version =%s", platform_version);
    } else {
        platform_version = strdup(defPlatformVersion);
    }

    //Get the DPI
    ret = system_info_get_platform_int("http://tizen.org/feature/screen.dpi", &dpi);
    if(ret == SYSTEM_INFO_ERROR_NONE) {
        Log::debug("InitHTTPUserAgent->Screen DPI =%d", dpi);
    } else {
        dpi = 0;
    }

    //get the Height
    ret = system_info_get_platform_int("http://tizen.org/feature/screen.height", &height);
    if(ret == SYSTEM_INFO_ERROR_NONE) {
        Log::debug("InitHTTPUserAgent->Screen Height =%d", height);
    } else {
        height = 0;
    }

    //get the width
    ret = system_info_get_platform_int("http://tizen.org/feature/screen.width", &width);
    if(ret == SYSTEM_INFO_ERROR_NONE) {
        Log::debug("InitHTTPUserAgent->Screen Width =%d", width);
    } else {
        width = 0;
    }

    //Get the Model name
    char *model_name;
    ret = system_info_get_platform_string("http://tizen.org/system/model_name", &model_name);
    if(ret == SYSTEM_INFO_ERROR_NONE) {
        Log::debug("InitHTTPUserAgent->Model Name =%s", model_name);
    } else {
        model_name = strdup(defModel);
    }

    //Get the Platform Name
    char * platform_name;
    ret = system_info_get_platform_string("http://tizen.org/system/platform.name", &platform_name);
    if(ret == SYSTEM_INFO_ERROR_NONE) {
        Log::debug("InitHTTPUserAgent->Platform Name =%s", platform_name);
    } else {
        platform_name = strdup(defPlatform);
    }

    //Get the manufacturer Name
    char * manuf_name;
    ret = system_info_get_platform_string("http://tizen.org/system/manufacturer", &manuf_name);
    if(ret == SYSTEM_INFO_ERROR_NONE) {
        Log::debug("InitHTTPUserAgent->Manufacturer =%s", manuf_name);
    } else {
        manuf_name = strdup(defMenufacturerName);
    }

    //Get the Locale
    char * locale;
    ret = system_settings_get_value_string (SYSTEM_SETTINGS_KEY_LOCALE_LANGUAGE, &locale);
    if(ret == SYSTEM_SETTINGS_ERROR_NONE ) {
        Log::debug("InitHTTPUserAgent->Locale =%s", locale);
    } else {
        locale = strdup(defLocale);
    }

    //Application version detials
    char * app_version;
    tret =  app_get_version (&app_version);
    if(tret != APP_ERROR_NONE) {
        //set the APP version as 0.0.0
        app_version = strdup(defAppVersion);
    }

    //Carrier
    char *mcc, *mnc;
    Utils::GetMCC_MNC(&mcc,&mnc);

    // An example of user-ageent string is given below:
    //Mozilla/5.0 (Linux; Tizen 2.3; SAMSUNG SM-Z130H) AppleWebKit/537.3 (KHTML, like Gecko) Version/2.3
    //Mobile Safari/537.3 [FBAN/FB4T;FBAV/1.0;FBBV/15;FBMF/Samsung;FBDV/Z3;FBSN/Tizen;FBSV/2.3;
    //FBCR/310-012;FBLC/en_US;FBCM/{density=3.0,width=720,height=1080};]

    char ua_agent_part1[MAX_UA_LENGTH/2] = {0};
    char ua_agent_part2[MAX_UA_LENGTH/2] = {0};
    char ua_agent_part3[MAX_UA_LENGTH/2] = {0};

    Utils::Snprintf_s(ua_agent_part1, MAX_UA_LENGTH/2, "Mozilla/5.0 (Linux; Tizen %s; %s %s) AppleWebKit/537.3 (KHTML, like Gecko) Version/%s Mobile Safari/537.3", platform_version, manuf_name, model_name, platform_version);
    Utils::Snprintf_s(ua_agent_part2, MAX_UA_LENGTH/2, "FBAN/FB4T;FBAV/%s;FBBV/%s;FBMF/%s;FBDV/%s;FBSN/Tizen", app_version, app_version, manuf_name, model_name);
    Utils::Snprintf_s(ua_agent_part3, MAX_UA_LENGTH/2, "FBSV/%s;FBCR/%s-%s;FBLC/%s;FBCM/{density=%d,width=%d,height=%d}", platform_version, mcc, mnc, locale, dpi, width, height);
    assert(strlen(ua_agent_part1) + strlen(ua_agent_part2) + strlen(ua_agent_part3) + 5 < MAX_UA_LENGTH);  /* 5 characters for " [;;]" */

    Utils::Snprintf_s(user_agent_string, MAX_UA_LENGTH, "%s [%s;%s;]", ua_agent_part1, ua_agent_part2, ua_agent_part3);
    Log::debug("InitHTTPUserAgent() - UserAgent='%s'", user_agent_string);

    //Free the allocated strings
    free(platform_version);
    free(model_name);
    free(platform_name);
    free(manuf_name);
    free(locale);
    free(app_version);
    free(mcc);
    free(mnc);
    return user_agent_string;
}

const char * Utils::GetHTTPUserAgent() {
    static const char *dataS_http_user_agent = InitHTTPUserAgent();
    return dataS_http_user_agent;
}

bool Utils::GetMCC_MNC(char **mcc, char **mnc) {
    telephony_handle_list_s handle_list;

    int ret;
    int l_sim_num;

    *mcc = NULL;
    *mnc = NULL;
    bool result = false;

    ret = telephony_init(&handle_list);// In case of single SIM, we get only one handle

    if (ret == TELEPHONY_ERROR_NONE) {
        for(l_sim_num=0;l_sim_num<handle_list.count;l_sim_num++) {
            if(handle_list.handle[l_sim_num] != NULL)
                break;
        }
        if(l_sim_num != handle_list.count ) {
            //Fetch the MCC and MNC of the first found SIM
            //Get the SIM state
            telephony_sim_state_e l_state;
            ret = telephony_sim_get_state(handle_list.handle[l_sim_num], &l_state);
            if(ret == TELEPHONY_ERROR_NONE) {
                //If the SIM state is active get the mcc and mnc details
                if(l_state == TELEPHONY_SIM_STATE_AVAILABLE) {
                    result = true;
                    ret = telephony_network_get_mcc(handle_list.handle[l_sim_num], mcc);
                    if(ret != TELEPHONY_ERROR_NONE) {
                        *mcc = strdup(defMCC);
                    }
                    ret = telephony_network_get_mnc(handle_list.handle[l_sim_num], mnc);
                    if(ret != TELEPHONY_ERROR_NONE) {
                        *mnc = strdup(defMNC);
                    }
                }
            }
        }
    }//Get MCC and MNC details

    if(!result){
        //Set the MCC and MNC as 0
        if(!*mcc) *mcc = strdup(defMCC);
        if(!*mnc) *mnc = strdup(defMNC);
    }
    return result;
}

void Utils::ReplaceFoundTextInText(std::string &composer, int matchCount, const char *text) {
    if (matchCount && text) {
        std::string strDup = composer;
        const char *message = strDup.c_str();
        regmatch_t * regmatch = Utils::GetRegmatch();
        int pos = 0;
        for (int i = 0; i < matchCount; i++) {
            char *tag = Utils::GetStringByOffset(message, regmatch[i].rm_so, regmatch[i].rm_eo);
            if(tag) {
                int tagLength = strlen(tag);
                int index = tagLength - 1;
                while((index >=0) && tag[index] == '\n') {
                    tag[index] = 0;
                    index--;
                }
                int delta = composer.length();
                composer = Utils::ReplaceString(composer, tag, text, pos, true);
                Log::info("Utils::ReplaceFoundTextInText: replace_text:%s: %s ", tag, text);
                delete[] tag;
                delta = composer.length() - delta;
                pos += regmatch[i].rm_so + tagLength + delta;
            }
            message = message + regmatch[i].rm_eo;
        }
    }
}

char* Utils::getCopy(const char* st)
{
    char* res = NULL;
    if(st) {
        res = new char[strlen(st) + 1];
        if(res) {
            eina_strlcpy(res, st, strlen(st) + 1);
        }
    }
    return res;
}

void Utils::FoundAndReplaceText(std::string &composer, const char *regExp, const char *repText)
{
    int matchCount = Utils::FindRegExpInText(regExp, composer.c_str());
    ReplaceFoundTextInText(composer, matchCount, repText);
}

char *Utils::escape(const char* str) {
    char *escape_encoded = NULL;
    if( str ) {
        CURL *curl = curl_easy_init();
        if(curl) {
              char *output = curl_easy_escape(curl, str, 0);
              if(output) {
                escape_encoded = strdup(output);
                curl_free(output);
              }
              curl_easy_cleanup(curl);
        }
    }
    return escape_encoded;
}

char *Utils::unescape(const char* str) {
    char *escape_decoded = NULL;
    if( str ) {
        CURL *curl = curl_easy_init();
        if(curl) {
            char *output = curl_easy_unescape(curl, str, 0, nullptr);
            if(output) {
                escape_decoded = strdup(output);
                curl_free(output);
            }
            curl_easy_cleanup(curl);
        }
    }
    return escape_decoded;
}

std::string Utils::ExtractEtag(const char *header) {
    std::string res;
    std::string str = header;
    std::size_t pos1 = str.find("\"");
    if(pos1 != std::string::npos) {
        std::size_t pos2 = str.find("\"",pos1+1);
        if(pos2 != std::string::npos) {
            res = str.substr (pos1+1, pos2-pos1-1);
        }
    }
    return res;
}

bool Utils::IsHeader_Etag(const char *header) {
    return (!strncmp(header, "ETag:",5));
}

bool Utils::IsHeader_304NotModified(const char *header) {
    return (!strncmp(header, "HTTP/1.1 304 Not Modified", 25));
}

int rm_cb(const char *fpath, const struct stat *sb, int tflag, struct FTW *ftwbuf)
{
    int ret;
    if (tflag == FTW_D) {
        ret = rmdir(fpath);
    }
    else {
        ret = remove(fpath);
    }
    return ret;
}

int Utils::RemoveFolderRecursively(const char * path)
{
    return nftw(path, rm_cb, 20, FTW_DEPTH | FTW_PHYS);
}

i18n_uchar * Utils::UDup(const char*str) {
    i18n_uchar *uStr = NULL;
    assert(str);
    if (str) {
//length of uStr should not be more than length of str.
        uStr = new i18n_uchar[strlen(str) + 1];
        i18n_ustring_copy_ua(uStr, str);
    }
    return uStr;
}

int Utils::UStrLen(const char*str) {
    int len = 0;
    assert(str);
    if (str) {
        i18n_uchar *uStr = UDup(str);
        len = i18n_ustring_get_length(uStr);
        delete[] uStr;
    }
    return len;
}

int Utils::UCompare(const i18n_uchar *uStr1, const i18n_uchar *uStr2, bool caseSensitive) {
    if (caseSensitive) {
        return i18n_ustring_compare(uStr1, uStr2);
    } else {
        return i18n_ustring_case_compare(uStr1, uStr2, I18N_USTRING_U_FOLD_CASE_DEFAULT);
    }
}

int Utils::UCompare(const char*str1, const char*str2, bool caseSensitive) {
    i18n_uchar *uStr1 = UDup(str1);
    i18n_uchar *uStr2 = UDup(str2);

    int res = UCompare(uStr1, uStr2, caseSensitive);

    delete[] uStr1;
    delete[] uStr2;

    return res;
}

int Utils::UCompareN(const i18n_uchar *uStr1, const i18n_uchar *uStr2, int len, bool caseSensitive) {
    if (caseSensitive) {
        return i18n_ustring_compare_n(uStr1, uStr2, len);
    } else {
        return i18n_ustring_case_compare_n(uStr1, uStr2, len, I18N_USTRING_U_FOLD_CASE_DEFAULT);
    }
}

int Utils::UCompareN(const char*str1, const char*str2, int len, bool caseSensitive) {
    i18n_uchar *uStr1 = UDup(str1);
    i18n_uchar *uStr2 = UDup(str2);

    int res = UCompareN(uStr1, uStr2, len, caseSensitive);

    delete[] uStr1;
    delete[] uStr2;

    return res;
}

bool Utils::UIsSubstringOfAnyWord(const char*text, const char*substring, bool caseSensitive) {
    bool res = false;
    assert(text && substring);

    if (text && substring && *text && *substring) {  //both text and substring are not empty
        i18n_uchar *saveState;
        i18n_uchar *uText = UDup(text);
        i18n_uchar *uDelim = UDup(" ");
        i18n_uchar *uSubstring = UDup(substring);

        i18n_uchar*uRes = i18n_ustring_tokenizer_r(uText, uDelim, &saveState);
        while (uRes) {
            res = !UCompareN(uRes, uSubstring, i18n_ustring_get_length(uSubstring), caseSensitive);
            if (res) {
                break;
            }
            uRes = i18n_ustring_tokenizer_r(NULL, uDelim, &saveState);
        }
        delete[] uText;
        delete[] uDelim;
        delete[] uSubstring;
    }

    return res;
}

bool Utils::UAreAllWordsSubstringsOfAnyWord(const char*text, const char*substrings, bool caseSensitive) {
    bool res = false;
    assert(text && substrings);

    if (text && substrings && *text && *substrings) {  //both text and substrings are not empty
        i18n_uchar *saveState;
        i18n_uchar *uDelim = UDup(" ");
        i18n_uchar *uSubstrings = UDup(substrings);

        i18n_uchar*uWord = i18n_ustring_tokenizer_r(uSubstrings, uDelim, &saveState);
        while (uWord) {
            char *word = new char[i18n_ustring_get_length(uWord)*6 + 1]; // len*6 should be enough. utf-8 characters can have 1 to 6 bytes.
            i18n_ustring_copy_au(word, uWord);
            res = UIsSubstringOfAnyWord(text, word, caseSensitive);
            delete []word;
            if (!res) {
                break;
            }
            uWord = i18n_ustring_tokenizer_r(NULL, uDelim, &saveState);
        }
        delete[] uDelim;
        delete[] uSubstrings;
    }

    return res;
}

bool Utils::IsSpace(char ch) {
    return (ch == ' ' || ch == '\n' || ch == '\t' );
}

char *Utils::Trim(const char*str) {
    assert(str);
    if (!str) return NULL;

    int length = strlen(str);

    while (IsSpace(str[length - 1]))
        --length;
    while (*str && IsSpace(*str))
        ++str, --length;

    return strndup(str, length);
}

bool gallery_media_item_get_thumbnail_path_with_pathfilter_cb(media_info_h media, void *user_data)
{
    bool retVal = false;
    char *thumbnailPath = NULL;
    char **multiImagesPathDetails = static_cast<char **>(user_data);

    if ( multiImagesPathDetails )
    {
        int ret = media_info_get_thumbnail_path(media, &thumbnailPath);
        if( MEDIA_CONTENT_ERROR_NONE == ret)
        {
            *multiImagesPathDetails = SAFE_STRDUP(thumbnailPath);
            Log::debug("Utils::gallery_media_item_get_thumbnail_path_with_pathfilter_cb() :: Thumbnail path is %s", *multiImagesPathDetails);
            free(thumbnailPath);
            retVal = true;
        }
    }
    return retVal;
}

void Utils::GetThumbNailPathforUsingPathFilter(char *imagePath, char** thumbNailPath)
{
    char filterCondition[256] = {0, };
    Utils::Snprintf_s(filterCondition, 256, "%s=\"%s\"", MEDIA_PATH, imagePath);
    media_content_collation_e collation = MEDIA_CONTENT_COLLATE_DEFAULT;
    media_content_order_e orderType = MEDIA_CONTENT_ORDER_DESC;

    Utils::SelectMediaFilesFromMediaDB(filterCondition, gallery_media_item_get_thumbnail_path_with_pathfilter_cb, thumbNailPath, MEDIA_MODIFIED_TIME, &collation,
            &orderType);
}

int Utils::GetSymbolCount(char symbol, const char * string)
{
    int result = 0;
    if (!string) {
        result = -1;
    }
    else {
        int length = strlen(string);
        for (int i = 0; i < length; ++i) {
            if (string[i] == symbol) {
                ++result;
            }
        }
    }
    return result;
}

unsigned int Utils::GetNLineLength(const std::string& message, unsigned int nline)
{
    int npos = -1;
    i18n::u16string uMessage(i18n::StdStringToU16String(message));
    i18n::u16string searchStr;
    i18n::u16string uN(i18n::StdStringToU16String("\n"));
    i18n::u16string uBR(i18n::StdStringToU16String("<br/>"));
    if (uMessage.find(uN)!= i18n::u16string::npos) {
        searchStr = uN;
    } else if (uMessage.find(uBR)!= i18n::u16string::npos) {
        searchStr = uBR;
    }

    if (!searchStr.empty()) {
       for (int i = 0; i < nline; i++) {
            if (uMessage.find(searchStr, npos + 1)!= i18n::u16string::npos) {
                npos = uMessage.find(searchStr, npos + 1);
            }
            else {
                npos = -1;
                break;
            }
        }
    }

    return npos + 1;
}

int Utils::GetEXIFOrientation(const char *file_path)
{
    int orientation = 0;
    ExifData *exifData = exif_data_new_from_file(file_path);
    if (exifData)
    {
        ExifByteOrder byteOrder = exif_data_get_byte_order(exifData);
        ExifEntry *exifEntry = exif_data_get_entry(exifData, EXIF_TAG_ORIENTATION);

        if (exifEntry)
        {
            orientation = (int)exif_get_short(exifEntry->data, byteOrder);
        }
        exif_data_free(exifData);
    }
    Log::debug("Utils::ExifOrientation: %d",orientation);
    return orientation;
}

bool Utils::FileExists(const char *filePath)
{
    bool result = false;
    if (filePath) {
        FILE *fp = fopen(filePath, "r");
        if(fp) {
            fclose(fp);
            result = true;
        }
    }
    Log::debug( "File with path %s %s!", filePath, result ? "exists" : "doesn't exists");
    return result;
}

GraphObject * Utils::FindGraphObjectInProvidersById(const char * id)
{
    GraphObject * object = NULL;
    if (id) {
        object = GroupProvider::GetInstance()->GetGraphObjectById(id);
        if (!object) {
            object = OwnFriendsProvider::GetInstance()->GetGraphObjectById(id);
            if (!object) {
                object = EventBatchProvider::GetInstance()->GetGraphObjectById(id);
            }
        }
    }
    return object;
}

void Utils::SetKeyboardFocus(Evas_Object* EvasObject, Eina_Bool EnableFocus) {
    elm_object_focus_set(EvasObject, EnableFocus);
    elm_entry_input_panel_enabled_set(EvasObject, EnableFocus);
}

const char* Utils::TextCutter(const char *text, int cutLength)
{
    char *cutted = NULL;

    if(text) {
        int count = strlen(text);

        if ( count > cutLength ) {
            std::string str2 = "...";
            // TODO: we should allocate memory for cutLength symbols only
            cutted = new char[count + str2.length() + 1];
            eina_strlcpy(cutted, text, cutLength + 1);
            eina_strlcat(cutted, str2.c_str(), count + str2.length() + 1);
        }
        else {
            cutted = Utils::getCopy(text);
        }
    }

    return cutted;
}

void Utils::SetImageToEvasAndFill(Evas_Object * layout, const char * part, const char * fileName)
{
    Evas_Object *cover = evas_object_image_add(evas_object_evas_get(layout));
    evas_object_size_hint_weight_set(cover, 1, 1);
    evas_object_size_hint_align_set(cover, -1, -1);
    evas_object_image_file_set(cover, fileName, NULL);
    elm_layout_content_set(layout, part, cover);

    int swallowW = 0;
    int swallowH = 0;
    edje_object_part_geometry_get(elm_layout_edje_get(layout), part, NULL, NULL, &swallowW, &swallowH);

    int imgW = 0;
    int imgH = 0;
    evas_object_image_size_get(cover, &imgW, &imgH);

    CropImageToFill(cover, swallowW, swallowH, imgW, imgH);
}

void Utils::IncreaseTime(tm* time, int days, int hours, int minutes, int seconds) {
    time_t tTime = mktime(time);
    tTime += (days*24*60*60 + hours*60*60 + minutes*60 + seconds);
    *time = *(localtime(&tTime));

}

char* Utils::UTF8TextCutter(const char *text, int cutLength, bool withDots)
{
    if (!text) {
        return NULL;
    }
    i18n_uchar *uStr = new i18n_uchar[strlen(text) + 1];
    char *clippedStr;

    i18n_ustring_copy_ua(uStr, text);
    if (i18n_ustring_get_length(uStr) > (cutLength + 1)) {  //"+1" as there's no sense to truncate only one character
        uStr[cutLength] = 0;
        clippedStr = new char[strlen(text) + 4];
        i18n_ustring_copy_au(clippedStr, uStr);
        if (withDots) {
            eina_strlcat(clippedStr, "...", strlen(text) + 4);
        }
    } else {
        clippedStr = Utils::getCopy(text);
    }
    delete[] uStr;
    return clippedStr;
}

const char* Utils::GetUTF8TextSubstring(const char *str, int offsetLength, int cutLength) {
    static const unsigned int maxUtfSize = 6; // 6 should be enough. utf-8 characters can have 1 to 6 bytes.

    if (!str) {
        return NULL;
    }

    i18n_uchar *uStr = UDup(str);
    int len = i18n_ustring_get_length(uStr);
    if (len < (offsetLength + cutLength)) {
        delete[] uStr;
        return NULL;
    }

    i18n_uchar *uRes = uStr + offsetLength;
    *(uRes + cutLength) = '\0';
    char *res = new char[i18n_ustring_get_length(uRes)*maxUtfSize + 1];
    i18n_ustring_copy_au(res, uRes);
    delete[] uStr;
    return res;
}

char* Utils::GetTimeShift(char* timeString) {
    char * res = NULL;
    if (timeString != NULL) {
            const char *token = "+-";
            char * tmp = strdup(timeString);

            res = strtok(tmp, token);

            res = strtok(NULL, token);

            res = strtok(NULL, token);

            res = strtok(NULL, token);

            res = SAFE_STRDUP(res);
            free(tmp);
    }

    return res;
}

char* Utils::GetCurrentTimeShift() {
    char time_shift[16] = {0};
    char * str = NULL;
    char * res = NULL;
    time_t local_time;
    struct tm *tm_localtime;

    time(&local_time);
    tm_localtime = localtime(&local_time);
    strftime(time_shift, sizeof(time_shift), "%z", tm_localtime);

    const char * token = "+-";

    str = strtok(time_shift, token);
    res = SAFE_STRDUP(str);

    return res;
}

double Utils::ConvertTimeToGMT(char *gotString, bool needTimeShift) {

    if (!gotString) {
        return 0.0;
    }

    char timeoffset[6] = {0};
    int hoursNorm = 0;
    int minutesNorm = 0;
    char hoursOff[3] = {0};
    char minutesOff[3] = {0};
    char tmp[5] = {0};

    static const char *timeZoneId = "GMT";
    i18n_uchar *ucharTimeZoneId;
    ucharTimeZoneId = (i18n_uchar*) calloc(strlen(timeZoneId) + 1, sizeof(i18n_uchar));
    if (ucharTimeZoneId == NULL) {
        return -1;
    }
    i18n_ustring_copy_ua(ucharTimeZoneId, timeZoneId);
    int timeZoneLen = i18n_ustring_get_length(ucharTimeZoneId);

    const char *locale = NULL;
    i18n_ulocale_get_default(&locale);

    i18n_ucalendar_h calendar;
    int ret = i18n_ucalendar_create(ucharTimeZoneId, timeZoneLen, locale, I18N_UCALENDAR_TRADITIONAL, &calendar);
    if (ret) {
        Log::error("Utils::ConvertTimeToGMT: i18n_ucalendar_create failed.");
        free(ucharTimeZoneId);
        return 0;
    }

    if(needTimeShift) {
        eina_strlcpy(timeoffset, (gotString+19), 6);
        // now we store offset in time like -0300
        eina_strlcpy(tmp, (gotString+20), 5);
        for (int i = 0; i < 2; ++i) {
            hoursOff[i] = tmp[i];
            minutesOff[i] = tmp[i + 2];
        }
        //now we have hoursOff = 03 and minutesOff = 00 (for example)
        hoursNorm = atoi(hoursOff);
        minutesNorm = atoi(minutesOff);
    }

    //now we have to parse time string to milliseconds
    if (gotString != NULL) {
        const char *token = "-T:+";
        char * copy = strdup(gotString);

        char * tmp = strtok(copy, token);
        int year = tmp != NULL ? atoi(tmp) : 0;

        tmp = strtok(NULL, token);
        int month = tmp != NULL ? atoi(tmp) : 0;

        tmp = strtok(NULL, token);
        int day = tmp != NULL ? atoi(tmp) : 0;

        tmp = strtok(NULL, token);
        int hour = tmp != NULL ? atoi(tmp) : 0;

        tmp = strtok(NULL, token);
        int minute = tmp != NULL ? atoi(tmp) : 0;

        tmp = strtok(NULL, token);
        int second = tmp != NULL ? atoi(tmp) : 0;

        free(copy);

        //at this point we create a calendar for another timezone if needed otherwise for ours
        if( needTimeShift ) {
            if(strstr(timeoffset, "+")) {
                i18n_ucalendar_set_date_time(calendar, year, month - 1, day, hour-hoursNorm, minute-minutesNorm, second);
            } else if (strstr(timeoffset, "-")) {
                i18n_ucalendar_set_date_time(calendar, year, month - 1, day, hour+hoursNorm, minute+minutesNorm,  second);
            }
        } else {
            i18n_ucalendar_set_date_time(calendar, year, month - 1, day, hour, minute, second);
        }
    }

    i18n_udate millisec;
    i18n_ucalendar_get_milliseconds(calendar, &millisec);

    i18n_ucalendar_destroy(calendar);
    free(ucharTimeZoneId);

    return (double) millisec;
}

void Utils::SetEntryGuideText(Evas_Object *entry, const char *formatPattern, const char *contentPart1, const char *contentPart2) {
    char *buf = WidgetFactory::WrapByFormat2( formatPattern, contentPart1, contentPart2 );
    if ( buf ) {
        elm_object_part_text_set( entry, "elm.guide", buf );
        delete[] buf;
    }
}

int Utils::GetLinesNumber(Evas_Object *entry) {
    Evas_Object* tb = elm_entry_textblock_get(entry);
    int lines = 0;
    if (tb) {
        for (int i = 0; i <= 100; i++) {
            int x, y, w, h;
            if (evas_object_textblock_line_number_geometry_get(tb, i, &x, &y, &w, &h)) {
                lines = i + 1;
            } else {
                break;
            }
        }
    }
    return lines;
}

bool Utils::IsTimeShiftNeeded(char *time)
{
    bool needTimeShift = false;

    char *timeShiftFromString = Utils::GetTimeShift(time);
    if (timeShiftFromString) {
        char *currentTimeShift = Utils::GetCurrentTimeShift();
        if (currentTimeShift) {
            needTimeShift = strcmp(timeShiftFromString, currentTimeShift);
            free(currentTimeShift);
        }
        free(timeShiftFromString);
    }
    return needTimeShift;
}

const char *Utils::CreateTimeWithShift(char *time, bool needShortFormat)
{
    bool needTimeShift = IsTimeShiftNeeded(time);
    double timeDouble = Utils::ConvertTimeToGMT(time, needTimeShift);

    if (needShortFormat) {
        return PresentTimeFromMilliseconds(timeDouble);
    } else {
        return PresentTimeFromMillisecondsGeneral(timeDouble, needTimeShift);
    }
}

void Utils::OpenProfileScreen(std::string id)
{
    ScreenBase *scr = nullptr;
    if (IsMe(id.c_str())) {
        scr = new OwnProfileScreen();
    } else {
        scr = new ProfileScreen(id.c_str());
    }
    Application::GetInstance()->AddScreen(scr);
}

bool Utils::IsVideoFormat(const char *filePath)
{
    if (filePath && *filePath) {
        const char* formatsSupported[] = {
                "3g2", "3gp", "3gpp", "asf", "avi", "dat", "divx", "dv", "f4v", "flv",
                "m2ts", "m4v", "mkv", "mod", "mov", "mp4", "mpe", "mpeg", "mpeg4", "mpg",
                "mts", "nsv", "ogm", "ogv", "qt", "tod", "ts", "vob", "wmv"
        };
        int numOfFormats = sizeof(formatsSupported) / sizeof(formatsSupported[0]);
        for (int i = 0; i < numOfFormats; ++i) {
            if (HasEnding(filePath, (char*)formatsSupported[i])) {
                return true;
            }
        }
    }
    return false;
}

bool Utils::HasEnding(std::string &fullString, std::string &ending) {
    if (fullString.length() >= ending.length()) {
        return (0 == fullString.compare (fullString.length() - ending.length(), ending.length(), ending));
    } else {
        return false;
    }
}

bool Utils::HasEnding(const char *fullString, const char *ending) {
    if (fullString && ending && *fullString && *ending) {
        int len1 = strlen(fullString);
        int len2 = strlen(ending);
        int offset = len1 - len2;
        return ((offset >= 0) && (0 == strncmp(fullString + offset, ending, len2)));
    }
    return false;
}

std::string Utils::FormatBigInteger(int count)
{
    char countStr[20];

    if (count < 1000) {    // 0...999
        Utils::Snprintf_s(countStr, 20, "%d", count);
    } else if (count < 100000) {    // 1K...99.9K
        if ((count % 1000) < 100) {
            Utils::Snprintf_s(countStr, 20, "%dK", count / 1000);    //omit meaningless '.0' part
        } else {
            Utils::Snprintf_s(countStr, 20, "%d.%dK", count / 1000, (count % 1000) / 100);
        }
    } else if (count < 1000000) {    // 100K...999K
        Utils::Snprintf_s(countStr, 20, "%dK", count / 1000);
    } else if (count < 100000000) {    //1M...99.9M
        if ((count % 1000000) < 100000) {
            Utils::Snprintf_s(countStr, 20, "%dM", count / 1000000);    //omit meaningless '.0' part
        } else {
            Utils::Snprintf_s(countStr, 20, "%d.%dM", count / 1000000, (count % 1000000) / 100000);
        }
    } else {    // 100M and bigger
        Utils::Snprintf_s(countStr, 20, "%dM", count / 1000000);
    }

    std::string countString = countStr;
    return countString;
}

bool Utils::CheckAccess(const char * path) {
    struct stat buf;
    unsigned int pathUID = 0;
    unsigned int pathGId = 0;
    unsigned int realOwnerUId = getuid();
    unsigned int realOwnerGId = getgid();
    if (stat(path, &buf) == 0) {
        pathUID = buf.st_uid;
        pathGId = buf.st_gid;
        Log::debug("CheckAccess(%s) - mode=%o; this path: UId=%o, GId=%o; realOwner: UId=%o, GId=%o",
                   path, buf.st_mode & (S_IRWXU | S_IRWXG | S_IRWXO), pathUID, pathGId, realOwnerUId, realOwnerGId);
    } else {
        Log::debug("CheckAccess(%s) - file with such path doesn't exist", path);
        return false;
    }
    return realOwnerGId == pathGId;
}

#define MAX_PHONE_NUMBER_LENGTH 30

bool Utils::IsPhoneNumber(char *numberStr) {  //allowed characters: digits and " ()+-"
    if (!numberStr) {
        return false;
    } else {
        int len = strlen(numberStr);
        if (len > MAX_PHONE_NUMBER_LENGTH) {
            return false;
        } else if (strspn(numberStr, " +-()0123456789") != len) {
            return false;
        }
    }
    return true;
}

char* Utils::CleanPhoneNumber(char *numberStr) {  //copy digits only, remove other characters
    char cleanNumber[MAX_PHONE_NUMBER_LENGTH + 1];
    char *src = numberStr;
    char *dst = cleanNumber;
    int len = strlen(numberStr);
    char oneChar;
    while (len--) {
        oneChar = *src++;
        if (oneChar >= '0' && oneChar <= '9') {
            *dst++ = oneChar;
        }
    }
    *dst = '\0';
    return strdup(cleanNumber);
}

int Utils::Memcpy_s(void *dst, size_t sizeDst, const void *src, size_t sizeSrc) {
    if(sizeDst >= sizeSrc && dst != NULL && src != NULL) {
        memcpy(dst, src, sizeSrc);
        return 1;
    } else {
        return 0;
    }
}

int Utils::Snprintf_s(char *buffer, size_t count, const char *format, ...) {

    va_list argptr;
    va_start(argptr, format);
    int res = vsnprintf(buffer, count, format, argptr);
    va_end(argptr);

    if (res > count) {
        res = 0;
    }

    assert(res > 0);
    return res;
}

char * Utils::GetRemaningPicturesNumber(unsigned int count){
//calculate remaining number of pictures
//if pictures more than 4 in post
    char *result = nullptr;
    int extraPicNum = count - 4;

    if (extraPicNum > 0) {
        result = new char[20];
        Snprintf_s(result, 20, "+%d", extraPicNum);
    }
    return result;
}

void Utils::RedrawEvasObject(Evas_Object* objectToRedraw) {
    assert(objectToRedraw);
    Evas_Coord w, h;
    evas_object_geometry_get(objectToRedraw, nullptr, nullptr, &w, &h);
    evas_object_resize(objectToRedraw, w, h-1);
    evas_object_resize(objectToRedraw, w, h);
}

char* Utils::ComputeHash(const char* string) {
    if (!string || (*string == '\0')) {
        return nullptr;
    }

    unsigned char digest[SHA_DIGEST_LENGTH] = {0,};
    char mdString[SHA_DIGEST_LENGTH * 2 + 1] = {0,};

    SHA1((unsigned char *)string, strlen(string), digest);

    for (int i = 0; i < SHA_DIGEST_LENGTH; i++) {
         sprintf(&mdString[i * 2], "%02x", digest[i]);
    }

    return SAFE_STRDUP(mdString);
}
