/*
 * DoubleSidedStack.cpp
 *
 *  Created on: Jul 6, 2015
 *      Author: Dmitry.Shcherbakov@SymphonyTeleca.com
 */

#include <tizen_type.h>
#include "DoubleSidedStack.h"

namespace Utils {

DoubleSidedStack::DoubleSidedStack()
{
    m_Head = NULL;
    m_Tail = NULL;
    m_Size = 0;
}

DoubleSidedStack::~DoubleSidedStack()
{
    Clear();
}

void DoubleSidedStack::PushToBegin( void *item )
{
    Node *node = new Node( item );
    if ( m_Head ) {
        node->next = m_Head;
        m_Head->prev = node;
    }
    m_Head = node;
    if ( m_Size == 0 ) {
        m_Tail = m_Head;
    }
    ++m_Size;
}

void DoubleSidedStack::PushToEnd( void *item )
{
    Node *node = new Node( item );
    if ( m_Tail ) {
        node->prev = m_Tail;
        m_Tail->next = node;
    }
    m_Tail = node;
    if ( m_Size == 0 ) {
        m_Head = m_Tail;
    }
    ++m_Size;
}

void* DoubleSidedStack::PopFromBegin()
{
    void *item = NULL;
    if ( m_Head ) {
        item = m_Head->data;
        Node *next = m_Head->next;
        if ( next ) {
            next->prev = NULL;
        }
        delete m_Head;
        m_Head = next;
        --m_Size;
        if ( m_Size == 0) {
            m_Tail = NULL;
        }
    }
    return item;
}

void* DoubleSidedStack::PopFromEnd()
{
    void *item = NULL;
    if ( m_Tail ) {
        item = m_Tail->data;
        Node *prev = m_Tail->prev;
        if ( prev ) {
            prev->next = NULL;
        }
        delete m_Tail;
        m_Tail = prev;
        --m_Size;
        if ( m_Size == 0) {
            m_Head = NULL;
        }
    }
    return item;
}

bool DoubleSidedStack::RemoveItem( void *itemToDelete, ItemsComparator comparator )
{
    for ( Node *node = m_Head; node; node = node->next ) {
        if ( comparator( itemToDelete, node->data ) ) {
            Node *next = node->next;
            if ( next ) {
                next->prev = node->prev;
            } else {
                m_Tail = node->prev;
            }
            Node *prev = node->prev;
            if ( prev ) {
                prev->next = node->next;
            } else {
                m_Head = node->next;
            }
            --m_Size;
            delete node;
            return true;
        }
    }
    return false;
}

bool DoubleSidedStack::IsPresentItem( void *itemToFind, ItemsComparator comparator )
{
    for ( Node *node = m_Head; node; node = node->next ) {
        if ( comparator( itemToFind, node->data ) ) {
            return true;
        }
    }
    return false;
}

void DoubleSidedStack::Clear()
{
    for ( Node *node = m_Head; node; node = node->next ) {
        delete node;
    }
    m_Head = NULL;
    m_Tail = NULL;
    m_Size = 0;
}

void * DoubleSidedStack::GetItem( void *itemToFind, ItemsComparator comparator )
{
    for ( Node *node = m_Head; node; node = node->next ) {
        if ( comparator( itemToFind, node->data ) ) {
            return node->data;
        }
    }
    return NULL;
}

void * DoubleSidedStack::GetItemByIndex(int index)
{
    if (!m_Size || index < 0 || index >= m_Size) {
        return NULL;
    }

    Node *node = NULL;
    if (index <= m_Size / 2) {
        int i;
        for ( node = m_Head, i = 0;
                node && i != index;
                node = node->next, i++ );
    }
    else {
        int i;
        for ( node = m_Tail, i = m_Size - 1;
                node && i != index;
                node = node->prev, i-- );
    }
    return node->data;
}

} /* namespace Utils */
