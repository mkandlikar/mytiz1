/*
 * jsonutilities.c
 *
 *  Created on: Apr 16, 2015
 *      Author: RUINMKOL
 */

#include "jsonutilities.h"
#include "Common.h"
#include "dlog.h"


/**
 * @brief This method should be used for converting JSON formatted text to JSON object,
 * which will be used for further parsing
 * @param[in] data - JSON Formatted text
 * @param[out] object - created JSON object
 * @return - JSON Parser. The client should take care about deleting this object after parsing is completed
 */
JsonParser* openJsonParser(const char * data, JsonObject ** object)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "jsonutilities::getJsonObject");

    if ( !data ) {
        return NULL;
    }

    JsonParser *jsonParser = json_parser_new ();
    GError *error = NULL;

    if ( jsonParser && json_parser_load_from_data (jsonParser, data, -1, &error) ) {
        if ( object != NULL ) {
            JsonNode* root = json_parser_get_root (jsonParser);
            if(root) {
                *object = json_node_get_object(root);
            }
        }
    } else {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Json parser error:%s", error->message);
        g_error_free (error);
    }

    return jsonParser;
}

char *GetStringFromJson(JsonObject *object, const char *member)
{
    if(object) {
        const char *string = json_object_get_string_member(object, member);
        if (string && strlen(string)) {
            return strdup(string);
        }
    }
    return NULL;
}

gint64 GetGint64FromJson( JsonObject *object, const char *member )
{
    if(object) {
        return json_object_get_int_member( object, member );
    }
    return 0;
}
