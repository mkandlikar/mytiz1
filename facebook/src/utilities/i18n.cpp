#include "i18n.h"

#include <memory>

#include <app_i18n.h>
#include <utils_i18n_ustring.h>

namespace i18n {

    u16string StdStringToU16String(const std::string& src) {
        std::unique_ptr<i18n_uchar[]> uStr(new i18n_uchar[src.length() + 1]);
        i18n_ustring_copy_ua(uStr.get(), src.c_str());
        return uStr.get();
    }

    std::string Localize(const char* msgId) {
        ASSERT_LOG(msgId, "You've passed nullptr as message id for localization");
        return _(msgId);
    }
}
