/*
 * curlutilities.c
 *
 *  Created on: Apr 14, 2015
 *      Author: RUINMKOL
 */

#include "curlutilities.h"

#include "Common.h"
#include "dlog.h"
#include "ErrorHandling.h"
#include "Utils.h"

#include <bundle.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>


#define CURL_RESPOND_BUFFER_INIT_SIZE 50*1024
#define CURL_RESPOND_BUFFER_STEP_SIZE 20*1024
/**
 * @brief This method should be used for sending HTTP GET requests via CURL library
 * @param[in] request - The full http request url
 * @param[in] respondBuffer - Memory buffer, where HTTP respond will be placed
 * @param[in] isCancelled - memory buffer, where cancell indicator is placed
 * @return - The code result of execution HTTP GET request
 */
CURLcode SendHttpGet(RequestUserData *contextData) {

    const char * request = contextData->mRequestUrl;
    struct MemoryStruct* respondBuffer = contextData->mResponseBuffer;
    bool * isCancelled = &contextData->mIsCancelled;
    bool isHeaderRequired = contextData->mIsHeaderReq;
    const char * eTags = contextData->mThrdEtagsHeader;

    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "SendHttpGet:%s", request);

    //Initialize the response member. These will contain the response received
    respondBuffer->memory = (char*) malloc(CURL_RESPOND_BUFFER_INIT_SIZE);  /* will be grown as needed by the realloc above */
    respondBuffer->memory_buffer_size = CURL_RESPOND_BUFFER_INIT_SIZE;
    *respondBuffer->memory = 0;
    respondBuffer->size = 0;    /* no data at this point */

    //Initialize the header members as well. These will be only required when headers are to be parsed
    respondBuffer->header = (char *)malloc(1);
    *respondBuffer->header = 0;
    respondBuffer->header_size = 0;

    CURL *curl = NULL;
    struct curl_slist *chunk = NULL;
    CURLcode res;

    curl = curl_easy_init();
    if (curl == NULL) {
        return CURLE_FAILED_INIT;
    }
    curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
    curl_easy_setopt(curl, CURLOPT_URL, request);
    curl_easy_setopt(curl, CURLOPT_USERAGENT,Utils::GetHTTPUserAgent());
    if(isHeaderRequired) {
        curl_easy_setopt(curl,CURLOPT_HEADERFUNCTION,&header_callback);
        curl_easy_setopt(curl, CURLOPT_HEADERDATA, (void *)respondBuffer);
    }
    //Check if etags custom header needs to be sent
    if(eTags != NULL)
    {
        char custom_etags_header[1024] = {0,};
        Utils::Snprintf_s(custom_etags_header, 1024,"If-None-Match: \"%s\" ",eTags);
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "custom_etags_header = %s", custom_etags_header);

        chunk = curl_slist_append(chunk, custom_etags_header);
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
    }
    curl_easy_setopt(curl, CURLOPT_HTTPGET, 1);

    /* send all data to this function  */
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &write_data);

    /* we pass our 'chunk' struct to the callback function */
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)respondBuffer);

    /* turn on progress reading*/
    if (isCancelled != NULL) {
        curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0);
        curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, &progress_callback);
        curl_easy_setopt(curl, CURLOPT_XFERINFODATA, (void * ) contextData);
    }

    /* get it! */
    res = curl_easy_perform(curl);

    if(res != CURLE_OK) {
        dlog_print(DLOG_ERROR, LOG_TAG_FACEBOOK, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(res));
    }

    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "SendHttpGet: Curl result with code = %d", res);

    /* cleanup curl stuff */
    curl_easy_cleanup(curl);

    if(chunk) {
        curl_slist_free_all(chunk);
    }

    return res;
}

/**
 * @brief This method should be used for sending HTTP POST requests via CURL library
 * @param[in] contextData - context
 * @return - The code result of execution HTTP GET request
 */

CURLcode SendHttpPost(RequestUserData *contextData) {
    const char * request = contextData->mRequestUrl;
    struct MemoryStruct* requestBuffer = contextData->mRequestBuffer;
    struct MemoryStruct* respondBuffer = contextData->mResponseBuffer;
    bool * isCancelled = &contextData->mIsCancelled;
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "SendHTTPPost:%s", request);
    respondBuffer->memory = (char*) malloc(CURL_RESPOND_BUFFER_INIT_SIZE);  /* will be grown as needed by the realloc above */
    respondBuffer->memory_buffer_size = CURL_RESPOND_BUFFER_INIT_SIZE;
    *respondBuffer->memory = 0;
    respondBuffer->size = 0;    /* no data at this point */

    CURL *curl = NULL;
    CURLcode res;

    curl = curl_easy_init();

    if (curl == NULL)
    {
        return CURLE_FAILED_INIT;
    }

    curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
    curl_easy_setopt(curl, CURLOPT_URL, request);
    curl_easy_setopt(curl, CURLOPT_HTTPPOST, 1);
    curl_easy_setopt(curl, CURLOPT_USERAGENT,Utils::GetHTTPUserAgent());
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "SendHTTPPost:%s User-Agent %s", request,Utils::GetHTTPUserAgent());

    if (requestBuffer != NULL)
    {
        curl_easy_setopt(curl, CURLOPT_POST, requestBuffer->size);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, requestBuffer->memory);
    }
    else
    {
        curl_easy_setopt(curl, CURLOPT_POST, 1);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "");
    }

    /* send all data to this function  */
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &write_data);

    /* we pass our 'chunk' struct to the callback function */
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)respondBuffer);

    /* turn on progress reading*/
    if (contextData != NULL)
    {
        curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0);
        curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, &progress_callback);
        curl_easy_setopt(curl, CURLOPT_XFERINFODATA, (void * ) contextData);
    }

    /* get it! */
    res = curl_easy_perform(curl);

    if(res != CURLE_OK) {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(res));
    }

    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "SendHttpPost: Curl result with code = %d", res);

    /* cleanup curl stuff */
    curl_easy_cleanup(curl);

    return res;

}

struct formdata_struct
{
    struct curl_httppost *formpost;
    struct curl_httppost *lastptr;

}typedef FormDataStruct;

void build_form_part_for_each_bundle_parameter(const char *key, const int type,
        const bundle_keyval_t *kv, void *user_data)
{
    void *ptr = NULL;
    unsigned int size = 0;
    FormDataStruct* formData = static_cast<FormDataStruct *> (user_data);

    if (type == BUNDLE_TYPE_STR)
    {
        bundle_keyval_get_basic_val((bundle_keyval_t *) kv, &ptr, &size);

        curl_formadd(&formData->formpost, &formData->lastptr, CURLFORM_COPYNAME,
                key, CURLFORM_COPYCONTENTS, ptr, CURLFORM_END);
    }
}

CURLcode SendHttpPostMultiPart(RequestUserData *contextData) {
    const char * request = contextData->mRequestUrl;
    const char * multipartAttach = contextData->mMultipartFileUploadName;
    bundle* paramsBundle = contextData->mBodyParamsBundle;
    struct MemoryStruct* respondBuffer = contextData->mResponseBuffer;
    bool * isCancelled = &contextData->mIsCancelled;

    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK,
            "SendHttpMultiPart, request = %s", request);

    //Initialize the response member. These will contain the response received
    respondBuffer->memory = (char*) malloc(1);  /* will be grown as needed by the realloc above */
    respondBuffer->memory_buffer_size = 1;
    *respondBuffer->memory = 0;
    respondBuffer->size = 0;    /* no data at this point */

    CURLcode result = CURLE_OK;
    CURLMcode mc = CURLM_OK; /* curl_multi_fdset() return code */
    CURL *curl;

    CURLM *multi_handle;
    int still_running;

    FormDataStruct formData;
    formData.formpost = NULL;
    formData.lastptr = NULL;

    struct curl_slist *headerlist = NULL;

    CURLFORMcode res = CURL_FORMADD_OK;
    if(multipartAttach) {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK,
                "SendHttpMultiPart, photo = %s", multipartAttach);

        /* Fill in the file upload field. This makes libcurl load data from
         the given file name when curl_easy_perform() is called. */
        res = curl_formadd(&formData.formpost, &formData.lastptr, CURLFORM_COPYNAME,
                "source", CURLFORM_FILE, multipartAttach, CURLFORM_END);
        CHECK_RET(res == CURL_FORMADD_OK, CURLE_FILE_COULDNT_READ_FILE);
    }

    if(paramsBundle) {
        //add the rest parts with parameters
        bundle_foreach(paramsBundle, &build_form_part_for_each_bundle_parameter,
                &formData);
    }

    curl = curl_easy_init();
    multi_handle = curl_multi_init();

    /* send all data to this function  */
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &write_data);

    /* we pass our 'chunk' struct to the callback function */
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void * )respondBuffer);

    curl_easy_setopt(curl, CURLOPT_USERAGENT,Utils::GetHTTPUserAgent());

    /* turn on progress reading*/
    if (contextData != NULL)
    {
        curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0);
        curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, &progress_callback);
        curl_easy_setopt(curl, CURLOPT_XFERINFODATA, (void * ) contextData);
    }

    /* initialize custom header list*/
    headerlist = curl_slist_append(headerlist,
            "Content-Type: multipart/form-data");
    if (curl && multi_handle)
    {

        /* what URL that receives this POST */
        curl_easy_setopt(curl, CURLOPT_URL, request);
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
        curl_easy_setopt(curl, CURLOPT_HTTPPOST, formData.formpost);

        CURLMcode curlMcode = curl_multi_add_handle(multi_handle, curl);

        curlMcode = curl_multi_perform(multi_handle, &still_running);

        do
        {
            struct timeval timeout;
            int rc; /* select() return code */

            fd_set fdread;
            fd_set fdwrite;
            fd_set fdexcep;
            int maxfd = -1;

            long curl_timeo = -1;

            FD_ZERO(&fdread);
            FD_ZERO(&fdwrite);
            FD_ZERO(&fdexcep);

            /* set a suitable timeout to play around with */
            timeout.tv_sec = 1;
            timeout.tv_usec = 0;

            curlMcode = curl_multi_timeout(multi_handle, &curl_timeo);
            if (curl_timeo >= 0)
            {
                timeout.tv_sec = curl_timeo / 1000;
                if (timeout.tv_sec > 1)
                    timeout.tv_sec = 1;
                else
                    timeout.tv_usec = (curl_timeo % 1000) * 1000;
            }

            /* get file descriptors from the transfers */
            mc = curl_multi_fdset(multi_handle, &fdread, &fdwrite, &fdexcep,
                    &maxfd);

            if (mc != CURLM_OK)
            {
                fprintf(stderr, "curl_multi_fdset() failed, code %d.\n", mc);
                dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK,
                        "curl_multi_fdset() failed, code %d.\n", mc);
                break;
            }

            /* On success the value of maxfd is guaranteed to be >= -1. We call
             select(maxfd + 1, ...); specially in case of (maxfd == -1) there are
             no fds ready yet so we call select(0, ...) --or Sleep() on Windows--
             to sleep 100ms, which is the minimum suggested value in the
             curl_multi_fdset() doc. */

            if (maxfd == -1)
            {
#ifdef _WIN32
                sleep(1);
                rc = 0;
#else
                /* Portable sleep for platforms other than Windows. */
                struct timeval wait =
                {   0, 100 * 1000}; /* 100ms */
                rc = select(0, NULL, NULL, NULL, &wait);
#endif
            }
            else
            {
                /* Note that on some platforms 'timeout' may be modified by select().
                 If you need access to the original value save a copy beforehand. */
                rc = select(maxfd + 1, &fdread, &fdwrite, &fdexcep, &timeout);
            }

            switch (rc)
            {
            case -1:
                /* select error */
                dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Error");
                break;
            case 0:
            default:
                /* timeout or readable/writable sockets */
                curl_multi_perform(multi_handle, &still_running);
                //dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "running: %d!\n", still_running);
                break;
            }
        } while (still_running);

        /* get error code and cleanup */
        CURLMsg *m = NULL;
        do {
          int msgq = 0;
          m = curl_multi_info_read(multi_handle, &msgq);
          if(m && (m->msg == CURLMSG_DONE)) {
            CURL *e = m->easy_handle;
            result = m->data.result;
          //  transfers--;
            curl_multi_remove_handle(multi_handle, e);
            curl_easy_cleanup(e);
          }
        } while(m);

        curl_multi_cleanup(multi_handle);

        /* then cleanup the formpost chain */
        curl_formfree(formData.formpost);

        /* free slist */
        curl_slist_free_all(headerlist);
    }

    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK,
            "SendHttpMultipart: Curl result with code = %d", mc);
    return result;
}

/**
 * @brief This method should be used for sending HTTP DELETE requests via CURL library
 * @param[in] contextData - Context data
 * @return - The code result of execution HTTP GET request
 */
CURLcode SendHttpDelete(RequestUserData *contextData)
{
    const char * request = contextData->mRequestUrl;
    struct MemoryStruct* respondBuffer = contextData->mResponseBuffer;
    bool * isCancelled = &contextData->mIsCancelled;

    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "SendHttpDelete:%s", request);
    respondBuffer->memory = (char*) malloc(1);  /* will be grown as needed by the realloc above */
    respondBuffer->memory_buffer_size = 1;
    *respondBuffer->memory = 0;
    respondBuffer->size = 0;    /* no data at this point */

    CURL *curl = NULL;
    CURLcode res;

    curl = curl_easy_init();

    if (curl == NULL)
    {
        return CURLE_FAILED_INIT;
    }

    curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
    curl_easy_setopt(curl, CURLOPT_URL, request);
    curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST,"DELETE");
    curl_easy_setopt(curl, CURLOPT_USERAGENT,Utils::GetHTTPUserAgent());

    /* send all data to this function  */
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &write_data);

    /* we pass our 'chunk' struct to the callback function */
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)respondBuffer);

    /* turn on progress reading*/
    if (contextData != NULL)
    {
        curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0);
        curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, &progress_callback);
        curl_easy_setopt(curl, CURLOPT_XFERINFODATA, (void * ) contextData);
    }

    /* get it! */
    res = curl_easy_perform(curl);

    if(res != CURLE_OK) {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(res));
    }

    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "SendHttpDelete: Curl result with code = %d", res);

    /* cleanup curl stuff */
    curl_easy_cleanup(curl);

    return res;

}

/**
 * @brief This is a callback passed to CURL library which will be called to store the output
 * @param[in] contents - content
 * @param[in] size - block size
 * @param[in] nmemb - blocks cound
 * @param[in] userp - user data passed to callback
 * @return
 */
size_t write_data(void *contents, size_t size, size_t nmemb, void *userp)
{
  size_t realsize = size * nmemb;
  struct MemoryStruct *mem = (struct MemoryStruct *)userp;

  if(mem->memory_buffer_size < (mem->size + realsize + 1)) {
      mem->memory_buffer_size = mem->size + realsize + CURL_RESPOND_BUFFER_STEP_SIZE + 1;
      mem->memory = (char*) realloc(mem->memory, mem->memory_buffer_size);
      //dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "mem->memory_buffer_size = %d \n", mem->memory_buffer_size);
      if(mem->memory == NULL) {
          /* out of memory! */
          dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "not enough memory (realloc returned NULL)");
          return 0;
      }
  }

  int res = Utils::Memcpy_s(&(mem->memory[mem->size]), mem->memory_buffer_size, contents, realsize);
  assert(res);
  mem->size += realsize;
  mem->memory[mem->size] = 0;
  //dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Response of Http Request -> %s \n", &mem->memory[mem->size-realsize]);

  return realsize;
}

/**
 * @brief This is callback for parsing the header ETAGS. Curl calls this function for each header once
 */
size_t header_callback(char *contents,size_t size,size_t nMemb,void *userData)
{
    size_t realSize = size * nMemb;
    char * tmpBuf = (char *)malloc(realSize + 1);
    if(tmpBuf == NULL) {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Not Enough memory malloc failed for Header storage");
        return 0;
    }
    int res = Utils::Memcpy_s(tmpBuf, realSize + 1, contents, realSize);
    assert(res);
    tmpBuf[realSize] = 0;
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Header: size =  %d; value = %s", realSize, tmpBuf);
    //There are just 2 headers that we are interested in Etags and HttP 304 Not Modified. These are exclusive
    //Headers and only one of these shall be present in the response. Both cannot come together. Since these are
    //exclusive we shall use the same data structure to save them. This shall be then parsed and processed by the
    //application logic.
    if(Utils::IsHeader_Etag(tmpBuf)) //The lenght of ETag: is 5
    {
        struct MemoryStruct *mem = (struct MemoryStruct *)userData;
        int size = mem->header_size + realSize + 1;
        mem->header = (char*) realloc(mem->header, size);
        if(mem->header == NULL) {
            /* out of memory! */
            dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Not Enough memory reallocation failed for Header storage");
        } else {
            int res = Utils::Memcpy_s(&(mem->header[mem->header_size]), size, contents, realSize);
            assert(res);
            mem->header_size += realSize;
            mem->header[mem->header_size] = 0;
            dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "ETAGS Header %s", mem->header);
        }
    }
    else if(Utils::IsHeader_304NotModified(tmpBuf)) //The lenght of header string is 25
    {
        struct MemoryStruct *mem = (struct MemoryStruct *)userData;
        int size = mem->header_size + realSize + 1;
        mem->header = (char*) realloc(mem->header, size);
        if(mem->header == NULL) {
            /* out of memory! */
            dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Not Enough memory reallocation failed for Header storage");
        } else {
            int res = Utils::Memcpy_s(&(mem->header[mem->header_size]), size, contents, realSize);
            assert(res);
            mem->header_size += realSize;
            mem->header[mem->header_size] = 0;
            dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "304 Header %s", mem->header);
        };
    }
    else {
      //  dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Ignoring Header %s", tmpBuf);
    }
    free(tmpBuf);
    return realSize;
}

/**
 * @brief This is callback for indicating request progress
 */
int progress_callback(void *clientp,   curl_off_t dltotal,   curl_off_t dlnow,   curl_off_t ultotal,   curl_off_t ulnow)
{
    int ret = 0;
    RequestUserData *ud = static_cast<RequestUserData *>(clientp);
    if(ud) {
        ret = (ud->mIsCancelled) ? 1 : 0;
        Sptr<GraphRequest> graphRequest = ud->mParentRequest;
        if (graphRequest && !graphRequest->IsProgressFunctionUsed() && !ud->mIsCancelled) {
            graphRequest->OnProgress(dltotal, dlnow, ultotal, ulnow);
        }
    }
    return ret;
}
