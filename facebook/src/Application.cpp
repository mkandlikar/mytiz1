#include "ActionBase.h"
#include "AccountInformationScreen.h"
#include "AlbumsProvider.h"
#include "Application.h"
#include "BriefHomeProvider.h"
#include "CacheManager.h"
#include "CheckInProvider.h"
#include "ChooseFriendProvider.h"
#include "CommentWidgetFactory.h"
#include "Common.h"
#include "Config.h"
#include "ConnectivityManager.h"
#include "ContactSynchronizer.h"
#include "EditHistoryProvider.h"
#include "ErrorHandling.h"
#include "EventBatchProvider.h"
#include "EventGuestListFactory.h"
#include "FbNotificationRecv.h"
#include "FriendBirthDayProvider.h"
#include "FriendRequestsBatchProvider.h"
#include "FriendRequestsProvider.h"
#include "FriendSearchProvider.h"
#include "OwnFriendsProvider.h"
#include "GroupFriendsProvider.h"
#include "GroupProvider.h"
#include "HomeProvider.h"
#include "LocationManager.h"
#include "Log.h"
#include "LoginScreen.h"
#include "MainScreen.h"
#include "MutualFriendsList.h"
#include "NotificationAbstractDataProvider.h"
#include "NotificationProvider.h"
#include "OperationManager.h"
#include "PhotoGalleryProvider.h"
#include "PhotosOfYouProvider.h"
#include "PlacesProvider.h"
#include "PostComposerScreen.h"
#include "PostScreen.h"
#include "PrivacyOptionsData.h"
#include "OwnProfileScreen.h"
#include "ProfileScreen.h"
#include "ScreenBase.h"
#include "SearchProvider.h"
#include "SelectAlbumProvider.h"
#include "SelectedMediaList.h"
#include "SettingsScreen.h"
#include "SignUpInProgressScreen.h"
#include "SubscribersProvider.h"
#include "SuggestedFriendsProvider.h"
#include "TrayNotificationsManager.h"
#include "UIRes.h"
#include "UploadsProvider.h"
#include "UsersDetailInfoProvider.h"
#include "UsersByIdProvider.h"
#include "Utils.h"
#include "WebViewScreen.h"
#include "WidgetFactory.h"

#include <app_manager.h>
#include <app_preference.h>
#include <badge.h>
#include <curl/curl.h>
#include <sound_manager.h>
#include <system_info.h>

#define APP_CONTROL_ACCOUNT_OPERATION_VIEW "http://tizen.org/appcontrol/operation/account/configure"
#define APP_CONTROL_ACCOUNT_OPERATION_SIGNIN "http://tizen.org/appcontrol/operation/account/add"
#define WINDOW_PREFERED_ENGINE "opengl_x11"

char Application::mThemeEdjPath[PATH_MAX] = {0};
char Application::mEdjPath[PATH_MAX] = {0};
int Application::device_width = 0;
int Application::device_height = 0;

Application::Application() :  mDataService(nullptr), mLocationManager(nullptr), mSuspendedAppControl(nullptr),
        mContactSynchronizer(nullptr), mScreenModeTimer(nullptr), mPackageManagerList(nullptr), mModelName(nullptr), mTranslatableFields(nullptr)

{
    char *ui_mode = nullptr;
    if (PREFERENCE_ERROR_NONE == preference_get_string("fbapp_ui_mode", &ui_mode)) {
        if (ui_mode && !strcmp(ui_mode, "without_ui")) {
            mNoUIMode = true;
            preference_set_string("fbapp_ui_mode", "");
            free(ui_mode);
            return;
        }
    }
    free(ui_mode);
    mNoUIMode = false;

    int ret = system_info_get_platform_string("tizen.org/system/model_name", (char **)&mModelName);
    if (ret == SYSTEM_INFO_ERROR_NONE) {
        Log::info(LOG_FACEBOOK, "Model name: %s", mModelName);
    }

    char *platformVersion_string = nullptr;
    mPlatformVersion = 0;
    //Get version string in format "(major).(minor)" and store the major part as an integer value.
    ret = system_info_get_platform_string("http://tizen.org/feature/platform.core.api.version", &platformVersion_string);
    if (ret == SYSTEM_INFO_ERROR_NONE &&
        platformVersion_string && sscanf(platformVersion_string, "%d.%*d", &mPlatformVersion) == 1) {
        Log::info(LOG_FACEBOOK, "Platform_version: %s -> %d", platformVersion_string, mPlatformVersion);
    }
    free(platformVersion_string);

    mScreenArray = eina_array_new(5);

    mThemeEdjPath[0] = 0;
    mEdjPath[0] = 0;
    char *res_path = app_get_resource_path();
    if (res_path) {

        int ret = system_info_get_platform_int("http://tizen.org/feature/screen.width", &device_width);
        if (ret == SYSTEM_INFO_ERROR_NONE) {
            switch (device_width) {
            case 480:
                Utils::Snprintf_s(mEdjPath, (int)PATH_MAX, "%s%s", res_path, APP_EDJ_480P_FILE);
                UIRes::GetInstance()->Init480();
                Utils::Snprintf_s(mThemeEdjPath, (int)PATH_MAX, "%s%s", res_path, APP_EDJ_480P_THEMES_FILE);
                break;
            case 720:
                Utils::Snprintf_s(mEdjPath, (int)PATH_MAX, "%s%s", res_path, APP_EDJ_720P_FILE);
                UIRes::GetInstance()->Init720();
                Utils::Snprintf_s(mThemeEdjPath, (int)PATH_MAX, "%s%s", res_path, APP_EDJ_720P_THEMES_FILE);
                break;
            default:
                break;
            }
            system_info_get_platform_int("http://tizen.org/feature/screen.height", &device_height);
        }
        free(res_path);
    }

    Init();

    /* Initialize libcurl */
    curl_global_init(CURL_GLOBAL_ALL);

    mContactSynchronizer = new ContactSynchronizer();

    AddPackageManagerListener();
    ConfigureSoundManager();// looks like sound manager should be configured one time on start
}

Application * Application::GetInstance()
{
    static Application *app = new Application();
    return app;
}

Application::~Application() {
    free ((void *)mModelName);

    if (mNoUIMode) {
        return;
    }

    /* we're done with libcurl, so clean it up */
    app_control_destroy(mSuspendedAppControl);
    if(mScreenArray) {
        eina_array_free(mScreenArray); mScreenArray = NULL;
    }
    curl_global_cleanup();

    if (mScreenModeTimer)
        ecore_timer_del(mScreenModeTimer);

    delete mEmergencyLogoutRequest;
}

void Application::Init() {
    static app_event_handler_h handlers[5] = {NULL, };

    ui_app_add_event_handler(&handlers[APP_EVENT_LOW_BATTERY], APP_EVENT_LOW_BATTERY, ui_app_low_battery, this);
    ui_app_add_event_handler(&handlers[APP_EVENT_LOW_MEMORY], APP_EVENT_LOW_MEMORY, ui_app_low_memory, this);
    ui_app_add_event_handler(&handlers[APP_EVENT_DEVICE_ORIENTATION_CHANGED], APP_EVENT_DEVICE_ORIENTATION_CHANGED, ui_app_orient_changed, this);
    ui_app_add_event_handler(&handlers[APP_EVENT_LANGUAGE_CHANGED], APP_EVENT_LANGUAGE_CHANGED, ui_app_lang_changed, this);
    ui_app_add_event_handler(&handlers[APP_EVENT_REGION_FORMAT_CHANGED], APP_EVENT_REGION_FORMAT_CHANGED, ui_app_region_changed, this);

    //TODO: Commenting this as it's causing the application to crash when debugging with gdb
    //elm_config_cache_flush_enabled_set(EINA_TRUE); // Cache flush by platform enabled
}

int Application::Run(int argc, char *argv[])
{
    CacheManager::GetInstance().Init();
    ConnectivityManager::Singleton().Init();
    ui_app_lifecycle_callback_s event_callback = {0,};

    event_callback.create = app_create;
    event_callback.terminate = app_terminate;
    event_callback.pause = app_pause;
    event_callback.resume = app_resume;
    event_callback.app_control = app_control;

    return ui_app_main(argc, argv, &event_callback, this);
}

void Application::ui_app_lang_changed(app_event_info_h event_info, void *user_data)
{
    /*APP_EVENT_LANGUAGE_CHANGED*/
    char *locale = NULL;
    system_settings_get_value_string(SYSTEM_SETTINGS_KEY_LOCALE_LANGUAGE, &locale);
    elm_language_set(locale);
    i18n_ulocale_set_default(locale);
    free(locale);
    Application *me = static_cast<Application *>(user_data);
    me->UpdateTranslations();
    AppEvents::Get().Notify(eLANGUAGE_CHANGED);
    return;
}

void
Application::ui_app_orient_changed(app_event_info_h event_info, void *user_data)
{
//AAA We don't support auto-rotation.
//AAA     app->SetManualRotation();
    /*APP_EVENT_DEVICE_ORIENTATION_CHANGED*/
    return;
}

void
Application::ui_app_region_changed(app_event_info_h event_info, void *user_data)
{
    /*APP_EVENT_REGION_FORMAT_CHANGED*/
}

void
Application::ui_app_low_battery(app_event_info_h event_info, void *user_data)
{
    /*APP_EVENT_LOW_BATTERY*/
}

void
Application::ui_app_low_memory(app_event_info_h event_info, void *user_data)
{
    /*APP_EVENT_LOW_MEMORY*/
}


bool
Application::app_create(void *data)
{
    /* Hook to take necessary actions before main event loop starts
        Initialize UI resources and application's data
        If this function returns true, the main loop of application starts
        If this function returns false, the application is terminated */
    Application *app = static_cast<Application*>(data);
    if (app->mNoUIMode) {
        unsigned int count;
        if (BADGE_ERROR_NONE == badge_get_count(PACKAGE, &count)) {
            badge_set_count(PACKAGE, count + 1);
            Log::debug(LOG_FACEBOOK_NOTIFICATION, "Application:app_create() increased badge till %d", count+1);
        }
        return false;
    }

    app->ReStartDataService();
    app->mDataService = new DataServiceProtocol();
#ifdef DATA_SERVICE_TEST
    MessageId id = 0;
    mDataService->DownloadImage(app, "https://scontent.xx.fbcdn.net/hphotos-xpf1/v/t1.0-9/310606_3980192736107_1618807629_n.jpg?oh=a7d1e6ad64ae52f24b70e3a48474b2d5&oe=55C5108F", id);
    mDataService->DownloadImage(app, "https://fbcdn-sphotos-f-a.akamaihd.net/hphotos-ak-xpt1/v/t1.0-9/s720x720/11146573_788724361196545_6421064143521601848_n.jpg?oh=8f3063f8cb127c510287bedb8f3442ea&oe=56046EBA&__gda__=1440135836_b743581953c6dc0c1c0d45daaf112f1f", id);
    mDataService->DownloadImage(app, "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xfa1/v/t1.0-1/c22.22.271.271/s50x50/284329_10150242220088240_2150878_n.jpg?oh=99d0b09d0e6667a69fbd7695ae8aa862&oe=55C6F4A9&__gda__=1440310501_2eeb53b2170d5662f876695481c93679", id);
    //dsp->StopDataService();
#endif

    elm_theme_extension_add(NULL, app->mThemeEdjPath);
    app->mPrivacyOptionsData = new PrivacyOptionsData;
    app->CreateBaseGUI();
    ecore_thread_max_set(8);
    app->mDataService->ClearOldUserData();
    return true;
}

bool Application::IsRTLLanguage() {
//    char *locale = nullpt;
//    system_settings_get_value_string(SYSTEM_SETTINGS_KEY_LOCALE_LANGUAGE, &locale);
//    free(locale);
    // This very simple solution seems to be ok, but if it will not work correctly please replace it with solution based on approach above.
    return elm_config_mirrored_get();
}

bool Application::DoHandleAppControl(app_control_h app_control) {
    bool isHandled = true;
    int ret = 0;
    char *operation = NULL;
    char *uri = NULL;
    char *mimeType = NULL;
    Log::info("DoHandleAppControl() called");
    ret = app_control_get_operation(app_control, &operation);
    char *thumbNailPath = NULL;

    if (APP_CONTROL_ERROR_NONE != ret) {
        Log::error("app_control_get_operation() Failed");
    }
    else {
        if (ScreenBase::SID_CAMERA_ROLL == GetTopScreenId()) {
            RemovePostSimplePickerScreen();
        }
        Log::debug("app_control_get_operation() Success & the operation is %s", operation);
        if (0 == strcmp(operation, APP_CONTROL_OPERATION_DEFAULT)) {
            //The application is launched by the user so do nothing special
        } else if (!mDataService->getLoginState() && //login is in progress
                (0 == strcmp(operation, APP_CONTROL_OPERATION_MAIN) ||
                0 == strcmp(operation, APP_CONTROL_OPERATION_SHARE_TEXT) ||
                0 == strcmp(operation, APP_CONTROL_OPERATION_SHARE) ||
                0 == strcmp(operation, APP_CONTROL_OPERATION_MULTI_SHARE))) {
            isHandled = false;
        } else if (0 == strcmp(operation, APP_CONTROL_OPERATION_MAIN)) {
            //The application is launched by an external sharing application and the user needs to share User Profile from Contacts etc.
            app_control_get_uri(app_control, &uri);

            if(!Config::GetInstance().GetAccessToken().empty()) {
                Log::debug("app_control_get_operation() Success & the operation is %s :: uri is %s", operation, uri);
                if (uri) {
                    ScreenBase* scr = Utils::IsMe(uri) ? static_cast<ScreenBase*>(new OwnProfileScreen()) : static_cast<ScreenBase*>(new ProfileScreen(uri));
                    scr->SetLaunchedByAppControl();
                    AddScreen(scr);
                } else {
                    Log::info("This is normal Wake up of the application");
                }
            } else {
                Log::info("No Access Token. Normal Processing shall follow");
                isHandled = false;
            }
        }
        //Share Text
        else if (0 == strcmp(operation, APP_CONTROL_OPERATION_SHARE_TEXT)) {
            Log::info("Share Text App Control called");
            //Remove any existing Composer screen
            RemoveOldPostComposerScreen();
            //Do nothing but just open the PostComposer Screen in case if the app has valid token
            if(!Config::GetInstance().GetAccessToken().empty()) {
                //Get the extra data sent by the browser
                char *extraDataRcvdURI = NULL;
                int ret = app_control_get_extra_data(app_control, APP_CONTROL_DATA_TEXT, &extraDataRcvdURI);
                if ((APP_CONTROL_ERROR_NONE == ret) && extraDataRcvdURI) {
                    Log::debug("Application::DoHandleAppControl() URI is %s. ", extraDataRcvdURI);
                    //If the URI received starts with http: or www then it is weburl
                    char *extraDataRcvdTitle = NULL;
                    ret = app_control_get_extra_data(app_control, APP_CONTROL_DATA_SUBJECT, &extraDataRcvdTitle);
                    if ((APP_CONTROL_ERROR_NONE == ret) && extraDataRcvdTitle) {
                        Log::debug("Application::DoHandleAppControl() title is %s.", extraDataRcvdTitle);
                        //Create the post composer screen and show the title / URI card
                        PostComposerScreen* postComposer = new PostComposerScreen(NULL, eSHARELINKWEB, NULL, NULL, extraDataRcvdURI, NULL, true, extraDataRcvdTitle);
                        postComposer->SetLaunchedByAppControl();
                        AddScreen(postComposer);
                    } else {
                        //An image or multiple images may be shared along with the text content.
                        char **path_array = NULL;
                        int count = 0;
                        app_control_get_extra_data_array(app_control, APP_CONTROL_DATA_PATH, &path_array, &count);
                        char *file_path;
                        for (int i = 0; i < count; i++) {
                            file_path = path_array[i];
                            if (strstr(file_path, ".jpg") || strstr(file_path, ".png")) {
                                // Store paths to thumbnails for each image if available,
                                //   and store them in Eina_array as well as paths to images themselves.
                                Utils::GetThumbNailPathforUsingPathFilter(file_path, &thumbNailPath);
                                PostComposerMediaData * share_image = new PostComposerMediaData(file_path,
                                    (thumbNailPath ? thumbNailPath : file_path), MEDIA_CONTENT_TYPE_IMAGE, NULL, 0, true);
                                SelectedMediaList::AddItem(share_image);  //add to the selected media list
                            }
                        }

                        PostComposerScreen* postComposer = new PostComposerScreen(NULL, eSHARETEXTPICTURES, NULL, NULL, NULL, NULL, true, NULL);
                        postComposer->SetLaunchedByAppControl();
                        postComposer->SetEntryText(extraDataRcvdURI);
                        AddScreen(postComposer);
                    }
                    free(extraDataRcvdTitle);
                } else {
                    //Just open the composer so that user can type in the contents
                    PostComposerScreen* postComposer = new PostComposerScreen(NULL, eDEFAULTAPPTEXTSHARE, NULL, NULL, NULL, NULL, true);
                    postComposer->SetLaunchedByAppControl();
                    AddScreen(postComposer);
                }
                free(extraDataRcvdURI);
            } else {
                //No Valid Access Token fall through to open login screen
                Log::error("No Access Token. Normal Processing shall follow");
                isHandled = false;
            }
        }
        //Share image/videos
        else if (0 == strcmp(operation, APP_CONTROL_OPERATION_SHARE)) {
            //The application is launched by an external sharing application and the user needs to share images etc.

            RemoveOldPostComposerScreen();

            app_control_get_uri(app_control, &uri);
            app_control_get_mime(app_control, &mimeType);
            char* file_path = NULL;
            if (uri != NULL) {
                if (strstr(uri, "file://")) {
                    file_path = strdup(uri + 7); // The string "file://" has 7 characters so start the copy after that
                    Log::debug("Removed first few characters %s", file_path);
                } else {
                    file_path = strdup(uri);
                    Log::debug("FIle path correct from URI %s", file_path);
                }
            }

            Log::debug("app_control_get_operation() Success & the operation is %s :: uri is %s :: mime is %s", operation, uri + 7, mimeType);
            if (!Config::GetInstance().GetAccessToken().empty()) {
                Log::info("Access Token Found. Normal Processing shall follow %s", Config::GetInstance().GetAccessToken().c_str());
                //                    app->RemoveAllScreensAndNfItemsFromNaviframe();

                if (mimeType == NULL) {
                    //We are not sure which file type this is, so cannot call the composer screen. It is seen that at times the platform sends operation type
                    //as share without the mime type. So this is just a defensive check on this
                    //For video's the platform is sometimes return mp4 while at other times it is returning NULL
                    CHECK_RET(file_path, false);
                    Log::debug("NULL MIME TYPE FOR Path = %s,NULL mime type received, check if it is video", file_path);
                    if (Utils::IsVideoFormat(file_path)) {
                        Log::debug("NULL MIME TYPE FOR Path = %s,assume video", file_path);
                        Utils::GetThumbNailPathforUsingPathFilter(file_path, &thumbNailPath);
                        if (thumbNailPath) {
                            PostComposerMediaData * share_video = new PostComposerMediaData(file_path, thumbNailPath, MEDIA_CONTENT_TYPE_VIDEO, NULL, 0, true);

                            //Add the share_video to the selectedmedialist
                            SelectedMediaList::AddItem(share_video);
                            PostComposerScreen* postComposer = new PostComposerScreen(NULL, eDEFAULT, NULL, NULL, NULL, NULL, true, (char*)"IDS_NOTIF_MSG_SHARE_VIDEO");
                            postComposer->SetLaunchedByAppControl();
                            AddScreen(postComposer);
                        }
                    } else if (strstr(file_path, ".jpg") || strstr(file_path, ".png")) {
                        Log::debug("NULL MIME TYPE FOR Path = %s,assume picture", file_path);
                        Utils::GetThumbNailPathforUsingPathFilter(file_path, &thumbNailPath);
                        if (thumbNailPath) {
                            PostComposerMediaData * share_image = new PostComposerMediaData(file_path, thumbNailPath, MEDIA_CONTENT_TYPE_IMAGE, NULL, 0, true);

                            //Add the share_image to the selected medialist
                            SelectedMediaList::AddItem(share_image);
                            PostComposerScreen* postComposer = new PostComposerScreen(NULL, eDEFAULT, NULL, NULL, NULL, NULL, true, (char*)"IDS_NOTIF_MSG_SHARE_PHOTO");
                            postComposer->SetLaunchedByAppControl();
                            AddScreen(postComposer);
                        }
                    }

                } else if (strstr(mimeType, "image") && file_path) {
                    //Create a new PostComposerMediaData type object. This object shall be used by the post composer screen to
                    //compose the screen.
                    //share only if it is an image as the video upload is currently being added
                    Utils::GetThumbNailPathforUsingPathFilter(file_path, &thumbNailPath);
                    if (thumbNailPath) {
                        PostComposerMediaData * share_image = new PostComposerMediaData(file_path, thumbNailPath, MEDIA_CONTENT_TYPE_IMAGE, NULL, 0, true);

                        //Add the share_image to the selected medialist
                        SelectedMediaList::AddItem(share_image);
                        PostComposerScreen* postComposer = new PostComposerScreen(NULL, eDEFAULT, NULL, NULL, NULL, NULL, true, (char*)"IDS_NOTIF_MSG_SHARE_PHOTO");
                        postComposer->SetLaunchedByAppControl();
                        AddScreen(postComposer);
                    }
                } else if (strstr(mimeType, "video") && file_path) {
                    Utils::GetThumbNailPathforUsingPathFilter(file_path, &thumbNailPath);
                    if (thumbNailPath) {
                        PostComposerMediaData * share_video = new PostComposerMediaData(file_path, thumbNailPath, MEDIA_CONTENT_TYPE_VIDEO, NULL, 0, true);

                        //Add the share_video to the selectedmedialist
                        SelectedMediaList::AddItem(share_video);
                        PostComposerScreen* postComposer = new PostComposerScreen(NULL, eDEFAULT, NULL, NULL, NULL, NULL, true, (char*)"IDS_NOTIF_MSG_SHARE_VIDEO");
                        postComposer->SetLaunchedByAppControl();
                        AddScreen(postComposer);
                    }
                }
                // app_control_destroy() is causing crash with the firmware version : OIC.
                // Commenting this call to fix this issue.This needs to be revisited Later.
                //app_control_destroy(app_control);
            } else {
                isHandled = false;
                Log::error("No Access Token. Normal Processing shall follow");
            }
        }                //End of Share

        else if (0 == strcmp(operation, APP_CONTROL_OPERATION_MULTI_SHARE)) {
            //This is a multi share operation. The user needs to share multiple images. The path of individual images would be received in the
            //data array. We only support share of multiple images (Multiple sharing of videos
            char **path_array = NULL;
            int length = 0;

            RemoveOldPostComposerScreen();

            app_control_get_extra_data_array(app_control, APP_CONTROL_DATA_PATH, &path_array, &length);

            if(!Config::GetInstance().GetAccessToken().empty() && length) {
                Log::info("Access Token Found.Multi Sharing Processing shall follow");

                bool ShowMaxPhotosWarn = false;
                if (length > CAMERA_ROLL_MAX_SELECTED_PHOTOS) {
                    length = CAMERA_ROLL_MAX_SELECTED_PHOTOS;
                    ShowMaxPhotosWarn = true;
                }
                // Get the Thumb Nail path for each image selected & store them in Eina_arry.
                // Also store the selected images path in Eina_array
                for (int i = 0; i < length; i++) {
                    Log::debug("Path of the ImageFile is %s", path_array[i]);
                    Utils::GetThumbNailPathforUsingPathFilter(path_array[i], &thumbNailPath);
                    if (thumbNailPath) {
                        PostComposerMediaData * share_image = new PostComposerMediaData(path_array[i], thumbNailPath, MEDIA_CONTENT_TYPE_IMAGE, NULL, 0, true);

                        //Add the share_image to the selectedmedialist
                        SelectedMediaList::AddItem(share_image);
                    }
                }
                //Create the post composer screen and show it
                PostComposerScreen* postComposer = new PostComposerScreen(NULL, eDEFAULT, NULL, NULL, NULL, NULL, true, (char*)"IDS_NOTIF_MSG_MULTI_SHARE_PHOTOS");
                postComposer->SetLaunchedByAppControl();
                AddScreen(postComposer);

                if (ShowMaxPhotosWarn) {
                    char ToastMsgBuf[FB_BUF_SIZE];
                    Utils::Snprintf_s(ToastMsgBuf, sizeof(ToastMsgBuf), i18n_get_text("IDS_MAY_NOT_SELECT_MORE_MAX_PHOTOS"), CAMERA_ROLL_MAX_SELECTED_PHOTOS);
                    notification_status_message_post(ToastMsgBuf);
                }

            }                //AccessToken Present
            else {
                Log::info("No Access Token. Normal Processing shall follow");
                isHandled = false;
            }                //No Access token

        }                // End of Multi Share
        else if (0 == strcmp(operation, APP_CONTROL_OPERATION_VIEW)) {
            app_control_get_uri(app_control, &uri);
            app_control_get_mime(app_control, &mimeType);
            Log::info("app_control_get_operation() Success & the operation is %s :: uri is %s :: mime is %s", operation, uri, mimeType);
        } else if (0 == strcmp(operation, APP_CONTROL_CUSTOM_OPERATION_NOTIF_RCVD_TO_PROCESS)) {
            //Check if the application has been launched because of some notification coming in.
            //Process received notification
            char *extraDataRcvd = NULL;
            int ret = app_control_get_extra_data(app_control, PUSH_NOTIF_JSON_DATA, &extraDataRcvd);
            Log::debug(LOG_FACEBOOK_NOTIFICATION, "Application::DoHandleAppControl() app_control_get_extra_data() is %s. Push notification JSON data is %s", (APP_CONTROL_ERROR_NONE == ret) ? "Success" : "Failed", extraDataRcvd);
            if (APP_CONTROL_ERROR_NONE == ret && extraDataRcvd && NotificationProvider::GetInstance() && MainScreen::getMainScreen()) {
                //AAA ToDo: Check if this is really needed. Otherwise, it should be redesigned and removed.
                if (GetTopScreenId() == ScreenBase::SID_VIDEO_PLAYER) {
                    AppEvents::Get().Notify(eVIDEOPLAYER_PAUSE_EVENT);
                    app_device_orientation_e curDevOrientation = app_get_device_orientation();
                    //If the device is in Landscape mode, then set it to portrait mode.
                    if ((APP_DEVICE_ORIENTATION_90 == curDevOrientation) || (APP_DEVICE_ORIENTATION_270 == curDevOrientation)) {
                        SetScreenOrientationPortrait();
                    }
                }

                // Decrease the Unseen Notification Count, if at least 1 unseen count is present : Fix for Issue #1823
                int unseenNotifCount = NotificationProvider::GetInstance()->GetUnreadBadgeNum();
                Log::debug(LOG_FACEBOOK_NOTIFICATION, "Application::DoHandleAppControl() : custom_operation_notif : unseen Notification count is : %d", unseenNotifCount);
                if (unseenNotifCount >= 1) {
                    unseenNotifCount--;
                    NotificationProvider::GetInstance()->SetUnreadBadgeNum(unseenNotifCount);
                    MainScreen *myMain = static_cast<MainScreen*>(MainScreen::getMainScreen());
                    if (myMain) {
                        myMain->SetTabbarBadge(MainScreen::eNotificationsTab, unseenNotifCount);
                    }
                    int ret = badge_set_count(PACKAGE, unseenNotifCount);
                    Log::debug(LOG_FACEBOOK_NOTIFICATION, "Application::DoHandleAppControl() : custom_operation_notif :badge_set_count() is %s & the Badge count = %d.", (BADGE_ERROR_NONE == ret) ? "Success" : "Failure", unseenNotifCount);
                }

                FbNotificationRecv *ntfRcvdParsedData = FbNotificationRecv::createFromJson(extraDataRcvd);
                free(extraDataRcvd);
                if (ntfRcvdParsedData) {
                    if (ntfRcvdParsedData->mWebLinkToOpen) {
                        WebViewScreen::Launch(ntfRcvdParsedData->mWebLinkToOpen, true);
                    } else if (ntfRcvdParsedData->mMessengerNotif) {
                        ecore_job_add(run_messenger_app_cb, NULL);
                    } else if (ntfRcvdParsedData->mObjectId) {
                        PostScreen *postScreen = new PostScreen(nullptr, ntfRcvdParsedData->mObjectId);
                        Application::GetInstance()->AddScreen(postScreen);
                    }
                    delete ntfRcvdParsedData;
                }
            }
        } else if (0 == strcmp(operation, APP_CONTROL_CUSTOM_OPERATION_PHOTO_VIDEO_UPLOAD)) {
            char *extraDataRcvd = nullptr;
            int ret = app_control_get_extra_data(app_control, NOTIF_POST_ID, &extraDataRcvd);
            if ((APP_CONTROL_ERROR_NONE == ret) && extraDataRcvd) {
                Log::debug(LOG_FACEBOOK_NOTIFICATION, "Application::DoHandleAppControl(): photo/video uploaded");
                PostScreen *postScreen = new PostScreen(nullptr, extraDataRcvd);
                Application::GetInstance()->AddScreen(postScreen);
            }
            free(extraDataRcvd);
        } else if (0 == strcmp(operation, APP_CONTROL_ACCOUNT_OPERATION_VIEW)) {
            AccountInformationScreen *places = new AccountInformationScreen();
            Application::GetInstance()->AddScreen(places);
            Log::debug("====== APP_CONTROL_ACCOUNT_OPERATION_VIEW ==== ");
        } else if (0 == strcmp(operation, APP_CONTROL_ACCOUNT_OPERATION_SIGNIN)) {
            if(!Config::GetInstance().GetAccessToken().empty()) {
                DoEmergencyLogout();
            }
            Log::debug("====== APP_CONTROL_ACCOUNT_OPERATION_SIGNIN ==== ");
        } else {
            Log::debug(LOG_FACEBOOK_NOTIFICATION, "Application::DoHandleAppControl(): other case");
        }
    }

    free(uri);
    free(mimeType);
    return isHandled;
}

void Application::run_messenger_app_cb( void *data ) {
        ActionBase::run_messanger_cb(NULL, NULL, NULL,NULL);
}

void Application::HandleAppControl(app_control_h app_control)
{
    if (mSuspendedAppControl) { //Clear previous suspended app_control if any
        app_control_destroy(mSuspendedAppControl);
        mSuspendedAppControl = NULL;
    }
    if (!DoHandleAppControl(app_control)) {
        app_control_clone(&(mSuspendedAppControl), app_control);
    }
}

bool Application::HandleSuspendedAppControl() {
    if (mSuspendedAppControl) {
        if (DoHandleAppControl(mSuspendedAppControl)) {
            app_control_destroy(mSuspendedAppControl);
            mSuspendedAppControl = NULL;
            return true;
        }
    }
    return false;
}

void
Application::app_control(app_control_h app_control, void *data) {
    /* Handle the launch request. */
    Application *app = static_cast<Application*>(data);
    app->HandleAppControl(app_control);
}

void
Application::app_pause(void *data)
{
    /* Take necessary actions when application becomes invisible. */
    Application * app = static_cast<Application *> (data);
    AppEvents::Get().Notify(ePAUSE_EVENT, data);
    if (app->mLocationManager) {
        app->mLocationManager->Stop();
    }
    app->mDataService->SendAppBGMessage();
}

void
Application::app_resume(void *data)
{
    /* Take necessary actions when application becomes visible. */
    Application * app = static_cast<Application *> (data);
    AppEvents::Get().Notify(eRESUME_EVENT, data);
    if (!app->mLocationManager) {
        app->mLocationManager = new LocationManager();
    }
    app->mLocationManager->Start();
    app->EnableHWBack();
}

void
Application::app_terminate(void *user_data)
{
    Application *app = static_cast<Application *>(user_data);
    if (app->mNoUIMode) {
        return;
    }

    void *listData;
    EINA_LIST_FREE(app->mTranslatableFields, listData) {
        TranslatableObject *tf = static_cast<TranslatableObject *>(listData);
        delete tf;
    }

    /* Release all resources (*/
    TrayNotificationsManager::GetInstance()->DeleteAllOngoingNotifications();
    if(!app) return;
    app->DeleteLocationManager();
    app->RemoveAllScreensAndNfItemsFromNaviframe();
    app->DeleteAllData();

    if(app->mDataService) {
        if (!Config::GetInstance().GetAccessToken().empty()) {  //is the user logged in?
            app->mDataService->NotifyDataService();  //notify data service that the application is closing
        }
        delete (app->mDataService);
        app->mDataService = NULL;
    }
    if (app->mWin) {
        evas_object_del(app->mWin);
    }
    app->RemovePackageManagerListener();
}

void
Application::win_delete_request_cb(void *data , Evas_Object *obj , void *event_info)
{
    ui_app_exit();
}

void
Application::win_back_cb(void *data, Evas_Object *obj, void *event_info)
{
    Log::info("Application::win_back_cb");
    Application *app = static_cast<Application *>(data);
    /* Let window go to hide state. */
    assert(app);
    ScreenBase *screen = app->GetTopScreen();
    if (screen) {
        if (screen->HandleBackButton()) {
            return;
        }
    }

    if(app->mIsHWBackCBAdded) {
        eext_object_event_callback_del(app->mWin, EEXT_CALLBACK_BACK, win_back_cb);
        app->mIsHWBackCBAdded = false;
    }

    CHECK_RET_NRV(app->mNf);
    if(elm_naviframe_top_item_get(app->mNf) == elm_naviframe_bottom_item_get(app->mNf)) {
        app->GoToBackground();
    } else {
        if(app->mScreenArray && eina_array_count(app->mScreenArray)) {
            app->RemoveTopScreen();
        } else {
            Log::error("Application::win_back_cb->All screens has been alraedy removed");
        }
    }
}

void Application::v_keypad_is_off(void *data, Evas_Object *obj, void *event_info)
{
    Application *app = static_cast<Application *>(data);
    /* Let window go to hide state. */
    if (app) {
        ScreenBase *screen = app->GetTopScreen();
        if (screen) {
            // To avoid situations when user clicked back button but screen is not already shown.
            // It happens on low performance devices.
            if (Utils::GetCurrentTime() - screen->GetVisibleTime() < 500.0 || screen->HandleKeyPadHide()) {
                return;
            }
        }
    }
}

void Application::v_keypad_is_on(void *data, Evas_Object *obj, void *event_info)
{
    Application *app = static_cast<Application *>(data);
    if (app) {
        ScreenBase *screen = app->GetTopScreen();
        if (screen) {
            if (Utils::GetCurrentTime() - screen->GetVisibleTime() < 500.0 || screen->HandleKeyPadShow()) {
                return;
            }
        }
    }
}

bool Application::account_read_cb(account_h account, void* user_data)
{
    char *user_name = NULL;
    char *display_name = NULL;
    int ret = -1;
    // Get the user name
    ret = account_get_user_name(account, &user_name);
    if (ret != ACCOUNT_ERROR_NONE)
    {
        Log::error( "account_get_user_name() Failed" );
        return false;
    }
    Log::info( "User Name: %s", user_name );
    // Get the display name
    ret = account_get_display_name(account, &display_name);
    if (ret != ACCOUNT_ERROR_NONE)
    {
        Log::error( "account_get_display_name() Failed" );
        return false;
    }
    Log::info( "Display Name: %s", display_name );
    /* Get the Account details from Account table */
    account_data accountData;
    memset(&accountData, 0, sizeof(accountData));
    bool result = CacheManager::GetInstance().SelectAccountData(user_name, &accountData);
    if( result )
        Log::info( "SelectAccountData() Success");
    else
    {
        Log::error("SelectAccountData() Failed");
        return false;
    }

    int account_id = 0;
    ret = account_get_account_id(account, &account_id);
    if (ret != ACCOUNT_ERROR_NONE) {
        Log::error( "account_get_account_id() Failed" );
        return false;
    }

    Config::GetInstance().SetUserName(user_name);
    Config::GetInstance().SetUserId(accountData.userId);
    Config::GetInstance().SetAccessToken(accountData.accessToken);
    Config::GetInstance().SetSessionKey(accountData.sessionKey);
    Config::GetInstance().SetSessionSecret(accountData.sessionSecret);
    free(user_name);
    free(display_name);
    Log::info( "account_read_cb() Success" );

    AppEvents::Get().Notify(eACCOUNT_DATA_IS_READY, &account_id);

    return true;
}

void Application::view_size_reset(void *data, Evas *e, Evas_Object *obj, void *event_info)
{
    Application *app = static_cast<Application *>(data);
    app->OnViewResize();
}

void Application::view_focused(void *data, Evas *e, void *event_info) {
    Log::info( "view_focused() called" );
    AppEvents::Get().Notify(eFOCUSED_EVENT, data); // App should work normally when it get the focus
}

void Application::view_unfocused(void *data, Evas *e, void *event_info) {
    Log::info( "view_unfocused() called" );
    AppEvents::Get().Notify(eUNFOCUSED_EVENT, data);
}

Eina_Bool Application::naviframe_pop_cb(void *data, Elm_Object_Item *it){
    Application *app = static_cast<Application *>(data);
    assert(app);
    assert(it);
    ScreenBase *screen = static_cast<ScreenBase *>(eina_array_pop(app->mScreenArray));

    Eina_Bool ret = EINA_TRUE;

    if(screen) {
        bool isLaunchedByAppControl = screen->IsLaunchedByAppControl();

        if(screen->GetNfItem() == it) {
            delete screen;
            Log::debug("Application::naviframe_pop_cb->Good deletion");
            int count = eina_array_count(app->mScreenArray);
            if (count) {
                ScreenBase *popToScreen = static_cast<ScreenBase *>(eina_array_data_get(app->mScreenArray, count-1));
                if (popToScreen) {
                    popToScreen->SetVisibleTime(Utils::GetCurrentTime());
                    popToScreen->OnResume(); // This becomes the new visible screen and UI update logic gets invoked if any
                }
            }
        } else {// maybe we should remove this 'else' code, I did not observe no one error during test
            Log::error( "Application::naviframe_pop_cb->NF Item extraction error, push screen back");
            eina_array_push(app->mScreenArray, screen);
            ret = EINA_FALSE;
        }
        if (isLaunchedByAppControl) {
            app->GoToBackground();
        }
    }
    return ret;
}

void Application::remove_main_layout_from_naviframe(void *data, Evas *e, Evas_Object *obj, void *event_info) {
    Application::GetInstance()->EnableHWBack();
}

void Application::CreateBaseGUI()
{
    elm_config_preferred_engine_set(WINDOW_PREFERED_ENGINE);

    edje_color_class_set("B001", FACEBOOK_DEEP_BLUE_COLOR, 255, 0,0,0,0, 0,0,0,0);

    mWin = elm_win_util_standard_add(PACKAGE, PACKAGE);
    CHECK_RET_NRV(mWin);
    elm_win_conformant_set(mWin, EINA_TRUE);
    elm_win_autodel_set(mWin, EINA_TRUE);
    elm_win_indicator_mode_set(mWin, ELM_WIN_INDICATOR_SHOW);
    elm_win_indicator_opacity_set(mWin, ELM_WIN_INDICATOR_OPAQUE);

    evas_object_event_callback_add(mWin, EVAS_CALLBACK_RESIZE, view_size_reset, this);

    evas_object_smart_callback_add(mWin, "focused", view_focused, this);
    evas_object_smart_callback_add(mWin, "unfocused", view_unfocused, this);
    evas_object_smart_callback_add(mWin, "delete,request", win_delete_request_cb, NULL);

    mConform = CreateConform(mWin);

    evas_object_smart_callback_add(mConform, "virtualkeypad,state,off", v_keypad_is_off, this);
    evas_object_smart_callback_add(mConform, "virtualkeypad,state,on", v_keypad_is_on, this);

    /* Naviframe */
    mNf = elm_naviframe_add(mConform);
    elm_object_content_set(mConform, mNf);

    ScreenBase *scr = nullptr;
    Utils::GetHTTPUserAgent();

    /* Read the Account Details */
    // If account Details already exists, then display Main Screen, else display LogIn screen.
    int ret = account_query_account_by_package_name(account_read_cb, PACKAGE, NULL);
    if (ACCOUNT_ERROR_NONE != ret) {
        Log::error( "account_query_account_by_package_name() Failed" );
        scr = new LoginScreen(NULL);
    } else {
        Log::debug( "account_query_account_by_package_name() Success" );
        // Display Main Screen, only when we successfully read the access Token from Accounts table. Otherwise show Login Screen.
        if (Config::GetInstance().GetAccessToken().empty()) {
            mDataService->setLoginState(false);
            scr =  static_cast<ScreenBase*>(new LoginScreen(NULL));
        } else {
            mDataService->setLoginState(true);
            scr = static_cast<ScreenBase*>(new MainScreen(NULL));
        }
    }

    AddScreen(scr);

    char * userName;
    if( PREFERENCE_ERROR_NONE == preference_get_string(KEY_USER_LOGIN,&userName) )
    {
        LoginScreen *ptr = dynamic_cast<LoginScreen*>(scr);
        if ( ptr ) {
            ptr->set_login_field(userName);
        }
    }
    /* Show window after base gui is set up */
    evas_object_show(mWin);
}

void Application::GoToBackground() {
    Log::debug("Application::GoToBackground");
    elm_win_lower(mWin);
}

Evas_Object *
Application::CreateConform(Evas_Object *parent)
{
    Evas_Object *conform, *bg;

    if (parent == NULL) return NULL;

    conform = elm_conformant_add(parent);
    evas_object_size_hint_weight_set(conform, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    elm_win_resize_object_add(parent, conform);

    bg = elm_bg_add(conform);
    elm_object_style_set(bg, "indicator/headerbg");
    elm_object_part_content_set(conform, "elm.swallow.indicator_bg", bg);
    evas_object_show(bg);

    evas_object_show(conform);
    return conform;
}

void Application::OnViewResize() {
    Evas_Coord w, h;
    evas_object_geometry_get(mWin, NULL, NULL, &w, &h);
    if(NULL != mScreenArray) {
        unsigned int size = eina_array_count(mScreenArray);
        for(int i = 0; i < size; i++) {
            ScreenBase *screen = static_cast<ScreenBase *>(eina_array_data_get(mScreenArray, i));
            if(NULL != screen) {
                screen->OnViewResize(w, h);
            }
        }
    }
}

void Application::AddScreen(ScreenBase *screen) {
    if(mScreenArray && screen) {
        ScreenBase* current = GetTopScreen();
        if(current) {
            current->OnPause();
        }
        eina_array_push(mScreenArray, screen);
        screen->OnResume();
        screen->Show();
        screen->Push();
        elm_naviframe_item_pop_cb_set(screen->GetNfItem(), naviframe_pop_cb, this);
    }
}

int Application::GetTopScreenId() {
    ScreenBase::ScreenIdEnum res = ScreenBase::SID_UNKNOWN;
    ScreenBase *screen = GetTopScreen();
    if(screen) {
        res = screen->GetScreenId();
    }
    return res;
}

ScreenBase *Application::GetTopScreen() {
    ScreenBase *screen = NULL;
    if(mScreenArray && (eina_array_count(mScreenArray) > 0)) {
        screen = static_cast<ScreenBase *>(eina_array_data_get(mScreenArray, eina_array_count(mScreenArray) - 1));
    }
    return screen;
}

ScreenBase *Application::GetParentFeedScreen() {
    ScreenBase *screen = nullptr;
    int screenId = 0;
    int size = eina_array_count(mScreenArray);
    for (int i = size - 1; i >= 0; i--) {
        screen = static_cast<ScreenBase *>(eina_array_data_get(mScreenArray, i));
        screenId = screen->GetScreenId();
        if (screenId == ScreenBase::SID_USER_PROFILE || screenId == ScreenBase::SID_OWN_PROFILE || screenId == ScreenBase::SID_GROUP_PROFILE_PAGE || screenId == ScreenBase::SID_MAIN_SCREEN)
            break;
    }

    return screen;
}

void Application::ReplaceScreen(ScreenBase * targetScreen) {
    if(mScreenArray && targetScreen) {
        ScreenBase *currentScreen = GetTopScreen();
        if(currentScreen) {
            targetScreen->Show();
            targetScreen->InsertBefore(currentScreen);
            elm_naviframe_item_pop_cb_set(targetScreen->GetNfItem(), naviframe_pop_cb, this);
            elm_naviframe_item_pop(mNf);
            eina_array_push(mScreenArray, targetScreen);
        }
    }
}

void Application::RemoveTopScreen() {
    Log::debug("Application::RemoveTopScreen->remove top naviframe item");
    //appropriate screen is destroyed inside naviframe_pop_cb (after internal checks)
    elm_naviframe_item_pop(mNf);
    //AAA ToDo: Lines below seems to be useless. It should be checked, redesigned and removed.
    /* If there is a video screen behind, then enable the rotation and send a resuming event */
//AAA    EnableRotationForVideoScreen();
    NotifyResumeForVideoScreen();
}

void Application::MakeThisScreenTop(ScreenBase *screen) {
// Remove all screens above this from the Naviframe.
    elm_naviframe_item_pop_to(screen->GetNfItem());

// Then remove those screens from mScreenArray.
    if (mScreenArray) {
        unsigned int arraycount = eina_array_count(mScreenArray);
        unsigned int indexToDelete = arraycount;
        unsigned int index;
        void* item;
        Eina_Array_Iterator iterator;
        EINA_ARRAY_ITER_NEXT(mScreenArray, index, item, iterator) {
            ScreenBase *screenToRemove = static_cast<ScreenBase *>(item);
            if (screenToRemove == screen) {
                indexToDelete = index + 1;
                break;
            }
        }

        for(unsigned int i = indexToDelete; i < arraycount; ++i) {
            ScreenBase *scr = static_cast <ScreenBase *> (eina_array_pop(mScreenArray));
            delete scr;
        }
    }

    screen->SetVisibleTime(Utils::GetCurrentTime());
    screen->OnResume(); // This becomes the new visible screen and UI update logic gets invoked if any
}

void Application::RemoveAllScreensAndNfItemsFromNaviframe() {
    Eina_List *scrList = elm_naviframe_items_get(mNf);
    int scrCount = eina_list_count(scrList);
    int scrArrayCount = eina_array_count(mScreenArray);
    Log::debug("Application::RemoveAllScreensAndNFItemsFromNaviframe->mNf count = %d, scrarry count = %d" ,scrCount, scrArrayCount );

    for(int i=0; i< scrArrayCount; i++)
    {
        ScreenBase *screen = static_cast<ScreenBase *>(eina_array_pop(mScreenArray));
        assert(screen);
        if(screen->GetNfItem()) {
            assert(screen->getMainLayout());
            evas_object_event_callback_del(screen->getMainLayout(), EVAS_CALLBACK_DEL, remove_main_layout_from_naviframe);
            elm_naviframe_item_pop_cb_set(screen->GetNfItem(), nullptr, nullptr);
        }
        delete screen;//remove all screens directly, NF items will be replaced at once after
    }
    for(int i=0; i< scrCount; i++)
    {
        RemoveTopScreen();
    }
    eina_array_clean(mScreenArray);
}

void Application::EraseAllProviderData() {
    OperationManager::GetInstance()->CancelAll();
    SubscribersProvider::GetInstance()->EraseData();
    BriefHomeProvider::GetInstance()->EraseData();
    UploadsProvider::GetInstance()->EraseData();
    PhotosOfYouProvider::GetInstance()->EraseData();
    SelectAlbumProvider::GetInstance()->EraseData();
    HomeProvider::GetInstance()->DeleteAllOperations();
    HomeProvider::GetInstance()->EraseData();
    OwnFriendsProvider::GetInstance()->EraseData();
    FriendRequestsProvider::GetInstance()->EraseData();
    SearchProvider::GetInstance()->EraseData();
    PhotoGalleryProvider::GetInstance()->EraseData();
    FriendRequestsBatchProvider::GetInstance()->EraseData();
    GroupProvider::GetInstance()->EraseData();
    FriendSearchProvider::GetInstance()->EraseData();
    ChooseFriendProvider::GetInstance()->EraseData();
    UsersDetailInfoProvider::GetInstance()->EraseData();
    UsersByIdProvider::GetInstance()->EraseData();
    AlbumsProvider::GetInstance()->EraseData();
    PlacesProvider::GetInstance()->EraseData();
    NotificationAbstractDataProvider::GetInstance()->EraseData();
    CheckInProvider::GetInstance()->EraseData();
    EventBatchProvider::GetInstance()->EraseData();
    FriendBirthDayProvider::GetInstance()->EraseData();
    EditHistoryProvider::GetInstance()->EraseData();
#ifdef EVENT_NATIVE_VIEW
    EventGuestListFactory::GetProvider(EVENT_GOING_TAB)->EraseData();
    EventGuestListFactory::GetProvider(EVENT_MAYBE_TAB)->EraseData();
    EventGuestListFactory::GetProvider(EVENT_INVITED_TAB)->EraseData();
#endif
    SuggestedFriendsProvider::GetInstance()->EraseData();
    GroupFriendsProvider::GetInstance()->EraseData();
    NotificationProvider::GetInstance()->EraseData();
    MutualFriendsList::GetInstance()->DeleteAllUsersMutualFriends();
}

void Application::DeleteAllData() {
    delete mPrivacyOptionsData; mPrivacyOptionsData = NULL;
    delete mContactSynchronizer; mContactSynchronizer = NULL;
    SelectedMediaList::Clear();
    AppEvents::Get().StopSendingNotifications();
    delete SubscribersProvider::GetInstance();
    delete BriefHomeProvider::GetInstance();
    delete UploadsProvider::GetInstance();
    delete PhotosOfYouProvider::GetInstance();
    delete SelectAlbumProvider::GetInstance();
    delete HomeProvider::GetInstance();
    delete OwnFriendsProvider::GetInstance();
    delete FriendRequestsProvider::GetInstance();
    delete SearchProvider::GetInstance();
    delete PhotoGalleryProvider::GetInstance();
    delete FriendRequestsBatchProvider::GetInstance();
    delete GroupProvider::GetInstance();
    delete FriendSearchProvider::GetInstance();
    delete ChooseFriendProvider::GetInstance();
    delete UsersDetailInfoProvider::GetInstance();
    delete UsersByIdProvider::GetInstance();
    delete AlbumsProvider::GetInstance();
    delete PlacesProvider::GetInstance();
    delete NotificationAbstractDataProvider::GetInstance();
    delete CheckInProvider::GetInstance();
    delete EventBatchProvider::GetInstance();
    delete FriendBirthDayProvider::GetInstance();
    delete EditHistoryProvider::GetInstance();
#ifdef EVENT_NATIVE_VIEW
    delete EventGuestListFactory::GetProvider(EVENT_GOING_TAB);
    delete EventGuestListFactory::GetProvider(EVENT_MAYBE_TAB);
    delete EventGuestListFactory::GetProvider(EVENT_INVITED_TAB);
#endif
    delete OperationManager::GetInstance();
    delete FacebookSession::GetInstance();
    delete SuggestedFriendsProvider::GetInstance();
    delete GroupFriendsProvider::GetInstance();
    delete NotificationProvider::GetInstance();
    CacheManager::GetInstance().CloseDatabase();
    delete TrayNotificationsManager::GetInstance();
    CommentWidgetFactory::StopLongpressTimer();
    delete MutualFriendsList::GetInstance();
}

void Application::ReStartDataService() {
    bool runnung = false;
    int ret = app_manager_is_running(DATASERVICE_NAME, &runnung);
    if((ret == APP_MANAGER_ERROR_NONE) && (!runnung)){
        StartDataService();
    }
}

void Application::StartDataService() {
    app_control_h app_control;

    app_control_create(&app_control);
    app_control_set_operation(app_control, APP_CONTROL_OPERATION_DEFAULT);
    app_control_set_app_id(app_control, DATASERVICE_NAME);
    app_control_set_launch_mode(app_control, APP_CONTROL_LAUNCH_MODE_GROUP);
    if (app_control_send_launch_request(app_control, NULL, NULL) == APP_CONTROL_ERROR_NONE)
    {
        Log::info( "Succeeded to launch " DATASERVICE_NAME );
    }
    else
    {
        Log::error( "Failed to launch " DATASERVICE_NAME );
    }

    app_control_destroy(app_control);
}

LocationManager *Application::GetLocationManager() {
    if (!mLocationManager) {
        mLocationManager = new LocationManager();
    }
    return mLocationManager;
}

 void Application::DeleteLocationManager() {
     delete mLocationManager;
     mLocationManager = NULL;
}

void Application::RemoveOldPostComposerScreen()
{
    /* Remove the old post composer screen if any */
    Log::info("Application::RemoveOldPostComposerScreen: TOP SCREEN ID = %d", GetTopScreenId());
    unsigned int screen_count = eina_array_count(mScreenArray);
    if (GetTopScreenId() == ScreenBase::SID_POST_COMPOSER_SCREEN) {
        if ((NULL != mScreenArray) && (screen_count > 0)) {
            ScreenBase *screen = static_cast<ScreenBase *>(eina_array_data_get(mScreenArray, screen_count - 1));
            if (screen)
                screen->Pop();
        }
    }
}

 void Application::RemovePostSimplePickerScreen() {
     unsigned int ScreensCnt = eina_array_count(mScreenArray);
     unsigned short PostCmprScrnIdx = 2;
     if (ScreensCnt > PostCmprScrnIdx) {
         ScreenBase* PastTopScreen = static_cast<ScreenBase*>(eina_array_data_get(mScreenArray, (ScreensCnt-PostCmprScrnIdx)));
         if(PastTopScreen && (ScreenBase::SID_POST_COMPOSER_SCREEN == PastTopScreen->GetScreenId())) {
             RemoveTopScreen();
         }
     }
 }

void Application::SetScreenOrientationPortrait() {
    elm_win_wm_rotation_preferred_rotation_set(mWin, APP_DEVICE_ORIENTATION_0);
}

//AAA Todo: It seems that we don't need 2 functions below.
// void Application :: SetScreenOrientationBoth()
// {
//     if (elm_win_wm_rotation_supported_get(mWin))
//     {
//         int rotation[3] = { 0, 90, 270 };
//         elm_win_wm_rotation_available_rotations_set(mWin, (const int *)(&rotation), 3);
//     }
//     SetManualRotation();
// }

// void Application::SetManualRotation()
// {
//     if ((GetTopScreenId() == ScreenBase::SID_VIDEO_PLAYER)){
//         if((NULL != mScreenArray) && (eina_array_count(mScreenArray) > 0)) {
//             app_device_orientation_e rotation = app_get_device_orientation();
//             if(rotation != APP_DEVICE_ORIENTATION_180){
//                 Log::debug( "Application :: Manual Rotation Set :: %d", (int)rotation );
//                 elm_win_wm_rotation_preferred_rotation_set(mWin, (int)rotation);
//                 elm_win_rotation_with_resize_set(mWin, (int)rotation);
//             }
//         }
//     }
// }

 void Application::EnableRotationForVideoScreen()
 {
    if ((GetTopScreenId() != ScreenBase::SID_VIDEO_PLAYER)) {
        return;
    }

    bool value;
    int ret = system_settings_get_value_bool(AUTO_ROTATION_SETTING, &value);
    if ((ret == SYSTEM_SETTINGS_ERROR_NONE) && value) {
//AAA We don't support auto-rotation.
//AAA     Application::GetInstance()->SetScreenOrientationBoth();
    }
}

void Application::NotifyResumeForVideoScreen()
{
    if ((GetTopScreenId() == ScreenBase::SID_VIDEO_PLAYER)) {
        AppEvents::Get().Notify(eVIDEOPLAYER_RESUME_EVENT);
    }
}

void Application::AddPackageManagerListener() {
    package_manager_h UninstallManager;
    int ret = package_manager_create(&UninstallManager);
    if (ret == PACKAGE_MANAGER_ERROR_NONE) {
        package_manager_set_event_status(UninstallManager, PACKAGE_MANAGER_STATUS_TYPE_UNINSTALL);
        package_manager_set_event_cb(UninstallManager, on_uninstall_me, NULL);
        mPackageManagerList = eina_list_append(mPackageManagerList, UninstallManager);
    }
    package_manager_h ClearDataManager;
    ret = package_manager_create(&ClearDataManager);
    if (ret == PACKAGE_MANAGER_ERROR_NONE) {
        package_manager_set_event_status(ClearDataManager, PACKAGE_MANAGER_STATUS_TYPE_CLEAR_DATA);
        package_manager_set_event_cb(ClearDataManager, on_clear_data, NULL);
        mPackageManagerList = eina_list_append(mPackageManagerList, ClearDataManager);
    }
}

void Application::on_uninstall_me( const char *type,
                                   const char *package,
                                   package_manager_event_type_e event_type,
                                   package_manager_event_state_e event_state,
                                   int progress,
                                   package_manager_error_e error,
                                   void *user_data ) {
    if ( !strcmp(package,PACKAGE) ) {
        if( event_state == PACKAGE_MANAGER_EVENT_STATE_STARTED ) {
            TrayNotificationsManager::GetInstance()->DeleteAllNotiNotifications();
            account_query_account_by_package_name(account_remove_cb, PACKAGE, user_data);
        }
    }
}

void Application::on_clear_data( const char *type,
                                 const char *package,
                                 package_manager_event_type_e event_type,
                                 package_manager_event_state_e event_state,
                                 int progress,
                                 package_manager_error_e error,
                                 void *user_data ) {
    if( event_state == PACKAGE_MANAGER_EVENT_STATE_STARTED ) {
        LogoutRequest::RedirectToLogin(nullptr);
    }
}
void Application::RemovePackageManagerListener() {
    void *data;
    EINA_LIST_FREE(mPackageManagerList, data)
    {
        package_manager_h manager = static_cast<package_manager_h>(data);
        if (manager) {
            package_manager_unset_event_cb(manager);
            package_manager_destroy(manager);
        }
    }
}

bool Application::account_remove_cb(account_h account, void* data) {
    int account_id = 0;
    int ret = 0;
    ret = account_get_account_id(account, &account_id);
    Log::debug(LOG_FACEBOOK, "account_remove_cb :: ACCOUNT ID = %d", account_id);
    if (ret == ACCOUNT_ERROR_NONE) {
        ret = account_delete_from_db_by_id(account_id);
    } else {
        Log::error(LOG_FACEBOOK, "account_delete_from_db_by_id() Failed = %d", ret);
    }
    return false;
}

void Application::SetScreenMode(efl_util_screen_mode_e mode) {
    if (mode == EFL_UTIL_SCREEN_MODE_DEFAULT) {
        if (mScreenModeTimer) {
            ecore_timer_reset(mScreenModeTimer);
        } else {
            int displayTimeout;
            int ret = system_settings_get_value_int(SYSTEM_SETTINGS_KEY_SCREEN_BACKLIGHT_TIME, &displayTimeout);
            if (ret == SYSTEM_SETTINGS_ERROR_NONE)
                mScreenModeTimer = ecore_timer_add(displayTimeout, set_default_mode_cb, this);
        }
    } else {
        efl_util_set_window_screen_mode(mWin, mode);
        if (mScreenModeTimer) {
            ecore_timer_del(mScreenModeTimer);
            mScreenModeTimer = nullptr;
        }
    }
}

Eina_Bool Application::set_default_mode_cb( void *data )
{
    Application *app = static_cast<Application*>(data);
    efl_util_set_window_screen_mode(app->mWin, EFL_UTIL_SCREEN_MODE_DEFAULT);
    return ECORE_CALLBACK_CANCEL;
}

void Application::EnableHWBack() {
    Log::debug("Application::EnableHWBack");
    if (!mIsHWBackCBAdded) {
        eext_object_event_callback_add(mWin, EEXT_CALLBACK_BACK, win_back_cb, this);
        mIsHWBackCBAdded = true;
    }
}

void Application::DoEmergencyLogout() {
    Log::info("SettingsScreen::DoEmergencyLogout");
    mEmergencyLogoutRequest = new LogoutRequest(SettingsScreen::on_logout_completed, nullptr);
    if (Application::GetInstance()->mDataService) {
        Application::GetInstance()->mDataService->setLoginState(false);
    }
    SignUpInProgressScreen * inProgressScreen = new SignUpInProgressScreen(nullptr);
    inProgressScreen->mInProgress = true;
    inProgressScreen->mRequestType = LogOutReq;
    Application::GetInstance()->AddScreen(inProgressScreen);
    inProgressScreen->mGraphRequest = mEmergencyLogoutRequest;
    mEmergencyLogoutRequest->DoLogout();
}

void Application::TranslatedTextSet(Evas_Object *obj, const char *format, const char *text1, const char *text2, const char *text3, const char *text4, const char *text5) {
    TranslatableTextField *tt = static_cast<TranslatableTextField *> (GetTranslatableByObject(obj));
    if (!tt) {
        tt = new TranslatableTextField(obj);
        evas_object_event_callback_add(obj, EVAS_CALLBACK_DEL, on_translatable_object_deleted, this);
        mTranslatableFields = eina_list_append(mTranslatableFields, tt);
    }
    tt->SetText(nullptr, format, text1, text2, text3, text4, text5);
    tt->Update();
}

void Application::TranslatedPartTextSet(Evas_Object *obj, const char *part, const char *format, const char *text1, const char *text2, const char *text3, const char *text4, const char *text5) {
    TranslatableTextField *tt = static_cast<TranslatableTextField *> (GetTranslatableByObject(obj));
    if (!tt) {
        tt = new TranslatableTextField(obj);
        evas_object_event_callback_add(obj, EVAS_CALLBACK_DEL, on_translatable_object_deleted, this);
        mTranslatableFields = eina_list_append(mTranslatableFields, tt);
    }
    tt->SetText(part, format, text1, text2, text3, text4, text5);
    tt->UpdatePart(part);
}

void Application::HeuristicTranslatedTextSet(Evas_Object *obj, const char *originalText) {
    TranslatableTextField *tf = static_cast<TranslatableTextField *> (GetTranslatableByObject(obj));
    if (!tf) {
        tf = new TranslatableTextField(obj);
        evas_object_event_callback_add(obj, EVAS_CALLBACK_DEL, on_translatable_object_deleted, this);
        mTranslatableFields = eina_list_append(mTranslatableFields, tf);
    }
    tf->SetText(nullptr, originalText);
    tf->Update();
}

void Application::HeuristicTranslatedPartTextSet(Evas_Object *obj, const char *part, const char *originalText) {
    TranslatableTextField *tf = static_cast<TranslatableTextField *> (GetTranslatableByObject(obj));
    if (!tf) {
        tf = new TranslatableTextField(obj);
        evas_object_event_callback_add(obj, EVAS_CALLBACK_DEL, on_translatable_object_deleted, this);
        mTranslatableFields = eina_list_append(mTranslatableFields, tf);
    }
    tf->SetText(part, originalText);
    tf->UpdatePart(part);
}

void Application::DateTranslatedTextSet(Evas_Object *obj, const char *part, const char *format, double time) {
    TranslatableTextField *tf = static_cast<TranslatableTextField *> (GetTranslatableByObject(obj));
    if (!tf) {
        tf = new TranslatableTextField(obj);
        evas_object_event_callback_add(obj, EVAS_CALLBACK_DEL, on_translatable_object_deleted, this);
        mTranslatableFields = eina_list_append(mTranslatableFields, tf);
    }
    tf->SetText(part, format, time);
    tf->Update();
}

void Application::ToStoryTranslatedTextSet(Evas_Object *obj, PostItemTitleType Type, PresentableAsPost &Post) {
    TranslatableTextField *tf = static_cast<TranslatableTextField *> (GetTranslatableByObject(obj));
    if (!tf) {
        tf = new TranslatableTextField(obj);
        evas_object_event_callback_add(obj, EVAS_CALLBACK_DEL, on_translatable_object_deleted, this);
        mTranslatableFields = eina_list_append(mTranslatableFields, tf);
    }
    tf->SetText(Type, Post);
    tf->Update();
}

//
void Application::FormattedTranslatedEntry(Evas_Object *obj, const char *formatLTR, const char *formatRTL) {
    TranslatableEntry *tf = static_cast<TranslatableEntry *> (GetTranslatableByObject(obj));
    if (!tf) {
        tf = new TranslatableEntry(obj, formatLTR, formatRTL);
        evas_object_event_callback_add(obj, EVAS_CALLBACK_DEL, on_translatable_object_deleted, this);
        mTranslatableFields = eina_list_append(mTranslatableFields, tf);
    }
    tf->Update();
}
void Application::SetElmImageFile(Evas_Object *obj, const char *LTRPath, const char *RTLPath) {
    TranslatableImage *ti = static_cast<TranslatableImage *> (GetTranslatableByObject(obj));
    if (!ti) {
        ti = new TranslatableImage(obj);
        evas_object_event_callback_add(obj, EVAS_CALLBACK_DEL, on_translatable_object_deleted, this);
        mTranslatableFields = eina_list_append(mTranslatableFields, ti);
    }
    ti->SetElmImageFile(LTRPath, RTLPath);
    ti->Update();
}
void Application::SetElmButtonStyle(Evas_Object *obj, const char *LTRPath, const char *RTLPath) {
    TranslatableButton *ti = static_cast<TranslatableButton *> (GetTranslatableByObject(obj));
    if (!ti) {
        ti = new TranslatableButton(obj);
        evas_object_event_callback_add(obj, EVAS_CALLBACK_DEL, on_translatable_object_deleted, this);
        mTranslatableFields = eina_list_append(mTranslatableFields, ti);
    }
    ti->SetElmButtonStyle(LTRPath, RTLPath);
    ti->Update();
}

Application::TranslatableObject *Application::GetTranslatableByObject(Evas_Object *obj) {
    TranslatableObject *ret = nullptr;
    Eina_List *listItem;
    void *listData;
    EINA_LIST_FOREACH(mTranslatableFields, listItem, listData) {
        TranslatableObject *to = static_cast<TranslatableObject *>(listData);
        if (to->GetObject() == obj) {
            ret = to;
            break;
        }
    }
    return ret;
}

void Application::UpdateTranslations() {
    Eina_List * listItem;
    void * listData;
    EINA_LIST_FOREACH(mTranslatableFields, listItem, listData) {
        TranslatableObject *tf = static_cast<TranslatableObject *>(listData);
        tf->Update();
    }
}

void Application::DeleteTranslatableObject(Evas_Object *obj) {
    Eina_List *listItem;
    Eina_List *nextItem;
    void *listData;
    EINA_LIST_FOREACH_SAFE(mTranslatableFields, listItem, nextItem, listData) {
        TranslatableObject *tf = static_cast<TranslatableObject *>(listData);
        if (tf->GetObject() == obj) {
            delete tf;
            mTranslatableFields = eina_list_remove(mTranslatableFields, tf);
            break;
        }
    }
}

void Application::on_translatable_object_deleted(void *data, Evas *e, Evas_Object *obj, void *event_info) {
    Application *me = static_cast<Application *>(data);
    Eina_List *listItem;
    Eina_List *nextItem;
    void *listData;
    EINA_LIST_FOREACH_SAFE(me->mTranslatableFields, listItem, nextItem, listData) {
        TranslatableObject *tf = static_cast<TranslatableObject *>(listData);
        if (tf->GetObject() == obj) {
            delete tf;
            me->mTranslatableFields = eina_list_remove(me->mTranslatableFields, tf);
            break;
        }
    }
}


/*
 * Class Application::TranslatableObject
 */
Application::TranslatableObject::TranslatableObject(Evas_Object *obj) : mObject(obj) {}


/*
 * Class Application::TranslatableTextField
 */
Application::TranslatableTextField::TranslatableTextField(Evas_Object *obj) : Application::TranslatableObject::TranslatableObject(obj), mParts(nullptr) {}

Application::TranslatableTextField::~TranslatableTextField() {
    void *listData;
    EINA_LIST_FREE(mParts, listData) {
        delete static_cast<Part *>(listData);
    }
}

void Application::TranslatableTextField::SetText(const char *part, const char *format, const char *text1, const char *text2, const char *text3, const char *text4, const char *text5) {
    FormatTranslator *tr = new FormatTranslator(format, text1, text2, text3, text4, text5);
    SetTranslator(part, tr);
}

void Application::TranslatableTextField::SetText(const char *part, const char *originalText) {
    HeuristicTranslator *tr = new HeuristicTranslator(originalText);
    SetTranslator(part, tr);
}

void Application::TranslatableTextField::SetText(const char *part, const char *format, double time) {
    DateTranslator *tr = new DateTranslator(format, time);
    SetTranslator(part, tr);
}

void Application::TranslatableTextField::SetText(PostItemTitleType Type, PresentableAsPost &Post) {
    ToStoryTranslator *tr = new ToStoryTranslator(Type, Post);
    SetTranslator(nullptr, tr);
}

void Application::TranslatableTextField::SetTranslator(const char *partName, TranslatorBase *tr) {
    Part *part = GetPartByName(partName);
    if (part) {
        part->SetTranslator(tr);
    } else {
        part = new Part(*mObject, partName, tr);
        mParts = eina_list_append(mParts, part);
    }
}

Application::TranslatableTextField::Part *Application::TranslatableTextField::GetPartByName(const char *name) {
    Part *res = nullptr;
    Eina_List * listItem;
    void * listData;
    EINA_LIST_FOREACH(mParts, listItem, listData) {
        Part *part = static_cast<Part *>(listData);
        if ((!name && !part->GetName()) || (name && part->GetName() && !strcmp(part->GetName(), name))) {
            res = part;
            break;
        }
    }
    return res;
}

void Application::TranslatableTextField::Update() {
    Eina_List * listItem;
    void * listData;
    EINA_LIST_FOREACH(mParts, listItem, listData) {
        Part *part = static_cast<Part *>(listData);
        part->Update();
    }
}

void Application::TranslatableTextField::UpdatePart(const char *name) {
    Part *part = GetPartByName(name);
    assert(part);
    part->Update();
}

/*
 * Class Application::TranslatableTextField::Part
 */
Application::TranslatableTextField::Part::Part(Evas_Object &object, const char *name, TranslatorBase *translator) : mObject(object), mName(nullptr), mTranslator(translator) {
    assert(mTranslator);
    mName = SAFE_STRDUP(name);
}

Application::TranslatableTextField::Part::~Part() {
    free(mName);
    delete mTranslator;
}

void Application::TranslatableTextField::Part::SetTranslator(TranslatorBase *translator) {
    delete mTranslator;
    mTranslator = translator;
    assert(mTranslator);
}
void Application::TranslatableTextField::Part::Update() {
    char *text = mTranslator->Translate();
    if (!mName) {
        elm_object_text_set(&mObject, text);
    } else {
        elm_object_part_text_set(&mObject, mName, text);
    }
    delete []text;
}

/*
 * Class Application::TranslatableTextField::FormatTranslator
 */
Application::TranslatableTextField::FormatTranslator::FormatTranslator(const char *format, const char *text1, const char *text2, const char *text3, const char *text4, const char *text5) {
    assert(format);
    mFormat = SAFE_STRDUP(format);
    mText1 = SAFE_STRDUP(text1);
    mText2 = SAFE_STRDUP(text2);
    mText3 = SAFE_STRDUP(text3);
    mText4 = SAFE_STRDUP(text4);
    mText5 = SAFE_STRDUP(text5);
}

Application::TranslatableTextField::FormatTranslator::~FormatTranslator() {
    free(mFormat);
    free(mText1);
    free(mText2);
    free(mText3);
    free(mText4);
    free(mText5);
}

char *Application::TranslatableTextField::FormatTranslator::Translate() {
    char *text = nullptr;
    if (mText1 && mText2 && mText3 && mText4 && mText5) {
        text = WidgetFactory::WrapByFormat5(mFormat, i18n_get_text(mText1), i18n_get_text(mText2), i18n_get_text(mText3), i18n_get_text(mText4), i18n_get_text(mText5));
    } else if (mText1 && mText2 && mText3 && mText4) {
        text = WidgetFactory::WrapByFormat4(mFormat, i18n_get_text(mText1), i18n_get_text(mText2), i18n_get_text(mText3), i18n_get_text(mText4));
    } else if (mText1 && mText2 && mText3) {
        text = WidgetFactory::WrapByFormat3(mFormat, i18n_get_text(mText1), i18n_get_text(mText2), i18n_get_text(mText3));
    } else if (mText1 && mText2) {
        text = WidgetFactory::WrapByFormat2(mFormat, i18n_get_text(mText1), i18n_get_text(mText2));
    } else if (mText1) {
        text = WidgetFactory::WrapByFormat(mFormat, i18n_get_text(mText1));
    } else {
        assert(false);
    }
    return text;
}

/*
 * Class Application::TranslatableTextField::HeuristicTranslator
 */
Application::TranslatableTextField::HeuristicTranslator::HeuristicTranslator(const char *originalText) {
    originalText = originalText ? originalText : "";
    mOriginalText = strdup(originalText);
}

Application::TranslatableTextField::HeuristicTranslator::~HeuristicTranslator() {
    free(mOriginalText);
}

void Application::TranslatableTextField::HeuristicTranslator::SetOriginalText(const char *originalText) {
    free(mOriginalText);
    originalText = originalText ? originalText : "";
    mOriginalText = strdup(originalText);
}

char *Application::TranslatableTextField::HeuristicTranslator::Translate() {
    struct Translation {
        Translation(const char *begin, size_t transLen, const char *translation) : mBegin(begin), mTransLen(transLen), mTranslation(translation) {}

        const char *mBegin;
        size_t mTransLen;
        const char *mTranslation;
    };
    Eina_List *translations = nullptr;

//Looking for possible translations in the original text.
    int sizeAlteration = 0;
    const char *idBegin = strstr(mOriginalText, "IDS_");
    while (idBegin) {
        size_t transLen = strspn(idBegin, "ETAOINSHRDLCUMWFGYPBVKXJQZ_0123456789");
        char *id = new char[transLen + 1];
        eina_strlcpy(id, idBegin, transLen + 1);
        const char* translation = i18n_get_text(id);
        if (strcmp(id, translation)) { //if it's really translated.
            Translation *tr = new Translation(idBegin, transLen, translation);
            translations = eina_list_append(translations, tr);
            sizeAlteration = sizeAlteration += (strlen(translation) - transLen);
        }
        delete []id;
        idBegin = strstr(idBegin + transLen, "IDS_");
    }
//Generate translated string.
    char *translatedStr = nullptr;
    if (translations) { //if there are translations
        int size = strlen(mOriginalText) + sizeAlteration + 1;
        translatedStr = new char[size];
        Eina_List *listItem;
        void *listData;
        const char* originalPos = mOriginalText;
        char* trPos = translatedStr;
        EINA_LIST_FOREACH(translations, listItem, listData) {
            Translation *tr = static_cast<Translation *>(listData);
            if (originalPos < tr->mBegin) { //test before the current translation
                eina_strlcpy(trPos, originalPos, (tr->mBegin - originalPos) + 1);
                trPos += tr->mBegin - originalPos;
                originalPos = tr->mBegin;
            }
            eina_strlcpy(trPos, tr->mTranslation, size - (trPos - translatedStr)); //the current translation itself
            trPos += strlen(tr->mTranslation);
            originalPos = tr->mBegin + tr->mTransLen;
        }
        if (originalPos < mOriginalText + strlen(mOriginalText)) { //end of the original string following the last translation.
            eina_strlcpy(trPos, originalPos, size - (trPos - translatedStr));
        }
    } else { //If there were not translations then return copy of original mOriginalText.
        translatedStr = Utils::getCopy(mOriginalText);
    }

    void *listData;
    EINA_LIST_FREE(translations, listData) {
        Translation *tr = static_cast<Translation *>(listData);
        delete tr;
    }
    return translatedStr;
}


/*
 * Class Application::TranslatableImage
 */
Application::TranslatableImage::TranslatableImage(Evas_Object *obj) : Application::TranslatableObject::TranslatableObject(obj),
        mRTLPath(nullptr), mLTRPath(nullptr) {
}

Application::TranslatableImage::~TranslatableImage() {
    free(mRTLPath);
    free(mLTRPath);
}

void Application::TranslatableImage::Update() {
    const char *imagePath = Application::IsRTLLanguage() ? mRTLPath : mLTRPath;
    const char *currentImagePath = nullptr;
    elm_image_file_get(mObject, &currentImagePath, nullptr);
    assert(imagePath);
    if (!currentImagePath || strcmp(currentImagePath, imagePath)) { //if it is really changed.
        elm_image_file_set(mObject, imagePath, nullptr);
    }
}

void Application::TranslatableImage::SetElmImageFile(const char *LTRPath, const char *RTLPath) {
    assert(LTRPath);

    free(mRTLPath);
    if (RTLPath) {
        mRTLPath = SAFE_STRDUP(RTLPath);
    } else {
        int size = strlen(LTRPath) + 5;  //'_rtl' + null-terminator
        mRTLPath = (char *) malloc(size);
        eina_strlcpy(mRTLPath, LTRPath, strlen(LTRPath) - 4 + 1);
        Utils::Snprintf_s(mRTLPath + strlen(LTRPath) - 4, size - (strlen(LTRPath) - 4), "%s%s", "_rtl", LTRPath + strlen(LTRPath) - 4);
        if (!Utils::FileExists(mRTLPath)) { //if there are no file for RTL then use LTR file always to avoid regressions.
            free(mRTLPath);
            mRTLPath = SAFE_STRDUP(LTRPath);
        }
    }

    free(mLTRPath);
    mLTRPath = SAFE_STRDUP(LTRPath);
}


/*
 * Class Application::TranslatableButton
 */
Application::TranslatableButton::TranslatableButton(Evas_Object *obj) : Application::TranslatableObject::TranslatableObject(obj),
        mLTRStyle(nullptr), mRTLStyle(nullptr) {
}
Application::TranslatableButton::~TranslatableButton() {
    free(mRTLStyle);
    free(mLTRStyle);
}
void Application::TranslatableButton::Update() {
    const char *style = Application::IsRTLLanguage() ? mRTLStyle : mLTRStyle;
    const char *currentStyle = elm_object_style_get(mObject);
    assert(style);
    if (!currentStyle || strcmp(currentStyle, style)) { //if it is really changed.
        if (!elm_object_style_set(mObject, style)) {
            elm_object_style_set(mObject, currentStyle); //if for some reason new style does not exist
        }
    }
}
void Application::TranslatableButton::SetElmButtonStyle(const char *LTRStyle, const char *RTLStyle) {
    assert(LTRStyle);

    free(mRTLStyle);
    if (RTLStyle) {
        mRTLStyle = SAFE_STRDUP(RTLStyle);
    } else {
        int size = strlen(LTRStyle) + 5;  //'_rtl' + null-terminator
        mRTLStyle = (char *) malloc(size);
        Utils::Snprintf_s(mRTLStyle, size, "%s_rtl", LTRStyle);
    }

    free(mLTRStyle);
    mLTRStyle = SAFE_STRDUP(LTRStyle);
}


void Application::ConfigureSoundManager() {

   int ret = sound_manager_set_session_type(SOUND_SESSION_TYPE_MEDIA);

   if (ret != SOUND_MANAGER_ERROR_NONE) {
       Log::error("Application::ConfigureSoundManager->can't set session type, Error code =  %d", ret);
       return;
   }
   ret = sound_manager_set_media_session_option(
       SOUND_SESSION_OPTION_PAUSE_OTHERS_WHEN_START,
       SOUND_SESSION_OPTION_INTERRUPTIBLE_DURING_PLAY);
   if (ret != SOUND_MANAGER_ERROR_NONE) {
       Log::error("Application::ConfigureSoundManager->can't set session option, Error code =  %d", ret);
   }
}

/*
 * Class Application::DateTranslator
 */
Application::TranslatableTextField::DateTranslator::DateTranslator(const char *format, double timeInMillisec) :
    HeuristicTranslator(nullptr) , mTimeInMillisec(timeInMillisec) {
    mFormat = SAFE_STRDUP(format);
}

Application::TranslatableTextField::DateTranslator::~DateTranslator() {
    free((void*)mFormat);
}

char * Application::TranslatableTextField::DateTranslator::Translate() {
    char* timeMillisec = Utils::PresentTimeFromMilliseconds(mTimeInMillisec);
    char *originalText = WidgetFactory::WrapByFormat(mFormat, timeMillisec);
    SetOriginalText(originalText);
    delete [] originalText;
    free(timeMillisec);
    return HeuristicTranslator::Translate();
}

/*
 * Class Application::ToStoryTranslator
 */

Application::TranslatableTextField::ToStoryTranslator::ToStoryTranslator(PostItemTitleType Type, PresentableAsPost &Post) : HeuristicTranslator(nullptr), mPost(Post), mType(Type) {
    mPost.AddRef();
}

Application::TranslatableTextField::ToStoryTranslator::~ToStoryTranslator() {
    mPost.Release();
}

char * Application::TranslatableTextField::ToStoryTranslator::Translate() {
    SetOriginalText(WidgetFactory::CreateTitle(mType, mPost).c_str());
    return HeuristicTranslator::Translate();
}

Application::TranslatableEntry::TranslatableEntry(Evas_Object *obj, const char *formatLTR, const char *formatRTL) :
        Application::TranslatableObject::TranslatableObject(obj) {
    mFormatLTR = SAFE_STRDUP(formatLTR);
    mFormatRTL = SAFE_STRDUP(formatRTL);
}

Application::TranslatableEntry::~TranslatableEntry() {
    free(mFormatLTR);
    free(mFormatRTL);
}

void Application::TranslatableEntry::Update() {

    elm_entry_text_style_user_push(mObject, IsRTLLanguage() ? mFormatRTL : mFormatLTR);

}
