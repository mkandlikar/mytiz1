#include <tizen.h>
#include "fbapp.h"

#include "Application.h"

int
main(int argc, char *argv[])
{
	int ret = 0;

	ret = Application::GetInstance()->Run(argc, argv);
	if (ret != APP_ERROR_NONE) {
		dlog_print(DLOG_ERROR, LOG_TAG, "ui_app_main() is failed. err = %d", ret);
	}
	delete Application::GetInstance();

	return ret;
}
