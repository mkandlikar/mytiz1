/* GetStartedAddPhoto.cpp
 *
 * Created on: 10th June 2015
 * Author: Jeyaramakrishnan Sundar
 *
 */

#include <app.h>
#include "Common.h"
#include "dlog.h"
#include "GetStartedAddPhoto.h"
#include "GetStartedProfilePic.h"
#include "Utils.h"



#define IMAGE_MIME_TYPE "image/*"
#define CAMERA_DIRECTORY    "/opt/usr/media/DCIM/Camera"
#define ERR(fmt, arg...) dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, fmt,  ##arg)
#define FILE_PREFIX "IMAGE"

#define RETVM_IF(expr, val, fmt, arg...) \
{ \
    if (expr) \
    { \
        ERR(fmt, ##arg); \
        return; \
    } \
}

GetStartedAddPhoto::GetStartedAddPhoto():ScreenBase(NULL)
{
    mLayout = elm_layout_add(getParentMainLayout());
    elm_layout_file_set(mLayout, Application::mEdjPath, "get_started_add_photo");
    evas_object_size_hint_weight_set(mLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_show(mLayout);

    elm_object_signal_callback_add(mLayout, "skip,clicked", "skip_button",
                (Edje_Signal_Cb)skip_btn_cb, this);
    elm_object_signal_callback_add(mLayout, "photo_btn,clicked", "photo_button",
                (Edje_Signal_Cb)take_photo_btn_cb, this);
    elm_object_signal_callback_add(mLayout, "choose_from_gallery_btn,clicked", "choose_from_gallery_button",
                (Edje_Signal_Cb)choose_from_gallery_btn_cb, this);

}

GetStartedAddPhoto::~GetStartedAddPhoto()
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "GetStartedAddPhoto Destructor");
}

void GetStartedAddPhoto :: skip_btn_cb(void *data , Evas_Object *obj, const char *emission, const char *source)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Skip button clicked");
    GetStartedAddPhoto *self = static_cast<GetStartedAddPhoto*>(data);
    self->Pop();
}

void GetStartedAddPhoto :: take_photo_btn_cb(void *data , Evas_Object *obj, const char *emission, const char *source)
{
    app_control_h app_control;

    int ret = app_control_create(&app_control);
    if (ret != APP_CONTROL_ERROR_NONE)
    {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "app_control_create() failed.");
        return;
    }

    ret = app_control_set_operation(app_control, APP_CONTROL_OPERATION_CREATE_CONTENT);
    if (ret != APP_CONTROL_ERROR_NONE)
    {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "app_control_set_operation() failed.");
        app_control_destroy(app_control);
        return;
    }

    ret = app_control_set_mime(app_control, IMAGE_MIME_TYPE);
    if (ret != APP_CONTROL_ERROR_NONE)
    {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "app_control_set_mime() failed.");
        app_control_destroy(app_control);
        return;
    }

    if (app_control_send_launch_request(app_control, camera_result_cb, data) == APP_CONTROL_ERROR_NONE)
    {
       dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Succeeded to launch camera app.");
    }
    else
    {
       dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Failed to launch camera app.");
    }

    app_control_destroy(app_control);

}

void GetStartedAddPhoto ::  camera_result_cb(app_control_h request, app_control_h reply, app_control_result_e result, void *data)
{
    GetStartedAddPhoto *self = static_cast<GetStartedAddPhoto*>(data);
    char *path = new char[PATH_MAX];

    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "CAMERA RESULT = %d", result);
    if(result == APP_CONTROL_RESULT_SUCCEEDED)
    {
        self->get_last_file_path_from_camera(&path);

        if(path != NULL){
            dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Got Camera picture path = %s", path);
            self->set_profile_picture(data,path);
        }
        else
            dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Could not Get Camera picture path");
    }
    delete []path;

}

void GetStartedAddPhoto :: get_last_file_path_from_camera(char* *file_path)
{
    DIR *files = opendir(CAMERA_DIRECTORY);
    struct dirent *node = NULL;
    char *cur_image = NULL;
    char *last_image = NULL;

    RETVM_IF(!files, 0, "opendir() failed")
    while ((node = readdir(files)) != NULL)
    {
        cur_image = node->d_name;
        if(cur_image != NULL)
        {
            if(last_image == NULL)
                last_image = cur_image;
            if(strcmp(last_image, cur_image) < 0)
                last_image = cur_image;
            dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "File name = %s", cur_image);
        }
    }
    closedir(files);

    if(last_image)
    {
        Utils::Snprintf_s(*file_path, PATH_MAX, "%s/%s", CAMERA_DIRECTORY, last_image);
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "File path = %s", *file_path);
    }
}

void GetStartedAddPhoto :: choose_from_gallery_btn_cb(void *data , Evas_Object *obj, const char *emission, const char *source)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Choose from gallery button clicked");
    app_control_h app_control;

    int ret = app_control_create(&app_control);
    if (ret != APP_CONTROL_ERROR_NONE)
    {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "app_control_create() failed.");
        return;
    }

    ret = app_control_set_operation(app_control, APP_CONTROL_OPERATION_PICK);
    if (ret != APP_CONTROL_ERROR_NONE)
    {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "app_control_set_operation() failed.");
        app_control_destroy(app_control);
        return;
    }

    ret = app_control_set_mime(app_control, IMAGE_MIME_TYPE);
    if (ret != APP_CONTROL_ERROR_NONE)
    {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "app_control_set_mime() failed.");
        app_control_destroy(app_control);
        return;
    }

    if (app_control_send_launch_request(app_control, file_select_result_cb, data) == APP_CONTROL_ERROR_NONE)
    {
       dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Succeeded to launch gallery app.");
    }
    else
    {
       dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Failed to launch gallery app.");
    }

    app_control_destroy(app_control);

}

void GetStartedAddPhoto :: file_select_result_cb(app_control_h request, app_control_h reply, app_control_result_e result, void *data)
{
    char *value=NULL;
    int ret;

    if (result == APP_CONTROL_RESULT_SUCCEEDED)
    {
        //ret = app_control_get_extra_data(reply, "path" , &value);
        ret = app_control_foreach_extra_data(reply, foreach_extra_data_cb, data);
        if ( ret == APP_CONTROL_ERROR_NONE)
        {
            dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "[app_control_result_cb] Succeeded: value(%s)", value);
        }
        else
        {
            dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "[app_control_result_cb] Failed, Error code: %d", ret);
        }
    }
    else
    {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "[app_control_result_cb] APP_CONTROL_RESULT_FAILED. Error code: %d", result);
    }

}

bool GetStartedAddPhoto :: foreach_extra_data_cb(app_control_h app_control, const char *key, void *data)
{
    char *value;
    int ret;

    GetStartedAddPhoto *self = static_cast<GetStartedAddPhoto*>(data);
    ret = app_control_get_extra_data(app_control, key, &value);
    if ( ret == APP_CONTROL_ERROR_NONE)
    {
        if(!strcmp(key,"path") &&  (value != NULL))
        {
            self->set_profile_picture(data,value);
        }

        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "[app_control_extra_data_cb] Succeeded: key(%s), value(%s)", key, value);
    }
    else
    {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "[app_control_extra_data_cb] Failed: key(%s), Error code: %d", key, ret);
        return FALSE;
    }
    return TRUE;
}

void GetStartedAddPhoto :: set_profile_picture(void *data, const char *path)
{
    ScreenBase *newScreen = (ScreenBase *) new GetStartedProfilePic(path);
    Application::GetInstance()->AddScreen(newScreen);
    elm_naviframe_item_title_enabled_set(elm_naviframe_top_item_get(newScreen->getNf()), EINA_FALSE, EINA_FALSE);
}


