#include "Popup.h"
#include "EventAllPostsScreen.h"
#include "SearchScreen.h"
#include "WidgetFactory.h"

#ifdef EVENT_NATIVE_VIEW

EventAllPostsScreen::EventAllPostsScreen(const char *id) : ScreenBase(NULL), TrackItemsProxyAdapter(NULL, mFeedProvider = new FeedProvider() )
{
    mAction = new FeedAction(this);

    mId = SAFE_STRDUP(id);
    mFeedProvider->SetEntityId(mId);

    mScreenId = SID_EVENT_ALL_POSTS_SCREEN;

    mLayout = WidgetFactory::CreateBaseScreenLayout(this, getParentMainLayout(), "Event Feed", true, true, false);

    ApplyScroller(mLayout, true);
    elm_object_part_content_set(mLayout, "content", GetScroller());

    RequestData();
}

EventAllPostsScreen::~EventAllPostsScreen()
{
    free((void *) mId); mId = NULL;
    delete mAction;
    EraseWindowData();
    mFeedProvider->Release();
}

void EventAllPostsScreen::Push()
{
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

bool EventAllPostsScreen::HandleBackButton()
{
    return PopupMenu::Close();
}

void* EventAllPostsScreen::CreateItem( void* dataForItem, bool isAddToEnd )
{
    Evas_Object *feed = NULL;
    Post *post = static_cast<Post *>(dataForItem);

    ActionAndData *data = NULL;

    if (!post->GetChildId()) {
        data = new ActionAndData(post, mAction, NULL, mId);
        if (post->GetParentId()) {
            feed = WidgetFactory::CreateAffectedPostBox(GetDataArea(), data, isAddToEnd);
        } else {
            feed = WidgetFactory::CreateStatusPost(GetDataArea(), data, isAddToEnd);
        }
        data->mParentWidget = feed;
    }

    return data;
}

void EventAllPostsScreen::DeletePost(ActionAndData *postData)
{
    DistroyItem(postData, TrackItemsProxyAdapter::items_comparator);
}

#endif

