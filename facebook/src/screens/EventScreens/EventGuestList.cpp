/*
 * EventGuestList.cpp
 *
 *  Created on: Nov 2, 2015
 *      Author: ruibezruch
 */
#include <assert.h>

#include "Common.h"
#include "EventGuestTab.h"
#include "EventGuestList.h"
#include "WidgetFactory.h"

#ifdef EVENT_NATIVE_VIEW

const short EventGuestList::MAX_BRING_IN_ATTEMPTS = 30;
const double EventGuestList::BRING_IN_TIME = 0.4;

EventGuestListToTab::EventGuestListToTab()
{
    ffScreen = NULL;
    mTab = EVENT_GOING_TAB;
}

EventGuestListToTab::~EventGuestListToTab()
{
    ffScreen = NULL;
}

EventGuestList::EventGuestList(EventGuestListTabs tab) : ScreenBase(NULL)
{
    mScreenId = SID_EVENT_GUEST;
    mStartTab = tab;

    mGoingSelected = mStartTab == EVENT_GOING_TAB;
    mMaybeSelected = mStartTab == EVENT_MAYBE_TAB;
    mInvitedSelected = mStartTab == EVENT_INVITED_TAB;
    if (!mGoingSelected && !mMaybeSelected && !mInvitedSelected) {
        dlog_print( DLOG_DEBUG, LOG_TAG_FACEBOOK, "EventGuestList: Can't get current start tab" );
        assert( false );
    }
    mAttemptCounter = 0;

    mLayout = CreateView(getParentMainLayout());
    mBringInScrollerTimer = ecore_timer_add( BRING_IN_TIME, bring_in_cb, this );
}

EventGuestList::~EventGuestList()
{
    if ( mBringInScrollerTimer ) {
        ecore_timer_del( mBringInScrollerTimer );
    }
    elm_object_signal_callback_del( mLayout, "back.clicked", "back*", (Edje_Signal_Cb) BackButtonClicked );
    evas_object_smart_callback_del( mScroller, "scroll,anim,stop", anim_stop_scroll_cb );

    elm_object_signal_callback_del( mGoingTab, "item.clicked", "", (Edje_Signal_Cb) tabbar_cb);
    elm_object_signal_callback_del( mMaybeTab, "item.clicked", "", (Edje_Signal_Cb) tabbar_cb);
    elm_object_signal_callback_del( mInvitedTab, "item.clicked", "", (Edje_Signal_Cb) tabbar_cb);
}

Evas_Object *EventGuestList::CreateView(Evas_Object *parent)
{
    Evas_Object *layout = WidgetFactory::CreateLayoutByGroup(parent, "guest_list_screen");

    elm_object_part_text_set(layout, "title", "Guest List");  //TODO: localization

    mTabbar = CreateTabbar(layout);
    elm_object_part_content_set (layout, "tabbar", mTabbar);

    Evas_Object *box = elm_box_add(layout);
    evas_object_size_hint_weight_set(box, 1, 1);
    evas_object_size_hint_align_set(box, -1, -1);
    elm_object_part_content_set(layout, "content", box);

    mScroller = CreateScroller(box);
    elm_box_pack_end(box, mScroller);

    mNestedScreenContainer = elm_box_add(mScroller);
    elm_box_horizontal_set(mNestedScreenContainer, EINA_TRUE);
    evas_object_size_hint_weight_set(mNestedScreenContainer, 1, 1);
    evas_object_size_hint_align_set(mNestedScreenContainer, -1, -1);
    elm_object_content_set(mScroller, mNestedScreenContainer);

    mGoingScreen = new EventGuestTab(this, EVENT_GOING_TAB);
    mFirstViewRect = MinSet(mGoingScreen->getMainLayout(), mNestedScreenContainer, 0, 0);
    mMaybeScreen = new EventGuestTab(this, EVENT_MAYBE_TAB);
    mSecondViewRect = MinSet(mMaybeScreen->getMainLayout(), mNestedScreenContainer, 0, 0);
    mInvitedScreen = new EventGuestTab(this, EVENT_INVITED_TAB);
    mThirdViewRect = MinSet(mInvitedScreen->getMainLayout(), mNestedScreenContainer, 0, 0);

    Evas_Coord w, h;
    evas_object_geometry_get(Application::GetInstance()->mWin, NULL, NULL, &w, &h);
    OnViewResize(w, h);

    elm_object_signal_callback_add(layout, "back.clicked", "back*", (Edje_Signal_Cb) BackButtonClicked, this);

    return layout;
}

void EventGuestList::Push()
{
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void EventGuestList::BackButtonClicked(void *data, Evas_Object *obj, void *event_info)
{
    if(Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_EVENT_GUEST) {
        EventGuestList *screen = static_cast<EventGuestList *>(data);
        if(screen) {
            screen->Pop();
        }
    }
}

void EventGuestList::OnViewResize(Evas_Coord w, Evas_Coord h)
{
    elm_scroller_page_size_set(mScroller, w, h);
    evas_object_size_hint_min_set(mFirstViewRect, R->FF_MAIN_RECT_SIZE_W, R->FF_MAIN_RECT_SIZE_H);
    evas_object_size_hint_min_set(mSecondViewRect, R->FF_MAIN_RECT_SIZE_W, R->FF_MAIN_RECT_SIZE_H);
    evas_object_size_hint_min_set(mThirdViewRect, R->FF_MAIN_RECT_SIZE_W, R->FF_MAIN_RECT_SIZE_H);
}

Evas_Object *EventGuestList::CreateTabbar(Evas_Object *parent)
{
    Evas_Object *scroller = elm_scroller_add(parent);
    elm_scroller_page_size_set(scroller, R->FF_MAIN_RECT_SIZE_W, 0);
    elm_scroller_single_direction_set(scroller, ELM_SCROLLER_SINGLE_DIRECTION_HARD);
    elm_scroller_page_scroll_limit_set(scroller, 1, 1);
    elm_scroller_policy_set(scroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_OFF);
    evas_object_show(scroller);

    Evas_Object *data_area = elm_box_add(scroller);
    evas_object_size_hint_weight_set(data_area, 1, 1);
    evas_object_size_hint_align_set(data_area, -1, -1);
    elm_box_padding_set(data_area, 0, 0);
    elm_box_horizontal_set(data_area, EINA_TRUE);
    elm_object_content_set(scroller, data_area);
    evas_object_show(data_area);

    mTopSelectedId = mStartTab;
    mLowSelectedId = mStartTab;

    mGoingTab = CreateTabbarItem(data_area, "Going", mGoingSelected, EVENT_GOING_TAB);
    mMaybeTab = CreateTabbarItem(data_area, "Maybe", mMaybeSelected, EVENT_MAYBE_TAB);
    mInvitedTab = CreateTabbarItem(data_area, "Invited", mInvitedSelected, EVENT_INVITED_TAB);

    return scroller;
}

Evas_Object *EventGuestList::CreateTabbarItem(Evas_Object *parent, const char *text2set, bool isSelected, EventGuestListTabs tab)
{
    Evas_Object *item = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(parent, "tabbar_item");

    char *text = NULL;
    Evas_Object *label = elm_label_add(item);
    elm_object_part_content_set(item, "item_text", label);
    evas_object_size_hint_weight_set(label, 1, 1);
    evas_object_size_hint_align_set(label, -1, 0.5);
    text = WidgetFactory::WrapByFormat2(SANS_MEDIUM_CENTER_9197a3_FORMAT, R->EVENT_GUESTLIST_TABTEXT_SIZE, text2set);
    if (text) {
        elm_object_text_set(label, text);
        delete[] text;
    }
    evas_object_show(label);

    EventGuestListToTab *to_data = new EventGuestListToTab;
    to_data->ffScreen = this;
    to_data->mTab = tab;

    if (isSelected) {
        elm_object_signal_emit(item, "set.selected", "");
    }

    elm_object_signal_callback_add(item, "item.clicked", "", (Edje_Signal_Cb) tabbar_cb, to_data);


    return item;
}

Evas_Object *EventGuestList::CreateScroller(Evas_Object *parent)
{
    Evas_Object *scroller = elm_scroller_add(parent);
    elm_scroller_policy_set(scroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_OFF);
    elm_scroller_page_size_set(scroller, 480, 0);
    elm_scroller_page_scroll_limit_set(scroller, 1, 1);
    elm_scroller_single_direction_set(scroller, ELM_SCROLLER_SINGLE_DIRECTION_HARD);
    evas_object_size_hint_weight_set(scroller, 1, 1);
    evas_object_size_hint_align_set(scroller, -1, -1);
    evas_object_smart_callback_add(scroller, "scroll,anim,stop", anim_stop_scroll_cb, this);
    evas_object_show(scroller);

    return scroller;
}

void EventGuestList::anim_stop_scroll_cb(void *data, Evas_Object *obj, void *event_info)
{
    EventGuestList *fs = static_cast<EventGuestList *>(data);
    int page = 0;
    elm_scroller_current_page_get(fs->mScroller, &page, NULL);

    if (!(page >= EVENT_GOING_TAB && page <= EVENT_INVITED_TAB)) {
        dlog_print( DLOG_DEBUG, LOG_TAG_FACEBOOK, "EventGuestList: out of range page" );
        assert( false );
        return;
    }
    fs->mLowSelectedId = fs->mTopSelectedId;

    fs->mTopSelectedId = (EventGuestListTabs) page;

    if (page == EVENT_GOING_TAB) {
        elm_scroller_page_bring_in(fs->mTabbar, 0, 0);
        EventGuestTab *tab = static_cast<EventGuestTab*>(fs->mGoingScreen);
        tab->ShiftDown();
    } else if (page == EVENT_MAYBE_TAB) {
        elm_scroller_page_bring_in(fs->mTabbar, 1, 0);
        EventGuestTab *tab = static_cast<EventGuestTab*>(fs->mMaybeScreen);
        tab->ShiftDown();
    } else if (page == EVENT_INVITED_TAB) {
        elm_scroller_page_bring_in(fs->mTabbar, 2, 0);
        EventGuestTab *tab = static_cast<EventGuestTab*>(fs->mInvitedScreen);
        tab->ShiftDown();
    }

    fs->DoAnimation(fs->mLowSelectedId, fs->mTopSelectedId);
}

void EventGuestList::DoAnimation(EventGuestListTabs wasChoosen, EventGuestListTabs isChoosen)
{
    if (wasChoosen == EVENT_GOING_TAB) {
        elm_object_signal_emit(mGoingTab, "set.unselected", "");
    } else if (wasChoosen == EVENT_MAYBE_TAB) {
        elm_object_signal_emit(mMaybeTab, "set.unselected", "");
    } else if (wasChoosen == EVENT_INVITED_TAB) {
        elm_object_signal_emit(mInvitedTab, "set.unselected", "");
    }

    if (isChoosen == EVENT_GOING_TAB) {
        elm_object_signal_emit(mGoingTab, "set.selected", "");
    } else if (isChoosen == EVENT_MAYBE_TAB) {
        elm_object_signal_emit(mMaybeTab, "set.selected", "");
    } else if (isChoosen == EVENT_INVITED_TAB) {
        elm_object_signal_emit(mInvitedTab, "set.selected", "");
    }
}

void EventGuestList::tabbar_cb(void *data, Evas_Object *obj, void * event_info)
{
    EventGuestListToTab *to_data = static_cast<EventGuestListToTab*>(data);
    EventGuestList *fs = static_cast<EventGuestList*>(to_data->ffScreen);

    fs->TabbarCb(to_data->mTab);
}

void EventGuestList::TabbarCb(EventGuestListTabs tab)
{
    if ( tab >= EVENT_GOING_TAB && tab <= EVENT_INVITED_TAB ) {
        elm_scroller_page_bring_in(mScroller, tab, 0);
        elm_scroller_page_bring_in(mTabbar, tab, 0);

        mLowSelectedId = mTopSelectedId;
        mTopSelectedId = tab;
        DoAnimation(mLowSelectedId, mTopSelectedId);
    }
}

short EventGuestList::AttempBringInScroll()
{
    return ++mAttemptCounter;
}

Eina_Bool EventGuestList::bring_in_cb( void *data )
{
    EventGuestList *guestList = static_cast<EventGuestList*>(data);
    int scrollPage = 0;
    elm_scroller_current_page_get(guestList->mScroller, &scrollPage, NULL);
    if ( scrollPage == guestList->mStartTab || guestList->AttempBringInScroll() > EventGuestList::MAX_BRING_IN_ATTEMPTS ) {
        guestList->mBringInScrollerTimer = NULL;
        return ECORE_CALLBACK_CANCEL;
    } else {
        guestList->TabbarCb( guestList->mStartTab );
        return ECORE_CALLBACK_RENEW;
    }
}

#endif
