#include <Eina.h>

#include "Common.h"
#include "Config.h"
#include "ConnectivityManager.h"
#include "DataUploader.h"
#include "EventBatchProvider.h"
#include "EventGuestListFactory.h"
#include "EventProfilePage.h"
#include "FbRespondGetEvent.h"
#include "FbRespondGetFeed.h"
#include "FriendsProvider.h"
#include "jsonutilities.h"
#include "Log.h"
#include "OperationManager.h"
#include "ParserWrapper.h"
#include "Popup.h"
#include "Post.h"
#include "ProfileWidgetFactory.h"
#include "SearchScreen.h"
#include "SuggestedFriendsProvider.h"
#include "Utils.h"
#include "WidgetFactory.h"

#ifdef EVENT_NATIVE_VIEW

//we need to show only first 5 friends who goes to an event
int EventProfilePage::NUMBER_OF_FRIENDS_TO_SHOW = 5;

EventProfilePage::EventProfilePage(const char *id, const char * name, Event *event) : ScreenBase(NULL)
{
    mActionAndDataForAvatarsDownload = new ActionAndData();

    mSelectFriendsScreen = NULL;

    mRequestData = NULL;
    mRequestLastPost = NULL;
    mRequestInviteFriends = NULL;
//    mRerequestBar = NULL;

    mLoadingPost = NULL;
    mPostWidget = NULL;
    mViewAllWidget = NULL;
    mStatsBox = NULL;
    mEventStatisticWidget = NULL;
    mProfileFriends = NULL;
    mUsersPhotosBox = NULL;
    mMyPhotoWidget = NULL;
    mTheLastPhotoWidget = NULL;

    m_GoingUsersDataUploader = NULL;
    m_MaybeUsersDataUploader = NULL;
    m_DeclinedUsersDataUploader = NULL;
    m_InvitedUsersDataUploader = NULL;

    mId = NULL;
    mActionAndData = NULL;

    mIsEventProfileUploadCompleted = false;

    isUsingSavedData = false;
    isReRequest = false;
    isAfterEdit = false;
    mMe = NULL;
    mFriendsPhotosCreated = 0;
    mIsUICreated = false;

    mAction = new EventActions(this);
    mFeedAction = new FeedAction(this);

    mScreenId = SID_EVENT_PROFILE_PAGE;

    mLayout = WidgetFactory::CreateBaseScreenLayout(this, getParentMainLayout(), name ? name :i18n_get_text("IDS_SETTING_EVENTS"), true, true, false);

    elm_object_translatable_part_text_set(mLayout, "error_text", "IDS_EVENT_NOT_AVAILABLE");

    mScroller = elm_scroller_add(mLayout);
    elm_object_part_content_set(mLayout, "content", mScroller);
    elm_scroller_single_direction_set(mScroller, ELM_SCROLLER_SINGLE_DIRECTION_HARD);
    elm_scroller_policy_set(mScroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_AUTO);
    evas_object_show(mScroller);

    mDataArea = elm_box_add(mScroller);
    elm_box_homogeneous_set(mDataArea, EINA_FALSE);
    elm_object_content_set(mScroller, mDataArea);
    evas_object_show( mDataArea );

    mId = SAFE_STRDUP(id);

    SetEntityId(mId);

    if (event) {
        mActionAndData = new ActionAndData(event, mAction);
        isUsingSavedData = true;
        SetData(mActionAndData);
    }

    if (ConnectivityManager::Singleton().IsConnected()) {
        mRequestMyAvatar = FacebookSession::GetInstance()->GetMyAvatar(100, 100, on_get_my_avatar_completed, this);
        RequestData();

        if (name) {
            elm_object_part_text_set(mLayout, "title", name);
        } else {
            elm_object_translatable_part_text_set(mLayout, "title", "IDS_SETTING_EVENTS");
        }
    }

    DoTinyPadding();

    // Now we do nothing with suggested friends in the screen. This is just needed to speedup suggested friends downloading for other screens which use them.
    mSuggestedFriendsUploader = new DataUploader(SuggestedFriendsProvider::GetInstance(), on_get_suggested_friends_completed, this);
    mSuggestedFriendsUploader->StartUploading();

    evas_object_smart_callback_add(mScroller, "edge,top", scroll_cb, this);

    OperationManager::GetInstance()->Subscribe(this);
    AppEvents::GetInstance()->Subscribe(eEVENT_PROFILE_UPDATE_EVENT, this);
    AppEvents::GetInstance()->Subscribe(eEVENT_PROFILE_UPDATE_STATS_BOX, this);
    AppEvents::GetInstance()->Subscribe(eINTERNET_CONNECTION_CHANGED, this);
}

EventProfilePage::~EventProfilePage()
{
    OperationManager::GetInstance()->UnSubscribe(this);
    AppEvents::GetInstance()->Unsubscribe(eEVENT_PROFILE_UPDATE_EVENT, this);
    AppEvents::GetInstance()->Unsubscribe(eEVENT_PROFILE_UPDATE_STATS_BOX, this);
    AppEvents::GetInstance()->Unsubscribe(eINTERNET_CONNECTION_CHANGED, this);

    if (mRequestData) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(mRequestData);
    }

    if (mRequestMyAvatar) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(mRequestMyAvatar);
    }

    if (mRequestLastPost) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(mRequestLastPost);
    }

    if (mRequestInviteFriends) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(mRequestInviteFriends);
    }

    if (mAction) {
        delete mAction;
    }
    if (mFeedAction) {
        delete mFeedAction;
    }

    if (mActionAndData) {
        delete mActionAndData;
    }

    if (mActionAndDataForAvatarsDownload) {
        delete mActionAndDataForAvatarsDownload;
    }

    if (mId) {
        free((void *) mId); mId = NULL;
    }

    if (m_GoingUsersDataUploader) {
        delete m_GoingUsersDataUploader;
    }
    if (m_MaybeUsersDataUploader) {
        delete m_MaybeUsersDataUploader;
    }
    if (m_InvitedUsersDataUploader) {
        delete m_InvitedUsersDataUploader;
    }
    if (m_DeclinedUsersDataUploader) {
        delete m_DeclinedUsersDataUploader;
    }
    if (mSuggestedFriendsUploader) {
        delete mSuggestedFriendsUploader;
    }

    if (mSelectFriendsScreen) {
        mSelectFriendsScreen->UnregisterClient();
    }

    delete mMe;
}

void EventProfilePage::DoTinyPadding()
{
    int padding = 3;

    Evas_Coord x, y, w, h;
    elm_scroller_region_get(mScroller, &x, &y, &w, &h );
    elm_scroller_region_show(mScroller, x, padding, w, h );
}

void EventProfilePage::scroll_cb(void *data, Evas_Object *obj, void *event_info)
{
    EventProfilePage *me = static_cast<EventProfilePage*>(data);
    me->ScrollCb();
}

void EventProfilePage::ScrollCb()
{
    Log::info(LOG_FACEBOOK_EVENT, "EventProfilePage::ScrollCb");

    if (mRequestData == NULL && !isReRequest && !isAfterEdit) {
        isReRequest = true;
        isUsingSavedData = false;

        SetEntityId(mId);

        RequestData();

//        if (!mRerequestBar) {
//            mRerequestBar = ProfileWidgetFactory::CreateLoadingItem(mDataArea, false, false);
//        }
    }
}

void EventProfilePage::SetEntityId(const char * id)
{
    if (id) {
        EventGuestListFactory::GetProvider(EVENT_GOING_TAB)->EraseData();
        EventGuestListFactory::GetProvider(EVENT_GOING_TAB)->SetEntityId(id);
        EventGuestListFactory::GetProvider(EVENT_MAYBE_TAB)->EraseData();
        EventGuestListFactory::GetProvider(EVENT_MAYBE_TAB)->SetEntityId(id);
        EventGuestListFactory::GetProvider(EVENT_INVITED_TAB)->EraseData();
        EventGuestListFactory::GetProvider(EVENT_INVITED_TAB)->SetEntityId(id);
        EventGuestListFactory::GetProvider(EVENT_DECLINED)->EraseData();
        EventGuestListFactory::GetProvider(EVENT_DECLINED)->SetEntityId(id);
    }

    delete m_GoingUsersDataUploader; m_GoingUsersDataUploader = NULL;
    delete m_MaybeUsersDataUploader; m_MaybeUsersDataUploader = NULL;
    delete m_InvitedUsersDataUploader; m_InvitedUsersDataUploader = NULL;
    delete m_DeclinedUsersDataUploader; m_DeclinedUsersDataUploader = NULL;
}

void EventProfilePage::RequestData()
{
    Log::info(LOG_FACEBOOK_EVENT, "EventProfilePage::RequestData");
    if (mRequestData == NULL) {
        if (!isReRequest) {
            ProgressBarShow();
        }
        mIsEventProfileUploadCompleted = false;
        mRequestData = FacebookSession::GetInstance()->GetEventProfileInfo(mId, on_get_event_profile_completed, this);
    }
}

void EventProfilePage::on_get_event_profile_completed(void *object, char *response, int code)
{
    Log::info(LOG_FACEBOOK_EVENT, "EventProfilePage::on_get_event_profile_completed");

    EventProfilePage *me = static_cast<EventProfilePage*>(object);

    if (me) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(me->mRequestData);
        me->isReRequest = false;

        if (code == CURLE_OK) {
            Log::debug(LOG_FACEBOOK_EVENT, "response: %s", response);
            FbRespondGetEvent * eventResponse = FbRespondGetEvent::createFromJson(response);
            if (!eventResponse) {
                Log::error(LOG_FACEBOOK_EVENT, "EventProfilePage::Error parsing http response");
            } else {
                if (eventResponse->mError) {
                    me->ProgressBarHide();
                    elm_box_clear(me->mDataArea);
                    elm_object_signal_emit(me->mLayout, "error.show", "");
                    Log::error(LOG_FACEBOOK_EVENT, "EventProfilePage::Error getting friend requests, error = %s", eventResponse->mError->mMessage);
                } else {
                    void *listData = eina_list_last_data_get(eventResponse->mEventsList);
                    Event *event = static_cast<Event*>(listData);
                    eventResponse->RemoveDataFromList(listData);

                    me->ActionAndDataCreate(event);

                    me->isUsingSavedData = false;
                    me->mIsEventProfileUploadCompleted = true;

                    me->m_GoingUsersDataUploader = new DataUploader(EventGuestListFactory::GetProvider(EVENT_GOING_TAB), friends_has_been_received_cb, me);
                    me->m_GoingUsersDataUploader->StartUploading();

                    me->m_MaybeUsersDataUploader = new DataUploader(EventGuestListFactory::GetProvider(EVENT_MAYBE_TAB), friends_has_been_received_cb, me);
                    me->m_MaybeUsersDataUploader->StartUploading();

                    me->m_InvitedUsersDataUploader = new DataUploader(EventGuestListFactory::GetProvider(EVENT_INVITED_TAB), friends_has_been_received_cb, me);
                    me->m_InvitedUsersDataUploader->StartUploading();

                    me->m_DeclinedUsersDataUploader = new DataUploader(EventGuestListFactory::GetProvider(EVENT_DECLINED), friends_has_been_received_cb, me);
                    me->m_DeclinedUsersDataUploader->StartUploading();
                }
            }
            delete eventResponse;
        } else {
            Log::error(LOG_FACEBOOK_EVENT, "EventProfilePage::Error sending http request");
//            if (me->mRerequestBar) {
//                evas_object_del(me->mRerequestBar);
//                me->mRerequestBar = NULL;
//            }
            if (!me->mIsUICreated) {
                me->ProgressBarHide();
                me->ShowErrorWidget(ScreenBase::eCONNECTION_ERROR);
            }
        }

        me->DoTinyPadding();
    }

    free(response);    //Attention!! client should take care of freeing the response buffer
}

void EventProfilePage::ActionAndDataCreate(Event *event)
{
    if (mActionAndData) {
        mActionAndData->ReSetData(event);
    } else {
        mActionAndData = new ActionAndData(event, mAction);
    }
}

void EventProfilePage::SetData(ActionAndData *action_data)
{
    Log::info(LOG_FACEBOOK_EVENT, "EventProfilePage::SetData");
    if (action_data) {
        elm_box_clear(mDataArea);
//        if (mRerequestBar) {
//            evas_object_del(mRerequestBar); mRerequestBar = NULL;
//        }
        mPostWidget = NULL;
        mIsUICreated = true;

        Event *event = static_cast<Event*>(action_data->mData);

        ProfileWidgetFactory::CreateCoverItem(mDataArea, action_data);

        Evas_Object *box = elm_box_add(mDataArea);
        evas_object_size_hint_weight_set(box, 1.0, 1.0);
        evas_object_size_hint_align_set(box, -1, -1);
        elm_box_pack_end(mDataArea, box);
        evas_object_show(box);

        if (event->mOwner && Utils::IsMe(event->mOwner->GetId())) {
            ProfileWidgetFactory::Create4ActionBtsItem(box, action_data, isUsingSavedData);
        } else {
            if (event->mRsvpStatus == Event::EVENT_RSVP_NOT_REPLIED) {
                ProfileWidgetFactory::Create4ActionBtsItem(box, action_data, isUsingSavedData);
                event->isInvited = true;
            } else if (event->mCanGuestsInvite) {
                ProfileWidgetFactory::Create4ActionBtsItem(box, action_data, isUsingSavedData);
            } else {
                ProfileWidgetFactory::Create3ActionBtsItem(box, action_data, isUsingSavedData);
            }
        }

        if (event->mStartTime && event->mEndTime) {
            ProfileWidgetFactory::CreateSingleInfoField(mDataArea, ProfileWidgetFactory::FIELD_DATE, ICON_DIR "/Events/event_time.png", event->GetStartTime(), event->GetEndTime(), action_data);
        } else if (event->mStartTime) {
            ProfileWidgetFactory::CreateSingleInfoField(mDataArea, ProfileWidgetFactory::FIELD_DATE, ICON_DIR "/Events/event_time.png", event->GetStartTime(), NULL, action_data);
        }

        if (event->mPlace && event->mPlace->mLocation && event->mPlace->mName) {
            const char *location = event->mPlace->mLocation->GetAddress();
            ProfileWidgetFactory::CreateSingleInfoField(mDataArea, ProfileWidgetFactory::FIELD_PLACE, ICON_DIR "/Events/event_location.png", event->mPlace->mName, location, action_data);
            delete [] location;

        } else if (event->mPlace && event->mPlace->mName) {
            ProfileWidgetFactory::CreateSingleInfoField(mDataArea, ProfileWidgetFactory::FIELD_PLACE, ICON_DIR "/Events/event_location.png", event->mPlace->mName, NULL, action_data);
        }
        if ( event->GetTicketURI() ) {
            ProfileWidgetFactory::CreateSingleInfoField(mDataArea, ProfileWidgetFactory::FIELD_TICKETS, ICON_DIR "/Events/event_location.png", i18n_get_text("IDS_TICKETS"), Utils::FindUrlInText(event->GetTicketURI(), 0, true), action_data);
        }

        if (event->mDescription) {
            ProfileWidgetFactory::CreateAboutProfileItem(mDataArea, action_data);
        }

        if (event->mName) {
            elm_object_part_text_set(mLayout, "title", event->mName);
        }

        if (event->mGuestListsEnabled || (event->mOwner && Utils::IsMe(event->mOwner->GetId()))) {
            if (!isUsingSavedData) {
                Log::info(LOG_FACEBOOK_EVENT, "!isUsingSavedData");
                mStatsBox = ProfileWidgetFactory::CreateProfileItemWrapper(mDataArea);
                mEventStatisticWidget = ProfileWidgetFactory::CreateSimpleProfileStatistic(mStatsBox, action_data);
            }
        }

        mLoadingPost = ProfileWidgetFactory::CreateLoadingItemWrapper(mDataArea);

        RequestLastPost();

        CreateInviteFriendsItem();
    }
}

void EventProfilePage::CreateInviteFriendsItem()
{
    Evas_Object *content = ProfileWidgetFactory::CreateProfileItemWrapper(mDataArea);

    Evas_Object *invite_item = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(content, "invite_friends_item");

    elm_object_translatable_part_text_set(invite_item, "text", "IDS_EVENT_PROFILE_INVITE_FRIENDS");
    elm_object_signal_callback_add(invite_item, "mouse,clicked,*", "spacer", on_invite_friends_clicked, this);
}

void EventProfilePage::friends_has_been_received_cb(void *data)
{
    EventProfilePage *me = static_cast<EventProfilePage*>(data);
    if (me) {
        if (me->IsUploadCompleted()) {
            //AAA ToDo: Probably we need to refresh something else on the screen
            me->CreateEventProfileFriends();
        }
    }
}

void EventProfilePage::CreateEventProfileFriends()
{
    if (mActionAndData) {

        ProgressBarHide();
        Event *event = static_cast<Event*>(mActionAndData->mData);

        if (event->mRsvpStatus == Event::EVENT_RSVP_NONE) {
            event->mRsvpStatus = EventGuestListFactory::GetLoggedInUserStatus();
            EventBatchProvider::GetInstance()->EventStatusSet(event->GetId(), event->mRsvpStatus);
        }

        EventBatchProvider::GetInstance()->ReSetDataByEvent(event);
        AppEvents::GetInstance()->Notify(eEVENT_ITEM_CHANGE_EVENT, event);

        Log::debug(LOG_FACEBOOK_EVENT, "CreateEventProfileFriends::NewEventID %s", event->GetId());

        SetData(mActionAndData);

        if (mProfileFriends) {
            //remove previous widget first if it exists
            elm_object_signal_callback_del(mProfileFriends, "mouse,clicked,*", "spacer", on_profile_friends_clicked_cb);
            evas_object_hide(mProfileFriends);
            elm_box_unpack(mStatsBox, mProfileFriends);
            evas_object_del(mProfileFriends);
            mProfileFriends = NULL;
            mUsersPhotosBox = NULL;
        }

        const Eina_List * goingFriends = EventGuestListFactory::GetProvider(EVENT_GOING_TAB)->GetFriendsList();
        const Eina_List * maybeFriends = EventGuestListFactory::GetProvider(EVENT_MAYBE_TAB)->GetFriendsList();
        if (event->mRsvpStatus == Event::EVENT_RSVP_ATTENDING || event->mRsvpStatus == Event::EVENT_RSVP_MAYBE
                || (goingFriends && eina_list_count(goingFriends) > 0) || (maybeFriends && eina_list_count(maybeFriends) > 0)) {
            mProfileFriends = elm_layout_add(mStatsBox);
            elm_layout_file_set(mProfileFriends, Application::mEdjPath, "event_profile_friends");
            elm_object_signal_callback_add(mProfileFriends, "mouse,clicked,*", "spacer", on_profile_friends_clicked_cb, this);
            elm_box_pack_start(mStatsBox, mProfileFriends);
            evas_object_show(mProfileFriends);

            mUsersPhotosBox = elm_box_add(mProfileFriends);
            elm_object_part_content_set(mProfileFriends, "photos_box", mUsersPhotosBox);
            elm_box_horizontal_set(mUsersPhotosBox, EINA_TRUE);

            int goingFriendsAdded = 0;
            int maybeFriendsAdded = 0;
            goingFriendsAdded = AddEventProfileFriend(goingFriends);
            if (goingFriendsAdded < NUMBER_OF_FRIENDS_TO_SHOW && maybeFriends) {
                maybeFriendsAdded = AddEventProfileFriend(maybeFriends, goingFriendsAdded, true);
            }
            mFriendsPhotosCreated = goingFriendsAdded + maybeFriendsAdded;

            // The logic is to create widgets for "me" and 5 friends and hide the last one.
            // The first user is "me" and when you click - "Not going" then "me" will hide and the last friend will show.
            // So we always show only 5 users.
            mMyPhotoWidget = CreateUserPhoto(mMe, false, false);
            if (event->mRsvpStatus == Event::EVENT_RSVP_MAYBE) {
                elm_object_signal_emit(mMyPhotoWidget, "set_state_maybe", "");
            }

            if (event->mRsvpStatus == Event::EVENT_RSVP_ATTENDING || event->mRsvpStatus == Event::EVENT_RSVP_MAYBE) {
                HideTheLastPhoto();
            }
            else {
                HideMyPhoto();
            }

            if (mFriendsPhotosCreated > 0) {
                char *text = NULL;
                if (goingFriendsAdded) {
                    text = GenerateFriendsText(goingFriends);
                } else if (maybeFriendsAdded) {
                    text = GenerateFriendsText(maybeFriends, true);
                }
                if (text) {
                    elm_object_signal_emit(mProfileFriends, "show_text", "");
                    elm_object_part_text_set(mProfileFriends, "text.text", text);
                    delete[] text;
                } else {
                    elm_object_signal_emit(mProfileFriends, "hide_text", "");
                }
            } else {
                elm_object_signal_emit(mProfileFriends, "hide_text", "");
            }
        }
    }
}

int EventProfilePage::AddEventProfileFriend(const Eina_List * friendsList, int addedCount, bool isMaybeState)
{
    Eina_List * listItem;
    void * listData;
    int i = 0;
    EINA_LIST_FOREACH(const_cast<Eina_List *> (friendsList), listItem, listData) {
        if (addedCount + i++ == NUMBER_OF_FRIENDS_TO_SHOW) {
            break;
        }

        mTheLastPhotoWidget = CreateUserPhoto(static_cast<User *>(listData), isMaybeState, true);
    }

    return i;
}

Evas_Object * EventProfilePage::CreateUserPhoto(User * user, bool isMaybeState, bool isAddToEnd)
{
    Evas_Object *photo = elm_layout_add(mUsersPhotosBox);
    elm_layout_file_set(photo, Application::mEdjPath, "event_profile_friend_photo");
    if (isAddToEnd) {
        elm_box_pack_end(mUsersPhotosBox, photo);
    } else {
        elm_box_pack_start(mUsersPhotosBox, photo);
    }
    evas_object_show(photo);
    //default state is attending, otherwise set "set_state_maybe".
    if (isMaybeState) {
        elm_object_signal_emit(photo, "set_state_maybe", "");
    }

    Evas_Object *avatar = elm_image_add(photo);
    elm_object_part_content_set(photo, "photo", avatar);
    if(user && user->mPicture) {
        if (user->mPicture->mIsSilhouette) {
            elm_image_file_set(avatar, ICON_DIR "/Events/event_empty_profile_image.png", NULL);
        } else {
            mActionAndDataForAvatarsDownload->UpdateImageLayoutAsync(user->mPicture->mUrl, avatar, ActionAndData::EImage);
        }
    }

    return photo;
}

void EventProfilePage::ShowTheLastPhoto()
{
    if (mTheLastPhotoWidget && !evas_object_visible_get(mTheLastPhotoWidget) && mFriendsPhotosCreated == NUMBER_OF_FRIENDS_TO_SHOW) {
        elm_box_pack_end(mUsersPhotosBox, mTheLastPhotoWidget);
        evas_object_show(mTheLastPhotoWidget);
    }
}

void EventProfilePage::HideTheLastPhoto()
{
    if (mTheLastPhotoWidget && evas_object_visible_get(mTheLastPhotoWidget) && mFriendsPhotosCreated == NUMBER_OF_FRIENDS_TO_SHOW) {
        elm_box_unpack(mUsersPhotosBox, mTheLastPhotoWidget);
        evas_object_hide(mTheLastPhotoWidget);
    }
}

void EventProfilePage::ShowMyPhoto()
{
    if (mMyPhotoWidget && !evas_object_visible_get(mMyPhotoWidget)) {
        elm_box_pack_start(mUsersPhotosBox, mMyPhotoWidget);
        evas_object_show(mMyPhotoWidget);
    }
}

void EventProfilePage::HideMyPhoto()
{
    if (mMyPhotoWidget && evas_object_visible_get(mMyPhotoWidget)) {
        elm_box_unpack(mUsersPhotosBox, mMyPhotoWidget);
        evas_object_hide(mMyPhotoWidget);
    }
}

void EventProfilePage::UpdateStatsBox()
{
    if (!mActionAndData || !mActionAndData->mData) {
        return;
    }
    Event *event = static_cast<Event*>(mActionAndData->mData);

    if (event->mRsvpStatus == Event::EVENT_RSVP_ATTENDING) {
        ShowMyPhoto();
        elm_object_signal_emit(mMyPhotoWidget, "set_state_attending", "");

        HideTheLastPhoto();
    }
    else if (event->mRsvpStatus == Event::EVENT_RSVP_MAYBE) {
        ShowMyPhoto();
        elm_object_signal_emit(mMyPhotoWidget, "set_state_maybe", "");

        HideTheLastPhoto();
    }
    else if (event->mRsvpStatus == Event::EVENT_RSVP_DECLINED) {
        HideMyPhoto();
        ShowTheLastPhoto();
    }
    ProfileWidgetFactory::UpdateSimpleProfileStatistic(mEventStatisticWidget, event);
}

char *EventProfilePage::GenerateFriendsText(const Eina_List * friendsList, bool isMaybeState)
{
// AAA ToDo: this is simple implementation of function. Lets extend it later for other use cases (more than 1 friend is going etc).
// ToDo: Add localization for this method
    char isGoing[] = "%s is going";
    char mightGo[] = "%s might go";
    char *resText = NULL;

    Eina_List *list;
    void *data;
    EINA_LIST_FOREACH(const_cast<Eina_List *> (friendsList), list, data) {
        User *user = static_cast<User*>(data);
        if (!Utils::IsMe(user->GetId()) && user->mName) {
            char *pattern = NULL;
            if (isMaybeState) {
                pattern = mightGo;
            } else {
                pattern = isGoing;
            }
            int size = strlen(pattern) + strlen(user->mName) + 1;
            resText = new char[size];
            Utils::Snprintf_s(resText, size, pattern, user->mName);
            break;
        }
    }
    return resText;
}

void EventProfilePage::on_profile_friends_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    ScreenBase *event_guest_list = new EventGuestList(EVENT_GOING_TAB);
    Application::GetInstance()->AddScreen(event_guest_list);
}

void EventProfilePage::RequestLastPost()
{
    Log::info(LOG_FACEBOOK_EVENT, "EventProfilePage::RequestLastPost");
    if (mRequestLastPost == NULL) {
        mRequestLastPost = FacebookSession::GetInstance()->GetLastFeedPost(mId, on_get_last_post_completed, this);
    }
}

void EventProfilePage::on_get_last_post_completed(void *object, char *response, int code)
{
    Log::info(LOG_FACEBOOK_EVENT, "EventProfilePage::on_get_last_post_completed");

    JsonObject *json_object = NULL;
    JsonParser *parser = openJsonParser(response, &json_object);
    ParserWrapper wr( parser );

    EventProfilePage *me = static_cast<EventProfilePage*>(object);

    FacebookSession::GetInstance()->ReleaseGraphRequest(me->mRequestLastPost);

    if (code == CURLE_OK) {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "response: %s", response);
        FbRespondGetFeed * postResponse = new FbRespondGetFeed( json_object );
        if (postResponse == NULL) {
            Log::error(LOG_FACEBOOK_EVENT, "EventProfilePage::Error parsing http response");
        } else {
            if (postResponse->mError != NULL) {
                Log::error(LOG_FACEBOOK_EVENT, "EventProfilePage::Error getting friend requests, error = %s", postResponse->mError->mMessage);
            } else {
                if (me->mLoadingPost) {
                    evas_object_del(me->mLoadingPost);
                    me->mLoadingPost = NULL;
                }
                void *listData = eina_list_last_data_get(postResponse->GetPostsList());
                if (listData) {
                    Post *post = static_cast<Post*>(listData);
                    ActionAndData *data = new ActionAndData(post, me->mFeedAction, NULL, me->mId );
                    postResponse->RemoveDataFromList(listData);
                    if (post->GetParentId()) {
                        me->mPostWidget = WidgetFactory::CreateAffectedPostBox( me->mDataArea, data, true );
                    } else {
                        me->mPostWidget = WidgetFactory::CreateStatusPost( me->mDataArea, data, true );
                    }
                    me->mViewAllWidget = ProfileWidgetFactory::CreateViewAllPostsItem(me->mDataArea, me->mId);
                    me->DoTinyPadding();
                }
            }
        }
        delete postResponse;
    } else {
        Log::error(LOG_FACEBOOK_EVENT, "EventProfilePage::Error sending http request");
        if (me->mLoadingPost) {
            evas_object_del(me->mLoadingPost);
            me->mLoadingPost = NULL;
        }
    }

    free(response);    //Attention!! client should take care of freeing the response buffer
    me->isAfterEdit = false;
}

void EventProfilePage::Push()
{
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

bool EventProfilePage::HandleBackButton()
{
    bool ret = true;
    if (Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_EVENT_PROFILE_PAGE) {
        ret = false;
        if (PopupMenu::Close()) {
            ret = true;
        }
        if (ProfileWidgetFactory::CloseEventPopup()) {
            ret = true;
        }
        if (mAction->GetUpdatingToast()) {
            ret = true;
        }
    } else {
        ret = true;
    }
    return ret;
}

void EventProfilePage::Update(AppEventId eventId, void * data)
{
    switch(eventId){
    case eOPERATION_ADDED_EVENT:
        if (data) {
            if (!ConnectivityManager::Singleton().IsConnected()) {
                Utils::ShowToast(getMainLayout(), i18n_get_text("IDS_LOGIN_CONNECT_ERR"));
            } else {
                Sptr<Operation> oper(static_cast<Operation*>(data));
                if (oper->GetDestination() == Operation::OD_Event && (oper->GetType() == Operation::OT_Post_Create || oper->GetType() == Operation::OT_Photo_Post_Create || oper->GetType() == Operation::OT_Video_Post_Create || oper->GetType() == Operation::OT_Delete_Post)) {
                    // Didn't have time to implement better logic. Will fix it ASAP
                    if (mPostWidget) {
                        evas_object_del(mPostWidget);
                        mPostWidget = NULL;
                    }
                    if (mViewAllWidget) {
                        evas_object_del(mViewAllWidget);
                        mViewAllWidget = NULL;
                    }
                    if (mLoadingPost) {
                        evas_object_del(mLoadingPost);
                        mLoadingPost = NULL;
                    }
                    mLoadingPost = ProfileWidgetFactory::CreateLoadingItemWrapper(mDataArea);
                }
            }
        }
        break;
    case eOPERATION_COMPLETE_EVENT:
        if (data) {
            Sptr<Operation> oper(static_cast<Operation*>(data));
            if (oper->GetDestination() == Operation::OD_Event) {
                switch (oper->GetType()) {
                case Operation::OT_Delete_Post:
                    Utils::ShowToast(mLayout, i18n_get_text("IDS_POST_DELETED"));
                case Operation::OT_Post_Create:
                case Operation::OT_Photo_Post_Create:
                case Operation::OT_Video_Post_Create:
                    RequestLastPost();
                    break;
                default:
                    break;
                }
            }
        }
        break;
    case eEVENT_PROFILE_UPDATE_EVENT:
        if (data) {
            isAfterEdit = true;
            Event *event = static_cast<Event*>(data);
            ActionAndDataCreate(event);
            SetData(mActionAndData);
        }
        break;
    case eEVENT_PROFILE_UPDATE_STATS_BOX:
        UpdateStatsBox();
        break;
    case eINTERNET_CONNECTION_CHANGED: {
        ProfileWidgetFactory::TransparentWidget(mActionAndData);
        if (!ConnectivityManager::Singleton().IsConnected()) {
//            if (mRerequestBar) {
//                evas_object_del(mRerequestBar);
//                mRerequestBar = NULL;
//            }
            if (!mIsUICreated) {
                ProgressBarHide();
                ShowErrorWidget(ScreenBase::eCONNECTION_ERROR);
            }
        } else {
            RequestData();
        }

    }
        break;
    default:
        break;
    }
}

void EventProfilePage::on_invite_friends_clicked(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    EventProfilePage *me = static_cast<EventProfilePage *> (data);
    me->InviteFriends();
}

void EventProfilePage::InviteFriends()
{
    if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_SELECT_FRIENDS) {
        mSelectFriendsScreen = new SelectFriendsScreen(this, "IDS_EVENT_PROFILE_INVITE_FRIENDS");
        Application::GetInstance()->AddScreen(mSelectFriendsScreen);
        Eina_List *list = GetFriendInfos();
        mSelectFriendsScreen->SetFriendInfos(list);
        list = eina_list_free(list);
    }
}

Eina_List *EventProfilePage::GetFriendInfos()
{
    Eina_List *list = NULL;

    Eina_Array* friends = FriendsProvider::GetInstance()->GetData();
    unsigned int friendsCount = FriendsProvider::GetInstance()->GetDataCount();

    const Eina_List *goingfriends = EventGuestListFactory::GetProvider(EVENT_GOING_TAB)->GetFriendsList();
    const Eina_List *maybefriends = EventGuestListFactory::GetProvider(EVENT_MAYBE_TAB)->GetFriendsList();
    const Eina_List *invitedfriends = EventGuestListFactory::GetProvider(EVENT_INVITED_TAB)->GetFriendsList();

    for (int i = 0; i < friendsCount; ++i) {
        Friend *fr = static_cast<Friend *> (eina_array_data_get(friends, i));
        bool IsInvited = false;
        Eina_List *l;
        void *listData;
        EINA_LIST_FOREACH(const_cast<Eina_List *> (goingfriends), l, listData) {
            User *user = static_cast<User *> (listData);
            if (*fr == *user) {
                IsInvited = true;
                break;
            }
        }

        if (IsInvited) continue;

        EINA_LIST_FOREACH(const_cast<Eina_List *> (maybefriends), l, listData) {
            User *user = static_cast<User *> (listData);
            if (*fr == *user) {
                IsInvited = true;
                break;
            }
        }

        if (IsInvited) continue;

        EINA_LIST_FOREACH(const_cast<Eina_List *> (invitedfriends), l, listData) {
            User *user = static_cast<User *> (listData);
            if (*fr == *user) {
                IsInvited = true;
                break;
            }
        }

        if (!IsInvited) {
            SelectFriendsScreen::FriendInfo *friendInfo = new SelectFriendsScreen::FriendInfo();
            friendInfo->mClientFriend = fr;
            friendInfo->SetFriend(fr);
            friendInfo->mIsSelected = false;
            list = eina_list_append(list, friendInfo);
        }
    }
    return list;
}

void EventProfilePage::FriendsSelectionCompleted(Eina_List *friends, bool isCanceled)
{
    mSelectFriendsScreen->UnregisterClient();
    mSelectFriendsScreen = NULL;
    if (!isCanceled) {
        SendInviteFriendsRequest(friends);
    }
}

void EventProfilePage::SendInviteFriendsRequest(Eina_List *friends)
{
    if (!mRequestInviteFriends) {
        Eina_List *l;
        void *listData;
    //calc size first
        int size = 0;
        EINA_LIST_FOREACH(friends, l, listData) {
            Friend *fr = static_cast<Friend *> (listData);
            size += strlen(fr->GetId()) + 1; // 1 more for ',' symbol
        }
        if (size) { //otherwise we don't have selected friends and don't need to send anything
            ++size; // for 0 terminator
            char *str = new char[size];
            str[0] = '\0';

            EINA_LIST_FOREACH(friends, l, listData) {
                Friend *fr = static_cast<Friend *> (listData);
                char *frStr = new char[strlen(fr->GetId()) + 2]; // 1 more for ',' symbol
                Utils::Snprintf_s(frStr, strlen(fr->GetId()) + 2, "%s,", fr->GetId());
                eina_strlcat(str, frStr, size);
                delete [] frStr;
            }

            str[strlen(str)-1] = '\0'; // replace last ',' with '\0'
            bundle *paramsBundle = bundle_create();
            bundle_add_str(paramsBundle, "users", str);
            delete [] str;

            mRequestInviteFriends = FacebookSession::GetInstance()->PostInviteFriendsToEvent(mId, paramsBundle, invite_friends_request_completed, this);
            bundle_free(paramsBundle);
        }
    }
}

void EventProfilePage::invite_friends_request_completed (void *object, char *response, int code)
{
    Log::info(LOG_FACEBOOK_EVENT, "EventProfilePage::invite_friends_request_completed");
    EventProfilePage *me = static_cast<EventProfilePage *> (object);

    FacebookSession::GetInstance()->ReleaseGraphRequest(me->mRequestInviteFriends);
    me->mRequestInviteFriends = NULL;

    if (code == CURLE_OK) {
        Log::debug(LOG_FACEBOOK_EVENT, "EventProfilePage::response: %s", response);
        FbRespondBase * responseBase = FbRespondBase::createFromJson(response);
        if (!responseBase) {
            Log::error(LOG_FACEBOOK_EVENT, "EventProfilePage::Error parsing http response");
        } else {
            if (responseBase->mError) {
                Log::error(LOG_FACEBOOK_EVENT, "EventProfilePage::Error sending invite friends, error = %s", responseBase->mError);
            } else if (responseBase->mSuccess) {
                if (me->mId) {
                    delete me->m_InvitedUsersDataUploader; me->m_InvitedUsersDataUploader = NULL;
                    EventGuestListFactory::GetProvider(EVENT_INVITED_TAB)->EraseData();
                    EventGuestListFactory::GetProvider(EVENT_INVITED_TAB)->SetEntityId(me->mId);
                    me->m_InvitedUsersDataUploader = new DataUploader(EventGuestListFactory::GetProvider(EVENT_INVITED_TAB), NULL, NULL);
                    me->m_InvitedUsersDataUploader->StartUploading();
                    // TODO paste callback for updating Invited counter here
                    // request summary of invited users or just increment the invited counter
                }
            }
            delete responseBase;
        }
    } else {
        Log::error(LOG_FACEBOOK_EVENT, "EventProfilePage::Error sending http request");
    }

    free(response);    //Attention!! client should take care to free response buffer
}

void EventProfilePage::on_get_suggested_friends_completed(void* data)
{
// AAA Now we do nothing here. This is just needed to speedup suggested friends downloading.
}

void EventProfilePage::on_get_my_avatar_completed(void *object, char *response, int code)
{
    Log::info(LOG_FACEBOOK_EVENT, "EventProfilePage::on_get_my_avatar_completed");

    EventProfilePage *me = static_cast<EventProfilePage*>(object);

    if (me) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(me->mRequestMyAvatar);
        me->mRequestMyAvatar = NULL;

        if (code == CURLE_OK) {
            Log::debug(LOG_FACEBOOK_EVENT, "EventProfilePage::response: %s", response);

            JsonObject *rootObject = NULL;
            ParserWrapper wr(openJsonParser(response, &rootObject));

            if (rootObject) {
                me->mMe = new User(rootObject);

                if (me->mMyPhotoWidget && me->mMe->mPicture) {
                    Evas_Object * avatar = elm_object_part_content_get(me->mMyPhotoWidget, "photo");
                    if (me->mMe->mPicture->mIsSilhouette) {
                        elm_image_file_set(avatar, ICON_DIR "/Events/event_empty_profile_image.png", NULL);
                    } else {
                        me->mActionAndDataForAvatarsDownload->UpdateImageLayoutAsync(me->mMe->mPicture->mUrl, avatar, ActionAndData::EImage);
                    }
                }
            }
        } else {
            Log::error(LOG_FACEBOOK_EVENT, "EventProfilePage::Error sending http request");
        }
    }

    free(response);    //Attention!! client should take care of freeing the response buffer
}

bool EventProfilePage::IsUploadCompleted()
{
    return (mIsEventProfileUploadCompleted
            && m_GoingUsersDataUploader && m_GoingUsersDataUploader->IsUploadCompleted()
            && m_MaybeUsersDataUploader && m_MaybeUsersDataUploader->IsUploadCompleted()
            && m_InvitedUsersDataUploader && m_InvitedUsersDataUploader->IsUploadCompleted()
            && m_DeclinedUsersDataUploader && m_DeclinedUsersDataUploader->IsUploadCompleted());
}

void EventProfilePage::OnErrorWidgetRetryClickedCb()
{
    if (!mMe) {
        mRequestMyAvatar = FacebookSession::GetInstance()->GetMyAvatar(100, 100, on_get_my_avatar_completed, this);
    }
    SetEntityId(mId);
    RequestData();
}

#endif
