/*
 * EventWidgetFactory.cpp
 *
 *  Created on: Aug 10, 2015
 *      Author: ruibezruch
 */
#include <Evas.h>
#include <Elementary.h>
#include "Log.h"

#include "Popup.h"
#include "Application.h"
#include "Common.h"
#include "Config.h"
#include "Event.h"
#include "EventBatchProvider.h"
#include "EventCreatePage.h"
#include "EventAllBirthdays.h"
#include "EventProfilePage.h"
#include "EventMainPage.h"
#include "EventWidgetFactory.h"
#include "Friend.h"
#include "PostComposerScreen.h"
#include "ProfileWidgetFactory.h"
#include "ScreenBase.h"
#include "WidgetFactory.h"
#include "UIRes.h"
#include "ProfileScreen.h"
#include "Utils.h"
#include "ConnectivityManager.h"

#ifdef EVENT_NATIVE_VIEW

static Evas_Object *delete_event_popup = NULL;
UIRes * EventWidgetFactory::R = UIRes::GetInstance();

void EventWidgetFactory::CreateSelectedEventItem(Evas_Object *parent, ActionAndData *action_data)
{
    Event *event = static_cast<Event*>(action_data->mData);

    Evas_Object *event_item = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(parent, "event_main_page_content_item");

    Evas_Object *avatar = elm_image_add(event_item);
    elm_object_part_content_set(event_item, "item_avatar", avatar);
    elm_image_file_set(avatar, ICON_DIR"/Events/event_empty_profile_image.png", NULL);
    evas_object_show(avatar);
    if (event->mCover && event->mCover->mSource) {
        action_data->UpdateImageLayoutAsync(event->mCover->mSource, avatar, ActionAndData::EImage, Post::EFromPicturePath);
    } else {
         if (event->mOwner->GetGraphObjectType() == GraphObject::GOT_USER) {
             User * owner = static_cast<User *>(event->mOwner);
             if (!owner->mPicture->mIsSilhouette) {
                 action_data->UpdateImageLayoutAsync(owner->mPicture->mUrl, avatar, ActionAndData::EImage, Post::EFromPicturePath);
             }
         }
    }

    action_data->mActionObj = event_item;

    Evas_Object *info_box = elm_box_add(event_item);
    elm_object_part_content_set(event_item, "item_info", info_box);
    evas_object_size_hint_weight_set(info_box, 1, 1);
    evas_object_size_hint_align_set(info_box, 0, -1);
    evas_object_show(info_box);

    Evas_Object *spacer1 = elm_box_add(info_box);
    evas_object_size_hint_weight_set(spacer1, 20, 20);
    evas_object_size_hint_align_set(spacer1, -1, -1);
    elm_box_pack_end(info_box, spacer1);
    evas_object_show(spacer1);

    if (event->mName) {
        char *event_title_text = NULL;
        Evas_Object *event_title = elm_label_add(info_box);
        evas_object_size_hint_weight_set(event_title, 1, 1);
        evas_object_size_hint_align_set(event_title, -1, -1);
        elm_label_wrap_width_set(event_title, R->EVENT_NEAREST_ITEM_WRAP_WIDTH);
        elm_label_ellipsis_set(event_title, EINA_TRUE);
        elm_label_line_wrap_set(event_title, ELM_WRAP_MIXED);
        event_title_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_4e5665_FORMAT, R->EVENT_NEAREST_TITLE_TEXT_SIZE, event->mName);
        if (event_title_text) {
            elm_object_text_set(event_title, event_title_text);
            delete[] event_title_text;
        }
        elm_box_pack_end(info_box, event_title);
        evas_object_show(event_title);

        action_data->mEventNameLabel = event_title;
    }

    if (event->mStartTime) {
        char *event_date_text = NULL;
        Evas_Object *event_date = elm_label_add(info_box);
        evas_object_size_hint_weight_set(event_date, 1, 1);
        evas_object_size_hint_align_set(event_date, -1, -1);
        elm_label_wrap_width_set(event_date, R->EVENT_NEAREST_ITEM_WRAP_WIDTH);
        elm_label_ellipsis_set(event_date, EINA_TRUE);
        elm_label_line_wrap_set(event_date, ELM_WRAP_MIXED);

        char * eventInfo = Utils::GetNearestEventInfo(event->mStartTimeString, false);

        event_date_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_9298A4_FORMAT, R->EVENT_NEAREST_SUBCONTENT_TEXT_SIZE, eventInfo);
        if (event_date_text) {
            elm_object_text_set(event_date, event_date_text);
            delete[] event_date_text;
        }

        if (eventInfo) {
            free((void*)eventInfo);
            eventInfo = NULL;
        }

        elm_box_pack_end(info_box, event_date);
        evas_object_show(event_date);

        action_data->mEventStartTimeLabel = event_date;
    }

    if (event->mPlace) {
        char *event_place_text = NULL;
        Evas_Object *event_place = elm_label_add(info_box);
        evas_object_size_hint_weight_set(event_place, 1, 1);
        evas_object_size_hint_align_set(event_place, -1, -1);
        elm_label_wrap_width_set(event_place, R->EVENT_NEAREST_ITEM_WRAP_WIDTH);
        elm_label_ellipsis_set(event_place, EINA_TRUE);
        elm_label_line_wrap_set(event_place, ELM_WRAP_MIXED);
        if (event->mPlace->mName) {
            event_place_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_9298A4_FORMAT, R->EVENT_NEAREST_SUBCONTENT_TEXT_SIZE, event->mPlace->mName);
        } else if (event->mPlace->mLocation->mCity) {
            event_place_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_9298A4_FORMAT, R->EVENT_NEAREST_SUBCONTENT_TEXT_SIZE, event->mPlace->mLocation->mCity);
        } else if (event->mPlace->mLocation->mCountry) {
            event_place_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_9298A4_FORMAT, R->EVENT_NEAREST_SUBCONTENT_TEXT_SIZE, event->mPlace->mLocation->mCountry);
        } else {
            event_place_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_9298A4_FORMAT, R->EVENT_NEAREST_SUBCONTENT_TEXT_SIZE, "Error");
        }
        if (event_place_text) {
            elm_object_text_set(event_place, event_place_text);
            delete[] event_place_text;
        }
        elm_box_pack_end(info_box, event_place);
        evas_object_show(event_place);

        action_data->mEventPlaceLabel = event_place;
    }

    if (event->mRsvpStatus != Event::EVENT_RSVP_NOT_REPLIED || !strcmp(event->mOwner->GetId(), Config::GetInstance().GetUserId())) {
        char *event_status_text = NULL;
        Evas_Object *event_status = elm_label_add(info_box);
        evas_object_size_hint_weight_set(event_status, 1, 1);
        evas_object_size_hint_align_set(event_status, -1, -1);
        elm_label_wrap_width_set(event_status, R->EVENT_NEAREST_ITEM_WRAP_WIDTH);
        elm_label_ellipsis_set(event_status, EINA_TRUE);
        elm_label_line_wrap_set(event_status, ELM_WRAP_MIXED);

        char * eventStatus = NULL;

        if (!strcmp(event->mOwner->GetId(), Config::GetInstance().GetUserId())) {
            eventStatus = SAFE_STRDUP(i18n_get_text("IDS_EVENT_TYPE_HOSTED"));
            // NO AVALIABLE ACTIONS FOR NOW
            elm_object_signal_emit(action_data->mActionObj, "hosted.set", "btn");
            elm_object_signal_callback_add(action_data->mActionObj, "mouse,clicked,*", "btn", on_manage_event_clicked, action_data);
        } else {
            if (event->mRsvpStatus == Event::EVENT_RSVP_MAYBE) {
                eventStatus = SAFE_STRDUP(i18n_get_text("IDS_EVENT_TYPE_MIGHT_GO"));
                elm_object_signal_emit(action_data->mActionObj, "maybe.set", "btn");
            } else if (event->mRsvpStatus == Event::EVENT_RSVP_ATTENDING) {
                eventStatus = SAFE_STRDUP(i18n_get_text("IDS_EVENT_TYPE_GOING"));
                elm_object_signal_emit(action_data->mActionObj, "going.set", "btn");
            } else if (event->mRsvpStatus == Event::EVENT_RSVP_DECLINED) {
                eventStatus = SAFE_STRDUP("You are not going");
                elm_object_signal_emit(action_data->mActionObj, "notgoing.set", "btn");
            }

            elm_object_signal_callback_add(action_data->mActionObj, "mouse,clicked,*", "btn", on_event_status_popup_clicked, action_data);
        }

        event_status_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_9298A4_FORMAT, R->EVENT_NEAREST_SUBCONTENT_TEXT_SIZE, eventStatus);
        if (event_status_text) {
            elm_object_text_set(event_status, event_status_text);
            delete[] event_status_text;
        }
        elm_box_pack_end(info_box, event_status);
        evas_object_show(event_status);

        action_data->mChangeableLabel = event_status;

        free((void*) eventStatus); eventStatus = NULL;
    } else {
        elm_object_signal_emit(action_data->mActionObj, "hide.set", "btn");
    }

    if (EventBatchProvider::GetInstance()->GetSelectorId() && !strcmp(EventBatchProvider::GetInstance()->GetSelectorId(), EventBatchProvider::SELECTOR_PAST)) {
        elm_object_signal_emit(action_data->mActionObj, "hide.set", "btn");
    }

    Evas_Object *spacer2 = elm_box_add(info_box);
    evas_object_size_hint_weight_set(spacer2, 20, 20);
    evas_object_size_hint_align_set(spacer2, -1, -1);
    elm_box_pack_end(info_box, spacer2);
    evas_object_show(spacer2);

    elm_object_signal_callback_add(event_item, "mouse,clicked,*", "item_*", on_selected_item_clicked, action_data);

    if (EventBatchProvider::GetInstance()->GetSelectorId() && !strcmp(EventBatchProvider::GetInstance()->GetSelectorId(), EventBatchProvider::SELECTOR_INVITES)) {
        event->isInvited = true;
        action_data->mEventInviteBtnsLayout = CreateInvitedBtns(parent, action_data);
        elm_object_signal_emit(action_data->mActionObj, "hide.set", "btn");
    }
}

void EventWidgetFactory::RefreshEventInfoBox(ActionAndData *action_data)
{
    Log::info(LOG_FACEBOOK_EVENT, "EventWidgetFactory::RefreshEventInfoBox");
    Event *event = static_cast<Event*>(action_data->mData);

    if (action_data->mEventNameLabel && event->mName) {
        char *event_title_text = NULL;
        event_title_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_4e5665_FORMAT, R->EVENT_NEAREST_TITLE_TEXT_SIZE, event->mName);
        if (event_title_text) {
            elm_object_text_set(action_data->mEventNameLabel, event_title_text);
            delete[] event_title_text;
        }
    }

    if (action_data->mEventPlaceLabel) {
        char *event_place_text = NULL;
        if (event->mPlace && event->mPlace->mName) {
            event_place_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_9298A4_FORMAT, R->EVENT_NEAREST_SUBCONTENT_TEXT_SIZE, event->mPlace->mName);
        } else if (event->mPlace && event->mPlace->mLocation->mCity) {
            event_place_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_9298A4_FORMAT, R->EVENT_NEAREST_SUBCONTENT_TEXT_SIZE, event->mPlace->mLocation->mCity);
        } else if (event->mPlace && event->mPlace->mLocation->mCountry) {
            event_place_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_9298A4_FORMAT, R->EVENT_NEAREST_SUBCONTENT_TEXT_SIZE, event->mPlace->mLocation->mCountry);
        } else {
            event_place_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_9298A4_FORMAT, R->EVENT_NEAREST_SUBCONTENT_TEXT_SIZE, "Error");
        }
        if (event_place_text) {
            elm_object_text_set(action_data->mEventPlaceLabel, event_place_text);
            delete[] event_place_text;
        }
    }

    if (action_data->mEventStartTimeLabel && event->mStartTime) {
        char *event_date_text = NULL;
        char * eventInfo = Utils::GetNearestEventInfo(event->mStartTimeString, false);
        event_date_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_9298A4_FORMAT, R->EVENT_NEAREST_SUBCONTENT_TEXT_SIZE, eventInfo);
        if (event_date_text) {
            elm_object_text_set(action_data->mEventStartTimeLabel, event_date_text);
            delete[] event_date_text;
        }
        if (eventInfo) {
            free((void*)eventInfo);
            eventInfo = NULL;
        }
    }

    char * eventStatus = NULL;

    if (event->mOwner && event->mOwner->GetId() && !strcmp(event->mOwner->GetId(), Config::GetInstance().GetUserId())) {
        eventStatus = SAFE_STRDUP(i18n_get_text("IDS_EVENT_TYPE_HOSTED"));
        // NO AVALIABLE ACTIONS FOR NOW
        elm_object_signal_emit(action_data->mActionObj, "hosted.set", "btn");
    } else {
        if (event->mRsvpStatus == Event::EVENT_RSVP_MAYBE) {
            eventStatus = SAFE_STRDUP("You might go");
            elm_object_signal_emit(action_data->mActionObj, "maybe.set", "btn");
        } else if (event->mRsvpStatus == Event::EVENT_RSVP_ATTENDING) {
            eventStatus = SAFE_STRDUP(i18n_get_text("IDS_EVENT_TYPE_GOING"));
            elm_object_signal_emit(action_data->mActionObj, "going.set", "btn");
        } else if (event->mRsvpStatus == Event::EVENT_RSVP_DECLINED) {
            eventStatus = SAFE_STRDUP("You are not going");
            elm_object_signal_emit(action_data->mActionObj, "notgoing.set", "btn");
        }
    }

    char *event_status_text = NULL;
    event_status_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_9298A4_FORMAT, R->EVENT_NEAREST_SUBCONTENT_TEXT_SIZE, eventStatus);
    if (event_status_text) {
        elm_object_text_set(action_data->mChangeableLabel, event_status_text);
        delete[] event_status_text;
    }

    if (eventStatus) {
        free((void*) eventStatus); eventStatus = NULL;
    }

    if (!strcmp(EventBatchProvider::GetInstance()->GetSelectorId(), EventBatchProvider::SELECTOR_PAST)) {
        elm_object_signal_emit(action_data->mActionObj, "hide.set", "btn");
    }
}

Evas_Object *EventWidgetFactory::CreateInvitedBtns(Evas_Object *parent, ActionAndData *actionData)
{
    Evas_Object *item = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(parent, "event_invite_list_btns");

    elm_object_translatable_part_text_set(item, "btn1_text", "IDS_EVENT_PROFILE_GOING");
    elm_object_translatable_part_text_set(item, "btn2_text", "IDS_EVENT_PROFILE_MAYBE");
    elm_object_translatable_part_text_set(item, "btn3_text", "IDS_EVENT_PROFILE_DECLINE");

    elm_object_signal_callback_add(item, "mouse,clicked,*", "btn1*", ActionBase::elm_on_event_going_btn_clicked_cb, actionData);
    elm_object_signal_callback_add(item, "mouse,clicked,*", "btn2*", ActionBase::elm_on_event_maybe_btn_clicked_cb, actionData);
    elm_object_signal_callback_add(item, "mouse,clicked,*", "btn3*", ActionBase::elm_on_event_not_going_btn_clicked_cb, actionData);

    return item;
}

Evas_Object *EventWidgetFactory::CreateBdayItem(Evas_Object *parent, ActionAndData *action_data)
{
    Friend *bdEvent = static_cast<Friend*>(action_data->mData);

    Evas_Object *b_day_item = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(parent, "event_main_page_content_bday_item");

    if (bdEvent->GetPicturePath()) {
        Evas_Object *item_avatar = elm_image_add(b_day_item);
        elm_layout_content_set(b_day_item, "item_avatar", item_avatar);
        action_data->UpdateImageLayoutAsync(bdEvent->GetPicturePath(), item_avatar, ActionAndData::EImage, Post::EFromPicturePath);
        evas_object_show(item_avatar);
    } else {
        Evas_Object *item_avatar = elm_image_add(b_day_item);
        elm_layout_content_set(b_day_item, "item_avatar", item_avatar);
        elm_image_file_set(item_avatar, ICON_DIR"/Events/event_empty_profile_image.png", NULL);
        evas_object_show(item_avatar);
    }

    Evas_Object *info_box = elm_box_add(b_day_item);
    elm_object_part_content_set(b_day_item, "item_info", info_box);
    evas_object_size_hint_weight_set(info_box, 1, 1);
    evas_object_size_hint_align_set(info_box, 0, -1);
    evas_object_show(info_box);

    Evas_Object *spacer1 = elm_box_add(info_box);
    evas_object_size_hint_weight_set(spacer1, 10, 10);
    evas_object_size_hint_align_set(spacer1, -1, -1);
    elm_box_pack_end(info_box, spacer1);
    evas_object_show(spacer1);

    char *user_name_text = NULL;
    Evas_Object *user_name = elm_label_add(info_box);
    evas_object_size_hint_weight_set(user_name, 1, 1);
    evas_object_size_hint_align_set(user_name, -1, -1);
    elm_label_wrap_width_set(user_name, R->EVENT_BDAY_ITEM_WRAP_WIDTH);
    elm_label_ellipsis_set(user_name, EINA_TRUE);
    elm_label_line_wrap_set(user_name, ELM_WRAP_MIXED);

    user_name_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_4e5665_FORMAT,
                                R->EVENT_BDAY_ITEM_NAME_TEXT_SIZE, bdEvent->mName);
    if (user_name_text) {
        elm_object_text_set(user_name, user_name_text);
        delete[] user_name_text;
    }
    elm_box_pack_end(info_box, user_name);
    evas_object_show(user_name);

    char *user_bday_text = NULL;
    Evas_Object *user_bday = elm_label_add(info_box);
    evas_object_size_hint_weight_set(user_bday, 1, 1);
    evas_object_size_hint_align_set(user_bday, -1, -1);
    elm_label_wrap_width_set(user_bday, R->EVENT_BDAY_ITEM_WRAP_WIDTH);
    elm_label_ellipsis_set(user_bday, EINA_TRUE);
    elm_label_line_wrap_set(user_bday, ELM_WRAP_MIXED);

    char * birthdayInfo = Utils::GetNearestBdayInfo(bdEvent->mBirthday, bdEvent->isBdayNextYear);

    user_bday_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_9298A4_FORMAT,
                                 R->EVENT_BDAY_ITEM_SUBCONTENT_TEXT_SIZE, birthdayInfo);
    if (user_bday_text) {
        elm_object_text_set(user_bday, user_bday_text);
        delete[] user_bday_text;
    }

    if (birthdayInfo) {
        free((void *) birthdayInfo);
        birthdayInfo = NULL;
    }

    elm_box_pack_end(info_box, user_bday);
    evas_object_show(user_bday);

    if (bdEvent->mTurningInfo != NULL) {
        char *user_age_text = NULL;
        Evas_Object *user_age = elm_label_add(info_box);
        evas_object_size_hint_weight_set(user_age, 1, 1);
        evas_object_size_hint_align_set(user_age, -1, -1);
        elm_label_wrap_width_set(user_age, R->EVENT_BDAY_ITEM_WRAP_WIDTH);
        elm_label_ellipsis_set(user_age, EINA_TRUE);
        elm_label_line_wrap_set(user_age, ELM_WRAP_MIXED);

        user_age_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_9298A4_FORMAT, R->EVENT_BDAY_ITEM_SUBCONTENT_TEXT_SIZE, bdEvent->mTurningInfo);
        if (user_age_text) {
            elm_object_text_set(user_age, user_age_text);
            delete[] user_age_text;
        }
        elm_box_pack_end(info_box, user_age);
        evas_object_show(user_age);
    }

    Evas_Object *spacer2 = elm_box_add(info_box);
    evas_object_size_hint_weight_set(spacer2, 10, 10);
    evas_object_size_hint_align_set(spacer2, -1, -1);
    elm_box_pack_end(info_box, spacer2);
    evas_object_show(spacer2);

    //Hiding "Post" button. API question https://github.com/fbmp/fb4t/issues/2252
    elm_object_signal_emit(b_day_item, "hide.bday", "btn");

    elm_object_signal_callback_add(b_day_item, "mouse,clicked,*", "btn", on_bday_btn_clicked, action_data);
    elm_object_signal_callback_add(b_day_item, "mouse,clicked,*", "item_*", on_selected_birthday_item_clicked, action_data);

    return b_day_item;
}

void EventWidgetFactory::on_selected_birthday_item_clicked(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_USER_PROFILE) {
        ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
        if(action_data && action_data->mData) {
            Friend *birthday = static_cast<Friend*>(action_data->mData);
            ProfileScreen *screen = new ProfileScreen(birthday->GetId());
            Application::GetInstance()->AddScreen(screen);
        }
    }
}

void EventWidgetFactory::CreateViewAllItem(Evas_Object *parent)
{
    Evas_Object *view_all = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(parent, "event_main_page_content_view_all");

    elm_object_translatable_part_text_set(view_all, "item_text", "IDS_VIEW_ALL");

    elm_object_signal_callback_add(view_all, "mouse,clicked,*", "item_*", on_view_all_btn_clicked, NULL);
}

void EventWidgetFactory::CreateEventJoinStatusPopup(ActionAndData *actionData, Evas_Object *obj)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "EventWidgetFactory::CreateEventJoinStatusPopup");

    static PopupMenu::PopupMenuItem context_menu_items[3] =
    {
            {
                    ICON_DIR"/Events/event_going.png",
                    "IDS_EVENT_PROFILE_GOING",
                    NULL,
                    ActionBase::on_event_going_btn_clicked_cb,
                    NULL,
                    0,
                    false,
                    false
            },
            {
                    ICON_DIR"/Events/event_maybe.png",
                    "IDS_EVENT_PROFILE_MAYBE",
                    NULL,
                    ActionBase::on_event_maybe_btn_clicked_cb,
                    NULL,
                    1,
                    false,
                    false
            },
            {
                    ICON_DIR"/Events/event_not_going.png",
                    "IDS_EVENT_PROFILE_NOT_GOING"),
                    NULL,
                    ActionBase::on_event_not_going_btn_clicked_cb,
                    NULL,
                    2,
                    true,
                    false
            },
    };
    context_menu_items[0].data = actionData;
    context_menu_items[1].data = actionData;
    context_menu_items[2].data = actionData;

    Event *event = static_cast<Event*>(actionData->mData);
    if (event->mRsvpStatus == Event::EVENT_RSVP_ATTENDING) {
        context_menu_items[0].itemIcon = ICON_DIR"/Events/event_going_active.png";
        context_menu_items[1].itemIcon = ICON_DIR"/Events/event_maybe.png";
        context_menu_items[2].itemIcon = ICON_DIR"/Events/event_not_going.png";
    } else if (event->mRsvpStatus == Event::EVENT_RSVP_MAYBE) {
        context_menu_items[0].itemIcon = ICON_DIR"/Events/event_going.png";
        context_menu_items[1].itemIcon = ICON_DIR"/Events/event_maybe_active.png";
        context_menu_items[2].itemIcon = ICON_DIR"/Events/event_not_going.png";
    } else {
        context_menu_items[0].itemIcon = ICON_DIR"/Events/event_going.png";
        context_menu_items[1].itemIcon = ICON_DIR"/Events/event_maybe.png";
        context_menu_items[2].itemIcon = ICON_DIR"/Events/event_not_going_active.png";
    }

    actionData->mActionObj = obj;

    Evas_Coord y = 0, h = 0;

    evas_object_geometry_get(obj, NULL, &y, NULL, &h);
    y += h/2;

    PopupMenu::Show(3, context_menu_items, actionData->mAction->getScreen()->getParentMainLayout(), false, y);
}

void EventWidgetFactory::on_manage_event_clicked(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::info(LOG_FACEBOOK_EVENT, "EventWidgetFactory::on_manage_clicked");

    ActionAndData *actionData = static_cast<ActionAndData*>(user_data);

    if (actionData) {
        static PopupMenu::PopupMenuItem context_menu_items[3] =
        {
                { ICON_EVENT_DIR "/event_filter_invites.png", "IDS_INVITE", NULL, on_invite_friends_btn_clicked, NULL, 0, false },
                { ICON_DIR "/edit.png", "IDS_EDIT", NULL, on_edit_event_btn_clicked, NULL, 1, false },
                { ICON_DIR "/delete.png", "IDS_DELETE", NULL, on_delete_event_btn_clicked, NULL, 2, true },
        };
        context_menu_items[0].data = actionData;
        context_menu_items[1].data = actionData;
        context_menu_items[2].data = actionData;

        Evas_Coord y = 0, h = 0;

        evas_object_geometry_get(obj, NULL, &y, NULL, &h);
        y += h/2;

        PopupMenu::Show(3, context_menu_items, actionData->mAction->getScreen()->getParentMainLayout(), false, y);
    }
}

void EventWidgetFactory::on_event_status_popup_clicked(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::info(LOG_FACEBOOK_EVENT, "EventWidgetFactory::on_event_status_popup_clicked");
    ActionAndData *actionData = static_cast<ActionAndData*>(user_data);

    if (actionData) {
        CreateEventJoinStatusPopup(actionData, obj);
    }
}

void EventWidgetFactory::on_bday_btn_clicked(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::info(LOG_FACEBOOK_EVENT, "EventWidgetFactory::on_bday_clicked");
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    Friend *friends = static_cast<Friend*>(action_data->mData);

    PostComposerScreen *screen = new PostComposerScreen(NULL, eFRIENDTIMELINE, friends->GetId(), friends->mName, NULL);
    Application::GetInstance()->AddScreen(screen);
}

void EventWidgetFactory::on_selected_item_clicked(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    Event *event = static_cast<Event*>(action_data->mData);
    if (event && event->GetId() && event->mName) {
        EventProfilePage *event_profile_screen = new EventProfilePage(event->GetId(), event->mName, event);
        Application::GetInstance()->AddScreen(event_profile_screen);
    }
}

void EventWidgetFactory::on_view_all_btn_clicked(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    EventAllBirthdays *event_all_birthdays = new EventAllBirthdays();
    Application::GetInstance()->AddScreen(event_all_birthdays);
}

void EventWidgetFactory::on_edit_event_btn_clicked(void *user_data, Evas_Object *obj, void *event_info)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    if (action_data) {
        Event *event = static_cast<Event*>(action_data->mData);
        if (event) {
            EventCreatePage *event_edit = new EventCreatePage(event, false);
            Application::GetInstance()->AddScreen(event_edit);
        }
    }
}

void EventWidgetFactory::on_delete_event_btn_clicked(void *user_data, Evas_Object *obj, void *event_info)
{
    Log::info(LOG_FACEBOOK_EVENT, "EventWidgetFactory::on_delete_event_btn_clicked");

    ActionAndData *action_data = static_cast<ActionAndData *>(user_data);

    delete_event_popup = elm_popup_add(action_data->mAction->getScreen()->getMainLayout());
    elm_popup_orient_set(delete_event_popup, ELM_POPUP_ORIENT_CENTER );
    evas_object_smart_callback_add(delete_event_popup, "block,clicked", popup_cancel_delete_cb, NULL);
    Evas_Object *keepDiscard = elm_layout_add(delete_event_popup);
    elm_layout_file_set(keepDiscard, Application::mEdjPath, "post_composer_keep_discard_popup");
    elm_object_content_set(delete_event_popup, keepDiscard);

    elm_object_translatable_part_text_set(keepDiscard, "top_text", "IDS_ASK_DELETE_EVENT");
    elm_object_translatable_part_text_set(keepDiscard, "description", "IDS_DELETE_EVENT_DESCRIPTION");
    elm_object_translatable_part_text_set(keepDiscard, "text.keep", "IDS_NO");
    elm_object_translatable_part_text_set(keepDiscard, "text.discard", "IDS_YES");

    elm_object_signal_callback_add(keepDiscard, "mouse,clicked,*", "keep", popup_signal_cancel_delete_cb, NULL);
    elm_object_signal_callback_add(keepDiscard, "mouse,clicked,*", "discard", popup_approved_delete_cb, action_data);

    evas_object_show(keepDiscard);
    evas_object_show(delete_event_popup);
}

void EventWidgetFactory::popup_approved_delete_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    CloseEventPopup();

    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    action_data->mAction->OnCtxMenuApprovedDeleteEventClicked(user_data);
}

void EventWidgetFactory::popup_cancel_delete_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    CloseEventPopup();
}

void EventWidgetFactory::popup_signal_cancel_delete_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    CloseEventPopup();
}

bool EventWidgetFactory::CloseEventPopup()
{
    bool result = false;

    if(delete_event_popup){
        evas_object_del(delete_event_popup);
        delete_event_popup = NULL;
        result = true;
    }
    return result;
}

void EventWidgetFactory::on_invite_friends_btn_clicked(void *user_data, Evas_Object *obj, void *event_info)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);

    if (action_data) {
        EventMainPage *screen = static_cast<EventMainPage *> (action_data->mAction->getScreen());
        Event *eventData = static_cast<Event *>(action_data->mData);
        screen->InviteFriends(eventData->GetId());
    }
}

#endif

