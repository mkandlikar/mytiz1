   /*
 * EventGuestTab.cpp
 *
 *  Created on: Nov 2, 2015
 *      Author: ruibezruch
 */

#include "EventGuestListFactory.h"
#include "EventGuestTab.h"
#include "Separator.h"
#include "ProfileScreen.h"
#include "Utils.h"
#include "WidgetFactory.h"

#ifdef EVENT_NATIVE_VIEW

UIRes * EventGuestTab::R = UIRes::GetInstance();

EventGuestTab::EventGuestTab(ScreenBase *parentScreen, EventGuestListTabs tab) : ScreenBase(parentScreen), TrackItemsProxyAdapter(getParentMainLayout(), EventGuestListFactory::GetProvider(tab))
{
    mAction = new EventActions(this);

    if (tab == EVENT_GOING_TAB) {
        SetIdentifier("EVENT_GOING_TAB");
    } else if (tab == EVENT_INVITED_TAB) {
        SetIdentifier("EVENT_INVITED_TAB");
    } else if (tab == EVENT_MAYBE_TAB) {
        SetIdentifier("EVENT_MAYBE_TAB");
    }

    mTab = tab;

    mLayout = WidgetFactory::CreateLayoutByGroup(getParentMainLayout(), "event_guesttab");

    if (tab == EVENT_GOING_TAB) {
        elm_object_translatable_part_text_set(mLayout, "nodata_view", "IDS_EVENT_GUEST_TAB_GOING_NONE");
    } else if (tab == EVENT_INVITED_TAB) {
        elm_object_translatable_part_text_set(mLayout, "nodata_view", "IDS_EVENT_GUEST_TAB_INVITED_NONE");
    } else if (tab == EVENT_MAYBE_TAB) {
        elm_object_translatable_part_text_set(mLayout, "nodata_view", "IDS_EVENT_GUEST_TAB_MAYBE_NONE");
    }

    ApplyScroller(mLayout, true);
    elm_object_part_content_set(mLayout, "layout", GetScroller());

    TrackItemsProxy::ProxySettings *settings = GetProxySettings();
    settings->wndSize = 50;
    settings->initialWndSize = 11;
    SetProxySettings( settings );

    AppEvents::GetInstance()->Subscribe(eEVENT_FRIENDSHIP_STATUS_CHANGED, this);

    RequestData();
}

EventGuestTab::~EventGuestTab()
{
    AppEvents::GetInstance()->Unsubscribe(eEVENT_FRIENDSHIP_STATUS_CHANGED, this);
    delete mAction;
    EraseWindowData();
}

void* EventGuestTab::CreateItem(void *dataForItem, bool isAddToEnd)
{
    GraphObject * graphObject = static_cast<GraphObject*>(dataForItem);
    ActionAndData *data = NULL;

    if (graphObject->GetGraphObjectType() == GraphObject::GOT_SEPARATOR) {
        Separator *separator = static_cast<Separator*>(graphObject);
        switch (separator->GetSeparatorType()) {
        case Separator::FRIENDS_SEPARATOR:
        case Separator::OTHERS_SEPARATOR:
            data = new ActionAndData(graphObject, mAction);
            data->mParentWidget = WidgetFactory::CreateSeparatorItem(GetDataArea(), isAddToEnd, separator->mCustomText);
            break;
        default:
            break;
        }
    } else {
        User * user = static_cast<User*>(dataForItem);
        if (!Utils::IsMe(user->GetId())) {
            data = new ActionAndData(graphObject, mAction);
            data->mParentWidget = GuestsGet(data, GetDataArea(), isAddToEnd);
        }
    }

    return data;
}

Evas_Object *EventGuestTab::GuestsGet(void *user_data, Evas_Object *obj, bool isAddToEnd)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    EventGuestTab *me = static_cast<EventGuestTab*>(action_data->mAction->getScreen());

    Evas_Object *content = me->CreateGuestItem(obj, action_data);
    if (isAddToEnd) {
        elm_box_pack_end(obj, content);
    } else {
        elm_box_pack_start(obj, content);
    }

    return content;
}

Evas_Object * EventGuestTab::CreateGuestItem(Evas_Object* parent, ActionAndData *action_data)
{
    User *user = static_cast<User*>(action_data->mData);

    Evas_Object *list_item = WidgetFactory::CreateLayoutByGroup(parent, "event_guestlist_item");

    Evas_Object *avatar = elm_image_add(list_item);
    elm_object_part_content_set(list_item, "item_avatar", avatar);
    action_data->UpdateImageLayoutAsync(user->mPicture->mUrl, avatar);
    evas_object_show(avatar);

    elm_object_part_text_set(list_item, "item_name", user->mName);
    elm_object_signal_callback_add(list_item, "mouse,clicked,*", "item_*", on_item_clicked_cb, action_data);
    elm_object_signal_callback_add(list_item, "mouse,clicked,*", "btn_msg", ActionBase::run_messanger_cb, action_data);
    elm_object_signal_callback_add(list_item, "mouse,clicked,*", "add_btn", ActionBase::on_add_friend_btn_clicked_cb, action_data);

    SetGuestFriendshipStatus(list_item, user->mFriendshipStatus);

    return list_item;
}

void EventGuestTab::SetGuestFriendshipStatus(Evas_Object * layout, User::FriendshipStatus status)
{
    switch (status) {
    case User::eNOT_AVAILABLE:
    case User::eCANNOT_REQUEST: {
        elm_object_signal_emit(layout, "add_and_cancel.btn", "hide");
        elm_object_signal_emit(layout, "message.btn", "hide");
    }
        break;
    case User::eARE_FRIENDS: {
        elm_object_signal_emit(layout, "add_and_cancel.btn", "hide");
    }
        break;
    case User::eCAN_REQUEST: {
        elm_object_signal_emit(layout, Application::IsRTLLanguage() ? "add.btn.rtl" : "add.btn.ltr", "show");
        elm_object_signal_emit(layout, "message.btn", "hide");
    }
        break;
    case User::eOUTGOING_REQUEST: {
        elm_object_signal_emit(layout, Application::IsRTLLanguage() ? "cancel.btn.rtl" : "cancel.btn.ltr", "show");
        elm_object_signal_emit(layout, "message.btn", "hide");
    }
        break;
    default:
        break;
    }
}

void EventGuestTab::SetDataAvailable( bool isAvailable )
{
    if ((EventGuestListFactory::GetProvider(mTab)->GetDataCount()) == 1 && EventGuestListFactory::GetProvider(mTab)->isLoggedInUserHere) {
        SetNoDataViewVisibility(true);
    } else {
        SetNoDataViewVisibility(!isAvailable);
    }
}

void EventGuestTab::SetNoDataViewVisibility(bool isVisible)
{
    if (isVisible) {
        elm_object_signal_emit(mLayout, "nodata_view_visibility,1", "");
    } else {
        elm_object_signal_emit(mLayout, "nodata_view_visibility,0", "");
    }
}

void EventGuestTab::on_item_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    if (Application::GetInstance()->GetTopScreenId() != SID_USER_PROFILE) {
        ActionAndData *data = static_cast<ActionAndData*>(user_data);
        if(data && data->mData) {
            User *item = static_cast<User*>(data->mData);
            ScreenBase *scr = new ProfileScreen(item->GetId());
            if(scr) {
                Application::GetInstance()->AddScreen(scr);
            }
        }
    }
}

void EventGuestTab::Update(AppEventId eventId, void *data)
{
    switch (eventId) {
    case eEVENT_FRIENDSHIP_STATUS_CHANGED:
        if (data) {
            void * item = GetItemById(static_cast<const char *>(data));
            if (item) {
                ActionAndData * actionAndData = static_cast<ActionAndData *>(item);
                if (actionAndData->mParentWidget && actionAndData->mData) {
                    SetGuestFriendshipStatus(actionAndData->mParentWidget, static_cast<User *>(actionAndData->mData)->mFriendshipStatus);
                }
            }
        }
        break;
    default:
        TrackItemsProxy::Update(eventId, data);
        break;
    }
}

#endif
