/*
 * EventAllBirthdays.cpp
 *
 */

#include "EventAllBirthdays.h"

#ifdef EVENT_NATIVE_VIEW

#include "EventWidgetFactory.h"
#include "Friend.h"
#include "FriendBirthDayProvider.h"
#include "WidgetFactory.h"

EventAllBirthdays::EventAllBirthdays(): ScreenBase(NULL), TrackItemsProxyAdapter(NULL, FriendBirthDayProvider::GetInstance())
{
    mScreenId = SID_ALLBDAYS_SCREEN;
    mLayout = WidgetFactory::CreateBaseScreenLayout(this, getParentMainLayout(), i18n_get_text("IDS_EVENTS_BIRTHDAYS"), true, true, true);

    ApplyScroller(mLayout, true);
    elm_object_part_content_set(mLayout, "content", GetScroller());

    RequestData();
}

void EventAllBirthdays::Push()
{
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void* EventAllBirthdays::CreateItem( void* dataForItem, bool isAddToEnd )
{
    ActionAndData *data = NULL;
    Friend *friends = static_cast<Friend*>(dataForItem);

    if (friends) {
        data = new ActionAndData(friends, NULL);
    }
    data->mParentWidget = BdaysGet(data, GetDataArea(), isAddToEnd);

    return data;
}

Evas_Object *EventAllBirthdays::BdaysGet(void *user_data, Evas_Object *obj, bool isAddToEnd)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);

    Evas_Object *content = WidgetFactory::CreateSimpleWrapper(obj, isAddToEnd);
    EventWidgetFactory::CreateBdayItem(content, action_data);

    return content;
}

#endif
