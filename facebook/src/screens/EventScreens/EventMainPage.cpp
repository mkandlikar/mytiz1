#include "Common.h"
#include "Event.h"
#include "EventCreatePage.h"
#include "EventBatchProvider.h"
#include "EventGuestListFactory.h"
#include "EventMainPage.h"
#include "EventProfilePage.h"
#include "EventWidgetFactory.h"
#include "FriendBirthDayProvider.h"
#include "FriendsProvider.h"
#include "Log.h"
#include "Popup.h"
#include "SearchScreen.h"
#include "SuggestedFriendsProvider.h"
#include "TrackItemsProxyAdapter.h"
#include "Utils.h"
#include "WebViewScreen.h"
#include "WidgetFactory.h"

#ifdef EVENT_NATIVE_VIEW

EventMainPage::EventMainPage() : ScreenBase(NULL), TrackItemsProxyAdapter(NULL, EventBatchProvider::GetInstance())
{
    mAction = new EventActions(this);

    mScreenId = SID_EVENT_MAIN_PAGE;

    mRequestInviteFriends = NULL;
    mContextMenu = NULL;
    mNoEvents = NULL;
    mSelectorText = NULL;
    mBirthDayBox = NULL;
    mSelectFriendsScreen = NULL;
    mId = NULL;

    Evas_Object *parent = getParentMainLayout();

    mLayout = WidgetFactory::CreateBaseScreenLayout(this, parent, i18n_get_text("IDS_SETTING_EVENTS"), true, true, false);

    ApplyScroller(mLayout, false);
    elm_object_part_content_set(mLayout, "content", GetScroller());

    Evas_Object* box_selector = elm_box_add( GetScroller() );
    elm_box_homogeneous_set(box_selector, EINA_FALSE);
    elm_object_content_set(GetScroller(), box_selector);
    evas_object_show(box_selector);

    Evas_Object* container  = elm_layout_add(box_selector);
    elm_layout_file_set(container, Application::mEdjPath, "event_area");
    elm_box_pack_end(box_selector, container);
    evas_object_show(container);

    mFirstBox = CreateSelector(container);
    evas_object_show(mFirstBox);
    elm_layout_content_set(container, "static_header", mFirstBox);

    Evas_Object* box_events = elm_box_add(container);
    evas_object_show(box_events);
    elm_layout_content_set(container,"content_area", box_events);

    SetDataArea( box_events );

    mIsEventProfileUploadCompleted = false;
    m_GoingUsersDataUploader = NULL;
    m_MaybeUsersDataUploader = NULL;
    m_DeclinedUsersDataUploader = NULL;
    m_InvitedUsersDataUploader = NULL;
    mSuggestedFriendsUploader = NULL;


    mBdayUploader = new DataUploader(FriendBirthDayProvider::GetInstance(), on_get_bdays_completed, this);
    mBdayUploader->StartUploading();

    AppEvents::GetInstance()->Subscribe(eREDRAW_EVENT, this);
    AppEvents::GetInstance()->Subscribe(eEVENT_ITEM_CHANGE_EVENT, this);
    AppEvents::GetInstance()->Subscribe(eEVENT_EDIT_UPDATE_EVENT, this);
    AppEvents::GetInstance()->Subscribe(eEVENT_ITEM_UPDATE_EVENT, this);
}

void EventMainPage::Push()
{
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

EventMainPage::~EventMainPage()
{
    AppEvents::GetInstance()->Unsubscribe(eREDRAW_EVENT, this);
    AppEvents::GetInstance()->Unsubscribe(eEVENT_ITEM_CHANGE_EVENT, this);
    AppEvents::GetInstance()->Unsubscribe(eEVENT_EDIT_UPDATE_EVENT, this);
    AppEvents::GetInstance()->Unsubscribe(eEVENT_ITEM_UPDATE_EVENT, this);

    EventBatchProvider::GetInstance()->EraseData();
    EventBatchProvider::GetInstance()->SetSelectorId(EventBatchProvider::SELECTOR_UPCOMING);

    delete mAction;
    FacebookSession::GetInstance()->ReleaseGraphRequest(mRequestInviteFriends);

    delete mBdayUploader;

    delete m_GoingUsersDataUploader;
    delete m_MaybeUsersDataUploader;
    delete m_InvitedUsersDataUploader;
    delete m_DeclinedUsersDataUploader;
    delete mSuggestedFriendsUploader;

    if (mSelectFriendsScreen) {
        mSelectFriendsScreen->UnregisterClient();
    }

    free((void *) mId); mId = NULL;
}

void EventMainPage::on_get_bdays_completed(void* data)
{
    EventMainPage *me = static_cast<EventMainPage*>(data);
    me->RequestData();
}

void* EventMainPage::CreateItem(void *dataForItem, bool isAddToEnd)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "EventMainPage::CreateItem");

    ActionAndData *data = NULL;
    GraphObject *graphObj = static_cast<GraphObject*>(dataForItem);
    if(graphObj){
        switch (graphObj->GetGraphObjectType()) {
        case GraphObject::GOT_SEPARATOR: {
            data = new ActionAndData(graphObj, mAction);
            data->mParentWidget = GetBirthdaysList(isAddToEnd);
        }
        break;
        case GraphObject::GOT_EVENT: {
            data = new ActionAndData(graphObj, mAction);
            data->mParentWidget = EventsGet(data, GetDataArea(), isAddToEnd);
        }
        break;
        default:
            break;
        }
    }
    return data;
}

void* EventMainPage::UpdateItem(void* dataForItem, bool isAddToEnd)
{
    ActionAndData *data = static_cast<ActionAndData*>(dataForItem);
    GraphObject *object = static_cast<GraphObject*>(data->mData);

    DeleteUIItem( dataForItem );

    switch( object->GetGraphObjectType() )
    {
        case GraphObject::GOT_SEPARATOR:
        {
            data->mParentWidget = GetBirthdaysList(isAddToEnd);
        }
        break;
        case GraphObject::GOT_EVENT:
        {
            data->mParentWidget = EventsGet(data, GetDataArea(), isAddToEnd);
        }
        break;
        default:
        {
            delete data;
            data = NULL;
        }
    }

    return data;
}

void EventMainPage::SetDataAvailable( bool isAvailable )
{
    if ( mNoEvents ) {
        evas_object_del( mNoEvents );
        mNoEvents = NULL;
    }

    if ( mBirthDayBox ) {
        evas_object_del( mBirthDayBox );
        mBirthDayBox = NULL;
    }

    if ( !IsConnectionError() &&  //No data visibility is meaningful when device is in connected mode only
         !isAvailable ) {
        mNoEvents = ShowNoAvailableEvents( GetDataArea(), EventBatchProvider::GetInstance()->GetSelectorId());
    }
}

Evas_Object *EventMainPage::EventsGet(void *user_data, Evas_Object *obj, bool isAddToEnd)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);

    Evas_Object *content = WidgetFactory::CreateSimpleWrapper(obj, isAddToEnd);
    EventWidgetFactory::CreateSelectedEventItem(content, action_data);

    return content;
}

Evas_Object *EventMainPage::CreateSelector(Evas_Object *parent)
{
    Evas_Object *selector_layout = elm_layout_add(parent);
    evas_object_size_hint_weight_set(selector_layout, 0.0, 0.0);
    elm_layout_file_set(selector_layout, Application::mEdjPath, "event_main_page_content_selector");
    elm_box_pack_start(parent, selector_layout);
    evas_object_show(selector_layout);

    Evas_Object *selector_box = elm_box_add(selector_layout);
    elm_object_part_content_set(selector_layout, "selector_box", selector_box);
    evas_object_size_hint_align_set(selector_box, 0.0, 0.5);
    elm_box_horizontal_set(selector_box, EINA_TRUE);
    evas_object_show(selector_box);

    char *selector_text = NULL;
    mSelectorText = elm_label_add(selector_box);
    evas_object_size_hint_weight_set(mSelectorText, 0.5, 0.5);
    evas_object_size_hint_align_set(mSelectorText, 0, 0.5);
    selector_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_000_FORMAT,
                            ScreenBase::R->EVENT_SELECTOR_TEXT_SIZE, i18n_get_text("IDS_UPCOMING_EVENTS"));
    if (selector_text) {
        elm_object_text_set(mSelectorText, selector_text);
        delete[] selector_text;
    }
    evas_object_event_callback_add(mSelectorText, EVAS_CALLBACK_MOUSE_UP, on_selector_clicked, this);
    elm_box_pack_end(selector_box, mSelectorText);
    evas_object_show(mSelectorText);

    Evas_Object *selector_icon = elm_image_add(selector_box);
    evas_object_size_hint_weight_set(selector_icon, 0.2, 0.2);
    evas_object_size_hint_align_set(selector_icon, 0, 0.5);
    evas_object_size_hint_min_set(selector_icon, ScreenBase::R->EVENT_SELECTOR_ICON_SIZE, ScreenBase::R->EVENT_SELECTOR_ICON_SIZE);
    evas_object_size_hint_max_set(selector_icon, ScreenBase::R->EVENT_SELECTOR_ICON_SIZE, ScreenBase::R->EVENT_SELECTOR_ICON_SIZE);
    elm_image_file_set(selector_icon, ICON_DIR"/Events/dropdown_arrow.png", NULL);
    evas_object_event_callback_add(selector_icon, EVAS_CALLBACK_MOUSE_UP, on_selector_clicked, this);
    elm_box_pack_end(selector_box, selector_icon);
    evas_object_show(selector_icon);

    Evas_Object *spacer = elm_box_add(selector_box);
    evas_object_size_hint_weight_set(spacer, 20, 20);
    evas_object_size_hint_align_set(spacer, -1, -1);
    elm_box_pack_end(selector_box, spacer);
    evas_object_show(spacer);

    char *create_text = NULL;
    Evas_Object *create_label = elm_label_add(selector_box);
    evas_object_size_hint_weight_set(create_label, 0.5, 0.5);
    evas_object_size_hint_align_set(create_label, 0, 0.5);
    create_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_5890FE_FORMAT,
                            ScreenBase::R->EVENT_CREATE_TEXT_SIZE, i18n_get_text("IDS_MAIN_CREATE_EVENT"));
    if (create_text) {
        elm_object_text_set(create_label, create_text);
        delete[] create_text;
    }
    evas_object_event_callback_add(create_label, EVAS_CALLBACK_MOUSE_UP, on_create_clicked, NULL);
    elm_box_pack_end(selector_box, create_label);
    evas_object_show(create_label);

    return selector_layout;
}

Evas_Object *EventMainPage::ShowNoAvailableEvents(Evas_Object *parent, const char * selectorId)
{
    Evas_Object *no_events = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(parent, "no_available_events");

    if (selectorId) {
        if (!strcmp(selectorId, EventBatchProvider::SELECTOR_UPCOMING)) {
            elm_object_translatable_part_text_set(no_events, "text", "IDS_NO_UPCOMING_EVENTS");
            mBirthDayBox = GetBirthdaysList(true);
        } else if (!strcmp(selectorId, EventBatchProvider::SELECTOR_INVITES)) {
            elm_object_translatable_part_text_set(no_events, "text", "IDS_NO_INVITES_EVENTS");
        } else if (!strcmp(selectorId, EventBatchProvider::SELECTOR_SAVED)) {
            elm_object_translatable_part_text_set(no_events, "text", "IDS_NO_SAVED_EVENTS");
        } else if (!strcmp(selectorId, EventBatchProvider::SELECTOR_HOSTED)) {
            elm_object_translatable_part_text_set(no_events, "text", "IDS_NO_HOSTING_EVENTS");
        } else {
            elm_object_translatable_part_text_set(no_events, "text", "IDS_NO_PAST_EVENTS");
        }
    }

    return no_events;
}

void EventMainPage::InviteFriends(const char *eventId)
{
    if (eventId) {
        LockScreen();
        ProgressBarShow();
        mId = SAFE_STRDUP(eventId);
        SetEntityId(mId);
    }
}

void EventMainPage::SetEntityId(const char * id)
{
    if (id) {
        EventGuestListFactory::GetProvider(EVENT_GOING_TAB)->SetEntityId(id);
        EventGuestListFactory::GetProvider(EVENT_MAYBE_TAB)->SetEntityId(id);
        EventGuestListFactory::GetProvider(EVENT_INVITED_TAB)->SetEntityId(id);
        EventGuestListFactory::GetProvider(EVENT_DECLINED)->SetEntityId(id);
    }

    if (m_GoingUsersDataUploader) {
        delete m_GoingUsersDataUploader; m_GoingUsersDataUploader = NULL;
    }
    if (m_MaybeUsersDataUploader) {
        delete m_MaybeUsersDataUploader; m_MaybeUsersDataUploader = NULL;
    }
    if (m_InvitedUsersDataUploader) {
        delete m_InvitedUsersDataUploader; m_InvitedUsersDataUploader = NULL;
    }
    if (m_DeclinedUsersDataUploader) {
        delete m_DeclinedUsersDataUploader; m_DeclinedUsersDataUploader = NULL;
    }
    if (mSuggestedFriendsUploader) {
        delete mSuggestedFriendsUploader; mSuggestedFriendsUploader = NULL;
    }

    mIsEventProfileUploadCompleted = true;

    m_GoingUsersDataUploader = new DataUploader(EventGuestListFactory::GetProvider(EVENT_GOING_TAB), friends_has_been_received_cb, this);
    m_GoingUsersDataUploader->StartUploading();

    m_MaybeUsersDataUploader = new DataUploader(EventGuestListFactory::GetProvider(EVENT_MAYBE_TAB), friends_has_been_received_cb, this);
    m_MaybeUsersDataUploader->StartUploading();

    m_InvitedUsersDataUploader = new DataUploader(EventGuestListFactory::GetProvider(EVENT_INVITED_TAB), friends_has_been_received_cb, this);
    m_InvitedUsersDataUploader->StartUploading();

    m_DeclinedUsersDataUploader = new DataUploader(EventGuestListFactory::GetProvider(EVENT_DECLINED), friends_has_been_received_cb, this);
    m_DeclinedUsersDataUploader->StartUploading();

    mSuggestedFriendsUploader = new DataUploader(SuggestedFriendsProvider::GetInstance(), on_get_suggested_friends_completed, this);
    mSuggestedFriendsUploader->StartUploading();
}

bool EventMainPage::IsUploadCompleted()
{
    return (mIsEventProfileUploadCompleted
            && m_GoingUsersDataUploader && m_GoingUsersDataUploader->IsUploadCompleted()
            && m_MaybeUsersDataUploader && m_MaybeUsersDataUploader->IsUploadCompleted()
            && m_InvitedUsersDataUploader && m_InvitedUsersDataUploader->IsUploadCompleted()
            && m_DeclinedUsersDataUploader && m_DeclinedUsersDataUploader->IsUploadCompleted()
            && mSuggestedFriendsUploader && mSuggestedFriendsUploader->IsUploadCompleted());
}

Eina_List *EventMainPage::GetFriendInfos()
{
    Eina_List *list = NULL;

    Eina_Array* friends = FriendsProvider::GetInstance()->GetData();
    unsigned int friendsCount = FriendsProvider::GetInstance()->GetDataCount();

    const Eina_List *goingfriends = EventGuestListFactory::GetProvider(EVENT_GOING_TAB)->GetFriendsList();
    const Eina_List *maybefriends = EventGuestListFactory::GetProvider(EVENT_MAYBE_TAB)->GetFriendsList();
    const Eina_List *invitedfriends = EventGuestListFactory::GetProvider(EVENT_INVITED_TAB)->GetFriendsList();

    for (int i = 0; i < friendsCount; ++i) {
        Friend *fr = static_cast<Friend *> (eina_array_data_get(friends, i));
        bool IsInvited = false;
        Eina_List *l;
        void *listData;
        EINA_LIST_FOREACH(const_cast<Eina_List *> (goingfriends), l, listData) {
            User *user = static_cast<User *> (listData);
            if (*fr == *user) {
                IsInvited = true;
                break;
            }
        }

        if (IsInvited) continue;

        EINA_LIST_FOREACH(const_cast<Eina_List *> (maybefriends), l, listData) {
            User *user = static_cast<User *> (listData);
            if (*fr == *user) {
                IsInvited = true;
                break;
            }
        }

        if (IsInvited) continue;

        EINA_LIST_FOREACH(const_cast<Eina_List *> (invitedfriends), l, listData) {
            User *user = static_cast<User *> (listData);
            if (*fr == *user) {
                IsInvited = true;
                break;
            }
        }

        if (!IsInvited) {
            SelectFriendsScreen::FriendInfo *friendInfo = new SelectFriendsScreen::FriendInfo();
            friendInfo->mClientFriend = fr;
            friendInfo->SetFriend(fr);
            friendInfo->mIsSelected = false;
            list = eina_list_append(list, friendInfo);
        }
    }
    return list;
}

void EventMainPage::FriendsSelectionCompleted(Eina_List *friends, bool isCanceled)
{
    if (mSelectFriendsScreen) {
        mSelectFriendsScreen->UnregisterClient();
        mSelectFriendsScreen = NULL;
    }
    if (!isCanceled) {
        SendInviteFriendsRequest(friends);
    }
}

void EventMainPage::SendInviteFriendsRequest(Eina_List *friends)
{
    if (!mRequestInviteFriends) {
        Eina_List *l;
        void *listData;
    //calc size first
        int size = 0;
        EINA_LIST_FOREACH(friends, l, listData) {
            Friend *fr = static_cast<Friend *> (listData);
            size += strlen(fr->GetId()) + 1; // 1 more for ',' symbol
        }
        if (size) { //otherwise we don't have selected friends and don't need to send anything
            ++size; // for 0 terminator
            char *str = new char[size];
            str[0] = '\0';

            EINA_LIST_FOREACH(friends, l, listData) {
                Friend *fr = static_cast<Friend *> (listData);
                char *frStr = new char[strlen(fr->GetId()) + 2]; // 1 more for ',' symbol
                Utils::Snprintf_s(frStr, strlen(fr->GetId()) + 2, "%s,", fr->GetId());
                eina_strlcat(str, frStr, size);
                delete [] frStr;
            }

            str[strlen(str)-1] = '\0'; // replace last ',' with '\0'
            bundle *paramsBundle = bundle_create();
            bundle_add_str(paramsBundle, "users", str);
            delete [] str;

            mRequestInviteFriends = FacebookSession::GetInstance()->PostInviteFriendsToEvent(mId, paramsBundle, invite_friends_request_completed, this);
            Log::info("EventMainPage::SendInviteFriendsRequest, mId = %s", mId);
            bundle_free(paramsBundle);
        }
    }
}

Evas_Object *EventMainPage::GetBirthdaysList(bool isAddToEnd)
{
    Evas_Object *bday_box = CreateBDay(GetDataArea(), isAddToEnd);
    return bday_box;
}

Evas_Object *EventMainPage::CreateBDay(Evas_Object *parent, bool isAddToEnd)
{
    Evas_Object *box = elm_box_add(parent);
    evas_object_size_hint_weight_set(box, 1, 1);
    evas_object_size_hint_align_set(box, -1, -1);
    if ( isAddToEnd ) {
        elm_box_pack_end(parent, box);
    } else {
        elm_box_pack_start(parent, box);
    }
    evas_object_show(box);

    Evas_Object *mContentBdayLayout = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(box, "event_main_page_content_bday_header");

    elm_object_translatable_part_text_set(mContentBdayLayout, "bday_text", "IDS_EVENTS_BIRTHDAYS");

    int count = FriendBirthDayProvider::GetInstance()->GetDataCount();

    for (int i = 0; i < count; i++) {

        GraphObject *user = static_cast<GraphObject*>(eina_array_data_get(FriendBirthDayProvider::GetInstance()->GetData(), i));
        ActionAndData *data = new ActionAndData(user, mAction);
        Evas_Object *item = EventWidgetFactory::CreateBdayItem(box, data);
        evas_object_event_callback_add(item, EVAS_CALLBACK_DEL, on_bday_item_delete_cb, data);

        if (i > 1) {
            break;
        }
    }

    if (count > 3) {
        EventWidgetFactory::CreateViewAllItem(box);
    }

    return box;
}

void EventMainPage::on_bday_item_delete_cb(void *data, Evas *e, Evas_Object *obj, void *event_info)
{
    delete static_cast<ActionAndData*>(data);
}

void EventMainPage::friends_has_been_received_cb(void *data)
{
    EventMainPage *me = static_cast<EventMainPage*>(data);
    if (me) {
        if (me->IsUploadCompleted()) {
            //AAA ToDo: Probably we need to refresh something else on the screen
            if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_SELECT_FRIENDS) {
                me->mSelectFriendsScreen = new SelectFriendsScreen(me, "IDS_EVENT_PROFILE_INVITE_FRIENDS");
                Application::GetInstance()->AddScreen(me->mSelectFriendsScreen);
                me->UnlockScreen();
                me->ProgressBarHide();
                Eina_List *list = me->GetFriendInfos();
                me->mSelectFriendsScreen->SetFriendInfos(list);
                list = eina_list_free(list);
            }
        }
    }
}

void EventMainPage::on_get_suggested_friends_completed(void* data)
{
// AAA Now we do nothing here. This is just needed to speedup suggested friends downloading.
}

void EventMainPage::on_selector_clicked(void *user_data, Evas *e, Evas_Object *obj, void *event_info)
{
    EventMainPage *me = static_cast<EventMainPage*>(user_data);

    const int menuItemsCount = 5;

    // TODO Change icon to active in case of selector clicking from appropriate screen
    static PopupMenu::PopupMenuItem context_menu_items[menuItemsCount] =
    {
        {
            ICON_DIR "/Events/event_filter_upcoming.png",
            "IDS_UPCOMING_EVENTS",
            NULL,
            on_upcoming_clicked,
            NULL,
            0,
            false,
            false
        },
        {
            ICON_DIR"/Events/event_filter_invites.png",
            "IDS_INVITES_EVENTS",
            NULL,
            on_invites_clicked,
            NULL,
            1,
            false,
            false
        },
        {
            ICON_DIR"/Events/event_filter_hosted.png",
            "IDS_HOSTING_EVENTS",
            NULL,
            on_hosting_clicked,
            NULL,
            2,
            false,
            false
        },
        {
            ICON_DIR"/Events/event_filter_past.png",
            "IDS_PAST_EVENTS",
            NULL,
            on_past_events,
            NULL,
            3,
            false,
            false
        },
        {
            ICON_DIR"/Events/event_filter_nearby.png",
            "IDS_NEARBY_EVENTS",
            NULL,
            on_nearby_events,
            NULL,
            4,
            true,
            false
        },
    };

    context_menu_items[0].data = me;
    context_menu_items[1].data = me;
    context_menu_items[2].data = me;
    context_menu_items[3].data = me;
    context_menu_items[4].data = me;

    context_menu_items[0].callback = on_upcoming_clicked;
    context_menu_items[1].callback = on_invites_clicked;
    context_menu_items[2].callback = on_hosting_clicked;
    context_menu_items[3].callback = on_past_events;
    context_menu_items[4].callback = on_nearby_events;

    Evas_Coord y = 0, h = 0;

    evas_object_geometry_get(obj, NULL, &y, NULL, &h);
    y += h/2;

    PopupMenu::Show(menuItemsCount, context_menu_items, me->getParentMainLayout(), false, y);
}

void EventMainPage::on_upcoming_clicked(void *user_data, Evas_Object *obj, void *event_info)
{
    EventMainPage *me = static_cast<EventMainPage*>(user_data);

    evas_object_del(me->mContextMenu);
    me->mContextMenu = NULL;

    me->EraseWindowData();
    EventBatchProvider::GetInstance()->EraseData();
    EventBatchProvider::GetInstance()->SetSelectorId(EventBatchProvider::SELECTOR_UPCOMING);
    me->RequestData();

    const char * selector_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_000_FORMAT,
                                        ScreenBase::R->EVENT_SELECTOR_TEXT_SIZE, i18n_get_text("IDS_UPCOMING_EVENTS"));
    if (selector_text) {
        elm_object_text_set(me->mSelectorText, selector_text);
        delete[] selector_text;
    }
}

void EventMainPage::on_invites_clicked(void *user_data, Evas_Object *obj, void *event_info)
{
    EventMainPage *me = static_cast<EventMainPage*>(user_data);

    evas_object_del(me->mContextMenu);
    me->mContextMenu = NULL;

    me->EraseWindowData();
    EventBatchProvider::GetInstance()->EraseData();
    EventBatchProvider::GetInstance()->SetSelectorId(EventBatchProvider::SELECTOR_INVITES);
    me->RequestData();

    const char * selector_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_000_FORMAT, ScreenBase::R->EVENT_SELECTOR_TEXT_SIZE, i18n_get_text("IDS_INVITES_EVENTS"));
    if (selector_text) {
        elm_object_text_set(me->mSelectorText, selector_text);
        delete[] selector_text;
    }
}

void EventMainPage::on_saved_clicked(void *user_data, Evas_Object *obj, void *event_info)
{
    EventMainPage *me = static_cast<EventMainPage*>(user_data);

    evas_object_del(me->mContextMenu);
    me->mContextMenu = NULL;

    me->EraseWindowData();
    EventBatchProvider::GetInstance()->EraseData();
    EventBatchProvider::GetInstance()->SetSelectorId(EventBatchProvider::SELECTOR_SAVED);
    me->RequestData();

    const char * selector_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_000_FORMAT,
                                        ScreenBase::R->EVENT_SELECTOR_TEXT_SIZE, i18n_get_text("IDS_SAVED_EVENTS"));
    if (selector_text) {
        elm_object_text_set(me->mSelectorText, selector_text);
        delete[] selector_text;
    }
}

void EventMainPage::on_hosting_clicked(void *user_data, Evas_Object *obj, void *event_info)
{
    EventMainPage *me = static_cast<EventMainPage*>(user_data);

    evas_object_del(me->mContextMenu);
    me->mContextMenu = NULL;

    me->EraseWindowData();
    EventBatchProvider::GetInstance()->EraseData();
    EventBatchProvider::GetInstance()->SetSelectorId(EventBatchProvider::SELECTOR_HOSTED);
    me->RequestData();

    const char * selector_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_000_FORMAT,
                                        ScreenBase::R->EVENT_SELECTOR_TEXT_SIZE, i18n_get_text("IDS_HOSTING_EVENTS"));
    if (selector_text) {
        elm_object_text_set(me->mSelectorText, selector_text);
        delete[] selector_text;
    }

}

void EventMainPage::on_past_events(void *user_data, Evas_Object *obj, void *event_info)
{
    EventMainPage *me = static_cast<EventMainPage*>(user_data);

    evas_object_del(me->mContextMenu);
    me->mContextMenu = NULL;

    me->EraseWindowData();
    EventBatchProvider::GetInstance()->EraseData();
    EventBatchProvider::GetInstance()->SetSelectorId(EventBatchProvider::SELECTOR_PAST);
    me->RequestData();

    const char * selector_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_000_FORMAT,
                                        ScreenBase::R->EVENT_SELECTOR_TEXT_SIZE, i18n_get_text("IDS_PAST_EVENTS"));
    if (selector_text) {
        elm_object_text_set(me->mSelectorText, selector_text);
        delete[] selector_text;
    }
}

void EventMainPage::on_nearby_events(void *user_data, Evas_Object *obj, void *event_info)
{
    WebViewScreen::Launch(URL_NEARBY_EVENTS, true);
    elm_genlist_item_selected_set((Elm_Object_Item *) event_info, EINA_FALSE);
}

void EventMainPage::on_create_clicked(void *user_data, Evas *e, Evas_Object *obj, void *event_info)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "EventWidgetFactory::on_create_clicked");
    EventCreatePage *event_create_screen = new EventCreatePage(NULL, false);
    Application::GetInstance()->AddScreen(event_create_screen);
}

void EventMainPage::invite_friends_request_completed (void* object, char* response, int code)
{
    Log::info(LOG_FACEBOOK_EVENT, "EventMainPage::invite_friends_request_completed");
    EventMainPage *me = static_cast<EventMainPage *> (object);

    FacebookSession::GetInstance()->ReleaseGraphRequest(me->mRequestInviteFriends);

    if (code == CURLE_OK) {
        Log::debug(LOG_FACEBOOK_EVENT, "EventMainPage::response: %s", response);
        FbRespondBase * responseBase = FbRespondBase::createFromJson(response);
        if (!responseBase) {
            Log::error(LOG_FACEBOOK_EVENT, "EventMainPage::Error parsing http response");
        } else {
            if (responseBase->mError) {
                Log::error(LOG_FACEBOOK_EVENT, "EventMainPage::Error sending invite friends, error = %s", responseBase->mError);
            } else if (responseBase->mSuccess) {
                if (me->mId) {
                    delete me->m_InvitedUsersDataUploader; me->m_InvitedUsersDataUploader = NULL;
                    EventGuestListFactory::GetProvider(EVENT_INVITED_TAB)->SetEntityId(me->mId);
                    me->m_InvitedUsersDataUploader = new DataUploader(EventGuestListFactory::GetProvider(EVENT_INVITED_TAB), NULL, NULL);
                    if (me->m_InvitedUsersDataUploader) {
                        me->m_InvitedUsersDataUploader->StartUploading();
                    }
                    // TODO paste callback for updating Invited counter here
                    // request summary of invited users or just increment the invited counter
                }
            }
            delete responseBase;
        }
    } else {
        Log::error(LOG_FACEBOOK_EVENT, "EventProfilePage::Error sending http request");
    }

    free(response)    //Attention!! client should take care to free respond buffer;
}

bool EventMainPage::HandleBackButton()
{
    return PopupMenu::Close();
}

void EventMainPage::Update(AppEventId eventId, void *data)
{
    switch(eventId) {
    case eREDRAW_EVENT:
        if (EventBatchProvider::GetInstance()->GetDataCount() == 0) {
            RequestData();
        }
        UpdateItemsWindow();
        break;
    case eEVENT_ITEM_CHANGE_EVENT:
        if (data) {
            Event *event = static_cast<Event*>(data);
            if (event && event->GetId()) {
                dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "EventMainPage::Update::eEVENT_ITEM_CHANGE_EVENT");
                dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "Update::NewEventID %s", event->GetId());
                CustomUpdateItem(eEVENT_ITEM_UPDATE_EVENT, static_cast<const char*>(event->GetId()));
            }
        }
        break;
    case eEVENT_EDIT_UPDATE_EVENT:
        if (data) {
            EventBatchProvider::GetInstance()->EraseData();
            UpdateItemsWindow();
            RequestData();
        }
        break;
    case eEVENT_ITEM_UPDATE_EVENT:
        if (data) {
            ActionAndData *actionData = static_cast<ActionAndData*>(data);
            EventWidgetFactory::RefreshEventInfoBox(actionData);
        }
        break;
    default:
        TrackItemsProxy::Update(eventId, data);
        break;
    }
}

#endif
