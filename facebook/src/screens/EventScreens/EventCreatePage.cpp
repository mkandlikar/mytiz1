/*
 * EventCreatePage.cpp
 *
 *  Created on: Aug 26, 2015
 *      Author: ruibezruch
 */
#include <notification_status.h>
#include <assert.h>

#include "Common.h"
#include "CheckInScreen.h"
#include "Event.h"
#include "EventBatchProvider.h"
#include "EventCreatePage.h"
#include "EventProfilePage.h"
#include "FbRespondGetEvent.h"
#include "Log.h"
#include "PCPrivacyModel.h"
#include "Popup.h"
#include "Utils.h"
#include "WidgetFactory.h"

#ifdef EVENT_NATIVE_VIEW

EventCreatePage::EventCreatePage(Event *event, bool isFromProfile) : ScreenBase(NULL), mEndSavedTimeDate()
{
    mScreenId = ScreenBase::SID_EVENT_CREATE_PAGE;
    mAction = new FriendsAction(this);

    mCreateRequest = NULL;
    mRequestData = NULL;
    mIsHeaderCallbacksActivated = false;

    mIsFromProfile = isFromProfile;

    if (event) {
        mIsCreate = false;
        mGuestsCanInvite = event->mCanGuestsInvite;
        if (event->GetId()) {
            mId = SAFE_STRDUP(event->GetId());
        }
        if (event->mPrivacyStatus && !strcmp(event->mPrivacyStatus, "private")) {
            mIsPrivate = true;
        } else {
            mIsPrivate = false;
        }
    } else {
        mIsCreate = true;
        mIsPrivate = true;
        mGuestsCanInvite = false;
    }

    mPlace = NULL;

    InitUi();

    mLayout = elm_layout_add(getParentMainLayout());
    elm_layout_file_set(mLayout, Application::mEdjPath, "event_create_page_layout");
    evas_object_size_hint_weight_set(mLayout, 1, 1);
    evas_object_show(mLayout);

    Evas_Object *scroller = elm_scroller_add(mLayout);
    elm_object_part_content_set(mLayout, "content", scroller);
    elm_scroller_single_direction_set(scroller, ELM_SCROLLER_SINGLE_DIRECTION_HARD);
    evas_object_show(scroller);

    mDataArea = elm_box_add(mLayout);
    evas_object_size_hint_weight_set(mDataArea, 1, 1);
    evas_object_size_hint_align_set(mDataArea, -1, -1);
    elm_box_padding_set(mDataArea, 0, 0);
    elm_object_content_set(scroller, mDataArea);
    evas_object_show(mDataArea);

    CreateHeadLayout(mDataArea);

    CreateStartTimeLayout(mDataArea);

    CreateEndTimeLayout(mDataArea);

    CreateOptionalLayout(mDataArea);

    CreateDetailsEntry(mDataArea);

    CreatePrivacyInfoItem(mDataArea, mIsCreate);

    if (mIsPrivate) {
        CreateFriendsPrivacyItem(mDataArea);
    }

    Evas_Object *spacer = elm_box_add(mDataArea);
    evas_object_size_hint_weight_set(spacer, 20, 1000);
    evas_object_size_hint_align_set(spacer, -1, -1);
    elm_box_pack_end(mDataArea, spacer);
    evas_object_show(spacer);

    elm_object_translatable_part_text_set(mLayout, "done_btn_text", "IDS_DONE");

    if (event) {
        elm_object_signal_emit(mLayout, "selector.hide", "image");

        elm_entry_entry_set(mNameEntry, event->mName);
        elm_object_part_text_set(mLayout, "selector_main_text", event->mName);

        if (event->mDescription) {
            std::string str;
            str = event->mDescription;
            str = Utils::ReplaceString(str, "\n", "<br/>");

            elm_entry_entry_set(mDetailsEntry, str.c_str());
        }

        if (mIsPrivate) {
            elm_object_translatable_part_text_set(mLayout, "selector_sub_text", "IDS_PRIVATE");
        } else {
            elm_object_translatable_part_text_set(mLayout, "selector_sub_text", "IDS_PUBLIC");
        }

        struct tm tm;
        strptime(event->mStartTimeString, "%Y-%m-%dT%H:%M:%S%z", &tm);
        mStartSavedTimeDate = tm;
        mktime(&mStartSavedTimeDate);

        if (event->mEndTimeString) {
            strptime(event->mEndTimeString, "%Y-%m-%dT%H:%M:%S%z", &tm);
            mEndSavedTimeDate = tm;
            mktime(&mEndSavedTimeDate);
        }

        if (event->mPlace && event->mPlace->mName && event->mPlace->GetId()) {
            elm_object_part_text_set(mLocationObj, "location_entry", event->mPlace->mName);
            elm_object_signal_emit(mLocationObj, "location.set", "clicked");

            mPlace = new PCPlace(NULL);
            mPlace->mId = SAFE_STRDUP(event->mPlace->GetId());
            mPlace->mName = SAFE_STRDUP(event->mPlace->mName);
        }
    } else {
        elm_object_translatable_part_text_set(mLayout, "selector_main_text", "IDS_PROFILE_CREATE_EVENT");
        mStartSavedTimeDate = Utils::GetCurrentDateTime();
        elm_object_translatable_part_text_set(mLayout, "selector_sub_text", "IDS_PRIVATE");
    }
    ActivateHeaderCallbacks();
    UpdateDateTimeFields();
    Utils::SetKeyboardFocus(mNameEntry, EINA_TRUE);
}

void EventCreatePage::InitUi()
{
    mStartTime = NULL;
    mEndTime = NULL;
    mTimePicker = NULL;
    mDatePicker = NULL;
    mDateSelectorPopup = NULL;
    mTimeSelectorPopup = NULL;
    mNameEntry = NULL;
    mDetailsEntry = NULL;
    mPrivacyItem = NULL;
    mPrivacyInfo = NULL;
    mKeepDiscardPopup = NULL;
    mLocationObj = NULL;
    mDetailsLayout = NULL;
}

EventCreatePage::~EventCreatePage()
{
    if (mAction) {
        delete mAction;
    }

    if (!mIsCreate) {
        free((void *) mId); mId = NULL;
    }

    mGuestsCanInvite = false;

    if (mKeepDiscardPopup) {
    	CloseKeepDiscardPopup();
    }

    RemoveDateCallbacks();
    RemoveTimeCallbacks();

    DeactivateHeaderCallbacks();

    evas_object_del(mStartTime); mStartTime = NULL;
    evas_object_del(mEndTime); mEndTime = NULL;
    evas_object_del(mTimePicker); mTimePicker = NULL;
    evas_object_del(mDatePicker); mDatePicker = NULL;

    evas_object_del(mNameEntry); mNameEntry = NULL;
    evas_object_del(mDetailsEntry); mDetailsEntry = NULL;

    evas_object_del(mPrivacyItem); mPrivacyItem = NULL;
    evas_object_del(mPrivacyInfo); mPrivacyInfo = NULL;

    FacebookSession::GetInstance()->ReleaseGraphRequest(mCreateRequest);
    FacebookSession::GetInstance()->ReleaseGraphRequest(mRequestData);

    delete mPlace;
}

void EventCreatePage::Push()
{
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void EventCreatePage::on_back_cb(void *user_data, Evas_Object *obj, const char *emisson, const char *source)
{
	EventCreatePage *screen = static_cast<EventCreatePage*>(user_data);
	if(screen->isEventChanged()){
		if (!screen->mKeepDiscardPopup) screen->ShowKeepDiscardPopup();
	} else {
		screen->Pop();
	}

}

void EventCreatePage::on_selector_cb(void *data, Evas_Object *obj, const char *emisson, const char *source)
{
    Log::info(LOG_FACEBOOK_EVENT, "EventCreatePage::on_selector_cb");

    EventCreatePage *me = static_cast<EventCreatePage*>(data);
    me->HideKeypad();

    static PopupMenu::PopupMenuItem context_menu_items[2] =
    {
            {
                    NULL,
                    "IDS_PRIVATE",
                    "IDS_PRIVATE_SUBTEXT",
                    on_private_cb,
                    NULL,
                    0,
                    false,
                    false
            },
            {
                    NULL,
                    "IDS_PUBLIC",
                    "IDS_PUBLIC_SUBTEXT",
                    on_public_cb,
                    NULL,
                    1,
                    true,
                    false
            },
            };

    context_menu_items[0].data = me;
    context_menu_items[1].data = me;


    if (me->mIsPrivate) {
        context_menu_items[0].itemSelected = true;
        context_menu_items[1].itemSelected = false;
        context_menu_items[0].itemIcon = ICON_DIR"/Events/ic_private_ivent_a.png";
        context_menu_items[1].itemIcon = ICON_DIR"/Events/ic_public_ivent.png";
    } else {
        context_menu_items[0].itemSelected = false;
        context_menu_items[1].itemSelected = true;
        context_menu_items[0].itemIcon = ICON_DIR"/Events/ic_private_ivent.png";
        context_menu_items[1].itemIcon = ICON_DIR"/Events/ic_public_ivent_a.png";
    }

    Evas_Coord y = 0, h = 0;

    Evas_Object *scroller = elm_object_part_content_get(me->mLayout, "content");
    if(scroller) {
        evas_object_geometry_get(scroller, NULL, &y, NULL, &h);
    } else {
        evas_object_geometry_get(obj, NULL, &y, NULL, &h);
        y += h/2;
    }

    PopupMenu::Show(2, context_menu_items, me->mLayout, false, y);
}

void EventCreatePage::on_private_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    EventCreatePage *me = static_cast<EventCreatePage*>(user_data);

    if (me->mPrivacyItem == NULL) {
        me->CreateFriendsPrivacyItem(me->mDataArea);
        elm_object_translatable_part_text_set(me->mLayout, "selector_sub_text", "IDS_PRIVATE");
        me->SetEventPrivate();
    }
}

void EventCreatePage::on_public_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    EventCreatePage *me = static_cast<EventCreatePage*>(user_data);

    if (me->mPrivacyItem) {
        evas_object_del(me->mPrivacyItem);
        me->mPrivacyItem = NULL;
        elm_object_translatable_part_text_set(me->mLayout, "selector_sub_text", "IDS_PUBLIC");
        me->SetEventPublic();
    }
}

void EventCreatePage::CreateHeadLayout(Evas_Object *parent)
{
    Evas_Object *head_layout = elm_layout_add(parent);
    elm_layout_file_set(head_layout, Application::mEdjPath, "event_name_entry_layout");
    evas_object_size_hint_weight_set(head_layout, 1, 1);
    elm_box_pack_end(parent, head_layout);
    evas_object_show(head_layout);

    mNameEntry = elm_entry_add(head_layout);
    evas_object_size_hint_weight_set(mNameEntry, 1, 1);
    evas_object_size_hint_align_set(mNameEntry, EVAS_HINT_FILL, 0.5);
    elm_object_part_content_set(head_layout, "event_name_entry", mNameEntry);
    elm_entry_prediction_allow_set(mNameEntry, EINA_FALSE);
    elm_entry_scrollable_set(mNameEntry, EINA_TRUE);
    elm_entry_single_line_set(mNameEntry, EINA_TRUE);
    elm_entry_cnp_mode_set(mNameEntry, ELM_CNP_MODE_PLAINTEXT);

    char *nameText = WidgetFactory::WrapByFormat2(SANS_MEDIUM_BDC1C9_FORMAT,
            R->EVENT_CREATE_NAME_FONT_SIZE,
            i18n_get_text("IDS_EVENTS_CREATE_EVENT_NAME"));
    char *guideText = WidgetFactory::WrapByFormat(NO_MARGIN_FORMAT, nameText);
    if (guideText) {
        elm_object_part_text_set(mNameEntry, "elm.guide", guideText);
    }
    delete[] guideText;
    delete[] nameText;

    elm_entry_text_style_user_push(mNameEntry, R->EVENT_CREATE_ENTRY_PUSH);
    elm_box_pack_end(head_layout, mNameEntry);
    Utils::SetKeyboardFocus(mNameEntry, EINA_FALSE);
    evas_object_show(mNameEntry);
}

void EventCreatePage::CreateStartTimeLayout(Evas_Object* parent) {
    mStartTime = elm_layout_add(parent);
    elm_layout_file_set(mStartTime, Application::mEdjPath, "time_layout");
    evas_object_size_hint_weight_set(mStartTime, 1, 1);
    elm_box_pack_end(parent, mStartTime);
    evas_object_show(mStartTime);

    elm_object_signal_callback_add(mStartTime, "mouse,clicked,*", "time_box", on_start_time_cb, this);
    elm_object_signal_callback_add(mStartTime, "mouse,clicked,*", "date_box", on_start_date_cb, this);
}

void EventCreatePage::CreateEndTimeLayout(Evas_Object* parent) {
    mEndTime = elm_layout_add(parent);
    elm_layout_file_set(mEndTime, Application::mEdjPath, "time_layout");
    evas_object_size_hint_weight_set(mEndTime, 1, 1);
    elm_box_pack_after(parent, mEndTime, mStartTime);
    evas_object_show(mEndTime);

    elm_object_signal_callback_add(mEndTime, "mouse,clicked,*", "time_box", on_end_time_cb, this);
    elm_object_signal_callback_add(mEndTime, "mouse,clicked,*", "date_box", on_end_date_cb, this);
}

void EventCreatePage::AddDateCallbacks() {
    elm_object_signal_callback_add(mStartTime, "mouse,clicked,*", "date_box", on_start_date_cb, this);
    elm_object_signal_callback_add(mEndTime, "mouse,clicked,*", "date_box", on_end_date_cb, this);
}

void EventCreatePage::RemoveDateCallbacks() {
    if (mStartTime) {
        elm_object_signal_callback_del(mStartTime, "mouse,clicked,*", "date_box", on_start_date_cb);
    }
    if (mEndTime) {
        elm_object_signal_callback_del(mEndTime, "mouse,clicked,*", "date_box", on_end_date_cb);
    }
}

void EventCreatePage::AddTimeCallbacks() {
    elm_object_signal_callback_add(mStartTime, "mouse,clicked,*", "time_box", on_start_time_cb, this);
    elm_object_signal_callback_add(mEndTime, "mouse,clicked,*", "time_box", on_end_time_cb, this);
}

void EventCreatePage::RemoveTimeCallbacks() {
    if (mStartTime) {
        elm_object_signal_callback_del(mStartTime, "mouse,clicked,*", "time_box", on_start_time_cb);
    }
    if (mEndTime) {
        elm_object_signal_callback_del(mEndTime, "mouse,clicked,*", "time_box", on_end_time_cb);
    }
}

void EventCreatePage::on_start_date_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    EventCreatePage *me = static_cast<EventCreatePage*>(user_data);
    me->OnDateClicked(true);
}

void EventCreatePage::on_end_date_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    EventCreatePage *me = static_cast<EventCreatePage*>(user_data);
    me->OnDateClicked(false);
}

void EventCreatePage::date_changed_cb (void *data, Evas_Object *obj, void *event_info) {
    EventCreatePage *me = static_cast<EventCreatePage*>(data);
    if (me->mDateSelectorPopup && me->mDatePicker) {
        struct tm time;
        me->GetDateTimeValue(me->mDatePicker, &time);
        char *dayText = Utils::GetEventDay(time, false);
        elm_object_part_text_set(me->mDateSelectorPopup, "title,text", dayText);
        free(dayText);
    }
}

void EventCreatePage::time_changed_cb (void *data, Evas_Object *obj, void *event_info) {
    EventCreatePage *me = static_cast<EventCreatePage*>(data);
    if (me->mTimeSelectorPopup && me->mTimePicker) {
        struct tm time;
        me->GetDateTimeValue(me->mTimePicker, &time);
        char *timeText = Utils::GetEventTime(time);
        elm_object_part_text_set(me->mTimeSelectorPopup, "title,text", timeText);
        free(timeText);
    }
}

bool EventCreatePage::IsDateSet(const tm *dateTime) const
{
    return dateTime->tm_year || dateTime->tm_mon || dateTime->tm_mday || dateTime->tm_hour || dateTime->tm_min || dateTime->tm_sec;
}

void EventCreatePage::OnDateClicked(bool isStartDate)
{
    RemoveDateCallbacks();

    mDateSelectorPopup = elm_popup_add(mLayout);
    eext_object_event_callback_add(mDateSelectorPopup, EEXT_CALLBACK_BACK, eext_popup_back_cb, NULL);
    evas_object_size_hint_weight_set(mDateSelectorPopup, 1, 1);
    evas_object_size_hint_align_set(mDateSelectorPopup, -1, 0.5);

    Evas_Object *cancel_btn = elm_button_add(mDateSelectorPopup);
    elm_object_style_set(cancel_btn, "popup");
    elm_object_part_content_set(mDateSelectorPopup, "button1", cancel_btn);

    Evas_Object *set_btn = elm_button_add(mDateSelectorPopup);
    elm_object_style_set(set_btn, "popup");
    elm_object_text_set(set_btn, "Set");
    elm_object_part_content_set(mDateSelectorPopup, "button2", set_btn);

    Evas_Object *box = elm_box_add(mDateSelectorPopup);

    mDatePicker = elm_datetime_add(mLayout);
    elm_object_style_set(mDatePicker, "date_layout");
    elm_box_pack_end(box, mDatePicker);
    elm_datetime_format_set(mDatePicker, "%d/%b/%Y%H:%M");
    evas_object_smart_callback_add(mDatePicker, "changed", date_changed_cb, this);

    evas_object_size_hint_align_set(mDatePicker, 0.5, 0.5);
    evas_object_show(mDatePicker);

    if (isStartDate) {
        evas_object_smart_callback_add(set_btn, "clicked", set_start_date_btn_clicked_cb, this);
        char *dayText = Utils::GetEventDay(mStartSavedTimeDate, false);
        elm_object_part_text_set(mDateSelectorPopup, "title,text", dayText);
        free(dayText);
        elm_datetime_value_set(mDatePicker, &mStartSavedTimeDate);
        evas_object_smart_callback_add(cancel_btn, "clicked", cancel_date_btn_clicked_cb, this);
        elm_object_text_set(cancel_btn, "Cancel");
    } else {
        struct tm timeValue = IsDateSet(&mEndSavedTimeDate) ? mEndSavedTimeDate : Utils::GetCurrentDateTime();
        evas_object_smart_callback_add(set_btn, "clicked", set_end_date_btn_clicked_cb, this);
        char *dayText = Utils::GetEventDay(timeValue, false);
        elm_object_part_text_set(mDateSelectorPopup, "title,text", dayText);
        free(dayText);
        elm_datetime_value_set(mDatePicker, &timeValue);
        evas_object_smart_callback_add(cancel_btn, "clicked", clear_date_btn_clicked_cb, this);
        elm_object_text_set(cancel_btn, "Clear");
    }

    elm_object_content_set(mDateSelectorPopup, box);
    evas_object_show(mDateSelectorPopup);

    EnableKeypad(false);
}

void EventCreatePage::EnableKeypad(bool isEnabled)
{
    if (mNameEntry) {
        elm_entry_input_panel_enabled_set(mNameEntry, isEnabled);
    }
    if (mDetailsEntry) {
        elm_entry_input_panel_enabled_set(mDetailsEntry, isEnabled);
    }
}

void EventCreatePage::HideKeypad()
{
    if (mNameEntry) {
        elm_entry_input_panel_hide(mNameEntry);
    }
    if (mDetailsEntry) {
        elm_entry_input_panel_hide(mDetailsEntry);
    }
}


bool EventCreatePage::CloseDateSelector()
{
    if (mDateSelectorPopup) {
        evas_object_del(mDateSelectorPopup);
        mDateSelectorPopup = NULL;
        mDatePicker = NULL;
        AddDateCallbacks();
        EnableKeypad(true);
        return true;
    } else {
        return false;
    }
}

bool EventCreatePage::CloseTimeSelector(){
    if (mTimeSelectorPopup) {
        evas_object_del(mTimeSelectorPopup);
        mTimeSelectorPopup = NULL;
        mTimePicker = NULL;
        AddTimeCallbacks();
        EnableKeypad(true);
        return true;
    } else {
        return false;
    }
}

void EventCreatePage::cancel_date_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    EventCreatePage *me = static_cast<EventCreatePage*>(user_data);
    me->CloseDateSelector();
}

void EventCreatePage::ClearEndDateTime() {
    memset(&mEndSavedTimeDate, 0, sizeof(mEndSavedTimeDate));
    UpdateDateTimeFields();
}

void EventCreatePage::clear_date_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info) {
    EventCreatePage *me = static_cast<EventCreatePage*>(user_data);
    me->CloseDateSelector();
    me->ClearEndDateTime();
}

void EventCreatePage::set_start_date_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    EventCreatePage *me = static_cast<EventCreatePage*>(user_data);
    me->DateTimeSelected(true, true);
    me->CloseDateSelector();
}

void EventCreatePage::set_end_date_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    EventCreatePage *me = static_cast<EventCreatePage*>(user_data);
    me->DateTimeSelected(false, true);
    me->CloseDateSelector();
}

// Workaround: we need this function to recalculate wrong tm_yday value
void EventCreatePage::UpdateTMStruct (tm *time) const {
    time_t tTime = mktime(time);
    *time = *(localtime(&tTime));
}

void EventCreatePage::GetDateTimeValue (Evas_Object *obj, tm *time) const {
    elm_datetime_value_get(obj, time);
    UpdateTMStruct(time);
}

void EventCreatePage::DateTimeSelected(bool isStartDateTime, bool isDateChanged) {
    tm lTime = {0};

    if (isDateChanged) {
        GetDateTimeValue(mDatePicker, &lTime);
    } else {
        GetDateTimeValue(mTimePicker, &lTime);
    }

    if (isStartDateTime) {
        mStartSavedTimeDate = lTime;
        AdjustEndTime(&mEndSavedTimeDate, isDateChanged);
    } else {
        AdjustEndTime(&lTime, isDateChanged);
    }
    UpdateDateTimeFields();
}

void EventCreatePage::on_start_time_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    EventCreatePage *me = static_cast<EventCreatePage*>(user_data);
    me->OnTimeClicked(true);
}

void EventCreatePage::on_end_time_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    EventCreatePage *me = static_cast<EventCreatePage*>(user_data);
    me->OnTimeClicked(false);
}

void EventCreatePage::OnTimeClicked(bool isStartTime)
{
    RemoveTimeCallbacks();

    mTimeSelectorPopup = elm_popup_add(mLayout);
    eext_object_event_callback_add(mTimeSelectorPopup, EEXT_CALLBACK_BACK, eext_popup_back_cb, NULL);
    evas_object_size_hint_weight_set(mTimeSelectorPopup, 1, 1);
    evas_object_size_hint_align_set(mTimeSelectorPopup, -1, 0.5);

    Evas_Object *cancel_btn = elm_button_add(mTimeSelectorPopup);
    elm_object_style_set(cancel_btn, "popup");
    elm_object_part_content_set(mTimeSelectorPopup, "button1", cancel_btn);

    Evas_Object *set_btn = elm_button_add(mTimeSelectorPopup);
    elm_object_style_set(set_btn, "popup");
    elm_object_text_set(set_btn, "Set");
    elm_object_part_content_set(mTimeSelectorPopup, "button2", set_btn);

    mTimePicker = elm_datetime_add(mLayout);
    elm_object_style_set(mTimePicker, "time_layout_24hr");
    evas_object_size_hint_weight_set(mTimePicker, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mTimePicker, EVAS_HINT_FILL, 0.5);
    elm_datetime_field_visible_set(mTimePicker, ELM_DATETIME_HOUR, EINA_TRUE);
    elm_datetime_field_visible_set(mTimePicker, ELM_DATETIME_MINUTE, EINA_TRUE);
    elm_datetime_field_visible_set(mTimePicker, ELM_DATETIME_YEAR, EINA_FALSE);
    elm_datetime_field_visible_set(mTimePicker, ELM_DATETIME_MONTH, EINA_FALSE);
    elm_datetime_field_visible_set(mTimePicker, ELM_DATETIME_DATE, EINA_FALSE);
    elm_datetime_field_visible_set(mTimePicker, ELM_DATETIME_AMPM, EINA_FALSE);

    evas_object_smart_callback_add(mTimePicker, "changed", time_changed_cb, this);

    elm_datetime_format_set(mTimePicker, "%d/%b/%Y%H:%M");
    evas_object_show(mTimePicker);

    if (isStartTime) {
        evas_object_smart_callback_add(set_btn, "clicked", set_start_time_btn_clicked_cb, this);
        char *timeText = Utils::GetEventTime(mStartSavedTimeDate);
        elm_object_part_text_set(mTimeSelectorPopup, "title,text", timeText);
        free(timeText);
        elm_datetime_value_set(mTimePicker, &mStartSavedTimeDate);
        evas_object_smart_callback_add(cancel_btn, "clicked", cancel_time_btn_clicked_cb, this);
        elm_object_text_set(cancel_btn, "Cancel");
    } else {
        evas_object_smart_callback_add(set_btn, "clicked", set_end_time_btn_clicked_cb, this);
        struct tm timeValue = IsDateSet(&mEndSavedTimeDate) ? mEndSavedTimeDate : Utils::GetCurrentDateTime();
        char *timeText = Utils::GetEventTime(timeValue);
        elm_object_part_text_set(mTimeSelectorPopup, "title,text", timeText);
        free(timeText);
        if (IsDateSet(&mEndSavedTimeDate))
        elm_datetime_value_set(mTimePicker, &timeValue);
        evas_object_smart_callback_add(cancel_btn, "clicked", clear_time_btn_clicked_cb, this);
        elm_object_text_set(cancel_btn, "Clear");
    }

    elm_object_content_set(mTimeSelectorPopup, mTimePicker);
    evas_object_show(mTimeSelectorPopup);

    EnableKeypad(false);
}

void EventCreatePage::cancel_time_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    EventCreatePage *me = static_cast<EventCreatePage*>(user_data);
    me->CloseTimeSelector();
}

void EventCreatePage::clear_time_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info) {
    EventCreatePage *me = static_cast<EventCreatePage*>(user_data);
    me->CloseTimeSelector();
    me->ClearEndDateTime();
}

void EventCreatePage::set_start_time_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    EventCreatePage *me = static_cast<EventCreatePage*>(user_data);
    if (me) {
        me->DateTimeSelected(true, false);
        me->CloseTimeSelector();
    }
}

void EventCreatePage::set_end_time_btn_clicked_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    EventCreatePage *me = static_cast<EventCreatePage*>(user_data);
    me->DateTimeSelected(false, false);
    me->CloseTimeSelector();
}

void EventCreatePage::UpdateDateTimeFields() {
    char *dayText = Utils::GetEventDay(mStartSavedTimeDate, true);
    elm_object_part_text_set(mStartTime, "date", dayText);
    free (dayText);
    char *timeText = Utils::GetEventTime(mStartSavedTimeDate);
    elm_object_part_text_set(mStartTime, "time", timeText);
    free (timeText);

    if (IsDateSet(&mEndSavedTimeDate)) {
        dayText = Utils::GetEventDay(mEndSavedTimeDate, true);
        elm_object_part_text_set(mEndTime, "date", dayText);
        free (dayText);
        timeText = Utils::GetEventTime(mEndSavedTimeDate);
        elm_object_part_text_set(mEndTime, "time", timeText);
        free (timeText);
        elm_object_signal_emit(mEndTime, "set_datetime_active", "");
    } else {
        elm_object_part_text_set(mEndTime, "date", "End Date"); //AAA ToDo: localize
        elm_object_part_text_set(mEndTime, "time", "End Time"); //AAA ToDo: localize
        elm_object_signal_emit(mEndTime, "set_datetime_dimmed", "");
    }
}

int EventCreatePage::CompareDateTimesWithoutSecs (tm firstTime, tm secondTime) const {
//set secs to 0 first because we want to ignore them.
    firstTime.tm_sec = 0;
    secondTime.tm_sec = 0;
//if 1st more than 2nd then return > 0, if they are equal return 0, otherwise < 0
    return (mktime(&firstTime) - mktime(&secondTime));
}

int EventCreatePage::CompareTimesWithoutSecs (const tm *firstTime, const tm *secondTime) const {
//if 1st more than 2nd then return > 0, if they are equal return 0, otherwise < 0
    return ((firstTime->tm_hour*3600+firstTime->tm_min*60) -
            (secondTime->tm_hour*3600+secondTime->tm_min*60));
}

void EventCreatePage::AdjustEndTime (const tm *newEndTime, bool IsDateChanged) {
//AAA ToDo: Implement correct warning for new events which are older than 4 months.
/*    if (((mStartSavedTimeDate.tm_year == mEndSavedTimeDate.tm_year) && (mEndSavedTimeDate.tm_yday - mStartSavedTimeDate.tm_yday > 123)) || // not more than 123 days (4 months)
            ((mStartSavedTimeDate.tm_year == mEndSavedTimeDate.tm_year - 1) && (mStartSavedTimeDate.tm_yday - mEndSavedTimeDate.tm_yday < 243)) || // the same as above (365 - 243)
            (mStartSavedTimeDate.tm_year < mEndSavedTimeDate.tm_year )) {
        notification_status_message_post(i18n_get_text("IDS_EVENT_TIME_CREATION_ERROR"));
*/
    if (IsDateSet(&mEndSavedTimeDate)) {
        //if end date less than start date then set end date to start date + 3 hours.
        if (CompareDateTimesWithoutSecs(*newEndTime, mStartSavedTimeDate) <= 0) {
            mEndSavedTimeDate = mStartSavedTimeDate;
            Utils::IncreaseTime(&mEndSavedTimeDate, 0, 3, 0, 0);
            notification_status_message_post(i18n_get_text("IDS_EVENT_END_TIME_CREATION_ERROR"));
        } else {
            mEndSavedTimeDate = *newEndTime;
        }
    // End date isn't set yet.
    } else {
        if (IsDateSet(newEndTime)) {
            mEndSavedTimeDate = mStartSavedTimeDate;
            if (IsDateChanged) {
                if (CompareDateTimesWithoutSecs(*newEndTime, mStartSavedTimeDate) <= 0) {
                    Utils::IncreaseTime(&mEndSavedTimeDate, 0, 3, 0, 0);
                    notification_status_message_post(i18n_get_text("IDS_EVENT_END_TIME_CREATION_ERROR"));
                } else {
                    mEndSavedTimeDate.tm_year = newEndTime->tm_year;
                    mEndSavedTimeDate.tm_mon = newEndTime->tm_mon;
                    mEndSavedTimeDate.tm_mday = newEndTime->tm_mday;
                    UpdateTMStruct(&mEndSavedTimeDate);
                }
            //if time is changed
            } else {
                if (CompareTimesWithoutSecs(newEndTime, &mStartSavedTimeDate) <= 0) {
                    Utils::IncreaseTime(&mEndSavedTimeDate, 1, 0, 0, 0);
                    notification_status_message_post(i18n_get_text("IDS_EVENT_END_TIME_CREATION_ERROR"));
                } else {
                    mEndSavedTimeDate.tm_hour = newEndTime->tm_hour;
                    mEndSavedTimeDate.tm_min = newEndTime->tm_min;
                    mEndSavedTimeDate.tm_sec = newEndTime->tm_sec;
                    UpdateTMStruct(&mEndSavedTimeDate);
                }
            }
        }
    }
}

void EventCreatePage::CreateOptionalLayout(Evas_Object *parent)
{
	mLocationObj = elm_layout_add(parent);
    elm_layout_file_set(mLocationObj, Application::mEdjPath, "optional_layout");
    evas_object_size_hint_weight_set(mLocationObj, 1, 1);
    elm_box_pack_end(parent, mLocationObj);
    evas_object_show(mLocationObj);

    elm_object_translatable_part_text_set(mLocationObj, "optional_header", "IDS_EVENT_OPTIONAL_TEXT");
    elm_object_translatable_part_text_set(mLocationObj, "location_entry", "IDS_EVENT_LOCATION_TEXT");

    elm_object_signal_callback_add(mLocationObj, "mouse,clicked,*", "location_*", on_checkin_button_clicked, this);
}

Evas_Object* EventCreatePage::CreateDetailsWrapper(Evas_Object* parent)
{
    mDetailsLayout = elm_layout_add(parent);
    elm_layout_file_set(mDetailsLayout, Application::mEdjPath, "event_create_wrapper");
    evas_object_size_hint_weight_set(mDetailsLayout, 1, 1);
    evas_object_size_hint_align_set(mDetailsLayout, -1, 0);
    elm_box_pack_end(parent, mDetailsLayout);
    evas_object_show(mDetailsLayout);

    Evas_Object *wrapperBox = elm_box_add(mDetailsLayout);
    evas_object_size_hint_weight_set(wrapperBox, 1, 0);
    evas_object_size_hint_align_set(wrapperBox, -1, 0);
    elm_layout_content_set(mDetailsLayout, "content", wrapperBox);
    elm_box_padding_set(wrapperBox, 0, 0);

    return wrapperBox;
}

void EventCreatePage::CreateDetailsEntry(Evas_Object *parent)
{
    Evas_Object *content = CreateDetailsWrapper(parent);

    mDetailsEntry = elm_entry_add(content);
    elm_entry_line_wrap_set(mDetailsEntry, ELM_WRAP_WORD);
    evas_object_size_hint_weight_set(mDetailsEntry, 1, 1);
    evas_object_size_hint_align_set(mDetailsEntry, EVAS_HINT_FILL, 0.5);
    elm_entry_prediction_allow_set(mDetailsEntry, EINA_FALSE);

    char *detailsText = WidgetFactory::WrapByFormat2(SANS_MEDIUM_BDC1C9_FORMAT,
            R->EVENT_CREATE_DETAIL_FONT_SIZE,
            i18n_get_text("IDS_EVENT_DETAILS_TEXT"));
    char *guideText = WidgetFactory::WrapByFormat(NO_MARGIN_FORMAT, detailsText);
    if (guideText) {
        elm_object_part_text_set(mDetailsEntry, "elm.guide", guideText);
    }
    delete[] guideText;
    delete[] detailsText;

    elm_entry_text_style_user_push(mDetailsEntry, R->EVENT_CREATE_ENTRY_PUSH);
    elm_box_pack_end(content, mDetailsEntry);
    elm_entry_cnp_mode_set(mDetailsEntry, ELM_CNP_MODE_PLAINTEXT);
    evas_object_show(mDetailsEntry);
}

void EventCreatePage::CreateFriendsPrivacyItem(Evas_Object *parent)
{
    mPrivacyItem = elm_layout_add(parent);
    elm_layout_file_set(mPrivacyItem, Application::mEdjPath, "friends_privacy_item");
    evas_object_size_hint_weight_set(mPrivacyItem, 1, 1);
    elm_box_pack_after(parent, mPrivacyItem, mDetailsLayout);
    evas_object_show(mPrivacyItem);

    if (mGuestsCanInvite) {
        elm_object_signal_emit(mPrivacyItem, "mouse,clicked,*", "friends_privacy_check");
    }

    elm_object_translatable_part_text_set(mPrivacyItem, "friends_privacy_text", "IDS_PRIVATE_INVITE_FRIENDS");

    elm_object_signal_callback_add(mPrivacyItem, "check.clicked", "btn", (Edje_Signal_Cb) set_gcif_true_cb, this);
    elm_object_signal_callback_add(mPrivacyItem, "check.unclicked", "btn", (Edje_Signal_Cb) set_gcif_false_cb, this);
}

void EventCreatePage::set_gcif_true_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    EventCreatePage *me = static_cast<EventCreatePage*>(user_data);
    me->mGuestsCanInvite = true;
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "mGuests = true");
    me->CreatePrivacyInfoItem(me->mDataArea, me->mIsCreate);
}

void EventCreatePage::set_gcif_false_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    EventCreatePage *me = static_cast<EventCreatePage*>(user_data);
    me->mGuestsCanInvite = false;
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "mGuests = false");
    me->CreatePrivacyInfoItem(me->mDataArea, me->mIsCreate);
}

void EventCreatePage::SetEventPrivate()
{
    if (mIsPrivate == false) {
        mIsPrivate = true;
    }
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "EVENT IS PRIVATE");
    CreatePrivacyInfoItem(mDataArea, mIsCreate);
}

void EventCreatePage::SetEventPublic()
{
    if (mIsPrivate == true) {
        mIsPrivate = false;
    }
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "EVENT IS PUBLIC");
    CreatePrivacyInfoItem(mDataArea, mIsCreate);
}

void EventCreatePage::CreatePrivacyInfoItem(Evas_Object *parent, bool isCreate)
{
    if (isCreate) {
        if (!mPrivacyInfo) {
            mPrivacyInfo = elm_layout_add(parent);
            elm_layout_file_set(mPrivacyInfo, Application::mEdjPath, "privacy_info_item");
            evas_object_size_hint_weight_set(mPrivacyInfo, 1, 1);
            elm_box_pack_end(parent, mPrivacyInfo);
            evas_object_show(mPrivacyInfo);
        }

        elm_object_translatable_part_text_set(mPrivacyInfo, "info_text_header", mIsPrivate ? "IDS_PRIVATE_EVENT_DESCRIPTION" : "IDS_PUBLIC_EVENT_DESCRIPTION");

        if (mIsPrivate) {
            elm_object_translatable_part_text_set(mPrivacyInfo, "info_text", mGuestsCanInvite ? "IDS_PRIVATE_EVENT_GUESTS_ON" : "IDS_PRIVATE_EVENT_GUESTS_OFF");
        } else {
            elm_object_translatable_part_text_set(mPrivacyInfo, "info_text", "IDS_PUBLIC_EVENT_CREATE");
        }
    }
}

void EventCreatePage::on_done_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::info(LOG_FACEBOOK_EVENT, "EventCreatePage::on_done_cb");

    EventCreatePage *me = static_cast<EventCreatePage*>(user_data);

    me->HideKeypad();

    bundle* paramsBundle;
    paramsBundle = bundle_create();

    char* name = elm_entry_markup_to_utf8(elm_entry_entry_get(me->mNameEntry));
    if(name) {
        if(!Utils::isEmptyString(name)) {
            bundle_add_str(paramsBundle, "name", name);
            free(name);
        } else {
            notification_status_message_post(i18n_get_text("IDS_EVENT_CREATION_ERROR_NO_NAME"));
            free(name);
            return;
        }
    }
    const char *pickedTime = Utils::GetPickedTime(&me->mStartSavedTimeDate);
    std::string url = pickedTime;
    url = Utils::ReplaceString(url, "+", "%2B");
    free((void*)pickedTime);

    bundle_add_str(paramsBundle, "start_time", url.c_str());

    if (me->IsDateSet(&me->mEndSavedTimeDate)) {
        pickedTime = Utils::GetPickedTime(&me->mEndSavedTimeDate);
        url = pickedTime;
        free((void*)pickedTime);
        url = Utils::ReplaceString(url, "+", "%2B");
        bundle_add_str(paramsBundle, "end_time", url.c_str());
    }

    char* description = elm_entry_markup_to_utf8(elm_entry_entry_get(me->mDetailsEntry));
    if(description) {
        if(!Utils::isEmptyString(description)) {
            bundle_add_str(paramsBundle, "description", description);
        }
        free(description);
    }

    if (me->mPlace && me->mPlace->mName && me->mPlace->mId) {
        bundle_add_str(paramsBundle, "location", me->mPlace->mName);
        bundle_add_str(paramsBundle, "location_id", me->mPlace->mId);
    }

    if (me->mIsPrivate == true) {
        bundle_add_str(paramsBundle, "type", "private");
        bundle_add_str(paramsBundle, "can_guests_invite_friends", me->mGuestsCanInvite ? "true" : "false");
    } else {
        bundle_add_str(paramsBundle, "type", "public");
        bundle_add_str(paramsBundle, "can_guests_invite_friends", "true");
    }

    me->DeactivateHeaderCallbacks();
    me->mCreateRequest = FacebookSession::GetInstance()->PostCreateNewEvent(paramsBundle, on_done_completed, me);
    me->ProgressBarShow();

    bundle_free(paramsBundle);
}


void EventCreatePage::on_done_completed(void *object, char *response, int code)
{
    Log::info(LOG_FACEBOOK_EVENT, "EventCreatePage::on_done_completed");

    EventCreatePage *me = static_cast<EventCreatePage*>(object);
    FacebookSession::GetInstance()->ReleaseGraphRequest(me->mCreateRequest);

    me->ProgressBarHide();

    if (code == CURLE_OK) {
        Log::debug(LOG_FACEBOOK_EVENT, "response: %s", response);
        FbRespondGetEvent * eventResponse = FbRespondGetEvent::createFromJson(response);
        if (!eventResponse) {
            Log::error(LOG_FACEBOOK_EVENT, "Error parsing response");
        } else {
            if (eventResponse->mError) {
                Utils::ShowToast(me->getMainLayout(), eventResponse->mError->mMessage);
                Log::error(LOG_FACEBOOK_EVENT, "Error editing message, error = %s", eventResponse->mError->mMessage);
                me->ActivateHeaderCallbacks();
            } else {
                EventBatchProvider::GetInstance()->EraseData();
                AppEvents::GetInstance()->Notify(eREDRAW_EVENT, NULL);
                dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "Result response = %d", eventResponse->mSuccess);

                Eina_List *l, *l_next;
                void *listData;
                EINA_LIST_FOREACH_SAFE(eventResponse->mEventsList, l, l_next, listData) {
                    Event *event = static_cast<Event*>(listData);
                    if (event) {
                        me->mId = SAFE_STRDUP(event->GetId());
                    }
                }
                EventProfilePage* screen = new EventProfilePage(me->mId, elm_entry_entry_get(me->mNameEntry), NULL);
                Application::GetInstance()->ReplaceScreen(screen);
                elm_naviframe_item_title_enabled_set(screen->GetNfItem(), EINA_FALSE, EINA_FALSE);
            }
            delete eventResponse;
        }
    } else {
        Utils::ShowToast(me->getMainLayout(), i18n_get_text("IDS_ON_EVENT_CREATE_CONNECTION_LOST"));
        Log::error(LOG_FACEBOOK_EVENT, "Error sending http request");
        me->ActivateHeaderCallbacks();
    }

    free(response);
}

void EventCreatePage::on_edit_done_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::info(LOG_FACEBOOK_EVENT, "EventCreatePage::on_edit_done_cb");

    EventCreatePage *me = static_cast<EventCreatePage*>(user_data);

    if (me) {
        me->HideKeypad();

        bundle* paramsBundle;
        paramsBundle = bundle_create();

        char* name = elm_entry_markup_to_utf8(elm_entry_entry_get(me->mNameEntry));
        if(name) {
            if(!Utils::isEmptyString(name)) {
                bundle_add_str(paramsBundle, "name", name);
                free(name);
            } else {
                notification_status_message_post(i18n_get_text("IDS_EVENT_CREATION_ERROR_NO_NAME"));
                free(name);
                return;
            }
        }

        const char *pickedTime = Utils::GetPickedTime(&me->mStartSavedTimeDate);
        std::string url = pickedTime;
        url = Utils::ReplaceString(url, "+", "%2B");
        free((void*)pickedTime);

        bundle_add_str(paramsBundle, "start_time", url.c_str());

        if (me->IsDateSet(&me->mEndSavedTimeDate)) {
            pickedTime = Utils::GetPickedTime(&me->mEndSavedTimeDate);
            url = pickedTime;
            free((void*)pickedTime);
            url = Utils::ReplaceString(url, "+", "%2B");
            bundle_add_str(paramsBundle, "end_time", url.c_str());
        } else {
            bundle_add_str(paramsBundle, "end_time", "0");
        }

        char* description = elm_entry_markup_to_utf8(elm_entry_entry_get(me->mDetailsEntry));
        if (description) {
            if(!Utils::isEmptyString(description)) {
                bundle_add_str(paramsBundle, "description", description);
            }
            free(description);
        }

        if (me->mPlace && me->mPlace->mId) {
            bundle_add_str(paramsBundle, "location_id", me->mPlace->mId);
        } else {
            bundle_add_str(paramsBundle, "location_id", "0");
        }

        if (me->mIsPrivate) {
            bundle_add_str(paramsBundle, "type", "private");
            bundle_add_str(paramsBundle, "can_guests_invite_friends", me->mGuestsCanInvite ? "true" : "false");
        } else {
            bundle_add_str(paramsBundle, "type", "public");
            bundle_add_str(paramsBundle, "can_guests_invite_friends", "true");
        }

        me->DeactivateHeaderCallbacks();
        me->mCreateRequest = FacebookSession::GetInstance()->PostEditEvent(me->mId, paramsBundle, on_edit_done_completed, me);
        me->ProgressBarShow();

        bundle_free(paramsBundle);
    } else {
        assert(false);
    }
}

void EventCreatePage::on_edit_done_completed(void *object, char *response, int code)
{
    Log::info(LOG_FACEBOOK_EVENT, "EventCreatePage::on_edit_done_completed");

    EventCreatePage *me = static_cast<EventCreatePage*>(object);

    if (me) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(me->mCreateRequest);

        me->ProgressBarHide();

        if (code == CURLE_OK) {
            dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "response: %s", response);
            FbRespondGetEvent * eventResponse = FbRespondGetEvent::createFromJson(response);
            if (!eventResponse) {
                dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "Error parsing http response");
            } else {
                if (eventResponse->mError) {
                    Utils::ShowToast(me->getMainLayout(), i18n_get_text("IDS_EVENT_IS_LOCKED"));
                    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "Error editing message, error = %s", eventResponse->mError->mMessage);
                    me->ActivateHeaderCallbacks();
                } else {
                    me->mRequestData = FacebookSession::GetInstance()->GetEventProfileInfo(me->mId, on_get_event_profile_completed, me);
                }
                delete eventResponse;
            }
        } else {
            dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "Error sending http request");
            me->ActivateHeaderCallbacks();
        }
    } else {
        assert(false);
    }
    free(response);  //client should take care of freeing the response buffer
}

void EventCreatePage::on_get_event_profile_completed(void *object, char *response, int code)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "EventProfilePage::on_get_event_profile_completed");

    EventCreatePage *me = static_cast<EventCreatePage*>(object);

    if (me) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(me->mRequestData);

        if (code == CURLE_OK) {
            dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "response: %s", response);
            FbRespondGetEvent * eventResponse = FbRespondGetEvent::createFromJson(response);
            if (!eventResponse) {
                dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "Error parsing http response");
            } else {
                if (eventResponse->mError) {
                    Utils::ShowToast(me->getMainLayout(), "Data in list could not be updated");
                    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "Error getting friend requests, error = %s", eventResponse->mError->mMessage);
                } else {
                    void *listData = eina_list_last_data_get(eventResponse->mEventsList);
                    Event *event = static_cast<Event*>(listData);
                    eventResponse->RemoveDataFromList(listData);

                    event->mRsvpStatus = Event::EVENT_RSVP_ATTENDING;

                    if (me->mIsFromProfile) {
                        AppEvents::GetInstance()->Notify(eEVENT_PROFILE_UPDATE_EVENT, event);
                    }
                    EventBatchProvider::GetInstance()->ReSetDataByEvent(event);
                    AppEvents::GetInstance()->Notify(eEVENT_ITEM_CHANGE_EVENT, event);
                }
                me->Pop();
            }
            delete eventResponse;
        } else {
            dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Error sending http request");
        }
    }
    free(response);  //Attention!! client should take care of freeing the response buffer
}

bool EventCreatePage::HandleBackButton()
{
    bool ret = true;
    if (PopupMenu::Close()) {
    } else if (CloseDateSelector() || CloseTimeSelector()) {
    } else if (!mKeepDiscardPopup && isEventChanged()) {
        if (mIsHeaderCallbacksActivated) {
            ShowKeepDiscardPopup();
        }
    } else if (mKeepDiscardPopup) {
        CloseKeepDiscardPopup();
    } else if (WidgetFactory::CloseErrorDialogue()) {
    } else {
        ret = false;
    }
    return ret;
}

void EventCreatePage::ShowKeepDiscardPopup() {
    dlog_print(DLOG_INFO, LOG_TAG_FACEBOOK_EVENT, "ShowKeepDiscardPopup()");

    mKeepDiscardPopup = elm_popup_add(mLayout);

    elm_popup_orient_set(mKeepDiscardPopup, ELM_POPUP_ORIENT_CENTER );

    evas_object_smart_callback_add(mKeepDiscardPopup, "block,clicked", menu_dismiss_cb, this);

    Evas_Object *keepDiscard = elm_layout_add(mKeepDiscardPopup);

    elm_layout_file_set(keepDiscard, Application::mEdjPath, "post_composer_keep_discard_popup");
    elm_object_content_set(mKeepDiscardPopup, keepDiscard);

    elm_object_translatable_part_text_set(keepDiscard, "top_text", "IDS_EVENT_CONFIRMATION");
    elm_object_translatable_part_text_set(keepDiscard, "description", "IDS_EVENT_CREATION_DESC");
    elm_object_translatable_part_text_set(keepDiscard, "text.keep", "IDS_KEEP");
    elm_object_translatable_part_text_set(keepDiscard, "text.discard", ("IDS_DISCARD");


    elm_object_signal_callback_add(keepDiscard, "mouse,clicked,*", "keep", onKeepClicked, this);
    elm_object_signal_callback_add(keepDiscard, "mouse,clicked,*", "discard", onDiscardClicked, this);

    evas_object_show(keepDiscard);

    evas_object_show(mKeepDiscardPopup);

    EnableCancelButton(true);
}

void EventCreatePage::CloseKeepDiscardPopup() {
    dlog_print(DLOG_INFO, LOG_TAG_FACEBOOK_EVENT, "CloseKeepDiscardPopup()");
    if (mKeepDiscardPopup) evas_object_del(mKeepDiscardPopup);
    mKeepDiscardPopup = NULL;
}

void EventCreatePage::onKeepClicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    dlog_print(DLOG_INFO, LOG_TAG_FACEBOOK_EVENT, "onKeepClicked:%s:%s", emission, source);

    EventCreatePage *screen = static_cast<EventCreatePage *>(data);
    screen->CloseKeepDiscardPopup();
}

void EventCreatePage::onDiscardClicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    dlog_print(DLOG_INFO, LOG_TAG_FACEBOOK_EVENT, "onDiscardClicked:%s:%s", emission, source);

    EventCreatePage *screen = static_cast<EventCreatePage *>(data);
    screen->EnableKeypad(false);
    screen->CloseKeepDiscardPopup();
    screen->Pop();
}

void EventCreatePage::menu_dismiss_cb(void *data, Evas_Object *obj, void *event_info){
	EventCreatePage *screen = static_cast<EventCreatePage *>(data);
    screen->CloseKeepDiscardPopup();
}

bool EventCreatePage::isEventChanged()
{
	bool isEventNameChanged = false;
	bool isEventDetailsChanged = false;

	char* name = elm_entry_markup_to_utf8(elm_entry_entry_get(mNameEntry));
    if(name) {
    	dlog_print(DLOG_INFO, LOG_TAG_FACEBOOK_EVENT, "isEventNameChanged:%s", name);
    	isEventNameChanged = (!Utils::isEmptyString(name));
        free(name);
    }

	char* details = elm_entry_markup_to_utf8(elm_entry_entry_get(mDetailsEntry));
    if(details) {
    	dlog_print(DLOG_INFO, LOG_TAG_FACEBOOK_EVENT, "isEventNameChanged:%s", details);
    	isEventDetailsChanged = (!Utils::isEmptyString(details));
        free(details);
    }

    return (isEventNameChanged || isEventDetailsChanged || mPlace);
}

void EventCreatePage::LaunchCheckInScreen()
{
    if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_CHECKIN) {
        CheckInScreen::OpenScreen(this, CheckInScreen::EFromEventScreen, mPlace);
    }
}

void EventCreatePage::on_checkin_button_clicked(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK_EVENT, "on_checkin_button_clicked");
    EventCreatePage *screen = static_cast<EventCreatePage *>(data);

    if (screen) {
        screen->LaunchCheckInScreen();
    }
}

void EventCreatePage::PlaceSelected(Place* place)
{
    delete mPlace;
    if (place) {
        mPlace = new PCPlace(*place);
        dlog_print(DLOG_INFO, LOG_TAG, "PLACE SELECTED:%s", mPlace->mName);
        elm_object_part_text_set(mLocationObj, "location_entry", mPlace->mName);
        elm_object_signal_emit(mLocationObj, "location.set", "clicked");
    } else {
        mPlace = NULL;
        elm_object_translatable_part_text_set(mLocationObj, "location_entry", "IDS_EVENT_LOCATION_TEXT");
        elm_object_signal_emit(mLocationObj, "location.set", "default");
    }
}

void EventCreatePage::ActivateHeaderCallbacks()
{
    if (!mIsHeaderCallbacksActivated && mLayout) {
        if (mIsCreate) {
            elm_object_signal_callback_add(mLayout, "done.clicked", "btn", on_done_cb, this);
            elm_object_signal_callback_add(mLayout, "selector.clicked", "btn", on_selector_cb, this);
        }
        else {
            elm_object_signal_callback_add(mLayout, "done.clicked", "btn", on_edit_done_cb, this);
        }
        elm_object_signal_callback_add(mLayout, "back.clicked", "btn", on_back_cb, this);
        mIsHeaderCallbacksActivated = true;
    }
}
void EventCreatePage::DeactivateHeaderCallbacks()
{
    if (mIsHeaderCallbacksActivated && mLayout) {
        if (mIsCreate) {
            elm_object_signal_callback_del(mLayout, "done.clicked", "btn", on_done_cb);
            elm_object_signal_callback_del(mLayout, "selector.clicked", "btn", on_selector_cb);
        }
        else {
            elm_object_signal_callback_del(mLayout, "done.clicked", "btn", on_edit_done_cb);
        }
        elm_object_signal_callback_del(mLayout, "back.clicked", "btn", on_back_cb);
        mIsHeaderCallbacksActivated = false;
    }
}

#endif
