#include <string>

#include "Common.h"
#include "Config.h"
#include "dlog.h"
#include "FacebookSession.h"
#include "FbResponseAboutMore.h"
#include "ScreenBase.h"
#include "UserProfileAboutMore.h"
#include "SearchScreen.h"

#define UP_IMAGES_PATH "images/profiles/"

UserProfileAboutMore::UserProfileAboutMore(ScreenBase* ParentScreen):
    ScreenBase(ParentScreen), mParsedResponse(NULL), mGraphRequest(NULL) {
    mAction = new ActionBase(this);
    mActionAndData = new ActionAndData(NULL, mAction);
    CreateView();
}

UserProfileAboutMore::~UserProfileAboutMore() {
    if (mGraphRequest)
        FacebookSession::GetInstance()->ReleaseGraphRequest(mGraphRequest);
    delete mActionAndData;
    delete mAction;
}

void UserProfileAboutMore::LoadEdjLayout() {
    mLayout = elm_layout_add(getParentMainLayout());
    elm_layout_file_set(mLayout, Application::mEdjPath, "UserProfileAboutMore");
    evas_object_size_hint_weight_set(mLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_show(mLayout);
}

void UserProfileAboutMore::CreateView() {
    LoadEdjLayout();
    elm_object_signal_callback_add(mLayout, "SignalHeaderBack", "SourceHeaderBack",
            (Edje_Signal_Cb)HeaderBackCb, this);
    elm_object_signal_callback_add(mLayout, "SearchClickSignal", "SearchClickSource",
            (Edje_Signal_Cb)SearchCb, this);

    mEvasProgressBar = elm_progressbar_add(mLayout);
    elm_object_style_set(mEvasProgressBar, "process_medium");
    elm_object_part_content_set(mLayout, "Work", mEvasProgressBar);
    elm_progressbar_pulse_set(mEvasProgressBar, EINA_TRUE);
    elm_progressbar_pulse(mEvasProgressBar, EINA_TRUE);
    evas_object_show(mEvasProgressBar);

    InitGraphRequests();
}

void UserProfileAboutMore::HeaderBackCb(void* Data, Evas_Object* Obj, void* EventInfo) {
    UserProfileAboutMore *Me = static_cast<UserProfileAboutMore*>(Data);
    Me->Pop();
}

void UserProfileAboutMore::SearchCb(void* Data, Evas_Object* Obj, void* EventInfo) {
    ScreenBase* NewScreen = new SearchScreen(NULL);
    Application::GetInstance()->AddScreen(NewScreen);
}

void UserProfileAboutMore::AddWorkCb(void* Data, Evas_Object* Obj, void* EventInfo) {
    //UserProfileAboutMore *Me = static_cast<UserProfileAboutMore*>(Data);
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Add Work Clicked");
}

void UserProfileAboutMore::WorkOptionsCb(void* Data, Evas_Object* Obj, void* EventInfo) {
    //UserProfileAboutMore *Me = static_cast<UserProfileAboutMore*>(Data);
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Work Options Clicked");
}

Evas_Object* UserProfileAboutMore::NewLabel(Evas_Object* Layout, const char* Part, int WrapMax) {
    Evas_Object* NewLabel = elm_label_add(Layout);
    elm_label_line_wrap_set(NewLabel, ELM_WRAP_MIXED);
    elm_label_wrap_width_set(NewLabel, WrapMax);
    elm_layout_content_set(Layout, Part, NewLabel);
    return NewLabel;
}

void UserProfileAboutMore::InitGraphRequests() {
    mGraphRequest = FacebookSession::GetInstance()->GetUPAboutMore(Config::GetInstance().GetUserId().c_str(), GraphRequestCb, this);
}

void UserProfileAboutMore::GraphRequestCb(void* Object, char* Respond, int CurlCode) {
    if (CURLE_OK == CurlCode) {
        UserProfileAboutMore* Me = static_cast<UserProfileAboutMore*>(Object);
        Me->mParsedResponse = FbResponseAboutMore::Parse(Respond);
        if (Me->mParsedResponse) {
            elm_object_part_text_set(Me->mLayout, "HeaderText", Me->mParsedResponse->mUserName.c_str());
            Me->LoadWork();
        }
    }
}
void UserProfileAboutMore::LoadWork() {
    static Elm_Genlist_Item_Class GenListItem;

    Evas_Object* GenList = elm_genlist_add(mLayout);
    elm_genlist_select_mode_set(GenList, ELM_OBJECT_SELECT_MODE_NONE);
    elm_scroller_single_direction_set(GenList, ELM_SCROLLER_SINGLE_DIRECTION_HARD);
    evas_object_hide(mEvasProgressBar);
    elm_layout_content_set(mLayout, "Work", GenList);

    GenListItem.item_style = "full";
    GenListItem.homogeneous = EINA_FALSE;
    GenListItem.func.content_get = CreateWorkItemView;
    GenListItem.func.state_get = NULL;
    GenListItem.func.del = NULL;

    Eina_List* List;
    void* ListItemData;
    EINA_LIST_FOREACH(mParsedResponse->mWorkList, List, ListItemData) {
        FbResponseAboutMore::ListItemWork* Data = static_cast<FbResponseAboutMore::ListItemWork*>(ListItemData);
        Data->mAction = mActionAndData;
        elm_genlist_item_append(GenList, &GenListItem, Data, NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);
    }

    evas_object_show(GenList);
}

Evas_Object* UserProfileAboutMore::CreateWorkItemView(void* Data, Evas_Object* Obj, const char* Part) {
    FbResponseAboutMore::ListItemWork* ListItemData = static_cast<FbResponseAboutMore::ListItemWork*>(Data);
    Evas_Object* TmpObj = NULL;
    std::string TmpStr;
    int LayoutH = 0;

    Evas_Object* Layout = elm_layout_add(Obj);
    evas_object_size_hint_weight_set(Layout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    if (!ListItemData->mHeading.empty()) {
        TmpStr = "UserProfileAboutMoreWorkHead";
        LayoutH = R->UPAM_WORK_HEADING_LAYOUT_H;
    } else if (!ListItemData->mAddWork.empty()) {
        TmpStr = "UserProfileAboutMoreWorkAdd";
        LayoutH = R->UPAM_WORK_ADD_LAYOUT_H;
    }
    else {
        TmpStr = "UserProfileAboutMoreWork";
        LayoutH = R->UPAM_WORK_LAYOUT_H;
    }
    evas_object_size_hint_min_set(Layout, R->UPAM_WORK_LAYOUT_W, LayoutH);
    evas_object_size_hint_max_set(Layout, R->UPAM_WORK_LAYOUT_W, LayoutH);
    elm_layout_file_set(Layout, Application::mEdjPath, TmpStr.c_str());
    TmpStr.clear();

    char *resourcePath = app_get_resource_path();
    if (!resourcePath) {
        return nullptr;
    }
    std::string AppImagesPath(resourcePath);
    AppImagesPath.append(UP_IMAGES_PATH);
    free(resourcePath);

    if (!ListItemData->mHeading.empty()) {
        TmpObj = NewLabel(Layout, "Text", R->UPAM_WORK_HEADING_WRAP_TEXT);
        TmpStr.append("<font=TizenSans:style=Med><font_size=");
        TmpStr.append(R->UPAM_WORK_HEADING_FONT_SIZE);
        TmpStr.append("><color=#4e5665>");
        TmpStr.append(ListItemData->mHeading.c_str());
        TmpStr.append("</color></font_size></font>");
        elm_object_text_set(TmpObj, TmpStr.c_str());
        TmpStr.clear();
    } else if (!ListItemData->mAddWork.empty()) {
        TmpObj = NewLabel(Layout, "AddWorkText", R->UPAM_WORK_WRAP_TEXT);
        TmpStr.append("<font=TizenSans:style=Med><font_size=");
        TmpStr.append(R->UPAM_WORK_ADD_FONT_SIZE);
        TmpStr.append("><color=#5890ff>");
        TmpStr.append(ListItemData->mAddWork.c_str());
        TmpStr.append("</color></font_size></font>");
        elm_object_text_set(TmpObj, TmpStr.c_str());
        TmpStr.clear();

        if (!ListItemData->mImage.empty()) {
            TmpStr = AppImagesPath;
            TmpStr.append(ListItemData->mImage.c_str());
            TmpObj = elm_image_add(Layout);
            elm_image_file_set(TmpObj, TmpStr.c_str(), NULL);
            TmpStr.clear();
            elm_layout_content_set(Layout, "AddWorkImage", TmpObj);
            evas_object_show(TmpObj);
        }
        elm_object_signal_callback_add(Layout, "SignalAddWork", "AddWork*",
                (Edje_Signal_Cb)AddWorkCb, NULL);
    }
    else {
        TmpObj = NewLabel(Layout, "Name", R->UPAM_WORK_WRAP_TEXT);
        TmpStr.append("<font=TizenSans:style=Bold><font_size=");
        TmpStr.append(R->UPAM_WORK_NAME_FONT_SIZE);
        TmpStr.append("><color=#141823>");
        TmpStr.append(ListItemData->mName.c_str());
        TmpStr.append("</color></font_size></font>");
        elm_object_text_set(TmpObj, TmpStr.c_str());
        TmpStr.clear();

        TmpObj = NewLabel(Layout, "Designation", R->UPAM_WORK_WRAP_TEXT);
        TmpStr.append("<font=TizenSans:style=Regular><font_size=");
        TmpStr.append(R->UPAM_WORK_DEFAULT_FONT_SIZE);
        TmpStr.append("><color=#4e5665>");
        TmpStr.append(ListItemData->mDesignation.c_str());
        TmpStr.append("</color></font_size></font>");
        elm_object_text_set(TmpObj, TmpStr.c_str());
        TmpStr.clear();

        TmpObj = NewLabel(Layout, "Duration", R->UPAM_WORK_WRAP_TEXT);
        TmpStr.append("<font=TizenSans:style=Regular><font_size=");
        TmpStr.append(R->UPAM_WORK_DEFAULT_FONT_SIZE);
        TmpStr.append("><color=#9298a4>");
        TmpStr.append(ListItemData->mDuration.c_str());
        TmpStr.append("</color></font_size></font>");
        elm_object_text_set(TmpObj, TmpStr.c_str());
        TmpStr.clear();

        TmpObj = NewLabel(Layout, "Location", R->UPAM_WORK_WRAP_TEXT);
        TmpStr.append("<font=TizenSans:style=Regular><font_size=");
        TmpStr.append(R->UPAM_WORK_DEFAULT_FONT_SIZE);
        TmpStr.append("><color=#9298a4>");
        TmpStr.append(ListItemData->mLocation.c_str());
        TmpStr.append("</color></font_size></font>");
        elm_object_text_set(TmpObj, TmpStr.c_str());
        TmpStr.clear();

        if (!ListItemData->mImage.empty()) {
            Evas_Object* TmpObj = elm_image_add(Layout);
            elm_object_part_content_set(Layout, "Image", TmpObj);
            ListItemData->mAction->UpdateImageLayoutAsync(ListItemData->mImage.c_str(), TmpObj);
            evas_object_show(TmpObj);
        }

        elm_object_signal_callback_add(Layout, "SignalOptions", "SourceOptions",
                (Edje_Signal_Cb)WorkOptionsCb, NULL);
    }

    return Layout;
}
