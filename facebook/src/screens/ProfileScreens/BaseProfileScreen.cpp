#include "BaseProfileScreen.h"
#include "CameraRollScreen.h"
#include "Common.h"
#include "ConnectivityManager.h"
#include "DataEventDescription.h"
#include "ErrorHandling.h"
#include "ImagesCarouselScreen.h"
#include "Log.h"
#include "LoggedInUserData.h"
#include "OperationManager.h"
#include "OperationPresenter.h"
#include "Popup.h"
#include "PostComposerScreen.h"
#include "PostPresenter.h"
#include "ProfileWidgetFactory.h"
#include "UserFriendsScreen.h"
#include "UserPhotosScreen.h"
#include "Utils.h"
#include "WebViewScreen.h"
#include "WidgetFactory.h"

#include <cassert>

const unsigned int BaseProfileScreen::REFRESH_TIMER_LIMIT = 1000000 * 180;

BaseProfileScreen::BaseProfileScreen(const char *id) : ScreenBase(NULL), TrackItemsProxyAdapter(NULL, mFeedProvider = new FeedProvider())
{
    Init();
    assert(id);
    mId = id;
    SetIdentifier("BASE PROFILE SCREEN");
    mFeedProvider->SetEntityId(mId.c_str());

    mLayout = WidgetFactory::CreateBaseScreenLayout(this, getParentMainLayout(), "", true, true, false);
    CreateBaseUI();

    ApplyProgressBar(false);
    SetShowConnectionErrorEnabled(false);
    SetOperationsComparator( TrackItemsProxyAdapter::operation_comparator );

    // We start uploading new data every time we entering screen.
    //If there is no valid data we wait till the upload is finished and then set UI and Data
    //If there is some data that could be shown, we show this data and when upload is finished refresh existing UI and Data
    UpdateProfileData();

    OperationManager::GetInstance()->Subscribe(mFeedProvider);

    mObjectDownloaders = nullptr;

    AppEvents::Get().Subscribe(eLIKE_COMMENT_EVENT, this);
    AppEvents::Get().Subscribe(ePOST_MESSAGE_EVENT, this);
    AppEvents::Get().Subscribe(eINTERNET_CONNECTION_CHANGED, this);
    AppEvents::Get().Subscribe(ePOST_UPDATE_ABORTED, this);
    AppEvents::Get().Subscribe(ePOST_NOT_AVAILABLE, this);
    AppEvents::Get().Subscribe(ePOST_DELETED, this);
    AppEvents::Get().Subscribe(ePOST_DELETION_CONFIRMED, this);
    AppEvents::Get().Subscribe(eALBUM_RENAMED_EVENT, this);
    AppEvents::Get().Subscribe(eALBUM_DELETED_EVENT, this);

    SetScrollerRegionTopOffset(R->USER_FEED_HEADER_REGION_H);
    // top edge region to automatically fetch items and show in progress shared posts
}

void BaseProfileScreen::Init()
{
    mScreenId = ScreenBase::SID_USER_PROFILE;

    mName = nullptr;
    mUserActionData = nullptr;

    mCover = nullptr;
    mPostAndPhotoBtn = nullptr;
    mProfileInfo = nullptr;

    mProfileWidgetExist = false;

    mProfileDataUploader = nullptr;

    mNotificationType = {
            {EAvatarUploadSuccess, "IDS_PROFILE_ME_UPLOAD_PROFILE_PICTURE_COMPLETED"},
            {ECoverUploadSuccess, "IDS_PROFILE_ME_UPLOAD_COVER_PICTURE_COMPLETED"},
            {ENetworkError, "IDS_GEN_NETWORK_ERROR"},
            {EUploadError, "IDS_POST_UPLOAD_ERROR_NOTIFICATION"},
    };

    mAction = new FeedAction(this);

    mProfileLayout = nullptr;

    mFeedWidget = nullptr;

    mProfileBtns = nullptr;
}

BaseProfileScreen::~BaseProfileScreen()
{
    EraseWindowData();
    OperationManager::GetInstance()->UnSubscribe(mFeedProvider);
    mFeedProvider->Release();

    free(mName);

    delete mAction;

    delete mProfileDataUploader;

    delete mUserActionData;

    if (mObjectDownloaders) {
        void *listData;
        EINA_LIST_FREE(mObjectDownloaders, listData)
        {
            delete static_cast<GraphObjectDownloader*>(listData);
        }
    }
}

void BaseProfileScreen::UpdateProfileData()
{
    Log::info(LOG_FACEBOOK_USER, "BaseProfileScreen::UpdateProfileData");

    delete mProfileDataUploader; mProfileDataUploader = NULL;
    mProfileDataUploader = new UserProfileDataUploader(mId.c_str(), on_user_profile_info_received_cb, this);
    mProfileDataUploader->StartUploading();
}

void BaseProfileScreen::on_user_profile_info_received_cb(void *data, int code)
{
    BaseProfileScreen *me = static_cast<BaseProfileScreen*>(data);
    CHECK_RET_NRV(me);
    CHECK_RET_NRV(me->mProfileDataUploader);

    me->ProgressBarHide();

    bool wasProfileWidgetExist = me->mProfileWidgetExist;

    me->UpdateProfileWidget(me->mProfileDataUploader->mUserData);

    Log::info(LOG_FACEBOOK_USER, "BaseProfileScreen::on_user_profile_info_received_cb info is %s", me->mProfileDataUploader->mUserData ? "available":"not available");

    if( code == CURLE_OK ) {
        if( !Utils::IsMe(me->GetId().c_str()) && !me->mProfileDataUploader->mUserData ) {
            if (me->mProfileLayout) {
                evas_object_del(me->mProfileLayout);
                me->mProfileLayout = nullptr;
            }
            me->ShowErrorWidget(eNO_DATA_AVAILABLE);
        }//no error, no data -> my profile blocked
    } else { //error code comes
        if (!me->mProfileWidgetExist) {//no widget profile->fullscreen error
            Log::info(LOG_FACEBOOK_USER, "BaseProfileScreen::on_user_profile_info_received_cb: Show FullScreen Error");
            me->ShowErrorWidget(eCONNECTION_ERROR);
        }
    }

    if(!wasProfileWidgetExist && me->mProfileWidgetExist) {
        Log::info(LOG_FACEBOOK_USER, "BaseProfileScreen::on_user_profile_info_received_cb: Add feed");
        me->SetShowConnectionErrorEnabled(true);
        me->ApplyProgressBar(true);
        me->RequestData(true);
    }
}

void BaseProfileScreen::UpdateProfileWidget(UserProfileData *userData)
{
    Log::info(LOG_FACEBOOK_USER, "BaseProfileScreen::UpdateProfileWidget");
    if ( userData && userData->mName) {
        delete mUserActionData;
        mUserActionData = new ActionAndData(userData, mAction);
        if (!mProfileWidgetExist) {
            free(mName);
            mName = SAFE_STRDUP(userData->mName);
            WidgetFactory::RefreshBaseScreenLayout(mLayout, mName);
            CreateProfileWidget();// create first time
            mProfileWidgetExist = true;
        } else {
            RefreshCover(mCover, mUserActionData);
            RefreshActionBtns();
            RefreshProfileInfo(mProfileInfo, mUserActionData);//refresh profile internal widgets only
        }
    }
}

void BaseProfileScreen::Push()
{
    if(IsLaunchedByAppControl()){
        evas_object_hide(getParentMainLayout());
    }
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

bool BaseProfileScreen::CheckDestination(const char * destination, int type) {
    Log::info(LOG_FACEBOOK_USER, "BaseProfileScreen::CheckDestination->%s",destination);
    if ( !strcmp( destination , GetId().c_str() ) ) {
        Log::info(LOG_FACEBOOK_USER, "BaseProfileScreen::CheckDestination->destinationIds are equal");
        return true;
    }
    else if ( ! strcmp( destination , "me" ) ) {// destination could be 'me' for own user profile
        if(Utils::IsMe( GetId().c_str()) ) {
            Log::info(LOG_FACEBOOK_USER, "BaseProfileScreen::CheckDestination->additional check that this is my profile");
            return true;
        }
    }
    else {
        if ( ( type == Operation::OT_Album_Add_New_Photos || type == Operation::OT_Share_Post_Create )
              && Utils::IsMe( GetId().c_str())
           ) {
            Log::info(LOG_FACEBOOK_USER, "BaseProfileScreen::CheckDestination->Shared or add to album posts");
            return true;
        }
    }
    return false;
}

void* BaseProfileScreen::CreateItem(void* dataForItem, bool isAddToEnd)
{
    ActionAndData *data = nullptr;

    if (dataForItem) {
        GraphObject *graphObject = static_cast<GraphObject *>(dataForItem);
        if (graphObject->GetGraphObjectType() == GraphObject::GOT_POST) {
            UpdatePost(data, dataForItem, isAddToEnd);
        } else if (graphObject->GetGraphObjectType() == GraphObject::GOT_OPERATION_PRESENTER) {
            OperationPresenter *operation = static_cast<OperationPresenter *>(dataForItem);
            Sptr<Operation> oper(operation->GetOperation());
            if( CheckDestination(oper->GetDestinationId(),static_cast<int>(oper->GetType())) )
            {
                UpdatePresenter(data, operation, isAddToEnd);
            }
        }
    }
    return data;
}

void* BaseProfileScreen::UpdateItem(void* dataForItem, bool isAddToEnd)
{
    assert(dataForItem);

    ActionAndData *data = static_cast<ActionAndData*>(dataForItem);
    assert(data->mData);
    GraphObject *object = static_cast<GraphObject *>(data->mData);

    DeleteUIItem(dataForItem);

    if (object->GetGraphObjectType() == GraphObject::GOT_POST) {
        UpdatePost(data, data->mData, isAddToEnd);
    } else if (object->GetGraphObjectType() == GraphObject::GOT_OPERATION_PRESENTER) {
        OperationPresenter *operation = static_cast<OperationPresenter *>(data->mData);
        UpdatePresenter(data, operation, isAddToEnd);
    } else {
        delete data;
        data = nullptr;
    }

    return data;
}

void* BaseProfileScreen::UpdateSingleItem( void* dataForItem, void* prevItemPtr )
{
    assert(dataForItem);
    ActionAndData *data = static_cast<ActionAndData*>(dataForItem);
    assert(data->mData);
    GraphObject *object = static_cast<GraphObject*>(data->mData);

    assert(prevItemPtr);

    DeleteUIItem( dataForItem );

    if (object->GetGraphObjectType() == GraphObject::GOT_POST) {
        UpdatePost(data, data->mData, false, static_cast<Evas_Object*>(prevItemPtr));
    } else {
        delete data;
        data = nullptr;
    }
    return data;
}

//private method with common logic
void BaseProfileScreen::UpdatePost(ActionAndData* &data, void* postPtr, bool isAddToEnd, Evas_Object* prevItem) {

    Evas_Object *postWidget = nullptr;
    Post *post = static_cast<Post*>(postPtr);

    Log::info("BaseProfileScreen::UpdatePost()->%s item", data ? "Update" : "Create" );

    if( (!post->GetChildId()) ||
          Utils::IsMe(post->GetFromId().c_str()) || //post from me (could be source of shared post)
          Utils::IsMe(post->GetToId()) //post for me (could be source of shared post)
    ) {
        if(!data) {
            data = new ActionAndData(post, mAction);
        }
        if (post->GetParentId()) {
            postWidget = WidgetFactory::CreateAffectedPostBox(GetDataArea(), data);
        } else if (!post->GetPrivacyIcon().empty()) { // workaround in case of no privacy due to Github #804
            postWidget = WidgetFactory::CreateStatusPost(GetDataArea(), data);
        }

        if(post->IsPhotoPost()) {
            StartDownloader(post->GetObjectId(), this, data);
        }

        if (postWidget) {
            data->mParentWidget = postWidget;
            if (prevItem) {
                elm_box_pack_after(GetDataArea(), postWidget, prevItem);
            } else if (isAddToEnd) {
                elm_box_pack_end(GetDataArea(), postWidget);
            } else {
                elm_box_pack_start(GetDataArea(), postWidget);
            }

            if (OperationManager::GetInstance()->GetActiveOperationById(post->GetId())) {
                WidgetFactory::SetPostActive(data, false);
            }

            RePackConnectionError();
        } else {
            Log::error(LOG_FACEBOOK_USER, "BaseProfileScreen::UpdatePost->no post widget");
            delete data;
            data = nullptr;
        }
    }
}

//private method with common logic
void BaseProfileScreen::UpdatePresenter(ActionAndData* &data, OperationPresenter *operation, bool isAddToEnd) {

    Evas_Object *postWidget = nullptr;

    Log::info("BaseProfileScreen::UpdatePresenter()->%s item", data ? "Update" : "Create" );

    if(!data) {//in CreateItem there is no data
        data = new ActionAndData(operation, mAction);
    }
    postWidget = WidgetFactory::CreatePostOperation( GetDataArea(), data, isAddToEnd );

    if ( postWidget ) {
        WidgetFactory::UpdatePostOperation(data, nullptr);
        data->mParentWidget = postWidget;
    } else {
        delete data;
        data = nullptr;
    }

    if (isAddToEnd) {
        elm_box_pack_end(GetDataArea(), postWidget);
    } else {
        elm_box_pack_start(GetDataArea(), postWidget);
    }
    RePackConnectionError(true);
}

void BaseProfileScreen::Update(AppEventId eventId, void * data)
{
    switch (eventId) {
    case eLIKE_COMMENT_EVENT:
        if (data) {
            ActionAndData * actionData = static_cast<ActionAndData *>(GetItemById(static_cast<char *>(data)));
            if (actionData) {
                WidgetFactory::RefreshPostItemFooter(actionData);
            }
        }
        break;
    case eDATA_CHANGES_EVENT:
        if (data) {
            DataEventDescription *dataEvent = static_cast<DataEventDescription*>(data);
            if( GetProvider() ==  dataEvent->GetProvider() ) {
                if ( dataEvent->GetDataEvent() == DataEventDescription::eDE_ITEM_ADDED ) {
                    if ( dataEvent->GetOperationType() == Operation::OT_Share_Post_Create ) {
                        if(!IsRealTopEdgeOfScroller()) {
                            SetDisableShiftUpForPresenter();
                        }
                    }
                }
                TrackItemsProxy::Update(eventId, data);
            }
        }
        break;
    case ePOST_MESSAGE_EVENT:
        if (data) {
            ActionAndData * actionData = static_cast<ActionAndData *>(GetItemById(static_cast<char *>(data)));
            if (actionData) {
                WidgetFactory::SetPostActive(actionData, false);
            }

            if (!ConnectivityManager::Singleton().IsConnected()) {
                Utils::ShowToast(getMainLayout(), i18n_get_text("IDS_LOGIN_CONNECT_ERR"));
            }
        }
        break;
    case ePOST_UPDATE_ABORTED:
        if (data) {
            ActionAndData *actionData = static_cast<ActionAndData *>(GetItemById(static_cast<char *>(data)));
            if (actionData) {
                WidgetFactory::SetPostActive(actionData, true);
            }
        }
        break;
    case eOPERATION_COMPLETE_EVENT:// event should comes from OperationManager
        if (data) {
            Log::info(LOG_FACEBOOK_USER, "BaseProfileScreen::Update, eOPERATION_COMPLETE_EVENT");
            Sptr<Operation> oper(static_cast<Operation*>(data));
            if( oper->IsFeedOperation() ) {
                BaseProfileScreen::FetchEdgeItems();
            }
        }
        break;
    case ePOST_DELETED:
    case ePOST_DELETION_CONFIRMED:
    case ePOST_NOT_AVAILABLE:
    case eALBUM_DELETED_EVENT:
        if (data) {
            Log::info( "BaseProfileScreen::Update, %s", eventId == ePOST_NOT_AVAILABLE ? "ePOST_NOT_AVAILABLE" : "ePOST_DELETED");
            FeedProvider* provider = dynamic_cast<FeedProvider*>(GetProvider());
            assert(provider);
            char *postId = static_cast<char*>(data);
            Eina_List *postsToRemove = nullptr;
            if (eventId == eALBUM_DELETED_EVENT) {
                postsToRemove = provider->GetPostsByAlbumId(postId);
            } else {
                Post* post = provider->GetPostById(postId);
                if (post) { //ToDo: check why it can be NULL. Sometimes this happens...
                    postsToRemove = eina_list_append(postsToRemove, post);
                }
            }
            Eina_List *list;
            void *data;
            EINA_LIST_FOREACH(postsToRemove, list, data) {
                Post *post = static_cast<Post*>(data);
                assert (post);
                // in case of deletion post will be replaced from screen at once
                // but from provider it will be replaced after confirmation from server comes
                if (eventId != ePOST_DELETION_CONFIRMED){
                    DestroyItemByData(post , TrackItemsProxyAdapter::items_comparator_Obj_vs_ActionAndData);
                }

                if (eventId != ePOST_DELETED) {
                    provider->DeleteItem(post);
                }
            }
            eina_list_free(postsToRemove);
        }
        break;
    case eALBUM_RENAMED_EVENT:
    {
        Eina_List *albumPosts = static_cast<FeedProvider *> (GetProvider())->GetPostsByAlbumId(static_cast<char *>(data));
        Eina_List *list;
        void *data;
        EINA_LIST_FOREACH(albumPosts, list, data) {
            Post *post = static_cast<Post*>(data);
            assert (post);
            ActionAndData *actionData = static_cast<ActionAndData *>(GetItemById(post->GetId()));
            Evas_Object *title = elm_layout_content_get(actionData->mHeader, "user_name");
            TO_STORY_TRNSLTD_TEXT_SET(title, eSTORY, *post);
        }
        eina_list_free(albumPosts);
        break;
    }
    case eINTERNET_CONNECTION_CHANGED:
        Log::info(LOG_FACEBOOK_USER, "BaseProfileScreen::Update, eINTERNET_CONNECTION_CHANGED");
        if (ConnectivityManager::Singleton().IsConnected()) {  //connection has been restored
            if(!mProfileWidgetExist) {
                HideErrorWidget();
                ProgressBarShow();
                UpdateProfileData();
            } else {
                ShowProgressBarWithTimer(IsServiceWidgetOnBottom());
                BaseProfileScreen::FetchEdgeItems();
            }
        }
        else if(mProfileWidgetExist) {
            SetShowConnectionErrorEnabled(true);
            ShowConnectionError(false);
        }
        break;
    default:
        TrackItemsProxy::Update(eventId, data);
        break;
    }
}

void BaseProfileScreen::ShowNotification(NotificationType status)
{
    const char* ids = "";
    auto it = mNotificationType.find(status);
    if (it != mNotificationType.end()) {
        ids = it->second;
    } else {
        Log::warning(LOG_FACEBOOK_USER, "ShowNotification: unknown error");
    }

    notification_status_message_post(i18n_get_text(ids));
}

void BaseProfileScreen::FetchEdgeItems()
{
    Log::info(LOG_FACEBOOK_USER, "BaseProfileScreen::FetchEdgeItems");
    if (IsUserProfileWidgetShown() &&
        mFeedProvider->GetPresentersCount()==0) {
        mFeedProvider->ResetRequestTimer();
        UpdateProfileData();
        if (GetProvider()->GetDataCount()==0) {
            Log::info(LOG_FACEBOOK_USER, "BaseProfileScreen::FetchEdgeItems->no data in provider, descending order request");
            RequestData(true);
        } else {
            Log::info(LOG_FACEBOOK_USER, "BaseProfileScreen::FetchEdgeItems->request only muted(new) items");
            RequestData(false, true);
        }
    }
}

void BaseProfileScreen::CreateProfileWidget()
{
    Log::info(LOG_FACEBOOK_USER, "BaseProfileScreen::CreateProfileWidget");
    mCover = CreateCover(mHeaderWidget, mUserActionData);
    mProfileBtns = CreateActionBtns(mHeaderWidget, mUserActionData);
    RefreshActionBtns();
    mProfileInfo = CreateProfileInfoItems(mHeaderWidget, mUserActionData);
    mPostAndPhotoBtn = CreatePostAndPhotoItem(mHeaderWidget, mUserActionData);
}

void BaseProfileScreen::CreateBaseUI()
{
    ApplyScroller(mLayout, false);
    elm_object_part_content_set(mLayout, "content", GetScroller());

    mProfileLayout = elm_layout_add(GetScroller());
    elm_layout_file_set(mProfileLayout, Application::mEdjPath, "posts.area");
    elm_object_content_set(GetScroller(), mProfileLayout);
    evas_object_show(mProfileLayout);

    mHeaderWidget = elm_box_add(mProfileLayout);
    elm_layout_content_set(mProfileLayout, "static.data.area.swallow", mHeaderWidget);
    SetHeaderArea(mHeaderWidget);

    mFeedWidget = elm_box_add(mProfileLayout);
    elm_layout_content_set(mProfileLayout, "feed.area.swallow", mFeedWidget);
    SetDataArea(mFeedWidget);
}

Evas_Object* BaseProfileScreen::CreateCover(Evas_Object *parent, ActionAndData *actionData)
{
    Evas_Object *coverLayout = WidgetFactory::CreateLayoutByGroup(mHeaderWidget, "userprofile_cover_item");

    RefreshCover(coverLayout, actionData);

    elm_box_pack_end(mHeaderWidget, coverLayout);
    evas_object_show(coverLayout);

    return coverLayout;
}

void BaseProfileScreen::RefreshCover(Evas_Object *coverLayout, ActionAndData *actionData)
{
    Log::info(LOG_FACEBOOK_USER, "BaseProfileScreen::RefreshCover");
    CHECK_RET_NRV(actionData);
    CHECK_RET_NRV(coverLayout);
    UserProfileData *data = static_cast<UserProfileData*>(actionData->mData);
    CHECK_RET_NRV(data);
    elm_object_signal_callback_del(coverLayout, "mouse,clicked,*", "cover*", on_profile_photo_clicked);
    elm_object_signal_callback_del(coverLayout, "mouse,clicked,*", "avatar*", on_profile_photo_clicked);
    if (data->mCover && data->mCover->mSource) {
        elm_object_signal_emit(coverLayout, "show_cover_photo", "");
        elm_object_signal_emit(coverLayout, "hide.default", "cover");
        actionData->UpdateImageLayoutAsync(data->mCover->mSource, coverLayout, ActionAndData::EImage, Post::EProfileCover);
        elm_object_signal_callback_add(coverLayout, "mouse,clicked,*", "cover*", on_profile_photo_clicked, this);
    } else {
        if (Utils::IsMe(mId.c_str())) {
            elm_object_signal_emit(coverLayout, "hide_cover_photo", "");
            elm_object_signal_emit(coverLayout, "show.default", "cover");
            elm_object_signal_callback_add(coverLayout, "mouse,clicked,*", "cover*", on_profile_photo_clicked, this);
        }
        Log::error(LOG_FACEBOOK_USER, "BaseProfileScreen::RefreshCover: No valid cover");
    }

    if (data->mPicture && data->mPicture->mUrl) {
        elm_object_signal_emit(coverLayout, "hide.default", "avatar");
        actionData->UpdateImageLayoutAsync(data->mPicture->mUrl, coverLayout, ActionAndData::EImage, Post::EProfileAvatar);
        elm_object_signal_callback_add(coverLayout, "mouse,clicked,*", "avatar*", on_profile_photo_clicked, this);
    } else {
        elm_object_signal_emit(coverLayout, "show.default", "avatar");
        if (Utils::IsMe(mId.c_str())) {
            elm_object_signal_callback_add(coverLayout, "mouse,clicked,*", "avatar*", on_profile_photo_clicked, this);
        }
        Log::error(LOG_FACEBOOK_USER, "BaseProfileScreen::RefreshCover: No valid avatar");
    }

    WidgetFactory::CreateFriendItemEmulator(coverLayout, R->PROFILE_TEXT_STYLE, R->PROFILE_NAME_ITEM_WIDTH);
    std::string strName = data->mName;
    int lines = WidgetFactory::GetEntryEmulatorLines(strName, R->PROFILE_NAME_ITEM_WIDTH);

    if (lines == 2) {
        elm_object_signal_emit(coverLayout, "2_lines", "");
    } else if (lines >= 3) {
        elm_object_signal_emit(coverLayout, "3_lines", "");
    }

    Evas_Object *userName = elm_object_part_content_get(coverLayout, "name");
    if (!userName) {
        userName = elm_entry_add(coverLayout);
        elm_entry_editable_set(userName, EINA_FALSE);
        elm_object_focus_allow_set(userName, EINA_FALSE);
        FRMTD_TRNSLTD_ENTRY_TXT(userName, R->PROFILE_TEXT_LTR_STYLE, R->PROFILE_TEXT_RTL_STYLE);
        elm_object_part_content_set(coverLayout, "name", userName);
    }
    elm_entry_entry_set(userName, strName.c_str());

    WidgetFactory::DeleteFriendItemEmulator();
}

void BaseProfileScreen::on_profile_photo_clicked(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::info(LOG_FACEBOOK_USER, "BaseProfileScreen::on_profile_photo_clicked");
    BaseProfileScreen *me = static_cast<BaseProfileScreen*>(user_data);
    CHECK_RET_NRV(me);
    if (source) {
        if (!strcmp(source, "cover_photo") || !strcmp(source, "cover_change") || !strcmp(source, "cover_default") || !strcmp(source, "cover_bg")) {
            me->OnCoverClicked(obj);
        } else if (!strcmp(source, "avatar") || !strcmp(source, "avatar_change") || !strcmp(source, "avatar_default") || !strcmp(source, "avatar_bg")) {
            me->OnAvatarClicked(obj);
        } else {
            Log::error(LOG_FACEBOOK_USER, "No valid source");
        }
    } else {
        Log::error(LOG_FACEBOOK_USER, "No source exist");
    }
}

void BaseProfileScreen::OnCoverClicked(Evas_Object *obj)
{
    Log::info(LOG_FACEBOOK_USER, "BaseProfileScreen::OnCoverClicked");
    assert(mUserActionData);
    ViewCover(mUserActionData);
}

void BaseProfileScreen::ViewCover(void *user_data, Evas_Object *obj, void *event_info)
{
    ActionAndData *actionData = static_cast<ActionAndData*>(user_data);
    CHECK_RET_NRV(actionData);
    CHECK_RET_NRV(actionData->mData);
    UserProfileData *data = static_cast<UserProfileData*>(actionData->mData);
    CHECK_RET_NRV(data);
    CHECK_RET_NRV(data->mCover);
    CHECK_RET_NRV(data->mCover->mPhotoPath);
    if (data->mCover->GetId()) {
        ImagesCarouselScreen * carouselScreen = new ImagesCarouselScreen(data->mCover->mPhotoPath, data->mCover->GetId(), ImagesCarouselScreen::ePROFILE_COVER_MODE);
        Application::GetInstance()->AddScreen(carouselScreen);
    }
}

void BaseProfileScreen::OnAvatarClicked(Evas_Object *obj)
{
    Log::info(LOG_FACEBOOK_USER, "BaseProfileScreen::OnAvatarClicked");
    assert(mUserActionData);
    ViewAvatar(mUserActionData);
}

void BaseProfileScreen::ViewAvatar(void *user_data, Evas_Object *obj, void *event_info)
{
    ActionAndData *actionData = static_cast<ActionAndData*>(user_data);
    CHECK_RET_NRV(actionData);
    CHECK_RET_NRV(actionData->mData);
    UserProfileData *data = static_cast<UserProfileData*>(actionData->mData);
    CHECK_RET_NRV(data);
    CHECK_RET_NRV(data->mUserAvatarPath);
    CHECK_RET_NRV(data->mUserAvatarId);

    ImagesCarouselScreen * carouselScreen = new ImagesCarouselScreen(data->mUserAvatarPath, data->mUserAvatarId, ImagesCarouselScreen::ePROFILE_COVER_MODE);
    Application::GetInstance()->AddScreen(carouselScreen);

}

Evas_Object *BaseProfileScreen::CreatePostAndPhotoItem(Evas_Object *parent, ActionAndData *actionData)
{
    Evas_Object *content = ProfileWidgetFactory::CreateProfileItemWrapper(parent);

    Evas_Object *post_photo = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(content, "post_and_photo_btns");

    elm_object_translatable_part_text_set(post_photo, "post_btn_text", "IDS_POST");
    elm_object_translatable_part_text_set(post_photo, "photo_btn_text", "IDS_PROFILE_ME_POST_PHOTO");

    elm_object_signal_callback_add(post_photo, "mouse,clicked,*", "post_btn*", on_profile_post_data_clicked, this);
    elm_object_signal_callback_add(post_photo, "mouse,clicked,*", "photo_btn*", on_profile_post_data_clicked, this);

    return post_photo;
}

void BaseProfileScreen::on_profile_post_data_clicked(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::info(LOG_FACEBOOK_USER, "BaseProfileScreen::on_profile_post_data_clicked");
    BaseProfileScreen *me = static_cast<BaseProfileScreen*>(user_data);
    CHECK_RET_NRV(me);
    if (source) {
        if (!strcmp(source, "post_btn") || !strcmp(source, "post_btn_text") || !strcmp(source, "post_btn_icon")) {
            me->OnPostTextClicked();
        } else if (!strcmp(source, "photo_btn") || !strcmp(source, "photo_btn_text") || !strcmp(source, "photo_btn_icon")) {
            me->OnPostPhotoClicked();
        } else {
            Log::error(LOG_FACEBOOK_USER, "No valid source");
        }
    } else {
        Log::error(LOG_FACEBOOK_USER, "No source exist");
    }
}

void BaseProfileScreen::OnPostTextClicked()
{
    PostComposerScreen *screen = new PostComposerScreen(this, eFRIENDTIMELINE, mId.c_str(), mName, NULL);
    Application::GetInstance()->AddScreen(screen);
}

void BaseProfileScreen::OnPostPhotoClicked()
{
    if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_CAMERA_ROLL) {
        CameraRollScreen *screen = new CameraRollScreen(eCAMERA_ROLL_POST_TO_FRIEND_TIMELINE_MODE, mId.c_str(), mName);
        Application::GetInstance()->AddScreen(screen);
    }
}

Evas_Object *BaseProfileScreen::CreateProfileInfoItems(Evas_Object *parent, ActionAndData *actionData)
{
    Evas_Object *profileInfo = WidgetFactory::CreateLayoutByGroup(parent, "profile_info_item");

    RefreshProfileInfo(profileInfo, actionData);

    elm_object_translatable_part_text_set(profileInfo, "about_text", "IDS_ABOUT");
    elm_object_translatable_part_text_set(profileInfo, "photo_text", "IDS_UP_PHOTOS");
    elm_object_translatable_part_text_set(profileInfo, "friend_text", "IDS_UP_FRIENDS");

    elm_object_signal_callback_add(profileInfo, "mouse,clicked,*", "about*", on_about_profile_clicked, this);
    elm_object_signal_callback_add(profileInfo, "mouse,clicked,*", "photo*", on_photo_profile_clicked, this);
    elm_object_signal_callback_add(profileInfo, "mouse,clicked,*", "friend*", on_friends_profile_clicked, this);

    elm_box_pack_end(parent, profileInfo);
    evas_object_show(profileInfo);

    return profileInfo;
}

void BaseProfileScreen::RefreshProfileInfo(Evas_Object *profileInfo, ActionAndData *actionData)
{
    Log::info(LOG_FACEBOOK_USER, "BaseProfileScreen::RefreshProfileInfo");

    CHECK_RET_NRV(profileInfo);
    CHECK_RET_NRV(actionData);

    UserProfileData *data = static_cast<UserProfileData*>(actionData->mData);
    CHECK_RET_NRV(data);
    if (data->mFirstFriendUrl) {
        elm_object_signal_emit(profileInfo, "hide", "friend.default");
        elm_object_signal_emit(profileInfo, "show", "friend");
        actionData->UpdateImageLayoutAsync(data->mFirstFriendUrl, profileInfo, ActionAndData::EImage, Post::EProfileFirstFriend);
    } else {
        elm_object_signal_emit(profileInfo, Application::IsRTLLanguage() ? "show.rtl" : "show.ltr", "friend.default");
        elm_object_signal_emit(profileInfo, "hide", "friend");
        Log::error(LOG_FACEBOOK_USER, "BaseProfileScreen::RefreshProfileInfo: No valid friend url");
    }
    if (data->mFirstPhotoUrl) {
        elm_object_signal_emit(profileInfo, "hide", "photo.default");
        elm_object_signal_emit(profileInfo, "show", "photo");
        actionData->UpdateImageLayoutAsync(data->mFirstPhotoUrl, profileInfo, ActionAndData::EImage, Post::EProfileFirstPhoto);
    } else {
        elm_object_signal_emit(profileInfo, Application::IsRTLLanguage() ? "show.rtl" : "show.ltr", "photo.default");
        elm_object_signal_emit(profileInfo, "hide", "photo");
        Log::error(LOG_FACEBOOK_USER, "BaseProfileScreen::RefreshProfileInfo: No valid photo url");
    }
    if (data->mAboutPhotoUrl) {
        elm_object_signal_emit(profileInfo, "hide", "about.default");
        elm_object_signal_emit(profileInfo, "show", "about");
        actionData->UpdateImageLayoutAsync(data->mAboutPhotoUrl, profileInfo, ActionAndData::EImage, Post::EProfileAboutInfo);
    } else {
        elm_object_signal_emit(profileInfo, "show", "about.default");
        elm_object_signal_emit(profileInfo, "hide", "about");
        Log::error(LOG_FACEBOOK_USER, "BaseProfileScreen::RefreshProfileInfo: No valid about url");
    }
}

void BaseProfileScreen::on_about_profile_clicked(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_WEB_VIEW) {
        BaseProfileScreen *me = static_cast<BaseProfileScreen*>(data);
        assert(me);
        std::string webUri = REQ_UPDATE_INFO_URI;
        webUri.append("&id=");
        webUri.append(me->mId);
        WebViewScreen::Launch(webUri.c_str(), true);
    }
}

void BaseProfileScreen::on_photo_profile_clicked(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    if (!ConnectivityManager::Singleton().IsConnected()) {
        notification_status_message_post(i18n_get_text("IDS_GEN_NETWORK_ERROR"));
    } else if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_USER_PHOTOS) {
        BaseProfileScreen *me = static_cast<BaseProfileScreen*>(data);
        assert(me);
        UserPhotosScreen *photosScreen = new UserPhotosScreen(NULL, me->mId.c_str(), me->mName);
        Application::GetInstance()->AddScreen(photosScreen);
    }
}

void BaseProfileScreen::on_friends_profile_clicked(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    if (!ConnectivityManager::Singleton().IsConnected()) {
        notification_status_message_post(i18n_get_text("IDS_GEN_NETWORK_ERROR"));
    } else if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_USER_PROFILE_FRIENDS_TAB) {
        BaseProfileScreen *me = static_cast<BaseProfileScreen*>(data);
        assert(me);
        UserFriendsScreen *userProfileFriendsScreen = new UserFriendsScreen(me->mId.c_str(), me->mName);
        Application::GetInstance()->AddScreen(userProfileFriendsScreen);
    }
}

void BaseProfileScreen::StartDownloader(const char *id, IGraphObjectDownloaderObserver *observer, ActionAndData *data)
{
    GraphObjectDownloader *objectDownloader = new GraphObjectDownloader(id, this, data);
    mObjectDownloaders = eina_list_append(mObjectDownloaders, objectDownloader);
    objectDownloader->StartDownloading();
}

void BaseProfileScreen::DownloaderProgress(IGraphObjectDownloaderObserver::Result result, GraphObjectDownloader *downloader) {
    switch (result) {
    case IGraphObjectDownloaderObserver::EAlbumDownloaded:
        if (downloader) {
            ActionAndData *actionData = downloader->GetGraphObjectData();
            if (ActionAndData::IsAlive(actionData)) {
                Post *post = static_cast<Post*>(actionData->mData);
                Album *album = static_cast<Album*>(downloader->GetGraphObject());
                post->SetAlbum(album);
                if (post->IsPostEditable()) {
                    elm_object_signal_emit(actionData->mHeader, "context.show", "context");
                }
                if (post->IsPhotoAddedToAlbum()) {
                    Evas_Object *title = elm_layout_content_get(actionData->mHeader, "user_name");
                    TO_STORY_TRNSLTD_TEXT_SET(title, eSTORY, *post);
                }
            }
            mObjectDownloaders = eina_list_remove(mObjectDownloaders, downloader);
            delete downloader;
        }
        break;
    default:
        break;
    }
}

void BaseProfileScreen::HideHeaderWidget() {
    Log::debug(LOG_FACEBOOK_USER,"BaseProfileScreen::HideHeaderWidget" );
    if (IsUserProfileWidgetShown()) {
        Log::debug(LOG_FACEBOOK_USER,"BaseProfileScreen::HideHeaderWidget->Unset profile layout and compensate height" );
        evas_object_hide(mHeaderWidget);
        elm_layout_content_unset(mProfileLayout, "static.data.area.swallow");
        Evas_Coord profileHeight = 0;
        evas_object_geometry_get(mHeaderWidget, nullptr, nullptr, nullptr, &profileHeight);
        ChangeYCoordinate(profileHeight,false);
        SetScrollerRegionTopOffset(0);
    }
}

void BaseProfileScreen::ShowHeaderWidget() {
    Log::debug(LOG_FACEBOOK_USER,"BaseProfileScreen::ShowHeaderWidget" );
    if (!IsUserProfileWidgetShown()) {
        Log::debug(LOG_FACEBOOK_USER,"BaseProfileScreen::ShowHeaderWidget->set hidden profile widget back" );
        elm_layout_content_set(mProfileLayout, "static.data.area.swallow",mHeaderWidget);
        evas_object_show(mHeaderWidget);
        Evas_Coord profileHeight = 0;
        evas_object_geometry_get(mHeaderWidget, nullptr, nullptr, nullptr, &profileHeight);
        ChangeYCoordinate(profileHeight,true);
        SetScrollerRegionTopOffset(R->USER_FEED_HEADER_REGION_H);
    }
}

bool BaseProfileScreen::IsUserProfileWidgetShown() {
    return elm_layout_content_get(mProfileLayout, "static.data.area.swallow");
}
