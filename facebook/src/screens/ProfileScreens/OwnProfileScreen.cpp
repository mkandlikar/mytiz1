#include "CameraRollScreen.h"
#include "Config.h"
#include "ConnectivityManager.h"
#include "ErrorHandling.h"
#include "Log.h"
#include "LoggedInUserData.h"
#include "OperationManager.h"
#include "OwnProfileScreen.h"
#include "Popup.h"
#include "PostComposerScreen.h"
#include "SelectedMediaList.h"
#include "UploadPhotoOperation.h"
#include "UserPhotosScreen.h"
#include "WebViewScreen.h"
#include "WidgetFactory.h"

OwnProfileScreen::OwnProfileScreen() : BaseProfileScreen(Config::GetInstance().GetUserId().c_str())
{
    mScreenId = SID_OWN_PROFILE;

    mTargetUploadType = UPLOAD_NONE;

    SetIdentifier("OWN PROFILE");

    if (LoggedInUserData::GetInstance().mName && LoggedInUserData::GetInstance().mUserProfileData) {
        mName = SAFE_STRDUP(LoggedInUserData::GetInstance().mName);
        mUserActionData = new ActionAndData(LoggedInUserData::GetInstance().mUserProfileData, mAction);
        WidgetFactory::RefreshBaseScreenLayout(mLayout, mName);
        CreateProfileWidget();
        ApplyProgressBar(true);
        SetShowConnectionErrorEnabled(true);
        mProfileWidgetExist = true;
        RequestData(true);
    } else {
        Log::info(LOG_FACEBOOK_USER, "OwnProfileScreen: no valid data. Downloading...");
        ProgressBarShow();
    }
    OperationManager::GetInstance()->Subscribe(this);

    AppEvents::Get().Subscribe(eUPDATED_COVER_PICTURE, this);
    AppEvents::Get().Subscribe(eUPDATED_PROFILE_PICTURE, this);
    AppEvents::Get().Subscribe(eDELETED_USER_PICTURE, this);
}

OwnProfileScreen::~OwnProfileScreen()
{
    OperationManager::GetInstance()->UnSubscribe(this);

    WidgetFactory::CloseAlertPopup();
    PopupMenu::Close();
}

void OwnProfileScreen::CreateProfileWidget()
{
    Log::info(LOG_FACEBOOK_USER, "OwnProfileScreen::CreateProfileWidget");

    BaseProfileScreen::CreateProfileWidget();
    elm_object_signal_emit(mCover, "show.edit", "btns");
}

void OwnProfileScreen::OnCoverClicked(Evas_Object *obj)
{
    assert(mUserActionData);
    assert(mUserActionData->mData);

    mTargetUploadType = UPLOAD_COVER;
    if (mUserActionData) {
        UserProfileData *data = static_cast<UserProfileData*>(mUserActionData->mData);
        if (data) {
            if (data->mCover && data->mCover->mSource) {
                CreatePhotoPopup(obj);
            } else {
                CreateNoPhotoPopup(obj);
            }
        }
    }
}

void OwnProfileScreen::OnAvatarClicked(Evas_Object *obj)
{
    assert(mUserActionData);
    assert(mUserActionData->mData);

    mTargetUploadType = UPLOAD_AVATAR;
    if (mUserActionData) {
        UserProfileData *data = static_cast<UserProfileData*>(mUserActionData->mData);
        if (data) {
            if (data->mPicture && data->mPicture->mUrl && !data->mPicture->mIsSilhouette) {
                CreatePhotoPopup(obj);
            } else {
                CreateNoPhotoPopup(obj);
            }
        }
    }
}

void OwnProfileScreen::CreatePhotoPopup( Evas_Object *obj ) {
    assert(mTargetUploadType != UPLOAD_NONE);
    static PopupMenu::PopupMenuItem context_menu_items[3] =
    {
        {
            NULL,
            "IDS_PROFILE_UPLOAD_PHOTO",
            NULL,
            UploadPhotoClicked,
            NULL,
            0,
            false,
            false
        },
        {
            NULL,
            "IDS_PROFILE_CHOOSE_PHOTO",
            NULL,
            ChooseFromPhotoClicked,
            NULL,
            1,
            false,
            false
        },
        {
            NULL,
            "IDS_PROFILE_VIEW_PHOTO",
            NULL,
            NULL,
            NULL,
            2,
            true,
            false
        },
    };

    context_menu_items[0].data = this;
    context_menu_items[1].data = this;
    context_menu_items[2].data = mUserActionData;

    context_menu_items[2].callback = (mTargetUploadType == UPLOAD_COVER) ? ViewCover : ViewAvatar;

    Evas_Coord y = 0, h = 0;
    evas_object_geometry_get(obj, NULL, &y, NULL, &h);
    y += h / 2;

    PopupMenu::Show(3, context_menu_items, getParentMainLayout(), false, y);
}

void OwnProfileScreen::CreateNoPhotoPopup( Evas_Object *obj )
{
    static PopupMenu::PopupMenuItem context_menu_items[2] =
    {
        {
            NULL,
            "IDS_PROFILE_UPLOAD_PHOTO",
            NULL,
            UploadPhotoClicked,
            NULL,
            0,
            false,
            false
        },
        {
            NULL,
            "IDS_PROFILE_CHOOSE_PHOTO",
            NULL,
            ChooseFromPhotoClicked,
            NULL,
            1,
            true,
            false
        },
    };

    context_menu_items[0].data = this;
    context_menu_items[1].data = this;

    Evas_Coord y = 0, h = 0;
    evas_object_geometry_get(obj, NULL, &y, NULL, &h);
    y += h / 2;

    PopupMenu::Show(2, context_menu_items, getParentMainLayout(), false, y);
}

void OwnProfileScreen::UploadPhotoClicked(void *user_data, Evas_Object *obj, void *event_info)
{
    OwnProfileScreen *me = static_cast<OwnProfileScreen*>(user_data);
    assert(me);
    ScreenBase *camRollScrn = new CameraRollScreen(eCAMERA_ROLL_COMMENT_MODE, me);
    Application::GetInstance()->AddScreen(camRollScrn);
}

void OwnProfileScreen::ChooseFromPhotoClicked(void *user_data, Evas_Object *obj, void *event_info)
{
    OwnProfileScreen *me = static_cast<OwnProfileScreen*>(user_data);
    assert(me);
    UserPhotosScreen *photosScreen =
        new UserPhotosScreen(NULL, me->mId.c_str(),
                             i18n_get_text(me->mTargetUploadType == UPLOAD_COVER ?
                                           "IDS_PROFILE_ME_CHANGECOVER_LABEL" :
                                           "IDS_PROFILE_ME_EDITPROFPIC_LABEL"),
                                           me->mTargetUploadType, me);
    Application::GetInstance()->AddScreen(photosScreen);
}

bool OwnProfileScreen::HandleBackButton()
{
    if (IsLaunchedByAppControl()) {
        evas_object_hide(getParentMainLayout());
    }
    bool handled = true;
    if (WidgetFactory::CloseAlertPopup()) {
    } else if (PopupMenu::Close()) {
    } else {
        handled = false;
    }
    return handled;
}

void OwnProfileScreen::UpdateMedia() {
    Log::info(LOG_FACEBOOK_USER, "OwnProfileScreen::UpdateMedia");
    Eina_List *mediaList = SelectedMediaList::Get();
    if (eina_list_count(mediaList)) {
        PostComposerMediaData * mediaData = static_cast<PostComposerMediaData *>(eina_list_data_get(mediaList));
        if (mediaData->GetFilePath()) {
            if (!ConnectivityManager::Singleton().IsConnected()) {
                ShowNotification(ENetworkError);
            } else {
                if (mTargetUploadType == UPLOAD_NONE) {
                    ShowNotification(EUploadError);
                } else {
                    ImageFile *imageToUpload = new ImageFile (nullptr, mediaData->GetFilePath());
                    OperationManager::GetInstance()->AddOperation(MakeSptr<UploadPhotoOperation>
                                                                  (mTargetUploadType == UPLOAD_COVER ?
                                                                   UploadPhotoOperation::ECoverPhoto : UploadPhotoOperation::EUserPhoto,
                                                                  *imageToUpload, true));
                    imageToUpload->Release();
                }
            }
        } else {
            ShowNotification(EUploadError);
            Log::error(LOG_FACEBOOK_USER, "The file path is not valid");
        }
        SelectedMediaList::Clear();
    } else {
        ShowNotification(EUploadError);
        Log::error(LOG_FACEBOOK_USER, "MediaList is empty");
    }
}

void OwnProfileScreen::OnPostTextClicked()
{
    PostComposerScreen *screen = new PostComposerScreen(this, eDEFAULT, nullptr, nullptr, nullptr, nullptr, false);
    Application::GetInstance()->AddScreen(screen);
}

void OwnProfileScreen::OnPostPhotoClicked()
{
    if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_CAMERA_ROLL) {
        CameraRollScreen *screen = new CameraRollScreen(eCAMERA_ROLL_CREATE_PHOTO_POST_MODE);
        Application::GetInstance()->AddScreen(screen);
    }
}

Evas_Object *OwnProfileScreen::CreateActionBtns(Evas_Object *parent, ActionAndData *actionData)
{
    Evas_Object *layout = WidgetFactory::CreateLayoutByGroup(parent, "profile_own_action_btns");

    elm_object_translatable_part_text_set(layout, "btn_one_text", "IDS_PROFILE_ME_UPDATEINFO");

    elm_object_signal_callback_add(layout, "mouse,clicked,*", "btn_one*", on_update_profile_clicked, this);
    elm_object_signal_callback_add(layout, "mouse,clicked,*", "btn_two*", on_more_profile_clicked, this);

    elm_box_pack_end(parent, layout);
    evas_object_show(layout);

    return layout;
}

void OwnProfileScreen::on_update_profile_clicked(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    WebViewScreen::Launch(REQ_UPDATE_INFO_URI, true);
}

void OwnProfileScreen::on_more_profile_clicked(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    if (!ConnectivityManager::Singleton().IsConnected()) {
        notification_status_message_post(i18n_get_text("IDS_GEN_NETWORK_ERROR"));
        return;
    }

    OwnProfileScreen *me = static_cast<OwnProfileScreen*>(user_data);
    assert(me);

    const int menuItemsNumber = 3;
    static PopupMenu::PopupMenuItem more_menu_items[menuItemsNumber] =
    {
        {
            ICON_DIR "/profiles/pm_edit_profile_pic.png",
            "IDS_PROFILE_ME_EDITPROFPIC_LABEL",
            NULL,
            change_avatar_from_more_popup_cb,
            NULL,
            0,
            false,
            false
        },
        {
            ICON_DIR "/profiles/pm_change_cover.png",
            "IDS_PROFILE_ME_CHANGECOVER_LABEL",
            NULL,
            change_cover_from_more_popup_cb,
            NULL,
            1,
            false,
            false
        },
        {
            ICON_DIR "/profiles/pm_privacy_shortcuts.png",
            "IDS_PROFILE_ME_VIEWPRIVACYSHTCUT_LABEL",
            NULL,
            on_edit_privacy_clicked,
            NULL,
            2,
            true,
            false
        }
    };

    more_menu_items[0].data = me;
    more_menu_items[1].data = me;
    more_menu_items[2].data = me;

    Evas_Coord y = ScreenBase::R->STATUS_BAR_SIZE_H + ScreenBase::R->ACTION_BAR_SIZE_H;
    PopupMenu::Show(menuItemsNumber, more_menu_items, me->getParentMainLayout(), false, y);
}

void OwnProfileScreen::change_cover_from_more_popup_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    OwnProfileScreen *me = static_cast<OwnProfileScreen*>(user_data);
    assert(me);
    me->mTargetUploadType = UPLOAD_COVER;
    me->ChooseFromPhotoClicked(me);
}

void OwnProfileScreen::change_avatar_from_more_popup_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    OwnProfileScreen *me = static_cast<OwnProfileScreen*>(user_data);
    assert(me);
    me->mTargetUploadType = UPLOAD_AVATAR;
    me->ChooseFromPhotoClicked(me);
}

void OwnProfileScreen::on_edit_privacy_clicked(void *data, Evas_Object *obj, void *event_info)
{
    PopupMenu::Close();
    WebViewScreen::Launch(URL_PRIVACY_SHORTCUTS, true);
}

void OwnProfileScreen::Update(AppEventId eventId, void * data)
{
    switch (eventId) {
    case eOPERATION_ADDED_EVENT:
    {
        Sptr<Operation> oper(static_cast<Operation*>(data));
        if (oper && oper->GetType() == Operation::OT_Upload_Profile_Photo) {
            if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_CAMERA_ROLL ) {
                //for Camera_Roll screen (via "Upload photo" menu item) profile becomes top automatically
                Application::GetInstance()->MakeThisScreenTop(this);
            }
        }
    }
        break;
    case eUPDATED_COVER_PICTURE:
    case eUPDATED_PROFILE_PICTURE:
    case eDELETED_USER_PICTURE:
        Log::info(LOG_FACEBOOK_USER, "BaseProfileScreen::Update - UPDATED/DELETED COVER/PROFILE PICTURE");
        if (data) { // means eUPDATED_COVER_PICTURE or eUPDATED_PROFILE_PICTURE, but not eDELETED_USER_PICTURE.
            char *path = static_cast<char *> (data);
            const char *part = (eUPDATED_COVER_PICTURE == eventId) ? "cover_photo" : (eUPDATED_PROFILE_PICTURE == eventId) ? "avatar" : nullptr;
            if (part) {
                Utils::SetImageToEvasAndFill(mCover, part, path);
            }
        }
        // ToDo: Unfortunately, we need to keep this call below, because it updates some internal structures (e.g. mProfileDataUploader->mUserData). This causes extra requests to server etc. MUST BE REDESIGNED.
        UpdateProfileData();
        break;
    default:
        BaseProfileScreen::Update(eventId, data);
        break;
    }
}
