#include "ConnectivityManager.h"
#include "ErrorHandling.h"
#include "FriendRequestsBatchProvider.h"
#include "FriendsAction.h"
#include "FriendOperation.h"
#include "Log.h"
#include "Messenger.h"
#include "OperationManager.h"
#include "OwnFriendsProvider.h"
#include "Popup.h"
#include "PostComposerScreen.h"
#include "ProfileScreen.h"
#include "RequestCompleteEvent.h"
#include "UserPhotosScreen.h"
#include "WebViewScreen.h"
#include "WidgetFactory.h"

ProfileScreen::ProfileScreen(const char *id) : BaseProfileScreen(id)
{
    mScreenId = SID_USER_PROFILE;
    mProfileBtns = nullptr;
    Log::info(LOG_FACEBOOK_USER, "ProfileScreen: no valid data. Downloading...");
    mFollowUnfollowRequest = nullptr;
    mPokeRequest = nullptr;
    mConfirmationDialog = nullptr;
    ProgressBarShow();
    OperationManager::GetInstance()->Subscribe(this);
    AppEvents::Get().Subscribe(eUNFRIEND_FRIEND_ERROR, this);
    AppEvents::Get().Subscribe(eBLOCK_FRIEND_ERROR, this);
    AppEvents::Get().Subscribe(eREMOVE_TOP_BLOCKED_SCREEN, this);
    AppEvents::Get().Subscribe(eFRIEND_REQUEST_CONFIRMED, this);
    AppEvents::Get().Subscribe(eFRIEND_REQUEST_CONFIRM_DELETE_ERROR, this);
    AppEvents::Get().Subscribe(eUPDATE_UNFRIENDED_USER_STATUS, this);
    AppEvents::Get().Subscribe(eADD_FRIEND_BTN_CLICKED, this);
    AppEvents::Get().Subscribe(eCANCEL_FRIEND_REQUEST_BTN_CLICKED, this);
    AppEvents::Get().Subscribe(eDELETE_FRIEND_REQUEST_BTN_CLICKED, this);
    SetIdentifier("USER PROFILE SCREEN");
}

ProfileScreen::~ProfileScreen()
{
    FacebookSession::GetInstance()->ReleaseGraphRequest(mFollowUnfollowRequest);
    FacebookSession::GetInstance()->ReleaseGraphRequest(mPokeRequest);
    PopupMenu::Close();
    DeleteConfirmationDialog();
    OperationManager::GetInstance()->UnSubscribe(this);
}

bool ProfileScreen::HandleBackButton()
{
    bool ret = true;

    if (WidgetFactory::CloseConfirmDialogue()) {
    } else if (WidgetFactory::CloseAlertPopup()) {
    } else if (WidgetFactory::CloseErrorDialogue()) {
    } else if (PopupMenu::Close()) {
    } else if (DeleteConfirmationDialog()) {
    } else {
        ret = false;
    }

    return ret;
}

void ProfileScreen::OnCoverClicked(Evas_Object *obj)
{
    assert(mUserActionData);
    assert(mUserActionData->mData);
    UserProfileData *data = static_cast<UserProfileData*>(mUserActionData->mData);

    Log::info(LOG_FACEBOOK_USER, "ProfileScreen::OnCoverClicked");
    if (data->mCover && data->mCover->mSource) {
        ViewCover(mUserActionData);
    } else {
        Log::info(LOG_FACEBOOK_USER, "ProfileScreen::OnCoverClicked: No user cover");
    }
}

void ProfileScreen::OnAvatarClicked(Evas_Object *obj)
{
    assert(mUserActionData);
    assert(mUserActionData->mData);
    UserProfileData *data = static_cast<UserProfileData*>(mUserActionData->mData);

    Log::info(LOG_FACEBOOK_USER, "ProfileScreen::OnAvatarClicked");
    if (data->mPicture && data->mPicture->mUrl) {
        ViewAvatar(mUserActionData);
    } else {
        Log::info(LOG_FACEBOOK_USER, "ProfileScreen::OnCoverClicked: No user avatar");
    }
}

void ProfileScreen::OnPostTextClicked()
{
    PostComposerScreen *screen = new PostComposerScreen(this, eFRIENDTIMELINE, mId.c_str(), mName, nullptr);
    Application::GetInstance()->AddScreen(screen);
}

void ProfileScreen::OnPostPhotoClicked()
{
    if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_CAMERA_ROLL) {
        CameraRollScreen *screen = new CameraRollScreen(eCAMERA_ROLL_POST_TO_FRIEND_TIMELINE_MODE, mId.c_str(), mName);
        Application::GetInstance()->AddScreen(screen);
    }
}

Evas_Object *ProfileScreen::CreateActionBtns(Evas_Object *parent, ActionAndData *actionData)
{
    Evas_Object *layout = WidgetFactory::CreateLayoutByGroup(parent, "profile_action_btns");
    elm_box_pack_end(parent, layout);
    evas_object_show(layout);
    return layout;
}

void ProfileScreen::RefreshActionBtns()
{
    assert(mUserActionData);
    assert(mUserActionData->mData);
    assert(mProfileBtns);

    Evas_Object *newContent = nullptr;
    UserProfileData *userData = static_cast<UserProfileData*>(mUserActionData->mData);
    if (userData->mFriendshipStatus == Person::eARE_FRIENDS || userData->mSubscribeStatus == UserProfileData::IS_SUBSCRIBED) {
        newContent = CreateFourButtons();
    } else if (userData->mFriendshipStatus == Person::eCAN_REQUEST && userData->mSubscribeStatus == UserProfileData::CAN_SUBSCRIBE) {
        newContent = CreateFourButtons();
    } else if ((userData->mFriendshipStatus == Person::eINCOMING_REQUEST || userData->mFriendshipStatus == Person::eOUTGOING_REQUEST) && userData->mSubscribeStatus == UserProfileData::CAN_SUBSCRIBE) {
        newContent = CreateFourButtons();
    } else if ((userData->mFriendshipStatus == Person::eCAN_REQUEST || userData->mFriendshipStatus == Person::eOUTGOING_REQUEST || userData->mFriendshipStatus == Person::eINCOMING_REQUEST) && userData->mSubscribeStatus == UserProfileData::CANNOT_SUBSCRIBE) {
        newContent = CreateThreeButtons();
    } else {
        newContent = CreateTwoButtons();
    }
    if (newContent) {
        elm_object_part_content_set(mProfileBtns, "content", newContent);
    }
}

Evas_Object *ProfileScreen::CreateTwoButtons()
{
    Evas_Object *layout = WidgetFactory::CreateLayoutByGroup(mProfileBtns, "profile_2_action_btns");

    elm_object_translatable_part_text_set(layout, "btn_one_text", "IDS_PROFILE_FRIEND_MESSAGE");

    elm_object_signal_callback_add(layout, "mouse,clicked,*", "btn_one*", on_message_btn_clicked, this);
    elm_object_signal_callback_add(layout, "mouse,clicked,*", "btn_two*", on_more_btn_clicked, this);

    return layout;
}

Evas_Object *ProfileScreen::CreateThreeButtons()
{
    assert(mUserActionData);
    assert(mUserActionData->mData);
    assert(mProfileBtns);

    Evas_Object *layout = nullptr;
    UserProfileData *userData = static_cast<UserProfileData*>(mUserActionData->mData);
    layout = WidgetFactory::CreateLayoutByGroup(mProfileBtns, "profile_3_action_btns");

    if (userData->mFriendshipStatus == Person::eCAN_REQUEST) {
        elm_object_translatable_part_text_set(layout, "btn_one_text", "IDS_PROFILE_OTHER_ADDFRIEND");
        elm_object_signal_emit(layout, Application::IsRTLLanguage() ? "add.friend.rtl" : "add.friend.ltr", "btn.one");
    } else if (userData->mFriendshipStatus == Person::eOUTGOING_REQUEST) {
        elm_object_translatable_part_text_set(layout, "btn_one_text", "IDS_CANCEL_REQUEST");
        elm_object_signal_emit(layout, Application::IsRTLLanguage() ? "cancel.request.rtl" : "cancel.request.ltr", "btn.one");
    } else if (userData->mFriendshipStatus == Person::eINCOMING_REQUEST) {
        elm_object_translatable_part_text_set(layout, "btn_one_text", "IDS_RESPOND");
        elm_object_signal_emit(layout, Application::IsRTLLanguage() ? "friend.respond.rtl" : "friend.respond.ltr", "btn.one");
    } else {
        elm_object_translatable_part_text_set(layout, "btn_one_text", "IDS_PROFILE_OTHER_ADDFRIEND");
        elm_object_signal_emit(layout, "lock", "btn.one");
    }

    elm_object_translatable_part_text_set(layout, "btn_two_text", "IDS_PROFILE_FRIEND_MESSAGE");

    if (userData->mFriendshipStatus == Person::eINCOMING_REQUEST) {
        elm_object_signal_callback_add(layout, "mouse,clicked,*", "btn_one*", on_friend_btn_clicked, this);
    } else {
        elm_object_signal_callback_add(layout, "mouse,clicked,*", "btn_one*", on_add_cancel_friend_btn_clicked, this);
    }

    elm_object_signal_callback_add(layout, "mouse,clicked,*", "btn_two*", on_message_btn_clicked, nullptr);
    elm_object_signal_callback_add(layout, "mouse,clicked,*", "btn_more*", on_more_btn_clicked, this);

    return layout;
}

Evas_Object *ProfileScreen::CreateFourButtons()
{
    assert(mUserActionData);
    assert(mUserActionData->mData);
    assert(mProfileBtns);
    UserProfileData *userData = static_cast<UserProfileData*>(mUserActionData->mData);

    Evas_Object *layout = nullptr;
    layout = WidgetFactory::CreateLayoutByGroup(mProfileBtns, "profile_4_action_btns");

    if (userData->mFriendshipStatus == Person::eARE_FRIENDS) {
        elm_object_translatable_part_text_set(layout, "btn_one_text", "IDS_UP_FRIENDS");
        elm_object_signal_emit(layout, Application::IsRTLLanguage() ? "friended.rtl" : "friended.ltr", "btn.one");
    } else if (userData->mFriendshipStatus == Person::eOUTGOING_REQUEST) {
        elm_object_translatable_part_text_set(layout, "btn_one_text", "IDS_CANCEL_REQUEST");
        elm_object_signal_emit(layout, Application::IsRTLLanguage() ? "cancel.request.rtl" : "cancel.request.ltr", "btn.one");
    } else if (userData->mFriendshipStatus == Person::eCAN_REQUEST) {
        elm_object_translatable_part_text_set(layout, "btn_one_text", "IDS_PROFILE_OTHER_ADDFRIEND");
        elm_object_signal_emit(layout, Application::IsRTLLanguage() ? "add.friend.rtl" : "add.friend.ltr", "btn.one");
    } else if (userData->mFriendshipStatus == Person::eINCOMING_REQUEST) {
        elm_object_translatable_part_text_set(layout, "btn_one_text", "IDS_RESPOND");
        elm_object_signal_emit(layout, Application::IsRTLLanguage() ? "friend.respond.rtl" : "friend.respond.ltr", "btn.one");
    } else {
        elm_object_translatable_part_text_set(layout, "btn_one_text", "IDS_PROFILE_OTHER_ADDFRIEND");
        elm_object_signal_emit(layout, "lock", "btn.one");
    }

    if (userData->mSubscribeStatus == UserProfileData::CAN_SUBSCRIBE) {
        elm_object_translatable_part_text_set(layout, "btn_two_text", "IDS_PROFILE_FRIEND_FOLLOW");
        elm_object_signal_emit(layout, "follow", "btn.two");
    } else if (userData->mSubscribeStatus == UserProfileData::IS_SUBSCRIBED) {
        elm_object_translatable_part_text_set(layout, "btn_two_text", "IDS_PROFILE_FRIEND_FOLLOWED");
        elm_object_signal_emit(layout, "followed", "btn.two");
    } else {
        elm_object_translatable_part_text_set(layout, "btn_two_text", "IDS_PROFILE_FRIEND_FOLLOW");
        elm_object_signal_emit(layout, "lock", "btn.two");
    }

    elm_object_translatable_part_text_set(layout, "btn_three_text", "IDS_PROFILE_FRIEND_MESSAGE");

    elm_object_signal_callback_add(layout, "mouse,clicked,*", "btn_one*", on_friend_btn_clicked, this);
    elm_object_signal_callback_add(layout, "mouse,clicked,*", "btn_two*", on_follow_btn_clicked, this);
    elm_object_signal_callback_add(layout, "mouse,clicked,*", "btn_three*", on_message_btn_clicked, nullptr);
    elm_object_signal_callback_add(layout, "mouse,clicked,*", "btn_more*", on_more_btn_clicked, this);

    return layout;
}

void ProfileScreen::on_friend_btn_clicked(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    assert(data);
    ProfileScreen *me = static_cast<ProfileScreen*>(data);
    assert(me->mUserActionData);
    assert(me->mUserActionData->mData);
    UserProfileData *userData = static_cast<UserProfileData*>(me->mUserActionData->mData);

    Log::info(LOG_FACEBOOK_USER, "ProfileScreen::on_friend_btn_clicked");
    if (userData->mFriendshipStatus == Person::eOUTGOING_REQUEST || userData->mFriendshipStatus == Person::eCAN_REQUEST) {
        me->AddCancelFriendClicked();
    } else if (userData->mFriendshipStatus == Person::eINCOMING_REQUEST) {
        me->RespondClicked(obj);
    } else if (userData->mFriendshipStatus == Person::eARE_FRIENDS) {
        me->FriendClicked(obj);
    } else {
        Log::error(LOG_FACEBOOK_USER, "ProfileScreen::on_friend_btn_clicked - error with handling action");
    }
}

void ProfileScreen::FriendClicked(Evas_Object *obj)
{
    assert(mUserActionData);
    assert(mUserActionData->mData);

    UserProfileData *userData = static_cast<UserProfileData*>(mUserActionData->mData);
    static PopupMenu::PopupMenuItem context_menu_items[] = {
        { nullptr, nullptr, nullptr, on_popup_follow_btn_clicked, nullptr, 0, false, false },
        { nullptr, "IDS_UNFRIEND", nullptr, on_popup_unfriend_btn_clicked, nullptr, 1, true, false },
    };

    static const char *itemText_unfollow = "IDS_UNFOLLOW";
    static const char *itemText_follow = "IDS_FOLLOW";
    if (userData->mSubscribeStatus == UserProfileData::IS_SUBSCRIBED) {
        context_menu_items[0].itemText = itemText_unfollow;
    } else if (userData->mSubscribeStatus == UserProfileData::CAN_SUBSCRIBE) {
        context_menu_items[0].itemText = itemText_follow;
    }

    context_menu_items[0].data = this;
    context_menu_items[1].data = this;

    Evas_Coord y = 0, h = 0;
    evas_object_geometry_get(obj, nullptr, &y, nullptr, &h);
    y += h / 2;
    PopupMenu::Show(2, context_menu_items, getParentMainLayout(), false, y);
}

void ProfileScreen::RespondClicked(Evas_Object *obj)
{
    assert(mUserActionData);
    assert(mUserActionData->mData);

    static PopupMenu::PopupMenuItem context_menu_items[] = {
        { nullptr, "IDS_CONFIRM_FRIEND_REQUEST", nullptr,
          on_confirm_friend_request_btn_clicked, nullptr,
          0, false, false
        },
        { nullptr, "IDS_DELETE_FRIEND_REQUEST", nullptr,
          on_delete_friend_request_btn_clicked, nullptr,
          1, true, false
        },
    };
    context_menu_items[0].data = this;
    context_menu_items[1].data = this;

    Evas_Coord y = 0, h = 0;
    evas_object_geometry_get(obj, nullptr, &y, nullptr, &h);
    y += h / 2;
    PopupMenu::Show(2, context_menu_items, getParentMainLayout(), false, y);
}

void ProfileScreen::on_confirm_friend_request_btn_clicked(void *data, Evas_Object *obj, void *event_info)
{
    assert(data);
    ProfileScreen *me = static_cast<ProfileScreen*>(data);
    Log::info(LOG_FACEBOOK_USER, "ProfileScreen::on_confirm_friend_request_btn_clicked");
    me->ConfirmFriendRequest();
}

void ProfileScreen::on_delete_friend_request_btn_clicked(void *data, Evas_Object *obj, void *event_info)
{
    assert(data);
    ProfileScreen *me = static_cast<ProfileScreen*>(data);
    Log::info(LOG_FACEBOOK_USER, "ProfileScreen::on_delete_friend_request_btn_clicked");
    me->DeleteFriendRequest();
}

void ProfileScreen::ConfirmFriendRequest()
{
    assert(mUserActionData);
    assert(mUserActionData->mData);
    UserProfileData *data = static_cast<UserProfileData*>(mUserActionData->mData);

    Log::info(LOG_FACEBOOK_USER, "ProfileScreen::ConfirmFriendRequest");
    if (ConnectivityManager::Singleton().IsConnected()) {
        data->mFriendshipStatus = Person::eARE_FRIENDS;
        FriendRequestsBatchProvider::GetInstance()->FriendConfirmedSuccessSet(mId.c_str());
        AppEvents::Get().Notify(eCONFIRM_FRIEND_REQUEST_BTN_CLICKED, const_cast<char*>(mId.c_str()));
        RefreshActionBtns();
        OperationManager::GetInstance()->AddOperation(MakeSptr<FriendOperation>(Operation::OT_ConfirmFriend, mId.c_str()));
    } else {
        ShowNotification(ENetworkError);
    }
}

void ProfileScreen::DeleteFriendRequest()
{
    assert(mUserActionData);
    assert(mUserActionData->mData);
    UserProfileData *data = static_cast<UserProfileData*>(mUserActionData->mData);

    Log::info(LOG_FACEBOOK_USER, "ProfileScreen::DeleteFriendRequest");
    if (ConnectivityManager::Singleton().IsConnected()) {
        data->mFriendshipStatus = Person::eCAN_REQUEST;
        RefreshActionBtns();
        FriendRequestsBatchProvider::GetInstance()->FriendDeletedSuccessSet(mId.c_str());
        AppEvents::Get().Notify(eDELETE_FRIEND_REQUEST_BTN_CLICKED, const_cast<char*>(mId.c_str()));
        OperationManager::GetInstance()->AddOperation(MakeSptr<FriendOperation>(Operation::OT_NotConfirmFriend, mId.c_str()));
    } else {
        ShowNotification(ENetworkError);
    }
}

void ProfileScreen::on_add_cancel_friend_btn_clicked(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    assert(data);
    ProfileScreen *me = static_cast<ProfileScreen*>(data);
    Log::info(LOG_FACEBOOK_USER, "ProfileScreen::on_add_cancel_friend_btn_clicked");
    me->AddCancelFriendClicked();
}

void ProfileScreen::AddCancelFriendClicked()
{
    if (ConnectivityManager::Singleton().IsConnected()) {
        assert(mUserActionData);
        UserProfileData *data = dynamic_cast<UserProfileData*>(mUserActionData->mData);
        if(!data || !data->GetTracker()) {
            Log::error("ProfileScreen::AddCancelFriendClicked->Wrong data type");
            return;
        }

        if (data->mFriendshipStatus == Person::eCAN_REQUEST) {
            data->mFriendshipStatus = Person::eOUTGOING_REQUEST;
            if (data->mSubscribeStatus != UserProfileData::CANNOT_SUBSCRIBE) {
                data->mSubscribeStatus = UserProfileData::IS_SUBSCRIBED;
            }
            FriendRequestsBatchProvider::GetInstance()->RequestSendSuccessSet(mId.c_str());
            AppEvents::Get().Notify(eADD_FRIEND_BTN_CLICKED, const_cast<char*>(mId.c_str()));
        } else if (data->mFriendshipStatus == Person::eOUTGOING_REQUEST) {
            data->mFriendshipStatus = Person::eCAN_REQUEST;
            if (data->mSubscribeStatus != UserProfileData::CANNOT_SUBSCRIBE) {
                data->mSubscribeStatus = UserProfileData::CAN_SUBSCRIBE;
            }
            FriendRequestsBatchProvider::GetInstance()->RequestCancelSuccessSet(mId.c_str());
            AppEvents::Get().Notify(eCANCEL_FRIEND_REQUEST_BTN_CLICKED, const_cast<char*>(mId.c_str()));
        }
        RefreshActionBtns();
        data->GetTracker()->RestartRequestTimer();
    } else {
        ShowNotification(ENetworkError);
    }
}

void ProfileScreen::on_popup_unfriend_btn_clicked(void *user_data, Evas_Object *obj, void *event_info)
{
    assert(user_data);
    ProfileScreen *me = static_cast<ProfileScreen*>(user_data);
    Log::info(LOG_FACEBOOK_USER, "ProfileScreen::on_popup_unfriend_btn_clicked");
    PopupMenu::Close();
    me->UnfriendClicked();
}

void ProfileScreen::UnfriendClicked() {
    char * message = WidgetFactory::WrapByFormat(i18n_get_text("IDS_UNFRIEND_CD_MESSAGE"), mName);
    char * title = WidgetFactory::WrapByFormat(i18n_get_text("IDS_UNFRIEND_CD_TITLE"), mName);
    mConfirmationDialog = ConfirmationDialog::CreateAndShow(mLayout,
                                                            title,
                                                            message,
                                                            "IDS_CANCEL",
                                                            "IDS_CONFIRM",
                                                            confirmation_dialog_cb_unfriend,
                                                            this);
    delete [] message;
    delete [] title;
}

void ProfileScreen::DoUnfriend()
{
    assert(mUserActionData);
    assert(mUserActionData->mData);

    Log::info(LOG_FACEBOOK_USER, "ProfileScreen::DoUnfriend");
    UserProfileData *data = static_cast<UserProfileData*>(mUserActionData->mData);
    if (ConnectivityManager::Singleton().IsConnected()) {
        if (data->mFriendshipStatus == Person::eARE_FRIENDS) {
            data->mFriendshipStatus = Person::eCAN_REQUEST;
            data->mSubscribeStatus = UserProfileData::CAN_SUBSCRIBE;
            OperationManager::GetInstance()->AddOperation(MakeSptr<FriendOperation>(Operation::OT_Unfriend, data->GetId()));
        }
        RefreshActionBtns();
    } else {
        ShowNotification(ENetworkError);
    }
}

void ProfileScreen::OnUnfriendError()
{
    Log::info(LOG_FACEBOOK_USER, "ProfileScreen::OnUnfriendError");
    if (mUserActionData && mUserActionData->mData) {
        UserProfileData *userData = static_cast<UserProfileData*>(mUserActionData->mData);
        if (userData->mFriendshipStatus == Person::eCAN_REQUEST) {
            userData->mFriendshipStatus = Person::eARE_FRIENDS;
        } else {
            Log::error(LOG_FACEBOOK_USER, "ProfileScreen::OnUnfriendError->Unexpected result but restore to friend");
        }
        RefreshActionBtns();
    }
}

void ProfileScreen::on_follow_btn_clicked(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    assert(data);
    ProfileScreen *me = static_cast<ProfileScreen*>(data);
    Log::info(LOG_FACEBOOK_USER, "ProfileScreen::on_follow_clicked");
    me->FollowClicked();
}

void ProfileScreen::on_popup_follow_btn_clicked(void *user_data, Evas_Object *obj, void *event_info)
{
    assert(user_data);
    ProfileScreen *me = static_cast<ProfileScreen*>(user_data);
    Log::info(LOG_FACEBOOK_USER, "ProfileScreen::on_popup_follow_btn_clicked");
    PopupMenu::Close();
    me->FollowClicked();
}

void ProfileScreen::FollowClicked()
{
    assert(mUserActionData);
    assert(mUserActionData->mData);
    UserProfileData *data = static_cast<UserProfileData*>(mUserActionData->mData);
    if (ConnectivityManager::Singleton().IsConnected()) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(mFollowUnfollowRequest);
        if (data->mSubscribeStatus == UserProfileData::CAN_SUBSCRIBE) {
            mFollowUnfollowRequest = FacebookSession::GetInstance()->FollowUser(data->GetId(), on_follow_completed, this);
            data->mSubscribeStatus = UserProfileData::IS_SUBSCRIBED;
        } else if (data->mSubscribeStatus == UserProfileData::IS_SUBSCRIBED) {
            mFollowUnfollowRequest = FacebookSession::GetInstance()->UnFollowUser(data->GetId(), on_follow_completed, this);
            data->mSubscribeStatus = UserProfileData::CAN_SUBSCRIBE;
        }
        RefreshActionBtns();
    } else {
        ShowNotification(ENetworkError);
    }
}

void ProfileScreen::on_follow_completed(void* object, char* respond, int code)
{
    assert(object);
    ProfileScreen *me = static_cast<ProfileScreen*>(object);
    assert(me->mUserActionData);

    Log::info(LOG_FACEBOOK_USER, "ProfileScreen::on_follow_completed");
    FacebookSession::GetInstance()->ReleaseGraphRequest(me->mFollowUnfollowRequest);
    if (code == CURLE_OK) {
        Log::debug(LOG_FACEBOOK_USER, "response: %s", respond);
        FbRespondBase *followRespond = FbRespondBase::createFromJson(respond);
        if (followRespond) {
            if (followRespond->mError) {
                Log::error(LOG_FACEBOOK_USER, "Error with follow/unfollow request, error = %s", followRespond->mError->mMessage);
                assert(me->mUserActionData->mData);
                UserProfileData *userData = static_cast<UserProfileData*>(me->mUserActionData->mData);
                if (userData->mSubscribeStatus == UserProfileData::CAN_SUBSCRIBE) {
                    userData->mSubscribeStatus = UserProfileData::IS_SUBSCRIBED;
                } else if (userData->mSubscribeStatus == UserProfileData::IS_SUBSCRIBED) {
                    userData->mSubscribeStatus = UserProfileData::CAN_SUBSCRIBE;
                } else {
                    Log::error(LOG_FACEBOOK_USER, "Unexpected result");
                }
                me->RefreshActionBtns();
            } else {
                //
            }
            delete followRespond;
        }
    } else {
        Log::error(LOG_FACEBOOK_USER, "Error sending http request");
        me->ShowNotification(ENetworkError);
    }
    free(respond);
}

void ProfileScreen::on_message_btn_clicked(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    if (Messenger::IsMessengerInstalled()) {
        Log::info(LOG_FACEBOOK_USER, "Launching the FB Messenger...");
        Messenger::LaunchMessenger();
    } else {
        Log::info(LOG_FACEBOOK_USER, "FB Messenger is not installed on your device, get it from Tizen store");
        Messenger *newMessengerScreen = new Messenger(nullptr);
        Application::GetInstance()->AddScreen(newMessengerScreen);
    }
}

void ProfileScreen::on_more_btn_clicked(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    ProfileScreen *me = static_cast<ProfileScreen*>(data);
    assert(me);
    Log::info(LOG_FACEBOOK_USER, "ProfileScreen::on_more_btn_clicked");
    me->OnMoreClicked(obj);
}

void ProfileScreen::OnMoreClicked(Evas_Object *obj)
{
    static PopupMenu::PopupMenuItem context_menu_items[] = {
        { nullptr, "IDS_PROFILE_POKE", nullptr,
          on_poke_btn_clicked, nullptr,
          0, false, false
        },
        { nullptr, "IDS_BLOCK", nullptr,
          on_block_btn_clicked, nullptr,
          1, true, false
        },
    };
    context_menu_items[0].data = this;
    context_menu_items[1].data = this;

    Evas_Coord y = 0, h = 0;
    evas_object_geometry_get(obj, nullptr, &y, nullptr, &h);
    y += h / 2;
    PopupMenu::Show(2, context_menu_items, getParentMainLayout(), false, y);
}

void ProfileScreen::BlockUser() {
    if (ConnectivityManager::Singleton().IsConnected()) {
        OperationManager::GetInstance()->AddOperation(MakeSptr<FriendOperation>(Operation::OT_Block, mId.c_str()));

        Evas_Object *layout = elm_layout_add(mLayout);
        elm_layout_file_set(layout, Application::mEdjPath, "block_screen");
        elm_object_part_content_set(mLayout, "lock_screen", layout);

        Evas_Object *progress_bar = elm_progressbar_add(layout);
        elm_object_part_content_set(layout, "progressbar", progress_bar);
        elm_object_style_set(progress_bar, "process_medium");
        evas_object_size_hint_align_set(progress_bar, EVAS_HINT_FILL, EVAS_HINT_FILL);
        evas_object_size_hint_weight_set(progress_bar, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
        elm_progressbar_pulse_set(progress_bar, EINA_TRUE);
        elm_progressbar_pulse(progress_bar, EINA_TRUE);
        evas_object_resize(progress_bar, 100, 100);
        evas_object_move(progress_bar, R->SCREEN_BASE_PROGRESS_BAR_ITEM_OBJ_MOVE_X, R->SCREEN_BASE_PROGRESS_BAR_ITEM_OBJ_MOVE_Y);
        evas_object_raise(progress_bar);
        evas_object_show(progress_bar);

        elm_object_translatable_part_text_set(layout, "block_user_text", "IDS_BLOCK_USER");

    } else {
        ShowNotification(ENetworkError);
    }
}

void ProfileScreen::on_block_btn_clicked(void *user_data, Evas_Object *obj, void *event_info)
{
    ProfileScreen *me = static_cast<ProfileScreen*>(user_data);
    assert(me);
    Log::info(LOG_FACEBOOK_USER, "ProfileScreen::on_block_btn_clicked");
    PopupMenu::Close();
    me->ShowCancelBlockPopup();
}

void ProfileScreen::ShowCancelBlockPopup()
{
    char * title = WidgetFactory::WrapByFormat(i18n_get_text("IDS_PROFILE_POPUP_BLOCK_TITLE"), mName);
    char *description = WidgetFactory::WrapByFormat(i18n_get_text("IDS_PROFILE_POPUP_BLOCK_MESSAGE"), mName);
    mConfirmationDialog =
            ConfirmationDialog::CreateAndShow(
                    mLayout,
                    title,
                    description,
                    "IDS_CANCEL",
                    "IDS_BLOCK",
                    confirmation_dialog_cb,
                    this);
    delete[] description;
    delete[] title;
}

void ProfileScreen::confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event)
{
    assert(user_data);
    ProfileScreen *me = static_cast<ProfileScreen*>(user_data);

    if (ScreenBase::SID_USER_PROFILE == me->GetScreenId()) {
        me->DeleteConfirmationDialog();
    }

    if (ConfirmationDialog::ECDNoPressed == event) {
        me->BlockUser();
    }
}

void ProfileScreen::on_poke_btn_clicked(void *user_data, Evas_Object *obj, void *event_info)
{
    assert(user_data);
    ProfileScreen *me = static_cast<ProfileScreen*>(user_data);

    Log::info(LOG_FACEBOOK_USER, "ProfileScreen::on_poke_btn_clicked");
    PopupMenu::Close();
    if (ConnectivityManager::Singleton().IsConnected()) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(me->mPokeRequest);
        me->mPokeRequest = FacebookSession::GetInstance()->PokeUser(me->mId.c_str(), on_poke_user_completed, me);
    } else {
        me->ShowNotification(ENetworkError);
    }
}

void ProfileScreen::on_poke_user_completed(void* object, char* respond, int code)
{
    assert(object);
    ProfileScreen *me = static_cast<ProfileScreen*>(object);

    Log::info(LOG_FACEBOOK_USER, "ProfileScreen::on_poke_user_completed");
    FacebookSession::GetInstance()->ReleaseGraphRequest(me->mPokeRequest);
    if (code == CURLE_OK) {
        Log::debug(LOG_FACEBOOK_USER, "response: %s", respond);
        FbRespondBase *pokeRespond = FbRespondBase::createFromJson(respond);
        if (pokeRespond) {
            std::stringstream notifMessage;
            if (pokeRespond->mError) {
                Log::error(LOG_FACEBOOK_USER, "Error with poke request, error = %s", pokeRespond->mError);
                notifMessage << me->mName << ' ' << i18n_get_text("IDS_POKE_ERROR_RESPONSE");
            } else {
                notifMessage << i18n_get_text("IDS_POKE_ERROR_SUCCESS") << ' ' << me->mName;
            }
            notification_status_message_post(notifMessage.str().c_str());
        }
        delete pokeRespond;
    } else {
        Log::error(LOG_FACEBOOK_USER, "Error sending http request");
        me->ShowNotification(ENetworkError);
    }
    free(respond);
}

void ProfileScreen::remove_this_screen(void *data) {
    assert(data);
    ProfileScreen *me = static_cast<ProfileScreen*>(data);
    me->Pop();
}

void ProfileScreen::Update(AppEventId eventId, void* data)
{
    switch (eventId) {
    case eREQUEST_COMPLETED_EVENT: {
        RequestCompleteEvent *eventData = static_cast<RequestCompleteEvent *>(data);
        if(eventData) {
            Log::info( "ProfileScreen::Update->eREQUEST_COMPLETED_EVENT %d Response Error %d", eventData->mCurlCode, eventData->mResponseError );
            if(eventData->mCurlCode == CURLE_OK && eventData->mResponseError == 100) {
                if(Application::GetInstance()->GetTopScreen() == this) {
                    if(mProfileLayout) {
                        evas_object_del(mProfileLayout);
                        mProfileLayout = nullptr;
                    }
                    ShowErrorWidget(eNO_DATA_AVAILABLE);
                }
                return;
            }
        }
        break;
    }
    case eUNFRIEND_FRIEND_ERROR:
        Log::info( "ProfileScreen::Update->eUNFRIEND_FRIEND_ERROR");
        OnUnfriendError();
        break;
    case eBLOCK_FRIEND_ERROR: {
        Log::info( "ProfileScreen::Update->eBLOCK_FRIEND_ERROR");
        Evas_Object *layout = elm_object_part_content_unset(mLayout, "lock_screen");
        evas_object_del(layout);
        break;
    }
    case eREMOVE_TOP_BLOCKED_SCREEN:
        if(Application::GetInstance()->GetTopScreen() == this) {
            Log::info( "ProfileScreen::Update->eREMOVE_TOP_BLOCKED_SCREEN");
            ecore_job_add(remove_this_screen, this);
            return;
        }
        break;
    case eADD_FRIEND_BTN_CLICKED:
    case eCANCEL_FRIEND_REQUEST_BTN_CLICKED:
    case eDELETE_FRIEND_REQUEST_BTN_CLICKED:
        if(mUserActionData && data) {
           if (!strcmp(static_cast<char*>(data),mId.c_str())) {
               UserProfileData *person = dynamic_cast<UserProfileData*>(mUserActionData->mData);
               if(person) {
                   if (eventId == eCANCEL_FRIEND_REQUEST_BTN_CLICKED ||
                       eventId == eDELETE_FRIEND_REQUEST_BTN_CLICKED) {
                       person->mFriendshipStatus = Person::eCAN_REQUEST;
                       if(person->mSubscribeStatus != UserProfileData::CANNOT_SUBSCRIBE) {
                           person->mSubscribeStatus = UserProfileData::CAN_SUBSCRIBE;
                       }
                   } else {
                       person->mFriendshipStatus = Person::eOUTGOING_REQUEST;
                       if(person->mSubscribeStatus != UserProfileData::CANNOT_SUBSCRIBE) {
                           person->mSubscribeStatus = UserProfileData::IS_SUBSCRIBED;
                       }
                   }
                   RefreshActionBtns();
               }
           }
        }
        break;
    case eFRIEND_REQUEST_CONFIRMED:
    case eUPDATE_UNFRIENDED_USER_STATUS:
    case eFRIEND_REQUEST_CONFIRM_DELETE_ERROR:
        if(mUserActionData && data) {
           if (!strcmp(static_cast<char*>(data),mId.c_str())) {
               UserProfileData *person = dynamic_cast<UserProfileData*>(mUserActionData->mData);
               if(person) {
                   if(eventId == eFRIEND_REQUEST_CONFIRMED) {
                       person->mFriendshipStatus = Person::eARE_FRIENDS;
                       if(person->mSubscribeStatus == UserProfileData::CAN_SUBSCRIBE) {
                           person->mSubscribeStatus = UserProfileData::IS_SUBSCRIBED;
                       } else if(person->mSubscribeStatus == UserProfileData::CANNOT_SUBSCRIBE) {
                           person->mSubscribeStatus = UserProfileData::CAN_SUBSCRIBE;
                       }
                   } else {
                       person->mFriendshipStatus = Person::eCAN_REQUEST;
                   }
                   RefreshActionBtns();
               }
           }
        }
        break;
    default:
        break;
    }
    BaseProfileScreen::Update(eventId, data);
}

void ProfileScreen::confirmation_dialog_cb_unfriend(void *user_data, ConfirmationDialog::CDEventType event) {
    assert(user_data);
    ProfileScreen *screen = static_cast<ProfileScreen*>(user_data);
    if (event == ConfirmationDialog::ECDNoPressed) {
        screen->DoUnfriend();
    }
    screen->DeleteConfirmationDialog();
}

bool ProfileScreen::DeleteConfirmationDialog() {
    if (mConfirmationDialog) {
        delete mConfirmationDialog;
        mConfirmationDialog = nullptr;
        return true;
    }
    return false;
}
