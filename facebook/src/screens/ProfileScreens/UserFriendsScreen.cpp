#include "ConnectivityManager.h"
#include "FriendsAction.h"
#include "MutualFriendsProvider.h"
#include "OperationManager.h"
#include "OwnFriendsProvider.h"
#include "Popup.h"
#include "RequestCompleteEvent.h"
#include "UserFriendsScreen.h"
#include "Utils.h"
#include "WidgetFactory.h"

const unsigned int UserFriendsScreen::DEFAULT_ITEMS_WND_SIZE = 50;

UserFriendsScreen::UserFriendsScreen(const char *id, const char *name, bool showOnlyMutualFriends) :
        ScreenBase(nullptr),
        TrackItemsProxyAdapter(nullptr, Utils::IsMe(id) ? static_cast<FriendsProvider*>(OwnFriendsProvider::GetInstance()) :
#ifdef FEATURE_MUTUAL_FRIENDS
            showOnlyMutualFriends ? static_cast<FriendsProvider*>(new MutualFriendsProvider(id)) :
#endif
                new FriendsProvider(id))
{
    mScreenId = ScreenBase::SID_USER_PROFILE_FRIENDS_TAB;

    mId = id ? id : "";

    mSearchEntry = nullptr;
    mSearchLayout = nullptr;

    mAction = new FriendsAction(this);

    mLayout = WidgetFactory::CreateBaseScreenLayout(this, getParentMainLayout(), name, true, true, true, false, true);
    WidgetFactory::CreateFriendItemEmulator(getParentMainLayout(), UIRes::GetInstance()->FF_ITEM_FRIEND_TEXT_STYLE, UIRes::GetInstance()->FF_SEARCH_RESULT_WRAP_WIDTH);
    mOwnFriendsList = Utils::IsMe(id);
    ApplyScroller(mLayout, true);
    elm_object_part_content_set(mLayout, "content", GetScroller());

    CreateSearchEntry();

    elm_object_translatable_part_text_set(mLayout, "error_text", "IDS_NO_FRIENDS_FOUND");

    SetIdentifier("USER FRIENDS SCREEN");

    TrackItemsProxy::ProxySettings* settings = GetProxySettings();
    settings->initialWndSize = DEFAULT_ITEMS_WND_SIZE;
    SetProxySettings(settings);

    if(Utils::IsMe(id)) {
        OwnFriendsProvider::GetInstance()->SetAlphabeticalOrder(true);
        AppEvents::Get().Subscribe(eFRIEND_UNFRIENDED, this);
        AppEvents::Get().Subscribe(eDELETE_BLOCKED_FRIEND_FROM_MY_LIST, this);
    }
    else{
        AppEvents::Get().Subscribe(eUPDATE_UNFRIENDED_USER_STATUS, this);
        AppEvents::Get().Subscribe(eDELETE_BLOCKED_FRIEND_FROM_OTHER_LIST, this);
        AppEvents::Get().Subscribe(eADD_FRIEND_BTN_CLICKED, this);
        AppEvents::Get().Subscribe(eCANCEL_FRIEND_REQUEST_BTN_CLICKED, this);
    }
    AppEvents::Get().Subscribe(eFRIEND_REQUEST_CONFIRMED, this);
    AppEvents::Get().Subscribe(eINTERNET_CONNECTION_CHANGED, this);
    AppEvents::Get().Unsubscribe(eDATA_CHANGES_EVENT, this);
    RequestData(true);
}

UserFriendsScreen::~UserFriendsScreen()
{
    if(!Utils::IsMe(mId.c_str())) {
        GetProvider()->Release();
    } else {
        static_cast<FriendsProvider *> (GetProvider())->ResetFilter();
    }
    delete mAction;
    OperationManager::GetInstance()->UnSubscribe(this);
    EraseWindowData();
    WidgetFactory::DeleteFriendItemEmulator();
}

void UserFriendsScreen::Push()
{
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void UserFriendsScreen::FetchEdgeItems()
{
    Log::info("UserFriendsScreen::FetchEdgeItems");
    if (ConnectivityManager::Singleton().IsConnected()) {
        Log::info("UserFriendsScreen::FetchEdgeItems->RequestData");
        GetProvider()->EraseData();
        if(Utils::IsMe(mId.c_str())) {
            static_cast<FriendsProvider *> (GetProvider())->SetAlphabeticalOrder(true);
        }
        static_cast<FriendsProvider *> (GetProvider())->MissCacheRetrieve(true);
        RequestData(true);
        RequestToRebuildAllItems();
    }
}

void UserFriendsScreen::RebuildAllItems() {
    Log::info("UserFriendsScreen::RebuildAllItems->ProviderSize= %u",GetProvider()->GetDataCount());
    Redraw();
}

void UserFriendsScreen::CreateSearchEntry()
{
    mSearchLayout = WidgetFactory::CreateLayoutByGroup(mLayout, "user_friends_entry_layout");
    elm_object_part_content_set(mLayout, "search_bar", mSearchLayout);

    mSearchEntry = WidgetFactory::CreateChooseScreenEntry(mSearchLayout);
    elm_object_focus_set(mSearchEntry, EINA_FALSE);
    elm_entry_input_panel_hide(mSearchEntry);

    elm_object_signal_emit(mSearchLayout, "clear.btn", "hide");

    evas_object_smart_callback_add(mSearchEntry, "changed", on_changed_cb, this);
    evas_object_smart_callback_add(mSearchEntry, "activated", on_search_keypad_btn_clicked_cb, this);

    elm_object_signal_callback_add(mSearchLayout, "mouse,clicked,*", "clear_*", on_clear_btn_clicked_cb, this);
}

void UserFriendsScreen::on_search_keypad_btn_clicked_cb(void *data, Evas_Object *obj, void *event_info)
{
    UserFriendsScreen *me = static_cast<UserFriendsScreen*>(data);
    assert(me);
    if (me->mSearchEntry) {
        elm_entry_input_panel_hide(me->mSearchEntry);
    }
}

void UserFriendsScreen::on_changed_cb(void *data, Evas_Object *obj, void *event_info)
{
    UserFriendsScreen *me = static_cast<UserFriendsScreen*>(data);
    assert(me);
    me->OnSearchBtnClicked();
}

void UserFriendsScreen::OnSearchBtnClicked()
{
    const char *str = elm_entry_entry_get(mSearchEntry);
    if (str && !strcmp(str, "")) {
        elm_object_signal_emit(mSearchLayout, "clear.btn", "hide");
    } else {
        elm_object_signal_emit(mSearchLayout, "clear.btn", "show");
    }

    char *searchStr = elm_entry_markup_to_utf8(elm_entry_entry_get(mSearchEntry));

    if (searchStr) {
        if (mOwnFriendsList) {
            OwnFriendsProvider::GetInstance()->SetFindText(searchStr);
        } else {
            FriendsProvider *provider = static_cast<FriendsProvider*>(GetProvider());
            provider->SetFindText(searchStr);
        }

        free(searchStr);
        EraseWindowData();
        ShiftDown();
    }

    SetNoDataViewVisibility(GetProvider()->GetDataCount() == 0);
}

void UserFriendsScreen::on_clear_btn_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    UserFriendsScreen *me = static_cast<UserFriendsScreen*>(data);
    assert(me);
    if (me->mSearchEntry) {
        elm_entry_entry_set(me->mSearchEntry, "");
    }
}

void* UserFriendsScreen::CreateItem(void *dataForItem, bool isAddToEnd) {

    assert(dataForItem);
    Friend *friends = static_cast<Friend*>(dataForItem);

    ActionAndData *data = new ActionAndData(friends, mAction);
    data->mParentWidget = CreateFriendItem(GetDataArea(), data, isAddToEnd);

    if (data && !data->mParentWidget) {
        delete data;
        return nullptr;
    }

    assert(data->mActionObj);
    RefreshButton(data->mActionObj, friends->mFriendshipStatus);

    return data;
}

void UserFriendsScreen::Update(AppEventId eventId, void * data)
{
    Log::debug( "UserFriendsScreen::Update with eventId %d",(int)eventId);
    switch(eventId) {
    case eREQUEST_COMPLETED_EVENT:{
        RequestCompleteEvent *eventData = static_cast<RequestCompleteEvent *>(data);
        if(eventData && eventData->mCurlCode == CURLE_OK) {
            HideConnectionError();
            TrackItemsProxy::Update(eventId,data);
        } else {
            // show error
            ShowConnectionError(true);
        }
        break;
    }
    case eINTERNET_CONNECTION_CHANGED:
        SetNoDataViewVisibility(false);
        if (ConnectivityManager::Singleton().IsConnected()) {
            RequestData();
            HideConnectionError();
        }
        break;
    case eFRIEND_REQUEST_CONFIRMED:
        Log::debug("UserFriendsScreen::Update->eFRIEND_REQUEST_CONFIRMED");
        if (!Utils::IsMe(static_cast<char*>(data))) {
            FriendsProvider* provider = dynamic_cast<FriendsProvider*>(GetProvider());
            assert(provider);
            Friend* fr = provider->GetFriendById(static_cast<char*>(data));
            if(fr) {
                fr->mFriendshipStatus = Person::eARE_FRIENDS;
                ActionAndData* actionData = static_cast<ActionAndData*>(FindItem(fr, items_comparator_Obj_vs_ActionAndData));
                if (actionData) {
                    assert(actionData->mActionObj);
                    RefreshButton(actionData->mActionObj, fr->mFriendshipStatus);
                }
            }
        } else if (!IsRequestedToRebuildAllItems()) {
            Redraw();
        }
        break;
    case eUPDATE_UNFRIENDED_USER_STATUS:
        Log::debug("UserFriendsScreen::Update->eUPDATE_UNFRIENDED_USER_STATUS");
        if (data) {
            FriendsProvider* provider = dynamic_cast<FriendsProvider*>(GetProvider());
            assert(provider);
            Friend* fr = provider->GetFriendById(static_cast<char*>(data));
            if(fr) {
                Log::debug("UserFriendsScreen::Update->eUPDATE_UNFRIENDED_USER_STATUS");
                fr->mFriendshipStatus = Person::eCAN_REQUEST;
                Redraw();
            }
        }
        break;
    case eFRIEND_UNFRIENDED:
    case eDELETE_BLOCKED_FRIEND_FROM_MY_LIST:
        Log::debug("UserFriendsScreen::Update->eDELETE_UNFRIENDED_OR_BLOCKED_FRIEND");
        if (data) {
            Friend *friends = static_cast<Friend *>(data);
            DestroyItemByData(friends , TrackItemsProxyAdapter::items_comparator_Obj_vs_ActionAndData);
        }
        break;
    case eDELETE_BLOCKED_FRIEND_FROM_OTHER_LIST:
        Log::debug("UserFriendsScreen::Update->eDELETE_BLOCKED_FRIEND_FROM_OTHER_LIST");
        if (data) {
            FriendsProvider* provider = dynamic_cast<FriendsProvider*>(GetProvider());
            assert(provider);
            Friend* user_fr = provider->GetFriendById(static_cast<char*>(data));
            if (user_fr) {
                DestroyItemByData(user_fr , TrackItemsProxyAdapter::items_comparator_Obj_vs_ActionAndData);
                provider->DeleteItem(user_fr);
            }
        }
        break;
    case eADD_FRIEND_BTN_CLICKED:
    case eCANCEL_FRIEND_REQUEST_BTN_CLICKED:
        if(data) {
            FriendsProvider* provider = dynamic_cast<FriendsProvider*>(GetProvider());
            assert(provider);
            Friend* fr = provider->GetFriendById(static_cast<char*>(data));
            if(fr) {
                fr->mFriendshipStatus = (eventId == eADD_FRIEND_BTN_CLICKED) ? Person::eOUTGOING_REQUEST : Person::eCAN_REQUEST;
                ActionAndData* actionData = static_cast<ActionAndData*>(FindItem(fr, items_comparator_Obj_vs_ActionAndData));
                if (actionData) {
                    assert(actionData->mActionObj);
                    RefreshButton(actionData->mActionObj, fr->mFriendshipStatus);
                }
            }
        }
        break;
    default:
        TrackItemsProxy::Update(eventId,data);
        break;
    }
}

void* UserFriendsScreen::UpdateItem(void *dataForItem, bool isAddToEnd) {
    assert(dataForItem);
    DeleteUIItem(dataForItem);

    ActionAndData *actionAndData = static_cast<ActionAndData*>(dataForItem);

    actionAndData->mParentWidget = CreateFriendItem(GetDataArea(), actionAndData, isAddToEnd);

    if (!actionAndData->mParentWidget) {
        delete actionAndData;
        return nullptr;
    }

    assert(actionAndData->mData);
    Friend *friends = static_cast<Friend*>(actionAndData->mData);

    assert(actionAndData->mActionObj);
    RefreshButton(actionAndData->mActionObj, friends->mFriendshipStatus);

    return actionAndData;
}

Evas_Object *UserFriendsScreen::CreateFriendItem(Evas_Object* parent, ActionAndData *action_data, bool isAddToEnd)
{
    Evas_Object *list_item = WidgetFactory::CreateFriendItem(parent, action_data, isAddToEnd);
    if (list_item) {
        elm_object_signal_callback_add(list_item, "mouse,clicked,*", "friend_*", on_friends_button_clicked_cb, action_data);
        elm_object_signal_callback_add(list_item, "mouse,clicked,*", "add_friend_*", ActionBase::on_add_friend_btn_clicked_cb, action_data);
        elm_object_signal_callback_add(list_item, "mouse,clicked,*", "cancel_friend_*", ActionBase::on_cancel_friend_btn_clicked_cb, action_data);
        elm_object_signal_callback_add(list_item, "mouse,clicked,*", "item_*", on_item_clicked_cb, action_data);
    }
    return list_item;
}

void UserFriendsScreen::on_friends_button_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    assert(action_data);
    assert(action_data->mAction);
    UserFriendsScreen *me = static_cast<UserFriendsScreen*>(action_data->mAction->getScreen());
    assert(me);

    assert(action_data->mData);
    Friend *friends = static_cast<Friend*>(action_data->mData);

    if (friends->mFriendshipStatus == Person::eARE_FRIENDS) {
        static PopupMenu::PopupMenuItem context_menu_items[] = {
            { nullptr, "IDS_UNFRIEND", nullptr,
              ActionBase::on_unfriend_friend_btn_clicked_cb, nullptr,
              0, false, false
            },
            { nullptr, "IDS_BLOCK", nullptr,
              ActionBase::on_block_friend_btn_clicked_cb, nullptr,
              1, true, false
            },
            //Feature was marked as "LOW PRIORITY" https://github.com/fbmp/fb4t/issues/59
            //{ nullptr, "Edit list", nullptr,
            //  nullptr/*on_share_with_post_clicked*/, nullptr,
            //  2, true, false
            //},
        };
        context_menu_items[0].data = action_data;
        context_menu_items[1].data = action_data;
        //context_menu_items[2].data = action_data;

        Evas_Coord y = 0, h = 0;
        evas_object_geometry_get(obj, nullptr, &y, nullptr, &h);
        y += h / 2;
        PopupMenu::Show(2, context_menu_items, me->mLayout, false, y);
    }
}

void UserFriendsScreen::on_item_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    if (Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_USER_PROFILE_FRIENDS_TAB) {
        ActionAndData *data = static_cast<ActionAndData*>(user_data);
        assert(data);
        assert(data->mData);
        const char* userId = data->mData->GetId();
        if (userId) {
            Utils::OpenProfileScreen(userId);
        } else {
            Log::error("UserFriendsScreen::on_item_clicked_cb->can't open profile, no screenId");
        }
    }
}

void UserFriendsScreen::SetDataAvailable(bool isAvailable)
{
    SetNoDataViewVisibility(!isAvailable);
}

void UserFriendsScreen::SetNoDataViewVisibility(bool isVisible)
{
    ApplyProgressBar(!isVisible);
    SetDataAreaHint(isVisible);
    if (ConnectivityManager::Singleton().IsConnected()) { //No data visibility is meaningful when device is in connected mode only
        if (isVisible) {
            elm_object_signal_emit(mLayout, "error.show", "");
        } else {
            elm_object_signal_emit(mLayout, "error.hide", "");
        }
    } else {
        elm_object_signal_emit(mLayout, "error.hide", "");
        ShowConnectionError(false);
    }
}

bool UserFriendsScreen::HandleBackButton()
{
    bool ret = true;
    if(mAction->DeleteConfirmationDialog()){
    } else if (PopupMenu::Close()) {
    } else {
        ret = false;
    }
    return ret;
}

void UserFriendsScreen::RefreshButton(Evas_Object *list_item, Person::FriendshipStatus status) {
    elm_object_signal_emit(list_item, (status == Person::eARE_FRIENDS ? "show.set" : "hide.set"), "friend");
    elm_object_signal_emit(list_item, (status == Person::eCAN_REQUEST || status == Person::eINCOMING_REQUEST ? "show.set" : "hide.set"), "add");
    elm_object_signal_emit(list_item, (status == Person::eOUTGOING_REQUEST ? "show.set" : "hide.set"), "cancel");
}
