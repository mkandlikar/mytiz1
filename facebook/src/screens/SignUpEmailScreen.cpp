/*
 *  SignUpEmailScreen.cpp
 *
 *  Created on: 4th June 2015
 *      Author: Manjunath Raja
 */

#include<regex.h>

#include "Common.h"
#include "CommonScreen.h"
#include "Config.h"
#include "dlog.h"
#include "SignUpEmailScreen.h"
#include "SignUpNameScreen.h"
#include "SignupRequest.h"
#include "Utils.h"


SignUpEmailScreen::SignUpEmailScreen(ScreenBase *parentScreen, bool ChangeEmail, bool ConfirmMobile):ScreenBase(parentScreen)  {
    mChangeEmail = ChangeEmail;
    mConfirmMobile = ConfirmMobile;
    mUsingMobile = ConfirmMobile;
    mEvasCtxpopup = NULL;
    CreateView();
}

SignUpEmailScreen::~SignUpEmailScreen() {
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Destryoing SignUpEmailScreen");
    evas_object_smart_callback_del(Application::GetInstance()->mConform, "virtualkeypad,state,on", on_screen_keyb_on);
    evas_object_smart_callback_del(Application::GetInstance()->mConform, "virtualkeypad,state,off", on_screen_keyb_off);
}

void SignUpEmailScreen::LoadEdjLayout() {
    mLayout = elm_layout_add(getParentMainLayout());
    elm_layout_file_set(mLayout, Application::mEdjPath, "signup_email_screen");
    evas_object_size_hint_weight_set(mLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_show(mLayout);
    evas_object_smart_callback_add(Application::GetInstance()->mConform, "virtualkeypad,state,on", on_screen_keyb_on, this);
    evas_object_smart_callback_add(Application::GetInstance()->mConform, "virtualkeypad,state,off", on_screen_keyb_off, this);
}

void SignUpEmailScreen::on_screen_keyb_on(void *data, Evas_Object *obj, void *event_info)
{
   //Emit the following signal
    SignUpEmailScreen *mescreen = static_cast<SignUpEmailScreen *>(data);
    if(mescreen->mUsingMobile)
        elm_object_signal_emit(mescreen->mLayout, "belowheadermobile", "Header");
    else
        elm_object_signal_emit(mescreen->mLayout, "belowheaderemail", "Header");

    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "EMITTING SIGNAL KB ON %d", mescreen->mUsingMobile);
}
void SignUpEmailScreen::on_screen_keyb_off(void *data, Evas_Object *obj, void *event_info)
{
   //Emit the following signal
    SignUpEmailScreen *mescreen = static_cast<SignUpEmailScreen *>(data);
    elm_object_signal_emit(mescreen->mLayout, "upheader", "Header");

    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "EMITTING SIGNAL KB OFF ON %d",mescreen->mUsingMobile);
}

void SignUpEmailScreen::CreateView() {
    const unsigned short int DefaultCountry= 91;//TODO
    char TmpBuff[128];

    LoadEdjLayout();

    elm_object_translatable_part_text_set(mLayout, "HeaderText",
        (mChangeEmail ? "IDS_CHANGE_EMAIL_ADDRESS" :
                        (mConfirmMobile ? "IDS_CONFIRM_BY_MOBILE_NO" : "IDS_SUP_EMAIL")));

    mEvasCountryDropDown = elm_button_add(mLayout);
    elm_object_style_set(mEvasCountryDropDown, "dropdown");
    mCountryCode = DefaultCountry;
    Utils::Snprintf_s(TmpBuff, sizeof(TmpBuff), "+%d", mCountryCode);
    elm_object_text_set(mEvasCountryDropDown, TmpBuff);
    evas_object_smart_callback_add(mEvasCountryDropDown, "clicked", CountryDropDownCb, this);

    elm_object_translatable_part_text_set(mLayout, "sup_email_nxt_btn_txt",
        ((mChangeEmail || mConfirmMobile) ? "IDS_CONTINUE" : "IDS_SUP_WC_NEXT"));
    elm_object_signal_callback_add(mLayout, "got.a.sup.enxt.btn.click", "sup_email_nxt_btn*",
            (Edje_Signal_Cb)ContinueBtnCb, this);

    if (!mChangeEmail || !mConfirmMobile)
        elm_object_signal_callback_add(mLayout, "got.a.sup.etoggle.link.click", "sup_email_toggle_link*",
                (Edje_Signal_Cb)ToggleButtonCb, this);
    elm_object_signal_callback_add(mLayout, "HeaderBack", "HeaderBack",
            (Edje_Signal_Cb)HeaderBackCb, this);

    SetScreenInfo(this, false);
    if (mConfirmMobile)
        CreateMobileInput(this);
    else
        CreateEmailInput(this);
}

void SignUpEmailScreen::CreateInput(Evas_Object *InputLayout,
        Evas_Object *Input, Elm_Input_Panel_Layout  EntryType)
{
    elm_entry_input_panel_layout_set(Input, EntryType);
    elm_layout_theme_set(InputLayout, "layout", "editfield", "singleline");
    evas_object_size_hint_align_set(InputLayout, EVAS_HINT_FILL, 0.0);
    evas_object_size_hint_weight_set(InputLayout, EVAS_HINT_EXPAND, 0.0);
    elm_entry_single_line_set(Input, EINA_TRUE);
    elm_entry_scrollable_set(Input, EINA_TRUE);
    elm_entry_text_style_user_push(Input, "DEFAULT='font_size=32 color=#141823'");
    evas_object_smart_callback_add(Input, "focused", InputFocusedCb, InputLayout);
    evas_object_smart_callback_add(Input, "unfocused", InputUnFocusedCb, InputLayout);
    evas_object_smart_callback_add(Input, "changed", InputChangedCb, InputLayout);
    evas_object_smart_callback_add(Input, "preedit,changed", InputChangedCb, InputLayout);
    elm_object_part_content_set(InputLayout, "elm.swallow.content", Input);

    Evas_Object *InputClearBtn = elm_button_add(InputLayout);
    elm_object_style_set(InputClearBtn, "editfield_clear");
    evas_object_smart_callback_add(InputClearBtn, "clicked", InputClearButtonCb, Input);
    elm_object_part_content_set(InputLayout, "elm.swallow.button", InputClearBtn);
}

void SignUpEmailScreen::CreateEmailInput(SignUpEmailScreen *data) {
    data->mEvasEmailLayout = elm_layout_add(data->getParentMainLayout());
    data->mEvasEmailInput = elm_entry_add(data->mEvasEmailLayout);
    CreateInput(data->mEvasEmailLayout,
            data->mEvasEmailInput, ELM_INPUT_PANEL_LAYOUT_EMAIL);
    elm_entry_prediction_allow_set(data->mEvasEmailInput, EINA_FALSE);
    elm_object_translatable_part_text_set(data->mEvasEmailInput, "elm.guide", "IDS_SUP_EMAIL");
    elm_object_part_content_set(data->mLayout, "sup_email_input", data->mEvasEmailLayout);
    elm_object_part_content_unset(data->mLayout, "sup_email_country_code");
    evas_object_hide(data->mEvasCountryDropDown);
}

void SignUpEmailScreen::CreateMobileInput(SignUpEmailScreen *data) {
    elm_object_part_content_set(data->mLayout, "sup_email_country_code", data->mEvasCountryDropDown);
    data->mEvasMobileLayout = elm_layout_add(data->getParentMainLayout());
    data->mEvasMobileInput = elm_entry_add(data->mEvasMobileLayout);
    CreateInput(data->mEvasMobileLayout,
            data->mEvasMobileInput, ELM_INPUT_PANEL_LAYOUT_PHONENUMBER);
    elm_entry_prediction_allow_set(data->mEvasMobileInput, EINA_FALSE);
    elm_object_translatable_part_text_set(data->mEvasMobileInput, "elm.guide", "IDS_SUP_MOBILE_ENTER");
    elm_object_part_content_set(data->mLayout, "sup_email_mobile_no", data->mEvasMobileLayout);
}

void SignUpEmailScreen::SetScreenInfo(SignUpEmailScreen *data, bool error) {
    if (!error) {
        elm_object_part_text_set(data->mLayout, "sup_email_error", " ");
        if (data->mUsingMobile) {
            elm_object_translatable_part_text_set(data->mLayout, "sup_email_heading", "IDS_SUP_MOBILE_HEADING");
            elm_object_translatable_part_text_set(data->mLayout, "sup_email_info", "IDS_SUP_MOBILE_INFO");
            elm_object_translatable_part_text_set(data->mLayout, "sup_email_toggle_link",
                                (data->mConfirmMobile ? " " : "IDS_SUP_USE_EMAIL"));
        } else {
            elm_object_translatable_part_text_set(data->mLayout, "sup_email_heading", "IDS_SUP_EMAIL_HEADING");
            elm_object_translatable_part_text_set(data->mLayout, "sup_email_info", "IDS_SUP_EMAIL_INFO");
            elm_object_translatable_part_text_set(data->mLayout, "sup_email_toggle_link",
                    (data->mChangeEmail ? " " : "IDS_SUP_USE_MOBILE_NO"));
        }
        if (!data->mChangeEmail && !data->mConfirmMobile)
            elm_object_translatable_part_text_set(data->mLayout, "HeaderText",
                    (data->mUsingMobile ? "IDS_SUP_MOBILE_ENTER" : "IDS_SUP_EMAIL"));
    } else {
        elm_object_part_text_set(data->mLayout, "sup_email_info", " ");
        if (data->mUsingMobile) {
            elm_object_translatable_part_text_set(data->mLayout, "sup_email_error", "IDS_SUP_MOBILE_INVALID");
        } else {
            elm_object_translatable_part_text_set(data->mLayout, "sup_email_error", "IDS_SUP_EMAIL_INVALID");
        }
    }
}

void SignUpEmailScreen::ContinueBtnCb(void *data, Evas_Object *obj, void *EventInfo) {
    SignUpEmailScreen *self = static_cast<SignUpEmailScreen*>(data);
    bool ValidEmailOrMobile = false;
    char MobileNo[128] = {0};

    if (!self->mUsingMobile) {
        if (IsValidEmail(elm_entry_entry_get(self->mEvasEmailInput))) {
            SignupRequest::Singleton().SetData(SignupRequest::EMAILADDRESS, elm_entry_entry_get(self->mEvasEmailInput));
            ValidEmailOrMobile = true;
        }
    } else {
        if (CommonScreen::IsValidNumber(elm_entry_entry_get(self->mEvasMobileInput))) {
            Utils::Snprintf_s(MobileNo, sizeof(MobileNo), "+%d%s", self->mCountryCode, elm_entry_entry_get(self->mEvasMobileInput));
            SignupRequest::Singleton().SetData(SignupRequest::MOBILENUMBER, MobileNo);
            ValidEmailOrMobile = true;
        }
    }

    SetScreenInfo(self, !ValidEmailOrMobile);
    if (ValidEmailOrMobile) {
        ScreenBase *NewScreen = NULL;
        if (self->mChangeEmail || self->mConfirmMobile) {
            CommonScreen::StartSigningUp();
        } else {
            NewScreen = new SignUpNameScreen(NULL);
            Application::GetInstance()->AddScreen(NewScreen);
            elm_naviframe_item_title_enabled_set(elm_naviframe_top_item_get(NewScreen->getNf()), EINA_FALSE, EINA_FALSE);
        }
    }
}

void SignUpEmailScreen::ToggleButtonCb(void *data, Evas_Object *obj, void *EventInfo) {
    SignUpEmailScreen *self = (SignUpEmailScreen*)data;
    if (!self->mUsingMobile) {
        if (self->mEvasEmailLayout) evas_object_del(self->mEvasEmailLayout);
        CreateMobileInput(self);
        self->mUsingMobile = true;
    } else {
        if (self->mEvasMobileLayout) evas_object_del(self->mEvasMobileLayout);
        CreateEmailInput(self);
        self->mUsingMobile = false;
    }
    SetScreenInfo(self, false);
}

void SignUpEmailScreen::HeaderBackCb(void* Data, Evas_Object* Obj, void* EventInfo) {
    SignUpEmailScreen* Me = static_cast<SignUpEmailScreen*>(Data);
    Me->Pop();
}

bool SignUpEmailScreen::IsValidEmail(const char *address)
{
    regex_t RegexHandle;
    bool IsValidEmailAddres = false;
    const char *EmailAddressRegex = "^([a-z0-9])(([-a-z0-9._])*([a-z0-9])*)*@([a-z0-9])(([a-z0-9-])*([a-z0-9])*)*([.]([a-z0-9])([-a-z0-9_-])?([a-z0-9])+)+$";

    if (regcomp(&RegexHandle, EmailAddressRegex, REG_ICASE | REG_EXTENDED)) {
        //dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Email alias regex unrecognized");
        return false;
    }

    int nsub = RegexHandle.re_nsub + 1;
    regmatch_t pmatch[nsub];
    memset(pmatch, 0, sizeof(regmatch_t) * nsub);

    if (regexec(&RegexHandle, address, nsub, pmatch, 0) == REG_NOMATCH) {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Email regex doesn't match");
    }
    else {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Email regex mathes");
        if ((int)(pmatch[0].rm_eo - pmatch[0].rm_so) == strlen(address))
            IsValidEmailAddres = true;
    }

    regfree(&RegexHandle);
    return IsValidEmailAddres;
}

void SignUpEmailScreen::InputClearButtonCb(void *data, Evas_Object *obj, void *EventInfo){
    Evas_Object *Input = (Evas_Object *)data;
    elm_entry_entry_set(Input, "");
}

void SignUpEmailScreen::InputChangedCb(void *data, Evas_Object *obj, void *EventInfo){
    Evas_Object *Input = (Evas_Object *) data;
    if (!elm_entry_is_empty(obj) && elm_object_focus_get(obj))
        elm_object_signal_emit(Input, "elm,action,show,button", "");
    else
        elm_object_signal_emit(Input, "elm,action,hide,button", "");
}

void SignUpEmailScreen::InputFocusedCb(void *data, Evas_Object *obj, void *EventInfo){
    Evas_Object *Input = (Evas_Object *) data;
    if (!elm_entry_is_empty(obj)){
        elm_object_signal_emit(Input, "elm,action,show,button", "");
    }
}

void SignUpEmailScreen::InputUnFocusedCb(void *data, Evas_Object *obj, void *EventInfo){
    Evas_Object *Input = (Evas_Object *) data;
    elm_object_signal_emit(Input, "elm,action,hide,button", "");
}

void SignUpEmailScreen::DropDownDismissedCb(void *data, Evas_Object *obj, void *EventInfo) {
    SignUpEmailScreen *self = (SignUpEmailScreen*)data;
    if (self->mEvasCtxpopup) {
        evas_object_del(self->mEvasCtxpopup);
        self->mEvasCtxpopup = NULL;
    }
}

void SignUpEmailScreen::DropDownSelectedItemCb(void *data, Evas_Object *obj, void *EventInfo) {
    SignUpEmailScreen *self = (SignUpEmailScreen*)data;
    const char *selectedItem= elm_object_item_text_get((Elm_Object_Item *) EventInfo);
    char *tmp = strdup(selectedItem);
    const char find = '(';
    char *extractor;

    extractor = strchr(tmp, find);
    ++extractor;
    extractor[strlen(extractor)-1] = 0;
    elm_object_text_set(self->mEvasCountryDropDown, extractor);
    self->mCountryCode = atoi(extractor);

    if (tmp) free(tmp);
    if (self->mEvasCtxpopup) {
        evas_object_del(self->mEvasCtxpopup);
        self->mEvasCtxpopup = NULL;
    }

    //dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Selected country code <%d>", self->mCountryCode);
}

void SignUpEmailScreen::CountryDropDownCb(void *data, Evas_Object *obj, void *EventInfo) {
    SignUpEmailScreen *self = (SignUpEmailScreen*)data;
    Evas_Object *ctxpopup = self->mEvasCtxpopup;
    if (ctxpopup != NULL) {
        evas_object_del(ctxpopup);
    }

    ctxpopup = elm_ctxpopup_add(self->mLayout);
    elm_object_style_set(ctxpopup, "dropdown/label");
    eext_object_event_callback_add(ctxpopup, EEXT_CALLBACK_BACK, eext_ctxpopup_back_cb, NULL);
    evas_object_smart_callback_add(ctxpopup,"dismissed", DropDownDismissedCb, data);

    elm_ctxpopup_item_append(ctxpopup, "Anguilla (+1)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Antigua (+1)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Argentina (+54)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Armenia (+374)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Aruba (+297)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Australia (+61)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Austria (+43)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Azerbaijan (+994)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Bahrain (+973)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Bangladesh (+880)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Barbados (+1)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Belarus (+375)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Belgium (+32)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Belize (+501)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Benin (+229)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Bermuda (+1)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Bhutan (+975)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Bolivia (+591)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Bonaire, Sint Eustatius and Saba (+599)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Bosnia and Herzegovina (+387)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Botswana (+267)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Brazil (+55)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "British Indian Ocean Territory (+246)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "British Virgin Islands (+1)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Brunei (+673)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Bulgaria (+359)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Burkina Faso (+226)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Burundi (+257)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Cambodia (+855)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Cameroon (+237)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Canada (+1)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Cape Verde (+238)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Cayman Islands (+1)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Central African Republic (+236)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Chad (+235)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Chile (+56)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "China (+86)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Colombia (+57)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Comoros (+269)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Cook Islands (+682)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Costa Rica (+506)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Côte d'Ivoire (+225)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Croatia (+385)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Cuba (+53)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Curaçao (+599)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Cyprus (+357)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Czech Republic (+420)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Democratic Republic of the Congo (+243)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Denmark (+45)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Djibouti (+253)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Dominica (+1)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Dominican Republic (+1)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Ecuador (+593)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Egypt (+20)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "El Salvador (+503)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Equatorial Guinea (+240)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Eritrea (+291)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Estonia (+372)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Ethiopia (+251)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Falkland Islands (+500)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Faroe Islands (+298)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Federated States of Micronesia (+691)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Fiji (+679)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Finland (+358)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "France (+33)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "French Guiana (+594)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "French Polynesia (+689)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Gabon (+241)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Georgia (+995)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Germany (+49)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Ghana (+233)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Gibraltar (+350)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Greece (+30)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Greenland (+299)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Grenada (+1)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Guadeloupe (+590)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Guam (+1)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Guatemala (+502)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Guernsey (+44)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Guinea (+224)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Guinea-Bissau (+245)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Guyana (+592)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Haiti (+509)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Honduras (+504)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Hong Kong (+852)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Hungary (+36)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Iceland (+354)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "India (+91)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Indonesia (+62)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Iran (+98)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Iraq (+964)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Ireland (+353)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Isle Of Man (+44)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Israel (+972)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Italy (+39)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Jamaica (+1)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Japan (+81)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Jersey (+44)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Jordan (+962)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Kazakhstan (+7)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Kenya (+254)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Kiribati (+686)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Kosovo (+381)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Kuwait (+965)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Kyrgyzstan (+996)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Laos (+856)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Latvia (+371))", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Lebanon (+961)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Lesotho (+266)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Liberia (+231)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Libya (+218)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Liechtenstein (+423)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Lithuania (+370)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Luxembourg (+352)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Macau (+853)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Macedonia (+389)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Madagascar (+261)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Malawi (+265)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Malaysia (+60)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Maldives (+960)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Mali (+223)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Malta (+356)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Marshall Islands (+692)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Martinique (+596)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Mauritania (+222)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Mauritius (+230)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Mayotte (+262)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Mexico (+52)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Moldova (+373)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Monaco (+377)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Mongolia (+976)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Montenegro (+382)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Montserrat (+1)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Morocco (+212)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Mozambique (+258)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Myanmar (+95)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Namibia (+264)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Nauru (+674)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Nepal (+977)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Netherlands (+31)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "New Caledonia (+687)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "New Zealand (+64)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Nicaragua (+505)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Niger (+227)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Nigeria (+234)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Niue (+683)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Norfolk Island (+672)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "North Korea (+850)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Northern Mariana Islands (+1)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Norway (+47)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Oman (+968)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Pakistan (+92)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Palau (+680)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Palestine (+970)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Panama (+507)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Papua New Guinea (+675)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Paraguay (+595)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Peru (+51)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Philippines (+63)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Poland (+48)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Portugal (+351)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Puerto Rico (+1)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Qatar (+974)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Republic of the Congo (+242)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Réunion (+262)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Romania (+40)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Russia (+7)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Rwanda (+250)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Saint Barthélemy (+590)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Saint Helena (+290)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Saint Kitts and Nevis (+1)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Saint Martin (+590)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Saint Pierre and Miquelon (+508)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Saint Vincent and the Grenadines (+1)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Samoa (+685)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "San Marino (+378)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Sao Tome and Principe (+239)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Saudi Arabia (+966)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Senegal (+221)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Serbia (+381)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Seychelles (+248)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Sierra Leone (+232)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Singapore (+65)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Sint Maarten (+599)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Slovakia (+421)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Slovenia (+386)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Solomon Islands (+677)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Somalia (+252)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "South Africa (+27)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "South Korea (+82)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "South Sudan (+211)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Spain (+34) ", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Sri Lanka (+94)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "St. Lucia (+1)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Sudan (+249)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Suriname (+597)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Swaziland (+268)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Sweden (+46)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Switzerland (+41)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Syria (+963)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Taiwan (+886)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Tajikistan (+992)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Tanzania (+255)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Thailand (+66)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "The Bahamas (+1)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "The Gambia (+220)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Timor-Leste (+670)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Togo (+228)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Tokelau (+690)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Tonga (+676)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Trinidad and Tobago (+1)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Tunisia (+216)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Turkey (+90)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Turkmenistan (+993)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Turks and Caicos Islands (+1)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Tuvalu (+688)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Uganda (+256)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Ukraine (+380)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "United Arab Emirates (+971)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "United Kingdom (+44)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "United States (+1)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Uruguay (+598)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "US Virgin Islands (+1)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Uzbekistan (+998)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Vanuatu (+678)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Vatican City (+39)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Venezuela (+58)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Vietnam (+84)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Wallis and Futuna (+681)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Western Sahara (+212)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Yemen (+967)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Zambia (+260)", NULL, DropDownSelectedItemCb, data);
    elm_ctxpopup_item_append(ctxpopup, "Zimbabwe (+263)", NULL, DropDownSelectedItemCb, data);

    elm_ctxpopup_direction_priority_set(ctxpopup,
            ELM_CTXPOPUP_DIRECTION_UNKNOWN, ELM_CTXPOPUP_DIRECTION_UNKNOWN,
            ELM_CTXPOPUP_DIRECTION_UNKNOWN, ELM_CTXPOPUP_DIRECTION_UNKNOWN);

    self->mEvasCtxpopup = ctxpopup;
    evas_object_show(ctxpopup);
}

