#include "ConnectivityManager.h"
#include "PhotosOfYouScreen.h"
#include "PhotosOfYouProvider.h"

#include <dlog.h>

const unsigned int POY_PROG_BAR_TIME = 2;

PhotosOfYouScreen::PhotosOfYouScreen(ScreenBase *parentScreen, const char* userProfileId, PhotoGalleryStrategyBase* strategy) :
        PhotoGalleryScreenBase(parentScreen, PhotosOfYouProvider::GetInstance(), strategy), mNoPhotosLayout(NULL) {
    dlog_print(DLOG_INFO, LOG_TAG, "PhotosOfYouScreen: create Photos Of You screen." );

    mPOYProgressBar = NULL;
    mPOYProgBarTimer = NULL;

    mLayout = elm_layout_add(getParentMainLayout());
    elm_layout_file_set(mLayout, Application::mEdjPath, "user_photos_tab_screen");
    evas_object_show(mLayout);

    elm_object_part_content_set(mLayout, "swallow.scroller", GetScroller());

    PhotosOfYouProvider::GetInstance()->SetUserId(userProfileId, PhotoGalleryProvider::ETagged, this);

    RequestData();
    //Display the Progress bar till the time, Images download is complete.
    // Hiding Progress bar is handled in call back functions.
    POYDisplayProgressBar();
}

PhotosOfYouScreen::~PhotosOfYouScreen() {
    dlog_print(DLOG_INFO, LOG_TAG, "PhotosOfYouScreen() destructor called");

    if (mPOYProgressBar) {
        evas_object_del(mPOYProgressBar);
    }

    if (mPOYProgBarTimer) {
        ecore_timer_del(mPOYProgBarTimer);
    }
}

void PhotosOfYouScreen::POYDisplayProgressBar() {
    if (!mPOYProgressBar) {
        mPOYProgressBar = elm_progressbar_add(mLayout);
        elm_object_style_set(mPOYProgressBar, "process_large");
        elm_progressbar_pulse_set(mPOYProgressBar, EINA_TRUE);
        elm_progressbar_pulse(mPOYProgressBar, EINA_TRUE);
        elm_object_part_content_set(mLayout, "photos_of_you_progress_bar", mPOYProgressBar);
    }
    evas_object_raise(mPOYProgressBar);
    evas_object_show(mPOYProgressBar);
    if (!mPOYProgBarTimer) {
        mPOYProgBarTimer = ecore_timer_add(POY_PROG_BAR_TIME, poy_prog_bar_timer_cb, this);
    }
}

void PhotosOfYouScreen::POYHideProgressBar() {
    if (mPOYProgressBar) {
        evas_object_lower(mPOYProgressBar);
        evas_object_hide(mPOYProgressBar);
        evas_object_del(mPOYProgressBar);
        mPOYProgressBar = NULL;
    }
}

Eina_Bool PhotosOfYouScreen::poy_prog_bar_timer_cb(void *data) {
	PhotosOfYouScreen* usrPhotoScr = static_cast<PhotosOfYouScreen*>(data);
    bool result = ECORE_CALLBACK_RENEW; // true
//AAA ToDo: replace with flag that all data is really downloaded by dataprovider.
    if(true) {
        usrPhotoScr->POYHideProgressBar();
        result = ECORE_CALLBACK_CANCEL; // false
        usrPhotoScr->mPOYProgBarTimer = NULL;
    }
    return result;
}

void PhotosOfYouScreen::CreateNoPhotosTextLayout() {
    if (!mNoPhotosLayout) {
        mNoPhotosLayout = elm_layout_add(GetDataArea());
        evas_object_size_hint_weight_set(mNoPhotosLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
        evas_object_size_hint_align_set(mNoPhotosLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);
        elm_layout_file_set(mNoPhotosLayout, Application::mEdjPath, "no_photos_text_layout");
        elm_object_translatable_part_text_set(mNoPhotosLayout, "no_result_text", "IDS_NO_PHOTO");
        evas_object_show(mNoPhotosLayout);
    }
}

void PhotosOfYouScreen::SetDataAvailable(bool isAvailable) {
    if(!isAvailable) {
        CreateNoPhotosTextLayout();
        SetTopListLayout(mNoPhotosLayout);
    } else {
        SetTopListLayout(NULL);
    }

    PhotoGalleryScreenBase::SetDataAvailable(isAvailable);
}

