#include "ActionBase.h"
#include "Common.h"
#include "ConnectivityManager.h"
#include "FbRespondGetSearchResults.h"
#include "FoundItem.h"
#include "GroupProfilePage.h"
#include "Log.h"
#include "OwnProfileScreen.h"
#include "ProfileScreen.h"
#include "SearchScreenTabbed.h"
#include "Utils.h"
#include "WebViewScreen.h"
#include "WidgetFactory.h"

#define ALL_FILTER "['user','page','group','app']"
#define USER_FILTER "['user']"
#define GROUP_FILTER "['group']"
#define PAGE_FILTER "['page']"
#define APP_FILTER "['app']"

SearchToTab::SearchToTab()
{
    stScreen = NULL;
    mTab = SEARCH_ALL_TAB;
}

SearchToTab::~SearchToTab()
{
}

SearchScreenTabbed::SearchScreenTabbed(const char * searchStr, SearchScreen * previousScreen): ScreenBase(NULL)
{
    mPreviousScreen = previousScreen;

    mSearchField = NULL;
    mTabbar = NULL;
    mAllTab = NULL;
    mPeopleTab = NULL;
    mPagesTab = NULL;
    mGroupsTab = NULL;

    mScroller = NULL;
    mNestedBoxContainer = NULL;

    mAllLayout = NULL;
    mPeopleLayout = NULL;
    mPageLayout = NULL;
    mGroupLayout = NULL;

    mAllInnerLayout = NULL;
    mPeopleInnerLayout = NULL;
    mPageInnerLayout = NULL;
    mGroupInnerLayout = NULL;

    for(int i= SEARCH_ALL_TAB; i<SEARCH_GROUP_TAB; i++) {
        mSearchToTab[i] = NULL;
    }

    mLayout = CreateView(getParentMainLayout());
    RegisterCbs();

    mSearchString = NULL;

    mSearchAllRequest = NULL;
    mSearchPeopleRequest = NULL;
    mSearchPageRequest = NULL;
    mSearchGroupRequest = NULL;

    mSearchString = SAFE_STRDUP(searchStr);

    if (searchStr) {
        elm_entry_entry_set(mSearchField, searchStr);
    }
    elm_entry_cursor_end_set(mSearchField);

    StartSearchRequest();
}

SearchScreenTabbed::~SearchScreenTabbed()
{
    FacebookSession::GetInstance()->ReleaseGraphRequest(mSearchAllRequest);
    FacebookSession::GetInstance()->ReleaseGraphRequest(mSearchPeopleRequest);
    FacebookSession::GetInstance()->ReleaseGraphRequest(mSearchPageRequest);
    FacebookSession::GetInstance()->ReleaseGraphRequest(mSearchGroupRequest);
    for(int i= SEARCH_ALL_TAB; i<SEARCH_GROUP_TAB; i++) {
        delete mSearchToTab[i];
    }
    free(mSearchString);
}

void SearchScreenTabbed::RegisterCbs() {
    evas_object_smart_callback_add(mScroller, "scroll,anim,stop", anim_stop_scroll_cb, this);
    if(mSearchField) {
        elm_object_signal_callback_add(mLayout, "search.clicked", "btn", searchfield_pressed_cb, this);
        elm_object_signal_callback_add(mLayout, "clear.text", "btn", searchfield_clear_button_clicked_cb, this);
    }

    elm_object_signal_callback_add(mAllTab, "item.clicked", "", tabbar_cb, mSearchToTab[SEARCH_ALL_TAB]);
    elm_object_signal_callback_add(mPeopleTab, "item.clicked", "", tabbar_cb, mSearchToTab[SEARCH_PEOPLE_TAB]);
    elm_object_signal_callback_add(mPagesTab, "item.clicked", "", tabbar_cb, mSearchToTab[SEARCH_PAGES_TAB]);
    elm_object_signal_callback_add(mGroupsTab, "item.clicked", "", tabbar_cb, mSearchToTab[SEARCH_GROUP_TAB]);

    elm_object_signal_callback_add(mAllInnerLayout, "mouse,clicked,*", "con_lost_*", retry_connect_cb, this);
    elm_object_signal_callback_add(mPeopleInnerLayout, "mouse,clicked,*", "con_lost_*", retry_connect_cb, this);
    elm_object_signal_callback_add(mPageInnerLayout, "mouse,clicked,*", "con_lost_*", retry_connect_cb, this);
    elm_object_signal_callback_add(mGroupInnerLayout, "mouse,clicked,*", "con_lost_*", retry_connect_cb, this);
    elm_object_signal_callback_add(mLayout, "back.clicked", "btn", back_button_clicked, this);
}

void SearchScreenTabbed::UnregisterCbs() {
    evas_object_smart_callback_del(mScroller, "scroll,anim,stop", anim_stop_scroll_cb);
    elm_object_signal_callback_del(mLayout, "back.clicked", "btn", back_button_clicked);

    if(mSearchField) {
        elm_object_signal_callback_del(mLayout, "search.clicked", "btn", searchfield_pressed_cb);
        elm_object_signal_callback_del(mLayout, "clear.text", "btn", searchfield_clear_button_clicked_cb);
    }

    elm_object_signal_callback_del(mAllTab, "item.clicked", "", tabbar_cb);
    elm_object_signal_callback_del(mPeopleTab, "item.clicked", "", tabbar_cb);
    elm_object_signal_callback_del(mPagesTab, "item.clicked", "", tabbar_cb);
    elm_object_signal_callback_del(mGroupsTab, "item.clicked", "", tabbar_cb);

    elm_object_signal_callback_del(mAllInnerLayout, "mouse,clicked,*", "con_lost_*", retry_connect_cb);
    elm_object_signal_callback_del(mPeopleInnerLayout, "mouse,clicked,*", "con_lost_*", retry_connect_cb);
    elm_object_signal_callback_del(mPageInnerLayout, "mouse,clicked,*", "con_lost_*", retry_connect_cb);
    elm_object_signal_callback_del(mGroupInnerLayout, "mouse,clicked,*", "con_lost_*", retry_connect_cb);
}

void SearchScreenTabbed::Push()
{
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

Evas_Object *SearchScreenTabbed::CreateView(Evas_Object *parent)
{
    Evas_Object *layout = WidgetFactory::CreateLayoutByGroup(parent, "search_screen_tabbed");

    CreateSearchField(layout);

    mTabbar = CreateTabbar(layout);
    elm_object_part_content_set(layout, "tabbar", mTabbar);

    Evas_Object *box = elm_box_add(layout);
    evas_object_size_hint_weight_set(box, 1, 1);
    evas_object_size_hint_align_set(box, -1, -1);
    elm_object_part_content_set(layout, "content", box);

    mScroller = CreateScroller(box);
    elm_box_pack_end(box, mScroller);

    mNestedBoxContainer = elm_box_add(mScroller);
    elm_box_horizontal_set(mNestedBoxContainer, EINA_TRUE);
    evas_object_size_hint_weight_set(mNestedBoxContainer, 1, 1);
    evas_object_size_hint_align_set(mNestedBoxContainer, -1, -1);
    elm_object_content_set(mScroller, mNestedBoxContainer);
    evas_object_show(mNestedBoxContainer);

    CreateAllPage(mNestedBoxContainer);
    CreatePeoplePage(mNestedBoxContainer);
    CreatePagePage(mNestedBoxContainer);
    CreateGroupPage(mNestedBoxContainer);

    Evas_Coord w, h;
    /* gets the object's position and size to build it correctly */
    evas_object_geometry_get(Application::GetInstance()->mWin, NULL, NULL, &w, &h);
    OnViewResize(w, h);

    return layout;
}

void SearchScreenTabbed::OnViewResize(Evas_Coord w, Evas_Coord h)
{
    int page = 0;
    elm_scroller_current_page_get(mScroller, &page, nullptr);
    elm_scroller_page_show(mScroller, page, 0);
    elm_scroller_page_size_set(mScroller, R->SST_MAIN_RECT_SIZE_W, R->SST_MAIN_RECT_SIZE_H);
    evas_object_size_hint_min_set(mAllLayout, R->SST_MAIN_RECT_SIZE_W, R->SST_MAIN_RECT_SIZE_H);
    evas_object_size_hint_min_set(mPeopleLayout, R->SST_MAIN_RECT_SIZE_W, R->SST_MAIN_RECT_SIZE_H);
    evas_object_size_hint_min_set(mPageLayout, R->SST_MAIN_RECT_SIZE_W, R->SST_MAIN_RECT_SIZE_H);
    evas_object_size_hint_min_set(mGroupLayout, R->SST_MAIN_RECT_SIZE_W, R->SST_MAIN_RECT_SIZE_H);
}

void SearchScreenTabbed::CreateSearchField(Evas_Object *parent) {
    /* Search field */
    mSearchField = elm_entry_add(parent);
    if(mSearchField) {
        elm_entry_input_panel_layout_set(mSearchField, ELM_INPUT_PANEL_LAYOUT_NORMAL);
        elm_entry_input_panel_return_key_type_set(mSearchField, ELM_INPUT_PANEL_RETURN_KEY_TYPE_SEARCH);
        elm_entry_prediction_allow_set(mSearchField, EINA_FALSE);
        elm_entry_single_line_set(mSearchField, EINA_TRUE);
        elm_entry_scrollable_set(mSearchField, EINA_TRUE);

        elm_entry_text_style_user_push(mSearchField, GLOBAL_SEARCH_STYLE);
        elm_entry_cnp_mode_set(mSearchField, ELM_CNP_MODE_PLAINTEXT);
        evas_object_show(mSearchField);
        elm_object_part_content_set(parent, "search_input_text", mSearchField);
        elm_object_focus_allow_set(mSearchField, EINA_FALSE);
    }
}

Evas_Object *SearchScreenTabbed::CreateTabbar(Evas_Object *parent)
{
    Evas_Object *scroller = elm_scroller_add(parent);
    elm_scroller_page_size_set(scroller, R->SST_LIST_ITEM_SIZE_W, 0);
    elm_scroller_single_direction_set(scroller, ELM_SCROLLER_SINGLE_DIRECTION_HARD);
    elm_scroller_page_scroll_limit_set(scroller, 1, 1);
    elm_scroller_policy_set(scroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_OFF);
    evas_object_show(scroller);

    Evas_Object *data_area = elm_box_add(scroller);
    evas_object_size_hint_weight_set(data_area, 1, 1);
    evas_object_size_hint_align_set(data_area, -1, -1);
    elm_box_padding_set(data_area, 0, 0);
    elm_box_horizontal_set(data_area, EINA_TRUE);
    elm_object_content_set(scroller, data_area);
    evas_object_show(data_area);

    mActiveTab = SEARCH_ALL_TAB;

    mAllTab = CreateTabbarItem(data_area, i18n_get_text("IDS_SS_ALL"), true, SEARCH_ALL_TAB);
    mPeopleTab = CreateTabbarItem(data_area, i18n_get_text("IDS_SS_PEOPLE"), false, SEARCH_PEOPLE_TAB);
    mPagesTab = CreateTabbarItem(data_area, i18n_get_text("IDS_SS_PAGES"), false, SEARCH_PAGES_TAB);
    mGroupsTab = CreateTabbarItem(data_area, i18n_get_text("IDS_SS_GROUPS"), false, SEARCH_GROUP_TAB);

    return scroller;
}

Evas_Object *SearchScreenTabbed::CreateTabbarItem(Evas_Object *parent, const char *text2set, bool isSelected, SearchScreenTabs tab)
{
    Evas_Object *item = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(parent, "tabbar_item");

    char *text = NULL;
    Evas_Object *label = elm_label_add(item);
    elm_object_part_content_set(item, "item_text", label);
    evas_object_size_hint_weight_set(label, 1, 1);
    evas_object_size_hint_align_set(label, -1, 0.5);
    text = WidgetFactory::WrapByFormat2(SANS_MEDIUM_9197a3_FORMAT, "28", text2set);
    if (text) {
        elm_object_text_set(label, text);
        delete[] text;
    }
    evas_object_show(label);

    if (isSelected) {
        elm_object_signal_emit(item, "set.selected", "");
    }

    mSearchToTab[tab] = new SearchToTab;
    mSearchToTab[tab]->stScreen = this;
    mSearchToTab[tab]->mTab = tab;

    return item;
}

Evas_Object *SearchScreenTabbed::CreateScroller(Evas_Object *parent)
{
    Evas_Object *scroller = elm_scroller_add(parent);
    elm_scroller_policy_set(scroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_OFF);
    elm_scroller_page_size_set(scroller, R->SST_MAIN_RECT_SIZE_W, 0);
    elm_scroller_page_scroll_limit_set(scroller, 1, 1);
    elm_scroller_single_direction_set(scroller, ELM_SCROLLER_SINGLE_DIRECTION_HARD);
    evas_object_size_hint_weight_set(scroller, 1, 1);
    evas_object_size_hint_align_set(scroller, -1, -1);
    evas_object_show(scroller);

    return scroller;
}

void SearchScreenTabbed::CreateAllPage(Evas_Object *parent)
{
    mAllLayout = elm_layout_add(parent);
    evas_object_size_hint_weight_set(mAllLayout, 1, 1);
    evas_object_size_hint_align_set(mAllLayout, 0, 0);
    elm_layout_file_set(mAllLayout, Application::mEdjPath, "search_page");
    elm_box_pack_start(parent, mAllLayout);
    evas_object_show(mAllLayout);

    mAllInnerLayout = WidgetFactory::CreateConnectivityLostLayout(mAllLayout, "connectivity_error_layout");
}

void SearchScreenTabbed::CreatePeoplePage(Evas_Object *parent)
{
    mPeopleLayout = elm_layout_add(parent);
    evas_object_size_hint_weight_set(mPeopleLayout, 1, 1);
    evas_object_size_hint_align_set(mPeopleLayout, 0, 0);
    elm_layout_file_set(mPeopleLayout, Application::mEdjPath, "search_page");

    elm_box_pack_after(parent, mPeopleLayout, mAllLayout);
    evas_object_show(mPeopleLayout);

    mPeopleInnerLayout = WidgetFactory::CreateConnectivityLostLayout(mPeopleLayout, "connectivity_error_layout");
}

void SearchScreenTabbed::CreatePagePage(Evas_Object *parent)
{
    mPageLayout = elm_layout_add(parent);
    evas_object_size_hint_weight_set(mPageLayout, 1, 1);
    evas_object_size_hint_align_set(mPageLayout, 0, 0);
    elm_layout_file_set(mPageLayout, Application::mEdjPath, "search_page");

    elm_box_pack_after(parent, mPageLayout, mPeopleLayout);
    evas_object_show(mPageLayout);

    mPageInnerLayout = WidgetFactory::CreateConnectivityLostLayout(mPageLayout, "connectivity_error_layout");
}

void SearchScreenTabbed::CreateGroupPage(Evas_Object *parent)
{
    mGroupLayout = elm_layout_add(parent);
    evas_object_size_hint_weight_set(mGroupLayout, 1, 1);
    evas_object_size_hint_align_set(mGroupLayout, 0, 0);
    elm_layout_file_set(mGroupLayout, Application::mEdjPath, "search_page");

    elm_box_pack_after(parent, mGroupLayout, mPageLayout);
    evas_object_show(mGroupLayout);

    mGroupInnerLayout = WidgetFactory::CreateConnectivityLostLayout(mGroupLayout, "connectivity_error_layout");
}

void SearchScreenTabbed::retry_connect_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    if(Application::GetInstance()->GetTopScreen() == data) {
        SearchScreenTabbed *me = static_cast<SearchScreenTabbed*>(data);
        me->StartSearchRequest();
    }
}

void SearchScreenTabbed::searchfield_clear_button_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::info("SearchScreenTabbed::searchfield_clear_button_clicked_cb");
    if(Application::GetInstance()->GetTopScreen() == data) {
        SearchScreenTabbed *screen = static_cast<SearchScreenTabbed *>(data);
        screen->UnregisterCbs();
        screen->mPreviousScreen->SetSearchFieldString("");
        screen->mPreviousScreen->ShowInputPanel();
        screen->Pop();
    }
}

void SearchScreenTabbed::searchfield_pressed_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::info("SearchScreenTabbed::searchfield_pressed_cb");
    if(Application::GetInstance()->GetTopScreen() == data) {
        SearchScreenTabbed *screen = static_cast<SearchScreenTabbed *>(data);
        screen->UnregisterCbs();
        screen->mPreviousScreen->ShowInputPanel();
        screen->Pop();
    }
}

void SearchScreenTabbed::back_button_clicked(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::info("SearchScreenTabbed::back_button_clicked");
    if(Application::GetInstance()->GetTopScreen() == data) {
        SearchScreenTabbed *screen = static_cast<SearchScreenTabbed *>(data);
        screen->UnregisterCbs();
        screen->Pop();
    } else {
        Log::error("SearchScreenTabbed::back_button_clicked->wrong top screen");
    }
}

void SearchScreenTabbed::tabbar_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    SearchToTab *fs = static_cast<SearchToTab*>(data);
    SearchScreenTabbed *screen = static_cast<SearchScreenTabbed*>(fs->stScreen);

    screen->SwitchTabbar(fs->mTab, true);
}

void SearchScreenTabbed::SwitchTabbar(SearchScreenTabs tab, bool changeScrollerPage)
{
    if (mActiveTab == tab) {
        return;
    }

    elm_object_signal_emit(mAllTab, "set.unselected", "");
    elm_object_signal_emit(mPeopleTab, "set.unselected", "");
    elm_object_signal_emit(mGroupsTab, "set.unselected", "");
    elm_object_signal_emit(mPagesTab, "set.unselected", "");

    switch(tab) {
    case SEARCH_ALL_TAB:
         elm_object_signal_emit(mAllTab, "set.selected", "");
         break;
    case SEARCH_PEOPLE_TAB:
         elm_object_signal_emit(mPeopleTab, "set.selected", "");
         break;
    case SEARCH_PAGES_TAB:
         elm_object_signal_emit(mPagesTab, "set.selected", "");
         break;
    case SEARCH_GROUP_TAB:
         elm_object_signal_emit(mGroupsTab, "set.selected", "");
         break;
    }

    elm_scroller_page_bring_in(mTabbar, tab, 0);
    if (changeScrollerPage) {
        elm_scroller_page_bring_in(mScroller, tab, 0);
    }

    mActiveTab = tab;
}


void SearchScreenTabbed::anim_stop_scroll_cb(void *data, Evas_Object *obj, void *event_info)
{
    SearchScreenTabbed *screen = static_cast<SearchScreenTabbed *>(data);
    int page = 0;
    elm_scroller_current_page_get(screen->mScroller, &page, nullptr);
    screen->SwitchTabbar((SearchScreenTabs)page);
}

void SearchScreenTabbed::ShowProgressBar(Evas_Object *layout)
{
    Evas_Object * pb_layout = elm_layout_add(layout);
    elm_layout_file_set(pb_layout, Application::mEdjPath, "sst_search_progress_bar");
    Evas_Object *pb = elm_progressbar_add(pb_layout);
    elm_object_part_content_set(pb_layout, "progress_bar", pb);
    elm_object_style_set(pb, "process_medium");
    elm_progressbar_pulse(pb, EINA_TRUE);
    evas_object_show(pb);
    elm_object_part_content_set(layout, "search_list", pb_layout);
    evas_object_show(pb_layout);
}

void SearchScreenTabbed::StartSearchRequest()
{
    if (!ConnectivityManager::Singleton().IsConnected()) {
        elm_object_signal_emit(mAllLayout, "error.show", "");
        elm_object_signal_emit(mPeopleLayout, "error.show", "");
        elm_object_signal_emit(mPageLayout, "error.show", "");
        elm_object_signal_emit(mGroupLayout, "error.show", "");
    } else {
        elm_object_signal_emit(mAllLayout, "error.hide", "");
        elm_object_signal_emit(mPeopleLayout, "error.hide", "");
        elm_object_signal_emit(mPageLayout, "error.hide", "");
        elm_object_signal_emit(mGroupLayout, "error.hide", "");

        if (!mSearchAllRequest) {
            ShowProgressBar(mAllLayout);
            mSearchAllRequest = FacebookSession::GetInstance()->GetSearchResults(mSearchString, ALL_FILTER, on_search_all_completed, this);
        }

        if (!mSearchPeopleRequest) {
            ShowProgressBar(mPeopleLayout);
            mSearchPeopleRequest = FacebookSession::GetInstance()->GetSearchResults(mSearchString, USER_FILTER, on_search_people_completed, this);
        }

        if (!mSearchPageRequest) {
            ShowProgressBar(mPageLayout);
            mSearchPageRequest = FacebookSession::GetInstance()->GetSearchResults(mSearchString, PAGE_FILTER, on_search_page_completed, this);
        }

        if (!mSearchGroupRequest) {
            ShowProgressBar(mGroupLayout);
            mSearchGroupRequest = FacebookSession::GetInstance()->GetSearchResults(mSearchString, GROUP_FILTER, on_search_group_completed, this);
        }
    }
}

void SearchScreenTabbed::on_search_all_completed(void* object, char* respond, int code)
{
    if(Application::GetInstance()->GetTopScreen() == object) {
        Log::info("SearchScreenTabbed::on_search_all_completed");
        SearchScreenTabbed * screen = static_cast<SearchScreenTabbed *>(object);
        on_search_completed(object, respond, code, &screen->mSearchAllRequest, screen->mAllLayout);
    }
}

void SearchScreenTabbed::on_search_people_completed(void* object, char* respond, int code)
{
    if(Application::GetInstance()->GetTopScreen() == object) {
        Log::info("SearchScreenTabbed::on_search_people_completed");
        SearchScreenTabbed * screen = static_cast<SearchScreenTabbed *>(object);
        on_search_completed(object, respond, code, &screen->mSearchPeopleRequest, screen->mPeopleLayout);
    }
}

void SearchScreenTabbed::on_search_page_completed(void* object, char* respond, int code)
{
    if(Application::GetInstance()->GetTopScreen() == object) {
        Log::info("SearchScreenTabbed::on_search_page_completed");
        SearchScreenTabbed * screen = static_cast<SearchScreenTabbed *>(object);
        on_search_completed(object, respond, code, &screen->mSearchPageRequest, screen->mPageLayout);
    }
}

void SearchScreenTabbed::on_search_group_completed(void* object, char* respond, int code)
{
    if(Application::GetInstance()->GetTopScreen() == object) {
        Log::info("SearchScreenTabbed::on_search_group_completed");
        SearchScreenTabbed * screen = static_cast<SearchScreenTabbed *>(object);
        on_search_completed(object, respond, code, &screen->mSearchGroupRequest, screen->mGroupLayout);
    }
}

/**
 * @brief This callback us called when Share request is completed
 * @param object - callback object
 * @param respond - http respond
 * @param code - curl code
 */
void SearchScreenTabbed::on_search_completed(void* object, char* respond, int code, GraphRequest ** request, Evas_Object *layout)
{
    Log::info("SearchScreenTabbed::on_search_completed");

    SearchScreenTabbed * screen = static_cast<SearchScreenTabbed *>(object);

    FacebookSession::GetInstance()->ReleaseGraphRequest((*request));
    (*request) = NULL;

    if (code == CURLE_OK)
    {
        Log::info("SearchScreenTabbed::on_search_completed->response: %s", respond);
        FbRespondGetSearchResults * searchRespond = FbRespondGetSearchResults::createFromJson(respond);

        if (searchRespond == NULL)
        {
            Log::error("SearchScreenTabbed::on_search_completed->Error parsing http respond");
        }
        else
        {
            if (searchRespond->mErrorMessage != NULL)
            {
                Log::error("SearchScreenTabbed::on_search_completed->Error sending search, error = %s", searchRespond->mErrorMessage);
            }
            else if (searchRespond->mFoundItems == NULL) {
                Evas_Object * text_layout = elm_layout_add(layout);
                elm_layout_file_set(text_layout, Application::mEdjPath, "sst_search_no_result");
                elm_object_part_content_set(layout, "search_list", text_layout);
                elm_object_translatable_part_text_set(text_layout, "no_result", "IDS_NO_RESULTS_FOUND");
                evas_object_show(text_layout);
            }
            else
            {
                screen->SetSearchResults(searchRespond, layout);
            }
            delete searchRespond;
        }
    }
    else
    {
        Log::error("SearchScreenTabbed::on_search_completed->Error sending http request");
    }

    if (respond != NULL)
    {
        //Attention!! client should take care to free respond buffer
        free(respond);
    }
}

void SearchScreenTabbed::SetSearchResults(FbRespondGetSearchResults * searchRespond, Evas_Object* layout)
{
    Log::info("SearchScreenTabbed::SetSearchResults");
    Evas_Object * list = elm_genlist_add(layout);
    evas_object_size_hint_min_set(list, R->SST_MAIN_RECT_SIZE_W, R->SST_MAIN_RECT_SIZE_H);
    elm_object_part_content_set(layout, "search_list", list);
    elm_genlist_select_mode_set(list, ELM_OBJECT_SELECT_MODE_NONE);
    elm_scroller_single_direction_set(list, ELM_SCROLLER_SINGLE_DIRECTION_HARD);
    evas_object_show(list);

    Eina_List *l;
    void *listData;
    Elm_Genlist_Item_Class *ItcSearch = elm_genlist_item_class_new();
    ItcSearch->item_style = "full";
    ItcSearch->func.content_get = gl_list_item_get;
    ItcSearch->func.del = on_item_delete_cb;

    EINA_LIST_FOREACH(searchRespond->mFoundItems, l, listData)
    {
        ActionAndData *data = NULL;
        FoundItem *search_data = static_cast<FoundItem*>(listData);
        if (search_data->mText && strcmp(search_data->mType, "app")) {
            data = new ActionAndData(search_data, NULL);
            elm_genlist_item_append(list, ItcSearch, data, NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);
        }
        // Release to delete search_data when the ActionAndData is deleted.
        search_data->Release();
    }
    // Set NULL to not to remove mFoundItems when the searchRespond is deleted.
    searchRespond->mFoundItems = NULL;
    elm_genlist_item_class_free(ItcSearch);
}

Evas_Object *SearchScreenTabbed::gl_list_item_get(void *user_data, Evas_Object *obj, const char *part)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);

    Evas_Object *list_item = elm_layout_add(obj);
    evas_object_size_hint_min_set(list_item, R->SST_LIST_ITEM_SIZE_W, R->SST_LIST_ITEM_SIZE_H);
    evas_object_size_hint_max_set(list_item, R->SST_LIST_ITEM_SIZE_W, R->SST_LIST_ITEM_SIZE_H);
    elm_layout_file_set(list_item, Application::mEdjPath, "search_screen_tabbed_list_item");

    if (action_data) {
        assert(action_data->mData);
        FoundItem* foundItem = static_cast<FoundItem*>(action_data->mData);
        bool isSubTextAvailable = (foundItem->mSubText && *(foundItem->mSubText));
        bool isCategoryAvailable = (foundItem->mCategory && *(foundItem->mCategory));

        if (isSubTextAvailable && isCategoryAvailable) {
            elm_object_part_text_set(list_item, "box_3line_1", foundItem->mText);
            elm_object_part_text_set(list_item, "box_3line_2", foundItem->mSubText);
            elm_object_part_text_set(list_item, "box_3line_3", foundItem->mCategory);
        } else if (isSubTextAvailable) {
            elm_object_part_text_set(list_item, "box_2line_1", foundItem->mText);
            elm_object_part_text_set(list_item, "box_2line_2", foundItem->mSubText);
        } else if (isCategoryAvailable) {
            elm_object_part_text_set(list_item, "box_2line_1", foundItem->mText);
            elm_object_part_text_set(list_item, "box_2line_2", foundItem->mCategory);
        } else {
            elm_object_part_text_set(list_item, "box_single_line", foundItem->mText);
        }

        Evas_Object *avatar = elm_image_add(list_item);
        elm_object_part_content_set(list_item, "box_avatar", avatar);
        action_data->UpdateImageLayoutAsync(foundItem->mPhoto, avatar);
        evas_object_show(avatar);

        elm_object_signal_callback_add(list_item, "item_clicked", "box", on_item_clicked_cb, action_data);
    }
    return list_item;
}

void SearchScreenTabbed::on_item_delete_cb(void *data, Evas_Object *obj)
{
    if(data) {
        delete static_cast<ActionAndData*>(data);
    }
}

void SearchScreenTabbed::on_item_clicked_cb(void *data, Evas_Object *obj, const char *emisson, const char *source)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(data);

    if (ActionAndData::IsAlive(action_data)) {
        assert(action_data->mData);
        FoundItem *item = static_cast<FoundItem*>(action_data->mData);
        assert(item->mType);
        if (!strcmp(item->mType, "group")) {
            if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_GROUP_PROFILE_PAGE) {
                ScreenBase *group_scr = new GroupProfilePage(item->GetId(), item->mText);
                Application::GetInstance()->AddScreen(group_scr);
            }
        } else if (!strcmp(item->mType, "user")) {
            if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_USER_PROFILE) {
                assert(item->GetId());
                Utils::OpenProfileScreen(item->GetId());
            }
        } else if (!strcmp(item->mType, "page")) {
            if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_WEB_VIEW) {
                WebViewScreen::Launch(item->mPath, true);
            }
        }
    } else {
        Log::error("SearchScreenTabbed::on_item_clicked_cb->ActionAndData is not valid");
    }
}

bool SearchScreenTabbed::HandleBackButton() {
    return false;
}
