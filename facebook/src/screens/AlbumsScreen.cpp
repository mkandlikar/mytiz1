#include "ActionBase.h"
#include "Album.h"
#include "AlbumGalleryScreen.h"
#include "AlbumsProvider.h"
#include "AlbumsScreen.h"
#include "Config.h"
#include "Log.h"
#include "PhotoGalleryShowCarouselStrategy.h"
#include "PhotoGalleryUploadPhotoStrategy.h"
#include "PostComposerScreen.h"
#include "Utils.h"
#include "AlbumPresenter.h"

#include <cassert>


static const char* LogTag = "AlbumsScreen";

/*
 * Class AlbumsScreenItem
 */
AlbumsScreenItem::AlbumsScreenItem() : mActionAndDates(NULL), mLayout(NULL) {
}

AlbumsScreenItem::~AlbumsScreenItem() {
    void *listData;
    EINA_LIST_FREE(mActionAndDates, listData) {
        delete (static_cast<ActionAndData *> (listData));
    }
}

void AlbumsScreenItem::AppendActionAndData(ActionAndData *actionAndData) {
    mActionAndDates = eina_list_append(mActionAndDates, actionAndData);
}

Eina_List *AlbumsScreenItem::GetActionAndDates() const {
    return mActionAndDates;
}

Evas_Object *AlbumsScreenItem::GetLayout() const {
    return mLayout;
}

void AlbumsScreenItem::SetLayout(Evas_Object *layout) {
    mLayout = layout;
}

void AlbumsScreenItem::Update() {
    Eina_List *list;
    void *data;
    EINA_LIST_FOREACH(mActionAndDates, list, data ) {
        ActionAndData *actionAndData = static_cast<ActionAndData*> (data);
        assert(actionAndData);
        if (actionAndData) {
            Album *album = static_cast<Album*> (actionAndData->mData);
            if (album) {
                Evas_Object *albumLayout = actionAndData->mParentWidget;
                AlbumPresenter presenter(*album);
                HRSTC_TRNSLTD_PART_TEXT_SET(albumLayout, "text.first", presenter.GenPhotosNumber().c_str());
                elm_object_part_text_set(albumLayout, "text.second", album->mName);
            }
        }
    }
}

ActionAndData *AlbumsScreenItem::GetActionAndDataById(const char *id) const {
    ActionAndData *res = nullptr;
    Eina_List *list;
    void *data;
    EINA_LIST_FOREACH(mActionAndDates, list, data ) {
        ActionAndData *actionAndData = static_cast<ActionAndData*> (data);
        assert(actionAndData);
        if (actionAndData) {
            Album *album = static_cast<Album*> (actionAndData->mData);
            if (album && !strcmp(album->GetId(), id)) {
                res = actionAndData;
                break;
            }
        }
    }
    return res;
}


/*
 * Class AlbumsScreen
 */
AlbumsScreen::AlbumsScreen(ScreenBase *parentScreen, const char* userProfileId, bool isOpenFrmUplodProfPhoto, void *calBakObj4UsrProfilScr) : mIsOpenFrmUplodProfPhoto(isOpenFrmUplodProfPhoto), mCalBakObj4UsrProfilScr(calBakObj4UsrProfilScr),
     ScreenBase(parentScreen), TrackItemsProxy(getParentMainLayout(), AlbumsProvider::GetInstance(), go_item_comparator),
     mSubscriber(this), mUserId(userProfileId), mCoverImagesList(NULL) {
    Log::info_tag(LogTag, "AlbumsScreen: create Photo Gallery screen." );

    Evas_Object* parent = getParentMainLayout();
    mLayout = elm_layout_add(parent);
    elm_layout_file_set(mLayout, Application::mEdjPath, "user_photos_tab_screen");
    elm_object_part_content_set(mLayout, "swallow.scroller", GetScroller());

    mAction = new ActionBase(this);
    AlbumsProvider::GetInstance()->SetUserId(mUserId, mAction);
    RequestData();
}

AlbumsScreen::~AlbumsScreen() {
    Log::info_tag(LogTag, "AlbumsScreen: destroy AlbumsScreen screen." );

    Eina_List *list;
    void *data;
    EINA_LIST_FOREACH(mCoverImagesList, list, data) {
        downloadCoverPhotoStruct *downloadCoverPhoto = static_cast<downloadCoverPhotoStruct *>(data);
        if (!(downloadCoverPhoto->isDownloadCompleted)) {
            Cancel(downloadCoverPhoto->requestId);
        }
        free(downloadCoverPhoto);
    }
    eina_list_free(mCoverImagesList);

    delete mAction;
    AlbumsProvider::GetInstance()->SetUserId(nullptr, nullptr);
}

const char *AlbumsScreen::SelectBestLayoutForItem(AlbumsScreenItem *item) {
    int imagesCount = eina_list_count(item->GetActionAndDates());
    if (imagesCount == 1) {
        return "gallery_photos_1";
    } else if (imagesCount == 3) {
        ActionAndData *firstData = static_cast<ActionAndData *> (eina_list_nth(item->GetActionAndDates(), 0));
        Album *firstAlbum = static_cast<Album*> (firstData->mData);
        int hash = firstAlbum->GetHash();
        if (hash % 7 == 0) {
            return "gallery_photos_3_1";
        } else if (hash % 6 == 0) {
            return "gallery_photos_3_2";
        } else {
            return "gallery_photos_3_0";
        }
    }
    return NULL;
}

void* AlbumsScreen::CreateItem(void* dataForItem, bool isAddToEnd) {
    AlbumsScreenItem *item = static_cast<AlbumsScreenItem *> (dataForItem);

    Log::info_tag(LogTag, "Create item with %d albums", eina_list_count(item->GetActionAndDates()));
    Evas_Object *itemLayout = elm_layout_add(GetDataArea());
    item->SetLayout(itemLayout);

    elm_layout_file_set(itemLayout, Application::mEdjPath, "albums_screen_item");
    evas_object_show(itemLayout);

    int i = 0;
    Eina_List *list;
    void *data;
    EINA_LIST_FOREACH(item->GetActionAndDates(), list, data ) {
        ActionAndData *actionAndData = static_cast<ActionAndData*> (data);
        Album *album = static_cast<Album*> (actionAndData->mData);

        Evas_Object *albumLayout = elm_layout_add(itemLayout);

        elm_layout_file_set(albumLayout, Application::mEdjPath, "album_item");
        evas_object_show(albumLayout);
        actionAndData->mParentWidget = albumLayout;
        Evas_Object *album_image = evas_object_image_add(evas_object_evas_get(GetDataArea()));
        int swallowW, swallowH;
        int w, h;
        edje_object_part_geometry_get(elm_layout_edje_get(albumLayout), "swallow.album_image", NULL, NULL, &swallowW, &swallowH);

        if (album) {
            Log::info_tag(LogTag, "ALBUM:%s, with %d photos", album->mName, album->mCount);
            AlbumPresenter presenter(*album);
            HRSTC_TRNSLTD_PART_TEXT_SET(albumLayout, "text.first", presenter.GenPhotosNumber().c_str());
            elm_object_part_text_set(albumLayout, "text.second", album->mName);
            if (album->mPicture->mIsSilhouette) {
                evas_object_image_file_set(album_image, ICON_DIR"/album_no_photos.png", NULL);
                evas_object_image_size_get(album_image, &w, &h);
                Utils::CropImageToFill(album_image, swallowW, swallowH, w, h);
            } else {
                char* imageFilePath = Utils::GetImagePathFromUrl(album->mPicture->mUrl);
                if (imageFilePath) {
                    FILE *fp = fopen(imageFilePath, "r");
                    if (fp) {
                        fclose(fp);
                        evas_object_image_file_set(album_image, imageFilePath, NULL);
                        evas_object_image_size_get(album_image, &w, &h);
                        Utils::CropImageToFill(album_image, swallowW, swallowH, w, h);
                    } else {
                        MessageId requestId;
                        Application::GetInstance()->mDataService->DownloadImage(this, album->mPicture->mUrl, requestId);
                        if (requestId) {
                            downloadCoverPhotoStruct *downloadCoverPhoto = (downloadCoverPhotoStruct *)malloc(sizeof(downloadCoverPhotoStruct));
                            downloadCoverPhoto->requestId = requestId;
                            downloadCoverPhoto->isDownloadCompleted = false;
                            downloadCoverPhoto->album_image = album_image;
                            mCoverImagesList = eina_list_append(mCoverImagesList, downloadCoverPhoto);
                        }
                        Utils::CropImageToFill(album_image, swallowW, swallowH, R->ALBUM_ITEM_SIZE, R->ALBUM_ITEM_SIZE);
                    }
                    free(imageFilePath);
                } else {
                    Utils::CropImageToFill(album_image, swallowW, swallowH, R->ALBUM_ITEM_SIZE, R->ALBUM_ITEM_SIZE);
                }
            }
        } else {
            Log::info_tag(LogTag, "CREATE ALBUM ITEM");
            elm_object_translatable_part_text_set(albumLayout, "text.first", "IDS_ALBUMS_CREATE_ALBUM");
            evas_object_image_file_set(album_image, ICON_DIR"/album_create.png", NULL);
            evas_object_image_size_get(album_image, &w, &h);
            Utils::CropImageToFill(album_image, swallowW, swallowH, w, h);
        }
        evas_object_size_hint_weight_set(album_image, 1, 1);
        evas_object_size_hint_align_set(album_image, -1, -1);
        evas_object_show(album_image);

        elm_object_part_content_set(albumLayout, "swallow.album_image", album_image);

        OnAlbumClickedDataStruct *dataToPass = (OnAlbumClickedDataStruct *)malloc(sizeof(OnAlbumClickedDataStruct));
        memset(dataToPass, 0, sizeof(OnAlbumClickedDataStruct));
        dataToPass->smActionNData = actionAndData;
        dataToPass->smIsOpenFrmUplodProfPhoto = mIsOpenFrmUplodProfPhoto;
        dataToPass->smCalBakObj4UsrProfilScr = mCalBakObj4UsrProfilScr;

        elm_object_signal_callback_add(albumLayout, "mouse,clicked,*", "*", on_album_clicked, dataToPass);

        char part[32];
        Utils::Snprintf_s(part, 32, "swallow.album_%d", i++);
        elm_object_part_content_set(itemLayout, part, albumLayout);
    }

    if ( isAddToEnd ) {
        elm_box_pack_end(GetDataArea(), itemLayout);
    } else {
        elm_box_pack_start(GetDataArea(), itemLayout);
    }

    item->AddRef();
    return item;
}

void AlbumsScreen::DeleteItem(void* itemData) {
    AlbumsScreenItem *item = static_cast<AlbumsScreenItem *> (itemData);
    elm_box_unpack(GetDataArea(), item->GetLayout());
    evas_object_del(item->GetLayout());
    Eina_List *list;
    void *data;
    EINA_LIST_FOREACH(item->GetActionAndDates(), list, data ) {
        ActionAndData *actionAndData = static_cast<ActionAndData*> (data);
        actionAndData->mParentWidget = NULL;
    }
}

void AlbumsScreen::DeleteAll_Prepare() {
    if(GetDataArea()) {
        elm_box_clear( GetDataArea() );
    }
}

void AlbumsScreen::DeleteAll_ItemDelete(void* itemData) {
    AlbumsScreenItem *item = static_cast<AlbumsScreenItem *> (itemData);
    item->Release();
}

ItemSize* AlbumsScreen::GetItemSize(void* itemData) {
    AlbumsScreenItem *item = static_cast<AlbumsScreenItem *> (itemData);
    Evas_Coord x, y, weight, height;
    evas_object_geometry_get(item->GetLayout(), &x, &y, &weight, &height);

    ActionAndData *firstData = static_cast<ActionAndData *> (eina_list_nth(item->GetActionAndDates(), 0));
    Album *firstAlbum = static_cast<Album*> (firstData->mData);
    if (firstAlbum) {
        return new ItemSize(strdup(firstAlbum->GetId()), height, weight);
    } else {
        return new ItemSize(strdup("ThisIsCreateAlbumItem"), height, weight);
    }
}

void AlbumsScreen::HideItem( void *itemData )
{
    AlbumsScreenItem *item = static_cast<AlbumsScreenItem *> (itemData);
    evas_object_hide( item->GetLayout() );
}

void AlbumsScreen::ShowItem( void *itemData )
{
    AlbumsScreenItem *item = static_cast<AlbumsScreenItem *> (itemData);
    elm_box_pack_start( GetDataArea(), item->GetLayout() );
    evas_object_show( item->GetLayout() );
}

void AlbumsScreen::ArrangeItems(Eina_List *albums, Eina_Array *items, const char* userId, ActionBase* action) {
    unsigned int albumsCount = 0;

    AlbumsScreenItem *previousItem = NULL;
    Eina_Array* array = AlbumsProvider::GetInstance()->GetData();
    unsigned int count = eina_array_count(array);
    if (count > 0) {
        previousItem = static_cast<AlbumsScreenItem *> (eina_array_data_get(array, count - 1));
    } else {
//Don't show the Create Album button for friends.
        if (Utils::IsMe(userId)) {
//This is the 1st item on the screen it must be "Create Album". Mark it as NULL album.
            previousItem = new AlbumsScreenItem();
            ActionAndData *actionAndData = new ActionAndData(NULL, action);
            previousItem->AppendActionAndData(actionAndData);
            eina_array_push(items, previousItem);
        }
    }

    Eina_List *list;
    void *data;
    AlbumsScreenItem *item = NULL;
    EINA_LIST_FOREACH(albums, list, data) {
        Album *album = static_cast<Album*>(data);
        if (album) {
            //Don't show empty albums for friends. Show empty albums only for me.
            if (Utils::IsMe(userId) || album->mCount) {
                ActionAndData *actionAndData = new ActionAndData(album, action);
//If there is lastItem and it has only 1 album then we need to append 1st album to it.
                if (albumsCount == 0 && previousItem && eina_list_count(previousItem->GetActionAndDates()) == 1) {
                    previousItem->AppendActionAndData(actionAndData);
                } else {
                    if (!item || (item && eina_list_count(item->GetActionAndDates()) == 2)) {
                        item = new AlbumsScreenItem();
                        eina_array_push(items, item);
                    }
                    item->AppendActionAndData(actionAndData);
                }
                ++albumsCount;
            } else {
                album->Release();
            }
        }
    }
    Log::info_tag(LogTag, "AlbumsScreen: there was handled %d albums and %d items created.", albumsCount, eina_array_count(items));
}

void AlbumsScreen::on_album_clicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    OnAlbumClickedDataStruct *dataRcvd = static_cast<OnAlbumClickedDataStruct *> (data);
    assert(dataRcvd);
    ActionAndData *actionAndData = static_cast<ActionAndData *> (dataRcvd->smActionNData);
    assert(actionAndData);
    Album *album = static_cast<Album*>(actionAndData->mData);

    if (album) {
        OwnProfileScreen *profileScreen = static_cast<OwnProfileScreen *> (dataRcvd->smCalBakObj4UsrProfilScr);
        assert(!dataRcvd->smIsOpenFrmUplodProfPhoto || profileScreen);
        AlbumGalleryScreen *albumScreen = new AlbumGalleryScreen(album, dataRcvd->smIsOpenFrmUplodProfPhoto ?
                static_cast<PhotoGalleryStrategyBase *> (new PhotoGalleryUploadPhotoStrategy(profileScreen->mTargetUploadType)) :
                static_cast<PhotoGalleryStrategyBase *> (new PhotoGalleryShowCarouselStrategy()));
        Application::GetInstance()->AddScreen(albumScreen);
    } else {
        PostComposerScreen* postScreen = new PostComposerScreen(NULL, eCREATEALBUM);
        Application::GetInstance()->AddScreen(postScreen);
    }
}

void AlbumsScreen::AlbumRenamed(ActionAndData *actionAndData) {
    if (actionAndData && actionAndData->mParentWidget) {
        Album *album = static_cast<Album *> (actionAndData->mData);
        if (album) {
            elm_object_part_text_set(actionAndData->mParentWidget, "text.second", album->mName);
        }
    }
}

void AlbumsScreen::AlbumCreated() {
    notification_status_message_post(i18n_get_text("IDS_MSG_ALBUM_CREATED"));
    ReloadScreen();
}

void AlbumsScreen::AlbumDeleted() {
    notification_status_message_post(i18n_get_text("IDS_MSG_ALBUM_DELETED"));
    ReloadScreen();
}

void AlbumsScreen::ReloadScreen() {
    EraseWindowData();
    AlbumsProvider::GetInstance()->EraseData();
    AlbumsProvider::GetInstance()->ResetRequestTimer();
    AlbumsProvider::GetInstance()->SetUserId(mUserId, mAction);
    RequestData();
}

Album *AlbumsScreen::GetAlbumById(const char*id) {
    Album *res = NULL;
    if (id) {
        Eina_Array* items = AlbumsProvider::GetInstance()->GetData();
        for (int i = 0; i < eina_array_count(items); ++i){
            AlbumsScreenItem *item = static_cast<AlbumsScreenItem*> (eina_array_data_get(items, i));
            if (item) {
                Eina_List *list;
                void *data;
                EINA_LIST_FOREACH(item->GetActionAndDates(), list, data ) {
                    ActionAndData *actionAndData = static_cast<ActionAndData*> (data);
                    assert(actionAndData);
                    if (actionAndData) {
                        Album *album = static_cast<Album*> (actionAndData->mData);
                        if (album) {
                            if (!strcmp(album->GetId(), id)) {
                                res = album;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
    return res;
}

void AlbumsScreen::UpdateItems() {
    Eina_Array* items = AlbumsProvider::GetInstance()->GetData();
    for (int i = 0; i < eina_array_count(items); ++i){
        AlbumsScreenItem *item = static_cast<AlbumsScreenItem*> (eina_array_data_get(items, i));
        if (item) {
            item->Update();
        }
    }
}

/*
 * Class AlbumsScreen::AlbumsEventsSubscriber
 */

AlbumsScreen::AlbumsEventsSubscriber::AlbumsEventsSubscriber(AlbumsScreen *screen) : mScreen(screen) {
    AppEvents::Get().Subscribe(eALBUM_DELETED_EVENT, this);
    AppEvents::Get().Subscribe(eALBUM_RENAMED_EVENT, this);
    AppEvents::Get().Subscribe(eALBUM_CREATED_EVENT, this);
    AppEvents::Get().Subscribe(eALBUM_NEW_PHOTOS_ADDITION_COMPLETED_EVENT, this);
}

AlbumsScreen::AlbumsEventsSubscriber::~AlbumsEventsSubscriber() {
}

void AlbumsScreen::AlbumsEventsSubscriber::Update(AppEventId eventId, void *data) {
    switch (eventId) {
    case eALBUM_CREATED_EVENT:
        mScreen->AlbumCreated();
        break;
    case eALBUM_DELETED_EVENT:
        mScreen->AlbumDeleted();
        break;
    case eALBUM_RENAMED_EVENT:
    {
        const char *id = static_cast<char *>(data);
        AlbumsScreenItem *item = static_cast<AlbumsScreenItem *> (mScreen->GetItemById(id));
        ActionAndData *actionAndData = item->GetActionAndDataById(id);
        mScreen->AlbumRenamed(actionAndData);
    }
        break;
    case eALBUM_NEW_PHOTOS_ADDITION_COMPLETED_EVENT:
        mScreen->UpdateItems();
        break;
    default:
        break;
    }
}

void AlbumsScreen::HandleDownloadImageResponse(ErrorCode error, MessageId requestId, const char* url, const char* fileName)
{
    if ((error == EBPErrNone) && fileName) {
        Eina_List *list;
        void *data;
        EINA_LIST_FOREACH(mCoverImagesList, list, data) {
            downloadCoverPhotoStruct *downloadCoverPhoto = static_cast<downloadCoverPhotoStruct *>(data);
            if (requestId == (downloadCoverPhoto->requestId)) {
                downloadCoverPhoto->isDownloadCompleted = true;
                evas_object_image_file_set(downloadCoverPhoto->album_image, fileName, NULL);
                int w, h;
                evas_object_image_size_get(downloadCoverPhoto->album_image, &w, &h);
                Utils::CropImageToFill(downloadCoverPhoto->album_image, R->ALBUM_ITEM_SIZE, R->ALBUM_ITEM_SIZE, w, h);
                break;
            }
        }
    } else {
        //ToDo: handle errors
    }
}

bool AlbumsScreen::FindItemId(void *item, const char *id) {
    AlbumsScreenItem *albumItem = static_cast<AlbumsScreenItem *> (item);
    assert(albumItem);
    return albumItem->GetActionAndDataById(id);
}

void* AlbumsScreen::GetDataForItem(void *item) {
    AlbumsScreenItem *albumItem = static_cast<AlbumsScreenItem *> (item);
    ActionAndData *firstData = static_cast<ActionAndData *> (eina_list_nth(albumItem->GetActionAndDates(), 0));
    return firstData ? firstData->mData : NULL;
}

ItemSize* AlbumsScreen::SaveSize( void *item ) {
    ActionAndData *data = NULL;
    AlbumsScreenItem *albumItem = static_cast<AlbumsScreenItem *> (item);
    if (albumItem->GetActionAndDates()) {
        data = static_cast<ActionAndData *>(eina_list_nth(albumItem->GetActionAndDates(), 0));
    }
    GraphObject *object = data ? static_cast<GraphObject*>(data->mData) : NULL;
    ItemSize *size = object ? static_cast<ItemSize *>(eina_hash_find( m_ItemSizeCache, object->GetId())) : NULL;
    if(!size) {
        size = GetItemSize(item);
        if (size) {
            if ((size->height != 0) && (size->weight != 0)) {
                eina_hash_add( m_ItemSizeCache, size->key, size );
            } else {
                delete_cached_item_size(size);
                size = NULL;
            }
        }
    }
    return size;
}

ItemSize* AlbumsScreen::GetSize( void *item ) {
    ItemSize *result = NULL;
    ActionAndData *data = NULL;
    AlbumsScreenItem *albumItem = static_cast<AlbumsScreenItem *> (item);
    if (albumItem->GetActionAndDates()) {
        data = static_cast<ActionAndData *>(eina_list_nth(albumItem->GetActionAndDates(), 0));
    }
    if(data) {
        GraphObject *object = static_cast<GraphObject*>(data->mData);
        if(object) {
            result = static_cast<ItemSize* > (eina_hash_find(m_ItemSizeCache, object->GetId()));
        }
    }
    return result;
}

bool AlbumsScreen::go_item_comparator(void *itemToCompare, void *itemForCompare) {
    AlbumsScreenItem *albumItem = static_cast<AlbumsScreenItem *> (itemForCompare);
    ActionAndData *firstData = static_cast<ActionAndData *> (eina_list_nth(albumItem->GetActionAndDates(), 0));

    GraphObject *toCompare = static_cast<GraphObject*>(itemToCompare);
    GraphObject *forCompare = static_cast<GraphObject*> (firstData->mData);

    return (toCompare->GetHash() == forCompare->GetHash()) &&
            (strcmp(toCompare->GetId(), forCompare->GetId()) == 0 );
}
