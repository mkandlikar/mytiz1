/*
 * ItemsBuilderAssistent.cpp
 *
 * Created on: Sep 17, 2015
 * Author: Dmitry.Shcherbakov@SymphonyTeleca.com
 */

#include "ItemsBuilderAssistent.h"

#include "Log.h"
#include <dlog.h>
#include "Common.h"

const unsigned int ItemsBuilderAssistent::SWITCH_OFF_HEDER_BUFFERIZATION = 0;

ItemsBuilderAssistent::ItemsBuilderAssistent( unsigned int headerSize )
{
    m_HeaderItems = new Utils::DoubleSidedStack();
    m_HeaderSize = headerSize;
}

ItemsBuilderAssistent::~ItemsBuilderAssistent()
{
    delete m_HeaderItems;
}

void ItemsBuilderAssistent::AddToDelete( void *item )
{
    if ( m_HeaderItems && ( m_HeaderItems->GetSize() < m_HeaderSize )) {
        HideItem( item );
        m_HeaderItems->PushToEnd( item );
    } else {
        DeleteItem( item );
    }
}

void* ItemsBuilderAssistent::AddToCreate( void *item, bool isAddToEnd )
{
    if ( m_HeaderItems && m_HeaderItems->GetSize() ) {
        void *itemToCreate = m_HeaderItems->PopFromEnd();
        if ( IsItemsEqual( item, itemToCreate ) ) {
            ShowItem( itemToCreate );
            return itemToCreate;
        }
        //
        // if items pointers are not equal, push it back to double sided stack
        m_HeaderItems->PushToEnd( itemToCreate );
    }
    return CreateItem( item, isAddToEnd );
}

void ItemsBuilderAssistent::ShowHeader( Utils::DoubleSidedStack *parentItemsStackToCopy )
{
    dlog_print( DLOG_DEBUG, LOG_TAG_FACEBOOK, "ShowHeader" );
    if(m_HeaderItems) {
        while ( m_HeaderItems->GetSize() ) {
            dlog_print( DLOG_DEBUG, LOG_TAG_FACEBOOK, "ShowHeader : show" );
            void *itemToSHow = m_HeaderItems->PopFromEnd();
            ShowItem( itemToSHow );
            parentItemsStackToCopy->PushToBegin( itemToSHow );
        }
    }
}

unsigned int ItemsBuilderAssistent::GetHeaderSize() const
{
    if(m_HeaderItems) {
        return m_HeaderItems->GetSize();
    }
    return 0;
}

void ItemsBuilderAssistent::EraseHeader()
{
    dlog_print( DLOG_DEBUG, LOG_TAG_FACEBOOK, "EraseHeader" );
    if(m_HeaderItems) {
        m_HeaderItems->Clear();
    }
}

void ItemsBuilderAssistent::DeleteLast()
{
    if ( m_HeaderItems && m_HeaderItems->GetSize() ) {
        void *item = m_HeaderItems->PopFromEnd();
        DeleteItem( item );
    }
}

void ItemsBuilderAssistent::PushToBegin( void *item )
{
    if(m_HeaderItems) {
        m_HeaderItems->PushToBegin( item );
    }
}

void* ItemsBuilderAssistent::UpdateItem( void* dataForItem, bool isAddToEnd )
{
	return NULL;
}
