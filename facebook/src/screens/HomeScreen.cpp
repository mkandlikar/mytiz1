#include <efl_extension.h>
#include "Log.h"

#include "SoundNotificationPlayer.h"
#include "Common.h"
#include "ConnectivityManager.h"
#include "CameraRollScreen.h"
#include "PostComposerScreen.h"
#include "ActionBase.h"
#include "FeedAction.h"
#include "FriendOperation.h"
#include "HomeProvider.h"
#include "jsonutilities.h"
#include "HomeScreen.h"
#include "Operation.h"
#include "OperationManager.h"
#include "OperationPresenter.h"
#include "Post.h"
#include "WidgetFactory.h"
#include "Utils.h"
#include "CacheManager.h"
#include "CheckInScreen.h"
#include "BriefHomeProvider.h"
#include "DataUploader.h"
#include "BriefPostInfo.h"
#include "DataEventDescription.h"
#include "Utils.h"
#include "MainScreen.h"
#include "PostPresenter.h"
#include "SimplePostOperation.h"

const unsigned int HomeScreen::DEFAUL_HOME_SCR_ITEMS_WINDOW_SIZE = 50;
const unsigned int HomeScreen::DEFAUL_HOME_SCR_ITEMS_WINDOW_STEP = 4;
const unsigned int HomeScreen::REFRESH_TIMER_LIMIT = 1000000 * 180; // 3 minutes
const double HomeScreen::BRIEF_POSTS_AUTO_UPDATE_TIME = 40;
const unsigned int HomeScreen::ATTEMPTS_TO_UPDATE_LIMIT = 3;

HomeScreen::HomeScreen( ScreenBase *parentScreen ) :
     ScreenBase( parentScreen ), TrackItemsProxyAdapter(NULL, HomeProvider::GetInstance(), 7 )
{
    Log::info( "HomeScreen: create Home screen." );

    mScreenId = ScreenBase::SID_HOME_SCREEN;
    mIsNewStoriesBt = false;
    mBriefUpdater = NULL;
    mLayout = elm_layout_add(getParentMainLayout());
    SetIdentifier("HOME_SCREEN");
    evas_object_size_hint_weight_set(mLayout, 1, 1);
    elm_layout_file_set(mLayout, Application::mEdjPath, "home_page");
    evas_object_show(mLayout);
    ApplyScroller(mLayout, true);

    elm_object_signal_emit(mLayout, "show.set", "spacer");
    elm_object_part_content_set(mLayout, "home_list", GetScroller());
    elm_object_translatable_part_text_set(mLayout, "status_text", "IDS_WRITE_STATUS");
    elm_object_signal_callback_add(mLayout, "status.clicked", "", (Edje_Signal_Cb) on_status_clicked_cb, this);
    elm_object_translatable_part_text_set(mLayout, "photo_text", "IDS_PHOTO_STATUS");
    elm_object_signal_callback_add(mLayout, "photo.clicked", "", (Edje_Signal_Cb) on_photo_clicked_cb, this);
    elm_object_translatable_part_text_set(mLayout, "check_in_text", "IDS_CHECK_IN_STATUS");
    elm_object_signal_callback_add(mLayout, "mouse,clicked,*", "check_in_btn_spacer", on_checkin_clicked_cb, this);
    elm_object_translatable_part_text_set(mLayout, "new_story_btn_text", "IDS_NEW_STORIES");

    mCustomItem = NULL;
    mIsBtnShown = true;
    mAction = new FeedAction(this);
    mEmptyStateCallbackData = new ActionAndData( nullptr, mAction );
    mEmptyStateItem = nullptr;

    // Customisation for progress bar and error
    SetProgressBarLayoutName("nf_progress_bar");

    ProxySettings *settings = GetProxySettings();
    settings->wndSize = DEFAUL_HOME_SCR_ITEMS_WINDOW_SIZE;
    settings->wndStep = DEFAUL_HOME_SCR_ITEMS_WINDOW_STEP;
    SetProxySettings( settings );
    SetPostponedItemDeletion( true );
    SetOperationsComparator( TrackItemsProxyAdapter::operation_comparator );

    SetMuteTimerEnabled( true );

    mObjectDownloaders = nullptr;

    RequestData(true);
    evas_object_smart_callback_add(GetScroller(), "scroll", hide_btns_cb, this);
    CheckForEmptyState();

    OperationManager::GetInstance()->Subscribe(this);

    AppEvents::Get().Subscribe(eLIKE_COMMENT_EVENT, this);
    AppEvents::Get().Subscribe(ePOST_MESSAGE_EVENT, this);
    AppEvents::Get().Subscribe(ePOST_UPDATE_ABORTED, this);
    AppEvents::Get().Subscribe(eINTERNET_CONNECTION_CHANGED, this);
    AppEvents::Get().Subscribe(ePOST_NOT_AVAILABLE, this);
    AppEvents::Get().Subscribe(eUNFOLLOW_COMPLETED, this);
    AppEvents::Get().Subscribe(eFOLLOW_COMPLETED, this);
    AppEvents::Get().Subscribe(ePOST_DELETED, this);
    AppEvents::Get().Subscribe(ePOST_DELETION_CONFIRMED, this);
    AppEvents::Get().Subscribe(eALBUM_RENAMED_EVENT, this);
    AppEvents::Get().Subscribe(eALBUM_DELETED_EVENT, this);

    mMainScreen = reinterpret_cast<MainScreen*>(parentScreen);
    m_AttemptsToUpdateCount = 0;

    mUnfollowActionDataList = NULL;
}

HomeScreen::~HomeScreen()
{
    Log::info( "HomeScreen: destroy Home screen." );
    EraseWindowData();
    OperationManager::GetInstance()->UnSubscribe(this);
    delete mBriefUpdater;
    delete mAction;
    delete mEmptyStateCallbackData;

    mUnfollowActionDataList = eina_list_free(mUnfollowActionDataList);

    void *listData;
    EINA_LIST_FREE(mObjectDownloaders, listData) {
        delete static_cast<GraphObjectDownloader*>(listData);
    }
}

void* HomeScreen::CreateItem( void* dataForItem, bool isAddToEnd )
{
    ActionAndData *data = nullptr;

    if (dataForItem) { //in CreateItem there is no data
        GraphObject *graphObject = static_cast<GraphObject *>(dataForItem);
        if (graphObject->GetGraphObjectType() == GraphObject::GOT_POST) {
            UpdatePost(data, dataForItem, isAddToEnd);
        } else if (graphObject->GetGraphObjectType() == GraphObject::GOT_OPERATION_PRESENTER) {
            OperationPresenter *operation = static_cast<OperationPresenter *>(dataForItem);
            Sptr<SimplePostOperation> oper(operation->GetOperation());
            if  ( oper && (oper->GetType() < Operation::OT_Photo_Post_Create ||
                           oper->GetType() > Operation::OT_Video_Post_Create
                         )//hot fix: should be reworked, have been cut off all PhotoPostOperations
                ) {
                ActionAndData * sharedActionData = oper->GetSharedActionAndPost();
                sharedActionData->mAction = mAction;
            }
            UpdatePresenter(data, operation, isAddToEnd);
        }
    }
    CheckForEmptyState();
    return data;
}

void* HomeScreen::UpdateItem( void* dataForItem, bool isAddToEnd )
{
    assert(dataForItem);
    ActionAndData *data = static_cast<ActionAndData*>(dataForItem);
    assert(data->mData);
    GraphObject *object = static_cast<GraphObject*>(data->mData);

    DeleteUIItem( dataForItem );

    if (object->GetGraphObjectType() == GraphObject::GOT_POST) {
        UpdatePost(data, data->mData, isAddToEnd);
    } else if (object->GetGraphObjectType() == GraphObject::GOT_OPERATION_PRESENTER) {
        OperationPresenter *operation = static_cast<OperationPresenter *>(data->mData);
        UpdatePresenter(data, operation, isAddToEnd);
    } else {
        delete data;
        data = nullptr;
    }
    return data;
}

void* HomeScreen::UpdateSingleItem( void* dataForItem, void* prevItemPtr )
{
    assert(dataForItem);
    ActionAndData *data = static_cast<ActionAndData*>(dataForItem);
    assert(data->mData);
    GraphObject *object = static_cast<GraphObject*>(data->mData);

    assert(prevItemPtr);

    DeleteUIItem( dataForItem );

    if (object->GetGraphObjectType() == GraphObject::GOT_POST) {
        UpdatePost(data, data->mData, false,  static_cast<Evas_Object*>(prevItemPtr));
    } else {
        delete data;
        data = nullptr;
    }
    return data;
}

//private method with common logic
void HomeScreen::UpdatePost(ActionAndData* &data, void* postPtr, bool isAddToEnd, Evas_Object* prevItem) {

    Evas_Object *postWidget = nullptr;
    Post *post = static_cast<Post*>(postPtr);

    Log::info("HomeScreen::UpdatePost()->%s item", data ? "Update" : "Create" );

    if( (!post->GetChildId()) ||
          Utils::IsMe(post->GetFromId().c_str()) || //post from me (could be source of shared post)
          Utils::IsMe(post->GetToId()) //post for me (could be source of shared post)
    ) {
        if(!data) {
            data = new ActionAndData(post, mAction);
        }
        if (post->GetParentId()) {
            postWidget = WidgetFactory::CreateAffectedPostBox(GetDataArea(), data);
        } else if (!post->GetPrivacyIcon().empty()) { // workaround in case of no privacy due to Github #804
            postWidget = WidgetFactory::CreateStatusPost(GetDataArea(), data);
        }
        if (Utils::IsMe(post->GetFromId().c_str()) && post->IsPhotoPost()) {
            StartDownloader(post->GetObjectId(), this, data);
        }
        if (postWidget) {
            data->mParentWidget = postWidget;
            if (OperationManager::GetInstance()->GetActiveOperationById(post->GetId())) {
                WidgetFactory::SetPostActive(data, false);
            }
        } else {
            delete data;
            data = nullptr;
        }

        if (prevItem) {
            elm_box_pack_after(GetDataArea(), postWidget, prevItem);
        } else if (isAddToEnd) {
            elm_box_pack_end(GetDataArea(), postWidget);
        } else {
            elm_box_pack_start(GetDataArea(), postWidget);
            RePackConnectionError();
        }
    }
}

//private method with common logic
void HomeScreen::UpdatePresenter(ActionAndData* &data, OperationPresenter *operation, bool isAddToEnd) {

    Evas_Object *postWidget = nullptr;

    Log::info("HomeScreen::UpdatePresenter()->%s item", data ? "Update" : "Create" );

    if(!data) {
        data = new ActionAndData(operation, mAction);
    }
    postWidget = WidgetFactory::CreatePostOperation( GetDataArea(), data, isAddToEnd );

    if ( postWidget ) {
        WidgetFactory::UpdatePostOperation(data, nullptr);
        data->mParentWidget = postWidget;
    } else {
        delete data;
        data = nullptr;
    }

    if (isAddToEnd) {
        elm_box_pack_end(GetDataArea(), postWidget);
    } else {
        elm_box_pack_start(GetDataArea(), postWidget);
        RePackConnectionError();
    }
}

void HomeScreen::ScrollerReachedTop(bool playSound)
{
    Log::info( LOG_FACEBOOK , "HomeScreen::ScrollerReachedTop" );

    if (!mIsBtnShown) {
        elm_object_signal_emit(mLayout, "show.set", "spacer");
        evas_object_smart_callback_add(GetScroller(), "scroll", hide_btns_cb, this);
        mMainScreen->AllowNewsFeedBtnClick(false);
        ResetUpdatesCount();
        mIsBtnShown = true;
    }
    else{
        ++m_AttemptsToUpdateCount;
    }

    if ( mIsNewStoriesBt ) {
        elm_object_signal_emit(mLayout, "hide.stories", "btn");
        mIsNewStoriesBt = false;
    }

    if(playSound || mIsBtnShown) {
        SoundNotificationPlayer::PlayNotification(SoundNotificationPlayer::eSoundPullToRefreshFast);
    }
}

void HomeScreen::ResetUpdatesCount() {
    Log::info( LOG_FACEBOOK , "HomeScreen::ResetUpdatesCount" );
    m_AttemptsToUpdateCount = 0;
}

void HomeScreen::ShowUpToDatePopup() {
    if(m_AttemptsToUpdateCount >= ATTEMPTS_TO_UPDATE_LIMIT) {
        Log::info( LOG_FACEBOOK , "HomeScreen::ShowUpToDatePopup" );
        Utils::ShowToast(mLayout, i18n_get_text("IDS_NEWS_FEED_IS_UP_TO_DATE"));
        ResetUpdatesCount();
    }
}

void HomeScreen::Update(AppEventId eventId, void * data)
{
    switch(eventId){
    case eLIKE_COMMENT_EVENT:
        Log::info( "HomeScreen::Update, eLIKE_COMMENT_EVENT" );
        if (data) {
            ActionAndData * actionData = static_cast<ActionAndData *>(GetItemById(static_cast<char *>(data)));
            if (actionData) {
                WidgetFactory::RefreshPostItemFooter(actionData);
            }
        }
        break;
    case eDATA_CHANGES_EVENT:
        if (data) {
            Log::info( "HomeScreen::Update, eDATA_CHANGES_EVENT" );
            DataEventDescription *dataEvent = static_cast<DataEventDescription*>(data);
            if ( GetProvider() == dataEvent->GetProvider() ) {
                if ( dataEvent->GetDataEvent() == DataEventDescription::eDE_ITEM_ADDED ) {
                    if ( dataEvent->GetOperationType() == Operation::OT_Share_Post_Create ) {
                        if(!IsRealTopEdgeOfScroller()) {
                            SetDisableShiftUpForPresenter();
                        }
                    }
                }
                TrackItemsProxy::Update(eventId, data);
            }
        }
        break;
    case ePOST_MESSAGE_EVENT:
        Log::info( "HomeScreen::Update, ePOST_MESSAGE_EVENT");
        if (data) {
            ActionAndData * actionData = static_cast<ActionAndData *>(GetItemById(static_cast<char *>(data)));
            if (actionData) {
                WidgetFactory::SetPostActive(actionData, false);
            }

            if (!ConnectivityManager::Singleton().IsConnected()) {
                Utils::ShowToast(getMainLayout(), i18n_get_text("IDS_LOGIN_CONNECT_ERR"));
            }
        }
        break;
    case ePOST_UPDATE_ABORTED:
        if (data) {
            ActionAndData *actionData = static_cast<ActionAndData *>(GetItemById(static_cast<char *>(data)));
            if (actionData) {
                WidgetFactory::SetPostActive(actionData, true);
            }
        }
        break;
    case eOPERATION_COMPLETE_EVENT:// event should comes from OperationManager
        Log::info( "HomeScreen::Update, eOPERATION_COMPLETE_EVENT" );
        if (data) {
            Sptr<Operation> oper(static_cast<Operation*>(data));
            if( oper->IsFeedOperation() ) {
                HomeScreen::FetchEdgeItems();
            }
        }
        break;
    case ePOST_DELETED:
    case ePOST_DELETION_CONFIRMED:
    case ePOST_NOT_AVAILABLE:
    case eALBUM_DELETED_EVENT:
        if (data) {
            Log::info( "HomeScreen::Update, %s", eventId == ePOST_NOT_AVAILABLE ? "ePOST_NOT_AVAILABLE" : "ePOST_DELETED");
            HomeProvider* provider = dynamic_cast<HomeProvider*>(GetProvider());
            assert(provider);
            char *postId = static_cast<char*>(data);
            Eina_List *postsToRemove = nullptr;
            if (eventId == eALBUM_DELETED_EVENT) {
                postsToRemove = provider->GetPostsByAlbumId(postId);
            } else {
                Post* post = provider->GetPostById(postId);
                if (post) { //ToDo: check why it can be NULL. Sometimes this happens...
                    postsToRemove = eina_list_append(postsToRemove, post);
                }
            }
            Eina_List *list;
            void *data;
            EINA_LIST_FOREACH(postsToRemove, list, data) {
                Post *post = static_cast<Post*>(data);
                assert (post);
                Log::info( "HomeScreen::Update, delete post with id = %s",postId );
                if (eventId != ePOST_DELETION_CONFIRMED){
                    DestroyItemByData(post , TrackItemsProxyAdapter::items_comparator_Obj_vs_ActionAndData);
                }
                if (eventId != ePOST_DELETED){
                    HomeProvider::GetInstance()->DeleteItem(post);
                }
                CheckForEmptyState();
            }
            eina_list_free(postsToRemove);
        }
        break;
    case eINTERNET_CONNECTION_CHANGED:
        Log::info( "HomeScreen::Update, eINTERNET_CONNECTION_CHANGED" );
        if (ConnectivityManager::Singleton().IsConnected()) {  //connection has been restored
            ShowProgressBarWithTimer(IsServiceWidgetOnBottom());
            HomeScreen::FetchEdgeItems();
        }
        else{
            ShowConnectionError(false);
        }
        break;
    case eUNFOLLOW_COMPLETED:
        if (data) {
            ActionAndData * actionData = static_cast<ActionAndData *>(GetItemById(static_cast<char *>(data)));
            if (actionData) {
                CreateUnfollowWidget(actionData);
            }
        }
        break;
    case eFOLLOW_COMPLETED:
        if (data) {
            ActionAndData * actionData = static_cast<ActionAndData *>(GetItemById(static_cast<char *>(data)));
            if (actionData) {
                CancelUnfollowWidget(actionData);
            }
        }
        break;
    case eALBUM_RENAMED_EVENT:
    {
        Eina_List *albumPosts = static_cast<HomeProvider *> (GetProvider())->GetPostsByAlbumId(static_cast<char *>(data));
        Eina_List *list;
        void *data;
        EINA_LIST_FOREACH(albumPosts, list, data) {
            Post *post = static_cast<Post*>(data);
            assert (post);
            ActionAndData *actionData = static_cast<ActionAndData *>(GetItemById(post->GetId()));
            Evas_Object *title = elm_layout_content_get(actionData->mHeader, "user_name");
            TO_STORY_TRNSLTD_TEXT_SET(title, eSTORY, *post);
        }
        eina_list_free(albumPosts);
        break;
    }
    default:
        TrackItemsProxy::Update(eventId, data);
    }

    CheckForEmptyState();

    if ( !mBriefUpdater ) {
        mBriefUpdater = new BriefUpdater( this );
        mBriefUpdater->Start();
    }
}

void HomeScreen::NewItemsAvailable()
{
    Log::info( "HomeScreen: NewItemsAvailable" );
    if ( !mIsNewStoriesBt ) {
        elm_object_signal_emit(mLayout, Application::IsRTLLanguage() ? "show.stories.rtl" : "show.stories.ltr", "btn");
        AllowNewStoriesBtnClick( true );
        mIsNewStoriesBt = true;
    }
}

bool HomeScreen::NoVisualData()
{
    return HomeProvider::GetInstance()->GetDataCount() == 0;
    //no data in provider or invisible items only...see Github 804, some items are missed
}

void HomeScreen::FetchEdgeItems()
{
    Log::info(LOG_FACEBOOK, "HomeScreen::FetchEdgeItems" );
    if (eina_list_count(HomeProvider::GetInstance()->GetOperationPresenters()) == 0) {
       if (NoVisualData())
       {
           HomeProvider::GetInstance()->EraseData();
           Log::info(LOG_FACEBOOK, "HomeScreen::FetchEdgeItems->no data in provider, descending order request");
           RequestData(true);
       }
       else {
           HomeProvider::GetInstance()->ResetRequestTimer();
           Log::info(LOG_FACEBOOK, "HomeScreen::FetchEdgeItems->request only muted(new) items");
           RequestData(false, true);
       }
    }
    else{
        Log::error(LOG_FACEBOOK, "HomeScreen::FetchEdgeItems->can't fetch due in-progress items" );
    }
}

void HomeScreen::DeleteFindFriendsItem (){
    if(mEmptyStateItem) {
        elm_box_unpack( GetDataArea(), mEmptyStateItem );
        evas_object_hide( mEmptyStateItem );
        evas_object_del( mEmptyStateItem );
        mEmptyStateItem = nullptr;
    }
}

void HomeScreen::hide_btns_cb(void *data, Evas_Object *obj, void *event_info)
{
    HomeScreen *screen = static_cast<HomeScreen *>(data);
    Evas_Coord y;
    elm_scroller_region_get( screen->GetScroller(), NULL, &y, NULL, NULL );
    if ( y > TrackItemsProxy::TINY_SCROLL_PADDING ) {
        elm_object_signal_emit(screen->mLayout, "hide.set", "spacer");
        evas_object_smart_callback_del(screen->GetScroller(), "scroll", hide_btns_cb);
        screen->mIsBtnShown = false;
        screen->mMainScreen->AllowNewsFeedBtnClick(true);
        Log::debug( LOG_FACEBOOK , "HomeScreen::hide_btns_cb hide bottom menu");
    }
}

void HomeScreen::on_status_clicked_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    Log::info( "Status clicked");
    HomeScreen *me = static_cast<HomeScreen*>(user_data);
    PostComposerScreen *screen = new PostComposerScreen(me, eDEFAULT, nullptr, nullptr, nullptr, nullptr, false);
    Application::GetInstance()->AddScreen(screen);
}

void HomeScreen::on_photo_clicked_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_CAMERA_ROLL) {
        Log::debug( "Photo clicked");
        CameraRollScreen *screen = new CameraRollScreen(eCAMERA_ROLL_CREATE_PHOTO_POST_MODE);
        Application::GetInstance()->AddScreen(screen);
    }
}

void HomeScreen::on_checkin_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source) {
    if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_CHECKIN) {
        Log::debug(  "Check-in clicked" );
        HomeScreen *me = static_cast<HomeScreen*>(data);
        CheckInScreen::OpenScreen(me, CheckInScreen::ENotFromPostComposer);
    }
}

void HomeScreen::new_stories_click_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::debug( "HomeScreen: new_stories_hide_cb" );
    HomeScreen *screen = static_cast<HomeScreen*>(data);
    if (screen){
        screen->AllowNewStoriesBtnClick(false);
        elm_object_signal_emit(screen->mLayout, "hide.stories", "btn");
        screen->mIsNewStoriesBt = false;
        screen->AnimatedTop();
    }
}

void HomeScreen::NotifyUpdate()
{
    Log::info( "HomeScreen::NotifyUpdate" );
    unsigned int index = 0;
    void* item;
    Eina_Array_Iterator iterator;
    EINA_ARRAY_ITER_NEXT( BriefHomeProvider::GetInstance()->GetData(), index, item, iterator ) {
        char * objectId = SAFE_STRDUP(static_cast<BriefPostInfo*>( item )->GetId());
        Update( eLIKE_COMMENT_EVENT, objectId );
        free(objectId);
    }
}

/**********************************************************************
 *
 * @class BriefUpdater
 *
 **********************************************************************/

HomeScreen::BriefUpdater::BriefUpdater( HomeScreen *parent )
{
    Log::info( "HomeScreen::BriefUpdater::BriefUpdater" );
    m_Parent = parent;
    m_BriefTimer = NULL;
    m_Uploader = new DataUploader( BriefHomeProvider::GetInstance(), brief_uploading_done_cb, this );
}

HomeScreen::BriefUpdater::~BriefUpdater()
{
    if ( m_BriefTimer ) {
        ecore_timer_del( m_BriefTimer );
    }
    delete m_Uploader;
}

void HomeScreen::BriefUpdater::Start()
{
    Log::info( "HomeScreen::BriefUpdater::Start" );
    if ( !m_BriefTimer ) {
        m_BriefTimer = ecore_timer_add( BRIEF_POSTS_AUTO_UPDATE_TIME, brief_auto_update_cb, this );
    }
}

void HomeScreen::BriefUpdater::brief_uploading_done_cb( void *parent )
{
    if ( BriefHomeProvider::GetInstance()->GetDataCount() > 0 ) {
        Log::debug( "BriefUpdater::brief_uploading_done_cb, some items have been changed." );
        HomeScreen::BriefUpdater *brief = static_cast<HomeScreen::BriefUpdater*>( parent );
        brief->m_Parent->NotifyUpdate();
    } else {
        Log::debug( "BriefUpdater::brief_uploading_done_cb, none items have been changed." );
    }
}

Eina_Bool HomeScreen::BriefUpdater::brief_auto_update_cb( void *data )
{
    // we must not request data if a scoller contains cached data
    if ( !HomeProvider::GetInstance()->IsDataCached() ) {
        HomeScreen::BriefUpdater *briefUpdater = static_cast<HomeScreen::BriefUpdater*>( data );
        Eina_Array *ids = briefUpdater->m_Parent->GetItemsIds();
        if ( ids ) {
            BriefHomeProvider::GetInstance()->EraseData();
            if ( BriefHomeProvider::GetInstance()->SetIds( ids ) ) {
                Log::info( "HomeScreen::BriefUpdater::brief_auto_update_cb, ids array has been applied." );
                briefUpdater->m_Uploader->StartUploading();
            } else {
                Log::info( "HomeScreen::BriefUpdater::brief_auto_update_cb, ids array has not been applied." );
                unsigned int index = 0;
                void* item;
                Eina_Array_Iterator iterator;
                EINA_ARRAY_ITER_NEXT( ids, index, item, iterator ) {
                    free( item );
                }
            }
            eina_array_free( ids );
            briefUpdater->m_Parent->CheckForEmptyState();
        }
    } else {
        Log::debug( "HomeScreen::brief_auto_update_cb, too early to request data; a scroller contains cached items." );
    }
    return ECORE_CALLBACK_RENEW;
}

void HomeScreen::AllowNewStoriesBtnClick(bool allow)
{
    if(allow)
    {
        Log::debug( LOG_FACEBOOK , "HomeScreen::AllowNewStoriesBtnClick Enable click");
        elm_object_signal_callback_add(mLayout, "got.stories", "item", new_stories_click_cb, this);
    }
    else
    {
        Log::debug( LOG_FACEBOOK , "HomeScreen::AllowNewStoriesBtnClick Disable click");
        elm_object_signal_callback_del(mLayout, "got.stories", "item", new_stories_click_cb);
    }
}

void HomeScreen::CheckForEmptyState()
{
    Log::info("HomeScreen::CheckForEmptyState() %sshow Find Frinds", NoVisualData() ? "": "don't ");
    if (NoVisualData()) {
        if(!mEmptyStateItem) {
            Log::info("HomeScreen::CheckForEmptyState() create <You Friends Are Waiting>");
            mEmptyStateItem = elm_layout_add(GetDataArea());
            elm_layout_file_set(mEmptyStateItem, Application::mEdjPath, "nuf_lo_res");
            evas_object_size_hint_weight_set(mEmptyStateItem, 1, 1);
            evas_object_size_hint_align_set(mEmptyStateItem, -1, 0);
            elm_object_translatable_part_text_set(mEmptyStateItem, "nuf_caption", "IDS_FRIENDS_ARE_WAITING_CAPTION");
            elm_object_translatable_part_text_set(mEmptyStateItem, "nuf_message", "IDS_FRIENDS_ARE_WAITING_MESSAGE");
            elm_object_translatable_part_text_set(mEmptyStateItem, "nuf_btn_text", "IDS_FRIENDS_ARE_WAITING_BUTTON");
            elm_box_pack_start(GetDataArea(), mEmptyStateItem);
            evas_object_show(mEmptyStateItem);
            elm_object_signal_callback_add(mEmptyStateItem, "nuf.clicked", "nuf_*",
                mAction->on_new_user_friends_clicked_cb, mEmptyStateCallbackData);
            ApplyProgressBar(true);
        }
    } else {
        Log::info("HomeScreen::CheckForEmptyState() delete <You Friends Are Waiting>");
        DeleteFindFriendsItem();
    }
}

void HomeScreen::FollowUser(ActionAndData *actionData)
{
    Post *post = static_cast<Post*>(actionData->mData);

    Evas_Object *unfollowItem = static_cast<Evas_Object*>(evas_object_data_get(actionData->mParentWidget, "unfollow_widget"));
    evas_object_data_set(actionData->mParentWidget, "undo", unfollowItem);
    evas_object_data_del(actionData->mParentWidget, "unfollow_widget");

    Evas_Object *layout = elm_layout_add(unfollowItem);
    elm_layout_file_set(layout, Application::mEdjPath, "progress_bar_wrapper");
    elm_object_part_content_set(unfollowItem, "grey_unfollow_wrap", layout);

    Evas_Object * progressBar = elm_progressbar_add(layout);
    elm_object_part_content_set(layout, "progressbar", progressBar);
    elm_object_style_set(progressBar, "process_medium");
    evas_object_size_hint_align_set(progressBar, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_size_hint_weight_set(progressBar, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    elm_progressbar_pulse_set(progressBar, EINA_TRUE);
    elm_progressbar_pulse(progressBar, EINA_TRUE);
    evas_object_resize(progressBar, 80, 80);
    evas_object_show(progressBar);

    const char * userId = post->GetIsGroupPost() ? post->GetToId() : post->GetFromId().c_str();

    OperationManager::GetInstance()->AddOperation(MakeSptr<FriendOperation>(Operation::OT_Follow, userId, post->GetId()));
}

void HomeScreen::CreateUnfollowWidget(ActionAndData *actionData) {
    Post *post = static_cast<Post*>(actionData->mData);

    Evas_Coord y, h;
    evas_object_geometry_get(actionData->mParentWidget, nullptr, &y, nullptr, &h);

    if (y + h > 0) {
        if (y > R->SCREEN_SIZE_HEIGHT) {
            UnpackAssociatedWithItemRect(actionData->mParentWidget);
        }
        Evas_Object *unfollowItem = elm_layout_add(GetDataArea());
        elm_layout_file_set(unfollowItem, Application::mEdjPath, "unfollow_item");
        evas_object_size_hint_weight_set(unfollowItem, 1, 1);
        evas_object_size_hint_align_set(unfollowItem, -1, 0);

        Evas_Object *layout = elm_layout_add(unfollowItem);
        elm_object_part_content_set(unfollowItem, "unfollow_spacer", layout);
        elm_layout_file_set(layout, Application::mEdjPath, "unfollow_image");
        if(R->SCREEN_SIZE_WIDTH == 720)
            elm_object_signal_emit(layout, "show_item_720", "");

        Evas_Object *unfollowText = elm_label_add(unfollowItem);
        elm_object_part_content_set(unfollowItem, "unfollow_text", unfollowText);
        elm_label_ellipsis_set(unfollowText, EINA_TRUE);
        evas_object_size_hint_weight_set(unfollowText, 1, 1);
        evas_object_size_hint_align_set(unfollowText, -1, 0);

        std::string fromName = post->GetFromName();
        const char *userName = post->GetIsGroupPost() ? post->GetToName() : fromName.c_str();
        const char *markupText = elm_entry_utf8_to_markup(userName);

        FRMTD_TRNSLTD_TXT_SET3(unfollowText, SANS_9197a3_FORMAT, R->UNFOLLOW_TEXT_FONT_SIZE, "IDS_UNFOLLOWED_FRIEND", markupText);
        evas_object_show(unfollowText);
        free((void *)markupText);

        elm_object_translatable_part_text_set(unfollowItem, "undo_text", "IDS_UNDO");
        elm_object_signal_callback_add(unfollowItem, "undo_unfollow", "undo", undo_unfollow_clicked, actionData);

        elm_box_pack_after(GetDataArea(), unfollowItem, actionData->mParentWidget);
        evas_object_hide(actionData->mParentWidget);
        elm_box_unpack(GetDataArea(), actionData->mParentWidget);
        evas_object_show(unfollowItem);

        evas_object_data_set(actionData->mParentWidget, "unfollow_widget", unfollowItem);
        mUnfollowActionDataList = eina_list_append(mUnfollowActionDataList, actionData);
    } else {
        DeleteUnfollowPost(actionData);
        DeleteAssociatedWithItemRect(actionData->mParentWidget);
    }
}

void HomeScreen::undo_unfollow_clicked(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *actionData = static_cast<ActionAndData*>(data);
    HomeScreen *screen = static_cast<HomeScreen*>(actionData->mAction->getScreen());
    screen->FollowUser(actionData);
}

void HomeScreen::CancelUnfollowWidget(ActionAndData *actionData)
{
    Evas_Object *unfollowItem = static_cast<Evas_Object*>(evas_object_data_get(actionData->mParentWidget, "undo"));
    Evas_Coord y, h;
    evas_object_geometry_get(unfollowItem, nullptr, &y, nullptr, &h);
    if (y + h < 0 || y > R->SCREEN_SIZE_HEIGHT) {
        UnpackAssociatedWithItemRect(unfollowItem);
    }

    elm_box_pack_after(GetDataArea(), actionData->mParentWidget, unfollowItem);
    evas_object_hide(unfollowItem);
    elm_box_unpack(GetDataArea(), unfollowItem);
    evas_object_show(actionData->mParentWidget);

    WidgetFactory::SetPostActive(actionData, true);

    evas_object_data_del(actionData->mParentWidget, "undo");
    evas_object_del(unfollowItem);
    mUnfollowActionDataList = eina_list_remove(mUnfollowActionDataList, actionData);
}

void HomeScreen::CheckOffStageUnfollowItems() 
{
    Eina_List *l;
    Eina_List *l_next;
    void *list_data;
    EINA_LIST_FOREACH_SAFE(mUnfollowActionDataList, l, l_next, list_data) {
        ActionAndData *actionData = static_cast<ActionAndData*>(list_data);
        Evas_Object *unfollowItem = static_cast<Evas_Object*>(evas_object_data_get(actionData->mParentWidget, "unfollow_widget"));
        if (unfollowItem) {
            Evas_Coord y, h;
            evas_object_geometry_get(unfollowItem, nullptr, &y, nullptr, &h);
            if (y + h < 0) {
                evas_object_data_del(actionData->mParentWidget, "unfollow_widget");
                DeleteAssociatedWithItemRect(unfollowItem);
                evas_object_del(unfollowItem);
                DeleteUnfollowPost(actionData);
                mUnfollowActionDataList = eina_list_remove(mUnfollowActionDataList, actionData);
            }
        }
    }
}

void HomeScreen::DeleteUnfollowPost(ActionAndData *postData)
{
    if (ActionAndData::IsAlive(postData)) {
        GraphObject *object = static_cast<GraphObject*>(postData->mData);
        HomeProvider::GetInstance()->DeleteItem(object);
        DestroyItem(postData, TrackItemsProxyAdapter::items_comparator);
    }
}

void HomeScreen::StartDownloader(const char *id, IGraphObjectDownloaderObserver *observer, ActionAndData *data)
{
    GraphObjectDownloader *objectDownloader = new GraphObjectDownloader(id, this, data);
    mObjectDownloaders = eina_list_append(mObjectDownloaders, objectDownloader);
    objectDownloader->StartDownloading();
}

void HomeScreen::DownloaderProgress(IGraphObjectDownloaderObserver::Result result, GraphObjectDownloader *downloader) {
    switch (result) {
    case IGraphObjectDownloaderObserver::EAlbumDownloaded:
        if (downloader) {
            ActionAndData *actionData = downloader->GetGraphObjectData();
            if(ActionAndData::IsAlive(actionData)) {
                //check could be useful if cached posts are replaced with new
                Post *post = static_cast<Post*>(actionData->mData);
                Album *album = static_cast<Album*>(downloader->GetGraphObject());
                post->SetAlbum(album);

                if (post->IsPostEditable()) {
                    elm_object_signal_emit(actionData->mHeader, "context.show", "context");
                }
                if (post->IsPhotoAddedToAlbum()) {
                    Evas_Object *title = elm_layout_content_get(actionData->mHeader, "user_name");
                    TO_STORY_TRNSLTD_TEXT_SET(title, eSTORY, *post);
                }
            }
            mObjectDownloaders = eina_list_remove(mObjectDownloaders, downloader);
            delete downloader;
        }
        break;
    default:
        break;
    }
}
