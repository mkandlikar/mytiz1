#include "AbstractDataProvider.h"
#include "ActionBase.h"
#include "ImagesCarouselScreen.h"
#include "Log.h"
#include "Photo.h"
#include "PhotoGalleryShowCarouselStrategy.h"

#include <cassert>


static const char* LogTag = "PhotoGalleryShowCarouselStrategy";

/*
 * Class PhotoGalleryShowCarouselStrategy
 */
PhotoGalleryShowCarouselStrategy::PhotoGalleryShowCarouselStrategy() {
    Log::info_tag(LogTag, "PhotoGalleryShowCarouselStrategy: create Photo Gallery screen." );
}

PhotoGalleryShowCarouselStrategy::~PhotoGalleryShowCarouselStrategy() {
    Log::info_tag(LogTag, "PhotoGalleryShowCarouselStrategy: destroy PhotoGalleryShowCarouselStrategy screen." );
}

void PhotoGalleryShowCarouselStrategy::PhotoSelected(Photo* photo) {
    Log::debug_tag(LogTag, "PhotoGalleryShowCarouselStrategy::PhotoClicked(photo)");
    Eina_List *photoList = NULL;
    int currentPage = 0;
    int j = 0;

    Eina_Array* loadedPhotos = mScreen->GetProvider()->GetData();
    for (int i = 0; i < eina_array_count(loadedPhotos); ++i){
        GalleryItem *item = static_cast<GalleryItem*> (eina_array_data_get(loadedPhotos, i));
        assert(item);
        if (item) {
            Eina_List *list;
            void *data;
            EINA_LIST_FOREACH(item->GetActionAndDates(), list, data) {
                ActionAndData *actionAndData = static_cast<ActionAndData *> (data);
                assert(actionAndData);
                if (actionAndData) {
                    Photo *albumPhoto = static_cast<Photo*> (actionAndData->mData);
                    assert(albumPhoto);
                    if (albumPhoto) {
                        photoList = eina_list_append(photoList, albumPhoto);
                        if (albumPhoto == photo) {
                            currentPage = j;
                        }
                        ++j;
                    }
                }
            }
        }
    }

    ImagesCarouselScreen* carouselScreen = new ImagesCarouselScreen(photoList, currentPage, ImagesCarouselScreen::eDATA_MODE);
    eina_list_free(photoList);
    Application::GetInstance()->AddScreen(carouselScreen);
}
