#include "Common.h"
#include "Config.h"
#include "SignUpInProgressScreen.h"
#include "LoginRequest.h"
#include "LogoutRequest.h"

SignUpInProgressScreen::SignUpInProgressScreen(ScreenBase *parentScreen):ScreenBase(parentScreen)  {
    mScreenId = ScreenBase::SID_INPROGRESS;
    mGraphRequest = NULL;
    mInProgress = false;
    mRequestType = UndefinedReq;
    CreateView();
    mLogOutReq = nullptr;
}

SignUpInProgressScreen::~SignUpInProgressScreen() {
    CancelGraphRequest();
    delete mLogOutReq;
}

void SignUpInProgressScreen::LoadEdjLayout() {
    mLayout = elm_layout_add(getParentMainLayout());
    elm_layout_file_set(mLayout, Application::mEdjPath, "signup_inprogress_screen");
    evas_object_size_hint_weight_set(mLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_show(mLayout);
}

void SignUpInProgressScreen::CreateView() {
    LoadEdjLayout();

    Evas_Object *ProgressBar = elm_progressbar_add(mLayout);
    elm_object_style_set(ProgressBar, "process_medium");
    elm_object_part_content_set(mLayout, "signup_inprogress_swallow", ProgressBar);
    evas_object_size_hint_align_set(ProgressBar, EVAS_HINT_FILL, 0.5);
    evas_object_size_hint_weight_set(ProgressBar, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    elm_progressbar_pulse_set(ProgressBar, EINA_TRUE);
    elm_progressbar_pulse(ProgressBar, EINA_TRUE);
    evas_object_show(ProgressBar);
}

void SignUpInProgressScreen::Push()
{
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void SignUpInProgressScreen::CancelGraphRequest()
{
    if( mInProgress && mGraphRequest )
    {
        switch(mRequestType)
        {
            case LoginReq:
            {
                LoginRequest* GraphRequestLogin = static_cast<LoginRequest*>(mGraphRequest);
                GraphRequestLogin->Cancel();
            }
            break;

            default:
            break;
        }
    }
}

bool SignUpInProgressScreen::HandleBackButton()
{
    // Hide the Applicaton window, if the request type is LogOut Request.
    // As if we cancel the logOut request, by that time FB server may invalidate the Session Key, access token.
    // Once the access token is invalid, we can not do any operation from the App.

    // Cancel the ongoing request, if the request is Login Request

    if( LogOutReq == mRequestType )
    {
        Application::GetInstance()->GoToBackground();
        return true;
    }
    else
    {
        if (!mInProgress) {
            mLogOutReq = new LogoutRequest(on_logout_completed, this);
            mLogOutReq->DoLogout();
            LogoutRequest::RedirectToLogin(nullptr);
        }
        return false;
    }
}

void SignUpInProgressScreen::on_logout_completed(void* object, char* respond, int code)
{
    if (Application::GetInstance()->mDataService) {
        Application::GetInstance()->mDataService->clearDownloadRequest();
    }

    AppEvents::Get().Notify(eACCOUNT_CHANGED, nullptr);
}

