#include "AbstractDataProvider.h"
#include "ActionBase.h"
#include "Common.h"
#include "GraphObject.h"
#include "IdleGarbageManager.h"
#include "Log.h"
#include "TrackItemsProxyAdapter.h"

#include <assert.h>
#include <efl_extension.h>
#include <dlog.h>
#include <Ecore_Legacy.h>

const unsigned int TrackItemsProxyAdapter::MAX_STORED_ITEMS = 75;
const unsigned int TrackItemsProxyAdapter::PORTION_SIZE = 5;

TrackItemsProxyAdapter::TrackItemsProxyAdapter( Evas_Object* parentLayout, AbstractDataProvider *provider, unsigned int headerSize, ItemsCache* itemsCache ) :
    TrackItemsProxy( parentLayout, provider, go_item_comparator, headerSize, itemsCache )
{
    m_PostponedItemDeletion = false;
    m_PendingItemsToDelete = new Utils::DoubleSidedStack();
}

TrackItemsProxyAdapter::~TrackItemsProxyAdapter()
{
    if ( m_PendingItemsToDelete->GetSize() ) {
        IdleGarbageManager::GetInstance()->AppendItemsStack( m_PendingItemsToDelete );
    }
    delete m_PendingItemsToDelete;
}

void TrackItemsProxyAdapter::HideItem( void *item )
{
    ActionAndData *data = static_cast<ActionAndData*>(item);

    if ( data && data->mParentWidget ) {
        Evas_Object *wgt = data->mParentWidget;
        evas_object_hide( wgt );
        elm_box_unpack( GetDataArea(), wgt );
    }
}

void TrackItemsProxyAdapter::ShowItem( void *item )
{
    ActionAndData *data = static_cast<ActionAndData*>(item);
    if(data->mParentWidget) {
        elm_box_pack_start( GetDataArea(), data->mParentWidget );
        evas_object_show( data->mParentWidget );
    }
}

bool TrackItemsProxyAdapter::IsItemsEqual( void *itemToCompare, void *itemForCompare )
{
    GraphObject *toCompare = static_cast<GraphObject*>(itemToCompare);
    GraphObject *forCompare = (GraphObject*)(static_cast<ActionAndData*>(itemForCompare))->mData;
    return ( toCompare->GetHash() == forCompare->GetHash() ) &&
            ( strcmp( toCompare->GetId(), forCompare->GetId() ) == 0 );
}

void TrackItemsProxyAdapter::DeleteItemFromGarbageManager()
{
    IdleGarbageManager::Item *item = static_cast<IdleGarbageManager::Item*>(m_PendingItemsToDelete->PopFromBegin());
    switch ( item->type )
    {
        case IdleGarbageManager::ACTION_AND_DATA_ITEM:
        {
            ActionAndData *actionAndData = static_cast<ActionAndData*>(item->data);
            evas_object_del( actionAndData->mParentWidget );
            delete actionAndData;
        } break;
        case IdleGarbageManager::EVAS_ITEM:
        {
            Evas_Object *evasItem = static_cast<Evas_Object*>(item->data);
            evas_object_del( evasItem );
        } break;
        default:
        {
            Log::error( "TrackItemsProxyAdapter::DeleteItemFromGarbageManager, Undetermined item type to delete." );
        }
    }
}

void TrackItemsProxyAdapter::IdleStateHandler()
{
    if ( m_PendingItemsToDelete->GetSize() > GetMaxWindowSize()/2 )
    {
        for (int i=0; i<PORTION_SIZE ;i++)//delete portion of items simultaneously
        {
            DeleteItemFromGarbageManager();
        }
    }
    else if( m_PendingItemsToDelete->GetSize() > 0 )
    {
        DeleteItemFromGarbageManager();
    }
}

bool TrackItemsProxyAdapter::IsItemInDataArea(void* item) {
    assert(item);
    bool ret = false;
    ActionAndData *data = static_cast<ActionAndData*>(item);
    if (data->mParentWidget) {
        Eina_List* children = elm_box_children_get(GetDataArea());
        ret = eina_list_data_find(children, data->mParentWidget);
        eina_list_free(children);
    }
    return ret;
}

// in some cases (especially for presenters) item with ActionAndData should be removed at once
// and such item should not be moved into Garbage Manager
Evas_Coord TrackItemsProxyAdapter::DeleteItemDirectly( void* item )
{
    assert (item);
    HideItem( item );
    Evas_Coord height = 0;
    ActionAndData *data = static_cast<ActionAndData*>(item);
    if(data->mParentWidget) {
        height = DeleteAssociatedWithItemRect(data->mParentWidget);
        evas_object_del( data->mParentWidget );
    }
    delete data;
    return height;
}

void TrackItemsProxyAdapter::DeleteUIItem( void* item )
{
    if (IsItemInDataArea(item)) { //item may be already killed somewhere else (e.g. DeleteAll_Prepare()).
        HideItem( item );
        ActionAndData *data = static_cast<ActionAndData*>(item);
        unsigned int waitingToDelete = m_PendingItemsToDelete->GetSize() + IdleGarbageManager::GetInstance()->WaitingForDelete();
        if( m_PostponedItemDeletion && waitingToDelete < MAX_STORED_ITEMS ) {
            IdleGarbageManager::Item *toDelete = new IdleGarbageManager::Item;
            toDelete->type = IdleGarbageManager::EVAS_ITEM;
            toDelete->data = data->mParentWidget;
            m_PendingItemsToDelete->PushToBegin( (void*)toDelete );
        } else if(data && data->mParentWidget) {
            Log::debug("TrackItemsProxyAdapter::DeleteUIItem");
            evas_object_del( data->mParentWidget );
            data->mParentWidget = NULL;
        }
    }
}

void TrackItemsProxyAdapter::DeleteItem( void* item )
{
    HideItem( item );
    unsigned int waitingToDelete = m_PendingItemsToDelete->GetSize() + IdleGarbageManager::GetInstance()->WaitingForDelete();
    if( m_PostponedItemDeletion && waitingToDelete < MAX_STORED_ITEMS ) {

        IdleGarbageManager::Item *toDelete = new IdleGarbageManager::Item;
        toDelete->type = IdleGarbageManager::ACTION_AND_DATA_ITEM;
        toDelete->data = item;

        m_PendingItemsToDelete->PushToBegin( (void*)toDelete );
    } else {
        ActionAndData *data = static_cast<ActionAndData*>(item);
        if(data && data->mParentWidget) {
            Log::debug("TrackItemsProxyAdapter::DeleteItem");
            DeleteAssociatedWithItemRect(data->mParentWidget);
            evas_object_del( data->mParentWidget );
        }
        delete data;
    }
}

void TrackItemsProxyAdapter::DeleteAll_Prepare() {
    if(GetDataArea()) {
        elm_box_clear( GetDataArea() );
    }
}

void TrackItemsProxyAdapter::DeleteAll_ItemDelete(void* item ) {
    ActionAndData *data = static_cast<ActionAndData*>(item);
    delete data;
}

ItemSize* TrackItemsProxyAdapter::GetItemSize( void* item )
{
    ItemSize *res = NULL;
    ActionAndData *data = static_cast<ActionAndData*>(item);
    if(data && data->mParentWidget) {
        GraphObject *object = static_cast<GraphObject*>(data->mData);
        Evas_Coord x, y, weight, height;
        evas_object_geometry_get( data->mParentWidget, &x, &y, &weight, &height );
        res = new ItemSize( SAFE_STRDUP(object->GetId()),height, weight );
    }
    return res;
}

bool TrackItemsProxyAdapter::items_comparator( void *itemToCompare, void *itemForCompare )
{
    ActionAndData *dataToCompare = static_cast<ActionAndData*>(itemToCompare);
    ActionAndData *dataForCompare = static_cast<ActionAndData*>(itemForCompare);

    GraphObject *toCompare = dataToCompare->mData;
    GraphObject *forCompare = dataForCompare->mData;

    return ( toCompare->GetHash() == forCompare->GetHash() ) &&
            ( strcmp( toCompare->GetId(), forCompare->GetId() ) == 0 );
}

bool TrackItemsProxyAdapter::items_comparator_by_ptr( void *itemToCompare, void *itemForCompare ) {
    return ( itemToCompare == itemForCompare );
}

bool TrackItemsProxyAdapter::items_comparator_Obj_vs_ActionAndData( void *itemToCompare, void *itemForCompare ) {
    if (!itemToCompare) return false;

    GraphObject *toCompare = static_cast<GraphObject*>(itemToCompare);

    ActionAndData *dataForCompare = static_cast<ActionAndData*>(itemForCompare);
    GraphObject *forCompare = dataForCompare->mData;

    return ( toCompare->GetHash() == forCompare->GetHash() ) &&
            ( strcmp( toCompare->GetId(), forCompare->GetId() ) == 0 );
}

char* TrackItemsProxyAdapter::GetItemId( void *item )
{
    assert( item );
    char* res = NULL;
    ActionAndData *data = static_cast<ActionAndData*>(item);
    if(data && data->mData) {
        res = SAFE_STRDUP( data->mData->GetId() );
    }
    return res;
}

bool TrackItemsProxyAdapter::operation_comparator( void *itemToCompare, void *itemForCompare ) {
    GraphObject *toCompare = static_cast<GraphObject*>(itemToCompare);

    ActionAndData *dataForCompare = static_cast<ActionAndData*>(itemForCompare);
    GraphObject *forCompare = dataForCompare->mData;

    if ( *toCompare == *forCompare ) {
        toCompare->AddRef();
        return true;
    }
    return false;
}

void TrackItemsProxyAdapter::UpdateDataForItem(void *item) {
    ActionAndData *actData = static_cast<ActionAndData*>(item);
    GraphObject *obj = actData ? actData->mData : nullptr;
    if (obj) {
        GraphObject *newObj = static_cast<GraphObject *> (GetProvider()->GetGraphObjectById(obj->GetId()));
        if (newObj && obj != newObj) { //if Graph object (e.g.) has been already updated in the Provider.
            actData->ReSetData(newObj);
        }
    }
}

void* TrackItemsProxyAdapter::GetDataForItem(void *item) {
    assert(item);
    ActionAndData *actData = static_cast<ActionAndData*>(item);
    return actData ? actData->mData : NULL;
}

void* TrackItemsProxyAdapter::GetWgtForItem(void *item) {
    assert(item);
    ActionAndData *actData = static_cast<ActionAndData*>(item);
    return actData ? actData->mParentWidget : nullptr;
}

ItemSize* TrackItemsProxyAdapter::SaveSize( void *item )
{
    ActionAndData *data = static_cast<ActionAndData*>(item);
    GraphObject *object = data ? static_cast<GraphObject*>(data->mData) : NULL;
    ItemSize *size = object ? static_cast<ItemSize *>(eina_hash_find( m_ItemSizeCache, object->GetId())) : NULL;
    if(!size){
        size = GetItemSize( item );
        if ( size ) {
            if (( size->height != 0 ) && ( size->weight != 0 ) ) {
                eina_hash_add( m_ItemSizeCache, size->key, size );
            } else {
                delete_cached_item_size(size);
                size = NULL;
            }
        }
    }
    return size;
}

ItemSize* TrackItemsProxyAdapter::GetSize( void *item )
{
    ItemSize *result = NULL;
    ActionAndData *data = static_cast<ActionAndData*>(item);
    if(data) {
        GraphObject *object = static_cast<GraphObject*>(data->mData);
        if(object) {
            result = static_cast<ItemSize* > ( eina_hash_find( m_ItemSizeCache, object->GetId() ) );
        }
    }
    return result;
}

bool TrackItemsProxyAdapter::go_item_comparator( void *itemToCompare, void *itemForCompare )
{
    ActionAndData *dataForCompare = static_cast<ActionAndData*>(itemForCompare);
    GraphObject *toCompare = static_cast<GraphObject*>(itemToCompare);
    if(toCompare == nullptr && dataForCompare == nullptr) return true;
    GraphObject *forCompare = static_cast<GraphObject*>(dataForCompare->mData);
    return ( toCompare->GetHash() == forCompare->GetHash() ) &&
            ( strcmp( toCompare->GetId(), forCompare->GetId() ) == 0 );
}

void TrackItemsProxyAdapter::RemovePresenter(void *item) {
    if (item) {
        ActionAndData* postData = static_cast<ActionAndData* >(item);
        if(ActionAndData::IsAlive(postData)) {
            DestroyItem( postData, TrackItemsProxyAdapter::items_comparator, false );
        }
    }
}

Evas_Coord TrackItemsProxyAdapter::GetWidgetHeight(void *item) {
    assert(item);
    ActionAndData* postData = static_cast<ActionAndData* >(item);
    assert (postData->mParentWidget);
    Evas_Coord height = 0;
    evas_object_geometry_get( postData->mParentWidget , nullptr, nullptr, nullptr, &height );
    return height;
}

void TrackItemsProxyAdapter::DestroyUIItem(void *data) {
    void *item = DestroyItemByDataKeepAnD(data, items_comparator_Obj_vs_ActionAndData);
    if (item) {
        DeleteUIItem(item);
        RefreshRectangles();
    }
}

bool TrackItemsProxyAdapter::FindDuplicateItem(void *item) {
    return FindItem(item, items_comparator_Obj_vs_ActionAndData);
}
