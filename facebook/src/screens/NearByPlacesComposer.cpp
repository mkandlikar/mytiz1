#include "Common.h"
#include "Config.h"
#include "FbResponseAboutPlaces.h"
#include "LocationService.h"
#include "Log.h"
#include "NearByPlacesComposer.h"
#include "PlacesProvider.h"
#include "WebViewScreen.h"
#include "WidgetFactory.h"

#include <system_info.h>

const unsigned int NearByPlacesComposer::DEFAULT_PLACES_WND_SIZE = 200;

NearByPlacesComposer::NearByPlacesComposer(NearByPlacesScreen *parentScreen, Evas_Object *container_layoyut) :
        ScreenBase(parentScreen), TrackItemsProxyAdapter(container_layoyut, PlacesProvider::GetInstance()) {
    mLocationService = NULL;
    mPlacesProvider = PlacesProvider::GetInstance();
    mLayout = GetScroller();
    mAction = new ActionBase(this);
    mParentScreen = parentScreen;
    mConfirmationDialog = NULL;

    SetPostponedItemDeletion( false);

    TrackItemsProxy::ProxySettings *settings = GetProxySettings();
    settings->wndSize = DEFAULT_PLACES_WND_SIZE;
    SetProxySettings(settings);

    /* Before starting the location service, just check for availability of a fresh location,
     * if fresh location is available use that, else get last location from services and check whether it is fresh or not
     * if not fresh location, which is older than 10 minutes then start the service and get location.
     * */
    if (LocationService::is_latest_location()) {
        mParentScreen->ShowHideMsg(false);
        mParentScreen->setMapPosition(LocationService::getLat(), LocationService::getLong());
        Log::info("Application::HAVING FRESH LOCATION");
        mPlacesProvider->setLatitudeLongitude(LocationService::getLat(), LocationService::getLong());
        RequestData();
    } else {
        Log::info("Application::NOT HAVING FRESH LOCATION");
        mLocationService = new LocationService(on_locationServices_get_cb, this);
    }
}

NearByPlacesComposer::~NearByPlacesComposer() {
    delete mAction;
    if (mConfirmationDialog) {
        delete mConfirmationDialog;
        mConfirmationDialog = nullptr;
    }
    mParentScreen = NULL;
    if (mLocationService) {
        mLocationService->stop();
        delete mLocationService;
    }
    EraseWindowData();
    PlacesProvider::GetInstance()->EraseData();
}

void NearByPlacesComposer::on_locationServices_get_cb(void * data, double latitude, double longitude, int result) {
    NearByPlacesComposer *me = static_cast<NearByPlacesComposer*>(data);

    if (result == LOCATIONS_ERROR_NONE) {
//        NearByPlacesScreen *parent = static_cast<NearByPlacesScreen*>(me->mParentScreen);
        me->mLayout = me->GetScroller();
        me->mParentScreen->ShowHideMsg(false);
        me->mParentScreen->setMapPosition(latitude, longitude);
        me->mPlacesProvider->setLatitudeLongitude(latitude, longitude);
        me->RequestData();
    } else {
        me->mParentScreen->ShowHideMsg(true);
    }
}

void* NearByPlacesComposer::CreateItem(void *dataForItem, bool isAddToEnd) {
    FbResponseAboutPlaces::PlacesList *places = static_cast<FbResponseAboutPlaces::PlacesList*>(dataForItem);
    ActionAndData *data = NULL;

    if (places) {
        data = new ActionAndData(places, mAction);
        data->mParentWidget = PlacesGet(places, GetDataArea(), isAddToEnd);
    }
    return data;
}

bool NearByPlacesComposer::HandleBackButton() {
    if (mConfirmationDialog) {
        delete mConfirmationDialog;
        mConfirmationDialog = nullptr;
    }
    return true;
}

Evas_Object *NearByPlacesComposer::PlacesGet(void *user_data, Evas_Object *obj, bool isAddToEnd) {
    FbResponseAboutPlaces::PlacesList *places_data = static_cast<FbResponseAboutPlaces::PlacesList *>(user_data);

    Evas_Object *content = CreateWrapper(obj, isAddToEnd);
    CreatePlacesItem(content, places_data);

    return content;
}

Evas_Object* NearByPlacesComposer::CreateWrapper(Evas_Object* parent, bool isAddToEnd) {
    Evas_Object *wrapper = elm_layout_add(parent);
    elm_layout_file_set(wrapper, Application::mEdjPath, "nearBy_places_wrapper");
    evas_object_size_hint_weight_set(wrapper, 1, 1);
    evas_object_size_hint_align_set(wrapper, -1, 0);
    if (isAddToEnd) {
        elm_box_pack_end(parent, wrapper);
    } else {
        elm_box_pack_start(parent, wrapper);
    }
    evas_object_show(wrapper);

    Evas_Object *wrapperBox = elm_box_add(wrapper);
    evas_object_size_hint_weight_set(wrapperBox, 1, 1);
    evas_object_size_hint_align_set(wrapperBox, -1, 0);
    elm_layout_content_set(wrapper, "content", wrapperBox);
    elm_box_padding_set(wrapperBox, 0, 0);

    return wrapperBox;
}

void NearByPlacesComposer::CreatePlacesItem(Evas_Object* parent, void *user_data) {
    FbResponseAboutPlaces::PlacesList *places_data = static_cast<FbResponseAboutPlaces::PlacesList *>(user_data);
    const char *dist_str = NULL;
    std::stringstream temp_buf_line_2;

    Evas_Object *list_item = elm_layout_add(parent);
    evas_object_size_hint_weight_set(list_item, 1, 1);
    elm_layout_file_set(list_item, Application::mEdjPath, "nearBy_places_composer");
    elm_box_pack_end(parent, list_item);
    evas_object_show(list_item);

    Evas_Object *avatar = elm_image_add(list_item);
    elm_image_file_set(avatar, places_data->mPicSquareWithLogo, NULL);
    elm_object_part_content_set(list_item, "item_photo", avatar);
    evas_object_show(avatar);

    mParentScreen->setPlacesMarker(places_data->mPicSquareWithLogo, places_data->mLatitude, places_data->mLongitude, places_data->mWebLinkToOpen);

    elm_object_part_text_set(list_item, "item_text_line_1", places_data->place_name);

    temp_buf_line_2 << places_data->city_name << ',' << places_data->country;
    elm_object_part_text_set(list_item, "item_text_line_2", temp_buf_line_2.str().c_str());
    elm_object_part_text_set(list_item, "item_text_line_3", places_data->created_time);

    dist_str = LocationService::get_distance_in_str(places_data->mLatitude, places_data->mLongitude);
    if (dist_str) {
        Log::debug("NearByPlacesComposer::CreatePlacesItem = Get Distance = %s", dist_str);
        elm_object_part_text_set(list_item, "item_text_line_1_distance", dist_str);
        delete[] dist_str;
    }

    elm_object_signal_callback_add(list_item, "item.clicked", "item_*", on_places_item_click, (void*) places_data->mWebLinkToOpen);

    temp_buf_line_2.str("");
    temp_buf_line_2.clear();

}

void NearByPlacesComposer::on_places_item_click(void *data, Evas_Object *obj, const char *emisson, const char *source) {
    if (data == NULL) {
        return;
    }
    const char *url = static_cast<char*>(data);
    Log::debug("NearByPlacesComposer::on_places_item_click:: URL = %s", url);
    WebViewScreen::Launch(url, true);
}

