#include "Application.h"
#include "Log.h"
#include "ScreenBase.h"
#include "VideoPlayerScreen.h"
#include "VideoPlayerView.h"

#include <metadata_extractor.h>

VideoPlayerView::VideoPlayerView(Evas_Object *parent, IVideoPlayerObserver *observer, const char *path) :
    mPlayer(NULL), mVideoRect(NULL), mPath(NULL),
    mVideoDuration(0), mVideoWidth(0), mVideoHeight(0),
    mObserver(observer), mNotifyPlayerReadyJob(NULL),
    mNotifyPlayerCompleteJob(NULL), mNotifyPlayerInterruptedJob(NULL), mInterruptedPlayerPosition(0) {

    if (path) {
        mPath = strdup(path);
    } //AAA ToDo else

    assert(parent);
    mVideoRect = evas_object_image_filled_add(evas_object_evas_get(parent));
    evas_object_show(mVideoRect);
    OpenMediaRes();
}


void VideoPlayerView::CheckMediaSessionOptions() {
    sound_session_option_for_starting_e soundSessionStartingOption;
    sound_session_option_for_during_play_e soundSessionDuringPlayOption;

    int ret = sound_manager_get_media_session_option(&soundSessionStartingOption,&soundSessionDuringPlayOption);
    if(ret != SOUND_MANAGER_ERROR_NONE) {
        Log::error("VideoPlayerView: CheckMediaSessionOptions() can't get sound manager session option, Error code = %d", ret );
        return;
    }
    Log::info("VideoPlayerView: CheckMediaSessionOptions() StartingOption = %d, DuringPlayingOption= %d",
              (int)soundSessionStartingOption, (int)soundSessionDuringPlayOption );

    if (!(soundSessionStartingOption == SOUND_SESSION_OPTION_PAUSE_OTHERS_WHEN_START &&
          soundSessionDuringPlayOption == SOUND_SESSION_OPTION_INTERRUPTIBLE_DURING_PLAY)) {
        Application::GetInstance()->ConfigureSoundManager();
    }
}

void VideoPlayerView::CreatePlayer() {
    Log::info("VideoPlayerView: CreatePlayer(), isPlayer: %d", mPlayer != NULL);
    CheckMediaSessionOptions();
    if (mPlayer == NULL) {
        if (player_create(&mPlayer) == PLAYER_ERROR_NONE) {
            player_set_sound_type(mPlayer, SOUND_TYPE_MEDIA);
            player_set_volume(mPlayer, 1.0, 1.0);
            player_set_looping(mPlayer, false);
            player_set_completed_cb(mPlayer, on_player_completed_cb, this);
            player_set_interrupted_cb(mPlayer, on_player_interrupted_cb, this);
        }
    }
}

void VideoPlayerView::DestroyPlayer() {
    Log::info("VideoPlayerView: DestroyPlayer, isPlayer: %d", mPlayer != NULL);
    if (mPlayer != NULL) {
        player_state_e state = GetPlayerState();
        if(state == PLAYER_STATE_PLAYING || state == PLAYER_STATE_PAUSED) {
            player_stop(mPlayer);
        }
        player_unset_completed_cb(mPlayer);
        player_unset_interrupted_cb(mPlayer);
        player_unprepare(mPlayer);
        int res = player_destroy(mPlayer);
        if (res == PLAYER_ERROR_NONE) {
            Log::info("VideoPlayerView: Player destroyed successfully!");
        } else {
            Log::error("VideoPlayerView: Player destroy error: %d", res);
        }
        mPlayer = NULL;
    }
}

VideoPlayerView::~VideoPlayerView() {
    if (mNotifyPlayerReadyJob) {
        ecore_job_del(mNotifyPlayerReadyJob);
    }
    if (mNotifyPlayerCompleteJob) {
        ecore_job_del(mNotifyPlayerCompleteJob);
    }
    if (mNotifyPlayerInterruptedJob) {
        ecore_job_del(mNotifyPlayerInterruptedJob);
    }

    DestroyPlayer();
    free((char *) mPath);
    evas_object_del(mVideoRect);
}

player_state_e VideoPlayerView::GetPlayerState() {
    player_state_e state;
    if (mPlayer != NULL) {
        player_get_state(mPlayer, &state);
    } else {
        state = PLAYER_STATE_NONE;
    }
    Log::info("VideoPlayerView: STATE: %d", state);
    return state;
}

bool VideoPlayerView::IsPlayerReady() {
    return (GetPlayerState() == PLAYER_STATE_READY);
}

bool VideoPlayerView::StartPlayer() {
    player_state_e state = GetPlayerState();
    if (state == PLAYER_STATE_READY || state == PLAYER_STATE_PAUSED) {
        int res = player_start(mPlayer);
        if (res == PLAYER_ERROR_NONE) {
            return true;
        } else if (res == PLAYER_ERROR_SOUND_POLICY) {
            notification_status_message_post( i18n_get_text("IDS_SOUND_POLICY_ERROR") );
        } else if (res == PLAYER_ERROR_INVALID_OPERATION) {
            Log::error("VideoPlayerView::StartPlayer(): player_start() returned PLAYER_ERROR_INVALID_OPERATION");
            if (mWithoutAudioV3) {
                if (player_start(mPlayer) == PLAYER_ERROR_NONE) {
                    return true;
                }
            }
        } else {
            Log::error("VideoPlayerView::StartPlayer(): player_start() returned error: %d", res);
        }
    } else if (state == PLAYER_STATE_IDLE){
        Log::info("VideoPlayerView::StartPlayer(): IDLE state, prepare to start interrupted player from position %d",
                  mInterruptedPlayerPosition);
        PreparePlayer();
    }
    return false;
}

bool VideoPlayerView::StopPlayer() {
    player_state_e state = GetPlayerState();
    if (state == PLAYER_STATE_PLAYING || state == PLAYER_STATE_PAUSED) {
        if (player_stop(mPlayer) == PLAYER_ERROR_NONE) {
            return true;
        }
    }
    return false;
}

bool VideoPlayerView::PausePlayer() {
    if (GetPlayerState() == PLAYER_STATE_PLAYING) {
        if (player_pause(mPlayer) == PLAYER_ERROR_NONE) {
            return true;
        }
    }
    return false;
}

void VideoPlayerView::on_player_async_prepared_cb(void *data) {
    VideoPlayerView *me = static_cast<VideoPlayerView*>(data);
    if (me && me->mPlayer && (me->GetPlayerState() == PLAYER_STATE_READY)) {
        //get properties of the video
        player_get_duration(me->mPlayer, &me->mVideoDuration);
        player_get_video_size(me->mPlayer, &me->mVideoWidth, &me->mVideoHeight);
        Log::info("VideoPlayerView::on_player_async_prepared_cb():  - duration=%d, size = %dx%d, mObserver=%p",
                  me->mVideoDuration, me->mVideoWidth, me->mVideoHeight, me->mObserver);

        me->HandlePrepare();
        if (Application::GetInstance()->GetPlatformVersion() >= 3) {
            int bit_rate, sample_rate, channels;
            player_get_audio_stream_info(me->mPlayer, &sample_rate, &channels, &bit_rate);
            if (sample_rate <= 0 || channels <= 0) {
                me->mWithoutAudioV3 = true;
            }
        }
    }
}

void VideoPlayerView::HandlePrepare() {
    metadata_extractor_h metadata;
    char *angle = nullptr;
    mVideoSceneModePortrait = false;
    if (METADATA_EXTRACTOR_ERROR_NONE == metadata_extractor_create(&metadata) &&
        METADATA_EXTRACTOR_ERROR_NONE == metadata_extractor_set_path(metadata, mPath) &&
        METADATA_EXTRACTOR_ERROR_NONE == metadata_extractor_get_metadata(metadata, METADATA_ROTATE, &angle) &&
        angle != nullptr) {
        // here we get the mode, in which video was filmed, portrait or landscape
        mVideoSceneModePortrait = (!strcmp(angle, "90") || !strcmp(angle, "270"));
        //This is a workaround. Unusual video rotation is observed only on Tizen 3.0
        if (Application::GetInstance()->GetPlatformVersion() >= 3) {
            int intAngle = atoi(angle);
            player_display_rotation_e rotation = player_display_rotation_e::PLAYER_DISPLAY_ROTATION_NONE;
            switch (intAngle) {
                case 90:
                    rotation = player_display_rotation_e::PLAYER_DISPLAY_ROTATION_270;
                    break;
                case 180:
                    rotation = player_display_rotation_e::PLAYER_DISPLAY_ROTATION_180;
                    break;
                case 270:
                    rotation = player_display_rotation_e::PLAYER_DISPLAY_ROTATION_90;
                    break;
                default:
                    break;
            }
            player_set_display_rotation(mPlayer, rotation);
        }
    }
    free(angle);
    if (metadata) {
        metadata_extractor_destroy(metadata);
    }

    if (mInterruptedPlayerPosition) {
        player_set_play_position(mPlayer, mInterruptedPlayerPosition, true,
                                 on_player_seek_completed_cb, this);
        mInterruptedPlayerPosition = 0;
    } else {
        mNotifyPlayerReadyJob = ecore_job_add(notify_player_ready, this);
    }
}

void VideoPlayerView::on_player_seek_completed_cb(void *user_data){
    VideoPlayerView *me = static_cast<VideoPlayerView*>(user_data);
    me->mNotifyPlayerReadyJob = ecore_job_add(notify_player_ready, me);
}

/**
 * This is a callback for a subscriber for notify in UI thread
 */
void VideoPlayerView::notify_player_ready(void *data) {
    VideoPlayerView *me = static_cast<VideoPlayerView *>(data);
    assert(me);
    me->mNotifyPlayerReadyJob = nullptr;

    if (Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_VIDEO_PLAYER) {
        VideoPlayerScreen *screen =
                static_cast<VideoPlayerScreen *>(Application::GetInstance()->GetTopScreen());
        if (screen && screen->isCorrectVideoPlayerView(me)) {
            me->mObserver->PlayerReady();
        }
    }
}

/**
 * This is a callback for a subscriber for notify in UI thread
 */
void VideoPlayerView::notify_player_complete(void *data) {
    VideoPlayerView *me = static_cast<VideoPlayerView *>(data);
    assert(me);
    me->mNotifyPlayerCompleteJob = nullptr;

    if (Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_VIDEO_PLAYER) {
        VideoPlayerScreen *screen =
            static_cast<VideoPlayerScreen *>(Application::GetInstance()->GetTopScreen());
        if (screen && screen->isCorrectVideoPlayerView(me)) {
            me->mObserver->PlayerCompleted();
        }
    }
}

/**
 * This is a callback for a subscriber for notify in UI thread
 */
void VideoPlayerView::notify_player_interrupted(void *data) {
    VideoPlayerView *me = static_cast<VideoPlayerView *>(data);
    assert(me);
    me->mNotifyPlayerInterruptedJob = nullptr;

    if (Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_VIDEO_PLAYER) {
        VideoPlayerScreen *screen =
                static_cast<VideoPlayerScreen *>(Application::GetInstance()->GetTopScreen());
        if (screen && screen->isCorrectVideoPlayerView(me)) {
            me->mObserver->PlayerInterrupted();
        }
    }
}

void VideoPlayerView::PreparePlayer() {
    Log::info("VideoPlayerView: Player preparing...");
    if (mPlayer && mPath && mVideoRect) {
        player_set_uri(mPlayer, mPath);
        player_set_display(mPlayer, PLAYER_DISPLAY_TYPE_EVAS, GET_DISPLAY(mVideoRect));
        player_set_display_mode(mPlayer, PLAYER_DISPLAY_MODE_FULL_SCREEN);

        if (GetPlayerState() == PLAYER_STATE_IDLE) {
            player_prepare_async(mPlayer, on_player_async_prepared_cb, this);
        }
    }
    //player_prepare(mPlayer);
//AAA ToDo: progressive download
//    char *path;
////    storage_get_directory(0, STORAGE_DIRECTORY_VIDEOS, &path);
//    path  = app_get_shared_data_path();
//    Log::info("PATH:%s", path);
//    player_set_progressive_download_path(mPlayer, path);
}

void VideoPlayerView::OpenMediaRes() {
    DestroyPlayer();
    CreatePlayer();
    PreparePlayer();
}

void VideoPlayerView::on_player_completed_cb(void *data) {
    VideoPlayerView *me = static_cast<VideoPlayerView *>(data);
    if (me
            && Application::GetInstance()->GetTopScreenId()
                    == ScreenBase::SID_VIDEO_PLAYER) {
        VideoPlayerScreen *screen =
                static_cast<VideoPlayerScreen *>(Application::GetInstance()->GetTopScreen());
        if (screen && screen->isCorrectVideoPlayerView(me)) {
            // Stopping the player is not needed here, delegate the observer to handle complete event
            me->mNotifyPlayerCompleteJob = ecore_job_add(notify_player_complete, me);
        }
    }
}

void VideoPlayerView::on_player_interrupted_cb(player_interrupted_code_e code, void *data) {
    VideoPlayerView *me = static_cast<VideoPlayerView *>(data);
    Log::info("VideoPlayerView::on_player_interrupted_cb(code=%d)", code);

    if (me) {
        if(code == PLAYER_INTERRUPTED_BY_RESOURCE_CONFLICT) {
            me->mInterruptedPlayerPosition = me->GetPosition();
        }
        if (Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_VIDEO_PLAYER) {
            VideoPlayerScreen *screen =
                static_cast<VideoPlayerScreen *>(Application::GetInstance()->GetTopScreen());
            if (screen && screen->isCorrectVideoPlayerView(me)) {
                // Pausing the player is not needed here,
                // delegate the observer to handle complete event
                me->mNotifyPlayerInterruptedJob = ecore_job_add(notify_player_interrupted, me);
            }
        }
    }
}
