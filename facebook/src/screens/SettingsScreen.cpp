#include "AboutApp.h"
#include "ActionBase.h"
#include "BaseObject.h"
#include "Common.h"
#include "Config.h"
#include "ConnectivityManager.h"
#include "ErrorHandling.h"
#include "EventMainPage.h"
#include "FbRespondGetGroups.h"
#include "FindFriendsScreen.h"
#include "Group.h"
#include "GroupProfilePage.h"
#include "GroupSeeAllScreen.h"
#include "jsonutilities.h"
#include "Log.h"
#include "LoggedInUserData.h"
#include "LogoutRequest.h"
#include "NearByPlacesScreen.h"
#include "OwnProfileScreen.h"
#include "SettingsScreen.h"
#include "SignUpInProgressScreen.h"
#include "UserPhotosScreen.h"
#include "Utils.h"
#include "WebViewScreen.h"
#include "WidgetFactory.h"

static Evas_Object *mLogOutPopUp = nullptr;

const int SettingsScreen::mLoggedinUserItemWitdth = 480;
const int SettingsScreen::mLoggedinUserItemHeight = 98;
const int SettingsScreen::mMaxShownGroups = 3;

SettingsScreen *SettingsScreen::settings_screen_instance = nullptr;
ActionAndData *SettingsScreen::mActionAndData = nullptr;

#define FBRES_INVALID_SESKEY_ECODE 102

Field::Field(const char *text, const char *icon, BaseObject *data)
{
    mText = SAFE_STRDUP(text);
    mIconPath = SAFE_STRDUP(icon);
    mData = data;
}

Field::~Field() {
    free(mText);
    free(mIconPath);
    if(mData) {
        mData->Release();
    }
}

GroupDescription::GroupDescription(const char *id, const char *name) {
    mId = strdup(id);
    mName = name ? strdup(name) : NULL;
}
GroupDescription::~GroupDescription() {
    free(mId);
    free(mName);
}

Field SettingsScreen::mArray[] =
{
    Field("PROFILETEST", NULL),
    Field("IDS_SETTING_FAVORITES", NULL),
    Field("IDS_SETTING_FIND_FRIENDS", ICON_SETTINGS_DIR"/bm_friends.png"),
    Field("IDS_SETTING_EVENTS", ICON_SETTINGS_DIR"/bm_events.png"),
    Field("IDS_SETTING_APPS", NULL),
    Field("IDS_SETTING_CHAT", ICON_SETTINGS_DIR"/bm_chat.png"),
    Field("IDS_SETTING_LIKE_PAGES", ICON_SETTINGS_DIR"/bm_like_pages.png"),
    Field("IDS_SETTING_SAVED", ICON_SETTINGS_DIR"/bm_saved.png"),
    Field("IDS_SETTING_FRIENDS", ICON_SETTINGS_DIR"/bm_friends.png"),
    Field("IDS_SETTING_NEAR_BY_PLACES", ICON_SETTINGS_DIR"/bm_nearby.png"),
    Field("IDS_SETTING_PHOTOS", ICON_SETTINGS_DIR"/bm_photos.png"),
    Field("IDS_SETTING_POKES", ICON_SETTINGS_DIR"/bm_pokes.png"),
    Field("IDS_SETTING_GROUPS", NULL),
    Field("IDS_SETTING_SUGGESTED_GROUPS", ICON_SETTINGS_DIR"/suggested_groups.png"),
    Field("IDS_SETTING_PAGES", NULL),
    Field("IDS_SETTING_CREATE_PAGE", ICON_SETTINGS_DIR"/[11]CreatePage.png"),
    Field("IDS_SETTING_FEEDS", NULL),
    Field("IDS_SETTING_MOST_RECENT", ICON_SETTINGS_DIR"/[12]MostRecent.png"),
    Field("IDS_SETTING_CLOSE_FRIENDS", ICON_SETTINGS_DIR"/[13]CloseFriends.png"),
    Field("IDS_SETTING_FAMILY", ICON_SETTINGS_DIR"/[14]Family.png"),
    Field("IDS_SETTING_HELP_N_SETTINGS", NULL),
    Field("IDS_SETTING_APP_SETTINGS", ICON_SETTINGS_DIR"/bm_app_settings.png"),
    Field("IDS_SETTING_NEWS_FEED_PREF", ICON_SETTINGS_DIR"/bm_news_feed_pref.png"),
    Field("IDS_SETTING_ACCOUNT_SETTINGS", ICON_SETTINGS_DIR"/bm_account_settings.png"),
    Field("IDS_SETTING_HELP_CENTER", ICON_SETTINGS_DIR"/bm_help_center.png"),
    Field("IDS_SETTING_PRIVACY_SHORTCUTS", ICON_SETTINGS_DIR"/bm_privacy_shortcuts.png"),
    Field("IDS_SETTING_TERMS_N_POLICIES", ICON_SETTINGS_DIR"/bm_terms.png"),
    Field("IDS_SETTING_REPORT_A_PROBLEM", ICON_SETTINGS_DIR"/[20]ReportProblem.png"),
    Field("IDS_SETTING_ABOUT",ICON_SETTINGS_DIR"/bm_about.png"),
    Field("IDS_SETTING_MOBILE_DATA",ICON_SETTINGS_DIR"/[22]MobileData.png"),
    Field("IDS_SETTING_LOG_OUT",ICON_SETTINGS_DIR"/bm_log_out.png")
};

#define NUM_OF_SETTINGS_FIELDS (sizeof(mArray) / sizeof(mArray[0]))

SettingsScreen::SettingsScreen(ScreenBase *parentScreen) : ScreenBase(parentScreen)
{
    mActionAndData = new ActionAndData(nullptr, nullptr);
    mLogOutReq = nullptr;

    mAfterItem = nullptr;
    mBeforeItem = nullptr;
    mGroupItem = nullptr;

    mOwnName = nullptr;
    mOwnAvatar = nullptr;

    mLogOutPopUp = nullptr;

    mGroupRequest = nullptr;
    mGroupItems = eina_array_new(mMaxShownGroups);

    mAvatarPath = nullptr;

    //Set the instance of the Settings Screen;
    settings_screen_instance_set(this);

    Elm_Object_Item *item = nullptr;
    int index;
    Evas_Object *parent = getParentMainLayout();

    Elm_Genlist_Item_Class *itcUser = elm_genlist_item_class_new();
    itcUser->item_style = "full";
    itcUser->func.content_get = gl_user_data_get;
    itcUser->func.del = nullptr;

    Elm_Genlist_Item_Class *itcGrInd = elm_genlist_item_class_new(); // titles (Favs, Apps, etc)
    itcGrInd->item_style = "full";
    itcGrInd->func.content_get = gl_titles_data_get;
    itcGrInd->func.del = nullptr;

    Elm_Genlist_Item_Class *itc = elm_genlist_item_class_new(); // categories
    itc->item_style = "full";
    itc->func.content_get = gl_categories_data_get;
    itc->func.del = nullptr;

    mLayout = elm_genlist_add(parent);
    elm_object_style_set(mLayout, "no_effect");
    elm_scroller_single_direction_set(mLayout, ELM_SCROLLER_SINGLE_DIRECTION_HARD);
    item = elm_genlist_item_append(mLayout, itcUser, this, nullptr, ELM_GENLIST_ITEM_NONE, gl_user_sel, this); // mLogedinUser is passed for item data
    for (index = 1; index < NUM_OF_SETTINGS_FIELDS; index++) {
        switch (index) {
            case Favorites:
            case Apps:
            case Groups:
//            case Pages:
//            case Feeds:
            case HelpNSettings:
            {
                item = elm_genlist_item_append(mLayout, itcGrInd, &mArray[index],
                        nullptr, ELM_GENLIST_ITEM_GROUP, gl_sel, nullptr);
                mBeforeItem = item;

                if (index == Groups)
                {
                    mGroupItem = item;
                }
//                if (index == Pages)
//                {
//                    mBeforeItem = item;
//                }

                elm_genlist_item_select_mode_set(item, ELM_OBJECT_SELECT_MODE_DISPLAY_ONLY);
            }
            break;

            case GroupCategory:
            {
                item = elm_genlist_item_append(mLayout, itc, &mArray[index],
                        nullptr, ELM_GENLIST_ITEM_NONE, gl_SuggestedGroupsSel, this);
            }
            break;

            case FindFriends:
            {
                item = elm_genlist_item_append(mLayout, itc, &mArray[index],
                            nullptr, ELM_GENLIST_ITEM_NONE, gl_FindFriendsScreenSel, this);
            }
            break;

            case Events:
            {
                item = elm_genlist_item_append(mLayout, itc, &mArray[index],
                            nullptr, ELM_GENLIST_ITEM_NONE, gl_EventScreenSel, this);
            }
            break;

            case Saved:
            {
                item = elm_genlist_item_append(mLayout, itc, &mArray[index],
                            nullptr, ELM_GENLIST_ITEM_NONE, gl_SavedFolderSel, this);
            }
            break;

            case NearbyPlaces:
            {
                item = elm_genlist_item_append(mLayout, itc, &mArray[index],
                            nullptr, ELM_GENLIST_ITEM_NONE, nearBy_places_sel_cb, this);
            }
            break;

            case Photos:
            {
                item = elm_genlist_item_append(mLayout, itc, &mArray[index],
                           nullptr, ELM_GENLIST_ITEM_NONE, gl_PhotosSel, this);
            }
            break;

            case Pokes:
            {
                item = elm_genlist_item_append(mLayout, itc, &mArray[index], nullptr, ELM_GENLIST_ITEM_NONE, pokes_cb, this);
            }
            break;

            case AccountSettings:
            {
                item = elm_genlist_item_append(mLayout, itc, &mArray[index],
                nullptr, ELM_GENLIST_ITEM_NONE, account_settings_sel_cb, //action on item
                        this // data for account_settings_sel_cb
                        );
            }
            break;

            case HelpCenter:
            {
                item = elm_genlist_item_append(mLayout, itc, &mArray[index],
                nullptr, ELM_GENLIST_ITEM_NONE, help_center_sel_cb, //action on item
                        this // data for help_center_sel_cb
                        );
            }
            break;

            case PrivacyShortcuts:
            {
                item = elm_genlist_item_append(mLayout, itc, &mArray[index],
                nullptr, ELM_GENLIST_ITEM_NONE, privacy_shortcuts_sel_cb, //action on item
                        this // data for privacy_shortcuts_sel_cb
                        );
            }
            break;

            case TermsNPolicies:
            {
                item = elm_genlist_item_append(mLayout, itc, &mArray[index],
                nullptr, ELM_GENLIST_ITEM_NONE, terms_n_policies_sel_cb, //action on item
                        this // data for terms_n_policies_sel_cb
                        );
            }
            break;

            case About:
            {
                item = elm_genlist_item_append(mLayout, itc, &mArray[index],
                nullptr, ELM_GENLIST_ITEM_NONE, about_sel_cb, //action on item
                        this // data for about_sel_cb
                        );
            }
            break;

            case LogOut:
            {
                item = elm_genlist_item_append(mLayout, itc, &mArray[index],
                nullptr, ELM_GENLIST_ITEM_NONE, log_out_sel_cb, //action on item
                        this // data for log_out_sel_cb
                        );
            }
            break;

            // These cases are not being supported currently.
            // So we are not displaying in the Settings List
            case Chat:
            case LikePages:
            case Friends:
            case Pages:
            case CreatePage:
            case Feeds:
            case MostRecent:
            case CloseFriends:
            case Family:
            case AppSettings:
            case NewsFeedPreferences:
            case ReportAProblem:
            case MobileData:
            {
                // Do Nothing.
            }
            break;

            default:
                item = elm_genlist_item_append(mLayout, itc, &mArray[index],
                nullptr, ELM_GENLIST_ITEM_NONE, gl_sel, //action on item
                        this // data for gl_sel
                        );
            break;
        } // switch(index)
    }// for loop
    elm_genlist_item_class_free(itc);
    elm_genlist_item_class_free(itcGrInd);
    elm_genlist_item_class_free(itcUser);

    evas_object_show(mLayout);

    GetGroupsList();
    AppEvents::Get().Subscribe(eOWN_PROFILE_DATA_UPDATED, this);
    AppEvents::Get().Subscribe(eUPDATED_PROFILE_PICTURE, this);
}

SettingsScreen::~SettingsScreen()
{
    settings_screen_instance_set(nullptr);
    ClearGetGroupsRequest();
    RemoveGroupsList();
    delete mActionAndData; mActionAndData = nullptr;
    delete mLogOutReq;
    eina_array_free(mGroupItems);
    if(mLogOutPopUp) {
        evas_object_del(mLogOutPopUp);
        mLogOutPopUp = nullptr;
    }
    free(mAvatarPath);
}

void SettingsScreen::Update( AppEventId eventId, void *data )
{
    switch (eventId) {
    case eUPDATED_PROFILE_PICTURE:
        Log::debug(LOG_FACEBOOK_USER, "SettingsScreen::Update, eUPDATED_PROFILE_PICTURE");
        if(data) {
            SetPathAndUploadAvatar(static_cast<const char*>(data));
        }
        break;
    case eOWN_PROFILE_DATA_UPDATED:
        Log::debug(LOG_FACEBOOK_USER, "SettingsScreen::Update, eOWN_PROFILE_DATA_UPDATED");
        ClearAvatarPath();
        RefreshOwnData();
        break;
    default:
        break;
    }
}

void SettingsScreen::cb_get_groups_list()
{
    SettingsScreen * inst_scr = settings_screen_instance_get();
    if(inst_scr) {
        inst_scr->GetGroupsList();
    }
}

/**
 * @brief Initiate GET group request to facebook
 */
// Groups List request
void SettingsScreen::GetGroupsList()
{
    Log::info("SettingsScreen::GetGroupsList");

    if (!mGroupRequest)
    {
        mGroupRequest = FacebookSession::GetInstance()->GetGroupsList(on_get_groups_request_completed, this);
    }
}

void SettingsScreen::on_get_groups_request_completed(void* object, char* respond, int code)
{
    Log::info("SettingsScreen::on_get_groups_request_completed");
    SettingsScreen *settingsScreen = static_cast<SettingsScreen*>(object);
    if(settingsScreen)
    {
        settingsScreen->OnGetGroupsRequestComplete(respond, code);
    }
}

void SettingsScreen::ClearGetGroupsRequest()
{
    FacebookSession::GetInstance()->ReleaseGraphRequest(mGroupRequest);
    mGroupRequest = nullptr;
}

/**
 * @brief - This callback is called when OnGetGroupsRequestComplete request is completed
 * @param object - user data passed to callback
 * @param respond - HTTP respond in json format
 * @param code - HTTP code
 */
void SettingsScreen::OnGetGroupsRequestComplete(char* respond, int code)
{
    Log::info("SettingsScreen::OnGetGroupsRequestComplete");

    ClearGetGroupsRequest();

    if (code == CURLE_OK) {
        FbRespondGetGroups * groupResponse = FbRespondGetGroups::createFromJson(respond);

        if (!groupResponse) {
            Log::error("SettingsScreen::Error parsing http respond");
        } else {
            if (groupResponse->mError != NULL) {
                Log::error("SettingsScreen::Error getting birthday requests, error = %s", groupResponse->mError->mMessage);
            } else if (groupResponse->mGroupsList) {
                Log::info("SettingsScreen::before SetEventsRequestsBatchData calling");
                SetGroupRequestsData(groupResponse);
            }
            delete groupResponse;
        }
    } else {
        Log::error("SettingsScreen::Error sending http request");
    }
    free(respond);
}

void SettingsScreen::RemoveGroupsList()
{
    if(!mGroupItems)
        return;
    unsigned int index = 0;
    void* item;
    Eina_Array_Iterator iterator;
    EINA_ARRAY_ITER_NEXT(mGroupItems, index, item, iterator)
    {
        elm_object_item_del((Elm_Widget_Item*)item);
    }

    eina_array_clean(mGroupItems);
}

void SettingsScreen::on_item_delete_cb(void *data, Evas_Object *obj) {
    if(data) {
        delete static_cast<Field *>(data);
    }
}

/**
 * @brief Update UI with received group request data
 * @param object - user data passed to callback
 * @param respond - HTTP respond in json format
 * @param code - HTTP code
 */
void SettingsScreen::SetGroupRequestsData(FbRespondGetGroups * respond)
{
    Log::info("SettingsScreen::SetGroupRequestsData");

    if(mGroupItem) {
        Eina_List *l;
        void *listData;

        Elm_Genlist_Item_Class *itc = elm_genlist_item_class_new();
        itc->item_style = "full";
        itc->func.content_get = gl_groups_data_get;
        itc->func.del = on_item_delete_cb;

        Elm_Genlist_Item_Class *itcGrInd = elm_genlist_item_class_new();
        itcGrInd->item_style = "full";
        itcGrInd->func.content_get = gl_see_all_data_get;
        itcGrInd->func.del = on_item_delete_cb;

        Elm_Object_Item *item = nullptr;

        Group *group_data = nullptr;
        Field * groupList = nullptr;
        Field * seeAll = nullptr;

        // Remove previous Group List and create new one
        if(mGroupItems && eina_array_count(mGroupItems) > 0)
        {
            RemoveGroupsList();
        }

        unsigned int count = 0;
        mAfterItem = mGroupItem;

        EINA_LIST_FOREACH(respond->mGroupsList, l, listData)
        {
            if(count == mMaxShownGroups)
            {
                break;
            }

            group_data = static_cast<Group *>(listData);
            GroupDescription *groupDescription = new GroupDescription(group_data->GetId(), group_data->mName);
            groupList = new Field(group_data->mName, group_data->mIcon, groupDescription);

            item = elm_genlist_item_insert_after(mLayout, itc, groupList,
                            nullptr, mAfterItem, ELM_GENLIST_ITEM_NONE, gl_GroupScreenSel, groupDescription);
            mAfterItem = item;
            eina_array_push(mGroupItems, item);
            count++;
        }

        if (respond->mGroupsList && (eina_list_count(respond->mGroupsList) > mMaxShownGroups) && mBeforeItem)
        {
            seeAll = new Field("IDS_SETTING_SEE_ALL", nullptr);

            item = elm_genlist_item_insert_before(mLayout, itcGrInd, seeAll,
                            nullptr, mBeforeItem, ELM_GENLIST_ITEM_NONE, gl_SeeAllGroupsSel, this);
            eina_array_push(mGroupItems, item);
        }
        elm_genlist_item_class_free(itc);
        elm_genlist_item_class_free(itcGrInd);
    }
}

Evas_Object *SettingsScreen::gl_user_data_get(void *user_data, Evas_Object *obj, const char *part)
{
    SettingsScreen *me = static_cast<SettingsScreen*>(user_data);
    CHECK_RET(me, nullptr);

    Evas_Object *viewYourPorfileItem = elm_layout_add(obj);
    evas_object_size_hint_weight_set(viewYourPorfileItem, 1, 1);
    elm_layout_file_set(viewYourPorfileItem, Application::mEdjPath, "settings_profiledata");
    evas_object_size_hint_min_set(viewYourPorfileItem, mLoggedinUserItemWitdth, mLoggedinUserItemHeight);
    evas_object_size_hint_max_set(viewYourPorfileItem, mLoggedinUserItemWitdth, mLoggedinUserItemHeight);
    evas_object_show(viewYourPorfileItem);

    me->mOwnAvatar = elm_image_add(viewYourPorfileItem);
    elm_layout_content_set(viewYourPorfileItem, "profiledata_picture_swallow", me->mOwnAvatar);
    evas_object_show(me->mOwnAvatar);

    me->mOwnName = elm_label_add(viewYourPorfileItem);
    elm_layout_content_set(viewYourPorfileItem, "profiledata_name_swallow", me->mOwnName);
    evas_object_show(me->mOwnName);

    me->RefreshOwnData();

    Evas_Object *itemSubtitle = elm_label_add(viewYourPorfileItem);

    const char *viewYourProfilelocalizedText = i18n_get_text("IDS_SETTING_VIEW_YOUR_PROFILE");
    if (viewYourProfilelocalizedText) {
        char *viewYourProfileStr = WidgetFactory::WrapByFormat2(SANS_REGULAR_9298A4_FORMAT, R->SETTINGS_VIEW_YOUR_PROFILE, viewYourProfilelocalizedText);
        elm_object_part_text_set(itemSubtitle,  "elm.text", viewYourProfileStr);
        delete []viewYourProfileStr;
    }
    elm_layout_content_set(viewYourPorfileItem, "profiledata_text_swallow", itemSubtitle);
    evas_object_show(itemSubtitle);

    return viewYourPorfileItem;
}

void SettingsScreen::RefreshOwnData()
{
    const char *loggedInUserText = nullptr;
    if (LoggedInUserData::GetInstance().mName) {
        loggedInUserText = LoggedInUserData::GetInstance().mName;
    } else {
        loggedInUserText = i18n_get_text("IDS_YOU");
    }

    if (loggedInUserText) {
        char *formattedStr = WidgetFactory::WrapByFormat2(SANS_REGULAR_4e5665_FORMAT, R->SETTINGS_LOGGEDIN_PROFILE_NAME, loggedInUserText);
        elm_object_part_text_set(mOwnName,  "elm.text", formattedStr);
        delete []formattedStr;
    }

    elm_image_file_set(mOwnAvatar, ICON_DIR"/profiles/empty_profile_image.png", nullptr);
    if (mAvatarPath) {
        evas_object_image_file_set(mOwnAvatar, mAvatarPath, nullptr);
    } else if(LoggedInUserData::GetInstance().mPicture && LoggedInUserData::GetInstance().mPicture->mUrl) {
        mActionAndData->UpdateImageLayoutAsync(LoggedInUserData::GetInstance().mPicture->mUrl, mOwnAvatar);
    }
}

void SettingsScreen::gl_FindFriendsScreenSel(void *data, Evas_Object *obj, void *event_info)
{
    FindFriendsScreen *ffscreen = new FindFriendsScreen(true);
    Application::GetInstance()->AddScreen(ffscreen);
    elm_genlist_item_selected_set((Elm_Object_Item *) event_info, EINA_FALSE);
}

void SettingsScreen::pokes_cb(void* Data, Evas_Object* Object, void* EventInfo)
{
    WebViewScreen::Launch(URL_POKES_PAGE, true);
    elm_genlist_item_selected_set(static_cast<Elm_Object_Item*>(EventInfo), EINA_FALSE);
}

void SettingsScreen::gl_PhotosSel(void *data, Evas_Object *obj, void *event_info)
{
    UserPhotosScreen *phscreen = new UserPhotosScreen(nullptr, Config::GetInstance().GetUserId().c_str());

    Application::GetInstance()->AddScreen(phscreen);
    elm_genlist_item_selected_set((Elm_Object_Item *) event_info, EINA_FALSE);
}

void SettingsScreen::gl_EventScreenSel(void *data, Evas_Object *obj, void *event_info)
{
#ifdef EVENT_NATIVE_VIEW
    EventMainPage *eventscreen = new EventMainPage();
    Application::Get()->AddScreen(eventscreen);
#else
    WebViewScreen::Launch(URL_EVENT_MAIN_PAGE, true);
#endif
    elm_genlist_item_selected_set((Elm_Object_Item *) event_info, EINA_FALSE);
}

void SettingsScreen::gl_SavedFolderSel(void *data, Evas_Object *obj, void *event_info)
{
    WebViewScreen::Launch(URL_SAVED_FOLDER, true);
    elm_genlist_item_selected_set((Elm_Object_Item *) event_info, EINA_FALSE);
}

void SettingsScreen::gl_sel(void *data, Evas_Object *obj, void *event_info)
{
    elm_genlist_item_selected_set((Elm_Object_Item *) event_info, EINA_FALSE);
}

void SettingsScreen::gl_user_sel(void *data, Evas_Object *obj, void *event_info)
{
    Log::info(LOG_TAG_FACEBOOK,"SettingsScreen::gl_user_sel->Profile clicked");
    //Create the Userprofile Screen object.
    /*if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_USER_PROFILE) {
        UserProfile *scr = new UserProfile(Config::GetInstance().GetUserId());
        Application::GetInstance()->AddScreen(scr);
    }*/
    OwnProfileScreen *scr = new OwnProfileScreen();
    Application::GetInstance()->AddScreen(scr);

    elm_genlist_item_selected_set((Elm_Object_Item *) event_info, EINA_FALSE);
}

void SettingsScreen::gl_GroupScreenSel(void *data, Evas_Object *obj, void *event_info)
{
    // Check for Internet Connectivity. If Net connectivity is not available, show a popup & return from this function.
    GroupDescription *groupDescription = static_cast<GroupDescription*>(data);
    if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_GROUP_PROFILE_PAGE) {
        GroupProfilePage *screen = new GroupProfilePage(groupDescription->mId, groupDescription->mName);
        Application::GetInstance()->AddScreen(screen);
        elm_genlist_item_selected_set((Elm_Object_Item *) event_info, EINA_FALSE);
    }
}

void SettingsScreen::gl_SeeAllGroupsSel(void *data, Evas_Object *obj, void *event_info)
{
    GroupSeeAllScreen *screen = new GroupSeeAllScreen();
    Application::GetInstance()->AddScreen(screen);
    elm_genlist_item_selected_set((Elm_Object_Item *) event_info, EINA_FALSE);
}

void SettingsScreen::gl_SuggestedGroupsSel(void *data, Evas_Object *obj, void *event_info)
{
    WebViewScreen::Launch(URL_SUGGESTED_GROUPS, true);
    elm_genlist_item_selected_set((Elm_Object_Item *) event_info, EINA_FALSE);
}

char *SettingsScreen::gl_text_get(void *data, Evas_Object *obj, const char *part)
{
    char *str = ((Field *) data)->mText;
    if (!strcmp(part, "elm.text")) {
        return strdup(str);
    }
    return nullptr;
}

Evas_Object *SettingsScreen::gl_titles_data_get(void *data, Evas_Object *obj, const char *part)
{
    Field *fieldData = static_cast<Field *>(data);

    Evas_Object *titleData = elm_layout_add(obj);
    evas_object_size_hint_weight_set(titleData, 1, 1);
    evas_object_size_hint_min_set(titleData, R->SETTINGS_TITLES_LIST_ITEM_WIDTH, R->SETTINGS_TITLES_LIST_ITEM_HEIGHT);
    evas_object_size_hint_max_set(titleData, R->SETTINGS_TITLES_LIST_ITEM_WIDTH, R->SETTINGS_TITLES_LIST_ITEM_HEIGHT);
    elm_layout_file_set(titleData, Application::mEdjPath, "settings_title");
    evas_object_show(titleData);

    //Add Title Name
    Evas_Object *titleName = elm_label_add(titleData);
    elm_object_part_content_set(titleData,"setng_title_name_spacer",titleName);
    const char *titleLocalizedText = i18n_get_text(fieldData->mText);
    if (titleLocalizedText)
    {
        char *titleTxtFormatStr = WidgetFactory::WrapByFormat2(SANS_MEDIUM_BDC1C9_FORMAT, R->SETTINGS_TITLES_ITEM_FONT_SIZE, titleLocalizedText);
        elm_object_text_set(titleName, titleTxtFormatStr);
        delete []titleTxtFormatStr;
        elm_label_line_wrap_set(titleName, ELM_WRAP_WORD);
    }
    evas_object_show(titleName);

    return titleData;
}

Evas_Object *SettingsScreen::gl_categories_data_get(void *data, Evas_Object *obj, const char *part)
{
    Field *fieldData = static_cast<Field *>(data);

    Evas_Object *categoriesData = elm_layout_add(obj);
    evas_object_size_hint_min_set(categoriesData, R->SETTINGS_CATEGORIES_LIST_ITEM_WIDTH, R->SETTINGS_CATEGORIES_LIST_ITEM_HEIGHT);
    evas_object_size_hint_max_set(categoriesData, R->SETTINGS_CATEGORIES_LIST_ITEM_WIDTH, R->SETTINGS_CATEGORIES_LIST_ITEM_HEIGHT);
    elm_layout_file_set(categoriesData, Application::mEdjPath, "settings_item");
    evas_object_show(categoriesData);

    //Add Icon
    Evas_Object *categoryIcon = elm_icon_add(categoriesData);
    elm_layout_content_set(categoriesData, "setng_icon", categoryIcon);
    ELM_IMAGE_FILE_SET(categoryIcon, GetSettingsScreenIcon((void *) data), nullptr);
    evas_object_show(categoryIcon);

    //Add Category Name
    Evas_Object *categoryName = elm_label_add(categoriesData);
    elm_object_part_content_set(categoriesData,"setng_name_spacer",categoryName);

    const char *categoryLocalizedTxt = i18n_get_text(fieldData->mText);
    if (categoryLocalizedTxt)
    {
        char *categoryTxtFormatStr = WidgetFactory::WrapByFormat2(SANS_REGULAR_4e5665_FORMAT, R->SETTINGS_CATEGORIES_ITEM_FONT_SIZE, categoryLocalizedTxt);
        elm_object_text_set(categoryName, categoryTxtFormatStr);
        delete []categoryTxtFormatStr;
        elm_label_line_wrap_set(categoryName, ELM_WRAP_WORD);
    }
    evas_object_show(categoryName);

    return categoriesData;
}

Evas_Object *SettingsScreen::gl_groups_data_get(void *data, Evas_Object *obj, const char *part)
{
    Field *fieldData = static_cast<Field *>(data);

    Evas_Object *groupsData = elm_layout_add(obj);
    evas_object_size_hint_min_set(groupsData, R->SETTINGS_CATEGORIES_LIST_ITEM_WIDTH, R->SETTINGS_CATEGORIES_LIST_ITEM_HEIGHT);
    evas_object_size_hint_max_set(groupsData, R->SETTINGS_CATEGORIES_LIST_ITEM_WIDTH, R->SETTINGS_CATEGORIES_LIST_ITEM_HEIGHT);
    elm_layout_file_set(groupsData, Application::mEdjPath, "settings_item");
    evas_object_show(groupsData);

    //Add Icon
    Evas_Object *groupIcon = elm_image_add(groupsData);
    elm_object_part_content_set(groupsData, "setng_icon", groupIcon);
    mActionAndData->UpdateImageLayoutAsync(fieldData->mIconPath, groupIcon);
    evas_object_show(groupIcon);

    //Add Category Name
    Evas_Object *groupName = elm_label_add(groupsData);
    elm_label_ellipsis_set(groupName, true);
    elm_object_part_content_set(groupsData,"setng_name_spacer", groupName);
    char *groupNameFormatStr = WidgetFactory::WrapByFormat2(SANS_REGULAR_4e5665_FORMAT, R->SETTINGS_CATEGORIES_ITEM_FONT_SIZE, fieldData->mText);

    std::string groupNameStr = groupNameFormatStr;
    Utils::FoundAndReplaceText(groupNameStr, "&", "&amp;" );

    elm_object_text_set(groupName, groupNameStr.c_str());

    delete []groupNameFormatStr;

    evas_object_show(groupName);

    return groupsData;
}

Evas_Object *SettingsScreen::gl_see_all_data_get(void *data, Evas_Object *obj, const char *part)
{
    Field *fieldData = static_cast<Field *>(data);

    Evas_Object *titleData = elm_layout_add(obj);
    evas_object_size_hint_weight_set(titleData, 1, 1);
    evas_object_size_hint_min_set(titleData, R->SETTINGS_TITLES_LIST_ITEM_WIDTH, R->SETTINGS_TITLES_LIST_ITEM_HEIGHT);
    evas_object_size_hint_max_set(titleData, R->SETTINGS_TITLES_LIST_ITEM_WIDTH, R->SETTINGS_TITLES_LIST_ITEM_HEIGHT);
    elm_layout_file_set(titleData, Application::mEdjPath, "settings_item_see_all");
    evas_object_show(titleData);

    //Add Title Name
    Evas_Object *titleName = elm_label_add(titleData);
    elm_object_part_content_set(titleData,"setng_title_name_spacer",titleName);
    const char *titleLocalizedText = i18n_get_text(fieldData->mText);
    if (titleLocalizedText)
    {
        char *titleTxtFormatStr = WidgetFactory::WrapByFormat2(SANS_REGULAR_4e5665_FORMAT, R->SETTINGS_CATEGORIES_ITEM_FONT_SIZE, titleLocalizedText);
        elm_object_text_set(titleName, titleTxtFormatStr);
        delete []titleTxtFormatStr;
        elm_label_line_wrap_set(titleName, ELM_WRAP_WORD);
    }
    evas_object_show(titleName);

    return titleData;
}

char *SettingsScreen::GetSettingsScreenIcon(void *data)
{
    char *str = ((Field *) data)->mIconPath;
    return strdup(str);
}

void SettingsScreen::account_settings_sel_cb(void* data, Evas_Object* obj, void* event_info)
{
    WebViewScreen::Launch(URL_ACCOUNT_SETTINGS, true);
    elm_genlist_item_selected_set((Elm_Object_Item *) event_info, EINA_FALSE);
}

void SettingsScreen::help_center_sel_cb(void* data, Evas_Object* obj, void* event_info)
{
    WebViewScreen::Launch(URL_HELP_CENTER);
    elm_genlist_item_selected_set((Elm_Object_Item *) event_info, EINA_FALSE);
}

void SettingsScreen::privacy_shortcuts_sel_cb(void* data, Evas_Object* obj, void* event_info)
{
    WebViewScreen::Launch(URL_PRIVACY_SHORTCUTS, true);
    elm_genlist_item_selected_set((Elm_Object_Item *) event_info, EINA_FALSE);
}

void SettingsScreen::terms_n_policies_sel_cb(void* data, Evas_Object* obj, void* event_info)
{
    WebViewScreen::Launch(URL_TERMS_N_POLICIES);
    elm_genlist_item_selected_set((Elm_Object_Item *) event_info, EINA_FALSE);
}

void SettingsScreen::about_sel_cb(void* data, Evas_Object* obj, void* event_info)
{
    Application::GetInstance()->AddScreen(new AboutApp(nullptr));
    elm_genlist_item_selected_set((Elm_Object_Item *) event_info, EINA_FALSE);
}

void SettingsScreen::log_out_sel_cb(void* data, Evas_Object* obj,
        void* event_info)
{
    /* Handling for Log Out Process */
	Log::info(LOG_TAG_FACEBOOK,"SettingsScreen::log_out_sel_cb");
    SettingsScreen* me = static_cast<SettingsScreen *>(data);
    me->CreateLogOutPopUp();
    elm_genlist_item_selected_set((Elm_Object_Item *) event_info, EINA_FALSE);
}

void SettingsScreen::on_logout_completed(void* object, char* respond, int code) {
    Log::debug(LOG_FACEBOOK, "SettingsScreen::on_logout_completed");

    //Check internet connection. Log out without verification request when device in offline mode.
    if (ConnectivityManager::Singleton().IsConnected()) {
        //Check curl code. If it is not valid, display a connectivity error popup & go back to Settings Screen.
        if (CURLE_OK != code) {
            Application::GetInstance()->RemoveTopScreen();
            WidgetFactory::ShowAlertPopup("IDS_LOGIN_CONNECT_ERR");
            if (Application::GetInstance()->mDataService) {
                Application::GetInstance()->mDataService->setLoginState(true);
            }
            free(respond);
            return;
        }

        JsonObject * rootObject;
        JsonParser * parser = openJsonParser(respond, &rootObject);
        if (rootObject) {
            const char* error = json_object_get_string_member(rootObject,"error_msg");
            if (error) {
                if (FBRES_INVALID_SESKEY_ECODE != GetGint64FromJson(rootObject, "error_code")) {
                    Log::debug(LOG_FACEBOOK, "Log Out Failed due the Error : %s", error);
                    Application::GetInstance()->RemoveTopScreen();
                    WidgetFactory::ShowAlertPopup(error);
                    if (Application::GetInstance()->mDataService) {
                        Application::GetInstance()->mDataService->setLoginState(true);
                    }
                    free(respond);
                    g_object_unref(parser);
                    return;
                }
            }
        }

        g_object_unref(parser);
        free(respond);

        Log::debug(LOG_FACEBOOK, "Log Out success");
    }
    if (Application::GetInstance()->mDataService) {
        Application::GetInstance()->mDataService->setLoginState(false);
        Application::GetInstance()->mDataService->clearDownloadRequest();
    }

    AppEvents::Get().Notify(eACCOUNT_CHANGED, nullptr);

    LogoutRequest::RedirectToLogin(nullptr);
}

void SettingsScreen::CreateLogOutPopUp()
{
    mLogOutPopUp = elm_popup_add(getMainLayout());
    elm_popup_align_set(mLogOutPopUp, ELM_NOTIFY_ALIGN_FILL, 0.5);

    evas_object_smart_callback_add(mLogOutPopUp, "block,clicked", pop_up_hide_cb, nullptr);
    Evas_Object *cancelLogout = elm_layout_add(mLogOutPopUp);
    elm_layout_file_set(cancelLogout, Application::mEdjPath, "confirmation_dialog");
    elm_object_content_set(mLogOutPopUp, cancelLogout);

    elm_object_translatable_part_text_set(cancelLogout, "top_text", "IDS_SETTING_LOG_OUT");
    elm_object_translatable_part_text_set(cancelLogout, "description", "IDS_SETTING_LOG_OUT_NOW");
    elm_object_translatable_part_text_set(cancelLogout, "text.keep", "IDS_CANCEL");
    elm_object_translatable_part_text_set(cancelLogout, "text.discard", "IDS_SETTING_LOG_OUT");

    elm_object_signal_callback_add(cancelLogout, "mouse,clicked,*", "keep", pop_up_cancel_btn_cb, nullptr);
    elm_object_signal_callback_add(cancelLogout, "mouse,clicked,*", "discard", pop_up_log_out_btn_cb, this);

    evas_object_show(cancelLogout);
    evas_object_show(mLogOutPopUp);
}

void SettingsScreen::SetPathAndUploadAvatar(const char *filePath) {
    assert(filePath);
    free(mAvatarPath);
    mAvatarPath = SAFE_STRDUP(filePath);
    evas_object_image_file_set(mOwnAvatar, filePath, nullptr);
}

void SettingsScreen::ClearAvatarPath() {
    free(mAvatarPath);
    mAvatarPath = nullptr;
}

void SettingsScreen::pop_up_log_out_btn_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::info("SettingsScreen::pop_up_log_out_btn_cb");
    SettingsScreen* me = static_cast<SettingsScreen *>(data);

    evas_object_hide(mLogOutPopUp);
    evas_object_del(mLogOutPopUp);

    if (Application::GetInstance()->mDataService) {
        Application::GetInstance()->mDataService->setLoginState(false);
    }

    mLogOutPopUp = nullptr;

    if(!me->mLogOutReq) {
        me->mLogOutReq =  new LogoutRequest(on_logout_completed, nullptr);
    }

    me->mScreenInProgress = new SignUpInProgressScreen(nullptr);
    me->mScreenInProgress->mInProgress = true;
    me->mScreenInProgress->mRequestType = LogOutReq;
    Application::GetInstance()->AddScreen(me->mScreenInProgress);
    me->mScreenInProgress->mGraphRequest = me->mLogOutReq;
    me->mLogOutReq->DoLogout();
}

void SettingsScreen::pop_up_hide_cb(void *data, Evas_Object *obj, void *EventInfo)
{
    if (obj) {
        clearPopup();
    }
}

void SettingsScreen::pop_up_cancel_btn_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    if (obj) {
        clearPopup();
    }
}

bool SettingsScreen::clearPopup()
{
    bool res = (mLogOutPopUp != NULL);

    if(res)
    {
        evas_object_del(mLogOutPopUp);
        mLogOutPopUp = NULL;
    }
    return res;
}

void SettingsScreen::Reload_Profile_Data_cb(void *data, void *thisScreen)
{
    SettingsScreen *me = static_cast<SettingsScreen *> (thisScreen);
    elm_genlist_realized_items_update(me->mLayout);
}

void SettingsScreen::settings_screen_instance_set(SettingsScreen * inst_scr) {
    settings_screen_instance = inst_scr;
}

SettingsScreen *SettingsScreen::settings_screen_instance_get() {
    return settings_screen_instance;
}

void SettingsScreen::nearBy_places_sel_cb(void *data, Evas_Object *obj, void *event_info)
{
    NearByPlacesScreen *places = new NearByPlacesScreen();
    Application::GetInstance()->AddScreen(places);
    elm_genlist_item_selected_set((Elm_Object_Item *) event_info, EINA_FALSE);
}


