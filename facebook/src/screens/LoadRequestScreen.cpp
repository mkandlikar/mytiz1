#include "Common.h"
#include "FacebookSession.h"
#include "FbRespondBase.h"
#include "LoadRequestScreen.h"
#include "Log.h"


LoadRequestScreen::LoadRequestScreen(ScreenBase *parentScreen):ScreenBase(parentScreen)
{
    Log::debug("LoadRequestScreen");
    mResponse = nullptr;
    mRequest = nullptr;
}

LoadRequestScreen::~LoadRequestScreen()
{
    Log::debug("~LoadRequestScreen");
    delete mResponse;
    FacebookSession::GetInstance()->ReleaseGraphRequest(mRequest);
}

/**
 * @brief - initiate an http request to facebook
 * @param id[in] - id of the object for which data is requested
 */
void LoadRequestScreen::StartRequest()
{
    Log::debug("StartRequest()");
    ShowProgressBar();
    if (!mRequest) {
        mRequest = DoRequest();
    }
}

void LoadRequestScreen::ShowProgressBar()
{
    ProgressBarShow();
}

/**
 * @brief Hides the ProgressBar
 */
void LoadRequestScreen::HideProgressBar()
{
    ProgressBarHide();
}

/**
 * @brief Callback which is called when GET-request is completed
 * @param object - callback object
 * @param response - http response
 * @param code - curl code
 */
void LoadRequestScreen::on_request_completed(void* object, char* response, int code)
{
    Log::debug("on_requests_completed()");

    LoadRequestScreen * screen = (LoadRequestScreen *) object;
    screen->HideProgressBar();

    FacebookSession::GetInstance()->ReleaseGraphRequest(screen->mRequest);

    if (code == CURLE_OK) {
        Log::debug("response: %s", response);

        FbRespondBase * parsedResponse = screen->ParseResponse(response);

        if (!parsedResponse) {
            Log::debug("Error parsing http response");
        } else {
            delete screen->mResponse;
            screen->mResponse = parsedResponse;

            if (parsedResponse->mError) {
                Log::debug("Error getting parsed response, error = %s", parsedResponse->mError->mMessage);
            } else {
                screen->SetData(parsedResponse);
            }
        }
    } else {
        Log::debug("Error sending http request");
    }

    if (response) {
        //Attention! The client should take care of freeing the response buffer.
        free(response);
    }
}
