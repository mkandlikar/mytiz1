#include "Common.h"
#include "Config.h"
#include "ConnectivityManager.h"
#include "FbRespondGetLinkInfo.h"
#include "Log.h"
#include "LoadRequestScreen.h"
#include "PostComposerScreen.h"
#include "Utils.h"
#include "WebViewScreen.h"

#include <Evas.h>
#include <EWebKit.h>
#include <sstream>
#include <utils_i18n.h>

/**
 * @brief Construct WebViewScreen object
 * @param originalUrl - original url to be loaded in this screen (without credentials etc)
 * @param url - url to be loaded in this screen
 * @param action_data - external ActionaAndData object to be used in this screen
 */
WebViewScreen::WebViewScreen(const char *originalUrl, const char * url, bool enableMenu, ActionAndData * action_data) : LoadRequestScreen(NULL)
{
    mOriginalUrl = SAFE_STRDUP(originalUrl);
    mWebViewActionNData = action_data;

    InitData();
    InitScreen();
    InitMenu(enableMenu);
    InitUrl(url);
}

void WebViewScreen::InitData() {
    mScreenId = SID_WEB_VIEW;
    mPopup = NULL;
    mUrl = NULL;
    mProgressBar = NULL;
    mWebView = NULL;
    mUrlCopyLabel = NULL;
    mThread = nullptr;
    mIsUrlValid = false;
    mIsFullScreen = false;
}

void WebViewScreen::InitScreen() {
    mLayout = elm_layout_add(getParentMainLayout());
    evas_object_size_hint_weight_set(mLayout, 1, 1);
    elm_layout_file_set(mLayout, Application::mEdjPath, "webview_screen");

    //create webview
    Evas *evas = evas_object_evas_get(mLayout);
    mWebView= ewk_view_add(evas);
    //Set the user agent Header
    std::string strUserAgent = Utils::GetHTTPUserAgent();
    if (!strcmp(mOriginalUrl, URL_EVENT_MAIN_PAGE)) {
        strUserAgent = Utils::ReplaceString(strUserAgent, "FBAN/FB4T;", "");
    }
    ewk_view_user_agent_set (mWebView, strUserAgent.c_str());

    evas_object_smart_callback_add(mWebView, "load,started", on_load_started, this);
    evas_object_smart_callback_add(mWebView, "load,finished", on_load_finished, this);
    evas_object_smart_callback_add(mWebView, "load,progress", on_load_progress, this);
    evas_object_smart_callback_add(mWebView, "title,changed", on_title_changed, this);
    evas_object_smart_callback_add(mWebView, "policy,newwindow,decide", new_window_policy_cb, this);
    evas_object_smart_callback_add(mWebView, "policy,navigation,decide", navigation_policy_cb, this);
    evas_object_smart_callback_add(mWebView, "url,changed", on_url_changed, this);
    evas_object_smart_callback_add(mWebView, "policy,response,decide", response_policy_cb, this);

    evas_object_smart_callback_add(mWebView, "fullscreen,enterfullscreen", on_enter_fullscreen, this);
    evas_object_smart_callback_add(mWebView, "fullscreen,exitfullscreen", on_exit_fullscreen, this);

    elm_object_part_content_set(mLayout, "web_view", mWebView);
    evas_object_show(mWebView);

    //create progress bar
    mProgressBar = elm_progressbar_add(mLayout);
    elm_object_style_set(mProgressBar, "custom");
    elm_progressbar_horizontal_set(mProgressBar, EINA_TRUE);
    evas_object_size_hint_align_set(mProgressBar, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_size_hint_weight_set(mProgressBar, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    elm_progressbar_value_set(mProgressBar, 0.0);
    elm_object_part_content_set(mLayout, "progressbar_swallow", mProgressBar);
    evas_object_show(mProgressBar);

    evas_object_show(mLayout);

    elm_object_signal_callback_add(mLayout, "close_button_clicked", "", on_close_button_clicked, this);

    AppEvents::Get().Subscribe(ePAUSE_EVENT, this);
    AppEvents::Get().Subscribe(eRESUME_EVENT, this);

    Application::GetInstance()->EnableHWBack();
}

void WebViewScreen::InitMenu (bool enableMenu) {
    if (enableMenu) {
        if (!mMenuEnabled) {
            char *resourcePath = app_get_resource_path();
            if (!resourcePath) {
                return;
            }
            std::string menuImgPath(resourcePath);
            menuImgPath.append("images/more.png");
            free(resourcePath);
            Evas_Object* evasMenuImg = elm_image_add(mLayout);
            elm_image_file_set(evasMenuImg, menuImgPath.c_str(), nullptr);
            elm_layout_content_set(mLayout, "menu_button_img", evasMenuImg);
            evas_object_show(evasMenuImg);
            elm_object_signal_emit(mLayout, "menu,state,default", "");
            elm_object_signal_callback_add(mLayout, "menu_button_clicked", "", on_menu_button_clicked, this);
        }
    } else {
        elm_object_signal_emit(mLayout, "menu,state,hidden", "");
    }
    mMenuEnabled = enableMenu;
}

void WebViewScreen::InitUrl (const char* url) {
    if (url) {
        std::string srcUrl = url;

        if(srcUrl.find("://", 0) == std::string::npos) {
            srcUrl = TRANSFER_PROTOCOL_DEFAULT + srcUrl;
        }

        mUrl = strdup(srcUrl.c_str());
        if (strstr(mOriginalUrl, "http") != mOriginalUrl &&
            strstr(mUrl, TRANSFER_PROTOCOL_SECURE) == mUrl) {
            //Protocol for the actual URL is guessed, so check validity of the URL in a thread.
            mThread = ecore_thread_run(thread_start_cb, thread_end_cb, thread_cancel_cb, this);
        } else {
            StartUrl();
        }
    }
}

void WebViewScreen::thread_start_cb(void *data, Ecore_Thread *thread) {
    if (!thread || ecore_thread_check(thread)) {
        return;
    }

    WebViewScreen *screen = static_cast<WebViewScreen*>(data);

    const char *url = screen->mUrl;
    //Test with CURL, if HTTPS is supported at the host side.
    CURL *curlHandle = curl_easy_init();
    if (curlHandle) {
        curl_easy_setopt(curlHandle, CURLOPT_URL, url);
        curl_easy_setopt(curlHandle, CURLOPT_USERAGENT, Utils::GetHTTPUserAgent());
        curl_easy_setopt(curlHandle, CURLOPT_FOLLOWLOCATION, 1);    //follow redirections - to cope with HTTP codes 301, 302
        curl_easy_setopt(curlHandle, CURLOPT_TIMEOUT, 5);
        curl_easy_setopt(curlHandle, CURLOPT_CONNECTTIMEOUT, 5);

        CURLcode res = curl_easy_perform(curlHandle);
        if (CURLE_OK == res) {
            long int httpcode = 0;
            res = curl_easy_getinfo(curlHandle, CURLINFO_RESPONSE_CODE, &httpcode);
            if (CURLE_OK == res) {
                if (httpcode >= 200 && httpcode < 400) {
                    if (ecore_thread_check(thread) == false) {
                        screen->mIsUrlValid = true;
                    }
                }
            }
        }

        curl_easy_cleanup(curlHandle);
    }
}

void WebViewScreen::thread_end_cb(void *data, Ecore_Thread *thread) {
    if (!thread || ecore_thread_check(thread)) {
        return;
    }

    WebViewScreen *screen = static_cast<WebViewScreen*>(data);

    if (screen->mIsUrlValid == false) {
        Log::info("WebViewScreen::thread_end_cb() - downgrade from HTTPS to HTTP");
        std::string srcUrl = screen->mUrl;
        srcUrl = Utils::ReplaceString(srcUrl, TRANSFER_PROTOCOL_SECURE, TRANSFER_PROTOCOL_PLAIN, 0, true);
        free((void*) screen->mUrl);
        screen->mUrl = strdup(srcUrl.c_str());
    }
    screen->mThread = nullptr;

    screen->StartUrl();
}

void WebViewScreen::thread_cancel_cb(void *data, Ecore_Thread *thread) {
    //at the moment this callback is empty
}

void WebViewScreen::StartUrl() {
    ewk_view_url_set(mWebView, mUrl);

    mUrlCopyLabel = elm_label_add(mLayout);
    elm_object_text_set(mUrlCopyLabel, mUrl);
}

/**
 * @brief Destruction
 */
WebViewScreen::~WebViewScreen()
{
    // Clear the cookies only when destructing the last instance of WebViewScreen.
    // Otherwise do not clear, let them persist between different WebView instances.
    if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_WEB_VIEW) {
        Ewk_Cookie_Manager* cookieManager = ewk_context_cookie_manager_get(ewk_view_context_get(mWebView));
        ewk_cookie_manager_cookies_clear(cookieManager);
    }

    evas_object_del(mProgressBar);
    evas_object_del(mWebView);
    if (mPopup != NULL) {
        evas_object_del(mPopup);
    }
    free((void*) mUrl);
    free((void*) mOriginalUrl);
    if (mThread) {
        ecore_thread_cancel(mThread);
    }
}

/**
 * @brief Overloaded Push method
 */
void WebViewScreen::Push()
{
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(elm_naviframe_top_item_get(getNf()), EINA_FALSE, EINA_FALSE);
}

/**
 * @brief This callback is called to report load progress
 * @param data - user data passed into callback
 * @param obj - object which generated event
 * @param event_info - progress rate
 */
void WebViewScreen::on_load_progress(void *data, Evas_Object *obj, void *event_info)
{
    WebViewScreen *screen = static_cast<WebViewScreen *>(data);
    double progress = *((double*)event_info);
    if (progress < 1.0) {
        elm_progressbar_value_set(screen->mProgressBar, progress);
    } else {
        elm_object_signal_emit(screen->mLayout, "progress,state,hidden", "");
    }
}


/**
 * @brief This callback is called when loading is started
 * @param data - user data passed into callback
 * @param obj - object which generated event
 * @param event_info - event information
 */
void WebViewScreen::on_load_started(void *data, Evas_Object *obj, void *event_info)
{
    Log::debug("on_load_started");
    WebViewScreen *screen = static_cast<WebViewScreen *>(data);
    //elm_object_part_text_set(screen->mLayout, "title_text", "");
    elm_progressbar_value_set(screen->mProgressBar, 0.0);
    elm_object_signal_emit(screen->mLayout, "progress,state,shown", "");
}

/**
 * @brief This callback is called when loading is finished
 * @param data - user data passed into callback
 * @param obj - object which generated event
 * @param event_info - event information
 */
void WebViewScreen::on_load_finished(void *data, Evas_Object *obj, void *event_info)
{
    Log::debug("on_load_finished");
    WebViewScreen *screen = static_cast<WebViewScreen *>(data);
    elm_object_signal_emit(screen->mLayout, "progress,state,hidden", "");
}

/**
 * @brief This callback is called when webview title is changed
 * @param data - user data passed into callback
 * @param obj - object which generated event
 * @param event_info - event information
 */
 void WebViewScreen::on_title_changed(void *data, Evas_Object *obj, void *event_info)
{
    WebViewScreen *screen = static_cast<WebViewScreen *>(data);
    assert(screen);
    const char *title = static_cast<const char*>(event_info);

    if (title) {
        Log::debug("WebViewScreen::on_title_changed(), new title = '%s'", title);
        elm_object_part_text_set(screen->mLayout, "title_text", title);
    }
    screen->StartRequest();
}

 void WebViewScreen::on_enter_fullscreen(void *data, Evas_Object *obj, void *event_info) {
     WebViewScreen *screen = static_cast<WebViewScreen *>(data);
     screen->mIsFullScreen = true;
 }

 void WebViewScreen::on_exit_fullscreen(void *data, Evas_Object *obj, void *event_info) {
     WebViewScreen *screen = static_cast<WebViewScreen *>(data);
     screen->mIsFullScreen = false;
 }

void WebViewScreen::new_window_policy_cb(void* data, Evas_Object *obj, void *event_info) {
    if (!event_info)
        return;

    Ewk_Policy_Decision* PolicyDecision = static_cast<Ewk_Policy_Decision*>(event_info);
    const char* url = ewk_policy_decision_url_get(PolicyDecision);

    if (url && *url) {
        WebViewScreen* me = static_cast<WebViewScreen*>(data);

        // Unlike a browser application, webview is not supposed to handle multiple windows (tabs).
        ewk_policy_decision_ignore(PolicyDecision);

        char *unescapedUrl = Utils::unescape(url);
        if (unescapedUrl) {
            if (check_url_for_group_file(unescapedUrl)) {
                download_document_file(url);
            } else if (Utils::HasEnding(unescapedUrl, ".pdf")) {
                download_document_file(url);
            } else if ((strstr(unescapedUrl, "&url=http") || strstr(unescapedUrl, "?u=http")) && strstr(unescapedUrl, ".pdf&")) {
                std::string fileLinkUrl = get_contained_link_to_file(unescapedUrl);
                if (!fileLinkUrl.empty()) {
                    download_document_file(fileLinkUrl.c_str());
                }
            } else if (strstr(unescapedUrl, "facebook.com/sharer?")) {
                me->ShareLink();
            } else {
                Launch(url);
            }
            free(unescapedUrl);
        }
    }
}

void WebViewScreen::response_policy_cb(void* data, Evas_Object *obj, void *event_info)
{
    if (event_info) {
        Ewk_Policy_Decision* PolicyDecision = static_cast<Ewk_Policy_Decision*>(event_info);
        int type_code = ewk_policy_decision_type_get(PolicyDecision);
        if (EWK_POLICY_DECISION_DOWNLOAD == type_code) {
            //Tizen_2.4 specifics: webview tries to show the file, even if it's not textual
            const char *mimetype = ewk_policy_decision_response_mime_get(PolicyDecision);
            if (mimetype && *mimetype && (strstr(mimetype, "application/") == mimetype)) {
                ewk_policy_decision_ignore(PolicyDecision);
            }
        }
    }
}

void WebViewScreen::navigation_policy_cb(void* data, Evas_Object *obj, void *event_info)
{
    if (!event_info) {
        return;
    }

    Ewk_Policy_Decision* PolicyDecision = static_cast<Ewk_Policy_Decision*>(event_info);
    const char* url = ewk_policy_decision_url_get(PolicyDecision);
    char *unescapedUrl = Utils::unescape(url);
    if (unescapedUrl) {
        WebViewScreen *screen = static_cast<WebViewScreen *>(data);
        if (strstr(unescapedUrl, "http") == unescapedUrl && strstr(unescapedUrl, "/download/") && strstr(unescapedUrl, "?hash=") && strstr(unescapedUrl, "&__tn__=")) {
            ;
        } else if (check_url_for_group_file(unescapedUrl)) {
            ewk_policy_decision_ignore(PolicyDecision);
            download_document_file(url);
        } else if (Utils::HasEnding(unescapedUrl, ".pdf")) {
            ewk_policy_decision_ignore(PolicyDecision);
            download_document_file(url);
            ewk_view_back (screen->mWebView);
        } else if ((strstr(unescapedUrl, "&url=http") && strstr(unescapedUrl, ".pdf&")) ||
                   (strstr(unescapedUrl, "http") && strstr(unescapedUrl, ".pdf?") && !strstr(unescapedUrl, ".pdf?dl=0"))) {
            ewk_policy_decision_ignore(PolicyDecision);
            std::string fileLinkUrl = get_contained_link_to_file(unescapedUrl);
            if (!fileLinkUrl.empty()) {
                if (fileLinkUrl.find("dropbox.com/") != std::string::npos) {
                    fileLinkUrl.append("?dl=1");  //actually download instead of viewing in an HTML frame
                }
                download_document_file(fileLinkUrl.c_str());
                ewk_view_back (screen->mWebView);
            }
        } else if (!strncmp(unescapedUrl, "tel:", strlen("tel:"))) {
            ewk_policy_decision_ignore(PolicyDecision);
            // Launch Dialer app
            send_launch_request(unescapedUrl, APP_CONTROL_OPERATION_DIAL);
        } else if (!strncmp(unescapedUrl, "mailto:", strlen("mailto:"))) {
            ewk_policy_decision_ignore(PolicyDecision);
            // Launch Email app
            send_launch_request(unescapedUrl, APP_CONTROL_OPERATION_COMPOSE);
        }
        free(unescapedUrl);
    }
}

/**
 * @brief - Launches native app based on provided uri and APP_CONTROL_OPERATION id
 */
bool WebViewScreen::send_launch_request(const char* uri, const char* operation_id)
{
    app_control_h app_control;

    app_control_create(&app_control);
    app_control_set_operation(app_control, operation_id);
    app_control_set_uri(app_control, uri);

    bool result = app_control_send_launch_request(app_control, NULL, NULL) == APP_CONTROL_ERROR_NONE;
    Log::debug("WebViewScreen::send_launch_request - %s!",
            result ? "Success" : "Failure");
    app_control_destroy(app_control);

    return result;
}

/**
 * @brief - Downloads file based on provided url
 */
void WebViewScreen::download_document_file(const char* url)
{
    if (send_launch_request(url, APP_CONTROL_OPERATION_DOWNLOAD)) {
        Utils::ShowToast(Application::GetInstance()->GetTopScreen()->getMainLayout(), i18n_get_text("IDS_DOWNLOAD_DOCUMENT_FILE"));
    }
}

void WebViewScreen::on_url_changed(void* data, Evas_Object *obj, void *event_info)
{
    if (data) {
        const char *newUrl = ewk_view_url_get(obj);
        if (newUrl) {
            WebViewScreen *me = static_cast<WebViewScreen *>(data);
            if (me->mUrl && strcmp(newUrl, me->mUrl) != 0) {
                Ewk_Back_Forward_List* bf_list = ewk_view_back_forward_list_get(me->mWebView);
                if (ewk_back_forward_list_count(bf_list) > 1 // Not required for webviews in the initial state
                    && strstr(newUrl, ".facebook.com/")      // Reload only FB URLs
                    && Config::GetInstance().IsLogin()) {    // Avoid reloading for the authentication and signup requests
                    ewk_view_reload(me->mWebView);
                }
            }
            free((void*) me->mUrl);
            me->mUrl = strdup(newUrl);

            if (strstr(newUrl, "https://www.youtube.com/watch?") == newUrl ||
                strstr(newUrl, "https://m.youtube.com/watch?") == newUrl) {
                me->InitMenu(true);
            }
        }
    }
}

 /**
  * @brief This callback is called when user pressed on close button
  * @param data - user data passed into callback
  * @param obj - object which generated event
  * @param emission - signal description
  * @param source - who emitted the signal
  */
void WebViewScreen::on_close_button_clicked (void *data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::debug("on_close_button_clicked");
    WebViewScreen *screen = static_cast<WebViewScreen *>(data);
    screen->Pop();
}

/**
 * @brief This callback is called when user pressed on menu button
 * @param data - user data passed into callback
 * @param obj - object which generated event
 * @param emission - signal description
 * @param source - who emitted the signal
 */
void WebViewScreen::on_menu_button_clicked (void *data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::debug("on_menu_button_clicked");
    WebViewScreen *screen = static_cast<WebViewScreen *>(data);

    if(!screen->mPopup) {
        static Elm_Genlist_Item_Class itc;
        Evas_Object *box;
        Evas_Object *genlist;

        screen->mPopup = elm_popup_add(screen->getMainLayout());
        elm_popup_align_set(screen->mPopup, ELM_NOTIFY_ALIGN_FILL, 0.5);

        eext_object_event_callback_add(screen->mPopup, EEXT_CALLBACK_BACK, eext_popup_back_cb, NULL);
        evas_object_smart_callback_add(screen->mPopup, "block,clicked", on_menu_dismiss_cb, screen);

        evas_object_size_hint_weight_set(screen->mPopup, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);

        Evas_Object *popuplayout = elm_layout_add(screen->mPopup);
        evas_object_size_hint_weight_set(popuplayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
        evas_object_size_hint_align_set(popuplayout, EVAS_HINT_FILL, EVAS_HINT_FILL);
        elm_layout_file_set(popuplayout, Application::mEdjPath, "webview_popup");

        /* box */
        box = elm_box_add(popuplayout);
        evas_object_size_hint_weight_set(box, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);

        /* genlist */
        genlist = elm_genlist_add(box);
        evas_object_size_hint_weight_set(genlist, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
        evas_object_size_hint_align_set(genlist, EVAS_HINT_FILL, EVAS_HINT_FILL);

        itc.item_style = "default";
        itc.func.text_get = menu_item_text_get_cb;
        itc.func.content_get = menu_item_icon_get_cb;
        itc.func.state_get = NULL;
        itc.func.del = NULL;

        int actions_num = 3;
        for (int i = 0; i < actions_num; i++) {
            elm_genlist_item_append(genlist, &itc, (void *) i, NULL, ELM_GENLIST_ITEM_NONE, menu_item_select_cb, data);
        }

        elm_box_pack_end(box, genlist);
        evas_object_show(genlist);

        evas_object_size_hint_min_set(box, -1, actions_num * R->POPUP_WIDGET_MENU_ITEM_HEIGHT);
        elm_object_part_content_set(popuplayout, "list", box);
        elm_object_content_set(screen->mPopup, popuplayout);
        evas_object_show(screen->mPopup);

        elm_object_signal_callback_add(popuplayout, "back_button_clicked", "", (Edje_Signal_Cb) on_back_button_clicked, screen);
        elm_object_signal_callback_add(popuplayout, "forward_button_clicked", "", (Edje_Signal_Cb) on_forward_button_clicked, screen);

        bool isBackPossible  = ewk_view_back_possible (screen->mWebView);
        bool isForwardPossible = ewk_view_forward_possible (screen->mWebView);

        if (!isBackPossible && !isForwardPossible) {
            elm_object_signal_emit(popuplayout, "navigation_bar,state,hidden", "");
            //This is a workaround. Popup's edges are cut - it is observed only on Tizen 3.0
            if (Application::GetInstance()->GetPlatformVersion() >= 3) {
                elm_object_signal_emit(popuplayout, "no_top_bar_ext,state,notopbar_ext", "");
            } else {
                elm_object_signal_emit(popuplayout, "no_top_bar,state,notopbar", "");
            }
        } else {
            if (Application::GetInstance()->GetPlatformVersion() >= 3) {
                elm_object_signal_emit(popuplayout, "with_top_bar_ext,state,default_ext", "");  //vertically
                elm_object_signal_emit(popuplayout, "navigation_bar,state,default_ext", "");  //to the left
                elm_object_signal_emit(popuplayout, "list_box,state,default_ext", "");  //to the right
            }

            if (!isBackPossible) {
                elm_object_signal_emit(popuplayout, (Application::IsRTLLanguage() ?
                        "back_button,state,disabled,rtl" : "back_button,state,disabled,ltr"), "");
            }

            if (!isForwardPossible) {
                elm_object_signal_emit(popuplayout, (Application::IsRTLLanguage() ?
                        "forward_button,state,disabled,rtl" : "forward_button,state,disabled,ltr"), "");
            }
        }
    }
}

/**
 * @brief This is genlist callback to get text of a context menu item
 * @param data The data passed in the item creation function
 * @param obj The base widget object
 * @param part The part name of the swallow
 * @return The allocated (NOT stringshared) string to set as the text
 */
char* WebViewScreen::menu_item_text_get_cb(void *data, Evas_Object *obj, const char *part)
{
    const char *item_names[] = {
            i18n_get_text("IDS_SHARE_IN_NEW_POST"),
            i18n_get_text("IDS_COPY_LINK"),
            i18n_get_text("IDS_OPEN_IN_BROWSER"),
            i18n_get_text("IDS_SAVE_LINK")
    };

    int index = (int) data;
    char * ret = NULL;
    if (part && !strcmp(part, "elm.text")) {
        ret = item_names[index] != NULL? strdup(item_names[index]): NULL;
    }

    return ret;
}

/**
 * @brief This is genlist callback to get icon of a context menu item
 * @param data The data passed in the item creation function
 * @param obj The base widget object
 * @param part The part name of the swallow
 * @return The image to set as the icon
 */
Evas_Object* WebViewScreen::menu_item_icon_get_cb(void *data, Evas_Object *obj, const char *part)
{
    int index = (int) data;
    Evas_Object *content = NULL;

    if (part && !strcmp(part, "elm.swallow.icon")) {
        content = elm_image_add(obj);
        switch (index) {
        case 0:
            elm_image_file_set(content, ICON_DIR"/share_in_post.png", NULL);
            break;
        case 1:
            elm_image_file_set(content, ICON_DIR"/copy_link.png", NULL);
            break;
        case 2:
            ELM_IMAGE_FILE_SET(content, ICON_DIR"/open_external.png", NULL);
            break;
        case 3:
            elm_image_file_set(content, ICON_DIR"/save.png", NULL);
            break;
        default:
            break;
        }
        evas_object_size_hint_align_set(content, EVAS_HINT_FILL, EVAS_HINT_FILL);
        evas_object_size_hint_weight_set(content, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);

        evas_object_size_hint_min_set(content, 50, 50);
        evas_object_size_hint_max_set(content, 50, 50);
    }
    return content;
}

/**
 * @brief This callback is called when a popup menu item is selected
 * @param data [in] - user data passed in callback
 * @param obj [in] - object which generates click event
 * @param event_info [in] - event information
 */
void WebViewScreen::menu_item_select_cb(void *data, Evas_Object *obj, void *event_info)
{
    WebViewScreen *screen = static_cast<WebViewScreen *>(data);

    Elm_Object_Item *item = (Elm_Object_Item *)event_info;
    int index = (int)elm_object_item_data_get(item);

    screen->ClosePopup();

    switch (index) {
    case 0:
        screen->ShareLink();
        break;
    case 1:
        screen->CopyToClipboard();
        break;
    case 2:
        // Open in Browser
        send_launch_request(screen->mUrl, APP_CONTROL_OPERATION_VIEW);
        break;
    default:
        break;
    }
}

/**
 * @brief Close popup menu
 * @param data
 * @param obj
 * @param event_info
 */
void WebViewScreen::on_menu_dismiss_cb(void *data, Evas_Object *obj, void *event_info)
{
    WebViewScreen *screen = static_cast<WebViewScreen *>(data);
    screen->ClosePopup();
}

/**
 * @brief This callback is called when user pressed on back navigation button
 * @param data - user data passed into callback
 * @param obj - object which generated event
 * @param emission - signal description
 * @param source - who emitted the signal
 */
void WebViewScreen::on_back_button_clicked (void *data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::debug("on_back_button_clicked");
    WebViewScreen *screen = static_cast<WebViewScreen *>(data);

    if (ewk_view_back_possible(screen->mWebView)) {
        screen->ClosePopup();
        ewk_view_back (screen->mWebView);
    }
}

/**
 * @brief This callback is called when user pressed on forward navigation button
 * @param data - user data passed into callback
 * @param obj - object which generated event
 * @param emission - signal description
 * @param source - who emitted the signal
 */
void WebViewScreen::on_forward_button_clicked (void *data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::debug("on_forward_button_clicked");
    WebViewScreen *screen = static_cast<WebViewScreen *>(data);

    if (ewk_view_forward_possible(screen->mWebView)) {
        screen->ClosePopup();
        ewk_view_forward(screen->mWebView);
    }
}

/**
 * @brief Closes menu popup
 */
void WebViewScreen::ClosePopup()
{
    evas_object_del(mPopup);
    mPopup = NULL;
}

/**
 * @brief - Opens PostComposer Screen to share link
 */
void WebViewScreen::ShareLink()
{
    PostComposerScreen* postComposer = new PostComposerScreen(GetParent(), eSHARELINKWEB, NULL, NULL, mUrl, NULL, true);
    Application::GetInstance()->AddScreen(postComposer);
}

/**
 * @brief - Copies url to clipboard
 */
void WebViewScreen::CopyToClipboard()
{
    if (mOriginalUrl) {
        elm_cnp_selection_set(mUrlCopyLabel, ELM_SEL_TYPE_CLIPBOARD, ELM_SEL_FORMAT_TEXT, mOriginalUrl, strlen(mOriginalUrl));
    }
}

/**
 * Place the ProgressBar on the screen
 */
void WebViewScreen::ShowProgressBar()
{
    // do nothing
}

/**
 * @brief Hides the ProgressBar
 */
void WebViewScreen::HideProgressBar()
{
    // do nothing
}

/**
 * @brief Sends request to facebook
 */
GraphRequest * WebViewScreen::DoRequest()
{
    return FacebookSession::GetInstance()->GetInfo(mUrl, on_request_completed, this);
}

/**
 * @brief Parse json response
 */
FbRespondBase* WebViewScreen::ParseResponse(char* response)
{
    return FbRespondGetLinkInfo::createFromJson(response);
}

/**
 * @brief - Update screen with received data
 * @param response[in] - received data
 */
void WebViewScreen::SetData(FbRespondBase * response)
{
    //TODO
}

bool WebViewScreen::HandleBackButton() {
    bool handled = false;

    if (mPopup) {
        ClosePopup();
        handled = true;
    } else if (mIsFullScreen && ewk_view_fullscreen_exit(mWebView)) { //if we are in the fullscreen mode then exit it first.
        handled = true;
    } else if (ewk_view_back_possible(mWebView)) {
        ewk_view_back(mWebView);
        handled = true;
    }
    return handled;
}

// Utility methods to launch webview for URLs commonly used
void WebViewScreen::privacy_shortcuts(void* data, Evas_Object* obj, void* event_info)
{
    // Generate the URL
    char sigBuf[MAX_REQ_WEBURL_LEN] = { 0, };
    time_t timeInSecs = time(NULL);

    int len = Utils::Snprintf_s(sigBuf, MAX_REQ_WEBURL_LEN,"%s%s%s%s%s%ld%s%s%s%s",
                       STR_APP_ID, Config::GetApplicationId(), STR_SESSION_KEY, Config::GetInstance().GetSessionKey().c_str(),
                       STR_TIME, timeInSecs, STR_USER_ID, Config::GetInstance().GetUserId().c_str(), URL_PRIVACY_SHORTCUTS,
                       Config::GetInstance().GetSessionSecret().c_str());

    char targetUrl[MAX_REQ_WEBURL_LEN] = { 0, };
    char *signature = Utils::str2md5(sigBuf, len);
    Utils::Snprintf_s(targetUrl, MAX_REQ_WEBURL_LEN, "%s&%s%s&%s%s&%s%ld&%s%s&%s&%s%s",
             URL_TARGET, STR_APP_ID, Config::GetApplicationId(), STR_SESSION_KEY, Config::GetInstance().GetSessionKey().c_str(),
             STR_TIME, timeInSecs, STR_USER_ID, Config::GetInstance().GetUserId().c_str(), URL_PRIVACY_SHORTCUTS,
             STR_SIGNATURE, signature);
    Log::debug("privacy_shortcuts(), url='%s'", targetUrl);
    free(signature);

    WebViewScreen *acctStngsWebScr = new WebViewScreen(NULL, targetUrl, true);
    Application::GetInstance()->AddScreen(acctStngsWebScr);
}

bool WebViewScreen::check_url_for_deep_link(const char* url)
{
    CHECK_RET(url, false);
    return (strstr(url, "m.facebook.com") ||
            strstr(url, "www.facebook.com") ||
            strstr(url, "l.facebook.com") ||
            strstr(url, "lm.facebook.com") ||
            strstr(url, "Facebook.com"));
}

bool WebViewScreen::check_url_for_group_file(const char* url)
{
    if (!url) {
        return false;
    } else if (strstr(url, "http") == url && strstr(url, "/file/") && strstr(url, "?token=")) {
        return true;  //URL points to a file in a Facebook group
    } else {
        return false;
    }
}

std::string WebViewScreen::get_contained_link(const char* url, bool isDeepLinkUrl) {
    if (!url || !isDeepLinkUrl) {
        return "";
    }

    std::string tempStr(url);
    if (std::string::npos != tempStr.find(REQ_AUTH_URL)) {
        return "";
    }

    int startOffset = tempStr.find(REQ_QUERY_STRING_DLIM);

    if (std::string::npos != startOffset) {
        int endOffset = tempStr.find(REQ_MORE_PARAM_DLIM);
        if (endOffset == std::string::npos) {
            endOffset = tempStr.length();
        }

        char *originalUrl = Utils::GetStringByOffset(url, startOffset, endOffset);
        if (originalUrl) {
            char *unescape = Utils::unescape(strstr(originalUrl, "http"));
            tempStr = unescape ? unescape : "";
            free(unescape);
            delete[] originalUrl;
        }
    } else {
        tempStr = "";
    }
    return tempStr;
}

std::string WebViewScreen::get_contained_link_to_file(const char* url) {
    if (!url || strstr(url, REQ_AUTH_URL)) {
        return "";
    }

    //Calculate the starting offset of the contained link - i.e. the position of "http" pattern.
    const char *start;
    if ((start = strstr(url, "&url=http"))) {
            start += 5;    //exclude "&url="
    } else if ((start = strstr(url, "?u=http"))) {
            start += 3;    //exclude "?u="
    } else if ((start = strstr(url, "http"))) {
        ;    //no need to offset in this case
    } else {
        return "";
    }

    const char *end = strstr(start, ".pdf");
    if (!end) {
        return "";
    }
    end += 4;  //include ".pdf"

    int urlLength = end - start;
    std::string fileLinkUrl(start, urlLength);
    char *unescape = Utils::unescape(fileLinkUrl.c_str());
    fileLinkUrl = unescape ? unescape : "";
    free(unescape);
    return fileLinkUrl;
}

void WebViewScreen::Launch(const char* url, bool enableDeeplink, bool enableMenu, ActionAndData *action_data) {
    if (!ConnectivityManager::Singleton().IsConnected()) { //Stop launching of webview if netconnection is off
        notification_status_message_post(i18n_get_text("IDS_GEN_NETWORK_ERROR"));
        return;
    }

    std::string deepLinkedUrl;
    std::string formattedUrl;
    bool isDeepLinkUrl = check_url_for_deep_link(url);
    std::string linkUrl = get_contained_link(url, isDeepLinkUrl);
    bool isUrlContainsLink = !linkUrl.empty();

    if (url) {
        if (!isUrlContainsLink) {
            //
            // We must not search for full protocol like: 'https://', because of the last part
            // ( '://' ) could be ecaped by special characters. It is quite enough to search only
            // for first part ( 'http' ).
            if (strstr( url, "http") != url) {
                formattedUrl += TRANSFER_PROTOCOL_DEFAULT;
            }
            formattedUrl += url;
        } else {
            formattedUrl = linkUrl;
        }
        const char* locale = NULL;
        if (I18N_ERROR_NONE == i18n_ulocale_get_default(&locale)) {
            if (std::string::npos != formattedUrl.find(REQ_QUERY_STRING_DLIM)) {
                formattedUrl.append(REQ_MORE_PARAM_DLIM);
            } else {
                formattedUrl.append(REQ_QUERY_STRING_DLIM);
            }
            formattedUrl.append(REQ_LOCALE);
            formattedUrl.append(locale);
        }

        const char *unescapedUrl = formattedUrl.c_str();
        if (unescapedUrl) {
            if (check_url_for_group_file(unescapedUrl)) {
                download_document_file(unescapedUrl);
                return;
            } else if (Utils::HasEnding(unescapedUrl, ".pdf")) {
                download_document_file(unescapedUrl);
                return;
            } else if (strstr(unescapedUrl, "&url=http") && strstr(unescapedUrl, ".pdf&")) {
                std::string fileLinkUrl = get_contained_link_to_file(unescapedUrl);
                if (!fileLinkUrl.empty()) {
                    download_document_file(fileLinkUrl.c_str());
                    return;
                }
            }
        }

        //To address the issues raising related to web view screens, As a quick fix, overriding the 'enableDeeplink' argument of this function
        //In SignUp process, we do not have any session secret / access token. So In these cases related to session secret/ access token not available, disable the DeepLinking.
        //Otherwise Enable DeepLinking by default.
        enableDeeplink = !Config::GetInstance().GetSessionSecret().empty();
        if (enableDeeplink && isDeepLinkUrl && !isUrlContainsLink) {
            const char* encodedUrl = encode_url(formattedUrl.c_str());
            if (encodedUrl) {
                std::stringstream timeInSecs;
                timeInSecs << time(NULL);

                std::string rawSignature = REQ_APP_ID;
                rawSignature.append(Config::GetApplicationId());
                rawSignature.append(REQ_SESSION_KEY).append(Config::GetInstance().GetSessionKey());
                rawSignature.append(REQ_TIME).append(timeInSecs.str());
                rawSignature.append(REQ_USER_ID).append(Config::GetInstance().GetUserId());
                rawSignature.append(REQ_URL).append(formattedUrl.c_str());
                rawSignature.append(Config::GetInstance().GetSessionSecret());

                char *md5Signature = Utils::str2md5(rawSignature.c_str(), rawSignature.length());
                if (md5Signature) {
                    deepLinkedUrl = REQ_AUTH_URL;
                    deepLinkedUrl.append(REQ_APP_ID).append(Config::GetApplicationId());
                    deepLinkedUrl.append(REQ_MORE_PARAM_DLIM).append(REQ_SESSION_KEY).append(Config::GetInstance().GetSessionKey());
                    deepLinkedUrl.append(REQ_MORE_PARAM_DLIM).append(REQ_TIME).append(timeInSecs.str());
                    deepLinkedUrl.append(REQ_MORE_PARAM_DLIM).append(REQ_USER_ID).append(Config::GetInstance().GetUserId());
                    deepLinkedUrl.append(REQ_MORE_PARAM_DLIM).append(REQ_URL).append(encodedUrl);
                    deepLinkedUrl.append(REQ_MORE_PARAM_DLIM).append(REQ_SIGNATURE).append(md5Signature);

                    /*
                     * For now, disabling the below functionality to work Deeplink URL redirections properly
                     * and will be enabled when everything works fine with this functionality.
                     *
                     * deepLinkedUrl.append(REQ_MORE_PARAM_DLIM);
                     * deepLinkedUrl.append(REQ_ENC);
                     * deepLinkedUrl.append(Config::GetInstance().GetAccessToken());
                     */

                    free(md5Signature);
                }

                free((void*)encodedUrl);
            }
        }
    }

    WebViewScreen *webViewScr = new WebViewScreen(url,
                                                  ((enableDeeplink && isDeepLinkUrl && !isUrlContainsLink) ? deepLinkedUrl.c_str() : formattedUrl.c_str()),
                                                  enableMenu, action_data);
    Application::GetInstance()->AddScreen(webViewScr);
}

const char* WebViewScreen::encode_url(const char* rawUrl) {
    const char*encodedUrl = NULL;
    if (rawUrl) {
        CURL *curlHandle = curl_easy_init();
        if (curlHandle) {
            char *escapedUrl = curl_easy_escape(curlHandle, rawUrl, strlen(rawUrl));
            if (escapedUrl) {
                encodedUrl = strdup(escapedUrl);
                curl_free(escapedUrl);
            }
            curl_easy_cleanup(curlHandle);
        }
    }
    return encodedUrl;
}

void WebViewScreen::Update(AppEventId eventId, void * data)
{
    switch (eventId) {
    case ePAUSE_EVENT:
        ewk_view_suspend(mWebView);
        break;
    case eRESUME_EVENT:
        ewk_view_resume(mWebView);
        break;
    default:
        break;
    }
}
