/*
 *  SignUpNoConnectScreen.cpp
 *
 *  Created on: 29th June 2015
 *      Author: Manjunath Raja
 */

#include "Common.h"
#include "CommonScreen.h"
#include "ConnectivityManager.h"
#include "Config.h"
#include "dlog.h"
#include "SignUpNoConnectScreen.h"
#include "SignupRequest.h"


SignUpNoConnectScreen::SignUpNoConnectScreen(ScreenBase *parentScreen):ScreenBase(parentScreen)  {
    CreateView();
}

SignUpNoConnectScreen::~SignUpNoConnectScreen() {
}

void SignUpNoConnectScreen::LoadEdjLayout() {
    mLayout = elm_layout_add(getParentMainLayout());
    elm_layout_file_set(mLayout, Application::mEdjPath, "signup_noconnect_screen");
    evas_object_size_hint_weight_set(mLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_show(mLayout);
}

void SignUpNoConnectScreen::CreateView() {
    LoadEdjLayout();

    elm_object_translatable_part_text_set(mLayout, "HeaderText", "IDS_CREATING_USER");
    elm_object_translatable_part_text_set(mLayout, "sup_noconnect_heading", "IDS_SUP_IDS_SUP_NOCONNECT_HEADING");
    elm_object_translatable_part_text_set(mLayout, "sup_noconnect_info", "IDS_SUP_IDS_SUP_NOCONNECT_INFO");

    elm_object_translatable_part_text_set(mLayout, "sup_noconnect_nxt_btn_txt", "IDS_TRYAGAIN");
    elm_object_signal_callback_add(mLayout, "got.a.sup.ncnxt.btn.click", "sup_noconnect_nxt_btn*",
            (Edje_Signal_Cb)TryAgainBtnCb, this);
    elm_object_signal_callback_add(mLayout, "HeaderBack", "HeaderBack",
            (Edje_Signal_Cb)HeaderBackCb, this);
}

void SignUpNoConnectScreen::TryAgainBtnCb(void *data, Evas_Object *obj, void *EventInfo) {
    SignUpNoConnectScreen *Self = static_cast<SignUpNoConnectScreen *>(data);
    if (ConnectivityManager::Singleton().IsConnected()) {
        Self->Pop();
        CommonScreen::StartSigningUp();
    }
}

void SignUpNoConnectScreen::HeaderBackCb(void* Data, Evas_Object* Obj, void* EventInfo) {
    SignUpNoConnectScreen* Me = static_cast<SignUpNoConnectScreen*>(Data);
    Me->Pop();
}
