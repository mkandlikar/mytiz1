#include <Elementary.h>

#include "Common.h"
#include "ErrorHandling.h"
#include "FbResponseImageDetails.h"
#include "HomeScreen.h"
#include "ImagesCarouselScreen.h"
#include "ImagePostScreen.h"
#include "Log.h"
#include "LikeOperation.h"
#include "OperationManager.h"
#include "Photo.h"
#include "PhotoPresenter.h"
#include "Popup.h"
#include "PostPresenter.h"
#include "WidgetFactory.h"

static const char *LogTag = "ImagePostScreen";

/*
 * Class ImagePostScreen
 */
ImagePostScreen::ImagePostScreen(ActionAndData *parentActionAndData, int currentImage) : ScreenBase(NULL),
        mVScroller(NULL),
        mBox(NULL),
        mCarouselScreen(NULL),
        mCurrentImage(currentImage),
        mImagePostAction(NULL),
        mImagePostActionAndData(NULL),
        mImagePhotoActionAndDataList(NULL),
        mImageWidgets(NULL),
        mRequests(NULL),
        mImagePhotoAction(NULL),
        mParentActionAndData(parentActionAndData),
        mTimer(NULL) {

    mScreenId = ScreenBase::SID_IMAGE_POST;
    mParent = ( parentActionAndData ? parentActionAndData->mAction->getScreen() : NULL );
    EnableUpdateParentCache( false );
    if (parentActionAndData && parentActionAndData->mData) {
        Post * post = static_cast<Post *>(parentActionAndData->mData);
        Eina_List *l;
        void *list_data;
        if (post->GetPhotoList() && eina_list_count(post->GetPhotoList()) > 1) {
            // Allocate memory for action and data for post
            mImagePostAction = new ImagePostAction(this);

            mImagePostActionAndData = new ActionAndData(post, mImagePostAction);

            mImagePhotoAction = new ActionBase(this);
            EINA_LIST_FOREACH(post->GetPhotoList(), l, list_data) {
                Photo * photo = static_cast<Photo*>(list_data);
                Log::info_tag(LogTag, "PHOTO:%s, %dX%d", photo->GetFilePath(), photo->GetWidth(), photo->GetHeight());
                mImagePhotoActionAndDataList = eina_list_append(mImagePhotoActionAndDataList, new ActionAndData(photo, mImagePhotoAction));
            }
        }
        Eina_List *imageIds = NULL;
        EINA_LIST_FOREACH(mImagePhotoActionAndDataList, l, list_data) {
            Photo *photo = static_cast<Photo *>(static_cast<ActionAndData *>(list_data)->mData);
            if (photo && photo->GetId()) {
                imageIds = eina_list_append(imageIds, photo->GetId());
            }
        }
        if (imageIds) {
            mRequests = FacebookSession::GetInstance()->GetImageDetails(imageIds, ImageDetailsReceived, this);
            eina_list_free(imageIds);
        }
        CreatePostScreenLayout();
        AppEvents::Get().Subscribe(eLIKE_COMMENT_EVENT, this);
        AppEvents::Get().Subscribe(ePOST_RELOAD_EVENT, this);
    }
}

bool ImagePostScreen::HandleBackButton() {
        return PopupMenu::Close();
}

void ImagePostScreen::CreatePostScreenLayout() {
    Log::info_tag(LogTag, "CreatePostScreenLayout()");

    mLayout = elm_layout_add(getParentMainLayout());
    elm_layout_file_set(mLayout, Application::mEdjPath, "image_post_screen");

    mVScroller = elm_scroller_add(mLayout);
    evas_object_size_hint_weight_set(mVScroller, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mVScroller, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_scroller_policy_set(mVScroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_OFF);
    elm_scroller_single_direction_set(mVScroller, ELM_SCROLLER_SINGLE_DIRECTION_HARD);
    elm_scroller_propagate_events_set(mVScroller, EINA_FALSE);
    elm_object_scroll_lock_x_set(mVScroller, EINA_TRUE);
    elm_object_part_content_set(mLayout, "image_post_screen_swallow", mVScroller);

    mBox = elm_box_add(mVScroller);
    elm_box_horizontal_set(mBox, EINA_FALSE);
    evas_object_size_hint_weight_set(mBox, EVAS_HINT_EXPAND, 0.0);
    evas_object_size_hint_align_set(mBox, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_object_content_set(mVScroller, mBox);
    evas_object_show(mBox);

    CreatePostWidget(mBox);

    int order = 0;
    Eina_List *l;
    void *list_data;
    EINA_LIST_FOREACH(mImagePhotoActionAndDataList, l, list_data) {
        ImageWidget *imageWidget = new ImageWidget(this, static_cast<ActionAndData *>(list_data), order);
        mImageWidgets = eina_list_append(mImageWidgets, imageWidget);
        elm_box_pack_end(mBox, imageWidget->GetLayout());
        ++order;
    }

    elm_scroller_movement_block_set(mVScroller, ELM_SCROLLER_MOVEMENT_NO_BLOCK );
    elm_box_recalculate(mBox);
    ShowWidget(mCurrentImage);
    mTimer = ecore_timer_add( 0.1, timer_cb, this );
}

Eina_Bool ImagePostScreen::timer_cb( void *data) {
    ImagePostScreen *screen = static_cast<ImagePostScreen *>(data);
    if(screen) {
        screen->ShowWidget(screen->mCurrentImage);
        screen->mTimer = NULL;
    }
    return ECORE_CALLBACK_CANCEL;
}

void ImagePostScreen::CreatePostWidget(Evas_Object * parent)
{
    assert(mImagePostActionAndData);
    Post * post = dynamic_cast<Post *>(mImagePostActionAndData->mData);
    assert(post);

    Evas_Object * wrapper = NULL;
    Evas_Object * postWidget = WidgetFactory::CreatePostItemWrapper(parent, &wrapper);

    WidgetFactory::PostHeaderCustomization(postWidget, mImagePostActionAndData, false);

    if (post->GetMessage()) {
        WidgetFactory::CreatePostFullText(postWidget, mImagePostActionAndData);
    }

    Evas_Object* separator = WidgetFactory::CreateLayoutByGroup(postWidget, "image_post_screen_post_widget_separator");
    elm_box_pack_end(postWidget, separator);

    mImagePostActionAndData->mLikesCommentsWidget = WidgetFactory::CreateLayoutByGroup(postWidget, "image_post_screen_footer_buttons");
    elm_box_pack_end(postWidget, mImagePostActionAndData->mLikesCommentsWidget);


    elm_object_signal_callback_add(mImagePostActionAndData->mLikesCommentsWidget, "clicked", "like_button",
            ActionBase::on_like_btn_clicked_cb, mImagePostActionAndData);

    elm_object_signal_callback_add(mImagePostActionAndData->mLikesCommentsWidget, "clicked", "comment_button",
            ActionBase::on_comment_btn_clicked_cb, mImagePostActionAndData);

    // Create share button
    Evas_Object* shareButton = WidgetFactory::CreateLayoutByGroup(mImagePostActionAndData->mLikesCommentsWidget,
            "image_post_screen_share_button");
    elm_object_part_content_set(mImagePostActionAndData->mLikesCommentsWidget, "share_or_tag.swallow", shareButton);
    if (post->GetCanShare()) {
        elm_object_signal_callback_add(shareButton, "clicked", "share_button", WidgetFactory::on_share_btn_clicked_cb,
                mImagePostActionAndData);
        elm_object_signal_emit(shareButton, Application::IsRTLLanguage() ? "share.enabled.rtl" : "share.enabled.ltr", "");
    } else {
        elm_object_signal_emit(shareButton, "share.disabled", "");
    }

    RefreshPostWidget();
    elm_box_pack_end(parent, wrapper);
    evas_object_show(wrapper);
}

void ImagePostScreen::RefreshPostWidget() {
    assert(mImagePostActionAndData);
    Post* post = dynamic_cast<Post*>(mImagePostActionAndData->mData);
    assert(post);

    PostPresenter presenter(*post);
    HRSTC_TRNSLTD_PART_TEXT_SET(mImagePostActionAndData->mLikesCommentsWidget, "likes_and_comments_text", presenter.GetImageLikesAndComments().c_str());

    elm_object_signal_callback_del(mImagePostActionAndData->mLikesCommentsWidget, "mouse,clicked,*", "likes_and_comments_text",
            ActionBase::on_comment_btn_clicked_cb);
    elm_object_signal_callback_del(mImagePostActionAndData->mLikesCommentsWidget, "mouse,clicked,*", "likes_and_comments_text",
            ActionBase::on_likes_count_clicked_cb);
    if (post->GetCommentsCount()) {
        elm_object_signal_callback_add(mImagePostActionAndData->mLikesCommentsWidget, "mouse,clicked,*", "likes_and_comments_text",
                ActionBase::on_comment_btn_clicked_cb, mImagePostActionAndData);
    }
    else {
        elm_object_signal_callback_add(mImagePostActionAndData->mLikesCommentsWidget, "mouse,clicked,*", "likes_and_comments_text",
                ActionBase::on_likes_count_clicked_cb, mImagePostActionAndData);
    }

    if (post->GetCanLike()) {
        elm_object_signal_emit(mImagePostActionAndData->mLikesCommentsWidget, "like.enabled", "");
        if (post->GetHasLiked()) {
            elm_object_signal_emit(mImagePostActionAndData->mLikesCommentsWidget, "like.liked", "");
        }
        else {
            elm_object_signal_emit(mImagePostActionAndData->mLikesCommentsWidget, "like.normal", "");
        }
    }
    else {
        elm_object_signal_emit(mImagePostActionAndData->mLikesCommentsWidget, "like.disabled", "");
    }

    if (post->GetCanComment()) {
        elm_object_signal_emit(mImagePostActionAndData->mLikesCommentsWidget, Application::IsRTLLanguage() ? "comment.enabled.rtl" : "comment.enabled.ltr", "");
    }
    else {
        elm_object_signal_emit(mImagePostActionAndData->mLikesCommentsWidget, "comment.disabled", "");
    }
}

void ImagePostScreen::Push() {
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(elm_naviframe_top_item_get(getNf()), EINA_FALSE, EINA_FALSE);
}

void ImagePostScreen::ShowCarousel(int imageNumber) {
    Log::info_tag(LogTag, "SwitchToCarousel()");

    mCurrentImage = imageNumber;
    Evas_Coord w, h;
    evas_object_geometry_get(mVScroller, NULL, NULL, &w, &h);
    Log::info_tag(LogTag, "SwitchToCarousel() %dx%d", w, h);

    mCarouselScreen = new ImagesCarouselScreen(mImagePhotoActionAndDataList, mCurrentImage,
            ImagesCarouselScreen::eACTION_AND_DATA_MODE, this);
    Application::GetInstance()->AddScreen(mCarouselScreen);
    // From this moment ImagePostScreen is not responsible for ImagesCarouselScreen destruction
    mCarouselScreen = nullptr;
}

ImagePostScreen::~ImagePostScreen() {
    Log::info_tag(LogTag, "~ImagePostScreen()");
    if(mTimer) {
        ecore_timer_del(mTimer);
    }
    Eina_List *l;
    void *list_data;
    if (mRequests) {
            EINA_LIST_FOREACH(mRequests, l, list_data) {
                GraphRequest *request = (GraphRequest*) list_data;
                FacebookSession::GetInstance()->ReleaseGraphRequest(request);
            }
            eina_list_free(mRequests);
        }
    if (mImagePhotoActionAndDataList) {
            EINA_LIST_FOREACH(mImagePhotoActionAndDataList, l, list_data) {
                delete static_cast<ActionAndData *>(list_data);
            }
        eina_list_free(mImagePhotoActionAndDataList);
    }
    delete mImagePhotoAction;
    if (mImageWidgets) {
        EINA_LIST_FOREACH(mImageWidgets, l, list_data) {
            ImageWidget *imageWidget = (ImageWidget *) list_data;
            delete imageWidget;
        }
        eina_list_free(mImageWidgets);
    }

    delete mImagePostActionAndData;
    delete mImagePostAction;
    EnableUpdateParentCache( true );
}

void ImagePostScreen::CalculateWidgetGeometry(int index, int *x, int *y, int *w, int *h) {
    Evas_Coord wh;
    *x = *y = *w = *h = 0;

    evas_object_geometry_get(mVScroller, x, NULL, NULL, NULL);

    int ch = 0;
    Eina_List *children = elm_box_children_get(mBox);
    if(children) {
        Evas_Object* topWidget = static_cast<Evas_Object*>(eina_list_data_get(children));
        if(topWidget) {
            evas_object_geometry_get(topWidget, NULL, NULL, NULL, &ch);
        }
    }
    *y = ch;
    Log::info_tag(LogTag, "***HEADER:%d", ch);

    int i = 0;
    Eina_List *l;
    void *list_data;
    EINA_LIST_FOREACH(mImageWidgets, l, list_data) {
        ImageWidget *widget = static_cast<ImageWidget *> (list_data);
        wh = widget->GetWidgetHeight();
        Log::info_tag(LogTag, "***WIDGET (%d):%d", i, wh);
        if (i < index) {
            *y += wh;
        } else {
            break;
        }
        i++;
    }
    *w = R->SCREEN_SIZE_WIDTH;
    *h = wh;
    Log::info_tag(LogTag, "CalculateWidgetGeometry(%d): x:%d, y:%d, w:%d, h:%d", index, *x, *y, *w, *h);
}

void ImagePostScreen::ShowWidget(int index) {
    Evas_Coord x, y, w, h, ww, wh;
    evas_object_geometry_get(mVScroller, NULL, NULL, &w, &h);  //AAA shall not work if the scroller isn't shown yet
    if (w <= 0 || h <= 0) {
        w = R->SCREEN_SIZE_WIDTH;
        h = R->SCREEN_SIZE_HEIGHT - R->STATUS_BAR_SIZE_H;
    }

    CalculateWidgetGeometry(index, &x, &y, &ww, &wh);

    if (wh < h) {
        y -= (h - wh)/2;
    }
    Log::info_tag(LogTag, "ShowWidget() : x%d, y%d, w%d, h%d", x, y, w, h);
    elm_scroller_region_show(mVScroller, x, y, w, h);
}

ImagePostScreen::ImageWidget *ImagePostScreen::GetImageWidgetByPhoto(Photo *photo) {
    ImageWidget *ret = NULL;
    Eina_List *l;
    void *list_data;
    EINA_LIST_FOREACH(mImageWidgets, l, list_data) {
        ImageWidget *imageWidget = static_cast<ImageWidget *>(list_data);
        if (static_cast<Photo *>(imageWidget->mActionAndData->mData) == photo) {
            ret = imageWidget;
            break;
        }
    }
    return ret;
}

void ImagePostScreen::ImageDetailsReceived(void *data, char* str, int code) {
    ImagePostScreen *me = static_cast<ImagePostScreen *>(data);
    if (code == CURLE_OK) {
        Log::info_tag(LogTag, "ImageDetailsReceived(), data: %s", str);
        FbResponseImageDetails *imageDetails = FbResponseImageDetails::createFromJson(str);
        if (!imageDetails || !imageDetails->mPhoto) {
            Log::error_tag(LogTag, "Error parsing http response");
        } else if (imageDetails->mError != NULL && imageDetails->mError->mMessage) {
            Log::error_tag(LogTag, "Error getting image details, error = %s", imageDetails->mError->mMessage);
        } else {
            Photo* newPhoto = imageDetails->mPhoto;
            int likesCount = newPhoto->GetLikesCount();
            Log::info_tag(LogTag, "results: id:%s, likesCount:%d, commentsCount:%d, name:%s", newPhoto->GetId(), likesCount, newPhoto->GetCommentsCount(), newPhoto->GetName());
            Eina_List *l;
            void *list_data;
            EINA_LIST_FOREACH(me->mImagePhotoActionAndDataList, l, list_data) {
                Photo *photo = static_cast<Photo *>(static_cast<ActionAndData *>(list_data)->mData);
                if (photo && photo->GetHash() == newPhoto->GetHash() && !strcmp(photo->GetId(), newPhoto->GetId())) {
                    photo->UpdatePhotoDetails(newPhoto); // Update photo data from JSON data
                    delete imageDetails;
                        ImageWidget *imageWidget = me->GetImageWidgetByPhoto(photo);
                        if (imageWidget) {
                            imageWidget->RefreshImageWidget(static_cast<ActionAndData *>(list_data));
                        }
                    break;
                }
            }
//AAA cause wrong visual effects           me->ShowWidget(me->mCurrentImage);
        }
    } else {
        Log::debug_tag(LogTag, "Error sending http request");
    }

    if (str != NULL) {
        //Attention!! client should take care to free respond buffer
        free(str);
    }
}

void ImagePostScreen::Update(AppEventId eventId, void * data)
{
    switch (eventId) {
    case ePOST_RELOAD_EVENT:
        if (data) {
            Log::info("ImagePostScreen::Update->ePOST_RELOAD_EVENT");
            Post *post = static_cast<Post *>(data);
            if(ActionAndData::IsAlive(mImagePostActionAndData)) {
                assert(mImagePostActionAndData->mData);
                if (*(mImagePostActionAndData->mData) == *post) {
                    mImagePostActionAndData->ReSetData(post);
                    RefreshPostWidget();
                }
            }
        }
        break;
    case eLIKE_COMMENT_EVENT:
        if (data) {
            Log::info("ImagePostScreen::Update->eLIKE_COMMENT_EVENT");
            if(ActionAndData::IsAlive(mImagePostActionAndData)) {
                assert(mImagePostActionAndData->mData);
                if(!strcmp(static_cast<char *>(data), mImagePostActionAndData->mData->GetId())){
                    RefreshPostWidget();
                }
            }
        }
        break;
    default:
        break;
    }
}

/*
 * Class ImagePostScreen::ImageWidget
 */

ImagePostScreen::ImageWidget::ImageWidget(ImagePostScreen *screen, ActionAndData *actionAndData, int order) :
        mOrder(order),
        mActionAndData(actionAndData),
        mScreen(screen)
{
    Photo *photo = static_cast<Photo *>(mActionAndData->mData);

    mLayout = elm_layout_add(screen->mBox);
    elm_layout_file_set(mLayout, Application::mEdjPath, "image_post_screen_image_widget");

    Evas_Object* img = elm_image_add(mLayout);
    elm_image_file_set(img, photo->GetFilePath(), NULL);
    evas_object_show(img);

    elm_object_part_content_set(mLayout, "swallow.image", img);

    int w, h;
    elm_image_object_size_get(img, &w, &h);
    double aspect = (double)h / w;
    w = R->SCREEN_SIZE_WIDTH;
    mImageHeight = aspect * w;
    Log::info_tag(LogTag, "ImageWidget(): image size: %dx%d aspect:%f", w, mImageHeight, aspect);

    Evas_Object * o = edje_object_part_swallow_get(elm_layout_edje_get(mLayout), "swallow.image");
    edje_extern_object_max_size_set(o, w, mImageHeight);

    elm_object_signal_callback_add(mLayout, "mouse,clicked,*", "swallow.image", on_image_widget_clicked_cb, this);
    evas_object_show(mLayout);

    // Create footer buttons - Like and Comment
    mActionAndData->mLikesCommentsWidget = WidgetFactory::CreateLayoutByGroup(mLayout, "image_post_screen_footer_buttons");
    elm_object_part_content_set(mLayout, "swallow.footer", mActionAndData->mLikesCommentsWidget);

    elm_object_signal_callback_add(mActionAndData->mLikesCommentsWidget, "clicked", "like_button",
            ActionBase::on_like_btn_clicked_cb, mActionAndData);

    elm_object_signal_callback_add(mActionAndData->mLikesCommentsWidget, "clicked", "comment_button",
            ActionBase::on_comment_btn_clicked_cb, mActionAndData);

    RefreshImageWidget(mActionAndData);

    AppEvents::Get().Subscribe(eLIKE_COMMENT_EVENT, this);
    AppEvents::Get().Subscribe(ePHOTO_COMMENTS_LIST_WAS_MODIFIED, this);
    AppEvents::Get().Subscribe(ePHOTO_CAPTION_WAS_CHANGED, this);
    AppEvents::Get().Subscribe(eLANGUAGE_CHANGED, this);
}

ImagePostScreen::ImageWidget::~ImageWidget() {
}

int ImagePostScreen::ImageWidget::GetWidgetHeight() {
    int height = 0;

    int descriptionHeight;
    int footerHeight;
    edje_object_part_geometry_get(elm_layout_edje_get(mLayout), "text.description", NULL, NULL, NULL, &descriptionHeight);
    edje_object_part_geometry_get(elm_layout_edje_get(mLayout), "swallow.footer", NULL, NULL, NULL, &footerHeight);

    height = mImageHeight + footerHeight + (R->IMAGE_POST_SCREEN_IMAGE_OFFSET_HEIGHT) * 2;
    if (static_cast<Photo *>(mActionAndData->mData)->GetName()) {
        height += descriptionHeight;
    }
    return height;
}

void ImagePostScreen::ImageWidget::RefreshPhotoText(ActionAndData * photoActionAndData) {
    Photo *photo = static_cast<Photo *>(photoActionAndData->mData);

    if (photo->GetName() && (*photo->GetName() != '\0')) {
        Evas_Object *photo_text = elm_label_add(mLayout);
        elm_object_part_content_set(mLayout, "text.description", photo_text);
        elm_label_wrap_width_set(photo_text, R->NF_POST_MSG_WRAP_WIDTH);
        elm_label_line_wrap_set(photo_text, ELM_WRAP_MIXED);

        PhotoPresenter presenter(*photo);
        char* text = WidgetFactory::WrapByFormat2(SANS_REGULAR_000_FORMAT, R->NF_POST_MSG_TEXT_SIZE, presenter.GetName(PresenterHelpers::eFeed).c_str());
        if (text) {
            elm_object_text_set(photo_text, text);
            Log::info_tag(LogTag, "ImageWidget::RefreshPhotoText: %s",text);
            delete[] text;
        }

        elm_object_signal_emit(mLayout, "show_widget_description", "");
        evas_object_size_hint_min_set(mLayout, R->SCREEN_SIZE_WIDTH, GetWidgetHeight());
        evas_object_smart_callback_add(photo_text, "anchor,clicked", ActionBase::on_anchor_clicked_cb, photoActionAndData);
    } else {
        elm_object_part_content_set(mLayout, "text.description", nullptr);
        elm_object_signal_emit(mLayout, "hide_widget_description", "");
        evas_object_size_hint_min_set(mLayout, R->SCREEN_SIZE_WIDTH, GetWidgetHeight());
    }
}
void ImagePostScreen::ImageWidget::RefreshImageWidget(ActionAndData * photoActionAndData) {
    Photo *photo = static_cast<Photo *>(photoActionAndData->mData);

    RefreshPhotoText(photoActionAndData);

    PhotoPresenter presenter(*photo);
    HRSTC_TRNSLTD_PART_TEXT_SET(photoActionAndData->mLikesCommentsWidget, "likes_and_comments_text", presenter.GetLikesAndCommentsCount().c_str());

    if (photo->GetLikesCount() > 0 || photo->GetCommentsCount() > 0)
    {
        elm_object_signal_callback_add(photoActionAndData->mLikesCommentsWidget, "mouse,clicked,*", "likes_and_comments_text",
                ActionBase::on_comment_btn_clicked_cb, photoActionAndData);
    }

    if (photo->GetCanLike()) {
        elm_object_signal_emit(photoActionAndData->mLikesCommentsWidget, "like.enabled", "");
        if (photo->GetHasLiked()) {
            elm_object_signal_emit(photoActionAndData->mLikesCommentsWidget, "like.liked", "");
        }
        else {
            elm_object_signal_emit(photoActionAndData->mLikesCommentsWidget, "like.normal", "");
        }
    }
    else {
        elm_object_signal_emit(photoActionAndData->mLikesCommentsWidget, "like.disabled", "");
    }

    if (photo->GetCanComment()) {
        elm_object_signal_emit(photoActionAndData->mLikesCommentsWidget, Application::IsRTLLanguage() ? "comment.enabled.rtl" : "comment.enabled.ltr", "");
    }
    else {
        elm_object_signal_emit(photoActionAndData->mLikesCommentsWidget, "comment.disabled", "");
    }
}

void ImagePostScreen::ImageWidget::on_image_widget_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source) {
    ImageWidget *me = static_cast<ImageWidget *>(data);
    me->mScreen->ShowCarousel(me->mOrder);
}

void ImagePostScreen::ImageWidget::Update(AppEventId eventId, void * data)
{
    switch (eventId) {
    case ePHOTO_CAPTION_WAS_CHANGED:
    case eLIKE_COMMENT_EVENT:
        if (data) {
            if (mActionAndData) {
                assert(mActionAndData->mData);
                if(!strcmp(static_cast<char *>(data), mActionAndData->mData->GetId())) {
                    Log::info("ImagePostScreen::ImageWidget::Update->eLIKE_COMMENT_EVENT Id=%s",data);
                    RefreshImageWidget(mActionAndData);
                }
            }
        }
        break;
    case ePHOTO_COMMENTS_LIST_WAS_MODIFIED:
        if (data) {
            Log::info("ImagePostScreen::ImageWidget::Update->ePHOTO_COMMENTS_LIST_WAS_MODIFIED");
            if (mActionAndData) {
                Photo *photo = dynamic_cast<Photo*>(mActionAndData->mData);
                const char * imageId = static_cast<char*>(data);

                if(photo && !strcmp(photo->GetId(),imageId)) {
                    Log::info("ImagePostScreen::ImageWidget::Update->Reload imageData for Id = %s", photo->GetId());
                    Eina_List *imageIds = eina_list_append(nullptr, imageId);
                    mScreen->mRequests = FacebookSession::GetInstance()->GetImageDetails(imageIds, ImageDetailsReceived, mScreen);
                    imageIds = eina_list_free(imageIds);
                }
            }
        }
        break;
    case eLANGUAGE_CHANGED:
        edje_extern_object_max_size_set(edje_object_part_swallow_get(elm_layout_edje_get(mLayout), "swallow.image"),
               R->SCREEN_SIZE_WIDTH - (R->IMAGE_POST_SCREEN_IMAGE_OFFSET_HEIGHT) * 2, mImageHeight);
        break;
    default:
        break;
    }
}

void ImagePostScreen::EnableUpdateParentCache( bool isEnable )
{
    switch (mParent->GetScreenId()) {
    case ScreenBase::SID_HOME_SCREEN: {
        HomeScreen *screen = dynamic_cast<HomeScreen*>(mParent);
        if (screen) {
            screen->SetUpdateCacheEnable(isEnable);
            if (isEnable) {
                screen->UpdateCachedData();
                screen->RequestData();
            }
        }
        break;
    }
    default:
        break;
    }
}
