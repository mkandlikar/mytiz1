#include "CSmartPtr.h"
#include "Log.h"
#include "OperationManager.h"
#include "Photo.h"
#include "PhotoGalleryUploadPhotoStrategy.h"
#include "UploadPhotoOperation.h"
#include "WidgetFactory.h"

#include <cassert>

static const char* LogTag = "PhotoGalleryUploadPhotoStrategy";


/*
 * Class PhotoGalleryUploadPhotoStrategy
 */
PhotoGalleryUploadPhotoStrategy::PhotoGalleryUploadPhotoStrategy(UploadPhotoType uploadType) :
        mUploadType(uploadType)
{
    Log::info_tag(LogTag, "PhotoGalleryUploadPhotoStrategy: create Photo Gallery screen." );
}

PhotoGalleryUploadPhotoStrategy::~PhotoGalleryUploadPhotoStrategy() {
    Log::info_tag(LogTag, "PhotoGalleryUploadPhotoStrategy: destroy PhotoGalleryUploadPhotoStrategy screen." );
}

void PhotoGalleryUploadPhotoStrategy::PhotoSelected(Photo* photo) {
    Log::debug_tag(LogTag, "PhotoGalleryUploadPhotoStrategy::PhotoSelected() and Opened From User Profile Screen");

    ImageFile *imageToUpload = photo->GetOptimumResolutionPicture(UIRes::GetInstance()->SCREEN_SIZE_WIDTH, UIRes::GetInstance()->SCREEN_SIZE_HEIGHT);
    if (imageToUpload) {
        OperationManager::GetInstance()->AddOperation(MakeSptr<UploadPhotoOperation>(mUploadType == UPLOAD_COVER ? UploadPhotoOperation::ECoverPhoto : UploadPhotoOperation::EUserPhoto, *imageToUpload));
    }
}
