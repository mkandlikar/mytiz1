/*
 *  SignUpGenderScreen.cpp
 *
 *  Created on: 10th June 2015
 *      Author: Manjunath Raja
 */

#include "Common.h"
#include "CommonScreen.h"
#include "Config.h"
#include "dlog.h"
#include "SignUpGenderScreen.h"
#include "SignupRequest.h"
#include "SignUpPswdScreen.h"

SignUpGenderScreen::SignUpGenderScreen(ScreenBase *parentScreen):ScreenBase(parentScreen)  {
    CreateView();
}

SignUpGenderScreen::~SignUpGenderScreen() {
}

void SignUpGenderScreen::LoadEdjLayout() {
    mLayout = elm_layout_add(getParentMainLayout());
    elm_layout_file_set(mLayout, Application::mEdjPath, "signup_gender_screen");
    evas_object_size_hint_weight_set(mLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_show(mLayout);
}

void SignUpGenderScreen::CreateView() {
    LoadEdjLayout();

    elm_object_translatable_part_text_set(mLayout, "HeaderText", "IDS_ABOUT_YOU");
    elm_object_translatable_part_text_set(mLayout, "sup_gender_heading", "IDS_SUP_GENDER_HEADING");
    elm_object_translatable_part_text_set(mLayout, "sup_gender_info", "IDS_SUP_GENDER_INFO");
    elm_object_translatable_part_text_set(mLayout, "sup_gender_female_text", "IDS_SUP_GENDER_FEMALE");
    elm_object_translatable_part_text_set(mLayout, "sup_gender_male_text", "IDS_SUP_GENDER_MALE");

    Evas_Object *MaleRadioBtn = elm_radio_add(mLayout);
    elm_object_part_content_set(mLayout, "sup_gender_male_radio", MaleRadioBtn);
    elm_radio_state_value_set(MaleRadioBtn, EGenderMale);
    evas_object_smart_callback_add(MaleRadioBtn, "changed", GenderChangeCb, NULL);
    SignupRequest::Singleton().SetData(SignupRequest::ISGENDERFEMALE, "false");

    Evas_Object *FemaleRadioBtn = elm_radio_add(mLayout);
    elm_object_part_content_set(mLayout, "sup_gender_female_radio", FemaleRadioBtn);
    elm_radio_group_add(FemaleRadioBtn, MaleRadioBtn);
    elm_radio_state_value_set(FemaleRadioBtn, EGenderFemale);
    evas_object_smart_callback_add(FemaleRadioBtn, "changed", GenderChangeCb, NULL);

    elm_object_translatable_part_text_set(mLayout, "sup_gender_nxt_btn_txt", "IDS_SUP_WC_NEXT");
    elm_object_signal_callback_add(mLayout, "got.a.sup.gnxt.btn.click", "sup_gender_nxt_btn*",
            (Edje_Signal_Cb)ContinueBtnCb, this);
    elm_object_signal_callback_add(mLayout, "HeaderBack", "HeaderBack",
            (Edje_Signal_Cb)HeaderBackCb, this);
}

void SignUpGenderScreen::ContinueBtnCb(void *data, Evas_Object *obj, void *EventInfo) {
    ScreenBase *newScreen = new SignUpPswdScreen(NULL);
    Application::GetInstance()->AddScreen(newScreen);
    elm_naviframe_item_title_enabled_set(elm_naviframe_top_item_get(newScreen->getNf()), EINA_FALSE, EINA_FALSE);
}

void SignUpGenderScreen::GenderChangeCb(void *data, Evas_Object *obj, void *EventInfo) {
    ERadioValue SelectedValue = static_cast<ERadioValue>(elm_radio_state_value_get(obj));
    if (SelectedValue == EGenderMale) {
        SignupRequest::Singleton().SetData(SignupRequest::ISGENDERFEMALE, "false");
    } else if (SelectedValue == EGenderFemale) {
        SignupRequest::Singleton().SetData(SignupRequest::ISGENDERFEMALE, "true");
    }
}

void SignUpGenderScreen::HeaderBackCb(void* Data, Evas_Object* Obj, void* EventInfo) {
    SignUpGenderScreen* Me = static_cast<SignUpGenderScreen*>(Data);
    Me->Pop();
}
