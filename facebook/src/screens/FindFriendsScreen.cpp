#include "Config.h"
#include "FindFriendsScreen.h"
#include "FriendRequestsBatchProvider.h"
#include "FriendsContactsTab.h"
#include "FriendsRequestTab.h"
#include "FriendsSearchTab.h"
#include "FriendsTab.h"
#include "Popup.h"
#include "Utils.h"
#include "WidgetFactory.h"

#include <assert.h>

FindFriendsToTab::FindFriendsToTab() : ffScreen(nullptr), mTab(FRIENDS_SEARCH_TAB) {
}

FindFriendsScreen::FindFriendsScreen(bool isFF) : ScreenBase(nullptr), mIsFF(isFF)
{
    mScreenId = SID_FIND_FRIENDS;

    if (mIsFF) {
        mActiveTab = FRIENDS_SEARCH_TAB;
    } else {
        mActiveTab = FRIENDS_REQUEST_TAB;
    }

    mScroller = nullptr;
    mTabbar = nullptr;

    mSearchTab = nullptr;
    mRequestTab = nullptr;
    mFriendsTab = nullptr;

    mSearchFriendsTab = nullptr;
    mFriendsRequest = nullptr;
    mFriends = nullptr;
    mTabs = nullptr;
    mLayout = CreateView(getParentMainLayout());
    if (mIsFF) {
        elm_object_focus_set(mSearchFriendsTab->mSearchEntry, EINA_TRUE);
        elm_entry_input_panel_enabled_set(mSearchFriendsTab->mSearchEntry, EINA_TRUE);
    }
    AppEvents::Get().Subscribe(eUPDATE_FIND_FRIENDS_REQUESTS_COUNT, this);
    AppEvents::Get().Subscribe(eDELETE_BLOCKED_FRIEND_FROM_OTHER_LIST, this);
}

Evas_Object *FindFriendsScreen::CreateView(Evas_Object *parent)
{
    Evas_Object *layout = WidgetFactory::CreateLayoutByGroup(parent, "find_friends_screen");

    elm_object_translatable_part_text_set(layout, "title", "IDS_FIND_FRIENDS");

    mTabbar = CreateTabbar(layout);
    elm_object_part_content_set (layout, "tabbar", mTabbar);

    Evas_Object *box = elm_box_add(layout);
    evas_object_size_hint_weight_set(box, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(box, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_object_part_content_set(layout, "content", box);

    mScroller = CreateScroller(box);
    evas_object_smart_callback_add(mScroller, "scroll,anim,stop", anim_stop_scroll_cb, this);
    elm_box_pack_end(box, mScroller);

    mNestedScreenContainer = elm_box_add(mScroller);
    elm_box_horizontal_set(mNestedScreenContainer, EINA_TRUE);
    evas_object_size_hint_weight_set(mNestedScreenContainer, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mNestedScreenContainer, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_object_content_set(mScroller, mNestedScreenContainer);
    evas_object_show(mNestedScreenContainer);


    mSearchFriendsTab = new FriendsSearchTab(this, (mActiveTab == FRIENDS_SEARCH_TAB));
    Evas_Object *tabLayout = MinSet(((ScreenBase *) mSearchFriendsTab)->getMainLayout(), mNestedScreenContainer, 0, 0);
    evas_object_size_hint_min_set(tabLayout, R->FF_MAIN_RECT_SIZE_W, 0);
    mFriendsRequest = new FriendsRequestTab(this, true);
    tabLayout = MinSet(((ScreenBase *) mFriendsRequest)->getMainLayout(), mNestedScreenContainer, 0, 0);
    evas_object_size_hint_min_set(tabLayout, R->FF_MAIN_RECT_SIZE_W, 0);
    mFriends = new FriendsTab(this);
    tabLayout = MinSet(((ScreenBase *) mFriends)->getMainLayout(), mNestedScreenContainer, 0, 0);
    evas_object_size_hint_min_set(tabLayout, R->FF_MAIN_RECT_SIZE_W, 0);

    evas_object_event_callback_add(mScroller, EVAS_CALLBACK_RESIZE, resize_cb, this);

    elm_object_signal_callback_add(layout, "mouse,clicked,*", "back_btn", BackButtonClicked, this);

    return layout;
}

FindFriendsScreen::~FindFriendsScreen()
{
    evas_object_smart_callback_del(mScroller, "scroll,anim,stop", anim_stop_scroll_cb);
    elm_object_signal_callback_del(mLayout, "mouse,clicked,*", "back_btn", on_back_button_clicked);

    elm_object_signal_callback_del(mSearchTab, "item.clicked", "", on_tabbar_clicked_cb);
    elm_object_signal_callback_del(mRequestTab, "item.clicked", "", on_tabbar_clicked_cb);
    elm_object_signal_callback_del(mFriendsTab, "item.clicked", "", on_tabbar_clicked_cb);

    void *listData;
    EINA_LIST_FREE(mTabs, listData) {
        delete static_cast<FindFriendsToTab *>(listData);
    }
}

void FindFriendsScreen::Push()
{
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void FindFriendsScreen::on_back_button_clicked(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    if (Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_FIND_FRIENDS) {
        FindFriendsScreen *screen = static_cast<FindFriendsScreen *>(data);
        if (screen) {
            screen->RemoveSearchEntryFocus();
            screen->Pop();
        }
    }
    Log::debug("FindFriendsScreen::on_back_button_clicked");
}

void FindFriendsScreen::resize_cb(void *data, Evas *e, Evas_Object *obj, void *event_info) {
    FindFriendsScreen *me = static_cast<FindFriendsScreen *> (data);
    Evas_Coord w, h;

    evas_object_geometry_get(obj, nullptr, nullptr, &w, &h);

    elm_scroller_page_size_set(me->mScroller, w, 0);
    //This is a workaround for keypad removing on T3.0. Otherwise, the FRIENDS_SEARCH_TAB will shown again.
    ecore_job_add(show_new_page_cb, me);
}

bool FindFriendsScreen::HandleBackButton()
{
    bool ret = true;
    if (mFriends && mFriends->DeleteConfirmationDialog()) {
    } else if (PopupMenu::Close()) {
    } else {
        ret = false;
    }
    return ret;
}

Evas_Object *FindFriendsScreen::CreateTabbar(Evas_Object *parent)
{
    Evas_Object *scroller = elm_scroller_add(parent);
    elm_scroller_single_direction_set(scroller, ELM_SCROLLER_SINGLE_DIRECTION_HARD);
    elm_scroller_policy_set(scroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_OFF);
    evas_object_show(scroller);

    Evas_Object *data_area = elm_box_add(scroller);
    evas_object_size_hint_weight_set(data_area, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(data_area, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_box_padding_set(data_area, 0, 0);
    elm_box_horizontal_set(data_area, EINA_TRUE);
    elm_object_content_set(scroller, data_area);
    evas_object_show(data_area);

    mSearchTab = CreateTabbarItem(data_area, "IDS_FFS_SEARCH", mIsFF, FRIENDS_SEARCH_TAB);
    mRequestTab = CreateTabbarItem(data_area, "IDS_FFS_REQUESTS", !mIsFF, FRIENDS_REQUEST_TAB);
    mFriendsTab = CreateTabbarItem(data_area, "IDS_FFS_FRIENDS", false, FRIENDS_FRIENDS_TAB);

    int badgeCount = FriendRequestsBatchProvider::GetInstance()->GetRequestsTotalCount();
    elm_object_signal_emit(mRequestTab, "badge.on", "");
    RefreshBadgeCount(badgeCount);

    return scroller;
}

Evas_Object *FindFriendsScreen::CreateTabbarItem(Evas_Object *parent, const char *text2set, bool isSelected, FindFriendsScreenTabs tab)
{
    Evas_Object *item = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(parent, "tabbar_item");

    Evas_Object *label = elm_label_add(item);
    elm_object_part_content_set(item, "item_text", label);
    evas_object_size_hint_weight_set(label, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(label, EVAS_HINT_FILL, 0.5);
    FRMTD_TRNSLTD_TXT_SET2(label, SANS_MEDIUM_9197a3_FORMAT, R->TABBAR_ITEM_FONT_SIZE, text2set);
    evas_object_show(label);

    if (isSelected) {
        elm_object_signal_emit(item, "set.selected", "");
    }

    FindFriendsToTab *tabData = new FindFriendsToTab();
    tabData->ffScreen = this;
    tabData->mTab = tab;
    mTabs = eina_list_append(mTabs, tabData);

    elm_object_signal_callback_add(item, "item.clicked", "", on_tabbar_clicked_cb, tabData);

    return item;
}

Evas_Object *FindFriendsScreen::CreateScroller(Evas_Object *parent)
{
    Evas_Object *scroller = elm_scroller_add(parent);
    elm_scroller_policy_set(scroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_OFF);
    elm_scroller_page_scroll_limit_set(scroller, 1, 1);
    elm_scroller_single_direction_set(scroller, ELM_SCROLLER_SINGLE_DIRECTION_HARD);
    evas_object_size_hint_weight_set(scroller, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(scroller, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_show(scroller);

    return scroller;
}

void FindFriendsScreen::anim_stop_scroll_cb(void *data, Evas_Object *obj, void *event_info)
{
    FindFriendsScreen *me = static_cast<FindFriendsScreen*>(data);
    assert(me);

    int page = 0;
    elm_scroller_current_page_get(me->mScroller, &page, nullptr);

    me->SwitchTab((FindFriendsScreenTabs) page);
}

void FindFriendsScreen::on_tabbar_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    FindFriendsToTab *fs = static_cast<FindFriendsToTab*>(data);
    FindFriendsScreen *me = static_cast<FindFriendsScreen*>(fs->ffScreen);

    me->SwitchTab(fs->mTab);
}

void FindFriendsScreen::SwitchTab(FindFriendsScreenTabs tab)
{
    if (mActiveTab == tab) {
        return;
    }

    elm_object_signal_emit(mSearchTab, "set.unselected", "");
    elm_object_signal_emit(mRequestTab, "set.unselected", "");
    elm_object_signal_emit(mFriendsTab, "set.unselected", "");

    if (tab == FRIENDS_SEARCH_TAB) {
        elm_object_signal_emit(mSearchTab, "set.selected", "");
    } else if (tab == FRIENDS_REQUEST_TAB) {
        elm_object_signal_emit(mRequestTab, "set.selected", "");
    } else if (tab == FRIENDS_FRIENDS_TAB) {
        elm_object_signal_emit(mFriendsTab, "set.selected", "");
    }

    int page = 0;
    elm_scroller_current_page_get(mScroller, &page, nullptr);
    if (page != tab) {
        elm_scroller_page_show(mScroller, tab, 0);
    }

    switch (tab) {
    case FRIENDS_SEARCH_TAB:
        SetSearchEntryFocus();
        break;
    case FRIENDS_REQUEST_TAB:
        RemoveSearchEntryFocus();
        break;
    case FRIENDS_FRIENDS_TAB:
        RemoveSearchEntryFocus();
        break;
    default:
        break;
    }
    mActiveTab = tab;
}

void FindFriendsScreen::show_new_page_cb(void *data) {
    FindFriendsScreen *me = static_cast<FindFriendsScreen*>(data);
    elm_scroller_page_show(me->mScroller, me->mActiveTab, 0);
}

void FindFriendsScreen::SetSearchEntryFocus()
{
    CHECK_RET_NRV(mSearchFriendsTab);
    CHECK_RET_NRV(mSearchFriendsTab->mSearchEntry);
    elm_object_focus_set(mSearchFriendsTab->mSearchEntry, EINA_TRUE);
    elm_entry_cnp_mode_set(mSearchFriendsTab->mSearchEntry, ELM_CNP_MODE_PLAINTEXT);
    elm_entry_input_panel_show(mSearchFriendsTab->mSearchEntry);
}

void FindFriendsScreen::RemoveSearchEntryFocus()
{
    CHECK_RET_NRV(mSearchFriendsTab);
    CHECK_RET_NRV(mSearchFriendsTab->mSearchEntry);
    elm_object_focus_set(mSearchFriendsTab->mSearchEntry, EINA_FALSE);
    elm_entry_input_panel_hide(mSearchFriendsTab->mSearchEntry);
}

void FindFriendsScreen::RefreshBadgeCount(int badgeCount) {
    if(mRequestTab) {
        if (badgeCount == 0) {
            elm_object_signal_emit(mRequestTab, "badge.off", "");
            elm_object_part_text_set(mRequestTab, "item_badge", "");
        } else if (badgeCount > 0) {
            elm_object_signal_emit(mRequestTab, "badge.on", "");
            if (mActiveTab == FRIENDS_REQUEST_TAB) {
                elm_object_signal_emit(mRequestTab, "set.selected", "");
            }
            char bageStrValue[3];
            if (badgeCount > 9) {
                eina_strlcpy(bageStrValue, "+9", 3);
            } else {
                Utils::Snprintf_s(bageStrValue, 3, "%d", badgeCount);
            }
            elm_object_part_text_set(mRequestTab, "item_badge", bageStrValue);
        }
    }
}

void FindFriendsScreen::OnResume() {
    if(mFriendsRequest){
        mFriendsRequest->OnResume();
    }
}

void FindFriendsScreen::Update(AppEventId eventId, void *data) {
    switch (eventId) {
    case eUPDATE_FIND_FRIENDS_REQUESTS_COUNT:
        RefreshBadgeCount(FriendRequestsBatchProvider::GetInstance()->GetRequestsTotalCount());
        break;
    case eDELETE_BLOCKED_FRIEND_FROM_OTHER_LIST:
        if (data) {
            mSearchFriendsTab->DeleteItemById(static_cast<char*>(data));
        }
        break;
    default:
        break;
    }
}
