#include <string>
#include <sstream>
#include <system_info.h>

#include "Common.h"
#include "Config.h"
#include "dlog.h"
#include "FacebookSession.h"
#include "FbRespondAbout.h"
#include "UserProfileAbout.h"
#include "UserProfileAboutMore.h"
#include "SearchScreen.h"
#include "UserProfilePlaces.h"

#define APP_EDJ "edje/fbapp.edj"
#define UP_IMAGES_PATH "images/profiles/"
#define LAYOUT_DEFAULT_WIDTH 480

UserProfileAbout::UserProfileAbout(ScreenBase* ParentScreen, const char *profileId):
    ScreenBase(ParentScreen) {
    mScreenId = ScreenBase::SID_USER_PROFILE_ABOUT;
    mProfileId = profileId ? strdup(profileId) : NULL;
    mGenList = NULL;
    CreateView();
}

UserProfileAbout::~UserProfileAbout() {
    if(mGenList) {
        elm_genlist_clear(mGenList);
    }
    free(mProfileId);
}

void UserProfileAbout::Push() {
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void UserProfileAbout::LoadEdjLayout() {
#if 0
    mLayout = elm_layout_add(getParentMainLayout());
    elm_layout_file_set(mLayout, Application::mEdjPath, "UserProfileAbout");
    evas_object_size_hint_weight_set(mLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_show(mLayout);
#endif

#if 1
    mLayout = elm_layout_add(getParentMainLayout());
    elm_layout_file_set(mLayout, Application::mEdjPath, "UserProfileAbout");
    evas_object_size_hint_weight_set(mLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);

    box = elm_box_add(mLayout);
    evas_object_size_hint_weight_set(box, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(box, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_object_part_content_set(mLayout, "UPAboutContentScroller", box);

    scroller = elm_scroller_add(mLayout);
    elm_scroller_policy_set(scroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_OFF);
	elm_scroller_page_size_set(scroller, 0 ,0);
    elm_scroller_page_scroll_limit_set(scroller, 0, 1);
    elm_scroller_single_direction_set(scroller, ELM_SCROLLER_SINGLE_DIRECTION_HARD);
    evas_object_size_hint_weight_set(scroller, 1, 1);
    evas_object_size_hint_align_set(scroller, -1, -1);
    evas_object_show(scroller);
    elm_box_pack_end(box, scroller);

    about_content_box_container = elm_box_add(scroller);
    elm_box_padding_set(about_content_box_container, 0, 20);
    elm_box_horizontal_set(about_content_box_container, EINA_FALSE);
    evas_object_size_hint_weight_set(about_content_box_container, 1, 1);
    evas_object_size_hint_align_set(about_content_box_container, -1, 0);
    elm_object_content_set(scroller, about_content_box_container);
    evas_object_show(about_content_box_container);

    about_overview_layout = elm_layout_add(about_content_box_container);
    evas_object_size_hint_weight_set(about_overview_layout, 1, 1);
    evas_object_size_hint_align_set(about_overview_layout, 0, 0);
    elm_layout_file_set(about_overview_layout,  Application::mEdjPath, "About_overview_group");
    evas_object_show(about_overview_layout);

    elm_box_pack_end(about_content_box_container, about_overview_layout);

    evas_object_show(mLayout);
#endif

}

void UserProfileAbout::CreateView() {
    LoadEdjLayout();
    elm_object_translatable_part_text_set(mLayout, "UPAboutHeaderText", "IDS_ABOUT");
    elm_object_signal_callback_add(mLayout, "UPAboutHeaderBack", "UPAboutHeaderBack",
            (Edje_Signal_Cb)UPAboutHeaderBackCb, this);
    elm_object_signal_callback_add(mLayout, "SearchClickSignal", "SearchClickSource",
            (Edje_Signal_Cb)SearchCb, this);

    mProgressBar = elm_progressbar_add(about_overview_layout);
    elm_object_style_set(mProgressBar, "process_medium");
    elm_object_part_content_set(about_overview_layout, "about_overview_content", mProgressBar);
    elm_progressbar_pulse_set(mProgressBar, EINA_TRUE);
    elm_progressbar_pulse(mProgressBar, EINA_TRUE);
    evas_object_show(mProgressBar);

    LoadData();

    UserProfilePlaces *places1 = new UserProfilePlaces(about_content_box_container, static_cast<ScreenBase*>(this), mProfileId);
}

void UserProfileAbout::UPAboutHeaderBackCb(void* Data, Evas_Object* Obj, void* EventInfo) {
    UserProfileAbout *Me = static_cast<UserProfileAbout*>(Data);
    Me->Pop();
}

void UserProfileAbout::SearchCb(void* Data, Evas_Object* Obj, void* EventInfo) {
    ScreenBase* NewScreen = new SearchScreen(NULL);
    Application::GetInstance()->AddScreen(NewScreen);
}

void UserProfileAbout::MoreCb(void* Data, Evas_Object* Obj, void* EventInfo) {
    ScreenBase* NewScreen = new UserProfileAboutMore(NULL);
    Application::GetInstance()->AddScreen(NewScreen);
    elm_naviframe_item_title_enabled_set(elm_naviframe_top_item_get(NewScreen->getNf()), EINA_FALSE, EINA_FALSE);
}

void UserProfileAbout::UpdateCb(void* Data, Evas_Object* Obj, void* EventInfo) {
    //UserProfileAbout *Me = static_cast<UserProfileAbout*>(Data);
}

void UserProfileAbout::LoadData() {
    FacebookSession::GetInstance()->GetUPAboutOverview(mProfileId, GROverviewCallBack, this);
}

void UserProfileAbout::LoadOverview() {
    static Elm_Genlist_Item_Class GenListItem;

    mGenList = elm_genlist_add(about_overview_layout);
    elm_genlist_select_mode_set(mGenList, ELM_OBJECT_SELECT_MODE_NONE);
    elm_scroller_single_direction_set(mGenList, ELM_SCROLLER_SINGLE_DIRECTION_HARD);
    evas_object_hide(mProgressBar);
    elm_layout_content_set(about_overview_layout, "about_overview_content", mGenList);

    GenListItem.item_style = "full";
    GenListItem.homogeneous = EINA_FALSE;
    GenListItem.func.content_get = CreateOverviewItemView;
    GenListItem.func.state_get = NULL;
    GenListItem.func.del = NULL;

    Eina_List *List;
    void *ListItemData;
    EINA_LIST_FOREACH(mParsedResponse->mOverviewList, List, ListItemData) {
        elm_genlist_item_append(mGenList, &GenListItem, ListItemData, NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);
    }

    evas_object_show(mGenList);
}

void UserProfileAbout::GROverviewCallBack(void* Object, char* Respond, int CurlCode) {
    if (CURLE_OK == CurlCode) {
        UserProfileAbout* Me = static_cast<UserProfileAbout*>(Object);
        Me->mParsedResponse = FbRespondAbout::Parse(Respond);
        if (Me->mParsedResponse) {
            Me->LoadOverview();
        }
    }
}

Evas_Object* UserProfileAbout::CreateOverviewItemView(void* Data, Evas_Object* Obj, const char* Part) {
    FbRespondAbout::ListItem *ListItemData = static_cast<FbRespondAbout::ListItem*>(Data);

    char *resourcePath = app_get_resource_path();
    if (!resourcePath) {
        return nullptr;
    }
    std::string AppImagesPath(resourcePath);
    AppImagesPath.append(UP_IMAGES_PATH);
    free(resourcePath);

    std::string LayoutGroup = "UPAboutOverviewItem";
    int LayoutHeight = R->UPA_LAYOUT_DEFAULT_H;
    if (!ListItemData->mPast.empty()) {
        LayoutGroup = "UPAboutOverviewItemMulti";
        LayoutHeight = R->UPA_LAYOUT_MULTI_H;
    } else if (ListItemData->mMore) {
        LayoutGroup = "UPAboutOverviewItemMore";
    }

    Evas_Object *Layout = elm_layout_add(Obj);
    evas_object_size_hint_weight_set(Layout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_min_set(Layout, R->UPA_LAYOUT_W, LayoutHeight);
    evas_object_size_hint_max_set(Layout, R->UPA_LAYOUT_W, LayoutHeight);
    elm_layout_file_set(Layout, Application::mEdjPath, LayoutGroup.c_str());

    Evas_Object *TmpObj = NULL;
    std::string TmpStr;
    if (!ListItemData->mMore) {
        if (!ListItemData->mImage.empty()) {
            TmpStr = AppImagesPath;
            TmpStr.append(ListItemData->mImage.c_str());
            TmpObj = elm_image_add(Layout);
            elm_image_file_set(TmpObj, TmpStr.c_str(), NULL);
            TmpStr.clear();
            elm_layout_content_set(Layout, "UPAboutOverviewItemImage", TmpObj);
            evas_object_show(TmpObj);
        }

        if (!ListItemData->mHeading.empty() || !ListItemData->mInfo.empty()) {
            TmpObj = NewLabel(Layout, "UPAboutOverviewItemText", R->UPA_OV_WRAP_MAX);
            TmpStr.append("<font=TizenSans:style=Med><font_size=");
            TmpStr.append(R->UPA_OV_TEXT_FONT_SIZE);
            TmpStr.append("><color=#6c7281>");
            TmpStr.append(ListItemData->mHeading.c_str());
            TmpStr.append(" ");
            if (ListItemData->mAt) {
                TmpStr.append(i18n_get_text("IDS_AT"));
                TmpStr.append(" ");
            }
            TmpStr.append("</color><color=#141823>");
            TmpStr.append(ListItemData->mInfo.c_str());
            TmpStr.append("</color></font_size></font>");
            elm_object_text_set(TmpObj, TmpStr.c_str());
            TmpStr.clear();
        }

        if (!ListItemData->mPast.empty()) {
            TmpObj = NewLabel(Layout, "UPAboutOverviewItemPast", R->UPA_OV_WRAP_MAX);
            TmpStr.append("<font=TizenSans:style=Regular><font_size=");
            TmpStr.append(R->UPA_OV_PAST_FONT_SIZE);
            TmpStr.append("><color=#9298a4>");
            TmpStr.append(ListItemData->mPast.c_str());
            TmpStr.append("</color></font_size></font>");
            elm_object_text_set(TmpObj, TmpStr.c_str());
            TmpStr.clear();
        }
    } else {
        TmpObj = NewLabel(Layout, "UPAboutOverviewItemHeading", R->UPA_OV_MORE_HEADING_WRAP_MAX);
        TmpStr.append("<font=TizenSans:style=Med><font_size=");
        TmpStr.append(R->UPA_OV_HEADING_FONT_SIZE);
        TmpStr.append("><color=#141823>");
        TmpStr.append(ListItemData->mHeading.c_str());
        TmpStr.append("</color></font_size></font>");
        elm_object_text_set(TmpObj, TmpStr.c_str());
        TmpStr.clear();

        TmpObj = NewLabel(Layout, "UPAboutOverviewItemInfo", R->UPA_OV_MORE_INFO_WRAP_MAX);
        TmpStr.append("<font=TizenSans:style=Regular><font_size=");
        TmpStr.append(R->UPA_OV_INFO_FONT_SIZE);
        TmpStr.append("><color=#9298a4>");
        TmpStr.append(ListItemData->mInfo.c_str());
        TmpStr.append("</color></font_size></font>");
        elm_object_text_set(TmpObj, TmpStr.c_str());

        elm_object_signal_callback_add(Layout, "UPAboutItemMoreClicked", "UPAboutOverviewItem*",
                (Edje_Signal_Cb)MoreCb, NULL);
        elm_object_signal_callback_add(Layout, "UPAboutItemUpdateClicked", "UPAboutOverviewUpdateIcon",
                (Edje_Signal_Cb)UpdateCb, NULL);
    }

    return Layout;
}

Evas_Object* UserProfileAbout::NewLabel(Evas_Object* Layout, const char* Part, int WrapMax) {
    Evas_Object* NewLabel = elm_label_add(Layout);
    elm_label_line_wrap_set(NewLabel, ELM_WRAP_MIXED);
    elm_label_wrap_width_set(NewLabel, WrapMax);
    elm_layout_content_set(Layout, Part, NewLabel);
    return NewLabel;
}
