#include "Common.h"
#include "CommonScreen.h"
#include "Config.h"
#include "SignUpBDateScreen.h"
#include "SignUpGenderScreen.h"
#include "SignupRequest.h"
#include "WebViewScreen.h"

#define LEARN_MORE_URL "https://m.facebook.com/birthday_help.php"

SignUpBDateScreen::SignUpBDateScreen(ScreenBase *parentScreen):ScreenBase(parentScreen)  {
    CreateView();
}

SignUpBDateScreen::~SignUpBDateScreen() {
}

void SignUpBDateScreen::LoadEdjLayout() {
    mLayout = elm_layout_add(getParentMainLayout());
    elm_layout_file_set(mLayout, Application::mEdjPath, "signup_bdate_screen");
    evas_object_size_hint_weight_set(mLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_show(mLayout);
}

void SignUpBDateScreen::CreateView() {
    LoadEdjLayout();

    elm_object_translatable_part_text_set(mLayout, "HeaderText", "IDS_ABOUT_YOU");
    elm_object_translatable_part_text_set(mLayout, "sup_bdate_heading", "IDS_SUP_BDATE_HEADING");

    mEvasBDate = elm_datetime_add(mLayout);
    elm_object_part_content_set(mLayout, "sup_bdate_input", mEvasBDate);
    evas_object_size_hint_weight_set(mEvasBDate, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    elm_datetime_format_set(mEvasBDate, "%b %d %Y");
    elm_datetime_field_visible_set(mEvasBDate, ELM_DATETIME_HOUR, EINA_FALSE);
    elm_datetime_field_visible_set(mEvasBDate, ELM_DATETIME_MINUTE, EINA_FALSE);
    elm_datetime_field_visible_set(mEvasBDate, ELM_DATETIME_AMPM, EINA_FALSE);
    elm_datetime_field_visible_set(mEvasBDate, ELM_DATETIME_YEAR, EINA_FALSE);
    elm_datetime_field_visible_set(mEvasBDate, ELM_DATETIME_MONTH, EINA_FALSE);
    elm_datetime_field_visible_set(mEvasBDate, ELM_DATETIME_DATE, EINA_FALSE);
    elm_box_pack_end(mEvasBDate, mEvasBDate);

    mEvasSelectBDateBtn = elm_button_add(mLayout);
    elm_object_part_content_set(mLayout, "sup_bdate_select", mEvasSelectBDateBtn);
    //elm_object_style_set(mEvasSelectBDateBtn, "anchor"); //TODO
    elm_object_translatable_text_set(mEvasSelectBDateBtn, "IDS_SUP_SELECT_BDATE");
    evas_object_smart_callback_add(mEvasSelectBDateBtn, "clicked", SelectBDateBtnCb, this);

    mEvasSelectBDateBtnOverlay = evas_object_rectangle_add(evas_object_evas_get(mLayout));
    elm_object_part_content_set(mLayout, "sup_bdate_select_overlay", mEvasSelectBDateBtnOverlay);

    elm_object_translatable_part_text_set(mLayout, "sup_bdate_nxt_btn_txt", "IDS_SUP_WC_NEXT");
    elm_object_signal_callback_add(mLayout, "got.a.sup.bdnxt.btn.click", "sup_bdate_nxt_btn*",
            (Edje_Signal_Cb)ContinueBtnCb, this);
    elm_object_signal_callback_add(mLayout, "HeaderBack", "HeaderBack",
            (Edje_Signal_Cb)HeaderBackCb, this);

    mEvasInfo = elm_entry_add(mLayout);
    elm_object_part_content_set(mLayout, "sup_bdate_info", mEvasInfo);
    elm_entry_editable_set(mEvasInfo, EINA_FALSE);
//    elm_entry_cursor_handler_disabled_set(mEvasInfo,EINA_FALSE);
//    elm_entry_select_allow_set(mEvasInfo,EINA_FALSE);
    elm_entry_text_style_user_push(mEvasInfo, "DEFAULT='font_size=23 color=#9298a4 align=center'");
    evas_object_size_hint_weight_set(mEvasInfo, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mEvasInfo, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_smart_callback_add(mEvasInfo, "anchor,clicked", AnchorCb, this);
    evas_object_show(mEvasInfo);
    SetInfo(false);
}

void SignUpBDateScreen::SetInfo(bool NoText) {
    std::string Info;
    if (!NoText) {
        Info = i18n_get_text("IDS_SUP_BDATE_INFO");
        Info.append(" ");
        Info.append("<a color=#3e5b9a>");
        Info.append(i18n_get_text("IDS_LEARN_MORE"));
        Info.append("</a>");
    }

    elm_object_part_text_set(mEvasInfo, "elm.text", Info.c_str());
}

void SignUpBDateScreen::SelectBDateBtnCb(void *data, Evas_Object *obj, void *EventInfo) {
    SignUpBDateScreen *self = static_cast<SignUpBDateScreen*>(data);

    elm_object_part_text_set(self->mLayout, "sup_bdate_error", " ");
    self->SetInfo(false);
    elm_object_part_content_unset(self->mLayout, "sup_bdate_select");
    evas_object_hide(self->mEvasSelectBDateBtn);
    elm_object_part_content_unset(self->mLayout, "sup_bdate_select_overlay");
    evas_object_hide(self->mEvasSelectBDateBtnOverlay);

    elm_datetime_field_visible_set(self->mEvasBDate, ELM_DATETIME_YEAR, EINA_TRUE);
    elm_datetime_field_visible_set(self->mEvasBDate, ELM_DATETIME_MONTH, EINA_TRUE);
    elm_datetime_field_visible_set(self->mEvasBDate, ELM_DATETIME_DATE, EINA_TRUE);
}

void SignUpBDateScreen::ContinueBtnCb(void *data, Evas_Object *obj, void *EventInfo) {
    char DateFormat[128] = {0};
    SignUpBDateScreen *self = static_cast<SignUpBDateScreen*>(data);
    bool IsValid = false;

    if (!evas_object_visible_get(self->mEvasSelectBDateBtn)) {
        elm_datetime_value_get(self->mEvasBDate, &self->mTmSelectedBDate);
        strftime(DateFormat, 128, "%m/%d/%Y", &self->mTmSelectedBDate);
        SignupRequest::Singleton().SetData(SignupRequest::BIRTHDAY, DateFormat);

        ScreenBase *newScreen = new SignUpGenderScreen(NULL);
        Application::GetInstance()->AddScreen(newScreen);
        elm_naviframe_item_title_enabled_set(elm_naviframe_top_item_get(newScreen->getNf()), EINA_FALSE, EINA_FALSE);
        IsValid = true;
    }
    self->SetInfo(!IsValid);
    elm_object_translatable_part_text_set(self->mLayout, "sup_bdate_error", !IsValid ? "IDS_SUP_BDATE_ERROR" : " ");
}

void SignUpBDateScreen::HeaderBackCb(void* Data, Evas_Object* Obj, void* EventInfo) {
    SignUpBDateScreen* Me = static_cast<SignUpBDateScreen*>(Data);
    Me->Pop();
}

void SignUpBDateScreen::AnchorCb(void* Data, Evas_Object* Obj, void* EventInfo) {
    WebViewScreen::Launch(LEARN_MORE_URL);
}
