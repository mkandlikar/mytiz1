#include <string>

#include "Common.h"
#include "ConnectivityManager.h"
#include "Log.h"
#include "ScreenBase.h"
#include "Utils.h"
#include "SearchScreen.h"
#include "WidgetFactory.h"

UIRes * ScreenBase::R = UIRes::GetInstance();

ScreenBase::ScreenBase(ScreenBase *parentScreen) :
        mScreenId(SID_UNKNOWN),
        mIsRightButtonEnabled(false),
        mIs1stActionBarActionEnabled(false),
        mIs2ndActionBarActionEnabled(false),
        mIsLaunchedByAppControl(false) {
    mParent = parentScreen;
    mLayout = NULL;
    mTitleLabel = NULL;
    mNfItem = NULL;
    mScreenArray = eina_array_new(5);
    if(mParent) {
        mParent->AddChildScreen(this);
    }

    mLockScreen = NULL;
    mHeaderCancelBtnWidget = NULL;
    mHeaderWidget = NULL;
    mVisibleTime = 0.0;
    mProgressBar = NULL;
    mTitle = NULL;
    mNestedScreenContainer = NULL;
    mErrorWidget = NULL;
    mErrorWidgetType = eCONNECTION_ERROR;

}

ScreenBase::~ScreenBase() {
    if (mTitleLabel)
        delete [] mTitleLabel;
    HideErrorWidget();
    ProgressBarHide();
    RemoveAllScreens();
    eina_array_free(mScreenArray);
}

void ScreenBase::Show() {
    if(mLayout) {
        evas_object_show(mLayout);
        mVisibleTime = Utils::GetCurrentTime();
    }
}

void ScreenBase::Hide() {
    if(mLayout) {
        evas_object_hide(mLayout);
    }
}

void ScreenBase::Push() {
    assert(mLayout);
    evas_object_event_callback_add(mLayout, EVAS_CALLBACK_DEL, Application::remove_main_layout_from_naviframe, nullptr);
    mNfItem = elm_naviframe_item_push(getNf(), mTitleLabel, NULL, NULL, mLayout, "basic");
}

void ScreenBase::Pop() {
    Application::GetInstance()->RemoveTopScreen();
}

void ScreenBase::InsertBefore(ScreenBase * parentScreen) {
    assert(mLayout);
    evas_object_event_callback_add(mLayout, EVAS_CALLBACK_DEL, Application::remove_main_layout_from_naviframe, nullptr);
    mNfItem = elm_naviframe_item_insert_before(getNf(), parentScreen->mNfItem, mTitleLabel, NULL, NULL, mLayout, "basic");
}

void ScreenBase::OnViewResize(Evas_Coord w, Evas_Coord h) {

}

void ScreenBase::AddChildScreen(ScreenBase *screen) {
    if((NULL != mScreenArray) && (NULL != screen)) {
        eina_array_push(mScreenArray, screen);
    }
}

void ScreenBase::RemoveAllScreens() {
    if(NULL != mScreenArray) {
        int size = eina_array_count(mScreenArray);
        for(int i = size-1; i >= 0; i--) {
            ScreenBase *screen = static_cast<ScreenBase *>(eina_array_data_get(mScreenArray, i));
            if(NULL != screen) {
                screen->Hide();
                delete screen;
            }
        }
        eina_array_clean(mScreenArray);
    }
}


Evas_Object *ScreenBase::getMainLayout() {
    return mLayout;
}

Evas_Object *ScreenBase::getParentMainLayout() {
    Evas_Object *parentLayout = NULL;
    if(mParent && mNestedScreenContainer) {
        parentLayout = mParent->getNestedScreenLayout();
    } else {
        parentLayout = Application::GetInstance()->mNf;
    }
    return parentLayout;
}

Evas_Object *ScreenBase::getNestedScreenLayout() {
    return mNestedScreenContainer;
}

Evas_Object *ScreenBase::getNf() {
    return Application::GetInstance()->mNf;
}

Evas_Object *ScreenBase::MinSet(Evas_Object *obj, Evas_Object *box, Evas_Coord w, Evas_Coord h)
{
    Evas_Object *table, *rect;

    table = elm_table_add(box);
    evas_object_show(table);

    rect = evas_object_rectangle_add(evas_object_evas_get(table));
    evas_object_size_hint_min_set(rect, w, h);
    evas_object_size_hint_weight_set(rect, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(rect, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_table_pack(table, rect, 0, 0, 1, 1);

    evas_object_size_hint_weight_set(obj, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(obj, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_table_pack(table, obj, 0, 0, 1, 1);

    evas_object_size_hint_weight_set(table, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(table, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_box_pack_end(box, table);

    return rect;
}

void ScreenBase::EnableCancelButton(bool enable) {
    if (enable) {
        evas_object_smart_callback_add(mHeaderCancelBtnWidget, "clicked", cancel_cb, this);
    } else {
        evas_object_smart_callback_del(mHeaderCancelBtnWidget, "clicked", cancel_cb);
    }
}

void ScreenBase::EnableRightButton(bool enable) {
    if (enable && !mIsRightButtonEnabled) {
        elm_object_signal_callback_add(mHeaderWidget, "mouse,clicked", "header_right_button", confirm_cb, this);
        mIsRightButtonEnabled = true;
    } else if (!enable && mIsRightButtonEnabled){
        mIsRightButtonEnabled = false;
        elm_object_signal_callback_del(mHeaderWidget, "mouse,clicked", "header_right_button", confirm_cb );
    }
}

void ScreenBase::cancel_cb(void *data, Evas_Object *obj, void *event_info)
{
    ScreenBase *screen = static_cast<ScreenBase *>(data);
    if (screen) {
        // Delete callback for current object to avoid loop of the same signals. This callback
        // is going to be registered again on the @obj recreation.
        screen->EnableRightButton(false);
        evas_object_smart_callback_del(screen->mHeaderCancelBtnWidget, "clicked", ScreenBase::cancel_cb);

        screen->CancelCallback(data, obj, event_info);
    }
}

void ScreenBase::on_back_btn_clicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    ScreenBase *screen = static_cast<ScreenBase *>(data);
    if (Application::GetInstance()->GetTopScreen() == screen) {
        // Delete callback for current object to avoid loop of the same signals. This callback
        // is going to be registered again on the @obj recreation.
        elm_object_signal_callback_del(screen->mHeaderWidget, "mouse,clicked,*", "image.back",
                ScreenBase::on_back_btn_clicked);
        screen->Pop();
    } else {
        Log::error("ScreenBase::on_back_btn_clicked->wrong top screen");
    }
}

void ScreenBase::on_actionbar_1st_action_clicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    ScreenBase *screen = static_cast<ScreenBase *>(data);
    if (screen) {
        screen->FirstActionClicked();
    }
}

void ScreenBase::Enable1stActionBarAction(bool enable) {
    if (enable && !mIs1stActionBarActionEnabled) {
        elm_object_signal_callback_add(mHeaderWidget, "mouse,clicked,*", "first_action", on_actionbar_1st_action_clicked, this);
        mIs1stActionBarActionEnabled = true;
    } else if (!enable && mIs1stActionBarActionEnabled) {
        mIs1stActionBarActionEnabled = false;
        elm_object_signal_callback_del(mHeaderWidget, "mouse,clicked,*", "first_action", on_actionbar_1st_action_clicked);
    }
}

void ScreenBase::on_actionbar_2nd_action_clicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    ScreenBase *screen = static_cast<ScreenBase *>(data);
    if (screen) {
        screen->SecondActionClicked();
    }
}

void ScreenBase::Enable2ndActionBarAction(bool enable) {
    if (enable && !mIs2ndActionBarActionEnabled) {
        elm_object_signal_callback_add(mHeaderWidget, "mouse,clicked,*", "second_action", on_actionbar_2nd_action_clicked, this);
        mIs2ndActionBarActionEnabled = true;
    } else if (!enable && mIs2ndActionBarActionEnabled) {
        mIs2ndActionBarActionEnabled = false;
        elm_object_signal_callback_del(mHeaderWidget, "mouse,clicked,*", "second_action", on_actionbar_2nd_action_clicked);
    }
}

void ScreenBase::CancelCallback(void *data, Evas_Object *obj, void *event_info)
{
    Log::debug("ScreenBase::CancelCallback");
    ScreenBase *screen = static_cast<ScreenBase *>(data);
    screen->Pop();
}

void ScreenBase::confirm_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::debug("ScreenBase::confirm_cb");
    ScreenBase *screen = static_cast<ScreenBase *>(data);
    if ( screen ) {
        //
        // Delete callback for current object to avoid loop of the same signals. This callback
        // is going to be registered again on the @obj recreation.
        screen->EnableRightButton(false);
        evas_object_smart_callback_del(screen->mHeaderCancelBtnWidget, "clicked", ScreenBase::cancel_cb);
        screen->ConfirmCallback(data, obj, emission, source);
    }
}

void ScreenBase::ConfirmCallback(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::debug("ScreenBase::ConfirmCallback");
    ScreenBase *screen = static_cast<ScreenBase *>(data);
    screen->ScreenBase::CancelCallback(data, obj, NULL);
}

void ScreenBase::ProgressBarShow()
{
    if (!mProgressBar) {
        mProgressBar = elm_progressbar_add(mLayout);
        elm_object_style_set(mProgressBar, "process_medium");
        evas_object_size_hint_align_set(mProgressBar, EVAS_HINT_FILL, EVAS_HINT_FILL);
        evas_object_size_hint_weight_set(mProgressBar, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
        elm_progressbar_pulse_set(mProgressBar, EINA_TRUE);
        elm_progressbar_pulse(mProgressBar, EINA_TRUE);
        evas_object_resize(mProgressBar, R->SCREEN_BASE_PROGRESS_BAR_ITEM_OBJ_RESIZE, R->SCREEN_BASE_PROGRESS_BAR_ITEM_OBJ_RESIZE);
        evas_object_move(mProgressBar, R->SCREEN_BASE_PROGRESS_BAR_ITEM_OBJ_MOVE_X, R->SCREEN_BASE_PROGRESS_BAR_ITEM_OBJ_MOVE_Y);
        evas_object_show(mProgressBar);
    }

    if ( Application::GetInstance()->GetTopScreen() == this) {
        evas_object_raise(mProgressBar);
    }
    else {
        evas_object_lower(mProgressBar);
    }
}

void ScreenBase::ProgressBarHide()
{
    if (mProgressBar) {
        evas_object_del(mProgressBar);
        mProgressBar = nullptr;
    }
}

void ScreenBase::ShowErrorWidget(ErrorWidgetType type)
{
    Log::debug("ScreenBase::ShowErrorWidget with type = %d", (int)type);
    if (mErrorWidget) {
        if (mErrorWidgetType == type) {
            if ( Application::GetInstance()->GetTopScreen() == this) {
                evas_object_raise(mErrorWidget);
            }
            else{
                evas_object_lower(mErrorWidget);
            }
            return;
        }
        else {
            evas_object_del(mErrorWidget);
        }
    }

    mErrorWidgetType = type;
    const char * errorTitle = NULL;
    switch (type) {
    case eCONNECTION_ERROR:
        errorTitle = "IDS_CANT_CONNECT";
        break;
    case eSOMETHING_WRONG_ERROR:
        errorTitle = "IDS_SOMETHING_WENT_WRONG";
        break;
    case eNO_DATA_AVAILABLE:
        mErrorWidget = WidgetFactory::CreateNoDataWidget(mLayout,
                                                         i18n_get_text("IDS_SORRY"),
                                                         "IDS_CONTENT_ISNT_AVAILABLE");
        return;
    }

    mErrorWidget = WidgetFactory::CreateErrorWidget(mLayout, errorTitle, "IDS_TAP_TO_RETRY",
            on_error_widget_retry_clicked_cb, this);
}

void ScreenBase::HideErrorWidget()
{
    if (mErrorWidget) {
        evas_object_del(mErrorWidget);
        mErrorWidget = nullptr;
    }
}

void ScreenBase::on_error_widget_retry_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    ScreenBase * me = static_cast<ScreenBase *>(data);
    if (!me) {
        return;
    }

    if (ConnectivityManager::Singleton().IsConnected()) {
        me->HideErrorWidget();
        me->OnErrorWidgetRetryClickedCb();
    }
    else if (me->mErrorWidgetType != eCONNECTION_ERROR) {
        me->ShowErrorWidget(eCONNECTION_ERROR);
    }
}

void ScreenBase::BackButtonClicked(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    ScreenBase *screen = static_cast<ScreenBase*>(data);
    if (Application::GetInstance()->GetTopScreen() == screen) {
        elm_object_signal_callback_del(screen->mLayout, "mouse,clicked,*", "back_btn_area", ScreenBase::BackButtonClicked);
        screen->Pop();
    } else {
        Log::error("ScreenBase::BackButtonClicked->wrong top screen");
    }
}

void ScreenBase::SearchButtonClicked(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_SEARCH) {
        SearchScreen* searchScreen = new SearchScreen();
        if (searchScreen) {
            Application::GetInstance()->AddScreen(searchScreen);
        }
    }
}

void ScreenBase::SetNoDataViewVisibility(bool isVisible)
{
    if (isVisible) {
        elm_object_signal_emit(mLayout, "error.show", "");
    } else {
        elm_object_signal_emit(mLayout, "error.hide", "");
    }
}

void ScreenBase::LockScreen() {
    if (!mLockScreen) {
        mLockScreen = elm_layout_add(mLayout);
        elm_layout_file_set(mLockScreen, Application::mEdjPath, "post_composer_locked_screen_layout");
        evas_object_show(mLockScreen);
    }
}

void ScreenBase::UnlockScreen() {
    if (mLockScreen) {
        evas_object_hide(mLockScreen);
        evas_object_del(mLockScreen); mLockScreen = NULL;
    }
}

void ScreenBase::OnResume() {
    if (mErrorWidget) {
        evas_object_raise(mErrorWidget);
    }
    if (mProgressBar) {
        evas_object_raise(mProgressBar);
    }
}

void ScreenBase::OnPause() {
    if (mErrorWidget) {
        evas_object_lower(mErrorWidget);
    }
    if (mProgressBar) {
        evas_object_lower(mProgressBar);
    }
}

