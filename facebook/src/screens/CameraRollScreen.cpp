#include "CameraRollScreen.h"
#include "Common.h"
#include "ImagesCarouselScreen.h"
#include "Log.h"

#include "PostComposerScreen.h"
#include "SelectedMediaList.h"
#include "VideoPlayerScreen.h"
#include "WidgetFactory.h"

#include <Elementary.h>
#include <media_video.h>
#include <notification.h>
#include <stdio.h>

const unsigned int CameraRollScreen::MIN_VIDEO_HEIGHT = 120;
static constexpr char LogTag[] = "Facebook::CameraRoll";

CameraRollScreen::CameraRollScreen(CameraRollMode mode, IUpdateMediaData * iUpdate, Album *album) :
        ScreenBase(NULL), mMediaList(NULL), mIsLongPressed(false), mAlbum(album), mToUserId(NULL), mToUserName(NULL)
{
    mScreenId = ScreenBase::SID_CAMERA_ROLL;
    Log::debug_tag(LogTag, "CameraRollScreen(iUpdate, album: %s)", (album ? album->mName : "???"));
    mIUpdate = iUpdate;
    mMode = mode;
    Init();
}

CameraRollScreen::CameraRollScreen(CameraRollMode mode, Group *userGroup) :
    ScreenBase(NULL), mMediaList(NULL), mIUpdate(NULL), mIsLongPressed(false), mAlbum(NULL), mToUserId(NULL), mToUserName(NULL) {
    mScreenId = ScreenBase::SID_CAMERA_ROLL;
    Log::debug_tag(LogTag, "CameraRollScreen(userGroup: %s)", (userGroup ? userGroup->mName : "???"));
    mUserGroup = userGroup;
    mMode = mode;
    Init();
}

// This variant is for posting gallery photo to friend's timeline
CameraRollScreen::CameraRollScreen(CameraRollMode mode, const char* userId, const char* userName) :
    ScreenBase(NULL), mMediaList(NULL), mIUpdate(NULL), mIsLongPressed(false), mAlbum(NULL) {
    mScreenId = ScreenBase::SID_CAMERA_ROLL;
    Log::debug_tag(LogTag, "CameraRollScreen(userId: %s, userName: %s)", userId, userName);
    mUserGroup = NULL;
    mToUserId = userId? strdup(userId) : NULL;
    mToUserName = userName? strdup(userName) : NULL;
    mMode = mode;
    Init();
}

CameraRollScreen::~CameraRollScreen() {
    evas_object_smart_callback_del(mGengrid, "unselected", gengrid_content_unselected_cb);
    evas_object_smart_callback_del(mGengrid, "longpressed", gengrid_content_longpressed_cb);
    evas_object_smart_callback_del(mGengrid, "realized", gengrid_content_realized_cb);
    evas_object_smart_callback_del(mGengrid, "selected", gengrid_content_selected_cb);

    elm_gengrid_clear(mGengrid);
    ClearMediaDataList(mMediaList);
    ClearMediaDataList(mCapturedMediaList);
    free(mToUserId);
    free(mToUserName);

    media_content_unset_db_updated_cb();
}

void CameraRollScreen::Push()
{
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void CameraRollScreen::Init() {
    mGengrid = NULL;
    mGIC = NULL;
    mCapturedMediaList = NULL;

    mLayout = WidgetFactory::CreateLayoutByGroup(getParentMainLayout(), "camera_roll_screen");

    CreateHeader();

    media_content_set_db_updated_cb(media_content_update_cb, this);

    CreateGengrid();
    PopulateMediaList();
    FillGengrid();

    AppEvents::Get().Subscribe(eCAMERA_ROLL_SELECT_EVENT, this);
}

void CameraRollScreen::media_content_update_cb (media_content_error_e error,
                                    int pid,
                                    media_content_db_update_item_type_e update_item,
                                    media_content_db_update_type_e update_type,
                                    media_content_type_e media_type,
                                    char *uuid,
                                    char *path,
                                    char *mime_type,
                                    void *user_data) {
    CameraRollScreen *me = static_cast<CameraRollScreen *> (user_data);

    if (update_type == MEDIA_CONTENT_DELETE) {
        Eina_List *listItem;
        void *listData;
        PostComposerMediaData *mediaData;
        EINA_LIST_FOREACH(me->mMediaList, listItem, listData) {
            mediaData = static_cast<PostComposerMediaData *>(listData);
            if (!strcmp(path, mediaData->GetFilePath())) {
                // Delete item from the grid
                elm_object_item_del(mediaData->GetGengridItem());

                if (mediaData->IsSelected()) {
                    // Update header controls is required if there are no items selected
                    me->UpdateHeaderButtons();

                    // Refresh gengrid indexes
                    gengrid_refresh_selected_indexes_from(me->mGengrid, mediaData->GetIndex());
                }

                // Delete item from the items list
                me->mMediaList = eina_list_remove_list(me->mMediaList, listItem);
                mediaData->Release();
                break;
            }
        }

        // Delete item from SelectedList
        EINA_LIST_FOREACH(SelectedMediaList::Get(), listItem, listData) {
            mediaData = static_cast<PostComposerMediaData *>(listData);
            if (!strcmp(path, mediaData->GetFilePath())) {
                SelectedMediaList::RemoveItem(mediaData);
                break;
            }
        }
    } else if (update_type == MEDIA_CONTENT_INSERT) {
        // Create MediaData object for inserted media
        Eina_List *capturedMedia = nullptr;
        Utils::SelectMediaFileFromMediaDBByPath(path, &gallery_media_item_cb, &capturedMedia);
        PostComposerMediaData *mediaData = static_cast<PostComposerMediaData *>(eina_list_data_get(capturedMedia));

        // Add new item to MediaList
        me->mMediaList = eina_list_prepend(me->mMediaList, mediaData);

        // Create tile and put it to the grid
        Elm_Object_Item *item = elm_gengrid_item_prepend(me->mGengrid, me->mGIC, mediaData, nullptr, nullptr);
        mediaData->SetGengridItem(item);
    }
}

void CameraRollScreen::ClearMediaDataList(Eina_List *mediaDataList) {
    void *listData;
    EINA_LIST_FREE(mediaDataList, listData) {
        PostComposerMediaData *mediaData = static_cast<PostComposerMediaData *>(listData);
        mediaData->Release();
    }
}

void CameraRollScreen::FillGengrid() {
    elm_gengrid_clear(mGengrid);

    Eina_List *itemsToSelect = nullptr;
    PostComposerMediaData *mediaData;
    Eina_List *listItem;
    void *listData;
    Elm_Object_Item *item;
    EINA_LIST_FOREACH(mMediaList, listItem, listData) {
        mediaData = static_cast<PostComposerMediaData *>(listData);
        item = elm_gengrid_item_append(mGengrid, mGIC, mediaData, nullptr, nullptr);
        mediaData->SetGengridItem(item);

        if (mediaData->IsSelected()) {
            itemsToSelect = eina_list_append(itemsToSelect, mediaData);
        }
    }

    // To keep proper order, items have to be selected as they appear in SelectedMediaList
    EINA_LIST_FOREACH(SelectedMediaList::Get(), listItem, listData) {
        mediaData = static_cast<PostComposerMediaData *>(eina_list_search_unsorted(itemsToSelect,
                PostComposerMediaData::media_data_comparator,
                static_cast<PostComposerMediaData *>(listData)));
        itemsToSelect = eina_list_remove(itemsToSelect, mediaData);
        elm_gengrid_item_selected_set(mediaData->GetGengridItem(), EINA_TRUE);
    }
}

void CameraRollScreen::PopulateMediaList() {
    ClearMediaDataList(mMediaList);

    char filterCondition[64];
    switch (mMode) {
    case eCAMERA_ROLL_POST_FROM_GROUP_MODE:
    case eCAMERA_ROLL_POST_COMPOSER_MODE:
    case eCAMERA_ROLL_POST_COMPOSER_MODE_FRIEND_TIMELINE:
    case eCAMERA_ROLL_POST_TO_FRIEND_TIMELINE_MODE:
    case eCAMERA_ROLL_CREATE_PHOTO_POST_MODE:
        if (mAlbum) {
            Utils::Snprintf_s(filterCondition, 64, MEDIA_TYPE" = %d", MEDIA_CONTENT_TYPE_IMAGE);  //albums don't accept videos
        } else {
            Utils::Snprintf_s(filterCondition, 64, MEDIA_TYPE " = %d OR " MEDIA_TYPE " = %d", MEDIA_CONTENT_TYPE_IMAGE,
                    MEDIA_CONTENT_TYPE_VIDEO);
        }
        break;
    case eCAMERA_ROLL_ADD_PHOTO_TO_ALBUM:
    case eCAMERA_ROLL_COMMENT_MODE:
    case eCAMERA_ROLL_ADD_PHOTO_FROM_USER_PHOTO_SCREEN:
        Utils::Snprintf_s(filterCondition, 64, MEDIA_TYPE" = %d", MEDIA_CONTENT_TYPE_IMAGE);
        break;
    default:
        break;
    }

    media_content_collation_e collation = MEDIA_CONTENT_COLLATE_DEFAULT;
    media_content_order_e orderType = MEDIA_CONTENT_ORDER_DESC;
    Utils::SelectMediaFilesFromMediaDB(filterCondition, &gallery_media_item_cb, &mMediaList, MEDIA_MODIFIED_TIME, &collation,
            &orderType);

    // Restore indexes if there are items selected
    if (eina_list_count(mMediaList) && SelectedMediaList::Count()) {
        Eina_List *selectedListItem;
        void *selectedListData;
        PostComposerMediaData *selectedMediaData;
        PostComposerMediaData *listMediaData;
        EINA_LIST_FOREACH(SelectedMediaList::Get(), selectedListItem, selectedListData) {
            selectedMediaData = static_cast<PostComposerMediaData *>(selectedListData);
            listMediaData = static_cast<PostComposerMediaData *>(eina_list_search_unsorted(mMediaList,
                    PostComposerMediaData::media_data_comparator,
                    selectedMediaData));

            if (listMediaData) {
                listMediaData->SetIndex(selectedMediaData->GetIndex());
                listMediaData->SetCaption(selectedMediaData->GetCaption());
            }
        }
    }
}

void CameraRollScreen::gengrid_refresh_selected_indexes_from(Evas_Object *gengrid, int index) {
    Eina_List *selectedMediaList = const_cast<Eina_List *>(elm_gengrid_selected_items_get(gengrid));
    Eina_List *list;
    void *listData;
    PostComposerMediaData *mediaData;

    EINA_LIST_FOREACH(eina_list_nth_list(selectedMediaList, index - 1), list, listData) {
        mediaData = static_cast<PostComposerMediaData *>(elm_object_item_data_get(static_cast<Elm_Object_Item *>(listData)));
        mediaData->SetIndex(index);

        Evas_Object *itemLayout = elm_object_item_part_content_get(mediaData->GetGengridItem(), "elm.swallow.icon");
        char indexStr[3];
        Utils::Snprintf_s(indexStr, 3, "%d", index);
        elm_object_part_text_set(itemLayout, "index", indexStr);

        ++index;
    }
}

void CameraRollScreen::CreateHeader()
{
    mHeader = WidgetFactory::CreateLayoutByGroup(mLayout, "camera_roll_header");
    elm_layout_content_set(mLayout, "header", mHeader);

    Evas_Object * commonHeader = WidgetFactory::CreateBlackHeaderWithLogo(mLayout, "IDS_CAMERA_ROLL",
            "IDS_UPPERCASE_DONE", this);
    elm_object_signal_emit(commonHeader, "hide_right_button", "header");
    elm_layout_content_set(mHeader, "header", commonHeader);

    // Create make new photo button
    Evas_Object *makePhotoBtn = elm_button_add(commonHeader);
    ELM_BUTTON_STYLE_SET1(makePhotoBtn, "make_new_photo");
    evas_object_smart_callback_add(makePhotoBtn, "clicked", on_make_new_photo_button_clicked_cb, this);

    switch (mMode) {
    case eCAMERA_ROLL_POST_TO_FRIEND_TIMELINE_MODE:
    case eCAMERA_ROLL_POST_FROM_GROUP_MODE:
    case eCAMERA_ROLL_POST_COMPOSER_MODE:
    case eCAMERA_ROLL_POST_COMPOSER_MODE_FRIEND_TIMELINE:
    case eCAMERA_ROLL_CREATE_PHOTO_POST_MODE: {
        if (mAlbum) {
            Log::debug_tag(LogTag, "CreateHeader(mode=%d, right=makePhoto)", mMode);
            elm_layout_content_set(mHeader, "right_button", makePhotoBtn);
        } else {
            Log::debug_tag(LogTag, "CreateHeader(mode=%d, left=makePhoto, right=makeVideo)", mMode);
            elm_layout_content_set(mHeader, "left_button", makePhotoBtn);

            // Create make new video button
            Evas_Object *makeVideoBtn = elm_button_add(commonHeader);
            ELM_BUTTON_STYLE_SET1(makeVideoBtn, "make_new_video");
            elm_layout_content_set(mHeader, "right_button", makeVideoBtn);
            evas_object_smart_callback_add(makeVideoBtn, "clicked", on_make_new_video_button_clicked_cb, this);
        }
    }
        break;
    case eCAMERA_ROLL_ADD_PHOTO_TO_ALBUM:
    case eCAMERA_ROLL_ADD_PHOTO_FROM_USER_PHOTO_SCREEN:
    case eCAMERA_ROLL_COMMENT_MODE: {
        elm_layout_content_set(mHeader, "right_button", makePhotoBtn);
    }
        break;
    default:
        break;
    }

    UpdateHeaderButtons();
}
void CameraRollScreen::UpdateHeaderButtons() {
    bool isNothingSelected = !elm_gengrid_selected_items_get(mGengrid);
    Evas_Object *commonHeader = elm_layout_content_get(mHeader, "header");
    elm_object_signal_emit(commonHeader, isNothingSelected ? "hide_right_button" : "show_right_button", "header"); // Done button
    elm_object_signal_emit(mHeader, isNothingSelected ? "show_left_button" : "hide_left_button", "camera_roll_header"); // Camera button
    elm_object_signal_emit(mHeader, isNothingSelected ? "show_right_button" : "hide_right_button", "camera_roll_header"); // Video button
}

bool CameraRollScreen::gallery_media_item_cb(media_info_h media, void *user_data)
{
    Eina_List **mediaList = static_cast<Eina_List **>(user_data);

    char * path = NULL;
    int ret = media_info_get_file_path(media, &path);
    if (!path || !(*path) || (ret != MEDIA_CONTENT_ERROR_NONE)) {
        return true;
    } else if (strstr(path, POST_COMPOSER_TEMP_DIR)) {  //a quick test
        char tempImagesPath[MAX_FULL_PATH_NAME_LENGTH];
        char *imageStoragePath = Utils::GetInternalStorageDirectory(STORAGE_DIRECTORY_IMAGES);
        Utils::Snprintf_s(tempImagesPath, MAX_FULL_PATH_NAME_LENGTH, "%s/%s", imageStoragePath, POST_COMPOSER_TEMP_DIR);
        free(imageStoragePath);

        if (strstr(path, tempImagesPath) == path) {  //a thorough test
            /* The image is either an intermediate result of cropping,
             * or it is not an image at all, but an orphan reference in the MediaDB. */
            return true;
        }
    }

    char * thumbnailPath = NULL;
    ret = media_info_get_thumbnail_path(media, &thumbnailPath);
    if (ret != MEDIA_CONTENT_ERROR_NONE) {
        free(path);
        return true;
    }

#ifdef FEATURE_ADD_TEXT
    Eina_List *listItem;
    void *listData;
    EINA_LIST_FOREACH(SelectedMediaList::Get(), listItem, listData) {
        PostComposerMediaData * mediaData = static_cast<PostComposerMediaData *>(listData);

        // Check if it's a already created photo with text in AddTextScreen
        if (strcmp(path, mediaData->GetOriginalPath())
                && mediaData->GetFilePath() != mediaData->GetOriginalPath()
                && !strcmp(path, mediaData->GetFilePath())) {
            free(path);
            return true;
        }
    }
#endif  /* FEATURE_ADD_TEXT */

    unsigned long long fileSize;
    ret = media_info_get_size(media, &fileSize);
    if (ret != MEDIA_CONTENT_ERROR_NONE) {
        free(path);
        return true;
    }

    media_content_type_e type;
    ret = media_info_get_media_type(media, &type);
    if (ret != MEDIA_CONTENT_ERROR_NONE) {
        free(path);
        return true;
    }

    char * mediaId = NULL;
    ret = media_info_get_media_id(media, &mediaId);
    if (ret != MEDIA_CONTENT_ERROR_NONE) {
        free(path);
        free(thumbnailPath);
        return true;
    }

    int width = 0;
    int height = 0;
    if ( type == MEDIA_CONTENT_TYPE_IMAGE ) {
        image_meta_h image;
        if ( media_info_get_image( media, &image ) == MEDIA_CONTENT_ERROR_NONE ) {
            if ( image_meta_get_height(image, &height) == MEDIA_CONTENT_ERROR_NONE && image_meta_get_width(image, &width) == MEDIA_CONTENT_ERROR_NONE ) {
                Log::debug_tag( LogTag, "image height is %d, image width is %d", height, width );
                if (width == 0 || height == 0) {
                    image_meta_destroy(image);
                    return true;
                }
            }
            image_meta_destroy(image);
        }
    } else if ( type == MEDIA_CONTENT_TYPE_VIDEO ) {
        video_meta_h meta;
        if ( media_info_get_video( media, &meta ) == MEDIA_CONTENT_ERROR_NONE ) {
            if ( video_meta_get_height( meta, &height ) == MEDIA_CONTENT_ERROR_NONE && video_meta_get_width( meta, &width ) == MEDIA_CONTENT_ERROR_NONE ) {
                Log::debug_tag( LogTag, "video height is %d, video width is %d", height, width );
                if (width == 0 || height == 0) {
                    video_meta_destroy(meta);
                    return true;
                }
            }
            video_meta_destroy( meta );
        }
    }
    Log::debug_tag(LogTag, "media folder: %s media_type: %d", path, type);
    PostComposerMediaData *mediaData = new PostComposerMediaData(path, thumbnailPath, type, mediaId, fileSize);
    mediaData->SetWidth(width);
    mediaData->SetHeight(height);
    *mediaList = eina_list_append(*mediaList, mediaData);

   return true;
}

void CameraRollScreen::CreateGengrid() {
    mGengrid = elm_gengrid_add(mLayout);
    elm_gengrid_highlight_mode_set(mGengrid, EINA_FALSE);
    elm_object_mirrored_automatic_set(mGengrid, EINA_FALSE);
    elm_object_mirrored_set(mGengrid, EINA_FALSE);
    evas_object_size_hint_weight_set(mGengrid, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mGengrid, EVAS_HINT_FILL, EVAS_HINT_EXPAND);
    elm_gengrid_item_size_set(mGengrid, R->CAMERA_ROLL_GENGRID_SIZE_W, R->CAMERA_ROLL_GENGRID_SIZE_H); //160x160 - Z1, 180x180 - Z3
    elm_gengrid_align_set(mGengrid, 0, 0);
    elm_gengrid_multi_select_set(mGengrid, IsMultiSelectMode(mMode));

    evas_object_smart_callback_add(mGengrid, "unselected", gengrid_content_unselected_cb, this);
    evas_object_smart_callback_add(mGengrid, "longpressed", gengrid_content_longpressed_cb, this);
    evas_object_smart_callback_add(mGengrid, "realized", gengrid_content_realized_cb, this);
    evas_object_smart_callback_add(mGengrid, "selected", gengrid_content_selected_cb, this);

    mGIC = elm_gengrid_item_class_new();
    mGIC->item_style = "default";
    mGIC->func.text_get = NULL;
    mGIC->func.content_get = gengrid_content_get_cb;
    mGIC->func.state_get = NULL;
    mGIC->func.del = NULL;

    elm_object_part_content_set(mLayout, "content", mGengrid);
}

bool CameraRollScreen::IsMultiSelectMode(CameraRollMode mode) {
    return mode == eCAMERA_ROLL_POST_COMPOSER_MODE
            || mode == eCAMERA_ROLL_POST_FROM_GROUP_MODE
            || mode == eCAMERA_ROLL_ADD_PHOTO_TO_ALBUM
            || mode == eCAMERA_ROLL_ADD_PHOTO_FROM_USER_PHOTO_SCREEN
            || mode == eCAMERA_ROLL_CREATE_PHOTO_POST_MODE;
}

Evas_Object* CameraRollScreen::gengrid_content_get_cb(void *data, Evas_Object *obj, const char *part) {
    PostComposerMediaData *mediaData = static_cast<PostComposerMediaData *>(data);

    if (!strcmp(part, "elm.swallow.icon")) {
        Evas_Object * itemLayout = WidgetFactory::CreateLayoutByGroup(obj, "camera_roll_gengrid_item");
        elm_object_mirrored_automatic_set(itemLayout, EINA_FALSE);
        elm_object_mirrored_set(itemLayout, EINA_FALSE);

        Evas * evas = evas_object_evas_get(obj);
        Evas_Object * imgEvas = evas_object_image_add(evas);
        elm_layout_content_set(itemLayout, "picture", imgEvas);
        evas_object_image_file_set(imgEvas, mediaData->GetOriginalThumbnailPath(), NULL);
        int imgW, imgH;
        evas_object_image_size_get(imgEvas, &imgW, &imgH);
        Utils::CropImageToFill(imgEvas, UIRes::GetInstance()->CAMERA_ROLL_GENGRID_SIZE_W_CROPPED, UIRes::GetInstance()->CAMERA_ROLL_GENGRID_SIZE_H_CROPPED, imgW, imgH);

        if (mediaData->GetType() == MEDIA_CONTENT_TYPE_VIDEO) {
            elm_object_signal_emit(itemLayout, "show_video_indicator", "camera_roll_screen");
        } else if (mediaData->GetType() == MEDIA_CONTENT_TYPE_IMAGE) {
            int width = mediaData->GetWidth();
            int height = mediaData->GetHeight();
            if (width && height) {
                int orientation = Utils::GetEXIFOrientation(mediaData->GetOriginalPath());
                float width_to_height_ratio = (orientation == Utils::ORIENTATION_ROTATE_90 ||
                                               orientation == Utils::ORIENTATION_ROTATE_270) ?
                                              (float)height / width :
                                              (float)width / height;
                if (width_to_height_ratio >= 3.5) {
                    elm_object_signal_emit(itemLayout, "show_panorama_indicator", "camera_roll_screen");
                }
            }
        }

        if (mediaData->IsSelected()) {
            elm_object_signal_emit(itemLayout, "show_frame", "camera_roll_screen");
            if (mediaData->GetType() == MEDIA_CONTENT_TYPE_VIDEO
                    || !elm_gengrid_multi_select_get(static_cast<Elm_Gengrid *>(obj))) {
                elm_object_signal_emit(itemLayout, "show_checkmark", "camera_roll_screen");
            } else {
                char indexStr[3];
                Utils::Snprintf_s(indexStr, 3, "%d", mediaData->GetIndex());
                elm_object_part_text_set(itemLayout, "index", indexStr);
                elm_object_signal_emit(itemLayout, "show_index", "camera_roll_screen");
            }
        }

        return itemLayout;
    }

    return NULL;
}

void CameraRollScreen::gengrid_content_selected_cb(void *data, Evas_Object *obj, void *event_info) {
    Log::debug_tag(LogTag, "CameraRollScreen::gengrid_content_selected_cb");

    CameraRollScreen *screen = static_cast<CameraRollScreen *>(data);
    Elm_Object_Item *selectedGengridItem = static_cast<Elm_Object_Item *>(event_info);
    PostComposerMediaData *mediaData = static_cast<PostComposerMediaData *>(elm_object_item_data_get(selectedGengridItem));

    if (screen->mIsLongPressed) {
        screen->HandleLongPressedState(selectedGengridItem);
        return;
    }

    // Use it to emit hide/show signals
    Evas_Object *itemLayout = elm_object_item_part_content_get(selectedGengridItem, "elm.swallow.icon");

    // Contains all selected items, including the recently selected one
    const Eina_List *selectedItems = elm_gengrid_selected_items_get(static_cast<Elm_Gengrid *>(obj));
    // Items count before the last selection
    uint wasSelectedItemsCount = eina_list_count(selectedItems) - 1;

    // As of now, multiple selection is allowed for images only, so we have to check and handle possible cases
    // as it's described below:
    // 1. Nothing is selected, then we allow to select either video or image file.
    // 2. At least one image is selected, then we allow selection for images only (up to CAMERA_ROLL_MAX_SELECTED_PHOTOS).
    // 3. Video is selected, then user can replace his choice by click on another video. Image selection is not allowed.
    if (elm_gengrid_multi_select_get(static_cast<Elm_Gengrid *>(obj))) {
        // Get MediaData for the previously selected item
        PostComposerMediaData *prevSelectedMediaData = nullptr;
        if (wasSelectedItemsCount == 1) {
            // Selected items comes in natural order, so the recently selected one locates at the tail
            Elm_Object_Item *prevSelectedGengridItem =
                    static_cast<Elm_Object_Item *>(eina_list_data_get(eina_list_prev(eina_list_last(selectedItems))));
            prevSelectedMediaData =
                    static_cast<PostComposerMediaData *>(elm_object_item_data_get(prevSelectedGengridItem));
        }

        if (mediaData->GetType() == MEDIA_CONTENT_TYPE_VIDEO) {
            if (mediaData->GetHeight() < MIN_VIDEO_HEIGHT) {
                notification_status_message_post(i18n_get_text("IDS_NOT_POSSIBLE_TO_SELECT_SMALL_VIDEO_HEIGHT"));
                elm_gengrid_item_selected_set(selectedGengridItem, EINA_FALSE);
                return;
            }

            // As of now multiple selection is allowed for images only. So if there was multiple items
            // selected then all of them are images. Recently selected item is video, so let's reject selection.
            if (wasSelectedItemsCount > 1) {
                notification_status_message_post(i18n_get_text("IDS_NOT_POSSIBLE_TO_SELECT_PHOTO_AND_VIDEO"));
                elm_gengrid_item_selected_set(selectedGengridItem, EINA_FALSE);
                return;
            } else if (wasSelectedItemsCount == 1) {
                // Photo and video selection at the same time is not currently supported.
                // So if "select" event is generated by click on a video and there is some image item
                // already selected, then the app shows error toast and ignore selection event.
                if (prevSelectedMediaData->GetType() == MEDIA_CONTENT_TYPE_IMAGE) {
                    notification_status_message_post(i18n_get_text("IDS_NOT_POSSIBLE_TO_SELECT_PHOTO_AND_VIDEO"));
                    elm_gengrid_item_selected_set(selectedGengridItem, EINA_FALSE);
                    return;
                }
                // Multiple video selection is not currently supported.
                // So if "select" event is generated by click on a video and there is some video item
                // already selected, then we have to remove existing selection and highlight newly selected item.
                elm_gengrid_item_selected_set(prevSelectedMediaData->GetGengridItem(), EINA_FALSE);
            }
        } else {
            if (wasSelectedItemsCount == 1) {
                // Photo and video selection at the same time is not currently supported.
                // So if "select" event is triggered by click on an image and there is some video item
                // already selected, then the app shows error toast and ignore selection event.
                if (prevSelectedMediaData->GetType() == MEDIA_CONTENT_TYPE_VIDEO) {
                    notification_status_message_post(i18n_get_text("IDS_NOT_POSSIBLE_TO_SELECT_PHOTO_AND_VIDEO"));
                    elm_gengrid_item_selected_set(selectedGengridItem, EINA_FALSE);
                    return;
                }
            } else if (wasSelectedItemsCount >= CAMERA_ROLL_MAX_SELECTED_PHOTOS) {
                char toastMessage[64];
                Utils::Snprintf_s(toastMessage, 64, i18n_get_text("IDS_MAY_NOT_SELECT_MORE_MAX_PHOTOS"), CAMERA_ROLL_MAX_SELECTED_PHOTOS);
                notification_status_message_post(toastMessage);
                elm_gengrid_item_selected_set(selectedGengridItem, EINA_FALSE);
                return;
            }
        }
    }

    // Update selected index
    mediaData->SetIndex(eina_list_count(selectedItems));

    if (!elm_gengrid_multi_select_get(static_cast<Elm_Gengrid *>(obj))
            || mediaData->GetType() == MEDIA_CONTENT_TYPE_VIDEO) {
        elm_object_signal_emit(itemLayout, "show_checkmark", "camera_roll_screen");
    } else {
        char indexStr[3];
        Utils::Snprintf_s(indexStr, 3, "%d", mediaData->GetIndex());
        elm_object_part_text_set(itemLayout, "index", indexStr);
        elm_object_signal_emit(itemLayout, "show_index", "camera_roll_screen");
    }
    elm_object_signal_emit(itemLayout, "show_frame", "camera_roll_screen");

    screen->UpdateHeaderButtons();
}

void CameraRollScreen::gengrid_content_unselected_cb(void *data, Evas_Object *obj, void *event_info) {
    Log::debug_tag(LogTag, "CameraRollScreen::gengrid_content_unselected_cb");
    CameraRollScreen *screen = static_cast<CameraRollScreen *>(data);
    Elm_Object_Item *gridItem = static_cast<Elm_Object_Item *>(event_info);
    PostComposerMediaData *mediaData = static_cast<PostComposerMediaData *>(elm_object_item_data_get(gridItem));

    if (screen->mIsLongPressed) {
        screen->HandleLongPressedState(gridItem);
        return;
    }

    int unselectedIndex = mediaData->GetIndex();

    // Reset selected index
    mediaData->SetIndex(0);

    // Refresh indexes for remaining selected items
    gengrid_refresh_selected_indexes_from(screen->mGengrid, unselectedIndex);

    // Remove custom highlight
    Evas_Object *itemLayout = elm_object_item_part_content_get(mediaData->GetGengridItem(), "elm.swallow.icon");
    elm_object_signal_emit(itemLayout, "hide_index", "camera_roll_screen");
    elm_object_signal_emit(itemLayout, "hide_checkmark", "camera_roll_screen");
    elm_object_signal_emit(itemLayout, "hide_frame", "camera_roll_screen");

    screen->UpdateHeaderButtons();
}

void CameraRollScreen::gengrid_content_longpressed_cb(void *data, Evas_Object *obj, void *event_info) {
    Log::debug_tag(LogTag, "CameraRollScreen::gengrid_content_longpressed_cb");
    CameraRollScreen *screen = static_cast<CameraRollScreen *>(data);
    Elm_Object_Item *gridItem = static_cast<Elm_Object_Item *>(event_info);
    PostComposerMediaData *mediaData = static_cast<PostComposerMediaData *>(elm_object_item_data_get(gridItem));

    screen->SetLongPressedState(true);

    if (mediaData->GetType() == MEDIA_CONTENT_TYPE_VIDEO) {
        VideoPlayerScreen *videoScreen = new VideoPlayerScreen(eVIDEO_PLAYER_CAMERA_ROLL_MODE, mediaData);
        Application::GetInstance()->AddScreen(videoScreen);
    }
    else {
        uint x, y;
        elm_gengrid_item_pos_get(gridItem, &x, &y);
        int itemsPerRow = R->SCREEN_SIZE_WIDTH / R->CAMERA_ROLL_GENGRID_SIZE_W;
        int itemIndex = itemsPerRow * y + x;

        ImagesCarouselScreen *imageCarouselScreen = new ImagesCarouselScreen(screen->mMediaList,
                itemIndex,
                ImagesCarouselScreen::eCAMERA_ROLL_MODE);
        Application::GetInstance()->AddScreen(imageCarouselScreen);
    }
}

void CameraRollScreen::gengrid_content_realized_cb(void *data, Evas_Object *obj, void *event_info) {
    Log::debug_tag(LogTag, "CameraRollScreen::gengrid_content_realized_cb");
    Elm_Object_Item *gridItem = static_cast<Elm_Object_Item *>(event_info);
    PostComposerMediaData *mediaData = static_cast<PostComposerMediaData *>(elm_object_item_data_get(gridItem));

    // Despite the fact that highlight mode is disabled for gengrid, each item in selected state gets
    // highlighted once it's realized. Let's emit the signal below to force remove item highlight,
    // in order to prevent this platform issue.
    if (mediaData->IsSelected()) {
        elm_object_item_signal_emit(gridItem, "elm,state,unselected", "elm");
    }
}

bool CameraRollScreen::HandleBackButton()
{
    CancelCallback();
    return true;
}

/**
 * @brief This method is part of workaround to handle long pressed event.
 * By default, Tizen OS has a weird behavior and triggers "select"/"unselect"
 * callbacks right after "longpressed" event happens, without an option to intercept
 * or cancel it. So this method is used in "select" & "unselect" callbacks to return
 * the original state for the longpressed item.
 *
 * @param[in] gengridItem The longpressed gengrid item object
 *
 * @see SetLongPressedState()
 */
void CameraRollScreen::HandleLongPressedState(Elm_Object_Item *gengridItem) {
    if (elm_gengrid_item_selected_get(gengridItem)) {
        // Long pressed item got selected, let's change the state back in implicit manner
        evas_object_smart_callback_del(mGengrid, "unselected", gengrid_content_unselected_cb);
        elm_gengrid_item_selected_set(gengridItem, EINA_FALSE);
        evas_object_smart_callback_add(mGengrid, "unselected", gengrid_content_unselected_cb, this);
    } else {
        // Long pressed item got unselected. In order to keep selected items on the same positions, we have to:
        // 1. Unselect all above items.
        // 2. Return selection for long pressed item.
        // 3. Select all the items unselected on step 1.
        PostComposerMediaData *mediaData = static_cast<PostComposerMediaData *>(elm_object_item_data_get(gengridItem));
        Eina_List *selectedMediaList = eina_list_clone(const_cast<Eina_List *>(elm_gengrid_selected_items_get(mGengrid)));
        Eina_List *list;
        void *listData;

        // Unselect all above items.
        evas_object_smart_callback_del(mGengrid, "unselected", gengrid_content_unselected_cb);
        EINA_LIST_FOREACH(eina_list_nth_list(selectedMediaList, mediaData->GetIndex() - 1), list, listData) {
            elm_gengrid_item_selected_set(static_cast<Elm_Object_Item *>(listData), EINA_FALSE);
        }
        evas_object_smart_callback_add(mGengrid, "unselected", gengrid_content_unselected_cb, this);

        evas_object_smart_callback_del(mGengrid, "selected", gengrid_content_selected_cb);
        // Return selection for long pressed item.
        elm_gengrid_item_selected_set(gengridItem, EINA_TRUE);
        // Select all the items previously unselected in this method.
        EINA_LIST_FOREACH(eina_list_nth_list(selectedMediaList, mediaData->GetIndex() - 1), list, listData) {
            elm_gengrid_item_selected_set(static_cast<Elm_Object_Item *>(listData), EINA_TRUE);
        }
        evas_object_smart_callback_add(mGengrid, "selected", gengrid_content_selected_cb, this);

        eina_list_free(selectedMediaList);
    }

    SetLongPressedState(false);
}

/**
 * @brief This method is part of workaround to handle long pressed event.
 * By default, Tizen OS has a weird behavior and triggers "select"/"unselect"
 * callbacks right after "longpressed" event happens, without an option to intercept
 * or cancel it. So this method is used to enable/disable "long pressed" state
 * and return the original state for the longpressed item once "select"/"unselect"
 * callbacks are triggered.
 *
 * @remark There are 2 possible selection modes in gengrid: multi- and single-selections.
 * In multi-selection mode gengrid triggers "select"/"unselect" (it depends on current state of item)
 * callback only once. In single-selection mode, up to two callbacks are triggered:
 * 1. Unselect for current item (if selected).
 * 2. Select for long pressed item.
 * To simplify logic to return the original state for all the items, let's
 * enable multi-selection mode along to "longpress" state and set it back
 * once handling is done.
 *
 * @param[in] enable True enables "long press" state, default is False
 *
 * @see HandleLongPressedState()
 */
void CameraRollScreen::SetLongPressedState(bool enable) {
    mIsLongPressed = enable;
    elm_gengrid_multi_select_set(mGengrid, enable ? EINA_TRUE : IsMultiSelectMode(mMode));
}

void CameraRollScreen::CancelCallback(void *data, Evas_Object *obj, void *event_info) {
    Pop();
}

void CameraRollScreen::ConfirmCallback(void *data, Evas_Object *obj, const char *emission, const char *source) {
    Eina_List *listItem;
    void *listData;

    // SelectedMediaList may contain temporary items such as modified images
    // in embedded editor (cropped or rotated copies). Those items don't not appear
    // in CameraRollScreen as they filtered out when list is populated.
    // These items will be deleted with the temporary folder once the post is published.
    // So once user confirms his selection, we have to check if there are modified
    // images in his selection and replace the original image with its modified copy
    // from SelectedMediaList, if presented.
    Eina_List *modifiedImages = nullptr;
    EINA_LIST_FOREACH(SelectedMediaList::Get(), listItem, listData) {
        PostComposerMediaData *mediaData = static_cast<PostComposerMediaData *>(listData);
        if (mediaData->GetTempCropImagePath()) {
            mediaData->AddRef();
            modifiedImages = eina_list_append(modifiedImages, mediaData);
        }
    }

    // Clear the selected items list
    SelectedMediaList::Clear();

    // Populate the final list with captured items
    EINA_LIST_FOREACH(mCapturedMediaList, listItem, listData) {
        PostComposerMediaData *mediaData = static_cast<PostComposerMediaData *>(listData);
        mediaData->AddRef();

        SelectedMediaList::AddItem(mediaData);
    }

    // Populate the final list with gengrid's selected items
    Elm_Object_Item *item;
    PostComposerMediaData *mediaData;
    EINA_LIST_FOREACH(const_cast<Eina_List *>(elm_gengrid_selected_items_get(mGengrid)), listItem, listData) {
        item = static_cast<Elm_Object_Item *>(listData);
        mediaData = static_cast<PostComposerMediaData *>(elm_object_item_data_get(item));

        PostComposerMediaData *modifiedImage = static_cast<PostComposerMediaData *>(eina_list_search_unsorted(modifiedImages,
                PostComposerMediaData::media_data_comparator,
                mediaData));

        if (modifiedImage) {
            SelectedMediaList::AddItem(modifiedImage);
        } else {
            mediaData->AddRef();
            SelectedMediaList::AddItem(mediaData);
        }
    }

    // Just for logging
    SelectedMediaList::Print();

    switch (mMode) {
    case eCAMERA_ROLL_ADD_PHOTO_TO_ALBUM: {
        // Create Post Composer screen
        PostComposerScreen* postComposer = new PostComposerScreen(NULL, eADDPHOTOSTOALBUM, NULL, NULL, NULL, mAlbum);
        // Replace current screen to PostComposerScreen because we don't need this screen anymore
        Application::GetInstance()->ReplaceScreen(postComposer);
    }
        break;
    case eCAMERA_ROLL_POST_COMPOSER_MODE:
    case eCAMERA_ROLL_POST_COMPOSER_MODE_FRIEND_TIMELINE: {
        Pop();
    }
        break;
    case eCAMERA_ROLL_CREATE_PHOTO_POST_MODE: {
        // Create Post Composer screen
        PostComposerScreen* postComposer = new PostComposerScreen(NULL, eDEFAULT, nullptr, nullptr, nullptr, nullptr, false);
        // Replace current screen to PostComposerScreen because we don't need this screen anymore
        Application::GetInstance()->ReplaceScreen(postComposer);
    }
        break;
    case eCAMERA_ROLL_ADD_PHOTO_FROM_USER_PHOTO_SCREEN: {
        PostComposerScreen* postComposer = new PostComposerScreen(NULL, eDEFAULT, nullptr, nullptr, nullptr, nullptr, false);
        // Replace current screen to PostComposerScreen because we don't need this screen anymore
        Application::GetInstance()->ReplaceScreen(postComposer);
        elm_naviframe_item_title_enabled_set(elm_naviframe_top_item_get(postComposer->getNf()), EINA_FALSE, EINA_FALSE);
    }
        break;
    case eCAMERA_ROLL_COMMENT_MODE: {
        if (mIUpdate) {
            mIUpdate->UpdateMedia();
            Pop();
        }
    }
        break;
    case eCAMERA_ROLL_POST_FROM_GROUP_MODE: {
        if (mIUpdate) {
            mIUpdate->UpdateMedia();
            Pop();
        }
        else {
            // Create Post Composer screen
            PostComposerScreen* postComposer = new PostComposerScreen(NULL, eADD_GROUP_POST, mUserGroup->GetId(),
                    mUserGroup->mName, mUserGroup->GetPrivacyAsStr());
            // Replace current screen to PostComposerScreen because we don't need this screen anymore
            Application::GetInstance()->ReplaceScreen(postComposer);
        }
        break;
    }
    case eCAMERA_ROLL_POST_TO_FRIEND_TIMELINE_MODE: {
        // Create Post Composer screen
        PostComposerScreen *screen = new PostComposerScreen(NULL, eFRIENDTIMELINE, mToUserId, mToUserName, NULL);
        Application::GetInstance()->ReplaceScreen(screen);
    }
    break;
    default:
        break;
    }
}

void CameraRollScreen::on_make_new_photo_button_clicked_cb(void *data, Evas_Object *obj, void *event_info) {
    app_control_h app_control;
    app_control_create(&app_control);
    app_control_set_operation(app_control, APP_CONTROL_OPERATION_CREATE_CONTENT);
    app_control_set_mime(app_control, "image/*");
    if (app_control_send_launch_request(app_control, app_control_reply_cb, data) != APP_CONTROL_ERROR_NONE) {
        CameraRollScreen *screen = static_cast<CameraRollScreen *>(data);
        Utils::ShowToast(screen->getMainLayout(), i18n_get_text("IDS_CAMERA_UNAVAILABLE"));
    }
    app_control_destroy(app_control);
}

void CameraRollScreen::on_make_new_video_button_clicked_cb(void *data, Evas_Object *obj, void *event_info) {
    app_control_h app_control;
    app_control_create(&app_control);
    app_control_set_operation(app_control, APP_CONTROL_OPERATION_CREATE_CONTENT);
    app_control_set_mime(app_control, "video/*");
    if (app_control_send_launch_request(app_control, app_control_reply_cb, data) != APP_CONTROL_ERROR_NONE) {
        CameraRollScreen *screen = static_cast<CameraRollScreen *>(data);
        Utils::ShowToast(screen->getMainLayout(), i18n_get_text("IDS_CAMERA_UNAVAILABLE"));
    }
    app_control_destroy(app_control);
}

void CameraRollScreen::app_control_reply_cb(app_control_h request, app_control_h reply, app_control_result_e result, void *userData) {
    if (ScreenBase::SID_CAMERA_ROLL != Application::GetInstance()->GetTopScreenId()) {
        return;
    }

    if (result == APP_CONTROL_RESULT_SUCCEEDED) {
        app_control_foreach_extra_data(reply, app_control_reply_data_cb, userData);

        CameraRollScreen *screen = static_cast<CameraRollScreen *>(userData);
        if (eina_list_count(screen->mCapturedMediaList)) {
            screen->ConfirmCallback();
        }
    }
}

bool CameraRollScreen::app_control_reply_data_cb(app_control_h app_control, const char *key, void *userData) {
    Log::debug_tag(LogTag, "CameraRollScreen::app_control_reply_data_cb");
    bool isArray;
    app_control_is_extra_data_array(app_control, key, &isArray);
    if (isArray) {
        char ** array;
        int arrayLength;
        app_control_get_extra_data_array(app_control, key, &array, &arrayLength);

        for (int i = 0; i < arrayLength; i++) {
            Log::debug_tag(LogTag, "  - key:%s - value:%s", key, array[i]);

            Eina_List *capturedMedia = nullptr;
            Utils::SelectMediaFileFromMediaDBByPath(array[i], &gallery_media_item_cb, &capturedMedia);
            // Exact file path is used to get the media, so there is maximum one item in that list
            if (eina_list_count(capturedMedia)) {
                CameraRollScreen *screen = static_cast<CameraRollScreen *>(userData);
                // Ideally is to set selection on gengrid item right here,
                // however it causes CameraRollScreen blinking on finishing, so let's just
                // collect captured items in separate list and add them to the SelectedMediaList in ConfirmChanges
                screen->mCapturedMediaList = eina_list_append(screen->mCapturedMediaList, eina_list_data_get(capturedMedia));
            }
        }
    } else {
        char * value;
        app_control_get_extra_data(app_control, key, &value);
        Log::debug_tag(LogTag, "  - key:%s - value:%s", key, value);
        free(value);
    }
    return true;
}

void CameraRollScreen::Update(AppEventId eventId, void * data) {
    switch (eventId) {
        case eCAMERA_ROLL_SELECT_EVENT: {
            SetLongPressedState(false);
            Elm_Object_Item * item = static_cast<Elm_Object_Item *>(data);
            elm_gengrid_item_selected_set(item, EINA_TRUE);
        }
        break;

        default: {
            // Do nothing
        }
    }
}

void CameraRollScreen::OnResume() {
    SetLongPressedState(false);
}
