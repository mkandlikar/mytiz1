#include "Application.h"
#include "CacheManager.h"
#include "Common.h"
#include "ConnectivityManager.h"
#include "HomeScreen.h"
#include "Log.h"
#include "Post.h"
#include "PostPresenter.h"
#include "Utils.h"
#include "VideoPlayerAction.h"
#include "VideoPlayerScreen.h"
#include "WidgetFactory.h"

#include <efl_util.h>
#include <fstream>

#define TIMER_UPD_PERIOD 0.1f
#define HIDE_PANELS_TIMEOUT 3000

const char *VideoPlayerScreen::mLastVideoId = nullptr;
int VideoPlayerScreen::mLastVideoPosition = 0;
Ecore_Timer * VideoPlayerScreen::mWaitDownloadTimer = nullptr;

VideoPlayerScreen::VideoPlayerScreen(VideoPlayerMode mode, void * inputData, const char* CacheFilePath) :
        ScreenBase(NULL), mPlaybackControl(NULL), mProgressBar(NULL), mWaitProgressBar(NULL),
        mFooter(NULL), mTimer(NULL), mExitTimer(NULL), mPlayingTimeCounter(-1),
        mVideoPlayerView(NULL), mPost(NULL), mState(EStateInitial), mPausedWhileUpdating(false),
        mMode(mode), mMediaData(NULL), mVideoActionData(NULL),
        mIsPlayedFirstTime(true) {
    mScreenId = ScreenBase::SID_VIDEO_PLAYER;
    mRotation = app_get_device_orientation();

    if (mMode == eVIDEO_PLAYER_FEED_MODE) {
        assert(inputData);
        mPost = static_cast<Post *>(static_cast<ActionAndData *>(inputData)->mData);
        mVideoActionData = new ActionAndData(mPost, new VideoPlayerAction(this));
        AppEvents::Get().Subscribe(eLIKE_COMMENT_EVENT, this);
    } else if (mMode == eVIDEO_PLAYER_CAMERA_ROLL_MODE
            || mMode == eVIDEO_PLAYER_POST_COMPOSER_MODE) {
        mMediaData = static_cast<PostComposerMediaData *>(inputData);
    }

    AppEvents::Get().Subscribe(ePAUSE_EVENT, this);
    AppEvents::Get().Subscribe(eRESUME_EVENT, this);
    AppEvents::Get().Subscribe(eVIDEOPLAYER_PAUSE_EVENT, this);
    AppEvents::Get().Subscribe(eVIDEOPLAYER_RESUME_EVENT, this);
    AppEvents::Get().Subscribe(eUNFOCUSED_EVENT, this);
    AppEvents::Get().Subscribe(eFOCUSED_EVENT, this);
    AppEvents::Get().Subscribe(ePOST_RELOAD_EVENT, this);

    //create layout
    MainLayoutAdd(getParentMainLayout());

    if (mMode == eVIDEO_PLAYER_FEED_MODE) {
        mVideoPlayerView = new VideoPlayerView(mLayout, this,
                (CacheFilePath ? CacheFilePath : mPost->GetSource()));
        if (GetPlayerLayout()) {
            evas_object_image_file_set(GetPlayerLayout(), nullptr, nullptr); //TODO: show the first frame while the video is being loaded
        }
    } else if (mMode == eVIDEO_PLAYER_CAMERA_ROLL_MODE
            || mMode == eVIDEO_PLAYER_POST_COMPOSER_MODE) {
        mVideoPlayerView = new VideoPlayerView(mLayout, this,
                mMediaData->GetOriginalPath());
        if (GetPlayerLayout() && mMediaData) {
            evas_object_image_file_set(GetPlayerLayout(), nullptr, nullptr);
        }
    }

    auto_rotation_setting_cb(AUTO_ROTATION_SETTING, this); // Get the current setting value.
    system_settings_set_changed_cb(AUTO_ROTATION_SETTING,
            auto_rotation_setting_cb, this);

    if (mLayout && GetPlayerLayout()) {
        elm_object_part_content_set(mLayout, "swallow.visualization",
                GetPlayerLayout());
        evas_object_show(GetPlayerLayout());
        if (mMode == eVIDEO_PLAYER_FEED_MODE) {
            WaitProgressBarShow();
        }
    }

    RegisterAppCbs();
    mTimer = ecore_timer_add(TIMER_UPD_PERIOD, OnTimerUpdCb, this);
    ecore_timer_freeze(mTimer);
    mExitTimer = ecore_timer_add(TIMER_UPD_PERIOD, OnExitTimerCb, this);
    ecore_timer_freeze(mExitTimer);

}

VideoPlayerScreen::~VideoPlayerScreen() {
    UnregisterAppCbs();

    system_settings_unset_changed_cb(AUTO_ROTATION_SETTING);

    if (mMode == eVIDEO_PLAYER_FEED_MODE) {
        mLastVideoPosition = mVideoPlayerView->GetPosition();
    }

    if (mExitTimer) {
        ecore_timer_del(mExitTimer);
    }
    if (mTimer) {
        ecore_timer_del(mTimer);
    }

    DeleteWaitDownloadTimer();

    StopPlayer();

    if (mVideoPlayerView) {
        delete mVideoPlayerView;
    }

    if (mVideoActionData) {
        delete mVideoActionData->mAction;
        delete mVideoActionData;
    }

    if (mLayout) {
        elm_object_signal_callback_del(mLayout, "mouse,clicked,*", "*",
                OnPlayerScreenClicked);
    }

    elm_win_wm_rotation_preferred_rotation_set(Application::GetInstance()->mWin, APP_DEVICE_ORIENTATION_0);
    WaitProgressBarHide();
}

void VideoPlayerScreen::DeleteWaitDownloadTimer() {
    if (mWaitDownloadTimer) {
        ecore_timer_del(mWaitDownloadTimer);
        mWaitDownloadTimer = nullptr;
    }
}

void VideoPlayerScreen::PlayerCompleted() {
    SetToInitialPosition();
    if (mMode == eVIDEO_PLAYER_FEED_MODE) {
        if (mTimer) {
            ecore_timer_del(mTimer);
            mTimer = nullptr;
        }
        if (mExitTimer) {
            ecore_timer_thaw(mExitTimer);
        }
    }
}

void VideoPlayerScreen::PlayerInterrupted() {
    Log::info("VideoPlayerScreen: PlayerInterrupted(), mMode=%d", mMode);
    if (mState == EStatePlaying) {
        // Pause the VideoPlayer, as the platform player has been just paused internally.
        if (mPlaybackControl) {
            Evas_Object *layout = elm_layout_edje_get(mPlaybackControl);
            Evas_Object *mainLayout = elm_layout_edje_get(mLayout);
            if (layout) {
                HidePanels(false);
                edje_object_signal_emit(mainLayout, "pause", "");
                edje_object_signal_emit(layout, "pause", "");
                ecore_timer_freeze(mTimer);
                mState = EStatePaused;
                Application::GetInstance()->SetScreenMode(EFL_UTIL_SCREEN_MODE_DEFAULT);
            }
        }
        UpdatePosition();
    }
}

Eina_Bool VideoPlayerScreen::OnExitTimerCb(void *data) {
    bool ret = ECORE_CALLBACK_CANCEL;
    if (Application::GetInstance()->GetTopScreenId()
            == ScreenBase::SID_VIDEO_PLAYER) {
        VideoPlayerScreen *me = static_cast<VideoPlayerScreen *>(data);
        if (me) {
            me->mExitTimer = NULL;
            me->Pop();
        }
    }
    return ret;
}

void VideoPlayerScreen::Push() {
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(elm_naviframe_top_item_get(getNf()),
            EINA_FALSE, EINA_FALSE);

    HidePanels(true);
}

void VideoPlayerScreen::ScreenClicked() {
    Evas_Object *layout = NULL;
    layout = elm_layout_edje_get(mLayout);

    if (layout) {
        switch (mState) {
        case EStatePlaying:
            edje_object_signal_emit(layout, "pause", "");
            PausePlayer();
            HidePanels(false);
            break;
        case EStatePaused:
            edje_object_signal_emit(layout, "play", "");
            mPausedWhileUpdating = false;
            StartPlayer();
            HidePanels(false);
            break;
        case EStateStopped:
        default:
            break;
        }
    }
}

void VideoPlayerScreen::OnPlayerScreenClicked(void *data, Evas_Object *obj,
        const char *emission, const char *source) {
    if (Application::GetInstance()->GetTopScreenId()
            == ScreenBase::SID_VIDEO_PLAYER) {
        VideoPlayerScreen *me = static_cast<VideoPlayerScreen *>(data);
        Log::info("VideoPlayerScreen: %s", source);
        if (source
                && (!strcmp(source, "bg_rect")
                        || !strcmp(source, "swallow.visualization"))) {
            me->ScreenClicked();
        }
    }
}

void VideoPlayerScreen::MainLayoutAdd(Evas_Object *parent) {
    if (parent) {
        mLayout = WidgetFactory::CreateLayoutByGroup(parent,
                "video_player_screen");

        if (mLayout) {
            if (mMode == eVIDEO_PLAYER_FEED_MODE) {
                elm_object_signal_emit(mLayout, "hide_header",
                        "video_player_screen");
                FooterAdd(mLayout);
            } else if (mMode == eVIDEO_PLAYER_CAMERA_ROLL_MODE
                    || mMode == eVIDEO_PLAYER_POST_COMPOSER_MODE) {
                HeaderAdd(mLayout);
            }
            elm_object_signal_callback_add(mLayout, "mouse,clicked,*", "*",
                    OnPlayerScreenClicked, this);
        }
    }
}

void VideoPlayerScreen::PlaybackControlAdd(Evas_Object *parent) {
    if (parent) {
        if (!mPlaybackControl) {
            mPlaybackControl = WidgetFactory::CreateLayoutByGroup(parent, "playback_controls");
            if (mPlaybackControl) {
                elm_object_mirrored_automatic_set(mPlaybackControl,EINA_FALSE);
                elm_object_mirrored_set(mPlaybackControl,EINA_FALSE);
                elm_object_part_content_set(parent, "swallow.playback_controls", mPlaybackControl);
                ProgressBarAdd(mPlaybackControl);
            }
        }
    }
}

void VideoPlayerScreen::HeaderAdd(Evas_Object *parent) {
    Evas_Object * header = NULL;
    switch (mMode) {
    case eVIDEO_PLAYER_CAMERA_ROLL_MODE:
        header = WidgetFactory::CreateHeaderWithBackBtnTextRightBtn(parent, false, "back_to_gallery",
                "IDS_PREVIEW", "IDS_UPPERCASE_SELECT", this);
        break;
    case eVIDEO_PLAYER_POST_COMPOSER_MODE:
        header = WidgetFactory::CreateBlackHeaderWithLogo(parent,
                "IDS_1_VIDEO_SELECTED",
                "IDS_UPPERCASE_DONE", this);
        break;
    default:
        return;
    }
    if (header) {
        elm_object_part_content_set(parent, "swallow.header", header);
    }
}

void VideoPlayerScreen::FooterAdd(Evas_Object *parent) {
    mFooter = WidgetFactory::CreateLayoutByGroup(parent, "video_player_footer");
    if (mFooter) {
        elm_object_part_content_set(parent, "swallow.footer", mFooter);

        elm_object_signal_callback_add(mFooter, "mouse,clicked,*",
                "image.like_button", ActionBase::on_like_btn_clicked_cb,
                mVideoActionData);
        elm_object_signal_callback_add(mFooter, "mouse,clicked,*",
                "image.comment_button", ActionBase::on_comment_btn_clicked_cb,
                mVideoActionData);
        elm_object_signal_callback_add(mFooter, "mouse,clicked,*",
                "image.share_button",
                (Edje_Signal_Cb) ActionBase::on_share_with_post_clicked,
                mVideoActionData);
        UpdateFooter(mVideoActionData);
    }
}

void VideoPlayerScreen::ProgressBarAdd(Evas_Object *parent) {
    mProgressBar = elm_slider_add(parent);
    if (mProgressBar) {
        elm_object_part_content_set(parent, "swallow.progressbar", mProgressBar);
        elm_slider_horizontal_set(mProgressBar, EINA_TRUE);
        evas_object_size_hint_weight_set(mProgressBar, EVAS_HINT_EXPAND, 0);
        evas_object_size_hint_align_set(mProgressBar, EVAS_HINT_FILL, 0);
        elm_slider_value_set(mProgressBar, 0.0);
        evas_object_smart_callback_add(mProgressBar, "slider,drag,start", OnSliderDragStartCb, this);
        evas_object_smart_callback_add(mProgressBar, "slider,drag,stop", OnSliderDragStopCb, this);
        evas_object_smart_callback_add(mProgressBar, "changed", OnSliderChangedCb, this);
        elm_object_disabled_set(mProgressBar, EINA_TRUE);
        evas_object_show(mProgressBar);
    }
}

void VideoPlayerScreen::WaitProgressBarShow() {
    if (!mWaitProgressBar) {
        mWaitProgressBar = elm_progressbar_add(mLayout);
        elm_object_style_set(mWaitProgressBar, "process_medium");
        evas_object_size_hint_align_set(mWaitProgressBar, EVAS_HINT_FILL,
                EVAS_HINT_FILL);
        evas_object_size_hint_weight_set(mWaitProgressBar, EVAS_HINT_EXPAND,
                EVAS_HINT_EXPAND);
        elm_progressbar_pulse_set(mWaitProgressBar, EINA_TRUE);
        elm_progressbar_pulse(mWaitProgressBar, EINA_TRUE);
        evas_object_resize(mWaitProgressBar,
                R->SCREEN_BASE_PROGRESS_BAR_ITEM_OBJ_RESIZE,
                R->SCREEN_BASE_PROGRESS_BAR_ITEM_OBJ_RESIZE);
        MoveWaitProgress(R->SCREEN_SIZE_WIDTH, R->SCREEN_SIZE_HEIGHT);
    }
    evas_object_raise(mWaitProgressBar);
    evas_object_show(mWaitProgressBar);
}

void VideoPlayerScreen::MoveWaitProgress(int width, int height) {
    evas_object_hide(mWaitProgressBar);
    evas_object_move(mWaitProgressBar, (width - R->SCREEN_BASE_PROGRESS_BAR_ITEM_OBJ_RESIZE) / 2,
                                       (height - R->SCREEN_BASE_PROGRESS_BAR_ITEM_OBJ_RESIZE) / 2);
    elm_progressbar_pulse(mWaitProgressBar, EINA_TRUE);
    evas_object_show(mWaitProgressBar);
}

void VideoPlayerScreen::WaitProgressBarHide() {
    if (mWaitProgressBar) {
        evas_object_lower(mWaitProgressBar);
        evas_object_hide(mWaitProgressBar);
        mWaitProgressBar = NULL;
    }
}

void VideoPlayerScreen::PlayerReady() {
    WaitProgressBarHide();
    PlaybackControlAdd(mLayout);
    if (mVideoPlayerView && StartPlayer()) {
        Log::info("VideoPlayerScreen::PlayerReady() - player is READY");
        HidePanels(false);
        SetRotation();
    } else {
        Log::error("VideoPlayerScreen::PlayerReady() - player is NOT ready!!!");
        if (mExitTimer) {
            ecore_timer_thaw(mExitTimer);
        }
    }
}

bool VideoPlayerScreen::StartPlayer() {
    Evas_Object *layout = NULL;

    if (!mPlaybackControl && mVideoPlayerView && mVideoPlayerView->IsPlayerReady()) {
        PlaybackControlAdd(mLayout);
    }

    if (mPlaybackControl && !mPausedWhileUpdating &&
        mVideoPlayerView && mVideoPlayerView->StartPlayer()) {
        SetPreviousPlayerPos();
        layout = elm_layout_edje_get(mPlaybackControl);
        if (layout) {
            if (mState != EStatePaused && mProgressBar) {
                //Get video duration in sec
                int ms = mVideoPlayerView->GetVideoDuration();
                int videoDuration_sec = ms / 1000;
                elm_object_disabled_set(mProgressBar, EINA_FALSE);
                elm_slider_min_max_set(mProgressBar, 0, videoDuration_sec);
                elm_slider_value_set(mProgressBar, 0.0);
            }

            edje_object_signal_emit(layout, "play", "");
            if (mTimer) {
                ecore_timer_thaw(mTimer);
            }
            mPlayingTimeCounter = 0;
            mState = EStatePlaying;
            Application::GetInstance()->SetScreenMode(EFL_UTIL_SCREEN_MODE_ALWAYS_ON);
            return true;
        }
    }
    return false;
}

bool VideoPlayerScreen::PausePlayer() {
    Evas_Object *layout = NULL;
    if (mPlaybackControl && mVideoPlayerView
            && mVideoPlayerView->PausePlayer()) {
        layout = elm_layout_edje_get(mPlaybackControl);
        if (layout) {
            edje_object_signal_emit(layout, "pause", "");
            ecore_timer_freeze(mTimer);
            mState = EStatePaused;
            Application::GetInstance()->SetScreenMode(EFL_UTIL_SCREEN_MODE_DEFAULT);
            return true;
        }
    }
    return false;
}

bool VideoPlayerScreen::StopPlayer() {
    if (mVideoPlayerView && mVideoPlayerView->StopPlayer()) {
        if (mProgressBar) {
            elm_slider_value_set(mProgressBar, 0.0);
            elm_object_disabled_set(mProgressBar, EINA_TRUE);
        }
        mState = EStateStopped;
        Application::GetInstance()->SetScreenMode(EFL_UTIL_SCREEN_MODE_DEFAULT);
        return true;
    }
    return false;
}

void VideoPlayerScreen::OnRotateCb(app_event_info_h event_info,
        void *user_data) {
    Log::info("VideoPlayerScreen::OnRotateCb()");
    VideoPlayerScreen *me = static_cast<VideoPlayerScreen *>(user_data);
    if (me
            && (Application::GetInstance()->GetTopScreenId()
                    == ScreenBase::SID_VIDEO_PLAYER)) {
        app_event_get_device_orientation(event_info, &me->mRotation);
        me->SetRotation();
    }
}

void VideoPlayerScreen::CalcVideoResolution(int *width, int *height) {
    if (height == 0 || width == 0) {
        return;
    }

    bool isVideoPortrait = mMode == eVIDEO_PLAYER_FEED_MODE ? height > width : mVideoPlayerView->IsVideoPortrait();
    if (mMode != eVIDEO_PLAYER_FEED_MODE && isVideoPortrait) {
        int tmp = *width;
        *width = *height;
        *height = tmp;
    }
}

void VideoPlayerScreen::SetRotation() {
    Log::info("VideoPlayerScreen::SetRotation()");
    int videoWidth = 0;
    int videoHeight = 0;
    bool isScreenPortrait = false;

    if (mVideoPlayerView) {
        if (mState == EStateInitial) {
            evas_object_image_size_get(GetPlayerLayout(), &videoWidth, &videoHeight);
        } else {
            videoWidth = mVideoPlayerView->GetVideoWidth();
            videoHeight = mVideoPlayerView->GetVideoHeight();
        }
        Log::info("VideoPlayerScreen::SetRotation() - dimensions=%d*%d", videoWidth, videoHeight);
        if (mLayout) {
            Evas_Object *rotationLayout = NULL;
            rotationLayout = elm_layout_edje_get(mLayout);
            if (rotationLayout) {
                CalcVideoResolution(&videoWidth, &videoHeight);
                Log::info("VideoPlayerScreen::SetRotation() - new dimensions=%d*%d, mRotation=%d", videoWidth, videoHeight, mRotation*90);
                switch (mRotation) {
                case APP_DEVICE_ORIENTATION_0:
                case APP_DEVICE_ORIENTATION_180:
                    if (videoWidth != 0 && videoHeight > videoWidth) {
                        edje_object_signal_emit(rotationLayout,"landscape", "");

                        // Move playback controls to the bottom of layout in case we do not have a footer
                        edje_object_signal_emit(rotationLayout,
                                mMode == eVIDEO_PLAYER_FEED_MODE ? "playback_controls_landscape_with_offset" : "playback_controls_landscape", "");
                        Log::info("VideoPlayerScreen::SetRotation(), Orientation 0 or 180, landscape");
                    } else {
                        edje_object_signal_emit(rotationLayout,"portrait", "");

                        // Move playback controls to the bottom of layout in case we do not have a footer
                        edje_object_signal_emit(rotationLayout,
                                mMode == eVIDEO_PLAYER_FEED_MODE ? "playback_controls_portrait_with_offset" : "playback_controls_portrait", "");
                        Log::info("VideoPlayerScreen::SetRotation(), Orientation 0 or 180, portrait");
                    }
                    isScreenPortrait = true;
                    break;
                case APP_DEVICE_ORIENTATION_270:
                case APP_DEVICE_ORIENTATION_90:
                    if (mAutoRotationEnabled) {
                        edje_object_signal_emit(rotationLayout,"landscape", "");

                        // Move playback controls to the bottom of layout in case we do not have a footer
                        edje_object_signal_emit(rotationLayout,
                                mMode == eVIDEO_PLAYER_FEED_MODE ? "playback_controls_landscape_with_offset" : "playback_controls_landscape", "");
                        Log::info("VideoPlayerScreen::SetRotation(), Orientation 90 or 270, landscape");
                    } else {
                        edje_object_signal_emit(rotationLayout,"portrait", "");

                        // Move playback controls to the bottom of layout in case we do not have a footer
                        edje_object_signal_emit(rotationLayout,
                                mMode == eVIDEO_PLAYER_FEED_MODE ? "playback_controls_portrait_with_offset" : "playback_controls_portrait", "");
                        Log::info("VideoPlayerScreen::SetRotation(), Orientation 90 or 270, portrait");
                    }
                    break;
                default:
                    break;
                }

                Evas_Object *rotationObj = edje_object_part_swallow_get(rotationLayout, "swallow.visualization");
                if (rotationObj) {
                    evas_object_size_hint_min_set(rotationObj, 1, 1);
                    evas_object_size_hint_aspect_set(rotationObj, EVAS_ASPECT_CONTROL_BOTH, videoWidth, videoHeight);
                }
                if ( mAutoRotationEnabled && (mRotation != APP_DEVICE_ORIENTATION_180)) {
                    if (isScreenPortrait) {
                        MoveWaitProgress(R->SCREEN_SIZE_WIDTH, R->SCREEN_SIZE_HEIGHT);
                    } else {
                        MoveWaitProgress(R->SCREEN_SIZE_HEIGHT, R->SCREEN_SIZE_WIDTH);
                    }
                    elm_win_wm_rotation_preferred_rotation_set(Application::GetInstance()->mWin, (int) mRotation);
                } else {
                    elm_win_wm_rotation_preferred_rotation_set(Application::GetInstance()->mWin, APP_DEVICE_ORIENTATION_0);
                }
            }
        }
    }
}

void VideoPlayerScreen::MsToMinSec(char *str, int ms) {
    int sec = ((ms + (1000 / 2)) / 1000);  //round to whole seconds
    int min = sec / 60;
    sec %= 60;
    Utils::Snprintf_s(str, 6, "%02d:%02d", min, sec);
}

void VideoPlayerScreen::UpdatePosition() {
    if (mVideoPlayerView) {
        int ms = mVideoPlayerView->GetPosition();

        //Update positiion of the Progress Bar:
        if (mProgressBar) {
            elm_slider_value_set(mProgressBar, (double)ms / 1000.0f);
        }

        if (mPlaybackControl) {
            char strTime[6];  //format "mm:ss"
            char strMinusTime[7] = "-";

            //Update Elapsed Time:
            MsToMinSec(strTime, ms);
            elm_object_part_text_set(mPlaybackControl, "elapsed_time_text", strTime);

            //Update Remaining Time:
            int dur = mVideoPlayerView->GetVideoDuration();
            dur = ((dur + (1000 / 2)) / 1000) * 1000;  //round till the nearest 1000 ms
            MsToMinSec(&strMinusTime[1], (dur >= ms ? (dur - ms) : 0));
            elm_object_part_text_set(mPlaybackControl, "remaining_time_text", strMinusTime);
        }
    }
}

Eina_Bool VideoPlayerScreen::OnTimerUpdCb(void *data) {
    VideoPlayerScreen *me = static_cast<VideoPlayerScreen *>(data);
    if (me) {
        me->UpdatePosition();
        me->UpdatePanelsVisibility();
        return ECORE_CALLBACK_RENEW;
    } else {
        return ECORE_CALLBACK_CANCEL;
    }
}

void VideoPlayerScreen::UpdatePanelsVisibility() {
    if (mPlayingTimeCounter != -1) {
        mPlayingTimeCounter += TIMER_UPD_PERIOD * 1000;
        if (HIDE_PANELS_TIMEOUT <= mPlayingTimeCounter) {
            HidePanels(true);
            mPlayingTimeCounter = -1;
        }
    }
}

void VideoPlayerScreen::OnSliderDragStartCb(void *data, Evas_Object *obj, void *event_info) {
    VideoPlayerScreen *me = static_cast<VideoPlayerScreen *>(data);
    if (me && (Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_VIDEO_PLAYER)
            && (Application::GetInstance()->GetTopScreen() == me) && me->mVideoPlayerView) {
        ecore_timer_freeze(me->mTimer);
    }
}



void VideoPlayerScreen::OnSliderDragStopCb(void *data, Evas_Object *obj, void *event_info) {
    VideoPlayerScreen *me = static_cast<VideoPlayerScreen *>(data);
    if (me && (Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_VIDEO_PLAYER)
            && (Application::GetInstance()->GetTopScreen() == me) && me->mVideoPlayerView) {
        int val = elm_slider_value_get(obj) * 1000.0f;
        me->mVideoPlayerView->SetPosition(val);
        me->UpdatePosition();
        me->mPlayingTimeCounter = 0;
        if (me->mState == EStatePlaying) {
            ecore_timer_thaw(me->mTimer);
        }
    }
}

void VideoPlayerScreen::OnSliderChangedCb(void *data, Evas_Object *obj, void *event_info) {
    VideoPlayerScreen *me = static_cast<VideoPlayerScreen *>(data);
    if (me && me->mVideoPlayerView) {
        int val = elm_slider_value_get(obj) * 1000.0f;
        me->mVideoPlayerView->SetPosition(val);
        me->UpdatePosition();
    }
}

void VideoPlayerScreen::Update(AppEventId eventId, void * data) {
    switch (eventId) {
    case eLIKE_COMMENT_EVENT:
        if (data) {
            if(mVideoActionData) {
                assert(mVideoActionData->mData);
                if(!strcmp(static_cast<char *>(data), mVideoActionData->mData->GetId())) {
                    UpdateFooter(mVideoActionData);
                }
            }
        }
        break;
    case ePOST_RELOAD_EVENT:
        if (data) {
            if(mVideoActionData) {
                assert(mVideoActionData->mData);
                Post *post = static_cast<Post*>(data);
                if (*mVideoActionData->mData == *post) {
                    mVideoActionData->ReSetData(post);
                    mPost = post;
                    UpdateFooter(mVideoActionData);
                }
            }
        }
        break;
    case eVIDEOPLAYER_PAUSE_EVENT:
    case eUNFOCUSED_EVENT:
    case ePAUSE_EVENT:
        // remember whether it was already paused prior to this Update call
        mPausedWhileUpdating = (mState == EStatePaused);
        PausePlayer();
        UnregisterAppCbs();
        //AAA We don't support auto-rotation.
//        if(mAutoRotationEnabled && (mRotation != APP_DEVICE_ORIENTATION_180)) {
//            elm_win_rotation_with_resize_set(Application::GetInstance()->mWin, (int)mPrevViewRotation);
//        } else {
//            elm_win_rotation_with_resize_set(Application::GetInstance()->mWin, APP_DEVICE_ORIENTATION_0);
//        }
        break;
    case eVIDEOPLAYER_RESUME_EVENT:
    case eFOCUSED_EVENT:
    case eRESUME_EVENT:
        if (Application::GetInstance()->GetTopScreenId()
                == ScreenBase::SID_VIDEO_PLAYER) {
            StartPlayer();
            mRotation = app_get_device_orientation();
//AAA We don't support auto-rotation.
//            if(mAutoRotationEnabled) {
//                Application::GetInstance()->SetScreenOrientationBoth();
//            }
            SetRotation();
            RegisterAppCbs();
        }
        break;
    default:
        break;
    }
}

bool VideoPlayerScreen::isCorrectVideoPlayerView(VideoPlayerView *videoPlayerView) {
    return (videoPlayerView == mVideoPlayerView);
}

void VideoPlayerScreen::UpdateFooter(ActionAndData *action_data) {
    if (mFooter && mPost) {
        if (mPost->GetCanLike()) {
            if (mPost->GetHasLiked()) {
                elm_object_signal_emit(mFooter, "select", "like_button");
            } else {
                elm_object_signal_emit(mFooter, "unselect", "like_button");
            }
        } else {
            elm_object_signal_emit(mFooter, "hide", "like_button");
        }

        if (mPost->GetCanComment()) {
            elm_object_signal_emit(mFooter, Application::IsRTLLanguage() ? "show,rtl" : "show,ltr", "comment_button");
        } else {
            elm_object_signal_emit(mFooter, "hide", "comment_button");
        }

        if (mPost->GetCanShare()) {
            elm_object_signal_emit(mFooter, Application::IsRTLLanguage() ? "show,rtl" : "show,ltr", "share_button");
        } else {
            elm_object_signal_emit(mFooter, "hide", "share_button");
        }

        if (action_data) {
            PostPresenter presenter(*mPost);
            if (mPost->GetLikesCount() > 0) {
                HRSTC_TRNSLTD_PART_TEXT_SET(mFooter, "likes_text", presenter.GetLikesCount().c_str());
                elm_object_signal_callback_add(mFooter, "mouse,clicked,*",
                        "likes_text", ActionBase::on_comment_btn_clicked_cb,
                        mVideoActionData);
            } else {
                elm_object_part_text_set(mFooter, "likes_text", "");
            }

            if (mPost->GetCommentsCount() > 0) {
                HRSTC_TRNSLTD_PART_TEXT_SET(mFooter, "comments_text", presenter.GetCommentsCount().c_str());
                elm_object_signal_callback_add(mFooter, "mouse,clicked,*",
                        "comments_text", ActionBase::on_comment_btn_clicked_cb,
                        mVideoActionData);
            } else {
                elm_object_part_text_set(mFooter, "comments_text", "");
            }
        }
    }
}

void VideoPlayerScreen::HidePanels(bool hide) {
    if (mLayout) {
        Log::info("VideoPlayerScreen::HidePanels() - %s panels", (hide ? "hide" : "show"));
        bool obj_visible;

        // Playback controls
        if (mPlaybackControl) {
            obj_visible = evas_object_visible_get(mPlaybackControl);
            Log::info("   playback controls visible: %d", obj_visible);
            if (hide && obj_visible) {
                Log::info("   HIDE PB Controls");
                evas_object_hide(mPlaybackControl);
            } else if (!hide && !obj_visible) {
                Log::info("   SHOW PB Controls");
                evas_object_show(mPlaybackControl);
            }
        }

        //Footer
        if (mFooter) {
            obj_visible = evas_object_visible_get(mFooter);
            Log::info("   footer visible: %d", obj_visible);
            if (hide && obj_visible) {
                Log::info("   HIDE Footer");
                evas_object_hide(mFooter);
            } else if (!hide && !obj_visible) {
                Log::info("   SHOW Footer");
                evas_object_show(mFooter);
            }
        }
    }
}

void VideoPlayerScreen::RegisterAppCbs() {
    ui_app_add_event_handler(
            &mEvent_handlers[APP_EVENT_DEVICE_ORIENTATION_CHANGED],
            APP_EVENT_DEVICE_ORIENTATION_CHANGED, OnRotateCb, this);
}

void VideoPlayerScreen::UnregisterAppCbs() {
    ui_app_remove_event_handler(
            mEvent_handlers[APP_EVENT_DEVICE_ORIENTATION_CHANGED]);
}

Evas_Object *VideoPlayerScreen::GetPlayerLayout() {
    Evas_Object *ret = NULL;
    if (mVideoPlayerView) {
        ret = mVideoPlayerView->GetPlayerLayout();
    }
    return ret;
}

void VideoPlayerScreen::Launch(void* data) {
    Log::info("VideoPlayerScreen: Launch()");
    if (!data ||
        (Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_VIDEO_PLAYER)) {  // prevent second launch
        return;
    }

    char* AppSharePath = app_get_shared_trusted_path();
    if (!AppSharePath) {
        return;
    }

    ActionAndData* postActionData = static_cast<ActionAndData*>(data);
    Post* postData = static_cast<Post*>(postActionData->mData);
    if (!postData) {
        return;
    }
    char *localPath = Utils::GetIconNameFromUrl(postData->GetSource());
    if (!localPath) {
        return;
    }

    std::string CacheFilePath = AppSharePath;
    CacheFilePath.append(DIR_VIDEOS);
    CacheFilePath.append("/");
    CacheFilePath.append(localPath);
    free(localPath);
    free(AppSharePath);

    std::ifstream VideoCacheFileStream(CacheFilePath.c_str());
    if (VideoCacheFileStream.good()) {
        VideoPlayerScreen* screen = new VideoPlayerScreen(eVIDEO_PLAYER_FEED_MODE, data, CacheFilePath.c_str());
        Application::GetInstance()->AddScreen(screen);
    } else {
        if (ConnectivityManager::Singleton().IsConnected()) {
            postActionData->DownloadVideo(postData->GetSource());
            if (!mWaitDownloadTimer) {
                mWaitDownloadTimer = ecore_timer_add(1.0, OnDownloadTimerCb, data);
            }
        } else {
            WidgetFactory::ShowAlertPopup("IDS_VIDEO_MSG_NO_INTERNET", "IDS_VIDEO_TITLE_CANNOT_PLAY");
        }
    }
}

Eina_Bool VideoPlayerScreen::OnDownloadTimerCb(void *data)
{
    mWaitDownloadTimer = nullptr;
    VideoPlayerScreen* screen = new VideoPlayerScreen(eVIDEO_PLAYER_FEED_MODE, data, nullptr);
    Application::GetInstance()->AddScreen(screen);
    return ECORE_CALLBACK_CANCEL;
}

void VideoPlayerScreen::ConfirmCallback(void *data, Evas_Object *obj,
        const char *emission, const char *source) {
    Log::info("VideoPlayerScreen: ConfirmCallback()");

    if (mMode == eVIDEO_PLAYER_CAMERA_ROLL_MODE && !mMediaData->IsSelected()) {
        AppEvents::Get().Notify(eCAMERA_ROLL_SELECT_EVENT,
                mMediaData->GetGengridItem());
    }
    if (Application::GetInstance()->GetTopScreenId()
            == ScreenBase::SID_VIDEO_PLAYER)
        Pop();
}

void VideoPlayerScreen::auto_rotation_setting_cb(system_settings_key_e sys_key,
        void *user_data) {
    VideoPlayerScreen *me = static_cast<VideoPlayerScreen *>(user_data);
    bool value;
    int ret;
    ret = system_settings_get_value_bool(sys_key, &value);
    if (ret == SYSTEM_SETTINGS_ERROR_NONE) {
        me->mAutoRotationEnabled = value;
    }
}

void VideoPlayerScreen::SetPreviousPlayerPos() {
    if (mMode == eVIDEO_PLAYER_FEED_MODE && mIsPlayedFirstTime) {
        mIsPlayedFirstTime = false;
        if (mPost->GetAttachmentTargetId() && mLastVideoId
                && !strcmp(mPost->GetAttachmentTargetId(), mLastVideoId)) {
            mVideoPlayerView->SetPosition(mLastVideoPosition);
        } else {
            free((void*) mLastVideoId);
            mLastVideoId = SAFE_STRDUP(mPost->GetAttachmentTargetId());
        }
    }
}

void VideoPlayerScreen::SetToInitialPosition() {
    // Display the starting frame of video as in FB4A
    if (PausePlayer() && mVideoPlayerView) {
        mVideoPlayerView->SetPosition(0); // Set player position to starting position
        if (mProgressBar) {
            UpdatePosition();
            elm_slider_value_set(mProgressBar, 0.0);
        }
    }
}
