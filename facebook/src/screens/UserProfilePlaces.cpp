#include "Common.h"
#include "Config.h"
#include "dlog.h"
#include "FacebookSession.h"
#include "FbResponseAboutPlaces.h"
#include "jsonutilities.h"
#include "UserProfilePlaces.h"
#include "UserProfilePlacesScreen.h"
#include "Utils.h"

#include <string>
#include <sstream>
#include <system_info.h>

#include <json-glib/json-glib.h>


UserProfilePlaces::UserProfilePlaces(Evas_Object *main_layout, ScreenBase *sc_parent, const char *profileId){

    mPlaceRequestData = NULL;
    mProfileId = profileId ? strdup(profileId) : NULL;
    mRequestLayout = elm_layout_add(main_layout);
    evas_object_size_hint_weight_set(mRequestLayout, 1, 1);
    evas_object_size_hint_align_set(mRequestLayout, 0, 0);
    elm_layout_file_set(mRequestLayout,  Application::mEdjPath, "user_profile_about_places_group");
    evas_object_show(mRequestLayout);

    elm_box_pack_end(main_layout, mRequestLayout);

    elm_object_signal_callback_add(mRequestLayout, "header_rect,click", "header_rect",
        (Edje_Signal_Cb)places_more_btn_clicked_cb, this);

    elm_object_translatable_part_text_set(mRequestLayout, "header_rect_title", "PLACES");  //TODO: rework string ID

    mProgressBar = elm_progressbar_add(mRequestLayout);
    elm_object_style_set(mProgressBar, "process_medium");
    elm_object_part_content_set(mRequestLayout, "rq_list", mProgressBar);
    elm_progressbar_pulse_set(mProgressBar, EINA_TRUE);
    elm_progressbar_pulse(mProgressBar, EINA_TRUE);
    evas_object_show(mProgressBar);

	LoadData();
}

UserProfilePlaces::~UserProfilePlaces() {
	free(mProfileId);
	FacebookSession::GetInstance()->ReleaseGraphRequest(mPlaceRequestData);
}

void UserProfilePlaces::LoadData() {
	mPlaceRequestData = FacebookSession::GetInstance()->GetUPPlacesOverview(mProfileId, places_response_cb, this);
}

void UserProfilePlaces::places_response_cb(void* Object, char* response, int CurlCode) {

    UserProfilePlaces* self = static_cast<UserProfilePlaces*>(Object);

    self->placesResponse = FbResponseAboutPlaces::createFromJson(response);
    if(self->placesResponse)
    	self->setPlacesData();
}

void UserProfilePlaces::setPlacesData()
{
	evas_object_hide(mProgressBar);

	mRequestList = elm_genlist_add(mRequestLayout);
	elm_genlist_select_mode_set(mRequestList, ELM_OBJECT_SELECT_MODE_NONE);
	elm_scroller_policy_set(mRequestList,ELM_SCROLLER_POLICY_OFF,ELM_SCROLLER_POLICY_OFF);
	elm_object_part_content_set(mRequestLayout, "rq_list", mRequestList);
	evas_object_show(mRequestList);

    Elm_Genlist_Item_Class *itc2 = elm_genlist_item_class_new();
    itc2->item_style = "full";
    itc2->homogeneous = EINA_FALSE;
    itc2->func.content_get = get_places_content;
    itc2->func.state_get = NULL;
    itc2->func.del = NULL;

    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(placesResponse->mListofPlaces, l, listData)
    {
        elm_genlist_item_append(mRequestList, itc2, listData, NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);
    }
    elm_genlist_item_class_free(itc2);
}

Evas_Object *UserProfilePlaces::get_places_content(void *user_data, Evas_Object *obj, const char *part)
{
    FbResponseAboutPlaces::PlacesList *places_data = (FbResponseAboutPlaces::PlacesList *)(user_data);

    Evas_Object *mFrPplComposer = elm_layout_add(obj);
    evas_object_size_hint_weight_set(mFrPplComposer, 1, 1);
    evas_object_size_hint_min_set(mFrPplComposer, 640, 144);  //TODO: define constants
    evas_object_size_hint_max_set(mFrPplComposer, 640, 144);
    elm_layout_file_set(mFrPplComposer,  Application::mEdjPath, "about_places_composer");
    evas_object_show(mFrPplComposer);

    Evas_Object *avatar = elm_image_add(mFrPplComposer);
    elm_image_file_set(avatar, places_data->mPicSquareWithLogo, NULL);
    elm_object_part_content_set(mFrPplComposer, "item_photo", avatar);
    evas_object_show(avatar);

    elm_object_part_text_set(mFrPplComposer, "item_text_line_1", places_data->place_name);

    int size = strlen(places_data->city_name) + strlen(places_data->country) + 2;
    char *temp_buf_line_2 = new char[size];
    Utils::Snprintf_s(temp_buf_line_2, size, "%s,%s", places_data->city_name , places_data->country);
    elm_object_part_text_set(mFrPplComposer, "item_text_line_2", temp_buf_line_2);
    elm_object_part_text_set(mFrPplComposer, "item_text_line_3", places_data->created_time);

    delete []temp_buf_line_2;
    return mFrPplComposer;
}

void UserProfilePlaces :: places_more_btn_clicked_cb(void *data , Evas_Object *obj, const char *emission, const char *source)
{
    UserProfilePlaces* self = static_cast<UserProfilePlaces*>(data);
    ScreenBase *newScreen = (ScreenBase *) new UserProfilePlacesScreen(self->mProfileId);
    Application::GetInstance()->AddScreen(newScreen);
    elm_naviframe_item_title_enabled_set(elm_naviframe_top_item_get(newScreen->getNf()), EINA_FALSE, EINA_FALSE);
}


