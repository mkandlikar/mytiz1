#include "FacebookSession.h"
#include "FbResponseImageDetails.h"
#include "ImagesCarouselAction.h"
#include "ImagesCarouselScreen.h"
#include "ImagePostScreen.h"
#include "Log.h"
#include "PhotoPresenter.h"
#include "PostComposerMediaData.h"
#include "PresenterHelpers.h"
#include "Utils.h"
#include "WidgetFactory.h"
#include "EditPhotoTagScreen.h"

#include <cassert>
/*
 * Class ImagesCarouselScreen
 */
const int ImagesCarouselScreen::MESSAGE_LENGTH_TO_SCROLL = 3000;

ImagesCarouselScreen::ImagesCarouselScreen(Eina_List * inputDataList, int defaultPage, ImagesCarouselMode mode, ImagePostScreen *postScreen, ActionAndData * parentActionAndData) :
                ScreenBase(NULL),
                mTagging(nullptr),
                mCarousel(NULL),
                mItems(NULL),
                mPostScreen(postScreen),
                mImagesCarouselAction(new ImagesCarouselAction(this)),
                mImagesCarouselActionAndData(NULL),
                mPost(NULL),
                mMode(mode),
                mHeader(NULL),
                mParentActionAndData(parentActionAndData),
                mRequests(NULL),
                mIsSaveButtonEnabled(false),
                mIsClearButtonEnabled(false),
                mIsShowTagButton(false)
{
    mImageRegion = new ImageRegion();
    mScreenId = ScreenBase::SID_IMAGE_CAROUSEL;
    Photo * defaultPhoto = NULL;
    int i = 0;
    Eina_List * dataList = NULL;
    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(inputDataList, l, listData) {
        switch (mMode) {
        case eACTION_AND_DATA_MODE: {
            ActionAndData * parentActionAndData = static_cast<ActionAndData *> (listData);
            dataList = eina_list_append(dataList, listData);
            if (defaultPage == i) {
                defaultPhoto = static_cast<Photo *>(parentActionAndData->mData);
                if (defaultPhoto && defaultPhoto->GetFromId()) {
                    mFromId = SAFE_STRDUP(defaultPhoto->GetFromId());
                }
            }
        }
        break;
        case ePOST_COMPOSER_MODE:
        case eDATA_MODE: {
            dataList = eina_list_append(dataList, listData);
            if (defaultPage == i) {
                defaultPhoto = static_cast<Photo *>(listData);
            }
        }
        break;
        case eCAMERA_ROLL_MODE: {
            PostComposerMediaData * mediaData = static_cast<PostComposerMediaData *>(listData);
            if (mediaData->GetType() == MEDIA_CONTENT_TYPE_IMAGE) {
                dataList = eina_list_append(dataList, listData);
            }

            if (i == defaultPage) {
                // Index correction is required as DataList contains meidaDatas for both image and video.
                // Original value is replaced by only images count once we reach selected page.
                defaultPage = eina_list_count(dataList) - 1;
            }
            break;
        }
        default:
            break;
        }
        i++;
    }
    if (mMode == eDATA_MODE) {
        if (mParentActionAndData && mParentActionAndData->mData) { //this is only haapens when a post with single photo is opened.
            mPost = static_cast<Post *>(mParentActionAndData->mData);
            mImagesCarouselActionAndData = new ActionAndData(mPost, mImagesCarouselAction);
            mFromId = SAFE_STRDUP(mPost->GetFromId().c_str());
            RequestOneImageDetails(defaultPhoto->GetId());
            AppEvents::Get().Subscribe(ePOST_RELOAD_EVENT, this);
        } else {
            AppEvents::Get().Subscribe(ePHOTO_COMMENTS_LIST_WAS_MODIFIED, this);
        }
    }
    if (dataList) {
        CreateActionAndDatesFromData(dataList);
        CreateLayout(defaultPage, defaultPhoto);
        if (mMode == eACTION_AND_DATA_MODE) {
            AppEvents::Get().Subscribe(eLIKE_COMMENT_EVENT, this);
            AppEvents::Get().Subscribe(ePHOTO_COMMENTS_LIST_WAS_MODIFIED, this);
        } else if (mMode == eDATA_MODE) {
            AppEvents::Get().Subscribe(eLIKE_COMMENT_EVENT, this);
        }
    }
#ifdef PHOTO_TAGGING
    AppEvents::Get().Subscribe(eLANGUAGE_CHANGED, this);
#endif
}
/**
 * @brief Constructor for one Photo
 */
ImagesCarouselScreen::ImagesCarouselScreen(Photo * photo, ImagesCarouselMode mode) :
        ScreenBase(NULL),
        mTagging(nullptr),
        mCarousel(NULL),
        mItems(NULL),
        mPostScreen(NULL),
        mImagesCarouselAction(new ImagesCarouselAction(this)),
        mImagesCarouselActionAndData(NULL),
        mPost(NULL),
        mMode(mode),
        mHeader(NULL),
        mParentActionAndData(NULL),
        mRequests(NULL),
        mIsSaveButtonEnabled(false),
        mIsClearButtonEnabled(false),
        mIsShowTagButton(false)
{
    mImageRegion = new ImageRegion();
    mScreenId = ScreenBase::SID_IMAGE_CAROUSEL;
    Eina_List * dataList = NULL;
    switch (mMode) {
    case eWITHOUT_UI_MODE: {
        dataList = eina_list_append(dataList, photo);
        if (dataList) {
            CreateActionAndDatesFromData(dataList);
            CreateLayout(0, photo);
        }
    }
        break;
    default:
        break;
    }
#ifdef PHOTO_TAGGING
    AppEvents::Get().Subscribe(eLANGUAGE_CHANGED, this);
#endif
}
ImagesCarouselScreen::ImagesCarouselScreen(const char *file_path, const char *id, ImagesCarouselMode mode) :
       ScreenBase(NULL),
       mTagging(nullptr),
       mCarousel(NULL),
       mItems(NULL),
       mPostScreen(NULL),
       mImagesCarouselAction(new ImagesCarouselAction(this)),
       mImagesCarouselActionAndData(NULL),
       mPost(NULL),
       mMode(mode),
       mHeader(NULL),
       mParentActionAndData(NULL),
       mRequests(NULL),
       mIsSaveButtonEnabled(false),
       mIsClearButtonEnabled(false),
       mIsShowTagButton(false)
{
    mImageRegion = new ImageRegion();
    mScreenId = ScreenBase::SID_IMAGE_CAROUSEL;
    Photo *coverPhoto = NULL;
    switch (mMode) {
    case ePROFILE_COVER_MODE:
        if (file_path && id) {
            coverPhoto = new Photo();
            coverPhoto->SetFilePath(file_path);
            coverPhoto->SetId(SAFE_STRDUP(id));
        }
        if (coverPhoto) {
            Eina_List * dataList = NULL;
            dataList = eina_list_append(dataList, coverPhoto);
            CreateActionAndDatesFromData(dataList);
            CreateLayout(0, NULL);
            ProgressBarShow();
            RequestOneImageDetails(coverPhoto->GetId());
            AppEvents::Get().Subscribe(eLIKE_COMMENT_EVENT, this); //Like comment event handling for cover photo
            AppEvents::Get().Subscribe(ePHOTO_COMMENTS_LIST_WAS_MODIFIED, this);
        }
        break;
    default:
        break;
    }
#ifdef PHOTO_TAGGING
    AppEvents::Get().Subscribe(eLANGUAGE_CHANGED, this);
#endif
}
ImagesCarouselScreen::~ImagesCarouselScreen() {
    if (mPostScreen && mCarousel && mMode == eACTION_AND_DATA_MODE) {
        mPostScreen->ShowWidget(mCarousel->GetCurrentPage());
    }
    delete mCarousel;
    // Free memory occupied by GraphRequest list for image details
    void *requestData = nullptr;
    EINA_LIST_FREE(mRequests, requestData) {
        GraphRequest *req = static_cast< GraphRequest*> (requestData);
        FacebookSession::GetInstance()->ReleaseGraphRequest(req);
    }
    FacebookSession::GetInstance()->ReleaseGraphRequest(mDeleteRequest);
    void *data = nullptr;
    EINA_LIST_FREE(mItems, data) {
        delete static_cast<ActionAndData*>(data);
    }
    delete mImagesCarouselAction;
    delete mImagesCarouselActionAndData;
    CloseEditCaption();
    delete mConfirmationDialog;
    free(mFromId);
    delete [] mMoreMenuItems;
    delete mImageRegion;
    RemoveTags();
}
void ImagesCarouselScreen::Push() {
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(elm_naviframe_top_item_get(getNf()), EINA_FALSE, EINA_FALSE);
}
void ImagesCarouselScreen::CloseEditCaption() {
    if (mEditCaptionLayout) {
        evas_object_del(mEditCaptionLayout);
        mEditCaptionLayout = nullptr;
        elm_object_signal_callback_add(mLayout, "mouse,clicked,*", "swallow.scroller", on_scroller_clicked, this);
        ShowImageControls();
        mCarousel->StopScroll(false);
        mCarousel->EnableZoom(true);
        delete mTagging;
    }
    evas_object_del(mEditCaptionPopup);
    mEditCaptionPopup = nullptr;
    mIsSaveButtonEnabled = false;
    mIsClearButtonEnabled = false;
}
bool ImagesCarouselScreen::HandleBackButton() {
    bool ret = true;
    if (mIsShowTagButton && !RemoveTags()) {
        ret = false;
    } else if (PopupMenu::Close()) {
    } else if (mConfirmationDialog) {
        DeleteConfirmationDialog();
    } else if (mEditCaptionLayout) {
        if (IsCaptionChanged()) {
            ShowKeepDiscardPopup();
        } else {
            CloseEditCaption();
        }
    } else if (!mDeleteRequest) {
        ret = false;
    }
    Log::info("ImagesCarouselScreen::HandleBackButton");
    return ret;
}
void ImagesCarouselScreen::on_get_image_details_completed(void *data, char* str, int code)
{
    ImagesCarouselScreen *me = static_cast<ImagesCarouselScreen*>(data);
    if (me) {
        if (code == CURLE_OK) {
            Log::debug( "data: %s", str);
            FbResponseImageDetails *imageDetails = FbResponseImageDetails::createFromJson(str);
            if (!imageDetails || !imageDetails->mPhoto) {
                Log::error("Error parsing http response");
            } else if (imageDetails->mError && imageDetails->mError->mMessage) {
                Log::error("Error getting image details, error = %s", imageDetails->mError->mMessage);
            } else {
                Photo* updatedPhoto = static_cast<Photo*>(imageDetails->mPhoto);
                Eina_List *l;
                void *listData;
                EINA_LIST_FOREACH(me->mItems, l, listData) {
                    assert(listData);
                    ActionAndData *actionAndData = static_cast<ActionAndData *> (listData);
                    assert(actionAndData->mData);
                    Photo *photo = static_cast<Photo *> (actionAndData->mData);
                    if(!strcmp(photo->GetId(),updatedPhoto->GetId())) {

                        if (me->mMode == ePROFILE_COVER_MODE) {
                            me->mFromId = SAFE_STRDUP(updatedPhoto->GetFromId());
                            me->ProgressBarHide();
                            me->CreateFooter(nullptr);// to do not update photo automatically
                        }
                        photo->UpdatePhotoDetails(static_cast<Photo*>(imageDetails->mPhoto));

                        if(actionAndData == me->GetActionAndDataAtCurrentPage()) {
                            me->UpdateUI(actionAndData);//update only for active current page
                        }
                        break;
                    }
                }
            }
            delete imageDetails;
        } else {
            Log::error("ImagesCarouselScreen::on_get_image_details_completed->Error sending http request");
        }
        free(str);
    }
}
void ImagesCarouselScreen::CreateLayout(int defaultPage, Photo * defaultPhoto) {
    mLayout = WidgetFactory::CreateLayoutByGroup(getParentMainLayout(), "images_carousel_screen");
    Log::info("ImagesCarouselScreen::CreateLayout");
    Eina_List *imageFiles = NULL;
    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(mItems, l, listData) {
        ActionAndData *actionAndData = static_cast<ActionAndData *> (listData);
        Photo *photo = static_cast<Photo *> (actionAndData->mData);
        ImageFile *imageFile = photo->GetOptimumResolutionPicture(R->SCREEN_SIZE_WIDTH, R->SCREEN_SIZE_HEIGHT);
        imageFiles = eina_list_append(imageFiles, imageFile);
    }
    mCarousel = new ImagesCarousel(mLayout, this, imageFiles);
    eina_list_free(imageFiles);
    elm_object_part_content_set(mLayout, "swallow.scroller", mCarousel->GetLayout());
    mCarousel->ShowPage(defaultPage);
    elm_object_signal_callback_add(mLayout, "mouse,clicked,*", "swallow.scroller", on_scroller_clicked, this);
    CreateHeader(mLayout, defaultPage); // For different mode different header layout need to be created
    const int size = 4;
    const int color1[size] = {0, 0, 0, 255};
    const int color2[size] = {0, 0, 0, 0};
    elm_object_part_content_set(mLayout, "top_gradient", CreateGradient(color1, color2, true));
    elm_object_part_content_set(mLayout, "bottom_gradient", CreateGradient(color2, color1, true));
    switch (mMode) {
    case eDATA_MODE:
    case eACTION_AND_DATA_MODE:
    case eWITHOUT_UI_MODE:
    case ePOST_COMPOSER_MODE:
    case eCAMERA_ROLL_MODE: {
        CreateFooter(defaultPhoto);
        }
        break;
    case ePROFILE_COVER_MODE:
        break;
    default:
        elm_object_signal_emit(mLayout, "hide_screen_description", "");
        break;
    }
}
void ImagesCarouselScreen::CreateFooter(Photo * defaultPhoto) {
    switch (mMode) {
    case eACTION_AND_DATA_MODE:
    case eDATA_MODE:
    case ePROFILE_COVER_MODE:
    {
        Evas_Object *footer_buttons = elm_object_part_content_unset(mLayout, "swallow.footer");
        evas_object_del(footer_buttons);
        footer_buttons = elm_layout_add(mLayout);
        elm_layout_file_set(footer_buttons, Application::mEdjPath, "image_viewer_buttons");
        elm_object_part_content_set(mLayout, "swallow.footer", footer_buttons);
        Evas_Object *label_text_like = elm_label_add(footer_buttons);
        elm_object_part_content_set(footer_buttons, "text.like", label_text_like);
        evas_object_size_hint_weight_set(label_text_like, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
        evas_object_size_hint_align_set(label_text_like, EVAS_HINT_FILL, 0.5);
        FRMTD_TRNSLTD_TXT_SET2(label_text_like, SANS_SHADOW_444444_FORMAT, R->LIKE_FONT_SIZE, "IDS_LIKE");
        evas_object_show(label_text_like);
        Evas_Object *label_text_only_like = elm_label_add(footer_buttons);
        elm_object_part_content_set(footer_buttons, "text.only_like", label_text_only_like);
        evas_object_size_hint_weight_set(label_text_only_like, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
        evas_object_size_hint_align_set(label_text_only_like, EVAS_HINT_FILL, 0.5);
        FRMTD_TRNSLTD_TXT_SET2(label_text_only_like, SANS_SHADOW_444444_FORMAT, R->LIKE_FONT_SIZE, "IDS_LIKE");
        evas_object_show(label_text_only_like);
        Evas_Object *comment = elm_label_add(footer_buttons);
        elm_object_part_content_set(footer_buttons, "swallow.comment", comment);
        evas_object_size_hint_weight_set(comment, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
        evas_object_size_hint_align_set(comment, EVAS_HINT_FILL, 0.5);
        FRMTD_TRNSLTD_TXT_SET2(comment, SANS_SHADOW_444444_FORMAT, R->LIKE_FONT_SIZE, "IDS_COMMENT");
        evas_object_show(comment);
        elm_object_signal_callback_add(footer_buttons, "mouse,clicked,*", "*", on_footer_button_clicked, this);
        elm_object_signal_callback_add(mLayout, "mouse,clicked,*", "like_comment.description_area", on_footer_button_clicked, this);
        elm_object_signal_emit(mLayout, "show_gradient", "");
        // Create description label
        Evas_Object *photo_text = elm_entry_add(mLayout);
        elm_object_part_content_set(mLayout, "text.description", photo_text);
        elm_entry_editable_set(photo_text, EINA_FALSE);
        elm_entry_context_menu_disabled_set(photo_text, EINA_TRUE);
        elm_scroller_policy_set(photo_text, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_OFF);
        evas_object_smart_callback_add(photo_text, "clicked,double", on_selected_text_cb, this);
        evas_object_smart_callback_add(photo_text, "longpressed", on_selected_text_cb, this);
        evas_object_smart_callback_add(photo_text, "selection,start", on_selected_text_cb, this);
        evas_object_smart_callback_add(photo_text, "anchor,clicked", on_anchor_clicked_cb, this);
    }
    break;
    default:
        Log::info("No footer UI mode");
        elm_object_signal_emit(mLayout, "up_of_lc_disabled", "");
    }
    if (defaultPhoto) {
        mDefaultPhoto = defaultPhoto;
        ecore_job_add(update_ui, this);
    }
}
void ImagesCarouselScreen::update_ui(void* data) {
    ImagesCarouselScreen *screen = static_cast<ImagesCarouselScreen *>(data);
    if (screen) {
        screen->UpdateUI(screen->mDefaultPhoto);
        screen->mDefaultPhoto = NULL;
    }
}
void ImagesCarouselScreen::CreateHeader(Evas_Object *parent, int defautPage) {
    switch (mMode) {
        case eCAMERA_ROLL_MODE: {
            mHeader = WidgetFactory::CreateHeaderWithBackBtnTextRightBtn(parent, false, "back_to_gallery",
            "IDS_PREVIEW", "IDS_UPPERCASE_SELECT", this);
            if(mHeader) {
                elm_object_part_content_set(mLayout, "swallow.post_composer_header", mHeader);
            }
        }
        break;
        case ePOST_COMPOSER_MODE: {
            int total = eina_list_count(mItems);
            if (total == 1) {
                mHeader = WidgetFactory::CreateBlackHeaderWithLogo(mLayout, "IDS_1_PHOTO_SELECTED", "IDS_UPPERCASE_DONE", this);
            } else {
                char * headerText = WidgetFactory::WrapByFormat2(i18n_get_text("IDS_INDEX_OF_TOTAL_SELECTED"),
                        defautPage + 1, total);
                if (headerText) {
                    mHeader = WidgetFactory::CreateBlackHeaderWithLogo(mLayout, headerText,
                            "IDS_UPPERCASE_DONE", this);
                    delete[] headerText;
                }
            }
            if (mHeader) {
                elm_object_part_content_set(mLayout, "swallow.post_composer_header", mHeader);
            }
        }
        break;
        case eDATA_MODE:
        case eACTION_AND_DATA_MODE:
        case eWITHOUT_UI_MODE:
        case ePROFILE_COVER_MODE: {
#ifdef PHOTO_TAGGING
            mComposerFooter = WidgetFactory::CreateLayoutByGroup(mLayout, "post_composer_footer");
            elm_object_part_content_set(mLayout, "swallow.post_composer_footer", mComposerFooter);
            elm_object_signal_callback_add(mComposerFooter, "mouse,clicked,*", "*", on_edit_tag_button_clicked, this);
            elm_object_signal_emit(mComposerFooter, "tag_button_off", "");
#endif
            mTopBarButtons = elm_layout_add(mLayout);
            elm_layout_file_set(mTopBarButtons, Application::mEdjPath, "image_viewer_topbar_buttons");
            elm_object_part_content_set(mLayout, "swallow.topbar", mTopBarButtons);
            elm_object_signal_callback_add(mTopBarButtons, "mouse,clicked,*", "*", on_top_bar_buttons_clicked, this);
            UpdateMoreMenuVisibility();
#ifdef PHOTO_TAGGING
            if (mMode != eWITHOUT_UI_MODE) {
                elm_object_signal_emit(mTopBarButtons, "show_tag_button", "");
            }
#else
            elm_object_signal_emit(mTopBarButtons, "hide_tag_button", "");
#endif
        }
        break;
        default:
            return;
    }
}
bool ImagesCarouselScreen::IsSavePhotoAvailable() {
    return (mCarousel->GetCurrentImageFile().GetStatus() == ImageFile::EDownloaded);
}
bool ImagesCarouselScreen::IsEditCaptionAvailable() {
    return (mFromId && Utils::IsMe(mFromId));
}
bool ImagesCarouselScreen::IsDeletePhotoAvailable() {
    return (mFromId && Utils::IsMe(mFromId) && mMode == ePROFILE_COVER_MODE);
}
unsigned int ImagesCarouselScreen::CalcMoreMenuItems() {
    unsigned int count = 0;
    if (IsSavePhotoAvailable()) {
        ++count;
    }
    if (IsEditCaptionAvailable()) {
        ++count;
    }
    if (IsDeletePhotoAvailable()) {
        ++count;
    }
    return count;
}
void ImagesCarouselScreen::ShowMoreMenu() {
    int menuItems = 0;
    delete [] mMoreMenuItems;
    mMoreMenuItems = new PopupMenu::PopupMenuItem[CalcMoreMenuItems()];
    if (IsSavePhotoAvailable()) {
        mMoreMenuItems[menuItems] = {ICON_DIR "/save_photo.png", "IDS_PHOTO_SAVE", nullptr, save_photo_cb, nullptr, menuItems, false, false};
        mMoreMenuItems[menuItems++].data = this;
    }
    if (IsEditCaptionAvailable()) {
        mMoreMenuItems[menuItems] = {ICON_DIR "/edit.png", "IDS_EDIT_CAPTION", nullptr, edit_caption_cb, nullptr, menuItems, false, false};
        mMoreMenuItems[menuItems++].data = this;
    }
    if (IsDeletePhotoAvailable()) {
        mMoreMenuItems[menuItems] = {ICON_DIR "/delete.png", "IDS_DELETE_PHOTO", nullptr, delete_photo_cb, nullptr, menuItems, false, false};
        mMoreMenuItems[menuItems++].data = this;
    }
    assert(menuItems); //ShowMoreMenu() MUST be called if at least 1 menu item is available; Otherwise logic is broken.
    mMoreMenuItems[menuItems - 1].itemLastItem = true;
    Evas_Coord y = R->STATUS_BAR_SIZE_H;
    PopupMenu::Show(menuItems, mMoreMenuItems, mLayout, false, y);
}
void ImagesCarouselScreen::UpdateMoreMenuVisibility() {
    if (mTopBarButtons && !mEditCaptionLayout) {
        elm_object_signal_emit(mTopBarButtons, CalcMoreMenuItems() ? "show_more_menu" : "hide_more_menu", "");
    }
}
void ImagesCarouselScreen::UpdateUI(ActionAndData * actionAndData) {
    Log::debug("ImagesCarouselScreen::UpdateUI->ActionAndData");
    if (!actionAndData) {
        actionAndData = GetActionAndDataAtCurrentPage();
        if (!actionAndData) {
            return;
        }
    }
    //Update photo description
    RefreshPhotoText(actionAndData);
    //Update likes and comments
    RefreshLikeAndCommentsInfo(actionAndData);

    RefreshTagButton();
}
void ImagesCarouselScreen::UpdateUI(Photo * photo) {
    Log::debug("ImagesCarouselScreen::UpdateUI->DefaultPhoto");
    ActionAndData *actionAndData = new ActionAndData(photo, NULL);
    //Update photo description
    RefreshPhotoText(actionAndData);
    //Update likes and comments
    RefreshLikeAndCommentsInfo(actionAndData);
    delete actionAndData;

    RefreshTagButton();
}
void ImagesCarouselScreen::RefreshPhotoText(ActionAndData * photoActionAndData) {
    Photo *photo = static_cast<Photo *>(photoActionAndData->mData);
    if (photo->GetName() && strcmp(photo->GetName(), "")) {
        Evas_Object *photoTextField = elm_object_part_content_get(mLayout, "text.description");
        if (photoTextField) {
            PhotoPresenter presenter(*photo);
            char *text = WidgetFactory::WrapByFormat2(SANS_MEDIUM_fff_FORMAT, R->NF_POST_MSG_TEXT_SIZE, presenter.GetName(PresenterHelpers::eFeed).c_str());
            if (text) {
               elm_object_text_set(photoTextField, text);
               Log::info("ImagesCarouselScreen::RefreshPhotoText: %s",text);
               delete[] text;
            }
            int textLength = strlen(photo->GetName());
            int lines = Utils::GetLinesNumber(photoTextField);

            //This is a workaround. When a photo caption is too long, GetLinesNumber function is called before the text is loaded fully.
            //We get a wrong number of lines (less than text has) and scroller doesn't appear.
            //The solution is to enable scrolling for a text which contains more than 3000 characters.
            if (lines > 2 || textLength > MESSAGE_LENGTH_TO_SCROLL) {
                elm_object_signal_emit(mLayout, "show.text.scroll", "text.description");
                elm_entry_scrollable_set(photoTextField, EINA_TRUE);
            } else {
                elm_object_signal_emit(mLayout, "hide.text.scroll", "text.description");
                elm_entry_scrollable_set(photoTextField, EINA_FALSE);
            }
        }
    } else {
        elm_object_signal_emit(mLayout, "hide_screen_description", "");
    }
    if (!mFromId) {
        mFromId = SAFE_STRDUP(photo->GetFromId());
    }
}
void ImagesCarouselScreen::RefreshLikeAndCommentsInfo(ActionAndData * actionAndData) {
    Photo * photo = static_cast<Photo *>(actionAndData->mData);
    if (photo->GetLikesCount() || photo->GetCommentsCount()) {
        PhotoPresenter presenter(*photo);
        HRSTC_TRNSLTD_PART_TEXT_SET(mLayout, "like_comment.description", presenter.GetLikesAndCommentsCount().c_str());
        elm_object_signal_emit(mLayout, "show_screen_like_comment_description", "");
    } else {
        elm_object_part_text_set(mLayout, "like_comment.description", "");
        elm_object_signal_emit(mLayout, "hide_screen_like_comment_description", "");
    }
    Evas_Object *footer_buttons = elm_object_part_content_get(mLayout, "swallow.footer");
    if (!photo->GetCanLike() && !photo->GetCanComment()) {
        elm_object_signal_emit(footer_buttons, "all_disabled", "");
    } else if (!photo->GetCanLike() && photo->GetCanComment()) {
        elm_object_signal_emit(footer_buttons, "only_comment", "");
    } else {
        Evas_Object *label_text_like = elm_object_part_content_get(footer_buttons, "text.like");
        Evas_Object *label_text_only_like = elm_object_part_content_get(footer_buttons, "text.only_like");

        if (photo->GetCanLike() && !photo->GetCanComment()) {
            elm_object_signal_emit(footer_buttons, "only_like", "");
            FRMTD_TRNSLTD_TXT_SET2(label_text_only_like, SANS_SHADOW_444444_FORMAT, R->LIKE_FONT_SIZE, "IDS_LIKE");
        } else {
            elm_object_signal_emit(footer_buttons,"all_enabled", "");
            FRMTD_TRNSLTD_TXT_SET2(label_text_like, SANS_SHADOW_444444_FORMAT, R->LIKE_FONT_SIZE, "IDS_LIKE");
        }
        if (photo->GetHasLiked()) {
            elm_object_signal_emit(footer_buttons, "like_liked", "");
            FRMTD_TRNSLTD_TXT_SET2(label_text_like, SANS_SHADOW_2f3f5c_FORMAT, R->LIKE_FONT_SIZE, "IDS_LIKE");
            FRMTD_TRNSLTD_TXT_SET2(label_text_only_like, SANS_SHADOW_2f3f5c_FORMAT, R->LIKE_FONT_SIZE, "IDS_LIKE");
        } else {
            elm_object_signal_emit(footer_buttons, "like.normal", "");
            FRMTD_TRNSLTD_TXT_SET2(label_text_like, SANS_SHADOW_444444_FORMAT, R->LIKE_FONT_SIZE, "IDS_LIKE");
        }
    }
}

void ImagesCarouselScreen::Update(AppEventId eventId, void * data) {
    switch (eventId) {
        case ePOST_RELOAD_EVENT:
            if(data) {
                Post *post = static_cast<Post *>(data);
                ActionAndData *actionAndData = GetActionAndDataAtCurrentPage();

                if(actionAndData && mParentActionAndData) {
                    assert(mParentActionAndData->mData);
                    if (*(mParentActionAndData->mData) == *post) {
                        Log::info("ImagesCarouselScreen::Update single image with parent post %s",post->GetId());
                        mPost = post;
                        mParentActionAndData->ReSetData(post);
                        Photo *photo = static_cast<Photo*>(actionAndData->mData);
                        photo->SetPhotoPost(mPost);
                        UpdateUI();
                    }
                }
            }
            break;
        case eLIKE_COMMENT_EVENT:
            if (data) {
                ActionAndData *actionAndData = NULL;
                Log::info("ImagesCarouselScreen::Update->eLIKE_COMMENT_EVENT");
                if (mMode == eDATA_MODE && mPost) {
                    actionAndData = mImagesCarouselActionAndData;
                } else {
                    actionAndData = GetActionAndDataAtCurrentPage();
                }
                if (actionAndData && actionAndData->mData && !strcmp(static_cast<char *>(data), actionAndData->mData->GetId())) {
                    UpdateUI();
                }
            }
            break;
        case eLANGUAGE_CHANGED:
            if (mIsShowTagButton) {
               if (mEditPhotoTagScreen) {
                    mEditPhotoTagScreen->CreateTags();
               } else {
                   CreateTags();
               }
            }
            break;
        case ePHOTO_COMMENTS_LIST_WAS_MODIFIED:
            if (data) {
                const char* imageId = static_cast<char*>(data);
                Log::info("ImagesCarouselScreen::Update->ePHOTO_COMMENTS_LIST_WAS_MODIFIED for photoId = %s",imageId);
                //maybe we need to find imageId inside ActionAndData mItems Eina_List before request???
                RequestOneImageDetails(imageId);
            }
            break;
        default:
        break;
    }
}
bool ImagesCarouselScreen::IsCaptionChanged() {
    bool isChanged = false;
    ActionAndData *actionAndData = GetActionAndDataAtCurrentPage();
    Photo *photo = static_cast<Photo *>(actionAndData->mData);
    char *newName = GetCaptionEntryText();
    if (photo && photo->GetName() && newName && strcmp(newName, photo->GetName())) { //there are old and new names, but they are not the same.
        isChanged = true;
    } else if ((photo && !(photo->GetName() && *(photo->GetName()))) && (newName && *newName)) { // name was empty, now it is non-empty
        isChanged = true;
    } else if (photo && (photo->GetName() && *(photo->GetName())) && !(newName && *newName)) { // name was non-empty, now it is empty
        isChanged = true;
    }
    free(newName);
    return isChanged;
}
void ImagesCarouselScreen::on_footer_button_clicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    Log::info("CLICKED: %s", source);
    ImagesCarouselScreen * me = static_cast<ImagesCarouselScreen *>(data);
    if (me) {
        ActionAndData *actionAndData = NULL;
        bool hasLikes = false;
        bool hasComments = false;
        if (me->mMode == eDATA_MODE && me->mPost) {
            actionAndData = me->mImagesCarouselActionAndData;
            hasLikes = me->mPost->GetCanLike();
            hasComments = me->mPost->GetCanComment();
        } else {
            actionAndData = me->GetActionAndDataAtCurrentPage();
            if (actionAndData && actionAndData->mData) {
                Photo * photo = static_cast<Photo *>(actionAndData->mData);
                hasLikes = photo->GetLikesCount() > 0;
                hasComments = photo->GetCommentsCount() > 0;
            }
        }
        if (!actionAndData) {
            return;
        }
        if (source) {
            if (!strcmp(source, "like_area")) {
                ActionBase::on_like_btn_clicked_cb(actionAndData, NULL, NULL, NULL);
            } else if (!strcmp(source, "comment_area") || (!strcmp(source, "like_comment.description_area") && (hasLikes  || hasComments))) {
                ActionBase::on_comment_btn_clicked_cb(actionAndData, NULL, NULL, NULL);
            }
        }
    }
}
void ImagesCarouselScreen::on_top_bar_buttons_clicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    ImagesCarouselScreen * me = static_cast<ImagesCarouselScreen *>(data);
    assert(me);
    if (source && !strcmp(source, "image.more")) {
        me->ShowMoreMenu();
    } else if (source && !strcmp(source, "image.tag")) {
        if (me->mTopBarButtons) {
            if (me->mIsShowTagButton) {
                me->mIsShowTagButton = false;
                me->RemoveTags();
                elm_object_signal_emit(me->mTopBarButtons, "tag_clicked_off", "");
                elm_object_signal_emit(me->mComposerFooter, "tag_button_off", "");
                me->ShowImageControls();
                me->mCarousel->EnableZoom(true);
            } else {
                me->mIsShowTagButton = true;
                elm_object_signal_emit(me->mTopBarButtons, "tag_clicked_on", "");
                elm_object_signal_emit(me->mComposerFooter, "tag_button_on", "");
                elm_object_signal_emit(me->mLayout, "up_of_lc_disabled", "");
                me->CreateTags();
                me->mCarousel->EnableZoom(false);
            }
        }
    }
}
void ImagesCarouselScreen::on_anchor_clicked_cb(void *user_data, Evas_Object *obj, void *event_info) {
    ImagesCarouselScreen * me = static_cast<ImagesCarouselScreen *>(user_data);
    if (me) {
        ActionAndData *actionAndData = me->GetActionAndDataAtCurrentPage();
        ActionBase::on_anchor_clicked_cb(actionAndData, obj, event_info);
    }
}
void ImagesCarouselScreen::ConfirmCallback(void *data, Evas_Object *obj, const char *emission, const char *source) {
    Log::info("ConfirmCallback into");
    if (mMode == eCAMERA_ROLL_MODE) {
        ActionAndData *actionAndData = GetActionAndDataAtCurrentPage();
        PostComposerMediaData * mediaData = static_cast<PostComposerMediaData *>(actionAndData->mData);
        if (!mediaData->IsSelected()) {
            AppEvents::Get().Notify(eCAMERA_ROLL_SELECT_EVENT, mediaData->GetGengridItem());
        }
    }
    Pop();
}
void ImagesCarouselScreen::CreateActionAndDatesFromData(Eina_List *data) {
    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(data, l, listData) {
        ActionAndData *actionAndData = CreateActionAndData(listData);
        if (actionAndData) {
            mItems = eina_list_append(mItems, actionAndData);
        }
    }
}
ActionAndData* ImagesCarouselScreen::CreateActionAndData(void* dataForItem) {
    ActionAndData * actionAndData = NULL;
    switch (mMode) {
    case eACTION_AND_DATA_MODE: {
        ActionAndData * parentActionAndData = static_cast<ActionAndData *>(dataForItem);
        actionAndData = new ActionAndData(parentActionAndData->mData, mImagesCarouselAction);
    }
        break;
    case eWITHOUT_UI_MODE:
    case ePOST_COMPOSER_MODE:
    case eDATA_MODE:
    case ePROFILE_COVER_MODE:
    case eCAMERA_ROLL_MODE: {
        Photo * photo = static_cast<Photo *>(dataForItem);
        actionAndData = new ActionAndData(photo, mImagesCarouselAction);
    }
        break;
    default:
        break;
    }
    return actionAndData;
}
Evas_Object * ImagesCarouselScreen::CreateCaptionEntry() {
    Evas_Object *entry = elm_entry_add(mEditCaptionLayout);
    elm_layout_content_set(mEditCaptionLayout, "entry_field", entry);
    elm_layout_text_set(mEditCaptionLayout, "save_changes_btn_text", i18n_get_text("IDS_SAVE_CHANGES"));
    elm_layout_text_set(mEditCaptionLayout, "clear_btn_text", i18n_get_text("IDS_CLEAR_CAPTION_BOX"));
    Utils::SetEntryGuideText( entry, SANS_REGULAR_9298A4_FORMAT, R->COMMENT_MSG_TEXT_SIZE, i18n_get_text("IDS_EDIT_CAPTION_HINT") );
    elm_entry_scrollable_set(entry, EINA_TRUE);
    Utils::SetKeyboardFocus(entry, EINA_TRUE);
    elm_entry_prediction_allow_set(entry, EINA_FALSE);
    char *fontStyle = WidgetFactory::WrapByFormat(SANS_REGULAR_BLACK_STYLE, R->COMMENT_MSG_TEXT_SIZE);
    if (fontStyle) {
        elm_entry_text_style_user_push(entry, fontStyle);
        delete[] fontStyle;
    }
    elm_entry_cnp_mode_set(entry, ELM_CNP_MODE_PLAINTEXT);
    evas_object_raise(entry);
    evas_object_show(entry);
    return entry;
}
void ImagesCarouselScreen::RefreshTagButton() {
    if (mIsShowTagButton) {
        elm_object_signal_emit(elm_object_part_content_get(mLayout, "swallow.post_composer_footer"), "tag_button_on", "");
        elm_object_signal_emit(mLayout, "up_of_lc_disabled", "");
    } else {
        elm_object_signal_emit(elm_object_part_content_get(mLayout, "swallow.post_composer_footer"), "tag_button_off", "");
        elm_object_signal_emit(mLayout, "up_of_lc_enabled", "");
    }
}
void ImagesCarouselScreen::PageChanged(int pageNumber) {
#ifdef PHOTO_TAGGING
    if (mIsShowTagButton) {
        CreateTags();
    }
#endif
    if (mIsShowImageControls) {
        if (mMode == ePOST_COMPOSER_MODE) {
            char * headerText = WidgetFactory::WrapByFormat2(i18n_get_text("IDS_INDEX_OF_TOTAL_SELECTED"), pageNumber + 1, eina_list_count(mItems));
            if (headerText) {
                WidgetFactory::SetHeaderText(mHeader, headerText);
                delete[] headerText;
            }
        }
        ShowImageControls();
        UpdateMoreMenuVisibility();
    } else {
        HideImageControls();
    }
}
void ImagesCarouselScreen::PageDownloaded(unsigned int pageNumber) {
    if (mCarousel->GetCurrentPage() == pageNumber) {
        UpdateMoreMenuVisibility();
#ifdef PHOTO_TAGGING
        if (mTopBarButtons && mMode != eWITHOUT_UI_MODE) {
            elm_object_signal_emit(mTopBarButtons, "show_tag_button", "");
        }
#endif
    }
}
void ImagesCarouselScreen::save_photo_cb(void *user_data, Evas_Object *obj, void *event_info) {
    PopupMenu::Close();
    ImagesCarouselScreen * me = static_cast<ImagesCarouselScreen *>(user_data);
    assert (me);

    delete [] me->mMoreMenuItems;
    me->mMoreMenuItems = nullptr;
    ActionAndData *actionAndData = me->GetActionAndDataAtCurrentPage();
    if (!actionAndData) {
        return;
    }
    ImagesCarouselAction *action = static_cast<ImagesCarouselAction *>(actionAndData->mAction);
    action->SaveBtnClicked(actionAndData);
}
/* edit caption callbacks */
void ImagesCarouselScreen::edit_caption_cb(void *user_data, Evas_Object *obj, void *event_info) {
    PopupMenu::Close();
    ImagesCarouselScreen * me = static_cast<ImagesCarouselScreen *>(user_data);
    assert (me);
    delete [] me->mMoreMenuItems;
    me->mMoreMenuItems = nullptr;
    me->HideImageControls();
    me->mCarousel->StopScroll(true);
    elm_object_signal_callback_del(me->mLayout, "mouse,clicked,*", "swallow.scroller", on_scroller_clicked);
#ifdef PHOTO_TAGGING
    me->mCarousel->EnableZoom(false);
    if (me->mIsShowTagButton) {
        me->RemoveTags();
        me->mIsShowTagButton = false;
        elm_object_signal_emit(me->mTopBarButtons, "tag_clicked_off", "");
        elm_object_signal_emit(me->mComposerFooter, "tag_button_off", "");
    }
#endif

    me->mEditCaptionPopup = elm_popup_add(me->mLayout);
    elm_popup_align_set(me->mEditCaptionPopup, ELM_NOTIFY_ALIGN_FILL, 1.0);
    evas_object_size_hint_weight_set(me->mEditCaptionPopup, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);

    evas_object_smart_callback_add(me->mEditCaptionPopup, "block,clicked", menu_dismiss_cb, me);

    ActionAndData *action_data = me->GetActionAndDataAtCurrentPage();
    Photo *photo = static_cast<Photo *>(action_data->mData);

    me->mEditCaptionLayout = WidgetFactory::CreateLayoutByGroup(me->mEditCaptionPopup, "edit_caption");
    elm_object_content_set(me->mEditCaptionPopup, me->mEditCaptionLayout);

    me->mEditCaptionEntry = me->CreateCaptionEntry();
    me->mTagging = new FriendTagging(me->mEditCaptionLayout, me->mEditCaptionEntry);

    PhotoPresenter presenter(*photo);
    elm_entry_entry_set(me->mEditCaptionEntry, presenter.GetName(PresenterHelpers::eComposer).c_str());
    evas_object_smart_callback_add(me->mEditCaptionEntry, "changed", entry_changed_cb, user_data);
    me->EnableSaveButton(false);
    me->EnableClearButton(!elm_entry_is_empty(me->mEditCaptionEntry));
    evas_object_show(me->mEditCaptionPopup);
}
void ImagesCarouselScreen::save_changed_caption_cb(void *data, Evas_Object *obj, const char *emission, const char *source) {
    ImagesCarouselScreen * me = static_cast<ImagesCarouselScreen *>(data);
    if (me) {
        ActionAndData *actionAndData = me->GetActionAndDataAtCurrentPage();
        if (!actionAndData) {
            return;
        }
        if (me->IsCaptionChanged()) {
            ImagesCarouselAction *action = static_cast<ImagesCarouselAction *>(actionAndData->mAction);
            action->SaveCaptionClicked(actionAndData);
        }
        me->CloseEditCaption();
    }
}
void ImagesCarouselScreen::entry_changed_cb(void* data, Evas_Object* Obj, void* EventInfo) {
    ImagesCarouselScreen *me = static_cast<ImagesCarouselScreen *>(data);
    me->EnableSaveButton(me->IsCaptionChanged());
    me->EnableClearButton(!elm_entry_is_empty(me->mEditCaptionEntry));
}
void ImagesCarouselScreen::EnableClearButton(bool enable) {
    if (enable) {
        if (!mIsClearButtonEnabled) {
            mIsClearButtonEnabled = true;
            elm_object_signal_callback_add(mEditCaptionLayout, "mouse,clicked,*", "clear_btn*", clear_caption_cb, this);
        }
        elm_object_signal_emit(mEditCaptionLayout, "enable_clear_button_text", "clear_btn_text");
        elm_object_signal_emit(mEditCaptionLayout, "enable_clear_button", "clear_btn");
    } else {
        if (mIsClearButtonEnabled) {
            mIsClearButtonEnabled = false;
            elm_object_signal_callback_del(mEditCaptionLayout, "mouse,clicked,*", "clear_btn*", clear_caption_cb);
        }
        elm_object_signal_emit(mEditCaptionLayout, "disable_clear_button_text", "clear_btn_text");
        elm_object_signal_emit(mEditCaptionLayout, "disable_clear_button", "clear_btn");
    }
}
void ImagesCarouselScreen::EnableSaveButton(bool enable) {
    if (enable) {
        if (!mIsSaveButtonEnabled) {
            mIsSaveButtonEnabled = true;
            elm_object_signal_callback_add(mEditCaptionLayout, "mouse,clicked,*", "save_changes*", save_changed_caption_cb, this);
        }
        elm_object_signal_emit(mEditCaptionLayout, "enable_save_button_text", "save_changes_btn_text");
        elm_object_signal_emit(mEditCaptionLayout, "enable_save_button", "save_changes_btn");
    } else {
        if (mIsSaveButtonEnabled) {
            mIsSaveButtonEnabled = false;
            elm_object_signal_callback_del(mEditCaptionLayout, "mouse,clicked,*", "save_changes*", save_changed_caption_cb);
        }
        elm_object_signal_emit(mEditCaptionLayout, "disable_save_button_text", "save_changes_btn_text");
        elm_object_signal_emit(mEditCaptionLayout, "disable_save_button", "save_changes_btn");
    }
}
char *ImagesCarouselScreen::GetCaptionEntryText() {
    return elm_entry_markup_to_utf8(elm_entry_entry_get(mEditCaptionEntry));
}
void ImagesCarouselScreen::menu_dismiss_cb(void *data, Evas_Object *obj, void *event_info){
    ImagesCarouselScreen * me = static_cast<ImagesCarouselScreen *>(data);
    if (me) {
        me->CloseEditCaption();
    }
}
void ImagesCarouselScreen::clear_caption_cb(void *data, Evas_Object *obj, const char *emission, const char *source) {
    ImagesCarouselScreen * me = static_cast<ImagesCarouselScreen *>(data);
    if (me && !elm_entry_is_empty(me->mEditCaptionEntry)) {
        elm_entry_entry_set(me->mEditCaptionEntry, "");
    }
}
/* end of edit caption callbacks */
void ImagesCarouselScreen::ShowKeepDiscardPopup() {
    mConfirmationDialog = ConfirmationDialog::CreateAndShow(mLayout,
            "IDS_LEAVE_CAPTION", "IDS_YOUR_CHANGES_WONT_BE_SAVED",
            "IDS_STAY", "IDS_LEAVE",
            confirmation_dialog_cb, this);
}
void ImagesCarouselScreen::confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event) {
    Log::info("confirmation_dialog_cb:%d", event);
    ImagesCarouselScreen *screen = static_cast<ImagesCarouselScreen *>(user_data);
    switch (event) {
    case ConfirmationDialog::ECDYesPressed:
        Utils::SetKeyboardFocus(screen->mEditCaptionEntry, EINA_TRUE);
        elm_entry_input_panel_show(screen->mEditCaptionEntry);
        break;
    case ConfirmationDialog::ECDNoPressed:
        screen->CloseEditCaption();
        break;
    case ConfirmationDialog::ECDDismiss:
        break;
    default:
        break;
    }
    screen->DeleteConfirmationDialog();
}
void ImagesCarouselScreen::DeleteConfirmationDialog() {
    delete mConfirmationDialog;
    mConfirmationDialog = nullptr;
}
/* delete photo callbacks */
void ImagesCarouselScreen::delete_photo_cb(void *user_data, Evas_Object *obj, void *event_info) {
    PopupMenu::Close();
    ImagesCarouselScreen * me = static_cast<ImagesCarouselScreen *>(user_data);
    assert(me);
    delete [] me->mMoreMenuItems;
    me->mMoreMenuItems = nullptr;
    me->mConfirmationDialog = ConfirmationDialog::CreateAndShow(me->mLayout,
        "IDS_DELETE_PHOTO_CONFIRMATION_CAPTION",
        "IDS_DELETE_PHOTO_CONFIRMATION_TEXT",
        "IDS_CANCEL", "IDS_DELETE",
        deletion_confirmation_dialog_cb, me);
}
void ImagesCarouselScreen::deletion_confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event) {
    ImagesCarouselScreen *screen = static_cast<ImagesCarouselScreen *>(user_data);
    assert(screen);
    ActionAndData *action_data = screen->GetActionAndDataAtCurrentPage();
    assert(action_data);
    Photo *photo = static_cast<Photo *>(action_data->mData);
    assert(photo);
    switch (event) {    //which option is chosen in the confirmation dialog
    case ConfirmationDialog::ECDNoPressed:    //'Delete' option
        if (!screen->mDeleteRequest) {
            screen->mDeleteRequest = FacebookSession::GetInstance()->DeletePhoto(photo->GetId(), on_delete_photo_completed, screen);
        }
        break;
    case ConfirmationDialog::ECDYesPressed:    //'Cancel' option
    case ConfirmationDialog::ECDDismiss:    //dismissed without selection
    default:
        break;
    }
    delete screen->mConfirmationDialog;
    screen->mConfirmationDialog = nullptr;
}
void ImagesCarouselScreen::on_delete_photo_completed(void *data, char* response, int code) {
    ImagesCarouselScreen *me = static_cast<ImagesCarouselScreen*>(data);
    Log::info("ImagesCarouselScreen::on_delete_photo_completed(code=%d), response='%s'", code, response);
    assert(me);
    FacebookSession::GetInstance()->ReleaseGraphRequest(me->mDeleteRequest);
    me->mDeleteRequest = nullptr;
    if (code == CURLE_OK) {
        AppEvents::Get().Notify(eDELETED_USER_PICTURE, nullptr);
        if (Application::GetInstance()->GetTopScreenId() == me->mScreenId) {
            me->Pop();
        }
    } else {
        notification_status_message_post(i18n_get_text("IDS_DELETE_PHOTO_FAILED"));
        Log::error("ImagesCarouselScreen::on_delete_photo_completed() - Error sending http request");
    }
    free(response);
}
void ImagesCarouselScreen::on_selected_text_cb(void *user_data, Evas_Object *obj, void *event_info) {
    elm_entry_select_none(obj);
}
/* end of delete photo callbacks */
Evas_Object *ImagesCarouselScreen::CreateGradient(const int *color1, const int *color2, bool horizontalGradient) {
    int colors[2][4];
    for (int i = 0; i < 4; i++) {
        colors[0][i] = color1[i];
        colors[1][i] = color2[i];
    }
    const int b_r = colors[0][0], b_g = colors[0][1], b_b = colors[0][2], b_a = colors[0][3];
    const int e_r = colors[1][0], e_g = colors[1][1], e_b = colors[1][2], e_a = colors[1][3];
    Evas_Object *img = evas_object_image_filled_add(evas_object_evas_get(mLayout));
    horizontalGradient ? evas_object_image_size_set(img, 1, 255) : evas_object_image_size_set(img, 255, 1);
    evas_object_image_alpha_set(img, EINA_TRUE);
    unsigned int *data32 = static_cast<unsigned int *>(evas_object_image_data_get(img, EINA_TRUE));
    for (unsigned x = 0; x < 255; x++) {
        int r, g, b, a;
        a = (b_a * (255 - x) + e_a * x) / (2 * 255);
        r = (b_r * b_a * (255 - x) + e_r * e_a * (x)) / (2 * 255 * 255);
        g = (b_g * b_a * (255 - x) + e_g * e_a * (x)) / (2 * 255 * 255);
        b = (b_b * b_a * (255 - x) + e_b * e_a * (x)) / (2 * 255 * 255);
        data32[x] = (a << 24) | (r << 16) | (g << 8) | b;
    }
    evas_object_image_data_set(img, data32);
    evas_object_size_hint_weight_set(img, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(img, EVAS_HINT_FILL, EVAS_HINT_FILL);
    return img;
}
void ImagesCarouselScreen::on_scroller_clicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    ImagesCarouselScreen* me = static_cast<ImagesCarouselScreen*>(data);
    if (me->mIsShowImageControls) {
        me->HideImageControls();
    } else {
        me->ShowImageControls();
    }
}
void ImagesCarouselScreen::HideImageControls() {
    elm_object_signal_emit(mLayout, "hide_controls", "");
    elm_object_signal_emit(elm_object_part_content_get(mLayout, "swallow.topbar"), "hide_top_buttons", "");
    elm_object_signal_emit(elm_object_part_content_get(mLayout, "swallow.footer"), "all_disabled", "");
    elm_object_signal_emit(elm_object_part_content_get(mLayout, "swallow.post_composer_footer"), "tag_button_off", "");
    mIsShowImageControls = false;
}
void ImagesCarouselScreen::ShowImageControls() {
    switch (mMode) {
        case ePOST_COMPOSER_MODE: {
            elm_object_signal_emit(mLayout, "show_default_gradient", "");
        }
            break;
        case eCAMERA_ROLL_MODE: {
            elm_object_signal_emit(mLayout, "show_default_gradient", "");
        }
            break;
        case eACTION_AND_DATA_MODE:
        case eDATA_MODE:
        case eWITHOUT_UI_MODE:
        case ePROFILE_COVER_MODE: {
            UpdateMoreMenuVisibility();
            elm_object_signal_emit(mLayout, "show_all_gradient", "");
#ifdef PHOTO_TAGGING
            if (mMode != eWITHOUT_UI_MODE) {
                elm_object_signal_emit(mTopBarButtons, "show_tag_button", "");
            }
#else
            elm_object_signal_emit(elm_object_part_content_get(mLayout, "swallow.footer"), "all_enabled", "");
#endif
            UpdateUI();
        }
            break;
        default:
            break;
    }
    mIsShowImageControls = true;
}
//Part for view PhotoTags
void ImagesCarouselScreen::CreateTags() {
    RemoveTags();
    CalculateImageGeometry();
    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(static_cast<Photo*>(GetActionAndDataAtCurrentPage()->mData)->GetPhotoTagList(), l, listData) {
        mTagList = eina_list_append(mTagList, new PhotoTagging(GetActionAndDataAtCurrentPage(), mLayout, static_cast<PhotoTag*>(listData), mImageRegion, false));
    }
    mAreTagsShown = true;
}

bool ImagesCarouselScreen::RemoveTags() {
    bool ret = false;
    if (mTagList && mTopBarButtons) {
        void *listData;
        EINA_LIST_FREE(mTagList, listData) {
            delete static_cast<PhotoTagging*>(listData);
        }
        ret = true;
    }
    mAreTagsShown = false;
    return ret;
}

void ImagesCarouselScreen::UpdateTags() {
    Eina_List *deletedTags = eina_list_clone(mTagList);
    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(static_cast<Photo*>(GetActionAndDataAtCurrentPage()->mData)->GetPhotoTagList(), l, listData) {
        PhotoTag* photoTag = static_cast<PhotoTag*>(listData);
        PhotoTagging *existentTagging = nullptr;
        Eina_List *l1;
        void *existentData;
        EINA_LIST_FOREACH(mTagList, l1, existentData) {
            PhotoTagging *tagging = static_cast<PhotoTagging *> (existentData);
            if (tagging->mPhotoTag->mX == photoTag->mX && tagging->mPhotoTag->mY == photoTag->mY) {
                existentTagging = tagging;
            }
        }
        if (existentTagging) {
            deletedTags = eina_list_remove(deletedTags, existentTagging);
            existentTagging->Show();
        } else {
            mTagList = eina_list_append(mTagList, new PhotoTagging(GetActionAndDataAtCurrentPage(), mLayout, photoTag, mImageRegion, false));
        }
    }
    EINA_LIST_FREE(deletedTags, listData) {
        mTagList = eina_list_remove(mTagList, listData);
        delete static_cast<PhotoTagging *> (listData);
    }
    mAreTagsShown = true;
}

void ImagesCarouselScreen::HideTags(bool hide) {
    if (mAreTagsShown == hide) {
        Eina_List *l;
        void *listData;
        EINA_LIST_FOREACH(mTagList, l, listData) {
            if (hide) {
                static_cast<PhotoTagging*>(listData)->Hide();
            } else {
                static_cast<PhotoTagging*>(listData)->Show();
            }
        }
        mAreTagsShown = !hide;
    }
}
void ImagesCarouselScreen::ScrollStart() {
    HideTags(true);
}
void ImagesCarouselScreen::ScrollStop() {
    if (mIsShowTagButton) {
        HideTags(false);
    }
}
void ImagesCarouselScreen::OnPause() {
#ifdef PHOTO_TAGGING
    HideTags(true);
#endif
}
void ImagesCarouselScreen::OnResume() {
#ifdef PHOTO_TAGGING
    if (mIsShowTagButton) {
        mEditPhotoTagScreen = nullptr;
        UpdateTags();
    }
#endif
}
void ImagesCarouselScreen::on_edit_tag_button_clicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    ImagesCarouselScreen* me = static_cast<ImagesCarouselScreen*>(data);
    me->mEditPhotoTagScreen = new EditPhotoTagScreen(static_cast<Photo*>(static_cast<ImagesCarouselScreen *>(data)->GetActionAndDataAtCurrentPage()->mData));
    Application::GetInstance()->AddScreen(me->mEditPhotoTagScreen);
}
void ImagesCarouselScreen::CalculateImageGeometry() {
    Photo * photo = static_cast<Photo*>(GetActionAndDataAtCurrentPage()->mData);
    int x, y;
    evas_object_geometry_get(getMainLayout(), &x, &y, 0, 0);
    int w = R->SCREEN_SIZE_WIDTH - x;
    int h = R->SCREEN_SIZE_HEIGHT - y;
    double displayAspect = h / (double) (w - x);
    double photoAspect = photo->GetHeight() / (double) photo->GetWidth();
    if (displayAspect > photoAspect) {
        mImageRegion->width = w - x;
        mImageRegion->height = mImageRegion->width * photoAspect;
        mImageRegion->y = (h - mImageRegion->height) / 2;
        mImageRegion->y += y;
        mImageRegion->x = x;
    } else {
        mImageRegion->height = h;
        mImageRegion->width = (double)mImageRegion->height / photoAspect;
        mImageRegion->x = (w - x - mImageRegion->width) / 2;
        mImageRegion->y = y;
    }
}

void ImagesCarouselScreen::RequestOneImageDetails(const char* imageId) {
    Eina_List *imageIds = eina_list_append(nullptr, imageId);
    mRequests = FacebookSession::GetInstance()->GetImageDetails(imageIds, on_get_image_details_completed, this); //ToDo: we don't need it anymore because we can get all the details from Post.
    imageIds = eina_list_free(imageIds);
}
