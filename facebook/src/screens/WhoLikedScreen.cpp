#include "Common.h"
#include "ConnectivityManager.h"
#include "FacebookSession.h"
#include "FriendOperation.h"
#include "jsonutilities.h"
#include "PeopleWhoLikedProvider.h"
#include "Popup.h"
#include "User.h"
#include "Utils.h"
#include "WhoLikedScreen.h"
#include "WidgetFactory.h"

const unsigned int WhoLikedScreen::DEFAULT_ITEMS_WND_SIZE = 40;
const unsigned int WhoLikedScreen::DEFAULT_INIT_ITEMS_SIZE = 20;
const unsigned int WhoLikedScreen::DEFAULT_ITEMS_TEP_SIZE = 5;

WhoLikedScreen::WhoLikedScreen(const char * id) : ScreenBase(nullptr), TrackItemsProxyAdapter(nullptr, mProvider = new PeopleWhoLikedProvider()), mUserDetailsRequestList(nullptr)
{
    assert(id);
    mScreenId = ScreenBase::SID_WHO_LIKED;
    mId = SAFE_STRDUP(id);
    mAction = new FriendsAction(this);
    SetIdentifier("WHO_LIKED");

    mLayout = WidgetFactory::CreateLayoutByGroup(getParentMainLayout(), "who_liked_screen_composer");

    ApplyScroller(mLayout, true);
    elm_object_part_content_set(mLayout, "list", GetScroller());

    elm_object_translatable_part_text_set(mLayout, "top_people_who_like_this_text", "IDS_PPL_WHO_LIKE_THIS");

    elm_object_signal_callback_add(mLayout, "mouse,clicked,*", "top_*", back_btn_cb, this);

    AppEvents::Get().Subscribe(eADD_FRIEND_BTN_CLICKED, this);
    AppEvents::Get().Subscribe(eCANCEL_FRIEND_REQUEST_BTN_CLICKED, this);
    AppEvents::Get().Subscribe(eFRIEND_REQUEST_CONFIRMED, this);
    AppEvents::Get().Subscribe(eFRIEND_REQUEST_CONFIRM_DELETE_ERROR, this);
    AppEvents::Get().Subscribe(eUPDATE_UNFRIENDED_USER_STATUS, this);

    mProvider->SetEntityId(mId);
    TrackItemsProxy::ProxySettings *settings = GetProxySettings();
    settings->wndSize = DEFAULT_ITEMS_WND_SIZE;
    settings->initialWndSize = DEFAULT_INIT_ITEMS_SIZE;
    settings->wndStep = DEFAULT_ITEMS_TEP_SIZE;
    SetProxySettings( settings );
    RequestData();
}

WhoLikedScreen::~WhoLikedScreen()
{
    elm_object_signal_callback_del(mLayout, "mouse,clicked,*", "top_back_btn", back_btn_cb);

    free(mId);
    delete mAction;
    EraseWindowData();
    DeleteAllUserDetailsRequests();
    mProvider->Release();
}

void* WhoLikedScreen::CreateItem(void *dataForItem, bool isAddToEnd) {
    User *user = static_cast<User*>(dataForItem);
    ActionAndData *action_data = nullptr;
    assert(user);
    if (user) {
        action_data = new ActionAndData(user, mAction);
        action_data->mParentWidget = CreateUserItem(GetDataArea(), action_data, isAddToEnd);
    }

    return action_data;
}

void WhoLikedScreen::user_details_cb(void* object, char* response, int code) {
    assert(object);
    ActionAndData *data = static_cast<ActionAndData*>(object);
    if (ActionAndData::IsAlive(data) == false) {
        //The list item has been destroyed during fast scrolling. No operation with a deleted item is allowed.
        free(response);
        return;
    }
    assert(data->mAction);

    WhoLikedScreen *me = dynamic_cast<WhoLikedScreen*>(data->mAction->getScreen());
    if (me && code == CURLE_OK) {
        JsonObject * rootObject = nullptr;
        JsonParser * parser = openJsonParser(response, &rootObject);
        if (rootObject) {
            User *newUser = new User(rootObject);
            if (me->mProvider->ReplaceUser(newUser)) {
                data->ReSetData(newUser);
            }
        }
        g_object_unref(parser);
    }
    free(response);

    User *user = static_cast<User*>(data->mData);
    assert(user);

    int hash = eina_hash_superfast(user->GetId(), strlen(user->GetId()));
    GraphRequest *userDetailsRequest = nullptr;
    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(me->mUserDetailsRequestList, l, listData) {
        userDetailsRequest = static_cast<GraphRequest*>(listData);
        if (userDetailsRequest->GetHash() == hash) {
            FacebookSession::GetInstance()->ReleaseGraphRequest(userDetailsRequest);
            me->mUserDetailsRequestList = eina_list_remove(me->mUserDetailsRequestList, listData);
            break;
        }
    }

    me->UpdateFriendshipStatus(data);
}

void WhoLikedScreen::UpdateFriendshipStatus(ActionAndData *data) {

    User *user = static_cast<User*>(data->mData);

    Evas_Object *list_item = data->mActionObj;
    switch (user->mFriendshipStatus) {
        case Person::eARE_FRIENDS:
            RefreshButton(list_item, ARE_FRIENDS);
            break;
        case Person::eCAN_REQUEST:
        case Person::eINCOMING_REQUEST:
            RefreshButton(list_item, ADD_FRIEND);
            break;
        case Person::eOUTGOING_REQUEST:
            RefreshButton(list_item, CANCEL_FRIEND);
            break;
        default:
            RefreshButton(list_item, HIDE_BUTTON);
            break;
    }
}

void WhoLikedScreen::on_item_click(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *actionData = static_cast<ActionAndData*>(data);
    if (actionData && actionData->mData) {
        User *user = static_cast<User*>(actionData->mData);
        if(user) {
            if (user->GetId() && !actionData->mAction->IsNestedProfile(user->GetId())) {
                if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_USER_PROFILE) {
                    Utils::OpenProfileScreen(user->GetId());
                }
            }
        }
    }
}

Evas_Object* WhoLikedScreen::CreateUserItem(Evas_Object* parent, ActionAndData *action_data , bool isAddToEnd)
{
    User *user = static_cast<User*>(action_data->mData);
    if (FriendOperation::IsOperationStarted(user->GetId())) {
        return nullptr;
    }

    Evas_Object *list_item = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(WidgetFactory::CreateSimpleWrapper(parent), "who_liked_list_composer");
    action_data->mActionObj = list_item;

    Evas_Object *avatar = elm_image_add(list_item);
    elm_object_part_content_set(list_item, "box_avatar", avatar);
    action_data->UpdateImageLayoutAsync(user->mPicture->mUrl, avatar);
    evas_object_show(avatar);

    elm_object_part_text_set(list_item, "box_name", user->mName);
    if (isAddToEnd) {
        elm_box_pack_end(parent, list_item);
    } else {
        elm_box_pack_start(parent, list_item);
    }
    evas_object_show(list_item);

    elm_object_translatable_part_text_set(list_item, "friend_btn_text", "IDS_FFS_FRIENDS");
    elm_object_translatable_part_text_set(list_item, "add_friend_btn_text", "IDS_ADD_FRIEND");
    elm_object_translatable_part_text_set(list_item, "cancel_friend_btn_text", "IDS_CANCEL");

    elm_object_signal_callback_add(list_item, "mouse,clicked,*", "friend_*", on_friends_button_clicked_cb, action_data);
    elm_object_signal_callback_add(list_item, "mouse,clicked,*", "add_friend_*", ActionBase::on_add_friend_btn_clicked_cb, action_data);
    elm_object_signal_callback_add(list_item, "mouse,clicked,*", "cancel_friend_*", ActionBase::on_cancel_friend_btn_clicked_cb, action_data);
    elm_object_signal_callback_add(list_item, "mouse,clicked,*", "box*", on_item_click, action_data);

    if (user->mFriendshipStatus == Person::eNOT_AVAILABLE) {
        RefreshButton(list_item, HIDE_BUTTON);
        GraphRequest * userDetailsRequest = FacebookSession::GetInstance()->GetUserDetails(user->GetId(), user_details_cb, action_data);
        userDetailsRequest->SetHash(eina_hash_superfast(user->GetId(), strlen(user->GetId())));
        mUserDetailsRequestList = eina_list_append(mUserDetailsRequestList, userDetailsRequest);
    } else {
        UpdateFriendshipStatus(action_data);
    }
    return list_item;
}

void WhoLikedScreen::back_btn_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    if (Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_WHO_LIKED) {
        ScreenBase *screen = static_cast<ScreenBase*>(data);
        if (screen) {
            screen->Pop();
        }
    }
}

void WhoLikedScreen::on_friends_button_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    User *user = static_cast<User*>(action_data->mData);
    assert(user);
    if (user->mFriendshipStatus == Person::eARE_FRIENDS) {
        static PopupMenu::PopupMenuItem context_menu_items[] = {
            { nullptr, "IDS_UNFRIEND", nullptr,
              ActionBase::on_unfriend_friend_btn_clicked_cb, nullptr,
              0, false, false
            },
            { nullptr, "IDS_BLOCK", nullptr,
              ActionBase::on_block_friend_btn_clicked_cb, nullptr,
              1, true, false
            },
        };
        context_menu_items[0].data = action_data;
        context_menu_items[1].data = action_data;

        Evas_Coord y = 0, h = 0;
        evas_object_geometry_get(obj, nullptr, &y, nullptr, &h);
        y += h / 2;
        WhoLikedScreen *me = static_cast<WhoLikedScreen*>(action_data->mAction->getScreen());
        PopupMenu::Show(2, context_menu_items, me->mLayout, false, y);
    }
}

void WhoLikedScreen::Push()
{
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void* WhoLikedScreen::UpdateItem(void *dataForItem, bool isAddToEnd) {
    ActionAndData *actionAndData = static_cast<ActionAndData*>(dataForItem);
    CHECK_RET(actionAndData, nullptr);
    DeleteUIItem(dataForItem);
    actionAndData->mParentWidget = CreateUserItem(GetDataArea(), actionAndData, isAddToEnd);

    return actionAndData;
}

void WhoLikedScreen::RefreshButton(Evas_Object *list_item, ButtonState buttonState) {
    assert(list_item);
    elm_object_signal_emit(list_item, (buttonState == ARE_FRIENDS ? "show.set" : "hide.set"), "friend");
    elm_object_signal_emit(list_item, (buttonState == ADD_FRIEND ? "show.set" : "hide.set"), "add");
    elm_object_signal_emit(list_item, (buttonState == CANCEL_FRIEND ? "show.set" : "hide.set"), "cancel");
}

void WhoLikedScreen::Update(AppEventId eventId, void * data)
{
    switch (eventId) {
    case eFRIEND_REQUEST_CONFIRMED:
    case eFRIEND_REQUEST_CONFIRM_DELETE_ERROR:
    case eUPDATE_UNFRIENDED_USER_STATUS:
        if (data) {
            User *user = mProvider->GetUserById(static_cast<char*>(data));
            if(user) {
                user->mFriendshipStatus = (eventId == eFRIEND_REQUEST_CONFIRMED) ? Person::eARE_FRIENDS : Person::eCAN_REQUEST;
                ActionAndData* actionData = static_cast<ActionAndData*>(FindItem(user, items_comparator_Obj_vs_ActionAndData));
                if (actionData) {
                    user->mFriendshipStatus == Person::eARE_FRIENDS ?
                    RefreshButton(actionData->mActionObj, ARE_FRIENDS) :
                    RefreshButton(actionData->mActionObj, ADD_FRIEND);
                }
            }
        }
        break;
    case eADD_FRIEND_BTN_CLICKED:
    case eCANCEL_FRIEND_REQUEST_BTN_CLICKED:
        if(data) {
            User *user = mProvider->GetUserById(static_cast<char*>(data));
            if (user) {
                user->mFriendshipStatus = (eventId == eADD_FRIEND_BTN_CLICKED) ? Person::eOUTGOING_REQUEST : Person::eCAN_REQUEST;
                ActionAndData* actionData = static_cast<ActionAndData*>(FindItem(user, items_comparator_Obj_vs_ActionAndData));
                if (actionData) {
                    eventId == eADD_FRIEND_BTN_CLICKED ?
                    RefreshButton(actionData->mActionObj, CANCEL_FRIEND) :
                    RefreshButton(actionData->mActionObj, ADD_FRIEND);
                }
            }
        }
        break;
    default:
        TrackItemsProxy::Update(eventId, data);
        break;
    }
}

void WhoLikedScreen::DeleteAllUserDetailsRequests() {
    void * list_data;
    EINA_LIST_FREE(mUserDetailsRequestList, list_data) {
        GraphRequest *userRequest = static_cast<GraphRequest*> (list_data);
        FacebookSession::GetInstance()->ReleaseGraphRequest(userRequest);
    }

}

void WhoLikedScreen::FetchEdgeItems () {
    if (ConnectivityManager::Singleton().IsConnected()) {
        DeleteAllUserDetailsRequests();
        mProvider->EraseData();
        RequestData(true);
        RequestToRebuildAllItems();
    }
}

void WhoLikedScreen::RebuildAllItems () {
    CreateTopItemsAfterPTR();
}
