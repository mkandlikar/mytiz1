/*
 *  SignUpPswdScreen.cpp
 *
 *  Created on: 9th June 2015
 *      Author: Manjunath Raja
 */

#include "Common.h"
#include "ConnectivityManager.h"
#include "CommonScreen.h"
#include "Config.h"
#include "dlog.h"
#include "SignUpPswdScreen.h"
#include "SignUpGenderScreen.h"
#include "SignUpNoConnectScreen.h"
#include "SignupRequest.h"
#include "Utils.h"

#define MAX_PSWD_CHARS 100

SignUpPswdScreen::SignUpPswdScreen(ScreenBase *parentScreen):ScreenBase(parentScreen)  {
    CreateView();
}

SignUpPswdScreen::~SignUpPswdScreen() {
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Destryoing SignUpPasswordScreen");
    evas_object_smart_callback_del(Application::GetInstance()->mConform, "virtualkeypad,state,on", on_screen_keyb_on);
    evas_object_smart_callback_del(Application::GetInstance()->mConform, "virtualkeypad,state,off", on_screen_keyb_off);
}

void SignUpPswdScreen::LoadEdjLayout() {
    mLayout = elm_layout_add(getParentMainLayout());
    elm_layout_file_set(mLayout, Application::mEdjPath, "signup_pswd_screen");
    evas_object_size_hint_weight_set(mLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_show(mLayout);

    evas_object_smart_callback_add(Application::GetInstance()->mConform, "virtualkeypad,state,on", on_screen_keyb_on, this);
    evas_object_smart_callback_add(Application::GetInstance()->mConform, "virtualkeypad,state,off", on_screen_keyb_off, this);
}

void SignUpPswdScreen::on_screen_keyb_on(void *data, Evas_Object *obj, void *event_info)
{
   //Emit the following signal
    SignUpPswdScreen *mescreen = static_cast<SignUpPswdScreen *>(data);
    elm_object_signal_emit(mescreen->mLayout, "belowheader", "Header");
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "EMITTING SIGNAL SignUp Passwd KB ON ");
}
void SignUpPswdScreen::on_screen_keyb_off(void *data, Evas_Object *obj, void *event_info)
{
   //Emit the following signal
    SignUpPswdScreen *mescreen = static_cast<SignUpPswdScreen *>(data);
    elm_object_signal_emit(mescreen->mLayout, "upheader", "Header");
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "EMITTING SIGNAL KB SIGNUP Passwd OFF ON");
}


void SignUpPswdScreen::CreateView() {
    Evas_Object *TmpEvasObj;

    LoadEdjLayout();

    elm_object_translatable_part_text_set(mLayout, "HeaderText", "IDS_SUP_PSWD_TITLE");
    elm_object_translatable_part_text_set(mLayout, "sup_pswd_heading", "IDS_SUP_PSWD_HEADING");
    std::string Info = i18n_get_text("IDS_SUP_PSWD_INFO");
    Info = Utils::ReplaceString(Info, "&", "&amp;");
    elm_object_part_text_set(mLayout, "sup_pswd_info", Info.c_str());

    TmpEvasObj = CreatePasswordInput();
    elm_object_part_content_set(mLayout, "sup_pswd_input", TmpEvasObj);

    elm_object_translatable_part_text_set(mLayout, "sup_pswd_nxt_btn_txt", "IDS_SUP_WC_NEXT");
    elm_object_signal_callback_add(mLayout, "got.a.sup.pnxt.btn.click", "sup_pswd_nxt_btn*",
            (Edje_Signal_Cb)ContinueBtnCb, this);
    elm_object_signal_callback_add(mLayout, "HeaderBack", "HeaderBack",
            (Edje_Signal_Cb)HeaderBackCb, this);
}

Evas_Object *SignUpPswdScreen::CreatePasswordInput()
{

    Evas_Object *InputLayout = elm_layout_add(getParentMainLayout());
    elm_layout_theme_set(InputLayout, "layout", "editfield", "singleline");
    evas_object_size_hint_align_set(InputLayout, EVAS_HINT_FILL, 0.0);
    evas_object_size_hint_weight_set(InputLayout, EVAS_HINT_EXPAND, 0.0);

    mEvasPasswordInput = elm_entry_add(InputLayout);
    elm_entry_single_line_set(mEvasPasswordInput, EINA_TRUE);
    elm_entry_scrollable_set(mEvasPasswordInput, EINA_TRUE);
    elm_entry_password_set(mEvasPasswordInput, EINA_TRUE);
    elm_object_translatable_part_text_set(mEvasPasswordInput, "elm.guide", "IDS_PASSWORD");
    evas_object_smart_callback_add(mEvasPasswordInput, "focused", InputFocusedCb, InputLayout);
    evas_object_smart_callback_add(mEvasPasswordInput, "unfocused", InputUnFocusedCb, InputLayout);
    evas_object_smart_callback_add(mEvasPasswordInput, "changed", InputChangedCb, InputLayout);
    evas_object_smart_callback_add(mEvasPasswordInput, "preedit,changed", InputChangedCb, InputLayout);
    elm_object_part_content_set(InputLayout, "elm.swallow.content", mEvasPasswordInput);

    Evas_Object *InputClearBtn = elm_button_add(InputLayout);
    elm_object_style_set(InputClearBtn, "editfield_clear");
    evas_object_smart_callback_add(InputClearBtn, "clicked", InputClearButtonCb, mEvasPasswordInput);
    elm_object_part_content_set(InputLayout, "elm.swallow.button", InputClearBtn);

    return InputLayout;
}

void SignUpPswdScreen::ContinueBtnCb(void *data, Evas_Object *obj, void *EventInfo) {
    SignUpPswdScreen *self = static_cast<SignUpPswdScreen*>(data);
    std::string Password = elm_entry_entry_get(self->mEvasPasswordInput);
    bool IsValid = false;
    if (!Password.empty() && !isspace(Password.at(0)) && (Password.length() > 5) && (Password.length() <= MAX_PSWD_CHARS)) {
        SignupRequest::Singleton().SetData(SignupRequest::PASSWORD, elm_entry_entry_get(self->mEvasPasswordInput));
        if (ConnectivityManager::Singleton().IsConnected()) {
            CommonScreen::StartSigningUp();
        } else {
            ScreenBase *NewScreen = new SignUpNoConnectScreen(NULL);
            Application::GetInstance()->AddScreen(NewScreen);
            elm_naviframe_item_title_enabled_set(elm_naviframe_top_item_get(NewScreen->getNf()), EINA_FALSE, EINA_FALSE);
        }
        IsValid = true;
    }

    std::string Info;
    if (IsValid) {
        Info = i18n_get_text("IDS_SUP_PSWD_INFO");
        Info = Utils::ReplaceString(Info, "&", "&amp;");
    }

    elm_object_part_text_set(self->mLayout, "sup_pswd_info", Info.c_str());
    elm_object_translatable_part_text_set(self->mLayout, "sup_pswd_error", (!IsValid ? "IDS_SUP_PSWD_ERROR" : " "));
}

void SignUpPswdScreen::InputClearButtonCb(void *data, Evas_Object *obj, void *EventInfo){
    Evas_Object *Input = (Evas_Object *)data;
    elm_entry_entry_set(Input, "");
}

void SignUpPswdScreen::InputChangedCb(void *data, Evas_Object *obj, void *EventInfo){
    Evas_Object *Input = (Evas_Object *)data;
    if (!elm_entry_is_empty(obj) && elm_object_focus_get(obj))
        elm_object_signal_emit(Input, "elm,action,show,button", "");
    else
        elm_object_signal_emit(Input, "elm,action,hide,button", "");
}

void SignUpPswdScreen::InputFocusedCb(void *data, Evas_Object *obj, void *EventInfo){
    Evas_Object *Input = (Evas_Object *)data;
    if (!elm_entry_is_empty(obj)){
        elm_object_signal_emit(Input, "elm,action,show,button", "");
    }
}

void SignUpPswdScreen::InputUnFocusedCb(void *data, Evas_Object *obj, void *EventInfo){
    Evas_Object *Input = (Evas_Object *)data;
    elm_object_signal_emit(Input, "elm,action,hide,button", "");
}

void SignUpPswdScreen::HeaderBackCb(void* Data, Evas_Object* Obj, void* EventInfo) {
    SignUpPswdScreen* Me = static_cast<SignUpPswdScreen*>(Data);
    Me->Pop();
}
