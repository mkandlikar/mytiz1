#include "Application.h"
#include "FriendOperation.h"
#include "FriendsAction.h"
#include "OwnFriendsProvider.h"
#include "SelectUserScreen.h"
#include "User.h"
#include "UserItemWidget.h"
#include "UsersByIdProvider.h"
#include "WidgetFactory.h"

#include <assert.h>

SelectUserScreen::SelectUserScreen(ScreenBase *parentScreen, Eina_List *ids) :
        ScreenBase(parentScreen), TrackItemsProxy(getParentMainLayout(), UsersByIdProvider::GetInstance(),
                go_item_comparator) {
    mScreenId = ScreenBase::SID_SELECT_USER_SCREEN;
    mAction = new FriendsAction(this);
    SetIdentifier("SELECT_USER");

    mLayout = elm_layout_add(getParentMainLayout());
    WidgetFactory::CreateFriendItemEmulator(mLayout, UIRes::GetInstance()->FF_ITEM_FRIEND_TEXT_STYLE, UIRes::GetInstance()->FF_SEARCH_RESULT_WRAP_WIDTH);
    elm_layout_file_set(mLayout, Application::mEdjPath, "select_user_screen");
    elm_object_part_content_set(mLayout, "swallow.list", GetScroller());
    evas_object_show(mLayout);

    UsersByIdProvider::GetInstance()->SetIds(ids);

    RequestData();
}

SelectUserScreen::~SelectUserScreen() {
    delete mAction;
    EraseWindowData();
    UsersByIdProvider::GetInstance()->EraseData();
    WidgetFactory::DeleteFriendItemEmulator();
}

void SelectUserScreen::Push() {
    ScreenBase::Push();
    // Remove standard blue header
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

bool SelectUserScreen::HandleBackButton() {
    return (PopupMenu::Close() || mAction->DeleteConfirmationDialog());
}

void SelectUserScreen::PackToDataArea(Evas_Object *layout, bool isAddToEnd) {
    if (isAddToEnd) {
        elm_box_pack_end(GetDataArea(), layout);
    } else {
        elm_box_pack_start(GetDataArea(), layout);
    }
}

void* SelectUserScreen::CreateItem(void *dataForItem, bool isAddToEnd) {
    User *user = static_cast<User*>(dataForItem);
    UserItemWidget *widget = nullptr;

    if (user) {
        if (FriendOperation::IsOperationStarted(user->GetId())) {
            return nullptr;
        }

        ActionAndData *actionAndData = new ActionAndData(user, mAction);
        widget = new UserItemWidget(actionAndData);
        Evas_Object *layout = widget->CreateLayout(GetDataArea());
        PackToDataArea(layout, isAddToEnd);
    }
    return widget;
}

void* SelectUserScreen::UpdateItem(void *item, bool isAddToEnd) {
    UserItemWidget *widget = static_cast<UserItemWidget*>(item);
    DeleteUIItem(item);

    Evas_Object *layout = NULL;
    if (widget) {
        layout = widget->CreateLayout(GetDataArea());
        widget->GetActionAndData()->mParentWidget = layout;
    }
    if (layout) {
        PackToDataArea(layout, isAddToEnd);
    } else {
        delete widget;
        widget = NULL;
    }
    return widget;
}

void SelectUserScreen::Update( AppEventId eventId, void *data ) {
    // for future implementation
    TrackItemsProxy::Update( eventId, data );
}

void SelectUserScreen::HideItem(void *item) {
    ItemWidgetBase *widget = static_cast<ItemWidgetBase*>(item);

    if (widget) {
        Evas_Object *layout = widget->GetLayout();
        if (layout) {
            evas_object_hide(layout);
            elm_box_unpack(GetDataArea(), layout);
        }
    }
}

void SelectUserScreen::ShowItem(void *item) {
    ItemWidgetBase *widget = static_cast<ItemWidgetBase*>(item);
    if (widget) {
        Evas_Object *layout = widget->GetLayout();
        if (layout) {
            elm_box_pack_start(GetDataArea(), layout);
            evas_object_show(layout);
        }
    }
}

bool SelectUserScreen::IsItemsEqual(void *itemToCompare, void *itemForCompare) {
    assert(itemToCompare && itemForCompare);
    if (!itemToCompare || !itemForCompare) return false;
    UserItemWidget *widget = static_cast<UserItemWidget*>(itemForCompare);
    GraphObject *toCompare = static_cast<GraphObject*> (itemToCompare);
    GraphObject *forCompare = static_cast<GraphObject*> (widget->GetActionAndData()->mData);
    return ( toCompare->GetHash() == forCompare->GetHash() ) &&
            ( strcmp( toCompare->GetId(), forCompare->GetId() ) == 0 );
}

void SelectUserScreen::DeleteUIItem(void* item) {
    HideItem(item);
    ItemWidgetBase *widget = static_cast<ItemWidgetBase*>(item);
    if (widget) {
        widget->DestroyLayout();
    }
}

void SelectUserScreen::DeleteItem(void* item) {
    HideItem(item);
    ItemWidgetBase *widget = static_cast<ItemWidgetBase*>(item);
    if (widget) {
        Evas_Object *layout = widget->GetLayout();
        if (layout) {
            evas_object_del(layout);
        }
    }
    delete widget;
}

void SelectUserScreen::DeleteAll_Prepare() {
    if(GetDataArea()) {
        elm_box_clear(GetDataArea());
    }
}

void SelectUserScreen::DeleteAll_ItemDelete(void* item ) {
    ItemWidgetBase *widget = static_cast<ItemWidgetBase*>(item);
    delete widget;
}

ItemSize* SelectUserScreen::GetItemSize(void* item){
    UserItemWidget *widget = static_cast<UserItemWidget*>(item);
    ItemSize *res = NULL;
    if (widget) {
        ActionAndData *actionAndData = widget->GetActionAndData();
        Evas_Object *layout = widget->GetLayout();
        if(actionAndData && actionAndData->mData && layout) {
            GraphObject *object = static_cast<GraphObject*>(actionAndData->mData);
            Evas_Coord x, y, weight, height;
            evas_object_geometry_get(layout, &x, &y, &weight, &height );
            res = new ItemSize( SAFE_STRDUP(object->GetId()), height, weight );
        }
    }
    return res;
}

char* SelectUserScreen::GetItemId(void *item) {
    assert(item);
    UserItemWidget *widget = static_cast<UserItemWidget*>(item);
    char* res = NULL;
    if (widget) {
        ActionAndData *actionAndData = widget->GetActionAndData();
        if(actionAndData && actionAndData->mData) {
            res = SAFE_STRDUP(actionAndData->mData->GetId());
        }
    }
    return res;
}

void* SelectUserScreen::GetDataForItem(void *item) {
    UserItemWidget *widget = static_cast<UserItemWidget*>(item);
    void *data = NULL;
    if (widget) {
        ActionAndData *actionAndData = widget->GetActionAndData();
        data = actionAndData ? actionAndData->mData : NULL;
    }
    return data;
}

ItemSize* SelectUserScreen::SaveSize(void *item) {
    UserItemWidget *widget = static_cast<UserItemWidget*>(item);
    ItemSize *size = NULL;
    if (widget) {
        ActionAndData *actionAndData = widget->GetActionAndData();
        GraphObject *object = actionAndData ? static_cast<GraphObject*>(actionAndData->mData) : NULL;
        size = object ? static_cast<ItemSize *>(eina_hash_find( m_ItemSizeCache, object->GetId())) : NULL;
        if(!size){
            size = GetItemSize( item );
            if ( size ) {
                if (( size->height != 0 ) && ( size->weight != 0 ) ) {
                    eina_hash_add( m_ItemSizeCache, size->key, size );
                } else {
                    delete_cached_item_size(size);
                    size = NULL;
                }
            }
        }
    }
    return size;
}

ItemSize* SelectUserScreen::GetSize( void *item ) {
    UserItemWidget *widget = static_cast<UserItemWidget*>(item);
    ItemSize *size = NULL;
    if (widget) {
        ActionAndData *actionAndData = widget->GetActionAndData();
        if (actionAndData) {
            GraphObject *object = static_cast<GraphObject*>(actionAndData->mData);
            if(object) {
                size = static_cast<ItemSize* > ( eina_hash_find( m_ItemSizeCache, object->GetId() ) );
            }
        }
    }
    return size;
}

bool SelectUserScreen::go_item_comparator(void *itemToCompare, void *itemForCompare) {
    assert(itemToCompare && itemForCompare);
    if (!itemForCompare || !itemForCompare) return false;

    UserItemWidget *widget = static_cast<UserItemWidget*>(itemForCompare);
    GraphObject *toCompare = static_cast<GraphObject*>(itemToCompare);
    GraphObject *forCompare = static_cast<GraphObject*> (widget->GetActionAndData()->mData);

    return ( toCompare->GetHash() == forCompare->GetHash() ) &&
            ( strcmp( toCompare->GetId(), forCompare->GetId() ) == 0 );
}

