#include "ActionBase.h"
#include "AbstractDataProvider.h"
#include "TrackItemsProxyCommentsAdapter.h"
#include "Log.h"

TrackItemsProxyCommentsAdapter::TrackItemsProxyCommentsAdapter( Evas_Object* parentLayout, AbstractDataProvider *provider, unsigned int headerSize ) :
    TrackItemsProxyAdapter( parentLayout, provider, headerSize, new CommentsItemsCache( this, provider ) )
{
    Log::info("TrackItemsProxyCommentsAdapter::TrackItemsProxyCommentsAdapter");
    m_CurrrenScrollerSize = 0;
    m_PreviousScrollerSize = 0;
    m_SetNewBottomYCoordinate = false;

    m_EcoreJob_ChangeY = nullptr;
    m_EcoreJob_DelFromTop = nullptr;

    m_RequestToDeleteAllItems = false;
    m_IsOffStageItemsBelow = false;
    m_UnblockShiftsTimer = nullptr;
}

CommentsItemsCache* TrackItemsProxyCommentsAdapter::GetItemsCache() {
    return static_cast<CommentsItemsCache*> (m_ItemsCache);
}

TrackItemsProxyCommentsAdapter::~TrackItemsProxyCommentsAdapter() {
    Log::info("TrackItemsProxyCommentsAdapter::~TrackItemsProxyCommentsAdapter");
    RemoveDataAreaResizeCB();
}

void TrackItemsProxyCommentsAdapter::AddDataAreaResizeCB() {
    Log::info("TrackItemsProxyCommentsAdapter::AddDataAreaResizeCB");
    evas_event_callback_add( evas_object_evas_get (GetScroller()), EVAS_CALLBACK_RENDER_FLUSH_PRE, data_area_resize_cb, this );

}

void TrackItemsProxyCommentsAdapter::RemoveDataAreaResizeCB() {
    Log::info("TrackItemsProxyCommentsAdapter::RemoveDataAreaResizeCB");
    if(GetScroller()) {
        void * ptr = evas_event_callback_del_full( evas_object_evas_get (GetScroller()), EVAS_CALLBACK_RENDER_FLUSH_PRE, data_area_resize_cb, this );
        assert(ptr && ptr==this);
    }
};

void TrackItemsProxyCommentsAdapter::change_y_coordinate_cb(void *data) {
    Log::info("TrackItemsProxyCommentsAdapter::change_y_coordinate_cb");

    TrackItemsProxyCommentsAdapter *me = static_cast<TrackItemsProxyCommentsAdapter *> (data);
    if ( me->m_CurrrenScrollerSize >  me->m_PreviousScrollerSize ) {

        if ( me->m_PostponedTopItemsDelete ) {
            if (!me->m_EcoreJob_DelFromTop) {
                me->m_EcoreJob_DelFromTop = ecore_job_add(delete_items_from_top_cb, me);
            }
            me->m_SetNewBottomYCoordinate = true;
            me->m_PostponedTopItemsDelete = false;
        }

        if ( me->m_PostponedBottomItemsDelete ) {
            me->GetItemsCache()->DeleteFromBottom(me->GetMaxWindowSize());
            me->m_PostponedBottomItemsDelete = false;
        }
    }
    else if ( me->m_SetNewBottomYCoordinate && (me->m_CurrrenScrollerSize <  me->m_PreviousScrollerSize) ) {
        Log::info("TrackItemsProxyCommentsAdapter::DeletedItemsFromTop->Compensate Y coordinate for ShiftDown action");
        me->ChangeYCoordinate(0,false);
        me->m_SetNewBottomYCoordinate = false;
    }

    if ( me->m_CurrrenScrollerSize != me->m_PreviousScrollerSize ) {
        me->m_PreviousScrollerSize = me->m_CurrrenScrollerSize;
    }
    me->m_EcoreJob_ChangeY = nullptr;
}

void TrackItemsProxyCommentsAdapter::delete_items_from_top_cb(void *data) {
    TrackItemsProxyCommentsAdapter *me = static_cast<TrackItemsProxyCommentsAdapter *> (data);
    me->GetItemsCache()->DeleteFromTop(me->GetMaxWindowSize());
    me->m_EcoreJob_DelFromTop  = nullptr;
}

void TrackItemsProxyCommentsAdapter::ChangeYCoordinate( int padding, bool isFromTop )
{
    Log::info("TrackItemsProxyCommentsAdapter::ChangeYCoordinate");
    Evas_Coord x, y, w, h;
    if ( padding<0 ) {
        padding = 0;
    }
    elm_scroller_region_get( GetScroller(), &x, &y, &w, &h );
    if ( isFromTop ) {
        elm_scroller_region_bring_in( GetScroller(), x, padding, w, h );
    } else {
    	elm_scroller_region_bring_in( GetScroller(), x, y - padding, w, h );
    }
}

void TrackItemsProxyCommentsAdapter::data_area_resize_cb(void *data, Evas_Object *obj, void *event_info) {
    TrackItemsProxyCommentsAdapter *me = static_cast<TrackItemsProxyCommentsAdapter *> (data);
    if (me) {
        elm_scroller_child_size_get( me->GetScroller(),nullptr,&me->m_CurrrenScrollerSize);
        if ( me->m_CurrrenScrollerSize != me->m_PreviousScrollerSize ) {
            if(!me->m_EcoreJob_ChangeY) {
                me->m_EcoreJob_ChangeY = ecore_job_add(change_y_coordinate_cb, me);
            }
        }
    }
}

bool TrackItemsProxyCommentsAdapter::IsOffStageItemsBelow() {
    return ( GetProvider()->GetDataCount() - 1  >=  GetItemsCache()->m_ItemsWindow.lowerIndex +1  &&
             GetItemsCache()->m_CurrentWindowSize >= GetItemsCache()->m_MaxWindowSize );
}

void TrackItemsProxyCommentsAdapter::CheckOffStageItemsBelow() {
    Log::info( "TrackItemsProxyCommentsAdapter::CheckOffStageItemsBelow = %u",IsOffStageItemsBelow());
    m_IsOffStageItemsBelow = IsOffStageItemsBelow();
}

void TrackItemsProxyCommentsAdapter::AddPresenter( ) {

    Log::info( "TrackItemsProxyCommentsAdapter::AddPresenter");

    if (m_IsOffStageItemsBelow) {
        Log::info( "TrackItemsProxyCommentsAdapter::AddPresenter-> Go to bottom, offstage items are below");
        elm_box_clear( GetDataArea() );
        GetItemsCache()->SetNewIndexesAndWindowSize();//To create new small comments screen (10+1 comments) on bottom
        GetItemsCache()->ShiftDown(true);
        m_RequestToDeleteAllItems = true;
        m_IsOffStageItemsBelow = false;
    }
    else {
        Log::info( "TrackItemsProxyCommentsAdapter::AddPresenter->No offstage items ");
        GetItemsCache()->ShiftDown(true);
    }
    ScrollToEnd();
}

void TrackItemsProxyCommentsAdapter::RemovePresenter(void *item) {
    Log::info( "TrackItemsProxyCommentsAdapter::RemovePresenter");
    if (item) {
        ActionAndData* postData = static_cast<ActionAndData* >(item);
        if(ActionAndData::IsAlive(postData)) {
            DestroyItem( postData, TrackItemsProxyAdapter::items_comparator );
        }
        GetItemsCache()->DecrementWindowSize();
    }
}

void TrackItemsProxyCommentsAdapter::UpdateCommentsWindowItems() {
    Log::info( "TrackItemsProxyCommentsAdapter::UpdateCommentsWindowItems");
    elm_object_scroll_lock_y_set(GetScroller(), true);
    GetItemsCache()->CheckProviderSizeChanged(false);
    DisableMouseUpDownEvents(true);
    TrackItemsProxy::UpdateItemsWindow();
    DisableMouseUpDownEvents(false);
    elm_object_scroll_lock_y_set(GetScroller(), false);
}

void TrackItemsProxyCommentsAdapter::OnScrollerResizeActions() {
    Log::info( "TrackItemsProxyCommentsAdapter::OnScrollerResizeActions");
    if(m_RequestToDeleteAllItems) {
        GetItemsCache()->DeleteAllItemsFromTop();
    }
}

void TrackItemsProxyCommentsAdapter::ScrollToEnd() {
    Evas_Coord x, w, h, scroller_content_h;
    Log::info( "TrackItemsProxyCommentsAdapter::ScrollToEnd");
    elm_scroller_child_size_get( GetScroller(),nullptr,&scroller_content_h);
    elm_scroller_region_get( GetScroller(), &x, nullptr, &w, &h);
    elm_scroller_region_show ( GetScroller(), x, scroller_content_h-(h+1), w, h);
}

void TrackItemsProxyCommentsAdapter::RestartUnblockShiftsTimer() {
    if(m_UnblockShiftsTimer) {
        ecore_timer_del(m_UnblockShiftsTimer);
    }
    m_UnblockShiftsTimer = ecore_timer_add(2.0, unblock_shifts ,this);
}

Eina_Bool TrackItemsProxyCommentsAdapter::unblock_shifts(void *data){
    Log::info( "TrackItemsProxyCommentsAdapter::unblock_shifts");
    TrackItemsProxyCommentsAdapter* adapter = static_cast<TrackItemsProxyCommentsAdapter*>(data);
    adapter->m_PostponedTopItemsDelete = false;
    adapter->m_PostponedBottomItemsDelete = false;
    adapter->m_UnblockShiftsTimer = nullptr;
    return ECORE_CALLBACK_CANCEL;
}

CommentsItemsCache::CommentsItemsCache(TrackItemsProxy* parent, AbstractDataProvider *provider):
    ItemsCache( parent, provider ) {
    m_CurProviderSize = 0;
    m_PrevProviderSize = 0;
}

TrackItemsProxyCommentsAdapter* CommentsItemsCache::GetParent() {
    return static_cast<TrackItemsProxyCommentsAdapter*> (m_Parent);
}

bool CommentsItemsCache::ShiftDown(bool isPreCreate) {

    if ( (GetParent()-> m_RequestToDeleteAllItems ||
          GetParent()-> m_PostponedTopItemsDelete ) && !isPreCreate
    ) {
        Log::info("CommentsItemsCache::ShiftDown: unable to shift down");
        return true;
    }

    bool isItFirstAdding = (m_ItemsWindowStack->GetSize() == 0 && isPreCreate == false);

    unsigned int itemsToAdd = m_ItemsStep;

    CheckProviderSizeChanged(true);
    if (isItFirstAdding) {
        itemsToAdd = m_CurProviderSize;
        m_CurrentWindowSize = m_CurProviderSize;
        Log::info("CommentsItemsCache::ShiftDown->isItFirstAdding:WinSize = %u", m_CurrentWindowSize);
    }

    if (isPreCreate) {
        itemsToAdd = 1;
        if (GetParent()->m_IsOffStageItemsBelow) {
            // offstage items are presented
            m_CurrentWindowSize = m_ItemsStep;
            itemsToAdd = m_ItemsStep + itemsToAdd;//10(usual step)+1(added comment)
        }
        Log::info("CommentsItemsCache::ShiftDown->Creates %u items on bottom", itemsToAdd);
    }

    Log::info("CommentsItemsCache::ShiftDown to add %u", itemsToAdd);

    Log::info("CommentsItemsCache::ShiftDown->DataCount =  %u",  GetParent()->GetProvider()->GetDataCount());
    Log::info("CommentsItemsCache::ShiftDown->m_ItemsWindowStack =  %u", m_ItemsWindowStack->GetSize());
    Log::info("CommentsItemsCache::ShiftDown->upperIndex %d", m_ItemsWindow.upperIndex);
    Log::info("CommentsItemsCache::ShiftDown->lowerIndex %d", m_ItemsWindow.lowerIndex);

    if ( (m_ItemsWindow.lowerIndex + 1) >= GetParent()->GetProvider()->GetDataCount() )
    {
        Log::info( "CommentsItemsCache::ShiftDown Not even 1 item on bottom");
        return false;
    }

    while ( itemsToAdd > 0 ) {
        ++m_ItemsWindow.lowerIndex;
        void* itemToCreate = eina_array_data_get( GetParent()->GetProvider()->GetData(), m_ItemsWindow.lowerIndex );
        void *item = GetParent()->CreateItem( itemToCreate, true );
        if ( item ) {
            m_ItemsWindowStack->PushToEnd( item );
            --itemsToAdd;
        } else {
            Log::info( "CommentsItemsCache::cannot create an item." );
        }

        if ( (m_ItemsWindow.lowerIndex + 1) >= GetParent()->GetProvider()->GetDataCount() ) break;
    }
    if ( (m_ItemsWindow.upperIndex == INVALID_WINDOW_INDEX) && (m_ItemsWindow.lowerIndex != INVALID_WINDOW_INDEX) ) {
        Log::info( "m_ItemsWindow.upperIndex is 0" );
        m_ItemsWindow.upperIndex = 0;
    }

    if (isPreCreate) {
        NormalizeCurrentWindowSize(1);//
    }

    if (isItFirstAdding) {
        GetParent()->ChangeYCoordinate( 0, true );
    }
    else {
        GetParent()->m_PostponedTopItemsDelete = true;//Delete top items after scroller will be resized
        GetParent()->RestartUnblockShiftsTimer();
    }

    Log::info("CommentsItemsCache::ShiftDown->after items added upperIndex = %d", m_ItemsWindow.upperIndex);
    Log::info("CommentsItemsCache::ShiftDown->after items added lowerIndex = %d", m_ItemsWindow.lowerIndex);

    return true;
}

bool CommentsItemsCache::ShiftUp() {

    if (GetParent()-> m_RequestToDeleteAllItems ||
        GetParent()-> m_PostponedBottomItemsDelete
    ) {
        Log::info("CommentsItemsCache::ShiftUp: unable to shift up");
        return true;
    }

    unsigned int itemsToAdd = m_ItemsStep;
    unsigned int addedItems = 0;

    Log::info("CommentsItemsCache::ShiftUp Request to add %u", itemsToAdd);

    Log::info("CommentsItemsCache::ShiftUp->DataCount =  %u",  GetParent()->GetProvider()->GetDataCount());
    Log::info("CommentsItemsCache::ShiftUp->m_ItemsWindowStack =  %u", m_ItemsWindowStack->GetSize());
    Log::info("CommentsItemsCache::ShiftUp->upperIndex %d", m_ItemsWindow.upperIndex);
    Log::info("CommentsItemsCache::ShiftUp->lowerIndex %d", m_ItemsWindow.lowerIndex);

    CheckProviderSizeChanged(false);

    if ( m_ItemsWindow.upperIndex <= 0 ) {//do not possibly to add even 1 item
        Log::info( "CommentsItemsCache::do not possibly to add even 1 item" );
        GetParent()->EnableRequestData(true);
        return false;
    }

    while ( itemsToAdd > 0 ) {
        Log::info( "CommentsItemsCache::ShiftUp AddSingleItem" );
        --m_ItemsWindow.upperIndex;
        void* itemToCreate = eina_array_data_get( GetParent()->GetProvider()->GetData(), m_ItemsWindow.upperIndex );
        void *item = GetParent()->AddToCreate( itemToCreate, false );
        if ( item ) {
            m_ItemsWindowStack->PushToBegin( item );
            --itemsToAdd;
            ++addedItems;
        } else {
            Log::info( "CommentsItemsCache:: cannot create an item." );
        }

        if ( m_ItemsWindow.upperIndex  <= 0 ) break;
    }
    if ( (m_ItemsWindow.lowerIndex == INVALID_WINDOW_INDEX) && (m_ItemsWindow.upperIndex != INVALID_WINDOW_INDEX) ) {
        Log::info( "CommentsItemsCache:: lowerIndex is 0" );
        m_ItemsWindow.lowerIndex = 0;
    }

    NormalizeCurrentWindowSize(addedItems);

    GetParent()->m_PostponedBottomItemsDelete = true;//Delete bottom items after scroller will be resized
    GetParent()->RestartUnblockShiftsTimer();

    Log::info("CommentsItemsCache::ShiftUp->after items added upperIndex = %d", m_ItemsWindow.upperIndex);
    Log::info("CommentsItemsCache::ShiftUp->after items added lowerIndex = %d", m_ItemsWindow.lowerIndex);

    return true;
}

void CommentsItemsCache::DeleteFromTop ( unsigned int limit ) {

    Log::info( "CommentsItemsCache::DeleteFromTop %d", m_ItemsWindowStack->GetSize() > limit ? m_ItemsWindowStack->GetSize() - limit : 0  );
    while ( m_ItemsWindowStack->GetSize() > limit ) {
        void *upperItemToDelete = m_ItemsWindowStack->PopFromBegin();
        if ( !upperItemToDelete ) {
            // It must not be happen
            Log::info( "CommentsItemsCache::DeleteFromTop: DoubleSidedStack returned NULL item." );
            assert( false );
            return;
        }
        GetParent()->RemoveRectangle( true );
        GetParent()->AddToDelete( upperItemToDelete );
        ++m_ItemsWindow.upperIndex;
    }
}

void CommentsItemsCache::DeleteAllItemsFromTop () {

    Log::info( "CommentsItemsCache::DeleteAllItemsFromTop");
    GetParent()->DeactivateRectangleCb();
    unsigned int limit = GetParent()->GetMaxWindowSize();
    while ( limit > 0 ) {
        void *upperItemToDelete = m_ItemsWindowStack->PopFromBegin();
        if ( !upperItemToDelete ) {
            // It must not be happen
            Log::info( "CommentsItemsCache::DeleteFromTop: DoubleSidedStack returned NULL item." );
            assert( false );
            GetParent()->ActivateRectangleCb();
            return;
        }
        GetParent()->RemoveRectangle( true );
        GetParent()->AddToDelete( upperItemToDelete );
        limit--;
    }
    GetParent()->m_RequestToDeleteAllItems = false;
    GetParent()->ActivateRectangleCb();
}

void CommentsItemsCache::DeleteFromBottom( unsigned int limit ) {

    Log::info( "CommentsItemsCache::DeleteFromBotom %d", m_ItemsWindowStack->GetSize() > limit ? m_ItemsWindowStack->GetSize() - limit : 0 );
    if ( m_ItemsWindowStack->GetSize() > limit ) {

        while ( m_ItemsWindowStack->GetSize() > limit ) {
            void *downItemToDelete = m_ItemsWindowStack->PopFromEnd();
            if ( !downItemToDelete ) {
                // It must not be happen
                Log::info( "CommentsItemsCache: DoubleSidedStack returned NULL item." );
                assert( false );
                return;
            }
            GetParent()->RemoveRectangle( false );
            GetParent()->DeleteItem( downItemToDelete );
           --m_ItemsWindow.lowerIndex;
        }
    }
}

bool CommentsItemsCache::SetNewProviderSize() {
    Log::info( "CommentsItemsCache::SetNewProviderSize");
    if(m_Provider->GetDataCount() != m_CurProviderSize) {
        m_PrevProviderSize = m_CurProviderSize;
        m_CurProviderSize = m_Provider->GetDataCount();
        Log::info( "CommentsItemsCache::SetNewProviderSize new = %u, old = %u", m_CurProviderSize, m_PrevProviderSize );
        return true;
    }
    return false;
}

void CommentsItemsCache::CheckProviderSizeChanged(bool disableNormalization) {
    Log::info( "CommentsItemsCache::CheckProviderSizeChanged->Normalization is %s ", disableNormalization ? "disabled" : "enabled" );
    if(SetNewProviderSize()) {
        if (m_CurProviderSize > m_PrevProviderSize && !disableNormalization) {
            Log::info( "CommentsItemsCache::CheckProviderSizeChanged->added items in provider = %u", m_CurProviderSize - m_PrevProviderSize);
            Log::info( "CommentsItemsCache::CheckProviderSizeChanged->comments/replies in progress = %u", eina_list_count(GetParent()->GetProvider()->GetOperationPresenters()));
            int shift_value = m_CurProviderSize - m_PrevProviderSize;
            Log::info( "CommentsItemsCache::CheckProviderSizeChanged->shift_value for Index Normalization = %d", shift_value);
            if (shift_value>0) {
                NormalizeWindowIndex(false, shift_value);
            }
        }
    }
}

void CommentsItemsCache::DecrementWindowSize() {
    if(!GetParent()->IsOffStageItemsBelow()) {
        m_CurrentWindowSize>0 ? m_CurrentWindowSize--: m_CurrentWindowSize=0 ;
        Log::info( "CommentsItemsCache::DecrementWindowSize = %u",m_CurrentWindowSize);
    }
}

void CommentsItemsCache::NormalizeCurrentWindowSize(unsigned int itemsToAdd) {
    Log::info( "CommentsItemsCache::NormalizeCurrentWindowSize->added items %u",itemsToAdd);
    if ( m_CurrentWindowSize < m_MaxWindowSize ) {
        m_CurrentWindowSize += itemsToAdd;
        if ( m_CurrentWindowSize > m_MaxWindowSize ) {
            m_CurrentWindowSize = m_MaxWindowSize;
        }
        Log::info("CommentsItemsCache::NormalizeCurrentWindowSize->m_CurrentWindowSize = %u", m_CurrentWindowSize);
        Log::info("CommentsItemsCache::NormalizeCurrentWindowSize->m_MaxWindowSize = %u", m_MaxWindowSize);
    }
}

void CommentsItemsCache::SetNewIndexesAndWindowSize() {
    Log::info( "CommentsItemsCache::SetNewIndexesAndWindowSize");
    m_CurrentWindowSize = 0;
    m_ItemsWindow.upperIndex = GetParent()->GetProvider()->GetDataCount() - m_ItemsStep - 1;
    m_ItemsWindow.lowerIndex = m_ItemsWindow.upperIndex;
}
