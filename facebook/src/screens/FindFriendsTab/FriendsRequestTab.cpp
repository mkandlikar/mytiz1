#include "ConnectivityManager.h"
#include "FbRespondGetFriendRequests.h"
#include "FbRespondGetPeopleYouMayKnow.h"
#include "FindFriendsScreen.h"
#include "FriendRequestsBatchProvider.h"
#include "FriendsRequestTab.h"
#include "Log.h"
#include "OwnFriendsProvider.h"
#include "PeopleYouMayKnowCarousel.h"
#include "PresenterHelpers.h"
#include "ProfileScreen.h"
#include "Separator.h"
#include "User.h"
#include "UsersDetailInfoProvider.h"
#include "Utils.h"
#include "WidgetFactory.h"

#include <assert.h>

const unsigned int FriendsRequestTab::DEFAULT_FRIEND_WND_SIZE = 50;
const unsigned int FriendsRequestTab::DEFAULT_FRIEND_WND_STEP = 10;
const double FriendsRequestTab::BADGE_UPDATE_TIMEOUT = 30.0;

FriendsRequestTab::FriendsRequestTab(ScreenBase *parentScreen, bool isFindFriends) :
        ScreenBase(parentScreen),
        TrackItemsProxyAdapter(nullptr, FriendRequestsBatchProvider::GetInstance()),
        mIsScreenActive(false)
{
    mFfScreen = nullptr;
    mScreenId = ScreenBase::SID_FRIENDS_REQUESTS;
    mFriendsSeparatorItem = nullptr;
    SetIdentifier("FRIENDS_REQUEST_TAB");
    mAction = new FriendsAction(this);
    mLayout = WidgetFactory::CreateLayoutByGroup(getParentMainLayout(), "ff_request_page");
    ApplyScroller(mLayout, true);
    elm_object_part_content_set(mLayout, "request_list", GetScroller());
    if (isFindFriends) {
        elm_object_signal_emit(mLayout, "hide.ff.set", "spacer");
    } else {
        elm_object_translatable_part_text_set(mLayout, "ff_text", "IDS_FIND_FRIENDS_BTN");
        elm_object_signal_callback_add(mLayout, "mouse,clicked,*", "ff*", on_find_friends_clicked_cb, this);
        evas_object_smart_callback_add( GetScroller(), "scroll", hide_ff_btn_cb, this );
        evas_object_smart_callback_add( GetScroller(), "edge,top", show_ff_btn_cb, this );
        elm_object_signal_emit(mLayout, "show.set", "spacer");
    }

    if(!isFindFriends) {
        AppEvents::Get().Subscribe(eRESUME_EVENT, this);
        AppEvents::Get().Subscribe(ePAUSE_EVENT, this);
        AppEvents::Get().Subscribe(eINTERNET_CONNECTION_CHANGED, this);
        AppEvents::Get().Subscribe(eUPDATE_FIND_FRIENDS_REQUESTS_COUNT, this);
    }
    AppEvents::Get().Subscribe(eMAINSCREEN_TAB_CHANGED, this);

    AppEvents::Get().Subscribe(ePYMK_ITEM_DELETE_EVENT, this);

    AppEvents::Get().Subscribe(eFR_STATUS_CHANGE_EVENT, this);
    AppEvents::Get().Subscribe(eFR_ITEM_DELETE_EVENT, this);
    AppEvents::Get().Subscribe(eFRIEND_REQUEST_CONFIRM_DELETE_ERROR, this);
    AppEvents::Get().Subscribe(eREQUEST_FOR_ERASE_EVENT, this);
    AppEvents::Get().Subscribe(eADD_NEW_FRIEND_REQUESTS, this);
    AppEvents::Get().Unsubscribe(eDATA_CHANGES_EVENT, this);

    AppEvents::Get().Subscribe(eADD_FRIEND_BTN_CLICKED, this);
    AppEvents::Get().Subscribe(eCANCEL_FRIEND_REQUEST_BTN_CLICKED, this);
    AppEvents::Get().Subscribe(eDELETE_FRIEND_REQUEST_BTN_CLICKED, this);
    AppEvents::Get().Subscribe(eCONFIRM_FRIEND_REQUEST_BTN_CLICKED, this);

    SetPostponedItemDeletion( false );
    EnableBigConnectionErrorWidget( false );
    TrackItemsProxy::ProxySettings *settings = GetProxySettings();
    settings->wndSize = DEFAULT_FRIEND_WND_SIZE;
    settings->wndStep = DEFAULT_FRIEND_WND_STEP;
    SetProxySettings( settings );

    mBadgeUpdateTimer = nullptr;
    mRequest = nullptr;

    mPostponedPostFriendRequestsWasSeen = false;
    mRedrawJob = nullptr;
    mUpdateWithDefaultEvenetJob = nullptr;

    if(!isFindFriends) {
        if(FriendRequestsBatchProvider::GetInstance()->GetDataCount() == 0) {
            RequestData(false);//will be called after Force Close, to upload FriendRequests
            //onto first launch provider is already uploaded with InitializationUploader
        }
        StartBadgeUpdateByTimer();
    }
    ShiftDown();
}

FriendsRequestTab::~FriendsRequestTab()
{
    delete mAction;
    if (mBadgeUpdateTimer) {
        ecore_timer_del(mBadgeUpdateTimer);
    }
    if (mRedrawJob) {
        ecore_job_del(mRedrawJob);
    }
    if (mUpdateWithDefaultEvenetJob) {
        ecore_job_del(mUpdateWithDefaultEvenetJob);
    }
    if(mRequest) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(mRequest);
    }
    EraseWindowData();
}

void FriendsRequestTab::FetchEdgeItems()
{
    Log::info("FriendsRequestTab::FetchEdgeItems");
    if (ConnectivityManager::Singleton().IsConnected()) {
        Log::info("FriendsRequestTab::FetchEdgeItems->RequestData");
        FriendRequestsBatchProvider::GetInstance()->SetMissCacheRetrive();
        FriendRequestsBatchProvider::GetInstance()->EraseData();
        RequestData(false);
    }
}

void* FriendsRequestTab::CreateItem(void *dataForItem, bool isAddToEnd)
{
    ActionAndData *data = nullptr;
    if (dataForItem) {
        GraphObject *check = static_cast<GraphObject*>(dataForItem);
        switch (check->GetGraphObjectType()) {
        case GraphObject::GOT_FRIEND_REQUEST: {
            User *user = static_cast<User*>(dataForItem);
            if (user) {
                if(OwnFriendsProvider::GetInstance()->IsFriendInFriendsArray(user->GetId())) {
                    Log::info("FriendsRequestTab::CreateItem->the same item should be removed from friends list");
                    OwnFriendsProvider::GetInstance()->DeleteItemById(user->GetId());
                    AppEvents::Get().Notify(eFR_REQUEST_FOR_USER_IN_FRIENDS, user);// to update FriendsTab UI
                }
                data = new ActionAndData(user, mAction);
            }
            break;
        }
        case GraphObject::GOT_PEOPLE_YOU_MAY_KNOW: {
            if(FindItem(check, items_comparator_Obj_vs_ActionAndData)){
                Log::info("FriendsRequestTab::CreateItem->Duplicated item was found");
                return nullptr;
            }
            FbRespondGetPeopleYouMayKnow::FriendableUser *user = static_cast<FbRespondGetPeopleYouMayKnow::FriendableUser*>(dataForItem);
            if (user) {
                data = new ActionAndData(user, mAction);
            }
            break;
        }
        case GraphObject::GOT_SEPARATOR: {
            Separator *separator = static_cast<Separator*>(dataForItem);
            data = new ActionAndData(separator, mAction);
            switch (separator->GetSeparatorType()) {
            case Separator::FRIENDS_SEPARATOR:
                data->mParentWidget = WidgetFactory::CreateSeparatorItem(GetDataArea(), isAddToEnd, "IDS_FFS_FRIEND_REQUESTS", true);
                mFriendsSeparatorItem = data->mParentWidget;
                SetFriendRequestsBadge();
                break;
            case Separator::PEOPLE_YOU_MAY_KNOW_SEPARATOR:
                data->mParentWidget = WidgetFactory::CreateSeparatorItem(GetDataArea(), isAddToEnd, "IDS_PPL_U_MAY_KNOW", true);
                break;
            case Separator::FRIENDS_NOREQUEST_SEPARATOR:
                data->mParentWidget = NoResultItem( isAddToEnd );
                break;
            default:
                Log::error("FriendsRequestTab::CreateItem->Wrong separator type");
                assert( false );
                break;
            }

            if ( !data->mParentWidget ) {
                delete data;
                data = nullptr;
            }
            return data;
            //break;  //end of case GOT_SEPARATOR
        }
        default:
            Log::error("FriendRequestTab::CreateItem->Wrong GraphObject");
            assert( false );
            break;
        }

        data->mParentWidget = FriendRequestsGet(data, isAddToEnd);
    }
    return data;
}

Evas_Object *FriendsRequestTab::FriendRequestsGet(void *data, bool isAddToEnd)
{
    assert(data);
    ActionAndData *action_data = static_cast<ActionAndData*>(data);
    assert(action_data->mData);
    Log::info("FriendsRequestTab::FriendRequestsGet");

    Evas_Object *content = WidgetFactory::CreateSimpleWrapper(GetDataArea());

    switch (action_data->mData->GetGraphObjectType()) {
    case GraphObject::GOT_FRIEND_REQUEST: {
        Log::error("FriendsRequestTab::FriendRequestsGet->GOT_FRIEND_REQUEST");
        CreateRequestItem(content, action_data);
    }
        break;
    case GraphObject::GOT_PEOPLE_YOU_MAY_KNOW: {
        FbRespondGetPeopleYouMayKnow::FriendableUser *user = static_cast<FbRespondGetPeopleYouMayKnow::FriendableUser*>(action_data->mData);
        if (user->mFriendRequestStatus == eUSER_REQUEST_UNKNOWN) {
            Log::error("FriendsRequestTab::FriendRequestsGet->GOT_PEOPLE_YOU_MAY_KNOW: UnknownCheloveck");
            CreatePYMKItem(content, action_data);
        }
    }
        break;
    default:
        break;
    }

    if (isAddToEnd) {
        elm_box_pack_end(GetDataArea(), content);
    } else {
        elm_box_pack_start(GetDataArea(), content);
    }
    evas_object_show(content);

    return content;
}

void FriendsRequestTab::CreateRequestItem(Evas_Object* parent, ActionAndData *action_data)
{
    assert(action_data);
    assert(action_data->mData);
    User *user = static_cast<User*>(action_data->mData);

    Evas_Object *mFrRequestComposer = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(parent, "ff_request_list_mutual");

    action_data->mActionObj = mFrRequestComposer;

    Evas_Object *avatar = elm_image_add(mFrRequestComposer);
    elm_object_part_content_set(mFrRequestComposer, "avatar", avatar);
    action_data->UpdateImageLayoutAsync(user->mPicture->mUrl, avatar, ActionAndData::EImage, Post::EFromPicturePath);
    evas_object_show(avatar);

    elm_object_part_text_set(mFrRequestComposer, "name", user->mName);

    RefreshRequestItem(action_data);

    elm_object_translatable_part_text_set(mFrRequestComposer, "add_btn_text", "IDS_CONFIRM_FRIEND_REQUEST");
    elm_object_translatable_part_text_set(mFrRequestComposer, "del_btn_text", "IDS_DELETE_FRIEND_REQUEST");

    elm_object_signal_callback_add(mFrRequestComposer, "mouse,clicked,*", "add_btn*", (Edje_Signal_Cb) ActionBase::on_confirm_friend_rq_btn_clicked_cb, action_data);
    elm_object_signal_callback_add(mFrRequestComposer, "mouse,clicked,*", "del_btn*", (Edje_Signal_Cb) ActionBase::on_delete_friend_rq_btn_clicked_cb, action_data);
    elm_object_signal_callback_add(mFrRequestComposer, "mouse,clicked,*", "global_spacer", on_request_item_click, action_data);
}

void FriendsRequestTab::RefreshRequestItem(ActionAndData *action_data)
{
    assert(action_data);
    assert(action_data->mData);
    User *user = FriendRequestsBatchProvider::GetInstance()->GetFrDataById(action_data->mData->GetId());
    if(!user) {
        return;
    }

    int count = user->mMutualFriendsCount;
    std::string format;
    format.append(Utils::FormatBigInteger(count).c_str());
    format.append(" %s");

    if (count == 1) {
        FRMTD_TRNSLTD_PART_TXT_SET1(action_data->mActionObj, "mutuals", format.c_str(), "IDS_SINGLE_MUTUAL_FRIEND");
    } else if (count > 0) {
        FRMTD_TRNSLTD_PART_TXT_SET1(action_data->mActionObj, "mutuals", format.c_str(), "IDS_MUTUAL_FRIENDS");
    }

    if (user->mFriendRequestStatus == eUSER_REQUEST_CONFIRMED) {
        elm_object_signal_emit(action_data->mActionObj, "add.set", "");
        elm_object_signal_emit(action_data->mActionObj, "move.name", "");
        elm_object_translatable_part_text_set(action_data->mActionObj, "mutuals", "IDS_YOU_FRIENDS");
    } else if (user->mFriendRequestStatus == eUSER_REQUEST_DELETED) {
        elm_object_signal_emit(action_data->mActionObj, "del.set", "");
        elm_object_signal_emit(action_data->mActionObj, "move.name", "");
        elm_object_translatable_part_text_set(action_data->mActionObj, "mutuals", "IDS_REQUEST_REMOVED");
    } else if (user->mFriendRequestStatus == eUSER_REQUEST_UNKNOWN) {
        elm_object_signal_emit(action_data->mActionObj, "default.set", "");
        elm_object_signal_emit(action_data->mActionObj, "move.name", "revert");
    }
}

Evas_Object *FriendsRequestTab::NoResultItem(bool isAddToEnd)
{
    Log::info("FriendsRequestTab::NoResultItem!!! %d", isAddToEnd);
    Evas_Object *item = WidgetFactory::CreateLayoutByGroup(GetDataArea(), "no_requests_box");
    if (isAddToEnd) {
        elm_box_pack_end(GetDataArea(), item);
    } else {
        elm_box_pack_start(GetDataArea(), item);
    }

    elm_object_translatable_part_text_set(item, "fr_rq_text", "IDS_NO_REQUESTS");

    return item;
}

void FriendsRequestTab::CreatePYMKItem(Evas_Object *parent, ActionAndData *action_data)
{
    assert(action_data);
    assert(action_data->mData);
    FbRespondGetPeopleYouMayKnow::FriendableUser *user = static_cast<FbRespondGetPeopleYouMayKnow::FriendableUser*>(action_data->mData);
    assert(user);

    Evas_Object *mFrPeopleComposer = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(parent, "ff_pumk_list");

    action_data->mActionObj = mFrPeopleComposer;

    Evas_Object *avatar = elm_image_add(mFrPeopleComposer);
    elm_object_part_content_set(mFrPeopleComposer, "item_avatar", avatar);
    action_data->UpdateImageLayoutAsync(user->GetPicSquareWithLogo(), avatar, ActionAndData::EImage, Post::EFromPicturePath);
    evas_object_show(avatar);

    Evas_Object *name_spacer = elm_box_add(mFrPeopleComposer);
    elm_object_part_content_set(mFrPeopleComposer, "item_name_spacer", name_spacer);
    evas_object_size_hint_weight_set(name_spacer, 1, 1);
    evas_object_size_hint_align_set(name_spacer, -1, 0.5);
    evas_object_show(name_spacer);

    Evas_Object *spacer1 = elm_box_add(name_spacer);
    evas_object_size_hint_weight_set(spacer1, 10, 10);
    evas_object_size_hint_align_set(spacer1, -1, -1);
    elm_box_pack_end(name_spacer, spacer1);
    evas_object_show(spacer1);

    Evas_Object *name = elm_label_add(name_spacer);
    evas_object_size_hint_weight_set(name, 1, 1);
    evas_object_size_hint_align_set(name, -1, -1);
    elm_label_wrap_width_set(name, R->FF_SEARCH_RESULT_WRAP_WIDTH);
    elm_label_line_wrap_set(name, ELM_WRAP_WORD);
    elm_box_pack_end(name_spacer, name);
    evas_object_show(name);

    action_data->mChangeableLabel = name;

    RefreshPeopleItem(action_data);

    Evas_Object *spacer2 = elm_box_add(name_spacer);
    evas_object_size_hint_weight_set(spacer2, 10, 10);
    evas_object_size_hint_align_set(spacer2, -1, -1);
    elm_box_pack_end(name_spacer, spacer2);
    evas_object_show(spacer2);

    elm_object_translatable_part_text_set(mFrPeopleComposer, "add_btn_text", "IDS_ADD_FRIEND");
    elm_object_signal_callback_add(mFrPeopleComposer, "mouse,clicked,*", "add_btn*", ActionBase::on_pymk_btn_clicked_cb, action_data);

    elm_object_signal_callback_add(mFrPeopleComposer, "mouse,clicked,*", "item_*", on_pymk_item_click, action_data);
}

void FriendsRequestTab::RefreshPeopleItem(ActionAndData *action_data)
{
    assert(action_data);
    assert(action_data->mData);
    FbRespondGetPeopleYouMayKnow::FriendableUser *user = FriendRequestsBatchProvider::GetInstance()->GetPymkDataById(action_data->mData->GetId());
    if(!user) {
        return;
    }

    assert(action_data->mActionObj);

    const char *ids = "";
    std::string format;
    if (user->mFriendRequestStatus == eUSER_REQUEST_SENT) {
        format = PresenterHelpers::CreateFormatString(SANS_MEDIUM_9298A4_FORMAT, R->FF_SEARCH_RESULT_MUTUAL_TEXT_SIZE);
        ids = "IDS_REQUEST_SENT";
    } else if (user->mFriendRequestStatus == eUSER_REQUEST_CANCELLED) {
        format = PresenterHelpers::CreateFormatString(SANS_MEDIUM_9298A4_FORMAT, R->FF_SEARCH_RESULT_MUTUAL_TEXT_SIZE);
        ids =  "IDS_REQUEST_CANCELLED";
    } else if (user->GetMutualFriendsCount() == 1) {
        format = PresenterHelpers::CreateMutualFriendsFormat(user->GetMutualFriendsCount(), R->FF_SEARCH_RESULT_MUTUAL_TEXT_SIZE);
        ids = "IDS_SINGLE_MUTUAL_FRIEND";
    } else if (user->GetMutualFriendsCount() > 0) {
        format = PresenterHelpers::CreateMutualFriendsFormat(user->GetMutualFriendsCount(), R->FF_SEARCH_RESULT_MUTUAL_TEXT_SIZE);
        ids = "IDS_MUTUAL_FRIENDS";
    }

    std::stringstream formatStream;
    std::unique_ptr<char[]> name_text(WidgetFactory::WrapByFormat2(SANS_REGULAR_000_FORMAT, R->FF_SEARCH_RESULT_NAME_TEXT_SIZE, user->GetName()));
    formatStream << name_text.get();
    if (ids && *ids) {
        formatStream <<  "<br>";
    }
    formatStream << format;
    FRMTD_TRNSLTD_TXT_SET1(action_data->mChangeableLabel, formatStream.str().c_str(), ids);

    if (user->mFriendRequestStatus == eUSER_REQUEST_SENT) {
        elm_object_translatable_part_text_set(action_data->mActionObj, "add_btn_text", "IDS_CANCEL");
    } else {
        elm_object_translatable_part_text_set(action_data->mActionObj, "add_btn_text", "IDS_ADD_FRIEND");
    }
    Utils::RedrawEvasObject(action_data->mActionObj);
}

void FriendsRequestTab::on_item_click(ActionAndData *action_data, bool isPYMKMode)
{
    assert(action_data);
    assert(action_data->mData);
    if (Application::GetInstance()->GetTopScreenId() != SID_PEOPLE_YOU_MAY_KNOW_CAROUSEL) {
        if (action_data->mData->GetId()) {
            UsersDetailInfoProvider::GetInstance()->SetCarouselMode(isPYMKMode);
            if (isPYMKMode) {
                UsersDetailInfoProvider::GetInstance()->SetIds(FriendRequestsBatchProvider::GetInstance()->GetPYMK_Ids());
            } else {
                UsersDetailInfoProvider::GetInstance()->SetIds(FriendRequestsBatchProvider::GetInstance()->GetFR_Ids());
            }
            PeopleYouMayKnowCarousel *ffscreen = new PeopleYouMayKnowCarousel(action_data->mData->GetId());
            Application::GetInstance()->AddScreen(ffscreen);
        }
    }
}

void FriendsRequestTab::on_pymk_item_click(void *data, Evas_Object *obj, const char *emisson, const char *source)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(data);
    on_item_click(action_data, true);
}

void FriendsRequestTab::on_request_item_click(void *data, Evas_Object *obj, const char *emisson, const char *source)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(data);
    on_item_click(action_data, false);
}

void FriendsRequestTab::hide_ff_btn_cb(void *data, Evas_Object *obj, void *event_info)
{
    FriendsRequestTab *screen = static_cast<FriendsRequestTab *>(data);
    assert(screen);
    //
    // @todo Need to reimplement logic later, because of 'scroll' signals is emited
    //       very often and call elm_scroller_region_get may be to light-weight.
    Evas_Coord y;
    elm_scroller_region_get( screen->GetScroller(), nullptr, &y, nullptr, nullptr );
    if ( y > TrackItemsProxy::TINY_SCROLL_PADDING ) {
        elm_object_signal_emit(screen->mLayout, "hide.ff.set", "spacer");
    }
}

void FriendsRequestTab::show_ff_btn_cb(void *data, Evas_Object *obj, void *event_info)
{
    FriendsRequestTab *screen = static_cast<FriendsRequestTab *>(data);
    assert(screen);
    elm_object_signal_emit(screen->mLayout, "show.ff.set", "spacer");
}

void FriendsRequestTab::on_find_friends_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    FriendsRequestTab * me = static_cast<FriendsRequestTab*>(user_data);
    assert(me);
    me->mFfScreen = new FindFriendsScreen(false);
    Application::GetInstance()->AddScreen(me->mFfScreen);
}

Eina_Bool FriendsRequestTab::on_badge_update_timer_cb(void *data) {
    FriendsRequestTab *me = static_cast<FriendsRequestTab *> (data);
    assert(me);
    me->StartGetFriendRequestsCountRequest();
    return ECORE_CALLBACK_RENEW;
}

void FriendsRequestTab::StartGetFriendRequestsCountRequest() {
    if (mRequest) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(mRequest);
    }
    mRequest = FacebookSession::GetInstance()->GetIncomingFriendRequestsCounts(on_friend_requests_count_completed, this);
}

void FriendsRequestTab::RequestForErase() {
    Log::debug("FriendsRequestTab::RequestForErase");
    // it will be updated by any TIP event after, but should be marked to erase
    TrackItemsProxy::UpdateErase();
}

void FriendsRequestTab::UpdateWithDefaultEvent() {
    Log::debug("FriendsRequestTab::UpdateWithDefaultEvent");
    //if screen was already requested for erase
    TrackItemsProxy::Update(eUNDEFINED_EVENT, nullptr);
}

void FriendsRequestTab::DestroyFriendRequest(User *user) {
    Log::debug("FriendsRequestTab::DestroyFriendRequest");
    if(DestroyItemByData(user, TrackItemsProxyAdapter::items_comparator_Obj_vs_ActionAndData)) {
        FriendRequestsBatchProvider::GetInstance()->DeleteItem(user);  //remove from provider
        FriendRequestsBatchProvider::GetInstance()->AppendNewFriendRequestsHeaderItems();
        Redraw();
    }
    else{
        Log::error("FriendsRequestTab::DestroyFriendRequest->Can't delete friend request");
    }

    if(mBadgeUpdateTimer){//to do not wait 30 seconds in case of Confirm/Delete
        StartGetFriendRequestsCountRequest();
        ecore_timer_reset(mBadgeUpdateTimer);
    }
}

void FriendsRequestTab::OnResume() {
    Log::debug("FriendsRequestTab::OnResume");
    UpdateWithDefaultEvent();
}

void FriendsRequestTab::OnAppResume() {// Note: only for FR in MainMenu
    StartBadgeUpdateByTimer();
}

void FriendsRequestTab::OnAppPause() {// Note: only for FR in MainMenu
    StopBadgeUpdateByTimer(); //Don't update by timer while FB4T app isn't active (background or sleep mode).
}

void FriendsRequestTab::OnMainScreenTabChanged(MainScreen::Tab tab) {
    bool wasActive = mIsScreenActive;
    mIsScreenActive = tab == MainScreen::eFriendRequestsTab;
    Log::debug("FriendsRequestTab::OnMainScreenTabChanged %d", tab);
    if (mIsScreenActive && !wasActive) {
        if (FriendRequestsBatchProvider::GetInstance()->mUnreadBadgeNum) {
            PostSeenFriendRequests();
            FriendRequestsBatchProvider::GetInstance()->mUnreadBadgeNum = 0;
        }
        SetFriendRequestsBadge();
    }
}

void FriendsRequestTab::StartBadgeUpdateByTimer() {// Note: only for FR in MainMenu
    if (!mBadgeUpdateTimer) {
        mBadgeUpdateTimer = ecore_timer_add(BADGE_UPDATE_TIMEOUT, on_badge_update_timer_cb, this);
    } else {
        ecore_timer_reset(mBadgeUpdateTimer);
    }
}

void FriendsRequestTab::StopBadgeUpdateByTimer() {// Note: only for FR in MainMenu
    if (mBadgeUpdateTimer) {
        ecore_timer_del(mBadgeUpdateTimer);
        mBadgeUpdateTimer = nullptr;
    }
}

void FriendsRequestTab::scroll_to_top (void *data, Evas_Object *obj, const char *emission, const char *source) {
    FriendsRequestTab *me = static_cast<FriendsRequestTab *> (data);
    Evas_Coord w,h;
    elm_scroller_region_get( me->GetScroller(), nullptr, nullptr, &w, &h );

    me->RequestForErase();
    me->UpdateWithDefaultEvent();
    show_ff_btn_cb(me,nullptr,nullptr);
    elm_scroller_region_bring_in( me->GetScroller(), 0, 0 , w, h );
}

void FriendsRequestTab::Update(AppEventId eventId, void * data)
{
    switch (eventId) {
    case eADD_FRIEND_BTN_CLICKED:
    case eCANCEL_FRIEND_REQUEST_BTN_CLICKED:
        if (data) {
            ActionAndData *actionData =   static_cast<ActionAndData*> ( GetItemById(static_cast<char*>(data)) );
            if (actionData) {
                RefreshPeopleItem(actionData);
            }
        }
        break;
    case eDELETE_FRIEND_REQUEST_BTN_CLICKED:
    case eCONFIRM_FRIEND_REQUEST_BTN_CLICKED:
        if (data) {
            ActionAndData *actionData =   static_cast<ActionAndData*> ( GetItemById(static_cast<char*>(data)) );
            if (actionData) {
                RefreshRequestItem(actionData);
            }
        }
        break;
    case eFR_STATUS_CHANGE_EVENT: {
        Log::debug("FriendsRequestTab::Update->eFR_STATUS_CHANGE_EVENT");
        GraphObject *graphObject = static_cast<GraphObject*>(data);
        User *user = dynamic_cast<User*>(graphObject);
        if (user) {
            CustomUpdateItem(eFR_ITEM_UPDATE_EVENT, user->GetId());
        }
        break;
    }
    case eFR_ITEM_UPDATE_EVENT:
        Log::debug("FriendsRequestTab::Update->eFR_ITEM_UPDATE_EVENT");
        if (data) {
            ActionAndData *actionData = static_cast<ActionAndData*>(data);
            RefreshRequestItem(actionData);
        }
        break;
    case ePYMK_ITEM_DELETE_EVENT:
        Log::debug("FriendsRequestTab::Update->ePYMK_ITEM_DELETE_EVENT");
        if (data) {
            FbRespondGetPeopleYouMayKnow::FriendableUser *pymkUser = FriendRequestsBatchProvider::GetInstance()->GetPymkDataById(static_cast<char*>(data));
            if (pymkUser) {
                DestroyItemByData(pymkUser, TrackItemsProxyAdapter::items_comparator_Obj_vs_ActionAndData);
                FriendRequestsBatchProvider::GetInstance()->DeleteItem(pymkUser);  //remove from provider
            }
        }
        break;
    case eFR_ITEM_DELETE_EVENT:
    case eFRIEND_REQUEST_CONFIRM_DELETE_ERROR:
        Log::debug("FriendsRequestTab::Update->eFR_ITEM_DELETE_EVENT");
        if (data) {
            User* friendRequest = FriendRequestsBatchProvider::GetInstance()->GetFrDataById(static_cast<char*>(data));
            if (friendRequest) {
                DestroyFriendRequest(friendRequest);
            }
        }
        break;
    case eRESUME_EVENT:
        OnAppResume();
        break;
    case ePAUSE_EVENT:
        OnAppPause();
        break;
    case eMAINSCREEN_TAB_CHANGED:
        assert(data);
        OnMainScreenTabChanged(*(static_cast<MainScreen::Tab*> (data)));
        break;
    case eINTERNET_CONNECTION_CHANGED:// only for FR Tab in Main Menu
        if (ConnectivityManager::Singleton().IsConnected()){
            if(mPostponedPostFriendRequestsWasSeen) {
                PostSeenFriendRequests();
            }
            StartBadgeUpdateByTimer();
        } else {
            StopBadgeUpdateByTimer();
        }
        break;
    case eREQUEST_FOR_ERASE_EVENT:
        RequestForErase();
        if(!mUpdateWithDefaultEvenetJob) {
            mUpdateWithDefaultEvenetJob = ecore_job_add(update_ui_with_default_event, this);
        }
        break;
    case eADD_NEW_FRIEND_REQUESTS:
        if(!mRedrawJob) {
            mRedrawJob = ecore_job_add(redraw_ui, this);
        }
        SetFriendRequestsBadge();
        break;
    case eUPDATE_FIND_FRIENDS_REQUESTS_COUNT:
        SetFriendRequestsBadge();
        break;
    default:
        TrackItemsProxy::Update(eventId, data);
        break;
    }
}

void FriendsRequestTab::redraw_ui(void * data) {
    FriendsRequestTab *me = static_cast<FriendsRequestTab *> (data);
    Log::debug("FriendsRequestTab::redraw_ui");
    me->Redraw();
    me->mRedrawJob = nullptr;
}

void FriendsRequestTab::update_ui_with_default_event(void * data) {
    FriendsRequestTab *me = static_cast<FriendsRequestTab *> (data);
    Log::debug("FriendsRequestTab::update_ui_with_default_event");
    me->UpdateWithDefaultEvent();
    me->mUpdateWithDefaultEvenetJob = nullptr;
}

void FriendsRequestTab::UpdateFriendsRequests(int unreadCount, int totalCount) {
    Log::debug("FriendsRequestTab::UpdateFriendsRequests");

    bool clearInvalidFRs = totalCount - unreadCount < FriendRequestsBatchProvider::GetInstance()->GetRequestsTotalCount() -
                                                      FriendRequestsBatchProvider::GetInstance()->mUnreadBadgeNum;

    if (unreadCount > FriendRequestsBatchProvider::GetInstance()->mUnreadBadgeNum ||
        totalCount > FriendRequestsBatchProvider::GetInstance()->GetRequestsTotalCount() ||
        clearInvalidFRs) {
        if (clearInvalidFRs) {
            FriendRequestsBatchProvider::GetInstance()->SetDeleteInvalidFRs();
        }
        RequestData(false);
        HideProgressBar();
    }

    if (mIsScreenActive && unreadCount) {
        PostSeenFriendRequests();
        FriendRequestsBatchProvider::GetInstance()->mUnreadBadgeNum = 0;
    } else {
        FriendRequestsBatchProvider::GetInstance()->mUnreadBadgeNum = unreadCount;
    }
}

void FriendsRequestTab::SetFriendRequestsBadge() {
    if (mFriendsSeparatorItem) {
        int counter = FriendRequestsBatchProvider::GetInstance()->GetRequestsTotalCount();
        if (counter) {
            if (counter > 99) {
                elm_object_part_text_set(mFriendsSeparatorItem, "badge_text", "99+");
                elm_object_signal_emit(mFriendsSeparatorItem, "show_badge_big", "");
            } else {
                elm_object_part_text_set(mFriendsSeparatorItem, "badge_text", std::to_string(counter).c_str());
                elm_object_signal_emit(mFriendsSeparatorItem, (counter >= 10 ? "show_badge_big" : "show_badge"), "");
            }
        } else {
            elm_object_signal_emit(mFriendsSeparatorItem, "hide_badge", "");
        }
    }
}

void FriendsRequestTab::PostSeenFriendRequests() {
    Log::debug("FriendsRequestTab::PostSeenFriendRequests");
    if(ConnectivityManager::Singleton().IsConnected())
    {
        GraphRequest *gr = FacebookSession::GetInstance()->PostSeenRequests();
        FacebookSession::GetInstance()->ReleaseGraphRequest(gr,TRUE);// without confirmation
        mPostponedPostFriendRequestsWasSeen = false;
    } else {
        mPostponedPostFriendRequestsWasSeen = true;
    }
}

//CB only for FriendRequests in MainScreen
void FriendsRequestTab::on_friend_requests_count_completed(void* object, char* response, int code)
{
    Log::debug("FriendsRequestTab::on_friend_requests_count_completed");

    FriendsRequestTab *me = static_cast<FriendsRequestTab *> (object);
    FacebookSession::GetInstance()->ReleaseGraphRequest(me->mRequest);

    if (CURLE_OK == code) {
        Log::debug("FriendsRequestTab::on_friend_requests_count_completed->response: %s", response);

        FbRespondGetFriendRequests * friendRequestsCountsResponse = FbRespondGetFriendRequests::createFromJson(response);
        if(friendRequestsCountsResponse && friendRequestsCountsResponse->mSummary) {
            Log::info("FriendsRequestTab::on_friend_requests_count_completed->mUnreadCount=%d", friendRequestsCountsResponse->mSummary->mUnreadCount);
            Log::info("FriendsRequestTab::on_friend_requests_count_completed->mTotalCount=%d", friendRequestsCountsResponse->mSummary->mTotalCount);

            me->UpdateFriendsRequests(friendRequestsCountsResponse->mSummary->mUnreadCount,
                                      friendRequestsCountsResponse->mSummary->mTotalCount );

            FriendRequestsBatchProvider::GetInstance()->SetRequestsTotalCount(friendRequestsCountsResponse->mSummary->mTotalCount);
        } else if (friendRequestsCountsResponse){
            me->UpdateFriendsRequests(0,0);
            FriendRequestsBatchProvider::GetInstance()->SetRequestsTotalCount(0);//response is empty in this case
            FriendRequestsBatchProvider::GetInstance()->mUnreadBadgeNum = 0;
        }

        MainScreen *screen = static_cast<MainScreen*>(me->GetParent());
        assert(screen);
        screen->SetTabbarBadge(MainScreen::eFriendRequestsTab, FriendRequestsBatchProvider::GetInstance()->mUnreadBadgeNum);

        delete friendRequestsCountsResponse;
    } else {
        Log::error("FriendsRequestTab::on_friend_requests_count_completed->Error sending http request, curl code=%d", code);
    }
    free(response);
}
