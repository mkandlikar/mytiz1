#include "ConnectivityManager.h"
#include "CSmartPtr.h"
#include "ErrorHandling.h"
#include "FoundItem.h"
#include "FriendSearchProvider.h"
#include "FriendsSearchTab.h"
#include "OwnProfileScreen.h"
#include "ProfileScreen.h"
#include "Utils.h"
#include "WidgetFactory.h"

UIRes * FriendsSearchTab::R = UIRes::GetInstance();

FriendsSearchTab::FriendsSearchTab(ScreenBase *parentScreen, bool isSelected) :
        ScreenBase(parentScreen),
        TrackItemsProxyAdapter(NULL, FriendSearchProvider::GetInstance())
{
    mAction = new FriendsAction(this);

    mLayout = WidgetFactory::CreateLayoutByGroup(getParentMainLayout(), "ff_search_page");

    mScreenId = ScreenBase::SID_FRIENDS_SEARCH_TAB;
    SetIdentifier("FRIENDS_SEARCH_TAB");
    mSearchEntry = elm_entry_add(mLayout);
    elm_object_part_content_set(mLayout, "entry", mSearchEntry);
    elm_entry_prediction_allow_set(mSearchEntry, EINA_FALSE);
    elm_entry_single_line_set(mSearchEntry, EINA_TRUE);
    elm_entry_scrollable_set(mSearchEntry, EINA_TRUE);
    elm_entry_input_panel_return_key_type_set(mSearchEntry, ELM_INPUT_PANEL_RETURN_KEY_TYPE_SEARCH);
    char *buf = WidgetFactory::WrapByFormat2(SANS_REGULAR_9298A4_FORMAT, R->FF_SEARCH_BTN_TEXT_SIZE, i18n_get_text("IDS_SEARCH_FIELD"));
    if (buf) {
        elm_object_part_text_set(mSearchEntry, "elm.guide", buf);
        delete[] buf;
        buf = NULL;
    }
    elm_entry_text_style_user_push(mSearchEntry, COMMENT_TEXT_STYLE);
    elm_entry_cursor_end_set(mSearchEntry);
    elm_entry_cnp_mode_set(mSearchEntry, ELM_CNP_MODE_PLAINTEXT);
    elm_entry_input_panel_enabled_set(mSearchEntry, EINA_FALSE);
    evas_object_show(mSearchEntry);

    evas_object_smart_callback_add (mSearchEntry, "press", show_input_text_panel, this);
    evas_object_smart_callback_add(mSearchEntry, "activated", on_search_entry_clicked_cb, this);

    elm_object_translatable_part_text_set(mLayout, "search_btn_text", "IDS_FFS_SEARCH");
    elm_object_translatable_part_text_set(mLayout, "nodata_view", "IDS_NO_RESULTS_FOUND");

    elm_object_signal_callback_add(mLayout, "mouse,clicked,*", "search_btn*", (Edje_Signal_Cb) on_search_entry_clicked_cb, this);

    AppEvents::Get().Subscribe(eADD_FRIEND_BTN_CLICKED, this);
    AppEvents::Get().Subscribe(eCANCEL_FRIEND_REQUEST_BTN_CLICKED, this);
    AppEvents::Get().Subscribe(eFRIEND_REQUEST_CONFIRMED, this);
}

FriendsSearchTab::~FriendsSearchTab()
{
    delete mAction;
    FriendSearchProvider::GetInstance()->EraseData();
    EraseWindowData();
}

void FriendsSearchTab::show_input_text_panel(void *data, Evas_Object *obj, void *event_info)
{
    FriendsSearchTab *me = static_cast<FriendsSearchTab *>(data);
    assert(me);
    elm_entry_input_panel_show(me->mSearchEntry);
}

void FriendsSearchTab::on_search_entry_clicked_cb(void *data, Evas_Object *obj, void *event_info)
{
    FriendsSearchTab *me = static_cast<FriendsSearchTab*>(data);
    assert(me);

    if (!me->GetScroller()) {
        me->ApplyScroller(me->mLayout, true);
        elm_object_part_content_set(me->mLayout, "search_list", me->GetScroller());
    }

    elm_entry_input_panel_hide(me->mSearchEntry);
    me->EraseWindowData();
    FriendSearchProvider::GetInstance()->EraseData();

    c_unique_ptr<char> searchStr(elm_entry_markup_to_utf8(elm_entry_entry_get(me->mSearchEntry)));
    FriendSearchProvider::GetInstance()->SetFindText(searchStr.get());
    me->RequestData();

    me->SetNoDataViewVisibility(false);
}

void FriendsSearchTab::SetDataAvailable( bool isAvailable )
{
    SetNoDataViewVisibility(!isAvailable);
}

void* FriendsSearchTab::CreateItem(void *dataForItem, bool isAddToEnd)
{
    assert(dataForItem);
	FoundItem *user = static_cast<FoundItem*>(dataForItem);

    ActionAndData *data = nullptr;
    data = new ActionAndData(user, mAction);
    data->mParentWidget = SearchGet(data, GetDataArea(), isAddToEnd);

    return data;
}

void FriendsSearchTab::DeleteItemById(char *id)
{
    FriendSearchProvider* provider = dynamic_cast<FriendSearchProvider*>(GetProvider());
    assert(provider);
    FoundItem *item = provider->GetItemById(id);
    if(item) {
        DestroyItemByData(item, TrackItemsProxyAdapter::items_comparator_Obj_vs_ActionAndData);
        provider->DeleteItem(item);
    }
}

Evas_Object *FriendsSearchTab::SearchGet(void *user_data, Evas_Object *obj, bool isAddToEnd)
{
    assert(user_data);
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    assert(action_data->mAction);
    assert(action_data->mAction->getScreen());

    FriendsSearchTab *me = static_cast<FriendsSearchTab*>(action_data->mAction->getScreen());

    Evas_Object *content = WidgetFactory::CreateSimpleWrapper(obj);
    me->CreateSearchItem(content, action_data);

    if (isAddToEnd) {
        elm_box_pack_end(obj, content);
    } else {
        elm_box_pack_start(obj, content);
    }
    evas_object_show(content);

    return content;
}

void FriendsSearchTab::CreateSearchItem(Evas_Object* parent, ActionAndData *action_data)
{
    assert(action_data);
    FoundItem *item = static_cast<FoundItem*>(action_data->mData);
    assert(item);

    Evas_Object *list_item = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(parent, "ff_search_list");

    action_data->mActionObj = list_item;

    Evas_Object *avatar = elm_image_add(list_item);
    elm_object_part_content_set(list_item, "item_avatar", avatar);
    action_data->UpdateImageLayoutAsync(item->mPhoto, avatar, ActionAndData::EImage, Post::EFromPicturePath);
    evas_object_show(avatar);

    Evas_Object *name_spacer = elm_box_add(list_item);
    elm_object_part_content_set(list_item, "item_name_spacer", name_spacer);
    evas_object_size_hint_weight_set(name_spacer, 1, 1);
    evas_object_size_hint_align_set(name_spacer, -1, 0.5);
    elm_box_pack_end(list_item, name_spacer);
    evas_object_show(name_spacer);

    Evas_Object *spacer1 = elm_box_add(name_spacer);
    evas_object_size_hint_weight_set(spacer1, 10, 10);
    evas_object_size_hint_align_set(spacer1, -1, -1);
    elm_box_pack_end(name_spacer, spacer1);
    evas_object_show(spacer1);

    Evas_Object *name = elm_label_add(name_spacer);
    evas_object_size_hint_weight_set(name, 1, 1);
    evas_object_size_hint_align_set(name, -1, -1);
    elm_label_wrap_width_set(name, R->FF_SEARCH_RESULT_WRAP_WIDTH);
    elm_label_line_wrap_set(name, ELM_WRAP_WORD);
    std::unique_ptr<char[]> name_text(WidgetFactory::WrapByFormat2(SANS_REGULAR_000_FORMAT, R->FF_SEARCH_RESULT_NAME_TEXT_SIZE, item->mText));
    elm_object_text_set(name, name_text.get());
    elm_box_pack_end(name_spacer, name);
    evas_object_show(name);

    if (item->mSubText) {
        Evas_Object *mutuals = elm_label_add(name_spacer);
        evas_object_size_hint_weight_set(mutuals, 1, 1);
        evas_object_size_hint_align_set(mutuals, -1, -1);
        elm_label_wrap_width_set(mutuals, R->FF_SEARCH_RESULT_WRAP_WIDTH);
        elm_label_line_wrap_set(mutuals, ELM_WRAP_WORD);
        std::unique_ptr<const char[]> ellipsisText(Utils::TextCutter(item->mSubText, R->FF_SUBTEXT_ELLIPSIS));
        std::unique_ptr<char[]> mutuals_text(WidgetFactory::WrapByFormat2(SANS_MEDIUM_9298A4_FORMAT, R->FF_SEARCH_RESULT_MUTUAL_TEXT_SIZE, ellipsisText.get()));
        elm_object_text_set(mutuals, mutuals_text.get());
        elm_box_pack_end(name_spacer, mutuals);
        evas_object_show(mutuals);
    }

    Evas_Object *spacer2 = elm_box_add(name_spacer);
    evas_object_size_hint_weight_set(spacer2, 10, 10);
    evas_object_size_hint_align_set(spacer2, -1, -1);
    elm_box_pack_end(name_spacer, spacer2);
    evas_object_show(spacer2);

    elm_object_translatable_part_text_set(list_item, "add_btn_text", "IDS_ADD_FRIEND");
    elm_object_translatable_part_text_set(list_item, "cancel_btn_text", "IDS_CANCEL");

    elm_object_signal_callback_add(list_item, "mouse,clicked,*", "add_btn*", ActionBase::on_add_friend_btn_clicked_cb, action_data);
    elm_object_signal_callback_add(list_item, "mouse,clicked,*", "cancel_btn*", ActionBase::on_cancel_friend_btn_clicked_cb, action_data);

    switch (item->mFriendshipStatus) {
    case Person::eCAN_REQUEST:
    case Person::eINCOMING_REQUEST:
        break;
    case Person::eOUTGOING_REQUEST:
        ShowCancelBtn(list_item);
        break;
    default:
        elm_layout_signal_emit(list_item, "no.btn", "all");
        break;
    }

    elm_object_signal_callback_add(list_item, "mouse,clicked,*", "item_*", on_item_clicked_cb, action_data);
}

void FriendsSearchTab::on_item_clicked_cb(void *user_data, Evas_Object *obj, const char *emisson, const char *source)
{
    if (Application::GetInstance()->GetTopScreenId() != SID_USER_PROFILE) {
        ActionAndData *data = static_cast<ActionAndData*>(user_data);
        assert(data);
        assert(data->mData);
        Utils::OpenProfileScreen(data->mData->GetId());
    }
}

void FriendsSearchTab::SetNoDataViewVisibility(bool isVisible)
{
    if (ConnectivityManager::Singleton().IsConnected()) {
        if (isVisible) {
            elm_object_signal_emit(mLayout, "nodata_view_visibility,1", "");
        } else {
            elm_object_signal_emit(mLayout, "nodata_view_visibility,0", "");
        }
    } else {
        elm_object_signal_emit(mLayout, "nodata_view_visibility,0", "");
        ShowConnectionError(false);
    }
}

void FriendsSearchTab::ShowAddBtn(Evas_Object* actionObject) {
    assert(actionObject);
    elm_object_signal_emit(actionObject, "hide.set", "cancel");
    elm_object_signal_emit(actionObject, "show.set", "add");
}

void FriendsSearchTab::ShowCancelBtn(Evas_Object* actionObject) {
    assert(actionObject);
    elm_object_signal_emit(actionObject, "hide.set", "add");
    elm_object_signal_emit(actionObject, "show.set", "cancel");
}

void FriendsSearchTab::Update(AppEventId eventId, void *data) {

    switch (eventId) {
    case eADD_FRIEND_BTN_CLICKED:
    case eCANCEL_FRIEND_REQUEST_BTN_CLICKED:
        if(data) {
            FriendSearchProvider* provider = dynamic_cast<FriendSearchProvider*>(GetProvider());
            assert(provider);
            FoundItem *item = provider->GetItemById(static_cast<char*>(data));
            if(item) {
                item->mFriendshipStatus = (eventId == eADD_FRIEND_BTN_CLICKED) ? Person::eOUTGOING_REQUEST : Person::eCAN_REQUEST;
                ActionAndData* actionData = static_cast<ActionAndData*>(FindItem(item, items_comparator_Obj_vs_ActionAndData));
                if (actionData) {
                    eventId == eADD_FRIEND_BTN_CLICKED ? ShowCancelBtn(actionData->mActionObj) : ShowAddBtn(actionData->mActionObj);
                }
            }
        }
        break;
    case eFRIEND_REQUEST_CONFIRMED:
        if(data) {
            DeleteItemById(static_cast<char*>(data));
        }
        break;
    default:
        TrackItemsProxy::Update(eventId, data);
        break;
    }
}

