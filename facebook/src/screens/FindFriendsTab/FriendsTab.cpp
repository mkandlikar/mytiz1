#include "Common.h"
#include "Config.h"
#include "ConnectivityManager.h"
#include "ErrorHandling.h"
#include "Friend.h"
#include "FriendsTab.h"
#include "Log.h"
#include "OwnFriendsProvider.h"
#include "Popup.h"
#include "Utils.h"
#include "WidgetFactory.h"

#include <string>

FriendsTab::FriendsTab(ScreenBase *parentScreen) :
        ScreenBase(parentScreen), TrackItemsProxyAdapter(nullptr, OwnFriendsProvider::GetInstance())
{
    mScreenId = ScreenBase::SID_FRIENDS_TAB;
    mAction = new FriendsAction(this);
    SetIdentifier("FRIENDS_TAB");
    mLayout = WidgetFactory::CreateLayoutByGroup(getParentMainLayout(), "simple_wrapper");
    WidgetFactory::CreateFriendItemEmulator(getParentMainLayout(), UIRes::GetInstance()->FF_ITEM_FRIEND_TEXT_STYLE, UIRes::GetInstance()->FF_SEARCH_RESULT_WRAP_WIDTH);
    ApplyScroller(mLayout, true);
    elm_object_part_content_set(mLayout, "content", GetScroller());

    OwnFriendsProvider::GetInstance()->SetAlphabeticalOrder(true);
    AppEvents::Get().Subscribe(eFR_REQUEST_FOR_USER_IN_FRIENDS, this);
    AppEvents::Get().Subscribe(eFRIEND_REQUEST_CONFIRMED, this);
    AppEvents::Get().Subscribe(eDELETE_BLOCKED_FRIEND_FROM_MY_LIST, this);
    AppEvents::Get().Subscribe(eFRIEND_UNFRIENDED, this);
    AppEvents::Get().Unsubscribe(eDATA_CHANGES_EVENT, this);
    FetchEdgeItems();
}

FriendsTab::~FriendsTab()
{
    delete mAction;
    EraseWindowData();
    WidgetFactory::DeleteFriendItemEmulator();
}

void* FriendsTab::CreateItem(void *dataForItem, bool isAddToEnd)
{
    assert(dataForItem);
    Log::info("FriendsTab::CreateItem");
    Friend *friends = static_cast<Friend*>(dataForItem);
    ActionAndData *data = nullptr;

    if (friends->mFriendshipStatus == Person::eARE_FRIENDS) {
        data = new ActionAndData(friends, mAction);
        data->mParentWidget = CreateFriendItem(GetDataArea(), data, isAddToEnd);
    }
    else{
        Log::error("FriendsTab::CreateItem->can't create friend");
    }

    if (data && !data->mParentWidget) {
        delete data;
        data = nullptr;
    }

    return data;
}

void* FriendsTab::UpdateItem(void *dataForItem, bool isAddToEnd)
{
    assert(dataForItem);
    Log::info("FriendsTab::UpdateItem");

    ActionAndData *actionAndData = static_cast<ActionAndData*>(dataForItem);

    actionAndData->mParentWidget = CreateFriendItem(GetDataArea(), actionAndData, isAddToEnd);

    if (!actionAndData->mParentWidget) {
        delete actionAndData;
        actionAndData = nullptr;
    }
    return actionAndData;
}

Evas_Object *FriendsTab::CreateFriendItem(Evas_Object* parent, ActionAndData *action_data, bool isAddToEnd)
{
    Evas_Object *list_item = WidgetFactory::CreateFriendItem(parent, action_data, isAddToEnd);
    if (list_item) {
        elm_object_signal_callback_add(list_item, "mouse,clicked,*", "friend_*", on_friends_button_clicked_cb, action_data);
        elm_object_signal_callback_add(list_item, "mouse,clicked,*", "add_friend_*", ActionBase::on_add_friend_btn_clicked_cb, action_data);
        elm_object_signal_callback_add(list_item, "mouse,clicked,*", "cancel_friend_*", ActionBase::on_cancel_friend_btn_clicked_cb, action_data);
        elm_object_signal_callback_add(list_item, "mouse,clicked,*", "item_*", on_item_clicked_cb, action_data);
    }
    return list_item;
}

void FriendsTab::FetchEdgeItems()
{
    Log::info("FriendsTab::FetchEdgeItems");
    if (ConnectivityManager::Singleton().IsConnected()) {
        Log::info("FriendsTab::FetchEdgeItems->RequestData");
        OwnFriendsProvider::GetInstance()->EraseData();
        OwnFriendsProvider::GetInstance()->SetAlphabeticalOrder(true);
        OwnFriendsProvider::GetInstance()->MissCacheRetrieve(true);
        RequestData(true);
        RequestToRebuildAllItems();
    }
}

void FriendsTab::RebuildAllItems() {
    Log::info("FriendsTab::RebuildAllItems:OwnFriendsProvider->ProviderSize= %u",OwnFriendsProvider::GetInstance()->GetDataCount());
    Redraw();
    ShiftDown();
}

void FriendsTab::on_friends_button_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    assert(user_data);
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    assert(action_data->mData);
    assert(action_data->mAction);

    FriendsTab *me = static_cast<FriendsTab*>(action_data->mAction->getScreen());
    assert(me);

    Friend *friends = static_cast<Friend*>(action_data->mData);
    if (friends->mFriendshipStatus == Person::eARE_FRIENDS) {
        static PopupMenu::PopupMenuItem context_menu_items[] = {
            { nullptr, "IDS_UNFRIEND", nullptr,
              ActionBase::on_unfriend_friend_btn_clicked_cb, nullptr,
              0, false, false
            },
            { nullptr, "IDS_BLOCK", nullptr,
              ActionBase::on_block_friend_btn_clicked_cb, nullptr,
              1, true, false
            },
            //Feature was marked as "LOW PRIORITY" https://github.com/fbmp/fb4t/issues/59
            //{ nullptr, "Edit list", nullptr,
            //  nullptr/*on_share_with_post_clicked*/, nullptr,
            //  2, true, false
            //},
        };
        context_menu_items[0].data = action_data;
        context_menu_items[1].data = action_data;
        //context_menu_items[2].data = action_data;

        Evas_Coord y = 0, h = 0;
        evas_object_geometry_get(obj, nullptr, &y, nullptr, &h);
        y += h / 2;
        PopupMenu::Show(2, context_menu_items, me->mLayout, false, y);
    }
}

void FriendsTab::on_item_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_USER_PROFILE) {
        assert(user_data);
        ActionAndData *data = static_cast<ActionAndData*>(user_data);
        assert(data->mData);
        Utils::OpenProfileScreen(data->mData->GetId());
    }
}

void FriendsTab::Update( AppEventId eventId, void *data )
{
    switch (eventId) {
    case eFR_REQUEST_FOR_USER_IN_FRIENDS:
        Log::debug("FriendsTab::Update->eFR_REQUEST_FOR_USER_IN_FRIENDS");
        if (data) {
            User *user = static_cast<User*>(data);
            DestroyItemByData( user, TrackItemsProxyAdapter::items_comparator_Obj_vs_ActionAndData);
        }
        break;
    case eFRIEND_REQUEST_CONFIRMED:
        Log::debug("FriendsTab::Update->eFRIEND_REQUEST_CONFIRMED");
        if (!IsRequestedToRebuildAllItems()) {
            Redraw();
        }
        break;
    case eFRIEND_UNFRIENDED:
    case eDELETE_BLOCKED_FRIEND_FROM_MY_LIST:
        Log::debug("FriendsTab::Update->eDELETE_UNFRIENDED_OR_BLOCKED_FRIEND");
        if (data){
            Friend *friends = static_cast<Friend *>(data);
            DestroyItemByData( friends, TrackItemsProxyAdapter::items_comparator_Obj_vs_ActionAndData);
        }
        break;
    default:
        TrackItemsProxy::Update( eventId, data );
        break;
    }
}

bool FriendsTab::DeleteConfirmationDialog() {
    return mAction->DeleteConfirmationDialog();
}
