#include "AbstractDataProvider.h"
#include "Application.h"
#include "ConnectivityManager.h"
#include "DataEventDescription.h"
#include "FacebookSession.h"
#include "Log.h"
#include "MutexLocker.h"
#include "RequestCompleteEvent.h"
#include "TrackItemsProxy.h"
#include "UIRes.h"
#include "Utils.h"
#include "WidgetFactory.h"

#include <assert.h>
#include <curl/curl.h>
#include <Elementary.h>
#include <string.h>

static const char *TRACK_TAG = "[ Track_Items_Proxy ]";
static const char *RECTANGLE = "rectangle";
static const char *FOREGROUND_RECT = "fr_gr_rect";
static const char *ELM_LAYOUT = "elm_layout";
static const char *TRACK_DEFAULT_ID = "???";

const int TrackItemsProxy::TINY_SCROLL_PADDING = 3;

const char* TrackItemsProxy::EDGE_TOP = "edge,top";
const char* TrackItemsProxy::EDGE_BOTTOM = "edge,bottom";

const char* TrackItemsProxy::SCROLL = "scroll";
const char* TrackItemsProxy::SCROLL_ANIM_START = "scroll,anim,start";
const char* TrackItemsProxy::SCROLL_ANIM_STOP = "scroll,anim,stop";

const unsigned int TrackItemsProxy::DELETE_RECTANGLES_REFRESH_OFFSET = 300;

const unsigned int TrackItemsProxy::PIXELS_HEIGHT_THRESHOLD = 1800;
const double TrackItemsProxy::HIDE_EARLY_PROGRESS_BAR_TIMEOUT = 2.0;
const double TrackItemsProxy::HIDE_REALLY_PROGRESS_BAR_TIMEOUT = 7.0;

const unsigned int TrackItemsProxy::AUTO_FETCH_TOP_EDGE_ITEMS_TIMEOUT = 60 * 20; // 20 minutes

const unsigned int TrackItemsProxy::DEFAULT_CACHE_WINDOW_SIZE = 25;
const unsigned int TrackItemsProxy::DEFAULT_CACHE_WINDOW_STEP = 5;

const unsigned int TrackItemsProxy::INITIALIZATION_CACHE_WINDOW_SIZE = 7;
const unsigned int TrackItemsProxy::MAX_DATA_PROVIDER_ITEMS_REST = 40;
const double TrackItemsProxy::REAL_IDLE_STATE_TIMEOUT = 0.5;
const double TrackItemsProxy::DELAYED_RECT_REDRAW_TIMEOUT = 0.2;

Eina_List *TrackItemsProxy::mAliveTrackItemsProxies = nullptr;

#ifdef MONITOR_SCROLL_CORRUPTION

Eina_Bool TrackItemsProxy::state_timer_cb( void *data )
{
    TrackItemsProxy *proxy = static_cast<TrackItemsProxy*>(data);
    Eina_List* childs = elm_box_children_get( proxy->GetDataArea() );
    Eina_List *l;
    void *listData;
    int count = 0;
    EINA_LIST_FOREACH( childs, l, listData ) {
        ++count;
    }
    eina_list_free(childs);
    Utils::DoubleSidedStack *itemsStack = proxy->m_ItemsCache->GetItemsStack();
    Log::error( "TrackItemsProxy: STATE_TIMER, scrollCount[%d], stackCount[%d], headerCount[%d]",
            count, itemsStack->GetSize(), proxy->GetHeaderSize() );
    return ECORE_CALLBACK_RENEW;
}

#endif

TrackItemsProxy::TrackItemsProxy( Evas_Object* parentLayout, AbstractDataProvider *provider, ItemsComparator updateItemsWndComp, int headerSize, ItemsCache* itemsCache ) :
        ItemsBuilderAssistent( headerSize ),
        m_ItemsCache( itemsCache ) ,
        m_UpdateItemsWndComp( updateItemsWndComp )
{
#ifdef MONITOR_SCROLL_CORRUPTION
    m_StateTimer = NULL;
#endif
    Log::debug( "TrackItemsProxy::TrackItemsProxy start create scroller proxy." );
    m_Scroller = NULL;
    m_ProgressBar = NULL;
    m_ProgressBarLayout = NULL;
    m_ConnectionErrorLayout = NULL;
    m_ShowServiceWidgetOnBottom = false;
    m_DataRequest = NULL;
    m_Provider = provider;
    m_RequestJob = NULL;
    m_RedrawJob = NULL;
    m_ProgressBarAboveHeader = false;
    m_HeaderArea = NULL;
    m_DataArea = NULL;
    m_IsSwipeUp_FromTop = true;
    m_HeaderSize = headerSize;
    m_MutedDataRequestTimer = NULL;
    m_ManualIdleTimer = NULL;
    m_PeriodicalTimer = NULL;
    m_BringInRegTimer = NULL;
    m_IsMuteTimerEnabled = false;
    m_IsMuteRequested = false;
    m_IsNeedToErase = false;
    m_IsRectangleCb = false;
    m_IsPeriodicalRedraw = false;
    m_Identifier = TRACK_DEFAULT_ID;
    m_LastRectangleProcessingTime = 0;
    m_OperationsComparator = NULL;
    m_IsUpdateCacheEnabled = true;
    m_DelayedRegEdgesCbTimer = NULL;
    m_ProgressBarApplied = true;
    m_ProxySettings = new ProxySettings( DEFAULT_CACHE_WINDOW_SIZE, DEFAULT_CACHE_WINDOW_STEP );
    m_MouseMovement.xCoordinate_Point = m_MouseMovement.yCoordinate_Point = 0;
    m_MouseMovement.xCoordinate_scroller = m_MouseMovement.yCoordinate_scroller = 0;

    m_ConnectionErrorLayoutName = strdup("connection_error_list_item");
    m_ProgressBarLayoutName = strdup("progress_bar");

    UpdateHistoryCount();
    m_UploadHistory.m_IsDescendingOrder = true;
    m_UploadHistory.m_UploadResult = NONE_DATA_COULD_BE_RESULT;
    eina_lock_new( &m_Mutex );

    mIsBeginEdgeCbAdded = false;
    mIsEndEdgeCbAdded = false;
    ApplyScroller( parentLayout );

    if (!m_ItemsCache) {
        m_ItemsCache = new ItemsCache( this, m_Provider );
    }

    mProgressBarTimer = NULL;
    AppEvents::Get().Subscribe(eDATA_CHANGES_EVENT, this);
    m_Provider->AddRef();
    m_EnableToShowConnectionError = true;
    m_EnableBigConnectionErrorWidget = true;
    m_ShowSingleItem = false;
    m_ItemSizeCache = eina_hash_string_superfast_new( delete_cached_item_size );
    if ( !m_ItemSizeCache ) {
        Log::debug( "TrackItemsProxy: cannot create hash table for item sizes." );
    }
    Log::debug( "TrackItemsProxy::TrackItemsProxy end create scroller proxy." );

    m_DisableMouseUpDown = false;

    m_AllowNewStoriesBtn = false;
    m_NewItemsWereAddedButNotShown = false;

    m_PostponedTopItemsDelete = false;
    m_PostponedBottomItemsDelete = false;
    m_DisableShiftUpForPresenter = false;
    m_RequestedToRebuildAllItems = false;
    m_TopOffset = 0;

    m_CheckForDuplicatesShiftUp = false;
    m_CheckForDuplicatesShiftDown = false;
    m_AddedNewItems = 0;

    mTimeStamp = clock();
    mAliveTrackItemsProxies = eina_list_append(mAliveTrackItemsProxies, this);
}

TrackItemsProxy::~TrackItemsProxy()
{
    Log::debug( "TrackItemsProxy::~TrackItemsProxy start" );
    free( m_ConnectionErrorLayoutName );
    free( m_ProgressBarLayoutName );

    if(m_Provider) {
        m_Provider->Unsubscribe( this );
    }

    if(m_RequestJob) {
        ecore_job_del( m_RequestJob );
    }

    if(m_RedrawJob) {
        ecore_job_del( m_RedrawJob );
    }

    if(m_Scroller) {
        evas_object_smart_callback_del( m_Scroller, EDGE_TOP, begin_edge_of_scroll_reached);
        evas_object_smart_callback_del( m_Scroller, EDGE_BOTTOM, end_edge_of_scroll_reached);

        RemoveAnimationCBs();

        evas_object_event_callback_del( m_Scroller, EVAS_CALLBACK_MOUSE_DOWN, mouse_down_cb );
        evas_object_event_callback_del( m_Scroller, EVAS_CALLBACK_MOUSE_UP, mouse_up_cb );
        DeactivateRectangleCb();

        evas_object_event_callback_del( m_Scroller, EVAS_CALLBACK_RESIZE, scroller_resize_cb);
    }

    delete m_ItemsCache;

    if ( m_ItemSizeCache ) {
        eina_hash_free_buckets( m_ItemSizeCache );
        eina_hash_free( m_ItemSizeCache );
    }

    delete m_ProxySettings;
    FacebookSession::GetInstance()->ReleaseGraphRequest( m_DataRequest );

    if ( m_ProgressBar ) {
        evas_object_del( m_ProgressBar );
    }

    if ( m_ProgressBarLayout ) {
        evas_object_del( m_ProgressBarLayout );
    }
    if ( m_MutedDataRequestTimer ) {
        ecore_timer_del( m_MutedDataRequestTimer );
    }
    if ( m_ManualIdleTimer ) {
        ecore_timer_del( m_ManualIdleTimer );
    }
    if ( mProgressBarTimer ) {
        ecore_timer_del( mProgressBarTimer );
    }
    if ( m_BringInRegTimer) {
        ecore_timer_del( m_BringInRegTimer );
    }
    if ( m_DelayedRegEdgesCbTimer ) {
        ecore_timer_del( m_DelayedRegEdgesCbTimer );
    }

    if (mAsyncResizeTimer) {
        ecore_timer_del(mAsyncResizeTimer);
    }

    DeactivatePeriodicalRedraw();

    eina_lock_free( &m_Mutex );
    if(m_Provider) {
        m_Provider->Release();
    }

    mAliveTrackItemsProxies = eina_list_remove(mAliveTrackItemsProxies, this);

    HideConnectionError();
    Log::debug( "TrackItemsProxy::~TrackItemsProxy end" );
}

int TrackItemsProxy::track_items_proxy_comparator(const void *data1, const void *data2) {
    const TrackItemsProxy *tip1 = static_cast<const TrackItemsProxy *> (data1);
    const TrackItemsProxy *tip2 = static_cast<const TrackItemsProxy *> (data2);
    if (tip1 && (tip2 == tip1) && (tip1->mTimeStamp == tip2->mTimeStamp)) {
        return 0;
    } else {
        return -1;
    }
}

void TrackItemsProxy::EraseWindowData()
{
    Log::debug( "TrackItemsProxy::EraseWindowData start for screen %s", m_Identifier );
    MutexLocker locker( &m_Mutex );
    Log::debug( "TrackItemsProxy::EraseWindowData after mutex" );
    if(m_ItemsCache) {
        m_ItemsCache->EraseWindowData();
        Log::debug( "TrackItemsProxy: erase finished" );
    }
    Log::debug( "TrackItemsProxy::EraseWindowData end" );
}

void TrackItemsProxy::ApplyScroller( Evas_Object *parentLayout, bool isDataAreaRequired, ProxySettings *settings )
{
    if ( parentLayout ) {
        Log::debug( "%s : TrackItemsProxy::ApplyScroller for %s", TRACK_TAG, m_Identifier );
        m_Scroller = elm_scroller_add( parentLayout );
        if(m_Scroller) {
            elm_scroller_single_direction_set( m_Scroller, ELM_SCROLLER_SINGLE_DIRECTION_HARD );

            elm_scroller_policy_set( m_Scroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_AUTO );
            evas_object_smart_callback_add( m_Scroller, EDGE_TOP, begin_edge_of_scroll_reached, this );
            // for cases when begin/end edges could not be reached for new data uploading (due to
            // small amout of items on a scroller.
            evas_object_event_callback_add( m_Scroller, EVAS_CALLBACK_MOUSE_DOWN, mouse_down_cb, this );
            evas_object_event_callback_add( m_Scroller, EVAS_CALLBACK_MOUSE_UP, mouse_up_cb, this );

            AddAnimationCBs();

            evas_object_smart_callback_add( m_Scroller, EDGE_BOTTOM, end_edge_of_scroll_reached, this );
            ActivateRectangleCb();

            mIsBeginEdgeCbAdded = true;
            mIsEndEdgeCbAdded = true;

            evas_object_event_callback_add( m_Scroller, EVAS_CALLBACK_RESIZE, scroller_resize_cb, this );
            AddDataAreaResizeCB();

            if ( isDataAreaRequired ) {
                m_DataArea = elm_box_add( m_Scroller );
                evas_object_size_hint_weight_set(m_DataArea, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
                evas_object_size_hint_align_set( m_DataArea, EVAS_HINT_FILL, EVAS_HINT_FILL );
                elm_box_padding_set( m_DataArea, 0, 0 );
                elm_object_content_set( m_Scroller, m_DataArea );
                evas_object_show( m_Scroller );
            }

            if ( settings ) {
                m_ItemsCache->SetSettings( settings->wndSize, settings->wndStep, settings->initialWndSize );
            }
        }
    }
}

void TrackItemsProxy::AddAnimationCBs() {
    evas_object_smart_callback_add( m_Scroller, SCROLL_ANIM_START, animation_started_cb, this );
    evas_object_smart_callback_add( m_Scroller, SCROLL_ANIM_STOP, animation_stopped_cb, this );
}

void TrackItemsProxy::RemoveAnimationCBs() {
    evas_object_smart_callback_del( m_Scroller, SCROLL_ANIM_START, animation_started_cb );
    evas_object_smart_callback_del( m_Scroller, SCROLL_ANIM_STOP, animation_stopped_cb );
}

Eina_Bool TrackItemsProxy::periodical_rectangle_redraw_cb( void *data )
{
    TrackItemsProxy *proxy = static_cast<TrackItemsProxy*>(data);
    if ( proxy->m_IsRectangleCb ) {
        long long diff = Utils::GetCurrentTimeMicroSecs() - proxy->m_LastRectangleProcessingTime;
        static unsigned int lowerBound = 150000;    // 0.15 secs
        static unsigned int upperBound = 10000000;  // 10 secs
        if ( diff > lowerBound && diff < upperBound ) {
            proxy->RefreshRectangles();
        }
    }
    return ECORE_CALLBACK_RENEW;
}

void TrackItemsProxy::mouse_down_cb(void *data, Evas *e, Evas_Object *obj, void *event_info)
{
    Log::debug( "%s : TrackItemsProxy_cb_ mouse_down_cb", TRACK_TAG );
    TrackItemsProxy *proxy = static_cast<TrackItemsProxy*>(data);
    if(proxy && proxy->m_DisableMouseUpDown) {
        return;
    }
    if(proxy && proxy->m_Scroller){
        proxy->ValidateScrollerAndPreloadItems();

        Evas_Event_Mouse_Down *eemd = (Evas_Event_Mouse_Down *)event_info;
        MouseMovement *movement = proxy->GetMouseMovement();

        movement->xCoordinate_Point = movement->yCoordinate_Point = 0;
        movement->xCoordinate_scroller = movement->yCoordinate_scroller = 0;

        {
            Evas_Coord w, h;
            elm_scroller_region_get( proxy->m_Scroller, &movement->xCoordinate_scroller, &movement->yCoordinate_scroller, &w, &h );
        }

        movement->xCoordinate_Point = eemd->canvas.x;
        movement->yCoordinate_Point = eemd->canvas.y;

        if( !proxy->mIsEndEdgeCbAdded )
        {
            evas_object_smart_callback_add( proxy->m_Scroller, EDGE_BOTTOM, end_edge_of_scroll_reached, proxy );
            proxy->mIsEndEdgeCbAdded = true;
            proxy->ActivateRectangleCb();
        }
    }
}

void TrackItemsProxy::mouse_up_cb(void *data, Evas *e, Evas_Object *obj, void *event_info)
{
    Log::debug( "%s : TrackItemsProxy_cb_ mouse_up_cb", TRACK_TAG );
    TrackItemsProxy *proxy = static_cast<TrackItemsProxy*>(data);
    if(proxy && proxy->m_DisableMouseUpDown) {
        return;
    }
    if(proxy && proxy->m_Scroller){
        Evas_Event_Mouse_Down *eemd = (Evas_Event_Mouse_Down *)event_info;
        MouseMovement *movement = proxy->GetMouseMovement();

        Evas_Coord x, y, w, h;
        elm_scroller_region_get( proxy->m_Scroller, &x, &y, &w, &h );

        if ( movement->yCoordinate_scroller <= TINY_SCROLL_PADDING && y >= 30 ) {
            proxy->EnableRequestData( false );
        }
        if ( movement->yCoordinate_scroller == 0 && !proxy->IsWaitingForData() ) {
            int diff = eemd->canvas.y - movement->yCoordinate_Point;
            if ( y == 0 && abs( diff ) > 50 ) { // we are still in the same position as we were when DOWN has been emitted
                Log::debug( "TrackItemsProxy_cb_ mouse_up_cb: detected request data, coordinate diff is: %d", diff );
                if( proxy->m_RequestJob ) {
                    ecore_job_del( proxy->m_RequestJob );
                    proxy->m_RequestJob = NULL;
                }
                if ( diff > 0 ) {
                    begin_edge_of_scroll_reached( proxy, NULL, NULL );
                } else {
                    end_edge_of_scroll_reached( proxy, NULL, NULL );
                }
            }
        }
        movement->xCoordinate_Point = movement->yCoordinate_Point = 0;
        movement->xCoordinate_scroller = movement->yCoordinate_scroller = 0;
    }
}

void TrackItemsProxy::animation_started_cb( void *data, Evas_Object *obj, void *event_info )
{
    Log::debug( "%s : TrackItemsProxy::animation_started_cb", TRACK_TAG );
    TrackItemsProxy *proxy = static_cast<TrackItemsProxy*>(data);
    if (proxy) {
        proxy->m_IsInScrollAnimation = true;
    }
}

void TrackItemsProxy::animation_stopped_cb( void *data, Evas_Object *obj, void *event_info )
{
    Log::debug( "%s : TrackItemsProxy::animation_stopped_cb", TRACK_TAG );
    TrackItemsProxy *proxy = static_cast<TrackItemsProxy*>(data);
    if (proxy) {
        proxy->m_IsInScrollAnimation = false;

        if (proxy->m_PostponedTopItemsDelete) {
            proxy->m_ItemsCache->DeleteFromTop();
            proxy->m_PostponedTopItemsDelete = false;
        }
        if (proxy->m_PostponedBottomItemsDelete) {
            proxy->m_ItemsCache->DeleteFromBottom(proxy->m_ItemsCache->GetCurrentWindowSize());
            proxy->m_PostponedBottomItemsDelete = false;
        }
    }
}

Eina_Bool TrackItemsProxy::bring_in_region_cb( void *data )
{
    TrackItemsProxy *proxy = static_cast<TrackItemsProxy*>(data);
    Eina_List *l, *l_next;
    void *element = NULL;

    Evas_Coord x, y, w, h;
    Eina_List* childs = elm_box_children_get( proxy->m_DataArea );

    EINA_LIST_FOREACH_SAFE( childs, l, l_next, element ) {
        Evas_Object *child = static_cast<Evas_Object*>(element);
        assert( child );
        if ( child ) {
            evas_object_geometry_get( child, &x, &y, &w, &h );
            if ( w == 0 || h == 0 ) {
                return ECORE_CALLBACK_RENEW;
            }
        }
    }
    eina_list_free(childs);
    elm_scroller_region_get( proxy->m_Scroller, &x, &y, &w, &h );
    elm_scroller_region_bring_in( proxy->m_Scroller, x, TINY_SCROLL_PADDING, w, h );
    proxy->m_BringInRegTimer = NULL;
    return ECORE_CALLBACK_CANCEL;
}

Eina_Bool TrackItemsProxy::delayed_registration_of_edged_cb( void *data )
{
    TrackItemsProxy *proxy = static_cast<TrackItemsProxy*>(data);
    if( !proxy->mIsBeginEdgeCbAdded )
    {
        evas_object_smart_callback_add( proxy->GetScroller(), EDGE_TOP, begin_edge_of_scroll_reached, proxy );
        proxy->mIsBeginEdgeCbAdded = true;
    }
    proxy->m_DelayedRegEdgesCbTimer = NULL;
    return ECORE_CALLBACK_CANCEL;
}

Eina_Bool TrackItemsProxy::user_idle_state_cb( void *data )
{
    TrackItemsProxy *proxy = static_cast<TrackItemsProxy*>(data);
    proxy->IdleStateHandler();
    return ECORE_CALLBACK_RENEW;
}

Eina_Bool TrackItemsProxy::hide_progress_bar_by_timer_cb( void *data )
{
    Log::debug( "%s : hide_progress_bar_by_timer_cb", TRACK_TAG );
    TrackItemsProxy *proxy = static_cast<TrackItemsProxy*>(data);
    proxy->HideProgressBar();
    proxy->mProgressBarTimer = NULL;
    return ECORE_CALLBACK_CANCEL;
}

Eina_Bool TrackItemsProxy::auto_fetch_top_edge_items_cb( void *data )
{
    Log::debug( "%s : auto_fetch_top_edge_items_cb", TRACK_TAG );
    TrackItemsProxy *proxy = static_cast<TrackItemsProxy*>(data);
    if(proxy->m_RequestJob) {
        ecore_job_del( proxy->m_RequestJob );
        proxy->m_RequestJob = NULL;
    }
    proxy->m_RequestJob = ecore_job_add(fetch_edge_data, proxy);
    if(!proxy->m_RequestJob) {
        Log::debug( "%s : auto_fetch_top_edge_items_cb: ecore_job_add returned NULL", TRACK_TAG );
    }
    proxy->m_AllowNewStoriesBtn = true;
    if ( proxy->m_NewItemsWereAddedButNotShown ) {// some items were created but was not shown still
        proxy->NewItemsAvailable();
    }
    return ECORE_CALLBACK_RENEW;
}

void TrackItemsProxy::SetHeaderArea( Evas_Object *headerArea )
{
    assert( m_HeaderArea == NULL );
    m_HeaderArea = headerArea;
}

void TrackItemsProxy::SetDataArea( Evas_Object *dataArea )
{
    assert( m_DataArea == NULL );
    m_DataArea = dataArea;
}

void TrackItemsProxy::SetIdentifier(const char *id)
{
    m_Identifier = id;
}

void TrackItemsProxy::RequestData( bool isDescendingOrder, bool isMuteRequest )
{
    Log::debug( "%s : RequestData, count is [ %d ], identifier = [ %s ], isDescendingOrder [ %s ], ItemsCount [ %d ]",
            TRACK_TAG, m_Provider->GetDataCount(), m_Identifier, (isDescendingOrder ? "true" : "false"),
            m_ItemsCache->GetItemsStack()->GetSize() );
//    if ( m_ItemsCache->isEmpty() ) {
//        // first request MUST BE in a descending order
//        Log::debug( "%s : Request for Empty Cache and data provider", TRACK_TAG );
//        isDescendingOrder = true;
//    } looks like without it works better for empty feed screen but we should monitor

    m_IsMuteRequested = isMuteRequest;
    if ( m_IsMuteTimerEnabled && !m_MutedDataRequestTimer ) {
        Log::debug( "%s : CREATE MUTE TIMER", TRACK_TAG );
        m_MutedDataRequestTimer = ecore_timer_add( AUTO_FETCH_TOP_EDGE_ITEMS_TIMEOUT, auto_fetch_top_edge_items_cb, this );
    }

    UpdateHistoryCount();
    m_UploadHistory.m_IsDescendingOrder = isDescendingOrder;

    if ( m_ItemsCache->isEmpty() && m_UploadHistory.m_DataCount && !m_Provider->IsDataCached() ) {
        Log::debug( "%s : Exist old data. Request will no be send.", TRACK_TAG );
        // If provider has some data and a user of a TrackItemsProxyc class do not have items, we
        // do not care about ascending or descending order. We just remember the default value for
        // this ( the default value is descending order ).
        m_UploadHistory.m_IsDescendingOrder = true;
        Update();
        if ( m_Provider->IsRequestAlive() ) {
            Log::debug( "%s : Alive request. Subscribe for notifications from data provider.", TRACK_TAG );
            m_Provider->Subscribe( this );
        }
        return;
    }

    m_Provider->Subscribe( this );
    m_UploadHistory.m_UploadResult = m_Provider->UploadData( isDescendingOrder );


    if (!ConnectivityManager::Singleton().IsConnected() && m_UploadHistory.m_UploadResult != ONLY_CACHED_DATA_RESULT) {
        ShowConnectionError(isDescendingOrder);
    } else {
        if ( ( m_UploadHistory.m_UploadResult == ONLY_REQUEST_SENT_RESULT ||
                m_UploadHistory.m_UploadResult == TOO_EARLY_TO_REQUEST_RESULT ) && !isMuteRequest ) {
                ShowProgressBar( isDescendingOrder );
        }
    }

    switch ( m_UploadHistory.m_UploadResult )
    {
        case NONE_DATA_COULD_BE_RESULT:
        {
            Log::debug( "%s : NONE_DATA_COULD_BE_RESULT", TRACK_TAG );
            m_Provider->Unsubscribe( this );
            Update();
            UpdateMuteTimer();
            break;
        }
        case TOO_EARLY_TO_REQUEST_RESULT:
        {
            Log::debug( "%s : TOO_EARLY_TO_REQUEST_RESULT", TRACK_TAG );
            if (!mProgressBarTimer) {
                mProgressBarTimer = ecore_timer_add( HIDE_EARLY_PROGRESS_BAR_TIMEOUT, hide_progress_bar_by_timer_cb, this );
            }
            break;
        }
        case ONLY_REQUEST_SENT_RESULT:
        {
            Log::debug( "%s : ONLY_REQUEST_SENT_RESULT", TRACK_TAG );
            if (!mProgressBarTimer) {
                mProgressBarTimer = ecore_timer_add( HIDE_REALLY_PROGRESS_BAR_TIMEOUT, hide_progress_bar_by_timer_cb, this );
            }
            UpdateMuteTimer();
            break;
        }
        case ONLY_CACHED_DATA_RESULT:
        {
            //
            // The cached data must be visible only for time, when the newest data will not be received. So, draw
            // cached data ( by using Update() ) and send a request for newest data immediately. If previously
            // a Data Provider was contain cached data, it must send a request during the next call of
            // UploadData method. ONLY_CACHED_DATA_RESULT must be changed by ONLY_REQUEST_SENT_RESULT in normal
            // case. If ONLY_REQUEST_SENT_RESULT will not be return, something is wrong. Probably, it could be due
            // to network error.
            //
            Log::debug( "%s : ONLY_CACHED_DATA_RESULT", TRACK_TAG );
            Update();
            UpdateMuteTimer();
            if (ConnectivityManager::Singleton().IsConnected()) {
                m_UploadHistory.m_UploadResult = m_Provider->UploadData( isDescendingOrder );
                if ( m_UploadHistory.m_UploadResult != ONLY_REQUEST_SENT_RESULT ) {
                    m_Provider->Unsubscribe( this );
                    Log::debug( "%s : Cannot upload the newest data. Something went wrong", TRACK_TAG );
                } else {
                    Log::debug( "%s : Manual requesting the newest data", TRACK_TAG );
                }
            }
            break;
        }
        default:
            assert( false );
    }
}

void TrackItemsProxy::SetConnectionErrorLayoutName(const char *layoutName) {
    if(layoutName) {
        free(m_ConnectionErrorLayoutName);
        m_ConnectionErrorLayoutName = strdup(layoutName);
    }
}

void TrackItemsProxy::SetProgressBarLayoutName(const char *layoutName) {
    if(layoutName) {
        free(m_ProgressBarLayoutName);
        m_ProgressBarLayoutName = strdup(layoutName);
    }
}

void TrackItemsProxy::HideConnectionError()
{
    Log::debug( "%s: HideConnectionError", TRACK_TAG);
    if ( m_ConnectionErrorLayout ) {
        elm_box_unpack( m_DataArea, m_ConnectionErrorLayout );
        evas_object_hide( m_ConnectionErrorLayout );
        evas_object_del( m_ConnectionErrorLayout );
        m_ConnectionErrorLayout = NULL;
    }
}

bool TrackItemsProxy::IsConnectionError()
{
    return m_ConnectionErrorLayout;
}

void TrackItemsProxy::UpdateMuteTimer()
{
    if ( m_IsMuteTimerEnabled && m_MutedDataRequestTimer && !m_UploadHistory.m_IsDescendingOrder ) {
        Log::debug( "%s : RESET MUTE TIMER", TRACK_TAG );
        Log::info( "HomeScreen::UpdateMuteTimer" );
        ecore_timer_reset( m_MutedDataRequestTimer );
    }
}

void TrackItemsProxy::AppendMuteItems()
{
    Log::debug( "%s : TrackItemsProxy::AppendMuteItems->new items [ %d ] for %s", TRACK_TAG, m_AddedNewItems , m_Identifier );
    bool wasRealTop = IsRealTopEdgeOfScroller();//should be done before normalization

    m_ItemsCache->NormalizeWindowIndex( false, m_AddedNewItems );

    int itemNumber = 0;
    int created = 0;
    int count = m_Provider->GetDataCount();
    std::unique_ptr<Utils::DoubleSidedStack> stack( new Utils::DoubleSidedStack() );
    while ( created < m_HeaderSize && itemNumber < m_AddedNewItems ) {
        if ( itemNumber >= count ) {
            // it must not happen
            assert( false );
            return;
        }
        void* itemToCreate = eina_array_data_get( m_Provider->GetData(), itemNumber );
        if( FindDuplicateItem(itemToCreate) ) {
            Log::error( "%s : TrackItemsProxy::AppendMuteItems->Duplicate was found for new item [ %d ] for %s", TRACK_TAG, itemNumber , m_Identifier );
            ++itemNumber;
            continue;
        }
        void *item = CreateItem( itemToCreate, false );// could be added only to top!
        if ( item ) {
            HideItem( item );
            stack->PushToEnd( item );
            ++created;
        } else {
            Log::debug( "%s : AppendMuteItems: cannot create an item.", TRACK_TAG );
        }
        ++itemNumber;
    }
    Log::debug( "%s : AppendMuteItems, created [ %d ]", TRACK_TAG, created );
    //
    // Delete the redundancy items
    //
    int headerItemsToDelCount = (stack->GetSize() + GetHeaderSize()) - m_HeaderSize;
    Log::debug( "%s : AppendMuteItems, before delete. stack [ %d ], headerSize [ %d ], to del [ %d ].",
            TRACK_TAG, stack->GetSize(),GetHeaderSize(), headerItemsToDelCount);
    while ( headerItemsToDelCount > 0 ) {
        Log::debug( "%s : AppendMuteItems, deleted from old header", TRACK_TAG );
        DeleteLast();
        --headerItemsToDelCount;
    }
    //
    // Append newly created items to a header
    //
    Log::debug( "%s : AppendMuteItems, manual add stack to header [ %d ]", TRACK_TAG, stack->GetSize() );
    while ( stack->GetSize() ) {
        PushToBegin( stack->PopFromEnd() );
    }

    if( created>0 ) {
        m_NewItemsWereAddedButNotShown = true;
    }

    m_CheckForDuplicatesShiftUp = true;
    m_CheckForDuplicatesShiftDown = true;

    if( wasRealTop ) {
        Log::debug( "%s : AppendMuteItems, add automatically for [ %s ]", TRACK_TAG, m_Identifier );
        AutomaticallyShowJustAddedItems();
        m_NewItemsWereAddedButNotShown = false;
    }
    ResetUpdatesCount();
    m_AddedNewItems = 0;
}

void TrackItemsProxy::SetShowConnectionErrorEnabled( bool isEnabled )
{
    m_EnableToShowConnectionError = isEnabled;
}

void TrackItemsProxy::EnableBigConnectionErrorWidget( bool isEnabled )
{
    m_EnableBigConnectionErrorWidget = isEnabled;
}

bool TrackItemsProxy::AllDataWasUploaded () {
    return m_Provider->IsReverseChronological() && // this check for comments while
           m_UploadHistory.m_UploadResult == NONE_DATA_COULD_BE_RESULT;
}

void TrackItemsProxy::ShowConnectionErrorBig( std::string layoutName,  Evas_Object* connectionErrorLayout ) {
    elm_layout_file_set( connectionErrorLayout, Application::mEdjPath, layoutName.c_str());
    Evas_Object *text_part1 = elm_label_add(connectionErrorLayout);
    elm_object_part_content_set(connectionErrorLayout, "text_part1", text_part1);
    char *text = WidgetFactory::WrapByFormat2(SANS_MEDIUM_CENTER_4e5665_FORMAT, UIRes::GetInstance()->SOMETHING_WRONG_TEXT_SIZE, i18n_get_text("IDS_CANT_CONNECT"));
    if (text) {
        elm_object_text_set(text_part1, text);
        delete[] text;
    }
    elm_label_wrap_width_set(text_part1, UIRes::GetInstance()->SCREEN_SIZE_WIDTH);
    elm_label_line_wrap_set(text_part1, ELM_WRAP_WORD);
}

void TrackItemsProxy::RePackConnectionError(bool packOnTop) {
    Log::debug( "%s : RePackConnectionError", TRACK_TAG );
    if(m_ConnectionErrorLayout) {
        elm_box_unpack(m_DataArea,m_ConnectionErrorLayout);
        if (packOnTop || !m_ShowServiceWidgetOnBottom) {
            elm_box_pack_start( m_DataArea, m_ConnectionErrorLayout );
        } else {
            elm_box_pack_end( m_DataArea, m_ConnectionErrorLayout );
        }
    }
}

void TrackItemsProxy::ShowConnectionError( bool isToEnd )
{
    Log::debug( "%s : ShowConnectionError %s", TRACK_TAG, isToEnd? "On Bottom":"On Top" );

    HideConnectionError();
    if( !m_EnableToShowConnectionError ) return;

    HideProgressBar();

    if ( (m_ConnectionErrorLayout == NULL) && m_ConnectionErrorLayoutName ) {
        m_ConnectionErrorLayout = elm_layout_add( m_DataArea );
        if((m_ItemsCache->isEmpty() && !IsPseudoPostVisible() && m_EnableBigConnectionErrorWidget)) // show small widget for screens with comments always
        {
            std::string st = m_ConnectionErrorLayoutName;
            st += "_big";
            ShowConnectionErrorBig(st,m_ConnectionErrorLayout);
        } else {
            elm_layout_file_set( m_ConnectionErrorLayout, Application::mEdjPath, m_ConnectionErrorLayoutName );
            elm_object_translatable_part_text_set(m_ConnectionErrorLayout, "text_part1", "IDS_CANT_CONNECT");
        }

        elm_object_translatable_part_text_set(m_ConnectionErrorLayout, "text_part2", "IDS_TAP_TO_RETRY");
        if ( isToEnd ) {
            elm_box_pack_end( m_DataArea, m_ConnectionErrorLayout );
        } else {
            elm_box_pack_start( m_DataArea, m_ConnectionErrorLayout );
        }

        m_ShowServiceWidgetOnBottom = isToEnd;
        evas_object_show( m_ConnectionErrorLayout );
        elm_object_signal_callback_add(m_ConnectionErrorLayout, "widget.click", "*", on_connectionerror_widget_handler, this);
    }
}

void TrackItemsProxy::HideProgressBar()
{
    Log::debug( "%s : HideProgressBar", TRACK_TAG );
    if ( m_ProgressBarLayout ) {
        evas_object_hide( m_ProgressBar );
        elm_box_unpack( m_ProgressBarLayout, m_ProgressBar );
        evas_object_del( m_ProgressBar );
        m_ProgressBar = NULL;
        evas_object_hide( m_ProgressBarLayout );
        elm_box_unpack(m_ProgressBarAboveHeader ? m_HeaderArea : GetDataArea(), m_ProgressBarLayout );
        m_ProgressBarAboveHeader = false;
        evas_object_del( m_ProgressBarLayout );
        m_ProgressBarLayout = NULL;
        m_LastRectangleProcessingTime = Utils::GetCurrentTimeMicroSecs();
        ShowUpToDatePopup();
    }
}

void TrackItemsProxy::ShowProgressBarWithTimer( bool isToEnd ) {
    ShowProgressBar( isToEnd );
    if (!mProgressBarTimer) {
        mProgressBarTimer = ecore_timer_add( HIDE_EARLY_PROGRESS_BAR_TIMEOUT, hide_progress_bar_by_timer_cb, this );
    }
}

void TrackItemsProxy::ShowProgressBar( bool isToEnd )
{
    Log::debug( "%s : ShowProgressBar", TRACK_TAG );
    if ( (m_ProgressBarLayout == NULL) && m_ProgressBarLayoutName && m_ProgressBarApplied) {

        HideConnectionError();

        m_ProgressBarAboveHeader = (m_HeaderArea && (!isToEnd || m_Provider->GetDataCount() == 0));
        m_ProgressBarLayout = elm_layout_add(m_ProgressBarAboveHeader ? m_HeaderArea : m_DataArea);
        if(m_ItemsCache->isEmpty()) {
            std::string st = m_ProgressBarLayoutName;
            st += "_big";
            elm_layout_file_set( m_ProgressBarLayout, Application::mEdjPath, st.c_str());
        } else {
            elm_layout_file_set( m_ProgressBarLayout, Application::mEdjPath, m_ProgressBarLayoutName );
        }

        if (m_ProgressBarAboveHeader) {
            elm_box_pack_start( m_HeaderArea, m_ProgressBarLayout );
        } else if (isToEnd) {
            elm_box_pack_end( m_DataArea, m_ProgressBarLayout );
        } else {
            elm_box_pack_start( m_DataArea, m_ProgressBarLayout );
        }
        evas_object_show( m_ProgressBarLayout );

        m_ShowServiceWidgetOnBottom = isToEnd; //property should be identical for progressbar and no connection_error

        m_ProgressBar = elm_progressbar_add( m_ProgressBarLayout );
        elm_object_style_set( m_ProgressBar, "process_medium" );
        elm_progressbar_pulse( m_ProgressBar, EINA_TRUE );
        elm_box_pack_end( m_ProgressBarLayout, m_ProgressBar );
        evas_object_show( m_ProgressBar );

        elm_layout_content_set( m_ProgressBarLayout, "elm.progressbar.swallow", m_ProgressBar );
    }
}

Evas_Object* TrackItemsProxy::GetScroller() const
{
    return m_Scroller;
}

Evas_Object* TrackItemsProxy::GetDataArea() const
{
    return m_DataArea;
}

Evas_Object* TrackItemsProxy::GetProgressBar() const
{
    return m_ProgressBar;
}

TrackItemsProxy::ProxySettings* TrackItemsProxy::GetProxySettings() const
{
    return m_ProxySettings;
}

void TrackItemsProxy::SetProxySettings( ProxySettings *proxySettings )
{
    m_ProxySettings = proxySettings;
    m_ItemsCache->SetSettings( proxySettings->wndSize, proxySettings->wndStep, proxySettings->initialWndSize );
}

void TrackItemsProxy::CleanRequest()
{
    m_DataRequest = NULL;
}

bool TrackItemsProxy::ShiftDown( bool isPreCreate )
{
    return m_ItemsCache->ShiftDown( isPreCreate );
}

bool TrackItemsProxy::ShiftUp()
{
    return m_ItemsCache->ShiftUp();
}

void TrackItemsProxy::fetch_edge_data( void *data ) {
    Log::debug( "%s : fetch_edge_data", TRACK_TAG );
    TrackItemsProxy* object = static_cast<TrackItemsProxy*>( data );
    if( object ) {
        object->m_RequestJob = NULL;
        object->FetchEdgeItems();
    }
}

void TrackItemsProxy::request_data_ASC( void *data ) {
    Log::debug( "%s : request_data_ASC", TRACK_TAG );
    TrackItemsProxy* object = static_cast<TrackItemsProxy*>( data );
    if( object ) {
        object->m_RequestJob = NULL;
        object->RequestData( false );
    }
}

void TrackItemsProxy::request_data_DSC( void *data ) {
    Log::debug( "%s : request_data_DSC", TRACK_TAG );
    TrackItemsProxy* object = static_cast<TrackItemsProxy*>( data );
    if( object ) {
        object->m_RequestJob = NULL;
        object->RequestData( true );
    }
}

void TrackItemsProxy::begin_edge_of_scroll_reached( void *data, Evas_Object *obj, void *event_info )
{
    Log::debug( "%s : begin_edge_of_scroll_reached", TRACK_TAG );
    TrackItemsProxy* object = static_cast<TrackItemsProxy*>( data );
    assert(object);

    if (object->IsRealTopEdgeOfScroller()) {
        object->ScrollerReachedTop(false);//used in NF to show status menu in footer
        object->ShowHeaderWidget();//used in all feeds except NF
        object->m_AllowNewStoriesBtn = false;
        object->m_NewItemsWereAddedButNotShown = false;
        object->m_CheckForDuplicatesShiftUp = true;
    } else {
        object->HideConnectionError();
    }

    if ( !object->GetProgressBar() ) {
        if ( !object->ShiftUp() && !object->DataIsFiltered() ) {
            // if Data Provider contains not enough data -- request more
            if ( object->isReadyToRequest() ) {
                if(object->m_RequestJob) {
                    ecore_job_del( object->m_RequestJob );
                    object->m_RequestJob = NULL;
                }
                if( object->IsRealTopEdgeOfScroller() &&
                    ( object->m_Provider->GetProviderType() == HOME_PROVIDER ||
                      object->m_Provider->GetProviderType() == FEED_PROVIDER ||
                      object->m_Provider->GetProviderType() == NOTIFICATIONS_PROVIDER ||
                      object->m_Provider->GetProviderType() == FRIENDS_REQUESTS_BATCH_PROVIDER ||
                      object->m_Provider->GetProviderType() == FRIENDS_PROVIDER ||
                      object->m_Provider->GetProviderType() == WHO_LIKED_PROVIDER)
                  )
                {
                    Log::debug( "%s : begin_edge_of_scroll_reached : fetch top items for %s screen", TRACK_TAG, object->m_Identifier );
                    if(ConnectivityManager::Singleton().IsConnected()) {
                        object->ShowProgressBar(false);
                    }
                    else {
                        object->ShowConnectionError(false);
                    }
                    object->m_RequestJob = ecore_job_add(fetch_edge_data, object);
                }
                else
                {
                    object->m_RequestJob = ecore_job_add(request_data_ASC, object);
                }
                if(!object->m_RequestJob) {
                    Log::error( "%s : TrackItemsProxy::begin_edge_of_scroll_reached ecore_job_add returns NULL", TRACK_TAG );
                }
            }
        }
    }
    else if (!object->mProgressBarTimer) {
        Log::error( "%s : begin_edge_of_scroll_reached : Set missed PB Timeout for %s screen", TRACK_TAG, object->m_Identifier );
        object->mProgressBarTimer = ecore_timer_add( HIDE_EARLY_PROGRESS_BAR_TIMEOUT, hide_progress_bar_by_timer_cb, object );
    }
    if (!object->GetProvider()->IsReverseChronological()) {
        object->EnableRequestData( true );
    }
}

void TrackItemsProxy::end_edge_of_scroll_reached( void *data, Evas_Object *obj, void *event_info )
{
    TrackItemsProxy* proxy = static_cast<TrackItemsProxy*>( data );

    long long diff = Utils::GetCurrentTimeMicroSecs() - proxy->m_LastRectangleProcessingTime;
    Log::debug( "%s : end_edge_of_scroll_reached print_diff=%u", TRACK_TAG ,diff);
    if ( proxy->GetProgressBar() == NULL ) {
        if ( !proxy->ShiftDown() ) {
            if(proxy->m_RequestJob) {
                ecore_job_del( proxy->m_RequestJob );
                proxy->m_RequestJob = NULL;
            }
            proxy->m_RequestJob = ecore_job_add(request_data_DSC, proxy);
            if(!proxy->m_RequestJob) {
                Log::debug( "TrackItemsProxy::end_edge_of_scroll_reached ecore_job_add returns NULL");
            }
        }
    }

    evas_object_smart_callback_del( proxy->m_Scroller, EDGE_BOTTOM, end_edge_of_scroll_reached );
    proxy->mIsEndEdgeCbAdded = false;

    if(proxy->m_Provider->IsReverseChronological()){ // not ideal approach but to allow auto fetch new comments if we on bottom
        proxy->m_ShowServiceWidgetOnBottom = true;
    }
}

Eina_Bool TrackItemsProxy::async_data_area_resize(void *data) {
    TrackItemsProxy *me = static_cast<TrackItemsProxy*>(data);
    assert(me);

    Evas_Coord w, h;
    evas_object_geometry_get(me->GetDataArea(), nullptr, nullptr, &w, &h);
    // ToDo: 2 lines below look strange, but I didn't find any other working solution. If someone knows better solution please replace.
    evas_object_resize(me->GetDataArea(), w, h-1);
    evas_object_resize(me->GetDataArea(), w, h);

    me->mAsyncResizeTimer = nullptr;
    return ECORE_CALLBACK_CANCEL;
}

void TrackItemsProxy::scroller_resize_cb(void *data, Evas *e, Evas_Object *obj, void *event_info) {
    TrackItemsProxy *me = static_cast<TrackItemsProxy *> (data);
    if (me) {
        me->RefreshRectangles();
        me->OnScrollerResizeActions();
        if (!me->mAsyncResizeTimer) {
            me->mAsyncResizeTimer = ecore_timer_add(0.5, async_data_area_resize, me);
        } else {
            ecore_timer_reset(me->mAsyncResizeTimer);
        }
    }
}


void TrackItemsProxy::on_connectionerror_widget_handler(void *data, Evas_Object *obj, const char  *emission, const char  *source) {
    // repeat data request
    TrackItemsProxy* object = static_cast<TrackItemsProxy*>( data );
    if(object) {
        if(object->m_RequestJob) {
            ecore_job_del( object->m_RequestJob );
            object->m_RequestJob = NULL;
        }
        if(object->m_ShowServiceWidgetOnBottom){
            object->m_RequestJob = ecore_job_add(request_data_DSC, object);
        }
        else if( object->m_Provider->GetProviderType() == HOME_PROVIDER ||
                 object->m_Provider->GetProviderType() == FEED_PROVIDER ) {
            if ( ConnectivityManager::Singleton().IsConnected() ) {
                object->ShowProgressBar(false);
            }
            object->m_RequestJob = ecore_job_add(fetch_edge_data, object);
        }
        else{
            object->m_RequestJob = ecore_job_add(request_data_ASC, object);
        }
    }
}

void TrackItemsProxy::UpdateErase()
{
    Log::debug("%s : TrackItemsProxy::UpdateErase [ %s ]", TRACK_TAG, m_Identifier);
    m_IsNeedToErase = true;
}

void TrackItemsProxy::PendingErase()
{
    Log::debug( "TrackItemsProxy::PendingErase" );
    EraseWindowData();
    EraseHeader();
    m_UploadHistory.m_DataCount = 0;
    m_UploadHistory.m_IsDescendingOrder = true;
    m_IsMuteRequested = false;
    m_ItemsCache->ResetCurrentWindowSize();
}

void TrackItemsProxy::SetDataAreaHint (bool expand) {
    if (expand) {
        evas_object_size_hint_weight_set(m_DataArea, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    } else {
        evas_object_size_hint_weight_set(m_DataArea, FB_EVAS_HINT_DEFAULT, FB_EVAS_HINT_DEFAULT);
    }
}

void TrackItemsProxy::Update(AppEventId eventId, void * data)
{
    Log::debug("TrackItemsProxy::Update start, eventId [ %d ],  [ %s ]", eventId, m_Identifier);

    if(!((eventId == eUNDEFINED_EVENT) || (eventId == eDATA_CHANGES_EVENT)
       ||(eventId == eDATA_IS_READY_EVENT) || (eventId == eREQUEST_COMPLETED_EVENT)
       ||(eventId == eREQUEST_CONTINUE_EVENT) )) {
        return;
    }

    if ( !m_IsUpdateCacheEnabled && m_IsNeedToErase ) {
        //
        // It means that some dependent screen is disabled cache erase for some time.
        return;
    }
    Log::debug("%s : TrackItemsProxy::Update [ %s ]", TRACK_TAG, m_Identifier);
    if ( m_IsNeedToErase ) {
        Log::debug("%s : TrackItemsProxy::Update Need to Erase before [ %s ].", TRACK_TAG, m_Identifier);
        DataErasedSignal();
        UpdateCachedData();
    }

    if(eventId == eDATA_CHANGES_EVENT) {
        DataEventDescription *dataEvent = static_cast<DataEventDescription*>(data);
        if(dataEvent && ( dataEvent->GetProvider() == m_Provider)) {
            m_ItemsCache->UpdateDataForItems();
            if(dataEvent->GetDataEvent() == DataEventDescription::eDE_POST_UPDATED) {
                Log::debug("%s :TrackItemsProxy::Update->eDE_POST_UPDATED in %s", TRACK_TAG, m_Identifier);
                GraphObject *post = m_Provider->GetGraphObjectById(dataEvent->GetItemId());
                if (post && IsPresentItem(post, m_UpdateItemsWndComp)) {
                    Evas *evas = evas_object_evas_get(m_Scroller);
                    evas_event_freeze (evas); //UpdateItemsWindow() performs huge of layout/re-layouts operations. So, we need to freeze evas during these operations. Otherwise a crash may occur.
                    m_ItemsCache->UpdateOneItem(post);
                    evas_event_thaw(evas);
                }
            } else if((dataEvent->GetDataEvent() == DataEventDescription::eDE_COMMENT_EDITED)) {
                //UpdateItemsWindow will be replaced onto method which rebuilds 1 item instead of all comments list
                if(m_Provider->GetGraphObjectById(dataEvent->GetItemId())) {
                    Log::debug("%s :TrackItemsProxy::Update->eDE_COMMENT_EDITED in %s", TRACK_TAG, m_Identifier);
                    UpdateItemsWindow();
                }
            } else if((dataEvent->GetDataEvent() == DataEventDescription::eDE_ITEM_ADDED)) {
                Log::debug("%s :TrackItemsProxy::Update->eDE_ITEM_ADDED to %s %d", TRACK_TAG, m_Identifier, m_Provider->IsReverseChronological());

                SetDataAreaHint( !m_Provider->GetDataCount() && m_UploadHistory.m_DataCount<=0 );
                m_ItemsCache->NormalizeWindowIndex( m_Provider->IsReverseChronological(), 1 );//should be missed for screen with ReverseChronological (CommentsScreen)
                elm_object_scroll_hold_push( m_Scroller );

                AddPresenter();//creates presenter for comments screen
                SetDataAvailable( m_Provider->GetDataCount() > 0 );

                if(!dataEvent->GetAddedToEnd()) {//creates presenters for feeds
                    SetShowSinglePresenter();

                    if (!m_DisableShiftUpForPresenter) {
                        m_ItemsCache->ShiftUp();
                    }

                    if(m_ShowSingleItem) {
                        m_ShowSingleItem = false;
                        if(m_ConnectionErrorLayout) {
                             //if connection error is on screen we delete big error wgt and recreate on top small error wgt
                             ShowConnectionError(false);
                        }
                    }

                    m_DisableShiftUpForPresenter = false;
                }
                elm_object_scroll_hold_pop( m_Scroller );
            } else if (dataEvent->GetDataEvent() == DataEventDescription::eDE_ITEM_REMOVED) {
                Log::debug("%s TrackItemsProxy::Update->eDE_ITEM_REMOVED from %s", TRACK_TAG, m_Identifier);
                void * item = GetItemById(dataEvent->GetItemId());
                RemovePresenter(item);
                if(!item) {
                    m_ItemsCache->NormalizeWindowIndex( m_Provider->IsReverseChronological(), -1 );
                    //decrease indexes back if some shared items are in progress but have no presenters (feed is not on top)
                    //If many items are in progress, we avoid creating duplicate items, it works faster than using FindItem() in CreateItem()
                } else {
                    RemovePresenter(item);
                }
                SetDataAvailable( m_Provider->GetDataCount() > 0 );
            }
        }
        return;
    }

    Log::debug( "%s : TrackItemsProxy: update data.", TRACK_TAG );
    HideProgressBar();
    if(eventId == eREQUEST_COMPLETED_EVENT) {
        RequestCompleteEvent *eventData = static_cast<RequestCompleteEvent *>(data);
        if(eventData) {
            if(eventData->mCurlCode == CURLE_OK) {
                HideConnectionError();
            } else {
                // show error
                ShowConnectionError(m_UploadHistory.m_IsDescendingOrder);
            }
        }
    }
    MutexLocker locker( &m_Mutex );
    Log::debug( "TrackItemsProxy::Update after mutex" );
//It is not useless code, it allows to show "No notifications" or "connection error" service items
//in the center of the screen, reverted and was added additional condition
    SetDataAreaHint( !m_Provider->GetDataCount() && !m_UploadHistory.m_DataCount );
    // How many items has been received
    int dataCount = m_Provider->GetDataCount() - m_Provider->GetPresentersCount();
    int added = dataCount - m_UploadHistory.m_DataCount;

    if ( m_IsMuteRequested ) {
        m_AddedNewItems += added;
    }

    if (!m_RequestedToRebuildAllItems) {
        UpdateHistoryCount();
    }
    SetDataAvailable( dataCount > 0 );
    elm_object_scroll_hold_push( m_Scroller );

    if ( m_RequestedToRebuildAllItems ) {
        RebuildAllItems();//now overridden in Notif/Friends screens only
        m_RequestedToRebuildAllItems = false;
        elm_object_scroll_hold_pop( m_Scroller );
        return;
    }

    if (m_Provider->GetProviderType() == FRIENDS_REQUESTS_BATCH_PROVIDER) {
        // on any event for/from FriendsRequestScreen // just shift down to update
        Log::debug( "%s : TrackItemsProxy::Update added %d items for %s, order = %d", TRACK_TAG, added, m_Identifier, m_UploadHistory.m_IsDescendingOrder);
        if(m_UploadHistory.m_IsDescendingOrder) {
            m_ItemsCache->ShiftDown();
        }
    } else if ( added > 0 || m_ItemsCache->isEmpty() ) {
        Log::debug( "%s : TrackItemsProxy::Update start creating items %d for screen %s", TRACK_TAG, added, m_Identifier);
        m_ItemsCache->CheckProviderSizeChanged(m_UploadHistory.m_IsDescendingOrder);//for comments screen, otherwise impossible to drag up top items

        if ( m_IsMuteRequested ) {
            //for feed screens, otherwise impossible to update top with new items
            if(!m_Provider->GetPresentersCount()) {
                AppendMuteItems();
            }
            if ( m_AllowNewStoriesBtn && m_NewItemsWereAddedButNotShown ) {
                NewItemsAvailable();// only for feed
            }
        } else {
            if ( m_UploadHistory.m_IsDescendingOrder ) {
                m_ItemsCache->ShiftDown();
            } else {
                m_ItemsCache->ShiftUp();
            }
        }
    } else if ( !m_UploadHistory.m_IsDescendingOrder && !m_IsMuteRequested ) {
        if(!m_Provider->IsReverseChronological()) {//disable for comments Screen
            ChangeYCoordinate( TINY_SCROLL_PADDING, true );
        }
    }
    elm_object_scroll_hold_pop( m_Scroller );
    if ( m_UploadHistory.m_UploadResult != ONLY_CACHED_DATA_RESULT ) {
        m_Provider->Unsubscribe( this );
    }
    Log::debug( "%s : TrackItemsProxy: update finished", TRACK_TAG );

    if ( !m_ManualIdleTimer ) {
        m_ManualIdleTimer = ecore_timer_add( REAL_IDLE_STATE_TIMEOUT, user_idle_state_cb, this );
    }
    ActivatePeriodicalRedraw();
#ifdef MONITOR_SCROLL_CORRUPTION
    if (!m_StateTimer && !strcmp(m_Identifier, "HOME_SCREEN")) {
        m_StateTimer = ecore_timer_add( REAL_IDLE_STATE_TIMEOUT, state_timer_cb, this );
    }
#endif
    Log::debug( "TrackItemsProxy::Update end" );
}

void TrackItemsProxy::SetUpdateCacheEnable( bool isEnable )
{
    m_IsUpdateCacheEnabled = isEnable;
}

void TrackItemsProxy::UpdateCachedData()
{
    Log::debug( "TrackItemsProxy::UpdateCachedData()." );
    if ( m_IsNeedToErase ) {
        m_IsNeedToErase = false;
        PendingErase();
    }
}

unsigned int TrackItemsProxy::ClearOperations()
{
    unsigned int operationsRemoved = 0;
    Eina_List *l, *l_next, *operations = m_Provider->GetOperationPresenters();
    void *itemData;
    EINA_LIST_FOREACH_SAFE( operations, l, l_next, itemData ) {
        DestroyItemByData( itemData, m_OperationsComparator, false );
        ++operationsRemoved;
    }
    return operationsRemoved;
}

void TrackItemsProxy::ChangeYCoordinate( int padding, bool isFromTop )
{
    Evas_Coord x, y, w, h;
    if ( padding == 0 ) {
        padding = TINY_SCROLL_PADDING;
    }
    elm_scroller_region_get( m_Scroller, &x, &y, &w, &h );
    if ( isFromTop ) {
        elm_scroller_region_show( m_Scroller, x, padding, w, h );
    } else {
        elm_scroller_region_show( m_Scroller, x, y - padding, w, h );
    }
}

void TrackItemsProxy::Redraw()
{
    Log::debug( "%s : TrackItemsProxy::Redraw", TRACK_TAG );
    if(m_Scroller) {
        DeleteTopEdgeCB();
        Evas_Coord x, y, w, h;
        elm_scroller_region_get( m_Scroller, &x, &y, &w, &h );
        elm_object_scroll_hold_push( m_Scroller );
        m_ItemsCache->RedrawWindow();
        elm_scroller_region_show( m_Scroller, x, y, w, h );
        elm_object_scroll_hold_pop( m_Scroller );
        if ( !m_DelayedRegEdgesCbTimer ) {
            m_DelayedRegEdgesCbTimer = ecore_timer_add( 1.0, TrackItemsProxy::delayed_registration_of_edged_cb, this );
        }
    }
}

void TrackItemsProxy::UpdateItemsWindow()
{
    Evas *evas = evas_object_evas_get(m_Scroller);
    evas_event_freeze (evas); //UpdateItemsWindow() performs huge of layout/re-layouts operations. So, we need to freeze evas during these operations. Otherwise a crash may occur.
    m_ItemsCache->UpdateItemsWindow();
    evas_event_thaw(evas);
}

bool TrackItemsProxy::DestroyItem( void *item, ItemsComparator comparator, bool updateHistoryCount )
{
    Utils::DoubleSidedStack *itemsStack = m_ItemsCache->GetItemsStack();
    bool isItemExist = itemsStack->RemoveItem( item, comparator );
    if ( isItemExist ) {
        Log::debug( "%s : TrackItemsProxy::DestroyItem for %s", TRACK_TAG , m_Identifier );
        Evas_Coord height = 0;
        Evas_Coord widgetHeight = 0;
        m_ItemsCache->DecreaseIndex();
        if (updateHistoryCount) {
            UpdateHistoryCount();
        }
        widgetHeight = GetWidgetHeight( item );
        height = DeleteItemDirectly( item );
        if(height) {
            Evas_Coord x,y,w,h;
            elm_scroller_region_get( GetScroller(), &x, &y, &w, &h );
            elm_scroller_region_show( GetScroller(), x, y-height, w, h );
            Log::debug( "%s : TrackItemsProxy::DestroyItem->compensate height", TRACK_TAG );
        }
        RefreshRectangles(true, widgetHeight);
    }
    return isItemExist;
}

bool TrackItemsProxy::DestroyItemByData( void *data, ItemsComparator comparator,  bool updateHistoryCount )
{
    Utils::DoubleSidedStack *itemsStack = m_ItemsCache->GetItemsStack();
    void *item = itemsStack->GetItem( data, comparator );
    if ( item ) {
        Log::debug( "%s : TrackItemsProxy::DestroyItemByData for %s", TRACK_TAG , m_Identifier );
        Evas_Coord height = 0;
        Evas_Coord widgetHeight = 0;
        itemsStack->RemoveItem( data, comparator );
        m_ItemsCache->DecreaseIndex();
        if (updateHistoryCount) {
            UpdateHistoryCount();
        }
        widgetHeight = GetWidgetHeight( item );
        height = DeleteItemDirectly( item );
        if(height) {
            Evas_Coord x,y,w,h;
            elm_scroller_region_get( GetScroller(), &x, &y, &w, &h );
            elm_scroller_region_show( GetScroller(), x, y-height, w, h );
            Log::debug( "%s : TrackItemsProxy::DestroyItemByData->compensate height", TRACK_TAG );
        }
        RefreshRectangles(true, widgetHeight);
        return true;
    }
    return false;
}

void * TrackItemsProxy::DestroyItemByDataKeepAnD( void *data, ItemsComparator comparator)
{
    Utils::DoubleSidedStack *itemsStack = m_ItemsCache->GetItemsStack();
    void *item = itemsStack->GetItem( data, comparator );
    if (item) {
        Log::debug( "%s : TrackItemsProxy::DestroyItemByDataKeepAnD for %s", TRACK_TAG , m_Identifier );
        itemsStack->RemoveItem( data, comparator );
        m_ItemsCache->DecreaseIndex();
    }
    return item;
}

void TrackItemsProxy::UpdateHistoryCount() {
    m_UploadHistory.m_DataCount = m_Provider->GetDataCount() - m_Provider->GetPresentersCount();
    Log::debug( "%s : TrackItemsProxy::UpdateHistoryCount mHistoryCount = %d for %s", TRACK_TAG ,  m_UploadHistory.m_DataCount , m_Identifier );
    Log::debug( "%s : TrackItemsProxy::UpdateHistoryCount DataCount =  %u PresentersCount = %u", TRACK_TAG ,
                                              m_Provider->GetDataCount() , m_Provider->GetPresentersCount() );
}

bool TrackItemsProxy::IsPresentItem( void *item, ItemsComparator comparator )
{
    Utils::DoubleSidedStack *itemsStack = m_ItemsCache->GetItemsStack();
    return itemsStack->IsPresentItem( item, comparator );
}

TrackItemsProxy::MouseMovement* TrackItemsProxy::GetMouseMovement()
{
    return &m_MouseMovement;
}

bool TrackItemsProxy::IsWaitingForData() const
{
    return m_ProgressBar != NULL;
}

void TrackItemsProxy::AnimatedTop()
{
    Log::debug( "%s : TrackItemsProxy::AnimatedTop", TRACK_TAG );
    if(m_ItemsCache->isEmpty()) {
        return;
    }
    HideConnectionError();
    DisableMouseUpDownEvents( true );
    elm_object_scroll_lock_y_set(m_Scroller, true);
    m_IsInScrollAnimation = false;
    if( mIsEndEdgeCbAdded )
    {
        evas_object_smart_callback_del( m_Scroller, EDGE_BOTTOM, end_edge_of_scroll_reached );
        mIsEndEdgeCbAdded = false;
    }
    DeactivateRectangleCb();

    SetProxySettings( m_ProxySettings );
    Utils::DoubleSidedStack *itemsStack = m_ItemsCache->GetItemsStack();
    Log::debug( "ANIM_TOP : before header [ %d ], stack [ %d ]", GetHeaderSize(), itemsStack->GetSize() );

    int limit = 0;
    if ( GetHeaderSize() < m_HeaderSize ) {
        int toDel = m_HeaderSize - GetHeaderSize();
        limit = itemsStack->GetSize() >= toDel ? toDel : itemsStack->GetSize();
    }
    m_ItemsCache->DeleteFromBottom( limit );

    Log::debug( "ANIM_TOP : after header [ %d ], stack [ %d ]", GetHeaderSize(), itemsStack->GetSize() );
    ShowOffStagedItems();
    ShowHeader( itemsStack );
    m_ItemsCache->SetIndex( 0, itemsStack->GetSize() - 1 );

    m_CheckForDuplicatesShiftDown = true;

    if (!m_BringInRegTimer) {
        m_BringInRegTimer = ecore_timer_add( 0.35, bring_in_region_cb, this );
    }
    UpdateItemsWindow();
    ChangeYCoordinate(0,true);
    EnableRequestData( true );
    if ( GetProgressBar() == NULL ) {
        if ( isReadyToRequest() ) {
            if(ConnectivityManager::Singleton().IsConnected()) {
                ShowProgressBar(false);
            }
            else {
                ShowConnectionError(false);
            }
            if(m_RequestJob) {
                ecore_job_del( m_RequestJob );
                m_RequestJob = NULL;
            }
            m_RequestJob = ecore_job_add(fetch_edge_data, this);
            if(!m_RequestJob) {
                Log::debug( "%s : TrackItemsProxy::AnimatedTop ecore_job_add returns NULL", TRACK_TAG );
            }
        }
    }
    ScrollerReachedTop( true );
    DisableMouseUpDownEvents( false );
    elm_object_scroll_lock_y_set(m_Scroller, false);

    m_NewItemsWereAddedButNotShown = false;
}

bool TrackItemsProxy::isReadyToRequest()
{
    return m_IsSwipeUp_FromTop;
}

void TrackItemsProxy::EnableRequestData( bool isEnabled )
{
    m_IsSwipeUp_FromTop = isEnabled;
}

void TrackItemsProxy::SetMuteTimerEnabled( bool isEnabled )
{
    m_IsMuteTimerEnabled = isEnabled;
}

void TrackItemsProxy::PreFetchTail()
{
    Log::debug( "%s : TrackItemsProxy::PreFetchTail m_ItemsWindow.lowerIndex = %d %s", TRACK_TAG, m_ItemsCache->GetLowerIndex(), m_Identifier);
    Log::debug( "%s : TrackItemsProxy::PreFetchTail DataCount = %d", TRACK_TAG, m_Provider->GetDataCount());

    if ( m_ItemsCache->GetLowerIndex() + TrackItemsProxy::MAX_DATA_PROVIDER_ITEMS_REST > m_Provider->GetDataCount() ) {
        UpdateHistoryCount();
        m_UploadHistory.m_IsDescendingOrder = true;
        UPLOAD_DATA_RESULT result = m_Provider->UploadData( m_UploadHistory.m_IsDescendingOrder );
        Log::debug( "%s : PreFetchTail, result is [ %d ]", TRACK_TAG, result );
    }
    //
    // we do not care about return code. Just try to send a request
}

Eina_Array* TrackItemsProxy::GetItemsIds()
{
    Utils::DoubleSidedStack *itemsStack = m_ItemsCache->GetItemsStack();
    unsigned int size = itemsStack->GetSize();
    Eina_Array *ids = ( size > 0 ? eina_array_new( size ) : NULL );
    for ( Utils::DoubleSidedStack::ConstIterator iter = itemsStack->Begin();
            iter; iter = itemsStack->Next( iter ) ) {
        if ( !eina_array_push( ids, GetItemId( iter->data ) ) ) {
            Log::error( "TrackItemsProxy::GetItemsIds, cannot push id" );
        }
    }
    return ids;
}

bool TrackItemsProxy::FindItemId(void *item, const char *id) {
    char* itemId = GetItemId(item);
    if (itemId) {
        if (id && !strcmp(id, itemId)) {
            free(itemId);
            return true;
        }
        free(itemId);
    }
    return false;
}

void * TrackItemsProxy::GetItemById( const char * itemId )
{
    assert( itemId );
    void *item = NULL;
    if(m_ItemsCache) {
        Utils::DoubleSidedStack *itemsStack = m_ItemsCache->GetItemsStack();
        for (Utils::DoubleSidedStack::ConstIterator iter = itemsStack->Begin(); iter; iter = itemsStack->Next(iter)) {
            if (FindItemId(iter->data, itemId)) {
                item = iter->data;
                break;
            }
        }
    }
    return item;
}

void TrackItemsProxy::CustomUpdateItem( AppEventId eventId, const char *itemId )
{
    assert( itemId );
    void * item = GetItemById(itemId);
    if (item) {
        Update( eventId, item );
    }
}

Evas_Object* TrackItemsProxy::add_rectangle_object( Evas_Coord w, Evas_Coord h, const Evas_Common_Interface *box )
{
    Evas_Object *rect = evas_object_rectangle_add(evas_object_evas_get(box));
    evas_object_color_set(rect, 217, 219, 224, 0);
    evas_object_size_hint_min_set(rect, w, h);
    evas_object_show(rect);
    return rect;
}

void TrackItemsProxy::SetOperationsComparator( ItemsComparator comparator ) {
    m_OperationsComparator = comparator;
}

Evas_Coord TrackItemsProxy::DeleteAssociatedWithItemRect(Evas_Object * itemToCompare)
{
    Eina_List *l, *l_next;
    void *element = NULL;

    Eina_List* childs = elm_box_children_get( m_DataArea );

    int h = 0;

    EINA_LIST_FOREACH_SAFE( childs, l, l_next, element ) {
        Evas_Object *child = static_cast<Evas_Object*>(element);
        if ( child ) {
            const char *type = evas_object_type_get( child );
            if ( strcmp( type, RECTANGLE ) == 0 ) {
                Evas_Object *layout = static_cast<Evas_Object*>(evas_object_data_get( child, FOREGROUND_RECT ));
                if ( layout == itemToCompare ) {
                    Log::debug("TrackItemsProxy::DeleteAssociatedWithItemRect->Rect was found and deleted!");
                    evas_object_geometry_get( child, nullptr, nullptr, nullptr, &h );
                    elm_box_unpack( m_DataArea, child );
                    evas_object_del( child );
                    evas_object_data_del(child,FOREGROUND_RECT);
                    break;
                }
            }
        }
    }
    eina_list_free(childs);
    return h;
}

void TrackItemsProxy::ShowOffStagedItems()
{
    Eina_List *l, *l_next;
    void *element = NULL;

    Eina_List* childs = elm_box_children_get( m_DataArea );

    EINA_LIST_FOREACH_SAFE( childs, l, l_next, element ) {
        Evas_Object *child = static_cast<Evas_Object*>(element);
        if ( child ) {
            const char *type = evas_object_type_get( child );
            if ( strcmp( type, RECTANGLE ) == 0 ) {
                Evas_Object *layout = static_cast<Evas_Object*>(evas_object_data_get( child, FOREGROUND_RECT ));
                if ( layout ) {
                    elm_box_pack_after( m_DataArea, layout, child );
                    elm_box_unpack( m_DataArea, child );
                    evas_object_show( layout );
                    evas_object_del( child );
                }
            }
        }
    }
    eina_list_free(childs);
}

void TrackItemsProxy::RefreshRectangles( bool use_exact_algorithm, int offset )
{
    Evas_Coord x, y, w, h, gx, gy, gw, gh;
    Eina_List *l, *l_next;
    void *element = NULL;

    Eina_List* childs = elm_box_children_get( m_DataArea );

    evas_object_geometry_get( m_Scroller, &gx, &gy, &gw, &gh );
    gy += offset;

    EINA_LIST_FOREACH_SAFE( childs, l, l_next, element ) {
        if ( !m_IsRectangleCb ) {
            eina_list_free(childs);
            return;
        }
        Evas_Object *child = static_cast<Evas_Object*>(element);
        if ( child ) {
            evas_object_geometry_get( child, &x, &y, &w, &h );
            const char *type = evas_object_type_get( child );
            if ( w != 0 && h != 0 && !IsServiceItem( type ) && child != m_ProgressBarLayout && child != m_ConnectionErrorLayout ) {
                if ( strcmp( type, RECTANGLE ) == 0 ) {
                    if ( !( y + h < gy - gh/2 || y > gy + gh + gh/2 )  ||
                           ( use_exact_algorithm && ( (y < gy - gh/2 && y + h > gy - gh/2)  ||
                                                      (y < gy + gh + gh/2 && y + h > gy + gh + gh/2)
                                                    )
                       ) ) {
                        Evas_Object *layout = static_cast<Evas_Object*>(evas_object_data_get( child, FOREGROUND_RECT ));
                        if ( layout ) {
                            if ( !m_IsRectangleCb ) {
                                return;
                            }
                            elm_box_pack_after( m_DataArea, layout, child );
                            elm_box_unpack( m_DataArea, child );
                            evas_object_show( layout );
                            evas_object_del( child );
                        }
                    }
                } else {
                    if ((y + h < gy - gh/2  || y > gy + gh + gh/2)) {
                        Evas_Object *rect = add_rectangle_object(w, h, m_DataArea);
                        if ( rect ) {
                            evas_object_data_set( rect, FOREGROUND_RECT, child );
                            elm_box_pack_after( m_DataArea, rect, child );
                            elm_box_unpack( m_DataArea, child );
                            evas_object_hide( child );
                        }
                    }
                }
            }
        }
    }
    eina_list_free(childs);
}

void TrackItemsProxy::off_stage_items_scroll_cb( void *data, Evas_Object *obj, void *event_info )
{
    TrackItemsProxy *proxy = static_cast<TrackItemsProxy*>(data);
    if ( proxy->m_IsRectangleCb ) {
        proxy->m_LastRectangleProcessingTime = Utils::GetCurrentTimeMicroSecs();
        proxy->RefreshRectangles();
    }
    proxy->CheckOffStageUnfollowItems();
}

bool TrackItemsProxy::IsServiceItem( const char *itemType )
{
    return ( strcmp( itemType, ELM_LAYOUT ) != 0 ) && ( strcmp( itemType, RECTANGLE ) != 0 );
}

bool TrackItemsProxy::IsItemOfType(Evas_Object *item, const char *type) {
    if (item) {
        const char *itemType = evas_object_type_get(item);
        if (itemType && type && strcmp(itemType, type) == 0) {
            return true;
        }
    }
    return false;
}

Evas_Object *TrackItemsProxy::GetFirstRectangle(bool isFromTop) {
    Eina_List *l;
    void *element = NULL;
    Evas_Object *result = NULL;

    Eina_List* childs = elm_box_children_get(GetDataArea());

    if (isFromTop) {
        EINA_LIST_FOREACH(childs, l, element) {
            Evas_Object *child = static_cast<Evas_Object*>(element);
            if (IsItemOfType(child, RECTANGLE)) {
                result = child;
                break;
            }
        }
    } else {
        EINA_LIST_REVERSE_FOREACH(childs, l, element) {
            Evas_Object *child = static_cast<Evas_Object*>(element);
            if (IsItemOfType(child, RECTANGLE)) {
                result = child;
                break;
            }
        }
    }

    eina_list_free(childs);
    return result;
}

void TrackItemsProxy::RemoveRectangle( bool isFromTop )
{
    Evas_Object *rect = GetFirstRectangle(isFromTop);

    if (rect) {
        Evas_Object *layout = static_cast<Evas_Object*>(evas_object_data_get(rect, FOREGROUND_RECT));
        if (layout) {
            // existing of layout is just for check
            elm_box_unpack(GetDataArea(), rect);
            evas_object_del(rect);
        }
    }
}

void TrackItemsProxy::ActivateRectangleCb()
{
    if ( !m_IsRectangleCb ) {
        evas_object_smart_callback_add( m_Scroller, SCROLL, off_stage_items_scroll_cb, this );
        m_IsRectangleCb = true;
        ActivatePeriodicalRedraw();
    }
}

void TrackItemsProxy::DeactivateRectangleCb()
{
    if ( m_IsRectangleCb ) {
        evas_object_smart_callback_del( m_Scroller, SCROLL, off_stage_items_scroll_cb );
        m_IsRectangleCb = false;
        DeactivatePeriodicalRedraw();
    }
}

void TrackItemsProxy::ActivatePeriodicalRedraw()
{
    if ( !m_IsPeriodicalRedraw ) {
        assert( !m_PeriodicalTimer );
        if ( !m_PeriodicalTimer ) {
            m_PeriodicalTimer = ecore_timer_add( DELAYED_RECT_REDRAW_TIMEOUT, periodical_rectangle_redraw_cb, this );
            m_IsPeriodicalRedraw = true;
        }
    }
}

void TrackItemsProxy::DeactivatePeriodicalRedraw()
{
    if ( m_IsPeriodicalRedraw ) {
        assert( m_PeriodicalTimer );
        if ( m_PeriodicalTimer ) {
            ecore_timer_del( m_PeriodicalTimer );
            m_PeriodicalTimer = NULL;
            m_IsPeriodicalRedraw = false;
        }
    }
}

bool TrackItemsProxy::IsEmpty() {
    return m_ItemsCache->isEmpty();
}

void* TrackItemsProxy::FindItem(void *data, ItemsComparator comparator) {
    Utils::DoubleSidedStack *itemsStack = m_ItemsCache->GetItemsStack();
    return itemsStack->GetItem(data, comparator);
}

void TrackItemsProxy::delete_cached_item_size( void *itemSize )
{
    ItemSize *size = static_cast<ItemSize*> ( itemSize );
    free( (void*)size->key );
    size->key = NULL;
    delete size;
}

void TrackItemsProxy::ApplyProgressBar(bool applied) {
    m_ProgressBarApplied = applied;
}

/**********************************************************************
 *
 * @class ItemsCache
 *
 **********************************************************************/

const int ItemsCache::ItemsCache::INVALID_WINDOW_INDEX = -1;

ItemsCache::ItemsCache( TrackItemsProxy* parent, AbstractDataProvider *provider )
{
    m_Parent = parent;
    m_Provider = provider;
    m_ItemsWindow.upperIndex = INVALID_WINDOW_INDEX;
    m_ItemsWindow.lowerIndex = INVALID_WINDOW_INDEX;
    m_RemainedItems = 0;
    m_ItemsWindowStack = new Utils::DoubleSidedStack();
    m_MaxWindowSize = TrackItemsProxy::DEFAULT_CACHE_WINDOW_SIZE;
    m_ItemsStep = TrackItemsProxy::DEFAULT_CACHE_WINDOW_STEP;
    m_CurrentWindowSize = TrackItemsProxy::INITIALIZATION_CACHE_WINDOW_SIZE;
    m_AddedHeight = 0;
}

ItemsCache::~ItemsCache()
{
    m_Parent = NULL;
    m_Provider = NULL;
    delete m_ItemsWindowStack; m_ItemsWindowStack = NULL;
}

void ItemsCache::SetSettings( unsigned int wndSize, unsigned int wndStep, unsigned int initWndSize )
{
    assert( m_ItemsStep <= m_MaxWindowSize );
    m_MaxWindowSize = wndSize;
    m_ItemsStep = wndStep;
    m_CurrentWindowSize = initWndSize;
    if ( m_CurrentWindowSize > m_MaxWindowSize ) {
        m_CurrentWindowSize = m_MaxWindowSize;
    }
}

void ItemsCache::EraseWindowData()
{
    if(m_Parent) {
        m_Parent->DeleteAll_Prepare();
        if(m_ItemsWindowStack) {
            while ( m_ItemsWindowStack->GetSize() ) {
                void *downItemToDelete = m_ItemsWindowStack->PopFromEnd();
                if ( !downItemToDelete ) {
                    // It must not be happen
                    Log::debug( "EraseWindowData: DoubleSidedStack returned NULL item." );
                    assert( false );
                }
                m_Parent->DeleteAll_ItemDelete( downItemToDelete );
            }
        }
    }
    m_ItemsWindow.upperIndex = INVALID_WINDOW_INDEX;
    m_ItemsWindow.lowerIndex = INVALID_WINDOW_INDEX;
}

void ItemsCache::NormalizeWindowIndex( bool isDescendigOrder, int shiftValue )
{
    if ( isDescendigOrder ) return;

    if ( m_ItemsWindowStack->GetSize() > 0 ) {
        if ( m_ItemsWindow.upperIndex == INVALID_WINDOW_INDEX ) {
            m_ItemsWindow.upperIndex = 0;
        }
        if ( m_ItemsWindow.lowerIndex == INVALID_WINDOW_INDEX ) {
            m_ItemsWindow.lowerIndex = 0;
        }
        if(m_ItemsWindow.upperIndex + shiftValue >=0) { //fix for AnimatedTop, shift value could be -1 for removed presenters
            m_ItemsWindow.upperIndex += shiftValue;
            m_ItemsWindow.lowerIndex += shiftValue;
        }
        Log::debug( "%s TrackItemsProxy::ItemsCache->NormalizeWindowIndex m_ItemsWindow.upperIndex = %d %s", TRACK_TAG, m_ItemsWindow.upperIndex, m_Parent->m_Identifier );
        Log::debug( "%s TrackItemsProxy::ItemsCache->NormalizeWindowIndex m_ItemsWindow.lowerIndex = %d", TRACK_TAG, m_ItemsWindow.lowerIndex);
    }
}

void ItemsCache::DecreaseIndex()
{
    --m_ItemsWindow.lowerIndex;
    if ( m_ItemsWindow.upperIndex > m_ItemsWindow.lowerIndex ) {
        m_ItemsWindow.upperIndex = INVALID_WINDOW_INDEX;
        m_ItemsWindow.lowerIndex = INVALID_WINDOW_INDEX;
    }
}

void ItemsCache::ResetCurrentWindowSize()
{
    m_CurrentWindowSize = TrackItemsProxy::INITIALIZATION_CACHE_WINDOW_SIZE;
}

bool ItemsCache::ShiftDown( bool isPreCreate )
{
    Log::debug( "%s : ItemsCache::ShiftDown %d", TRACK_TAG, isPreCreate);
    if ( m_Parent->m_ProgressBarLayout ) {
        Log::debug( "%s : ItemsCache::ShiftDown, cannot create new item due to active progress bar.", TRACK_TAG );
        return true;
    }
    if ( m_Provider->IsRequestAlive() && m_Provider->IsDataCached()  &&
         m_Provider->GetProviderType() != FRIENDS_REQUESTS_BATCH_PROVIDER ) {
        Log::debug( "%s : ItemsCache::ShiftDown, disable to add items for [%s] during provider update", TRACK_TAG, m_Parent->m_Identifier );
        return true;
    }
    bool isItFirstAdding = ( m_ItemsWindowStack->GetSize() == 0 );
    unsigned int itemsCount = m_Provider->GetDataCount();
    unsigned int itemsToAdd = 0;
    if ( isPreCreate ) {
        Log::debug( "%s : ShiftDown pre-create", TRACK_TAG );
        itemsToAdd = 1;
    } else {
        if ( m_ItemsWindowStack->GetSize() < m_CurrentWindowSize ) {
            // add new items up to 'cache window limit'
            itemsToAdd = m_CurrentWindowSize - m_ItemsWindowStack->GetSize();
        } else {
            itemsToAdd = ( m_RemainedItems > 0 ? m_RemainedItems : m_ItemsStep );
        }
    }
    Log::debug( "%s ItemsCache::ShiftDown itemsToAdd = %d", TRACK_TAG, itemsToAdd );
    // add items
    while ( itemsToAdd > 0 ) {
        if ( (m_ItemsWindow.lowerIndex + 1) >= itemsCount ) {
            //
            // There is not enough data. Need to request more.
            DeleteFromTop();
            m_RemainedItems = itemsToAdd;
            if ( isItFirstAdding ) {
                Utils::RedrawEvasObject(m_Parent->m_Scroller);
            }
            return false;
        }
        ++m_ItemsWindow.lowerIndex;
        void* itemToCreate = eina_array_data_get( m_Provider->GetData(), m_ItemsWindow.lowerIndex );

        if( m_Parent->m_CheckForDuplicatesShiftDown && m_Parent->FindDuplicateItem(itemToCreate) ) {
            Log::error( "%s ItemsCache::ShiftDown->Duplicate was found" , TRACK_TAG );
            continue; //works one time after Animated to Top
        }

        void *item = m_Parent->CreateItem( itemToCreate, true );
        if ( item ) {
            m_ItemsWindowStack->PushToEnd( item );
            --itemsToAdd;
        } else {
            Log::debug( "ItemsCache::ShiftDown: cannot create an item." );
        }
    }
    if ( (m_ItemsWindow.upperIndex == INVALID_WINDOW_INDEX) && (m_ItemsWindow.lowerIndex != INVALID_WINDOW_INDEX) ) {
        m_ItemsWindow.upperIndex = 0;
    }
    // Delete old items up to available 'cache window'.
    DeleteFromTop();
    m_RemainedItems = 0;
    if ( isItFirstAdding ) {
        Utils::RedrawEvasObject(m_Parent->m_Scroller);
        m_Parent->ChangeYCoordinate( TrackItemsProxy::TINY_SCROLL_PADDING, true );
    }
    if ( m_CurrentWindowSize < m_MaxWindowSize ) {
        m_CurrentWindowSize += ( isPreCreate ? 1 : m_ItemsStep );
        if ( m_CurrentWindowSize > m_MaxWindowSize ) {
            m_CurrentWindowSize = m_MaxWindowSize;
        }
    }

    if(!isPreCreate) {
        m_Parent->m_CheckForDuplicatesShiftDown = false;//works one time after Animated to top
    }

    m_Parent->PreFetchTail();

    return true;
}

void ItemsCache::DeleteFromTop()
{
    if (m_Parent->m_IsInScrollAnimation) {
        m_Parent->m_PostponedTopItemsDelete = true;
        return;
    }
    Log::debug( "%s ItemsCache::DeleteFromTop %d", TRACK_TAG , m_ItemsWindowStack->GetSize() - m_CurrentWindowSize);
    m_Parent->DeactivateRectangleCb();
    unsigned int deletedHeight = 0;
    while ( m_ItemsWindowStack->GetSize() > m_CurrentWindowSize ) {
        void *upperItemToDelete = m_ItemsWindowStack->PopFromBegin();
        if ( !upperItemToDelete ) {
            // It must not be happen
            Log::debug( "ItemsCache::DeleteFromTop: DoubleSidedStack returned NULL item." );
            assert( false );
            m_Parent->ActivateRectangleCb();
            return;
        }
        ItemSize* size = m_Parent->SaveSize( upperItemToDelete );
        if(size) {
            deletedHeight += size->height;
        }
        m_Parent->RemoveRectangle( true );
        m_Parent->AddToDelete( upperItemToDelete );
        ++m_ItemsWindow.upperIndex;
    }
    if ( deletedHeight > 0 ) {
        m_Parent->HideHeaderWidget();
        m_Parent->ChangeYCoordinate( deletedHeight, false );
    }
    m_Parent->ActivateRectangleCb();
}

bool ItemsCache::ShiftUp()
{
    Log::debug( "%s : ItemsCache::ShiftUp", TRACK_TAG );
    unsigned int itemsToAdd = ( m_RemainedItems > 0 ? m_RemainedItems : m_ItemsStep );
    m_AddedHeight = 0;

    Log::debug( "%s ItemsCache::ShiftUp itemsToAdd = %d", TRACK_TAG, itemsToAdd , m_Parent->m_Identifier);

    if(m_Parent->m_ShowSingleItem)
    {
        m_ItemsWindow.upperIndex = 1;
        m_ItemsWindow.lowerIndex = 0;
    }
    // add items
    while ( itemsToAdd > 0 ) {
        if ( (m_ItemsWindow.upperIndex - 1) < 0 ) {
            // There is not enough data. Need to request more.
            DeleteFromBottom( m_CurrentWindowSize );
            m_RemainedItems = itemsToAdd;
            return false;
        }
        --m_ItemsWindow.upperIndex;
        void* itemToCreate = eina_array_data_get( m_Provider->GetData(), m_ItemsWindow.upperIndex );

        if( m_Parent->m_CheckForDuplicatesShiftUp && m_Parent->FindDuplicateItem(itemToCreate) ) {
            Log::error( "%s ItemsCache::ShiftUp->Duplicate was found" , TRACK_TAG );
            continue;
        }

        void *item = m_Parent->AddToCreate( itemToCreate, false );
        if ( item ) {
            m_ItemsWindowStack->PushToBegin( item );
            --itemsToAdd;
            ItemSize *size = m_Parent->GetSize( item );
            if ( size ) {
                m_AddedHeight += size->height;
            } else {
                Log::debug( "ItemsCache::ShiftUp: cannot find size for a specified item." );
            }
        } else {
            Log::debug( "ItemsCache::ShiftUp: cannot create an item." );
        }
    }
    if ( (m_ItemsWindow.lowerIndex == INVALID_WINDOW_INDEX) && (m_ItemsWindow.upperIndex != INVALID_WINDOW_INDEX) ) {
        m_ItemsWindow.lowerIndex = 0;
    }
    // Delete old items up to available 'cache window'.
    DeleteFromBottom (m_CurrentWindowSize);
    m_Parent->m_CheckForDuplicatesShiftUp = false;
    m_RemainedItems = 0;
    return true;
}

void ItemsCache::DeleteFromBottom( unsigned int limit )
{
    if (m_Parent->m_IsInScrollAnimation) {
        m_Parent->m_PostponedBottomItemsDelete = true;
        return;
    }
    Log::debug( "%s ItemsCache::DeleteFromBottom %d", TRACK_TAG , m_ItemsWindowStack->GetSize() - limit);
    bool isItems = m_ItemsWindowStack->GetSize() > limit;
    if ( isItems ) {
        m_Parent->DeactivateRectangleCb();
    }
    while ( m_ItemsWindowStack->GetSize() > limit ) {
        void *downItemToDelete = m_ItemsWindowStack->PopFromEnd();
        if ( !downItemToDelete ) {
            // It must not be happen
            Log::debug( "DeleteFromBottom: DoubleSidedStack returned NULL item." );
            assert( false );
            m_Parent->ActivateRectangleCb();
            return;
        }
        m_Parent->SaveSize( downItemToDelete );
        m_Parent->RemoveRectangle( false );
        m_Parent->DeleteItem( downItemToDelete );
        --m_ItemsWindow.lowerIndex;
    }
    if ( isItems ) {
        if ( m_AddedHeight > 0 ) {
            m_Parent->ChangeYCoordinate( m_AddedHeight, true );
            m_AddedHeight = 0;
        }
        m_Parent->ActivateRectangleCb();
    }
}

void ItemsCache::UpdateDataForItems() {
    for (auto iter = m_ItemsWindowStack->Begin(), nextIter = m_ItemsWindowStack->Next( iter );
            iter; iter = nextIter, nextIter = m_ItemsWindowStack->Next( iter ) ) {
        m_Parent->UpdateDataForItem(iter->data);
    }
}


void ItemsCache::UpdateOneItem(void* data) {

    assert(data);
    Log::debug( "%s ItemsCache::UpdateOneItem", TRACK_TAG);
    m_Parent->DeleteTopEdgeCB();

    elm_object_scroll_hold_push( m_Parent->m_Scroller );

    bool wasFound = false;
    for (auto iter = m_ItemsWindowStack->Begin(), nextIter = m_ItemsWindowStack->Next( iter );
            iter; iter = nextIter, nextIter = m_ItemsWindowStack->Next( iter ) ) {
        void *item = m_Parent->GetDataForItem(iter->data);
        if (data != item) {
            continue;
        } else {
            void *wgt = m_Parent->GetWgtForItem(iter->data);
            if (wgt) {
                int index = 0;
                Log::debug( "%s ItemsCache::UpdateOneItem->IsWidget %s", TRACK_TAG, m_Parent->m_Identifier);
                void *prevItem = nullptr;
                Eina_List* children = elm_box_children_get(m_Parent->GetDataArea());
                Eina_List* l, *l_next;
                void* listdata;
                EINA_LIST_FOREACH_SAFE(children, l, l_next, listdata) {
                    if( listdata == wgt ) {
                        Log::info("BaseProfileScreenLog::UpdateOneItem->Found wgt", wgt);
                        wasFound = true;
                        break;
                    } else {
                        Evas_Object *child = static_cast<Evas_Object*>(listdata);
                        Evas_Object *attachedToGrayRectWgt = static_cast<Evas_Object*>(evas_object_data_get( child, FOREGROUND_RECT ));
                        if ( attachedToGrayRectWgt && attachedToGrayRectWgt == wgt  ) {
                            elm_box_unpack( m_Parent->GetDataArea(), child );
                            evas_object_del(child);
                            wasFound = true;
                            Log::info("BaseProfileScreenLog::UpdateOneItem->Found gray rect", wgt);
                            break;
                        }
                    }
                    prevItem = listdata;
                    index ++;
                }
                eina_list_free(children);
                if (wasFound) {
                    if(!prevItem) {
                        Log::debug( "%s ItemsCache::UpdateOneItem->update first item", TRACK_TAG, m_Parent->m_Identifier);
                        iter->data = m_Parent->UpdateItem(iter->data, false);
                    } else {
                        Log::debug( "%s ItemsCache::UpdateOneItem->update other single item", TRACK_TAG, m_Parent->m_Identifier);
                        iter->data = m_Parent->UpdateSingleItem(iter->data, prevItem);
                    }
                }
                else{
                    Log::error( "%s ItemsCache::UpdateOneItem->can't find item", TRACK_TAG);
                }
            } else {
                Log::error( "%s ItemsCache::UpdateOneItem->No widget in data area", TRACK_TAG);
            }
            break;
        }
    }

    if ( !m_Parent->m_DelayedRegEdgesCbTimer ) {
        m_Parent->m_DelayedRegEdgesCbTimer = ecore_timer_add( 1.0, TrackItemsProxy::delayed_registration_of_edged_cb, m_Parent );
    }
    elm_object_scroll_hold_pop( m_Parent->m_Scroller );
}

void ItemsCache::UpdateItemsWindow()
{
    Log::debug( "%s ItemsCache::UpdateItemsWindow", TRACK_TAG);
    m_Parent->DeleteTopEdgeCB();

    elm_object_scroll_hold_push( m_Parent->m_Scroller );
    m_Parent->DeleteAll_Prepare();
    unsigned int deletedItems = 0;
    unsigned int deletedHeight = 0;
    for (auto iter = m_ItemsWindowStack->Begin(), nextIter = m_ItemsWindowStack->Next( iter );
            iter; iter = nextIter, nextIter = m_ItemsWindowStack->Next( iter ) ) {
        void *data = m_Parent->GetDataForItem(iter->data);
        if ( !m_Provider->IsContainItem(data) ) {
            Log::info( "UpdateItemsWindow, delete items which is not in a provider's cache." );
            ItemSize *size = m_Parent->GetItemSize( iter->data );
            if ( size ) {
                deletedHeight += size->height;
                delete size;
            }
            m_ItemsWindowStack->RemoveItem(data, m_Parent->m_UpdateItemsWndComp );
            m_Parent->DeleteItem( iter->data );
            ++deletedItems;
            continue;
        }
        iter->data = m_Parent->UpdateItem(iter->data, true);
        if(!iter->data) {
            m_ItemsWindowStack->RemoveItem(nullptr, m_Parent->m_UpdateItemsWndComp);
            ++deletedItems;
        }
    }
    m_ItemsWindow.lowerIndex -= deletedItems;
    if ( m_Parent->m_ConnectionErrorLayout ){
        m_Parent->ShowConnectionError(false);
    }
    if ( deletedHeight > 0 ) {
        Evas_Coord x, y, w, h;
        elm_scroller_region_get( m_Parent->GetScroller(), &x, &y, &w, &h );
        elm_scroller_region_show( m_Parent->GetScroller(), x, y - deletedHeight, w, h );
    }
    if ( !m_Parent->m_DelayedRegEdgesCbTimer ) {
        m_Parent->m_DelayedRegEdgesCbTimer = ecore_timer_add( 1.0, TrackItemsProxy::delayed_registration_of_edged_cb, m_Parent );
    }
    elm_object_scroll_hold_pop( m_Parent->m_Scroller );
}

void ItemsCache::RedrawWindow()
{
    Log::debug( "%s TrackItemsProxy::RedrawWindow upperIndex=%d", TRACK_TAG, m_ItemsWindow.upperIndex);
    Log::debug( "%s TrackItemsProxy::RedrawWindow lowerIndex=%d", TRACK_TAG, m_ItemsWindow.lowerIndex);

    unsigned int size = m_ItemsWindowStack->GetSize();
    // delete all windowed items
    m_Parent->DeleteAll_Prepare();
    while ( m_ItemsWindowStack->GetSize() ) {
        void *downItemToDelete = m_ItemsWindowStack->PopFromEnd();
        assert( downItemToDelete );
        m_Parent->DeleteAll_ItemDelete( downItemToDelete );
    }
    if ( m_Provider && (size != m_ItemsWindowStack->GetSize()) ) {
        // recreate all windowed items
        m_ItemsWindow.lowerIndex = m_ItemsWindow.upperIndex - 1;
        if(m_ItemsWindow.lowerIndex < -1) {
            m_ItemsWindow.lowerIndex = -1;
        }
        m_Provider->AddRef();
        unsigned int dataSize = eina_array_count(m_Provider->GetData());
        while ( (m_ItemsWindowStack->GetSize() != size) && ((m_ItemsWindow.lowerIndex + 1) < dataSize) ) {
            ++m_ItemsWindow.lowerIndex;
            void* itemToCreate = eina_array_data_get( m_Provider->GetData(), m_ItemsWindow.lowerIndex );
            if(itemToCreate) {
                void *item = m_Parent->CreateItem( itemToCreate, true );
                if ( item ) {
                    m_ItemsWindowStack->PushToEnd( item );
                } else {
                    Log::debug( "RedrawWindow: cannot create an item." );
                }
            }
        }
        m_Provider->Release();
    }
}

bool ItemsCache::isEmpty()
{
    return !(m_ItemsWindowStack && m_ItemsWindowStack->GetSize());
}

void ItemsCache::SetIndex( int upperBound, int lowerBound )
{
    Log::debug( "%s : ItemsCache->NormalizeWindowIndex::SetIndex %d %d", TRACK_TAG, upperBound, lowerBound );
    m_ItemsWindow.upperIndex = upperBound;
    m_ItemsWindow.lowerIndex = lowerBound;
}

Utils::DoubleSidedStack* ItemsCache::GetItemsStack()
{
    return m_ItemsWindowStack;
}

void TrackItemsProxy::ValidateScrollerAndPreloadItems() {
    Evas_Coord x,y,w,h,scroller_content_h;
    elm_scroller_region_get( m_Scroller, &x, &y, &w, &h);
    elm_scroller_child_size_get( m_Scroller, nullptr, &scroller_content_h );
    if ( y+h >= scroller_content_h ) {
        Log::debug( "%s : TrackItemsProxy::ValidateScrollerAndPreloadItems-> fix scroller region", TRACK_TAG );
        y= scroller_content_h-(h+1);
        elm_scroller_region_show ( m_Scroller, x, y, w, h);
    }
    if ( !m_Provider->IsReverseChronological() && y >= scroller_content_h - 3*h ) {
        Log::debug( "%s : TrackItemsProxy::ValidateScrollerAndPreloadItems-> preload items", TRACK_TAG );
        ShiftDown(true);
    }
}

bool TrackItemsProxy::IsScrollerRegionOnTop(void) {
    Evas_Coord y;
    Log::debug( "%s : TrackItemsProxy::IsScrollerRegionOnTop m_TopOffset =%d", TRACK_TAG, m_TopOffset);
    elm_scroller_region_get( m_Scroller, nullptr, &y ,nullptr, nullptr);
    return ( y <= TINY_SCROLL_PADDING + m_TopOffset );
}

void TrackItemsProxy::AutomaticallyShowJustAddedItems() {
    Log::debug( "%s : TrackItemsProxy::AutomaticallyShowJustAddedItems", TRACK_TAG);
    if (m_Provider->GetDataCount() == 1) {// for single item
        m_ItemsCache->ShiftDown(true);
    }
    else {
        m_ItemsCache->ShiftUp();
    }
}

void TrackItemsProxy::SetShowSinglePresenter() {
    Log::debug("%s : TrackItemsProxy::SetShowSinglePresenter for %s", TRACK_TAG, m_Identifier);
    if(m_ItemsCache->isEmpty() && m_Provider->GetDataCount() >0 )
    {
        m_ShowSingleItem = true;
    }// this actions are for fisrt in-progress item, which are added onto empty feed
}

bool TrackItemsProxy::DataIsFiltered(void)
{
    return m_Provider->GetDataCount() != m_Provider->AbstractDataProvider::GetDataCount();
}

void TrackItemsProxy::DeleteTopEdgeCB(void)
{
    if ( !m_DelayedRegEdgesCbTimer ) {
        if( mIsBeginEdgeCbAdded )
        {
            evas_object_smart_callback_del( m_Scroller, TrackItemsProxy::EDGE_TOP, TrackItemsProxy::begin_edge_of_scroll_reached );
            mIsBeginEdgeCbAdded = false;
        }
    }
}

void TrackItemsProxy::UnpackAssociatedWithItemRect(Evas_Object *toCompare)
{
    Eina_List *l, *l_next;
    void *element = NULL;

    Eina_List* childs = elm_box_children_get( m_DataArea );

    EINA_LIST_FOREACH_SAFE( childs, l, l_next, element ) {
        Evas_Object *child = static_cast<Evas_Object*>(element);
        if ( child ) {
            const char *type = evas_object_type_get( child );
            if ( strcmp( type, RECTANGLE ) == 0 ) {
                Evas_Object *layout = static_cast<Evas_Object*>(evas_object_data_get( child, FOREGROUND_RECT ));
                if (layout == toCompare) {
                    elm_box_pack_after(m_DataArea, layout, child);
                    elm_box_unpack(m_DataArea, child);
                    evas_object_del(child);
                }
            }
        }
    }
}

void TrackItemsProxy::CreateTopItemsAfterPTR() {
    m_ItemsCache->EraseWindowData();
    ShiftDown();
}
