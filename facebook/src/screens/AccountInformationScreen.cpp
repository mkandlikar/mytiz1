#include "AccountInformationScreen.h"
#include "Common.h"
#include "Config.h"
#include "WidgetFactory.h"

AccountInformationScreen::AccountInformationScreen(): ScreenBase(NULL)
{
    Evas_Object *parent = getParentMainLayout();
    mMoreOptionPopup = NULL;
    mRemoveAccountDialog = NULL;
    CreateAccountScreen(parent);
    mLogOutReq = new LogoutRequest(on_logout_completed, this);
}

AccountInformationScreen::~AccountInformationScreen()
{
    if (mRemoveAccountDialog)
        HidePopUp();
    if (mMoreOptionPopup)
        CloseMorePopup();
    delete mLogOutReq;
}

void AccountInformationScreen::Push() {
    evas_object_hide(getParentMainLayout());
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void AccountInformationScreen::CreateAccountScreen(Evas_Object *parent) {
    mLayout = elm_layout_add(parent);
    elm_layout_file_set(mLayout, Application::mEdjPath, "account_info_page");
    evas_object_size_hint_weight_set(mLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);

    elm_object_translatable_part_text_set(mLayout, "title", "IDS_FACEBOOK");
    elm_object_signal_callback_add(mLayout, "mouse,clicked,*", "back_btn", back_btn_cb, this);
    elm_object_signal_callback_add(mLayout, "mouse,clicked,*", "more_menu_icon*", more_btn_cb, this);

    elm_object_part_text_set(mLayout, "item_text_line_1", Config::GetInstance().GetUserName().c_str());
    elm_object_translatable_part_text_set(mLayout, "item_text_line_2", "IDS_FACEBOOK");
    evas_object_show(mLayout);

}

void AccountInformationScreen::back_btn_cb(void *data, Evas_Object *obj, const char *emission, const char *source) {
    AccountInformationScreen *self = static_cast<AccountInformationScreen*>(data);
    if (!self) {
        return;
    }
    self->CloseScreenWindow();
}

void AccountInformationScreen::more_btn_cb(void *data, Evas_Object *obj, const char *emission, const char *source) {
    Log::debug(LOG_FACEBOOK, " ==== more_btn_cb ====");
    AccountInformationScreen *me = static_cast<AccountInformationScreen*>(data);
    if (!me) {
        return;
    }
    me->mMoreOptionPopup = elm_ctxpopup_add(me->getMainLayout());
    elm_ctxpopup_hover_parent_set(me->mMoreOptionPopup, obj);
    evas_object_size_hint_weight_set(me->mMoreOptionPopup, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(me->mMoreOptionPopup, EVAS_HINT_FILL, 0);
    eext_object_event_callback_add(me->mMoreOptionPopup, EEXT_CALLBACK_BACK, eext_ctxpopup_back_cb, NULL);
    evas_object_smart_callback_add(me->mMoreOptionPopup, "dismissed", ctxpopup_dismissed_cb, me);
#ifdef FEATURE_SYNC_FB_CONTACTS
    elm_ctxpopup_item_append(me->mMoreOptionPopup, i18n_get_text("IDS_SYNC_NOW_AIS"), NULL, sync_now_cb, me);
#endif
    elm_ctxpopup_item_append(me->mMoreOptionPopup, i18n_get_text("IDS_REMOVE_ACCOUNT_AIS"), NULL, show_popup_cb, me);

    if (Application::GetInstance()->IsRTLLanguage()) {
        evas_object_move(me->mMoreOptionPopup, R->SCREEN_SIZE_WIDTH - R->AC_REMOVE_POPUP_MOVE_X, R->AC_REMOVE_POPUP_MOVE_Y);
    } else {
        evas_object_move(me->mMoreOptionPopup, R->AC_REMOVE_POPUP_MOVE_X, R->AC_REMOVE_POPUP_MOVE_Y);
    }
    evas_object_show(me->mMoreOptionPopup);
}

void AccountInformationScreen::ctxpopup_dismissed_cb(void *data, Evas_Object *obj, void *event_info) {
    AccountInformationScreen *me = static_cast<AccountInformationScreen*>(data);
    if (!me) {
        return;
    }
    if (me->mMoreOptionPopup) {
        evas_object_del(me->mMoreOptionPopup);
        me->mMoreOptionPopup = NULL;
    }
}

#ifdef FEATURE_SYNC_FB_CONTACTS
void AccountInformationScreen::sync_now_cb(void *data, Evas_Object *obj, void *event_info) {
    AccountInformationScreen *me = static_cast<AccountInformationScreen*>(data);
    if (!me) {
        return;
    }
    me->CloseMorePopup();

    int ret = account_query_account_by_package_name(account_contact_sync_cb, PACKAGE, me);
    if (ret != ACCOUNT_ERROR_NONE)
        Log::error(LOG_FACEBOOK, "sync_now_cb() Failed");
}

bool AccountInformationScreen::account_contact_sync_cb(account_h account, void* data)
{
    int account_id = 0;
    int ret = account_get_account_id(account, &account_id);
    Log::debug(LOG_FACEBOOK, "account_contact_sync_cb :: ACCOUNT ID = %d", account_id);
    if (ret == ACCOUNT_ERROR_NONE) {
        AppEvents::Get().Notify(eACCOUNT_CHANGED, &account_id);
    }
    return false;
}
#endif

bool AccountInformationScreen::account_remove_cb(account_h account, void* data) {
    AccountInformationScreen *me = static_cast<AccountInformationScreen*>(data);
    if (!me) {
        return true;
    }
    if (Application::GetInstance()->mDataService) {
        Application::GetInstance()->mDataService->setLoginState(false);
    }
    me->mLogOutReq->DoLogout();
    Application::GetInstance()->GoToBackground();
    Application::GetInstance()->RemoveAllScreensAndNfItemsFromNaviframe();
    LogoutRequest::RedirectToLogin(nullptr);
    return false;
}

void AccountInformationScreen::show_popup_cb(void *data, Evas_Object *obj, void *event_info) {
    AccountInformationScreen *screen = static_cast<AccountInformationScreen*>(data);
    if (!screen) {
        return;
    }
    screen->CloseMorePopup();
    screen->mRemoveAccountDialog = ConfirmationDialog::CreateAndShow(screen->mLayout, "IDS_AC_INFO_TITLE", "IDS_AC_INFO_MSG", "IDS_REMOVE_ACCOUNT",
            "IDS_CAPS_CANCEL", confirmation_dialog_cb, screen);

}

bool AccountInformationScreen::HidePopUp() {
    if (mRemoveAccountDialog) {
        delete mRemoveAccountDialog;
        mRemoveAccountDialog = nullptr;
        return true;
    } else
        return false;
}

bool AccountInformationScreen::HandleBackButton()
{
    if (CloseMorePopup()) {
    } else if (HidePopUp()) {
    } else {
        CloseScreenWindow();
    }
    return true;
}

bool AccountInformationScreen::CloseMorePopup()
{
    if (mMoreOptionPopup) {
        elm_ctxpopup_clear(mMoreOptionPopup);
        evas_object_del(mMoreOptionPopup);
        mMoreOptionPopup = NULL;
        return true;
    } else
        return false;
}

void AccountInformationScreen::CloseScreenWindow()
{
    Application::GetInstance()->GoToBackground();
    Pop();
}

void AccountInformationScreen::on_logout_completed(void* object, char* respond, int code)
{
    if (Application::GetInstance()->mDataService) {
        Application::GetInstance()->mDataService->clearDownloadRequest();
    }
    AppEvents::Get().Notify(eACCOUNT_CHANGED, nullptr);
}

void AccountInformationScreen::confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event)
{
    AccountInformationScreen *screen = static_cast<AccountInformationScreen *>(user_data);
    if (!screen) {
        return;
    }
    switch (event) {
    case ConfirmationDialog::ECDYesPressed: {
        int ret = account_query_account_by_package_name(account_remove_cb, PACKAGE, screen);
        if (ret != ACCOUNT_ERROR_NONE) {
            Log::error(LOG_FACEBOOK, "account_query_account_by_package_name() Failed = %d ", ret);
        }
    }
        break;
    default:
        screen->HidePopUp();
        break;
    }
}
