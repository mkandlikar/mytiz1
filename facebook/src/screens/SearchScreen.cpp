#include "ActionBase.h"
#include "Common.h"
#include "FbRespondGetSearchResults.h"
#include "FoundItem.h"
#include "GroupProfilePage.h"
#include "OwnProfileScreen.h"
#include "ProfileScreen.h"
#include "SearchProvider.h"
#include "SearchScreen.h"
#include "SearchScreenTabbed.h"
#include "Utils.h"
#include "WebViewScreen.h"
#include "WidgetFactory.h"

SearchScreen::SearchScreen(const char * searchFieldString ) :
    ScreenBase(nullptr),
    TrackItemsProxyAdapter(nullptr, SearchProvider::GetInstance()),
    mSearchField(nullptr)
{
    mScreenId = ScreenBase::SID_SEARCH;
    mSearchString = nullptr;
    mLayout = elm_layout_add(getParentMainLayout());
    evas_object_size_hint_weight_set(mLayout, 1, 1);
    elm_layout_file_set(mLayout, Application::mEdjPath, "search_screen");

    CreateSearchField(mLayout);

    ApplyScroller(mLayout, true);
    elm_object_part_content_set(mLayout, "list", GetScroller());

    if (searchFieldString)
    {
        SetSearchFieldString(searchFieldString);
    }

    elm_object_signal_callback_add(mLayout, "back.clicked", "btn", searchfield_back_button_clicked_cb, this);
}

SearchScreen::~SearchScreen()
{
    free(mSearchString);
    SearchProvider::GetInstance()->EraseData();
}

/**
 * @brief Overloaded Push metod
 */
void SearchScreen::Push()
{
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void SearchScreen::OnPause()
{
    elm_entry_input_panel_hide(mSearchField);
}

void SearchScreen::SetSearchFieldString(const char * searchFieldString)
{
    elm_entry_entry_set(mSearchField, searchFieldString);
    elm_entry_cursor_end_set(mSearchField);
}

void SearchScreen::CreateSearchField(Evas_Object *parent)
{
    /* Search field */
    mSearchField = elm_entry_add(parent);
    if(mSearchField) {
        elm_entry_input_panel_layout_set(mSearchField, ELM_INPUT_PANEL_LAYOUT_NORMAL);
        elm_entry_input_panel_return_key_type_set(mSearchField, ELM_INPUT_PANEL_RETURN_KEY_TYPE_SEARCH);
        elm_entry_input_panel_return_key_autoenabled_set(mSearchField, EINA_TRUE);
        elm_entry_prediction_allow_set(mSearchField, EINA_FALSE);
        elm_entry_single_line_set(mSearchField, EINA_TRUE);
        elm_entry_scrollable_set(mSearchField, EINA_TRUE);
        elm_object_translatable_part_text_set(mSearchField, "elm.guide", "IDS_SEARCH");

        char *buf = WidgetFactory::WrapByFormat(GLOBAL_SEARCH_GUIDE_FORMAT, i18n_get_text("IDS_SEARCH"));
        if(buf) {
            elm_object_part_text_set(mSearchField, "elm.guide", buf);
            delete[] buf;
            buf = NULL;
        }

        elm_entry_text_style_user_push(mSearchField, GLOBAL_SEARCH_STYLE);
        elm_entry_cursor_end_set(mSearchField);
        elm_entry_cnp_mode_set(mSearchField, ELM_CNP_MODE_PLAINTEXT);
        evas_object_show(mSearchField);
        elm_object_part_content_set(parent, "search_input_text", mSearchField);

        evas_object_smart_callback_add(mSearchField, "changed", searchfield_changed_cb, this);
        evas_object_smart_callback_add(mSearchField, "activated", searchfield_activated_cb, this);
        evas_object_smart_callback_add(mSearchField, "preedit,changed", searchfield_changed_cb, this);
        elm_object_signal_callback_add(parent, "clear.text", "btn", searchfield_clear_button_clicked_cb, this);
    }
}

/**
 * @brief Change search field handler: show/hide clear button in according to value in entry
 * @param[in] data - pointer to MainScreen object
 * @param[in] obj - pointer to entry object
 * @param[in] event_info - pointer to event info (depends on the object type )
 */
void SearchScreen::searchfield_changed_cb(void *data, Evas_Object *obj, void *event_info)
{
    SearchScreen *screen = static_cast<SearchScreen *>(data);
    if(screen) {
        Evas_Object *layout = screen->mLayout;
        if (!elm_entry_is_empty(obj)) {
            elm_object_signal_emit(layout, "show_clear_button", "");
        } else {
            elm_object_signal_emit(layout, "hide_clear_button", "");
        }

        const char * str = elm_entry_entry_get(obj);
        if(str) {
            char *strUTF8 = elm_entry_markup_to_utf8(str);
            if (strUTF8 &&
               (!screen->mSearchString || strcmp(strUTF8, screen->mSearchString)))
            {
                free ((void*) screen->mSearchString);
                screen->mSearchString = strUTF8;

                Log::debug("SearchScreen::searchfield_changed_cb = %s", screen->mSearchString);
                screen->EraseWindowData();
                SearchProvider::GetInstance()->EraseData();
                SearchProvider::GetInstance()->SetSearchString(screen->mSearchString);
                screen->RequestData();
            }
        }
    }
}

void SearchScreen::searchfield_activated_cb(void *data, Evas_Object *obj, void *event_info)
{
    Log::debug("SearchScreen::searchfield_activated_cb");
    const char * str = elm_entry_entry_get(obj);
    if(str) {
        SearchScreen *screen = static_cast<SearchScreen *>(data);
        char *strUTF8 = elm_entry_markup_to_utf8(str);
        SearchScreenTabbed* searchScreenTabbed = new SearchScreenTabbed(strUTF8, screen);
        Application::GetInstance()->AddScreen(searchScreenTabbed);
        free(strUTF8);
    }
}


/**
 * @brief Clear field button handler for "click" event - clear search field
 * @param[in] data - pointer to MainScreen object
 * @param[in] obj - pointer to button object
 * @param[in] event_info - pointer to event info (depends on the object type )
 */
void SearchScreen::searchfield_clear_button_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    SearchScreen *me = static_cast<SearchScreen *> (data);
    elm_entry_entry_set(me->mSearchField, "");
}

/**
 * @brief Back button handler for "click" event
 * @param[in] data - pointer to MainScreen object
 * @param[in] obj - pointer to button object
 * @param[in] event_info - pointer to event info (depends on the object type )
 */
void SearchScreen::searchfield_back_button_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source) {
    SearchScreen *screen = static_cast<SearchScreen *>(data);
    if(Application::GetInstance()->GetTopScreen() == data) {
        elm_object_signal_callback_del(screen->mLayout, "back.clicked", "btn" , searchfield_back_button_clicked_cb);
        if (screen->mSearchField) {
            evas_object_smart_callback_del(screen->mSearchField, "changed", searchfield_changed_cb);
            evas_object_smart_callback_del(screen->mSearchField, "preedit,changed", searchfield_changed_cb);
            evas_object_smart_callback_del(screen->mSearchField, "activated", searchfield_activated_cb);
            elm_object_signal_callback_del(screen->mLayout, "clear.text", "btn", searchfield_clear_button_clicked_cb);
        }
        screen->Pop();
    } else {
        Log::error("SearchScreen::searchfield_back_button_clicked_cb->wrong top screen");
    }
}

bool SearchScreen::HandleBackButton() {
    return false;
}

void* SearchScreen::CreateItem(void *dataForItem, bool isAddToEnd)
{
    FoundItem *foundItem = static_cast<FoundItem*>(dataForItem);
    ActionAndData *data = nullptr;

    if (foundItem) {
        data = new ActionAndData(foundItem, nullptr);
        data->mParentWidget = ItemGet(data, GetDataArea(), isAddToEnd);

        if (!data->mParentWidget) {
            delete data;
            data = nullptr;
        }
    }
    return data;
}

Evas_Object *SearchScreen::ItemGet(void *user_data, Evas_Object *obj, bool isAddToEnd)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    Evas_Object *content = WidgetFactory::CreateSimpleWrapper(obj);
    CreateUserItem(content, action_data);

    if (isAddToEnd) {
        elm_box_pack_end(obj, content);
    } else {
        elm_box_pack_start(obj, content);
    }
    evas_object_show(content);

    return content;
}

void SearchScreen::CreateUserItem(Evas_Object* parent, ActionAndData *action_data)
{
    Evas_Object *list_item = elm_layout_add(parent);
    evas_object_size_hint_weight_set(list_item, 1, 1);
    evas_object_size_hint_weight_set(list_item, -1, 0);
    elm_layout_file_set(list_item, Application::mEdjPath, "search_screen_list_item");
    elm_box_pack_end(parent, list_item);
    evas_object_show(list_item);

    if (action_data)
    {
        FoundItem *item = static_cast<FoundItem*>(action_data->mData);

        if (item->mCategory && (strcmp(item->mCategory, "") != 0))
        {
            elm_object_part_text_set(list_item, "box_line_1", item->mText);
            elm_object_part_text_set(list_item, "box_line_2", item->mCategory);
        }
        else
        {
            elm_object_part_text_set(list_item, "box_line_single", item->mText);
        }

        Evas_Object *avatar = elm_image_add(list_item);
        elm_object_part_content_set(list_item, "box_avatar", avatar);
        action_data->UpdateImageLayoutAsync(item->mPhoto, avatar);
        evas_object_show(avatar);

        Log::debug("SearchScreen::CreateUserItem->TYPE IS: %s", item->mType);

        elm_object_signal_callback_add(list_item, "item_clicked", "box", on_item_clicked_cb, action_data);
    }
}

void SearchScreen::on_item_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(data);

    if (ActionAndData::IsAlive(action_data) && action_data->mData) {
        FoundItem *item = static_cast<FoundItem*>(action_data->mData);
        assert(item->mType);
        if (!strcmp(item->mType, "group")) {
            if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_GROUP_PROFILE_PAGE) {
                ScreenBase *group_scr = new GroupProfilePage(item->GetId(), item->mText);
                Application::GetInstance()->AddScreen(group_scr);
            }
        } else if (!strcmp(item->mType, "user")) {
            if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_USER_PROFILE) {
                assert(item->GetId());
                Utils::OpenProfileScreen(item->GetId());
            }
        } else if (!strcmp(item->mType, "page")) {
            if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_WEB_VIEW) {
                WebViewScreen::Launch(item->mPath, true);
            }
        }
    } else {
        Log::error("SearchScreen::on_item_clicked_cb->ActionAndData is not valid");
    }
}

void SearchScreen::ShowInputPanel()
{
    elm_entry_input_panel_show(mSearchField);
}
