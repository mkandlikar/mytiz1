#include "Common.h"
#include "Config.h"
#include "FacebookSession.h"
#include "jsonutilities.h"
#include "Log.h"
#include "Messenger.h"
#include "WebViewScreen.h"

#include <app_manager.h>
#include <string>


#define ICONS_PATH "images/GetMessenger/"
#define MESSENGER_WEB_URI "http://www.messenger.com/features"
#define MESSENGER_INSTALL_LAYOUT "MessengerInstallLayout"

Messenger::Messenger(ScreenBase *parentScreen) : ScreenBase(parentScreen),
    mIsMessengerInstalled(false) {

    mIsMessengerInstalled = IsMessengerInstalled();
    CreateView();
}

Messenger::~Messenger() {
    if (mGraphRequest)
        FacebookSession::GetInstance()->ReleaseGraphRequest(mGraphRequest);
}

void Messenger::Push() {
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

bool Messenger::IsMessengerInstalled() {
    app_info_h MessengerAppHandle = NULL;
    int AppInfoReturnVal = app_manager_get_app_info(MESSENGER_APPLICATION_ID, &MessengerAppHandle);

    if (MessengerAppHandle)
        app_info_destroy(MessengerAppHandle);

    return (APP_MANAGER_ERROR_NONE == AppInfoReturnVal);
}

void Messenger::CreateView() {
    mLayout = elm_layout_add(getParentMainLayout());
    evas_object_size_hint_weight_set(mLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_show(mLayout);

    mGraphRequest = FacebookSession::GetInstance()->GetUserProfileName(GraphRequestCb, this);

    char *resourcePath = app_get_resource_path();
    if (!resourcePath) {
        mIconPath = resourcePath;
        mIconPath.append(ICONS_PATH);
        free(resourcePath);
    }
    InstallMessenger();
}

void Messenger::InstallMessenger() {
    elm_layout_file_set(mLayout, Application::mEdjPath, MESSENGER_INSTALL_LAYOUT);
    elm_object_translatable_part_text_set(mLayout, "Title", "IDS_MESSENGER_TITLE");
    elm_object_translatable_part_text_set(mLayout, "Heading", "IDS_MESSENGER_HEADING");
    elm_object_translatable_part_text_set(mLayout, "Info", "IDS_MESSENGER_INFO");
    mIconPath.append("messanger_icon.png");

    Evas_Object* EvasObj = elm_image_add(mLayout);
    elm_image_file_set(EvasObj, mIconPath.c_str(), NULL);
    elm_layout_content_set(mLayout, "Icon", EvasObj);
    evas_object_show(EvasObj);

    EvasObj = elm_button_add(mLayout);
    elm_object_part_content_set(mLayout, "LearnMoreBtn", EvasObj);
    elm_object_translatable_text_set(EvasObj, "IDS_LEARN_MORE");
    evas_object_smart_callback_add(EvasObj, "clicked", LearnMoreCb, this);
    evas_object_show(EvasObj);

    EvasObj = elm_button_add(mLayout);
    elm_object_part_content_set(mLayout, "GetAppBtn", EvasObj);
    elm_object_translatable_text_set(EvasObj, "IDS_MESSENGER_INS_TITLE");
    evas_object_smart_callback_add(EvasObj, "clicked", GetAppCb, this);
    evas_object_show(EvasObj);

}

void Messenger::LearnMoreCb(void* Data, Evas_Object* Obj, void* EventInfo) {
    WebViewScreen::Launch(MESSENGER_WEB_URI);
}

void Messenger::GetAppCb(void* Data, Evas_Object* Obj, void* EventInfo) {
    app_control_h AppServiceHandle = NULL;
    app_control_create(&AppServiceHandle);
    if (AppServiceHandle) {
        app_control_set_app_id(AppServiceHandle, "org.tizen.tizenstore");
        std::string AppStoreUri = "tizenstore://ProductDetail/";
        AppStoreUri.append(MESSENGER_APPLICATION_ID);
        app_control_set_uri(AppServiceHandle, AppStoreUri.c_str());
        app_control_set_operation(AppServiceHandle, APP_CONTROL_OPERATION_VIEW);
        app_control_send_launch_request(AppServiceHandle, NULL, NULL);
    }
}

void Messenger::LaunchMessenger() {
    app_control_h AppServiceHandle = NULL;
    app_control_create(&AppServiceHandle);
    if (AppServiceHandle) {
        app_control_set_app_id(AppServiceHandle, MESSENGER_APPLICATION_ID);
        app_control_set_operation(AppServiceHandle, APP_CONTROL_OPERATION_VIEW);
        app_control_send_launch_request(AppServiceHandle, NULL, NULL);
    }
}

void Messenger::GraphRequestCb(void* Object, char* Respond, int CurlCode) {
    if (CURLE_OK == CurlCode) {
        Messenger* Me = static_cast<Messenger*>(Object);
        JsonObject* JsonRoot = NULL;
        JsonParser* JsonParser = openJsonParser(Respond, &JsonRoot);
        const char* ProfileName = (JsonRoot ? json_object_get_string_member(JsonRoot, "name") : "NULL");
        if(ProfileName) {
            std::string MessengerHeading = ProfileName;
            MessengerHeading.append(", ");
            MessengerHeading.append(i18n_get_text("IDS_MESSENGER_HEADING"));
            elm_object_part_text_set(Me->mLayout, "Heading", MessengerHeading.c_str());
        }
        g_object_unref(JsonParser);
    }
}

