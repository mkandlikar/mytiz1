/*
 *  SignUpConfirmScreen.cpp
 *
 *  Created on: 22nd June 2015
 *      Author: Manjunath Raja
 */


#include <regex.h>

#include "Common.h"
#include "CommonScreen.h"
#include "Config.h"
#include "dlog.h"
#include "SignUpConfirmScreen.h"
#include "SignupRequest.h"
#include "SignUpEmailScreen.h"


SignUpConfirmScreen::SignUpConfirmScreen(ScreenBase *parentScreen):ScreenBase(parentScreen)  {
    Evas_Object *parent = getParentMainLayout();
    CreateView(parent);
}

SignUpConfirmScreen::~SignUpConfirmScreen() {
    //TODO
}

Evas_Object *SignUpConfirmScreen::LoadEdcLayout(Evas_Object *parent) {
    Evas_Object *layout;

    layout = elm_layout_add(parent);
    elm_layout_file_set(layout, Application::mEdjPath, "signup_confirm_screen");
    evas_object_size_hint_weight_set(layout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(layout, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_show(layout);

    return layout;
}

void SignUpConfirmScreen::CreateView(Evas_Object *parent) {
    Evas_Object *TmpEvasObj;


    mLayout = LoadEdcLayout(parent);

    TmpEvasObj = elm_button_add(mLayout);
    elm_object_part_content_set(mLayout, "signup_confirm_continue", TmpEvasObj);
    elm_object_style_set(TmpEvasObj, "anchor");
    elm_object_translatable_text_set(TmpEvasObj, "IDS_CONFIRM");
    evas_object_smart_callback_add(TmpEvasObj, "clicked", ContinueBtnCb, this);

    TmpEvasObj = elm_button_add(mLayout);
    elm_object_part_content_set(mLayout, "signup_confirm_send_again", TmpEvasObj);
    elm_object_style_set(TmpEvasObj, "anchor");
    elm_object_translatable_text_set(TmpEvasObj, "IDS_SEND_CODE_AGAIN");
    evas_object_smart_callback_add(TmpEvasObj, "clicked", SendCodeAgainBtnCb, this);

    TmpEvasObj = elm_button_add(mLayout);
    elm_object_part_content_set(mLayout, "signup_confirm_change_email", TmpEvasObj);
    elm_object_style_set(TmpEvasObj, "anchor");
    elm_object_translatable_text_set(TmpEvasObj, "IDS_CHANGE_EMAIL_ADDRESS");
    evas_object_smart_callback_add(TmpEvasObj, "clicked", ChangeEmailBtnCb, this);

    TmpEvasObj = elm_button_add(mLayout);
    elm_object_part_content_set(mLayout, "signup_confirm_by_phone", TmpEvasObj);
    elm_object_style_set(TmpEvasObj, "anchor");
    elm_object_translatable_text_set(TmpEvasObj, "IDS_CONFIRM_BY_MOBILE_NO");
    evas_object_smart_callback_add(TmpEvasObj, "clicked", ConfirmByPhoneBtnCb, this);

    SetScreenInfo(this, false);
    CreateCodeInput(this);
}

Evas_Object *SignUpConfirmScreen::CreateInput(Evas_Object *parent, Evas_Object *Input,
        Evas_Object *entry, Elm_Input_Panel_Layout  EntryType)
{
    elm_entry_input_panel_layout_set(entry, EntryType);
    elm_layout_theme_set(Input, "layout", "editfield", "singleline");
    evas_object_size_hint_align_set(Input, EVAS_HINT_FILL, 0.0);
    evas_object_size_hint_weight_set(Input, EVAS_HINT_EXPAND, 0.0);
    elm_entry_single_line_set(entry, EINA_TRUE);
    elm_entry_scrollable_set(entry, EINA_TRUE);
    evas_object_smart_callback_add(entry, "focused", InputFocusedCb, Input);
    evas_object_smart_callback_add(entry, "unfocused", InputUnFocusedCb, Input);
    evas_object_smart_callback_add(entry, "changed", InputChangedCb, Input);
    evas_object_smart_callback_add(entry, "preedit,changed", InputChangedCb, Input);
    elm_object_part_content_set(Input, "elm.swallow.content", entry);

    Evas_Object *button = elm_button_add(Input);
    elm_object_style_set(button, "editfield_clear");
    evas_object_smart_callback_add(button, "clicked", InputClearButtonCb, entry);
    elm_object_part_content_set(Input, "elm.swallow.button", button);

    return Input;
}

void SignUpConfirmScreen::CreateCodeInput(SignUpConfirmScreen *data) {
    Evas_Object *mobileInputLayout = elm_layout_add(data->getParentMainLayout());
    data->mCodeInput = elm_entry_add(mobileInputLayout);
    data->mCodeLayout = CreateInput(data->mLayout, mobileInputLayout,
            data->mCodeInput, ELM_INPUT_PANEL_LAYOUT_PHONENUMBER);
    elm_object_translatable_part_text_set(data->mCodeInput, "elm.guide", "IDS_SUP_CONFIRM_CODE_ENTER");
    elm_object_part_content_set(data->mLayout, "signup_confirm_code_input", data->mCodeLayout);
    elm_entry_prediction_allow_set(data->mCodeInput, EINA_FALSE);
}

void SignUpConfirmScreen::SetScreenInfo(SignUpConfirmScreen *data, bool error) {
    std::string EmailInfo = i18n_get_text("IDS_SUP_CONFIRM_EMAIL_INFO");
    std::string SignedUpEmail = SignupRequest::Singleton().GetData(SignupRequest::EMAILADDRESS);
    std::string ConfirmInfo;
    if (!error) {
        elm_object_part_text_set(data->mLayout, "signup_confirm_error", " ");
        elm_object_part_text_set(data->mLayout, "signup_confirm_heading", " ");
        if (!SignedUpEmail.empty())
            ConfirmInfo = EmailInfo + " " + SignedUpEmail;
        elm_object_part_text_set(data->mLayout, "signup_confirm_info", ConfirmInfo.c_str());
    } else {
       //TODO
    }
}

void SignUpConfirmScreen::ContinueBtnCb(void *data, Evas_Object *obj, void *EventInfo) {
    SignUpConfirmScreen *self = (SignUpConfirmScreen*)data;
    if (CommonScreen::IsValidNumber(elm_entry_entry_get(self->mCodeInput))) {
        SignupRequest::Singleton().SetData(SignupRequest::CONFIRMCODE, elm_entry_entry_get(self->mCodeInput));
        CommonScreen::StartProgressScreen();
        SignupRequest::Singleton().InitAsync(SignupRequest::CONFIRM);
    } else {
        SetScreenInfo(self, true);
    }
}

void SignUpConfirmScreen::SendCodeAgainBtnCb(void *data, Evas_Object *obj, void *EventInfo) {
    CommonScreen::StartProgressScreen();
    SignupRequest::Singleton().InitAsync(SignupRequest::SENDCODE);
}

void SignUpConfirmScreen::ChangeEmailBtnCb(void *data, Evas_Object *obj, void *EventInfo) {
    ScreenBase *NewScreen = new SignUpEmailScreen(NULL, true, false);
    Application::GetInstance()->AddScreen(NewScreen);
}

void SignUpConfirmScreen::ConfirmByPhoneBtnCb(void *data, Evas_Object *obj, void *EventInfo) {
    ScreenBase *NewScreen = new SignUpEmailScreen(NULL, false, true);
    Application::GetInstance()->AddScreen(NewScreen);
}

void SignUpConfirmScreen::InputClearButtonCb(void *data, Evas_Object *obj, void *EventInfo){
    Evas_Object *entry = (Evas_Object *)data;
    elm_entry_entry_set(entry, "");
}

void SignUpConfirmScreen::InputChangedCb(void *data, Evas_Object *obj, void *EventInfo){
    Evas_Object *Input = (Evas_Object *) data;
    if (!elm_entry_is_empty(obj) && elm_object_focus_get(obj))
        elm_object_signal_emit(Input, "elm,action,show,button", "");
    else
        elm_object_signal_emit(Input, "elm,action,hide,button", "");
}

void SignUpConfirmScreen::InputFocusedCb(void *data, Evas_Object *obj, void *EventInfo){
    Evas_Object *Input = (Evas_Object *) data;
    if (!elm_entry_is_empty(obj)){
        elm_object_signal_emit(Input, "elm,action,show,button", "");
    }
}

void SignUpConfirmScreen::InputUnFocusedCb(void *data, Evas_Object *obj, void *EventInfo){
    Evas_Object *Input = (Evas_Object *) data;
    elm_object_signal_emit(Input, "elm,action,hide,button", "");
}


