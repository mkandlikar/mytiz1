#include "CameraRollScreen.h"
#include "CommentsAction.h"
#include "CommentsProvider.h"
#include "CommentWidgetFactory.h"
#include "ConnectivityManager.h"
#include "FriendTagging.h"
#include "LikeOperation.h"
#include "Log.h"
#include "OperationManager.h"
#include "OperationPresenter.h"
#include "Popup.h"
#include "PostScreen.h"
#include "PostScreenAction.h"
#include "SelectedMediaList.h"
#include "SimpleCommentOperation.h"
#include "SoundNotificationPlayer.h"
#include "WhoLikedScreen.h"
#include "WidgetFactory.h"

#include <string.h>


const unsigned int PostScreen::DEFAULT_ITEMS_WND_SIZE = 50;
const unsigned int PostScreen::DEFAULT_INIT_ITEMS_SIZE = 40;
const unsigned int PostScreen::DEFAULT_ITEMS_STEP_SIZE = 20;

PostScreen::PostScreen(ActionAndData *parentActionAndData, const char *postId, GraphObject *object) :
    ScreenBase(nullptr),
    IUpdateMediaData(),
    TrackItemsProxyCommentsAdapter(nullptr, mCommentsProvider = new CommentsProvider()),
    mCommentsAction(nullptr),
    mIsReadyForPost(false),
    mTagging(nullptr),
    mPostScreenAction(nullptr),
    mInActionNData(nullptr),
    mSharedSourceNData(nullptr),
    mSelectedActionNData(nullptr),
    mCommentEntry(nullptr),
    mHeaderWidget(nullptr),
    mCommentEntryLayout(nullptr),
    mContainer(nullptr),
    mAttachedPhoto(nullptr),
    mUnattachBtn(nullptr),
    mMediaDataPath(nullptr),
    mMediaDataThumbnailPath(nullptr),
    mPostText(nullptr),
    mObjectDownloader(nullptr),
    mPostInPostDownloader(nullptr),
    mPostInPostWrapper(nullptr)
{
    mCommentsAction = new CommentsAction(this);
    mScreenId = ScreenBase::SID_POST;

    mLayout = WidgetFactory::CreateBaseScreenLayout(this, getParentMainLayout(), "", true, true, false, false);

    ApplyProgressBar(false);
    SetShowConnectionErrorEnabled(false);
    EnableBigConnectionErrorWidget(false);

    SetScrollerUi();

    TrackItemsProxy::ProxySettings *settings = GetProxySettings();
    settings->wndSize = DEFAULT_ITEMS_WND_SIZE;
    settings->initialWndSize = DEFAULT_INIT_ITEMS_SIZE;
    settings->wndStep = DEFAULT_ITEMS_STEP_SIZE;
    SetProxySettings(settings);

    SetIdentifier("POST_SCREEN");

    if (parentActionAndData) {
        Post *post = static_cast<Post*>(parentActionAndData->mData);
        if (post) {
            mCommentsAction->SetParentId(post->GetId());
            mPostScreenAction = new PostScreenAction(this);
//AAA ToDo: The commented code below seems to be not used, but let's keep it until B2787 is fixed.
//            if (post->GetParentId()) {
//                mPost = post->GetParent();
//                mInActionNData = new ActionAndData(mPost, mPostScreenAction);
//            } else {
                mInActionNData = new ActionAndData(post, mPostScreenAction);
//            }
            SetData();
        }
    } else if (postId) {
        Log::info("PostScreen::PostScreen->mId = %s", postId);
        mCommentsAction->SetParentId(postId);
        mObjectDownloader = new GraphObjectDownloader(postId, this);
        mObjectDownloader->StartDownloading();
    } else if (object) {
        if(object->GetGraphObjectType() == GraphObject::GOT_POST) {
            mCommentsAction->SetParentId(object->GetId());
        }
        mPostScreenAction = new PostScreenAction(this);
        mInActionNData = new ActionAndData(object, mPostScreenAction);
        SetData();
    } else {
        Log::info("PostScreen::PostScreen->NoDataAvailable");
        ShowErrorWidget(eNO_DATA_AVAILABLE);
    }
    AppEvents::Get().Subscribe(eREDRAW_EVENT, this);
    AppEvents::Get().Subscribe(eLIKE_COMMENT_EVENT, this);
    AppEvents::Get().Subscribe(ePOST_MESSAGE_EVENT, this);
    AppEvents::Get().Subscribe(ePOST_UPDATE_ABORTED, this);
    AppEvents::Get().Subscribe(ePOST_RELOAD_EVENT, this);
    AppEvents::Get().Subscribe(eINTERNET_CONNECTION_CHANGED, this);
    AppEvents::Get().Subscribe(eUPDATE_POST_LIKES_SUMMARY, this);
    AppEvents::Get().Subscribe(eREPLIES_LIST_WAS_MODIFIED, this);
    AppEvents::Get().Subscribe(eCOMMENT_WAS_DELETED, this);

    OperationManager::GetInstance()->Subscribe(this);
}

PostScreen::~PostScreen()
{
    OperationManager::GetInstance()->UnSubscribe(this);

    mCommentsProvider->Release();

    if (mCommentEntry) {
        elm_object_focus_set(mCommentEntry, EINA_FALSE);
        elm_entry_input_panel_enabled_set(mCommentEntry, EINA_FALSE);
        elm_entry_input_panel_hide(mCommentEntry);
    }

    delete mCommentsAction;

    TrackItemsProxy::EraseWindowData();

    delete mInActionNData;
    delete mSharedSourceNData;
    delete mPostScreenAction;

    free(mMediaDataPath);
    free(mMediaDataThumbnailPath);

    CommentWidgetFactory::remove_scroller_events(GetScroller());

    delete mTagging;

    delete mObjectDownloader;
    delete mPostInPostDownloader;
}

void PostScreen::GraphObjectDownloaderProgress(IGraphObjectDownloaderObserver::Result result) {
    switch (result) {
    case IGraphObjectDownloaderObserver::ENetworkErrorToBeResumed:
        ProgressBarHide();
        if(!mPostInPostDownloader) {
            ShowErrorWidget(eCONNECTION_ERROR);
        }
        break;
    case IGraphObjectDownloaderObserver::EDownloadingStarted:
        HideErrorWidget();
        if(!mPostInPostDownloader) {
            ProgressBarShow();
        }
        break;
    case IGraphObjectDownloaderObserver::EPostDownloaded:
    case IGraphObjectDownloaderObserver::EPhotoDownloaded:
        if(mObjectDownloader) {
            mPostScreenAction = new PostScreenAction(this);
            mInActionNData = new ActionAndData(mObjectDownloader->GetGraphObject(), mPostScreenAction);
            ProgressBarHide();
            SetData();
            delete mObjectDownloader;
            mObjectDownloader = nullptr;
        } else if(mPostInPostDownloader) {
            Post* post = static_cast<Post*>(mPostInPostDownloader->GetGraphObject());
            mSharedSourceNData = new ActionAndData(post, mPostScreenAction);
            WidgetFactory::CreatePostInPostItem(mHeaderWidget, mSharedSourceNData, mPostInPostWrapper);
            post->SetIsSharedPost(true);
            delete mPostInPostDownloader;
            mPostInPostDownloader = nullptr;
        }
        break;
    case IGraphObjectDownloaderObserver::ENothingDownloaded:
        /**
         * The workaround for Bugzilla 2084 issue has not helped. We have a Graph object which ID cannot be found on FB-server.
         */
        delete mObjectDownloader;
        mObjectDownloader = nullptr;
        delete mPostInPostDownloader;
        mPostInPostDownloader = nullptr;

        ProgressBarHide();
        if (Application::GetInstance()->GetTopScreenId() == mScreenId) {
            Pop();
        } else {
            ShowErrorWidget(eSOMETHING_WRONG_ERROR);
        }
        break;
    default:
        break;
    }
}

void PostScreen::SetData()
{
    assert(mInActionNData);
    assert(mInActionNData->mData);
    Log::debug("PostScreen::SetData() for post (%s)", mInActionNData->mData->GetId());
    mCommentsProvider->SetEntityId(mInActionNData->mData->GetId());
    mCommentsProvider->SetReverseChronological(true);

    SetBasicData();

    elm_object_signal_emit(mLayout, "entry.show", "default");

    SetEntryUi();

    ApplyProgressBar(true);
    SetShowConnectionErrorEnabled(true);
    RequestData(true);
}

void* PostScreen::CreateItem(void* dataForItem, bool isAddToEnd)
{
    Log::info("PostScreen::CreateItem");
    ActionAndData *data = NULL;
    if (dataForItem) {
        GraphObject *graphObject = static_cast<GraphObject *>(dataForItem);
        if (graphObject->GetGraphObjectType() == GraphObject::GOT_COMMENT) {
            Comment *comment = static_cast<Comment*>(dataForItem);
            data = new ActionAndData(comment, mCommentsAction);
            data->mParentWidget = CommentWidgetFactory::CommentsGet(data, GetDataArea(), isAddToEnd);
            if (!data->mParentWidget) {
                delete data;
                data = NULL;
            }
            HideProgressBar();
        } else if (graphObject->GetGraphObjectType() == GraphObject::GOT_OPERATION_PRESENTER) {
            Log::info("PostScreen::CreateItem - operation");
            OperationPresenter *operation = static_cast<OperationPresenter *>(dataForItem);
            data = new ActionAndData(operation, mCommentsAction);
            data->mParentWidget = CommentWidgetFactory::CreateCommentOperation(GetDataArea(), data, isAddToEnd);
            if (!data->mParentWidget) {
                delete data;
                data = NULL;
            }
            HideProgressBar();
            RePackConnectionError();
        }
    }
    return data;
}

void* PostScreen::UpdateItem( void* dataForItem, bool isAddToEnd )
{
    ActionAndData *data = static_cast<ActionAndData*>(dataForItem);

    DeleteUIItem(dataForItem);

    if (data) {
        GraphObject *graphObject = static_cast<GraphObject *>(data->mData);
        if (graphObject->GetGraphObjectType() == GraphObject::GOT_COMMENT) {
            data->mParentWidget = CommentWidgetFactory::CommentsGet(data, GetDataArea(), isAddToEnd);
            if (!data->mParentWidget) {
                delete data;
                data = NULL;
            }
        } else if (graphObject->GetGraphObjectType() == GraphObject::GOT_OPERATION_PRESENTER) {
            data->mParentWidget = CommentWidgetFactory::CreateCommentOperation(GetDataArea(), data, isAddToEnd);
            if (data->mParentWidget) {
                CommentWidgetFactory::UpdateCommentOperation(data);
            }
            else {
                delete data;
                data = NULL;
            }
        }
    }

    return data;
}

void PostScreen::DeleteItem(void* item)
{
    if (mSelectedActionNData == item) {
        mSelectedActionNData = NULL;
    }
    TrackItemsProxyAdapter::DeleteItem(item);
}

void PostScreen::SetSelectedActionNData(ActionAndData *selectedActionNData)
{
    mSelectedActionNData = selectedActionNData;
}

ActionAndData *PostScreen::GetSelectedActionNData()
{
    return mSelectedActionNData;
}

void PostScreen::Push()
{
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void PostScreen::OnResume()
{
    CommentWidgetFactory::add_scroller_events(GetScroller());
    ScreenBase::OnResume();
}

void PostScreen::SetScrollerUi()
{
    ApplyScroller(mLayout, false);
    elm_object_part_content_set(mLayout, "content", GetScroller());
    CommentWidgetFactory::add_scroller_events(GetScroller());

    Evas_Object* boxCoverAndProfPhoto = elm_box_add(GetScroller());
    elm_box_homogeneous_set(boxCoverAndProfPhoto, EINA_FALSE);
    elm_object_content_set(GetScroller(), boxCoverAndProfPhoto);
    evas_object_show(boxCoverAndProfPhoto);

    mContainer = elm_layout_add(boxCoverAndProfPhoto);
    elm_layout_file_set(mContainer, Application::mEdjPath, "post_screen");
    elm_box_pack_end(boxCoverAndProfPhoto, mContainer);
    evas_object_show(mContainer);

    mHeaderWidget = elm_box_add(mContainer);
    evas_object_show(mHeaderWidget);
    elm_layout_content_set(mContainer, "static.data.area.swallow", mHeaderWidget);

    Evas_Object* boxFeedWidget = elm_box_add(mContainer);
    evas_object_show(boxFeedWidget);
    elm_layout_content_set(mContainer, "feed.area.swallow", boxFeedWidget);
    SetDataArea(boxFeedWidget);
}

void PostScreen::SetEntryUi()
{
    mCommentEntryLayout = WidgetFactory::CreateLayoutByGroup(mLayout, "base_comment_entry_layout");
    elm_object_part_content_set(mLayout, "bot_bar", mCommentEntryLayout);

    mCommentEntry = elm_entry_add(mCommentEntryLayout);
    if (mCommentEntry) {
        elm_entry_prediction_allow_set(mCommentEntry, EINA_FALSE);
        elm_object_part_content_set(mCommentEntryLayout, "entry_field", mCommentEntry);
        elm_entry_scrollable_set(mCommentEntry, EINA_TRUE);
        char *buf = WidgetFactory::WrapByFormat2( SANS_REGULAR_9298A4_FORMAT, R->COMMENT_ENTRY_TEXT_SIZE, i18n_get_text("IDS_WRITE_COMMENT"));
        if (buf) {
            elm_object_part_text_set(mCommentEntry, "elm.guide", buf);
            delete[] buf;
        }

        elm_entry_text_style_user_push(mCommentEntry, COMMENT_TEXT_STYLE);
        evas_object_smart_callback_add(mCommentEntry, "changed,user", enable_send_btn, this);
        evas_object_smart_callback_add(mCommentEntry, "press", show_input_text_panel, this);
    }

    elm_object_signal_emit(mCommentEntryLayout,
            (Application::IsRTLLanguage() ? "send.icon.disable.rtl" : "send.icon.disable.ltr"),
            "send_comment_btn");

    mTagging = new FriendTagging(mLayout, mCommentEntry);
    mTagging->SetUpsideDownList(true);

    elm_object_signal_callback_add(mCommentEntryLayout, "mouse,clicked,*", "add_photo_icon*", on_add_photo_button_clicked_cb, this);
    elm_object_signal_callback_add(mCommentEntryLayout, "mouse,clicked,*", "send_comment_btn*", send_comment_cb, this);
    elm_object_signal_callback_add(mCommentEntryLayout, "mouse,clicked,*", "unattach_btn*", on_delete_button_clicked_cb, this);
}

void PostScreen::SetBasicData()
{
    if (mInActionNData && mInActionNData->mData) {
        Log::info("PostScreen::SetBasicData");
        if (mInActionNData->mData->GetGraphObjectType() == GraphObject::GOT_POST) {
            Post *post = static_cast<Post *> (mInActionNData->mData);
            WidgetFactory::RefreshBaseScreenLayout(mLayout, i18n_get_text("IDS_POSTS"));
            WidgetFactory::PostHeaderCustomization(mHeaderWidget, mInActionNData);
            if (post->GetMessage()) {
                WidgetFactory::CreatePostFullText(mHeaderWidget, mInActionNData);
            }
            if(post->GetParentId()) {
                Post* sharedSource = post->GetParent();
                mPostInPostWrapper = WidgetFactory::CreatePostInPostItemWrapper(mHeaderWidget);
                if (sharedSource) {
                    Log::info("PostScreen::SetBasicData->shared source post is loaded");
                    mSharedSourceNData = new ActionAndData(sharedSource, mPostScreenAction);
                    WidgetFactory::CreatePostInPostItem(mHeaderWidget, mSharedSourceNData, mPostInPostWrapper);
                    post->SetIsSharedPost(true);
                } else {
                    Log::info("PostScreen::SetBasicData->shared source post should be loaded");
                    mPostInPostDownloader = new GraphObjectDownloader(post->GetParentId(), this);
                    mPostInPostDownloader->StartDownloading();
                }
            } else {
                Log::info("PostScreen::SetBasicData->no parent");
                WidgetFactory::PostContentCustomization(mHeaderWidget, mInActionNData);
            }
            WidgetFactory::AddPostBtnsItemFooter(mHeaderWidget, mInActionNData);
            OperationManager::GetInstance()->AddOperation(MakeSptr<LikeOperation<Post>>(post->GetId() ? post->GetId() : "",
                                                                                        Operation::OT_GetLikesSummary,
                                                                                        mInActionNData));
        } else if (mInActionNData->mData->GetGraphObjectType() == GraphObject::GOT_PHOTO) {
            std::string headerText;
            Photo *photo = static_cast<Photo*>(mInActionNData->mData);
            if (photo) {
                if (photo->GetAlbum()) {
                    headerText = photo->GetAlbum()->mName;
                }
            }
            WidgetFactory::RefreshBaseScreenLayout(mLayout, headerText.c_str());
            CreatePhotoHeader();
            CreatePhotoItem();
        }
        CreateLikeText();
        RefreshLikeText();
        evas_object_show(mHeaderWidget);

        if (OperationManager::GetInstance()->GetActiveOperationById(mInActionNData->mData->GetId())) {
            elm_layout_signal_emit(mContainer, "deactivate_post", "");
        }
    }
}

void PostScreen::CreatePhotoHeader()
{
    Evas_Object *item_header = WidgetFactory::CreateLayoutByGroup(mHeaderWidget, "post_header");
    if (mInActionNData && mInActionNData->mData) {
        Photo *photo = static_cast<Photo*>(mInActionNData->mData);
        if (photo) {
            User *from = static_cast<User*>(photo->GetFrom());
            if (from) {
                Evas_Object *avatar = elm_image_add(item_header);
                elm_object_part_content_set(item_header, "avatar", avatar);
                if (from->mPicture && from->mPicture->mUrl) {
                    mInActionNData->UpdateImageLayoutAsync(from->mPicture->mUrl, avatar, ActionAndData::EImage, Post::EFromPicturePath);
                }
                evas_object_smart_callback_add(avatar, "clicked", ActionBase::on_avatar_clicked_cb, mInActionNData);
                evas_object_show(avatar);

                Evas_Object *title = elm_label_add(item_header);
                elm_layout_content_set(item_header, "user_name", title);
                evas_object_smart_callback_add(item_header, "clicked", ActionBase::on_avatar_clicked_cb, mInActionNData);
                std::stringstream titleString;
                char *name = WidgetFactory::WrapByFormat2(SANS_MEDIUM_000_FORMAT, R->NF_PH_TITLE_TEXT_SIZE, from->mName);
                if (name) {
                    titleString << name;
                    delete[] name;
                }
                elm_object_text_set(title, titleString.str().c_str());
                elm_label_wrap_width_set(title, R->NF_PH_TITLE_WRAP_WIDTH);
                evas_object_smart_callback_add(title, "anchor,clicked", ActionBase::on_anchor_clicked_cb, mInActionNData);
                elm_label_line_wrap_set(title, ELM_WRAP_MIXED);
                evas_object_show(title);
            }
            Evas_Object *info_box = WidgetFactory::CreatePostInfoBox(item_header);
            if (info_box) {
                WidgetFactory::CreatePostInfoDate(info_box, photo->GetCreatedTimeInMilliseconds());
            }
            elm_object_signal_emit(item_header, "context.hide", "context");
            elm_object_signal_callback_add(item_header, "post.comment.open", "", mInActionNData->mAction->on_comment_btn_clicked_cb, mInActionNData);
        }
    }
    elm_box_pack_end(mHeaderWidget, item_header);
}

void PostScreen::CreatePhotoItem()
{
    Evas_Object *photo_wrapper = elm_layout_add(mHeaderWidget);
    if (photo_wrapper && mInActionNData && mInActionNData->mData) {
        Photo *photoBase = static_cast<Photo*>(mInActionNData->mData);
        ImageFile *photo = static_cast<ImageFile*>(eina_list_nth(photoBase->GetImages(), 0));
        evas_object_size_hint_weight_set(photo_wrapper, 1, 1);
        evas_object_size_hint_align_set(photo_wrapper, -1, 0);
        Evas_Object *post_photo = elm_image_add(photo_wrapper);
        if (post_photo) {
            Evas_Coord post_photo_width = 0;
            Evas_Coord post_photo_height = 0;

            if (photo->GetWidth() > photo->GetHeight()) {
                post_photo_width = R->NF_LANDSCAPE_PHOTO_LAYOUT_SIZE_W - R->NF_LANDSCAPE_PHOTO_LAYOUT_OFFSET_W_L - R->NF_LANDSCAPE_PHOTO_LAYOUT_OFFSET_W_R;
                elm_layout_file_set(photo_wrapper, Application::mEdjPath, "post_landscape_photo");
            } else {
                post_photo_width = R->NF_SQUARE_PHOTO_LAYOUT_SIZE_W - R->NF_SQUARE_PHOTO_LAYOUT_OFFSET_W_L - R->NF_SQUARE_PHOTO_LAYOUT_OFFSET_W_R;
                elm_layout_file_set(photo_wrapper, Application::mEdjPath, "post_square_photo");
            }
            post_photo_height = (Evas_Coord) (photo->GetHeight() * (post_photo_width / (double) photo->GetWidth()));
            evas_object_size_hint_min_set(post_photo, post_photo_width, post_photo_height);
            evas_object_size_hint_max_set(post_photo, post_photo_width, post_photo_height);
            evas_object_size_hint_weight_set(post_photo, 1, 1);
            evas_object_size_hint_align_set(post_photo, -1, -1);
            evas_object_show(post_photo);
            elm_layout_content_set(photo_wrapper, "photo_box", post_photo);
            mInActionNData->UpdateImageLayoutAsync(photo->GetUrl(), post_photo, ActionAndData::EImage);
        }
        elm_box_pack_end(mHeaderWidget, photo_wrapper);
        evas_object_show(photo_wrapper);
    }
}

void PostScreen::enable_send_btn(void *data, Evas_Object *obj, void *event_info)
{
    PostScreen *me = static_cast<PostScreen*>(data);
    if (me) {
        char *CommentText = elm_entry_markup_to_utf8(elm_entry_entry_get(me->GetCommentEntry()));
        bool isTextExist = !Utils::isEmptyString(CommentText);
        free(CommentText);

        bool isPhotoExist = me->GetAttachedPhoto();
        me->mIsReadyForPost = isTextExist || isPhotoExist;

        std::string signalname = me->mIsReadyForPost ?
                (Application::IsRTLLanguage() ? "send.icon.enable.rtl" : "send.icon.enable.ltr") :
                (Application::IsRTLLanguage() ? "send.icon.disable.rtl" : "send.icon.disable.ltr");
        elm_object_signal_emit(me->mCommentEntryLayout, signalname.c_str(), "send_comment_btn");
    }
}

void PostScreen::show_input_text_panel(void *data, Evas_Object *obj, void *event_info)
{
    app_device_orientation_e rotation = app_get_device_orientation();
    if (rotation == APP_DEVICE_ORIENTATION_0) {
        PostScreen *screen = static_cast<PostScreen*>(data);
        elm_entry_input_panel_enabled_set(screen->mCommentEntry, EINA_TRUE);
        elm_entry_cnp_mode_set(screen->mCommentEntry, ELM_CNP_MODE_PLAINTEXT);
        elm_entry_input_panel_show(screen->mCommentEntry);
    }
}

void PostScreen::on_delete_button_clicked_cb(void *data, Evas_Object *obj, const char *emisson, const char *source)
{
    Log::info("PostScreen::on_delete_button_clicked_cb");

    PostScreen *screen = static_cast<PostScreen*>(data);
    if (screen) {
        screen->DeleteMedia();
        enable_send_btn(screen);
    }
}

void PostScreen::on_add_photo_button_clicked_cb(void *data, Evas_Object *obj, const char *emisson, const char *source)
{
    Log::info("PostScreen::on_add_photo_button_clicked_cb");

    if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_CAMERA_ROLL) {
        PostScreen *screen = static_cast<PostScreen*>(data);
        if (screen) {
            elm_entry_input_panel_hide(screen->mCommentEntry);
            CameraRollScreen* cameraRollScreen = new CameraRollScreen(eCAMERA_ROLL_COMMENT_MODE, screen);
            Application::GetInstance()->AddScreen(cameraRollScreen);
        }
    }
}

void PostScreen::send_comment_cb(void *data, Evas_Object *obj, const char *emisson, const char *source)
{
    Log::info("PostScreen::send_comment_cb");

    PostScreen *screen = static_cast<PostScreen*>(data);
    if (screen && screen->mIsReadyForPost) {
        screen->CheckOffStageItemsBelow();
        screen->mTagging->HideFriendsPopup();
        screen->PostComment();
    }
}

void PostScreen::PostComment()
{
    Log::info("PostScreen::PostComment");

    bundle* paramsBundle;
    paramsBundle = bundle_create();
    if (mTagging) {
        char *originTextToPost = mTagging->GetStringToPost(true);
        bundle_add_str(paramsBundle, "origin_message", originTextToPost);
        free(originTextToPost);
        char *textToPost = mTagging->GetStringToPost(false);
        if (textToPost && *textToPost != '\0') {
            bundle_add_str(paramsBundle, "message", textToPost);
        }
        free(textToPost);
    }

    Operation::OperationType type;
    type = (mScreenId == ScreenBase::SID_REPLY) ? Operation::OT_Add_Reply : Operation::OT_Add_Comment;

    assert(mInActionNData);
    assert(mInActionNData->mData);
    OperationManager::GetInstance()->AddOperation(MakeSptr<SimpleCommentOperation>(type, mInActionNData->mData->GetId() ? mInActionNData->mData->GetId() : "", true, paramsBundle, mMediaDataPath, mMediaDataThumbnailPath));
    SoundNotificationPlayer::PlayNotification(SoundNotificationPlayer::eSoundPostMain);

    bundle_free(paramsBundle);

    elm_entry_entry_set(mCommentEntry, "");
    elm_entry_input_panel_enabled_set( mCommentEntry, EINA_FALSE );
    elm_object_focus_set(mCommentEntry, EINA_FALSE);
    elm_entry_input_panel_hide(mCommentEntry);
    elm_object_signal_emit(mCommentEntryLayout,
            (Application::IsRTLLanguage() ? "send.icon.disable.rtl" : "send.icon.disable.ltr"),
            "send_comment_btn");
    enable_send_btn(this);

    DeleteMedia();
}

void PostScreen::UpdateMedia()
{
    Log::info("PostScreen::UpdateMedia");

    elm_object_signal_emit(mLayout, "entry.show", "expanded");
    elm_object_signal_emit(mCommentEntryLayout, "photo.attached", "show");

    Eina_List *mediaList = SelectedMediaList::Get();
    if (eina_list_count(mediaList)) {
        PostComposerMediaData * mediaData = static_cast<PostComposerMediaData *>(eina_list_data_get(mediaList));

        mAttachedPhoto = elm_image_add(mCommentEntryLayout);
        elm_object_part_content_set(mCommentEntryLayout, "attached_photo_box", mAttachedPhoto);
        elm_image_file_set(mAttachedPhoto, mediaData->GetThumbnailPath(), NULL);
        evas_object_show(mAttachedPhoto);

        mUnattachBtn = elm_image_add(mCommentEntryLayout);
        elm_object_part_content_set(mCommentEntryLayout, "unattach_btn", mUnattachBtn);
        elm_image_file_set(mUnattachBtn, ICON_COMMENTS_DIR "/delete_photo.png", NULL);
        evas_object_show(mUnattachBtn);
        free(mMediaDataPath);
        mMediaDataPath = SAFE_STRDUP(mediaData->GetFilePath());
        free(mMediaDataThumbnailPath);
        mMediaDataThumbnailPath = SAFE_STRDUP(mediaData->GetThumbnailPath());
        enable_send_btn(this);
    }
}

void PostScreen::DeleteMedia()
{
    if (SelectedMediaList::Count() == 1) {
        if (mAttachedPhoto) {
            evas_object_del(mAttachedPhoto);
            mAttachedPhoto = NULL;
        }
        if (mUnattachBtn) {
            evas_object_del(mUnattachBtn);
            mUnattachBtn = NULL;
        }
        SelectedMediaList::Clear();
        enable_send_btn(this);
        elm_object_signal_emit(mLayout, "entry.show", "default");
        elm_object_signal_emit(mCommentEntryLayout, "photo.unattached", "show");
    }
    RemoveMediaPath();
}

void PostScreen::RemoveMediaPath()
{
    free(mMediaDataPath);
    mMediaDataPath = nullptr;
    free(mMediaDataThumbnailPath);
    mMediaDataThumbnailPath = nullptr;
    SelectedMediaList::Clear();
}

void PostScreen::CreateLikeText()
{
    Log::info("PostScreen::CreateLikeText");

    int like_count = 0;
    bool has_liked = false;

    Evas_Object * text_wrapper = elm_layout_add(mHeaderWidget);
    evas_object_size_hint_weight_set(text_wrapper, 1, 1);
    evas_object_size_hint_align_set(text_wrapper, -1, 0);
    elm_layout_file_set(text_wrapper, Application::mEdjPath, "post_screen_like_bar");
    elm_box_pack_end(mHeaderWidget, text_wrapper);
    evas_object_show(text_wrapper);

    if (mInActionNData->mData->GetGraphObjectType() == GraphObject::GOT_POST) {
        Post * post = static_cast<Post*>(mInActionNData->mData);
        like_count = post->GetLikesCount();
        has_liked = post->GetHasLiked();
    } else if (mInActionNData->mData->GetGraphObjectType() == GraphObject::GOT_PHOTO) {
        Photo * photo = static_cast<Photo*>(mInActionNData->mData);
        like_count = photo->GetLikesCount();
        has_liked = photo->GetHasLiked();
    }

    Log::debug("PostScreen:GenPostLikes: likes_count = %d, has_liked = %d", like_count, has_liked);

    mPostText = elm_label_add(text_wrapper);
    elm_object_part_content_set(text_wrapper, "like_text", mPostText);
    elm_label_wrap_width_set(mPostText, R->NF_POST_MSG_WRAP_WIDTH);
    elm_label_line_wrap_set(mPostText, ELM_WRAP_MIXED);
    evas_object_show(mPostText);
}

void PostScreen::Update(AppEventId eventId, void * data)
{
    switch (eventId) {
    case eLIKE_COMMENT_EVENT:
        if (data && mInActionNData && mInActionNData->mData && !strcmp(static_cast<char *>(data), mInActionNData->mData->GetId())) {
            WidgetFactory::RefreshPostItemFooter(mInActionNData);
            RefreshLikeText();
        }
        break;
    case ePOST_MESSAGE_EVENT: {
        Log::info( "PostScreen::Update, ePOST_MESSAGE_EVENT");
        if (data) {
            const char *id = static_cast<char *> (data);
            if (mInActionNData) {
                assert(mInActionNData->mData);
                assert(mInActionNData->mData->GetId());
                if (mContainer && !strcmp(mInActionNData->mData->GetId(), id)) {
                    elm_layout_signal_emit(mContainer, "deactivate_post", "");
                }
            }
        }
        break;
    }
    case ePOST_RELOAD_EVENT:
        if (data) {
            Log::info("PostScreen::ePOST_RELOAD_EVENT");
            Post *post = static_cast<Post *>(data);
            if (mInActionNData) {
                assert(mInActionNData->mData);
                if (*(mInActionNData->mData) == *post) {
//should be added condition to replace sharedPostSource with new post (if they are equal)
//but let me fix it with other bug)
//AAA ToDo: The commented code below may be useful for B2787.
//                        if (post->GetParentId()) {
//                            Post *parentPost = GetPostById(post->GetParentId());
//                            if (parentPost) {
//                                post->SetParent(parentPost);
//                            }
//                        }
                    mInActionNData->ReSetData(post);
                    if (mHeaderWidget) {
                        elm_box_clear(mHeaderWidget);
                    }
                    mCommentsProvider->SetEntityId(mInActionNData->mData->GetId());
                    SetBasicData();
                    if (mContainer) {
                        elm_layout_signal_emit(mContainer, "activate_post", "");
                    }
                }
            }
        }
        break;
    case eUPDATE_POST_LIKES_SUMMARY:
        if(data) {
            Log::info("PostScreen::Update->eUPDATE_POST_LIKES_SUMMARY");
            if(mInActionNData) {
                assert(mInActionNData->mData);
                Post *post = static_cast<Post *>(data);
                Post *thisPost = static_cast<Post *>(mInActionNData->mData);
                if (thisPost != post) {// if object is other
                    if ( strcmp (thisPost->GetId(), post->GetId()) ==0 )  {
                         Log::info("PostScreen::Update->eUPDATE_POST_LIKES_SUMMARY set new summary for = %s",thisPost->GetId());
                         thisPost->CopyLikesSummary(*post);
                    }
                }
            }
        }
        break;
    case ePOST_UPDATE_ABORTED:
        if (data) {
            const char *id = static_cast<char *> (data);
            if (mInActionNData) {
                assert(mInActionNData->mData);
                assert(mInActionNData->mData->GetId());
                if (mContainer && !strcmp(mInActionNData->mData->GetId(), id)) {
                    elm_layout_signal_emit(mContainer, "activate_post", "");
                }
            }
        }
        break;
    case eINTERNET_CONNECTION_CHANGED:
        if (ConnectivityManager::Singleton().IsConnected()) {  //connection has been restored
            if (eina_list_count(GetProvider()->GetOperationPresenters()) == 0 &&
                  !IsOffStageItemsBelow() ) { //fetch new comments if no items in progress and no comments below scroller's bottom
                if (GetProvider()->GetDataCount()==0) {
                    Log::info(LOG_FACEBOOK, "PostScreen::Update->no comments erase data and request");
                    GetProvider()->EraseData();
                    RequestData(true);
                } else if (IsServiceWidgetOnBottom()) {
                    Log::info(LOG_FACEBOOK, "PostScreen::Update->request new bottom comments");
                    GetProvider()->ResetRequestTimer();
                    RequestData(true);
                }
            }
            HideConnectionError();
        } else {
            ShowConnectionError(IsServiceWidgetOnBottom());
        }
        break;
    case eREPLIES_LIST_WAS_MODIFIED:
        if(data) {
            Comment* comment = static_cast<Comment*>(data);
            GraphObject* oldGraphObject = static_cast<GraphObject*>(GetProvider()->GetGraphObjectById(comment->GetId()));
            if(oldGraphObject) {
                ActionAndData* actionData = static_cast<ActionAndData*>(FindItem(oldGraphObject, items_comparator_Obj_vs_ActionAndData));
                bool isModified = mCommentsProvider->ReplaceCommentObject(comment);;
                if(ActionAndData::IsAlive(actionData) && isModified) {
                    actionData->ReSetData(comment);
                    evas_object_del(actionData->mReplyActionObject);
                    Evas_Object* content = elm_layout_content_get (actionData->mParentWidget, "content");
                    if(content) {
                        CommentWidgetFactory::ReplyItem(content, actionData);
                    }
                } else {
                    Log::error("PostScreen::Update->Can't refresh reply widget");
                }
            }
        }
        break;
    case eCOMMENT_WAS_DELETED:
        if(data) {
            const char* id = static_cast<char*>(data);
            Comment * commentToDelete = static_cast<Comment*>(GetProvider()->GetGraphObjectById(id));
            if(commentToDelete) {
                commentToDelete->AddRef();
                DestroyItemByData(commentToDelete , TrackItemsProxyAdapter::items_comparator_Obj_vs_ActionAndData);
                GetProvider()->DeleteItem(commentToDelete);
                commentToDelete->Release();
            }
        }
        break;
    default:
        break;
    }
    TrackItemsProxy::Update(eventId,data);
}

void PostScreen::RefreshLikeText()
{
    Log::info("PostScreen::RefreshLikes()");

    if (mInActionNData->mData) {
        if (mInActionNData->mData->GetGraphObjectType() == GraphObject::GOT_POST) {
            Post *post = static_cast<Post *>(mInActionNData->mData);
            GenPostLikes(post->GetLikesCount(), post->GetHasLiked(), post->GetLikeFromId(), post->GetLikeFromName(), post->GetId());
        } else if (mInActionNData->mData->GetGraphObjectType() == GraphObject::GOT_PHOTO) {
            Photo *photo = static_cast<Photo *>(mInActionNData->mData);
            GenPostLikes(photo->GetLikesCount(), photo->GetHasLiked(), photo->GetLikeFromId(), photo->GetLikeFromName(), photo->GetId());
        }
    }
}


void PostScreen::GenPostLikes(int likeCount, bool hasLiked, const char *likeFromId, const char *likeFromName, const char *id) {
    char *untranslatedText = nullptr;

    if (hasLiked) {
        --likeCount;
    }

    if (likeCount == 0 || !likeFromId) {
        if (hasLiked) {
            char *anchorFormat1 = WidgetFactory::WrapByFormat2(SANS_MEDIUM_000_FORMAT_BOLD, R->COMMENT_TOP_TITLE, ANCHOR_FORMAT1);
            untranslatedText = WidgetFactory::WrapByFormat2(anchorFormat1, id, "%s");
            FRMTD_TRNSLTD_TXT_SET1(mPostText, untranslatedText, "IDS_YOU_LIKE_THIS");
            evas_object_smart_callback_add(mPostText, "anchor,clicked", on_anchor_clicked_cb, nullptr);
            delete[] anchorFormat1;
        } else {
            char *nonAnchorFormat = WidgetFactory::WrapByFormat2(SANS_MEDIUM_000_FORMAT, R->COMMENT_TOP_TITLE, "%s");
            FRMTD_TRNSLTD_TXT_SET1(mPostText, nonAnchorFormat, "IDS_BE_THE_FIRST_TO_LIKE_THIS");
            delete[] nonAnchorFormat;
        }
    } else if (likeCount == 1) {
        if (hasLiked) {
            char *anchorFormat3 = WidgetFactory::WrapByFormat2(SANS_MEDIUM_000_FORMAT_BOLD, R->COMMENT_TOP_TITLE, ANCHOR_FORMAT3);
            untranslatedText = WidgetFactory::WrapByFormat4(anchorFormat3, id, "%s", likeFromName, "%s");
            if (untranslatedText) {
                FRMTD_TRNSLTD_TXT_SET2(mPostText, untranslatedText, "IDS_YOU_AND", "IDS_LIKE_THIS");
            }
            delete[] anchorFormat3;
        } else {
            char *anchorFormat2 = WidgetFactory::WrapByFormat2(SANS_MEDIUM_000_FORMAT_BOLD, R->COMMENT_TOP_TITLE, ANCHOR_FORMAT2);
            untranslatedText = WidgetFactory::WrapByFormat3(anchorFormat2, id, likeFromName, "%s");
            if (untranslatedText) {
                FRMTD_TRNSLTD_TXT_SET1(mPostText, untranslatedText, "IDS_LIKES_THIS");
            }
            delete[] anchorFormat2;
        }
        evas_object_smart_callback_add(mPostText, "anchor,clicked", on_anchor_clicked_cb, nullptr);

    } else if (likeCount > 1) {
        if (hasLiked) {
            char *anchorFormat3 = WidgetFactory::WrapByFormat2(SANS_MEDIUM_000_FORMAT_BOLD, R->COMMENT_TOP_TITLE, ANCHOR_FORMAT3);
            std::string countString = Utils::FormatBigInteger(likeCount);
            untranslatedText = WidgetFactory::WrapByFormat4(anchorFormat3, id, "%s", countString.c_str(), "%s");
            if (untranslatedText) {
                FRMTD_TRNSLTD_TXT_SET2(mPostText, untranslatedText, "IDS_YOU_AND", "IDS_NUM_OTHERS_LIKE_THIS");
            }
            delete[] anchorFormat3;
        } else {
            --likeCount;
            if (likeCount == 1) {
                char *anchorFormat3 = WidgetFactory::WrapByFormat2(SANS_MEDIUM_000_FORMAT_BOLD, R->COMMENT_TOP_TITLE, ANCHOR_FORMAT3);
                untranslatedText = WidgetFactory::WrapByFormat4(anchorFormat3, id, likeFromName, "%s", "%s");
                if (untranslatedText) {
                    FRMTD_TRNSLTD_TXT_SET2(mPostText, untranslatedText, "IDS_WHO_LIKE_THIS_AND", "IDS_ONE_OTHER_LIKE_THIS");
                }
                delete[] anchorFormat3;
            } else {
                char *anchorFormat4 = WidgetFactory::WrapByFormat2(SANS_MEDIUM_000_FORMAT_BOLD, R->COMMENT_TOP_TITLE, ANCHOR_FORMAT4);
                std::string countString = Utils::FormatBigInteger(likeCount);
                untranslatedText = WidgetFactory::WrapByFormat5(anchorFormat4, id, likeFromName, "%s", countString.c_str(), "%s");
                if (untranslatedText) {
                    FRMTD_TRNSLTD_TXT_SET2(mPostText, untranslatedText, "IDS_WHO_LIKE_THIS_AND", "IDS_NUM_OTHERS_LIKE_THIS");
                }
                delete[] anchorFormat4;
            }
        }
        evas_object_smart_callback_add(mPostText, "anchor,clicked", on_anchor_clicked_cb, nullptr);
    }
    delete[] untranslatedText;
}

void PostScreen::on_anchor_clicked_cb(void *data, Evas_Object *obj, void *eventInfo) {
    Elm_Label_Anchor_Info *event = static_cast<Elm_Label_Anchor_Info*>(eventInfo);
    assert(event);
    if (event->name) {
        if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_WHO_LIKED) {
            WhoLikedScreen* screen = new WhoLikedScreen(event->name);
            Application::GetInstance()->AddScreen(screen);
        }
    }
}

bool PostScreen::HandleBackButton()
{
    bool ret = true;
    if (PopupMenu::Close()) {
    } else if (CommentWidgetFactory::CloseCommentPopup()) {
    } else if (mTagging && mTagging->HideFriendsPopup()) {
    } else {
        DeleteMedia(); // Clear the selected media if any
        ret = false;
    }
    return ret;
}

bool PostScreen::HandleKeyPadHide() {
    bool ret = true;
    if (mTagging && mTagging->HideFriendsPopup()) {
        elm_entry_input_panel_show(mCommentEntry);
    } else {
        ret = false;
    }
    return ret;
}
