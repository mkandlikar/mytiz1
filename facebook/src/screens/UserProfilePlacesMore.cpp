/*
 *  UserProfilePlacesMore.cpp
 *
 * Created: 18 SEP 2015
 * Author: Jeyaramakrishnan Sundar
 */

#include <string>
#include <sstream>
#include <system_info.h>

#include "ActionBase.h"
#include "Common.h"
#include "Config.h"
#include "UserProfilePlacesMore.h"
#include "PlacesProvider.h"
#include "FbResponseAboutPlaces.h"
#include "Utils.h"

UserProfilePlacesMore::UserProfilePlacesMore(ScreenBase *parentScreen, Evas_Object *container_layoyut, const char *profileId) :
        ScreenBase(parentScreen), TrackItemsProxyAdapter(container_layoyut, PlacesProvider::GetInstance())
{
    PlacesProvider::GetInstance()->SetEntityId(profileId);
    mLayout = GetScroller();
    mAction = new ActionBase(this);
    RequestData();
}

UserProfilePlacesMore::UserProfilePlacesMore(ScreenBase *parentScreen, Evas_Object *container_layoyut, const char *profileId, AbstractDataProvider *provider) : ScreenBase(parentScreen) ,TrackItemsProxyAdapter(container_layoyut, PlacesProvider::GetInstance())
{
    PlacesProvider::GetInstance()->SetEntityId(profileId);
    mLayout = GetScroller();
    mAction = new ActionBase(this);
    RequestData();
}

UserProfilePlacesMore::~UserProfilePlacesMore()
{
    delete mAction;
    EraseWindowData();
    PlacesProvider::GetInstance()->EraseData();
}

void* UserProfilePlacesMore::CreateItem(void *dataForItem, bool isAddToEnd)
{
    FbResponseAboutPlaces::PlacesList *places = static_cast<FbResponseAboutPlaces::PlacesList*>(dataForItem);
    ActionAndData *data = NULL;

    if (places) {
        data = new ActionAndData(places, mAction);
        data->mParentWidget = PlacesGet(places, GetDataArea(), isAddToEnd, this);
    }

    return data;
}

Evas_Object *UserProfilePlacesMore::PlacesGet(void *user_data, Evas_Object *obj, bool isAddToEnd, void *self_data)
{
    FbResponseAboutPlaces::PlacesList *places_data = static_cast<FbResponseAboutPlaces::PlacesList*>(user_data);
    UserProfilePlacesMore *me = static_cast<UserProfilePlacesMore*>(self_data);

    Evas_Object *content = me->CreateWrapper(obj, isAddToEnd);
    me->CreatePlaceItem(content, places_data);

    return content;
}

Evas_Object* UserProfilePlacesMore::CreateWrapper(Evas_Object* parent, bool isAddToEnd)
{
    Evas_Object *wrapper = elm_layout_add(parent);
    elm_layout_file_set(wrapper, Application::mEdjPath, "places_wrapper");
    evas_object_size_hint_weight_set(wrapper, 1, 1);
    evas_object_size_hint_align_set(wrapper, -1, 0);
    if ( isAddToEnd ) {
        elm_box_pack_end(parent, wrapper);
    } else {
        elm_box_pack_start(parent, wrapper);
    }
    evas_object_show(wrapper);

    Evas_Object *wrapperBox = elm_box_add(wrapper);
    evas_object_size_hint_weight_set(wrapperBox, 1, 1);
    evas_object_size_hint_align_set(wrapperBox, -1, 0);
    elm_layout_content_set(wrapper, "content", wrapperBox);
    elm_box_padding_set(wrapperBox, 0, 0);

    return wrapperBox;
}

void UserProfilePlacesMore::CreatePlaceItem(Evas_Object* parent, FbResponseAboutPlaces::PlacesList *places_data)
{
    Evas_Object *list_item = elm_layout_add(parent);
    evas_object_size_hint_weight_set(list_item, 1, 1);
    elm_layout_file_set(list_item, Application::mEdjPath, "about_places_composer");
    elm_box_pack_end(parent, list_item);
    evas_object_show(list_item);

    Evas_Object *avatar = elm_image_add(list_item);
    elm_image_file_set(avatar, places_data->mPicSquareWithLogo, NULL);
    elm_object_part_content_set(list_item, "item_photo", avatar);
    evas_object_show(avatar);

    elm_object_part_text_set(list_item, "item_text_line_1", places_data->place_name);
    int size = strlen(places_data->city_name) + strlen(places_data->country) + 2;
    char *temp_buf_line_2 = new char[size];
    Utils::Snprintf_s(temp_buf_line_2, size, "%s,%s", places_data->city_name , places_data->country);
    elm_object_part_text_set(list_item, "item_text_line_2", temp_buf_line_2);
    elm_object_part_text_set(list_item, "item_text_line_3", places_data->created_time);

    delete []temp_buf_line_2;

}

