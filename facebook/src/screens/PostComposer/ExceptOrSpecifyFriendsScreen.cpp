#include "ExceptOrSpecifyFriendsScreen.h"
#include "WidgetFactory.h"

#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "POSTCOMPOSER"


ExceptOrSpecifyFriendsScreen::ExceptOrSpecifyFriendsScreen(PCPrivacyModel *privacyModel, const char *caption, SelectionMode selectionMode) : ScreenBase(nullptr),
        mPrivacyModel(privacyModel), mGenList(nullptr), mLGPeople(nullptr), mMinCurPos(0), mGeneratedText(nullptr), mFilterText(nullptr), mSelectionMode(selectionMode) {
    mScreenId = (mSelectionMode == EExceptedFriendsMode ? ScreenBase::SID_EXCEPT_FRIENDS : ScreenBase::SID_SPECIFY_FRIENDS);

    mLayout = elm_layout_add(getParentMainLayout());
    elm_layout_file_set(mLayout, Application::mEdjPath, "except_friends_screen");

//Caption
    Evas_Object *header_text = PostComposerScreen::CreateCaptionText(mLayout, caption);
    Evas_Object *header = WidgetFactory::CreateGrayHeaderWithLogo(mLayout, header_text, nullptr, this);
    elm_object_part_content_set(mLayout, "swallow.top_toolbar", header);

//To: field
    mToField = elm_entry_add(mLayout);
    elm_layout_content_set(mLayout, "swallow.to_selector", mToField);
    elm_entry_single_line_set(mToField, EINA_TRUE);
    elm_entry_editable_set(mToField, EINA_FALSE);
    elm_entry_scrollable_set(mToField, EINA_TRUE);
    elm_scroller_policy_set(mToField, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_OFF);

    elm_object_translatable_part_text_set(mLayout, "text.to", "IDS_TO");

//Except: or Specific: field
    mSelectField = elm_entry_add(mLayout);
    elm_layout_content_set(mLayout, "swallow.except_txt", mSelectField);
    elm_entry_single_line_set(mSelectField, EINA_FALSE);
    elm_entry_editable_set(mSelectField, EINA_TRUE);
    elm_entry_scrollable_set(mSelectField, EINA_TRUE);
    elm_entry_prediction_allow_set(mSelectField, EINA_FALSE);
    elm_entry_text_style_user_push(mSelectField, R->EXCEPT_SPECIFY_FRIENDS_SEARCH_TEXT_STYLE);
    elm_scroller_policy_set(mSelectField, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_OFF);
    elm_entry_input_panel_return_key_disabled_set(mSelectField, EINA_TRUE);
    elm_entry_cnp_mode_set(mSelectField, ELM_CNP_MODE_PLAINTEXT);
//    const char* text = GenTaggedFriendsField();
//    elm_entry_entry_set(mSelectField, text);
    const char *LTRformat = "DEFAULT='align=left'";
    const char *RTLformat = "DEFAULT='align=right'";
    FRMTD_TRNSLTD_ENTRY_TXT(mSelectField, LTRformat, RTLformat);
    elm_object_translatable_part_text_set(mLayout, "text.except", (mSelectionMode == EExceptedFriendsMode ? "IDS_EXCEPT" : "IDS_FRIENDS"));

    evas_object_smart_callback_add(mSelectField, "cursor,changed,manual", cursor_changed_cb, this);
//AAA ToDo: add support for preedit later   evas_object_smart_callback_add(mSelectedFriendsField, "preedit,changed", FilterChangedCb, this);
    evas_object_smart_callback_add(mSelectField, "changed", filter_changed_cb, this);

//swallow.friends_list
    mGenList = new GenList(mLayout);
    elm_object_part_content_set(mLayout, "swallow.list", mGenList->GetGenList());
    elm_scroller_policy_set(mGenList->GetGenList(), ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_AUTO);

    FillList(mPrivacyModel->GetFriendsList());
    mGenList->ApplyFilter();

    elm_naviframe_item_title_enabled_set(elm_naviframe_top_item_get(getNf()), EINA_FALSE, EINA_FALSE);
    mPrivacyModel->AddObserver(this);
    UpdateFields();

    evas_object_event_callback_add(mSelectField, EVAS_CALLBACK_KEY_DOWN, key_down_cb, this);
}

ExceptOrSpecifyFriendsScreen::~ExceptOrSpecifyFriendsScreen() {
    delete mGenList;
    delete [] mGeneratedText;
    mPrivacyModel->RemoveObserver(this);
}

void ExceptOrSpecifyFriendsScreen::Push() {
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(elm_naviframe_top_item_get(getNf()), EINA_FALSE, EINA_FALSE);
}

void ExceptOrSpecifyFriendsScreen::FillList(Eina_List *friends) {
    mLGPeople = new ListGroup("IDS_CAP_PEOPLE", this);
    Elm_Object_Item *it = mGenList->AddItem(mLGPeople);
    elm_genlist_item_select_mode_set(it, ELM_OBJECT_SELECT_MODE_NONE);

    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(friends, l, listData) {
        Friend * fr = static_cast<Friend *> (listData);

        Log::debug_tag(LOG_TAG, "NAME:%s", fr->mName.c_str());
        ListItem *friendItem = new ListItem(this);
        friendItem->mFriend = fr;
        friendItem->mIsSelected = (mSelectionMode == EExceptedFriendsMode ? mPrivacyModel->IsDeniedFriend(fr) : mPrivacyModel->IsAllowedFriend(fr));

        it = mLGPeople->AddItem(friendItem);
        elm_genlist_item_select_mode_set(it, ELM_OBJECT_SELECT_MODE_NONE);
    }
}

void ExceptOrSpecifyFriendsScreen::UpdateFields() {
    const char *toText = PostComposerScreen::GenPrivacy(mPrivacyModel);
    if (toText) {
        int size = strlen(toText) + strlen(R->EXCEPT_SPECIFY_FRIENDS_TO_TEXT_FORMAT) + 1;
        char *frmText = new char[size];
        Utils::Snprintf_s(frmText, size, R->EXCEPT_SPECIFY_FRIENDS_TO_TEXT_FORMAT, toText);
        delete [] toText;
        elm_entry_entry_set(mToField, frmText);
        delete [] frmText;
    }

//remove callbacks before update
    evas_object_smart_callback_del(mSelectField, "cursor,changed,manual", cursor_changed_cb);
    evas_object_smart_callback_del(mSelectField, "changed", filter_changed_cb);

    const char *selectedFriendsText = (mSelectionMode == EExceptedFriendsMode ? mPrivacyModel->GenDeniedFriends() : mPrivacyModel->GenAllowedFriends());
    delete [] mGeneratedText;
    mGeneratedText = nullptr;
    if (selectedFriendsText) {
        int size = strlen(selectedFriendsText) + strlen(R->EXCEPT_SPECIFY_FRIENDS_TEXT_FORMAT) + 1;
        mGeneratedText = new char[size];
        Utils::Snprintf_s(mGeneratedText, size, R->EXCEPT_SPECIFY_FRIENDS_TEXT_FORMAT, selectedFriendsText);
        delete [] selectedFriendsText;
        elm_entry_entry_set(mSelectField, mGeneratedText);
    } else {
        elm_entry_entry_set(mSelectField, "");
    }

//restore callbacks after update
    evas_object_smart_callback_add(mSelectField, "cursor,changed,manual", cursor_changed_cb, this);
    evas_object_smart_callback_add(mSelectField, "changed", filter_changed_cb, this);

    elm_entry_cursor_end_set(mSelectField);
    mMinCurPos = elm_entry_cursor_pos_get(mSelectField);
}

void ExceptOrSpecifyFriendsScreen::PrivacyChanged(IPCPrivacyObserver::PrivacyEvent privacyEvent) {
    switch (privacyEvent) {
    case IPCPrivacyObserver::EPrivacyOptionManuallyChanged:
    case IPCPrivacyObserver::EPrivacyOptionChangedByProgram:
    case IPCPrivacyObserver::EExceptedFriendsChanged:
    case IPCPrivacyObserver::ESpecificFriendsChanged:
        UpdateFields();
        break;
    case IPCPrivacyObserver::EExceptedFriendsLimitExceeded:
        WidgetFactory::ShowAlertPopup("IDS_MAX_EXCEPTED_FRIENDS_LIMIT_REACHED_MESSAGE", "IDS_MAX_EXCEPTED_FRIENDS_LIMIT_REACHED_CAPTION");
        break;
    case IPCPrivacyObserver::ESpecificFriendsLimitExceeded:
        WidgetFactory::ShowAlertPopup("IDS_MAX_SPECIFIC_FRIENDS_LIMIT_REACHED_MESSAGE", "IDS_MAX_SPECIFIC_FRIENDS_LIMIT_REACHED_CAPTION");
        break;
    default:
        break;
    }
}

void ExceptOrSpecifyFriendsScreen::onItemClicked (void *data, Evas_Object *obj, const char *emission, const char *source) {
    if (data) {
        ListItem* Item = static_cast<ListItem*>(data);
        Item->Toggle();
    }
}

void ExceptOrSpecifyFriendsScreen::filter_changed_cb(void *data, Evas_Object *obj, void *event_info) {
    ExceptOrSpecifyFriendsScreen *me = static_cast<ExceptOrSpecifyFriendsScreen *>(data);
    int curPos = 0;
    if(me->mSelectField) curPos = elm_entry_cursor_pos_get(me->mSelectField);
    if (curPos < me->mMinCurPos) {
        me->UpdateFields();
    } else {
        const char *text = elm_object_part_text_get(me->mSelectField, nullptr);
        me->mFilterText = text + (me->mGeneratedText ? strlen(me->mGeneratedText) : 0);
        me->mFilterText = strlen(me->mFilterText) > 0 ? me->mFilterText : nullptr;
        Log::debug_tag(LOG_TAG, "mFilterText:%s", me->mFilterText);
//AAA ToDo: add support for preedit later        char* ttt = elm_entry_markup_to_utf8 (me->mFilterText);
//AAA ToDo: add support for preedit later        LOGI("mFilterText 1 :%s", ttt);
        me->mGenList->ApplyFilter();
    }
}

void ExceptOrSpecifyFriendsScreen::cursor_changed_cb(void *data, Evas_Object *obj, void *event_info) {
    ExceptOrSpecifyFriendsScreen *me = static_cast<ExceptOrSpecifyFriendsScreen *>(data);
   int curPos = elm_entry_cursor_pos_get(me->mSelectField);
   Log::debug_tag(LOG_TAG, "cursor_changed_cb:%d", curPos);
   if (curPos < me->mMinCurPos) {
       elm_entry_cursor_pos_set(me->mSelectField, me->mMinCurPos);
   }
}

/*
 * Class ExceptOrSpecifyFriendsScreen::ListGroup
 */
ExceptOrSpecifyFriendsScreen::ListGroup::ListGroup(const char*groupName, ExceptOrSpecifyFriendsScreen *screen) : mScreen(screen) {
    mGroupName = Utils::getCopy(groupName);
}

Evas_Object* ExceptOrSpecifyFriendsScreen::ListGroup::DoCreateLayout(Evas_Object* parent) {
    Log::debug_tag(LOG_TAG, "ListGroup::DoCreateLayout");

    mLayout = elm_layout_add(parent);
    elm_layout_file_set(mLayout, Application::mEdjPath, "except_friends_group_item");

//text
    elm_object_translatable_part_text_set(mLayout, "text", "IDS_CAP_PEOPLE");

//min size
    Evas_Coord maxw, maxh;
    Evas_Object* layout = elm_layout_edje_get(mLayout);
    edje_object_size_min_calc ((Evas_Object *)layout, &maxw, &maxh);
    Log::debug_tag(LOG_TAG, "GROUP ITEM size: %dx%d", maxw, maxh);
    evas_object_size_hint_min_set(mLayout, maxw, maxh);

    return mLayout;
}

void ExceptOrSpecifyFriendsScreen::key_down_cb(void* data, Evas* e, Evas_Object* obj, void* eventInfo) {
    ExceptOrSpecifyFriendsScreen *me = static_cast<ExceptOrSpecifyFriendsScreen*>(data);
    Evas_Event_Key_Up* EvasKeyEvent = static_cast<Evas_Event_Key_Up*>(eventInfo);
    if (!strcmp(EvasKeyEvent->keyname, "BackSpace")) {
        me->BackspacePressed();
    }
}

void ExceptOrSpecifyFriendsScreen::BackspacePressed() {
    if (!mFilterText) {
        ListItem* item = GetLastSelectedItem();
        if (item) {
            item->Toggle();
        }
    }
}

ExceptOrSpecifyFriendsScreen::ListItem* ExceptOrSpecifyFriendsScreen::GetLastSelectedItem() {
    ListItem* LastSelectdItem = nullptr;
    Eina_List* PeopleList;
    void* ListData = nullptr;
    EINA_LIST_FOREACH(mLGPeople->GetList(), PeopleList, ListData) {
        ListItem* Item = static_cast<ListItem*>(ListData);
        if (Item->mIsSelected) {
            LastSelectdItem = Item;
        }
    }

    return LastSelectdItem;
}


/*
 * Class ExceptOrSpecifyFriendsScreen::ListItem
 */
ExceptOrSpecifyFriendsScreen::ListItem::ListItem(ExceptOrSpecifyFriendsScreen *screen) : mFriend(nullptr), mLayout(nullptr),
        mScreen(screen), mIsSelected(false), mRequestId(0), mImage(nullptr) {
}

ExceptOrSpecifyFriendsScreen::ListItem::~ListItem() {
    Cancel(mRequestId);
}

void ExceptOrSpecifyFriendsScreen::ListItem::CreateImage(Evas_Object *parent) {
    mImage = elm_image_add(parent);
    elm_object_part_content_set(mLayout, "swallow.icon", mImage);

    //Check if the image is already downloaded. ToDo: probably move this functionality to
    //Application::GetInstance()->mDataService->DownloadImage() later.
    char *imagePath = Utils::GetImagePathFromUrl(mFriend->mPicturePath.c_str());
    if (imagePath) {
        FILE *fp = fopen(imagePath, "r");
        if(fp) {
            fclose(fp);
            elm_image_file_set(mImage, imagePath, nullptr);
        } else {
            //AAA replace with placeholder
            Application::GetInstance()->mDataService->DownloadImage(this, mFriend->mPicturePath.c_str(), mRequestId);
        }
        free((void *) imagePath);
    }
}

Evas_Object* ExceptOrSpecifyFriendsScreen::ListItem::DoCreateLayout(Evas_Object* parent) {
    Log::debug_tag(LOG_TAG, "DoCreateLayout");

    mLayout = elm_layout_add(parent);
    elm_layout_file_set(mLayout, Application::mEdjPath, "except_friends_item_check_image_text");

//check
    Evas_Object* check = elm_check_add(mLayout);
    elm_check_state_set(check, mIsSelected);
    elm_object_part_content_set(mLayout, "swallow.check", check);

//avatar
    CreateImage(mLayout);

//text
    elm_object_part_text_set(mLayout, "text", mFriend->mName.c_str());

//min size
    Evas_Coord maxw, maxh;
    Evas_Object* layout = elm_layout_edje_get(mLayout);
    edje_object_size_min_calc ((Evas_Object *)layout, &maxw, &maxh);
    Log::debug_tag(LOG_TAG, "!!!:%d, %d", maxw, maxh);
    evas_object_size_hint_min_set(mLayout, maxw, maxh);

//callback on click
    elm_object_signal_callback_add(mLayout, "mouse,clicked", "item", onItemClicked, this);

    return mLayout;
}

bool ExceptOrSpecifyFriendsScreen::HandleBackButton() {
    return WidgetFactory::CloseAlertPopup();
}

void ExceptOrSpecifyFriendsScreen::ListItem::HandleDownloadImageResponse(ErrorCode error, MessageId requestID, const char* url, const char* fileName) {
    Log::debug_tag(LOG_TAG, "HandleDownloadImageResponse:%s", fileName);
    elm_image_file_set(mImage, fileName, nullptr);
}

int ExceptOrSpecifyFriendsScreen::ListItem::Compare(GenListItemBase* otherItem) {
    ListItem *other = (ListItem *)otherItem;
    return (strcmp(mFriend->mName.c_str(), other->mFriend->mName.c_str()));
}

bool ExceptOrSpecifyFriendsScreen::ListItem::Filter(Elm_Object_Item *insertAfter) {
    bool isFiltered = false;
    if (mScreen->GetFilterText()) {
        char* filter = Utils::Trim(mScreen->GetFilterText());
        if (filter && strlen(filter) > 0) {
            isFiltered = mScreen->GetFilterText() && !Utils::UAreAllWordsSubstringsOfAnyWord(mFriend->mName.c_str(), filter, false);
            free (filter);
        }
    }
    return isFiltered;
}

void ExceptOrSpecifyFriendsScreen::ListItem::Toggle() {
    if (!mIsSelected) {
        if (mScreen->mSelectionMode == ExceptOrSpecifyFriendsScreen::EExceptedFriendsMode &&
            mScreen->mPrivacyModel->DeniedFriendSelected(mFriend, true)) {
            mIsSelected = true;
        } else if (mScreen->mSelectionMode == ExceptOrSpecifyFriendsScreen::ESpecificFriendsMode &&
            mScreen->mPrivacyModel->AllowedFriendSelected(mFriend, true)) {
            mIsSelected = true;
        }
    } else {
        if (mScreen->mSelectionMode == ExceptOrSpecifyFriendsScreen::EExceptedFriendsMode) {
            mScreen->mPrivacyModel->DeniedFriendUnselected(mFriend);
        } else if (mScreen->mSelectionMode == ExceptOrSpecifyFriendsScreen::ESpecificFriendsMode) {
            mScreen->mPrivacyModel->AllowedFriendUnselected(mFriend);
        }
        mIsSelected = false;
    }

    Evas_Object* checkBox = elm_object_part_content_get(mLayout, "swallow.check");
    if (checkBox) {
        elm_check_state_set(checkBox, mIsSelected);
    }
}
