#include "AddCropScreen.h"
#include "Album.h"
#include "CameraRollScreen.h"
#include "CheckInScreen.h"
#include "ChooseFriendScreen.h"
#include "ChooseGroupScreen.h"
#include "Common.h"
#include "Config.h"
#include "ConnectivityManager.h"
#include "ComposerPhotoWidget.h"
#include "CSmartPtr.h"
#include "DataUploader.h"
#include "EditCaptionsScreen.h"
#include "FbRespondPost.h"
#include "FbResponseAlbumDetails.h"
#include "FbResponseImageDetails.h"
#include "GroupFriendsProvider.h"
#include "Log.h"
#include "LoggedInUserData.h"
#include "Operation.h"
#include "OperationManager.h"
#include "ParserWrapper.h"
#include "Photo.h"
#include "PhotoPostOperation.h"
#include "Popup.h"
#include "PopupContextTip.h"
#include "Post.h"
#include "PostComposerScreen.h"
#include "PostPresenter.h"
#include "PostUpdateOperation.h"
#include "PresenterHelpers.h"
#include "ProfileWidgetFactory.h"
#include "SelectAlbumScreen.h"
#include "SelectedMediaList.h"
#include "ShareWithScreen.h"
#include "SimplePostOperation.h"
#include "SuggestedFriendsProvider.h"
#include "User.h"
#include "Utils.h"
#include "VideoPlayerScreen.h"
#include "WidgetFactory.h"

#include <cassert>
#include <notification.h>
#include <stdio.h>
#include <utils_i18n.h>

#define MAX_PHOTOS_IN_POST_LAYOUT 5

static const char* LogTag = "POSTCOMPOSER";

const char *KPrivacyFormatForAddPhotosToAlbum = "%s (%s)";
const int PostComposerScreen::MAX_POST_MESSAGE_LENGTH = 63206;
const unsigned int PostComposerScreen::MAX_ALBUM_TITLE_LENGTH = 65;

void PostComposerScreen::posttofacebook_item_select_cb(void *data, Evas_Object *obj, void *event_info) {
    Log::debug_tag(LogTag, "posttofacebook_item_select_cb");
    PostComposerScreen *me = static_cast<PostComposerScreen *>(data);
    me->mIsAudienceChanged = true;
    me->SetToId(NULL);
    me->SetToName(NULL);
    me->mPostDestinationType = Operation::OD_Home;
    me->UpdateDestinationSelector();
    me->UpdatePrivacyField();
    if (SelectedMediaList::Count() && !me->mShareTextPictures) {
        me->SetPhotoAlbumButtonMode(EShown);
        if (!me->mAddPhotoButtonLayout && me->mMediaBox) {
            me->CreateAddPhotoButton(me->mMediaBox);
        }
    }
}

void PostComposerScreen::onfriendstimeline_item_select_cb(void *data, Evas_Object *obj, void *event_info) {
    Log::debug_tag(LogTag, "onfriendstimeline_item_select_cb");
    PostComposerScreen *me = static_cast<PostComposerScreen *>(data);
    if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_CHOOSE_FRIEND) {
        ChooseFriendScreen* screen = new ChooseFriendScreen(me);
        Application::GetInstance()->AddScreen(screen);
    }
}

void PostComposerScreen::ingroup_item_select_cb(void *data, Evas_Object *obj, void *event_info)
{
    Log::debug_tag(LogTag, "ingroup_item_select_cb");
    PostComposerScreen *me = static_cast<PostComposerScreen *>(data);
    if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_CHOOSE_GROUP) {
        ChooseGroupScreen* screen = new ChooseGroupScreen(me);
        Application::GetInstance()->AddScreen(screen);
    }
}

void PostComposerScreen::on_tag_friend_button_clicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    Log::debug_tag(LogTag, "on_tag_friend_button_clicked");
    PostComposerScreen *screen = static_cast<PostComposerScreen *>(data);
    screen->LaunchTagFriendsScreen();
}

void PostComposerScreen::on_checkin_button_clicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    Log::debug_tag(LogTag, "on_checkin_button_clicked");
    PostComposerScreen *screen = static_cast<PostComposerScreen *>(data);
    screen->LaunchCheckInScreen();
}

void PostComposerScreen::add_media_button_clicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    Log::debug_tag(LogTag, "add_media_button_clicked");
    PostComposerScreen *screen = static_cast<PostComposerScreen *>(data);
    if(screen) {
        if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_CAMERA_ROLL) {
            if (screen->mMessageEntry) {
                elm_entry_input_panel_hide(screen->mMessageEntry);
            }
            CameraRollScreen* cameraRollScreen = NULL;
            if (screen->IsOnFriendTimelinePost()) {
                cameraRollScreen = new CameraRollScreen(eCAMERA_ROLL_POST_COMPOSER_MODE_FRIEND_TIMELINE);
            } else {
                cameraRollScreen = new CameraRollScreen(eCAMERA_ROLL_POST_COMPOSER_MODE, nullptr, screen->mAlbumToAddPhotos);
            }
            Application::GetInstance()->AddScreen(cameraRollScreen);
        }
    }
}

void PostComposerScreen::add_select_album_button_clicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    Log::debug_tag(LogTag, "add_select_album_button_clicked");
    PostComposerScreen *screen = static_cast<PostComposerScreen *>(data);
    if(screen) {
        if(screen->mSelectAlbumScreen) {
            delete screen->mSelectAlbumScreen;
            screen->mSelectAlbumScreen = NULL;
        }
        screen->mSelectAlbumScreen = new SelectAlbumScreen(screen, Config::GetInstance().GetUserId().c_str(), screen->mAlbumToAddPhotos ? screen->mAlbumToAddPhotos->GetId() : nullptr);
        screen->mSelectAlbumPopup = elm_popup_add(screen->mLayout);
        elm_popup_align_set(screen->mSelectAlbumPopup, ELM_NOTIFY_ALIGN_FILL, 0.5);
        evas_object_smart_callback_add(screen->mSelectAlbumPopup, "block,clicked", select_album_screen_dismiss_cb, screen);
        elm_object_content_set(screen->mSelectAlbumPopup, screen->mSelectAlbumScreen->GetLayout());
        evas_object_show(screen->mSelectAlbumPopup);
    }
}

void PostComposerScreen::CloseSelectAlbumPopup() {
    Log::debug_tag(LogTag, "CloseSelectAlbumPopup()");
    if(mSelectAlbumScreen) {
        delete mSelectAlbumScreen;
        mSelectAlbumScreen = NULL;
    }
    if (mSelectAlbumPopup) {
        evas_object_del(mSelectAlbumPopup);
        mSelectAlbumPopup = NULL;
    }
}

void PostComposerScreen::select_album_screen_dismiss_cb(void *data, Evas_Object *obj, void *event_info){
    PostComposerScreen *screen = static_cast<PostComposerScreen *>(data);
    screen->CloseSelectAlbumPopup();
}

void PostComposerScreen::AlbumSelected(Album* album, bool isNewCreation) {
    CloseSelectAlbumPopup();
    if (!isNewCreation) {
        mAlbumToAddPhotos = album;
        if (mAlbumToAddPhotos) {
            Log::debug_tag(LogTag, "ALBUM SELECTED:%s", album->mName);
            mAlbumToAddPhotos->AddRef();
            SwitchFromDefaultToAddPhotosToAlbumMode();
            SetPhotoAlbumButtonMode(EActive);
        } else {
            SwitchFromAddPhotosToAlbumToDefaultMode();
            SetPhotoAlbumButtonMode(EShown);
        }
    }
}

void PostComposerScreen::PlaceSelected(Place* place) {
    delete mPlace;
    if (place) {
        mPlace = new PCPlace(*place);
        Log::debug_tag(LogTag, "PLACE SELECTED:%s", mPlace->mName.c_str());

    } else {
        mPlace = NULL;
    }
    PostChanged(EPlace);
}

void PostComposerScreen::on_to_selector_clicked (void *data, Evas_Object *obj, const char *emission, const char *source) {
    Log::debug_tag(LogTag, "on_to_selector_clicked!!%s:%s", emission, source);

    PostComposerScreen *me = static_cast<PostComposerScreen *>(data);

    if(me->mIsPrivacyFieldEnabled) {
        if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_SHARE_WITH) {
            if(me->mPrivacyModel) {
//if ShareWithScreen is opened then we can't refresh privacy options anymore
                me->mPrivacyModel->EnableRefresh(false);
            }
            me->mShareWithScreen = new ShareWithScreen(me, &(me->GetPrivacy()), nullptr);
            Application::GetInstance()->AddScreen(me->mShareWithScreen);
        }
    } else {
        if (me->GetToPrivacy()) {
            if(!strcmp(me->GetToPrivacy(), "Open") ) {
                PopupContextTip::Open(me->mPrivacyLayout, i18n_get_text("IDS_GROUP_POST_TIP_PRIVACY_PUBLIC"));
            } else {
                PopupContextTip::Open(me->mPrivacyLayout, i18n_get_text("IDS_GROUP_POST_TIP_PRIVACY_CLOSED"));
            }
        } else if (me->IsOnFriendTimelinePost()) {
            const char *text = WidgetFactory::WrapByFormat2("%s %s", me->mToName, i18n_get_text("IDS_GROUP_POST_TIP_PRIVACY_FRIENDS_TIMELINE"));
            PopupContextTip::Open(me->mPrivacyLayout, text);
            delete []text;
        }
    }
}

Friend *PostComposerScreen::GetFriendById(const char*id) {
    Friend *fr = NULL;
    if (id) {
        Eina_List *l;
        void *listData;
        EINA_LIST_FOREACH(mPrivacyModel->GetFriendsList(), l, listData) {
            Friend *get_fr = static_cast<Friend *> (listData);
            if (!strcmp(get_fr->GetId(), id)) {
                fr = get_fr;
                break;
            }
        }
    }
    return fr;
}

void PostComposerScreen::UpdateDestinationSelector() {
    if(mDestinationSelector) {
        const char* buttonStyle;
        buttonStyle = "back_logo_grey";
        if (mToId) {
            char *caption = NULL;
            switch (mPostDestinationType) {
                case Operation::OD_Friend_Timeline:
                    caption = WidgetFactory::WrapByFormat(i18n_get_text("IDS_ON_TIMELINE"), mToName);
                    break;
                case Operation::OD_Group:
                    caption = WidgetFactory::WrapByFormat(i18n_get_text("IDS_POST_TO_GROUP"), mToName);
                    buttonStyle = "back_group_logo_grey";
                    break;
                case Operation::OD_Page:
                case Operation::OD_Event:
                    caption = WidgetFactory::WrapByFormat("%s", mToName);
                    break;
                default:
                    break;
            }

            if(caption) {
                elm_object_part_text_set(mDestinationSelector, "selected_item_text", caption);
                delete[] caption;
            }
        } else {
            switch (mMode) {
            case eDEFAULT:
            case eDEFAULTAPPTEXTSHARE:
                elm_object_translatable_part_text_set(mDestinationSelector, "selected_item_text", "IDS_POST_TO_FACEBOOK");
                break;
            case eSHARE:
                elm_object_translatable_part_text_set(mDestinationSelector, "selected_item_text", "IDS_SHARE_TO_FACEBOOK");
                break;
            case eSHARE_EVENT:
                elm_object_translatable_part_text_set(mDestinationSelector, "selected_item_text", "IDS_SHARE_TO_FACEBOOK");
                break;
            default:
                break;
            }
        }
        if(mHeader) {
            Evas_Object *backButton = elm_layout_content_get(mHeader, "back_button");
            ELM_BUTTON_STYLE_SET1(backButton, buttonStyle);
        }
    }
}

void PostComposerScreen::on_dest_selector_clicked (void *data, Evas_Object *obj, const char *emission, const char *source) {
    Log::debug_tag(LogTag, "on_dest_selector_clicked!!%s:%s", emission, source);

    PostComposerScreen *screen = static_cast<PostComposerScreen *>(data);
#ifdef _SCROLLER_WORKAROUND_ENABLED_
    screen->StoreScrollPosition();
#endif

    //To hide the blue pointer
    if (screen->mMessageEntry) {
        elm_object_focus_set(screen->mMessageEntry, EINA_FALSE);
        elm_object_focus_set(screen->mMessageEntry, EINA_TRUE);

        elm_entry_input_panel_hide(screen->mMessageEntry);
        screen->RemoveEntryFocus();
    }

    Evas_Coord y = 0, h = 0;
    if(obj) {
        evas_object_geometry_get(obj, NULL, &y, NULL, &h);
        y += h;
    }

// don't shown friend's timeline option if more that 1 photo is selected.
    if (SelectedMediaList::Count() > 1) {
        const int menuItemsNumber = 2;
        static PopupMenu::PopupMenuItem destination_menu_items[menuItemsNumber] =
        {
            {
                ICON_DIR "/to_facebook.png",
                "IDS_POST_TO_FACEBOOK",
                NULL,
                posttofacebook_item_select_cb,
                NULL,
                0,
                false,
                false
            },
            {
                ICON_DIR"/group.png",
                "IDS_IN_A_GROUP",
                NULL,
                ingroup_item_select_cb,
                NULL,
                1,
                true,
                false
            }
        };

        destination_menu_items[0].data = screen;
        destination_menu_items[1].data = screen;
        PopupMenu::Show(menuItemsNumber, destination_menu_items, screen->mLayout, false, y);
    } else {
        const int menuItemsNumber = 3;
        static PopupMenu::PopupMenuItem destination_menu_items[menuItemsNumber] =
        {
                {
                    ICON_DIR "/to_facebook.png",
                    NULL,
                    NULL,
                    posttofacebook_item_select_cb,
                    NULL,
                    0,
                    false,
                    false
                },
                {
                    ICON_DIR "/friends.png",
                    "IDS_ON_A_FRIENDS_TIMELINE",
                    NULL,
                    onfriendstimeline_item_select_cb,
                    NULL,
                    1,
                    false,
                    false
                },
                {
                    ICON_DIR"/group.png",
                    "IDS_IN_A_GROUP",
                    NULL,
                    ingroup_item_select_cb,
                    NULL,
                    2,
                    true,
                    false
                }
            };

            static const char *itemText_shareToFb = "IDS_SHARE_TO_FACEBOOK";
            static const char *itemText_postToFb = "IDS_POST_TO_FACEBOOK";
            if (screen->mMode == eSHARE || screen->mMode == eSHARELINKWEB || screen->mMode == eSHARE_EVENT) {
                destination_menu_items[0].itemText = itemText_shareToFb;
            } else {
                destination_menu_items[0].itemText = itemText_postToFb;
            }
            destination_menu_items[0].data = screen;
            destination_menu_items[1].data = screen;
            destination_menu_items[2].data = screen;
            PopupMenu::Show(menuItemsNumber, destination_menu_items, screen->mLayout, false, y);
    }
}

void PostComposerScreen::SetSelectedFriendsTimeline(const char*id, const char *name) {
    if (id) {
        mIsAudienceChanged = true;
        SetDestinationData(id, name, nullptr, Operation::OD_Friend_Timeline);
        UpdateDestinationSelector();
        UpdatePrivacyField();
        if (mAddPhotoButtonLayout) {
            evas_object_del(mAddPhotoButtonLayout); mAddPhotoButtonLayout = NULL;
        }
    }
}

void PostComposerScreen::SetSelectedGroupTimeline(Group *group) {
    if (group) {
        Log::debug_tag(LogTag, "mToPrivacy = %s", mToPrivacy);

        mIsAudienceChanged = true;
        SetDestinationData(group->GetId(), group->mName, group->GetPrivacyAsStr(), Operation::OD_Group);
        DownloadGroupMembers(group->GetId());
        UpdateDestinationSelector();
        UpdatePrivacyField();

        if (SelectedMediaList::Count() && mMediaBox && !mAddPhotoButtonLayout && !mShareTextPictures) {
            CreateAddPhotoButton(mMediaBox);
        }
    }
}

void PostComposerScreen::LockScreen() {
    if (!mLockScreen) {
        RemoveEntryFocus();
        ScreenBase::LockScreen();
    }
}

void PostComposerScreen::SendingStarted() {
    Log::debug_tag(LogTag, "SENDING STARTED");

    if (!mIsSendingPost) {
        mIsSendingPost = true;
        LockScreen();
        ProgressBarShow();
        PostChanged(ENothing); //just to update Post button
        EnablePrivacyField(false);
        if (mMessageEntry) elm_object_disabled_set(mMessageEntry, EINA_TRUE);
        if (mAlbumNameEntry) elm_object_disabled_set(mAlbumNameEntry, EINA_TRUE);
        if (mAlbumDescriptionEntry) elm_object_disabled_set(mAlbumDescriptionEntry, EINA_TRUE);
        if (mDestinationSelector) {
            elm_object_signal_callback_del(mDestinationSelector, "mouse,clicked,*", "workaround_for_click", on_dest_selector_clicked);
        }
    }
}

void PostComposerScreen::SendingCompleted() {
    Log::debug_tag(LogTag, "SENDING COMPLETED");

    if(mIsSendingPost) {
        mIsSendingPost = false;
        UnlockScreen();
        ProgressBarHide();
        PostChanged(ENothing); //just to update Post button
        EnablePrivacyField(true);
        if (mMessageEntry) elm_object_disabled_set(mMessageEntry, EINA_FALSE);
        if (mAlbumNameEntry) elm_object_disabled_set(mAlbumNameEntry, EINA_FALSE);
        if (mAlbumDescriptionEntry) elm_object_disabled_set(mAlbumDescriptionEntry, EINA_FALSE);
        if (mDestinationSelector) {
            elm_object_signal_callback_add(mDestinationSelector, "mouse,clicked,*", "workaround_for_click", on_dest_selector_clicked, this);
        }
    }
}

void PostComposerScreen::ConfirmCallback(void *data, Evas_Object *obj, const char *emission, const char *source) {
    Log::info(LOG_FACEBOOK_TAGGING, "PostComposerScreen::ConfirmCallback()");
    Eina_List *list = mPrivacyModel ? mPrivacyModel->FindExceptedFriendsWhoAreTagged() : nullptr;
    if (list) {
        ShowConfirmAccessForExceptedToPopup(list);
    } else {
        PostStatus();
    }
    eina_list_free(list);
}

void PostComposerScreen::PostStatus() {
    Log::debug_tag(LogTag, "SEND!!!!!!!!");
    if (mTagging && mTagging->IsLongText(MAX_POST_MESSAGE_LENGTH)) {
        EnableRightButton(true);
        return;
    }
    bundle* paramsBundle = CreatePostBundle();
    bool closeScreen = false;

        CHECK_RET_NRV(mPrivacyModel);
        CHECK_RET_NRV(mPrivacyModel->GetPrivacyOption());
        CHECK_RET_NRV(mPrivacyModel->GetPrivacyOption()->GetType());

        Eina_List* list = nullptr;
        void* data = nullptr;
        std::list<Friend> taggedFriends;
        EINA_LIST_FOREACH(mPrivacyModel->GetTaggedFriends(), list, data) {
            Friend* fr = static_cast<Friend*>(data);
            CHECK_CONT(fr);
            taggedFriends.push_back(*fr);
        }

    switch (mMode) {
    case eEDIT: {
        // Bundle is not required if there are only changes in pics caption or media list content
        if (!IsPostEdited(false, false)) {
            bundle_free(paramsBundle);
            paramsBundle = nullptr;
        }

        Eina_List *photosWithChangedCaption = nullptr;
        Eina_List *listItem;
        void *listData;
        PostComposerMediaData *media;

        EINA_LIST_FOREACH(SelectedMediaList::Get(), listItem, listData) {
            media = static_cast<PostComposerMediaData *>(listData);

            if (media->IsCaptionChanged()) {
                photosWithChangedCaption = eina_list_append(photosWithChangedCaption, media);
            }
        }
        assert(mPostToEdit);
        OperationManager::GetInstance()->AddOperation(MakeSptr<PostUpdateOperation>(PostUpdateOperation::OT_Post_Update,
                                                                                    mPostToEdit->GetId() ? mPostToEdit->GetId() : "",
                                                                                    paramsBundle,
                                                                                    taggedFriends,
                                                                                    mPrivacyModel->GetPrivacyOption()->GetType(),
                                                                                    mPlace,
                                                                                    mPostToEdit->IsCoverPhotoPost() || mPostToEdit->IsProfilePhotoPost() ? mPostToEdit->GetObjectId() : nullptr,
                                                                                    photosWithChangedCaption,
                                                                                    mMediaListToDelete,
                                                                                    mPostToEdit));
        closeScreen = true;
        break;
    }
    case eADD_GROUP_POST:
    case eDEFAULT:
    case eDEFAULTAPPTEXTSHARE:
    case eFRIENDTIMELINE:
    case eADD_EVENT_POST:
        if (!NothingToPost()) {
            closeScreen = true;
            if (SelectedMediaList::Count() || (mMode ==  eDEFAULTAPPTEXTSHARE)) {
                // process photo uploading has been started
                // post will be created when all photos will be uploaded
                SendPostWithPhoto();
                Eina_List *mediaList = SelectedMediaList::Get();
                PostComposerMediaData *tempMediaData = static_cast<PostComposerMediaData *>(eina_list_data_get(mediaList));
                if((tempMediaData && tempMediaData->IsMediaDataAppCtrl() )||(mMode ==  eDEFAULTAPPTEXTSHARE))
                    Application::GetInstance()->GoToBackground();

            } else {
                const char* userId = "me";
                if(mToId) {
                    userId = mToId;
                }
                const char *privacy = mMode == eADD_GROUP_POST ? mToPrivacy : mPrivacyModel->GetPrivacyOption()->GetType();
                OperationManager::GetInstance()->AddOperation(MakeSptr<SimplePostOperation>(Operation::OT_Post_Create,
                                                                                            userId ? userId : "",
                                                                                            paramsBundle,
                                                                                            taggedFriends,
                                                                                            privacy ? privacy : "",
                                                                                            mPlace,
                                                                                            mPostDestinationType));
                Log::debug_tag(LogTag, "REALLY SEND!!!!!!!!");
            }
        } else {
            Log::debug_tag(LogTag, "MESSAGE EMPTY!!!!!!!!");
        }
        break;
    case eSHARE: {
        assert(mSharedPost);
        bundle_add_str(paramsBundle, "id", mSharedPost->GetId());
        if (mToId) {
            bundle_add_str(paramsBundle, "to", mToId);
        }

        closeScreen = true;
        OperationManager::GetInstance()->AddOperation(MakeSptr<SimplePostOperation>(SimplePostOperation::OT_Share_Post_Create,
                                                                                    "",
                                                                                    paramsBundle,
                                                                                    taggedFriends,
                                                                                    mPrivacyModel->GetPrivacyOption()->GetType(),
                                                                                    mPlace,
                                                                                    mPostDestinationType,
                                                                                    mSharedPost));
        break;
    }
    case eSHARE_EVENT: {
        bundle_add_str(paramsBundle, "id", mSharedEvent->GetId());
        if (mToId) {
            bundle_add_str(paramsBundle, "to", mToId);
        }

        closeScreen = true;
        OperationManager::GetInstance()->AddOperation(MakeSptr<SimplePostOperation>(SimplePostOperation::OT_Share_Post_Create,
                                                                                    "",
                                                                                    paramsBundle,
                                                                                    taggedFriends,
                                                                                    mPrivacyModel->GetPrivacyOption()->GetType(),
                                                                                    mPlace,
                                                                                    mPostDestinationType));
        break;
    }
    case eSHARELINKWEB: {
        const char *userId = "me";
        if(mToId) {
            userId = mToId;
        }
        closeScreen = true;
        OperationManager::GetInstance()->AddOperation(MakeSptr<SimplePostOperation>(Operation::OT_Share_WEB_Link,
                                                                                    userId ? userId : "",
                                                                                    paramsBundle,
                                                                                    taggedFriends,
                                                                                    mPrivacyModel->GetPrivacyOption()->GetType(),
                                                                                    mPlace,
                                                                                    mPostDestinationType));
        break;
    }
    case eCREATEALBUMANDNOTIFY:
    case eCREATEALBUM: {
        if (ConnectivityManager::Singleton().IsConnected()) {
            char *originalName = GetEntryText(mAlbumNameEntry);
            if (originalName && strlen(originalName) > 0) {
                const char *albumName = Utils::UTF8TextCutter(originalName, MAX_ALBUM_TITLE_LENGTH, false);
                bundle_add_str(paramsBundle, "name", albumName);
                delete[] albumName;
            } else {
                bundle_add_str(paramsBundle, "name", i18n_get_text("IDS_ALBUMS_UNTITLED_ALBUM"));
            }

            char *description = GetEntryText(mAlbumDescriptionEntry);
            if (description && strlen(description) > 0) {
                bundle_add_str(paramsBundle, "description", description);
            }
            free (originalName);
            free (description);

            if(mMode == eCREATEALBUM) {
                closeScreen = true;
                OperationManager::GetInstance()->AddOperation(MakeSptr<SimplePostOperation>(Operation::OT_Album_Create,
                                                                                            "",
                                                                                            paramsBundle,
                                                                                            taggedFriends,
                                                                                            mPrivacyModel->GetPrivacyOption()->GetType(),
                                                                                            mPlace));
            } else {
                SendingStarted();
                mRequest = FacebookSession::GetInstance()->PostCreateAlbum(paramsBundle, on_create_album_completed, this);
            }
            Log::debug_tag(LogTag, "ALBUM IS CREATED!!!!!!!!");
        } else {
            Utils::ShowToast(getMainLayout(), i18n_get_text("IDS_CREATE_ALBUM_MSG_NO_INTERNET"));
            EnableRightButton(true);
        }
        break;
    }
    case eADDPHOTOSTOALBUM: {
        if (!NothingToPost()) {
            closeScreen = true;
            SendPostWithPhoto();
        } else {
            Log::debug_tag(LogTag, "POST IS EMPTY!");
        }
        break;
    }
        default:
            break;

    }
    if(paramsBundle) {
        bundle_free(paramsBundle);
    }

    if (mPrivacyModel) {
        mPrivacyModel->UpdateDefaultPrivacyOptionOnFB();
    }
    if (closeScreen) {
        if (mNotifMsgToShowAtPopScr) {
            notification_status_message_post(i18n_get_text(mNotifMsgToShowAtPopScr));
        }
        Pop();
    }
}

/**
* @brief Creates bundle with parameters that should be used in Graph API post request
* @return bundle with post parameters
*/
bundle* PostComposerScreen::CreatePostBundle() {

    bundle* paramsBundle;
    paramsBundle = bundle_create();

    if (mTagging) {
        char *textToPost = mTagging->GetStringToPost(false);
        char *escape = Utils::escape(textToPost);
        free(textToPost);
        if (mMode == eEDIT && (mPostToEdit->IsCoverPhotoPost() || mPostToEdit->IsProfilePhotoPost())) {
            bundle_add_str(paramsBundle, "name", escape);
        } else {
            bundle_add_str(paramsBundle, "message", escape);
        }
        free(escape);
        char *originTextToPost = mTagging->GetStringToPost(true);
        bundle_add_str(paramsBundle, "origin_message", originTextToPost);
        free(originTextToPost);
    }

    if (mSharedLink) { //when a link is shared.
        bundle_add_str(paramsBundle, "link", mSharedLink);
    } else if (mSharedGroup) { //when a group is shared.
        std::stringstream link;
        link << "https://www.facebook.com/groups/" << mSharedGroup->GetId() << "/";
        bundle_add_str(paramsBundle, "link", link.str().c_str());
    }

    if (mPlace) {
        bundle_add_str(paramsBundle, "place", mPlace->mId.c_str());
        bundle_add_str(paramsBundle, "place_name", mPlace->mName.c_str());
    } else {
        if (mMode == eEDIT) {
            bundle_add_str(paramsBundle, "place", "0");
        }
    }

    if (IsPrivacyEditable()) {
        const char *privacy = mPrivacyModel->GenPrivacyString();
        const char *privacy_icon = mPrivacyModel->GetPrivacyOption()->GenIconPath(true);
        if(privacy && privacy_icon) {
            bundle_add_str(paramsBundle, "privacy", privacy);
            bundle_add_str(paramsBundle, "privacy_icon", privacy_icon);
        }
        free((char *)privacy);
        delete[] privacy_icon;
    }
    const char* str = GenTaggedList();
    if (str) {
        bundle_add_str(paramsBundle, "tags", str);
        delete[] str;
    } else {
        if (mMode == eEDIT) {
            bundle_add_str(paramsBundle, "tags", "[]");
        }
    }
    return paramsBundle;
}

/**
 * @brief Uploading post with photos/video to FB server
 * @return - true if process has been launched, false in other cases
 */
bool PostComposerScreen::SendPostWithPhoto()
{
    Log::info(LOG_FACEBOOK_TAGGING, "PostComposerScreen::SendPostWithPhoto");

    bool res = true;

    AppEvents::Get().Notify(ePOST_SENDING_PHOTO_EVENT);

    bundle *paramsBundle = CreatePostBundle();
    Operation::OperationType opType = Operation::OT_Photo_Post_Create;
    if (IsVideoMediaPresented()) {
        opType = Operation::OT_Video_Post_Create;
    }

    assert(mPrivacyModel);
    char *privacy = mPrivacyModel->GenPrivacyString();

    const char *userId = "me";
    if ((mMode == eADDPHOTOSTOALBUM) && mAlbumToAddPhotos && mAlbumToAddPhotos->GetId()) {
        userId = mAlbumToAddPhotos->GetId();
        opType = Operation::OT_Album_Add_New_Photos;
        mPostDestinationType = Operation::OD_Home;
        bundle_add_str(paramsBundle, "album_name", mAlbumToAddPhotos->mName);
    } else if (mToId) {
        userId = mToId;
    }

    char *textToPost = mTagging ? mTagging->GetStringToPost(false) : nullptr;
    if (opType == Operation::OT_Video_Post_Create) {
        bundle_add_str(paramsBundle, "description", textToPost);
    }

    Eina_List* list = nullptr;
    void* data = nullptr;
    std::list<Friend> taggedFriends;
    EINA_LIST_FOREACH(mPrivacyModel->GetTaggedFriends(), list, data) {
        Friend* fr = static_cast<Friend*>(data);
        CHECK_CONT(fr);
        taggedFriends.push_back(*fr);
    }

    const char *privacyType = mMode == eADD_GROUP_POST ? mToPrivacy : mPrivacyModel->GetPrivacyOption()->GetType();

    OperationManager::GetInstance()->AddOperation(MakeSptr<PhotoPostOperation>(opType,
                                                                           userId ? userId : "",
                                                                           paramsBundle,
                                                                           SelectedMediaList::Get(),
                                                                           textToPost,
                                                                           privacy,
                                                                           taggedFriends,
                                                                           privacyType ? privacyType : "",
                                                                           mPlace,
                                                                           mPostDestinationType));

    free(textToPost);
    free(privacy);
    bundle_free(paramsBundle);

    return res;
}

bool PostComposerScreen::IsVideoMediaPresented() {
    bool isVideoMediaPresented = false;

    Eina_List *list;
    void *listData;
    PostComposerMediaData *mediaData;
    EINA_LIST_FOREACH(SelectedMediaList::Get(), list, listData) {
        mediaData = static_cast<PostComposerMediaData *>(listData);
        if (mediaData->GetType() == MEDIA_CONTENT_TYPE_VIDEO) {
            isVideoMediaPresented = true;
            break;
        }
    }

    return isVideoMediaPresented;
}

void PostComposerScreen::SetDestinationData(const char *id, const char *name, const char *privacy, Operation::OperationDestination destination) {
    SetToId(id);
    SetToName(name);
    SetToPrivacy(privacy);
    mPostDestinationType = destination;
}

void PostComposerScreen::DownloadGroupMembers(const char *id) {
    delete mGroupMembersUploader;
    mGroupMembersUploader = new DataUploader(GroupFriendsProvider::GetInstance(), on_get_group_members_completed, this);
    GroupFriendsProvider::GetInstance()->SetGroupId(id);
    mGroupMembersUploader->StartUploading();
}

void PostComposerScreen::on_get_group_members_completed(void* data) {
    PostComposerScreen *me = static_cast<PostComposerScreen *> (data);
    delete me->mGroupMembersUploader;
    me->mGroupMembersUploader = NULL;
    me->UpdateGroupMembersList();
}

void PostComposerScreen::UpdateGroupMembersList() {
    void *listData;
    EINA_LIST_FREE(mGroupMembers, listData) {
        (static_cast<Friend *> (listData))->Release();
    }
    Eina_Array* friends = GroupFriendsProvider::GetInstance()->GetData();
    for (int i = 0; i < eina_array_count(friends); ++i){
        Friend *member = static_cast<Friend*> (eina_array_data_get(friends, i));
        if (member) {
            if (Utils::IsMe(member->GetId())) {
                member->AddRef();
                mGroupMembers = eina_list_append(mGroupMembers, member);
            } else {
                Eina_List *l;
                void *itemData;
                EINA_LIST_FOREACH(mPrivacyModel->GetFriendsList(), l, itemData) {
                    Friend *fr = static_cast<Friend*> (itemData);
                    if (*fr == *member) {
                        fr->AddRef();
                        mGroupMembers = eina_list_append(mGroupMembers, fr);
                    }
                }
            }
        }
    }
    mGroupMembers = eina_list_sort(mGroupMembers, 0, PCPrivacyModel::sort_cb);
    if (mSelectFriendsScreen) {
        Eina_List *friendInfos = GetFriendInfos();
        mSelectFriendsScreen->SetFriendInfos(friendInfos);
        friendInfos = eina_list_free(friendInfos);
    }
}

void PostComposerScreen::SetEntryText(char *text) {
    if (mMessageEntry) {
        //Insert multi-line text shared by another application:
        char *markup_string = elm_entry_utf8_to_markup(text);
        elm_entry_entry_set(mMessageEntry, markup_string);
        free(markup_string);
    }
}

/**
 * @brief Construction
 * @param mode - the mode on which screen will work
 * @param id - the id of action object
 */
PostComposerScreen::PostComposerScreen(ScreenBase *screen, PostComposerMode mode, GraphObject *obj, Event *event, const char * id, Album* album , bool isDestinationSelectorEnabled, char * notifMsgToShowAtPopScr) :
        ScreenBase(NULL), mScreen(screen), mMode(mode) {
    InitData();
    mAlbumToAddPhotos = album;
    if (mAlbumToAddPhotos) {
        mAlbumToAddPhotos->AddRef();
    }

//this is a fake mode to fix the issue, probably we should remove it later. But now it's used to minimize the fix.
    if (mMode == eFRIENDTIMELINE) {
        mPostDestinationType = Operation::OD_Friend_Timeline;
        SetToId(id);
    } else if (mMode == eSHARELINKWEB) {
        mSharedLink = Utils::getCopy(id);
    } else {
        mId = SAFE_STRDUP(id);
    }
    mIsDestinationSelectorEnabled = isDestinationSelectorEnabled;

    if (mMode == eSHAREDAPPCONTROL) {
        mshare_appcontrolpath = id;
    }

    mNotifMsgToShowAtPopScr = SAFE_STRDUP(notifMsgToShowAtPopScr);
    if (mMode == eSHAREGROUP) {
        mSharedGroup = dynamic_cast<Group *> (obj);
        assert(mSharedGroup);
        mMode = eSHARELINKWEB; //Group is actually sared as link to the group page.
        mSharedGroup->AddRef();
        Init(nullptr, event);
    } else {
        Post *post = dynamic_cast<Post *> (obj);
        Init(post, event);
    }
}

PostComposerScreen::PostComposerScreen(ScreenBase *screen, PostComposerMode mode, ActionAndData *action_data,
        Operation::OperationDestination opDest, const char * targetId, const char * targetName, const char * targetPrivacy) :
        ScreenBase(NULL),
        mScreen(screen),
        mMode(mode) {
    InitData();
    SetToId(targetId);
    SetToName(targetName);
    SetToPrivacy(targetPrivacy);
    mPostDestinationType = opDest;

    mParentActionNData = action_data;
    Post *post = static_cast<Post*>(action_data->mData);

    Init(post, NULL);
}

PostComposerScreen::PostComposerScreen(ScreenBase *screen, PostComposerMode mode, const char * targetId, const char * targetName, const char * targetPrivacy) :
        ScreenBase(NULL), mScreen(screen), mMode(mode)
{
    InitData();

    switch (mMode) {
    case eFRIENDTIMELINE:
        SetDestinationData(targetId, targetName, targetPrivacy, Operation::OD_Friend_Timeline);
        break;
    case eADD_GROUP_POST:
        SetDestinationData(targetId, targetName, targetPrivacy, Operation::OD_Group);
        break;
    default:
        assert(false);
        break;
    }

    Init(nullptr, nullptr);
}

/**
 * @brief Destruction
 */
PostComposerScreen::~PostComposerScreen() {
    PopupContextTip::Close();

    void *list_data;
    if (mImageDetailsRequests) {
        EINA_LIST_FREE(mImageDetailsRequests, list_data) {
            GraphRequest *request = (GraphRequest*) list_data;
            FacebookSession::GetInstance()->ReleaseGraphRequest(request);
        }
    }
    if (mTextLayout) {
        RemoveText();
    }

    delete mConfirmationDialog;
    delete mSelectAlbumScreen;
    elm_object_signal_callback_del(mPrivacyLayout, "clicked", "post_composer_privacy", on_to_selector_clicked);
    if (mScrollerGesture) {
        elm_gesture_layer_cb_set(mScrollerGesture, ELM_GESTURE_N_TAPS, ELM_GESTURE_STATE_END, NULL, NULL);
    }
    delete mActionAndData;
    free((void *)mId);
    free((void *)mNotifMsgToShowAtPopScr);
    FacebookSession::GetInstance()->ReleaseGraphRequest(mRequest);
    FacebookSession::GetInstance()->ReleaseGraphRequest(mReqLinkPreview);

    free(mToId); mToId = NULL;
    free(mToName); mToName = NULL;
    free(mToPrivacy); mToPrivacy = NULL;

    if (NULL != mDestAllow) {
        void * listData;
        EINA_LIST_FREE(mDestAllow, listData) {
            free(listData);
        }
    }

    if (NULL != mDestDeny) {
        void * listData;
        EINA_LIST_FREE(mDestDeny, listData) {
            free(listData);
        }
    }

    void *listData;
    EINA_LIST_FREE(mGroupMembers, listData) {
        (static_cast<Friend *> (listData))->Release();
    }

    if (mPrivacyModel) {
        mPrivacyModel->RemoveObserver(this);
        delete mPrivacyModel;
    }

    if (mMode != eCREATEALBUMANDNOTIFY) {
        SelectedMediaList::Clear();
    }
    delete mPlace;
    Log::debug_tag(LogTag, "Update subscribers!!!!!!!!");
    AppEvents::Get().Notify(eVIDEOPLAYER_RESUME_EVENT);
    delete mSuggestedFriendsUploader;
    delete mGroupMembersUploader;
    if (mSelectFriendsScreen) {
        mSelectFriendsScreen->UnregisterClient();
    }
    evas_object_del(mToSelectorBox); mToSelectorBox = NULL;
    evas_object_del(mPrivacyLayout); mPrivacyLayout = NULL;

    EINA_LIST_FREE(mDiscardedLinks, listData) {
        delete[] static_cast<char *>(listData);
    }
    delete[] mSharedLink;

    if (mSharedGroup) {
        mSharedGroup->Release();
    }

    if (mAlbumToAddPhotos) {
        mAlbumToAddPhotos->Release();
    }

    if (mGenTextUpdateTimer) {
        ecore_timer_del(mGenTextUpdateTimer);
    }

    EINA_LIST_FREE(mMediaListToDelete, listData) {
        (static_cast<PostComposerMediaData *>(listData))->Release();
    }

    delete mPhotoWidget;

    AppEvents::Get().UnsubscribeFromAll(this);
}

void PostComposerScreen::InitData() {
    if (eCREATEALBUMANDNOTIFY != mMode) {  //otherwise creation of a new Album deletes all results of crop/rotate operations
        DeletePostComposerTempDirectory();
    }
    mScreenId = ScreenBase::SID_POST_COMPOSER_SCREEN;
    mSelectAlbumScreen = NULL;
    mId = NULL;
    mRequest = NULL;
    mMediaBox = NULL;
    mSharedItemBox = NULL;
    mMessageEntry = NULL;
    mAlbumNameEntry = NULL;
    mAlbumDescriptionEntry = NULL;
    mIsSendingPost = false;
    mDestinationSelector = NULL;
    mAlbumToAddPhotos = NULL;
    mPlace = NULL;
    mLockScreen = NULL;
    mSelectAlbumPopup = NULL;
    mSharedPost = nullptr;
    mPostToEdit = nullptr;
    mSharedEvent = NULL;

    mPrivacyModel = NULL;

    mToId = NULL;
    mToName = NULL;
    mToPrivacy = NULL;
    mPostDestinationType = Operation::OD_Home;
    mShareWithScreen = NULL;
    mSelectFriendsScreen = NULL;
    mDestPrivacy = FRIENDS_OF_FRIENDS;
    mDestAllow = NULL;
    mDestDeny = NULL;
    mIsVideoMedia = false;
    mAlbumButtonMode = EHidden;
    mActionAndData = NULL;
    mHeader = NULL;
    mParentActionNData = NULL;

    mAddPhotoButtonLayout = NULL;
    mScrollerGesture = NULL;
    mSuggestedFriendsUploader = NULL;
    mGroupMembersUploader = NULL;

    mToSelectorBox = NULL;
    mPrivacyLayout = NULL;
    mPrivacyField = nullptr;
    mConfirmationDialog = NULL;
    mScrollerBox = nullptr;
    mTextLayout = nullptr;
    mSharedLink = nullptr;
    mSharedGroup = nullptr;
    mPreviewWidgetLayout = nullptr;
    mIsPasteRequested = false;
    mDiscardedLinks = NULL;
    mNotifMsgToShowAtPopScr = NULL;

    mTagging = nullptr;
    mGroupMembers = NULL;
    mReqLinkPreview = NULL;

    if (mMode == eSHARETEXTPICTURES) {
        mShareTextPictures = true;
        mMode = eDEFAULT;
    } else {
        mShareTextPictures = false;
    }
    mGenTextLayout = nullptr;
    mGenTextUpdateTimer = nullptr;

    mIsAudienceChanged = false;
    mImageDetailsRequests = NULL;

    mIsDestinationSelectorEnabled = false;
    mPhotoWidget = NULL;
    mMediaListToDelete = NULL;

    Application::GetInstance()->SetScreenOrientationPortrait();
}

Privacy *PostComposerScreen::GetEditedPostPrivacy() {
    if (mPostToEdit) {
        return mPostToEdit->GetPrivacy();
    } else {
        return nullptr;
    }
}

/**
 * @brief Init screen UI
 */
void PostComposerScreen::Init(Post *post, Event *event) {
    if (mMode == eSHARE) {
        if (post) {
            mSharedPost = !post->GetIsGroupPost() && post->GetParent() ? post->GetParent() : post;
        }
    } else {
        mPostToEdit = post;
    }

    if (mMode == eSHARE_EVENT) {
        if (event) {
            mSharedEvent = event;
        }
    }

    mPrivacyModel = new PCPrivacyModel;
    mPrivacyModel->AddObserver(this);
    if (mMode == eEDIT) {
        mPrivacyModel->SetTaggedFriendsFromPost(mPostToEdit);
        if (mPostToEdit && mPostToEdit->GetPlace()) {
            mPlace = new PCPlace(*(mPostToEdit->GetPlace()));
        }
    }

    mLayout = elm_layout_add(getParentMainLayout());
    evas_object_size_hint_weight_set(mLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    elm_layout_file_set(mLayout, Application::mEdjPath, "post_composer_screen");

    evas_object_show(mLayout);

    CreateHeader();
    UpdateDestinationSelector();
    CreateFooterButtons();

    // scroller
    mScroller = elm_scroller_add(mLayout);
#ifdef _SCROLLER_WORKAROUND_ENABLED_
    mScrollPosX = 0;
    mScrollPosY = 0;
    mScrollWidth = 0;
    mScrollHeight = 0;
#endif
    elm_object_part_content_set(mLayout, "composer_scroller", mScroller);
    elm_scroller_policy_set(mScroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_OFF);
    elm_scroller_single_direction_set(mScroller, ELM_SCROLLER_SINGLE_DIRECTION_HARD);

    mScrollerGesture = elm_gesture_layer_add(mLayout);
    elm_gesture_layer_attach(mScrollerGesture, mScroller);
    elm_gesture_layer_cb_set(mScrollerGesture, ELM_GESTURE_N_TAPS, ELM_GESTURE_STATE_END, show_input_text_panel, this);

    elm_object_scroll_lock_x_set(mScroller, EINA_TRUE);

    elm_scroller_movement_block_set(mScroller, ELM_SCROLLER_MOVEMENT_BLOCK_HORIZONTAL);

    // privacy and text
    mScrollerBox = elm_box_add(mScroller);
    evas_object_size_hint_weight_set(mScrollerBox, 0, 0);
    evas_object_size_hint_align_set(mScrollerBox, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_object_content_set(mScroller, mScrollerBox);
    evas_object_show(mScroller);

    if (IsPrivacyFieldRequired(post)) {
        CreatePrivacy(mScrollerBox);
        ShowPrivacyIsLoading();
    }

    ManageText();

    if (mMode == eCREATEALBUM || mMode == eCREATEALBUMANDNOTIFY) {
        CreateCreateAlbumContent(mScrollerBox);
    }

    // media list
    switch (mMode) {
    case eADD_GROUP_POST:
    case eDEFAULT:
    case eDEFAULTAPPTEXTSHARE:
    case eFRIENDTIMELINE:
    case eADD_EVENT_POST: {
            mMediaBox = WidgetFactory::CreateWrapperByName(mScrollerBox, "post_composer_wrapper");
            CreateSelectedMedia();
        }
        break;
    case eEDIT:
        if (mPostToEdit) {
            if (!mPostToEdit->GetIsSharedPost()) {
                if (mPostToEdit->GetPhotoList() && !(mPostToEdit->GetPlace() && !mPostToEdit->GetFullPicture())) { //post with photos; "GetPlace() && !GetFullPicture()" means post with place and without photos
                    RequestImageData();
                    // Here is SelectedMediaList gets populated.
                    PrepareEditContent();
                }
            } else if (mPostToEdit->GetParent()) {  //all shared posts (including those with web-links or group links)
                mSharedPost = mPostToEdit->GetParent();
                Evas_Object *sharedBox = WidgetFactory::CreateWrapperByName(mScrollerBox, "post_composer_wrapper");
                CreateSharedItemContent(sharedBox);
            } else {
                bool isSharedLink = mPostToEdit->IsSharedLink() && mPostToEdit->GetLink();
                bool isSharedGroup = mPostToEdit->IsSharedGroup();
                if (isSharedLink || isSharedGroup) {
                    PreviewWidgetData lp;
                    if (isSharedLink) {
                        PostPresenter presenter(*mPostToEdit);
                        mSharedLink = Utils::getCopy(mPostToEdit->GetLink());
                        lp.SetName(mPostToEdit->GetAttachmentTitle().c_str());
                        lp.SetCaption(mPostToEdit->GetCaption().c_str());
                        lp.SetDescription(mPostToEdit->GetAttachmentDescription().c_str());
                        lp.SetImage(mPostToEdit->GetAttachmentSrc());
                    } else {
                        lp.SetName(mPostToEdit->GetAttachmentTitle().c_str());
                        lp.SetCaption(mPostToEdit->GetCaption().c_str());
                        lp.SetDescription(mPostToEdit->GetAttachmentDescription().c_str());
                    }
                    ShowPreviewWidget(EPreview, &lp);
                }
            }
        }
        break;
    case eSHARE: {
//        mSharedItemBox = WidgetFactory::CreateWrapperByName(scrollerBox, "post_composer_wrapper");
//        Evas_Object * sharedLayout = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(scrollerBox, "post_shared_item");
            Evas_Object * sharedBox = WidgetFactory::CreateWrapperByName(mScrollerBox, "post_composer_wrapper");
            CreateSharedItemContent(sharedBox);
        }
        break;
    case eSHARE_EVENT: {
            if (mSharedEvent) {
                Evas_Object * sharedBox = WidgetFactory::CreateWrapperByName(mScrollerBox, "post_composer_wrapper");
                CreateSharedEventItemContent(sharedBox);
            }
        }
        break;
    case eSHARELINKWEB: {
        if (mSharedGroup) {
            PreviewWidgetData lp;
            lp.SetName(mSharedGroup->mName);
            lp.SetCaption("");
            if (mSharedGroup->mMembersCount) {
                char *members = ProfileWidgetFactory::gen_members_number_text(mSharedGroup->mMembersCount);
                lp.SetDescription(members);
                free(members);
            }

            ShowPreviewWidget(EPreview, &lp);
        } else {
            ShowPreviewWidget(EProgress, nullptr);
            if (!mReqLinkPreview) {
                mReqLinkPreview = FacebookSession::GetInstance()->PostLinkPreview(mSharedLink, on_link_preview_completed, this);
            }
        }
        }
        break;
    case eADDPHOTOSTOALBUM: {
            mMediaBox = WidgetFactory::CreateWrapperByName(mScrollerBox, "post_composer_wrapper");
            SetPhotoAlbumButtonMode(EActive);
            CreateSelectedMedia();
        }
        break;
    default:
        break;
    }

    //AAA Now we do nothing with suggested friends in the Post Composer. This is just needed to speedup suggested friends downloading for other screens which use them.
    mSuggestedFriendsUploader = new DataUploader(SuggestedFriendsProvider::GetInstance(), on_get_suggested_friends_completed, this);
    mSuggestedFriendsUploader->StartUploading();

    //Set focus for entry
    if (mMessageEntry) {
        if (!SelectedMediaList::Count()) {
            elm_object_focus_set(mMessageEntry, EINA_TRUE);
            elm_entry_cnp_mode_set(mMessageEntry, ELM_CNP_MODE_PLAINTEXT);
        }
    } else if (mAlbumNameEntry) {
        Utils::SetKeyboardFocus(mAlbumNameEntry, EINA_TRUE);
    }

    if (mMode == eADD_GROUP_POST || IsEditGroupPost()) {
        DownloadGroupMembers(mToId);
    }

    AppEvents::Get().Subscribe(eRESUME_EVENT, this);
#ifdef _SCROLLER_WORKAROUND_ENABLED_
    AppEvents::Get().Subscribe(eFOCUSED_EVENT, this);
    AppEvents::Get().Subscribe(eUNFOCUSED_EVENT, this);
#endif

}

//Todo: Simplify logic of this function. It's too complex.
bool PostComposerScreen::IsPrivacyFieldRequired(Post *post) {
    return ((post && post->GetPrivacy() && post->GetPrivacy()->GetDescription() && !post->IsPhotoAddedToAlbum() &&
        !post->IsCoverPhotoPost() && !post->IsProfilePhotoPost()) ||
        mMode == eDEFAULT || mMode == eCREATEALBUM || mMode == eCREATEALBUMANDNOTIFY || mMode == eADDPHOTOSTOALBUM || mMode == eFRIENDTIMELINE ||
        mMode == eSHARE || mMode == eSHARELINKWEB || mPostDestinationType == Operation::OD_Event || mPostDestinationType == Operation::OD_Group);
}

bool PostComposerScreen::IsPrivacyEditable() {
    return (mPrivacyLayout && !mToId &&
            ((!mPostToEdit && mMode != eADDPHOTOSTOALBUM) || //new post and it's not eADDPHOTOSTOALBUM mode
            (mPostToEdit  && !mPostToEdit->IsCoverPhotoPost() && !mPostToEdit->IsProfilePhotoPost()))); //edited post
}

Evas_Object *PostComposerScreen::CreateSharedItemContent(Evas_Object *parent) {
    assert(mSharedPost);

    Evas_Object *sharedLayout = elm_layout_add(parent);
    evas_object_size_hint_weight_set(sharedLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(sharedLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_layout_file_set(sharedLayout, Application::mEdjPath, "post_shared_item");

    Evas_Object *img = elm_image_add(sharedLayout);
    elm_object_part_content_set(sharedLayout, "shared_image", img);

    Post *parentPost = mSharedPost;
    const char *imagePath = nullptr;
    while(parentPost) {
        Photo *photo = static_cast<Photo *> (eina_list_nth(parentPost->GetPhotoList(), 0));
        if (photo && photo->GetFilePath()) {
            imagePath = photo->GetFilePath();
         } else if (photo && photo->GetSrcUrl()) {
            //request Icon
            if (!mActionAndData) {
                mActionAndData = new ActionAndData(nullptr, nullptr);
            }
            mActionAndData->UpdateImageLayoutAsync(photo->GetSrcUrl(), img);
            break;
        } else if (parentPost->GetFullPicturePath()) {
            imagePath = parentPost->GetFullPicturePath();
        } else if (parentPost->GetFromPicturePath()) {
            imagePath = parentPost->GetFromPicturePath();
        } else if (parentPost->GetFromPictureLink()) {
            //request Icon
            if (!mActionAndData) {
                mActionAndData = new ActionAndData(parentPost, nullptr);
            }
            mActionAndData->UpdateImageLayoutAsync(parentPost->GetFromPictureLink(), img, ActionAndData::EImage, Post::EFromPicturePath);
            break;
        }

        if (imagePath) {
            break;
        }
        parentPost = parentPost->GetParent();
    }
    if (imagePath) {
        Log::debug_tag(LogTag, "IMAGE:%s", imagePath);
        elm_image_file_set(img, imagePath, NULL);
    }

    std::string text(mSharedPost->GetFromName());
    if (!text.empty()) elm_object_part_text_set(sharedLayout, "shared_text_author", text.c_str());

    text = mSharedPost->GetStory();
    if (!text.empty()) elm_object_part_text_set(sharedLayout, "shared_text_story", text.c_str());

    if (mSharedPost->GetMessage()) {
       text = mSharedPost->GetMessage();
    } else {
       text = mSharedPost->GetDescription() ? mSharedPost->GetDescription() : "";
    }

    if (!text.empty()) elm_object_part_text_set(sharedLayout, "shared_text_message", text.c_str());

    elm_box_pack_end(parent, sharedLayout);
    evas_object_show(sharedLayout);
    return sharedLayout;
}

Evas_Object *PostComposerScreen::CreateSharedEventItemContent(Evas_Object *parent)
{
    Evas_Object *sharedLayout = elm_layout_add(parent);
    evas_object_size_hint_weight_set(sharedLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(sharedLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_layout_file_set(sharedLayout, Application::mEdjPath, "post_shared_item");

    Evas_Object *img = elm_image_add(sharedLayout);
    elm_object_part_content_set(sharedLayout, "shared_image", img);
    if (mSharedEvent->mCover) {
        if (mSharedEvent->mCover->mPhotoPath) {
            elm_image_file_set(img, mSharedEvent->mCover->mPhotoPath, NULL);
        } else if (mSharedEvent->mCover->mSource) {
            elm_image_file_set(img, mSharedEvent->mCover->mSource, NULL);
        }
    } else {
        if (mSharedEvent->mOwner && mSharedEvent->mOwner->GetGraphObjectType() == GraphObject::GOT_USER) {
            User *user = static_cast<User*>(mSharedEvent->mOwner);
            if (user->mPicture && user->mPicture->mUrl) {
                elm_image_file_set(img, user->mPicture->mUrl, NULL);
            } else {
                elm_image_file_set(img, ICON_DIR"/profiles/empty_profile_image.png", NULL);
            }
        } else {
            elm_image_file_set(img, ICON_DIR"/profiles/empty_profile_image.png", NULL);
        }
    }
    evas_object_show(img);

    elm_object_part_text_set(sharedLayout, "shared_text_author", mSharedEvent->mName);

    elm_box_pack_end(parent, sharedLayout);
    evas_object_show(sharedLayout);
    return sharedLayout;
}

void PostComposerScreen::PrepareEditContent() {
    assert(mPostToEdit);

    Eina_List *listItem;
    void *listData;
    EINA_LIST_FOREACH(mPostToEdit->GetPhotoList(), listItem, listData) {
        Photo *photo = static_cast<Photo *>(listData);
        Log::debug_tag(LogTag, "IMAGE:%s", photo->GetFilePath());

        bool isVideoPost = !strcmp(mPostToEdit->GetStatusType(), "added_video");
        const char *path = isVideoPost ? mPostToEdit->GetSource() : photo->GetFilePath();

        PostComposerMediaData *mediaData = new PostComposerMediaData(
                SAFE_STRDUP(path),
                SAFE_STRDUP(photo->GetFilePath()),
                isVideoPost ? MEDIA_CONTENT_TYPE_VIDEO : MEDIA_CONTENT_TYPE_IMAGE,
                SAFE_STRDUP(photo->GetId()),
                0,
                false,
                SAFE_STRDUP(photo->GetSrcUrl()));

        mediaData->SetWidth(photo->GetWidth());
        mediaData->SetHeight(photo->GetHeight());
        mediaData->SetCaption(photo->GetName());
        mediaData->SetOriginalCaption(photo->GetName());

        SelectedMediaList::AddItem(mediaData);
    }
}

void PostComposerScreen::ShowEditContent(Eina_List *mediaDataList) {
    // Remove all existing items from MediaBox
    DeleteMediaList();
    // Remove MediaBox and its wrapper layout
    Evas_Object *mediaBoxWrapperLayout = elm_object_parent_widget_get(mMediaBox);
    elm_box_unpack(mScrollerBox, mediaBoxWrapperLayout);
    evas_object_del(mediaBoxWrapperLayout);

    unsigned int photosCount = eina_list_count(mediaDataList);
    if (photosCount == 0) {
        return;
    }

    if (photosCount > 1) {
        // Show clickable tiles for 2 and more pics
        mMediaBox = WidgetFactory::CreateWrapperByName(mScrollerBox, "edit_post_composer_wrapper");

        int layoutDimension = MIN(photosCount, MAX_PHOTOS_IN_POST_LAYOUT);
        Evas_Object *gridLayout = elm_layout_add(mMediaBox);
        evas_object_size_hint_weight_set(gridLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
        evas_object_size_hint_align_set(gridLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);

        char edjeGroup[25];
        Utils::Snprintf_s(edjeGroup, 25, "edit_photos_%d", layoutDimension);
        elm_layout_file_set(gridLayout, Application::mEdjPath, edjeGroup);

        Evas_Object *gestureLayer = elm_gesture_layer_add(mLayout);
        elm_gesture_layer_attach(gestureLayer, gridLayout);
        elm_gesture_layer_cb_set(gestureLayer, ELM_GESTURE_N_TAPS, ELM_GESTURE_STATE_END, on_image_clicked_cb, this);

        int swallowW = 0;
        int swallowH = 0;

        Eina_List * listItem;
        void * listData;
        int i = 0;
        EINA_LIST_FOREACH(mediaDataList, listItem, listData) {
            PostComposerMediaData *mediaData = static_cast<PostComposerMediaData *>(listData);

            if (i < MAX_PHOTOS_IN_POST_LAYOUT) {
                int imgWidth = mediaData->GetWidth();
                int imgHeight = mediaData->GetHeight();

                Evas_Object *img = evas_object_image_add(evas_object_evas_get(gridLayout));
                evas_object_size_hint_weight_set(gridLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
                evas_object_size_hint_align_set(gridLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);
                evas_object_show(img);

                char part[32];
                Utils::Snprintf_s(part, 32, "swallow.photo_%d", i);
                elm_object_part_content_set(gridLayout, part, img);

                if (edje_object_part_geometry_get(elm_layout_edje_get(gridLayout), part, NULL, NULL, &swallowW, &swallowH)) {
                    Utils::CropImageToFill(img, swallowW, swallowH, imgWidth, imgHeight);
                }
                evas_object_image_file_set(img, mediaData->GetFilePath(), NULL);

                if (i == MAX_PHOTOS_IN_POST_LAYOUT - 1 && photosCount > MAX_PHOTOS_IN_POST_LAYOUT) {
                    char *textExtraNum = Utils::GetRemaningPicturesNumber(photosCount);
                    if (textExtraNum) {
                        elm_object_signal_emit(gridLayout, "show_black_rect", "");
                        elm_object_part_text_set(gridLayout, "edit_text", textExtraNum);
                    }
                    delete[] textExtraNum;
                }
                i++;
            }
        }

        elm_box_pack_end(mMediaBox, gridLayout);
        evas_object_show(gridLayout);
    } else if (photosCount == 1) {
        // Show full size pic with option to remove it
        mMediaBox = WidgetFactory::CreateWrapperByName(mScrollerBox, "post_composer_wrapper");
        mPhotoWidget = new ComposerPhotoWidget(ePOST_EDIT, mLayout, mMediaBox, this);
    }
}

Evas_Object *PostComposerScreen::CreateCreateAlbumContent(Evas_Object *parent) {
    Evas_Object *createAlbumLayout = elm_layout_add(parent);
    elm_layout_file_set(createAlbumLayout, Application::mEdjPath, "post_create_album");

//mAlbumNameEntry
    mAlbumNameEntry = elm_entry_add(createAlbumLayout);
    elm_layout_content_set(createAlbumLayout, "swallow.name", mAlbumNameEntry);
    elm_entry_prediction_allow_set(mAlbumNameEntry, EINA_FALSE);
    elm_entry_single_line_set(mAlbumNameEntry, EINA_TRUE);
    elm_entry_input_panel_return_key_type_set(mAlbumNameEntry, ELM_INPUT_PANEL_RETURN_KEY_TYPE_DONE);
    elm_entry_scrollable_set(mAlbumNameEntry, EINA_TRUE);
    elm_entry_cnp_mode_set(mAlbumNameEntry, ELM_CNP_MODE_PLAINTEXT);
    evas_object_event_callback_add(mAlbumNameEntry, EVAS_CALLBACK_KEY_DOWN, album_name_key_down_cb, this);

    char *buf = WidgetFactory::WrapByFormat2(CREATE_ALBUM_TEXT_FORMAT, R->POST_COMPOSER_CREATE_ALBUM_TEXT_FONT_SIZE,
            i18n_get_text("IDS_ALBUMS_UNTITLED_ALBUM"));
    if (buf) {
        elm_object_part_text_set(mAlbumNameEntry, "elm.guide", buf);
        delete[] buf;
    }
    Utils::SetKeyboardFocus(mAlbumNameEntry, EINA_FALSE);
    evas_object_show(mAlbumNameEntry);

//mAlbumDescriptionEntry
    mAlbumDescriptionEntry = elm_entry_add(createAlbumLayout);
    elm_layout_content_set(createAlbumLayout, "swallow.description", mAlbumDescriptionEntry);
    elm_entry_prediction_allow_set(mAlbumDescriptionEntry, EINA_FALSE);
    elm_entry_single_line_set(mAlbumDescriptionEntry, EINA_FALSE);
    elm_entry_scrollable_set(mAlbumDescriptionEntry, EINA_FALSE);
    elm_entry_cnp_mode_set(mAlbumDescriptionEntry, ELM_CNP_MODE_PLAINTEXT);
    buf = WidgetFactory::WrapByFormat2(CREATE_ALBUM_TEXT_FORMAT, R->POST_COMPOSER_CREATE_ALBUM_TEXT_FONT_SIZE,
            i18n_get_text("IDS_ALBUMS_ADD_DESCRIPTION"));
    if (buf) {
        elm_object_part_text_set(mAlbumDescriptionEntry, "elm.guide", buf);
        delete[] buf;
    }
    evas_object_show(mAlbumDescriptionEntry);

    char *entryStyle = WidgetFactory::WrapByFormat(SANS_REGULAR_BLACK_STYLE, R->POST_COMPOSER_CREATE_ALBUM_TEXT_FONT_SIZE);
    if (entryStyle) {
        elm_entry_text_style_user_push(mAlbumNameEntry, entryStyle);
        elm_entry_text_style_user_push(mAlbumDescriptionEntry, entryStyle);
        delete[] entryStyle;
    }

    elm_box_pack_end(parent, createAlbumLayout);
    evas_object_show(createAlbumLayout);
    return createAlbumLayout;
}

void PostComposerScreen::CreateHeader() {
    if(mHeader) {
        evas_object_hide(mHeader);
        evas_object_del(mHeader);
    }

    switch (mMode) {
    case eEDIT:
        mHeader = WidgetFactory::CreateGrayHeaderWithLogo(mLayout, mPostToEdit && !mPostToEdit->GetIsSharedPost() ? "IDS_EDIT_POST" : "IDS_SHARE_TO_FACEBOOK", "IDS_SAVE", this);
        break;
    case eCREATEALBUM:
    case eCREATEALBUMANDNOTIFY:
        mHeader = WidgetFactory::CreateGrayHeaderWithLogo(mLayout, "IDS_ALBUMS_CREATE_ALBUM", "IDS_ALBUMS_CREATE", this);
        break;
    case eADDPHOTOSTOALBUM:
        mHeader = WidgetFactory::CreateGrayHeaderWithLogo(mLayout, "IDS_POST_TO_FACEBOOK", "IDS_POST", this);
        break;
    case eADD_GROUP_POST: {
        char *caption = WidgetFactory::WrapByFormat(i18n_get_text("IDS_POST_TO_GROUP"), mToName);
        mHeader = WidgetFactory::CreateGrayHeaderWithGroupLogo(mLayout, caption, "IDS_POST", this);
        delete[] caption;
    }
        break;
    case eFRIENDTIMELINE: {
        char *caption = WidgetFactory::WrapByFormat(i18n_get_text("IDS_ON_TIMELINE"), mToName);
        mHeader = WidgetFactory::CreateGrayHeaderWithGroupLogo(mLayout, caption, "IDS_POST", this);
        delete[] caption;
    }
        break;
    case eADD_EVENT_POST:
        mHeader = WidgetFactory::CreateGrayHeaderWithGroupLogo(mLayout, mToName, "IDS_POST", this);
        break;
    default: {
        if(mIsDestinationSelectorEnabled) {
            CreateDestinationSelector();
            mHeader = WidgetFactory::CreateGrayHeaderWithLogo(mLayout, mDestinationSelector, "IDS_POST", this);
            evas_object_show(mHeader);
        } else {
            mHeader = WidgetFactory::CreateGrayHeaderWithLogo(mLayout, "IDS_POST_TO_FACEBOOK", "IDS_POST", this);
        }
    }
        break;
    }

    elm_object_part_content_set(mLayout, "swallow.top_toolbar", mHeader);
}

void PostComposerScreen::CreateDestinationSelector() {
    //post_destination_button_group
    mDestinationSelector = elm_layout_add(mLayout);
    elm_layout_file_set(mDestinationSelector, Application::mEdjPath, "post_destination_button_group");
    elm_object_signal_callback_add(mDestinationSelector, "mouse,clicked,*", "workaround_for_click", on_dest_selector_clicked, this);
    if (mMode == eSHARE || mMode == eSHARELINKWEB || mMode == eSHARE_EVENT) {
        elm_object_translatable_part_text_set(mDestinationSelector, "selected_item_text", "IDS_SHARE_TO_FACEBOOK");
    } else {
        elm_object_translatable_part_text_set(mDestinationSelector, "selected_item_text", "IDS_POST_TO_FACEBOOK");
    }
    elm_object_translatable_part_text_set(mDestinationSelector, "tap_to_change_text", "IDS_CAP_TAP_TO_CHANGE");
    evas_object_show(mDestinationSelector);
}

void PostComposerScreen::CreateFooterButtons() {
    if ((mMode != eCREATEALBUM) && mMode != eCREATEALBUMANDNOTIFY) {
        mFooterLayout = elm_layout_add(mLayout);
        elm_layout_file_set(mFooterLayout, Application::mEdjPath, "composer_buttons_group");
        elm_object_part_content_set(mLayout, "footer_buttons", mFooterLayout);

        switch (mMode) {
        case eDEFAULT:
        case eDEFAULTAPPTEXTSHARE:
        case eADD_GROUP_POST:
        case eADDPHOTOSTOALBUM:
        case eFRIENDTIMELINE:
        case eADD_EVENT_POST: {
            if (!mShareTextPictures) {  //workaround for non-standard paths shared from Memo app
                elm_object_signal_callback_add(mFooterLayout, "clicked", "camera_button", add_media_button_clicked, this);
                if (SelectedMediaList::Count()) {
                    elm_object_signal_emit(mFooterLayout, "activate", "camera_button");
                } else {
                    elm_object_signal_emit(mFooterLayout, "show", "camera_button");
                }
            }
        }
            break;
        case eEDIT:
            if (mPostToEdit && (mPostToEdit->IsCoverPhotoPost() || mPostToEdit->IsProfilePhotoPost())) {
                elm_object_signal_emit(mFooterLayout, "hide.tag.friend", "tag_friend_button");
                elm_object_signal_emit(mFooterLayout, "hide_checkin_button", "");
            } else {
                if (mPlace) {
                    elm_object_signal_emit(mFooterLayout, "activate", "checkin_button");
                } else {
                    elm_object_signal_emit(mFooterLayout, "show", "checkin_button");
                }
                if (mPrivacyModel->AreThereTaggedFriends() > 0) {
                    elm_object_signal_emit(mFooterLayout, "activate", "tag_friend_button");
                } else {
                    elm_object_signal_emit(mFooterLayout, "show", "tag_friend_button");
                }
            }
            break;
        default:
            break;
        }

        elm_object_signal_callback_add(mFooterLayout, "clicked", "tag_friend_button", on_tag_friend_button_clicked, this);
        elm_object_signal_callback_add(mFooterLayout, "mouse,clicked,*", "checkin_button", on_checkin_button_clicked, this);
    }
    //Fix for Issue #1910 : Bug 1
    if ( (mMode == eCREATEALBUM) || (mMode == eCREATEALBUMANDNOTIFY) )
    {
        mFooterLayout = elm_layout_add(mLayout);
        elm_layout_file_set(mFooterLayout, Application::mEdjPath, "composer_buttons_group");
        elm_object_part_content_set(mLayout, "footer_buttons", mFooterLayout);

        // Hide the Tag Friend Button
        elm_object_signal_emit(mFooterLayout, "hide.tag.friend", "tag_friend_button");
        elm_object_signal_callback_add(mFooterLayout, "mouse,clicked,*", "checkin_button", on_checkin_button_clicked, this);
    }
}

void PostComposerScreen::SetPhotoAlbumButtonMode(AlbumButtonMode newMode) {
    if (mAlbumButtonMode == newMode ||
        mShareTextPictures) {  //workaround for non-standard paths shared from Memo app
        return;
    }

    if (mAlbumButtonMode == EHidden && (newMode == EShown || newMode == EActive)) {
        elm_object_signal_callback_add(mFooterLayout, "clicked", "photoalbum_button", add_select_album_button_clicked, this);
    } else if ((mAlbumButtonMode == EShown || mAlbumButtonMode == EActive) && newMode == EHidden) {
        elm_object_signal_callback_del(mFooterLayout, "clicked", "photoalbum_button", add_select_album_button_clicked);
    }

    mAlbumButtonMode = newMode;
    switch(mAlbumButtonMode) {
    case EHidden:
        elm_object_signal_emit(mFooterLayout, "hide", "photoalbum_button");
        break;
    case EShown:
        elm_object_signal_emit(mFooterLayout, "show", "photoalbum_button");
        break;
    case EActive:
        elm_object_signal_emit(mFooterLayout, "activate", "photoalbum_button");
        break;
    default:
        break;
    }
}

void PostComposerScreen::LaunchTagFriendsScreen() {
    if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_SELECT_FRIENDS) {
        mSelectFriendsScreen = new SelectFriendsScreen(this, mPostDestinationType == Operation::OD_Group ? "IDS_TAG_MEMBERS" : "IDS_TAG_FRIENDS");
        Application::GetInstance()->AddScreen(mSelectFriendsScreen);
        if (!mGroupMembersUploader) { //this means that downloading is completed and data is ready.
            Eina_List *friendInfos = GetFriendInfos();
            mSelectFriendsScreen->SetFriendInfos(friendInfos);
            friendInfos = eina_list_free(friendInfos);
        }
    }
}

void PostComposerScreen::entry_anchor_clicked_cb(void *data, Evas_Object *obj, void *event_info) {
    PostComposerScreen *screen = static_cast<PostComposerScreen*>(data);
    assert(screen);
    Elm_Entry_Anchor_Info* ii = (Elm_Entry_Anchor_Info* ) event_info;
    Log::debug_tag(LogTag, "!!!!!!!!!!!ANCHOR:%s", ii->name);
    if (!strcmp("tagged_friend", ii->name)) {
        screen->LaunchTagFriendsScreen();
    } else if (!strcmp("place", ii->name)) {
        screen->LaunchCheckInScreen();
    }
}

void PostComposerScreen::LaunchCheckInScreen() {
    if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_CHECKIN) {
        CheckInScreen::OpenScreen(this, CheckInScreen::EFromPostComposer, mPlace);
    }
}

void PostComposerScreen::PrivacyChanged(IPCPrivacyObserver::PrivacyEvent event) {
    if (EPrivacyOptionsRefreshed == event) {
        if(mMode == eEDIT) {
            assert(GetEditedPostPrivacy());
            mPrivacyModel->SetOptionFromPostPrivacy(GetEditedPostPrivacy());
        }
        PostChanged(ENothing); //just to update Post button.
    } else if (EPrivacyOptionManuallyChanged == event) {
        if(mMode != eEDIT) {
            mIsAudienceChanged = true;
        }
    }
    UpdatePrivacyField();
}

const char*KTaggedFormattedAnchor = "<font=TizenSans:style=Regular font_size=%s color=#3b5998><a href=tagged_friend>%s</a></font>";
const char*KPlaceFormattedAnchor = "<font=TizenSans:style=Regular font_size=%s color=#3b5998><a href=place>%s</a></font>";

char* PostComposerScreen::GenTaggedFriends() {
    char *genTaggedFriends = nullptr;

    int count = mPrivacyModel ? eina_list_count(mPrivacyModel->GetTaggedFriends()) : 0;
    if (count > 0) {
        Friend *fr = static_cast<Friend *>(eina_list_nth(mPrivacyModel->GetTaggedFriends(), 0));
        char *markupFriend = elm_entry_utf8_to_markup(fr->mName.c_str());
        char *others = NULL;
        if(count > 1) {
            char friendsOther[20] = {0};
            Utils::Snprintf_s(friendsOther, 20, "%d", count - 1);
            if (count == 2) {
                others = WidgetFactory::WrapByFormat("%s IDS_NUM_OTHER", friendsOther);
            } else {
                others = WidgetFactory::WrapByFormat("%s IDS_NUM_OTHERS", friendsOther);
            }
        }
//formatting...
        char *firstFormattedAnchor = NULL;
        char *othersAnchor = NULL;
        firstFormattedAnchor = WidgetFactory::WrapByFormat2(KTaggedFormattedAnchor, R->TAGGED_FRIENDS_ANCHOR_FONT_SIZE, markupFriend);
        othersAnchor = WidgetFactory::WrapByFormat2(KTaggedFormattedAnchor, R->TAGGED_FRIENDS_ANCHOR_FONT_SIZE, others);
        delete[] others;

        if(count == 1) {
            genTaggedFriends = WidgetFactory::WrapByFormat2(" %s %s", "IDS_WITH", firstFormattedAnchor);
        } else {
            genTaggedFriends = WidgetFactory::WrapByFormat4(" %s %s %s %s", "IDS_WITH",
                                           firstFormattedAnchor, "IDS_AND", othersAnchor);
        }
        delete[] firstFormattedAnchor;
        delete[] othersAnchor;

        free(markupFriend);
    }
    return genTaggedFriends;
}

char*PostComposerScreen::GenPlace() {
    char *genPlace = NULL;
    if (mPlace) {
        char *markupName = elm_entry_utf8_to_markup(mPlace->mName.c_str());
        char *anchor = WidgetFactory::WrapByFormat2(KPlaceFormattedAnchor, R->PLACE_ANCHOR_FONT_SIZE, markupName);
        genPlace = WidgetFactory::WrapByFormat(" IDS_CHECK_IN_AT %s", anchor);
        delete []anchor;
        free(markupName);
    }
    return genPlace;
}

Eina_Bool PostComposerScreen::update_gen_text_field(void *data) {
    PostComposerScreen *me = static_cast<PostComposerScreen *> (data);
    me->mGenTextUpdateTimer = nullptr;
    me->UpdateGenTextField();
    return EINA_FALSE;
}

void PostComposerScreen::UpdateGenTextField() {
    Evas_Object *entry = elm_object_part_content_get(mGenTextLayout, "swallow.text");
    int lines = Utils::GetLinesNumber(entry);
    if (lines > 3) {
        elm_object_signal_emit(mGenTextLayout, "more_than_3_lines", "");
        elm_entry_scrollable_set(entry, EINA_TRUE);
    } else {
        elm_object_signal_emit(mGenTextLayout, "not_more_than_3_lines", "");
        elm_entry_scrollable_set(entry, EINA_FALSE);
    }
}

const char*KGeneratedTextEditFormat = "DEFAULT='font_size=%s color=#bec1c9'";

void PostComposerScreen::UpdateGenText(const char *text) {
    if (text) {
        Evas_Object *entry = nullptr;
        if (!mGenTextLayout) {
            mGenTextLayout = WidgetFactory::CreateLayoutByGroup(mScrollerBox, "generated_text");

            entry = elm_entry_add(mGenTextLayout);
            elm_entry_editable_set(entry, EINA_FALSE);
            elm_entry_context_menu_disabled_set(entry, EINA_TRUE);
            evas_object_size_hint_weight_set(entry, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
            evas_object_size_hint_align_set(entry, EVAS_HINT_FILL, EVAS_HINT_FILL);
            elm_scroller_policy_set(entry, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_OFF);

            char *entryStyle = WidgetFactory::WrapByFormat(SANS_REGULAR_BLACK_STYLE, R->POST_COMPOSER_TEXT_FONT_SIZE);
            if (entryStyle) {
                elm_entry_text_style_user_push(entry, entryStyle);
                delete[] entryStyle;
            }
            evas_object_smart_callback_add(entry, "anchor,clicked", entry_anchor_clicked_cb, this);
            evas_object_show(entry);

            elm_object_part_content_set(mGenTextLayout, "swallow.text", entry);

            elm_box_pack_start(mScrollerBox, mGenTextLayout);
            HRSTC_TRNSLTD_TEXT_SET(entry, text);
            //When entry is newly created then its size isn't known yet. So, let's update it async.
            //In the eEDIT mode it takes more time, because the field is created during entire screen creation.
            if (!mGenTextUpdateTimer) {
                mGenTextUpdateTimer = ecore_timer_add(mMode == eEDIT ? 0.5 : 0.2, update_gen_text_field, this);
            }
        } else {
            entry = elm_object_part_content_get(mGenTextLayout, "swallow.text");
            HRSTC_TRNSLTD_TEXT_SET(entry, text);
            UpdateGenTextField();
        }
    } else {
        if (mGenTextLayout) {
            evas_object_del(mGenTextLayout);
            mGenTextLayout = nullptr;
        }
    }
}

const char*KGeneratedTextUserNameFormat = "<font=TizenSans:style=Bold font_size=%s color=#000>%s</>";

void PostComposerScreen::SetGeneratedText() {
    char* taggedFriends = GenTaggedFriends();
    char* place = GenPlace();

    if (taggedFriends || place) {

        char *formattedName = nullptr;
        if(mMode != eEDIT) {
            formattedName = WidgetFactory::WrapByFormat2(KGeneratedTextUserNameFormat, R->POST_COMPOSER_TEXT_FONT_SIZE, LoggedInUserData::GetInstance().mName);
        } else {
            formattedName = Utils::getCopy(LoggedInUserData::GetInstance().mName);
        }

        int genTextLen = (taggedFriends ? strlen(taggedFriends) : 0) + (place ? strlen(place) : 0) + (formattedName ? strlen(formattedName) : 0);
        genTextLen += (genTextLen == 0) ? 0 : 3; // 2 for " -".

        char *nonTranslatedGeneratedText = nullptr;
        if (genTextLen) {
            nonTranslatedGeneratedText = (char *) malloc(genTextLen + 1);
            Utils::Snprintf_s(nonTranslatedGeneratedText, genTextLen + 1, "%s -%s%s",   (formattedName ? formattedName : ""), (taggedFriends ? taggedFriends : ""), (place ? place : ""));
        }
        delete[] formattedName;
        delete[] taggedFriends;
        delete[] place;

        UpdateGenText(nonTranslatedGeneratedText);
        free(nonTranslatedGeneratedText);
    } else {
        UpdateGenText(nullptr);
    }
}

void PostComposerScreen::SetGeneratedEditText()
{
    assert(mPostToEdit);
    if (mMessageEntry) {
        if(mPostToEdit->GetMessage()) {
            Log::debug(LOG_FACEBOOK_TAGGING, "mPostToEdit->GetMessage(): = %s",  mPostToEdit->GetMessage());
            std::string newString = PresenterHelpers::AddFriendTags(mPostToEdit->GetMessage(), mPostToEdit->GetMessageTagsList(), PresenterHelpers::eComposer);
            elm_entry_entry_set(mMessageEntry, newString.c_str());
        }
        SetGeneratedText();
    }
}

void PostComposerScreen::PostChanged(ChangeType type) {
    Log::debug_tag(LogTag, "PostChanged(%d)", type);
    //AAA ToDo: IsModelReady() should be checked not for all modes, but only for modes which require privacy.
    DisablePostButton(mIsSendingPost || NothingToPost() || !mPrivacyModel->IsModelReady());

    switch(type) {
    case EText:
    {
        DeleteEntryCallbacks();

        if (mIsPasteRequested) {
            CheckForLink();
            mIsPasteRequested = false;
        }
        SetEntryCallbacks();
    }
        break;
    case EPrivacy:
        break;
    case ETagged:
        SetGeneratedText();
        elm_object_signal_emit(mFooterLayout, (mPrivacyModel && mPrivacyModel->AreThereTaggedFriends()) ? "activate" : "deactivate", "tag_friend_button");
        break;
    case EPlace:
        SetGeneratedText();
        elm_object_signal_emit(mFooterLayout, mPlace ? "activate" : "deactivate", "checkin_button");
        break;
    case EMedia:
        if (!mShareTextPictures) {  //workaround for non-standard paths shared from Memo app
            ManageText();

            if (!SelectedMediaList::Count()) {
                // Close Album popup if persists
                CloseSelectAlbumPopup();
                if (mMode == eADDPHOTOSTOALBUM) {
                    SwitchFromAddPhotosToAlbumToDefaultMode();
                }
            }

            if (mMode != eEDIT) {
                elm_object_signal_emit(mFooterLayout,
                        SelectedMediaList::Count() ? "activate" : "deactivate",
                        "camera_button");
            }
            CheckForLink();
        }
        break;
    default:
        break;
    }
}

void PostComposerScreen::TextChangedCb(void *data, Evas_Object *obj, void *event_info)
{
    PostComposerScreen *me = static_cast<PostComposerScreen *>(data);
    me->PostChanged(EText);
}

const char *PostComposerScreen::GenTaggedList() {
    char *str = nullptr;
    if (mPrivacyModel) {
//Calc required size first
        int size = 0;
        Eina_List *l;
        void *listData;
        EINA_LIST_FOREACH(mPrivacyModel->GetTaggedFriends(), l, listData) {
            Friend * fr = static_cast<Friend *> (listData);
            size += strlen(fr->GetId()) + 4; //3 for ',""' and one more for null-terminator
        }
        listData = eina_list_nth(mPrivacyModel->GetTaggedFriends(), 0);
        if (listData) {
            str = new char[size + 7];
            Friend *fr = static_cast<Friend *> (listData);
            Utils::Snprintf_s(str, size + 7, "[\"%s\"", fr->GetId());
        } else {
            return str;
        }

        Eina_List *second = eina_list_nth_list(mPrivacyModel->GetTaggedFriends(), 1);
        EINA_LIST_FOREACH(second, l, listData) {
            Friend *fr = static_cast<Friend *> (listData);
            char *text = new char [strlen(fr->GetId()) + 4]; //3 for ',""' and one more for null-terminator
            Utils::Snprintf_s(text, strlen(fr->GetId()) + 4, ",\"%s\"", fr->GetId());
            eina_strlcat(str, text, size + 7);
            delete []text;
        }
        eina_strlcat(str, "]", size + 7);
        Log::debug_tag(LogTag, "Tagged list:[%s]", str);
    }
    return str;
}

char *PostComposerScreen::GenPrivacy(PCPrivacyModel *privacyModel) {
    assert(privacyModel);
    PCPrivacyOption *privacyOption = privacyModel->GetPrivacyOption();
    if (privacyOption) {
        const char *icon = privacyOption->GenIconPath(true);
        if (icon) {
            char *imageItem = WidgetFactory::WrapByFormat2("<item absize=%s vsize=full href=file://%s></item>",
                    R->POST_COMP_PRIVACY_TO_IMG_SIZE, icon);
            delete[] icon;

            char *str = WidgetFactory::WrapByFormat2("%s  %s", imageItem, privacyOption->GetDescription());

            delete[] imageItem;
            return str;
        }
    }
    return NULL;
}

void PostComposerScreen::EnablePrivacyField(bool enable) {
    edje_object_signal_emit(elm_layout_edje_get(mPrivacyLayout),
            (enable ? (Application::IsRTLLanguage() ? "show_arrow,rtl" : "show_arrow,ltr") :
            "hide_arrow"), "forward_arrow");
    mIsPrivacyFieldEnabled = enable;
}

void PostComposerScreen::UpdatePrivacyField() {
    SetPrivacyContent();

    char *privacyText = NULL;
    char *str = NULL;

    if (!mPrivacyLayout) return;
    if (!IsPrivacyEditable()) {
        switch (mPostDestinationType) {
            case Operation::OD_Friend_Timeline:
                if(mToName) {
                    const char *localizedText = "%sIDS_S_FRIENDS";
                    int size = strlen(localizedText) + strlen(mToName) + 1;
                    privacyText = new char[size];
                    Utils::Snprintf_s(privacyText, size, localizedText, mToName);
                }
                break;
            case Operation::OD_Page:
            case Operation::OD_Event:
            case Operation::OD_Group:
                assert(mToPrivacy);
                if (!strcmp(mToPrivacy,"Open")) {
                    privacyText = new char[strlen("IDS_OPEN")+1];
                    eina_strlcpy(privacyText, "IDS_OPEN", strlen("IDS_OPEN")+1);
                } else if(!strcmp(mToPrivacy,"Closed")){
                    privacyText = new char[strlen("IDS_CLOSED")+1];
                    eina_strlcpy(privacyText, "IDS_CLOSED", strlen("IDS_CLOSED")+1);
                } else if(!strcmp(mToPrivacy,"Secret")){
                    privacyText = new char[strlen("IDS_SECRET")+1];
                    eina_strlcpy(privacyText, "IDS_SECRET", strlen("IDS_SECRET")+1);
                }
                break;
            default:
                if (mMode == eADDPHOTOSTOALBUM && mAlbumToAddPhotos && mAlbumToAddPhotos->mName) {
                    if (mAlbumToAddPhotos->mPrivacy) {
                        int size = strlen(mAlbumToAddPhotos->mName) + strlen(mAlbumToAddPhotos->mPrivacy) + 3 + 1;
                        privacyText = new char[size];
                        Utils::Snprintf_s(privacyText, size, KPrivacyFormatForAddPhotosToAlbum, mAlbumToAddPhotos->mName, mAlbumToAddPhotos->mPrivacy);
                    } else {
                        privacyText = new char[strlen(mAlbumToAddPhotos->mName) + 1];
                        eina_strlcpy(privacyText, mAlbumToAddPhotos->mName, strlen(mAlbumToAddPhotos->mName) + 1);
                    }
                }
                break;
        }

        str = WidgetFactory::WrapByFormat2(SANS_REGULAR_9298A4_FORMAT,
                                           R->POST_COMP_PRIVACY_TO_SELECTOR_FONT_SIZE,
                                           privacyText);
        EnablePrivacyField(false);
    } else {
        EnablePrivacyField(true);
        privacyText = GenPrivacy(mPrivacyModel);
        str = WidgetFactory::WrapByFormat2(SANS_REGULAR_588FFF_FORMAT,
                                           R->POST_COMP_PRIVACY_TO_SELECTOR_FONT_SIZE,
                                           privacyText);
    }

    if (mPrivacyField) {
        HRSTC_TRNSLTD_TEXT_SET(mPrivacyField, str);
    }

    PostChanged(EPrivacy);

    delete []privacyText;
    delete []str;
}

void PostComposerScreen::CreatePrivacy(Evas_Object * parent) {
    mPrivacyLayout = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(parent, "post_composer_privacy");
    elm_object_signal_callback_add(mPrivacyLayout, "clicked", "post_composer_privacy", on_to_selector_clicked, this);

    mToSelectorBox = elm_box_add(mPrivacyLayout);
    elm_object_part_content_set(mPrivacyLayout, "to_selector", mToSelectorBox);
    elm_box_horizontal_set(mToSelectorBox, EINA_TRUE);

    SetPrivacyContent();

    bool enablePrivacyField = !((mMode == eADD_GROUP_POST) || (mMode == eEDIT));
    EnablePrivacyField(enablePrivacyField);
}

void PostComposerScreen::SetPrivacyContent() {
    if (mToSelectorBox) {
        elm_box_clear(mToSelectorBox);

        if(mPostDestinationType == Operation::OD_Friend_Timeline || mPostDestinationType == Operation::OD_Home) {
            Evas_Object *toTextLabel = elm_label_add(mToSelectorBox);
            evas_object_size_hint_align_set(toTextLabel, 0, 0.5);
            FRMTD_TRNSLTD_TXT_SET2(toTextLabel, SANS_REGULAR_9298A4_FORMAT, R->POST_COMP_PRIVACY_TO_TEXT_FONT_SIZE, "IDS_POST_COMP_TO");
            elm_box_pack_end(mToSelectorBox, toTextLabel);
            evas_object_show(toTextLabel);
        } else {
            Evas_Object *image = elm_image_add(mToSelectorBox);
            elm_image_no_scale_set(image, EINA_TRUE);
            elm_image_resizable_set(image, EINA_TRUE, EINA_TRUE);
            evas_object_size_hint_align_set(image, 0, 0.5);
            evas_object_size_hint_min_set(image, R->POST_COMP_PRIVACY_GROUP_ICON_SIZE, R->POST_COMP_PRIVACY_GROUP_ICON_SIZE);
            evas_object_size_hint_max_set(image, R->POST_COMP_PRIVACY_GROUP_ICON_SIZE, R->POST_COMP_PRIVACY_GROUP_ICON_SIZE);
            if (GetToPrivacy() && !strcmp(GetToPrivacy(), "Open")) {
                elm_image_file_set(image, ICON_DIR "/privacy_public_big.png", NULL);
            } else {
                Application::GetInstance()->SetElmImageFile(image, POST_COMPOSER_ICON_DIR "/privacy_group_small.png");
            }
            elm_box_pack_end(mToSelectorBox, image);
            evas_object_show(image);
        }

        Evas_Object *spacer = elm_box_add(mToSelectorBox);
        evas_object_size_hint_weight_set(spacer, 0.5, 1);
        evas_object_size_hint_align_set(spacer, 0, 0.5);
        evas_object_resize(spacer, R->POST_COMP_PRIVACY_TO_TEXT_OFFSET_R, 0);
        elm_box_pack_end(mToSelectorBox, spacer);
        evas_object_show(spacer);

        // to_selector
        mPrivacyField = elm_entry_add(mToSelectorBox);
        Utils::SetKeyboardFocus(mPrivacyField, EINA_FALSE);
        evas_object_size_hint_weight_set(mPrivacyField, 20, 1);
        evas_object_size_hint_align_set(mPrivacyField, -1, 0.5);
        elm_entry_single_line_set(mPrivacyField, EINA_TRUE);
        elm_entry_editable_set(mPrivacyField, EINA_FALSE);
        elm_entry_scrollable_set(mPrivacyField, EINA_TRUE);
        elm_entry_cnp_mode_set(mPrivacyField, ELM_CNP_MODE_PLAINTEXT);
        elm_scroller_policy_set(mPrivacyField, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_OFF);
        elm_box_pack_end(mToSelectorBox, mPrivacyField);
        evas_object_show(mPrivacyField);
    }
}

void PostComposerScreen::message_entry_focus_in(void *data, Evas_Object *obj, void *event_info) {
    PostComposerScreen *me = static_cast<PostComposerScreen *>(data);
    assert(me);
    if (me->mMessageEntry) {
        elm_entry_input_panel_show(me->mMessageEntry);
    }
}

void PostComposerScreen::message_entry_focus_out(void *data, Evas_Object *obj, void *event_info) {
    PostComposerScreen *me = static_cast<PostComposerScreen *>(data);
    assert(me);
    if (me->mMessageEntry) {
        elm_entry_input_panel_hide(me->mMessageEntry);
    }
}

void PostComposerScreen::ManageText() {
    if (mMode != eCREATEALBUM && mMode != eCREATEALBUMANDNOTIFY && (mMode != eADDPHOTOSTOALBUM || SelectedMediaList::Count() == 1)) { //Text field must be shown
        if (!mTextLayout) {
            CreateText();
        }
    } else { // Text field must NOT be shown
        if (mTextLayout) {
            RemoveText();
        }
    }
}

void PostComposerScreen::CreateText() {
    assert(!mTextLayout);
    mTextLayout = elm_layout_add(mScrollerBox);
    elm_layout_file_set(mTextLayout, Application::mEdjPath, "post_composer_wrapper");

    evas_object_size_hint_weight_set(mTextLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mTextLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);
    if (mPrivacyLayout) {
        elm_box_pack_after(mScrollerBox, mTextLayout, mPrivacyLayout);
    } else { // If there is no mPrivacyLayout then mTextLayout is first.
        elm_box_pack_start(mScrollerBox, mTextLayout);
    }
    evas_object_show(mTextLayout);

    mMessageEntry = elm_entry_add(mTextLayout);
    elm_layout_content_set(mTextLayout, "content", mMessageEntry);
    elm_entry_scrollable_set(mMessageEntry, EINA_FALSE);
    elm_entry_prediction_allow_set(mMessageEntry, EINA_FALSE);
    elm_entry_cnp_mode_set(mMessageEntry, ELM_CNP_MODE_PLAINTEXT);
    elm_entry_input_panel_enabled_set(mMessageEntry, EINA_FALSE); // Please never set it to true for this entry!!!
    char * entryStyle = WidgetFactory::WrapByFormat(SANS_REGULAR_BLACK_STYLE, R->POST_COMPOSER_TEXT_FONT_SIZE);
    if (entryStyle) {
        elm_entry_text_style_user_push(mMessageEntry, entryStyle);
        delete[] entryStyle;
        entryStyle = NULL;
    }
    mTagging = new FriendTagging(mLayout, mMessageEntry);
    SetMessageEntryGuide();
    if (mPostToEdit && mMode == eEDIT) {
        SetGeneratedEditText();
    }
    evas_object_smart_callback_add(mMessageEntry, "changed,user", TextChangedCb, this);
    elm_object_focus_set(mMessageEntry, EINA_FALSE);
    evas_object_show(mMessageEntry);
    evas_object_smart_callback_add(mMessageEntry, "focused", message_entry_focus_in, this);
    evas_object_smart_callback_add(mMessageEntry, "unfocused", message_entry_focus_out, this);
    evas_object_event_callback_add(mMessageEntry, EVAS_CALLBACK_KEY_DOWN, key_down_cb, this);
}

void PostComposerScreen::RemoveText() {
    assert(mTextLayout);
    assert(mMessageEntry);
    elm_object_focus_set(mMessageEntry, EINA_FALSE);
    elm_entry_input_panel_hide(mMessageEntry);
    evas_object_del(mTextLayout);
    mTextLayout = nullptr;
    mMessageEntry = nullptr;
    delete mTagging;
    mTagging = nullptr;
}

void PostComposerScreen::SetMessageEntryGuide()
{
    if (!mMessageEntry) {
        return;
    }

    switch (SelectedMediaList::Count()) {
    case 0:
        if (mMode == eSHARELINKWEB || mMode == eSHARE || mMode == eSHARE_EVENT) {
            FRMTD_TRNSLTD_PART_TXT_SET2(mMessageEntry, "elm.guide", SANS_REGULAR_9298A4_FORMAT, R->POST_COMPOSER_TEXT_FONT_SIZE, "IDS_WRITE_SOMETHING");
        } else {
            FRMTD_TRNSLTD_PART_TXT_SET2(mMessageEntry, "elm.guide", SANS_REGULAR_9298A4_FORMAT, R->POST_COMPOSER_TEXT_FONT_SIZE, "IDS_WHATS_ON_YOUR_MIND");
        }
        break;
    case 1: {
        PostComposerMediaData * mediaData = static_cast<PostComposerMediaData *>(eina_list_data_get(SelectedMediaList::Get()));
        if (mediaData->GetType() == MEDIA_CONTENT_TYPE_VIDEO) {
            FRMTD_TRNSLTD_PART_TXT_SET2(mMessageEntry, "elm.guide", SANS_REGULAR_9298A4_FORMAT, R->POST_COMPOSER_TEXT_FONT_SIZE, "IDS_SAY_SOMETHING_ABOUT_THIS_VIDEO");
        } else {
            FRMTD_TRNSLTD_PART_TXT_SET2(mMessageEntry, "elm.guide", SANS_REGULAR_9298A4_FORMAT, R->POST_COMPOSER_TEXT_FONT_SIZE, "IDS_SAY_SOMETHING_ABOUT_THIS_PHOTO");
        }
    }
        break;
    default:
        FRMTD_TRNSLTD_PART_TXT_SET2(mMessageEntry, "elm.guide", SANS_REGULAR_9298A4_FORMAT, R->POST_COMPOSER_TEXT_FONT_SIZE, "IDS_SAY_SOMETHING_ABOUT_THESE_PHOTOS");
        break;
    }
}

void PostComposerScreen::DeleteMediaList() {
    delete mPhotoWidget;
    mPhotoWidget = nullptr;
    elm_box_clear(mMediaBox);
}

void PostComposerScreen::SetMediaListToDelete(Eina_List *mediaListToDelete) {
    Eina_List *list = nullptr;
    void *listData = nullptr;
    EINA_LIST_FOREACH(mediaListToDelete, list, listData) {
        PostComposerMediaData* mediaData = static_cast<PostComposerMediaData*>(listData);
        mediaData->AddRef();
        mMediaListToDelete = eina_list_append(mMediaListToDelete, mediaData);
    }
}

void PostComposerScreen::CreateSelectedMedia() {
    int mediaCount = SelectedMediaList::Count();
    if (mediaCount) {
        mPhotoWidget = new ComposerPhotoWidget(ePOST_COMPOSE, mLayout, mMediaBox, this);

        // Entry focus logic is based on this button. Be careful if you want to remove it.
        if (mPostDestinationType != Operation::OD_Friend_Timeline && !mShareTextPictures && !IsVideoMediaPresented()) {
            CreateAddPhotoButton(mMediaBox);
        }
        if((mMode == eDEFAULT || mMode ==  eDEFAULTAPPTEXTSHARE) && !mToId && !mShareTextPictures) {
            if (IsVideoMediaPresented()) { //We can't send video to an album (B2848).
                SetPhotoAlbumButtonMode(EHidden);
            } else {
                SetPhotoAlbumButtonMode(EShown);
            }
        }
    } else {
        SetPhotoAlbumButtonMode(EHidden);
    }
}

void PostComposerScreen::CaptionTextChanged(PostComposerMediaData *mediaData) {
    DisablePostButton(NothingToPost());
}

void PostComposerScreen::CropImageButtonClick(PostComposerMediaData *mediaData) {
    ScreenBase *addCropScreen = new AddCropScreen(mediaData, this);
    Application::GetInstance()->AddScreen(addCropScreen);
}

void PostComposerScreen::DeleteMediaButtonClick(PostComposerMediaData *mediaData, Evas_Object *photoWidgetLayout) {
    // Remove MediaData object from the list and update UI accordingly
    if (mediaData) {
        mediaData->RemoveCaptionLayout();

        if (mMode == eEDIT) {
            mediaData->AddRef();
            mMediaListToDelete = eina_list_append(mMediaListToDelete, mediaData);
            DisablePostButton(NothingToPost());
        }

        SelectedMediaList::RemoveItem(mediaData);
        PostChanged(EMedia);

        int mediaListCount = SelectedMediaList::Count();
        // Change message guide text.
        if (mediaListCount == 1 || mediaListCount == 0) {
            SetMessageEntryGuide();
        }
        Log::debug_tag(LogTag, "DeleteMediaButtonClick:: DELETE MEDIA");
    }

    // Remove Photo Widget Layout from mMediaBox or remove all children of mMediaBox if there are no items.
    if (photoWidgetLayout && mMediaBox) {
        if (SelectedMediaList::Count() == 0) {
            SetPhotoAlbumButtonMode(EHidden);
            DeleteMediaList();
        } else {
            elm_box_unpack(mMediaBox, photoWidgetLayout);
            // Get and delete wrapper layout (structure is described in delete media handler of PhotoWidget class).
            evas_object_del(photoWidgetLayout);
        }
    }
}

Evas_Event_Flags PostComposerScreen::on_image_clicked_cb(void *data, void *event_info) {

    PostComposerScreen *me = static_cast<PostComposerScreen *> (data);
    EditCaptionsScreen *screen = new EditCaptionsScreen(me);
    Application::GetInstance()->AddScreen(screen);

    return EVAS_EVENT_FLAG_NONE;
}


void PostComposerScreen::CreateAddPhotoButton(Evas_Object * parent) {
    mAddPhotoButtonLayout = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(parent, "post_composer_add_photo_button");

    elm_object_signal_callback_add(mAddPhotoButtonLayout, "mouse,clicked", "add_photo_button",
                    add_media_button_clicked, this);
    elm_object_translatable_part_text_set(mAddPhotoButtonLayout, "add_photo_text", "IDS_ADD_PHOTOS");
}



void PostComposerScreen::on_create_album_completed(void* object, char* response, int code) {
    Log::debug_tag(LogTag, "on_create_album_completed");

    PostComposerScreen * screen = static_cast<PostComposerScreen *>(object);
    FacebookSession::GetInstance()->ReleaseGraphRequest(screen->mRequest);
    if (code == CURLE_OK) {
        Log::info_tag(LogTag, "response: %s", response);
        FbRespondPost * postResponse = FbRespondPost::createFromJson(response);

        if (postResponse == NULL) {
            Log::debug_tag(LogTag, "Error parsing http response");
        } else {
            if (postResponse->mError != NULL) {
                Log::error_tag(LogTag, "Error creating album, error = %s", postResponse->mError->mMessage);
            } else {
                Log::debug_tag(LogTag, "on_create_album_completed, postRespond->mId = %s", postResponse->mId);
                AppEvents::Get().Notify(eALBUM_CREATED_EVENT, NULL);
                if (screen->mMode == eCREATEALBUMANDNOTIFY) {
                    screen->mRequest = FacebookSession::GetInstance()->GetAlbumDetails(postResponse->mId, on_get_album_details_completed, screen);
                }
            }
            delete postResponse;
        }
    } else {
        Log::debug_tag(LogTag, "Error sending http request");
    }

    if (response != NULL) {
        //Attention!! client should take care to free respond buffer
        free(response);
    }
    if(screen) {
        if (screen->mMode != eCREATEALBUMANDNOTIFY) {
            screen->SendingCompleted();
            screen->Pop();
        }
    }
}

void PostComposerScreen::on_get_album_details_completed(void* object, char* response, int code) {
    Log::debug_tag(LogTag, "on_get_album_details_completed");

    PostComposerScreen * screen = static_cast<PostComposerScreen *>(object);
    FacebookSession::GetInstance()->ReleaseGraphRequest(screen->mRequest);
    if (code == CURLE_OK) {
        Log::debug_tag(LogTag, "response: %s", response);
        FbResponseAlbumDetails * postResponse = FbResponseAlbumDetails::createFromJson(response);

        if (postResponse == NULL) {
            Log::debug_tag(LogTag, "Error parsing http response");
        } else {
            if (postResponse->mError != NULL) {
                Log::debug_tag(LogTag, "Error get album details, error = %s", postResponse->mError->mMessage);
            } else {
                Album *album = postResponse->GetAlbum();
                if (album) {
                    Log::debug_tag(LogTag, "on_get_album_details_completed() id:%s, name:%s, privacy:%s", album->GetId(), album->mName, album->mPrivacy);
                    static_cast<PostComposerScreen *> (screen->mScreen)->AlbumSelected(album, false);
                }
            }
            delete postResponse;
        }
    } else {
        Log::error_tag(LogTag, "Error sending http request");
    }

    if (response != NULL) {
        //Attention!! client should take care to free respond buffer
        free(response);
    }
    if(screen) {
        screen->SendingCompleted();
        screen->Pop();
    }
}

void PostComposerScreen::RefreshScreen()
{
    Log::debug_tag(LogTag, "RefreshScreen");
    DeleteMediaList();
    SetMessageEntryGuide();
    CreateSelectedMedia();
    PostChanged(EMedia);
}

void PostComposerScreen::Push()
{
    if(IsLaunchedByAppControl()){
        evas_object_hide(getParentMainLayout());
    }
    ScreenBase::Push();
    // Remove standard blue header
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void PostComposerScreen::InsertBefore(ScreenBase * parentScreen) {
    ScreenBase::InsertBefore(parentScreen);
    // Remove standard blue header
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void PostComposerScreen::on_get_suggested_friends_completed(void* data) {
// AAA Now we do nothing here. This is just needed to speedup suggested friends downloading.
}

void PostComposerScreen::Update(AppEventId eventId, void * data) {
// ToDo: Implement friends list update later.
//    TagFriendsScreen::UpdateFriends();
    switch (eventId) {
    case eRESUME_EVENT:
        Log::debug_tag(LogTag, "Update(eRESUME_EVENT)");
        if (1 == SelectedMediaList::Count()) {
            PostComposerMediaData *mediaData = static_cast<PostComposerMediaData*>(eina_list_nth(SelectedMediaList::Get(), 0));
            if (mediaData && (-1 == access(mediaData->GetFilePath(), F_OK))) {
                SelectedMediaList::Clear();
                DeleteMediaList();
                SetMessageEntryGuide();
                PostChanged(ENothing);
                if (!mShareTextPictures) {  //workaround for non-standard paths shared from Memo app
                    elm_object_signal_emit(mFooterLayout, "show", "camera_button");
                }
                if (mMessageEntry) {
                    elm_entry_input_panel_show(mMessageEntry);
                }
            }
        }
        break;
#ifdef _SCROLLER_WORKAROUND_ENABLED_
    case eFOCUSED_EVENT:
        Log::debug_tag(LogTag, "Update(eFOCUSED_EVENT)");
        OnResume();
        break;
    case eUNFOCUSED_EVENT:
        Log::debug_tag(LogTag, "Update(eUNFOCUSED_EVENT)");
        OnPause();
        break;
    case eCONFIRMATION_DIALOG_SHOW_EVENT:
        Log::debug_tag(LogTag, "Update(eCONFIRMATION_DIALOG_SHOW_EVENT)");
        StoreScrollPosition();
        break;
    case eERROR_DIALOG_SHOW_EVENT:
        Log::debug_tag(LogTag, "Update(eERROR_DIALOG_SHOW_EVENT)");
        StoreScrollPosition();
        break;
    case eSELECT_ALBUM_SCREEN_SHOW_EVENT:
        Log::debug_tag(LogTag, "Update(eSELECT_ALBUM_SCREEN_SHOW_EVENT)");
        StoreScrollPosition();
        break;
#endif
    default:
        Log::debug_tag(LogTag, "Update(%d)", eventId);
        break;
    }
}

void PostComposerScreen::UpdateTaggedFriends(Eina_List *friends) {
    if (mPrivacyModel) {
        Eina_List *list = GetFriendsListForSelection();
        Eina_List *l;
        void *listData;
        EINA_LIST_FOREACH(list, l, listData) {
            Friend *fr = static_cast<Friend *> (listData);
            if (eina_list_data_find(friends, fr)) {
                mPrivacyModel->AppendTaggedFriend(fr);
            } else {
                mPrivacyModel->RemoveTaggedFriend(fr);
            }
        }
    }
}

Eina_List *PostComposerScreen::GetFriendsListForSelection() {
    Eina_List *list = NULL;
    if (mPostDestinationType == Operation::OD_Group) {
        list = mGroupMembers;
    } else {
        list = mPrivacyModel->GetFriendsList();
    }
    return list;
}

void PostComposerScreen::DeletePostComposerTempDirectory() {
    char dirName[MAX_FULL_PATH_NAME_LENGTH];
    // Delete created folder for photos with text
    char * imageStoragePath = Utils::GetInternalStorageDirectory(STORAGE_DIRECTORY_IMAGES);
    Utils::Snprintf_s(dirName, MAX_FULL_PATH_NAME_LENGTH, "%s/%s", imageStoragePath, POST_COMPOSER_TEMP_DIR);
    free(imageStoragePath);
    Utils::RemoveFolderRecursively(dirName);

    // Delete created folder for text images
    imageStoragePath = app_get_data_path();
    Utils::Snprintf_s(dirName, MAX_FULL_PATH_NAME_LENGTH, "%s%s", imageStoragePath, POST_COMPOSER_TEMP_DIR);
    free(imageStoragePath);
    Utils::RemoveFolderRecursively(dirName);

}

/**
 * @brief Starts GetPrivacyOptions request to facebook
 */
void PostComposerScreen::ShowPrivacyIsLoading() {
    EnablePrivacyField(false);
    char *str = WidgetFactory::WrapByFormat2(SANS_REGULAR_9298A4_FORMAT,
                                             R->POST_COMP_PRIVACY_TO_SELECTOR_FONT_SIZE,
                                             i18n_get_text("IDS_LOADING"));

    elm_entry_entry_set(mPrivacyField, str);
    delete []str;
}

Evas_Object *PostComposerScreen::CreateCaptionText(Evas_Object *parent, const char* text) {
    Evas_Object *textLayout = elm_layout_add(parent);
    evas_object_size_hint_weight_set(textLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    elm_layout_file_set(textLayout, Application::mEdjPath, "post_composer_caption_text");
    elm_object_translatable_part_text_set(textLayout, "text", text);
    return textLayout;
}

bool PostComposerScreen::IsAudienceChanged() {
    return mIsAudienceChanged;
}

void PostComposerScreen::CancelCallback(void *data, Evas_Object *obj, void *event_info) {
    if(!mIsSendingPost) {
        if (!IsSomethingToLose()) {
            Pop();
        } else {
            ShowKeepDiscardPopup();
            EnableCancelButton(true);
            EnableRightButton(true);
        }
    }
}

bool PostComposerScreen::HandleBackButton(){
    Log::debug_tag(LogTag, "BACK");
    bool ret = true;
    if(!mIsSendingPost) {
        if(WidgetFactory::CloseErrorDialogue()){
        } else if (PopupMenu::Close()) {
        } else if (mTagging && mTagging->HideFriendsPopup()) {
        } else if (mConfirmationDialog) {
            EnableRightButton(true);
            delete mConfirmationDialog;
            mConfirmationDialog = NULL;
        } else if (mSelectAlbumPopup) {
            CloseSelectAlbumPopup();
        } else if (PopupContextTip::Close()) {
        } else {
            if (IsSomethingToLose()) {
                ShowKeepDiscardPopup();
            } else {
                ret = false;
            }
        }
    }
    return ret;
}

bool PostComposerScreen::HandleKeyPadHide()
{
    Log::info(LOG_FACEBOOK_TAGGING, "PostComposerScreen::HandleKeyPadHide");

    bool ret = true;
    if (mMessageEntry && mTagging && mTagging->HideFriendsPopup()) {
        elm_object_focus_set(mMessageEntry, EINA_TRUE);
        elm_entry_input_panel_show(mMessageEntry);
    } else {
        ret = false;
    }
    return ret;
}

void PostComposerScreen::RemoveEntryFocus() {
//this is a workaround to remove focus from any other input fields.
    elm_object_focus_set(mPrivacyLayout, EINA_TRUE);
}

void PostComposerScreen::OnPause()
{
    Log::debug_tag(LogTag, "OnPause()");
#ifdef _SCROLLER_WORKAROUND_ENABLED_
    AppEvents::Get().Unsubscribe(eCONFIRMATION_DIALOG_SHOW_EVENT, this);
    AppEvents::Get().Unsubscribe(eERROR_DIALOG_SHOW_EVENT, this);
    AppEvents::Get().Unsubscribe(eSELECT_ALBUM_SCREEN_SHOW_EVENT, this);
    StoreScrollPosition();
#endif
}

void PostComposerScreen::OnResume() {
    Log::debug_tag(LogTag, "OnResume()");
#ifdef _SCROLLER_WORKAROUND_ENABLED_
    AppEvents::Get().Subscribe(eCONFIRMATION_DIALOG_SHOW_EVENT, this);
    AppEvents::Get().Subscribe(eERROR_DIALOG_SHOW_EVENT, this);
    AppEvents::Get().Subscribe(eSELECT_ALBUM_SCREEN_SHOW_EVENT, this);
#endif
    if (mMessageEntry && SelectedMediaList::Count()) {
        RemoveEntryFocus();
        elm_entry_input_panel_hide(mMessageEntry);
    }

    if (mMode == eEDIT) {
        // It's good to have refresh logic in OnResume() method for all post types,
        // however non-shared post types are out of scope for now.
        if (mPostToEdit && !mPostToEdit->GetIsSharedPost()) {
            ShowEditContent(SelectedMediaList::Get());
        }
        DisablePostButton(NothingToPost());
    } else {
        Eina_List *listItem;
        Eina_List *itemNext;
        void *listData;
        EINA_LIST_FOREACH_SAFE(SelectedMediaList::Get(), listItem, itemNext, listData) {
            PostComposerMediaData * mediaData = static_cast<PostComposerMediaData *>(listData);
            if (!Utils::FileExists(mediaData->GetOriginalPath())) {
                SelectedMediaList::RemoveItem(mediaData);
            }
        }

        if (!mPhotoWidget || !mPhotoWidget->IsDataValid(SelectedMediaList::Get())) {
            RefreshScreen();
        }
    }
}

void PostComposerScreen::ShowKeepDiscardPopup() {
    Log::debug_tag(LogTag, "ShowKeepDiscardPopup()");

    const char *caption = NULL;
    const char *message = NULL;

    switch (mMode) {
        case eCREATEALBUM:
        case eCREATEALBUMANDNOTIFY:
            caption = "IDS_DISCARD_ALBUM";
            message = "IDS_DISCARD_ALBUM_DESC";
            break;
        case eEDIT:
            caption = "IDS_DISCARD_CHANGES";
            message = "IDS_DISCARD_CHANGES_DESC";
            break;
        default:
            caption = "IDS_DISCARD_POST";
            message = "IDS_DISCARD_POST_DESC";
            break;
    }

    mConfirmationDialog = ConfirmationDialog::CreateAndShow(mLayout, caption, message, "IDS_KEEP", "IDS_DISCARD", confirmation_dialog_cb, this);
}

void PostComposerScreen::confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event) {
    Log::debug_tag(LogTag, "confirmation_dialog_cb:%d", event);
    PostComposerScreen *screen = static_cast<PostComposerScreen *>(user_data);

    delete screen->mConfirmationDialog;
    screen->mConfirmationDialog = NULL;

    switch (event) {
        case ConfirmationDialog::ECDYesPressed:
        case ConfirmationDialog::ECDDismiss:
            break;
        case ConfirmationDialog::ECDNoPressed:
            screen->EnableKeypad(EINA_FALSE);
            screen->Pop();
            break;
    }
}

char *PostComposerScreen::GenExceptedFriendsWhoAreTagged(const Eina_List *list) {
    char *res = NULL;
    int count = eina_list_count(list);
    if (count == 1) {
        Friend *fr = static_cast<Friend *> (eina_list_nth(list, 0));
        if (fr) {
            res = Utils::getCopy(fr->mName.c_str());
        }
    } else if (count == 2) {
        Friend *first = static_cast<Friend *> (eina_list_nth(list, 0));
        Friend *second = static_cast<Friend *> (eina_list_nth(list, 1));
        if (first && second) {
            res = WidgetFactory::WrapByFormat2(i18n_get_text("IDS_FIRST_FRIEND_AND_SECOND"), first->mName.c_str(), second->mName.c_str());
        }
    } else if (count > 2) {
        Friend *fr = static_cast<Friend *> (eina_list_nth(list, 0));
        if (fr) {
            char strCount [32];
            Utils::Snprintf_s(strCount, 32, "%d", count - 1);
            res = WidgetFactory::WrapByFormat2(i18n_get_text("IDS_FRIEND_AND_OTHERS"), fr->mName.c_str(), strCount);
        }
    }
    return res;
}

void PostComposerScreen::ShowConfirmAccessForExceptedToPopup(const Eina_List *list) {
    char *friends = GenExceptedFriendsWhoAreTagged(list);
    char *message = WidgetFactory::WrapByFormat(i18n_get_text("IDS_CONFIRM_SHOW_POST_FOR_TAGGED"), friends);
    delete []friends;

    mConfirmationDialog = ConfirmationDialog::CreateAndShow(mLayout, NULL, message,
            "IDS_CANCEL", "IDS_OK", confirmation_access_for_excepted_cb, this);
    delete []message;
}

void PostComposerScreen::confirmation_access_for_excepted_cb(void *user_data, ConfirmationDialog::CDEventType event) {
    PostComposerScreen *screen = static_cast<PostComposerScreen *>(user_data);

    switch (event) {
    case ConfirmationDialog::ECDYesPressed:
        screen->EnableRightButton(true);
        break;
    case ConfirmationDialog::ECDNoPressed:
        screen->EnableKeypad(EINA_FALSE);
        delete screen->mConfirmationDialog;
        screen->mConfirmationDialog = NULL;
        screen->PostStatus();
        return;
    case ConfirmationDialog::ECDDismiss:
        screen->EnableRightButton(true);
        break;
    default:
        break;
    }
    delete screen->mConfirmationDialog;
    screen->mConfirmationDialog = NULL;
}

void PostComposerScreen::DisablePostButton(bool disable) {
    Log::debug_tag(LogTag, "DISABLE POST BUTTON:%d", disable);

    Evas_Object * header = elm_object_part_content_get(mLayout, "swallow.top_toolbar");
    if (disable) {
        EnableRightButton(false);
        elm_object_signal_emit(header, "disable_right_button", "header");
    } else {
        EnableRightButton(true);
        elm_object_signal_emit(header, "enable_right_button", "header");
    }
}

char *PostComposerScreen::GetEntryText(Evas_Object *entry) {
    const char *rawTxt = NULL;
    if (entry) {
        rawTxt = elm_object_part_text_get(entry, "elm.text");
    } else {
        rawTxt = "";
    }
    char *msg = elm_entry_markup_to_utf8(rawTxt);
    return msg;
}

bool PostComposerScreen::NothingToPost() {
    bool ret = false;
    if (mMode == eEDIT) {
        ret = !IsPostEdited() || IsPostEmpty();
    } else {
        ret = !IsModeAllowsEmpty() && IsPostEmpty();
    }
    Log::debug_tag(LogTag, "NothingToPost: ret=%d", ret);
    return ret;
}

bool PostComposerScreen::IsSomethingToLose() {
    bool isSomethingToLose = false;
    bool postChanged = mMode == eEDIT ? IsPostEdited() : !IsPostEmpty();
    isSomethingToLose = IsAudienceChanged() || postChanged;
    Log::debug_tag(LogTag, "IsSomethingToLose: isSomethingToLose=%d", isSomethingToLose);
    return isSomethingToLose;
}

//Word "Empty" relates to Text field and all related to the Text field things like tagged friends, places and shared links. It also relates Selected Media. It doesn't relate shared posts, privacy.
bool PostComposerScreen::IsModeAllowsEmpty() {
    bool isModeAllowsEmpty = (mMode == eCREATEALBUM || mMode == eCREATEALBUMANDNOTIFY || mMode == eSHARE || mMode == eSHARELINKWEB || mMode == eSHARE_EVENT);
    Log::debug_tag(LogTag, "IsModeAllowsEmpty: isModeAllowsEmpty=%d", isModeAllowsEmpty);
    return isModeAllowsEmpty;
}

//Word "Empty" relates to Text field and all related to the Text field things like tagged friends, places and shared links. It also relates Selected Media. It doesn't relate shared posts, privacy.
bool PostComposerScreen::IsPostEmpty() {
    bool isPostEmpty = false;
    bool isText = false;
    bool isMediaSelected = false;


    if (mMode == eCREATEALBUM || mMode == eCREATEALBUMANDNOTIFY) {
        char *albumText = GetEntryText(mAlbumNameEntry);
        isText = (albumText && !Utils::isEmptyString(albumText));
        if (!isText) {
            free(albumText);
            char *albumText = GetEntryText(mAlbumDescriptionEntry);
            isText = (albumText && !Utils::isEmptyString(albumText));
        }
        free(albumText);
    } else {
        if (mMessageEntry) {
            const char *rawTxt = elm_entry_entry_get(mMessageEntry);
            char *msg = elm_entry_markup_to_utf8(rawTxt);
            isText = (msg && !Utils::isEmptyString(msg));
            free(msg);
        }
    }

    isMediaSelected = (mMode == eCREATEALBUM || mMode == eCREATEALBUMANDNOTIFY) ? false : SelectedMediaList::Count() > 0;

    isPostEmpty = !isText && !isMediaSelected && (!mPrivacyModel || !mPrivacyModel->AreThereTaggedFriends()) && !mPlace && !mSharedLink;
    Log::debug_tag(LogTag, "IsPostEmpty: isPostEmpty=%d", isPostEmpty);

    return isPostEmpty;
}

bool PostComposerScreen::IsPostEdited(bool includeCaptionChanges, bool includeMediaListChanges) {
//It makes sense only for eEDIT mode.
    assert(mMode == eEDIT);
    assert(mPostToEdit);
    assert(GetEditedPostPrivacy());
    bool isChanged = IsTextChanged()
            || (IsPrivacyEditable() && mPrivacyModel->IsPostPrivacyChanged(GetEditedPostPrivacy()))
            || mPrivacyModel->AreTaggedFriendsChanged(mPostToEdit->GetTaggedFriends())
            || IsPlaceChanged()
            || (includeCaptionChanges && SelectedMediaList::IsAnyCaptionEdited())
            || (includeMediaListChanges && IsMediaListChanged());
    Log::debug_tag(LogTag, "IsPostEdited: isChanged=%d", isChanged);
    return isChanged;
}

bool PostComposerScreen::IsTextChanged() {
    bool isChanged = false;
    if (mMessageEntry) {
        const char *rawTxt = elm_entry_entry_get(mMessageEntry);
        char *msg = elm_entry_markup_to_utf8(rawTxt);
        char *trimmedMsg = Utils::Trim(msg);
        free(msg);

        const char *postMsg = mPostToEdit->GetMessage();

        if ((trimmedMsg && *trimmedMsg) != (postMsg && *postMsg)) {
            isChanged = true;
        } else if (trimmedMsg && postMsg && strcmp(trimmedMsg, postMsg)) {
            isChanged = true;
        } else { //they are equal
            isChanged = false;
        }

        free(trimmedMsg);
    }

    Log::debug_tag(LogTag, "IsTextChanged: isChanged=%d", isChanged);
    return isChanged;
}

bool PostComposerScreen::IsPlaceChanged() {
    assert(mPostToEdit);
    return ((!mPlace && mPostToEdit->GetPlace()) || (mPlace && !mPostToEdit->GetPlace()) || (mPlace && mPostToEdit->GetPlace() && strcmp(mPlace->mId.c_str(), mPostToEdit->GetPlace()->mId)));
}

bool PostComposerScreen::IsEditGroupPost() {
    return (mMode == eEDIT) && mPostToEdit->GetIsGroupPost();
}



void PostComposerScreen::SetToId(const char *id) {
    free(mToId);
    mToId = SAFE_STRDUP(id);
}

const char *PostComposerScreen::GetToId() {
    return mToId;
}

void PostComposerScreen::SetToName(const char *name) {
    free(mToName);
    mToName = SAFE_STRDUP(name);
}

const char *PostComposerScreen::GetToName() {
    return mToName;
}

void PostComposerScreen::SetToPrivacy(const char *privacy) {
    free(mToPrivacy);
    mToPrivacy = SAFE_STRDUP(privacy);
}

const char *PostComposerScreen::GetToPrivacy() {
    return mToPrivacy;
}

void PostComposerScreen::SwitchFromDefaultToAddPhotosToAlbumMode() {
    mMode = eADDPHOTOSTOALBUM;
    mPrivacyModel->ClearTaggedFriends();
    PostChanged(ETagged);
    elm_object_signal_emit(mFooterLayout, "hide.tag.friend", "tag_friend_button");
    CreateHeader();
    UpdatePrivacyField();
    ManageText();
}

void PostComposerScreen::SwitchFromAddPhotosToAlbumToDefaultMode() {
    mMode = eDEFAULT;
    if (mAlbumToAddPhotos) {
        mAlbumToAddPhotos->Release();
        mAlbumToAddPhotos = NULL;
    }
    elm_object_signal_emit(mFooterLayout, "deactivate", "tag_friend_button");
    PostChanged(ETagged);
    CreateHeader();
    UpdatePrivacyField();
    ManageText();
}

Evas_Event_Flags PostComposerScreen::show_input_text_panel(void *data, void *event_info)
{
    Elm_Gesture_Taps_Info * e = static_cast<Elm_Gesture_Taps_Info *>(event_info);
    PostComposerScreen *screen = static_cast<PostComposerScreen *>(data);

    int targetY, targetH;
    evas_object_geometry_get(screen->mPrivacyLayout, NULL, &targetY, NULL, &targetH);
    bool showInputPanel = e->y > targetY + targetH; //area below the top layout

    if (SelectedMediaList::Count()) {
        evas_object_geometry_get(screen->mMediaBox, NULL, &targetY, NULL, &targetH);
        showInputPanel = (showInputPanel && e->y < targetY) || //area between the top layout and the media box
                          e->y > targetY + targetH; //area below the "Add photos" button
    }

    if (showInputPanel) {
        if (screen->mMessageEntry) {
            elm_object_focus_set(screen->mMessageEntry, EINA_TRUE);
            elm_entry_input_panel_show(screen->mMessageEntry);
        } else if (screen->mAlbumDescriptionEntry) {
            elm_object_focus_set(screen->mAlbumDescriptionEntry, EINA_TRUE);
            elm_entry_input_panel_show(screen->mAlbumDescriptionEntry);
        }
    }

    return EVAS_EVENT_FLAG_NONE;
}

Eina_List *PostComposerScreen::GetFriendInfos() {
    Eina_List *list = GetFriendsListForSelection();
    Eina_List *infosList = NULL;
    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(list, l, listData) {
        Friend *fr = static_cast<Friend *> (listData);
        SelectFriendsScreen::FriendInfo *friendInfo = new SelectFriendsScreen::FriendInfo();
        friendInfo->mClientFriend = fr;
        friendInfo->SetFriend(fr);
        assert(mPrivacyModel); //mPrivacyModel must be created for all modes which work with tagged friends.
        friendInfo->mIsSelected = mPrivacyModel->ReplaceTaggedFriend(fr);
        infosList = eina_list_append(infosList, friendInfo);
    }
    return infosList;
}

void PostComposerScreen::FriendsSelectionCompleted(Eina_List *friends, bool isCanceled) {
    mSelectFriendsScreen->UnregisterClient();
    mSelectFriendsScreen = NULL;
    if (!isCanceled) {
        UpdateTaggedFriends(friends);
        PostChanged(ETagged);
    }
}

void PostComposerScreen::EnableKeypad(Eina_Bool enabled) {
    if (mMessageEntry) elm_entry_input_panel_hide(mMessageEntry);
    if (mAlbumNameEntry) elm_entry_input_panel_enabled_set(mAlbumNameEntry, enabled);
    if (mAlbumDescriptionEntry) elm_entry_input_panel_enabled_set(mAlbumDescriptionEntry, enabled);
}

void PostComposerScreen::CheckForLink() {
    //we don't need to recognize link if it is already a link post or shared post (Issue 2340).
    //We can't update or add a link in/to a post during editing.
    if (mMode == eEDIT || mMode == eSHARE || (mMode == eSHARELINKWEB && mSharedGroup)) {
        return;
    }

    if (!mSharedLink && SelectedMediaList::Count() == 0) { //links can't be shared if a post contains images.
        const char *rawTxt = mMessageEntry ? elm_entry_entry_get(mMessageEntry) : nullptr;
        char *userInput = rawTxt ? elm_entry_markup_to_utf8(rawTxt) : nullptr;
        mSharedLink = userInput ? Utils::FindUrlInText(userInput, 0) : nullptr;
        free(userInput);
        if (mSharedLink) {
            if (!IsDiscardedLink(mSharedLink)) {
                Log::debug_tag(LogTag, "link: %s", mSharedLink);
                ShowPreviewWidget(EProgress, NULL);
                if (!mReqLinkPreview) {
                    mReqLinkPreview = FacebookSession::GetInstance()->PostLinkPreview(mSharedLink, on_link_preview_completed, this);
                }
            } else {
                delete [] mSharedLink;
                mSharedLink = NULL;
            }
        }
    } else if (SelectedMediaList::Count()) { //if a post contains images then links must be removed.
        DeleteSharedLink();
        ShowPreviewWidget(ENone, nullptr);
    }
}

bool PostComposerScreen::IsDiscardedLink(const char *link) {
    bool res = false;
    if (link) {
        Eina_List *l;
        void *listData;
        EINA_LIST_FOREACH(mDiscardedLinks, l, listData) {
            const char *dlink = static_cast<const char *> (listData);
            assert(dlink);
            if (dlink && !strcmp(link, dlink)) {
                res = true;
                break;
            }
        }
    }
    return res;
}

void PostComposerScreen::album_name_key_down_cb(void *data, Evas *e, Evas_Object *obj, void *event_info) {
    PostComposerScreen *me = static_cast<PostComposerScreen *>(data);
    Evas_Event_Key_Up *ev = static_cast<Evas_Event_Key_Up *> (event_info);
    if (me->mAlbumNameEntry && !strcmp(ev->keyname, "Return")) {
        elm_entry_input_panel_hide(me->mAlbumNameEntry);
    }
}

void PostComposerScreen::key_down_cb(void *data, Evas *e, Evas_Object *obj, void *event_info) {
    PostComposerScreen *me = static_cast<PostComposerScreen *>(data);
    Evas_Event_Key_Up *ev = static_cast<Evas_Event_Key_Up *> (event_info);
    if (!strcmp(ev->keyname, "space") || !strcmp(ev->keyname, "Return")) {
        me->CheckForLink();
    }
}

void PostComposerScreen::on_link_preview_completed(void *object, char *response, int code) {
    Log::debug_tag(LogTag, "on_link_preview_completed");

    PostComposerScreen *me = static_cast<PostComposerScreen *> (object);

    FacebookSession::GetInstance()->ReleaseGraphRequest(me->mReqLinkPreview);

    int errorCode = -1;
    if (code == CURLE_OK) {
        if (response) {
            JsonObject *object = NULL;
            JsonParser *parser = openJsonParser(response, &object);
            ParserWrapper wr(parser);
            if (object) {
                errorCode = json_object_get_int_member(object, "error_code");
                if (errorCode == 0) {
                    PreviewWidgetData *preview = new PreviewWidgetData(object);
                    me->ShowPreviewWidget(EPreview, preview);
                    delete preview;
                } else {
                    Log::error_tag(LogTag, "Error code:%d", errorCode);
                }
            }
        } else {
            Log::error_tag(LogTag, "response is NULL");
        }
    } else {
        Log::error_tag(LogTag, "Error sending http request");
    }
    free(response);

    if (errorCode != 0) {
        me->ShowPreviewWidget(EPlaceholder, NULL);
    }
}

void PostComposerScreen::ShowPreviewWidget(PreviewWidgetMode mode, const PreviewWidgetData *preview) {
    if (mode == ENone) {
        RemovePreviewWidget();
    } else {
        if (!mPreviewWidgetLayout) {
            mPreviewWidgetLayout = elm_layout_add(mScrollerBox);
            elm_layout_file_set(mPreviewWidgetLayout, Application::mEdjPath, "post_composer_preview_frame");
            if (mMode == eEDIT || mSharedGroup) { // we can't change or remove a widget of an edited post. Also, we can't change eSHAREGROUP mode.
                elm_object_signal_emit(mPreviewWidgetLayout, "hide_remove_icon", "");
            } else {
                elm_object_signal_callback_add(mPreviewWidgetLayout, "mouse,clicked,*", "remove_icon", on_remove_preview_widget_cb, this);
            }
            if (mTextLayout) {
                elm_box_pack_after(mScrollerBox, mPreviewWidgetLayout, mTextLayout);
            } else {
                elm_box_pack_end(mScrollerBox, mPreviewWidgetLayout);
            }
            evas_object_show(mPreviewWidgetLayout);
        }
        Evas_Object *content = NULL;
        if (mode == EProgress) {
            content = elm_progressbar_add(mPreviewWidgetLayout);
            elm_object_style_set(content, "process_medium");
            evas_object_size_hint_align_set(content, EVAS_HINT_FILL, EVAS_HINT_FILL);
            evas_object_size_hint_weight_set(content, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
            elm_progressbar_pulse_set(content, EINA_TRUE);
            elm_progressbar_pulse(content, EINA_TRUE);
            evas_object_raise(content);
            evas_object_show(content);
        } else if (mode == EPreview && preview) {
            content = elm_layout_add(mPreviewWidgetLayout);
            if (preview->GetImage()) {
                elm_layout_file_set(content, Application::mEdjPath, "post_shared_item");

                Evas_Object *img = elm_image_add(content);
                elm_object_part_content_set(content, "shared_image", img);

                if (!mActionAndData) {
                    mActionAndData = new ActionAndData(nullptr, nullptr);
                }
                mActionAndData->UpdateImageLayoutAsync(preview->GetImage(), img);
            } else {
                elm_layout_file_set(content, Application::mEdjPath, "post_shared_item_without_icon");
            }
            if (preview->GetName()) {
                elm_object_part_text_set(content, "shared_text_author", preview->GetName());
            }
            const char* caption = preview->GetCaption();
            const char* description = preview->GetDescription();
            if (caption && description) {
                elm_object_part_text_set(content, "shared_text_story", preview->GetDescription());
                elm_object_part_text_set(content, "shared_text_message", preview->GetCaption() ? preview->GetCaption() : preview->GetDescription());
            } else if (description) {
                elm_object_part_text_set(content, "shared_text_message", description);
            } else if (caption) {
                elm_object_part_text_set(content, "shared_text_message", caption);
            }
        } else if (mode == EPlaceholder) {
            content = elm_layout_add(mPreviewWidgetLayout);
            elm_layout_file_set(content, Application::mEdjPath, "post_preview_widget_placeholder");
            elm_object_translatable_part_text_set(content, "text_message", "IDS_POST_COMPOSER_LINK_PREVIEW_PLACEHOLDER");
        }
        elm_object_part_content_set(mPreviewWidgetLayout, "swallow.content", content);
    }
}

void PostComposerScreen::on_remove_preview_widget_cb(void *data, Evas_Object *obj, const char *emission, const char *source) {
    Log::debug_tag(LogTag, "Remove link preview.");
    PostComposerScreen *me = static_cast<PostComposerScreen *>(data);
    me->DiscardSharedLink();
    me->ShowPreviewWidget(ENone, NULL);
}

void PostComposerScreen::RemovePreviewWidget() {
    if (mPreviewWidgetLayout) {
        elm_object_signal_callback_del(mPreviewWidgetLayout, "mouse,clicked,*", "remove_icon", on_remove_preview_widget_cb);
        evas_object_hide(mPreviewWidgetLayout);
        elm_box_unpack(mScrollerBox, mPreviewWidgetLayout);
        evas_object_del(mPreviewWidgetLayout);
        mPreviewWidgetLayout = nullptr;
        FacebookSession::GetInstance()->ReleaseGraphRequest(mReqLinkPreview);
        PostChanged(ENothing);
    }
}

void PostComposerScreen::DiscardSharedLink() {
    mDiscardedLinks = eina_list_append(mDiscardedLinks, mSharedLink);
    mSharedLink = nullptr;
}

void PostComposerScreen::DeleteSharedLink() {
    delete[] mSharedLink;
    mSharedLink = nullptr;
}

void PostComposerScreen::DeleteEntryCallbacks()
{
    if (mMessageEntry) {
        evas_object_smart_callback_del(mMessageEntry, "changed,user", TextChangedCb);
        evas_object_smart_callback_del(mMessageEntry, "preedit,changed", TextChangedCb);
    }
}

void PostComposerScreen::SetEntryCallbacks()
{
    if (mMessageEntry) {
        evas_object_smart_callback_add(mMessageEntry, "changed,user", TextChangedCb, this);
        evas_object_smart_callback_add(mMessageEntry, "preedit,changed", TextChangedCb, this);
    }
}

#ifdef _SCROLLER_WORKAROUND_ENABLED_
void PostComposerScreen::StoreScrollPosition()
{
    elm_scroller_region_get(mScroller, &mScrollPosX, &mScrollPosY, &mScrollWidth,&mScrollHeight);
    evas_object_smart_callback_del(mScroller, "scroll", unexpected_scroll_cb);
    evas_object_event_callback_del( mScroller, EVAS_CALLBACK_MOUSE_DOWN, mouse_down_cb );
    evas_object_smart_callback_add(mScroller, "scroll", unexpected_scroll_cb, this);
    evas_object_event_callback_add( mScroller, EVAS_CALLBACK_MOUSE_DOWN, mouse_down_cb, this );
}

void PostComposerScreen::RestoreScrollPosition()
{
    elm_scroller_region_show(mScroller, mScrollPosX, mScrollPosY, mScrollWidth,mScrollHeight);
}

void PostComposerScreen::unexpected_scroll_cb(void *data, Evas_Object *obj, void *event_info) {
    Log::debug_tag(LogTag, "SCROLLING...");
    PostComposerScreen* me = static_cast<PostComposerScreen *>(data);
    me->RestoreScrollPosition();
}

void PostComposerScreen::mouse_down_cb(void *data, Evas *e, Evas_Object *obj, void *event_info) {
    Log::debug_tag(LogTag, "Mouse Down");
    PostComposerScreen* me = static_cast<PostComposerScreen *>(data);
    evas_object_event_callback_del( me->mScroller, EVAS_CALLBACK_MOUSE_DOWN, mouse_down_cb );
    evas_object_smart_callback_del(me->mScroller, "scroll", unexpected_scroll_cb);
}

#endif

void PostComposerScreen::on_image_details_received(void *data, char* str, int code) {
    PostComposerScreen *me = static_cast<PostComposerScreen*>(data);
    if (me) {
        if (code == CURLE_OK) {
            Log::debug("data: %s", str);
            FbResponseImageDetails *imageDetails = FbResponseImageDetails::createFromJson(str);
            if (!imageDetails || !imageDetails->mPhoto) {
                Log::error("Error parsing http response");
            } else if (imageDetails->mError && imageDetails->mError->mMessage) {
                Log::error("Error getting image details, error = %s", imageDetails->mError->mMessage);
            } else {
                Photo* newPhoto = imageDetails->mPhoto;
                Eina_List * list;
                void * list_data;
                Photo *photo = NULL;
                assert(me->mPostToEdit);
                EINA_LIST_FOREACH(me->mPostToEdit->GetPhotoList(), list, list_data) {
                    photo = static_cast<Photo *>(list_data);
                    if (photo && *photo == *newPhoto) {
                        photo->UpdatePhotoDetails(newPhoto);
                        break;
                    }
                }
                Eina_List * listItem;
                void * listData;
                EINA_LIST_FOREACH(SelectedMediaList::Get(), listItem, listData) {
                    PostComposerMediaData *media = static_cast<PostComposerMediaData*>(listData);
                    if (media && !strcmp(photo->GetId(), media->GetMediaId())) {
                        media->SetCaption(photo->GetName());
                        media->SetOriginalCaption(photo->GetName());
                        media->SetPhotoMessageTagsList(photo->GetPhotoMessageTagsList());
                        Evas_Object *caption = elm_object_part_content_get(media->GetCaptionLayout(), "caption");
                        if (caption) {
                            me->RefreshPhotoCaption(media);
                        }
                        if (!media->GetFilePath()) {
                            int w, h;
                            h = photo->GetHeight();
                            w = photo->GetWidth();
                            Evas_Object * imgEvas = media->GetImageLayout();
                            evas_object_size_hint_min_set(imgEvas, R->POST_COMPOSER_IMAGE_SIZE, (w ? R->POST_COMPOSER_IMAGE_SIZE * h / w : 0));
                        }
                        break;
                    }
                }
                delete imageDetails;
            }
        } else {
            Log::error("Error sending http request");
        }
    }
    free(str);
}

void PostComposerScreen::RefreshPhotoCaption(PostComposerMediaData *media) {
    Evas_Object * parent = media->GetCaptionLayout();
    Evas_Object *entry = elm_object_part_content_get(parent, "caption");

    if (media->GetCaption()) {
        std::string captionText = PresenterHelpers::AddFriendTags(media->GetCaption(), media->GetPhotoMessageTagsList(), PresenterHelpers::eComposer);
        elm_entry_entry_set(entry, captionText.c_str());
    }
}

void PostComposerScreen::RequestImageData() {
    Eina_List * listItem;
    void * listData;
    Eina_List *imageIds = NULL;
    assert(mPostToEdit);
    EINA_LIST_FOREACH(mPostToEdit->GetPhotoList(), listItem, listData) {
        Photo * photo = static_cast<Photo*>(listData);
        imageIds = eina_list_append(imageIds, photo->GetId());
    }
    mImageDetailsRequests = FacebookSession::GetInstance()->GetImageDetails(imageIds, on_image_details_received, this);
    eina_list_free(imageIds);
}


PostComposerScreen::PreviewWidgetData::PreviewWidgetData(): mHref(nullptr),mName(nullptr), mCaption(nullptr), mDescription(nullptr), mImage(nullptr) {}

PostComposerScreen::PreviewWidgetData::PreviewWidgetData(JsonObject * object): mHref(nullptr),mName(nullptr), mCaption(nullptr), mDescription(nullptr), mImage(nullptr) {
    if (object) {
        mHref = GetStringFromJson(object, "href");
        mName = GetStringFromJson(object, "name");
        mCaption = GetStringFromJson(object, "caption");
        mDescription = GetStringFromJson(object, "description");
        JsonArray *mediaArray = json_object_get_array_member(object, "media");
        if (mediaArray) {
            int len = json_array_get_length(mediaArray);
            for (int i = 0; i < len; ++i) {
                JsonObject *media = json_array_get_object_element(mediaArray, i);
                if(media) {
                    c_unique_ptr<char> type(GetStringFromJson(media, "type"));
                    if (type && !strcmp(type.get(), "image")) {
                        mImage = GetStringFromJson(media, "src");
                        break;
                    }
                }
            }
        }
    }

}

PostComposerScreen::PreviewWidgetData::~PreviewWidgetData()
{
    free(mHref);
    free(mName);
    free(mCaption);
    free(mDescription);
    free(mImage);
}

void PostComposerScreen::PreviewWidgetData::SetName(const char *name) {
    free(mName);
    mName = SAFE_STRDUP(name);

}

void PostComposerScreen::PreviewWidgetData::SetCaption(const char *caption) {
    free(mCaption);
    mCaption = SAFE_STRDUP(caption);
}

void PostComposerScreen::PreviewWidgetData::SetDescription(const char *description) {
    free(mDescription);
    mDescription = SAFE_STRDUP(description);
}

void PostComposerScreen::PreviewWidgetData::SetImage(const char *image) {
    free(mImage);
    mImage = SAFE_STRDUP(image);
}
