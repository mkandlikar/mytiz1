#include "SelectAlbumScreen.h"
#include "WidgetFactory.h"
#include "SelectAlbumProvider.h"
#include "ActionBase.h"
#include "AlbumPresenter.h"
#include "Photo.h"

#include <utils_i18n.h>

#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "POSTCOMPOSER"


/*
 * Class SelectAlbumScreen
 */
SelectAlbumScreen::SelectAlbumScreen(PostComposerScreen *postComposer, const char* userProfileId, const char* selectedAlbumId) :
        TrackItemsProxyAdapter(postComposer->getMainLayout(), SelectAlbumProvider::GetInstance()), mSelectedAlbumId(NULL),
        mSelectedActionAndData(NULL), mPostComposer(postComposer), mCoverImagesList(NULL){
    Log::debug_tag( LOG_TAG, "SelectAlbumScreen: create Select Album screen." );
    AppEvents::Get().Notify(eSELECT_ALBUM_SCREEN_SHOW_EVENT, nullptr);

    if (selectedAlbumId) {
        mSelectedAlbumId = strdup(selectedAlbumId);
    }

    mLayout = elm_layout_add(mPostComposer->getMainLayout());
    elm_layout_file_set(mLayout, Application::mEdjPath, "post_composer_select_album_screen");
    elm_object_part_content_set(mLayout, "swallow.scroller", GetScroller());
    elm_object_translatable_part_text_set(mLayout, "text.caption", "IDS_SELECT_ALBUM");
    elm_object_signal_callback_add(mLayout, "mouse,clicked,*", "create_album", on_create_album_clicked, this);
    evas_object_show(mLayout);

    mAction = new SelectAlbumAction(this);
    SelectAlbumProvider::GetInstance()->SetUserId(userProfileId);
    RequestData();
}

SelectAlbumScreen::~SelectAlbumScreen() {
    Log::debug_tag( LOG_TAG, "SelectAlbumScreen: destroy SelectAlbumScreen screen." );
    delete mAction;
    free(mSelectedAlbumId);

    void *data;
    EINA_LIST_FREE(mCoverImagesList, data)
    {
        delete static_cast<DownloadCover *>(data);
    }
}

void* SelectAlbumScreen::CreateItem(void* dataForItem, bool isAddToEnd) {
    Album *album = static_cast<Album *>(dataForItem);

    Log::debug_tag( LOG_TAG, "Create item");
    Evas_Object *itemLayout = elm_layout_add(GetDataArea());
    elm_layout_file_set(itemLayout, Application::mEdjPath, "post_composer_select_album_item");
    evas_object_show(itemLayout);

    ActionAndData *actionAndData = new ActionAndData(album, mAction);
    actionAndData->mParentWidget = itemLayout;

    Evas_Object *album_image = evas_object_image_add(evas_object_evas_get(GetDataArea()));
    int swallowW, swallowH;
    edje_object_part_geometry_get(elm_layout_edje_get(itemLayout), "swallow.image", NULL, NULL, &swallowW, &swallowH);

    if (album) {
        Log::debug_tag( LOG_TAG, "ALBUM:%s, with %d photos", album->mName, album->mCount);

        elm_object_part_text_set(itemLayout, "text.album_name", album->mName);
        AlbumPresenter presenter(*album);
        HRSTC_TRNSLTD_PART_TEXT_SET(itemLayout, "text.photos_number", presenter.GenPhotosNumber().c_str());
        if (album->mPicture->mIsSilhouette) {
            int x, y;
            evas_object_image_file_set(album_image, ICON_DIR"/album_no_photos.png", NULL);
            evas_object_image_size_get(album_image, &x, &y);
            Utils::CropImageToFill(album_image, swallowW, swallowH, x, y);
        } else {
            ImageFile * coverImage = new ImageFile(album->mPicture->mUrl, NULL);
            switch (coverImage->GetStatus()) {
            case ImageFile::EDownloaded: {
                int x, y;
                evas_object_image_file_set(album_image, coverImage->GetPath(), NULL);
                evas_object_image_size_get(album_image, &x, &y);
                Utils::CropImageToFill(album_image, swallowW, swallowH, x, y);
                break;
            }
            case ImageFile::ENotDownloaded: {
                DownloadCover * downloader = new DownloadCover(*coverImage, album_image, swallowW, swallowH);
                downloader->Download();
                mCoverImagesList = eina_list_append(mCoverImagesList, downloader);
                break;
            }
            default:
                break;
            }
            coverImage->Release();
        }

        evas_object_show(album_image);
        elm_object_part_content_set(itemLayout, "swallow.image", album_image);

        if (mSelectedAlbumId && !strcmp(album->GetId(), mSelectedAlbumId)) {
            elm_object_signal_emit(itemLayout, "show_checked", "");
            mSelectedActionAndData = actionAndData;
        } else {
            elm_object_signal_emit(itemLayout, "hide_checked", "");
        }
        elm_object_signal_callback_add(itemLayout, "mouse,clicked,*", "*", on_album_clicked, actionAndData);
    }

    if (isAddToEnd) {
        elm_box_pack_end(GetDataArea(), itemLayout);
    } else {
        elm_box_pack_start(GetDataArea(), itemLayout);
    }

    return actionAndData;
}

void SelectAlbumScreen::DeleteItem(void* itemData) {
    ActionAndData *actionAndData = static_cast<ActionAndData *> (itemData);
    elm_box_unpack(GetDataArea(), actionAndData->mParentWidget);
    evas_object_del(actionAndData->mParentWidget);
    elm_object_signal_callback_del(actionAndData->mParentWidget, "mouse,clicked,*", "*", on_album_clicked);
    if (mSelectedActionAndData == actionAndData) {
        mSelectedActionAndData = NULL;
    }
    delete actionAndData;
}

ItemSize* SelectAlbumScreen::GetItemSize(void* itemData) {
    ActionAndData *actionAndData = static_cast<ActionAndData *> (itemData);
    Evas_Coord x, y, weight, height;
    evas_object_geometry_get(actionAndData->mParentWidget, &x, &y, &weight, &height);

    Album *album = static_cast<Album*> (actionAndData->mData);
    return new ItemSize(strdup(album->GetId()), height, weight);
}

void SelectAlbumScreen::on_album_clicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    ActionAndData *actionAndData = static_cast<ActionAndData *> (data);
    Album *album = static_cast<Album*>(actionAndData->mData);
    SelectAlbumAction *action = static_cast<SelectAlbumAction*>(actionAndData->mAction);
    SelectAlbumScreen *me = reinterpret_cast<SelectAlbumScreen*>(action->getScreen());

    if (album) {
        //there was no selected album before and now album is selected
        if (!me->mSelectedAlbumId) {
            me->mSelectedAlbumId = strdup(album->GetId());
            me->mSelectedActionAndData = actionAndData;
            elm_object_signal_emit(actionAndData->mParentWidget, "show_checked", "");
            me->mPostComposer->AlbumSelected(album, false);
        //selected the same album as before. this means that album is unselected.
        } else if(!strcmp(me->mSelectedAlbumId, album->GetId())) {
            free(me->mSelectedAlbumId);
            me->mSelectedAlbumId = NULL;
            me->mSelectedActionAndData = NULL;
            elm_object_signal_emit(actionAndData->mParentWidget, "hide_checked", "");
            me->mPostComposer->AlbumSelected(nullptr, false);
            //selected new album. unselect previously selected and select new.
        } else {
            free(me->mSelectedAlbumId);
            me->mSelectedAlbumId = strdup(album->GetId());
            if (me->mSelectedActionAndData) {
                ActionAndData *prevActionData = me->mSelectedActionAndData;
                elm_object_signal_emit(prevActionData->mParentWidget, "hide_checked", "");
            }
            me->mSelectedActionAndData = actionAndData;
            elm_object_signal_emit(actionAndData->mParentWidget, "show_checked", "");
            me->mPostComposer->AlbumSelected(album, false);
        }
    }
}

void SelectAlbumScreen::on_create_album_clicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    SelectAlbumScreen *me = static_cast<SelectAlbumScreen*>(data);
    //album isn't selected yet. attempt to create new album and then select it.
    PostComposerScreen* postScreen = new PostComposerScreen(me->mPostComposer, eCREATEALBUMANDNOTIFY);
    Application::GetInstance()->AddScreen(postScreen);
    me->mPostComposer->AlbumSelected(nullptr, true);
}

DownloadCover::DownloadCover( ImageFile &albumFile, Evas_Object * coverArea, int width, int height ) : ImageFileDownloader(albumFile),
                                                                                                       mCoverArea(coverArea),
                                                                                                       mWidth(width),
                                                                                                       mHeight(height) {}

void DownloadCover::ImageDownloaded( bool result ) {
    if (result) {
        evas_object_image_file_set(mCoverArea, mImage.GetPath(), NULL);
        int x, y;
        evas_object_image_size_get(mCoverArea, &x, &y);
        Utils::CropImageToFill(mCoverArea, mWidth, mHeight, x, y);
    }
}
