#include "AddTextInputScreen.h"
#include "AddTextScreen.h"
#include "Common.h"
#include "UIRes.h"
#include "WidgetFactory.h"

#include <stdio.h>

#ifdef FEATURE_ADD_TEXT

/**
 * @brief Keeps list of AddTextScreenAndTextData. It used for checking is the object alive or not.
 */
static Eina_List * textDataList;

AddTextScreen::AddTextScreen(PostComposerMediaData * mediaData, AddTextMode mode) :
        ScreenBase(NULL), mSelectedTextImageObject(NULL), mPressedXDelta(-1),
        mPressedYDelta(-1), mOnDeleteArea(false), mTextGestureList(NULL), mIsMoveStarted(false)
{
    mScreenId = ScreenBase::SID_ADD_TEXT;
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "AddTextSetScreen()");

    mMediaData = mediaData;
    mLayout = WidgetFactory::CreateLayoutByGroup(getParentMainLayout(), "add_text_screen");

    elm_object_signal_callback_add(mLayout, "add_text_clicked", "add_remove_text", on_add_text_clicked_cb, this);

    CreateHeader();

    PlacePhoto();

    CreateClipper();

    mLayoutGesture = elm_gesture_layer_add(mLayout);
    elm_gesture_layer_attach(mLayoutGesture, mLayout);

    elm_gesture_layer_cb_set(mLayoutGesture, ELM_GESTURE_N_TAPS, ELM_GESTURE_STATE_END, on_layout_tap_end, this);

    CreateText(mode);

    mBoundaryLeft = UIRes::GetInstance()->ADD_TEXT_BOUNDARY_LEFT;
    mBoundaryRight = UIRes::GetInstance()->SCREEN_SIZE_WIDTH - mBoundaryLeft;
    mBoundaryTop = UIRes::GetInstance()->STATUS_BAR_SIZE_H + UIRes::GetInstance()->HEADER_SIZE_H + mBoundaryLeft;
    mBoundaryBottom = UIRes::GetInstance()->SCREEN_SIZE_HEIGHT - mBoundaryLeft;
}

AddTextScreen::~AddTextScreen()
{
    mMediaData = NULL;
    evas_object_del(mClipper);
    evas_object_del(mTextBorder);
    if (mTextGestureList) {
        eina_list_free(mTextGestureList);
    }
    if (textDataList) {
        eina_list_free(textDataList);
        textDataList = NULL;
    }
}

void AddTextScreen::Push()
{
    ScreenBase::Push();
    // Remove standard blue header
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void AddTextScreen::InsertBefore(ScreenBase * parentScreen) {
    ScreenBase::InsertBefore(parentScreen);
    // Remove standard blue header
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void AddTextScreen::CreateHeader()
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "AddTextSetScreen::CreateHeader");

    Evas_Object * header = WidgetFactory::CreateGrayHeaderWithLogo(mLayout, "IDS_ADD_TEXT",
                                                                   "IDS_DONE", this);
    elm_object_part_content_set(mLayout, "header", header);
    evas_object_raise(header);
}

void AddTextScreen::PlacePhoto()
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "AddTextSetScreen::CreateImage");
    mPhoto = elm_image_add(mLayout);
    elm_image_file_set(mPhoto, mMediaData->GetOriginalPath(), NULL);
    elm_object_part_content_set(mLayout, "image", mPhoto);
}

void AddTextScreen::CreateText(AddTextMode mode)
{
    CreateTextBorder();
    textDataList = NULL;

    Eina_List * textList = mMediaData->GetTextList();
    int listCount = eina_list_count(textList);
    Eina_List * listItem;
    void * listData;
    int counter = 0;
    EINA_LIST_FOREACH(textList, listItem, listData) {
        ++counter;
        PostComposerMediaText * mediaText = static_cast<PostComposerMediaText *>(listData);

        if (counter == listCount && mode == eADD_TEXT_NEW) {

            // If we are adding new text than show text border
            // Set default size and position
            int x = UIRes::GetInstance()->SCREEN_SIZE_WIDTH / 3;
            int y = UIRes::GetInstance()->SCREEN_SIZE_HEIGHT / 2;
            int w = 400;
            int h = 150;
            mediaText->SetPosition(x, y);
            mediaText->SetSize(w, h);

            evas_object_raise(mTextBorder);
            evas_object_resize(mTextBorder, w, h);
            evas_object_move(mTextBorder, x, y);
            evas_object_show(mTextBorder);

            AddTextScreenAndTextData * screenAndTextData = CreateTextImage(mediaText, listItem);
            mSelectedTextImageObject = screenAndTextData->mTextImageObject;

            elm_gesture_layer_cb_set(mLayoutGesture, ELM_GESTURE_N_LINES, ELM_GESTURE_STATE_START, on_text_line_start,
                    screenAndTextData);
            elm_gesture_layer_cb_set(mLayoutGesture, ELM_GESTURE_N_LINES, ELM_GESTURE_STATE_MOVE, on_text_line_move,
                    screenAndTextData);

            elm_gesture_layer_cb_set(mLayoutGesture, ELM_GESTURE_ZOOM, ELM_GESTURE_STATE_MOVE, on_zoom_move,
                    screenAndTextData);
            elm_gesture_layer_cb_set(mLayoutGesture, ELM_GESTURE_ZOOM, ELM_GESTURE_STATE_END, on_zoom_end,
                    screenAndTextData);
            elm_gesture_layer_cb_set(mLayoutGesture, ELM_GESTURE_ZOOM, ELM_GESTURE_STATE_ABORT, on_zoom_abort,
                    screenAndTextData);

#ifdef ROTATE_IMAGE
            elm_gesture_layer_cb_set(mLayoutGesture, ELM_GESTURE_ROTATE, ELM_GESTURE_STATE_MOVE, on_rotate_move,
                    screenAndTextData);
            elm_gesture_layer_cb_set(mLayoutGesture, ELM_GESTURE_ROTATE, ELM_GESTURE_STATE_END, on_rotate_end,
                    screenAndTextData);
            elm_gesture_layer_cb_set(mLayoutGesture, ELM_GESTURE_ROTATE, ELM_GESTURE_STATE_ABORT, on_rotate_abort,
                    screenAndTextData);
#endif
        }
        else {
            CreateTextImage(mediaText, listItem);
        }
    }

}

void AddTextScreen::CreateClipper()
{
    Evas * evas = evas_object_evas_get(mLayout);
    mClipper = evas_object_rectangle_add(evas);
    evas_object_move(mClipper, 0, UIRes::GetInstance()->STATUS_BAR_SIZE_H + UIRes::GetInstance()->HEADER_SIZE_H);
    evas_object_resize(mClipper, UIRes::GetInstance()->SCREEN_SIZE_WIDTH, UIRes::GetInstance()->SCREEN_SIZE_HEIGHT -
            (UIRes::GetInstance()->STATUS_BAR_SIZE_H + UIRes::GetInstance()->HEADER_SIZE_H));
    evas_object_show(mClipper);
}

void AddTextScreen::CreateTextBorder()
{
    Evas * evas = evas_object_evas_get(mLayout);
    mTextBorder = evas_object_image_filled_add(evas);
    evas_object_image_file_set(mTextBorder, ICON_DIR"/PostComposer/text_layer_boundry.png", NULL);
    evas_object_image_border_set(mTextBorder, 3, 3, 3, 3);
    evas_object_clip_set(mTextBorder, mClipper);
}

AddTextScreenAndTextData * AddTextScreen::CreateTextImage(PostComposerMediaText * mediaText, Eina_List * textList)
{
    int x, y, w, h;
    mediaText->GetGeometry(&x, &y, &w, &h);

    char * textImagePath = mediaText->GetImagePath();
    struct stat st = { 0 };

    if (!textImagePath || stat(textImagePath, &st) == -1) {
        int r, g, b;
        mediaText->GetColor(&r, &g, &b);

        char dirName[MAX_FULL_PATH_NAME_LENGTH];
        char * imageStoragePath = app_get_data_path();
        Utils::Snprintf_s(dirName, MAX_FULL_PATH_NAME_LENGTH, "%s%s", imageStoragePath, POST_COMPOSER_TEMP_DIR);
        free(imageStoragePath);

        textImagePath = Utils::DrawText(mediaText->GetText(), 0, w, h, r, g, b, dirName);
        mediaText->SetImagePath(textImagePath);
    }

    Evas_Object * textImageBackground = elm_bg_add(mLayout);
    evas_object_resize(textImageBackground, w, h);
    evas_object_move(textImageBackground, x, y);
    evas_object_show(textImageBackground);
    evas_object_color_set(textImageBackground, 0, 0, 0, 0);
    evas_object_clip_set(textImageBackground, mClipper);

    Evas_Object * textImage = elm_image_add(mLayout);
    elm_image_file_set(textImage, textImagePath, NULL);
    evas_object_resize(textImage, w, h);
    evas_object_move(textImage, x, y);
    evas_object_show(textImage);
    evas_object_clip_set(textImage, mClipper);

    AddTextScreenAndTextData * screenAndTextData = new AddTextScreenAndTextData(this, textImageBackground, textImage,
            textList, mediaText);
    textDataList = eina_list_append(textDataList, screenAndTextData);

    Evas_Object *gesture = elm_gesture_layer_add(textImage);
    mTextGestureList = eina_list_append(mTextGestureList, gesture);
    elm_gesture_layer_attach(gesture, textImage);

    elm_gesture_layer_cb_set(gesture, ELM_GESTURE_N_TAPS, ELM_GESTURE_STATE_END, on_text_tap_end, screenAndTextData);

    elm_gesture_layer_cb_set(gesture, ELM_GESTURE_N_LINES, ELM_GESTURE_STATE_START, on_text_line_start, screenAndTextData);
    elm_gesture_layer_cb_set(gesture, ELM_GESTURE_N_LINES, ELM_GESTURE_STATE_MOVE, on_text_line_move, screenAndTextData);

    elm_gesture_layer_cb_set(gesture, ELM_GESTURE_ZOOM, ELM_GESTURE_STATE_MOVE, on_zoom_move, screenAndTextData);
    elm_gesture_layer_cb_set(gesture, ELM_GESTURE_ZOOM, ELM_GESTURE_STATE_END, on_zoom_end, screenAndTextData);
    elm_gesture_layer_cb_set(gesture, ELM_GESTURE_ZOOM, ELM_GESTURE_STATE_ABORT, on_zoom_abort, screenAndTextData);

#ifdef ROTATE_IMAGE
    elm_gesture_layer_cb_set(gesture, ELM_GESTURE_ROTATE, ELM_GESTURE_STATE_MOVE, on_rotate_move, screenAndTextData);
    elm_gesture_layer_cb_set(gesture, ELM_GESTURE_ROTATE, ELM_GESTURE_STATE_END, on_rotate_end, screenAndTextData);
    elm_gesture_layer_cb_set(gesture, ELM_GESTURE_ROTATE, ELM_GESTURE_STATE_ABORT, on_rotate_abort, screenAndTextData);
#endif

    evas_object_event_callback_add(textImage, EVAS_CALLBACK_DEL, on_text_delete_cb, screenAndTextData);

    return screenAndTextData;
}

void AddTextScreen::on_text_delete_cb(void *data, Evas *e, Evas_Object *obj, void *event_info)
{
    if (data) {
        if (textDataList) {
            textDataList = eina_list_remove(textDataList, data);
        }
        delete static_cast<AddTextScreenAndTextData *>(data);
    }
}

Evas_Event_Flags AddTextScreen::on_text_line_start(void *data, void *event_info)
{
    if (eina_list_data_find(textDataList, data) != data) {
        return EVAS_EVENT_FLAG_NONE;
    }
    AddTextScreenAndTextData * screenAndTextData = static_cast<AddTextScreenAndTextData *>(data);

    // Show "Add text" icon when finger up
    evas_object_event_callback_add(screenAndTextData->mScreen->mLayout, EVAS_CALLBACK_MOUSE_UP, on_mouse_up_cb,
            screenAndTextData);
    evas_object_event_callback_add(screenAndTextData->mTextImageObject, EVAS_CALLBACK_MOUSE_UP, on_mouse_up_cb,
                screenAndTextData);

    if (screenAndTextData->mScreen->mSelectedTextImageObject != screenAndTextData->mTextImageObject) {

        screenAndTextData->mScreen->mMediaData->TextListDemoteList(screenAndTextData->mTextList);

        int x, y, w, h;
        screenAndTextData->mTextData->GetGeometry(&x, &y, &w, &h);
        evas_object_raise(screenAndTextData->mScreen->mTextBorder);
        evas_object_resize(screenAndTextData->mScreen->mTextBorder, w, h);
        evas_object_move(screenAndTextData->mScreen->mTextBorder, x, y);
        evas_object_show(screenAndTextData->mScreen->mTextBorder);

        evas_object_raise(screenAndTextData->mTextImageBackground);
        evas_object_raise(screenAndTextData->mTextImageObject);

        screenAndTextData->mScreen->mSelectedTextImageObject = screenAndTextData->mTextImageObject;

        elm_gesture_layer_cb_set(screenAndTextData->mScreen->mLayoutGesture, ELM_GESTURE_N_LINES,
                ELM_GESTURE_STATE_START, on_text_line_start, screenAndTextData);
        elm_gesture_layer_cb_set(screenAndTextData->mScreen->mLayoutGesture, ELM_GESTURE_N_LINES,
                ELM_GESTURE_STATE_MOVE, on_text_line_move, screenAndTextData);

        elm_gesture_layer_cb_set(screenAndTextData->mScreen->mLayoutGesture, ELM_GESTURE_ZOOM,
                ELM_GESTURE_STATE_MOVE, on_zoom_move, screenAndTextData);
        elm_gesture_layer_cb_set(screenAndTextData->mScreen->mLayoutGesture, ELM_GESTURE_ZOOM,
                ELM_GESTURE_STATE_END, on_zoom_end, screenAndTextData);
        elm_gesture_layer_cb_set(screenAndTextData->mScreen->mLayoutGesture, ELM_GESTURE_ZOOM,
                ELM_GESTURE_STATE_ABORT, on_zoom_abort, screenAndTextData);

#ifdef ROTATE_IMAGE
        elm_gesture_layer_cb_set(screenAndTextData->mScreen->mLayoutGesture, ELM_GESTURE_ROTATE,
                ELM_GESTURE_STATE_MOVE, on_rotate_move, screenAndTextData);
        elm_gesture_layer_cb_set(screenAndTextData->mScreen->mLayoutGesture, ELM_GESTURE_ROTATE,
                ELM_GESTURE_STATE_END, on_rotate_end, screenAndTextData);
        elm_gesture_layer_cb_set(screenAndTextData->mScreen->mLayoutGesture, ELM_GESTURE_ROTATE,
                ELM_GESTURE_STATE_ABORT, on_rotate_abort, screenAndTextData);
#endif
    }

    // Calculate x and y delta

    // Get current cursor position
    Evas *evas = evas_object_evas_get(screenAndTextData->mScreen->mLayout);
    int cursorX, cursorY;
    evas_pointer_output_xy_get(evas, &cursorX, &cursorY);

    int objectX, objectY;
    screenAndTextData->mTextData->GetPosition(&objectX, &objectY);

    screenAndTextData->mScreen->mPressedXDelta = cursorX - objectX;
    screenAndTextData->mScreen->mPressedYDelta = cursorY - objectY;

    // Show "Trash" icon
    if (!screenAndTextData->mScreen->mIsMoveStarted) {
        elm_object_signal_emit(screenAndTextData->mScreen->mLayout, "show_trash", "add_text_set_layout");
        screenAndTextData->mScreen->mIsMoveStarted = true;
    }

    return EVAS_EVENT_FLAG_NONE;
}

Evas_Event_Flags AddTextScreen::on_text_line_move(void *data, void *event_info)
{
    if (eina_list_data_find(textDataList, data) != data) {
        return EVAS_EVENT_FLAG_NONE;
    }
    AddTextScreenAndTextData * screenAndTextData = static_cast<AddTextScreenAndTextData *>(data);

    int objectW, objectH;
    screenAndTextData->mTextData->GetSize(&objectW, &objectH);

    // Get current cursor position
    Evas *evas = evas_object_evas_get(screenAndTextData->mScreen->mLayout);
    int cursorX, cursorY;
    evas_pointer_output_xy_get(evas, &cursorX, &cursorY);

    // TODO Change the logic of text deletion. Possibility to delete text image when this object is over the delete area.
    //      Current logic: when your finger is over the delete area then you can delete this text image.
    if (cursorY >= UIRes::GetInstance()->ADD_TEXT_DELETE_AREA && !screenAndTextData->mScreen->mOnDeleteArea) {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "cursorY >= delete area");
        screenAndTextData->mScreen->mOnDeleteArea = true;
        elm_object_signal_emit(screenAndTextData->mScreen->mLayout, "press_trash", "add_text_set_layout");
        evas_object_resize(screenAndTextData->mTextImageObject, 46, 32);
        evas_object_resize(screenAndTextData->mScreen->mTextBorder, 46, 32);
    }
    else if (cursorY < UIRes::GetInstance()->ADD_TEXT_DELETE_AREA && screenAndTextData->mScreen->mOnDeleteArea) {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "cursorY < delete area");
        screenAndTextData->mScreen->mOnDeleteArea = false;
        elm_object_signal_emit(screenAndTextData->mScreen->mLayout, "show_trash", "add_text_set_layout");
        evas_object_resize(screenAndTextData->mTextImageObject, objectW, objectH);
        evas_object_resize(screenAndTextData->mScreen->mTextBorder, objectW, objectH);
    }

    int moveToX, moveToY;

    // If we are over the delete area than move to the finger position
    if (screenAndTextData->mScreen->mOnDeleteArea) {
        moveToX = cursorX;
        moveToY = cursorY;
    }
    else {
        moveToX = cursorX - screenAndTextData->mScreen->mPressedXDelta;
        moveToY = cursorY - screenAndTextData->mScreen->mPressedYDelta;

        // checking of the boundary conditions
        if (objectW + moveToX < screenAndTextData->mScreen->mBoundaryLeft) {
            moveToX = screenAndTextData->mScreen->mBoundaryLeft - objectW;
        }
        else if (moveToX > screenAndTextData->mScreen->mBoundaryRight) {
            moveToX = screenAndTextData->mScreen->mBoundaryRight;
        }

        if (objectH + moveToY < screenAndTextData->mScreen->mBoundaryTop) {
            moveToY = screenAndTextData->mScreen->mBoundaryTop - objectH;
        }
        else if (moveToY > screenAndTextData->mScreen->mBoundaryBottom) {
            moveToY = screenAndTextData->mScreen->mBoundaryBottom;
        }

    }

    evas_object_move(screenAndTextData->mTextImageBackground, moveToX, moveToY);
    evas_object_move(screenAndTextData->mTextImageObject, moveToX, moveToY);
    evas_object_move(screenAndTextData->mScreen->mTextBorder, moveToX, moveToY);

    screenAndTextData->mTextData->SetPosition(moveToX, moveToY);

    return EVAS_EVENT_FLAG_NONE;
}

Evas_Event_Flags AddTextScreen::on_zoom_move(void *data, void *event_info)
{
    if (eina_list_data_find(textDataList, data) != data) {
        return EVAS_EVENT_FLAG_NONE;
    }
    AddTextScreenAndTextData * screenAndTextData = static_cast<AddTextScreenAndTextData *>(data);

    double zoom = static_cast<Elm_Gesture_Zoom_Info *>(event_info)->zoom;

    int w, h;
    screenAndTextData->mTextData->GetSize(&w, &h);
    double newW = (double) w * zoom;
    double newH = (double) h * zoom;

    if (newH > UIRes::GetInstance()->ADD_TEXT_MIN_TEXT_HEIGHT && newH < UIRes::GetInstance()->ADD_TEXT_MAX_TEXT_HEIGHT) {
        screenAndTextData->mObjectZoom = zoom;
        evas_object_resize(screenAndTextData->mScreen->mTextBorder, newW, newH);
        evas_object_resize(screenAndTextData->mTextImageBackground, newW, newH);
        evas_object_resize(screenAndTextData->mTextImageObject, newW, newH);
    }

    return EVAS_EVENT_FLAG_NONE;
}

Evas_Event_Flags AddTextScreen::on_zoom_end(void *data, void *event_info)
{
    if (eina_list_data_find(textDataList, data) != data) {
        return EVAS_EVENT_FLAG_NONE;
    }
    AddTextScreenAndTextData * screenAndTextData = static_cast<AddTextScreenAndTextData *>(data);

    if (screenAndTextData->mObjectZoom) {
        int w, h;
        screenAndTextData->mTextData->GetSize(&w, &h);
        double newW = (double) w * screenAndTextData->mObjectZoom;
        double newH = (double) h * screenAndTextData->mObjectZoom;

        screenAndTextData->mTextData->SetSize(newW, newH);

        screenAndTextData->mObjectZoom = 0.0;
    }

    return EVAS_EVENT_FLAG_NONE;
}

Evas_Event_Flags AddTextScreen::on_zoom_abort(void *data, void *event_info)
{
    return on_zoom_end(data, event_info);
}

Evas_Event_Flags AddTextScreen::on_rotate_move(void *data, void *event_info)
{
    if (eina_list_data_find(textDataList, data) != data) {
        return EVAS_EVENT_FLAG_NONE;
    }
    AddTextScreenAndTextData * screenAndTextData = static_cast<AddTextScreenAndTextData *>(data);
    double angle = static_cast<Elm_Gesture_Rotate_Info *>(event_info)->angle;
    double baseAngle = static_cast<Elm_Gesture_Rotate_Info *>(event_info)->base_angle;

    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "basic angle: %f, rotation angle: %f, delta: %f", baseAngle, angle, angle - baseAngle);

    double newAngle = screenAndTextData->mTextData->GetAngle() + angle - baseAngle;

    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "late angle: %f, new angle: %f",
            screenAndTextData->mTextData->GetAngle(), newAngle);

    int x, y, w, h;
    screenAndTextData->mTextData->GetGeometry(&x, &y, &w, &h);

    Evas_Map * map = evas_map_new(4);
    evas_map_util_points_populate_from_object(map, screenAndTextData->mScreen->mTextBorder);
    evas_map_util_rotate(map, newAngle, x + (w / 2), y + (h / 2));

    evas_object_map_set(screenAndTextData->mScreen->mTextBorder, map);
    evas_object_map_enable_set(screenAndTextData->mScreen->mTextBorder, EINA_TRUE);

    evas_object_map_set(screenAndTextData->mTextImageBackground, map);
    evas_object_map_enable_set(screenAndTextData->mTextImageBackground, EINA_TRUE);

    evas_object_map_set(screenAndTextData->mTextImageObject, map);
    evas_object_map_enable_set(screenAndTextData->mTextImageObject, EINA_TRUE);
    evas_map_free(map);

    return EVAS_EVENT_FLAG_NONE;
}

Evas_Event_Flags AddTextScreen::on_rotate_end(void *data, void *event_info)
{
    if (eina_list_data_find(textDataList, data) != data) {
        return EVAS_EVENT_FLAG_NONE;
    }
    AddTextScreenAndTextData * screenAndTextData = static_cast<AddTextScreenAndTextData *>(data);

    double angle = static_cast<Elm_Gesture_Rotate_Info *>(event_info)->angle;
    double baseAngle = static_cast<Elm_Gesture_Rotate_Info *>(event_info)->base_angle;

    double newAngle = screenAndTextData->mTextData->GetAngle() + angle - baseAngle;
    screenAndTextData->mTextData->SetAngle(newAngle);

    return EVAS_EVENT_FLAG_NONE;
}

Evas_Event_Flags AddTextScreen::on_rotate_abort(void *data, void *event_info)
{
    return on_rotate_end(data, event_info);
}

void AddTextScreen::on_add_text_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "AddTextSetScreen::on_add_text_clicked_cb");
    AddTextScreen * screen = static_cast<AddTextScreen *>(data);

    elm_object_signal_callback_del(screen->mHeaderWidget, "mouse,clicked", "header_right_button",
            ScreenBase::confirm_cb );
    evas_object_smart_callback_del(screen->mHeaderCancelBtnWidget, "clicked", ScreenBase::cancel_cb);
    elm_object_signal_callback_del(screen->mLayout, "add_text_clicked", "add_remove_text", on_add_text_clicked_cb);
    screen->LayoutGestureTurnOff();
    screen->TextGestureTurnOff(false);

    AddTextInputScreen * addTextScreen = new AddTextInputScreen(screen->mMediaData);
    Application::GetInstance()->ReplaceScreen(addTextScreen);
}

Evas_Event_Flags AddTextScreen::on_layout_tap_end(void *data, void *event_info)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "AddTextScreen::on_layout_tap_end");
    AddTextScreen * screen = static_cast<AddTextScreen *>(data);
    evas_object_hide(screen->mTextBorder);

    screen->mSelectedTextImageObject = NULL;

    screen->LayoutGestureTurnOff();

    return EVAS_EVENT_FLAG_NONE;
}

Evas_Event_Flags AddTextScreen::on_text_tap_end(void *data, void *event_info)
{
    if (eina_list_data_find(textDataList, data) != data) {
        return EVAS_EVENT_FLAG_NONE;
    }
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "AddTextScreen::on_text_tap_end");
    AddTextScreenAndTextData * screenAndTextData = static_cast<AddTextScreenAndTextData *>(data);

    elm_object_signal_callback_del(screenAndTextData->mScreen->mHeaderWidget, "mouse,clicked", "header_right_button",
            ScreenBase::confirm_cb );
    evas_object_smart_callback_del(screenAndTextData->mScreen->mHeaderCancelBtnWidget, "clicked", ScreenBase::cancel_cb);

    evas_object_event_callback_del(screenAndTextData->mTextImageObject, EVAS_CALLBACK_MOUSE_UP, on_mouse_up_cb);
    elm_object_signal_callback_del(screenAndTextData->mScreen->mLayout, "add_text_clicked", "add_remove_text",
                on_add_text_clicked_cb);
    screenAndTextData->mScreen->LayoutGestureTurnOff();
    screenAndTextData->mScreen->TextGestureTurnOff(true);

    AddTextInputScreen * addTextScreen = new AddTextInputScreen(
            screenAndTextData->mScreen->mMediaData, screenAndTextData->mTextList);
    Application::GetInstance()->ReplaceScreen(addTextScreen);

    return EVAS_EVENT_FLAG_NONE;
}

void AddTextScreen::on_mouse_up_cb(void *data, Evas *e, Evas_Object *obj, void *event_info)
{
    if (eina_list_data_find(textDataList, data) != data) {
        return;
    }
    AddTextScreenAndTextData * screenAndTextData = static_cast<AddTextScreenAndTextData *>(data);

    evas_object_event_callback_del(screenAndTextData->mScreen->mLayout, EVAS_CALLBACK_MOUSE_UP, on_mouse_up_cb);
    evas_object_event_callback_del(screenAndTextData->mTextImageObject, EVAS_CALLBACK_MOUSE_UP, on_mouse_up_cb);

    // Show "Add text" icon
    elm_object_signal_emit(screenAndTextData->mScreen->mLayout, "show_add_text", "add_text_set_layout");
    screenAndTextData->mScreen->mIsMoveStarted = false;

    if (screenAndTextData->mScreen->mOnDeleteArea) {
        screenAndTextData->mScreen->mOnDeleteArea = false;
        screenAndTextData->mScreen->LayoutGestureTurnOff();
        evas_object_hide(screenAndTextData->mScreen->mTextBorder);
        delete screenAndTextData->mTextData;
        screenAndTextData->mScreen->mMediaData->TextListRemoveList(screenAndTextData->mTextList);
        evas_object_del(screenAndTextData->mTextImageBackground);
        evas_object_del(screenAndTextData->mTextImageObject);
    }
}

void AddTextScreen::LayoutGestureTurnOff()
{
    evas_object_event_callback_del(mLayout, EVAS_CALLBACK_MOUSE_UP, on_mouse_up_cb);

    elm_gesture_layer_cb_set(mLayoutGesture, ELM_GESTURE_N_LINES, ELM_GESTURE_STATE_START, NULL, NULL);
    elm_gesture_layer_cb_set(mLayoutGesture, ELM_GESTURE_N_LINES, ELM_GESTURE_STATE_MOVE, NULL, NULL);

    elm_gesture_layer_cb_set(mLayoutGesture, ELM_GESTURE_ZOOM, ELM_GESTURE_STATE_MOVE, NULL, NULL);
    elm_gesture_layer_cb_set(mLayoutGesture, ELM_GESTURE_ZOOM, ELM_GESTURE_STATE_END, NULL, NULL);
    elm_gesture_layer_cb_set(mLayoutGesture, ELM_GESTURE_ZOOM, ELM_GESTURE_STATE_ABORT, NULL, NULL);

#ifdef ROTATE_IMAGE
    elm_gesture_layer_cb_set(mLayoutGesture, ELM_GESTURE_ROTATE, ELM_GESTURE_STATE_MOVE, NULL, NULL);
    elm_gesture_layer_cb_set(mLayoutGesture, ELM_GESTURE_ROTATE, ELM_GESTURE_STATE_END, NULL, NULL);
    elm_gesture_layer_cb_set(mLayoutGesture, ELM_GESTURE_ROTATE, ELM_GESTURE_STATE_ABORT, NULL, NULL);
#endif
}

void AddTextScreen::TextGestureTurnOff(bool isEditMode)
{
    void * listData;
    Eina_List * listItem;
    EINA_LIST_FOREACH(mTextGestureList, listItem, listData) {
        Evas_Object * gesture = static_cast<Evas_Object *>(listData);

        if(!isEditMode)
        {
            elm_gesture_layer_cb_set(gesture, ELM_GESTURE_N_TAPS, ELM_GESTURE_STATE_END, NULL, NULL);
        }
        elm_gesture_layer_cb_set(gesture, ELM_GESTURE_N_LINES, ELM_GESTURE_STATE_START, NULL, NULL);
        elm_gesture_layer_cb_set(gesture, ELM_GESTURE_N_LINES, ELM_GESTURE_STATE_MOVE, NULL, NULL);

        elm_gesture_layer_cb_set(gesture, ELM_GESTURE_ZOOM, ELM_GESTURE_STATE_MOVE, NULL, NULL);
        elm_gesture_layer_cb_set(gesture, ELM_GESTURE_ZOOM, ELM_GESTURE_STATE_END, NULL, NULL);
        elm_gesture_layer_cb_set(gesture, ELM_GESTURE_ZOOM, ELM_GESTURE_STATE_ABORT, NULL, NULL);

#ifdef ROTATE_IMAGE
        elm_gesture_layer_cb_set(gesture, ELM_GESTURE_ROTATE, ELM_GESTURE_STATE_MOVE, NULL, NULL);
        elm_gesture_layer_cb_set(gesture, ELM_GESTURE_ROTATE, ELM_GESTURE_STATE_END, NULL, NULL);
        elm_gesture_layer_cb_set(gesture, ELM_GESTURE_ROTATE, ELM_GESTURE_STATE_ABORT, NULL, NULL);
#endif
    }
}

void AddTextScreen::CancelCallback(void *data, Evas_Object *obj, void *event_info)
{
    ConfirmCallback(data, obj, NULL, NULL);
}

void AddTextScreen::ConfirmCallback(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    ProgressBarShow();
    ApplyChanges();
    Pop();
}

bool AddTextScreen::HandleBackButton()
{
    ConfirmCallback(NULL, NULL, NULL, NULL);
    return true;
}

void AddTextScreen::ApplyChanges()
{
    // Remove all callbacks. To avoid user interactions while applying process
    elm_object_signal_callback_del(mLayout, "add_text_clicked", "add_remove_text", on_add_text_clicked_cb);
    TextGestureTurnOff(false);

    // Delete old result picture if it's not the original picture
    if (mMediaData->GetFilePath() != mMediaData->GetOriginalPath()) {
        Utils::DeleteMediaFileFromDB(mMediaData->GetMediaId());
        remove(mMediaData->GetFilePath());
        free((void *)mMediaData->GetFilePath());
        mMediaData->SetFilePath(mMediaData->GetOriginalPath());
    }

    if (!eina_list_count(mMediaData->GetTextList())) {
        if (mMediaData->GetThumbnailPath() != mMediaData->GetOriginalThumbnailPath()) {
            remove(mMediaData->GetThumbnailPath());
            free((void *) mMediaData->GetThumbnailPath());
            mMediaData->SetThumbnailPath(mMediaData->GetOriginalThumbnailPath());
        }

        elm_image_file_set(mMediaData->GetImageObject(), mMediaData->GetOriginalThumbnailPath(), NULL);
        return;
    }

    // Geometry of part for image
    int photoPartX, photoPartY, photoPartW, photoPartH;
    evas_object_geometry_get(mPhoto, &photoPartX, &photoPartY, &photoPartW, &photoPartH);

    // Photo image real size
    // Get evas_object_image object to get the pixel data and save the image to a file
    Evas_Object * originalPhotoObject = elm_image_object_get(mPhoto);
    int photoRealW, photoRealH;
    evas_object_image_size_get(originalPhotoObject, &photoRealW, &photoRealH);

    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "photo object photoPartX: %d, photoPartY: %d, photoPartW: %d, photoPartH: %d", photoPartX, photoPartY, photoPartW, photoPartH);
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "source photo photoRealW: %d, photoRealH: %d", photoRealW, photoRealH);

    // 1) Calculate size factor
    // 2) Calculate size of photo object
    // 3) Calculate position of photo object
    // FOREACH text image
    // 4) Calculate text position relative to the photo object
    // 5) Calculate new size for text object and Draw it with new parameters
    // 6) Draw text image at the photo image.

    int photoObjectX, photoObjectY, photoObjectW, photoObjectH;
    double imageScaleFactor;
    if (photoRealW > photoRealH) {
        photoObjectW = photoPartW;
        imageScaleFactor = (double) photoRealW / photoPartW;
        photoObjectH = photoRealH / imageScaleFactor;

        photoObjectX = photoPartX;
        // Calculate Y offset
        photoObjectY = (photoPartH - photoObjectH) / 2 + photoPartY;
    }
    else {
        photoObjectH = photoPartH;
        imageScaleFactor = (double) photoRealH / photoPartH;
        photoObjectW = photoRealW / imageScaleFactor;

        photoObjectY = photoPartY;
        // Calculate Y offset
        photoObjectX = (photoPartW - photoObjectW) / 2 + photoPartX;
    }
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "photo object W: %d, H: %d", photoObjectW, photoObjectH);

    // Create evas_image_object for original picture on which we work
    Evas_Object * resultPhotoElm = elm_image_add(mLayout);

    Eina_List * listItem;
    void * listData;
    EINA_LIST_FOREACH(mMediaData->GetTextList(), listItem, listData) {
        PostComposerMediaText * mediaText = static_cast<PostComposerMediaText *>(listData);
        int textX, textY;
        mediaText->GetPosition(&textX, &textY);

        // Calculate position relative to photo
        int textRelativePhotoX = textX - photoObjectX;
        int textRelativePhotoY = textY - photoObjectY;

        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "relative position to photo X: %d, Y: %d", textRelativePhotoX, textRelativePhotoY);

        int textR, textG, textB;
        mediaText->GetColor(&textR, &textG, &textB);

        // Draw text on the image
        elm_image_file_set(resultPhotoElm, mMediaData->GetFilePath(), NULL);
        Evas_Object * resultPhotoEvas = elm_image_object_get(resultPhotoElm);
        double textZoomFactor = mediaText->GetZoomFactor();

        char dirName[MAX_FULL_PATH_NAME_LENGTH];
        char * imageStoragePath = Utils::GetInternalStorageDirectory(STORAGE_DIRECTORY_IMAGES);
        Utils::Snprintf_s(dirName, MAX_FULL_PATH_NAME_LENGTH, "%s/%s", imageStoragePath, POST_COMPOSER_TEMP_DIR);
        free(imageStoragePath);

        char * resultFilePath = Utils::DrawTextOnImage(resultPhotoEvas, imageScaleFactor, mediaText->GetText(), textZoomFactor,
                textRelativePhotoX, textRelativePhotoY, textR, textG, textB, dirName);
        if (mMediaData->GetFilePath() != mMediaData->GetOriginalPath()) {
            remove(mMediaData->GetFilePath());
            free((void *)mMediaData->GetFilePath());
        }
        mMediaData->SetFilePath(resultFilePath);
    }

    // Generate thumbnail for new photo
    Utils::InsertMediaFileToMediaDB(mMediaData->GetFilePath(), set_new_thumbnail, mMediaData);

    elm_image_file_set(mMediaData->GetImageObject(), mMediaData->GetThumbnailPath(), NULL);
}

bool AddTextScreen::set_new_thumbnail(media_info_h media, void *user_data)
{
    char * thumbnailPath = NULL;
    int ret = media_info_get_thumbnail_path(media, &thumbnailPath);
    if (ret != MEDIA_CONTENT_ERROR_NONE) {
        return true;
    }

    PostComposerMediaData * mediaData = static_cast<PostComposerMediaData *>(user_data);
    if (mediaData->GetThumbnailPath() != mediaData->GetOriginalThumbnailPath()) {
        remove(mediaData->GetThumbnailPath());
        free((void *)mediaData->GetThumbnailPath());
    }
    mediaData->SetThumbnailPath(thumbnailPath);

    char * mediaId = NULL;
    ret = media_info_get_media_id(media, &mediaId);
    if (ret != MEDIA_CONTENT_ERROR_NONE) {
        return true;
    }
    free((void *)mediaData->GetMediaId());
    mediaData->SetMediaId(mediaId);

    unsigned long long fileSize;
    ret = media_info_get_size(media, &fileSize);
    if (ret == MEDIA_CONTENT_ERROR_NONE) {
        mediaData->SetFileSize(fileSize);
    }

    return true;
}

AddTextScreenAndTextData::AddTextScreenAndTextData(AddTextScreen * screen, Evas_Object * imageTextBackground,
        Evas_Object * imageTextObject, Eina_List * textList, PostComposerMediaText * textData) :
                mObjectZoom(0.0)
{
    mScreen = screen;
    mTextImageBackground = imageTextBackground;
    mTextImageObject = imageTextObject;
    mTextList = textList;
    mTextData = textData;
}

AddTextScreenAndTextData::~AddTextScreenAndTextData()
{
    mScreen = NULL;
    mTextImageBackground = NULL;
    mTextImageObject = NULL;
    mTextList = NULL;
    mTextData = NULL;
}

#endif  /* FEATURE_ADD_TEXT */
