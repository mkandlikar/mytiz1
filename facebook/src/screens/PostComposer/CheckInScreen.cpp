#include "CheckInScreen.h"
#include "WidgetFactory.h"
#include "CheckInProvider.h"
#include "LocationManager.h"
#include "PostComposerScreen.h"
#include "EventCreatePage.h"
#include "NearByPlacesScreen.h"

#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "CHECK-IN"

const unsigned int CheckInScreen::DEFAULT_ITEMS_WINDOW_SIZE = 50;
const unsigned int CheckInScreen::DEFAULT_ITEMS_WINDOW_STEP = 4;
const unsigned int CheckInScreen::DEFAULT_ITEMS_INITIAL_WINDOW_SIZE = 25;


void CheckInScreen::OpenScreen(ScreenBase *screen, CheckinScreenMode mode, PCPlace *selectedPlace) {
    double latitude, longitude;
    LocationManager::LocationManagerError err = Application::GetInstance()->GetLocationManager()->GetLocation(&latitude, &longitude);
    if (err == LocationManager::EErrorNone) {
        CheckInScreen *me = new CheckInScreen(screen, mode, selectedPlace);
        Application::GetInstance()->AddScreen(me);
    } else if(err == LocationManager::EErrorSettingOff) {
        WidgetFactory::ShowErrorDialog(screen->getMainLayout(), "IDS_CHECK_IN_SERVICE_OFF");
    } else {
        WidgetFactory::ShowErrorDialog(screen->getMainLayout(), "IDS_CHECK_IN_DATA_NOT_AVAILABLE");
    }
}

void CheckInScreen::onItemClicked (void *data, Evas_Object *obj, const char *emission, const char *source) {
    ActionAndData *actionAndData = static_cast<ActionAndData*> (data);
    Place *place = static_cast<Place*> (actionAndData->mData);
    dlog_print(DLOG_INFO, LOG_TAG, "Place selected:%s", place->mName);
    static_cast<CheckInScreen *> (actionAndData->mAction->getScreen())->PlaceSelected(place);
}

void CheckInScreen::PlaceSelected(Place *place) {
    if(mMode == EFromPostComposer) {
        mPostComposer->PlaceSelected(place);
        Pop();
#ifdef EVENT_NATIVE_VIEW
    } else if (mMode == EFromEventScreen && mEventCreatePage) {
        mEventCreatePage->PlaceSelected(place);
        Pop();
#endif
    } else if (mMode == EFromNearByPlacesScreen){
        mNearByPlacesScreen->PlaceSelected(place);
    } else {
        mPostComposer = new PostComposerScreen(NULL, eDEFAULT, nullptr, nullptr, nullptr, nullptr, false);
        mPostComposer->PlaceSelected(place);
        Application::GetInstance()->ReplaceScreen(mPostComposer);
    }
}

void CheckInScreen::onRemoverClicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    dlog_print(DLOG_INFO, LOG_TAG, "onRemoveClicked!!%s:%s", emission, source);
    CheckInScreen *me = static_cast<CheckInScreen *> (data);
    if (!strcmp(source, "remove")) {
        CheckInScreen *me = static_cast<CheckInScreen *>(data);
        me->PlaceSelected(NULL);
    } else if (!strcmp(source, "rect.text")) {
        me->Pop();
    }
}

CheckInScreen::CheckInScreen(ScreenBase *screen, CheckinScreenMode mode, PCPlace *selectedPlace) : ScreenBase(NULL),
        TrackItemsProxyAdapter(NULL, CheckInProvider::GetInstance()),
        mSearchFieldGroup(NULL), mSearchField(NULL),
        mMode(mode), mSelectedPlace(selectedPlace),
        mLatitude(0.0f), mLongitude(0.0f) {
    mScreenId = ScreenBase::SID_CHECKIN;
    mEventCreatePage = NULL;
    mNearByPlacesScreen = NULL;
    if(mMode == EFromPostComposer) {
        mPostComposer = static_cast<PostComposerScreen *> (screen);
#ifdef EVENT_NATIVE_VIEW
    } else if (mMode == EFromEventScreen) {
    	mEventCreatePage = static_cast<EventCreatePage *> (screen);
#endif
    } else if (mMode == EFromNearByPlacesScreen){
        mNearByPlacesScreen = static_cast<NearByPlacesScreen *> (screen);
    } else {
        mPostComposer = NULL;
    }

    mLayout = elm_layout_add(getParentMainLayout());
    ApplyScroller(mLayout, true);
    elm_layout_file_set(mLayout, Application::mEdjPath, "check_in_screen");

//header
    mSearchFieldGroup = elm_layout_add(mLayout);
    elm_layout_file_set(mSearchFieldGroup, Application::mEdjPath, "check_in_search_field");

    mSearchField = elm_entry_add(mSearchFieldGroup);
    elm_entry_single_line_set(mSearchField, EINA_TRUE);
    elm_entry_scrollable_set(mSearchField, EINA_TRUE);
    elm_entry_prediction_allow_set(mSearchField, EINA_FALSE);
    elm_entry_text_style_user_push(mSearchField, ScreenBase::R->CHECKIN_SEARCH_FIELD_TEXT_FORMAT);
    elm_entry_editable_set(mSearchField, EINA_TRUE);
    elm_entry_cnp_mode_set(mSearchField, ELM_CNP_MODE_PLAINTEXT);
    elm_entry_input_panel_return_key_type_set(mSearchField, ELM_INPUT_PANEL_RETURN_KEY_TYPE_DONE);
    char* buf = WidgetFactory::WrapByFormat2( SANS_REGULAR_9298A4_FORMAT, R->HINT_SEARCH_FOR_PLACES_TEXT_SIZE, i18n_get_text("IDS_HINT_SEARCH_FOR_PLACES"));
    if (buf) {
        elm_object_part_text_set(mSearchField, "elm.guide", buf);
        delete[] buf;
    }

    evas_object_smart_callback_add(mSearchField, "changed", FilterChangedCb, this);
    evas_object_smart_callback_add(mSearchField, "activated", on_done_btn_clicked_cb, this);

    elm_layout_content_set(mSearchFieldGroup, "swallow.text", mSearchField);
    elm_object_signal_callback_add(mSearchFieldGroup, "mouse,clicked,*", "icon.clear", onClearClicked, this);

    Evas_Object *header = WidgetFactory::CreateGrayHeaderWithLogo(mLayout, mSearchFieldGroup, NULL, this);
    elm_object_signal_emit(header, "hide_right_button", "header");
    elm_object_part_content_set(mLayout, "swallow.top_toolbar", header);

//swallow.selected_item
    if (mSelectedPlace) {
        Evas_Object *selectedItem = elm_layout_add(mLayout);
        elm_layout_file_set(selectedItem, Application::mEdjPath, "check_in_selected_item");
        elm_object_signal_callback_add(selectedItem, "mouse,clicked,*", "*", onRemoverClicked, this);
        elm_object_part_content_set(mLayout, "swallow.selected_item", selectedItem);
        elm_object_part_text_set(selectedItem, "text", mSelectedPlace->mName.c_str());
    } else {
        elm_object_signal_emit(mLayout, "hide_selected_item", "");
    }

    elm_object_part_content_set(mLayout, "swallow.list", GetScroller());

    mAction = new ActionBase(this);

    ProxySettings *settings = GetProxySettings();
    settings->wndSize = DEFAULT_ITEMS_WINDOW_SIZE;
    settings->wndStep = DEFAULT_ITEMS_WINDOW_STEP;
    settings->initialWndSize = DEFAULT_ITEMS_INITIAL_WINDOW_SIZE;
    SetProxySettings(settings);

    Application::GetInstance()->GetLocationManager()->GetLocation(&mLatitude, &mLongitude);
    dlog_print(DLOG_INFO, LOG_TAG, "CheckInScreen::c: %05.2f, %05.2f", mLatitude, mLongitude);

    CheckInProvider::GetInstance()->SetSearchCriteria(mLatitude, mLongitude, NULL);
    RequestData();
}

void CheckInScreen::Push() {
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(elm_naviframe_top_item_get(getNf()), EINA_FALSE, EINA_FALSE);
}

CheckInScreen::~CheckInScreen() {
    delete mAction;
}

void CheckInScreen::ConfirmCallback (void *data, Evas_Object *obj, const char *emission, const char *source) {
    Pop();
}

void* CheckInScreen::CreateItem(void* dataForItem, bool isAddToEnd) {
    Place *place = static_cast<Place *> (dataForItem);

    dlog_print(DLOG_INFO, LOG_TAG, "Create item");
    Evas_Object *itemLayout = elm_layout_add(GetDataArea());
    elm_layout_file_set(itemLayout, Application::mEdjPath, "check_in_list_item");
    evas_object_show(itemLayout);

    ActionAndData *actionAndData = new ActionAndData(place, mAction);
    actionAndData->mParentWidget = itemLayout;

    if (place) {
    //text
       elm_object_part_text_set(itemLayout, "text", place->mName);

    //min size
        Evas_Coord maxw, maxh;
        Evas_Object* edjeLayout = elm_layout_edje_get(itemLayout);
        edje_object_size_min_calc ((Evas_Object *)edjeLayout, &maxw, &maxh);
        evas_object_size_hint_min_set(itemLayout, maxw, maxh);

    //callback on click
        elm_object_signal_callback_add(itemLayout, "mouse,clicked,*", "item", onItemClicked, actionAndData);
    }

    if (isAddToEnd) {
        elm_box_pack_end(GetDataArea(), itemLayout);
    } else {
        elm_box_pack_start(GetDataArea(), itemLayout);
    }

    return actionAndData;
}

void CheckInScreen::DeleteItem(void* itemData) {
    ActionAndData *actionAndData = static_cast<ActionAndData *> (itemData);
    elm_object_signal_callback_del(actionAndData->mParentWidget, "mouse,clicked,*", "item", onItemClicked);
    elm_box_unpack(GetDataArea(), actionAndData->mParentWidget);
    evas_object_del(actionAndData->mParentWidget);
    delete actionAndData;
}

ItemSize* CheckInScreen::GetItemSize(void* itemData) {
    ActionAndData *actionAndData = static_cast<ActionAndData *> (itemData);
    Evas_Coord x, y, weight, height;
    evas_object_geometry_get(actionAndData->mParentWidget, &x, &y, &weight, &height);

    Place *place = static_cast<Place*> (actionAndData->mData);
    return new ItemSize(strdup(place->GetId()), height, weight);
}

void CheckInScreen::onClearClicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    CheckInScreen *me = static_cast<CheckInScreen *>(data);
    elm_object_part_text_set(me->mSearchField, NULL, "");
}

void CheckInScreen::on_done_btn_clicked_cb(void *data, Evas_Object *obj, void *event_info)
{
    CheckInScreen *me = static_cast<CheckInScreen *>(data);
    if (me) {
        elm_entry_input_panel_hide(me->mSearchField);
    }
}

void CheckInScreen::FilterChangedCb(void *data, Evas_Object *obj, void *event_info)
{
    CheckInScreen *me = static_cast<CheckInScreen *>(data);
    const char *text = elm_object_part_text_get(me->mSearchField, NULL);
    if (text) {
        dlog_print(DLOG_INFO, LOG_TAG, "Search text :%s", text);
        me->UpdateClearButton();

        me->EraseWindowData();
        CheckInProvider::GetInstance()->SetSearchCriteria(me->mLatitude, me->mLongitude, text);
        me->RequestData();
    }
}

void CheckInScreen::UpdateClearButton() {
    if (elm_entry_is_empty(mSearchField)) {
        edje_object_signal_emit(elm_layout_edje_get(mSearchFieldGroup), "hide_clear_button", "");
    } else {
        edje_object_signal_emit(elm_layout_edje_get(mSearchFieldGroup), "show_clear_button", "");
    }
}
