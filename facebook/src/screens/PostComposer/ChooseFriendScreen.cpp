/*
 * ChooseGroupScreen.cpp
 *
 *  Created on: Sep 9, 2015
 *      Author: ruibezruch
 */

#include "ChooseFriendScreen.h"
#include "ChooseFriendProvider.h"
#include "Common.h"
#include "ConnectivityManager.h"
#include "Friend.h"
#include "Log.h"
#include "Separator.h"
#include "WidgetFactory.h"

const int ChooseFriendScreen::DEFAULT_INIT_ITEMS_SIZE = 25;
const int ChooseFriendScreen::DEFAULT_MAX_ITEMS_SIZE = 80;

ChooseFriendScreen::ChooseFriendScreen(PostComposerScreen *postComposer) :
        ScreenBase(NULL), TrackItemsProxyAdapter(NULL, ChooseFriendProvider::GetInstance())
{
    mScreenId = ScreenBase::SID_CHOOSE_FRIEND;
    mAction = new ActionBase(this);
    mPostComposerScreen = postComposer;
    mSearchEntry = NULL;

    mLayout = WidgetFactory::CreateChooseScreenLayout(this, getParentMainLayout(), "IDS_CHOOSE_FRIEND");

    elm_object_translatable_part_text_set(mLayout, "error_text", "IDS_NO_FRIENDS_FOUND");

    elm_object_signal_emit(mLayout, "clear.btn", "hide");

    ApplyScroller(mLayout, true);
    elm_object_part_content_set(mLayout, "content", GetScroller());

    mSearchEntry = WidgetFactory::CreateChooseScreenEntry(mLayout);

    if (mSearchEntry) {
        Utils::SetKeyboardFocus(mSearchEntry, true);
        elm_entry_cnp_mode_set(mSearchEntry, ELM_CNP_MODE_PLAINTEXT);
        elm_entry_input_panel_show(mSearchEntry);
    }

    evas_object_smart_callback_add(mSearchEntry, "changed", on_search_btn_clicked_cb, this);
    evas_object_smart_callback_add(mSearchEntry, "activated", on_search_keypad_btn_clicked_cb, this);
    elm_object_signal_callback_add(mLayout, "mouse,clicked,*", "entry_search_*", on_search_btn_clicked_signal, this);
    elm_object_signal_callback_add(mLayout, "mouse,clicked,*", "entry_clear_*", on_clear_btn_clicked_cb, this);

    ProxySettings *settings = GetProxySettings();
    settings->wndSize = DEFAULT_MAX_ITEMS_SIZE;
    settings->initialWndSize = DEFAULT_INIT_ITEMS_SIZE;
    SetProxySettings(settings);

    RequestData();
}

ChooseFriendScreen::~ChooseFriendScreen()
{
    delete mAction;

    ChooseFriendProvider::GetInstance()->EraseData();

    evas_object_del(mSearchEntry); mSearchEntry = NULL;
}

void ChooseFriendScreen::on_search_keypad_btn_clicked_cb(void *data, Evas_Object *obj, void *event_info)
{
    ChooseFriendScreen *me = static_cast<ChooseFriendScreen*>(data);

    if (me && me->mSearchEntry) {
        elm_entry_input_panel_hide(me->mSearchEntry);
    }
}

void ChooseFriendScreen::on_search_btn_clicked_cb(void *data, Evas_Object *obj, void *event_info)
{
    ChooseFriendScreen *me = static_cast<ChooseFriendScreen*>(data);

    me->OnSearchBtnClicked();
}

void ChooseFriendScreen::on_search_btn_clicked_signal(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    ChooseFriendScreen *me = static_cast<ChooseFriendScreen*>(data);

    me->OnSearchBtnClicked();
}

void ChooseFriendScreen::OnSearchBtnClicked()
{
    const char *str = elm_entry_entry_get(mSearchEntry);
    if (str && !strcmp(str, "")) {
        elm_object_signal_emit(mLayout, "clear.btn", "hide");
    } else {
        elm_object_signal_emit(mLayout, "clear.btn", "show");
    }

    char * searchStr = elm_entry_markup_to_utf8(elm_entry_entry_get(mSearchEntry));

    if (searchStr) {
        ChooseFriendProvider::GetInstance()->SetFindText(searchStr);
        free(searchStr);
        EraseWindowData();
        ShiftDown();
    }

    SetNoDataViewVisibility(ChooseFriendProvider::GetInstance()->GetDataCount() == 0);
}

void ChooseFriendScreen::on_clear_btn_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    ChooseFriendScreen *me = static_cast<ChooseFriendScreen*>(data);
    if (me && me->mSearchEntry) {
        elm_entry_entry_set(me->mSearchEntry, "");
    }
}

void ChooseFriendScreen::Push()
{
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void* ChooseFriendScreen::CreateItem(void* dataForItem, bool isAddToEnd)
{
    ActionAndData *data = NULL;
    GraphObject *check = static_cast<GraphObject*>(dataForItem);
    switch (check->GetGraphObjectType()) {
    case GraphObject::GOT_FRIEND: {
        Friend *friends = static_cast<Friend*>(dataForItem);
        if (friends) {
            data = new ActionAndData(friends, mAction);
            data->mParentWidget = CreateFriendItem(data, GetDataArea(), isAddToEnd);
        }
    }
        break;
    case GraphObject::GOT_SEPARATOR: {
        Separator *separator = static_cast<Separator*>(dataForItem);
        data = new ActionAndData(separator, mAction);
        data->mParentWidget = WidgetFactory::CreateSeparatorItem(GetDataArea(), isAddToEnd, separator->mCustomText);
    }
        break;
    default:
        break;
    }
    if (data != NULL && data->mParentWidget == NULL) {
        delete data;
        data = NULL;
    }
    return data;
}

Evas_Object *ChooseFriendScreen::CreateFriendItem(ActionAndData *action_data, Evas_Object* parent, bool isAddToEnd)
{
    Evas_Object *list_item = WidgetFactory::CreateChooseScreenItem(parent, action_data);

    elm_object_signal_callback_add(list_item, "mouse,clicked,*", "item_spacer", on_item_clicked_cb, action_data);

    if (isAddToEnd) {
        elm_box_pack_end(parent, list_item);
    } else {
        elm_box_pack_start(parent, list_item);
    }

    return list_item;
}

void ChooseFriendScreen::on_item_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    Friend *friends = static_cast<Friend*>(action_data->mData);
    ChooseFriendScreen *me = static_cast<ChooseFriendScreen*>(action_data->mAction->getScreen());

    if (me->mPostComposerScreen) {
        me->mPostComposerScreen->SetSelectedFriendsTimeline(friends->GetId(), friends->mName.c_str());
        me->mPostComposerScreen->SetPhotoAlbumButtonMode(me->mPostComposerScreen->EHidden);
    }

    me->Pop();
}

void ChooseFriendScreen::SetDataAvailable(bool isAvailable)
{
    SetNoDataViewVisibility(!isAvailable);
}

void ChooseFriendScreen::SetNoDataViewVisibility(bool isVisible)
{
    if (ConnectivityManager::Singleton().IsConnected()) { //No data visibility is meaningful when device is in connected mode only
        if (isVisible) {
            elm_object_signal_emit(mLayout, "error.show", "");
        } else {
            elm_object_signal_emit(mLayout, "error.hide", "");
        }
    } else {
        elm_object_signal_emit(mLayout, "error.hide", "");
        ShowConnectionError(false);
    }
}

