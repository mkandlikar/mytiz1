#include "Application.h"
#include "ConnectivityManager.h"
#include "CSmartPtr.h"
#include "FacebookSession.h"
#include "Log.h"
#include "OperationManager.h"
#include "OwnFriendsProvider.h"
#include "PCPrivacyModel.h"
#include "SimplePostOperation.h"
#include "Utils.h"
#include "WidgetFactory.h"

#include <app_i18n.h>
#include <cassert>

#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "PRIVACYMODEL"

 /*
 * Class PCPrivacyOption
 */
void PCPrivacyOption::Update(const char *id, const char *description, bool isCurrentlySelected) {
    Log::debug_tag(LOG_TAG, "PCPrivacyOption::Update(mId: %s, mType: %s, selected:%d)", mId, mType, isCurrentlySelected);

    if (id) {
        delete []mId;
        mId = new char[strlen(id) + 1];
        eina_strlcpy(mId, id, strlen(id) + 1);
    }

    Log::debug_tag(LOG_TAG, "mDescription: %s", mDescription);
    if (description) {
        delete []mDescription;
        mDescription = new char[strlen(description) + 1];
        eina_strlcpy(mDescription, description, strlen(description) + 1);
    }

    mIsCurrentlySelected = isCurrentlySelected;
}

const char *PCPrivacyOption::GenIconPath(bool isSmall) {
    char *icon = nullptr;
    if (isSmall) {
        int size = strlen(mIcon) + strlen(KSmallIconPostfix) + 1;
        icon = new char [size];
        Utils::Snprintf_s(icon, size, "%s%s", mIcon, KSmallIconPostfix);
    } else {
        int size = strlen(mIcon) + strlen(KBigIconPostfix) + 1;
        icon = new char [size];
        Utils::Snprintf_s(icon, size, "%s%s", mIcon, KBigIconPostfix);
    }
    return icon;
}

/*
 * Class PCPrivacyModel
 */

const unsigned int PCPrivacyModel::MAX_EXCEPTED_FRIENDS = 25;
const unsigned int PCPrivacyModel::MAX_SPECIFIC_FRIENDS = 25;

PCPrivacyModel::PCPrivacyModel() : mOptions(nullptr), mPrivacyId(nullptr), mValue(EVUnknown),
        mDeniedFriends(nullptr), mAllowedFriends(nullptr), mPrivacyOption(nullptr),
        mLastActivePrivacyOption(nullptr), mObservers(nullptr), mTaggedFriends(nullptr), mIsRefreshEnabled(true), mFriends(nullptr), mState(ENotReady) {
    //ToDo: subscribe for updates later, but now we assume that friends list is already downloaded.
    UpdateFriendsList();
    //    FriendsProvider::GetInstance()->Subscribe(this);

    mAsyncJob = ecore_job_add(start_request_cb, this);
}

PCPrivacyModel::~PCPrivacyModel() {
    Application::GetInstance()->mPrivacyOptionsData->RemoveObserver(this);

    if (mAsyncJob) {
        ecore_job_del(mAsyncJob);
    }

    ClearTaggedFriends();

    eina_list_free(mDeniedFriends);
    eina_list_free(mAllowedFriends);
    delete []mPrivacyId;
    eina_list_free(mObservers);
    void *listData;
    EINA_LIST_FREE(mOptions, listData) {
        delete static_cast<PCPrivacyOption*>(listData);
    }
    EINA_LIST_FREE(mFriends, listData) {
        (static_cast<Friend *> (listData))->Release();
    }
    AppEvents::Get().Unsubscribe(eINTERNET_CONNECTION_CHANGED, this);
}

void PCPrivacyModel::start_request_cb(void *data) {
    PCPrivacyModel *me = static_cast<PCPrivacyModel *>(data);
    me->mAsyncJob = nullptr;
    me->StartGetPrivacyOptionsRequest();
}

void PCPrivacyModel::StartGetPrivacyOptionsRequest() {
    Eina_List *privacyOptions = Application::GetInstance()->mPrivacyOptionsData->GetPrivacyOptions();
    if (mIsRefreshEnabled && privacyOptions) {
        UpdateOptions(privacyOptions);
        mState = ECachedData;
        NotifyObservers(IPCPrivacyObserver::EPrivacyOptionsRefreshed);
    }

    Application::GetInstance()->mPrivacyOptionsData->GetPrivacyOptionsFromFB(this);
    AppEvents::Get().Subscribe(eINTERNET_CONNECTION_CHANGED, this);
}

void PCPrivacyModel::Update(AppEventId eventId, void *data) {
    switch (eventId) {
    case eINTERNET_CONNECTION_CHANGED: {
        Log::debug_tag(LOG_TAG, "PCPrivacyModel::Update = eINTERNET_CONNECTION_CHANGED");
        if(ConnectivityManager::Singleton().IsConnected() && mState != EUpdatedData) {
            Application::GetInstance()->mPrivacyOptionsData->GetPrivacyOptionsFromFB(this);
        }
        break;
    }
    default:
        break;
    }
}

const char *PCPrivacyModel::GenDescriptionForCustomDenied() {
    char *str = nullptr;
    const char *friends = GenDeniedFriends();
    if (friends) {
        str = WidgetFactory::WrapByFormat(i18n_get_text("IDS_POST_COMP_FRIENDS_EXCEPT_ONE"), friends);
        delete [] friends;
    } else {
        int size = strlen(i18n_get_text("IDS_FRIENDS_EXCEPT")) + 1;
        str = new char [size];
        eina_strlcpy(str, i18n_get_text("IDS_FRIENDS_EXCEPT"), size);
    }
    return str;
}

const char *PCPrivacyModel::GenDescriptionForCustomAllowed() {
    const char *friends = GenAllowedFriends();
    if (friends) {
        return friends;
    } else {
        int size = strlen(i18n_get_text("IDS_FRIENDS_SPECIFIC")) + 1;
        char *str = new char[size];
        eina_strlcpy(str, i18n_get_text("IDS_FRIENDS_SPECIFIC"), size);
        return str;
    }
}

char *PCPrivacyModel::GenFriendsList(Eina_List* friends, bool names) {
//Calc required size first
    int size = 0;
    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(friends, l, listData) {
        Friend * fr = static_cast<Friend *>(listData);
        size += (names ? fr->mName.length() : strlen(fr->GetId())) + 2;
    }

    char *str = nullptr;
    listData = eina_list_nth(friends, 0);
    if (listData) {
        str = new char[size + 3];
        Friend * fr = static_cast<Friend *>(listData);
        eina_strlcpy(str, (names ? fr->mName.c_str() : fr->GetId()), size + 3);
        Eina_List *second = eina_list_nth_list(friends, 1);
        EINA_LIST_FOREACH(second, l, listData) {
            char text [100];
            Friend * fr = static_cast<Friend *>(listData);
            if (names) {
                Utils::Snprintf_s(text, 100, ", %s", fr->mName.c_str());
            } else {
                Utils::Snprintf_s(text, 100, ",%s", fr->GetId());
            }
            eina_strlcat(str, text, size + 3);
        }
        Log::debug_tag(LOG_TAG, "GenFriendsList() -> %s", str);
    }
    return str;
}

char *PCPrivacyModel::GenDeniedFriends() {
    char *friends = nullptr;
    if (mDeniedFriends && eina_list_count(mDeniedFriends)) {
        friends = GenFriendsList(mDeniedFriends, true);
    }
    return friends;
}

char *PCPrivacyModel::GenAllowedFriends() {
    char *friends = nullptr;
    if (mAllowedFriends && eina_list_count(mAllowedFriends)) {
        friends = GenFriendsList(mAllowedFriends, true);
    }
    return friends;
}

void PCPrivacyModel::PrivacyOptionSelected(PCPrivacyOption *privacyOption) {
    if (!strcmp(privacyOption->GetType(), PRIVACY_CUSTOM_MANUAL_DENIED) && !eina_list_count(mDeniedFriends)) {
        SetPrivacyOption(GetStandardOptionByType(PRIVACY_ALL_FRIENDS), true);
    } else if (!strcmp(privacyOption->GetType(), PRIVACY_CUSTOM_MANUAL_ALLOWED) && !eina_list_count(mAllowedFriends)) {
        SetPrivacyOption(GetStandardOptionByType(PRIVACY_SELF), true);
    } else {
        SetPrivacyOption(privacyOption, true);
    }
}

void PCPrivacyModel::SetPrivacyOption(PCPrivacyOption *po, bool manual) {
    if (mPrivacyOption != po) {
        mPrivacyOption = po;
        if (!strcmp(mPrivacyOption->GetType(), PRIVACY_EVERYONE)) {
            mValue = EVEveryone;
        } else if (!strcmp(mPrivacyOption->GetType(), PRIVACY_ALL_FRIENDS)) {
            mValue = EVAllFriends;
        } else if (!strcmp(mPrivacyOption->GetType(), PRIVACY_SELF)) {
            mValue = EVSelf;
        } else if (!strcmp(mPrivacyOption->GetType(), PRIVACY_CUSTOM_MANUAL_DENIED)) {
            mValue = EVCustomExcepted;
            const char *newDescription = GenDescriptionForCustomDenied();
            mPrivacyOption->Update(nullptr, newDescription, true);
            delete [] newDescription;
        } else if (!strcmp(mPrivacyOption->GetType(), PRIVACY_CUSTOM_MANUAL_ALLOWED)) {
            mValue = EVCustomSpecific;
            const char *newDescription = GenDescriptionForCustomAllowed();
            mPrivacyOption->Update(nullptr, newDescription, true);
            delete [] newDescription;
        } else {
            mValue = EVUnknown;
        }
        NotifyObservers(manual ? IPCPrivacyObserver::EPrivacyOptionManuallyChanged : IPCPrivacyObserver::EPrivacyOptionChangedByProgram);
    }
}

bool PCPrivacyModel::DeniedFriendSelected(Friend *fr, bool manual) {
    if (AddDeniedFriend(fr)) {
        PCPrivacyOption *custom = GetStandardOptionByType(PRIVACY_CUSTOM_MANUAL_DENIED);
        const char *newDescription = GenDescriptionForCustomDenied();
        custom->Update(nullptr, newDescription, true);
        delete [] newDescription;
        NotifyObservers(IPCPrivacyObserver::EExceptedFriendsChanged);
        if (!strcmp(mPrivacyOption->GetType(), PRIVACY_ALL_FRIENDS) || !strcmp(mPrivacyOption->GetType(), PRIVACY_CUSTOM_MANUAL_DENIED)) {
            SetPrivacyOption(custom, manual);
        }
        return true;
    } else {
        return false;
    }

}

bool PCPrivacyModel::AllowedFriendSelected(Friend *fr, bool manual) {
    if (AddAllowedFriend(fr)) {
        PCPrivacyOption *custom = GetStandardOptionByType(PRIVACY_CUSTOM_MANUAL_ALLOWED);
        const char *newDescription = GenDescriptionForCustomAllowed();
        custom->Update(nullptr, newDescription, true);
        delete [] newDescription;
        NotifyObservers(IPCPrivacyObserver::ESpecificFriendsChanged);
        if (!strcmp(mPrivacyOption->GetType(), PRIVACY_SELF) || !strcmp(mPrivacyOption->GetType(), PRIVACY_CUSTOM_MANUAL_ALLOWED)) {
            SetPrivacyOption(custom, manual);
        }
        return true;
    } else {
        return false;
    }
}

void PCPrivacyModel::DeniedFriendUnselected(Friend *fr) {
    RemoveDeniedFriend(fr);
    PCPrivacyOption *custom = GetStandardOptionByType(PRIVACY_CUSTOM_MANUAL_DENIED);
    const char *newDescription = GenDescriptionForCustomDenied();
    custom->Update(nullptr, newDescription, true);
    delete [] newDescription;
    NotifyObservers(IPCPrivacyObserver::EExceptedFriendsChanged);
    if (!strcmp(mPrivacyOption->GetType(), PRIVACY_CUSTOM_MANUAL_DENIED)) {
        SetPrivacyOption(GetStandardOptionByType(eina_list_count(mDeniedFriends) ? PRIVACY_CUSTOM_MANUAL_DENIED : PRIVACY_ALL_FRIENDS), true);
    }
}

void PCPrivacyModel::AllowedFriendUnselected(Friend *fr) {
    RemoveAllowedFriend(fr);
    PCPrivacyOption *custom = GetStandardOptionByType(PRIVACY_CUSTOM_MANUAL_ALLOWED);
    const char *newDescription = GenDescriptionForCustomAllowed();
    custom->Update(nullptr, newDescription, true);
    delete [] newDescription;
    NotifyObservers(IPCPrivacyObserver::ESpecificFriendsChanged);
    if (!strcmp(mPrivacyOption->GetType(), PRIVACY_CUSTOM_MANUAL_ALLOWED)) {
        SetPrivacyOption(GetStandardOptionByType(eina_list_count(mAllowedFriends) ? PRIVACY_CUSTOM_MANUAL_ALLOWED : PRIVACY_SELF), true);
    }
}

bool PCPrivacyModel::AddDeniedFriend(Friend *fr) {
    if (eina_list_count(mDeniedFriends) == MAX_EXCEPTED_FRIENDS) {
        NotifyObservers(IPCPrivacyObserver::EExceptedFriendsLimitExceeded);
        return false;
    } else {
        mDeniedFriends = eina_list_append(mDeniedFriends, fr);
        return true;
    }
}

bool PCPrivacyModel::AddAllowedFriend(Friend *fr) {
    if (eina_list_count(mAllowedFriends) == MAX_SPECIFIC_FRIENDS) {
        NotifyObservers(IPCPrivacyObserver::ESpecificFriendsLimitExceeded);
        return false;
    } else {
        mAllowedFriends = eina_list_append(mAllowedFriends, fr);
        return true;
    }
}

void PCPrivacyModel::RemoveDeniedFriend(Friend *fr) {
    mDeniedFriends = eina_list_remove(mDeniedFriends, fr);
}

void PCPrivacyModel::RemoveAllowedFriend(Friend *fr) {
    mAllowedFriends = eina_list_remove(mAllowedFriends, fr);
}

char *PCPrivacyModel::GenPrivacyString() {
    char *res = nullptr;
    if (mPrivacyOption) {
        if (mDeniedFriends && AreDeniedFriends()) {
            res = GenCustomPrivacy();
        } else if (mAllowedFriends && AreAllowedFriends()) {
            res = GenCustomPrivacy();
        } else if (mPrivacyOption->GetId()) {
            res = strdup(mPrivacyOption->GetId());
        }
    }
    return res;
}

bool PCPrivacyModel::AreDeniedFriends() const {
    return (!strcmp(mPrivacyOption->GetType(), PRIVACY_CUSTOM_MANUAL_DENIED) && eina_list_count(mDeniedFriends));
}

bool PCPrivacyModel::AreAllowedFriends() const {
    return (!strcmp(mPrivacyOption->GetType(), PRIVACY_CUSTOM_MANUAL_ALLOWED) && eina_list_count(mAllowedFriends));
}

Eina_List *PCPrivacyModel::GetDeniedFriends() const {
    return AreDeniedFriends() ? eina_list_clone(mDeniedFriends) : nullptr;
}

Eina_List *PCPrivacyModel::GetAllowedFriends() const {
    return AreAllowedFriends() ? eina_list_clone(mAllowedFriends) : nullptr;
}

char *PCPrivacyModel::GenCustomPrivacy() {
    Eina_List *allowed = nullptr;
    Eina_List *denied = nullptr;
    Eina_List *l;
    void *listData;

    if (mDeniedFriends && mAllowedFriends && eina_list_count(mDeniedFriends) && eina_list_count(mAllowedFriends)) {
        Log::error("GenCustomPrivacy() - ERROR: both 'denied' and 'allowed' lists are present!");
    } else if (mAllowedFriends && eina_list_count(mAllowedFriends)) {
        EINA_LIST_FOREACH(mAllowedFriends, l, listData) {
            Friend *fr = static_cast<Friend *>(listData);
            allowed = eina_list_append(allowed, strdup(fr->GetId()));
        }
        denied = eina_list_append(denied, strdup(""));
    } else if (mDeniedFriends && eina_list_count(mDeniedFriends)) {
        EINA_LIST_FOREACH(mDeniedFriends, l, listData) {
            Friend *fr = static_cast<Friend *>(listData);
            denied = eina_list_append(denied, strdup(fr->GetId()));
        }
        allowed = eina_list_append(allowed, strdup(PRIVACY_ALL_FRIENDS));
    }

    char *privacy = FacebookSession::BuildPrivacyValue(CUSTOM, allowed, denied);

    EINA_LIST_FOREACH(allowed, l, listData) {
        free((char*)listData);
    }
    eina_list_free(allowed);

    EINA_LIST_FOREACH(denied, l, listData) {
        free((char*)listData);
    }
    eina_list_free(denied);

    return privacy;
}

void PCPrivacyModel::NotifyObservers(IPCPrivacyObserver::PrivacyEvent event) {
    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(mObservers, l, listData) {
        IPCPrivacyObserver *observer = static_cast<IPCPrivacyObserver *> (listData);
        observer->PrivacyChanged(event);
    }
}

void PCPrivacyModel::UpdateDefaultPrivacyOptionOnFB() {
    if (mLastActivePrivacyOption != mPrivacyOption) {
        if (!strcmp(mPrivacyOption->GetType(), PRIVACY_CUSTOM_MANUAL_DENIED) ||
            !strcmp(mPrivacyOption->GetType(), PRIVACY_CUSTOM_MANUAL_ALLOWED)) {
            c_unique_ptr<char> privacy(GenCustomPrivacy());
            OperationManager::GetInstance()->AddOperation(MakeSptr<SimplePostOperation>(SimplePostOperation::OT_Set_Custom_Privacy_Option_As_Default,
                                                                                        privacy ? privacy.get() : "",
                                                                                        nullptr,
                                                                                        std::list<Friend>(),
                                                                                        mPrivacyOption->GetType(),
                                                                                        nullptr));
        } else {
            const char* privacyOptionId = mPrivacyOption->GetId();
            OperationManager::GetInstance()->AddOperation(MakeSptr<SimplePostOperation>(SimplePostOperation::OT_Set_Privacy_Option_As_Default,
                                                                                        privacyOptionId ? privacyOptionId : "",
                                                                                        nullptr,
                                                                                        std::list<Friend>(),
                                                                                        mPrivacyOption->GetType(),
                                                                                        nullptr));
        }
    }
}

void PCPrivacyModel::AppendOption(PCPrivacyOption *option, PrivacyOption *newOp) {
    if (newOp) {
        option->Update(newOp->GetId(), newOp->GetDescription(), newOp->GetIsSelected());
    }
    mOptions = eina_list_append(mOptions, option);
    if (option->GetIsSelected()) {
        mLastActivePrivacyOption = option;
        SetPrivacyOption(option, false);
    }
}

void PCPrivacyModel::UpdateOptions(Eina_List *newOptions) {
    PCPrivacyOption *option = nullptr;
    Eina_List *l;
    void *listData;

    EINA_LIST_FREE(mOptions, listData) {
        delete static_cast<PCPrivacyOption*>(listData);
    }

    EINA_LIST_FOREACH(newOptions, l, listData) {
        PrivacyOption *newOp = static_cast<PrivacyOption *> (listData);
        if (!strcmp(newOp->GetType(), PRIVACY_EVERYONE)) {
            option = new PCPrivacyOption(nullptr, i18n_get_text("IDS_PUBLIC"), PRIVACY_EVERYONE, ICON_DIR "/privacy_public", true);
            AppendOption(option, newOp);
        } else if (!strcmp(newOp->GetType(), PRIVACY_ALL_FRIENDS)) {
            option = new PCPrivacyOption(nullptr, i18n_get_text("IDS_FRIENDS"), PRIVACY_ALL_FRIENDS, ICON_DIR "/privacy_friends", false);
            AppendOption(option, newOp);
        } else if (!strcmp(newOp->GetType(), PRIVACY_SELF)) {
            option = new PCPrivacyOption(nullptr, i18n_get_text("IDS_ONLY_ME"), PRIVACY_SELF, ICON_DIR "/privacy_private", false);
            AppendOption(option, newOp);

            //add option for Excepted Friends
            option = new PCPrivacyOption(nullptr, i18n_get_text("IDS_FRIENDS_EXCEPT"), PRIVACY_CUSTOM_MANUAL_DENIED, ICON_DIR "/privacy_except", false);
            AppendOption(option);

            //add option for Specific Friends
            option = new PCPrivacyOption(nullptr, i18n_get_text("IDS_FRIENDS_SPECIFIC"), PRIVACY_CUSTOM_MANUAL_ALLOWED, ICON_DIR "/privacy_specific", false);
            AppendOption(option);
        } else {
            if (!strcmp(newOp->GetType(), PRIVACY_FRIENDLIST) && strstr(newOp->GenIconPath(false), "/mapPin.png")) {
                option = new PCPrivacyOption(newOp->GetId(), newOp->GetDescription(), newOp->GetType(), POST_COMPOSER_ICON_DIR "/privacy_location", newOp->GetIsSelected());
            } else {
                option = new PCPrivacyOption(newOp->GetId(), newOp->GetDescription(), newOp->GetType(), ICON_DIR "/privacy_custom", newOp->GetIsSelected());
            }
            AppendOption(option);
        }
    }
}

PCPrivacyOption *PCPrivacyModel::GetActiveOption() {
    PCPrivacyOption *option = nullptr;
    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(mOptions, l, listData) {
        PCPrivacyOption *op = (PCPrivacyOption *) listData;
        if (op->GetIsSelected()) {
               option = op;
               break;
        }
    }
    return option;
}

PCPrivacyOption *PCPrivacyModel::GetStandardOptionByType(const char *type) {
    PCPrivacyOption *option = nullptr;
    if (type) {
        Eina_List *l;
        void *listData;
        EINA_LIST_FOREACH(mOptions, l, listData) {
            PCPrivacyOption *op = (PCPrivacyOption *) listData;
            if (!strcmp(op->GetType(), type)) {
                option = op;
                break;
            }
        }
    }
    return option;
}

PCPrivacyOption *PCPrivacyModel::GetStandardOption(PCPrivacyValue privacyValue) {
    const char *type = nullptr;
    switch (privacyValue) {
    case EVEveryone:
        type = PRIVACY_EVERYONE;
        break;
    case EVAllFriends:
        type = PRIVACY_ALL_FRIENDS;
        break;
    case EVSelf:
        type = PRIVACY_SELF;
        break;
    case EVCustomExcepted:
        type = PRIVACY_CUSTOM_MANUAL_DENIED;
        break;
    case EVCustomSpecific:
        type = PRIVACY_CUSTOM_MANUAL_ALLOWED;
        break;
    default:
        break;
    }
    return GetStandardOptionByType(type);
}

PCPrivacyOption *PCPrivacyModel::SetOptionFromPostPrivacy(Privacy *postPrivacy) {
    assert(postPrivacy);
    PCPrivacyOption *option = nullptr;
    if (postPrivacy->GetDeny()) {
        SetDeniedFriendsFromStr(postPrivacy->GetDeny());
        option = GetStandardOptionByType(PRIVACY_CUSTOM_MANUAL_DENIED);
    } else {
        Eina_List *l;
        void *listData;
        EINA_LIST_FOREACH(mOptions, l, listData) {
            PCPrivacyOption *op = static_cast<PCPrivacyOption *> (listData);
            if (postPrivacy->CompareWithOption(op)) {
                option = op;
                break;
            }
        }
    }

    //The GetAllow() list is non-empty for "Specific Friends" as well as for other Friendlist-like options - such as "Self-employed" or "Ahmedabad, India area".
    if (!option && postPrivacy->GetAllow()) {
        SetAllowedFriendsFromStr(postPrivacy->GetAllow());
        option = GetStandardOptionByType(PRIVACY_CUSTOM_MANUAL_ALLOWED);
    }

    if (option) {
        SetPrivacyOption(option, false);
    }
    return option;
}

bool PCPrivacyModel::IsPostPrivacyChanged(Privacy *postPrivacy) {
    assert(postPrivacy);
    PCPrivacyOption *option = GetPrivacyOption();
    if (option) {
        return !postPrivacy->CompareWithOption(option);
    } else { //option must be not NULL when Model is fully initialized. If it's NULL then this means that model isn't initialized yet, Let's consider this as privacy isn't changed.
        return false;
    }
}

void PCPrivacyModel::UpdatePrivacyOptions(Eina_List *privacyOptions) {
    if (mIsRefreshEnabled) {
        UpdateOptions(privacyOptions);
        mState = EUpdatedData;
        NotifyObservers(IPCPrivacyObserver::EPrivacyOptionsRefreshed);
        AppEvents::Get().Unsubscribe(eINTERNET_CONNECTION_CHANGED, this);
    }
}

void PCPrivacyModel::EnableRefresh(bool enable) {
    mIsRefreshEnabled = enable;
}

void PCPrivacyModel::UpdateFriendsList() {
    void *listData;
    EINA_LIST_FREE(mFriends, listData) {
        (static_cast<Friend *> (listData))->Release();
    }
    Eina_Array* friends = OwnFriendsProvider::GetInstance()->GetData();
    for (int i = 0; i < eina_array_count(friends); ++i){
        Friend *fr = static_cast<Friend*> (eina_array_data_get(friends, i));
        if (fr) {
            fr->AddRef();
            mFriends = eina_list_append(mFriends, fr);
        }
    }
    mFriends = eina_list_sort(mFriends, 0, sort_cb);
}

Friend *PCPrivacyModel::GetFriendById(const char *id) const {
    Friend *ret = nullptr;
    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(mFriends, l, listData) {
        Friend *fr = static_cast<Friend *> (listData);
        assert(fr);
        if (!strcmp(id, fr->GetId())) {
            ret = fr;
            break;
        }
    }
    return ret;
}

int PCPrivacyModel::sort_cb(const void *d1, const void *d2) {
   const Friend *fr1 = static_cast<const Friend *> (d1);
   const Friend *fr2 = static_cast<const Friend *> (d2);
   return(strcmp(fr1->mName.c_str(), fr2->mName.c_str()));
}

//Set denied friends from string. E.g. "100014606628576,100014498284216,100012088479327"
void PCPrivacyModel::SetDeniedFriendsFromStr(const char *friends) {
    assert(friends);
    mDeniedFriends = eina_list_free(mDeniedFriends); //remove previously denied friends if any.
    char *str = SAFE_STRDUP(friends);
    char *pch = strtok (str, ",");
    while (pch) {
        Friend *fr = GetFriendById(pch);
        if (fr) {
            DeniedFriendSelected(fr, false);
        }

        pch = strtok (nullptr, ",");
    }
    free(str);
}

//Set allowed friends from string. E.g. "100014606628576,100014498284216,100012088479327"
void PCPrivacyModel::SetAllowedFriendsFromStr(const char *friends) {
    assert(friends);
    mAllowedFriends = eina_list_free(mAllowedFriends); //remove previously allowed friends if any.
    char *str = SAFE_STRDUP(friends);
    char *pch = strtok (str, ",");
    while (pch) {
        Friend *fr = GetFriendById(pch);
        if (fr) {
            AllowedFriendSelected(fr, false);
        }

        pch = strtok (nullptr, ",");
    }
    free(str);
}

Eina_List *PCPrivacyModel::FindExceptedFriendsWhoAreTagged() {
    Eina_List *res = nullptr;
    Eina_List *deniedFriends = GetDeniedFriends();
    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(deniedFriends, l, listData) {
        Friend *fr = static_cast<Friend *> (listData);
        if (eina_list_data_find(mTaggedFriends, fr)) {
            res = eina_list_append(res, fr);
        }
    }
    eina_list_free(deniedFriends);
    return res;
}

bool PCPrivacyModel::AreThereTaggedFriends() {
    return (eina_list_count(mTaggedFriends));
}

void PCPrivacyModel::SetTaggedFriendsFromPost(Post * post)
{
    assert(post);

    const Eina_List *friends = post->GetTaggedFriends();
    const Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(friends, l, listData) {
        Friend *taggedFriend = static_cast<Friend *> (listData);
        Friend *fr = OwnFriendsProvider::GetInstance()->GetFriendById(taggedFriend->GetId());
        if (fr) {
            AppendTaggedFriend(fr);
        } else { //looks like ex-friend, but add them anyway.
            AppendTaggedFriend(taggedFriend);
        }
    }
}

void PCPrivacyModel::AppendTaggedFriend(Friend* fr) {
    assert(fr);
    if (!ReplaceTaggedFriend(fr)) {
        fr->AddRef();
        mTaggedFriends = eina_list_append(mTaggedFriends, fr);
    }
}

void PCPrivacyModel::RemoveTaggedFriend(Friend* fr) {
    if (ReplaceTaggedFriend(fr)) {
        fr->Release();
        mTaggedFriends = eina_list_remove(mTaggedFriends, fr);
    }
}

bool PCPrivacyModel::ReplaceTaggedFriend(Friend* fr) {
    bool isFound = false;
    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(mTaggedFriends, l, listData) {
        Friend *oldFr = static_cast<Friend *> (listData);
        if (*oldFr == *fr) {
            if (oldFr != fr) {
                eina_list_data_set(l, fr);
                fr->AddRef();
                oldFr->Release();
            }
            isFound = true;
            break;
        }
    }
    return isFound;
}

void PCPrivacyModel::ClearTaggedFriends() {
    void * listData;
    EINA_LIST_FREE(mTaggedFriends, listData) {
        (static_cast<Friend *> (listData))->Release();
    }
}

Friend *PCPrivacyModel::FindTaggedFriend(const Friend *friendToFind) {
    Eina_List *l;
    void *data;
    Friend *res = nullptr;
    EINA_LIST_FOREACH(mTaggedFriends, l, data) {
        Friend *fr = static_cast<Friend *> (data);
        if (*fr == *friendToFind) {
            res = fr;
            break;
        }
    }
    return res;
}

bool PCPrivacyModel::AreTaggedFriendsChanged(const Eina_List *taggedFriends) {
    if (eina_list_count(taggedFriends) != eina_list_count(mTaggedFriends)) return true;
    const Eina_List *l;
    void *data;
    bool res = false;
    EINA_LIST_FOREACH(taggedFriends, l, data) {
        Friend *fr = static_cast<Friend *> (data);
        if (!FindTaggedFriend(fr)) {
            res = true;
            break;
        }
    }
    return res;
}
