#include "EditCaptionsScreen.h"
#include "PostComposerScreen.h"
#include "SelectedMediaList.h"

static const char* LogTag = "EDIT_CAPTIONS_SCREEN";

EditCaptionsScreen::EditCaptionsScreen(ScreenBase *screen) :
        ScreenBase(NULL), mScreen(screen) {

    mScreenId = ScreenBase::SID_EDIT_CAPTIONS_SCREEN;
    mHeader = NULL;
    mPhotoWidget = NULL;
    mConfirmationDialog = NULL;
    mMediaBox = NULL;
    mScroller = NULL;
    mScrollerGesture = NULL;
    mScrollerBox = NULL;
    mMediaListToDelete = NULL;

    mLayout = elm_layout_add(getParentMainLayout());
    evas_object_size_hint_weight_set(mLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    elm_layout_file_set(mLayout, Application::mEdjPath, "post_composer_screen");
    evas_object_show(mLayout);

    mHeader = WidgetFactory::CreateGrayHeaderWithLogo(mLayout, "IDS_EDIT_PHOTO_CAPTION", "IDS_UPPERCASE_DONE", this);
    elm_object_part_content_set(mLayout, "swallow.top_toolbar", mHeader);

    mScroller = elm_scroller_add(mLayout);
    elm_object_part_content_set(mLayout, "composer_scroller", mScroller);
    elm_scroller_policy_set(mScroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_OFF);
    elm_scroller_single_direction_set(mScroller, ELM_SCROLLER_SINGLE_DIRECTION_HARD);

    mScrollerGesture = elm_gesture_layer_add(mLayout);
    elm_gesture_layer_attach(mScrollerGesture, mScroller);
    elm_object_scroll_lock_x_set(mScroller, EINA_TRUE);
    elm_scroller_movement_block_set(mScroller, ELM_SCROLLER_MOVEMENT_BLOCK_HORIZONTAL);

    mScrollerBox = elm_box_add(mScroller);
    evas_object_size_hint_weight_set(mScrollerBox, 0, 0);
    evas_object_size_hint_align_set(mScrollerBox, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_object_content_set(mScroller, mScrollerBox);
    evas_object_show(mScroller);

    mMediaBox = WidgetFactory::CreateWrapperByName(mScrollerBox, "post_composer_wrapper");

    mPhotoWidget = new ComposerPhotoWidget(ePOST_EDIT, mLayout, mMediaBox, this);
}

EditCaptionsScreen::~EditCaptionsScreen() {
    void *listData;
    EINA_LIST_FREE(mMediaListToDelete, listData) {
        (static_cast<PostComposerMediaData *>(listData))->Release();
    }

    delete mPhotoWidget;
    delete mConfirmationDialog;
}

void EditCaptionsScreen::CancelChanges() {
    Eina_List *list;
    void *list_data;
    PostComposerMediaData *media;

    EINA_LIST_FOREACH(SelectedMediaList::Get(), list, list_data) {
        media = static_cast<PostComposerMediaData *>(list_data);
        if (media->IsCaptionChanged()) {
            media->SetCaption(media->GetOriginalCaption());
        }
    }
}

void EditCaptionsScreen::ConfirmCallback(void *data, Evas_Object *obj, const char *emission, const char *source) {
    Log::info(LOG_FACEBOOK_TAGGING, "EditCaptionsScreen::ConfirmCallback()");
    Eina_List *list = nullptr;
    void *listData = nullptr;

    // Filter out SelectedMediaList() as all changes have been confirmed
    EINA_LIST_FOREACH(mMediaListToDelete, list, listData) {
        PostComposerMediaData* mediaData = static_cast<PostComposerMediaData*>(listData);
        SelectedMediaList::RemoveItem(mediaData);
    }

    EINA_LIST_FOREACH(SelectedMediaList::Get(), list, listData) {
        PostComposerMediaData* mediaData = static_cast<PostComposerMediaData*>(listData);
        // Update caption field with user input as changes are confirmed
        mediaData->UpdateCaption();
    }

    PostComposerScreen *parent = static_cast<PostComposerScreen*>(mScreen);
    parent->SetMediaListToDelete(mMediaListToDelete);

    Pop();
}

void EditCaptionsScreen::CancelCallback(void *data, Evas_Object *obj, void *event_info) {
    // At least one caption has been changed or at least one pic is removed
    if (SelectedMediaList::IsAnyCaptionEdited() || eina_list_count(mMediaListToDelete) > 0) {
        ShowKeepDiscardPopup();
        EnableCancelButton(true);
        EnableRightButton(true);
    } else {
        Pop();
    }
}

void EditCaptionsScreen::ShowKeepDiscardPopup() {
    Log::debug_tag(LogTag, "ShowKeepDiscardPopup()");
    mConfirmationDialog = ConfirmationDialog::CreateAndShow(mLayout, "IDS_DISCARD_CAPTIONS", "IDS_DISCARD_CHANGES_DESC", "IDS_KEEP", "IDS_DISCARD",
            confirmation_dialog_cb, this);
}

void EditCaptionsScreen::confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event) {
    Log::debug_tag(LogTag, "confirmation_dialog_cb:%d", event);
    EditCaptionsScreen *screen = static_cast<EditCaptionsScreen *>(user_data);
    delete screen->mConfirmationDialog;
    screen->mConfirmationDialog = NULL;

    switch (event) {
        case ConfirmationDialog::ECDYesPressed:
        case ConfirmationDialog::ECDDismiss:
            break;
        case ConfirmationDialog::ECDNoPressed:
            screen->CancelChanges();
            screen->Pop();
            break;
    }
}

bool EditCaptionsScreen::HandleBackButton() {
    Log::debug_tag(LogTag, "BACK");
    bool ret = true;

    if (!WidgetFactory::CloseErrorDialogue()) {
        if (mConfirmationDialog) {
            EnableRightButton(true);
            delete mConfirmationDialog;
            mConfirmationDialog = NULL;
        } else {
            // At least one caption has been changed or at least one pic is removed
            if (SelectedMediaList::IsAnyCaptionEdited() || eina_list_count(mMediaListToDelete) > 0) {
                ShowKeepDiscardPopup();
            } else {
                ret = false;
            }
        }
    }
    return ret;
}

void EditCaptionsScreen::DeleteMediaButtonClick(PostComposerMediaData *mediaDataToDelete, Evas_Object *photoWidgetLayout) {
    if (!mediaDataToDelete || !photoWidgetLayout || !mMediaBox) {
        return;
    }

    mediaDataToDelete->RemoveCaptionLayout();
    mediaDataToDelete->AddRef();
    mMediaListToDelete = eina_list_append(mMediaListToDelete, mediaDataToDelete);

    int itemsLeft = SelectedMediaList::Count() - eina_list_count(mMediaListToDelete);
    // Remove Photo Widget Layout from mMediaBox or remove all children of mMediaBox if there are no items.
    if (photoWidgetLayout && mMediaBox) {
        if (itemsLeft == 0) {
            elm_box_clear(mMediaBox);
        } else {
            elm_box_unpack(mMediaBox, photoWidgetLayout);
            // Get and delete wrapper layout (structure is described in delete media handler of PhotoWidget class).
            evas_object_del(photoWidgetLayout);
        }
    }
}

void EditCaptionsScreen::Push() {
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}
