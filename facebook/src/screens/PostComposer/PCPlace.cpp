#include "PCPlace.h"

PCPlace::PCPlace(const Place& place): mId(place.GetId() ? place.GetId() : ""),
                                      mName(place.mName ? place.mName : ""),
                                      mCity(place.mLocation ? (place.mLocation->mCity ? place.mLocation->mCity : "") : ""){
}

PCPlace::PCPlace(const Post::Place& place): mId(place.mId ? place.mId : ""),
                                            mName(place.mName ? place.mName : ""),
                                            mCity(place.mCity ? place.mCity : "") {
}

PCPlace::PCPlace(const PCPlace& place): mId(place.mId),
                                        mName(place.mName),
                                        mCity(place.mCity) {
}

Post::Place *PCPlace::GetPlace() {
    Post::Place *place = new Post::Place();
    place->mId = strdup(mId.c_str());
    place->mName = strdup(mName.c_str());
    place->mCity = strdup(mCity.c_str());
    return place;
}
