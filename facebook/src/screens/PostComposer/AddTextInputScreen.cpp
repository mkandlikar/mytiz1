#include "AddTextInputScreen.h"
#include "AddTextScreen.h"
#include "Common.h"
#include "UIRes.h"
#include "Utils.h"
#include "WidgetFactory.h"

#ifdef FEATURE_ADD_TEXT

AddTextInputScreen::AddTextInputScreen(PostComposerMediaData * mediaData,
        Eina_List * textList) :
        ScreenBase(NULL), mColorPicker(NULL), mColorPickerRect(NULL), mEntry(NULL)
{
    mScreenId = ScreenBase::SID_ADD_TEXT_INPUT;
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "AddTextInputScreen()");
    mMediaData = mediaData;
    mTextList = textList;

    mLayout = WidgetFactory::CreateLayoutByGroup(getParentMainLayout(), "add_text_input_screen");

    // Removed because this callback invoked after virtual keyboard off in PostComposerScreen. Looks like Tizen's bug.
    // I've tried to add this callback in Push function but it had no effect.
    //evas_object_smart_callback_add(Application::GetInstance()->mConform, "virtualkeypad,state,off", on_virtualkeyboard_off_cb, this);

    elm_object_signal_callback_add(mLayout, "spectrum_move", "spectrum", on_spectrum_move_cb, this);
    elm_object_signal_callback_add(mLayout, "spectrum_unpressed", "spectrum", on_spectrum_unpressed_cb, this);

    // If you want to set default focus on the entry than you need to create it the first.
    CreateEntry();

    // Create header of screen
    CreateHeader();

    // Create background
    CreateBackground();

    // Create image of color spectrum
    CreateSpectrumImage();

    // Create color picker
    CreateColorPicker();

}

AddTextInputScreen::~AddTextInputScreen()
{
//    if(Application::GetInstance()->mConform) {
//        evas_object_smart_callback_del(Application::GetInstance()->mConform, "virtualkeypad,state,off", on_virtualkeyboard_off_cb);
//    }
    if(mLayout) {
        elm_object_signal_callback_del(mLayout, "spectrum_move", "spectrum", on_spectrum_move_cb);
        elm_object_signal_callback_del(mLayout, "spectrum_unpressed", "spectrum", on_spectrum_unpressed_cb);
    }
    mMediaData = NULL;
    mColorPicker = NULL;
    mColorPickerRect = NULL;
    mEntry = NULL;
}

void AddTextInputScreen::Push()
{
    ScreenBase::Push();
    // Remove standard blue header
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void AddTextInputScreen::InsertBefore(ScreenBase * parentScreen) {
    ScreenBase::InsertBefore(parentScreen);
    // Remove standard blue header
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void AddTextInputScreen::CreateHeader()
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "AddTextInputScreen::CreateHeader");

    Evas_Object * header = WidgetFactory::CreateGrayHeaderWithLogo(mLayout, "IDS_ADD_TEXT",
                                                                   "IDS_DONE", this);
    elm_object_part_content_set(mLayout, "header", header);
    evas_object_raise(header);
}

void AddTextInputScreen::CreateBackground()
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "AddTextInputScreen::CreateBackground");
    Evas *evas = evas_object_evas_get(mLayout);
    Evas_Object *img = evas_object_image_filled_add(evas);
    evas_object_image_file_set(img, mMediaData->GetThumbnailPath(), NULL);
    // Scale down for simplify blur operation because of decreased size of image.

    // It works but with aspect ratio and it's strange. Need to investigate
    //evas_object_image_load_size_set(img, 10, 10);

    // Scale down picture in (* arg2) times.
    evas_object_image_load_scale_down_set(img, 8);

    // Make blur on image
    Utils::ImageBlur(img);

    elm_object_part_content_set(mLayout, "background", img);
}

void AddTextInputScreen::CreateEntry()
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "AddTextInputScreen::CreateEntry");

    mEntry = elm_entry_add(mLayout);
    elm_object_part_content_set(mLayout, "text_entry", mEntry);
    elm_entry_prediction_allow_set(mEntry, EINA_FALSE);
    elm_entry_scrollable_set(mEntry, EINA_TRUE);
    elm_entry_text_style_user_push(mEntry, ADD_TEXT_TEXT_STYLE);
    elm_entry_line_wrap_set(mEntry, ELM_WRAP_WORD);
    elm_entry_cnp_mode_set(mEntry, ELM_CNP_MODE_PLAINTEXT);

    if (mTextList) {
        PostComposerMediaText * mediaText = static_cast<PostComposerMediaText *>(eina_list_data_get(mTextList));
        int r, g, b;
        mediaText->GetColor(&r, &g, &b);
        evas_object_color_set(mEntry, r, g, b, 255);
        elm_entry_entry_set(mEntry, mediaText->GetText());
    }

}

void AddTextInputScreen::CreateColorPicker()
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "AddTextInputScreen::CreateColorPicker");

    mColorPicker = elm_image_add(mLayout);
    ELM_IMAGE_FILE_SET(mColorPicker, ICON_DIR"/PostComposer/color_picker_indicator.png", NULL);
    elm_object_part_content_set(mLayout, "color_picker", mColorPicker);

    Evas *evas = evas_object_evas_get(mLayout);
    mColorPickerRect = evas_object_rectangle_add(evas);
    evas_object_resize(mColorPickerRect, R->ADD_TEXT_COLOR_PICKER_RECT_SIZE_W_H,
            R->ADD_TEXT_COLOR_PICKER_RECT_SIZE_W_H);
    elm_object_part_content_set(mLayout, "picker_rectangle_color", mColorPickerRect);
}

void AddTextInputScreen::CreateSpectrumImage()
{
    Evas *evas = evas_object_evas_get(mLayout);
    Evas_Object *spectrum = evas_object_image_filled_add(evas);
    evas_object_image_file_set(spectrum, ICON_DIR"/PostComposer/color_picker_spectrum.png", NULL);
    elm_object_part_content_set(mLayout, "spectrum_color", spectrum);
    evas_object_image_size_get(spectrum, NULL, &mSpectrumSourceHeight);
}

void AddTextInputScreen::on_spectrum_move_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    AddTextInputScreen * screen = static_cast<AddTextInputScreen *>(data);

    // Get current cursor position
    Evas *evas = evas_object_evas_get(screen->mLayout);
    int cursorX, cursorY;
    evas_pointer_output_xy_get(evas, &cursorX, &cursorY);

    Evas_Object * spectrum = elm_object_part_content_get(screen->mLayout, "spectrum_color");
    if (spectrum) {
        int spectrumRel1Y, spectrumH, spectrumRel2Y, pickerY;
        evas_object_geometry_get(spectrum, NULL, &spectrumRel1Y, NULL, &spectrumH);
        // Check position of cursor relatively to spectrum size
        if (cursorY < spectrumRel1Y) {
            pickerY = spectrumRel1Y;
        }
        else if (cursorY > (spectrumRel2Y = spectrumRel1Y + spectrumH)) {
            pickerY = spectrumRel2Y;
        }
        else {
            pickerY = cursorY;
        }
        // Picker's height / 2. To render picker at the center of finger position.
        evas_object_move(screen->mColorPicker, R->ADD_TEXT_COLOR_PICKER_OFFSET_L,
                pickerY - R->ADD_TEXT_COLOR_PICKER_SIZE_H / 2);
        // The same for rectangle.
        evas_object_move(screen->mColorPickerRect, R->ADD_TEXT_COLOR_PICKER_RECT_OFFSET_L,
                pickerY - R->ADD_TEXT_COLOR_PICKER_RECT_SIZE_W_H / 2);

        // Set color of text.
        // To get color of area that cursor points need to get calculate position of cursor relatively to
        // the source image of spectrum. Because we can get color only from source image. So here the steps:

        // Calculate factor of current spectrum height relatively to source spectrum height.
        double heightFactor = (double) screen->mSpectrumSourceHeight / spectrumH;
        // Multiply height factor with current cursor position at the spectrum.
        int sourceYPosition = heightFactor * (pickerY - spectrumRel1Y);
        // Get color of source position.
        int r, g, b;
        bool ret = Utils::GetImageColorByXY(spectrum, 0, sourceYPosition, &r, &g, &b);
        if (ret) {
            evas_object_color_set(screen->mEntry, r, g, b, 255);
            evas_object_color_set(screen->mColorPickerRect, r, g, b, 255);
        }
        evas_object_show(screen->mColorPickerRect);
        evas_object_show(screen->mColorPicker);
    }
}

void AddTextInputScreen::on_spectrum_unpressed_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "AddTextInputScreen::on_spectrum_unpressed_cb");
    AddTextInputScreen * screen = static_cast<AddTextInputScreen *>(data);
    evas_object_hide(screen->mColorPickerRect);
    evas_object_hide(screen->mColorPicker);
}

void AddTextInputScreen::on_virtualkeyboard_off_cb(void *data, Evas_Object *obj, void *event_info)
{
    AddTextInputScreen *screen = static_cast<AddTextInputScreen *>(data);
    if (screen) {
        screen->ConfirmCallback(data, obj, NULL, NULL);
    }
}

void AddTextInputScreen::CancelCallback(void *data, Evas_Object *obj, void *event_info)
{
    ConfirmCallback(data, obj, NULL, NULL);
}

bool AddTextInputScreen::HandleBackButton()
{
    ConfirmCallback(this, NULL, NULL, NULL);
    return true;
}

void AddTextInputScreen::ConfirmCallback(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    AddTextInputScreen *screen = static_cast<AddTextInputScreen *>(data);
    char * text = elm_entry_markup_to_utf8(elm_entry_entry_get(screen->mEntry));
    if (strlen(text)) {
        int r, g, b;
        evas_object_color_get(screen->mEntry, &r, &g, &b, NULL);
        if (screen->mTextList) {
            PostComposerMediaText * mediaText = static_cast<PostComposerMediaText *>(eina_list_data_get(screen->mTextList));
            int pastR, pastG, pastB;
            mediaText->GetColor(&pastR, &pastG, &pastB);
            if (strcmp(mediaText->GetText(), text) || r != pastR || g != pastG || b != pastB) {
                mediaText->SetText(text);
                mediaText->SetColor(r, g, b);
                free(mediaText->GetImagePath());
                mediaText->SetImagePath(NULL);
            }
        }
        else {
            PostComposerMediaText * mediaText = new PostComposerMediaText(text, r, g, b);
            screen->mMediaData->TextListAppend(mediaText);
        }
        AddTextScreen * addTextSetScreen = new AddTextScreen(screen->mMediaData, eADD_TEXT_NEW);
        Application::GetInstance()->ReplaceScreen(addTextSetScreen);
    }
    else {
        if (screen->mTextList) {
            PostComposerMediaText * mediaText = static_cast<PostComposerMediaText *>(eina_list_data_get(screen->mTextList));
            delete mediaText;
            screen->mMediaData->TextListRemoveList(screen->mTextList);
        }
        if (eina_list_count(screen->mMediaData->GetTextList())) {
            AddTextScreen * addTextSetScreen = new AddTextScreen(screen->mMediaData, eADD_TEXT_VIEW);
            Application::GetInstance()->ReplaceScreen(addTextSetScreen);
        }
        else {
            screen->ScreenBase::CancelCallback(data, obj, NULL);
        }
    }
}

#endif  /* FEATURE_ADD_TEXT */
