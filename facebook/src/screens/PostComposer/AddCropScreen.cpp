#include "AddCropScreen.h"
#include "Common.h"
#include "Log.h"
#include "Utils.h"
#include "WidgetFactory.h"

#define CROP_MARKER_ICON_NORMAL  POST_COMPOSER_ICON_DIR"/crop_image_marker.png"
#define CROP_MARKER_ICON_ACTIVE  POST_COMPOSER_ICON_DIR"/crop_image_marker_active.png"
#define ADCRS_LOG_PREFIX "AddCropScreen:"

AddCropScreen::AddCropScreen(PostComposerMediaData * mediaData, PostComposerScreen * screen) :
        ScreenBase(NULL), mElmRotationAngle((int)IMAGE_UTIL_ROTATION_NONE),
        mTimer(NULL)
{
    mScreenId = ScreenBase::SID_ADD_CROP_SCREEN;
    Log::debug(ADCRS_LOG_PREFIX " constructor");

    mCropRect = {0};
    mImgRegion = {0};

    mMediaData = mediaData;
    mScreen = screen;
    mLayout = WidgetFactory::CreateLayoutByGroup(getParentMainLayout(), "add_crop_screen");
    mDisableVerticalCrop = false;
    mDisableHorizontalCrop = false;
    mPrevMarkerIndex = MI_NO_MARKER;

    elm_object_signal_callback_add(mLayout, "rotate_btn.click", "*", on_rotate_btn_clicked_cb, this);

    Log::debug(ADCRS_LOG_PREFIX "   orig_path='%s'", mMediaData->GetOriginalPath());
    Log::debug(ADCRS_LOG_PREFIX "   file_path='%s'", mMediaData->GetFilePath());
    Log::debug(ADCRS_LOG_PREFIX "   thumb_path='%s'", mMediaData->GetThumbnailPath());
    Log::debug(ADCRS_LOG_PREFIX "   orig_thumb_path='%s'", mMediaData->GetOriginalThumbnailPath());

    DrawLayout();  //creates cropping rectangle, markers, and lines

    if (mMediaData->AbleToRestoreCropParams()) {
        Log::debug(ADCRS_LOG_PREFIX "   AbleToRestoreCropParams");
        mMediaData->GetCropParams(mCropRect.x, mCropRect.y, mCropRect.width, mCropRect.height, mElmRotationAngle);
        elm_image_orient_set(mPhoto, (Elm_Image_Orient)mElmRotationAngle);
    } else {
        Log::debug(ADCRS_LOG_PREFIX "   not AbleToRestoreCropParams");
    }

    mRealRotationAngle = mElmRotationAngle;
    mExifNeededAngle = (int)Utils::GetEXIFOrientation(mMediaData->GetOriginalPath());

    switch (mExifNeededAngle) {
    case Utils::ORIENTATION_ROTATE_90:
        mRealRotationAngle = ((int)IMAGE_UTIL_ROTATION_90 + mElmRotationAngle)%4;
        break;
    case Utils::ORIENTATION_ROTATE_180:
        mRealRotationAngle = ((int)IMAGE_UTIL_ROTATION_180 + mElmRotationAngle)%4;
        break;
    case Utils::ORIENTATION_ROTATE_270:
        mRealRotationAngle = ((int)IMAGE_UTIL_ROTATION_270 + mElmRotationAngle)%4;
        break;
    }
    Log::debug(ADCRS_LOG_PREFIX "   ANGLEs: mExifNeeded=[%d], mRealRotation=%d, mElmRotation=%d",
               mExifNeededAngle, mRealRotationAngle*90, mElmRotationAngle*90);

    mMoveGesture = elm_gesture_layer_add(mLayout);

    elm_gesture_layer_attach(mMoveGesture, mCropRect.cropRect);
    for (int i = 0; i < CROP_MARKERS_AMOUNT; i++) {
        mResizeGesture[i] = elm_gesture_layer_add(mLayout);
        elm_gesture_layer_attach(mResizeGesture[i], mCropRect.resizePoint[i]);
    }
    RegisterGestureCBs();
    mTimer = ecore_timer_add(TIMEOUT, ecore_timer_cb, this);
}

Eina_Bool AddCropScreen::ecore_timer_cb(void *data)
{
    AddCropScreen * screen = static_cast<AddCropScreen *>(data);
    ecore_timer_del(screen->mTimer);
    screen->DrawCrop(screen->mMediaData->AbleToRestoreCropParams());
    screen->mTimer = NULL;
    return ECORE_CALLBACK_CANCEL;
}

void AddCropScreen::DrawCrop(bool restoreCropParams)
{
    Log::debug(ADCRS_LOG_PREFIX " DrawCrop(%s crop params)", (restoreCropParams ? "restore" : "NOT restore"));

    int elmImgW, elmImgH;  //dimensions of the original picture
    elm_image_object_size_get(mPhoto, &elmImgW, &elmImgH);
    Log::debug(ADCRS_LOG_PREFIX "  elmImgSize:(%d %d)", elmImgW, elmImgH);

    int imgGeomX, imgGeomY, imgGeomWidth, imgGeomHeight;  //the screen area a picture is drawn to
    evas_object_geometry_get(mPhoto, &imgGeomX, &imgGeomY, &imgGeomWidth, &imgGeomHeight);
    Log::debug(ADCRS_LOG_PREFIX "  elmImgRegion:(%d %d)(%d %d)", imgGeomX, imgGeomY, imgGeomWidth, imgGeomHeight);

    int x, y, w, h;  //on-screen coordinates and dimensions of a resized image
    if ((double)elmImgH/imgGeomHeight > (double)elmImgW/imgGeomWidth) {  //portrait-oriented
        h = imgGeomHeight;
        w = imgGeomHeight * elmImgW / elmImgH;
        x = imgGeomX + (imgGeomWidth - w)/2;
        y = imgGeomY;
    } else {  //landscape-oriented
        h = imgGeomWidth * elmImgH /elmImgW ;
        w = imgGeomWidth;
        x = imgGeomX;
        y = imgGeomY + (imgGeomHeight - h)/2;
    }
    mDisableHorizontalCrop = (w <= R->CROP_REGION_MIN_WIDTH);
    mDisableVerticalCrop = (h <= R->CROP_REGION_MIN_HEIGHT);
    Log::debug(ADCRS_LOG_PREFIX "   x=%d y=%d w=%d h=%d; Hcrop:%d, Vcrop:%d",
               x, y, w, h, !mDisableHorizontalCrop, !mDisableVerticalCrop);

    if (restoreCropParams == false) {
       SetCropRect(x, y, w, h);
    }
    SetImageRegion(x, y, w, h);
    RedrawCropRect(MI_NO_MARKER);
}

void AddCropScreen::RegisterGestureCBs()
{
    elm_gesture_layer_cb_set(mMoveGesture, ELM_GESTURE_MOMENTUM, ELM_GESTURE_STATE_START, momentum_start, this);
    elm_gesture_layer_cb_set(mMoveGesture, ELM_GESTURE_MOMENTUM, ELM_GESTURE_STATE_MOVE, momentum_move_cropRect, this);
    elm_gesture_layer_cb_set(mMoveGesture, ELM_GESTURE_MOMENTUM, ELM_GESTURE_STATE_END, momentum_end, this);
    elm_gesture_layer_cb_set(mMoveGesture, ELM_GESTURE_MOMENTUM, ELM_GESTURE_STATE_ABORT, momentum_abort, this);

    for (int i = 0; i < CROP_MARKERS_AMOUNT; i++) {
        elm_gesture_layer_cb_set(mResizeGesture[i], ELM_GESTURE_MOMENTUM, ELM_GESTURE_STATE_START, momentum_start, this);
        elm_gesture_layer_cb_set(mResizeGesture[i], ELM_GESTURE_MOMENTUM, ELM_GESTURE_STATE_END, momentum_end, this);
        elm_gesture_layer_cb_set(mResizeGesture[i], ELM_GESTURE_MOMENTUM, ELM_GESTURE_STATE_ABORT, momentum_abort, this);
    }

    elm_gesture_layer_cb_set(mResizeGesture[0], ELM_GESTURE_MOMENTUM, ELM_GESTURE_STATE_MOVE, momentum_move_resizePoint0_TL, this);
    elm_gesture_layer_cb_set(mResizeGesture[1], ELM_GESTURE_MOMENTUM, ELM_GESTURE_STATE_MOVE, momentum_move_resizePoint1_T, this);
    elm_gesture_layer_cb_set(mResizeGesture[2], ELM_GESTURE_MOMENTUM, ELM_GESTURE_STATE_MOVE, momentum_move_resizePoint2_TR, this);
    elm_gesture_layer_cb_set(mResizeGesture[3], ELM_GESTURE_MOMENTUM, ELM_GESTURE_STATE_MOVE, momentum_move_resizePoint3_R, this);
    elm_gesture_layer_cb_set(mResizeGesture[4], ELM_GESTURE_MOMENTUM, ELM_GESTURE_STATE_MOVE, momentum_move_resizePoint4_BR, this);
    elm_gesture_layer_cb_set(mResizeGesture[5], ELM_GESTURE_MOMENTUM, ELM_GESTURE_STATE_MOVE, momentum_move_resizePoint5_B, this);
    elm_gesture_layer_cb_set(mResizeGesture[6], ELM_GESTURE_MOMENTUM, ELM_GESTURE_STATE_MOVE, momentum_move_resizePoint6_BL, this);
    elm_gesture_layer_cb_set(mResizeGesture[7], ELM_GESTURE_MOMENTUM, ELM_GESTURE_STATE_MOVE, momentum_move_resizePoint7_L, this);
}

void AddCropScreen::UnegisterGestureCBs()
{
    elm_gesture_layer_cb_set(mMoveGesture, ELM_GESTURE_MOMENTUM, ELM_GESTURE_STATE_START, NULL, NULL);
    elm_gesture_layer_cb_set(mMoveGesture, ELM_GESTURE_MOMENTUM, ELM_GESTURE_STATE_MOVE, NULL, NULL);
    elm_gesture_layer_cb_set(mMoveGesture, ELM_GESTURE_MOMENTUM, ELM_GESTURE_STATE_END, NULL, NULL);
    elm_gesture_layer_cb_set(mMoveGesture, ELM_GESTURE_MOMENTUM, ELM_GESTURE_STATE_ABORT, NULL, NULL);

    for (int i = 0; i < CROP_MARKERS_AMOUNT; i++) {
        elm_gesture_layer_cb_set(mResizeGesture[i], ELM_GESTURE_MOMENTUM, ELM_GESTURE_STATE_START, NULL, NULL);
        elm_gesture_layer_cb_set(mResizeGesture[i], ELM_GESTURE_MOMENTUM, ELM_GESTURE_STATE_MOVE, NULL, NULL);
        elm_gesture_layer_cb_set(mResizeGesture[i], ELM_GESTURE_MOMENTUM, ELM_GESTURE_STATE_END, NULL, NULL);
        elm_gesture_layer_cb_set(mResizeGesture[i], ELM_GESTURE_MOMENTUM, ELM_GESTURE_STATE_ABORT, NULL, NULL);
    }
}

AddCropScreen::~AddCropScreen()
{
    Log::debug(ADCRS_LOG_PREFIX " destructor");
    if (mTimer != NULL) {
        ecore_timer_del(mTimer);
    }
    UnegisterGestureCBs();
    DeleteCropRectObjs();
    evas_object_del(mPhoto);
    mMediaData = NULL;
}

void AddCropScreen::Push()
{
    ScreenBase::Push();
    // Remove standard blue header
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void AddCropScreen::ConfirmCallback(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::debug(ADCRS_LOG_PREFIX " ConfirmCallback");
    if (IsImageChanged()) {
        mMediaData->SetCropParams(mCropRect.x, mCropRect.y, mCropRect.width, mCropRect.height, elm_image_orient_get(mPhoto));
        ProgressBarShow();
        TransformImage();
        if (mMediaData->GetTempCropImagePath())
            mMediaData->SetFilePath(mMediaData->GetTempCropImagePath());
    }
    Pop();
}

bool AddCropScreen::HandleBackButton()
{
    Log::debug(ADCRS_LOG_PREFIX " HandleBackButton");
    Pop();
    return true;
}

void AddCropScreen::DrawLayout()
{
    // Create header
    Log::debug(ADCRS_LOG_PREFIX " DrawLayout");
    Evas_Object * header =
        WidgetFactory::CreateGrayHeaderWithLogo(mLayout,
                                                "IDS_CROP",
                                                "IDS_DONE", this);
    elm_object_part_content_set(mLayout, "header", header);
    evas_object_raise(header);

    // Place photo
    mPhoto = elm_image_add(mLayout);
    elm_image_file_set(mPhoto, mMediaData->GetOriginalThumbnailPath(), NULL);
    elm_object_part_content_set(mLayout, "crop_image", mPhoto);

    // Create crop rectangle's markers
    Evas * canvas = evas_object_evas_get(mLayout);
    mCropRect.cropRect = evas_object_rectangle_add(canvas);
    evas_object_color_set(mCropRect.cropRect, 122, 122, 122, 20);  //almost transparent rectangle (alpha=20)
    evas_object_show(mCropRect.cropRect);

    Evas * evas = evas_object_evas_get(mLayout);
    for (int i = 0; i < CROP_MARKERS_AMOUNT; i++) {
        mCropRect.resizePoint[i] = evas_object_image_filled_add(evas);
        evas_object_image_file_set(mCropRect.resizePoint[i], CROP_MARKER_ICON_NORMAL, NULL);
        evas_object_show(mCropRect.resizePoint[i]);
    }

    for (int i = 0; i < CROP_MARKERS_AMOUNT*LINE_WIDTH; i++) {
        mCropRect.borderLine[i] = evas_object_line_add(canvas);
        evas_object_color_set(mCropRect.borderLine[i], FACEBOOK_DEEP_BLUE_COLOR, 255);
        evas_object_show(mCropRect.borderLine[i]);
    }
}

void AddCropScreen::DeleteCropRectObjs()
{
    evas_object_del(mCropRect.cropRect);

    for (int i = 0; i < CROP_MARKERS_AMOUNT; i++) {
        evas_object_del(mCropRect.resizePoint[i]);
    }

    for (int i = 0; i < CROP_MARKERS_AMOUNT*LINE_WIDTH; i++) {
        evas_object_del(mCropRect.borderLine[i]);
    }
}

void AddCropScreen::RedrawCropRect(int index)
{
    if ((index >= MI_FIRST_MARKER) && (index <= MI_LAST_MARKER) && (index != mPrevMarkerIndex)) {
        if (mPrevMarkerIndex >= MI_FIRST_MARKER) {
            evas_object_image_file_set(mCropRect.resizePoint[mPrevMarkerIndex], CROP_MARKER_ICON_NORMAL, nullptr);
        }
        evas_object_image_file_set(mCropRect.resizePoint[index], CROP_MARKER_ICON_ACTIVE, nullptr);
        mPrevMarkerIndex = index;
    }

    int x = mCropRect.x;
    int y = mCropRect.y;
    int width = mCropRect.width-1;
    int height = mCropRect.height-1;

    evas_object_move(mCropRect.cropRect, x, y);
    evas_object_resize(mCropRect.cropRect, width, height);

    //corner-transparent-resize-regions
    int rect_size = R->CROP_RECT_SIZE;  // for code readability

    evas_object_move(mCropRect.resizePoint[0], x - rect_size/2, y - rect_size/2);
    evas_object_resize(mCropRect.resizePoint[0], rect_size, rect_size);

    evas_object_move(mCropRect.resizePoint[2], x - rect_size/2 + width, y - rect_size/2);
    evas_object_resize(mCropRect.resizePoint[2], rect_size, rect_size);

    evas_object_move(mCropRect.resizePoint[4], x - rect_size/2 + width, y - rect_size/2 + height);
    evas_object_resize(mCropRect.resizePoint[4], rect_size, rect_size);

    evas_object_move(mCropRect.resizePoint[6], x - rect_size/2, y - rect_size/2 + height);
    evas_object_resize(mCropRect.resizePoint[6], rect_size, rect_size);

    int centerX = width/2;
    int centerY = height/2;

    //center-transparent-resize-regions
    evas_object_move(mCropRect.resizePoint[1], x - rect_size/2 + centerX, y - rect_size/2);
    evas_object_resize(mCropRect.resizePoint[1], rect_size, rect_size);

    evas_object_move(mCropRect.resizePoint[3], x - rect_size/2 + width, y - rect_size/2 + centerY);
    evas_object_resize(mCropRect.resizePoint[3], rect_size, rect_size);

    evas_object_move(mCropRect.resizePoint[5], x - rect_size/2 + centerX, y - rect_size/2 + height);
    evas_object_resize(mCropRect.resizePoint[5], rect_size, rect_size);

    evas_object_move(mCropRect.resizePoint[7], x - rect_size/2, y - rect_size/2 + centerY);
    evas_object_resize(mCropRect.resizePoint[7], rect_size, rect_size);

    //draw lines
    int offset = R->CM_OFFSET;
    for (int i = 0; i < LINE_WIDTH; i++) {
        evas_object_line_xy_set(mCropRect.borderLine[0+CROP_MARKERS_AMOUNT*i], x + offset+i, y+i, x+(centerX-offset), y+i);
        evas_object_line_xy_set(mCropRect.borderLine[1+CROP_MARKERS_AMOUNT*i], x + (centerX+offset), y+i, x+(width-offset)-i, y+i);

        evas_object_line_xy_set(mCropRect.borderLine[2+CROP_MARKERS_AMOUNT*i], x + width-i, y+offset+i, x+width-i, y+(centerY-offset));
        evas_object_line_xy_set(mCropRect.borderLine[3+CROP_MARKERS_AMOUNT*i], x + width-i, y+(centerY+offset), x+width-i, y+(height-offset)-i);

        evas_object_line_xy_set(mCropRect.borderLine[4+CROP_MARKERS_AMOUNT*i], x + (width-offset)-i, y+height-i, x+(centerX+offset), y+height-i);
        evas_object_line_xy_set(mCropRect.borderLine[5+CROP_MARKERS_AMOUNT*i], x + (centerX-offset), y+height-i, x+offset+i, y+height-i);

        evas_object_line_xy_set(mCropRect.borderLine[6+CROP_MARKERS_AMOUNT*i], x + i, y+(height-offset)-i, x+i, y+(centerY+offset));
        evas_object_line_xy_set(mCropRect.borderLine[7+CROP_MARKERS_AMOUNT*i], x + i, y+(centerY-offset), x+i, y+offset+i);
    }
}

Evas_Event_Flags AddCropScreen::momentum_start(void *data, void *event_info)
{
    AddCropScreen * scr = static_cast<AddCropScreen *>(data);
    Elm_Gesture_Momentum_Info *p = (Elm_Gesture_Momentum_Info *)event_info;
    scr->mPrevMomentumX2 = p->x1;
    scr->mPrevMomentumY2 = p->y1;

    return EVAS_EVENT_FLAG_NONE;
}

Evas_Event_Flags AddCropScreen::momentum_end(void *data, void *event_info)
{
    AddCropScreen * scr = static_cast<AddCropScreen *>(data);
    if (scr->mPrevMarkerIndex >= MI_FIRST_MARKER) {
        evas_object_image_file_set(scr->mCropRect.resizePoint[scr->mPrevMarkerIndex], CROP_MARKER_ICON_NORMAL, nullptr);
        scr->mPrevMarkerIndex = MI_NO_MARKER;
    }
    return EVAS_EVENT_FLAG_NONE;
}

Evas_Event_Flags AddCropScreen::momentum_abort(void *data, void *event_info)
{
    return momentum_end(data, event_info);
}

Evas_Event_Flags AddCropScreen::momentum_move_cropRect(void *data, void *event_info)
{
    AddCropScreen * scr = static_cast<AddCropScreen *>(data);
    Elm_Gesture_Momentum_Info *p = (Elm_Gesture_Momentum_Info *)event_info;

    if (p->n > 1) {
        return EVAS_EVENT_FLAG_NONE;
    }

    int offsetX = 0;
    if (!scr->mDisableHorizontalCrop) {
        offsetX = p->x2 - scr->mPrevMomentumX2;
        scr->mPrevMomentumX2 = p->x2;
    }
    int offsetY = 0;
    if (!scr->mDisableVerticalCrop) {
        offsetY = p->y2 - scr->mPrevMomentumY2;
        scr->mPrevMomentumY2 = p->y2;
    }

    int x1 = scr->mCropRect.x + offsetX;
    if (x1 < scr->mImgRegion.x1) {
        return EVAS_EVENT_FLAG_NONE;
    }
    int y1 = scr->mCropRect.y + offsetY;
    if (y1 < scr->mImgRegion.y1) {
        return EVAS_EVENT_FLAG_NONE;
    }
    int x2 = x1 + scr->mCropRect.width;
    if (x2 > scr->mImgRegion.x2) {
        return EVAS_EVENT_FLAG_NONE;
    }
    int y2 = y1 + scr->mCropRect.height;
    if (y2 > scr->mImgRegion.y2) {
        return EVAS_EVENT_FLAG_NONE;
    }
    scr->mCropRect.x = x1;
    scr->mCropRect.y = y1;

    scr->RedrawCropRect(MI_NO_MARKER);
    return EVAS_EVENT_FLAG_NONE;
}


Evas_Event_Flags AddCropScreen::momentum_move_resizePoint0_TL(void *data, void *event_info)  //top-left corner
{
    AddCropScreen * scr = static_cast<AddCropScreen *>(data);
    Elm_Gesture_Momentum_Info *p = (Elm_Gesture_Momentum_Info *)event_info;

    if (p->n > 1) {
        return EVAS_EVENT_FLAG_NONE;
    }

    int offsetX = 0;
    if (!scr->mDisableHorizontalCrop) {
        offsetX = p->x2 - scr->mPrevMomentumX2;
        scr->mPrevMomentumX2 = p->x2;
    }
    int offsetY = 0;
    if (!scr->mDisableVerticalCrop) {
        offsetY = p->y2 - scr->mPrevMomentumY2;
        scr->mPrevMomentumY2 = p->y2;
    }

    int x1 = scr->mCropRect.x + offsetX;
    if (x1 < scr->mImgRegion.x1) {
        return EVAS_EVENT_FLAG_NONE;
    }
    int y1 = scr->mCropRect.y + offsetY;
    if (y1 < scr->mImgRegion.y1) {
        return EVAS_EVENT_FLAG_NONE;
    }
    int width = scr->mCropRect.width;
    if (scr->mImgRegion.w > R->CROP_REGION_MIN_WIDTH) {
        width -= offsetX;
        if (width < R->CROP_REGION_MIN_WIDTH) {
            width = R->CROP_REGION_MIN_WIDTH;
            x1 = scr->mCropRect.x + scr->mCropRect.width - width;
        }
    }
    int height = scr->mCropRect.height;
    if (scr->mImgRegion.h > R->CROP_REGION_MIN_HEIGHT) {
        height -= offsetY;
        if (height < R->CROP_REGION_MIN_HEIGHT) {
            height = R->CROP_REGION_MIN_HEIGHT;
            y1 = scr->mCropRect.y + scr->mCropRect.height - height;
        }
    }
    scr->mCropRect.x = x1;
    scr->mCropRect.y = y1;
    scr->mCropRect.width = width;
    scr->mCropRect.height = height;

    scr->RedrawCropRect(MI_TOP_LEFT);
    return EVAS_EVENT_FLAG_NONE;
}

Evas_Event_Flags AddCropScreen::momentum_move_resizePoint1_T(void *data, void *event_info)  //top edge
{
    AddCropScreen * scr = static_cast<AddCropScreen *>(data);
    Elm_Gesture_Momentum_Info *p = (Elm_Gesture_Momentum_Info *)event_info;

    if ((p->n > 1) || scr->mDisableVerticalCrop) {
        return EVAS_EVENT_FLAG_NONE;
    }

    scr->mPrevMomentumX2 = p->x2;
    int offsetY = p->y2 - scr->mPrevMomentumY2;
    scr->mPrevMomentumY2 = p->y2;

    int y1 = scr->mCropRect.y + offsetY;
    if (y1 < scr->mImgRegion.y1) {
        return EVAS_EVENT_FLAG_NONE;
    }
    int height = scr->mCropRect.height;
    if (scr->mImgRegion.h > R->CROP_REGION_MIN_HEIGHT) {
        height -= offsetY;
        if (height < R->CROP_REGION_MIN_HEIGHT) {
            height = R->CROP_REGION_MIN_HEIGHT;
            y1 = scr->mCropRect.y + scr->mCropRect.height - height;
        }
    }
    scr->mCropRect.y = y1;
    scr->mCropRect.height = height;

    scr->RedrawCropRect(MI_TOP);
    return EVAS_EVENT_FLAG_NONE;
}

Evas_Event_Flags AddCropScreen::momentum_move_resizePoint2_TR(void *data, void *event_info)  //top-right corner
{
    AddCropScreen * scr = static_cast<AddCropScreen *>(data);
    Elm_Gesture_Momentum_Info *p = (Elm_Gesture_Momentum_Info *)event_info;

    if ((p->n)>1) {
        return EVAS_EVENT_FLAG_NONE;
    }

    int offsetX = 0;
    if (!scr->mDisableHorizontalCrop) {
        offsetX = p->x2 - scr->mPrevMomentumX2;
        scr->mPrevMomentumX2 = p->x2;
    }
    int offsetY = 0;
    if (!scr->mDisableVerticalCrop) {
        offsetY = p->y2 - scr->mPrevMomentumY2;
        scr->mPrevMomentumY2 = p->y2;
    }

    int y1 = scr->mCropRect.y + offsetY;
    if (y1 < scr->mImgRegion.y1) {
        return EVAS_EVENT_FLAG_NONE;
    }
    int width = scr->mCropRect.width;
    if (scr->mImgRegion.w > R->CROP_REGION_MIN_WIDTH) {
        width += offsetX;
        if (width < R->CROP_REGION_MIN_WIDTH) {
            width = R->CROP_REGION_MIN_WIDTH;
        }
    }
    int height = scr->mCropRect.height;
    if (scr->mImgRegion.h > R->CROP_REGION_MIN_HEIGHT) {
        height -= offsetY;
        if (height < R->CROP_REGION_MIN_HEIGHT) {
            height = R->CROP_REGION_MIN_HEIGHT;
            y1 = scr->mCropRect.y + scr->mCropRect.height - height;
        }
    }
    int x2 = scr->mCropRect.x + width;
    if (x2 > scr->mImgRegion.x2) {
        return EVAS_EVENT_FLAG_NONE;
    }
    scr->mCropRect.y = y1;
    scr->mCropRect.width = width;
    scr->mCropRect.height = height;

    scr->RedrawCropRect(MI_TOP_RIGHT);
    return EVAS_EVENT_FLAG_NONE;
}

Evas_Event_Flags AddCropScreen::momentum_move_resizePoint3_R(void *data, void *event_info)  //right edge
{
    AddCropScreen * scr = static_cast<AddCropScreen *>(data);
    Elm_Gesture_Momentum_Info *p = (Elm_Gesture_Momentum_Info *)event_info;

    if ((p->n > 1) || scr->mDisableHorizontalCrop) {
        return EVAS_EVENT_FLAG_NONE;
    }

    int offsetX = p->x2 - scr->mPrevMomentumX2;
    scr->mPrevMomentumX2 = p->x2;
    scr->mPrevMomentumY2 = p->y2;

    int width = scr->mCropRect.width + offsetX;
    if (width < R->CROP_REGION_MIN_WIDTH) {
        width = R->CROP_REGION_MIN_WIDTH;
    }
    int x2 = scr->mCropRect.x + width;
    if (x2 > scr->mImgRegion.x2) {
        return EVAS_EVENT_FLAG_NONE;
    }
    scr->mCropRect.width = width;

    scr->RedrawCropRect(MI_RIGHT);
    return EVAS_EVENT_FLAG_NONE;
}

Evas_Event_Flags AddCropScreen::momentum_move_resizePoint4_BR(void *data, void *event_info)  //bottom-right corner
{
    AddCropScreen * scr = static_cast<AddCropScreen *>(data);
    Elm_Gesture_Momentum_Info *p = (Elm_Gesture_Momentum_Info *)event_info;

    if (p->n > 1) {
        return EVAS_EVENT_FLAG_NONE;
    }

    int offsetX = 0;
    if (!scr->mDisableHorizontalCrop) {
        offsetX = p->x2 - scr->mPrevMomentumX2;
        scr->mPrevMomentumX2 = p->x2;
    }
    int offsetY = 0;
    if (!scr->mDisableVerticalCrop) {
        offsetY = p->y2 - scr->mPrevMomentumY2;
        scr->mPrevMomentumY2 = p->y2;
    }

    int width = scr->mCropRect.width;
    if (scr->mImgRegion.w > R->CROP_REGION_MIN_WIDTH) {
        width += offsetX;
        if (width < R->CROP_REGION_MIN_WIDTH) {
            width = R->CROP_REGION_MIN_WIDTH;
        }
    }
    int height = scr->mCropRect.height;
    if (scr->mImgRegion.h > R->CROP_REGION_MIN_HEIGHT) {
        height += offsetY;
        if (height < R->CROP_REGION_MIN_HEIGHT) {
            height = R->CROP_REGION_MIN_HEIGHT;
        }
    }
    int x2 = scr->mCropRect.x + width;
    if (x2 > scr->mImgRegion.x2) {
        return EVAS_EVENT_FLAG_NONE;
    }
    int y2 = scr->mCropRect.y + height;
    if (y2 > scr->mImgRegion.y2) {
        return EVAS_EVENT_FLAG_NONE;
    }
    scr->mCropRect.width = width;
    scr->mCropRect.height = height;

    scr->RedrawCropRect(MI_BOTTOM_RIGHT);
    return EVAS_EVENT_FLAG_NONE;
}

Evas_Event_Flags AddCropScreen::momentum_move_resizePoint5_B(void *data, void *event_info)  //bottom edge
{
    AddCropScreen * scr = static_cast<AddCropScreen *>(data);
    Elm_Gesture_Momentum_Info *p = (Elm_Gesture_Momentum_Info *)event_info;

    if ((p->n > 1) || scr->mDisableVerticalCrop) {
        return EVAS_EVENT_FLAG_NONE;
    }

    scr->mPrevMomentumX2 = p->x2;
    int offsetY = p->y2 - scr->mPrevMomentumY2;
    scr->mPrevMomentumY2 = p->y2;

    int height = scr->mCropRect.height + offsetY;
    if (height < R->CROP_REGION_MIN_HEIGHT) {
        height = R->CROP_REGION_MIN_HEIGHT;
    }
    int y2 = scr->mCropRect.y + height;
    if (y2 > scr->mImgRegion.y2) {
        return EVAS_EVENT_FLAG_NONE;
    }
    scr->mCropRect.height = height;

    scr->RedrawCropRect(MI_BOTTOM);
    return EVAS_EVENT_FLAG_NONE;
}

Evas_Event_Flags AddCropScreen::momentum_move_resizePoint6_BL(void *data, void *event_info)  //bottom-left corner
{
    AddCropScreen * scr = static_cast<AddCropScreen *>(data);
    Elm_Gesture_Momentum_Info *p = (Elm_Gesture_Momentum_Info *)event_info;

    if (p->n > 1) {
        return EVAS_EVENT_FLAG_NONE;
    }

    int offsetX = 0;
    if (!scr->mDisableHorizontalCrop) {
        offsetX = p->x2 - scr->mPrevMomentumX2;
        scr->mPrevMomentumX2 = p->x2;
    }
    int offsetY = 0;
    if (!scr->mDisableVerticalCrop) {
        offsetY = p->y2 - scr->mPrevMomentumY2;
        scr->mPrevMomentumY2 = p->y2;
    }

    int x1 = scr->mCropRect.x + offsetX;
    if (x1 < scr->mImgRegion.x1) {
        return EVAS_EVENT_FLAG_NONE;
    }
    int width = scr->mCropRect.width;
    if (scr->mImgRegion.w > R->CROP_REGION_MIN_WIDTH) {
        width -= offsetX;
        if (width < R->CROP_REGION_MIN_WIDTH) {
            width = R->CROP_REGION_MIN_WIDTH;
            x1 = scr->mCropRect.x + scr->mCropRect.width - width;
        }
    }
    int height = scr->mCropRect.height;
    if (scr->mImgRegion.h > R->CROP_REGION_MIN_HEIGHT) {
        height += offsetY;
        if (height < R->CROP_REGION_MIN_HEIGHT) {
            height = R->CROP_REGION_MIN_HEIGHT;
        }
    }
    int y2 = scr->mCropRect.y + height;
    if (y2 > scr->mImgRegion.y2) {
        return EVAS_EVENT_FLAG_NONE;
    }
    scr->mCropRect.x = x1;
    scr->mCropRect.width = width;
    scr->mCropRect.height = height;

    scr->RedrawCropRect(MI_BOTTOM_LEFT);
    return EVAS_EVENT_FLAG_NONE;
}

Evas_Event_Flags AddCropScreen::momentum_move_resizePoint7_L(void *data, void *event_info)  //left edge
{
    AddCropScreen * scr = static_cast<AddCropScreen *>(data);
    Elm_Gesture_Momentum_Info *p = (Elm_Gesture_Momentum_Info *)event_info;

    if ((p->n > 1) || scr->mDisableHorizontalCrop) {
        return EVAS_EVENT_FLAG_NONE;
    }

    int offsetX = p->x2 - scr->mPrevMomentumX2;
    scr->mPrevMomentumX2 = p->x2;
    scr->mPrevMomentumY2 = p->y2;

    int x1 = scr->mCropRect.x + offsetX;
    if (x1 < scr->mImgRegion.x1) {
        return EVAS_EVENT_FLAG_NONE;
    }
    int width = scr->mCropRect.width;
    if (scr->mImgRegion.w > R->CROP_REGION_MIN_WIDTH) {
        width -= offsetX;
        if (width < R->CROP_REGION_MIN_WIDTH) {
            width = R->CROP_REGION_MIN_WIDTH;
            x1 = scr->mCropRect.x + scr->mCropRect.width - width;
        }
    }
    scr->mCropRect.x = x1;
    scr->mCropRect.width = width;

    scr->RedrawCropRect(MI_LEFT);
    return EVAS_EVENT_FLAG_NONE;
}

void AddCropScreen::TransformImage()
{
    unsigned char *imgBuffer = NULL;
    int width = 0, height = 0;
    int croppedWidth = 0, croppedHeight = 0;
    unsigned int sizeDecode = 0;

    Log::debug(ADCRS_LOG_PREFIX " TransformImage");

    Evas_Object * evasImageObject = elm_image_object_get(mPhoto);
    evas_object_image_size_get(evasImageObject, &width, &height);

    if (ABLE_DECODE_TO_PNG == mMediaData->GetDecodeStatus()) {
        if (EVAS_COLORSPACE_ARGB8888 == evas_object_image_colorspace_get(evasImageObject)) {
            unsigned char *imagePixels = static_cast<unsigned char *>(evas_object_image_data_get(evasImageObject, EINA_FALSE));
            int pixelsToCopy = width * height;
            imgBuffer = (unsigned char *)malloc(pixelsToCopy*3);  //consider three bytes per pixel - R,G,B
            unsigned char *srcPtr = imagePixels;
            unsigned char *dstPtr = imgBuffer;
            unsigned char r, g, b;
            while (pixelsToCopy--) {  //convert from Little Endian 32-bit ARGB to Big Endian 24-bit RGB
                b = *srcPtr++;
                g = *srcPtr++;
                r = *srcPtr++;
                srcPtr++;  //skip 'a' (transparency) component
                *dstPtr++ = r;
                *dstPtr++ = g;
                *dstPtr++ = b;
            }
            mRealImageW = width;
            mRealImageH = height;
            CalculateCropCoords();
            croppedWidth = mCroppedImageX2 - mCroppedImageX1;
            croppedHeight = mCroppedImageY2 - mCroppedImageY1;

            SimpleCrop(imgBuffer, width, croppedWidth, croppedHeight);
            free(imgBuffer);
        }
    } else {
        int ret = image_util_decode_jpeg(mMediaData->GetOriginalPath(), IMAGE_UTIL_COLORSPACE_RGB888, &imgBuffer, &width, &height, &sizeDecode);
        Log::debug(ADCRS_LOG_PREFIX "  -> ret=%d, %d*%d, size=%d KByte",
                   ret, width, height, sizeDecode/1024);
        if (ret != IMAGE_UTIL_ERROR_NONE) {
            Log::debug(ADCRS_LOG_PREFIX "  Could not decode jpeg file");
            return;
        }
        Log::debug(ADCRS_LOG_PREFIX "  Successfully decoded jpeg file");

        if ((mRealRotationAngle == IMAGE_UTIL_ROTATION_NONE) ||
            (mRealRotationAngle == IMAGE_UTIL_ROTATION_180)) {
            mRealImageW = width;
            mRealImageH = height;
            CalculateCropCoords();
            croppedWidth = mCroppedImageX2 - mCroppedImageX1;
            croppedHeight = mCroppedImageY2 - mCroppedImageY1;
        } else {
            mRealImageW = height;
            mRealImageH = width;
            CalculateCropCoords();
            croppedWidth = mCroppedImageY2 - mCroppedImageY1;
            croppedHeight = mCroppedImageX2 - mCroppedImageX1;
        }

        SimpleCropRotateJPG(imgBuffer, width, height, croppedWidth, croppedHeight);
        free(imgBuffer);  //the buffer populated by image_util_decode_jpeg() must be freed
    }
}

//Todo: Currently this function just indicates that timeout is completed and screen is ready to crop operations.
//It's needed to implement logic checking that image is really changed by user.
bool AddCropScreen::IsImageChanged() {
    return !mTimer;
}

void AddCropScreen::CalculateCropCoords()
{
    mCroppedImageX1 = ((mCropRect.x - mImgRegion.x1)*((double)mRealImageW/mImgRegion.w));
    mCroppedImageY1 = ((mCropRect.y - mImgRegion.y1)*((double)mRealImageH/mImgRegion.h));
    mCroppedImageX2 = ((mCropRect.x + mCropRect.width -  mImgRegion.x1)*((double)mRealImageW/mImgRegion.w));
    mCroppedImageY2 = ((mCropRect.y + mCropRect.height - mImgRegion.y1)*((double)mRealImageH/mImgRegion.h));

    //fix geometry calculation errors if any
    if (mCroppedImageX1 < 0 || mCroppedImageX1 > mRealImageW) mCroppedImageX1 = 0;
    if (mCroppedImageY1 < 0 || mCroppedImageY1 > mRealImageH) mCroppedImageY1 = 0;
    if (mCroppedImageX2 > mRealImageW) mCroppedImageX2 = mRealImageW;
    if (mCroppedImageY2 > mRealImageH) mCroppedImageY2 = mRealImageH;
    Log::debug(ADCRS_LOG_PREFIX " CalculateCropCoords %d %d %d %d",
               mCroppedImageX1, mCroppedImageY1, mCroppedImageX2, mCroppedImageY2);
}

void AddCropScreen::SimpleCrop(unsigned char *srcImageBuffer, int width, int croppedWidth, int croppedHeight)
{
    unsigned char *dstBuffer = NULL;
    unsigned char *srcPtr, *dstPtr;

    int croppedPixels = croppedWidth * croppedHeight;
    int croppedRowSize = croppedWidth * 3;    //RGB888 format implies 3 bytes per pixel
    Log::debug(ADCRS_LOG_PREFIX "SimpleCrop()");
    srcPtr = &srcImageBuffer[(width * mCroppedImageY1 + mCroppedImageX1) * 3];
    dstBuffer = new unsigned char[croppedPixels * 3];
    dstPtr = dstBuffer;
    int originalRowSize = width * 3;
    int passes = croppedHeight;
    while (passes--) {
        int res = Utils::Memcpy_s(dstPtr, croppedPixels * 3, srcPtr, croppedRowSize);
        assert(res);
        srcPtr += originalRowSize;
        dstPtr += croppedRowSize;
    }
    ApplyNewPicture(dstBuffer);  //encode jpeg, handle thumbnail
    delete [] dstBuffer;
}

void AddCropScreen::SimpleCropRotateJPG(unsigned char *srcImageBuffer, int width, int height, int croppedWidth, int croppedHeight)
{
    unsigned char *dstBuffer = NULL;
    unsigned char *dstBuffer2 = NULL;
    unsigned char *srcPtr, *dstPtr;
    unsigned char *imgData;

    // Perform cropping first, then rotation. This order of operations saves memory and time.
    int croppedPixels = croppedWidth * croppedHeight;
    int croppedRowSize = croppedWidth * 3;    //RGB888 format implies 3 bytes per pixel
    Log::debug(ADCRS_LOG_PREFIX "SimpleCropRotateJPG()");
    switch (mRealRotationAngle) {
    case IMAGE_UTIL_ROTATION_90:
        srcPtr = &srcImageBuffer[(width * (height - mCroppedImageX2) + mCroppedImageY1) * 3];
        break;
    case IMAGE_UTIL_ROTATION_180:
        srcPtr = &srcImageBuffer[(width * (height - mCroppedImageY2) + (width - mCroppedImageX2)) * 3];
        break;
    case IMAGE_UTIL_ROTATION_270:
        srcPtr = &srcImageBuffer[(width * mCroppedImageX1 + (width - mCroppedImageY2)) * 3];
        break;
    default:
        srcPtr = &srcImageBuffer[(width * mCroppedImageY1 + mCroppedImageX1) * 3];
        break;
    }
    dstBuffer = new unsigned char[croppedPixels * 3];
    dstPtr = dstBuffer;
    int originalRowSize = width * 3;
    int passes = croppedHeight;
    while (passes--) {
        int res = Utils::Memcpy_s(dstPtr, croppedPixels * 3, srcPtr, croppedRowSize);
        assert(res);
        srcPtr += originalRowSize;
        dstPtr += croppedRowSize;
    }
    imgData = dstBuffer;

    if (mRealRotationAngle != IMAGE_UTIL_ROTATION_NONE) {
        Log::debug(ADCRS_LOG_PREFIX "   Simple Rotate");
        int x, y;
        dstBuffer2 = new unsigned char[croppedPixels * 3];
        dstPtr = dstBuffer2;
        if (mRealRotationAngle == IMAGE_UTIL_ROTATION_90) {
            srcPtr = &imgData[(croppedPixels - croppedWidth) * 3];
            for (x = croppedWidth; x > 0; x--) {
                for (y = croppedHeight; y > 0; y--) {
                    int res = Utils::Memcpy_s(dstPtr, croppedPixels * 3, srcPtr, 3);
                    assert(res);
                    srcPtr -= croppedRowSize;
                    dstPtr += 3;
                }
                srcPtr += croppedRowSize * croppedHeight + 3;
            }
        } else if (mRealRotationAngle == IMAGE_UTIL_ROTATION_180) {
            int pixelsCount = croppedPixels;
            srcPtr = &imgData[(croppedPixels - 1) * 3];
            while (pixelsCount--) {
                int res = Utils::Memcpy_s(dstPtr, croppedPixels * 3, srcPtr, 3);
                assert(res);
                srcPtr -= 3;
                dstPtr += 3;
            }
        } else if (IMAGE_UTIL_ROTATION_270) {
            srcPtr = &imgData[(croppedWidth - 1) * 3];
            for (x = croppedWidth; x > 0; x--) {
                for (y = croppedHeight; y > 0; y--) {
                    int res = Utils::Memcpy_s(dstPtr, croppedPixels * 3, srcPtr, 3);
                    assert(res);
                    srcPtr += croppedRowSize;
                    dstPtr += 3;
                }
                srcPtr -= croppedRowSize * croppedHeight + 3;
            }
        }
        imgData = dstBuffer2;
    }

    ApplyNewPicture(imgData);  //encode jpeg, handle thumbnail
    delete [] dstBuffer;
    delete [] dstBuffer2;
}

void AddCropScreen::on_rotate_btn_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    AddCropScreen * screen = static_cast<AddCropScreen *>(data);
    Log::debug(ADCRS_LOG_PREFIX " on_rotate_btn_clicked_cb(source='%s')", source);
    Log::debug(ADCRS_LOG_PREFIX "   old angles: mRealRotation=%d , mElmRotation=%d",
               screen->mRealRotationAngle*90, screen->mElmRotationAngle*90);
    if (screen->mElmRotationAngle == (int)IMAGE_UTIL_ROTATION_NONE) {
        screen->mElmRotationAngle = (int)IMAGE_UTIL_ROTATION_270;
    } else {
        screen->mElmRotationAngle--;
    }

    if (screen->mRealRotationAngle == (int)IMAGE_UTIL_ROTATION_NONE) {
        screen->mRealRotationAngle = (int)IMAGE_UTIL_ROTATION_270;
    } else {
        screen->mRealRotationAngle--;
    }

    elm_image_orient_set(screen->mPhoto, (Elm_Image_Orient)(screen->mElmRotationAngle));

    Log::debug(ADCRS_LOG_PREFIX "   new angles: mRealRotation=%d , mElmRotation=%d",
               screen->mRealRotationAngle*90, screen->mElmRotationAngle*90);
    screen->DrawCrop(false);
}

void AddCropScreen::ApplyNewPicture(unsigned char *ptr)
{
    char dirName[MAX_FULL_PATH_NAME_LENGTH];
    char * imageStoragePath = Utils::GetInternalStorageDirectory(STORAGE_DIRECTORY_IMAGES);
    Utils::Snprintf_s(dirName, MAX_FULL_PATH_NAME_LENGTH, "%s/%s", imageStoragePath, POST_COMPOSER_TEMP_DIR);
    free(imageStoragePath);

    struct stat st = { 0 };
    if (stat(dirName, &st) == -1) {
        mkdir(dirName, 0740);
    }

    char * fullFileName = (char*)malloc(MAX_FULL_PATH_NAME_LENGTH * sizeof(char));
    Utils::Snprintf_s(fullFileName, MAX_FULL_PATH_NAME_LENGTH * sizeof(char), "%s/%.0f.jpg", dirName, Utils::GetCurrentTime());

    int crpImageW = mCroppedImageX2 - mCroppedImageX1;
    int crpImageH = mCroppedImageY2 - mCroppedImageY1;
    int ret = image_util_encode_jpeg(ptr, crpImageW, crpImageH, IMAGE_UTIL_COLORSPACE_RGB888, 100, (const char*)fullFileName);
    Log::debug(ADCRS_LOG_PREFIX " ApplyNewPicture() after encode: realW=%d realH=%d", crpImageW, crpImageH);

    if (ret != IMAGE_UTIL_ERROR_NONE) {
        Log::debug(ADCRS_LOG_PREFIX "   Could not encode jpeg");
        return;
    }
    Log::debug(ADCRS_LOG_PREFIX "   Successfully encoded jpeg");

    if (mMediaData->GetFilePath() && mMediaData->GetOriginalPath() && strcmp(mMediaData->GetFilePath(), mMediaData->GetOriginalPath())) {
        Utils::DeleteMediaFileFromDB(mMediaData->GetMediaId());
        remove(mMediaData->GetFilePath());
    }
    mMediaData->SetTempCropImagePath(fullFileName);
    ret = Utils::InsertMediaFileToMediaDB(fullFileName, set_new_thumbnail_callback, mMediaData);
    if (ret != MEDIA_CONTENT_ERROR_NONE) {
        mMediaData->SetFilePath(mMediaData->GetOriginalPath());
        free(fullFileName);
    }
}

bool AddCropScreen::set_new_thumbnail_callback(media_info_h media, void *user_data)
{
    Log::debug(ADCRS_LOG_PREFIX " set_new_thumbnail_callback");
    char * thumbnailPath = NULL;
    int ret = media_info_get_thumbnail_path(media, &thumbnailPath);
    if (ret != MEDIA_CONTENT_ERROR_NONE) {
        Log::debug(ADCRS_LOG_PREFIX "   Could not get thumbnail path");
        return true;
    }
    Log::debug(ADCRS_LOG_PREFIX "   Successfully got thumbnail path");

    PostComposerMediaData * mediaData = static_cast<PostComposerMediaData *>(user_data);

    if (mediaData->GetThumbnailPath() != mediaData->GetOriginalThumbnailPath()) {
       remove(mediaData->GetThumbnailPath());
       free((void *)mediaData->GetThumbnailPath());
    }
    mediaData->SetThumbnailPath(thumbnailPath);

    char * mediaId = NULL;
    ret = media_info_get_media_id(media, &mediaId);
    if (ret != MEDIA_CONTENT_ERROR_NONE) {
        Log::debug(ADCRS_LOG_PREFIX "   Could not get media ID");
        return true;
    }
    Log::debug(ADCRS_LOG_PREFIX "   Successfully got media ID");

    free((void *)mediaData->GetMediaId());
    mediaData->SetMediaId(mediaId);

    unsigned long long fileSize;
    ret = media_info_get_size(media, &fileSize);
    if (ret == MEDIA_CONTENT_ERROR_NONE) {
        Log::debug(ADCRS_LOG_PREFIX "   Successfully got size");
        mediaData->SetFileSize(fileSize);
    } else {
        Log::debug(ADCRS_LOG_PREFIX "   Could not get size");
    }
    return true;
}

