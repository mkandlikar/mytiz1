#include "CacheManager.h"
#include "Common.h"
#include "ConnectivityManager.h"
#include "ExceptOrSpecifyFriendsScreen.h"
#include "GroupProfilePage.h"
#include "HomeProvider.h"
#include "HomeScreen.h"
#include "OperationManager.h"
#include "PostUpdateOperation.h"
#include "ProfileScreen.h"
#include "ShareWithScreen.h"
#include "WidgetFactory.h"


#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "EDIT_PRIVACY"


/*
 * Class ShareWithScreen::ListItem
 */

ShareWithScreen::ListItem::ListItem(int radioValue, PCPrivacyOption *privacyOption, const char *bottomText, const char *secondImage, PCPrivacyValue privacyValue) :
    mItemLayout(nullptr), mRadioValue(radioValue), mFirstImage(nullptr), mTopText(nullptr), mBottomText(nullptr), mSecondImage(nullptr), mPrivacyValue(privacyValue), mPrivacyOption(privacyOption) {

    mFirstImage = mPrivacyOption->GenIconPath(false);
    if (EFriendsExcept == radioValue) {
        mTopText = strdup("IDS_FRIENDS_EXCEPT");
    } else if (EFriendsSpecific == radioValue) {
        mTopText = strdup("IDS_FRIENDS_SPECIFIC");
    } else {
        mTopText = strdup(mPrivacyOption->GetDescription());
    }

    mBottomText = SAFE_STRDUP(bottomText);
    mSecondImage = SAFE_STRDUP(secondImage);
}

ShareWithScreen::ListItem::ListItem(RadioValue radioValue, const char *firstImage, const char *topText, const char *bottomText, const char *secondImage, PCPrivacyValue privacyValue) :
    mItemLayout(nullptr), mRadioValue(radioValue), mFirstImage(nullptr), mTopText(nullptr), mBottomText(nullptr), mSecondImage(nullptr), mPrivacyValue(privacyValue), mPrivacyOption(nullptr) {

    mTopText = SAFE_STRDUP(topText);
    mBottomText = SAFE_STRDUP(bottomText);
    mSecondImage = SAFE_STRDUP(secondImage);
}

ShareWithScreen::ListItem::~ListItem() {
    delete[] mFirstImage;
    free((char *)mTopText);
    free((char *)mBottomText);
    free((char *)mSecondImage);

}

ShareWithScreen::ListItem *ShareWithScreen::GetListItemByLayout(Evas_Object *layout) {
    ListItem *li = nullptr;
    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(mListItems, l, listData) {
        ListItem *lli = (ListItem *) listData;
        if (layout && lli->mItemLayout && layout == lli->mItemLayout) {
            li = lli;
            break;
        }
    }
    if (li) {
        Log::debug_tag(LOG_TAG, "GetListItemByLayout() -> ITEM:%s", li->mTopText);
    }
    return li;
}

ShareWithScreen::ListItem *ShareWithScreen::GetListItemByDescription(const char *text) {
    ListItem *li = nullptr;
    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(mListItems, l, listData) {
        ListItem *lli = (ListItem *) listData;
        if (text && lli->mTopText && !strcmp(lli->mTopText, text)) {
            li = lli;
            break;
        }
    }
    if (li) {
        Log::debug_tag(LOG_TAG, "GetListItemByDescription(%s) -> ITEM:%s:", text, li->mTopText);
    }
    return li;
}

ShareWithScreen::ListItem *ShareWithScreen::GetListItemByPrivacyOption(PCPrivacyOption* po) {
    ListItem *li = nullptr;
    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(mListItems, l, listData) {
        ListItem *lli = (ListItem *) listData;
        if (po && lli->mPrivacyOption && po == lli->mPrivacyOption) {
            li = lli;
            break;
        }
    }
    if (li) {
        Log::debug_tag(LOG_TAG, "GetListItemByPrivacyOption() -> ITEM:%s", li->mTopText);
    }
    return li;
}

ShareWithScreen::ListItem *ShareWithScreen::GetListItemById(int id) {
    ListItem *li = nullptr;
    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(mListItems, l, listData) {
        ListItem *lli = (ListItem *) listData;
        if (lli->mRadioValue == id) {
            li = lli;
            break;
        }
    }
    if (li) {
        Log::debug_tag(LOG_TAG, "GetListItemById(%d) -> ITEM:%s", id, li->mTopText);
    }
    return li;
}

ShareWithScreen::ListItem *ShareWithScreen::GetSelectedListItem() {
    int selected = elm_radio_value_get(mRadioGroup);
    ListItem *li = GetListItemById(selected);
    if (li) {
        Log::debug_tag(LOG_TAG, "GetSelectedListItem() -> SELECTED ITEM:%s", li->mTopText);
    }
    return li;
}

Evas_Object* ShareWithScreen::ListItem::CreateImage(const char *file) {
    Evas_Object *img = elm_image_add(mItemLayout);
    Application::GetInstance()->SetElmImageFile(img, file);
    return img;
}

void ShareWithScreen::ListItem::AddLayout(Evas_Object *parent, int width) {
    mItemLayout = elm_layout_add(parent);
    elm_layout_file_set(mItemLayout, Application::mEdjPath, "share_with_item_check_image_doubletext_image");

    if (mRadioValue != EMore) {
        Evas_Object *radio = elm_radio_add(mItemLayout);
        elm_radio_state_value_set(radio, mRadioValue);
        elm_object_part_content_set(mItemLayout, "swallow.check", radio);
    }

    if (mFirstImage) {
        Evas_Object* image = CreateImage(mFirstImage);
        elm_object_part_content_set(mItemLayout, "swallow.icon", image);
    }

    if (EFriendsExcept == mRadioValue || EFriendsSpecific == mRadioValue || EMore == mRadioValue) {
        if (mBottomText) {
            FRMTD_TRNSLTD_PART_TXT_SET1(mItemLayout, "toptext", "%s", mTopText);
            FRMTD_TRNSLTD_PART_TXT_SET1(mItemLayout, "bottomtext", "%s", mBottomText);
        } else {
            FRMTD_TRNSLTD_PART_TXT_SET1(mItemLayout, "text", "%s", mTopText);
        }
    } else if (mBottomText) {
        elm_object_part_text_set(mItemLayout, "toptext", mTopText);
        FRMTD_TRNSLTD_PART_TXT_SET1(mItemLayout, "bottomtext", "%s", mBottomText);
    } else {
        elm_object_part_text_set(mItemLayout, "text", mTopText);
    }

    if (mSecondImage) {
        Evas_Object* secImage = CreateImage(mSecondImage);
        elm_object_part_content_set(mItemLayout, "swallow.second_icon", secImage);
    }

    evas_object_size_hint_min_set(mItemLayout, width, 0);
    elm_box_pack_end (parent, mItemLayout);
    evas_object_show(mItemLayout);
}

void ShareWithScreen::ListItem::UpdateLayout() {
    free((char *)mTopText);
    DLT_TRNSLTD_OBJ(mItemLayout);
    if (EFriendsExcept == mRadioValue || EFriendsSpecific == mRadioValue) {
        mTopText = strdup(EFriendsExcept == mRadioValue ? "IDS_FRIENDS_EXCEPT" : "IDS_FRIENDS_SPECIFIC");
        if (mBottomText) {
            FRMTD_TRNSLTD_PART_TXT_SET1(mItemLayout, "toptext", "%s", mTopText);
        } else {
            FRMTD_TRNSLTD_PART_TXT_SET1(mItemLayout, "text", "%s", mTopText);
        }
    } else {
        mTopText = strdup(mPrivacyOption->GetDescription());
        if (mBottomText) {
            elm_object_part_text_set(mItemLayout, "toptext", mTopText);
        } else {
            elm_object_part_text_set(mItemLayout, "text", mTopText);
        }
    }

    if (mBottomText) {
        FRMTD_TRNSLTD_PART_TXT_SET1(mItemLayout, "bottomtext", "%s", mBottomText);
        elm_object_part_text_set(mItemLayout, "text", "");
    } else {
        elm_object_part_text_set(mItemLayout, "toptext", "");
        elm_object_part_text_set(mItemLayout, "bottomtext", "");
    }
}


void ShareWithScreen::ListItem::RemoveLayout(Evas_Object *parent) {
    elm_box_unpack (parent, mItemLayout);
    evas_object_hide(mItemLayout);
    evas_object_del(mItemLayout);
    mItemLayout = nullptr;
}

void ShareWithScreen::ListItem::SetBottomText(const char *bottomText) {
    free((char *)mBottomText);
    mBottomText = SAFE_STRDUP(bottomText);
}


/*
 * Class ShareWithScreen
 */

ShareWithScreen::ShareWithScreen(ScreenBase *screen, PCPrivacyModel *privacyModel, Post *post) : ScreenBase(nullptr),
    mScreen(screen), mListItems(nullptr), mPrivacyModel(privacyModel), mPost(post), mConfirmationDialog(nullptr) {

    mScreenId = ScreenBase::SID_SHARE_WITH;
    if (mPost) {
        mPost->AddRef();
    }
    //mPrivacyModel isn't NULL if the screen started from PostComposer. Otherwise, mPost must not be NULL.
    assert(mPrivacyModel || mPost);
    if (!mPrivacyModel) {
        mPrivacyModel = new PCPrivacyModel;
        mPrivacyModel->SetTaggedFriendsFromPost(mPost);
    }
    mPrivacyModel->AddObserver(this);

    mLayout = elm_layout_add(getParentMainLayout());
    elm_layout_file_set(mLayout, Application::mEdjPath, "share_with_screen");

    const char *caption = mPost ? "IDS_EDIT_PRIVACY" : "IDS_SHARE_WITH";
    Evas_Object *header_text = PostComposerScreen::CreateCaptionText(mLayout, caption);

    Evas_Object *header = nullptr;
    if (mPost) {
        header = WidgetFactory::CreateGrayHeaderWithLogo(mLayout, header_text, "IDS_SAVE", this);
    } else {
        header = WidgetFactory::CreateGrayHeaderWithLogo(mLayout, header_text, nullptr, this);
    }
    elm_object_part_content_set(mLayout, "swallow.top_toolbar", header);

    mRadioGroup = nullptr;

    if (mPrivacyModel->IsModelReady()) { //at least cache is available.
        CreateOptionsList();
        RefreshList();
    } else {
        if(ConnectivityManager::Singleton().IsConnected()) {
            ProgressBarShow();
        } else {
            ShowErrorWidget(ScreenBase::eCONNECTION_ERROR);
        }
    }
    if (mPost) {
        DisablePostButton(true);
    }
}

void ShareWithScreen::CreateOptionsList() {
//Scroller
    Evas_Object *scroller = elm_scroller_add(mLayout);
    evas_object_size_hint_weight_set(scroller, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(scroller, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_scroller_policy_set(scroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_OFF);
    elm_scroller_single_direction_set(scroller, ELM_SCROLLER_SINGLE_DIRECTION_HARD);
    elm_scroller_propagate_events_set(scroller, EINA_FALSE);
    elm_object_scroll_lock_x_set(scroller, EINA_TRUE);
    elm_object_part_content_set(mLayout, "swallow.scroller", scroller);

//Box
    mBox = elm_box_add(scroller);
    elm_box_horizontal_set(mBox, EINA_FALSE);
    evas_object_size_hint_align_set(mBox, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_object_content_set(scroller, mBox);
    evas_object_show(mBox);
}

void ShareWithScreen::PrivacyChanged(IPCPrivacyObserver::PrivacyEvent privacyEvent) {
    switch (privacyEvent) {
    case IPCPrivacyObserver::EPrivacyOptionsRefreshed:
        assert(mPost); //this event may happen only for Edit Privacy mode.
        if (!mPrivacyModel->IsCached()) { //we don't use cached data in the Edit Privacy Mode.
            ProgressBarHide();
            HideErrorWidget();
            CreateOptionsList();
            RefreshList();
            if (mPost) { //We set privacy manually only if we are in the Edit Privacy Mode.
                Privacy *postPrivacy = mPost->GetPrivacy();
                mPrivacyModel->SetOptionFromPostPrivacy(postPrivacy);
            }
        }
        break;
    case IPCPrivacyObserver::EPrivacyOptionManuallyChanged:
    case IPCPrivacyObserver::EPrivacyOptionChangedByProgram:
        SetActivePrivacyOption();
        //no break here - fall through!
    case IPCPrivacyObserver::EExceptedFriendsChanged:
    case IPCPrivacyObserver::ESpecificFriendsChanged: {
        ListItem *li = GetListItemById(EFriendsExcept);
        if (li) {
            char *exceptedFriends = mPrivacyModel->GenDeniedFriends();
            li->SetBottomText(exceptedFriends);
            delete []exceptedFriends;
            li->UpdateLayout();
        }

        li = GetListItemById(EFriendsSpecific);
        if (li) {
            char *specificFriends = mPrivacyModel->GenAllowedFriends();
            li->SetBottomText(specificFriends);
            delete []specificFriends;
            li->UpdateLayout();
        }

        if (mPost) {
            DisablePostButton(mPrivacyModel->IsCached() ||  //don't compare with cached data
                              !mPrivacyModel->IsPostPrivacyChanged(mPost->GetPrivacy()));
        }
        break;
    }
    default:
        break;
    }
}

void ShareWithScreen::SetActivePrivacyOption() {
    ListItem *li = GetListItemByPrivacyOption(mPrivacyModel->GetPrivacyOption());
    if (li) {
        int radioValue = li->mRadioValue;
        if (mStartedFriendsSelection) {
            /* Select the option the user has just clicked. This is necessary because on opening one
             * of the selection screens, current ShareWith screen remains visible for a rather long time!
             */
            if (EFriends == radioValue) {
                radioValue = EFriendsExcept;
            } else if (EOnlyMe == radioValue) {
                radioValue = EFriendsSpecific;
            }
        }
        elm_radio_value_set(mRadioGroup, radioValue);
    }
    mStartedFriendsSelection = false;
}

const unsigned int KMoreItemPosition = 5;

void ShareWithScreen::RefreshList() {
    int width;
    evas_object_geometry_get(getParentMainLayout(), nullptr, nullptr, &width, nullptr);

    ListItem *li = nullptr;
    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(mListItems, l, listData) {
        ListItem *li = (ListItem *) listData;
        li->RemoveLayout(mBox);
        delete li;
    }
    mListItems = eina_list_free(mListItems);

    int moreItemPosition = KMoreItemPosition;
    mRadioGroup = nullptr;
    int radioValue = ENextRadioValue;
    int totalCount = eina_list_count(mPrivacyModel->GetOptionsList());
    int i = 0;
    PCPrivacyOption *po = nullptr;
    PCPrivacyOption *selectedOption = mPrivacyModel->GetPrivacyOption();

    EINA_LIST_FOREACH(mPrivacyModel->GetOptionsList(), l, listData) {
        po = static_cast<PCPrivacyOption *> (listData);
        Log::debug_tag(LOG_TAG, "type:%s, description:%s", po->GetType(), po->GetDescription());
        if (!strcmp(PRIVACY_EVERYONE, po->GetType())) {
//Public
            li = new ListItem(EPublic, po, "IDS_ANYONE_ON_FACEBOOK", nullptr, EVEveryone);
            if (i < moreItemPosition) {
                li->AddLayout(mBox, width);
                elm_object_signal_callback_add(li->mItemLayout, "mouse,clicked", "item", onItemClicked, this);
            }
        } else if (!strcmp(PRIVACY_ALL_FRIENDS, po->GetType())) {
//Friends
            li = new ListItem(EFriends, po, "IDS_YOUR_FRIENDS_ON_FACEBOOK", nullptr, EVAllFriends);
            if (i < moreItemPosition) {
                li->AddLayout(mBox, width);
                elm_object_signal_callback_add(li->mItemLayout, "mouse,clicked", "item", onItemClicked, this);
            }
        } else if (!strcmp(PRIVACY_SELF, po->GetType())) {
//Only Me
            li = new ListItem(EOnlyMe, po, "IDS_ONLY_ME", nullptr, EVSelf);
            if (i < moreItemPosition) {
                li->AddLayout(mBox, width);
                elm_object_signal_callback_add(li->mItemLayout, "mouse,clicked", "item", onItemClicked, this);
            }
        } else if (!strcmp(PRIVACY_CUSTOM_MANUAL_DENIED, po->GetType())) {
//Friends except...
            char *exceptedFriends = mPrivacyModel->GenDeniedFriends();
            li = new ListItem(EFriendsExcept, po, exceptedFriends, ICON_DIR"/sharewith_next_arrow.png", EVCustomExcepted);
            delete [] exceptedFriends;
            if (i < moreItemPosition) {
                li->AddLayout(mBox, width);
                elm_object_signal_callback_add(li->mItemLayout, "mouse,clicked", "item", onItemClicked, this);
            }
        } else if (!strcmp(PRIVACY_CUSTOM_MANUAL_ALLOWED, po->GetType())) {
//Specific friends
            char *specificFriends = mPrivacyModel->GenAllowedFriends();
            li = new ListItem(EFriendsSpecific, po, specificFriends, ICON_DIR"/sharewith_next_arrow.png", EVCustomSpecific);
            delete [] specificFriends;
            if (i < moreItemPosition) {
                li->AddLayout(mBox, width);
                elm_object_signal_callback_add(li->mItemLayout, "mouse,clicked", "item", onItemClicked, this);
            }
        } else {
//any other (custom) item
            li = new ListItem(radioValue, po, nullptr, nullptr, EVUnknown);
            if (po == selectedOption) {
                li->AddLayout(mBox, width);
                elm_object_signal_callback_add(li->mItemLayout, "mouse,clicked", "item", onItemClicked, this);
                moreItemPosition++;
            }
            radioValue++;
        }
        mListItems = eina_list_append(mListItems, li);
        ++i;

        if (li->mItemLayout) {
            if (!mRadioGroup) {
                mRadioGroup = elm_object_part_content_get(li->mItemLayout, "swallow.check");
            } else {
                elm_radio_group_add(elm_object_part_content_get(li->mItemLayout, "swallow.check"), mRadioGroup);
            }
        }

        if (i > moreItemPosition &&    //options list is long
            i == totalCount) {    //the whole list is processed
//More...
            li = new ListItem(EMore, nullptr, "IDS_MORE", nullptr, nullptr, EVUnknown);
            mListItems = eina_list_append(mListItems, li);
            ++i;
            li->AddLayout(mBox, width);
            elm_object_signal_callback_add(li->mItemLayout, "mouse,clicked", "item", onItemMoreClicked, this);
        }
    }

    SetActivePrivacyOption();
}

ShareWithScreen::~ShareWithScreen(){
    delete mConfirmationDialog;
    void * listData;
    EINA_LIST_FREE(mListItems, listData) {
        ListItem *li = (ListItem *)listData;
        delete li;
    }
    mPrivacyModel->RemoveObserver(this);
    if (mPost) { // We own privacy model only if we are in the Post Edit Mode. Otherwise, it will deleted in the PostComposerScreen.
        delete mPrivacyModel;
        mPost->Release();
    }
}

void ShareWithScreen::Push() {
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(elm_naviframe_top_item_get(getNf()), EINA_FALSE, EINA_FALSE);
}

void ShareWithScreen::CancelCallback(void *data, Evas_Object *obj, void *event_info) {
    if (mPost && !mPrivacyModel->IsCached() && mPrivacyModel->IsPostPrivacyChanged(mPost->GetPrivacy())) {// only if we are in the Post Edit Mode.
        ShowKeepDiscardPopup();
        EnableCancelButton(true);
        EnableRightButton(true);
     } else {
         Pop();
     }
}

bool ShareWithScreen::HandleBackButton() {
    bool ret = false;
    if (mConfirmationDialog) {
        delete mConfirmationDialog;
        mConfirmationDialog = nullptr;
        ret = true;
    } else {
        if (mPost && !mPrivacyModel->IsCached() && mPrivacyModel->IsPostPrivacyChanged(mPost->GetPrivacy())) {// only if we are in the Post Edit Mode.
            ShowKeepDiscardPopup();
            ret = true;
        }
    }
    return ret;
}

void ShareWithScreen::OnResume()
{
    //Just in case refresh the actual selection, as after returning from FriendsExceptScreen it may need an update.
    SetActivePrivacyOption();
}

void ShareWithScreen::DisablePostButton(bool disable) {
    Evas_Object *header = elm_object_part_content_get(mLayout, "swallow.top_toolbar");
    if (disable) {
        EnableRightButton(false);
        elm_object_signal_emit(header, "disable_right_button", "header");
    } else {
        EnableRightButton(true);
        elm_object_signal_emit(header, "enable_right_button", "header");
    }
}

void ShareWithScreen::ChangePrivacy() {
//This function can only be called in the Edit Post Mode. So, mPost must be not NULL.
    assert(mPost);
    PrivacyOption *selectedOption = mPrivacyModel->GetPrivacyOption();
    assert(selectedOption);

    std::list<Friend> taggedFriends;
    bundle* paramsBundle;

    paramsBundle = bundle_create();
    char *privacy = mPrivacyModel->GenPrivacyString();
    const char *privacyIcon = selectedOption->GenIconPath(true);
    if(privacy && privacyIcon) {
        bundle_add_str(paramsBundle, "privacy", privacy);
        bundle_add_str(paramsBundle, "privacy_icon", privacyIcon);
    }
    free(privacy);
    delete[] privacyIcon;

    OperationManager::GetInstance()->AddOperation(MakeSptr<PostUpdateOperation>(PostUpdateOperation::OT_Post_Update,
            mPost->GetId() ? mPost->GetId() : "",
            paramsBundle,
            taggedFriends,
            selectedOption->GetType(),
            nullptr));
    bundle_free(paramsBundle);


    mPrivacyModel->UpdateDefaultPrivacyOptionOnFB();

    Pop();
}

void ShareWithScreen::ConfirmCallback(void *data, Evas_Object *obj, const char *emission, const char *source) {
    Eina_List *list = mPrivacyModel ? mPrivacyModel->FindExceptedFriendsWhoAreTagged() : nullptr;
    if (list) {
        ShowConfirmAccessForExceptedToPopup(list);
        list = eina_list_free(list);
    } else {
        ChangePrivacy();
    }
}

void ShareWithScreen::ShowConfirmAccessForExceptedToPopup(const Eina_List *list) {
    char *friends = PostComposerScreen::GenExceptedFriendsWhoAreTagged(list);
    char *message = WidgetFactory::WrapByFormat(i18n_get_text("IDS_CONFIRM_SHOW_POST_FOR_TAGGED"), friends);
    delete []friends;

    mConfirmationDialog = ConfirmationDialog::CreateAndShow(mLayout, NULL, message,
            "IDS_CANCEL", "IDS_OK", confirmation_access_for_excepted_cb, this);
    delete []message;
}

void ShareWithScreen::confirmation_access_for_excepted_cb(void *user_data, ConfirmationDialog::CDEventType event) {
    ShareWithScreen *screen = static_cast<ShareWithScreen *>(user_data);

    delete screen->mConfirmationDialog;
    screen->mConfirmationDialog = nullptr;

    switch (event) {
    case ConfirmationDialog::ECDYesPressed:
    case ConfirmationDialog::ECDDismiss:
        screen->EnableRightButton(true);
        break;
    case ConfirmationDialog::ECDNoPressed:
        screen->ChangePrivacy();
        break;
    default:
        break;
    }
}

void ShareWithScreen::ShowKeepDiscardPopup() {
    mConfirmationDialog = ConfirmationDialog::CreateAndShow(mLayout,
        "IDS_DISCARD_POST", "IDS_EDIT_PRIVACY_KEEP_DISCARD_TEXT", "IDS_KEEP", "IDS_DISCARD",
        confirmation_dialog_cb, this);
}

void ShareWithScreen::confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event) {
    ShareWithScreen *screen = static_cast<ShareWithScreen *>(user_data);

    delete screen->mConfirmationDialog;
    screen->mConfirmationDialog = nullptr;

    switch (event) {
    case ConfirmationDialog::ECDYesPressed:
    case ConfirmationDialog::ECDDismiss:
        break;
    case ConfirmationDialog::ECDNoPressed:
        screen->Pop();
        break;
    default:
        break;
    }
}

void ShareWithScreen::onItemMoreClicked (void *data, Evas_Object *obj, const char *emission, const char *source) {
    Log::debug_tag(LOG_TAG, "onItemMoreClicked(%s:%s)", emission, source);
    ShareWithScreen *me = (ShareWithScreen *)data;

    ListItem *li = me->GetListItemById(EMore);
    li->RemoveLayout(me->mBox);
    me->mListItems = eina_list_remove(me->mListItems, li);
    delete li;

    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(me->mListItems, l, listData) {
        ListItem *li = (ListItem *) listData;
        if (!li->mItemLayout) {
            li->AddLayout(me->mBox, R->SCREEN_SIZE_WIDTH);
            elm_object_signal_callback_add(li->mItemLayout, "mouse,clicked", "item", onItemClicked, me);
            Evas_Object *radio = elm_object_part_content_get(li->mItemLayout, "swallow.check");
            elm_radio_group_add(radio, me->mRadioGroup);
        }
    }
}

void ShareWithScreen::onItemClicked (void *data, Evas_Object *obj, const char *emission, const char *source) {
    ShareWithScreen *me = (ShareWithScreen*) data;
    ListItem *li = me->GetListItemByLayout(obj);

    if (li->mRadioValue == EFriendsExcept || li->mRadioValue == EFriendsSpecific) {
        ExceptOrSpecifyFriendsScreen *selectionScreen;
        if (li->mRadioValue == EFriendsExcept) {
            selectionScreen =
                new ExceptOrSpecifyFriendsScreen(me->mPrivacyModel,
                                                 me->mPost ? "IDS_EDIT_PRIVACY" : "IDS_FRIENDS_EXCEPT",
                                                 ExceptOrSpecifyFriendsScreen::EExceptedFriendsMode);
        } else {
            selectionScreen =
                new ExceptOrSpecifyFriendsScreen(me->mPrivacyModel,
                                                 me->mPost ? "IDS_EDIT_PRIVACY" : "IDS_FRIENDS_SPECIFIC",
                                                 ExceptOrSpecifyFriendsScreen::ESpecificFriendsMode);
        }
        Application::GetInstance()->AddScreen(selectionScreen);
        me->mStartedFriendsSelection = true;
    }
    me->mPrivacyModel->PrivacyOptionSelected(li->mPrivacyOption);
}
