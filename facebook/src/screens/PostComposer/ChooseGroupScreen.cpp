/*
 * ChooseGroupScreen.cpp
 *
 *  Created on: Sep 9, 2015
 *      Author: ruibezruch
 */

#include "ChooseGroupScreen.h"
#include "Common.h"
#include "Group.h"
#include "GroupProvider.h"
#include "ConnectivityManager.h"
#include "WidgetFactory.h"

const int ChooseGroupScreen::DEFAULT_INIT_ITEMS_SIZE = 25;
const int ChooseGroupScreen::DEFAULT_MAX_ITEMS_SIZE = 80;

ChooseGroupScreen::ChooseGroupScreen(PostComposerScreen *postComposer) :
        ScreenBase(NULL), TrackItemsProxyAdapter(NULL, GroupProvider::GetInstance())
{
    mScreenId = ScreenBase::SID_CHOOSE_GROUP;
    mAction = new ActionBase(this);
    mPostComposerScreen = postComposer;
    mSearchEntry = NULL;

    mLayout = WidgetFactory::CreateChooseScreenLayout(this, getParentMainLayout(), "IDS_CHOOSE_GROUP");

    elm_object_translatable_part_text_set(mLayout, "error_text", "IDS_NO_GROUPS");

    elm_object_signal_emit(mLayout, "clear.btn", "hide");

    ApplyScroller(mLayout, true);
    elm_object_part_content_set(mLayout, "content", GetScroller());

    mSearchEntry = WidgetFactory::CreateChooseScreenEntry(mLayout);

    if (mSearchEntry) {
        Utils::SetKeyboardFocus(mSearchEntry, true);
        elm_entry_input_panel_show(mSearchEntry);
    }

    evas_object_smart_callback_add(mSearchEntry, "changed", on_search_btn_clicked_cb, this);
    evas_object_smart_callback_add(mSearchEntry, "activated", on_search_keypad_btn_clicked_cb, this);
    elm_object_signal_callback_add(mLayout, "mouse,clicked,*", "entry_search_*", on_search_btn_clicked_signal, this);
    elm_object_signal_callback_add(mLayout, "mouse,clicked,*", "entry_clear_*", on_clear_btn_clicked_cb, this);

    ProxySettings *settings = GetProxySettings();
    settings->wndSize = DEFAULT_MAX_ITEMS_SIZE;
    settings->initialWndSize = DEFAULT_INIT_ITEMS_SIZE;
    SetProxySettings(settings);

    RequestData();
}

ChooseGroupScreen::~ChooseGroupScreen()
{
    delete mAction;
    evas_object_del(mSearchEntry); mSearchEntry = NULL;
}


void ChooseGroupScreen::on_search_keypad_btn_clicked_cb(void *data, Evas_Object *obj, void *event_info)
{
    ChooseGroupScreen *me = static_cast<ChooseGroupScreen*>(data);

    if (me && me->mSearchEntry) {
        elm_entry_input_panel_hide(me->mSearchEntry);
    }
}

void ChooseGroupScreen::on_search_btn_clicked_cb(void *data, Evas_Object *obj, void *event_info)
{
    ChooseGroupScreen *me = static_cast<ChooseGroupScreen*>(data);

    me->OnSearchBtnClicked();
}

void ChooseGroupScreen::on_search_btn_clicked_signal(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    ChooseGroupScreen *me = static_cast<ChooseGroupScreen*>(data);

    me->OnSearchBtnClicked();
}

void ChooseGroupScreen::OnSearchBtnClicked()
{
    const char *str = elm_entry_entry_get(mSearchEntry);
    if (str && !strcmp(str, "")) {
        elm_object_signal_emit(mLayout, "clear.btn", "hide");
    } else {
        elm_object_signal_emit(mLayout, "clear.btn", "show");
    }

    char * searchStr = elm_entry_markup_to_utf8(elm_entry_entry_get(mSearchEntry));

    if (searchStr) {
        GroupProvider::GetInstance()->SetFindText(searchStr);
        free(searchStr);
        EraseWindowData();
        ShiftDown();
    }

    SetNoDataViewVisibility(GroupProvider::GetInstance()->GetDataCount() == 0);
}

void ChooseGroupScreen::on_clear_btn_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    ChooseGroupScreen *me = static_cast<ChooseGroupScreen*>(data);
    if (me && me->mSearchEntry) {
        elm_entry_entry_set(me->mSearchEntry, "");
    }
}

void ChooseGroupScreen::Push()
{
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void* ChooseGroupScreen::CreateItem( void* dataForItem, bool isAddToEnd )
{
    ActionAndData *data = NULL;
    GraphObject *graphObj = static_cast<GraphObject*>(dataForItem);
    if (graphObj) {
        switch (graphObj->GetGraphObjectType()) {
        case GraphObject::GOT_GROUP: {
            Group *group = static_cast<Group *>(dataForItem);
            if (group) {
                data = new ActionAndData(group, mAction);
                data->mParentWidget = CreateGroupItem(data, GetDataArea(), isAddToEnd);
            }
        }
            break;
        default:
            break;
        }
    }
    return data;
}

Evas_Object *ChooseGroupScreen::CreateGroupItem(ActionAndData *action_data, Evas_Object* parent, bool isAddToEnd)
{
    Evas_Object *list_item = WidgetFactory::CreateChooseScreenItem(parent, action_data);

    elm_object_signal_callback_add(list_item, "mouse,clicked,*", "item_*", on_item_clicked_cb, action_data);

    if (isAddToEnd) {
        elm_box_pack_end(parent, list_item);
    } else {
        elm_box_pack_start(parent, list_item);
    }

    return list_item;
}

void ChooseGroupScreen::on_item_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    Group *group = static_cast<Group*>(action_data->mData);
    ChooseGroupScreen *me = static_cast<ChooseGroupScreen*>(action_data->mAction->getScreen());

    if (me->mPostComposerScreen) {
        me->mPostComposerScreen->SetSelectedGroupTimeline(group);
        me->mPostComposerScreen->SetPhotoAlbumButtonMode(me->mPostComposerScreen->EHidden);
    }

    me->Pop();
}

void ChooseGroupScreen::SetDataAvailable(bool isAvailable)
{
    SetNoDataViewVisibility(!isAvailable);
}

void ChooseGroupScreen::SetNoDataViewVisibility(bool isVisible)
{
    SetShowConnectionErrorEnabled(true);
    if (GroupProvider::GetInstance()->GroupsDataRequestSuccessful()) {
        if (isVisible) {
            elm_object_signal_emit(mLayout, "error.show", "");
        } else {
            elm_object_signal_emit(mLayout, "error.hide", "");
        }
        if (!ConnectivityManager::Singleton().IsConnected()) {
            if (GroupProvider::GetInstance()->GetDataCount() == 0) {
                SetShowConnectionErrorEnabled(false); //show 'no groups' only
            }
        }
    } else {
        ShowConnectionError(false);
    }
}
