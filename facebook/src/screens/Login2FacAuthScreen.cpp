// Custom headers
#include "AppEvents.h"
#include "Login2FacAuthScreen.h"
#include "CacheManager.h"
#include "Common.h"
#include "Config.h"
#include "ConnectivityManager.h"
#include "CSmartPtr.h"
#include "InitializationDataUploader.h"
#include "jsonutilities.h"
#include "Log.h"
#include "LoginRequest.h"
#include "LoginScreen.h"
#include "MainScreen.h"
#include "Popup.h"
#include "SignUpInProgressScreen.h"
#include "ScreenBase.h"
#include "Utils.h"
#include "WebViewScreen.h"
#include "WidgetFactory.h"

// System headers
#include <account.h>
#include <algorithm>
#include <badge.h>
#include <cstdlib>
#include <efl_extension.h>

#define URL_HELP_F2ACODE "https://m.facebook.com/help/contact/1729592097283562"
#define URL_HELP_CENTER_ACCOUNT_DISABLED "https://www.facebook.com/help/245058342280723"

#define MAX_FIELD_TEXT_LEN 512

Login2FacAuthScreen::Login2FacAuthScreen(ScreenBase *parentScreen, const char *username, const char *password, const char *firstFactor) : ScreenBase(parentScreen) {
    mFirstFactor = SAFE_STRDUP(firstFactor);
    mUserName = SAFE_STRDUP(username);
    mPswdAcc = SAFE_STRDUP(password);
    mScreenInProgress = nullptr;
    mLoginRequest = nullptr;
    mInitDataUploader = nullptr;
    mConfirmationDialog = nullptr;
    CreateView();
    AppEvents::Get().Subscribe(eINTERNET_CONNECTION_CHANGED, this);

    // Show pop-up
    ShowAlertPopup("IDS_LOGIN_APPROVAL_ERR_MSG", "IDS_LOGIN_APPROVAL_TITLE");
}

Login2FacAuthScreen::~Login2FacAuthScreen() {
    elm_object_signal_callback_del(mLayout, "got.a.auth.resendcode.click", "login_2fac_resend_code", resend_code_btn_cb);
    elm_object_signal_callback_del(mLayout, "got.a.auth.needhelp.click", "login_2fac_needhelp_link", need_help_btn_cb);
    evas_object_smart_callback_del(mEvasLoginCodeInput, "activated", login_btn_cb);
    evas_object_smart_callback_del(mEvasLoginCodeInput, "changed", entry_changed_cb);
    delete mInitDataUploader;
    delete mLoginRequest;
    DeleteConfirmationDialog();
    free((char *) mFirstFactor);
    free((char *) mUserName);
    free((char *) mPswdAcc);
}

void Login2FacAuthScreen::Push() {
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void Login2FacAuthScreen::LoadEdjLayout() {
    mLayout = elm_layout_add(getParentMainLayout());
    elm_layout_file_set(mLayout, Application::mEdjPath, "login_2fac_screen");

    evas_object_size_hint_weight_set(mLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);
}

void Login2FacAuthScreen::CreateView() {
    LoadEdjLayout();

    mEvasLoginCodeInput = CreateInput(ELM_INPUT_PANEL_LAYOUT_NUMBERONLY, "IDS_LOGIN_CODE");
    elm_object_part_content_set(mLayout, "login_2fac_code_input", mEvasLoginCodeInput);

    elm_object_translatable_part_text_set(mLayout, "login_2fac_button", "IDS_LOGIN_CONTINUE");
    elm_object_signal_emit(mLayout, "disable_login_2fac_button", "login_2fac_button");
    elm_object_signal_emit(mLayout, "button_hide", "login_2fac_button_bg");

    elm_object_translatable_part_text_set(mLayout, "login_2fac_resend_code", "IDS_RESEND_CODE");
    elm_object_signal_callback_add(mLayout, "got.a.auth.resendcode.click", "login_2fac_resend_code", resend_code_btn_cb, this);

    elm_object_translatable_part_text_set(mLayout, "login_2fac_needhelp_link", "IDS_HAVING_TROUBLE");
    elm_object_signal_callback_add(mLayout, "got.a.auth.needhelp.click", "login_2fac_needhelp_link", need_help_btn_cb, this);
}

Evas_Object* Login2FacAuthScreen::CreateInput(Elm_Input_Panel_Layout  entryType, const char *placeHolderTxt) {
    Evas_Object *input = elm_entry_add(mLayout);
    elm_entry_scrollable_set(input, EINA_TRUE);
    elm_entry_single_line_set(input, EINA_TRUE);
    elm_entry_input_panel_layout_set(input, entryType);

    elm_entry_input_panel_return_key_type_set(input, ELM_INPUT_PANEL_RETURN_KEY_TYPE_DONE);
    elm_entry_input_panel_return_key_disabled_set(input, EINA_TRUE);
    evas_object_smart_callback_add(input, "activated", login_btn_cb, this);
    evas_object_smart_callback_add(input, "changed", entry_changed_cb, this);

    FRMTD_TRNSLTD_PART_TXT_SET1(input, "elm.guide", "<font_size=40>%s</font_size>", placeHolderTxt);
    elm_entry_text_style_user_push(input, "DEFAULT='color=#FFF font_size=40'");
    evas_object_size_hint_align_set(input, EVAS_HINT_FILL, 0.0);
    evas_object_size_hint_weight_set(input, EVAS_HINT_EXPAND, 0.0);
    elm_object_focus_set(input, EINA_FALSE);

    elm_entry_cnp_mode_set(input, ELM_CNP_MODE_PLAINTEXT);

    return input;
}

void Login2FacAuthScreen::login_btn_cb(void *data, Evas_Object *obj, void *eventInfo) {
    login_btn_cb(data, obj, nullptr, nullptr);
}

void Login2FacAuthScreen::login_btn_cb(void *data, Evas_Object *obj, const char *emission, const char *source) {
	Login2FacAuthScreen *self = static_cast<Login2FacAuthScreen*>(data);
    const char *loginCode = elm_entry_entry_get(self->mEvasLoginCodeInput);
    if (loginCode && (strlen(loginCode) > MAX_FIELD_TEXT_LEN)) {
        // Prevent buffer overflow in case of pasting too long text in credential fields.
        notification_status_message_post(i18n_get_text("IDS_MAX_CHARACTERS_REACHED"));
        return;
    }

    if (!ConnectivityManager::Singleton().IsConnected()) {
        WidgetFactory::ShowAlertPopup("IDS_LOGIN_CONNECT_ERR", "IDS_LOGIN_FAILED_TITLE");
    } else {
        self->mScreenInProgress = new SignUpInProgressScreen(nullptr);
        self->mScreenInProgress->mInProgress = true;
        self->mScreenInProgress->mRequestType = LoginReq;
        Application::GetInstance()->AddScreen(self->mScreenInProgress);

        // Second request for F2A authorization includes first_factor and Login Code from SMS as two_factor
        self->mLoginRequest = new LoginRequest(OnLoginCompleted, self, self->mUserName, self->mPswdAcc, self->mFirstFactor, loginCode);

        self->mScreenInProgress->mGraphRequest = self->mLoginRequest;
        self->mLoginRequest->DoLogin();
    }
}

void Login2FacAuthScreen::resend_code_btn_cb(void *data, Evas_Object *obj, const char *emission, const char *source) {
	Login2FacAuthScreen *self = static_cast<Login2FacAuthScreen *>(data);
    if (!ConnectivityManager::Singleton().IsConnected()) {
        WidgetFactory::ShowAlertPopup("IDS_LOGIN_CONNECT_ERR", "IDS_LOGIN_FAILED_TITLE");
    } else {
        self->mLoginRequest = new LoginRequest(OnResendCodeCompleted, self, nullptr, nullptr, self->mFirstFactor);
        self->mLoginRequest->ResendLoginCodeExecuteAsync();
    }
}

void Login2FacAuthScreen::OnResendCodeCompleted(void *object, char *response, int code) {
    Login2FacAuthScreen *self = static_cast<Login2FacAuthScreen *>(object);
    // Check curl code. If it is not valid, display a connectivity error popup & go back to Auth Screen.
    if (CURLE_OK != code) {
        self->DisplayCurlErrorMsg(code);
        free(response);
        return;
    }
    JsonObject *rootObject;
    JsonParser *parser = openJsonParser(response, &rootObject);
    JsonObject *error = json_object_get_object_member(rootObject, "error");
    if (error) {
        elm_entry_entry_set(self->mEvasLoginCodeInput, "");
        int errorCode = json_object_get_int_member(error, "code");
        int errorSubcode = json_object_get_int_member(error, "error_subcode");
        if (errorCode == 10) {
            self->ShowAccountDisabledPopup();
        } else if (errorCode == 9) {
            self->ShowAlertPopup("IDS_AUTH_TOO_MANY_SMS_ERR_MSG", "IDS_AUTH_TOO_MANY_SMS");
        } else if (errorCode == 1 && errorSubcode == 99) {
            self->ShowAlertPopup("IDS_LOGIN_UNKNOWN_ERROR", "IDS_LOGIN_FAILED_TITLE");
        } else {
            std::string errMsgTitle = json_object_get_string_member(error, "error_user_title");
            std::string errMsg = json_object_get_string_member(error, "error_user_msg");
            self->ShowAlertPopup(errMsg.c_str(), errMsgTitle.empty() ? nullptr : errMsgTitle.c_str());
        }
    }
    g_object_unref(parser);
    free(response);
}
void Login2FacAuthScreen::need_help_btn_cb(void *data, Evas_Object *obj, const char *emission, const char *source) {
    Login2FacAuthScreen *self = static_cast<Login2FacAuthScreen *>(data);
	// Make help link for 2FA
	std::string helpUri = URL_HELP_F2ACODE;
	helpUri.append ("?u=").append (Config::GetInstance().GetUserId().c_str()).append ("&n=").append(self->mFirstFactor);
	helpUri.append("&Field1008492539162442").append("=%7B\"u\"%3A").append(Config::GetInstance().GetUserId().c_str()).append("%2C\"n\"%3A\"");
	helpUri.append(self->mFirstFactor).append("\"%7D&Field755946081169295=%7B\"u\"%3A").append(Config::GetInstance().GetUserId().c_str());
	helpUri.append("%2C\"n\"%3A\"").append(self->mFirstFactor).append("\"%7D");
	WebViewScreen::Launch(helpUri.c_str());
}

void Login2FacAuthScreen::OnLoginCompleted(void *object, char *response, int code) {
    Login2FacAuthScreen *self = static_cast<Login2FacAuthScreen *>(object);
    self->mScreenInProgress->mInProgress = false;

    // Check curl code. If it is not valid, display a connectivity error popup & go back to Auth Screen.
    if (CURLE_OK != code) {
        self->Pop();
        self->DisplayCurlErrorMsg(code);
        free(response);
        return;
    }

    JsonObject *rootObject;
    JsonParser *parser = openJsonParser(response, &rootObject);
    const char *accessToken = json_object_get_string_member(rootObject, "access_token");
    if (accessToken == NULL) {
        Log::debug("Login2FacAuthScreen::Error in 2FA Login %s", response);
        const char *errorMsg = json_object_get_string_member(rootObject, "error_msg");
        self->Pop();

        if (errorMsg) {
            elm_entry_entry_set(self->mEvasLoginCodeInput, "");

            std::string Error = errorMsg;
            std::string ErrMsgTitle;
            std::string ErrMsg;
            if (std::string::npos != Error.find ("(401)")) {
                ErrMsgTitle = "IDS_AUTH_ERROR";
                ErrMsg = "IDS_AUTH_ERR_MSG";
            } else if (std::string::npos != Error.find ("(613)")) {
                ErrMsgTitle = "IDS_TRY_AGAIN_LATER";
                ErrMsg = "IDS_LOGIN_TRYING_TOO_OFTEN";
            } else {
                ErrMsg = errorMsg;
            }
            if (std::string::npos != Error.find("(10)")) {
                self->ShowAccountDisabledPopup();
            } else {
                self->ShowAlertPopup(ErrMsg.c_str(), ErrMsgTitle.empty() ? nullptr : ErrMsgTitle.c_str());
            }
        } else {
            // Handle unclassified errors caused by logging in.
            JsonObject *error = json_object_get_object_member(rootObject, "error");
            if (error) {
                elm_entry_entry_set(self->mEvasLoginCodeInput, "");

                int errorCode = json_object_get_int_member(error, "code");
                int errorSubcode = json_object_get_int_member(error, "error_subcode");
                if (errorCode == 1 && errorSubcode == 99) {
                    self->ShowAlertPopup("IDS_LOGIN_UNKNOWN_ERROR", "IDS_LOGIN_FAILED_TITLE");
                }
            }
        }
    } else {

        // Create Application Badge to display on Application Icon in the Launch Viewer
        int badgeRet = badge_add(PACKAGE);
        Log::debug("Login2FacAuthScreen::OnLoginCompleted()::badge_add() is %s. Return Value is %d", (BADGE_ERROR_NONE == badgeRet) ? "Success" : "Failure", badgeRet);

        Config::GetInstance().SetAccessToken(accessToken);
        Log::debug("Login2FacAuthScreen Access Token = %s", Config::GetInstance().GetAccessToken().c_str());

//      setLoginState(true) will be done later in provider_sequence_init_done_cb()
//      Application::GetInstance()->mDataService->setLoginState(true);

        Log::debug("User Id = %s", Config::GetInstance().GetUserId().c_str());
        loggedin_user_data prevUserData;
        prevUserData.rowCount = 0;
        prevUserData.prevUserId = NULL;
        CacheManager::GetInstance().SelectLoggedInUser(&prevUserData);
        // Insert the Current User ID in to the LoggedInUser Table, if the table is empty.
        if (0 == prevUserData.rowCount) {
            CacheManager::GetInstance().SetLoggedInUserData(Config::GetInstance().GetUserId().c_str());
        } else {
            // Compare the current user id & prev user id. If they are different delete the Notifications Data of the previous user.
            std::string currentUserId(Config::GetInstance().GetUserId());
            if (currentUserId != prevUserData.prevUserId)
            {
                Log::debug("Previous & Current Logged In user IDs are different. Deleting Notifications Data of the previous User.");
                CacheManager::GetInstance().DeleteDatabase();
                CacheManager::GetInstance().Init();
                CacheManager::GetInstance().SetLoggedInUserData(currentUserId.c_str());
            }
            free((char *)prevUserData.prevUserId);
        }

        Config::GetInstance().SetSessionKey(json_object_get_string_member(rootObject, "session_key"));
        Log::debug("Session Key = %s", Config::GetInstance().GetSessionKey().c_str());

        Config::GetInstance().SetSessionSecret(json_object_get_string_member(rootObject, "secret"));
        Log::debug("Session Secret = %s", Config::GetInstance().GetSessionSecret().c_str());
        Config::GetInstance().SetUserName(self->mUserName);
        // Add this account details to Tizen Account Settings
        account_h account = NULL;
        int account_id = 0;
        int ret = account_create(&account);
        Utils::AcctFuncRetValLog("account_create", ret);
        if (ACCOUNT_ERROR_NONE == ret) {
            // Set account User Name
            ret = account_set_user_name(account, self->mUserName);
            Utils::AcctFuncRetValLog("account_set_user_name", ret);
            // Set account Display Name
            ret = account_set_display_name(account, self->mUserName);
            Utils::AcctFuncRetValLog("account_set_display_name", ret);

            std::string accountIconPath(app_get_shared_resource_path());
            accountIconPath += "fb4t_icon.png";
            ret = account_set_icon_path(account, accountIconPath.c_str());
            Utils::AcctFuncRetValLog("account_set_icon_path", ret);

            // Insert Account into DB
            ret = account_insert_to_db(account, &account_id);
            Utils::AcctFuncRetValLog("account_insert_to_db", ret);
            /* save in the Data Base -> Accounts table */
            bool result = CacheManager::GetInstance().UpdateAccountData();
            Log::debug("UpdateAccountData() - %s", (result ? "Success" : "Failed"));

            ret = account_destroy(account);
            Utils::AcctFuncRetValLog("account_destroy", ret);
        }

        Login2FacAuthScreen *self = static_cast<Login2FacAuthScreen*>(object);
        self->StartDownloadInitData();

        AppEvents::Get().Notify(eACCOUNT_CHANGED, &account_id);
    }
    g_object_unref(parser);
    free(response);
}

void Login2FacAuthScreen::provider_sequence_init_done_cb() {
    Log::debug("Login2FacAuthScreen::provider_sequence_init_done_cb" );
    AppEvents::Get().Notify(eDATA_IS_READY);
    // OLD CODE FOR CHECK PURPOSE
    Application::GetInstance()->mNf = elm_naviframe_add(Application::GetInstance()->mConform);
    elm_object_content_set(Application::GetInstance()->mConform, Application::GetInstance()->mNf);

    Application::GetInstance()->RemoveAllScreensAndNfItemsFromNaviframe();
    ScreenBase *scr = (ScreenBase *) new MainScreen(NULL);
    Application::GetInstance()->AddScreen(scr);

    Application::GetInstance()->mDataService->setLoginState(true);
    Application::GetInstance()->HandleSuspendedAppControl();
}

void Login2FacAuthScreen::entry_changed_cb(void *data, Evas_Object *obj, void *eventInfo) {
	Login2FacAuthScreen *self = static_cast<Login2FacAuthScreen *>(data);
    elm_entry_input_panel_return_key_disabled_set(self->mEvasLoginCodeInput,
            elm_entry_is_empty(self->mEvasLoginCodeInput));

    self->UpdateLoginButton();
}

void Login2FacAuthScreen::UpdateLoginButton() {
    bool isEntriesEmpty = elm_entry_is_empty(mEvasLoginCodeInput);

    if (isEntriesEmpty) {
        EnableLoginButton(false);
        elm_object_signal_emit(mLayout, "disable_login_2fac_button", "login_2fac_button");
        elm_object_signal_emit(mLayout, "button_hide", "login_2fac_button_bg");
    } else {
        EnableLoginButton(true);
        elm_object_signal_emit(mLayout, "enable_login_2fac_button", "login_2fac_button");
        elm_object_signal_emit(mLayout, "button_show", "login_2fac_button_bg");
    }
}

void Login2FacAuthScreen::EnableLoginButton(bool enable) {
    if (enable && !mIsRightButtonEnabled) {
        elm_object_signal_callback_add(mLayout, "got.a.auth.button.click", "login_2fac_button*", login_btn_cb, this);
        mIsRightButtonEnabled = true;
    } else if (!enable && mIsRightButtonEnabled) {
        elm_object_signal_callback_del(mLayout, "got.a.auth.button.click", "login_2fac_button*", login_btn_cb);
        mIsRightButtonEnabled = false;
    }
}

bool Login2FacAuthScreen::HandleBackButton() {
    bool ret = true;
    if (WidgetFactory::CloseAlertPopup()) {
    } else if (DeleteConfirmationDialog()) {
    } else {
        ret = false;
    }
    return ret;
}

void Login2FacAuthScreen::StartDownloadInitData() {
    if ( !mInitDataUploader ) {
        // Need to request the main data before we start to use MainScreen
        mInitDataUploader = new InitializationDataUploader( provider_sequence_init_done_cb );
    }
    mInitDataUploader->StartUploading();
}

void Login2FacAuthScreen::DisplayCurlErrorMsg(int curlCode) {
    switch (curlCode) {
    case CURLE_SSL_CACERT:
        ShowAlertPopup("IDS_CURL_ERRORMSG_CACERT", NULL);
        break;
    default:
        ShowAlertPopup("IDS_LOGIN_CONNECT_ERR", NULL);
        break;
    }
}

void Login2FacAuthScreen::Update(AppEventId eventId, void *data) {
    switch (eventId) {
    case eINTERNET_CONNECTION_CHANGED:
        if (!ConnectivityManager::Singleton().IsConnected()) {
            if (SID_INPROGRESS == Application::GetInstance()->GetTopScreenId()) {
                Pop();
                DisplayCurlErrorMsg(CURLE_NO_CONNECTION_AVAILABLE);
            }
        }
        break;
    default:
        break;
    }
}

void Login2FacAuthScreen::ShowAccountDisabledPopup() {
    mConfirmationDialog =
            ConfirmationDialog::CreateAndShow(
                    mLayout,
                    "IDS_ACCOUNT_DISABLED",
                    "IDS_ACCOUNT_HAS_BEEN_DISABLED",
                    "IDS_OK",
                    "IDS_ACCOUNT_DISABLED_LEARN_MORE",
                    confirmation_dialog_cb,
                    this);
}

void Login2FacAuthScreen::confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event) {
    assert(user_data);
    Login2FacAuthScreen *me = static_cast<Login2FacAuthScreen*>(user_data);

    me->DeleteConfirmationDialog();

    if (ConfirmationDialog::ECDNoPressed == event) {
        WebViewScreen::Launch(URL_HELP_CENTER_ACCOUNT_DISABLED);
    }
}

bool Login2FacAuthScreen::DeleteConfirmationDialog() {
    if (mConfirmationDialog) {
        delete mConfirmationDialog;
        mConfirmationDialog = nullptr;
        return true;
    }
    return false;
}

void Login2FacAuthScreen::ShowAlertPopup(const char *message, const char *title) {
    // This workaround is required for Tizen 3.0 to prevent popup overlapping with keyboard
    if (Application::GetInstance()->GetPlatformVersion() >= 3) {
        evas_object_hide(mLayout);
    }

    WidgetFactory::ShowAlertPopup(message, title, mLayout);
}
