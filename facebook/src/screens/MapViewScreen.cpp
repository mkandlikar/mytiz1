#include "Common.h"
#include "Event.h"
#include "MapViewScreen.h"
#include "PostPresenter.h"
#include "Utils.h"
#include "WidgetFactory.h"

void MapViewScreen::Push() {
    ScreenBase::Push();
    // Remove standard blue header
    elm_naviframe_item_title_enabled_set(elm_naviframe_top_item_get(getNf()), EINA_FALSE, EINA_FALSE);
}

MapViewScreen::MapViewScreen(ActionAndData *data) : ScreenBase(NULL) {

    mScreenId = ScreenBase::SID_MAP_VIEW;

    mLayout = elm_layout_add(getParentMainLayout());

    elm_layout_file_set(mLayout, Application::mEdjPath, "map_view_screen");
    evas_object_show(mLayout);

    Evas_Object *headerData = CreateHeader(mLayout, data);
    elm_object_part_content_set(mLayout, "map_view_header", headerData);

    Evas_Object *box_post = WidgetFactory::CreateGreyWrapper(mLayout);
    elm_object_part_content_set(mLayout, "map_view_map", box_post);

    WidgetFactory::CreateLocationItemFullView(box_post, data);
}

MapViewScreen::~MapViewScreen() {}


void MapViewScreen::back_btn_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    MapViewScreen * screen = static_cast<MapViewScreen *>(data);
    if (screen){
        if (Application::GetInstance()->GetTopScreenId() == SID_MAP_VIEW) {
            screen->Pop();
        }
    }
}

Evas_Object *MapViewScreen::CreateHeader(Evas_Object *obj, void *user_data)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);

    Evas_Object *headerData = elm_layout_add(obj);
    elm_layout_file_set(headerData, Application::mEdjPath, "map_view_header_item");
    evas_object_show(headerData);

    //Add Icon
    Evas_Object *categoryIcon = elm_icon_add(headerData);
    elm_layout_content_set(headerData, "mwhg_icon", categoryIcon);
    ELM_IMAGE_FILE_SET(categoryIcon, ICON_COMMON_DIR "/back.png", nullptr);
    evas_object_show(categoryIcon);

    //Add Title
    if (action_data->mData) {
        std::string placeTitleNameStr;
        if (action_data->mData->GetGraphObjectType() == GraphObject::GOT_POST) {
            Post* post = static_cast<Post*>(action_data->mData);
            PostPresenter presenter(*post);
            placeTitleNameStr = presenter.GetLocationPlace();
        } else if (action_data->mData->GetGraphObjectType() == GraphObject::GOT_EVENT) {
            Event *event = static_cast<Event*>(action_data->mData);
            if (event->mPlace && event->mPlace->mName) {
                placeTitleNameStr = event->mPlace->mName;
            }
        }

        if (!placeTitleNameStr.empty()) {
            elm_object_part_text_set(headerData, "title_text", placeTitleNameStr.c_str());
        }
    }

    elm_object_signal_callback_add(headerData, "item_clicked", "mwhg_icon", back_btn_clicked_cb, this);

    return headerData;
}
