#include "Common.h"
#include "dlog.h"
#include "SignUpEmailScreen.h"
#include "SignUpWelcomeScreen.h"
#include "Utils.h"

#define BUF_SIZE 100
#define URL_FACEBOOK_TERMS "https://m.facebook.com/legal/terms"
//#define URL_FACEBOOK_TERMS "http://www.xhaus.com/headers" // Test server for user agent
#define URL_PRIVACY_POLICY "https://m.facebook.com/about/privacy"
#define URL_COOKIE_USE "https://m.facebook.com/help/cookies"

SignUpWelcomeScreen::SignUpWelcomeScreen(ScreenBase *parentScreen):ScreenBase(parentScreen)
{
    mLayout = elm_layout_add(getParentMainLayout());
    elm_layout_file_set(mLayout, Application::mEdjPath, "signup_welcome_screen");
    evas_object_size_hint_weight_set(mLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_show(mLayout);

    elm_object_translatable_part_text_set(mLayout, "join_fb_text", "IDS_SUP_JOIN_FB");
    create_next_button(mLayout);
    create_entry(mLayout);

    mTitleLabel = new char [50];
    eina_strlcpy(mTitleLabel,"Terms and Privacy", 50);

    elm_object_part_text_set(mLayout, "HeaderText", mTitleLabel);
    elm_object_signal_callback_add(mLayout, "HeaderBack", "HeaderBack",
            (Edje_Signal_Cb)HeaderBackCb, this);
    elm_object_translatable_part_text_set(mLayout, "NextButtonText", "IDS_SUP_WC_NEXT");
    elm_object_signal_callback_add(mLayout, "SignalNext", "SourceNext",
            (Edje_Signal_Cb)next_btn_clicked_cb, this);
    /*
    elm_naviframe_prev_btn_auto_pushed_set(this->getNf() , EINA_FALSE);
    Evas_Object *prev_btn = elm_button_add(mApp->mNf);
    elm_object_style_set(prev_btn, "naviframe/prev_btn/default");
    elm_object_item_part_content_set(elm_naviframe_top_item_get(this->getNf()), "prev_btn", prev_btn);
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "SignUp Welcome Screen");
    */

}

SignUpWelcomeScreen::~SignUpWelcomeScreen()
{
    //dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "SignUpWelcomeScreen DESTRUCTOR");
}

void
SignUpWelcomeScreen :: create_next_button(Evas_Object *parent)
{
    Evas_Object *next_btn = elm_button_add(parent);
    elm_object_style_set(next_btn, "anchor");
    elm_object_part_content_set(parent, "next_inner_swallow_button", next_btn);
    elm_object_translatable_text_set(next_btn, "IDS_SUP_WC_NEXT");
    evas_object_smart_callback_add(next_btn, "clicked", next_btn_clicked_cb, this);
}

void
SignUpWelcomeScreen :: create_entry(Evas_Object *parent)
{
    // ADD ENTRY
    Evas_Object *entry = elm_entry_add(parent);
    elm_object_part_content_set(parent, "signup_entry", entry);

    elm_object_part_text_set(entry, "elm.text",  elm_object_part_text_get(parent, "signup_terms_text"));
    elm_entry_editable_set(entry, EINA_FALSE);
//    elm_entry_cursor_handler_disabled_set(entry,EINA_FALSE);
//    elm_entry_select_allow_set(entry,EINA_FALSE);

    elm_entry_text_style_user_push(entry, "DEFAULT='font_size=23 color=#9298a4 align=left'");
    evas_object_size_hint_weight_set(entry, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(entry, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_smart_callback_add(entry, "anchor,clicked", anchor_clicked_cb, this);
    evas_object_show(entry);
}

void
SignUpWelcomeScreen :: next_btn_clicked_cb(void *data , Evas_Object *obj, void *event_info)
{
    ScreenBase *newScreen = new SignUpEmailScreen(NULL, false, false);
    Application::GetInstance()->AddScreen(newScreen);
    elm_naviframe_item_title_enabled_set(elm_naviframe_top_item_get(newScreen->getNf()), EINA_FALSE, EINA_FALSE);
}

void
SignUpWelcomeScreen :: anchor_clicked_cb(void *data, Evas_Object *obj, void *event_info)
{
    char buf[BUF_SIZE];

    Elm_Entry_Anchor_Info *event = (Elm_Entry_Anchor_Info*)event_info;

    if(!strcmp(event->name , "fb_terms"))
        Utils::Snprintf_s(buf, BUF_SIZE, URL_FACEBOOK_TERMS);
    else if(!strcmp(event->name , "fb_privacy_policy"))
        Utils::Snprintf_s(buf, BUF_SIZE, URL_PRIVACY_POLICY);
    else if(!strcmp(event->name , "fb_cookie_use"))
        Utils::Snprintf_s(buf, BUF_SIZE, URL_COOKIE_USE);

    WebViewScreen::Launch(buf);
}

void SignUpWelcomeScreen::HeaderBackCb(void* Data, Evas_Object* Obj, void* EventInfo) {
    SignUpWelcomeScreen* Me = static_cast<SignUpWelcomeScreen*>(Data);
    Me->Pop();
}
