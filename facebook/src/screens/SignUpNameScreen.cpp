/*
 *  SignUpNameScreen.cpp
 *
 *  Created on: 9th June 2015
 *      Author: Manjunath Raja
 */

#include <string>

#include "Common.h"
#include "CommonScreen.h"
#include "Config.h"
#include "dlog.h"
#include "SignUpNameScreen.h"
#include "SignUpBDateScreen.h"
#include "SignupRequest.h"

#define MAX_NAME_CHARS 100

SignUpNameScreen::SignUpNameScreen(ScreenBase *parentScreen):ScreenBase(parentScreen)  {
    CreateView();
}

SignUpNameScreen::~SignUpNameScreen() {

    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Destryoing SignUpNameScreen");
        evas_object_smart_callback_del(Application::GetInstance()->mConform, "virtualkeypad,state,on", on_screen_keyb_on);
        evas_object_smart_callback_del(Application::GetInstance()->mConform, "virtualkeypad,state,off", on_screen_keyb_off);

}

void SignUpNameScreen::CreateLayout(Evas_Object *Parent) {
    mLayout = elm_layout_add(Parent);
    elm_layout_file_set(mLayout, Application::mEdjPath, "signup_name_screen");
    evas_object_size_hint_weight_set(mLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_show(mLayout);

    evas_object_smart_callback_add(Application::GetInstance()->mConform, "virtualkeypad,state,on", on_screen_keyb_on, this);
    evas_object_smart_callback_add(Application::GetInstance()->mConform, "virtualkeypad,state,off", on_screen_keyb_off, this);
}


void SignUpNameScreen::on_screen_keyb_on(void *data, Evas_Object *obj, void *event_info)
{
   //Emit the following signal
    SignUpNameScreen *mescreen = static_cast<SignUpNameScreen *>(data);
    elm_object_signal_emit(mescreen->mLayout, "belowheader", "Header");
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "EMITTING SIGNAL SignUp NAME KB ON ");
}
void SignUpNameScreen::on_screen_keyb_off(void *data, Evas_Object *obj, void *event_info)
{
   //Emit the following signal
    SignUpNameScreen *mescreen = static_cast<SignUpNameScreen *>(data);
    elm_object_signal_emit(mescreen->mLayout, "upheader", "Header");
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "EMITTING SIGNAL KB SIGNUP NAME OFF ON");
}

void SignUpNameScreen::CreateView() {
    Evas_Object *Parent;
    Evas_Object *TmpEvasObj;

    Parent = getParentMainLayout();
    CreateLayout(Parent);

    elm_object_translatable_part_text_set(mLayout, "HeaderText", "IDS_ABOUT_YOU");
    elm_object_translatable_part_text_set(mLayout, "sup_name_heading", "IDS_SUP_NAME_HEADING");
    elm_object_translatable_part_text_set(mLayout, "sup_name_info", "IDS_SUP_NAME_INFO");

    TmpEvasObj = elm_layout_add(Parent);
    mEvasFNameInput = elm_entry_add(TmpEvasObj);
    CreateInput(mLayout, TmpEvasObj, mEvasFNameInput);
    elm_entry_prediction_allow_set(mEvasFNameInput, EINA_FALSE);
    elm_object_translatable_part_text_set(mEvasFNameInput, "elm.guide", "IDS_SUP_FIRST_NAME");
    elm_object_part_content_set(mLayout, "sup_first_name_input", TmpEvasObj);

    TmpEvasObj = elm_layout_add(Parent);
    mEvasLNameInput = elm_entry_add(TmpEvasObj);
    CreateInput(mLayout, TmpEvasObj, mEvasLNameInput);
    elm_entry_prediction_allow_set(mEvasLNameInput, EINA_FALSE);
    elm_object_translatable_part_text_set(mEvasLNameInput, "elm.guide", "IDS_SUP_LAST_NAME");
    elm_object_part_content_set(mLayout, "sup_last_name_input", TmpEvasObj);

    elm_object_translatable_part_text_set(mLayout, "sup_name_nxt_btn_txt", "IDS_SUP_WC_NEXT");
    elm_object_signal_callback_add(mLayout, "got.a.sup.nnxt.btn.click", "sup_name_nxt_btn*",
            (Edje_Signal_Cb)ContinueBtnCb, this);
    elm_object_signal_callback_add(mLayout, "HeaderBack", "HeaderBack",
            (Edje_Signal_Cb)HeaderBackCb, this);
}

void SignUpNameScreen::CreateInput(Evas_Object *Parent, Evas_Object *InputLayout, Evas_Object *Input)
{
    elm_layout_theme_set(InputLayout, "layout", "editfield", "singleline");
    evas_object_size_hint_align_set(InputLayout, EVAS_HINT_FILL, 0.0);
    evas_object_size_hint_weight_set(InputLayout, EVAS_HINT_EXPAND, 0.0);
    elm_entry_single_line_set(Input, EINA_TRUE);
    elm_entry_scrollable_set(Input, EINA_TRUE);
    elm_entry_text_style_user_push(Input, "DEFAULT='font_size=32 color=#141823'");
    evas_object_smart_callback_add(Input, "focused", InputFocusedCb, InputLayout);
    evas_object_smart_callback_add(Input, "unfocused", InputUnFocusedCb, InputLayout);
    evas_object_smart_callback_add(Input, "changed", InputChangedCb, InputLayout);
    evas_object_smart_callback_add(Input, "preedit,changed", InputChangedCb, InputLayout);
    elm_object_part_content_set(InputLayout, "elm.swallow.content", Input);

    Evas_Object *InputClearBtn = elm_button_add(InputLayout);
    elm_object_style_set(InputClearBtn, "editfield_clear");
    evas_object_smart_callback_add(InputClearBtn, "clicked", InputClearButtonCb, Input);
    elm_object_part_content_set(InputLayout, "elm.swallow.button", InputClearBtn);
}

void SignUpNameScreen::ContinueBtnCb(void *data, Evas_Object *obj, void *EventInfo) {
    SignUpNameScreen *self = static_cast<SignUpNameScreen*>(data);
    std::string FirstName = elm_entry_entry_get(self->mEvasFNameInput);
    std::string LastName = elm_entry_entry_get(self->mEvasLNameInput);
    bool IsValid = false;

    if (((!FirstName.empty() && !LastName.empty()) &&
          (!isspace(FirstName.at(0)) && !isspace(LastName.at(0))) &&
          ((FirstName.length() <= MAX_NAME_CHARS) && (LastName.length() <= MAX_NAME_CHARS)))) {
        SignupRequest::Singleton().SetData(SignupRequest::FIRSTNAME, elm_entry_entry_get(self->mEvasFNameInput));
        SignupRequest::Singleton().SetData(SignupRequest::LASTNAME, elm_entry_entry_get(self->mEvasLNameInput));
        ScreenBase *NewScreen = new SignUpBDateScreen(NULL);
        Application::GetInstance()->AddScreen(NewScreen);
        elm_naviframe_item_title_enabled_set(elm_naviframe_top_item_get(NewScreen->getNf()), EINA_FALSE, EINA_FALSE);
        IsValid = true;
    }

    elm_object_translatable_part_text_set(self->mLayout, "sup_name_info", (IsValid ? "IDS_SUP_NAME_INFO" : " "));
    elm_object_translatable_part_text_set(self->mLayout, "sup_name_error", (!IsValid ? "IDS_SUP_NAME_ERROR" : " "));
}

void SignUpNameScreen::InputClearButtonCb(void *data, Evas_Object *obj, void *EventInfo){
    Evas_Object *Input = (Evas_Object *)data;
    elm_entry_entry_set(Input, "");
}

void SignUpNameScreen::InputChangedCb(void *data, Evas_Object *obj, void *EventInfo){
    Evas_Object *Input = (Evas_Object *)data;
    if (!elm_entry_is_empty(obj) && elm_object_focus_get(obj))
        elm_object_signal_emit(Input, "elm,action,show,button", "");
    else
        elm_object_signal_emit(Input, "elm,action,hide,button", "");
}

void SignUpNameScreen::InputFocusedCb(void *data, Evas_Object *obj, void *EventInfo){
    Evas_Object *Input = (Evas_Object *)data;
    if (!elm_entry_is_empty(obj)){
        elm_object_signal_emit(Input, "elm,action,show,button", "");
    }
}

void SignUpNameScreen::InputUnFocusedCb(void *data, Evas_Object *obj, void *EventInfo){
    Evas_Object *Input = (Evas_Object *) data;
    elm_object_signal_emit(Input, "elm,action,hide,button", "");
}

void SignUpNameScreen::HeaderBackCb(void* Data, Evas_Object* Obj, void* EventInfo) {
    SignUpNameScreen* Me = static_cast<SignUpNameScreen*>(Data);
    Me->Pop();
}
