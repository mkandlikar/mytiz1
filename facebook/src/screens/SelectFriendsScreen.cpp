#include "Common.h"
#include "DataUploader.h"
#include "Log.h"
//AAA ToDo: remove PostComposer from here. Now it's needed.
#include "PostComposerScreen.h"
#include "SelectFriendsScreen.h"
#include "SuggestedFriend.h"
#include "SuggestedFriendsProvider.h"
#include "Utils.h"
#include "WidgetFactory.h"

#include <utils_i18n.h>

#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "SELECTFRIENDS"


void SelectFriendsScreen::UnregisterClient() {
    mClient = NULL;
}

void SelectFriendsScreen::onClearClicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    Log::info_tag(LOG_TAG, "onClearClicked() - %s:%s", emission, source);
    SelectFriendsScreen *me = static_cast<SelectFriendsScreen *>(data);
    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(me->mAllFriendsGroup->GetList(), l, listData) {
        ListItem * li = (ListItem *) listData;
        if (li->mData->mFriendInfo->mIsSelected) {
            li->mData->mFriendInfo->mIsSelected = false;
            li->mScreen->UpdateAllItemsById(li->mData->mFriendInfo->GetFriend()->GetId(), SelectFriendsScreen::ListItem::ECheck);
        }
    }
    me->mHighlightedFriend = nullptr;
    me->UpdateSelectedFriendsField();
}

void SelectFriendsScreen::onItemClicked (void *data, Evas_Object *obj, const char *emission, const char *source) {
    Log::info_tag(LOG_TAG, "%s:%s", emission, source);
    ListItem *item = static_cast<ListItem*> (data);

    item->mScreen->ItemSelected(item);
}

char *SelectFriendsScreen::GItemTextGet(void *data, Evas_Object *obj, const char *part)
{
      char *name = strdup(i18n_get_text("IDS_ALL_FRIENDS"));
      return name;
}

void SelectFriendsScreen::on_get_suggested_friends_completed(void* data) {
    Log::info_tag(LOG_TAG, "on_get_suggested_friends_completed");

    SelectFriendsScreen *me = static_cast<SelectFriendsScreen *> (data);
    me->UpdateSuggestedFriends(SuggestedFriendsProvider::GetInstance()->GetData());
}

SelectFriendsScreen::ListItem *SelectFriendsScreen::GetListItemForFriend(GraphObject *aFr) {
    ListItem * res = NULL;
    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(mAllFriendsGroup->GetList(), l, listData) {
        ListItem *li = static_cast<ListItem *> (listData);
        Friend *fr = li->mData->mFriendInfo->GetFriend();
        if (*fr == *aFr) {
            res = li;
            break;
        }
    }
    return res;
}

void SelectFriendsScreen::UpdateSuggestedFriends(const Eina_Array *suggested) {
    int suggestionOrder = -1;
    for (int i = 0; i < eina_array_count(suggested); ++i) {
        SuggestedFriend *suggestedFriend = static_cast<SuggestedFriend *> (eina_array_data_get(suggested, i));
        if(suggestedFriend) {
            ListItem *item = GetListItemForFriend(suggestedFriend);
            if (item && item->mData) {
                item->mData->mSuggestionOrder = ++suggestionOrder;
            }
        }
    }
    if (suggestionOrder > -1) {
        FillSuggestedGroup();
    }
}

const int KMaxSuggestedFriendsAtFirst = 5;
const int KMaxSuggestedFriends = 10;

void SelectFriendsScreen::FillSuggestedGroup () {
    Log::info_tag(LOG_TAG, "FillSuggestedGroup");
    mSuggestedFriendsGroup = new ListGroup("IDS_SUGGESTIONS", ListGroup::ESuggested, this);
    Elm_Object_Item *it = mGenList->AddItem(mSuggestedFriendsGroup);
    elm_genlist_item_select_mode_set(it, ELM_OBJECT_SELECT_MODE_NONE);

    int suggestedCount = 0;
    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(mAllFriendsGroup->GetList(), l, listData) {
        ListItem *li = static_cast<ListItem *> (listData);
        Friend *fr = li->mData->mFriendInfo->GetFriend();

        if (li->mData->mSuggestionOrder >= 0 && li->mData->mSuggestionOrder < KMaxSuggestedFriendsAtFirst) {
            Log::info_tag(LOG_TAG, "FillSuggestedGroup() ADDED SUGGESTED: %s :%d", fr->mName.c_str(), li->mData->mSuggestionOrder);
            SuggestedListItem *suggestedFriendsItem = new SuggestedListItem(this, li->mData);
            mSuggestedFriendsGroup->AddItem(suggestedFriendsItem);

        } else if (li->mData->mSuggestionOrder >= 0) {
            suggestedCount++;
        }
    }

    if (suggestedCount > KMaxSuggestedFriendsAtFirst) {
        mMoreGroup = new ListGroup("IDS_MORE", ListGroup::EMore, this);
        it = mGenList->AddItem(mMoreGroup);
        elm_genlist_item_select_mode_set(it, ELM_OBJECT_SELECT_MODE_NONE);
    }
    mSuggestedFriendsGroup->ShowItem();
}

void SelectFriendsScreen::ExtendSuggestedGroup() {
    mGenList->RemoveItem(mMoreGroup);
    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(mAllFriendsGroup->GetList(), l, listData) {
        ListItem *li = static_cast<ListItem *> (listData);
        Friend *fr = li->mData->mFriendInfo->GetFriend();

        Log::info_tag(LOG_TAG, "ExtendSuggestedGroup() SUGGESTED FRIEND NAME:%s", fr->mName.c_str());

        if (li->mData->mSuggestionOrder >= KMaxSuggestedFriendsAtFirst && li->mData->mSuggestionOrder < KMaxSuggestedFriends) {
            Log::info_tag(LOG_TAG, "ADDED SUGGESTED: %s :%d", fr->mName.c_str(), li->mData->mSuggestionOrder);
            SuggestedListItem *suggestedFriendsItem = new SuggestedListItem(this, li->mData);
            mSuggestedFriendsGroup->AddItem(suggestedFriendsItem);
        }
    }
    mGenList->ApplyFilter();
}

void SelectFriendsScreen::onMoreClicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    Log::info_tag(LOG_TAG, "onMoreClicked() MORE CLICKED: %s:%s", emission, source);
    ListGroup *group = static_cast<ListGroup*>(data);
    group->mScreen->ExtendSuggestedGroup();
}

void SelectFriendsScreen::SetFriendInfos(Eina_List *friendInfos) {
    mFriendInfos = eina_list_clone(friendInfos);
    //before update the list we need to allow screen to be drawn.
    if (!mUpdateTimer) {
        mUpdateTimer = ecore_timer_add(0.2f, set_friend_infos_async, this);
    }
}

Eina_Bool SelectFriendsScreen::set_friend_infos_async(void* data) {
    SelectFriendsScreen *me = static_cast<SelectFriendsScreen *> (data);
    me->UpdateList();
    me->mUpdateTimer = nullptr;
    return EINA_FALSE;
}

void SelectFriendsScreen::UpdateList() {
    //if data has been already set then we ignore this call. In the current implementation this function can be called only once.
    if (mIsDataSet) return;
    EnableRightButton(true);
    FillAllFriendsGroup(mFriendInfos);
    mGenList->ApplyFilter();

//Selected friends field
    if (AreThereSelectedFriends()) {
        CreateSelectedFriendsField();
        UpdateSelectedFriendsField();
    } else {
        mSearchPlaceholder = elm_layout_add(mLayout);
        elm_layout_file_set(mSearchPlaceholder, Application::mEdjPath, "post_composer_search_placeholder");
        elm_object_translatable_part_text_set(mSearchPlaceholder, "text", "IDS_SEARCH");
        elm_object_part_content_set(mLayout, "swallow.tagged_friends_field", mSearchPlaceholder);
        elm_object_signal_callback_add(mSearchPlaceholder, "mouse,clicked,*", "*", onSearchPlaceholderClicked, this);
    }
    if (!mSuggestedFriendsUploader) {
        mSuggestedFriendsUploader = new DataUploader(SuggestedFriendsProvider::GetInstance(), on_get_suggested_friends_completed, this);
        mSuggestedFriendsUploader->StartUploading();
    }
    mFriendInfos = eina_list_free(mFriendInfos);
    ProgressBarHide();
}

void SelectFriendsScreen::FillAllFriendsGroup(Eina_List *friendInfos) {
    Log::info_tag(LOG_TAG, "FillAllFriendsGroup");
    mAllFriendsGroup = new ListGroup("IDS_ALL_FRIENDS", ListGroup::EAllFriends, this);
    Elm_Object_Item *it = mGenList->AddItem(mAllFriendsGroup);
    elm_genlist_item_select_mode_set(it, ELM_OBJECT_SELECT_MODE_NONE);

//    if (friendInfos) {
//        Eina_List *friends = mClient->GetFriendInfos();
        Eina_List *l;
        void *listData;
        EINA_LIST_FOREACH(friendInfos, l, listData) {
            FriendInfo *friendInfo = static_cast<FriendInfo *> (listData);
            Friend *fr = friendInfo->GetFriend();

            Log::info_tag(LOG_TAG, "FillAllFriendsGroup() NAME:%s", fr->mName.c_str());
            ListItem *allFriendsItem = new ListItem(this, NULL);

            allFriendsItem->mData->mFriendInfo = friendInfo;
            mAllFriendsGroup->AddItem(allFriendsItem);
            UpdateSelectedList(allFriendsItem);

    //Check if the image is already downloaded. ToDo: probably move this functionality to
    //Application::GetInstance()->mDataService->DownloadImage() later.
            char *imagePath = Utils::GetImagePathFromUrl(fr->mPicturePath.c_str());
            if (imagePath) {
                FILE *fp = fopen(imagePath, "r");
                if(fp) {
                    fclose(fp);
                    allFriendsItem->mData->mImagePath = imagePath;
                } else {
                    free(imagePath);
                }
            }
            if (!allFriendsItem->mData->mImagePath) {
                Application::GetInstance()->mDataService->DownloadImage(allFriendsItem, fr->mPicturePath.c_str(), allFriendsItem->mRequestId);
            }
        }
//    }
}

const char *KGeneralSelectedFormat = "<linesize=42 linegap=4 valign=0.0 font=Sans:style=Regular font_size=24 wrap='word'>%s</>";

const char *KFormattedFriendAnchor = "<item absize=%dx%s vsize=full href=%s></item> ";

bool SelectFriendsScreen::AreThereSelectedFriends() {
    bool isChecked = false;
    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(mAllFriendsGroup->GetList(), l, listData) {
        ListItem * li = (ListItem *) listData;
        if (li->mData->mFriendInfo->mIsSelected) {
            isChecked = true;
            break;
        }
    }
    return isChecked;
}

int SelectFriendsScreen::SelectedFriendsCount() {
    return eina_list_count(mSelectedList);
}

const char* SelectFriendsScreen::GenSelectedFriendsField() {
    if (!AreThereSelectedFriends()) return NULL;
    const char *format = nullptr;

    Eina_List *l;
    void *listData;
//calc size
    int size = 0;
    EINA_LIST_FOREACH(mSelectedList, l, listData) {
        ListItem *li = static_cast<ListItem *> (listData);
        format = KFormattedFriendAnchor;
        size += strlen(format) + strlen(li->mData->mFriendInfo->GetFriend()->GetId()) + 2; //2: we may replace '%dx%s' with '999x999'. So, 2 additional bytes are required.
    }
    char *str = new char[++size]; // 1 more byte for NULL terminator.
    str[0] = '\0';

    EINA_LIST_FOREACH(mSelectedList, l, listData) {
        ListItem *li = static_cast<ListItem *> (listData);
        int sizeFr = strlen(format) + strlen(li->mData->mFriendInfo->GetFriend()->GetId()) + 3;
        char *frStr = new char[sizeFr];
        int w = CalculateSelectedTextItemWidth(li->mData->mFriendInfo->GetFriend()->mName.c_str(), "bg");
        Utils::Snprintf_s(frStr, sizeFr, format, w, R->SELECT_FRIENDS_SELECTED_FRIEND_ITEM_HEIGHT, li->mData->mFriendInfo->GetFriend()->GetId());
        eina_strlcat(str, frStr, size);
        delete [] frStr;
    }
    return str;
}

Evas_Object *SelectFriendsScreen::item_provider(void *data, Evas_Object *entry, const char *item) {
    SelectFriendsScreen *me = static_cast<SelectFriendsScreen *>(data);
    Evas_Object* layout = nullptr;
    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(me->mSelectedList, l, listData) {
        ListItem *li = (ListItem *) listData;
        if (!strcmp(li->mData->mFriendInfo->GetFriend()->GetId(), item)) {
            layout = me->CreateSelectedTextItem(li);
            break;
        }
    }
    return layout;
}

Evas_Object* SelectFriendsScreen::CreateSelectedTextItem(ListItem *li) {
    Evas_Object* layout = CreateSelectedTextItemLayout(li->mData->mFriendInfo->GetFriend()->mName.c_str());
    elm_object_signal_callback_add(layout, "mouse,clicked,*", "*", text_item_clicked, li);
    if (mHighlightedFriend == li) {
        elm_layout_signal_emit(layout, "highlight", "");
    }
    return layout;
}

Evas_Object* SelectFriendsScreen::CreateSelectedTextItemLayout(const char *text) {
    Evas_Object* layout = elm_layout_add(mSelectedFriendsField);
    elm_layout_file_set(layout, Application::mEdjPath, "tagged_friends_tagged_friend_item");
    elm_object_part_text_set(layout, "text", text);
    return layout;
}

int SelectFriendsScreen::CalculateSelectedTextItemWidth(const char *text, const char *part) {
    Evas_Coord w;
    Evas_Object *layout = CreateSelectedTextItemLayout(text);
    edje_object_part_geometry_get(elm_layout_edje_get(layout), part, nullptr, nullptr, &w, nullptr);
    evas_object_del(layout);
    return w;
}

SelectFriendsScreen::SelectFriendsScreen(ISelectFriendsScreenClient *client, const char*caption) : ScreenBase(NULL),
        mSelectedFriendsGroup(nullptr),
        mSelectedFriendsField(nullptr),
        mSearchPlaceholder(nullptr),
        mClient(client),
        mGenList(nullptr),
        mSuggestedFriendsGroup(nullptr),
        mAllFriendsGroup(nullptr),
        mMoreGroup(nullptr),
        mSelectedList(nullptr),
        mMinCurPos(0),
        mTagsText(nullptr),
        mFilterText(nullptr),
        mAsyncJob(nullptr),
        mUpdateTimer(nullptr),
        mSuggestedFriendsUploader(nullptr),
        mHighlightedFriend(nullptr),
        mIsDataSet(false),
        mFriendInfos(nullptr) {
    mScreenId = ScreenBase::SID_SELECT_FRIENDS;
    mLayout = elm_layout_add(getParentMainLayout());
    elm_layout_file_set(mLayout, Application::mEdjPath, "tag_friends_screen");

//header
    //AAA ToDo: remove PostComposer from here
    Evas_Object *headerText = PostComposerScreen::CreateCaptionText(mLayout, caption);
    Evas_Object *header = WidgetFactory::CreateGrayHeaderWithLogo(mLayout, headerText, "IDS_DONE", this);
    elm_object_part_content_set(mLayout, "swallow.top_toolbar", header);

//swallow.friends_list
    mGenList = new GenList(mLayout);
    elm_object_focus_allow_set(mGenList->GetGenList(), EINA_FALSE);

    elm_object_part_content_set(mLayout, "swallow.list", mGenList->GetGenList());
    elm_scroller_policy_set(mGenList->GetGenList(), ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_AUTO);
    ProgressBarShow();
    EnableRightButton(false);
}

void SelectFriendsScreen::CreateSelectedFriendsField() {
    if (mSearchPlaceholder != NULL) {
        elm_object_part_content_unset(mLayout, "swallow.tagged_friends_field");
        evas_object_hide(mSearchPlaceholder);
        evas_object_del(mSearchPlaceholder);
        mSearchPlaceholder = NULL;
    }

    mSelectedFriendsGroup = elm_layout_add(mLayout);
    elm_layout_file_set(mSelectedFriendsGroup, Application::mEdjPath, "post_composer_search_field");
    elm_layout_content_set(mLayout, "swallow.tagged_friends_field", mSelectedFriendsGroup);

    mSelectedFriendsField = elm_entry_add(mSelectedFriendsGroup);
    evas_object_size_hint_align_set(mSelectedFriendsField, 0.0, 0.5);
    elm_layout_content_set(mSelectedFriendsGroup, "swallow.text", mSelectedFriendsField);
    elm_entry_single_line_set(mSelectedFriendsField, EINA_FALSE);
    elm_entry_prediction_allow_set(mSelectedFriendsField, EINA_FALSE);
    elm_entry_input_panel_return_key_disabled_set(mSelectedFriendsField, EINA_TRUE);
    elm_entry_cnp_mode_set(mSelectedFriendsField, ELM_CNP_MODE_PLAINTEXT);

    elm_entry_text_style_user_push(mSelectedFriendsField, R->TAG_FRIENDS_SEARCH_TEXT_STYLE);

    elm_entry_editable_set(mSelectedFriendsField, EINA_TRUE);
    evas_object_show(mSelectedFriendsField);
    elm_object_signal_callback_add(mSelectedFriendsGroup, "mouse,clicked,*", "icon.clear", onClearClicked, this);

    elm_entry_item_provider_append(mSelectedFriendsField, item_provider, this);
    evas_object_smart_callback_add(mSelectedFriendsField, "cursor,changed,manual", CursorChangedCb, this);
    //    evas_object_smart_callback_add(me->mSearch, "changed,user", filter_changed_cb, me);
//AAA ToDo: add support for preedit later   evas_object_smart_callback_add(mSelectedFriendsField, "preedit,changed", FilterChangedCb, this);
    evas_object_smart_callback_add(mSelectedFriendsField, "changed", FilterChangedCb, this);
    evas_object_event_callback_add(mSelectedFriendsField, EVAS_CALLBACK_KEY_DOWN, key_down_cb, this);
    evas_object_smart_callback_add(mSelectedFriendsField, "anchor,clicked", anchor_clicked_cb, this);

// TODO: Don't use the package name explicitly.

//    elm_object_part_text_set(mSelectedFriendsField, "elm.guide", "<font=Sans:style=Regular font_size=40>Guide Text</font>");

//    elm_object_text_set(mSelectedFriendsField, "xcvd xcvxcv cvcv xcv xvx cvxc c vx vxvbbc: <item absize=36x32 vsize=full href=file:///opt/usr/apps/com.facebook.tizen/res/images/photo_icon.png></item>:end, <item relsize=16x16 vsize=full href=emoticon/angry></item> dfsd sdfsdf sdf sdfs fsdf sdf sdf dfs dfsd fdf f sf sdf f sf sf s sf s fsdf sd f");
//    elm_label_ellipsis_set(mSelectedFriendsField, EINA_TRUE);
//    evas_object_size_hint_max_set(mSelectedFriendsField, 200, 64);
//    elm_entry_entry_set(mSelectedFriendsField, "<font_size=30 color=#F00 ellipsis=0.0>Big, e we er erwe er er r er r wer   r we we wr wr wer wr we rwe w w r we wr rw erbbc: <item absize=36x32 vsize=full href=file:///opt/usr/apps/com.facebook.tizen/res/images/privacy_friends_small.png></item>:end, <item relsize=16x16 vsize=full href=emoticon/angry></item></>");

//    elm_entry_entry_set(mSelectedFriendsField, "<linesize=40 linegap=4 valign=0.0 font_size=24><item absize=24x24 vsize=full href=file:///opt/usr/apps/com.facebook.tizen/res/images/privacy_friends_small.png></item> end, er gdfg fg fg <color=#F00 backing='on' backing_color=#9fb2d9><a href=anc-01>  but this one is an anchor</a> dfg</> d gdfg fg df gfg dfg fgd<item relsize=480x32 vsize=full href=MyButton></item>gdf gdfg g  gf gf gg g f gfg d fg df g f g g  gg g  g gd sd fg df sdfg df gf gd f fg g g g d g fg dfgsdfgg fg dff gfg  dfgdf df g df g dfgfg g  fgwer wer we <item relsize=16x16 vsize=full href=emoticon/angry></item></>");


//    Evas_Object* tb =  elm_entry_textblock_get(mSelectedFriendsField);
//    evas_textblock_style_set(tb, "font=Sans:style=Regular ellipsis=1.0 font_size=60");
//    evas_object_textblock_text_markup_set (tb, "<font_size=30 color=#F00 ellipsis=1.0>: <item absize=36x32 vsize=full href=file:///opt/usr/apps/com.facebook.tizen/res/images/photo_icon.png></item>:end, <item relsize=16x16 vsize=full href=emoticon/angry></item></>");
//    evas_object_textblock_text_markup_set (tb, "<font_size=25 color=#F00 backing='on' backing_color=#0F0 ellipsis=1.0>end,asdr er wer er wer we wr wr wer wr wer wr wr we rwe rw erw er wer wer we <item relsize=16x16 vsize=full href=emoticon/angry></item></>");
//    "font=Sans font_size=21 color=#000 wrap=word"

//Tagged - end
}

void SelectFriendsScreen::UpdateClearButton() {
    if (elm_entry_is_empty(mSelectedFriendsField)) {
        edje_object_signal_emit(elm_layout_edje_get(mSelectedFriendsGroup), "hide_clear_button", "");
    } else {
        edje_object_signal_emit(elm_layout_edje_get(mSelectedFriendsGroup), "show_clear_button", "");
    }
}

void SelectFriendsScreen::onSearchPlaceholderClicked (void *data, Evas_Object *obj, const char *emission, const char *source) {
    SelectFriendsScreen *me = static_cast<SelectFriendsScreen *>(data);
    me->CreateSelectedFriendsField();
    elm_object_focus_set(me->mSelectedFriendsField, EINA_TRUE);
}

void SelectFriendsScreen::FilterChangedCb(void *data, Evas_Object *obj, void *event_info) {
    SelectFriendsScreen *me = static_cast<SelectFriendsScreen *>(data);
    int curPos = 0;
    if(me->mSelectedFriendsField) curPos = elm_entry_cursor_pos_get(me->mSelectedFriendsField);
    Log::info_tag(LOG_TAG, "FilterChangedCb:%d", curPos);
    if (curPos < me->mMinCurPos) {
        me->UpdateSelectedFriendsField();
    //    me->mFilterText =
    } else {
        const char *text = elm_object_part_text_get(me->mSelectedFriendsField, NULL);
        me->mFilterText = text + (me->mTagsText ? strlen(me->mTagsText) : 0);
        me->mFilterText = strlen(me->mFilterText) > 0 ? me->mFilterText : NULL;
        Log::info_tag(LOG_TAG, "mFilterText:%s", me->mFilterText);
//AAA ToDo: add support for preedit later        char* ttt = elm_entry_markup_to_utf8 (me->mFilterText);
//AAA ToDo: add support for preedit later        LOGI("mFilterText 1 :%s", ttt);
        me->UpdateClearButton();
        me->mGenList->ApplyFilter();
    }
}

void SelectFriendsScreen::CursorChangedCb(void *data, Evas_Object *obj, void *event_info) {
   SelectFriendsScreen *me = static_cast<SelectFriendsScreen *>(data);
   int curPos = elm_entry_cursor_pos_get(me->mSelectedFriendsField);
   Log::info_tag(LOG_TAG, "CursorChangedCb:%d", curPos);
   if (curPos < me->mMinCurPos) {
       elm_entry_cursor_pos_set(me->mSelectedFriendsField, me->mMinCurPos);
   }
}

void SelectFriendsScreen::update_field(void *data) {
    SelectFriendsScreen *me = static_cast<SelectFriendsScreen *>(data);
    me->mAsyncJob = NULL;
    me->UpdateSelectedFriendsField();
}

void SelectFriendsScreen::UpdateSelectedFriendsField() {
    evas_object_smart_callback_del(mSelectedFriendsField, "changed", FilterChangedCb);

    delete []mTagsText;
    mTagsText = GenSelectedFriendsField();

    if (mTagsText) {
        elm_object_text_set(mSelectedFriendsField, mTagsText);
    } else {
        elm_object_text_set(mSelectedFriendsField, "");
    }

    UpdateClearButton();

    if (GetSelectedFriendsFieldLinesCount() > 4) {
        elm_entry_scrollable_set(mSelectedFriendsField, EINA_TRUE);
        elm_scroller_policy_set(mSelectedFriendsField, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_OFF);
        edje_object_signal_emit(elm_layout_edje_get(mLayout), "lines_max", ""); // group
        edje_object_signal_emit(elm_layout_edje_get(mSelectedFriendsGroup), "lines_max", ""); // entry
    } else {
        elm_entry_scrollable_set(mSelectedFriendsField, EINA_FALSE);
        edje_object_signal_emit(elm_layout_edje_get(mLayout), "lines_normal", ""); // group
        edje_object_signal_emit(elm_layout_edje_get(mSelectedFriendsGroup), "lines_normal", ""); // entry
    }

    elm_entry_cursor_end_set(mSelectedFriendsField);

    mMinCurPos = elm_entry_cursor_pos_get(mSelectedFriendsField);
    Log::info_tag(LOG_TAG, "Entry focused:%d", mMinCurPos);
    evas_object_smart_callback_add(mSelectedFriendsField, "changed", FilterChangedCb, this);
}

int SelectFriendsScreen::GetSelectedFriendsFieldLinesCount() {
    int linesCount = 0;
    Evas_Object* tb = elm_entry_textblock_get(mSelectedFriendsField);

    if (tb) {
        linesCount++; // tb is present, so there is at least one line
        int namesSeparatorWidth = CalculateSelectedTextItemWidth(" ", "text");
        int currentLineWidth = 0, tbWidth, lineIndex = 0;
        int entryFieldWidth;
        edje_object_part_geometry_get(elm_layout_edje_get(mSelectedFriendsGroup), "swallow.text", nullptr, nullptr, &entryFieldWidth, nullptr);
        while (evas_object_textblock_line_number_geometry_get(tb, lineIndex++, nullptr, nullptr, &tbWidth, nullptr)) {

            if (tbWidth > 0) {
                // Extremely long names appear as single line with carriage character on the next line
                if (tbWidth > entryFieldWidth) {
                    linesCount++;
                    currentLineWidth = 0;
                    continue;
                }

                // There is a whitespace between names and it's ignored when tbWidth is calculated
                currentLineWidth += (tbWidth + namesSeparatorWidth);
            }

            // Current line exceeds field width, so the last word will be moved to the next line
            if (currentLineWidth > entryFieldWidth) {
                linesCount++;
                currentLineWidth = tbWidth + namesSeparatorWidth;
            }
        }
    }
    return linesCount;
}

void SelectFriendsScreen::UpdateAllItemsById(const char* id, ListItem::UpdateType uType) {
    Eina_List *l;
    void *listData;
    if (mAllFriendsGroup) {
        EINA_LIST_FOREACH(mAllFriendsGroup->GetList(), l, listData) {
            ListItem * li = (ListItem *) listData;
            if (!strcmp(li->mData->mFriendInfo->GetFriend()->GetId(), id)) {
                if (uType == ListItem::ECheck && mHighlightedFriend == li && !li->mData->mFriendInfo->mIsSelected) {
                    mHighlightedFriend = NULL;
                }
                li->UpdateAllItems(uType);
                UpdateSelectedList(li);
                break;
            }
        }
    }
    if (mSuggestedFriendsGroup) {
        EINA_LIST_FOREACH(mSuggestedFriendsGroup->GetList(), l, listData) {
            ListItem * li = (ListItem *) listData;
            if (!strcmp(li->mData->mFriendInfo->GetFriend()->GetId(), id)) {
                li->UpdateAllItems(uType);
                break;
            }
        }
    }
}

char* SelectFriendsScreen::GenReplacedStr(const char*str, const char*orig, const char*rep) {
    const char *p = NULL;
    char *buf = NULL;
    p = strstr(str, orig);
    if (p) {
        int size = strlen(str) + strlen(rep) - strlen(orig) + 1;
        buf = new char[size];
        eina_strlcpy(buf, str, p - str + 1);
        eina_strlcat(buf, rep, size);
        eina_strlcat(buf, p + strlen(orig), size);
    }
    return buf;
}

void SelectFriendsScreen::Push() {
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(elm_naviframe_top_item_get(getNf()), EINA_FALSE, EINA_FALSE);
}

SelectFriendsScreen::~SelectFriendsScreen() {
    delete mSuggestedFriendsUploader;
    delete mGenList;
    delete []mTagsText;
    mSelectedList = eina_list_free(mSelectedList);

    if (mAsyncJob) {
        ecore_job_del(mAsyncJob);
    }
    if (mUpdateTimer) {
        ecore_timer_del(mUpdateTimer);
    }
    mFriendInfos = eina_list_free(mFriendInfos);
}

void SelectFriendsScreen::ConfirmCallback (void *data, Evas_Object *obj, const char *emission, const char *source) {
    Log::info_tag(LOG_TAG, "ConfirmCallback:%s:%s", emission, source);
    if (mClient) { // if client is still alive then notify it.
        Eina_List *l;
        void *listData;
        Eina_List *friends = NULL;
        EINA_LIST_FOREACH(mAllFriendsGroup->GetList(), l, listData) {
            ListItem * li = (ListItem *) listData;
            if (li->mData->mFriendInfo->mIsSelected) {
                friends = eina_list_append(friends, li->mData->mFriendInfo->mClientFriend);
            }
        }
        mClient->FriendsSelectionCompleted(friends, false);
        friends = eina_list_free(friends);
    }
    Pop();
}

bool SelectFriendsScreen::HandleBackButton() {
    mClient->FriendsSelectionCompleted(NULL, true);
    return ScreenBase::HandleBackButton();
}

void SelectFriendsScreen::CancelCallback(void *data, Evas_Object *obj, void *event_info) {
    mClient->FriendsSelectionCompleted(NULL, true);
    ScreenBase::CancelCallback(data, obj, event_info);
}

SelectFriendsScreen::ListItem *SelectFriendsScreen::GetLastSelectedItem() {
    ListItem *ret = nullptr;
    if (mSelectedList) {
        ret = static_cast<ListItem *> (eina_list_data_get(eina_list_last(mSelectedList)));
    }
    return ret;
}

void SelectFriendsScreen::BackspacePressed() {
    ListItem *itemToDelete = NULL;
    if (mHighlightedFriend) {  //remove highlighted item if any
        itemToDelete = mHighlightedFriend;
    } else if (!mFilterText) { //otherwise remove the last selected item
        itemToDelete = GetLastSelectedItem();
    }

    if (itemToDelete) {
        ItemSelected(itemToDelete);
    }
}

void SelectFriendsScreen::UpdateSelectedList(ListItem *item) {
    if (item->mData->mFriendInfo->mIsSelected) {
        mSelectedList = eina_list_append(mSelectedList, item);
    } else {
        mSelectedList = eina_list_remove(mSelectedList, item);
    }
}

void SelectFriendsScreen::ItemSelected(ListItem *item) {
    item->mData->mFriendInfo->mIsSelected = !item->mData->mFriendInfo->mIsSelected;
    UpdateAllItemsById(item->mData->mFriendInfo->GetFriend()->GetId(), SelectFriendsScreen::ListItem::ECheck);
    if (mSearchPlaceholder) {
        CreateSelectedFriendsField();
    }
    if (mAsyncJob) {
        ecore_job_del(mAsyncJob);
        mAsyncJob = NULL;
    }
    mAsyncJob = ecore_job_add(update_field, this);
    //AAA now we do this async  item->mScreen->UpdateSelectedFriendsField();
}

void SelectFriendsScreen::key_down_cb(void *data, Evas *e, Evas_Object *obj, void *event_info) {
    SelectFriendsScreen *me = static_cast<SelectFriendsScreen *>(data);
    Evas_Event_Key_Up *ev = static_cast<Evas_Event_Key_Up *> (event_info);
    if (!strcmp(ev->keyname, "BackSpace")) {
        me->BackspacePressed();
    }
}

void SelectFriendsScreen::text_item_clicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    ListItem *li = static_cast<ListItem *>(data);
    assert(li);
    SelectFriendsScreen *me = li->mScreen;
    assert(me);

    if (!me->mHighlightedFriend) { //If new item is selected
        me->mHighlightedFriend = li;
    } else { //this means that previously selected item is unselected.
        me->mHighlightedFriend = nullptr;
    }
    me->UpdateSelectedFriendsField();
}
void SelectFriendsScreen::anchor_clicked_cb(void *data, Evas_Object *obj, void *event) {
    SelectFriendsScreen *me = static_cast<SelectFriendsScreen *>(data);
    Elm_Entry_Anchor_Info *ev = static_cast<Elm_Entry_Anchor_Info *> (event);

//If new item is selected
    if (!me->mHighlightedFriend || strcmp(me->mHighlightedFriend->mData->mFriendInfo->GetFriend()->GetId(), ev->name)) {
        Eina_List *l;
        void *listData;
        EINA_LIST_FOREACH(me->mSelectedList, l, listData) {
            ListItem * li = static_cast<ListItem *> (listData);
            if (!strcmp(li->mData->mFriendInfo->GetFriend()->GetId(), ev->name)) {
                me->mHighlightedFriend = li;
                break;
            }
        }
    } else { //this means that previously selected item is unselected.
        me->mHighlightedFriend = NULL;
    }
    me->UpdateSelectedFriendsField();
}


/*
 * Class SelectFriendsScreen::ListGroup
 */
Evas_Object* SelectFriendsScreen::ListGroup::DoCreateLayout(Evas_Object* parent) {
    Evas_Object *layout = elm_layout_add(parent);
    if (mType == EMore) {
        elm_layout_file_set(layout, Application::mEdjPath, "tag_friends_group_more");
        elm_object_signal_callback_add(layout, "mouse,clicked,*", "group_caption_bg", onMoreClicked, this);
    } else {
        elm_layout_file_set(layout, Application::mEdjPath, "tag_friends_group_caption");
    }

//            elm_object_translatable_part_text_set(item, "text", "IDS_NO_COMMENTS");

/*    int swallowW, swallowH;
    edje_object_part_geometry_get(elm_layout_edje_get(layout), "group_caption_bg", NULL, NULL, &swallowW, &swallowH);
    evas_object_size_hint_min_set(layout, swallowW, swallowH);
    LOGE("SIZE: %dx%d", swallowW, swallowH);*/
    Evas_Coord maxw, maxh;
    Evas_Object* edjeLayout = elm_layout_edje_get(layout);
    edje_object_size_min_calc ((Evas_Object *)edjeLayout, &maxw, &maxh);
    evas_object_size_hint_min_set(layout, maxw, maxh);

    elm_object_translatable_part_text_set(layout, "text", mGroupName);
    return layout;
}

bool SelectFriendsScreen::ListGroup::Filter(Elm_Object_Item *insertAfter) {
    if (mType == EMore) {
        return mScreen->mSuggestedFriendsGroup->IsGroupFiltered();
    } else {
        return GenListGroupItemBase::Filter(insertAfter);
    }
}

int SelectFriendsScreen::ListGroup::Compare(GenListItemBase* otherItem) {
    ListGroup *other = (ListGroup *)otherItem;
    return (mType > other->mType ? 1 : (mType == other->mType ? 0 : -1));
}


/*
 * Class SelectFriendsScreen::ListItem
 */
SelectFriendsScreen::ListItem::ListItem(SelectFriendsScreen *screen, ListItemSharedData *data) : mData(data), mIsDataOwner(false), mRequestId(0), mScreen(screen) {
    if (!mData) {
        mData = new ListItemSharedData();
        mIsDataOwner = true;
    }
}

SelectFriendsScreen::ListItem::~ListItem() {
    if (mIsDataOwner){
        delete mData;
        Cancel(mRequestId);
    }
}

Evas_Object* SelectFriendsScreen::ListItem::ItemCreateImage(Evas_Object *parent) {
    Evas_Object *img = elm_image_add(parent);
    if (mData->mImagePath) {
        elm_image_file_set(img, mData->mImagePath, NULL);
    } else {
        //AAA replace with placeholder
    }
    return img;
}

Evas_Object* SelectFriendsScreen::ListItem::ItemContentGet(Evas_Object *parent) {
    Evas_Object *layout = elm_layout_add(parent);
    elm_layout_file_set(layout, Application::mEdjPath, "tag_friends_item_check_image_text");

//check
    Evas_Object* check = elm_check_add(layout);
    elm_check_state_set(check, mData->mFriendInfo->mIsSelected);
    elm_object_part_content_set(layout, "swallow.check", check);

//avatar
    if (mData->mImagePath) {
        elm_object_part_content_set(layout, "swallow.icon", ItemCreateImage(layout));
    }

//text
    elm_object_part_text_set(layout, "text", mData->mFriendInfo->GetFriend()->mName.c_str());

//min size
    Evas_Coord maxw, maxh;
    Evas_Object* edjeLayout = elm_layout_edje_get(layout);
    edje_object_size_min_calc((Evas_Object *)edjeLayout, &maxw, &maxh);
    evas_object_size_hint_min_set(layout, maxw, maxh);

//callback on click
    elm_object_signal_callback_add(layout, "mouse,clicked", "item", onItemClicked, this);

    return layout;
}

void SelectFriendsScreen::ListItem::UpdateAllItems(UpdateType uType) {
    switch (uType) {
    case ECheck: //elm_object_item_part_content_get() doesn't work
//            {
//            Evas_Object *check = elm_object_item_part_content_get (it, "swallow.check");
//            LOGE("CHECK%s", check);
//            elm_check_state_set(check, mFriendInfo->mIsSelected);
//            }
//            break;
    case EImage:
//            elm_object_item_part_content_set(it, "swallow.icon", ItemCreateImage(mLayout, this)); because mLayout may not exist
    default:
        elm_genlist_item_update(mItem);
        break;
    }
}

void SelectFriendsScreen::ListItem::HandleDownloadImageResponse(ErrorCode error, MessageId requestID, const char* url, const char* fileName) {
    if (EBPErrNone == error) {
        if (fileName) {
            free ((char*) mData->mImagePath);
            mData->mImagePath = strdup(fileName);
            mScreen->UpdateAllItemsById(mData->mFriendInfo->GetFriend()->GetId(), EImage);
        }
    }
}

Evas_Object* SelectFriendsScreen::ListItem::DoCreateLayout(Evas_Object* parent) {
    return ItemContentGet(parent);
}

int SelectFriendsScreen::ListItem::Compare(GenListItemBase* otherItem) {
    ListItem *other = (ListItem *)otherItem;
    return(strcmp(mData->mFriendInfo->GetFriend()->mName.c_str(), other->mData->mFriendInfo->GetFriend()->mName.c_str()));
}

bool SelectFriendsScreen::ListItem::Filter(Elm_Object_Item *insertAfter) {
    bool isFiltered = false;
    if (mScreen->GetFilterText()) {
        char* filter = Utils::Trim(mScreen->GetFilterText());
        if (filter && strlen(filter) > 0) {
            isFiltered = mScreen->GetFilterText() && !Utils::UAreAllWordsSubstringsOfAnyWord(mData->mFriendInfo->GetFriend()->mName.c_str(), filter, false);
            free (filter);
        }
    }
    return isFiltered;
}

/*
 * Class SelectFriendsScreen::SuggestedListItem
 */
int SelectFriendsScreen::SuggestedListItem::Compare(GenListItemBase* otherItem) {
    int otherOrder = ((SuggestedListItem*) otherItem)->mData->mSuggestionOrder;
    if (mData->mSuggestionOrder > otherOrder) {
        return 1;
    } else if (mData->mSuggestionOrder < otherOrder) {
        return -1;
    } else {
        return 0;
    }
}


/*
 * Class SelectFriendsScreen::ListItem::ListItemSharedData
 */
SelectFriendsScreen::ListItem::ListItemSharedData::~ListItemSharedData() {
    free((char *)mImagePath);
    delete mFriendInfo;
}


/*
 * Class SelectFriendsScreen::FriendInfo
 */
SelectFriendsScreen::FriendInfo::FriendInfo() : mClientFriend(NULL), mIsSelected(false), mFriend(NULL) {
}

SelectFriendsScreen::FriendInfo::~FriendInfo() {
    if (mFriend) {
         mFriend->Release();
    }
}

void SelectFriendsScreen::FriendInfo::SetFriend(Friend *fr) {
    if (mFriend && fr && mFriend != fr) {
        mFriend->Release();
        mFriend = NULL;
    }
    if (!mFriend && fr) {
        mFriend = fr;
        mFriend->AddRef();
    }
}

