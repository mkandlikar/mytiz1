#include "CheckInScreen.h"
#include "Common.h"
#include "Log.h"
#include "NearByPlacesScreen.h"
#include "NearByPlacesComposer.h"
#include "PCPlace.h"
#include "SearchScreen.h"
#include "WebViewScreen.h"

NearByPlacesScreen::NearByPlacesScreen(): ScreenBase(NULL)
{
    Evas_Object *parent = getParentMainLayout();

    mMapObj = NULL;
    places_ovl_class = NULL;
    my_loc_ovl_class = NULL;
    mPlace = NULL;

    mLayout = CreatePlacesPage(parent);
}

NearByPlacesScreen::~NearByPlacesScreen()
{
    if (places_ovl_class) {
        elm_map_overlay_del(places_ovl_class);
    }
    if (my_loc_ovl_class) {
        elm_map_overlay_del(my_loc_ovl_class);
    }
    delete mPlace;
}

void NearByPlacesScreen::Push() {
    ScreenBase::Push();
    // hide standard title of naviframe
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

Evas_Object *NearByPlacesScreen :: CreatePlacesPage(Evas_Object *parent)
{
    playout = elm_layout_add(parent);
    elm_layout_file_set(playout, Application::mEdjPath, "near_by_places_page");
    evas_object_size_hint_weight_set(playout, 1, 1);
    evas_object_size_hint_align_set(playout, -1, -1);
    evas_object_show(playout);

    elm_object_translatable_part_text_set(playout, "title", "IDS_SETTING_NEAR_BY_PLACES");
    elm_object_signal_callback_add(playout, "back,clicked", "back_btn", back_btn_cb, this);
    elm_object_signal_callback_add(playout, "bring_in,clicked", "bring_in", bringIn_btn_cb, this);
    elm_object_signal_callback_add(playout, "titlebar.search.clicked", "titlebar_search_icon", on_search_button_clicked, this);

    //Nearby places search button
    Evas_Object *searchLayout = elm_box_add(playout);
    elm_box_horizontal_set(searchLayout, EINA_TRUE);
    elm_object_translatable_part_text_set(playout, "search_nearby_title", "IDS_SETTING_NEAR_BY_PLACES");
    elm_object_part_content_set(playout, "search_nearby_places", searchLayout);
    elm_object_signal_callback_add(playout, "mouse,clicked,*", "search_nearby_*", on_nearby_button_clicked, this);
    evas_object_show(searchLayout);

    create_map_area(playout);

    mNestedScreenContainer = elm_box_add(playout);
    elm_box_horizontal_set(mNestedScreenContainer, EINA_TRUE);
    elm_object_part_content_set(playout, "content", mNestedScreenContainer);
    evas_object_show(mNestedScreenContainer);

    NearByPlacesComposer* places_more = new NearByPlacesComposer(this , mNestedScreenContainer);
    MinSet((static_cast<ScreenBase*>(places_more))->getMainLayout(), mNestedScreenContainer, 0, 0);

    elm_object_translatable_part_text_set(playout, "msg_text", "IDS_COULD_NOT_FIND_YOUR_LOCATION");

    return playout;

}

void NearByPlacesScreen :: create_map_area(Evas_Object *parent)
{
    mMapObj = elm_map_add(parent);
    elm_map_zoom_mode_set(mMapObj, ELM_MAP_ZOOM_MODE_MANUAL);
//    elm_map_rotate_set(mMapObj, 90, 77.7245, 12.983);

    evas_object_size_hint_weight_set(mMapObj, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mMapObj, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_object_part_content_set(parent, "map_content_rect", mMapObj);
}

void NearByPlacesScreen :: setMapPosition(double latitude , double longitude)
{
    Eina_List *item;
    void *list_data;

    Eina_List *list = elm_map_overlays_get(mMapObj);
    if(list){
        EINA_LIST_FOREACH(list, item, list_data) {
            if(list_data){
                elm_map_overlay_del(static_cast<Elm_Map_Overlay*>(list_data));
                dlog_print(DLOG_DEBUG, "123", "Overlay deleted");
            }
        }
    }

    mLat = latitude;
    mLong = longitude;

    elm_map_zoom_set(mMapObj, 17);
    elm_map_region_bring_in(mMapObj, mLong, mLat);
    places_ovl_class = elm_map_overlay_class_add(mMapObj);
    my_loc_ovl_class = elm_map_overlay_class_add(mMapObj);
    add_icon(my_loc_ovl_class, ICON_DIR"/my_location.png", mLat, mLong, NULL, 64);
}

void NearByPlacesScreen :: setPlacesMarker(const char *filename , double latitude, double longitude , const char *link)
{
    add_icon(places_ovl_class, filename , latitude, longitude , link, 64);
}

void NearByPlacesScreen :: add_icon(Elm_Map_Overlay *parent, const char *filename , double latitude, double longitude, const char *link, int size)
{
    Evas_Object *icon;
    Elm_Map_Overlay *ovl;

    // Add an overlay
    ovl = elm_map_overlay_add(mMapObj, longitude, latitude);
    icon = elm_icon_add(mMapObj);
    elm_image_file_set(icon, filename, NULL);
    evas_object_size_hint_max_set(icon, size, size);
    evas_object_size_hint_min_set(icon, size, size);
    elm_map_overlay_icon_set(ovl, icon);

    // Add the new ovl object to the parent class
    elm_map_overlay_class_append(parent, ovl);
    evas_object_smart_callback_add(icon, "clicked", on_places_item_click, (void*)link);
}

void NearByPlacesScreen :: back_btn_cb(void *data , Evas_Object *obj, const char *emission, const char *source)
{
    NearByPlacesScreen *self = static_cast<NearByPlacesScreen*>(data);
    self->Pop();
}

void NearByPlacesScreen :: bringIn_btn_cb(void *data , Evas_Object *obj, const char *emission, const char *source)
{
    NearByPlacesScreen *self = static_cast<NearByPlacesScreen*>(data);
    if(self->mMapObj){
        elm_map_zoom_set(self->mMapObj, 17);
        elm_map_region_bring_in(self->mMapObj, self->mLong, self->mLat);
    }
}

void NearByPlacesScreen::on_nearby_button_clicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    Log::debug("NearByPlacesScreen::on_nearby_button_clicked");
    NearByPlacesScreen *screen = static_cast<NearByPlacesScreen *>(data);
    if (screen)
    {
        screen->LaunchCheckInScreen();
    }
}

void NearByPlacesScreen::on_search_button_clicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    Log::debug("NearByPlacesScreen::on_search_button_clicked");
    if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_SEARCH) {
        SearchScreen* searchScreen = new SearchScreen();
        Application::GetInstance()->AddScreen(searchScreen);
    }
}

void NearByPlacesScreen::LaunchCheckInScreen() {
    if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_CHECKIN) {
        CheckInScreen::OpenScreen(this, CheckInScreen::EFromNearByPlacesScreen, mPlace);
    }
}

void NearByPlacesScreen::PlaceSelected(Place* place) {
    Log::debug("NearByPlacesScreen::PlaceSelectedClick");
    delete mPlace;
    if (place) {
        mPlace = new PCPlace(*place);
        WebViewScreen::Launch((std::string("m.facebook.com/") + mPlace->mId).c_str(), true);
    } else {
        mPlace = NULL;
    }
}

void NearByPlacesScreen::on_places_item_click(void *data, Evas_Object *obj, void *event_info)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "on_places_item_click::");
    if (data == NULL) {
        return;
    }
    const char *url = static_cast<char*>(data);
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "on_places_item_click:: URL = %s", url);
    WebViewScreen::Launch(url, true);
}

void NearByPlacesScreen :: ShowHideMsg(bool show)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "NearByPlacesScreen :: SHOW MSG CONTENT = %d", show);
    if(show){
        elm_object_signal_emit(playout, "hide_place_content", "near_by_places_page");
        elm_object_signal_emit(playout, "show_msg_content", "near_by_places_page");
    }
    else{
        elm_object_signal_emit(playout, "hide_msg_content", "near_by_places_page");
        elm_object_signal_emit(playout, "show_place_content", "near_by_places_page");
    }
}

