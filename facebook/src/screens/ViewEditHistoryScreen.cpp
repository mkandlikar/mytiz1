#include "EditAction.h"
#include "EditHistoryProvider.h"
#include "Log.h"
#include "ViewEditHistoryScreen.h"
#include "Utils.h"
#include "WidgetFactory.h"

ViewEditHistoryScreen::ViewEditHistoryScreen(const char *id) : ScreenBase(NULL), TrackItemsProxyAdapter(NULL, EditHistoryProvider::GetInstance())
{
    mAction = new FeedAction(this);

    mId = SAFE_STRDUP(id);
    EditHistoryProvider::GetInstance()->SetId(mId);
    mScreenId = SID_VIEW_EDIT_HISTORY_SCREEN;
    mLayout = WidgetFactory::CreateBaseScreenLayout(this, getParentMainLayout(), i18n_get_text("IDS_EDIT_HISTORY"), true, true, false);

    ApplyScroller(mLayout, true);
    elm_object_part_content_set(mLayout, "content", GetScroller());

    Log::info("ViewEditHistoryScreen = id = (%s)", mId);

    RequestData();
}

ViewEditHistoryScreen::~ViewEditHistoryScreen()
{
    free((void *) mId); mId = NULL;
    delete mAction;
    EraseWindowData();
    EditHistoryProvider::GetInstance()->EraseData();
}

void ViewEditHistoryScreen::Push()
{
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void* ViewEditHistoryScreen::CreateItem( void* dataForItem, bool isAddToEnd )
{
    EditAction *editAction = static_cast<EditAction*>(dataForItem);
    ActionAndData *data = NULL;

    if (editAction) {
        data = new ActionAndData(editAction, mAction);

        Evas_Object *item = CreatePostHistoryItem(GetDataArea(), data, isAddToEnd);
        data->mParentWidget = item;
    }

    return data;
}

Evas_Object *ViewEditHistoryScreen::CreatePostHistoryItem(Evas_Object *parent, ActionAndData *actionData, bool isAddToEnd)
{
    Evas_Object * wrapper = NULL;
    Evas_Object * postWidget = WidgetFactory::CreatePostItemWrapper(parent, &wrapper);


    EditAction *action = static_cast<EditAction*>(actionData->mData);

    Evas_Object *text_wrapper_spacer = elm_layout_add(postWidget);
    evas_object_size_hint_weight_set(text_wrapper_spacer, 1, 1);
    evas_object_size_hint_align_set(text_wrapper_spacer, -1, 0);
    elm_layout_file_set(text_wrapper_spacer, Application::mEdjPath, "post_text");
    elm_box_pack_end(postWidget, text_wrapper_spacer);
    evas_object_show(text_wrapper_spacer);

    Evas_Object *text_wrapper = elm_layout_add(postWidget);
    evas_object_size_hint_weight_set(text_wrapper, 1, 1);
    evas_object_size_hint_align_set(text_wrapper, -1, 0);
    elm_layout_file_set(text_wrapper, Application::mEdjPath, "post_text");
    elm_box_pack_end(postWidget, text_wrapper);
    evas_object_show(text_wrapper);

    Evas_Object *post_text = elm_label_add(text_wrapper);
    elm_object_part_content_set(text_wrapper, "rect_text_box", post_text);
    elm_label_wrap_width_set(post_text, R->NF_POST_MSG_WRAP_WIDTH);
    elm_label_line_wrap_set(post_text, ELM_WRAP_MIXED);

    char *text = WidgetFactory::WrapByFormat2(SANS_MEDIUM_9197a3_FORMAT, R->NF_PH_DATE_TEXT_SIZE, action->mEditTime);
    if(text) {
        elm_object_text_set(post_text, text);
        delete[] text;
    }

    WidgetFactory::CreatePostFullText(postWidget, actionData);

    elm_box_pack_end(parent, wrapper);
    evas_object_show(wrapper);

    return postWidget;
}


