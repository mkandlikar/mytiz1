#include "EditPhotoTagScreen.h"
#include "OperationManager.h"
#include "PhotoTagOperation.h"

#define MAX_ZOOM_VALUE 8
const char *tapPhotoLabelFormat = "<font_size=%s font_style=italic color=#588FFF align=center>%s</font_size>";
EditPhotoTagScreen::EditPhotoTagScreen(Photo *item) : ScreenBase(NULL), mMouseDownRect(nullptr) {
    item->AddRef();
    mPhoto = item;
    mTagList = nullptr;
    mLayout = WidgetFactory::CreateLayoutByGroup(getParentMainLayout(), "edit_tags_screen");

    mHeader = WidgetFactory::CreateLayoutByGroup(mLayout, "edited_tags_header");
    elm_layout_content_set(mHeader, "header", WidgetFactory::CreateBlackHeaderWithLogo(mLayout, "IDS_TAG_PHOTO", "IDS_UPPERCASE_DONE", this));
    evas_object_size_hint_weight_set(mHeader, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mHeader, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_show(mHeader);
    evas_object_move(mHeader, 0 , R->STATUS_BAR_SIZE_H);
    evas_object_resize(mHeader, R->SCREEN_SIZE_WIDTH, R->HEADER_SIZE_H);

    mScreenSize.x = 0;
    mScreenSize.y = UIRes::GetInstance()->STATUS_BAR_SIZE_H;
    mScreenSize.width = UIRes::GetInstance()->SCREEN_SIZE_WIDTH;
    mScreenSize.height = UIRes::GetInstance()->SCREEN_SIZE_HEIGHT - mScreenSize.y;
    mPhotoEvasObject = elm_image_add(mLayout);

    elm_image_file_set(mPhotoEvasObject, mPhoto->GetOptimumResolutionPicture(mScreenSize.width, mScreenSize.height)->GetPath(), NULL);
    evas_object_size_hint_weight_set(mPhotoEvasObject, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mPhotoEvasObject, EVAS_HINT_FILL, EVAS_HINT_FILL);

    evas_object_event_callback_add(mPhotoEvasObject, EVAS_CALLBACK_RESIZE, on_image_resize_cb, this);
    evas_object_event_callback_add(mPhotoEvasObject, EVAS_CALLBACK_MOVE, on_image_resize_cb, this);

    evas_object_stack_below(mPhotoEvasObject, mHeader);

    evas_object_show(mPhotoEvasObject);
    FitToScreen();

    mClipper = evas_object_rectangle_add(evas_object_evas_get(mLayout));
    evas_object_move(mClipper, 0 , R->STATUS_BAR_SIZE_H);
    evas_object_resize(mClipper, R->SCREEN_SIZE_WIDTH, R->SCREEN_SIZE_HEIGHT-R->STATUS_BAR_SIZE_H);
    evas_object_clip_set(mPhotoEvasObject, mClipper);
    evas_object_show(mClipper);

    mTapToTagLabel = elm_label_add(mLayout);
    evas_object_size_hint_weight_set(mTapToTagLabel, 0.5, 0.5);
    evas_object_size_hint_align_set(mTapToTagLabel, 0, 0.5);
    FRMTD_TRNSLTD_TXT_SET2(mTapToTagLabel, tapPhotoLabelFormat, R->PT_TAP_PHOTO_LABEL_FONT_SIZE, "IDS_TAP_PHOTO_LABEL");
    evas_object_move(mTapToTagLabel, 0 , R->SCREEN_SIZE_HEIGHT - R->PT_TAP_PHOTO_LABEL_Y_POSITION);
    evas_object_resize(mTapToTagLabel, R->SCREEN_SIZE_WIDTH, R->PT_TAP_PHOTO_LABEL_HEIGHT);
    evas_object_show(mTapToTagLabel);

    mGestureLayer = elm_gesture_layer_add(mPhotoEvasObject);
    elm_gesture_layer_attach(mGestureLayer, mPhotoEvasObject);
    RegisterZoomListeners();
    RegisterMomentumListeners();
    RegisterTapsListeners();
    CreateTags();
    on_image_resize_cb(this, NULL, NULL, NULL);
}
EditPhotoTagScreen::~EditPhotoTagScreen() {
    evas_object_del(mHeader);
    evas_object_del(mClipper);
    evas_object_del(mTapToTagLabel);
    evas_object_event_callback_del(mPhotoEvasObject, EVAS_CALLBACK_RESIZE, on_image_resize_cb);
    evas_object_event_callback_del(mLayout, EVAS_CALLBACK_MOVE, on_image_resize_cb);
    UnRegisterMomentumListeners();
    UnRegisterTapsListeners();
    UnRegisterZoomListeners();
    mPhoto->Release();
    RemoveTags();
}

void EditPhotoTagScreen::menu_dismiss_cb(void *data, Evas *evas, Evas_Object *o, void *einfo) {
    EditPhotoTagScreen * me = static_cast<EditPhotoTagScreen*>(data);
    me->ClosePopup(data);
}

void EditPhotoTagScreen::ClosePopup(void *data) {
    EditPhotoTagScreen * me = static_cast<EditPhotoTagScreen*>(data);
    PhotoTagging * pt = static_cast<PhotoTagging*>(eina_list_last_data_get(me->mTagList));
    if (pt && pt->ClosePopUp()) {
        me->RemoveTag(pt);
    }
}

Evas_Event_Flags EditPhotoTagScreen::zoom_start(void *data, void *event_info) {
    EditPhotoTagScreen * me = static_cast<EditPhotoTagScreen*>(data);
    assert(me);
    Elm_Gesture_Zoom_Info *p = (Elm_Gesture_Zoom_Info *) event_info;
    me->mImageRegion.prevZoom = p->zoom;
    me->UnRegisterMomentumListeners();
    Log::info("EditPhotoTagScreen zoom_start");
    return EVAS_EVENT_FLAG_NONE;
}
Evas_Event_Flags EditPhotoTagScreen::zoom_abort(void *data, void *event_info) {
    EditPhotoTagScreen * me = static_cast<EditPhotoTagScreen*>(data);
    assert(me);
    Log::info("EditPhotoTagScreen zoom_abort");
    me->RegisterMomentumListeners();
    return EVAS_EVENT_FLAG_NONE;
}
void EditPhotoTagScreen::ConfirmCallback(void *data, Evas_Object *obj, const char *emission, const char *source) {
    PhotoTagging * pt = static_cast<PhotoTagging*>(eina_list_last_data_get(mTagList));
    if (pt) {
        pt->StartCreatingTag(pt->mBufferString.c_str(), nullptr);
    }
    ApplyChanges();
    Pop();
}
void EditPhotoTagScreen::ApplyChanges() {
    Eina_List *l;
    void *listData;
    Eina_List * tagList = mPhoto->GetPhotoTagList();
    Eina_List * tagListToDelete = nullptr;
    Eina_List * tagListToCreate = nullptr;
    EINA_LIST_FREE(tagList,listData) {
        static_cast<PhotoTag*>(listData)->Release();
    }
    EINA_LIST_FOREACH(mTagList,l,listData) {
        PhotoTagging * tagging = static_cast<PhotoTagging*>(listData);
        PhotoTag * tag = tagging->mPhotoTag;
        tag->AddRef();
        if (tagging->mIsNeedDelete) {
            tagListToDelete = eina_list_append(tagListToDelete, tag);
        } else if (tagging->mIsNeedCreate) {
            tagListToCreate = eina_list_append(tagListToCreate, tag);
            tag->AddRef();
            tagList = eina_list_append(tagList, tag);
        } else {
            tagList = eina_list_append(tagList, tag);
        }
    }
    mPhoto->SetPhotoTagList(tagList);
    if (eina_list_count(tagListToCreate)) {
        OperationManager::GetInstance()->AddOperation(MakeSptr<PhotoTagOperation>(Operation::OT_CreatePhotoTag, mPhoto->GetId(), tagListToCreate));
    }
    if (eina_list_count(tagListToDelete)) {
        OperationManager::GetInstance()->AddOperation(MakeSptr<PhotoTagOperation>(Operation::OT_DeletePhotoTag, mPhoto->GetId(), tagListToDelete));
    }
}
Evas_Event_Flags EditPhotoTagScreen::zoom_move(void *data, void *event_info) {
    Elm_Gesture_Zoom_Info *p = (Elm_Gesture_Zoom_Info *) event_info;
    EditPhotoTagScreen * me = static_cast<EditPhotoTagScreen*>(data);
    assert(me);
    double currentZoom = 1.0 + p->zoom - me->mImageRegion.prevZoom;
    me->mImageRegion.prevZoom = p->zoom;
    if (currentZoom != 1.0) {
        CalculateImageSize(me);
        ZoomImage(me, (int) p->x, (int) p->y, currentZoom);
    }
    return EVAS_EVENT_FLAG_NONE;
}
Evas_Event_Flags EditPhotoTagScreen::zoom_end(void *data, void *event_info) {
    EditPhotoTagScreen * me = static_cast<EditPhotoTagScreen*>(data);
    assert(me);
    Log::info("EditPhotoTagScreen zoom_end");
    me->RegisterMomentumListeners();
    return EVAS_EVENT_FLAG_NONE;
}
void EditPhotoTagScreen::ZoomImage(void * data, int xTouch, int yTouch, double zoom) {
    EditPhotoTagScreen * me = static_cast<EditPhotoTagScreen*>(data);
    assert(me);
    int imgWidth = zoom * (double) me->mImageRegion.width;
    int imgHeight = zoom * (double) me->mImageRegion.height;
    if (imgWidth > me->mScreenSize.width && imgWidth < (MAX_ZOOM_VALUE * me->mScreenSize.width) && imgHeight < (MAX_ZOOM_VALUE * me->mScreenSize.height)) {
        if (me->mImageRegion.height < me->mScreenSize.height ) {
            me->mImageRegion.y = me->mScreenSize.y - (imgHeight - me->mScreenSize.height) / 2;
        } else if (me->mImageRegion.y >= me->mScreenSize.y && zoom <= 1.0) {
            me->mImageRegion.y = me->mScreenSize.y;
        } else if (me->mImageRegion.y <= (me->mScreenSize.height - imgHeight) && zoom <= 1.0) {
            me->mImageRegion.y = me->mScreenSize.height - imgHeight;
        } else {
            me->mImageRegion.y = yTouch * (1.0 - zoom) + me->mImageRegion.y * zoom;
        }
        if (me->mImageRegion.width < me->mScreenSize.width ) {
            me->mImageRegion.x = me->mScreenSize.x - (imgWidth - me->mScreenSize.width) / 2;
        } else if (me->mImageRegion.x >= me->mScreenSize.x && zoom <= 1.0) {
            me->mImageRegion.x = me->mScreenSize.x;
        } else if (me->mImageRegion.x <= (me->mScreenSize.width - imgWidth) && zoom <= 1.0) {
            me->mImageRegion.x = me->mScreenSize.width - imgWidth;
        } else {
            me->mImageRegion.x = xTouch * (1.0 - zoom) + me->mImageRegion.x * zoom;
        }
        me->mImageRegion.width = imgWidth;
        me->mImageRegion.height = imgHeight;
        evas_object_resize(me->mPhotoEvasObject, me->mImageRegion.width, me->mImageRegion.height);
        evas_object_move(me->mPhotoEvasObject, me->mImageRegion.x, me->mImageRegion.y);
    }
}

void EditPhotoTagScreen::FitToUserScreen() {
    if (mImageRegion.height > mScreenSize.height) {
        if (mImageRegion.y > 0) {
            mImageRegion.y = mScreenSize.y;
        } else if (mImageRegion.y + mImageRegion.height < mScreenSize.height ) {
            mImageRegion.y = mImageRegion.y + mScreenSize.height  - (mImageRegion.height + mImageRegion.y);
            mImageRegion.y += mScreenSize.y;
        }
    } else {
        mImageRegion.y = (mScreenSize.height - mImageRegion.height) / 2;
        mImageRegion.y += mScreenSize.y;
    }

    if (mImageRegion.width > mScreenSize.width) {
        if (mImageRegion.x > 0) {
            mImageRegion.x = mScreenSize.x;
        } else if (mImageRegion.x + mImageRegion.width < mScreenSize.width ) {
            mImageRegion.x = mImageRegion.x + mScreenSize.width  - (mImageRegion.width + mImageRegion.x);
        }
    } else {
        mImageRegion.x = mScreenSize.x;
    }
    evas_object_move(mPhotoEvasObject, mImageRegion.x, mImageRegion.y);
    UpdateTagsPosition();
}

bool EditPhotoTagScreen::FitToScreen() {
    int w, h;
    elm_image_object_size_get(mPhotoEvasObject, &w, &h);

    bool ret = false;
    double displayAspect = (double) (mScreenSize.height) / (mScreenSize.width - mScreenSize.x);
    double photoAspect = (double) h / w;
    if (((mImageRegion.x != mScreenSize.x || mImageRegion.width != mScreenSize.width) && displayAspect > photoAspect)
     || ((mImageRegion.y != mScreenSize.y || mImageRegion.height != mScreenSize.height) && displayAspect <= photoAspect)) {
        if (displayAspect > photoAspect) {
            mImageRegion.width = mScreenSize.width - mScreenSize.x;
            mImageRegion.height = mImageRegion.width * photoAspect;
            mImageRegion.y = (mScreenSize.height - mImageRegion.height) / 2;
            mImageRegion.y += mScreenSize.y;
            mImageRegion.x = mScreenSize.x;
        } else {
            mImageRegion.height = mScreenSize.height;
            mImageRegion.width = (double) mImageRegion.height / photoAspect;
            mImageRegion.x = (mScreenSize.width - mScreenSize.x - mImageRegion.width) / 2;
            mImageRegion.y = mScreenSize.y;
        }
        evas_object_resize(mPhotoEvasObject, mImageRegion.width, mImageRegion.height);
        evas_object_move(mPhotoEvasObject, mImageRegion.x, mImageRegion.y);
        ret = true;
    }
    UpdateTagsPosition();
    return ret;
}
void EditPhotoTagScreen::CalculateImageSize(void * data, bool fitToBox) {
    EditPhotoTagScreen * me = static_cast<EditPhotoTagScreen*>(data);
    assert(me);
    int imgRealW, imgRealH;
    elm_image_object_size_get(me->mPhotoEvasObject, &imgRealW, &imgRealH);
    int imgGeoHeight, imgGeoWidth;
    evas_object_geometry_get(me->mPhotoEvasObject, &me->mImageRegion.x, &me->mImageRegion.y, &imgGeoWidth, &imgGeoHeight);
    evas_object_geometry_get(me->getMainLayout(), &me->mScreenSize.x, &me->mScreenSize.y, 0 ,0);//&me->mScreenSize.width, &me->mScreenSize.height);

    if (fitToBox) {
        imgGeoWidth = me->mScreenSize.width;
        imgGeoHeight = me->mScreenSize.height;
    }
    if ((double) imgRealH / imgGeoHeight > (double) imgRealW / imgGeoWidth) {
        me->mImageRegion.height = imgGeoHeight;
        me->mImageRegion.width = imgGeoHeight * imgRealW / imgRealH;
    } else {
        me->mImageRegion.height = imgGeoWidth * imgRealH / imgRealW;
        me->mImageRegion.width = imgGeoWidth;
    }
}
void EditPhotoTagScreen::MoveImage(void * data, int offsetX, int offsetY) {
    EditPhotoTagScreen * me = static_cast<EditPhotoTagScreen*>(data);
    assert(me);
    int imgX, imgY;
    bool needMove = false;
    imgX = me->mImageRegion.x + offsetX;
    imgY = me->mImageRegion.y + offsetY;
    if (me->mImageRegion.width > me->mScreenSize.width && imgX < -1 && imgX > (me->mScreenSize.width - me->mImageRegion.width)
            && (imgX + me->mImageRegion.width) > me->mScreenSize.width) {
        me->mImageRegion.x = imgX;
        needMove = true;
    }
    if (me->mImageRegion.height > me->mScreenSize.height && imgY < me->mScreenSize.y && imgY > (me->mScreenSize.height - me->mImageRegion.height)
            && (imgY + me->mImageRegion.height) > me->mScreenSize.height + me->mScreenSize.y) {
        me->mImageRegion.y = imgY;
        needMove = true;
    }
    if (needMove) {
        evas_object_move(me->mPhotoEvasObject, me->mImageRegion.x, me->mImageRegion.y);
        me->UpdateTagsPosition();
    }
}
Evas_Event_Flags EditPhotoTagScreen::momentum_start(void *data, void *event_info) {
    EditPhotoTagScreen * me = static_cast<EditPhotoTagScreen*>(data);
    assert(me);
    Elm_Gesture_Momentum_Info *p = (Elm_Gesture_Momentum_Info *) event_info;
    me->mImageRegion.momentumX = p->x1;
    me->mImageRegion.momentumY = p->y1;
    Log::info("EditPhotoTagScreen momentum_start");
    return EVAS_EVENT_FLAG_NONE;
}
Evas_Event_Flags EditPhotoTagScreen::momentum_move(void *data, void *event_info) {
    EditPhotoTagScreen * me = static_cast<EditPhotoTagScreen*>(data);
    assert(me);
    Elm_Gesture_Momentum_Info *p = (Elm_Gesture_Momentum_Info *) event_info;
    int offsetX = p->x2 - me->mImageRegion.momentumX;
    int offsetY = p->y2 - me->mImageRegion.momentumY;
    me->mImageRegion.momentumX = p->x2;
    me->mImageRegion.momentumY = p->y2;
    MoveImage(me, offsetX, offsetY);
    return EVAS_EVENT_FLAG_NONE;
}

void EditPhotoTagScreen::on_image_resize_cb(void *data, Evas *e, Evas_Object *obj, void *event_info) {
    EditPhotoTagScreen * me = static_cast<EditPhotoTagScreen*>(data);
    if (me && me->mPhotoEvasObject) {
        int w = 0;
        int x = 0;
        evas_object_geometry_get(me->mPhotoEvasObject, &x, 0, &w, 0);
        if (me->mImageRegion.width != w || me->mImageRegion.x != x) {
            evas_object_resize(me->mPhotoEvasObject, me->mImageRegion.width, me->mImageRegion.height);
            evas_object_move(me->mPhotoEvasObject, me->mImageRegion.x, me->mImageRegion.y);
        }
        me->UpdateTagsPosition();
    }
}
Evas_Event_Flags EditPhotoTagScreen::tap_end(void *data, void *event_info) {
    EditPhotoTagScreen * me = static_cast<EditPhotoTagScreen*>(data);
    assert(me);
    Elm_Gesture_Taps_Info *p = (Elm_Gesture_Taps_Info *) event_info;

    double xTap = (p->x - me->mImageRegion.x) * 100 / me->mImageRegion.width;
    double yTap = (p->y - me->mImageRegion.y) * 100 / me->mImageRegion.height;
    me->mImageRegion.x = me->mImageRegion.x - (p->x - UIRes::GetInstance()->PT_POPUP_X_POSITION);
    me->mImageRegion.y = me->mImageRegion.y - (p->y - UIRes::GetInstance()->PT_POPUP_Y_POSITION);
    PhotoTagging* pT = new PhotoTagging(me->mPhotoEvasObject, xTap, yTap, &me->mImageRegion, me, me->mClipper, me->mHeader);
    me->mTagList = eina_list_append(me->mTagList, pT);
    evas_object_move(me->mPhotoEvasObject, me->mImageRegion.x, me->mImageRegion.y);

    return EVAS_EVENT_FLAG_NONE;
}

void EditPhotoTagScreen::EnableDismissCallback(bool enable){
    if (enable) {
        mMouseDownRect = evas_object_rectangle_add(mPhotoEvasObject);
        evas_object_color_set(mMouseDownRect, 0, 0, 0, 0);
        evas_object_move(mMouseDownRect, 0, R->STATUS_BAR_SIZE_H);
        evas_object_resize(mMouseDownRect, R->SCREEN_SIZE_WIDTH, R->SCREEN_SIZE_HEIGHT-R->STATUS_BAR_SIZE_H);
        evas_object_stack_below(mMouseDownRect, mHeader);
        evas_object_show(mMouseDownRect);
        evas_object_event_callback_add(mMouseDownRect, EVAS_CALLBACK_MOUSE_DOWN, menu_dismiss_cb, this);
    } else {
        if (mMouseDownRect) {
            evas_object_del(mMouseDownRect);
            mMouseDownRect = nullptr;
        }
    }
}

void EditPhotoTagScreen::RegisterZoomListeners() {
    Log::info("EditPhotoTagScreen RegisterZoomListeners");
    elm_gesture_layer_cb_set(mGestureLayer, ELM_GESTURE_ZOOM, ELM_GESTURE_STATE_START, zoom_start, this);
    elm_gesture_layer_cb_set(mGestureLayer, ELM_GESTURE_ZOOM, ELM_GESTURE_STATE_MOVE, zoom_move, this);
    elm_gesture_layer_cb_set(mGestureLayer, ELM_GESTURE_ZOOM, ELM_GESTURE_STATE_END, zoom_end, this);
    elm_gesture_layer_cb_set(mGestureLayer, ELM_GESTURE_ZOOM, ELM_GESTURE_STATE_ABORT, zoom_abort, this);
}
void EditPhotoTagScreen::UnRegisterZoomListeners() {
    Log::info("EditPhotoTagScreen UnRegisterZoomListeners");
    elm_gesture_layer_cb_set(mGestureLayer, ELM_GESTURE_ZOOM, ELM_GESTURE_STATE_MOVE, NULL, NULL);
    elm_gesture_layer_cb_set(mGestureLayer, ELM_GESTURE_ZOOM, ELM_GESTURE_STATE_START, NULL, NULL);
    elm_gesture_layer_cb_set(mGestureLayer, ELM_GESTURE_ZOOM, ELM_GESTURE_STATE_END, NULL, NULL);
    elm_gesture_layer_cb_set(mGestureLayer, ELM_GESTURE_ZOOM, ELM_GESTURE_STATE_ABORT, NULL, NULL);
}
void EditPhotoTagScreen::RegisterTapsListeners() {
    Log::info("EditPhotoTagScreen RegisterTapsListeners");
    elm_gesture_layer_cb_set(mGestureLayer, ELM_GESTURE_N_TAPS, ELM_GESTURE_STATE_END, tap_end, this);
}
void EditPhotoTagScreen::UnRegisterTapsListeners() {
    Log::info("EditPhotoTagScreen UnRegisterTapsListeners");
    elm_gesture_layer_cb_set(mGestureLayer, ELM_GESTURE_N_TAPS, ELM_GESTURE_STATE_END, NULL, NULL);
}
void EditPhotoTagScreen::RegisterMomentumListeners() {
    Log::info("EditPhotoTagScreen RegisterMomentumListeners");
    elm_gesture_layer_cb_set(mGestureLayer, ELM_GESTURE_MOMENTUM, ELM_GESTURE_STATE_START, momentum_start, this);
    elm_gesture_layer_cb_set(mGestureLayer, ELM_GESTURE_MOMENTUM, ELM_GESTURE_STATE_MOVE, momentum_move, this);
}
void EditPhotoTagScreen::UnRegisterMomentumListeners() {
    Log::info("EditPhotoTagScreen UnRegisterMomentumListeners");
    elm_gesture_layer_cb_set(mGestureLayer, ELM_GESTURE_MOMENTUM, ELM_GESTURE_STATE_START, NULL, NULL);
    elm_gesture_layer_cb_set(mGestureLayer, ELM_GESTURE_MOMENTUM, ELM_GESTURE_STATE_MOVE, NULL, NULL);
}
void EditPhotoTagScreen::UpdateTagsPosition() {
    Eina_List *l; void *listData;
    EINA_LIST_FOREACH(mTagList, l, listData) {
        PhotoTagging* tag = static_cast<PhotoTagging*>(listData);
        tag->UpdateImageGeometry(&mImageRegion);
    }
}
void EditPhotoTagScreen::Push() {
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}
void EditPhotoTagScreen::CreateTags() {
    ClosePopup(this);
    RemoveTags();
    Eina_List *l; void *listData;
    EINA_LIST_FOREACH(mPhoto->GetPhotoTagList(), l, listData) {
        mTagList = eina_list_append(mTagList,
                new PhotoTagging(nullptr, mLayout, static_cast<PhotoTag*>(listData), &mImageRegion, true, mClipper, mHeader));
    }
}
bool EditPhotoTagScreen::RemoveTags() {
    bool ret = false;
    if (mTagList ) {
        void *listData;
        EINA_LIST_FREE(mTagList, listData) {
            delete static_cast<PhotoTagging*>(listData);
        }
        ret = true;
    }
    return ret;
}
bool EditPhotoTagScreen::HandleBackButton() {
    bool ret = true;
    PhotoTagging * pt = static_cast<PhotoTagging*>(eina_list_last_data_get(mTagList));
    if (pt && pt->ClosePopUp()) {
        RemoveTag(pt);
    } else if (FitToScreen()) {
    } else {
        ret = false;
    }
    Log::info("EditPhotoTagScreen::HandleBackButton");
    return ret;
}
void EditPhotoTagScreen::RemoveTag(void * data) {
    PhotoTagging * tag = static_cast<PhotoTagging*>(data);
    assert(tag);
    mTagList = eina_list_remove(mTagList, tag);
    delete tag;
}
