#include "UploadsScreen.h"
#include "UploadsProvider.h"


UploadsScreen::UploadsScreen(ScreenBase *parentScreen, const char* userProfileId, PhotoGalleryStrategyBase* strategy) :
        PhotoGalleryScreenBase(parentScreen, UploadsProvider::GetInstance(), strategy),
                mNoPhotosLayout(NULL) {
    dlog_print(DLOG_INFO, LOG_TAG, "UploadsScreen: create Uploads screen." );

    mLayout = elm_layout_add(getParentMainLayout());
    elm_layout_file_set(mLayout, Application::mEdjPath, "user_photos_tab_screen");
    evas_object_show(mLayout);

    elm_object_part_content_set(mLayout, "swallow.scroller", GetScroller());

    UploadsProvider::GetInstance()->SetUserId(userProfileId, PhotoGalleryProvider::EUploaded, this);

    RequestData();
}

UploadsScreen::~UploadsScreen() {
}

void UploadsScreen::CreateNoPhotosTextLayout() {
    if (!mNoPhotosLayout) {
        mNoPhotosLayout = elm_layout_add(GetDataArea());
        evas_object_size_hint_weight_set(mNoPhotosLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
        evas_object_size_hint_align_set(mNoPhotosLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);
        elm_layout_file_set(mNoPhotosLayout, Application::mEdjPath, "no_photos_text_layout");
        elm_object_translatable_part_text_set(mNoPhotosLayout, "no_result_text", "IDS_NO_PHOTO");
        evas_object_show(mNoPhotosLayout);
    }
}

void UploadsScreen::SetDataAvailable(bool isAvailable) {
    if(!isAvailable) {
        CreateNoPhotosTextLayout();
        SetTopListLayout(mNoPhotosLayout);
    } else {
        SetTopListLayout(NULL);
    }

    PhotoGalleryScreenBase::SetDataAvailable(isAvailable);
}
