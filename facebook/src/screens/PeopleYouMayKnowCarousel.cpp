#include "ActionBase.h"
#include "Common.h"
#include "ConnectivityManager.h"
#include "FacebookSession.h"
#include "FbRespondBaseRestApi.h"
#include "FbRespondGetPeopleYouMayKnow.h"
#include "FbRespondGetUsersDetailInfo.h"
#include "FriendRequestsBatchProvider.h"
#include "FriendsAction.h"
#include "Log.h"
#include "Messenger.h"
#include "OwnFriendsProvider.h"
#include "OwnProfileScreen.h"
#include "PeopleYouMayKnowCarousel.h"
#include "Popup.h"
#include "ProfileScreen.h"
#include "RequestCompleteEvent.h"
#include "SubscribersProvider.h"
#include "TrackItemsProxy.h"
#include "UserDetailInfo.h"
#include "UserFriendsScreen.h"
#include "UserProfileAbout.h"
#include "Utils.h"
#include "WebViewScreen.h"
#include "WidgetFactory.h"

PeopleYouMayKnowCarousel::PeopleYouMayKnowCarousel(const char *user_id) : ScreenBase(nullptr)
{
    mScreenId = ScreenBase::SID_PEOPLE_YOU_MAY_KNOW_CAROUSEL;
    mProvider = UsersDetailInfoProvider::GetInstance();
    // m_Followers = new Followers(SubscribersProvider::GetInstance());
    //  m_Followers->RequestData();
    mAction = new FriendsAction(this);

    mLayout = elm_layout_add(getParentMainLayout());
    elm_layout_file_set(mLayout, Application::mEdjPath, "ppl_carousel_screen");
    evas_object_size_hint_weight_set(mLayout, 1, 1);
    evas_object_size_hint_align_set(mLayout, -1, -1);

    if (!mPymkScroller) {
        mPymkScroller = new PageScroller(this,mLayout);
        mPymkScroller->ShowProgressBarPage(true);
    }
    elm_object_part_content_set(mLayout, "scroller", mPymkScroller->GetScroller() );

    mProvider->SetPositionById(user_id);

    AppEvents::Get().Subscribe(eFR_STATUS_CHANGE_EVENT, this);
    AppEvents::Get().Subscribe(eFRIEND_REQUEST_CONFIRM_DELETE_ERROR, this);
    AppEvents::Get().Subscribe(eINTERNET_CONNECTION_CHANGED, this);
    AppEvents::Get().Subscribe(eREMOVE_TOP_BLOCKED_SCREEN, this);
    AppEvents::Get().Subscribe(eFRIEND_REQUEST_CONFIRMED, this);
    AppEvents::Get().Subscribe(eUPDATE_UNFRIENDED_USER_STATUS, this);
    AppEvents::Get().Subscribe(eADD_FRIEND_BTN_CLICKED, this);
    AppEvents::Get().Subscribe(eCANCEL_FRIEND_REQUEST_BTN_CLICKED, this);
    AppEvents::Get().Subscribe(eDELETE_FRIEND_REQUEST_BTN_CLICKED, this);
    AppEvents::Get().Subscribe(ePYMK_ITEM_DELETE_EVENT, this);

    mCarouselItemsCount = 0;

    mProvider->Subscribe(this);
    mProvider->UploadData(true);
}

PeopleYouMayKnowCarousel::~PeopleYouMayKnowCarousel()
{
   // delete m_Followers;
    if(mPymkScroller->mIndex < GetLastIndex()) {
        DeleteMiniProfile(mActionsAndDates[mPymkScroller->mIndex+1]);
        mPymkScroller->ResetCurPageForRTL();
    }

    delete mAction;
    FacebookSession::GetInstance()->ReleaseGraphRequest(mGetPeopleYouMayKnowRequest);
    PopupMenu::Close();
    mProvider->EraseData();
    mProvider->Unsubscribe(this);

    for (int i=0; i<mActionsAndDates.size(); i++) {
        if(ActionAndData::IsAlive(mActionsAndDates[i])) {
            delete mActionsAndDates[i];
        }
    }

    delete mPymkScroller;
    delete mConfirmationDialog;
}

void PeopleYouMayKnowCarousel::DeleteMiniProfile(ActionAndData *action_data) {
    assert(action_data);
    if (action_data->mParentWidget) {
        elm_box_unpack(mPymkScroller->GetDataArea(),action_data->mParentWidget);
        evas_object_del(action_data->mParentWidget);
        action_data->mParentWidget = nullptr;
        action_data->mActionObj = nullptr;
    } else {
        Log::info("PeopleYouMayKnowCarousel::DeleteMiniProfile->Page is not presented on DataArea of Scroller");
    }
}

void PeopleYouMayKnowCarousel::UpdateScrollerPages() {
    Log::info("PeopleYouMayKnowCarousel::UpdateScrollerPages mIndex=%d mPrevIndex=%d", mPymkScroller->mIndex, mPymkScroller->mPrevIndex);

    int index = mPymkScroller->mIndex;
    int prevIndex = mPymkScroller->mPrevIndex;

    int lastIndex = GetLastIndex();

    if(index < 0 || index >lastIndex) {
        return;
    }

    assert(index!=prevIndex);

    mPymkScroller->RemovePageChangedListener();

    elm_object_scroll_hold_push( mPymkScroller->GetScroller());

    Evas_Object* serviceWidget;

    if (index > prevIndex) {
        serviceWidget = mPymkScroller->UnpackServiceWidget(true);

        ActionAndData *adCur = mActionsAndDates[index];
        if(!adCur->mParentWidget) {
            Log::debug( "PeopleYouMayKnowCarousel::UpdateScrollerPages:CurPage->ToTheEndDirection");
            CreateMiniProfile(adCur);
            elm_box_pack_end(mPymkScroller->GetDataArea(), adCur->mParentWidget );
        }

        if(index < lastIndex) {
            ActionAndData *adNext = mActionsAndDates[index+1];
            if(!adNext->mParentWidget) {
                CreateMiniProfile(adNext);
                elm_box_pack_end(mPymkScroller->GetDataArea(), adNext->mParentWidget );
            }
        }
        if(index - 1 > 0) {
            DeleteMiniProfile(mActionsAndDates[index-2]);
            //means that should be 3 pages at minimum
            //try to show central page; <Begin_PB - Prev - Central - Next - End_PB>
            elm_scroller_page_show( mPymkScroller->GetScroller(), 2 , 0 );
            mPymkScroller->SetPreviousPage();// only for cases when we change cur page manually
        }

        mPymkScroller->PackServiceWidget(serviceWidget,true);
    }
    else{
        serviceWidget = mPymkScroller->UnpackServiceWidget(false);

        ActionAndData *adCur = mActionsAndDates[index];
        if(!adCur->mParentWidget) {
            Log::debug( "PeopleYouMayKnowCarousel::UpdateScrollerPages:CurPage->ToTheBeginDirection");
            CreateMiniProfile(adCur);
            elm_box_pack_start(mPymkScroller->GetDataArea(), adCur->mParentWidget );
        }

        if(index > 0 ) {
            ActionAndData *adPrev = mActionsAndDates[index-1];
            if(!adPrev->mParentWidget) {
                CreateMiniProfile(adPrev);
                elm_box_pack_start(mPymkScroller->GetDataArea(), adPrev->mParentWidget );//
                //means that should be 3 pages at minimum
                //try to show central page; <Begin_PB - Prev - Central - Next - End_PB>
                elm_scroller_page_show( mPymkScroller->GetScroller(), 2 , 0 );
                mPymkScroller->SetPreviousPage();// only for cases when we change cur page manually
            }
        }
        if(index + 1 < lastIndex) {
            DeleteMiniProfile(mActionsAndDates[index+2]);
        }

        mPymkScroller->PackServiceWidget(serviceWidget,false);
    }
    elm_object_scroll_hold_pop( mPymkScroller->GetScroller());
    mPymkScroller->AddPageChangedListener();
}

void PeopleYouMayKnowCarousel::CreateMiniProfile(ActionAndData *action_data)
{
    assert(action_data);
    assert(action_data->mData);

    if( action_data->mParentWidget ) {
        Log::info("PeopleYouMayKnowCarousel::CreateMiniProfile->Page already created");
        return;
    }

    UserDetailInfo * userDetailInfo = static_cast<UserDetailInfo*>(action_data->mData);

    Evas_Object *item = elm_layout_add(mPymkScroller->GetDataArea());
    evas_object_size_hint_align_set(item, -1, 0.5);
    elm_layout_file_set(item, Application::mEdjPath, "ppl_u_may_know_item");

    evas_object_show(item);
    action_data->mActionObj = item;

    Evas_Object *cover = elm_image_add(item);
    elm_object_part_content_set(item, "cover", cover);
    if (userDetailInfo && userDetailInfo->GetCoverUrl() != NULL ) {
        action_data->UpdateImageLayoutAsync(userDetailInfo->GetCoverUrl(), cover);
    } else {
        elm_image_file_set(cover, ICON_DIR"/No-Facebook-Cover.png", NULL);
    }
    evas_object_show(cover);

    Evas_Object *avatar = elm_image_add(item);
    elm_object_part_content_set(item, "cover_avatar", avatar);
    action_data->UpdateImageLayoutAsync(userDetailInfo->GetPictureUrl(), avatar);
    evas_object_show(avatar);

    elm_object_part_text_set(item, "cover_name", userDetailInfo->GetName());

    if ( UsersDetailInfoProvider::GetInstance()->GetIsFriendRequestsMode() ) {
        //if mode is FriendRequests carousel : try to copy current FriendRequestStatus from FR/Pymk tab
        Log::debug(LOG_FACEBOOK_FRIENDS_ACTION, "PeopleYouMayKnowCarousel::CreateMiniProfile->Try to init data for UserId %s",userDetailInfo->GetId());
        User *user = FriendRequestsBatchProvider::GetInstance()->GetFrDataById(userDetailInfo->GetId());
        if (user) {
            userDetailInfo->mFriendRequestStatus = user->mFriendRequestStatus;
        } else if (userDetailInfo->mFriendRequestStatus == eUSER_REQUEST_UNKNOWN) {
            //we suppose that request has been confirmed or deleted
            if( OwnFriendsProvider::GetInstance()->GetFriendById( userDetailInfo->GetId()) ) {
                userDetailInfo->mFriendRequestStatus = eUSER_REQUEST_CONFIRMED;
            } else {
                userDetailInfo->mFriendRequestStatus = eUSER_REQUEST_DELETED;
            }
        }
    } else {
        FbRespondGetPeopleYouMayKnow::FriendableUser *user = FriendRequestsBatchProvider::GetInstance()->GetPymkDataById(userDetailInfo->GetId());
        if (user) {
            userDetailInfo->mFriendRequestStatus = user->mFriendRequestStatus; //copy statuses
        }
    }

    RefreshBtns(action_data);

    if (UsersDetailInfoProvider::GetInstance()->GetIsFriendRequestsMode()) {
        elm_object_signal_callback_add(item, "btn.clicked", "respond", on_ctxmenu_respond_cb, action_data);
        elm_object_signal_callback_add(item, "btn.clicked", "add", ActionBase::on_fr_carousel_btn_clicked_cb, action_data);
    } else {
        elm_object_signal_emit(item, "hide.set", "respond");
        elm_object_signal_callback_add(item, "btn.clicked", "add", ActionBase::on_pymk_btn_clicked_cb, action_data);
    }

    /* Message btn and callback */
    elm_object_translatable_part_text_set(item, "message_btn_text", "IDS_MESSAGE");
    elm_object_signal_callback_add(item, "message.clicked", "message_btn_*", on_message_cb, this);

    /* More btn callback */
    elm_object_signal_callback_add(item, "more.clicked", "more_btn_*", on_ctxmenu_cb, action_data);

    /* ACTION BTNS INIT END*/

    int fields_count = 0;

    if ( NO_ITEMS_FOUND != Utils::FindElement((void *)(userDetailInfo->GetId()), SubscribersProvider::GetInstance()->GetIds(), SubscribersProvider::idsComparator) )
    {
        Evas_Object *icon = elm_image_add(item);
        elm_object_part_content_set(item, GetIconName(fields_count), icon);
        elm_image_file_set(icon, ICON_DIR"/profiles/about_follows.png", NULL);
        evas_object_show(icon);

        elm_object_translatable_part_text_set(item, GetStoryName(fields_count), "IDS_FOLLOWS_YOUR_POSTS");

        fields_count++;
    }

    char * mutualFriendAvatar = userDetailInfo->GetMutualFriendAvatar();

    if ( mutualFriendAvatar && strcmp( mutualFriendAvatar , "" ) )
    {
        const int mutualFriendCount = userDetailInfo->GetMutualFriendsCount();

        Evas_Object *icon = elm_image_add(item);
        elm_object_part_content_set(item, GetIconName(fields_count), icon);
        action_data->UpdateImageLayoutAsync(mutualFriendAvatar, icon);
        evas_object_show(icon);

        std::string format;
        switch (mutualFriendCount) {
            case 1:
                format.append("1 %s: ");
                format.append(userDetailInfo->GetFirstMutualFriendName());
                FRMTD_TRNSLTD_PART_TXT_SET1(item, GetStoryName(fields_count),format.c_str(), "IDS_SINGLE_MUTUAL_FRIEND");
                break;
            case 2:
                format.append("2 %s: ");
                format.append(userDetailInfo->GetFirstMutualFriendName());
                format.append(" %s ");
                format.append(userDetailInfo->GetSecondMutualFriendName());
                FRMTD_TRNSLTD_PART_TXT_SET2(item, GetStoryName(fields_count),format.c_str(), "IDS_MUTUAL_FRIENDS", "IDS_AND");
                break;
            default:
                format.append(Utils::FormatBigInteger(mutualFriendCount).c_str());
                format.append(" %s %s ");
                format.append(userDetailInfo->GetFirstMutualFriendName());
                format.append(" %s ");
                format.append(userDetailInfo->GetSecondMutualFriendName());
                FRMTD_TRNSLTD_PART_TXT_SET3(item, GetStoryName(fields_count),format.c_str(), "IDS_MUTUAL_FRIENDS", "IDS_INCLUDING", "IDS_AND");
                break;
        }

        elm_object_signal_callback_add(item, GetEventName(fields_count), "*", on_friends_clicked_cb, action_data);
        fields_count++;
    }

    const char *birthday = userDetailInfo->GetBirthday();

    if ( birthday && strcmp( birthday , "" ) )
    {
        Evas_Object *icon = elm_image_add(item);
        elm_object_part_content_set(item, GetIconName(fields_count), icon);
        elm_image_file_set(icon, ICON_DIR"/profiles/about_birthday.png", NULL);
        evas_object_show(icon);

        char cbirthday[strlen(birthday) + 1];
        eina_strlcpy(cbirthday, birthday, strlen(birthday) + 1);
        char *split_string = strtok (cbirthday, "/");

        int timeArray[3] = {0};
        int index = 0;
        while ( split_string != NULL && index < 3 )
        {
            timeArray[index] = atoi(split_string);
            split_string = strtok (NULL, "/");
            index++;
        }

        struct tm time = {0};
        char DateFormat[256] = {0};

        time.tm_mday = timeArray[1];
        time.tm_mon = timeArray[0]-1;//see description
        time.tm_year = timeArray[2] - 1900;  //year without century
        if ( time.tm_year > 0 ) {
            time.tm_year++;
            strftime(DateFormat, 30, "%B %e, %G", &time);
        } else {
            strftime(DateFormat, 30, "%B %e", &time);
        }
        int size = strlen("%s %s") + strlen(i18n_get_text("IDS_BORN_ON")) + strlen(DateFormat) + 1;
        char story[size];
        Utils::Snprintf_s(story, size, "%s %s", i18n_get_text("IDS_BORN_ON"), DateFormat);

        elm_object_part_text_set(item, GetStoryName(fields_count), story);
        elm_object_signal_callback_add(item, GetEventName(fields_count), "*", on_about_clicked_cb, action_data);
        fields_count++;
    }

    const char *location = userDetailInfo->GetLocation();

    if ( fields_count < 3 && location && strcmp(location, ""))
    {
        char * locationUrl = userDetailInfo->GetLocationIconUrl();

        Evas_Object *icon = elm_image_add(item);
        elm_object_part_content_set(item, GetIconName(fields_count), icon);
        if ( locationUrl && strcmp(locationUrl, "") ) {
            action_data->UpdateImageLayoutAsync(locationUrl, icon);
        } else {
            elm_image_file_set(icon, ICON_DIR"/profiles/about_lives.png", NULL);
        }
        evas_object_show(icon);


        char *story = WidgetFactory::WrapByFormat2("%s %s", "Lives in", location);
        elm_object_part_text_set(item, GetStoryName(fields_count), story);
        delete[] story;
        elm_object_signal_callback_add(item, GetEventName(fields_count), "*", on_about_clicked_cb, action_data);

        fields_count++;
    }

    const char *position = userDetailInfo->GetPosition();
    const char *employer = userDetailInfo->GetEmployer();

    if ( fields_count < 3 && ((position && strcmp(position, "")) || (employer && strcmp(employer, ""))))
    {
        char * workUrl = userDetailInfo->GetWorkIconUrl();

        Evas_Object *icon = elm_image_add(item);
        elm_object_part_content_set(item, GetIconName(fields_count), icon);
        if ( workUrl && strcmp(workUrl, "") ) {
            action_data->UpdateImageLayoutAsync(workUrl, icon);
        } else {
            elm_image_file_set(icon, ICON_DIR"/profiles/about_work.png", NULL);
        }
        evas_object_show(icon);

        char *story = nullptr;
        if (position && strcmp(position , "") && employer && strcmp( employer , "")) {
            story = WidgetFactory::WrapByFormat3("%s %s %s", position, i18n_get_text("IDS_AT") , employer);
        } else if (position && strcmp( position , "" )) {
            story = WidgetFactory::WrapByFormat("%s", position);
        } else if (employer && strcmp( employer , "")) {
            story = WidgetFactory::WrapByFormat2("%s %s", "Works at", employer);
        }
        if (story) {
            elm_object_part_text_set(item, GetStoryName(fields_count), story);
            delete[] story;
        }
        elm_object_signal_callback_add(item, GetEventName(fields_count), "*", on_about_clicked_cb, action_data);

        fields_count++;
    }

    const char *education = userDetailInfo->GetEducation();

    if ( fields_count < 3 && education && strcmp(education, ""))
    {
        char * educationUrl = userDetailInfo->GetEducationUrl();

        Evas_Object *icon = elm_image_add(item);
        elm_object_part_content_set(item, GetIconName(fields_count), icon);
        if ( educationUrl && strcmp(educationUrl, "") ) {
            action_data->UpdateImageLayoutAsync(educationUrl, icon);
        } else {
            elm_image_file_set(icon, ICON_DIR"/profiles/about_education.png", NULL);
        }
        evas_object_show(icon);

        int size = strlen("%s %s") + strlen("Studied at") + strlen(education) + 1;
        char story[size];
        Utils::Snprintf_s(story, size, "%s %s", "Studied at", education);
        elm_object_part_text_set(item, GetStoryName(fields_count), story);
        elm_object_signal_callback_add(item, GetEventName(fields_count), "*", on_about_clicked_cb, action_data);

        fields_count++;
    }

    const char *relationshipStatus = userDetailInfo->GetRelationshipStatus();

    if (fields_count < 3 && relationshipStatus && strcmp(relationshipStatus,""))
    {
        Evas_Object *icon = elm_image_add(item);
        elm_object_part_content_set(item, GetIconName(fields_count), icon);
        elm_image_file_set(icon, ICON_DIR"/profiles/Relationship.png", NULL);
        evas_object_show(icon);

        elm_object_part_text_set(item, GetStoryName(fields_count), relationshipStatus);
        elm_object_signal_callback_add(item, GetEventName(fields_count), "*", on_about_clicked_cb, action_data);

        fields_count++;
    }

    if (fields_count == 2) {
        elm_object_signal_emit(item, "hide.one.box", "story");
    } else if (fields_count == 1) {
        elm_object_signal_emit(item, "hide.two.box", "story");
    } else if (fields_count == 0) {
        elm_object_signal_emit(item, "hide.three.box", "story");
    }

    elm_object_translatable_part_text_set(item, "view_profile", "IDS_VIEW_PROFILE");

    elm_object_signal_callback_add(item, "viewprofile.clicked", "viewprofile_*", on_cover_clicked_cb, action_data);
    elm_object_signal_callback_add(item, "cover.clicked", "cover_*", on_cover_clicked_cb, action_data);

    action_data->mParentWidget=item;
}

void PeopleYouMayKnowCarousel::RefreshBtns(ActionAndData *actionData)
{
    assert(actionData);
    assert(actionData->mData);

    UserDetailInfo * userDetailInfo = static_cast<UserDetailInfo*>(actionData->mData);

    assert(actionData->mActionObj);
    if (UsersDetailInfoProvider::GetInstance()->GetIsFriendRequestsMode()) {
        switch (userDetailInfo->mFriendRequestStatus) {
            case eUSER_REQUEST_UNKNOWN: {
                Log::debug(LOG_FACEBOOK_FRIENDS_ACTION, "PeopleYouMayKnowCarousel::RefreshBtns::eUSER_REQUEST_UNKNOWN");
                elm_object_signal_emit(actionData->mActionObj, Application::IsRTLLanguage() ? "show.set.rtl" : "show.set.ltr", "respond");
                elm_object_signal_emit(actionData->mActionObj, "hide.set", "add");
                elm_object_translatable_part_text_set(actionData->mActionObj, "respond_btn_text", "IDS_RESPOND");
            }
                break;
            case eUSER_REQUEST_CONFIRMED: {
                Log::debug(LOG_FACEBOOK_FRIENDS_ACTION, "PeopleYouMayKnowCarousel::RefreshBtns::eUSER_REQUEST_CONFIRMED");
                elm_object_signal_emit(actionData->mActionObj, "hide.set", "respond");
                elm_object_signal_emit(actionData->mActionObj, Application::IsRTLLanguage() ? "friended.set.rtl" : "friended.set.ltr", "add");
                elm_object_signal_emit(actionData->mActionObj, "friended.set", "add");
                elm_object_translatable_part_text_set(actionData->mActionObj, "add_friend_btn_text", "IDS_PROFILE_FRIEND_FRIENDED");
            }
                break;
            case eUSER_REQUEST_DELETED:
            case eUSER_REQUEST_CANCELLED: {
                Log::debug(LOG_FACEBOOK_FRIENDS_ACTION, "PeopleYouMayKnowCarousel::RefreshBtns::eUSER_REQUEST_DELETEDorCancel");
                elm_object_signal_emit(actionData->mActionObj, "hide.set", "respond");
                elm_object_signal_emit(actionData->mActionObj, Application::IsRTLLanguage() ? "show.set.rtl" : "show.set.ltr", "add");
                elm_object_signal_emit(actionData->mActionObj, "add.set", "add");
                elm_object_translatable_part_text_set(actionData->mActionObj, "add_friend_btn_text", "IDS_PROFILE_OTHER_ADDFRIEND");
            }
                break;
            case eUSER_REQUEST_SENT: {
                Log::debug(LOG_FACEBOOK_FRIENDS_ACTION, "PeopleYouMayKnowCarousel::RefreshBtns::eUSER_REQUEST_SENT");
                elm_object_signal_emit(actionData->mActionObj, "hide.set", "respond");
                elm_object_signal_emit(actionData->mActionObj, Application::IsRTLLanguage() ? "cancel_request.set.rtl" : "cancel_request.set.ltr", "add");
                elm_object_translatable_part_text_set(actionData->mActionObj, "add_friend_btn_text", "IDS_CANCEL_REQUEST");
            }
                break;
            default: {
                Log::debug(LOG_FACEBOOK_FRIENDS_ACTION, "PeopleYouMayKnowCarousel::RefreshBtns::default");
                elm_object_signal_emit(actionData->mActionObj, Application::IsRTLLanguage() ? "show.set.rtl" : "show.set.ltr", "respond");
                elm_object_signal_emit(actionData->mActionObj, "hide.set", "add");
                elm_object_translatable_part_text_set(actionData->mActionObj, "respond_btn_text", "IDS_RESPOND");
            }
        }
    } else {
        if (userDetailInfo->mFriendRequestStatus ==  eUSER_REQUEST_SENT) {
            elm_object_translatable_part_text_set(actionData->mActionObj, "add_friend_btn_text", "IDS_CANCEL");
        } else if (userDetailInfo->mFriendRequestStatus ==  eUSER_REQUEST_CANCELLED) {
            elm_object_translatable_part_text_set(actionData->mActionObj, "add_friend_btn_text", "IDS_PROFILE_OTHER_ADDFRIEND");
        } else if (userDetailInfo->mFriendRequestStatus ==  eUSER_REQUEST_CONFIRMED) {
            elm_object_signal_callback_del(actionData->mActionObj, "btn.clicked", "add", ActionBase::on_pymk_btn_clicked_cb);
            elm_object_translatable_part_text_set(actionData->mActionObj, "add_friend_btn_text", "IDS_PROFILE_FRIEND_FRIENDED");
        } else {
            elm_object_translatable_part_text_set(actionData->mActionObj, "add_friend_btn_text", "IDS_PROFILE_OTHER_ADDFRIEND");
        }
    }
}

void PeopleYouMayKnowCarousel::Push()
{
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(elm_naviframe_top_item_get(getNf()), EINA_FALSE, EINA_FALSE);
}

void PeopleYouMayKnowCarousel::UpdatePymkItem(const char *id, FriendRequestStatus friendRequestStatus){
    for (int i=0; i<mActionsAndDates.size(); i++) {
        if (mActionsAndDates[i]->mData) {
            if( !strcmp(mActionsAndDates[i]->mData->GetId(), id) ){
                Log::debug( "PeopleYouMayKnowCarousel::UpdatePymkItem->requested item was found");
                if( friendRequestStatus != eUSER_REQUEST_UNKNOWN ) {
                    static_cast<UserDetailInfo*>(mActionsAndDates[i]->mData)->mFriendRequestStatus = friendRequestStatus;
                }
                RefreshBtns(mActionsAndDates[i]);
                break;
            }
        }
    }
}

void PeopleYouMayKnowCarousel::Update(AppEventId eventId, void * data)
{
    Log::debug( "PeopleYouMayKnowCarousel::Update with eventId %d",(int)eventId);
    switch(eventId) {
    case eFR_STATUS_CHANGE_EVENT: {
        GraphObject *graphObject = static_cast<GraphObject*>(data);
        UserDetailInfo *userDetailInfo = dynamic_cast<UserDetailInfo*>(graphObject);
        if (userDetailInfo) {
            UpdatePymkItem(userDetailInfo->GetId());
        } else {
            User *user = dynamic_cast<User*>(graphObject);
            if (user) {
                UpdatePymkItem(user->GetId());
            }
        }
    }
        break;
    case eFR_ITEM_UPDATE_EVENT:
        if (data) {
            ActionAndData *actionData = static_cast<ActionAndData*>(data);
            RefreshBtns(actionData);
        }
        break;
    case eADD_FRIEND_BTN_CLICKED:
        if (data) {
            UpdatePymkItem(static_cast<char*>(data),eUSER_REQUEST_SENT);
        }
        break;
    case eDELETE_FRIEND_REQUEST_BTN_CLICKED://comes from profile
    case eCANCEL_FRIEND_REQUEST_BTN_CLICKED:
    case eUPDATE_UNFRIENDED_USER_STATUS:
    case eFRIEND_REQUEST_CONFIRM_DELETE_ERROR:
        if (data) {
            UpdatePymkItem(static_cast<char*>(data),eUSER_REQUEST_CANCELLED);
        }
        break;
    case eFRIEND_REQUEST_CONFIRMED:
        if (data) {
            UpdatePymkItem(static_cast<char*>(data),eUSER_REQUEST_CONFIRMED);
        }
        break;
    case eINTERNET_CONNECTION_CHANGED:
        if (ConnectivityManager::Singleton().IsConnected()) {  //connection has been restored
            if (!ProviderIsEmpty()) {
                if(mPymkScroller->IsFirstPage()) {
                    mProvider->UploadData(false);
                }
                mPymkScroller->ShowProgressBarPage(false);
            }

            if (mPymkScroller->IsLastPage() || ProviderIsEmpty()) {
                 mProvider->UploadData(true);
            }
            mPymkScroller->ShowProgressBarPage(true);
        }
        else {
            mPymkScroller->ShowErrorPage(false);//try to show both
            mPymkScroller->ShowErrorPage(true);
        }
        break;
    case eREQUEST_COMPLETED_EVENT:
    {
        RequestCompleteEvent *eventData = static_cast<RequestCompleteEvent *>(data);
        if(eventData) {
            if(eventData->mCurlCode == CURLE_OK) {
                AddItemsToCarousel();
            }
            else{
                mPymkScroller->ShowErrorPage();
            }
        }
        break;
    }
    case eREMOVE_TOP_BLOCKED_SCREEN:
        if (CarouselIsEmpty()) {
            if (Application::GetInstance()->GetTopScreen() == this) {
                ecore_job_add(remove_this_screen, this);
                return;
            }
        }
        break;
    case ePYMK_ITEM_DELETE_EVENT:
        if (data) {
            RemoveMiniProfileFromCarousel(data);
            UserDetailInfo* userDetailInfo = UsersDetailInfoProvider::GetInstance()->GetUsersDetailInfoObjectById(static_cast<char*>(data));
            if (userDetailInfo) {
                UsersDetailInfoProvider::GetInstance()->DeleteItem(userDetailInfo);
            }
        }
        break;
    default:
        Log::debug( "PeopleYouMayKnowCarousel::Update->other event");
        break;
    }
}

void PeopleYouMayKnowCarousel::RemoveMiniProfileFromCarousel(void *data) {
    int index = 0;
    ActionAndData* foundItemData = FindPymkItem(static_cast<char*>(data), index);
    if (foundItemData) {
        DeleteMiniProfile(foundItemData);// try to remove
        mActionsAndDates.erase(mActionsAndDates.begin() + index);
        if (index == mPymkScroller->mIndex) {
            if (mPymkScroller->mIndex == mActionsAndDates.size()) {
                elm_scroller_page_show(mPymkScroller->GetScroller(), 0 , 0);
            } else {
                elm_scroller_page_show(mPymkScroller->GetScroller(), 1 , 0);
            }
        } else if (index < mPymkScroller->mIndex) {
            if (index + 1 == mPymkScroller->mIndex) {
                //jump to next card, all indexes will be changed inside scroller_page_changed
                elm_scroller_page_show(mPymkScroller->GetScroller(), 1 , 0);
            } else {
                // or just decrease index, possibly blocked card has been already deleted
                mPymkScroller->mIndex--;
                mPymkScroller->mPrevIndex--;
            }
        }
    }
}

ActionAndData* PeopleYouMayKnowCarousel::FindPymkItem(const char *id, int &index) {
    assert(id);
    for (int i=0; i<mActionsAndDates.size(); i++) {
        if (mActionsAndDates[i]->mData) {
            if( !strcmp(mActionsAndDates[i]->mData->GetId(), id) ){
                Log::debug( "PeopleYouMayKnowCarousel::FindPymkItem->requested item was found");
                index = i;
                return mActionsAndDates[i];
            }
        }
    }
    return nullptr;
}

void PeopleYouMayKnowCarousel::SetScrollerPageManually(int pageNumber) {
    mPymkScroller->RemovePageChangedListener();
    elm_scroller_page_show( mPymkScroller->GetScroller(), pageNumber , 0 );
    mPymkScroller->mPrevScrollerPage = pageNumber;
    mPymkScroller->AddPageChangedListener();
}

void PeopleYouMayKnowCarousel::AddItemsToCarousel() {
    Log::debug( "PeopleYouMayKnowCarousel::AddItemsToCarousel-> IsDescOrder = %d", mProvider->GetIsLastRequestInDescendingOrder());
    Log::debug( "PeopleYouMayKnowCarousel::AddItemsToCarousel-> ProviderSize = %d", mProvider->GetDataCount());
    Log::debug( "PeopleYouMayKnowCarousel::AddItemsToCarousel-> mCarouselItemsCount = %d", mCarouselItemsCount);
    int index;

    if(mProvider->GetIsLastRequestInDescendingOrder()) {

        index = mCarouselItemsCount;

        for ( ; index < mProvider->GetDataCount(); index++) {
            void* item = eina_array_data_get( mProvider->GetData(), index );
            CreatePymkItem(item, true);// just creates action and data
        }

        if(mCarouselItemsCount == 0)  {// first request
            mPymkScroller->ShowProgressBarPage(false);
            mPymkScroller->mIndex = 0;
            mPymkScroller->mPrevIndex = -1;

            SetScrollerPageManually(1);
            UpdateScrollerPages();
        }
        if(mPymkScroller->IsLastPage()){
            UpdateScrollerPages();
        }
    }
    else {
        int added = index = mProvider->GetDataCount() - mCarouselItemsCount;

        for ( ; index > 0; index--) {
            void* item = eina_array_data_get( mProvider->GetData(), index-1 );
            CreatePymkItem(item, false);// just creates action and data
        }
        if(mPymkScroller->IsFirstPage()) {
            if(added < 2){
                SetScrollerPageManually(1);
            }
            else{
                SetScrollerPageManually(2);
            }
            UpdateScrollerPages();
        }
    }
    mCarouselItemsCount = mProvider->GetDataCount();
}

void PeopleYouMayKnowCarousel::on_cover_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    assert(user_data);
    Log::debug(LOG_FACEBOOK_USER, "PeopleYouMayKnowCarousel::on_cover_clicked_cb");
    ActionAndData *action_data = static_cast<ActionAndData *>(user_data);
    if(action_data->mData) {
        UserDetailInfo * userDetailInfo = static_cast<UserDetailInfo*>(action_data->mData);
        if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_USER_PROFILE) {
            Utils::OpenProfileScreen(userDetailInfo->GetId());
        }
    }
    else{
        Log::error(LOG_FACEBOOK_USER, "PeopleYouMayKnowCarousel::on_cover_clicked_cb-> no UserDetailInfo");
    }
}

void PeopleYouMayKnowCarousel::on_ctxmenu_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    if (obj) {
        assert(user_data);
        ActionAndData *action_data = static_cast<ActionAndData*>(user_data);

        static PopupMenu::PopupMenuItem context_menu_items[] = {
            { nullptr, nullptr, nullptr, nullptr, nullptr, 0, true, false },
            // Poke was marked as "LOW PRIORITY" - https://github.com/fbmp/fb4t/issues/11
            //{ nullptr, "Poke", nullptr, nullptr, nullptr, 1, true, false },
        };

        static const char *itemText_block = "IDS_BLOCK";
        context_menu_items[0].itemText = itemText_block;
        context_menu_items[0].callback = on_block_user_clicked_cb;
        context_menu_items[0].data = action_data;

        Evas_Coord y = 0, h = 0;
        evas_object_geometry_get(obj, nullptr, &y, nullptr, &h);
        y += h / 2;
        PopupMenu::Show(1, context_menu_items, action_data->mAction->getScreen()->getParentMainLayout(), false, y);
    }
}

void PeopleYouMayKnowCarousel::on_ctxmenu_respond_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);

    if (obj) {
        static PopupMenu::PopupMenuItem context_menu_items[] = {
            { nullptr, "IDS_CONFIRM_FRIEND_REQUEST", nullptr, nullptr, nullptr, 0, false, false },
            { nullptr, "IDS_DELETE_FRIEND_REQUEST", nullptr, nullptr, nullptr, 1, false, false },
            { nullptr, "IDS_CANCEL", nullptr, close_popup, nullptr, 2, true, false },
        };
        context_menu_items[0].callback = action_data->mAction->on_confirm_friend_rq_btn_clicked_cb;
        context_menu_items[0].data = action_data;
        context_menu_items[1].callback = action_data->mAction->on_delete_friend_rq_btn_clicked_cb;
        context_menu_items[1].data = action_data;

        Evas_Coord y = 0, h = 0;
        evas_object_geometry_get(obj, nullptr, &y, nullptr, &h);
        y += h / 2;
        PopupMenu::Show(3, context_menu_items, action_data->mAction->getScreen()->getParentMainLayout(), false, y);
    }
}

void PeopleYouMayKnowCarousel::on_block_user_clicked_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    assert(user_data);
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    assert(action_data->mAction);
    PeopleYouMayKnowCarousel *screen = static_cast<PeopleYouMayKnowCarousel*> (action_data->mAction->getScreen());
    screen->ShowCancelBlockPopup(action_data, screen->mLayout);
}

void PeopleYouMayKnowCarousel::ShowCancelBlockPopup(ActionAndData *action_data, Evas_Object *layout)
{
    assert(action_data);
    assert(action_data->mData);
    UserDetailInfo * userDetailInfo = static_cast<UserDetailInfo*>(action_data->mData);
    if (userDetailInfo) {
        char * title = WidgetFactory::WrapByFormat(i18n_get_text("IDS_PROFILE_POPUP_BLOCK_TITLE"), userDetailInfo->GetName());
        char *description = WidgetFactory::WrapByFormat(i18n_get_text("IDS_PROFILE_POPUP_BLOCK_MESSAGE"), userDetailInfo->GetName());
        mConfirmationDialog = ConfirmationDialog::CreateAndShow(layout,
                                                                title,
                                                                description,
                                                                "IDS_CANCEL",
                                                                "IDS_BLOCK",
                                                                confirmation_dialog_cb,
                                                                action_data);
        delete[] description;
        delete[] title;
    }
}

void PeopleYouMayKnowCarousel::confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event) {
    assert(user_data);
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    PeopleYouMayKnowCarousel *screen = static_cast<PeopleYouMayKnowCarousel*> (action_data->mAction->getScreen());

    if (screen && (ScreenBase::SID_PEOPLE_YOU_MAY_KNOW_CAROUSEL == screen->GetScreenId())) {
        delete screen->mConfirmationDialog;
        screen->mConfirmationDialog = NULL;
    }
    if (ConfirmationDialog::ECDNoPressed == event) {
        action_data->mAction->DoBlock(action_data);
    }
}

void PeopleYouMayKnowCarousel::on_message_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    if (Messenger::IsMessengerInstalled()) {
        Log::info(LOG_FACEBOOK_USER, "PeopleYouMayKnowCarousel::on_message_cb() - Launching the FB Messenger...");
        Messenger::LaunchMessenger();
    } else {
        Log::info(LOG_FACEBOOK_USER, "PeopleYouMayKnowCarousel::on_message_cb() - FB Messenger is not installed on your device, get it from Tizen store");
        Messenger *newMessengerScreen = new Messenger(nullptr);
        Application::GetInstance()->AddScreen(newMessengerScreen);
    }
}

void PeopleYouMayKnowCarousel::on_friends_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *action_data = static_cast<ActionAndData *>(user_data);
    if(action_data && action_data->mData) {
        UserDetailInfo * userDetailInfo = static_cast<UserDetailInfo*>(action_data->mData);
        if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_USER_PROFILE_FRIENDS_TAB) {
            UserFriendsScreen *userProfileFriendsScreen = new UserFriendsScreen(userDetailInfo->GetId(), i18n_get_text("IDS_MUTUAL_FRIENDS_TITLE"), true);
            Application::GetInstance()->AddScreen(userProfileFriendsScreen);
        }
    }
}

void PeopleYouMayKnowCarousel::on_about_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source) {
    assert(user_data);
    ActionAndData *action_data = static_cast<ActionAndData *>(user_data);
    if(action_data->mData) {
        UserDetailInfo * userDetailInfo = static_cast<UserDetailInfo*>(action_data->mData);
        if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_USER_PROFILE_ABOUT) {
            std::string WebUri = REQ_UPDATE_INFO_URI;
            WebUri.append("&id=");
            WebUri.append(userDetailInfo->GetId());
            WebViewScreen::Launch(WebUri.c_str(), true);
        }
    }
}

void PeopleYouMayKnowCarousel::CreatePymkItem( void* dataForItem, bool addToEnd ) {
    assert(dataForItem);
    Log::debug(LOG_FACEBOOK_USER, "PeopleYouMayKnowCarousel::CreatePymkItem");
    UserDetailInfo *user_detail_info_data = static_cast<UserDetailInfo*>(dataForItem);
    ActionAndData *data = new ActionAndData(user_detail_info_data, mAction);
    if(addToEnd) {
        mActionsAndDates.push_back(data);
    }
    else {
        mActionsAndDates.insert(mActionsAndDates.begin(),1,data);
        //Normalize Indexes on every insert
        mPymkScroller->mIndex++;
        mPymkScroller->mPrevIndex++;
    }
}

/**
 * @brief Get the required line to set an icon within it
 * @param[in] line_number - number of the line to fill
 * @return icon place in edc related to the given line
 */
const char * PeopleYouMayKnowCarousel::GetIconName(const int line_number) {
    switch (line_number) {
    case 0:
        return "first_icon";
    case 1:
        return "second_icon";
    case 2:
        return "third_icon";
    default:
        return "first_icon";
    }
}

/**
 * @brief Get the required line to set a story within it
 * @param[in] line_number - number of the line to fill
 * @return story place in edc related to the given line
 */
const char * PeopleYouMayKnowCarousel::GetStoryName(const int line_number) {
    switch (line_number) {
    case 0:
        return "first_story";
    case 1:
        return "second_story";
    case 2:
        return "third_story";
    default:
        return "first_story";
    }
}

const char * PeopleYouMayKnowCarousel::GetEventName(const int line_number) {
    switch (line_number) {
    case 0:
        return "first_box.clicked";
    case 1:
        return "second_box.clicked";
    case 2:
        return "third_box.clicked";
    default:
        return "unknown";
    }
}

bool PeopleYouMayKnowCarousel::HandleBackButton()
{
    if (mConfirmationDialog) {
        delete mConfirmationDialog;
        mConfirmationDialog = NULL;
        PopupMenu::Close();
        return true;
    } else {
        return PopupMenu::Close();
    }
}

void PeopleYouMayKnowCarousel::close_popup(void *user_data, Evas_Object *obj, void *event_info){
    PopupMenu::Close();
}

void PeopleYouMayKnowCarousel::remove_this_screen(void *data) {
    assert(data);
    PeopleYouMayKnowCarousel *me = static_cast<PeopleYouMayKnowCarousel*>(data);
    me->Pop();
}
/**********************************************************************
 *
 * @class PageScroller
 *
 **********************************************************************/
PeopleYouMayKnowCarousel::PageScroller::PageScroller(PeopleYouMayKnowCarousel *pymk,Evas_Object *parentLayout)
{
    mPymk = pymk;
    mScroller = elm_scroller_add( parentLayout );
    elm_scroller_page_size_set( mScroller, UIRes::GetInstance()->FF_CAROUSEL_SCROLLER_W, 0 );
    elm_scroller_page_scroll_limit_set( mScroller, 1, 1 );
    elm_scroller_policy_set( mScroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_OFF );

    mDataArea = elm_box_add( mScroller );
    evas_object_size_hint_weight_set(mDataArea, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set( mDataArea, EVAS_HINT_FILL, EVAS_HINT_FILL );
    elm_box_padding_set( mDataArea, 0, 0 );
    elm_box_horizontal_set( mDataArea, EINA_TRUE );
    elm_object_content_set( mScroller, mDataArea );
    evas_object_show( mScroller );

    evas_object_event_callback_add( mScroller, EVAS_CALLBACK_MOUSE_DOWN, mouse_down_cb, this );
    evas_object_event_callback_add( mScroller, EVAS_CALLBACK_MOUSE_UP, mouse_up_cb, this );

    mIndex = 0;;
    mPrevIndex = -1;
    mPrevScrollerPage = -1;
}

void PeopleYouMayKnowCarousel::PageScroller::mouse_down_cb(void *data, Evas *e, Evas_Object *obj, void *event_info) {
    PageScroller* me = static_cast<PageScroller*>( data );
    elm_scroller_movement_block_set(me->mScroller,ELM_SCROLLER_MOVEMENT_NO_BLOCK);
    if(me->mIsHeadData){
        if(me->mIndex == 0) {//first card
            Log::debug( "PeopleYouMayKnowCarousel::PageScroller::mouse_down_cb->UploadHead");
            me->mIsHeadData = me->mPymk->mProvider->UploadData(false);
            if(!me->mIsHeadData) {
                if(me->mPymkConnectionErrorLayout[0]) {
                    me->ShowProgressBarPage(false);
                }
                me->DeleteProgressBarPulseLayout(false);
            }
        }
    }// but fisrt card could be last card (1 item), hence no 'else if'
    if(me->mIsTailData){
        if(me->mIndex == me->mPymk->GetLastIndex()) {//last card
            Log::debug( "PeopleYouMayKnowCarousel::PageScroller::mouse_down_cb->UploadTail");
            me->mIsTailData = me->mPymk->mProvider->UploadData(true);
            if(!me->mIsTailData) {
                if(me->mPymkConnectionErrorLayout[1]) {
                    me->ShowProgressBarPage(true);
                }
                me->DeleteProgressBarPulseLayout(true);
            }
        }
    }
}

void PeopleYouMayKnowCarousel::PageScroller::mouse_up_cb(void *data, Evas *e, Evas_Object *obj, void *event_info) {
    PageScroller* me = static_cast<PageScroller*>( data );
    elm_scroller_movement_block_set(me->mScroller,ELM_SCROLLER_MOVEMENT_BLOCK_HORIZONTAL);
    Log::debug( "PeopleYouMayKnowCarousel::PageScroller::mouse_up_cb");
    if(!me->mIsHeadData && me->IsFirstPage()) {
        Log::debug( "PeopleYouMayKnowCarousel::PageScroller::mouse_up_cb->BringBegin");
        me->BringInToFirstPage();
    }
    else if(!me->mIsTailData && me->IsLastPage()) {
        Log::debug( "PeopleYouMayKnowCarousel::PageScroller::mouse_up_cb->BringEnd");
        me->BringInToLastPage();
    }
}


void PeopleYouMayKnowCarousel::PageScroller::AddPageChangedListener()
{
    evas_object_smart_callback_add( mScroller, "scroll,page,changed", scroller_page_changed, this );
}

void PeopleYouMayKnowCarousel::PageScroller::RemovePageChangedListener()
{
    evas_object_smart_callback_del( mScroller, "scroll,page,changed", scroller_page_changed );
}


PeopleYouMayKnowCarousel::PageScroller::~PageScroller()
{
    HideProgressBarPage(false);
    HideProgressBarPage(true);
    HideErrorPage(false);
    HideErrorPage(true);
    RemovePageChangedListener();
    evas_object_event_callback_del( mScroller, EVAS_CALLBACK_MOUSE_DOWN, mouse_down_cb );
    evas_object_event_callback_del( mScroller, EVAS_CALLBACK_MOUSE_UP, mouse_up_cb );
}

bool PeopleYouMayKnowCarousel::PageScroller::IsFirstPage() {

    int currentPage = 0;
    elm_scroller_current_page_get( mScroller, &currentPage, nullptr);
    Log::debug( "PeopleYouMayKnowCarousel::PageScroller::IsFirstPage() %d", currentPage );
    return currentPage == 0 ;
}

bool PeopleYouMayKnowCarousel::PageScroller::IsLastPage(){

    int currentPage = 0;
    elm_scroller_current_page_get( mScroller, &currentPage, nullptr);

    int lastPage = 0;
    elm_scroller_last_page_get( mScroller, &lastPage, nullptr);
    Log::debug( "PeopleYouMayKnowCarousel::PageScroller::IsLastPage currentPage=%d LastPage=%d", currentPage, lastPage );
    return currentPage == lastPage ;
}

void PeopleYouMayKnowCarousel::PageScroller::SetPreviousPage() {
    int currentPage = 0;
    elm_scroller_current_page_get( mScroller, &currentPage , nullptr );
    mPrevScrollerPage = currentPage;
    Log::debug( "PeopleYouMayKnowCarousel::PageScroller::SetPreviousPage %d",mPrevScrollerPage);
}

void PeopleYouMayKnowCarousel::PageScroller::ResetCurPageForRTL() {
    if(Application::IsRTLLanguage()) {
        int currentPage = 0;
        elm_scroller_current_page_get(mScroller, &currentPage, nullptr);
        elm_scroller_page_show( mScroller, currentPage, 0 );//workaround for arabic
     }
}

void PeopleYouMayKnowCarousel::PageScroller::BringInToFirstPage() {
    Log::debug( "PeopleYouMayKnowCarousel::PageScroller::BringInToFirstPage");
    elm_scroller_page_bring_in( mScroller, 1, 0 );
    mIndex = 0;
    mPrevIndex = 1;// go to 1 st card
    mPrevScrollerPage = 1; // set prev page again, after forcibly bring in
}

void PeopleYouMayKnowCarousel::PageScroller::BringInToLastPage() {
    Log::debug( "PeopleYouMayKnowCarousel::PageScroller::BringInToLastPage");
    int currentPage = 0;
    elm_scroller_current_page_get(mScroller, &currentPage, nullptr);
    elm_scroller_page_bring_in( mScroller, currentPage-1, 0 );
    mIndex = mPymk->GetLastIndex();
    mPrevIndex = mIndex-1;// go to last card
    mPrevScrollerPage = currentPage-1; //set prev page again, after forcibly bring in
}

void PeopleYouMayKnowCarousel::PageScroller::scroller_page_changed( void *data, Evas_Object *obj, void *event_info )
{
    Log::debug( "PeopleYouMayKnowCarousel::PageScroller::scroller_page_changed");
    PageScroller* me = static_cast<PageScroller*>( data );

    elm_scroller_movement_block_set(me->mScroller,ELM_SCROLLER_MOVEMENT_BLOCK_HORIZONTAL);

    int currentPage = 0;
    elm_scroller_current_page_get( me->mScroller, &currentPage, nullptr);

    Log::debug( "PeopleYouMayKnowCarousel::PageScroller::scroller_page_changed-> currentPage= %d",currentPage);
    Log::debug( "PeopleYouMayKnowCarousel::PageScroller::scroller_page_changed-> mPrevScrollerPage %d ",me->mPrevScrollerPage);

    if(currentPage > me->mPrevScrollerPage) {
        me->mPrevIndex = me->mIndex;
        me->mIndex++;
    }
    else if(currentPage < me->mPrevScrollerPage) {
        me->mPrevIndex = me->mIndex;
        me->mIndex--;
    }
    else{
        Log::error( "PeopleYouMayKnowCarousel::PageScroller::scroller_page_changed-> indexes equal");
        return;
    }

    me->SetPreviousPage();

    if(me->IsFirstPage()) {
        if(!me->mIsHeadData) {
            me->BringInToFirstPage();
            return;
        }
    }
    else if(me->IsLastPage()) {
        if(!me->mIsTailData) {
            me->BringInToLastPage();
            return;
        }
    }
    me->mPymk->UpdateScrollerPages();
    me->ResetCurPageForRTL();
}

void PeopleYouMayKnowCarousel::PageScroller::ShowErrorPage(bool packToEnd)
{
    int index = packToEnd ? 1:0;
    if(!mPymkProgressBar[index]) return;// no appropriate progress bar and no error widget
    HideProgressBarPage(packToEnd);
    if(!mPymkConnectionErrorLayout[index]) {
        mPymkConnectionErrorLayout[index] = elm_layout_add( mDataArea );
        TrackItemsProxy::ShowConnectionErrorBig("connection_error_scroll_by_pages_big",mPymkConnectionErrorLayout[index]);
        elm_object_translatable_part_text_set(mPymkConnectionErrorLayout[index], "text_part2", "IDS_TAP_TO_RETRY");
        if(packToEnd) {
            elm_box_pack_end( mDataArea, mPymkConnectionErrorLayout[index] );
        }
        else{
            elm_box_pack_start( mDataArea, mPymkConnectionErrorLayout[index] );
        }
        evas_object_show( mPymkConnectionErrorLayout[index] );
    }
}

void PeopleYouMayKnowCarousel::PageScroller::HideErrorPage(bool isPackedToEnd) {
    int index = isPackedToEnd ? 1:0;
    if ( mPymkConnectionErrorLayout[index] ) {
        elm_box_unpack( mDataArea, mPymkConnectionErrorLayout[index] );
        evas_object_del( mPymkConnectionErrorLayout[index] );
        mPymkConnectionErrorLayout[index] = nullptr;
    }
}

void PeopleYouMayKnowCarousel::PageScroller::ShowProgressBarPage(bool packToEnd) {
    HideErrorPage(packToEnd);
    int index = packToEnd ? 1:0;
    if (!mPymkProgressBarLayout[index] && !mPymkProgressBar[index]) {
        mPymkProgressBarLayout[index]  = elm_layout_add(mDataArea);
        elm_layout_file_set( mPymkProgressBarLayout[index], Application::mEdjPath, "progress_bar_big");
        if(packToEnd) {
            elm_box_pack_end( mDataArea, mPymkProgressBarLayout[index] );
        }
        else{
            elm_box_pack_start( mDataArea, mPymkProgressBarLayout[index] );
        }
        evas_object_show( mPymkProgressBarLayout[index] );
        mPymkProgressBar[index] = elm_progressbar_add( mPymkProgressBarLayout[index] );
        elm_object_style_set( mPymkProgressBar[index], "process_medium" );
        elm_progressbar_pulse( mPymkProgressBar[index], EINA_TRUE );
        elm_layout_content_set( mPymkProgressBarLayout[index], "elm.progressbar.swallow", mPymkProgressBar[index] );
    }
}

void PeopleYouMayKnowCarousel::PageScroller::HideProgressBarPage(bool isPackedToEnd) {
    int index = isPackedToEnd ? 1:0;
    if ( mPymkProgressBarLayout[index] && mPymkProgressBar[index] ) {
        evas_object_del( mPymkProgressBar[index] );
        mPymkProgressBar[index] = nullptr;
        elm_box_unpack( mDataArea, mPymkProgressBarLayout[index] );
        evas_object_del( mPymkProgressBarLayout[index] );
        mPymkProgressBarLayout[index] = nullptr;
    }
}

void PeopleYouMayKnowCarousel::PageScroller::DeleteProgressBarPulseLayout(bool isPackedToEnd) {
    int index = isPackedToEnd ? 1:0;
    if(mPymkProgressBar[index]){
        evas_object_del( mPymkProgressBar[index] );
        mPymkProgressBar[index] = nullptr;
    }
}

Evas_Object* PeopleYouMayKnowCarousel::PageScroller::UnpackServiceWidget(bool isPackedToEnd) {
    int index = isPackedToEnd ? 1:0;

    Evas_Object* serviceWidget = nullptr;
    if(mPymkProgressBarLayout[index]) {
        serviceWidget = mPymkProgressBarLayout[index];
    }
    else if(mPymkConnectionErrorLayout[index]){
        serviceWidget = mPymkConnectionErrorLayout[index];
    }

    if (serviceWidget) {
        elm_box_unpack( mDataArea, serviceWidget );
        evas_object_hide(serviceWidget);
    }
    return serviceWidget;
}

void PeopleYouMayKnowCarousel::PageScroller::PackServiceWidget(Evas_Object* serviceWidget,bool packToEnd) {
    if(packToEnd) {
        elm_box_pack_end( mDataArea, serviceWidget );
    }
    else{
        elm_box_pack_start( mDataArea, serviceWidget );
    }
    evas_object_show(serviceWidget);
}

/**********************************************************************
 *
 * @class Followers
 *
 **********************************************************************/
PeopleYouMayKnowCarousel::Followers::Followers(AbstractDataProvider *provider )
{
    m_Provider = provider;
    mId = NULL;
    mName = NULL;
}

PeopleYouMayKnowCarousel::Followers::~Followers()
{
    m_Provider->Unsubscribe( this );
}

void PeopleYouMayKnowCarousel::Followers::Update(AppEventId eventId, void * data)
{
    m_Provider->Unsubscribe( this );
}

void PeopleYouMayKnowCarousel::Followers::RequestData()
{
    m_Provider->Subscribe( this );
    if ( !m_Provider->UploadData(true) ) {
        m_Provider->Unsubscribe(this);
    }
}

void PeopleYouMayKnowCarousel::Followers::Redraw()
{

}
