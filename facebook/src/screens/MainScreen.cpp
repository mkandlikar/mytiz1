#include <efl_extension.h>

#include "CacheManager.h"
#include "CameraRollScreen.h"
#include "Common.h"
#include "Config.h"
#include "ConnectivityManager.h"
#include "ErrorHandling.h"
#include "FbRespondGetFeed.h"
#include "FbRespondGetFriendRequests.h"
#include "FindFriendsScreen.h"
#include "FriendsRequestTab.h"
#include "GraphRequest.h"
#include "GroupProvider.h"
#include "HomeScreen.h"
#include "jsonutilities.h"
#include "Log.h"
#include "LoggedInUserData.h"
#include "LoginScreen.h"
#include "MainScreen.h"
#include "MessagerScreen.h"
#include "Messenger.h"
#include "NotificationsScreen.h"
#include "OwnFriendsProvider.h"
#include "PostComposerScreen.h"
#include "SearchScreen.h"
#include "SettingsScreen.h"
#include "WidgetFactory.h"

#define IMAGES_DIR "images/"
#define MESSENGER_ICONS_DIR "GetMessenger/"

ScreenBase *MainScreen::my_instance = NULL;

MainScreen::MainScreen(ScreenBase *parentScreen):ScreenBase(parentScreen) {
    Evas_Object *parent = getParentMainLayout();

    mScreenId = ScreenBase::SID_MAIN_SCREEN;
    mDrawer = NULL;
    mScroller = NULL;
    mTabbar = NULL;
    mLayout = CreateMainView(parent);
    mEvasMessengerIcon = NULL;
    my_instance = this;
    mOwnProfileDataUploader = nullptr;
    //Load friends
    OwnFriendsProvider::GetInstance()->UploadData();
    //Load groups
    GroupProvider::GetInstance()->UploadData();
    //Load profile
    if (!LoggedInUserData::GetInstance().mUserProfileData) {
        mOwnProfileDataUploader = new UserProfileDataUploader(Config::GetInstance().GetUserId().c_str(), nullptr, nullptr);
        mOwnProfileDataUploader->StartUploading();// uploads own profile data after Force Close
    }
    Tab tab {eFeedTab};
    AppEvents::Get().Notify(eMAINSCREEN_TAB_CHANGED, &tab);
}

MainScreen::~MainScreen()
{
    delete mOwnProfileDataUploader;
}

void MainScreen::Push() {
    ScreenBase::Push();

    // We can add drawer when screen will be placed into naviframe
    /* Drawer bg */
    Evas_Object *bg = CreateBg(mLayout);
    elm_object_part_content_set(mLayout, "elm.swallow.bg", bg);

#if 0
    /* Drawer */
    mDrawer = CreatePanel(mLayout);
    eext_object_event_callback_add(mDrawer, EEXT_CALLBACK_BACK, drawer_back_cb, this);
    evas_object_smart_callback_add(mDrawer, "scroll", panel_scroll_cb, bg);
    elm_object_part_content_set(mLayout, "elm.swallow.right", mDrawer);

    /* Drawers Button */
    Evas_Object *btn = CreateDrawersBtn(mLayout, on_drawer_button_cb, mDrawer);
    Eina_Bool res = elm_layout_content_set(mLayout, "elm.swallow.drawerbutton", btn);
#endif

    SetMessengerIcon();
    elm_object_signal_callback_add(mLayout, "MessengerIconSignal", "MessengerIconSource",
            (Edje_Signal_Cb)MessengerCb, this);

    // hide standard title of naviframe
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);

    if (ConnectivityManager::Singleton().IsConnected()) {
        Application::GetInstance()->mPrivacyOptionsData->GetPrivacyOptionsFromFB(NULL);
    }
}

void MainScreen::MessengerCb(void* Data, Evas_Object* Obj, void* EventInfo) {
    if (Messenger::IsMessengerInstalled()) {
        Messenger::LaunchMessenger();
    } else {
        Messenger* NewMessengerScreen = new Messenger(NULL);
        Application::GetInstance()->AddScreen(NewMessengerScreen);
    }
}

void MainScreen::SetMessengerIcon() {
    char *resourcePath = app_get_resource_path();
    if (!resourcePath) {
        return;
    }
    std::string MessengerIconPath(resourcePath);
    MessengerIconPath.append(IMAGES_DIR);
    MessengerIconPath.append(MESSENGER_ICONS_DIR);
    MessengerIconPath.append("messenger_white.png");
    free(resourcePath);
    if (mEvasMessengerIcon)
        evas_object_del(mEvasMessengerIcon);
    mEvasMessengerIcon = elm_image_add(mLayout);
    ELM_IMAGE_FILE_SET(mEvasMessengerIcon, MessengerIconPath.c_str(), nullptr);
    elm_layout_content_set(mLayout, "MessengerIcon", mEvasMessengerIcon);
    evas_object_show(mEvasMessengerIcon);
}

void MainScreen::OnViewResize(Evas_Coord w, Evas_Coord h)
{
    elm_scroller_page_size_set(mScroller, w, h);
//https://www.dropbox.com/sh/ho4ftjneuykd3z6/AACo8jwO3AuobwvsSQE2hD7ia?dl=0&preview=Screen_20170328_184540.png
    int currentPage;
    elm_scroller_current_page_get(mScroller, &currentPage, nullptr);
    elm_scroller_page_show(mScroller, currentPage, 0); //workaround for RTL. Otherwise, most left page is shown in the scroller instead of most right.
    evas_object_size_hint_min_set(mFirstViewRect, R->MS_MAIN_RECT_SIZE_W, R->MS_MAIN_RECT_SIZE_H);
    evas_object_size_hint_min_set(mSecondViewRect, R->MS_MAIN_RECT_SIZE_W, R->MS_MAIN_RECT_SIZE_H);
    evas_object_size_hint_min_set(mFourthViewRect, R->MS_MAIN_RECT_SIZE_W, R->MS_MAIN_RECT_SIZE_H);
    evas_object_size_hint_min_set(mFifthViewRect, R->MS_MAIN_RECT_SIZE_W, R->MS_MAIN_RECT_SIZE_H);
}

bool MainScreen::HandleBackButton() {
    bool ret = true;
    int page = 0;
    elm_scroller_current_page_get(mScroller, &page, NULL);
    Log::debug(LOG_FACEBOOK, "MainScreen::HandleBackButton():: page : %d", page);

    if (WidgetFactory::CloseErrorDialogue()){
    } else if (WidgetFactory::CloseConfirmDialogue()){
    } else if (WidgetFactory::CloseAlertPopup()) {
    } else if (WidgetFactory::close_popup_menu()){
    } else if (page) {
        int targetPageToMove = eFeedTab;
        // Handling of Logout popup Back button from Settings screen
        if ((page == eSettingsTab) && (mSettingsScreen->clearPopup())) {
            targetPageToMove = eSettingsTab;
        }
        elm_scroller_page_bring_in(mScroller, targetPageToMove, 0);
        return true;

    } else {
    	ret = false;
    }
    return ret;
}

Evas_Object *MainScreen::CreateMainView(Evas_Object *parent)
{
    Evas_Object *layout, *box;

    /* Layout for drawer */
    layout = CreateDrawerLayout(parent);

    CreateSearchField(layout);

    /* Box */
    box = elm_box_add(layout);
    evas_object_size_hint_weight_set(box, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(box, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_object_part_content_set(layout, "elm.swallow.content", box);

    /* Tabbar */
    mTabbar = CreateTabbar(box);
    elm_box_pack_end(box, mTabbar);

    /* Scroller */
    mScroller = CreateScroller(box);
    elm_box_pack_end(box, mScroller);

    /* Box */
    mNestedScreenContainer = elm_box_add(mScroller);
    elm_box_horizontal_set(mNestedScreenContainer, EINA_TRUE);
    evas_object_size_hint_weight_set(mNestedScreenContainer, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mNestedScreenContainer, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_object_content_set(mScroller, mNestedScreenContainer);

    /* Feeds view */
    mHomeScreen = new HomeScreen(this);
    mFirstViewRect = MinSet(((ScreenBase *)mHomeScreen)->getMainLayout(), mNestedScreenContainer, 0, 0);


    /* Friends view */
    mFriendsScreen = new FriendsRequestTab(this, false);
    mSecondViewRect = MinSet(((ScreenBase *) mFriendsScreen)->getMainLayout(), mNestedScreenContainer, 0, 0);

    /* Notifications  view */
    mNotificationScreen = new NotificationsScreen(this);
    mFourthViewRect = MinSet(((ScreenBase *) mNotificationScreen)->getMainLayout(), mNestedScreenContainer, 0, 0);

    /* Settings view */
    mSettingsScreen = new SettingsScreen(this);
    mFifthViewRect = MinSet(mSettingsScreen->getMainLayout(), mNestedScreenContainer, 0, 0);

    Evas_Coord w, h;
    evas_object_geometry_get(Application::GetInstance()->mWin, NULL, NULL, &w, &h);
    // Fix for Bug # 595 : Subtract the height of search bar & tab bar from the mWin height.
    OnViewResize(w, h-(R->GE_SEARCH_BAR_N_TAB_BAR_SIZE_H ));

    elm_object_signal_callback_add(mTabbar, "mouse,clicked,*", "tab_img_0", scroll_to_top, this);
    return layout;
}

void MainScreen::scroll_to_top(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    MainScreen *screen = static_cast<MainScreen *>(data);
    if (screen){
        int h_page = 0;
        elm_scroller_current_page_get(screen->mScroller, &h_page, 0);

        Log::debug(LOG_FACEBOOK, "MainScreen::scroll_to_top. The source is: %s; page = %d", source, h_page);

        if (h_page == eFeedTab && (!strcmp(source, "tab_img_0"))) {
            screen->AllowNewsFeedBtnClick(false);
            screen->mHomeScreen->AnimatedTop();
        }
        // TODO: implement the same for other tabs when providers are ready
        /*
        else if (h_page == eFriendRequestsTab && (!strcmp(source, "tab_img_1") || !strcmp(source, "tab_item_1"))) {
            elm_scroller_page_bring_in(screen->mFriendsScreen->GetScroller(), 0, 0);
        }
        else if (h_page == eNotificationsTab && (!strcmp(source, "tab_img_2") || !strcmp(source, "tab_item_2"))) {
            elm_scroller_page_bring_in(screen->mNotificationScreen->GetScroller(), 0, 0);
        }
        else if (h_page == eSettingsTab && (!strcmp(source, "tab_img_3") || !strcmp(source, "tab_item_3"))) {
            elm_scroller_page_bring_in(screen->m, 0, 0);
        }
        */
    }
}

void MainScreen::AllowNewsFeedBtnClick(bool allow)
{
    if (allow) {
        Log::debug(LOG_FACEBOOK, "MainScreen::AllowNeewsFeedBtnClick Enable click");
        elm_object_signal_callback_add(mTabbar, "mouse,clicked,*", "tab_img_0", scroll_to_top, this);
    } else {
        Log::debug(LOG_FACEBOOK, "MainScreen::AllowNeewsFeedBtnClick Disable click");
        elm_object_signal_callback_del(mTabbar, "mouse,clicked,*", "tab_img_0", scroll_to_top);
    }
}

void MainScreen::anim_stop_cb(void *data, Evas_Object *obj, void *event_info)
{
    MainScreen *screen = static_cast<MainScreen *>(data);
    if (screen && screen->mScroller && screen->mTabbar) {
        int page = 0;
        elm_scroller_current_page_get(screen->mScroller, &page, NULL);
        char signalName[50];
        Utils::Snprintf_s(signalName, 50, "tabbar,select,%d", page);
        elm_object_signal_emit(screen->mTabbar, signalName, "");
        if (page == eFeedTab) {
            elm_object_focus_set(screen->mHomeScreen->getMainLayout(), EINA_TRUE);
            elm_object_signal_emit(screen->mMainLayout, "hide.ff.set", "spacer");
            elm_object_signal_emit(screen->mMainLayout, "show.set", "spacer");
            screen->AllowNewsFeedBtnClick(true);
        } else if (page == eFriendRequestsTab) {
            elm_object_focus_set(screen->mFriendsScreen->getMainLayout(), EINA_TRUE);
            elm_object_signal_emit(screen->mMainLayout, "hide.set", "spacer");
            elm_object_signal_emit(screen->mMainLayout, "show.ff.set", "spacer");
            screen->SetTabbarBadge(page, 0); //setting badge number to func to display Jewels(Badge)
            elm_object_signal_callback_add(screen->mTabbar, "mouse,clicked,*", "tab_img_1", FriendsRequestTab::scroll_to_top , screen->mFriendsScreen);
        } else if (page == eNotificationsTab) {
            elm_object_focus_set(screen->mNotificationScreen->getMainLayout(), EINA_TRUE);
            screen->mNotificationScreen->FetchNewNotifications();
            elm_object_signal_emit(screen->mMainLayout, "hide.set", "spacer");
            elm_object_signal_emit(screen->mMainLayout, "hide.ff.set", "spacer");
            AppEvents::Get().Notify(eNOTIFICATION_UPDATE_BADGE, NULL);
        } else if (page == eSettingsTab) {
            elm_object_focus_set(screen->mSettingsScreen->getMainLayout(), EINA_TRUE);
            elm_object_signal_emit(screen->mMainLayout, "hide.set", "spacer");
            elm_object_signal_emit(screen->mMainLayout, "hide.ff.set", "spacer");
            SettingsScreen::cb_get_groups_list();
        } else {
            elm_object_focus_set(screen->mSettingsScreen->getMainLayout(), EINA_TRUE);
            elm_object_signal_emit(screen->mMainLayout, "hide.set", "spacer");
            elm_object_signal_emit(screen->mMainLayout, "hide.ff.set", "spacer");
        }
        AppEvents::Get().Notify(eMAINSCREEN_TAB_CHANGED, &page);
    }
}

Evas_Object *MainScreen::CreateScroller(Evas_Object *parent)
{
    Evas_Object *scroller;

    scroller = elm_scroller_add(parent);
    elm_scroller_policy_set(scroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_OFF);
    elm_scroller_page_size_set(scroller, 0, 0);
    elm_scroller_page_scroll_limit_set(scroller, 1, 0);
    elm_scroller_single_direction_set(scroller, ELM_SCROLLER_SINGLE_DIRECTION_HARD);
    evas_object_size_hint_weight_set(scroller, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(scroller, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_smart_callback_add(scroller, "scroll,anim,stop", anim_stop_cb, this);
    evas_object_show(scroller);

    return scroller;
}

/**
 * @brief This method create search field, add it to parent and assign callback functions
 * @param[in] parent - parent evas object for tabbar
 */
void MainScreen::CreateSearchField(Evas_Object *parent) {
    /* Search field */
    elm_object_translatable_part_text_set(parent, "search_input_text", "IDS_SEARCH");
    elm_object_signal_callback_add(parent, "search_field_clicked", "", (Edje_Signal_Cb) searchfield_clicked_cb, this);

}

/**
 * @brief This method create main tabbar and assign callback function
 * @param[in] parent - parent evas object for tabbar
 * @return a pointer to tabbar object
 */
Evas_Object *MainScreen::CreateTabbar(Evas_Object *parent)
{
    Evas_Object *tabbar = NULL;

    tabbar = elm_layout_add(parent);
    if(tabbar) {
        elm_layout_file_set(tabbar, Application::mEdjPath, "main_tabs");
        evas_object_size_hint_weight_set(tabbar, 1, 0);
        evas_object_size_hint_align_set(tabbar, -1, -1);
        evas_object_size_hint_min_set(tabbar, R->CW_MAINTABS_SIZE_W, R->CW_MAINTABS_SIZE_H);
        evas_object_size_hint_max_set(tabbar, -1, R->CW_MAINTABS_SIZE_H);
        evas_object_show(tabbar);
        elm_object_signal_callback_add(tabbar, "tabbar.selected", "*", on_tabbar_handler, this);
        // Mark the first tab as selected
        elm_object_signal_emit(tabbar, "tabbar,select,0", "");
    }
    return tabbar;
}

int MainScreen::GetTabIndex() const
{
    int page = 0;
    elm_scroller_current_page_get( mScroller, &page, NULL );
    return page;
}
/**
 * @brief Set badge in tabbar item
 * @param[in] index - tab index
 * @param[in] num - number that should be displayed in badge
 */
void MainScreen::SetTabbarBadge(int index, int num) {
    if (mTabbar) {
        int page = 0;
        elm_scroller_current_page_get(mScroller, &page, NULL);

        char signalName[50];
        if (num > 0) {
            char badgeStrValue[] = "99+";
            if (num > 99) {
                Utils::Snprintf_s(signalName, 50, "tabbar,badge,big,%d", index);
            } else {
                Utils::Snprintf_s(badgeStrValue, sizeof(badgeStrValue), "%d", num); //the actual number of unread items
                Utils::Snprintf_s(signalName, 50, "tabbar,badge,on,%d", index);
            }

            char badgeTextPartName[] = "tag_badge_text_0";
            badgeTextPartName[sizeof(badgeTextPartName) - 2] = '0' + index;
            elm_object_part_text_set(mTabbar, badgeTextPartName, badgeStrValue);

            //set News Feed tab selected
            if (page == eFeedTab) {
                elm_object_signal_emit(mTabbar, "tabbar,select,0", "");
            }
        } else {
            Utils::Snprintf_s(signalName, 50, "tabbar,badge,off,%d", index);
        }
        elm_object_signal_emit(mTabbar, signalName, "");
    }
}

/**
 * @brief It is handler signals from tabbar
 * @param[in] data - pointer on MainScreen object
 * @param[in] obj - pointer on tabbar evas object
 * @param[in] emission - signal: it should be equal to "tabbar.selected"
 * @param[in] source - source of signal: it can contain values: "tab_0", "tab_1",... "tab_4"
 * @return a pointer to tabbar object
 */
void MainScreen::on_tabbar_handler(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    MainScreen *screen = static_cast<MainScreen *>(data);
    if (screen->mScroller) {
        screen->AllowNewsFeedBtnClick(false);
        elm_object_signal_callback_del(screen->mTabbar, "mouse,clicked,*", "tab_img_1", FriendsRequestTab::scroll_to_top );
        if (source) {
            int tabIndex = -1;
            if (!strcmp(source, "tab_0")) {
                tabIndex = (int)eFeedTab;
            } else if (!strcmp(source, "tab_1")) {
                tabIndex = (int)eFriendRequestsTab;
            } else if (!strcmp(source, "tab_2")) {
                tabIndex = (int)eNotificationsTab;
            } else if (!strcmp(source, "tab_3")) {
                tabIndex = (int)eSettingsTab;
            }
            if (tabIndex >= 0) {
                elm_scroller_page_bring_in(screen->mScroller, tabIndex, 0);
            }
        }
    }
}

#if 0
Evas_Object *MainScreen::CreateDrawersBtn(Evas_Object *parent, Evas_Smart_Cb func, void *data)
{
    Evas_Object *btn = elm_button_add(parent);
    if (!btn) return NULL;
    ELM_BUTTON_STYLE_SET2(btn, "drawers_button_ltr", "drawers_button_rtl");
    evas_object_smart_callback_add(btn, "clicked", func, data);

    return btn;
}
#endif

Evas_Object *MainScreen::CreateDrawerLayout(Evas_Object *parent)
{
    mMainLayout = elm_layout_add(parent);
    if(mMainLayout) {
        elm_layout_file_set(mMainLayout, Application::mEdjPath, "main_screen");
        evas_object_size_hint_weight_set(mMainLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
        evas_object_size_hint_align_set(mMainLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);
        evas_object_show(mMainLayout);
    }
    return mMainLayout;
}

Evas_Object *MainScreen::CreateBg(Evas_Object *parent)
{
    Evas_Object *rect;
    rect = evas_object_rectangle_add(evas_object_evas_get(parent));
    evas_object_color_set(rect, 0, 0, 0, 0);
    evas_object_show(rect);

    return rect;
}

#if 0
void MainScreen::ToggleDrawerPanel(Eina_Bool hidden) {
    if(mDrawer) {
        if (!elm_object_disabled_get(mDrawer)) {
            elm_panel_hidden_set(mDrawer, hidden);
        }
    }
}
#endif

void MainScreen::panel_scroll_cb(void *data, Evas_Object *obj, void *event_info)
{
    Elm_Panel_Scroll_Info *ev = static_cast<Elm_Panel_Scroll_Info *>(event_info);
    Evas_Object *bg = static_cast<Evas_Object *>(data);
    int col = 127 * ev->rel_x;

    evas_object_color_set(bg, 0, 0, 0, col);
}

void MainScreen::on_login_item_selected(void *data, Evas_Object *obj, void *event_info)
{
    Log::debug(LOG_FACEBOOK, "MainScreen::on_login_item_selected");
    MainScreen* mainScreen = reinterpret_cast<MainScreen*>(data);

    LoginScreen * screen = (LoginScreen *)new LoginScreen(mainScreen);
}

void MainScreen::on_friends_item_selected(void *data, Evas_Object *obj, void *event_info)
{
    Log::debug(LOG_FACEBOOK, "MainScreen::on_friends_item_selected");
}

void MainScreen::on_friends_from_db_item_selected(void *data, Evas_Object *obj, void *event_info)
{
    Log::debug(LOG_FACEBOOK, "MainScreen::on_friends_from_db_item_selected");
    MainScreen* mainScreen = reinterpret_cast<MainScreen*>(data);

    Eina_List *friendsList = CacheManager::GetInstance().SelectFriendsData();
    if (friendsList) {
        // Removed
    }
}

void MainScreen::on_feed_from_db_item_selected(void *data, Evas_Object *obj, void *event_info)
{
    Log::debug(LOG_FACEBOOK, "MainScreen::on_feed_from_db_item_selected");
    MainScreen* mainScreen = reinterpret_cast<MainScreen*>(data);

    Eina_List *feedList = CacheManager::GetInstance().SelectFeedData(10);
    if (feedList) {
        // TODO make this works
//        mainScreen->mHomeScreen->SetData(NULL);
    }
}

/*TODO: User profile functionalities are being handled from Setting screen, so can be removed from here
//UserProfile Functions
//To get Userpfofile from FB server
void MainScreen::on_useprofile_item_selected(void *data, Evas_Object *obj, void *event_info)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "on_userprofile_item_selected");
    MainScreen* mainScreen = reinterpret_cast<MainScreen*>(data);
    //Create the Userprofile Screen object.
    ScreenBase *scr = (ScreenBase *)new UserProfile();
    Application::GetInstance()->AddScreen(scr);
    elm_naviframe_item_title_enabled_set(elm_naviframe_top_item_get(scr->getNf()), EINA_FALSE, EINA_FALSE);
}


//Fetch the stored userprofile
void MainScreen::on_userprofile_from_db_selected(void *data, Evas_Object *obj, void *event_info)
{
    //Do nothing
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "on_userprofile_from_db_selected");
}
*/


Evas_Object *MainScreen::CreatePanel(Evas_Object *parent)
{
    Evas_Object *panel, *list;
    int i;
    char buf[64];

    /* Panel */
    panel = elm_panel_add(parent);
    elm_panel_scrollable_set(panel, EINA_TRUE);

    /* Default is visible, hide the content in default. */
    elm_panel_hidden_set(panel, EINA_TRUE);
    elm_panel_orient_set(panel, ELM_PANEL_ORIENT_RIGHT);
    evas_object_show(panel);

    /* Panel content */
    list = elm_list_add(panel);
    evas_object_size_hint_weight_set(list, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(list, EVAS_HINT_FILL, EVAS_HINT_FILL);
    for (i = 0; i < 20; i++)
    {
        switch (i) {
        case LOGIN:
            Utils::Snprintf_s(buf, 64, "Login");
            elm_list_item_append(list, buf, NULL, NULL, on_login_item_selected, this);
            break;
        case FRIENDS:
            Utils::Snprintf_s(buf, 64, "Friends");
            elm_list_item_append(list, buf, NULL, NULL, on_friends_item_selected, this);
            break;
        case FRIENDS_FROM_DB:
            Utils::Snprintf_s(buf, 64,"Friends from DB");
            elm_list_item_append(list, buf, NULL, NULL, on_friends_from_db_item_selected, this);
            break;
        case FEEDS_FROM_DB:
            Utils::Snprintf_s(buf, 64, "Feed from DB");
            elm_list_item_append(list, buf, NULL, NULL, on_feed_from_db_item_selected, this);
            break;
        default:
            Utils::Snprintf_s(buf, 64, "list item %d", i);
            elm_list_item_append(list, buf, NULL, NULL, NULL, NULL);
            break;
        }
    }

    evas_object_show(list);

    elm_object_content_set(panel, list);

    return panel;
}

#if 0
void MainScreen::drawer_back_cb(void *data, Evas_Object *obj, void *event_info)
{
    elm_panel_hidden_set(obj, EINA_TRUE);
}


/**
 * @brief Click drawer button handler: show drawer panel
 * @param[in] data - pointer to drawer panel object
 * @param[in] obj - pointer to button object
 * @param[in] event_info - pointer to event info (depends on the object type )
 */
void MainScreen::on_drawer_button_cb(void *data, Evas_Object *obj, void *event_info)
{
    Evas_Object *panel = static_cast<Evas_Object *>(data);
    if (!elm_object_disabled_get(panel)) {
        elm_panel_toggle(panel);
    }
}
#endif

/**
 * @brief Focus search field handler: move top_part in right, show clear button in case of content
 * @param[in] data - pointer to MainScreen object
 * @param[in] obj - pointer to entry object
 * @param[in] event_info - pointer to event info (depends on the object type )
 */
void MainScreen::searchfield_clicked_cb(void *data, Evas_Object *obj, void *event_info) {
    MainScreen *screen = static_cast<MainScreen *>(data);
    if(screen) {
        SearchScreen* searchScreen = new SearchScreen();
        Application::GetInstance()->AddScreen(searchScreen);
    }
}

void MainScreen::OnResume() {
    if(mFriendsScreen){
        mFriendsScreen->OnResume();
    }
}
