#include "Common.h"
#include "dlog.h"
#include "FacebookSession.h"
#include "GetStartedAddFriends.h"
#include "GetStartedProfilePic.h"
#include "Utils.h"
#include "WebViewScreen.h"


#define BUF_SIZE 100
#define URL_SETTINGS "https://m.facebook.com/settings"
#define URL_MANAGE "https://m.facebook.com/about/privacy"
#define URL_LEARN_MORE "https://m.facebook.com/help/cookies"

GraphRequest *GetStartedProfilePic::mUploadRequest = NULL;

GetStartedProfilePic::GetStartedProfilePic(const char *path):ScreenBase(NULL)
{
    mLayout = elm_layout_add(getParentMainLayout());
    elm_layout_file_set(mLayout, Application::mEdjPath, "get_started_profile_pic");
    evas_object_size_hint_weight_set(mLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_show(mLayout);

    create_entry(mLayout);

    elm_object_signal_callback_add(mLayout, "skip,clicked", "skip_button",
                (Edje_Signal_Cb)skip_btn_cb, this);
    elm_object_signal_callback_add(mLayout, "get_started,clicked", "get_started_btn",
                (Edje_Signal_Cb)get_started_btn_cb, this);

    setProfilePic(path);
}

GetStartedProfilePic::~GetStartedProfilePic()
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "GetStartedProfilePic DESTRUCTOR");
    FacebookSession::GetInstance()->ReleaseGraphRequest(mUploadRequest);
}

void
GetStartedProfilePic :: create_entry(Evas_Object *parent)
{
    Evas_Object *entry = elm_entry_add(parent);

    elm_object_part_text_set(entry, "elm.text",  elm_object_part_text_get(parent, "bottom_info_text"));
    elm_entry_editable_set(entry, EINA_FALSE);
//    elm_entry_cursor_handler_disabled_set(entry,EINA_FALSE);
//    elm_entry_select_allow_set(entry,EINA_FALSE);

    elm_entry_text_style_user_push(entry, "DEFAULT='font_size=15 color=#9298a4 align=center wrap=mixed'");
    evas_object_size_hint_weight_set(entry, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(entry, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_smart_callback_add(entry, "anchor,clicked", anchor_clicked_cb, this);
    elm_object_part_content_set(parent, "info_entry", entry);
    evas_object_show(entry);
}

void
GetStartedProfilePic :: anchor_clicked_cb(void *data, Evas_Object *obj, void *event_info)
{
    char buf[BUF_SIZE];

    Elm_Entry_Anchor_Info *event = (Elm_Entry_Anchor_Info*)event_info;

    if (!strcmp(event->name , "settings")) {
        Utils::Snprintf_s(buf, BUF_SIZE, URL_SETTINGS);
    } else if (!strcmp(event->name , "manage")) {
        Utils::Snprintf_s(buf, BUF_SIZE, URL_MANAGE);
    } else if (!strcmp(event->name , "learn_more")) {
        Utils::Snprintf_s(buf, BUF_SIZE, URL_LEARN_MORE);
    }
    WebViewScreen::Launch(buf);
}

void GetStartedProfilePic :: setProfilePic(const char *path)
{
    Evas_Object *pro_pic = elm_image_add(mLayout);
    elm_image_aspect_fixed_set(pro_pic, EINA_TRUE);
    elm_image_file_set(pro_pic, path, NULL);
    elm_object_part_content_set(mLayout , "pro_pic_photo", pro_pic);
    upload_profile_picture(path);

}

void GetStartedProfilePic :: upload_profile_picture(const char *image_file_path)
{
    bundle* paramsBundle;
    paramsBundle = bundle_create();
    bundle_add_str(paramsBundle, "caption", "This is photo caption");
    mUploadRequest = FacebookSession::GetInstance()->PostProfilePicture(image_file_path , paramsBundle, upload_profile_picture_completed_cb, this);
    bundle_free(paramsBundle);
}

void GetStartedProfilePic::upload_profile_picture_completed_cb(void* object, char* respond, int code)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "on_post_photo_completed");
    GetStartedProfilePic * self = static_cast<GetStartedProfilePic *>(object);

    FacebookSession::GetInstance()->ReleaseGraphRequest(self->mUploadRequest);

    if (code == CURLE_OK) {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, " upload_profile_picture_completed_cb response: %s", respond);

    } else {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Error sending http request");
    }

    if (respond != NULL) {
        //Attention!! client should take care to free respond buffer
        free(respond);
    }
}

void GetStartedProfilePic :: skip_btn_cb(void *data , Evas_Object *obj, const char *emission, const char *source)
{
    GetStartedProfilePic *self = static_cast<GetStartedProfilePic*>(data);
    self->Pop();
}

void GetStartedProfilePic :: get_started_btn_cb(void *data , Evas_Object *obj, const char *emission, const char *source)
{
    ScreenBase *newScreen = (ScreenBase *) new GetStartedAddFriends();
    Application::GetInstance()->AddScreen(newScreen);
    elm_naviframe_item_title_enabled_set(elm_naviframe_top_item_get(newScreen->getNf()), EINA_FALSE, EINA_FALSE);
}
