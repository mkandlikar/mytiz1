#include "Common.h"
#include "Config.h"
#include "curlutilities.h"
#include "dlog.h"
#include "FacebookSession.h"
#include "FbRespondBaseRestApi.h"
#include "FoundItem.h"
#include "GetStartedAddFriends.h"
#include "GraphRequest.h"
#include "Utils.h"

#include <app.h>
#include <app_preference.h>
#include <Eina.h>
#include <openssl/md5.h>
#include <openssl/sha.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>


FrObjAndData::FrObjAndData()
{
    mEData = NULL;
    mEObject = NULL;
}

FrObjAndData::~FrObjAndData()
{
    mEData = NULL;
    mEObject = NULL;
}

#define URL "https://api.facebook.com/method/phonebook.lookup"
#define ACCESS_TOKEN_PARAM "access_token="
#define MAX_REQUEST_LEN 20480
#define ERR(fmt, arg...) dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, fmt,  ##arg)

#define WARN_IF(expr, fmt, arg...) \
{ \
    if (expr) \
    { \
        ERR(fmt, ##arg); \
    } \
}

#define RETM_IF(expr, fmt, arg...) \
{ \
    if (expr) \
    { \
        ERR(fmt, ##arg); \
        return; \
    } \
}


GetStartedAddFriends::GetStartedAddFriends(): ScreenBase(NULL)
{
    mPeople = NULL;
    mAction = new FriendsAction(this);

    mLayout = elm_layout_add(getParentMainLayout());
    elm_layout_file_set(mLayout, Application::mEdjPath, "get_started_add_friends");
    evas_object_size_hint_weight_set(mLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_show(mLayout);

    get_contact_list();
    ExecuteAsync();
    CreateAddFriendsPage(mLayout);

}

GetStartedAddFriends::~GetStartedAddFriends()
{
    delete mAction;
    if (NULL != mPeople)
    {
        void * listData;
        EINA_LIST_FREE(mPeople, listData) {
            free(listData);
        }
    }
}

void GetStartedAddFriends :: get_contact_list()
{
    contacts_list_h list = NULL;
    int error_code;
    int count =0;

    error_code = contacts_connect();
    //dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "contacts_connect ERROR CODE = %d" , error_code);

    error_code = contacts_db_get_all_records(_contacts_contact._uri, 0, 0, &list);
    //dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "contacts_db_get_all_records ERROR CODE = %d" , error_code);

    if(list != NULL){
        contacts_list_get_count(list,&count);
        get_phone_numbers(list);
    }
}

void GetStartedAddFriends :: get_phone_numbers(contacts_list_h associated_contacts)
{
   int error_code;
   contacts_record_h contact;
   if (NULL == associated_contacts)
   {
      dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "associated_contacts is NULL");

      return;
   }
   while (contacts_list_get_current_record_p(associated_contacts, &contact) == CONTACTS_ERROR_NONE)
   {
      int i;
      int count = 0;

      error_code = contacts_record_get_child_record_count(contact, _contacts_contact.number, &count);

      if (CONTACTS_ERROR_NONE != error_code)
      {
         dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "contacts_record_get_child_record_count(%d)", error_code);

         return;
      }
      for (i = 0; i < count; i++)
      {
         contacts_record_h number = NULL;
         local_contacts lc;
         error_code = contacts_record_get_child_record_at_p(contact, _contacts_contact.number, i, &number);
         if (error_code != CONTACTS_ERROR_NONE)
            continue;

         int number_id;
         contacts_record_get_int(number, _contacts_number.id, &number_id);
         lc.number_id = number_id;

         char *number_str[20];
         bzero(number_str,sizeof(number_str));
         contacts_record_get_str_p(number, _contacts_number.number, &number_str[0]);
         bzero(lc.number,sizeof(lc.number));
         eina_strlcpy(lc.number , *number_str, 20);
         //dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "number: %s", lc.number);

         lc_st.push_back(lc);

         if(lc_st.size() >= 100)
             return;
      }
      error_code = contacts_list_next(associated_contacts);
   }
   contacts_list_destroy(associated_contacts, true);
}

Ecore_Thread * GetStartedAddFriends::ExecuteAsync() {
    //dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "ExecuteAsync");

    char requestBuf[MAX_REQUEST_LEN];
    char paramBuf[MAX_REQUEST_LEN];
    std::string entries_ph_num;
    int i=0;
    char tmp_buf[100];

    memset(requestBuf, 0, sizeof(requestBuf));
    memset(paramBuf, 0, sizeof(paramBuf));
    //entries_ph_num.reserve((lc_st.size())*40);

    st_local_contacts::iterator itr;
    for (itr = lc_st.begin(), i = 0; itr != lc_st.end() ; ++itr,i++)
    {
        bzero(tmp_buf, sizeof(tmp_buf));
        Utils::Snprintf_s(tmp_buf, 100, "{'phones':['%s'],'record_id':%d}," , itr->number,i);
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "PHONE NUM = %s", tmp_buf);
        entries_ph_num.append(tmp_buf);
    }

    Utils::Snprintf_s(paramBuf, MAX_REQUEST_LEN, "&not_for_sync=false&country_code=IN&include_non_fb=true&omit_invited=true&multi_creds=true&silhouettes=true&entries=[%s]",entries_ph_num.c_str());
    Utils::Snprintf_s(requestBuf, MAX_REQUEST_LEN, "%s%s%s", ACCESS_TOKEN_PARAM,  Config::GetInstance().GetAccessToken().c_str(), paramBuf);

    //dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Full Request Url =%s\n", requestBuf);

    RequestUserData *ud = new RequestUserData(URL, requestBuf, response_cb, this);

    //Run a thread
    Ecore_Thread * thread = ecore_thread_run(thread_start_cb, thread_end_cb, thread_cancel_cb, ud);
    return thread;
}


void GetStartedAddFriends::thread_start_cb(void *data, Ecore_Thread *thread) {
    RequestUserData *ud = static_cast<RequestUserData *>(data);
    CURLcode code;

    code = SendHttpPost(ud);
    ud->curlCode = code;
}

void GetStartedAddFriends::thread_end_cb(void *data, Ecore_Thread *thread) {
    RequestUserData *ud = static_cast<RequestUserData *>(data);

    int curlCode = ud->curlCode;
    char * respond = ud->mResponseBuffer->memory;
    void * object = ud->callbackObject;
    RequestUserData_CB callback = ud->callback;

    delete ud;

    if (callback != NULL) {
        callback(object, respond, curlCode);
    }
}

void GetStartedAddFriends::thread_cancel_cb(void *data, Ecore_Thread *thread) {
    //TODO:
}

void GetStartedAddFriends::response_cb(void* object, char* response, int code){
    GetStartedAddFriends *self = (GetStartedAddFriends*)object;
    if(response != NULL){
        self->xml_parser(response);
        self->SetAddFriendsData();
    }
}

void GetStartedAddFriends :: xml_parser(char *response)
{
    xmlInitParser();
    xmlNodePtr cur;
    xmlDocPtr doc = xmlParseDoc((xmlChar *)response);

    cur = xmlDocGetRootElement(doc);
    if((!xmlStrcmp(cur->name, (const xmlChar *)"phonebook_lookup_response"))){
        for(cur = cur->xmlChildrenNode; cur != NULL; cur = cur->next)
        {
            if ((cur->type == XML_ELEMENT_NODE) && (!xmlStrcmp(cur->name, (const xmlChar *)"contact_info_user_map"))){
                parseContact (doc, cur);
            }
        }
    }
    xmlFreeDoc(doc);
    xmlCleanupParser();

}

void GetStartedAddFriends :: parseContact (xmlDocPtr doc, xmlNodePtr cur)
{
    xmlChar *key;
    struct_addFriends st_addFrnds;
    for(cur = cur->xmlChildrenNode; cur != NULL; cur = cur->next){
        if ((!xmlStrcmp(cur->name, (const xmlChar *)"uid"))) {
            key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
            if(key == NULL)
                return;
				
			int size = strlen((char*)key) + 1;
            st_addFrnds.uId = new char[size];
            eina_strlcpy(st_addFrnds.uId,(char *)key, size);
            xmlFree(key);
        }
        if ((!xmlStrcmp(cur->name, (const xmlChar *)"is_friend"))) {
            key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
            st_addFrnds.isFriends = atoi((char*)key);
            xmlFree(key);
            if(st_addFrnds.isFriends == 1)
                            return;
        }
        if ((!xmlStrcmp(cur->name, (const xmlChar *)"pic_square_with_logo"))) {
            key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			int size = strlen((char*)key) + 1;
            st_addFrnds.photo = new char[size];
            eina_strlcpy(st_addFrnds.photo,(char *)key, size);
            xmlFree(key);
        }
        if ((!xmlStrcmp(cur->name, (const xmlChar *)"name"))) {
            key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			int size = strlen((char*)key) + 1;
            st_addFrnds.name = new char[size];
            eina_strlcpy(st_addFrnds.name,(char *)key, size);
            xmlFree(key);
        }
    }
    UserFriends* friendEl = new UserFriends(st_addFrnds);
    mPeople = eina_list_append(mPeople, friendEl);
}

Evas_Object *GetStartedAddFriends :: CreateAddFriendsPage(Evas_Object *parent)
{
    Evas_Object *box = elm_box_add(parent);
    evas_object_size_hint_weight_set(box, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(box, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_object_part_content_set(parent, "elm.swallow.content", box);

    mScroller = CreateScroller(parent);
    elm_box_pack_end(box, mScroller);

    mNestedBoxContainer = elm_box_add(mScroller);
    elm_box_horizontal_set(mNestedBoxContainer, EINA_TRUE);
    evas_object_size_hint_weight_set(mNestedBoxContainer, 1, 1);
    evas_object_size_hint_align_set(mNestedBoxContainer, -1, 0);
    elm_object_content_set(mScroller, mNestedBoxContainer);
    evas_object_show(mNestedBoxContainer);


    mRequestLayout = elm_layout_add(mNestedBoxContainer);
    evas_object_size_hint_weight_set(mRequestLayout, 1, 1);
    evas_object_size_hint_align_set(mRequestLayout, 0, 0);
    elm_layout_file_set(mRequestLayout,  Application::mEdjPath, "add_friends_page");

    mRequestList = elm_genlist_add(mRequestLayout);
    elm_object_part_content_set(mRequestLayout, "rq_list", mRequestList);
    elm_genlist_select_mode_set(mRequestList, ELM_OBJECT_SELECT_MODE_NONE);
    elm_scroller_single_direction_set(mRequestList, ELM_SCROLLER_SINGLE_DIRECTION_HARD);
    evas_object_show(mRequestList);

    elm_object_translatable_part_text_set(mRequestLayout, "rq_text", "CONTACTS");

    elm_box_pack_end(mNestedBoxContainer, mRequestLayout);
    evas_object_show(mRequestLayout);

    return mRequestLayout;
}

Evas_Object *GetStartedAddFriends :: CreateScroller(Evas_Object *parent)
{
    Evas_Object *scroller = elm_scroller_add(parent);
    elm_scroller_policy_set(scroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_OFF);
    elm_scroller_page_size_set(scroller, 480, 0);
    elm_scroller_page_scroll_limit_set(scroller, 1, 0);
    elm_scroller_single_direction_set(scroller, ELM_SCROLLER_SINGLE_DIRECTION_HARD);
    evas_object_size_hint_weight_set(scroller, 1, 1);
    evas_object_size_hint_align_set(scroller, -1, -1);
    //evas_object_smart_callback_add(scroller, "scroll", scroll_cb, this);
    //evas_object_smart_callback_add(scroller, "scroll,anim,stop", anim_stop_scroll_cb, this);
    evas_object_show(scroller);

    return scroller;
}


void GetStartedAddFriends::SetAddFriendsData()
{
    Elm_Genlist_Item_Class *itc2 = elm_genlist_item_class_new();
    itc2->item_style = "full";
    itc2->homogeneous = EINA_FALSE;
    itc2->func.content_get = gl_ppl_u_may_know_get;
    itc2->func.state_get = NULL;
    itc2->func.del = on_item_delete_cb;

    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(mPeople, l, listData)
    {

        ActionAndData *data = NULL;
        GetStartedAddFriends::UserFriends *friend_data = (GetStartedAddFriends::UserFriends *) listData;
        data = new ActionAndData(friend_data, mAction);
        elm_genlist_item_append(mRequestList, itc2, data, NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);
    }
    elm_genlist_item_class_free(itc2);
}

Evas_Object *GetStartedAddFriends::gl_ppl_u_may_know_get(void *user_data, Evas_Object *obj, const char *part)
{
    ActionAndData *action_data = (ActionAndData *) user_data;

    Evas_Object *mFrPplComposer = elm_layout_add(obj);
    evas_object_size_hint_weight_set(mFrPplComposer, 1, 1);
    evas_object_size_hint_min_set(mFrPplComposer, 481, 144);
    evas_object_size_hint_max_set(mFrPplComposer, 481, 144);
    elm_layout_file_set(mFrPplComposer,  Application::mEdjPath, "add_friends_composer");
    evas_object_show(mFrPplComposer);

    FrObjAndData *e_data = new FrObjAndData;
    e_data->mEData = action_data;
    e_data->mEObject = mFrPplComposer;

    Evas_Object *avatar = elm_image_add(mFrPplComposer);
    elm_object_part_content_set(mFrPplComposer, "item_avatar", avatar);
    action_data->UpdateImageLayoutAsync(((GetStartedAddFriends::UserFriends *) action_data->mData)->mPicSquareWithLogo, avatar, ActionAndData::EImage, Post::EFromPicturePath);
    evas_object_show(avatar);

    elm_object_part_text_set(mFrPplComposer, "item_user_name", ((GetStartedAddFriends::UserFriends *) action_data->mData)->mName);
    elm_object_translatable_part_text_set(mFrPplComposer, "add_btn_text", "IDS_ADD_FRIEND");
    elm_object_signal_callback_add(mFrPplComposer, "add.clicked", "add_btn*", (Edje_Signal_Cb) on_add_friend_button_clicked_cb, e_data);

    elm_object_translatable_part_text_set(mFrPplComposer, "cancel_btn_text", "IDS_CANCEL");
    elm_object_signal_callback_add(mFrPplComposer, "cancel.clicked", "cancel_btn*", (Edje_Signal_Cb) on_cancel_friend_button_clicked_cb, e_data);

    return mFrPplComposer;
}

void GetStartedAddFriends::on_add_friend_button_clicked_cb(void *data, Evas *e, Evas_Object *obj, const char *emisson, void *source)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Friend added");
    FrObjAndData * screen = (FrObjAndData*) data;
    FacebookSession::GetInstance()->SendFriendRequest(((FoundItem *) screen->mEData->mData)->GetId(), on_add_friend_completed, screen);
}

void GetStartedAddFriends::on_add_friend_completed(void* object, char* respond, int code)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "on_add_friend_completed response: %s", respond);

    FrObjAndData * screen = static_cast<FrObjAndData *>(object);

    //GetStartedAddFriends * me = (GetStartedAddFriends *) screen->mEData->mAction->getScreen();
    if (code == CURLE_OK)
    {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "response: %s", respond);
        FbRespondBase * addRespond = FbRespondBase::createFromJson(respond);

        if (addRespond == NULL)
        {
            dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Error parsing http respond");
        }
        else
        {
            if (addRespond->mError != NULL)
            {
                dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Error sending confirm/delete, error = %s", addRespond->mError);
            }
            else if (addRespond->mSuccess)
            {
                elm_object_translatable_part_text_set(screen->mEObject, "request_sent_text", "IDS_REQUEST_SENT");
                edje_object_signal_emit(elm_layout_edje_get(screen->mEObject), "hide.set", "add");
                edje_object_signal_emit(elm_layout_edje_get(screen->mEObject), "show.set", "request_sent_text");
            }
        }
        delete addRespond;
    }
    else
    {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Error sending http request");
    }
    if (respond != NULL)
    {
        free(respond);
    }
}

void GetStartedAddFriends::on_cancel_friend_button_clicked_cb(void *data, Evas *e, Evas_Object *obj, const char *emisson, void *source)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Cancel rq");
    FrObjAndData * screen = (FrObjAndData*) data;
    FacebookSession::GetInstance()->CancelOutgoingFriendRequest(((FoundItem *) screen->mEData->mData)->GetId(), on_cancel_friend_completed, screen);
}

void GetStartedAddFriends::on_cancel_friend_completed(void* object, char* respond, int code)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "on_cancel_friend_completed");

    FrObjAndData * screen = static_cast<FrObjAndData *>(object);

    //GetStartedAddFriends * me = (GetStartedAddFriends *) screen->mEData->mAction->getScreen();

    if (code == CURLE_OK)
    {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "response: %s", respond);
        FbRespondBaseRestApi * cancelRespond = FbRespondBaseRestApi::createFromJson(respond);

        if (cancelRespond->mErrorMessage)
        {
            dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Error sending confirm/delete, error = %s", cancelRespond->mErrorMessage);
        }
        else
        {
            edje_object_signal_emit(elm_layout_edje_get(screen->mEObject), "hide.cancel.set", "");
            edje_object_signal_emit(elm_layout_edje_get(screen->mEObject), "hide.set", "request_sent_text");
        }
    delete cancelRespond;
    }
    else
    {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Error sending http request");
    }
    if (respond != NULL)
    {
        free(respond);
    }
}

void GetStartedAddFriends::on_item_delete_cb(void *data, Evas_Object *obj)
{
    if(data) {
        delete (ActionAndData *) data;
    }
}


/* GetStartedAddFriends::UserFriends CLASS */

GetStartedAddFriends::UserFriends::UserFriends(struct_addFriends st_addFrnds)
{
    mIsFriends = st_addFrnds.isFriends;
    mId = st_addFrnds.uId;
    mName = st_addFrnds.name;
    mUserName = NULL;
    mPicSquareWithLogo = st_addFrnds.photo;
}

GetStartedAddFriends::UserFriends::~UserFriends()
{
    free((void*) mId);
    free((void*) mName);
    free((void*) mUserName);
    free((void*) mPicSquareWithLogo);
}
