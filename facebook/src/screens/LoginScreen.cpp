#ifdef _DEBUG
    #define FEATURE_EASY_LOGIN
#endif

//Custom headers
#include "AppEvents.h"
#include "Login2FacAuthScreen.h"
#include "CacheManager.h"
#include "Common.h"
#include "Config.h"
#include "ConnectivityManager.h"
#include "CSmartPtr.h"
#include "InitializationDataUploader.h"
#include "jsonutilities.h"
#include "Log.h"
#include "LoginRequest.h"
#include "LoginScreen.h"
#include "MainScreen.h"
#include "Popup.h"
#include "ScreenBase.h"
#include "SignUpInProgressScreen.h"
#include "SignUpWelcomeScreen.h"
#include "Utils.h"
#include "WebViewScreen.h"
#include "WidgetFactory.h"

//System headers
#include <account.h>
#include <algorithm>
#include <badge.h>
#include <cstdlib>
#include <efl_extension.h>
#ifdef FEATURE_EASY_LOGIN
    #include <map>
#endif
#include <regex>
#include <string>

#define URL_SIGNUP "https://m.facebook.com/reg"
#define URL_FORGOT_PSWD "https://m.facebook.com/recover/initiate"
#define URL_HELP_CENTER "https://m.facebook.com/help"
#define URL_HELP_CENTER_ACCOUNT_DISABLED "https://www.facebook.com/help/245058342280723"
#define EMAIL "Email"
#define PHONE_NO "Phone Number"
#define EMAIL_ADDRESS "email address"

#define MAX_FIELD_TEXT_LEN 512

#ifdef FEATURE_EASY_LOGIN
/* Up to 10 easy logins consisting of one decimal digit: */
static std::map<long int, std::pair<std::string, std::string>> EasyLoginCredentials({
    std::pair<long int, std::pair<std::string, std::string>>(1, {"tizen.fb.test1@mail.ru", "facebooktest1"}),
    std::pair<long int, std::pair<std::string, std::string>>(2, {"tizen.fb.test2@mail.ru", "facebooktest2"}),
    std::pair<long int, std::pair<std::string, std::string>>(3, {"tizen.fb.test3@mail.ru", "testfacebook3"}),
    std::pair<long int, std::pair<std::string, std::string>>(4, {"tizen.fb.testk1@mail.ru", "facebooktestk1"}),
    std::pair<long int, std::pair<std::string, std::string>>(5, {"tizen.fb.testk2@mail.ru", "facebooktestk2"}),
    std::pair<long int, std::pair<std::string, std::string>>(6, {"xrsniukeqk_1493044203@tfbnw.net", "testfacebook6"})
});
#endif

LoginScreen::LoginScreen(ScreenBase *parentScreen):ScreenBase(parentScreen)  {
    mScreenInProgress = NULL;
    mLoginRequest = NULL;
    mInitDataUploader = NULL;
    mConfirmationDialog = nullptr;
    AppEvents::Get().Subscribe(eINTERNET_CONNECTION_CHANGED, this);
    mFirstFactor = nullptr;
    mPswdAcc = nullptr;
    mUserName = nullptr;
    CreateView();
}

LoginScreen::~LoginScreen() {
    delete mInitDataUploader;
    delete mLoginRequest;
    evas_object_smart_callback_del(Application::GetInstance()->mConform, "virtualkeypad,state,on", on_screen_keyb_on_cb);
    evas_object_smart_callback_del(Application::GetInstance()->mConform, "virtualkeypad,state,off", on_screen_keyb_off_cb);
    DeleteConfirmationDialog();
    free((char *) mFirstFactor);
    free((char *) mPswdAcc);
    free((char *) mUserName);
}

void LoginScreen::Push() {
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void LoginScreen::LoadEdjLayout() {
    mLayout = elm_layout_add(getParentMainLayout());
    elm_layout_file_set(mLayout, Application::mEdjPath, "login_screen");
    evas_object_size_hint_weight_set(mLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_show(mLayout);
    evas_object_smart_callback_add(Application::GetInstance()->mConform, "virtualkeypad,state,on", on_screen_keyb_on_cb, this);
    evas_object_smart_callback_add(Application::GetInstance()->mConform, "virtualkeypad,state,off", on_screen_keyb_off_cb, this);
}

void LoginScreen::CreateView() {
    LoadEdjLayout();

    mEvasUidInput = CreateInput(ELM_INPUT_PANEL_LAYOUT_EMAIL, "IDS_LOGIN_UID");
    elm_object_part_content_set(mLayout, "login_uid_input", mEvasUidInput);

    mEvasPswdInput = CreateInput(ELM_INPUT_PANEL_LAYOUT_PASSWORD, "IDS_PASSWORD");
    elm_object_part_content_set(mLayout, "login_pswd_input", mEvasPswdInput);

    elm_object_translatable_part_text_set(mLayout, "login_button", "IDS_LOGIN");
    elm_object_signal_emit(mLayout, "disable_login_button", "login_button");
    elm_object_signal_emit(mLayout, "button.hide", "login_button_bg");

    elm_object_translatable_part_text_set(mLayout, "login_signup_link", "IDS_SIGNUP");
    elm_object_signal_callback_add(mLayout, "got.a.login.signup.click", "login_signup_link", SignUpBtnCb, this);
    elm_object_translatable_part_text_set(mLayout, "login_needhelp_link", "IDS_LOGIN_HELP");
    elm_object_signal_callback_add(mLayout, "got.a.login.needhelp.click", "login_needhelp_link", NeedHelpBtnCb, this);

    elm_object_focus_next_object_set(mEvasUidInput, mEvasPswdInput, ELM_FOCUS_NEXT);
}

Evas_Object* LoginScreen::CreateInput(Elm_Input_Panel_Layout  EntryType, const char *PlaceHolderTxt)
{
    Evas_Object *Input = elm_entry_add(mLayout);
    elm_entry_scrollable_set(Input, EINA_TRUE);
    elm_entry_single_line_set(Input, EINA_TRUE);
    elm_entry_input_panel_layout_set(Input, EntryType);
    if (ELM_INPUT_PANEL_LAYOUT_PASSWORD == EntryType) {
        elm_entry_password_set(Input, EINA_TRUE);
        elm_entry_input_panel_return_key_type_set(Input, ELM_INPUT_PANEL_RETURN_KEY_TYPE_DONE);
        elm_entry_input_panel_return_key_disabled_set(Input, EINA_TRUE);
        evas_object_smart_callback_add(Input, "activated", LoginBtnCb, this);
        evas_object_smart_callback_add(Input, "changed", entry_changed_cb, this);
    } else {
        elm_entry_prediction_allow_set(Input, EINA_FALSE);
        elm_entry_input_panel_return_key_type_set(Input, ELM_INPUT_PANEL_RETURN_KEY_TYPE_NEXT);
        evas_object_smart_callback_add(Input, "activated", entry_activated_cb, this);
        evas_object_smart_callback_add(Input, "cursor,changed", entry_changed_cb, this);
    }

    FRMTD_TRNSLTD_PART_TXT_SET1(Input, "elm.guide", "<font_size=40>%s</font_size>", PlaceHolderTxt);
    elm_entry_text_style_user_push(Input, "DEFAULT='color=#FFF font_size=40'");
    evas_object_size_hint_align_set(Input, EVAS_HINT_FILL, 0.0);
    evas_object_size_hint_weight_set(Input, EVAS_HINT_EXPAND, 0.0);
    elm_object_focus_set(Input, EINA_FALSE);

    elm_entry_cnp_mode_set(Input, ELM_CNP_MODE_PLAINTEXT);

    return Input;
}

void LoginScreen::LoginBtnCb(void *data, Evas_Object *obj, void *EventInfo) {
    LoginBtnCb(data, obj, nullptr, nullptr);
}

void LoginScreen::LoginBtnCb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    LoginScreen *self = static_cast<LoginScreen*>(data);
    char *userName = elm_entry_markup_to_utf8(elm_entry_entry_get(self->mEvasUidInput));
    const char* pswd = elm_entry_entry_get(self->mEvasPswdInput);
    if ((userName && (strlen(userName) > MAX_FIELD_TEXT_LEN)) ||
        (pswd && (strlen(pswd) > MAX_FIELD_TEXT_LEN))) {
        //Prevent buffer overflow in case of pasting too long text in credential fields.
        notification_status_message_post(i18n_get_text("IDS_MAX_CHARACTERS_REACHED"));
        free(userName);
        return;
    }
#ifdef FEATURE_EASY_LOGIN
    if (userName && (strlen(userName) == 1) && (!pswd || (*pswd == '\0'))) {
        auto credentials = EasyLoginCredentials.find(strtol(userName, nullptr, 10));
        if (credentials != EasyLoginCredentials.end()) {
            Log::debug_tag("Facebook::LoginScreen", "LoginScreen::LoginBtnCb easy login to user #%d", credentials->first);
            free(userName);
            userName = strdup(credentials->second.first.c_str());
            pswd = credentials->second.second.c_str();
        }
    } else
#endif
    if (userName && Utils::IsPhoneNumber(userName)) {
        char *cleanNumber = Utils::CleanPhoneNumber(userName);
        free(userName);
        userName = cleanNumber;
    }

    if (!ConnectivityManager::Singleton().IsConnected()) {
        WidgetFactory::ShowAlertPopup("IDS_LOGIN_CONNECT_ERR", "IDS_LOGIN_FAILED_TITLE");
    } else {
        self->mScreenInProgress = new SignUpInProgressScreen(nullptr);
        self->mScreenInProgress->mInProgress = true;
        self->mScreenInProgress->mRequestType = LoginReq;
        Application::GetInstance()->AddScreen(self->mScreenInProgress);

        self->mPswdAcc = SAFE_STRDUP(pswd);
        self->mUserName = SAFE_STRDUP(userName);

        self->mLoginRequest = new LoginRequest(OnLoginCompleted, self, userName, pswd);
        self->mScreenInProgress->mGraphRequest = self->mLoginRequest;
        self->mLoginRequest->DoLogin();
    }
    free(userName);
}

void LoginScreen::SignUpBtnCb(void *data, Evas_Object *obj, const char *emission, const char *source) {
    WebViewScreen::Launch(URL_SIGNUP);

    /*
     * As of now, native app signup is not supported.
     *
        ScreenBase *NewScreen = new SignUpWelcomeScreen(NULL);
        Application::GetInstance()->AddScreen(NewScreen);
        elm_naviframe_item_title_enabled_set(elm_naviframe_top_item_get(NewScreen->getNf()), EINA_FALSE, EINA_FALSE);
     */
}

void LoginScreen::NeedHelpBtnCb(void *data, Evas_Object *obj, const char *emission, const char *source) {
    static PopupMenu::PopupMenuItem NeedHelpPopUp[] = {
        { NULL, "IDS_FORGOT_PSWD", NULL,
          NeedHelpPopupCb, (void*)URL_FORGOT_PSWD,
          0, false, false },
        { NULL, "IDS_HELP_CENTER", NULL,
          NeedHelpPopupCb, (void*)URL_HELP_CENTER,
          1, true, false }
    };
    int numItems = sizeof(NeedHelpPopUp) / sizeof(NeedHelpPopUp[0]);

    LoginScreen *Me = static_cast<LoginScreen*>(data);
    Evas_Coord h = 0;
    evas_object_geometry_get(obj, NULL, NULL, NULL, &h);
    PopupMenu::Show(numItems, NeedHelpPopUp, Me->getNf(), false, h/2);
}

void LoginScreen::on_screen_keyb_on_cb(void *Data, Evas_Object *Obj, void *EventInfo) {
    LoginScreen *Self = static_cast<LoginScreen *>(Data);
    elm_object_signal_emit(Self->mLayout, "toppad,state,inputon", "");
    elm_object_signal_emit(Self->mLayout, "link_need_help.hide", "");
}

void LoginScreen::on_screen_keyb_off_cb(void *Data, Evas_Object *Obj, void *EventInfo) {
    LoginScreen *Self = static_cast<LoginScreen *>(Data);
    elm_object_signal_emit(Self->mLayout, "toppad,state,default", "");
    elm_object_signal_emit(Self->mLayout, "link_need_help.show", "");
}

void LoginScreen::OnLoginCompleted(void *object, char *response, int code)
{
    Log::info(LOG_FACEBOOK_USER, "LoginScreen::OnLoginCompleted");

    LoginScreen *self = static_cast<LoginScreen *>(object);
    self->mScreenInProgress->mInProgress = false;

    //Check curl code. If it is not valid, display a connectivity error popup & go back to Login Screen.
    if (CURLE_OK != code) {
        self->Pop();
        self->DisplayCurlErrorMsg(code);
        free(response);
        return;
    }

    JsonObject * rootObject;
    JsonParser * parser = openJsonParser(response, &rootObject);
    const char* accessToken = json_object_get_string_member(rootObject, "access_token");
    if (accessToken == NULL) {
        Log::debug("LoginScreen::Error in Login %s", response);

        const char *errorMsg = json_object_get_string_member(rootObject, "error_msg");
        const char *errorData = json_object_get_string_member(rootObject, "error_data");
        self->Pop();

        if (errorMsg) {
            elm_entry_entry_set(self->mEvasPswdInput, "");

            std::string Error = errorMsg;
            std::string ErrMsgTitle;
            std::string ErrMsg;
            if (std::string::npos != Error.find ("(400)")) {
                ErrMsgTitle = i18n_get_text("IDS_INCORRECT_EMAIL");
                ErrMsg = i18n_get_text("IDS_LOGIN_EMAIL_ERR_MSG");
                if (errorData) {
                    Error = errorData;
                    if (std::string::npos != Error.find(PHONE_NO)) {
                        std::string Email = EMAIL;
                        std::string PhoneNo = PHONE_NO;
                        std::transform(Email.begin(), Email.end(), Email.begin(), ::tolower);
                        std::transform(PhoneNo.begin(), PhoneNo.end(), PhoneNo.begin(), ::tolower);
                        if (std::string::npos != ErrMsgTitle.find(EMAIL_ADDRESS)) {
                            ErrMsgTitle = Utils::ReplaceString(ErrMsgTitle, EMAIL_ADDRESS, PHONE_NO);
                        } else {
                            ErrMsgTitle = Utils::ReplaceString(ErrMsgTitle, EMAIL, PHONE_NO);
                        }
                        ErrMsg = Utils::ReplaceString(ErrMsg, EMAIL_ADDRESS, Email);
                        ErrMsg = Utils::ReplaceString(ErrMsg, Email, PhoneNo);
                    }
                }
            } else if (std::string::npos != Error.find ("(401)")) {
                ErrMsgTitle = "IDS_INCORRECT_PSWD";
                ErrMsg = "IDS_LOGIN_PSWD_ERR_MSG";
            } else if (std::string::npos != Error.find ("(407)")) {
                ErrMsgTitle = "IDS_LOGIN_ACCOUNT_CONFIRMATION_CAPTION";
                ErrMsg = "IDS_LOGIN_ACCOUNT_CONFIRMATION_MESSAGE";
            } else if (std::string::npos != Error.find ("(613)")) {
                ErrMsgTitle = "IDS_TRY_AGAIN_LATER";
                ErrMsg = "IDS_LOGIN_TRYING_TOO_OFTEN";
            } else {
                ErrMsg = errorMsg;
            }
            if (std::string::npos != Error.find("(10)")) {
                self->ShowAccountDisabledPopup();
            } else if (std::string::npos != Error.find("(406)")) {
                self->ParseFirstFactorFromResponse(errorData);
                ScreenBase *login2FacScreen = new Login2FacAuthScreen(NULL, self->mUserName, self->mPswdAcc, self->mFirstFactor);
                Application::GetInstance()->AddScreen(login2FacScreen);
            } else {
                self->ShowAlertPopup(ErrMsg.c_str(), ErrMsgTitle.empty() ? nullptr : ErrMsgTitle.c_str());
            }
        } else {
            // Handle unclassified errors caused by logging in.
            JsonObject *error = json_object_get_object_member(rootObject, "error");
            if (error) {
                elm_entry_entry_set(self->mEvasPswdInput, "");

                int errorCode = json_object_get_int_member(error, "code");
                int errorSubcode = json_object_get_int_member(error, "error_subcode");
                if (errorCode == 1 && errorSubcode == 99) {
                    self->ShowAlertPopup("IDS_LOGIN_UNKNOWN_ERROR", "IDS_LOGIN_FAILED_TITLE");
                }
            }
        }
    } else {

        // Create Application Badge to display on Application Icon in the Launch Viewer
        int badgeRet = badge_add(PACKAGE);
        Log::debug("LoginScreen::OnLoginCompleted()::badge_add() is %s. Return Value is %d", (BADGE_ERROR_NONE == badgeRet) ? "Success" : "Failure", badgeRet);

        Config::GetInstance().SetAccessToken(accessToken);
        Log::debug("Access Token = %s", Config::GetInstance().GetAccessToken().c_str());

//    setLoginState(true) will be done later in provider_sequence_init_done_cb()
//        Application::GetInstance()->mDataService->setLoginState(true);

        Config::GetInstance().SetUserId(json_object_get_string_member(rootObject, "uid"));
        Log::debug("User Id = %s", Config::GetInstance().GetUserId().c_str());
        loggedin_user_data prevUserData;
        prevUserData.rowCount = 0;
        prevUserData.prevUserId = NULL;
        CacheManager::GetInstance().SelectLoggedInUser(&prevUserData);
        //Insert the Current User ID in to the LoggedInUser Table, if the table is empty.
        if (0 == prevUserData.rowCount) {
            CacheManager::GetInstance().SetLoggedInUserData(Config::GetInstance().GetUserId().c_str());
        } else {
            // compare the current user id & prev user id. If they are different delete the Notifications Data of the previous user.
            std::string currentUserId(Config::GetInstance().GetUserId());
            if (currentUserId != prevUserData.prevUserId)
            {
                Log::debug("Previous & Current Logged In user IDs are different. Deleting Notifications Data of the previous User.");
                CacheManager::GetInstance().DeleteDatabase();
                CacheManager::GetInstance().Init();
                CacheManager::GetInstance().SetLoggedInUserData(currentUserId.c_str());
            }
            free((char *)prevUserData.prevUserId);
        }

        Config::GetInstance().SetSessionKey(json_object_get_string_member(rootObject, "session_key"));
        Log::debug("Session Key = %s", Config::GetInstance().GetSessionKey().c_str());

        Config::GetInstance().SetSessionSecret(json_object_get_string_member(rootObject, "secret"));
        Log::debug("Session Secret = %s", Config::GetInstance().GetSessionSecret().c_str());
        const char* user_name = elm_entry_entry_get(self->mEvasUidInput);
        Config::GetInstance().SetUserName(user_name);
        /* Add this account details to Tizen Account Settings*/
        account_h account = NULL;
        int account_id = 0;
        int ret = account_create(&account);
        Utils::AcctFuncRetValLog("account_create", ret);
        if (ACCOUNT_ERROR_NONE == ret) {
            /* set User Name */
            ret = account_set_user_name(account, user_name);
            Utils::AcctFuncRetValLog("account_set_user_name", ret);
            /* set Display Name */
            ret = account_set_display_name(account, user_name);
            Utils::AcctFuncRetValLog("account_set_display_name", ret);

            std::string accountIconPath(app_get_shared_resource_path());
            accountIconPath += "fb4t_icon.png";
            ret = account_set_icon_path(account, accountIconPath.c_str());
            Utils::AcctFuncRetValLog("account_set_icon_path", ret);

            /* Insert Account into DB */
            ret = account_insert_to_db(account, &account_id);
            Utils::AcctFuncRetValLog("account_insert_to_db", ret);
            /* save in the Data Base -> Accounts table */
            bool result = CacheManager::GetInstance().UpdateAccountData();
            Log::debug("UpdateAccountData() - %s", (result ? "Success" : "Failed"));

            ret = account_destroy(account);
            Utils::AcctFuncRetValLog("account_destroy", ret);
        }

        LoginScreen *self = static_cast<LoginScreen*>(object);
        self->StartDownloadInitData();

        AppEvents::Get().Notify(eACCOUNT_CHANGED, &account_id);
    }
    g_object_unref(parser);
    free(response);
}

void LoginScreen::provider_sequence_init_done_cb()
{
    Log::debug("LoginScreen::provider_sequence_init_done_cb" );
    AppEvents::Get().Notify(eDATA_IS_READY);
    //OLD CODE FOR CHECK PURPOSE
    Application::GetInstance()->mNf = elm_naviframe_add(Application::GetInstance()->mConform);
    elm_object_content_set(Application::GetInstance()->mConform, Application::GetInstance()->mNf);

    Application::GetInstance()->RemoveAllScreensAndNfItemsFromNaviframe();
    ScreenBase *scr = (ScreenBase *) new MainScreen(NULL);
    Application::GetInstance()->AddScreen(scr);
    
    Application::GetInstance()->mDataService->setLoginState(true);
    Application::GetInstance()->HandleSuspendedAppControl();
}

void LoginScreen::entry_activated_cb(void* Data, Evas_Object* Obj, void* EventInfo) {
    LoginScreen *Me = static_cast<LoginScreen *>(Data);
    elm_object_focus_next(Me->mEvasUidInput, ELM_FOCUS_NEXT);
    elm_entry_input_panel_return_key_disabled_set(Me->mEvasPswdInput,
            elm_entry_is_empty(Me->mEvasUidInput) ||
            elm_entry_is_empty(Me->mEvasPswdInput));
    elm_entry_input_panel_show(Me->mEvasPswdInput);
}

void LoginScreen::entry_changed_cb(void* Data, Evas_Object* Obj, void* EventInfo) {
    LoginScreen *Me = static_cast<LoginScreen *>(Data);
    elm_entry_input_panel_return_key_disabled_set(Me->mEvasPswdInput,
            elm_entry_is_empty(Me->mEvasUidInput) ||
            elm_entry_is_empty(Me->mEvasPswdInput));

    Me->UpdateLoginButton();
}

void LoginScreen::UpdateLoginButton() {
    bool isEntriesEmpty = elm_entry_is_empty(mEvasUidInput) ||
                          elm_entry_is_empty(mEvasPswdInput);
#ifdef FEATURE_EASY_LOGIN
    if (!elm_entry_is_empty(mEvasUidInput) && elm_entry_is_empty(mEvasPswdInput)) {
        c_unique_ptr<char> userId(elm_entry_markup_to_utf8(elm_entry_entry_get(mEvasUidInput)));
        if (userId && (strlen(userId.get()) == 1) &&
            EasyLoginCredentials.find(strtol(userId.get(), nullptr, 10)) != EasyLoginCredentials.end()) {
            isEntriesEmpty = false;
        }
    }
#endif
    if (isEntriesEmpty) {
        EnableLoginButton(false);
        elm_object_signal_emit(mLayout, "disable_login_button", "login_button");
        elm_object_signal_emit(mLayout, "button.hide", "login_button_bg");
    } else {
        EnableLoginButton(true);
        elm_object_signal_emit(mLayout, "enable_login_button", "login_button");
        elm_object_signal_emit(mLayout, "button.show", "login_button_bg");
    }
}

void LoginScreen::EnableLoginButton(bool enable) {
    if (enable && !mIsRightButtonEnabled) {
        elm_object_signal_callback_add(mLayout, "got.a.login.button.click", "login_button*", LoginBtnCb, this);
        mIsRightButtonEnabled = true;
    } else if (!enable && mIsRightButtonEnabled){
        mIsRightButtonEnabled = false;
        elm_object_signal_callback_del(mLayout, "got.a.login.button.click", "login_button*", LoginBtnCb);
    }
}

bool LoginScreen::HandleBackButton()
{
    if (WidgetFactory::CloseAlertPopup()) {
    } else if (PopupMenu::Close()) {
    } else if (DeleteConfirmationDialog()) {
    } else {
        Application::GetInstance()->GoToBackground();
    }
    return true;
}

void LoginScreen::StartDownloadInitData()
{
    if ( !mInitDataUploader ) {
        // Need to request the main data before we start to use MainScreen
        mInitDataUploader = new InitializationDataUploader( provider_sequence_init_done_cb );
    }
    mInitDataUploader->StartUploading();
}

void LoginScreen::NeedHelpPopupCb(void *Data, Evas_Object *Obj, void *EventInfo) {
    WebViewScreen::Launch(static_cast<char*>(Data));
}

void LoginScreen::DisplayCurlErrorMsg(int CurlCode) {
    switch(CurlCode) {
    case CURLE_SSL_CACERT:
        ShowAlertPopup("IDS_CURL_ERRORMSG_CACERT", NULL);
        break;
    default:
        ShowAlertPopup("IDS_LOGIN_CONNECT_ERR", NULL);
        break;
    }

}

void LoginScreen::Update(AppEventId EventId, void* Data) {
    switch(EventId) {
    case eINTERNET_CONNECTION_CHANGED:
        if (!ConnectivityManager::Singleton().IsConnected()) {
            if (SID_INPROGRESS == Application::GetInstance()->GetTopScreenId()) {
                Pop();
                DisplayCurlErrorMsg(CURLE_NO_CONNECTION_AVAILABLE);
            }
        }
        break;
    default:
        break;
    }
}

void LoginScreen::ShowAccountDisabledPopup() {
    mConfirmationDialog =
            ConfirmationDialog::CreateAndShow(
                    mLayout,
                    "IDS_ACCOUNT_DISABLED",
                    "IDS_ACCOUNT_HAS_BEEN_DISABLED",
                    "IDS_OK",
                    "IDS_ACCOUNT_DISABLED_LEARN_MORE",
                    confirmation_dialog_cb,
                    this);
}

void LoginScreen::confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event)
{
    assert(user_data);
    LoginScreen *me = static_cast<LoginScreen*>(user_data);

    me->DeleteConfirmationDialog();

    if (ConfirmationDialog::ECDNoPressed == event) {
        WebViewScreen::Launch(URL_HELP_CENTER_ACCOUNT_DISABLED);
    }
}

bool LoginScreen::DeleteConfirmationDialog() {
    if (mConfirmationDialog) {
        delete mConfirmationDialog;
        mConfirmationDialog = nullptr;
        return true;
    }
    return false;
}

void LoginScreen::ShowAlertPopup(const char* message, const char* title) {
    // This workaround is required for Tizen 3.0 to prevent popup overlapping with keyboard
    if (Application::GetInstance()->GetPlatformVersion() >= 3) {
        evas_object_hide(mLayout);
    }
    WidgetFactory::ShowAlertPopup(message, title, mLayout);
}

void LoginScreen::ParseFirstFactorFromResponse(const char *errorData) {
    std::string firstFactor;
    std::string uId;
    const std::string error = SAFE_STRDUP(errorData);
    if (std::string::npos != error.find ("login_first_factor")) {
        std::regex rgxFF(".*login_first_factor\":\"(\\w+)\",");
        std::smatch matchFF;
        if (std::regex_search(error.begin(), error.end(), matchFF, rgxFF)) {
            firstFactor = matchFF[1];
            mFirstFactor = SAFE_STRDUP(firstFactor.c_str());
        }
        //Parse User Id also
        if (std::string::npos != error.find ("uid")) {
            std::regex rgxUID(".*uid\":(\\d+),");
            std::smatch matchUID;
            if (std::regex_search(error.begin(), error.end(), matchUID, rgxUID)) {
                uId = matchUID[1];
                Config::GetInstance().SetUserId(uId.c_str());
            }
        }
    }
}
