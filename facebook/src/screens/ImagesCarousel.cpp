#include "Application.h"
#include "ImagesCarousel.h"
#include "Log.h"
#include "Photo.h"
#include "UIRes.h"
#include "Utils.h"

#include <cassert>


static const char* LogTag = "ImagesCarousel";

#ifdef _IMAGE_CAROUSEL_DEBUG
const int EvasObjectsKiller::MAX_KILLED_PER_ONCE = 5;
Eina_List *EvasObjectsKiller::mObjects = NULL;
#endif


/*
 * Class CarouselImageDownloader
 */
ImagesCarousel::CarouselImageDownloader::CarouselImageDownloader(ImagesCarousel &carousel, ImageFile &image, unsigned int page) :
        ImageFileDownloader(image), mCarousel(carousel), mPage(page) {
}

void ImagesCarousel::CarouselImageDownloader::ImageDownloaded(bool result) {
    if (result) {
        mCarousel.PageDownloaded(mPage);
    }
}


/*
 * Class ImagesCarousel
 */
const unsigned int ImagesCarousel::MAX_BOX_BUFFERED_ITEMS = 5;
const unsigned int ImagesCarousel::MAX_ITEMS_TO_DOWNLOAD = 10;

ImagesCarousel::ImagesCarousel(Evas_Object* parent, IImageCarouselObserver* observer, Eina_List *images) :
        mScroller(NULL),
        mBox(NULL),
        mPhotocams(NULL),
        mDownloaders(NULL),
        mCurrentPage(0),
        mShift(0),
        mObserver(observer) {

    unsigned int page = 0;
    Eina_List *l;
    void *list_data;
    EINA_LIST_FOREACH(images, l, list_data) {
        ImageFile *imageFile = static_cast<ImageFile *> (list_data);
        assert(imageFile);
        CarouselImageDownloader *downloader = new CarouselImageDownloader(*this, *imageFile, page++);
        mDownloaders = eina_list_append(mDownloaders, downloader);
    }

    CreateLayout(parent);
}

ImagesCarousel::~ImagesCarousel() {
    void *list_data;
    EINA_LIST_FREE(mPhotocams, list_data) {
        PhotocamWidget *photocam = static_cast<PhotocamWidget*> (list_data);
        delete photocam;
    }

    EINA_LIST_FREE(mDownloaders, list_data) {
        CarouselImageDownloader*downloader = static_cast<CarouselImageDownloader*> (list_data);
        delete downloader;
    }
    if (mScroller) {
        evas_object_smart_callback_del(mScroller, "scroll,anim,start", scroll_start_cb);
        evas_object_smart_callback_del(mScroller, "scroll,anim,stop", scroll_stop_cb);
        evas_object_smart_callback_del(mScroller, "scroll,left", scroll_drag_start_cb);
        evas_object_smart_callback_del(mScroller, "scroll,right", scroll_drag_start_cb);
        evas_object_event_callback_del(mScroller, EVAS_CALLBACK_RESIZE, resize_cb);
        evas_object_del(mScroller);
    }
}

void ImagesCarousel::CreateLayout(Evas_Object* parent) {
    mScroller = elm_scroller_add(parent);
    evas_object_size_hint_weight_set(mScroller, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mScroller, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_object_mirrored_automatic_set(mScroller, EINA_FALSE);
    elm_object_mirrored_set(mScroller, EINA_FALSE);
    elm_scroller_policy_set(mScroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_OFF);
    elm_scroller_single_direction_set(mScroller, ELM_SCROLLER_SINGLE_DIRECTION_HARD);
    elm_scroller_propagate_events_set(mScroller, EINA_FALSE);
    elm_scroller_page_scroll_limit_set(mScroller, 1, 0);
    StopScroll(false);

    evas_object_smart_callback_add(mScroller, "scroll,anim,start", scroll_start_cb, this);
    evas_object_smart_callback_add(mScroller, "scroll,anim,stop", scroll_stop_cb, this);
    evas_object_smart_callback_add(mScroller, "scroll,left", scroll_drag_start_cb, this);
    evas_object_smart_callback_add(mScroller, "scroll,right", scroll_drag_start_cb, this);

    mBox = elm_box_add(mScroller);
    elm_object_mirrored_automatic_set(mBox, EINA_FALSE);
    elm_object_mirrored_set(mBox, EINA_FALSE);
    elm_box_horizontal_set(mBox, EINA_TRUE);
    evas_object_size_hint_weight_set(mBox, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mBox, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_object_content_set(mScroller, mBox);
    evas_object_show(mBox);

    evas_object_event_callback_add(mScroller, EVAS_CALLBACK_RESIZE, resize_cb, this);
}

void ImagesCarousel::ShowPage(int pageNumber) {
    Log::info_tag(LogTag, "ShowPage: %d", pageNumber);
    mCurrentPage = pageNumber;
    elm_box_clear(mBox);

    mLeftFillerBox = elm_box_add(mBox);
    evas_object_size_hint_weight_set(mLeftFillerBox, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mLeftFillerBox, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_box_pack_end(mBox, mLeftFillerBox);

    CarouselImageDownloader *downloader = static_cast <CarouselImageDownloader *> (eina_list_nth(mDownloaders, mCurrentPage));
    assert(downloader);
    downloader->Download();
    Evas_Object *currentLayout = nullptr;

    int i = 0;
    int leftPlaceholdersCount = 0;
    Eina_List *l;
    void *list_data;
    EINA_LIST_FOREACH(mDownloaders, l, list_data) {
        bool isPlaceholder = (mCurrentPage > i && (mCurrentPage - i > MAX_BOX_BUFFERED_ITEMS)) || (i > mCurrentPage && (i - mCurrentPage > MAX_BOX_BUFFERED_ITEMS));
        if (isPlaceholder) {
            if (i < mCurrentPage) {
                ++leftPlaceholdersCount;
            }
            bool IsItemToDownload = (mCurrentPage != i && abs(mCurrentPage - i) <= MAX_ITEMS_TO_DOWNLOAD);
            if (IsItemToDownload) {
                CarouselImageDownloader *downloader = static_cast <CarouselImageDownloader *> (list_data);
                assert(downloader);
                downloader->Download();
            }
        } else {
            CarouselImageDownloader *downloader = static_cast <CarouselImageDownloader *> (list_data);
            assert(downloader);
            if (downloader) {
                Log::debug_tag(LogTag, "Image file path:%s", downloader->GetImage().GetPath());
                PhotocamWidget *photocam = new PhotocamWidget(mBox, downloader->GetImage(), i);
                mPhotocams = eina_list_append(mPhotocams, photocam);
                elm_box_pack_end(mBox, photocam->GetLayout());
                if (mCurrentPage == i) {
                    currentLayout = photocam->GetLayout();
                }
            }
        }
        ++i;
    }
    int leftFillerLength = UIRes::GetInstance()->SCREEN_SIZE_WIDTH*leftPlaceholdersCount;
    evas_object_size_hint_min_set(mLeftFillerBox, leftFillerLength, 0);

    elm_scroller_page_show(mScroller, mCurrentPage, 0);
    if (currentLayout) { //this is a workaround for #3306.
        elm_object_focus_set(currentLayout, EINA_TRUE);
    }
}

void ImagesCarousel::page_changed(void *data) {
    ImagesCarousel *me = static_cast<ImagesCarousel *> (data);
    Log::info_tag(LogTag, "Page changed:%d, shift:%d", me->mCurrentPage, me->mShift);
    if (me->mShift) {
        if (me->mShift > 0) {
            me->ShiftToRight(me->mShift);
        } else if (me->mShift < 0) {
            me->ShiftToLeft(-(me->mShift));
        }
        me->mShift = 0;
        if(me->mObserver) {
            me->mObserver->PageChanged(me->mCurrentPage);
        }
    } else {
        if(me->mObserver) {
            me->mObserver->ScrollStop();
        }
    }
}

void ImagesCarousel::ShiftToLeft(unsigned int shiftItems) {
    Evas_Coord w;
    evas_object_geometry_get(mLeftFillerBox, NULL, NULL, &w, NULL);

    for (int i = shiftItems - 1; i >= 0; --i) {
        int effectiveCurPage = mCurrentPage + i;
        int itemToPhotocam = effectiveCurPage - MAX_BOX_BUFFERED_ITEMS;
        if (itemToPhotocam >= 0) {
            CarouselImageDownloader *downloader = static_cast <CarouselImageDownloader *> (eina_list_nth(mDownloaders, itemToPhotocam));
            if (downloader) {
                PhotocamWidget *photocam = new PhotocamWidget(mBox, downloader->GetImage(), itemToPhotocam);
                mPhotocams = eina_list_prepend(mPhotocams, photocam);
                elm_box_pack_after(mBox, photocam->GetLayout(), mLeftFillerBox);
                w -= UIRes::GetInstance()->SCREEN_SIZE_WIDTH;
            } else {
                Log::error_tag(LogTag, "ShiftToLeft Error: downloader for itemToPhotocam is NULL.");
                assert(false);
            }
        }

        int itemToDownload = effectiveCurPage - MAX_ITEMS_TO_DOWNLOAD;
        if (itemToDownload >= 0) {
            CarouselImageDownloader *downloader = static_cast<CarouselImageDownloader *> (eina_list_nth(mDownloaders, itemToDownload));
            if (downloader) {
                downloader->Download();
            } else {
                Log::error_tag(LogTag, "ShiftToLeft Error: downloader for itemToDownload is NULL.");
                assert(false);
            }
        }

        int itemToPlaceholder = effectiveCurPage + MAX_BOX_BUFFERED_ITEMS + 1;
        if (itemToPlaceholder < eina_list_count(mDownloaders)) {
            PhotocamWidget *widgetToRemove = static_cast<PhotocamWidget *> (eina_list_data_get(eina_list_last(mPhotocams)));
            if (widgetToRemove) {
                mPhotocams = eina_list_remove(mPhotocams, widgetToRemove);
                delete widgetToRemove;
            } else {
                Log::error_tag(LogTag, "ShiftToLeft Error: widgetToRemove is NULL.");
                assert(false);
            }
        }
    }
    evas_object_size_hint_min_set(mLeftFillerBox, w, 0);
#ifdef _IMAGE_CAROUSEL_DEBUG
    if (mCurrentPage >= MAX_BOX_BUFFERED_ITEMS && w/UIRes::GetInstance()->SCREEN_SIZE_WIDTH+MAX_BOX_BUFFERED_ITEMS != mCurrentPage) {
        Log::error_tag(LogTag, "ShiftToLeft Error: %dx%d", w/UIRes::GetInstance()->SCREEN_SIZE_WIDTH-1, mCurrentPage);
    }
    PrintPhotocams();
#endif
}

void ImagesCarousel::ShiftToRight(unsigned int shiftItems) {
    Evas_Coord w;
    evas_object_geometry_get(mLeftFillerBox, NULL, NULL, &w, NULL);
    for (int i = shiftItems - 1; i >= 0; --i) {
        int effectiveCurPage = mCurrentPage - i;
        int itemToPhotocam = effectiveCurPage + MAX_BOX_BUFFERED_ITEMS;
        if (itemToPhotocam < eina_list_count(mDownloaders)) {
            CarouselImageDownloader *downloader = static_cast <CarouselImageDownloader *> (eina_list_nth(mDownloaders, itemToPhotocam));
            if (downloader) {
                PhotocamWidget *photocam = new PhotocamWidget(mBox, downloader->GetImage(), itemToPhotocam);
                mPhotocams = eina_list_append(mPhotocams, photocam);
                elm_box_pack_end(mBox, photocam->GetLayout());
            } else {
                Log::error_tag(LogTag, "ShiftToRight Error: downloader for itemToPhotocam is NULL.");
                assert(false);
            }
        }

        int itemToDownload = effectiveCurPage + MAX_ITEMS_TO_DOWNLOAD;
        if (itemToDownload < eina_list_count(mDownloaders)) {
            CarouselImageDownloader *downloader = static_cast<CarouselImageDownloader *> (eina_list_nth(mDownloaders, itemToDownload));
            if (downloader) {
                downloader->Download();
            } else {
                Log::error_tag(LogTag, "ShiftToRight Error: downloader for itemToDownload is NULL.");
                assert(false);
            }
        }

        int itemToPlaceholder = effectiveCurPage - MAX_BOX_BUFFERED_ITEMS - 1;
        if (itemToPlaceholder >= 0) {
            PhotocamWidget *widgetToRemove = static_cast<PhotocamWidget *> (eina_list_data_get(mPhotocams));
            assert(widgetToRemove);
            if (widgetToRemove) {
                mPhotocams = eina_list_remove(mPhotocams, widgetToRemove);
                delete widgetToRemove;

                w += UIRes::GetInstance()->SCREEN_SIZE_WIDTH;
            } else {
                Log::error_tag(LogTag, "ShiftToRight Error: widgetToRemove is NULL.");
                assert(false);
            }
        }
    }
    evas_object_size_hint_min_set(mLeftFillerBox, w, 0);
#ifdef _IMAGE_CAROUSEL_DEBUG
    if (mCurrentPage >= MAX_BOX_BUFFERED_ITEMS && w/UIRes::GetInstance()->SCREEN_SIZE_WIDTH+MAX_BOX_BUFFERED_ITEMS != mCurrentPage) {
        Log::error_tag(LogTag, "ShiftToRight Error: %dx%d", w/UIRes::GetInstance()->SCREEN_SIZE_WIDTH-1, mCurrentPage);
    }
    PrintPhotocams();
#endif
}

void ImagesCarousel::scroll_start_cb(void *data, Evas_Object *scroller, void *event_info) {
    ImagesCarousel *me = static_cast<ImagesCarousel *> (data);
    int page_no;

    elm_scroller_current_page_get(me->mScroller, &page_no, NULL);
    Log::info_tag(LogTag, "scroll_start_cb, old page:%d, new page:%d", me->mCurrentPage, page_no);
    me->EnableDoubleClick(false);
    if (me->mCurrentPage != page_no) {
//AAA The code below is required for further development/bug fixing. Please don't remove.
//        me->EnableGesture(false);
    }
}

void ImagesCarousel::scroll_drag_start_cb(void *data, Evas_Object *scroller, void *event_info) {
        static_cast<ImagesCarousel *>(data)->mObserver->ScrollStart();
}

void ImagesCarousel::scroll_stop_cb(void *data, Evas_Object *scroller, void *event_info) {
    ImagesCarousel *me = static_cast<ImagesCarousel *> (data);
    int page_no;

    elm_scroller_current_page_get(me->mScroller, &page_no, NULL);
    Log::info_tag(LogTag, "scroll_stop_cb, old page:%d, new page:%d", me->mCurrentPage, page_no);
    bool isPageChanged = me->mCurrentPage != page_no;
    if (isPageChanged) {
        me->mShift += page_no - me->mCurrentPage;
        me->mCurrentPage = page_no;
        ecore_job_add(page_changed, me);
    }
    me->EnableDoubleClick(true);
    me->NormalizePhotocams();
//AAA The code below is required for further development/bug fixing. Please don't remove.
//    me->EnableGesture(true);
    PhotocamWidget *current = me->GetPhotocamByPage(me->mCurrentPage);
    assert(current);
    if (current) {
        elm_object_focus_set(current->GetLayout(), EINA_TRUE);
    }
    if (!isPageChanged) {
        static_cast<ImagesCarousel *>(data)->mObserver->ScrollStop();
    }
}

void ImagesCarousel::EnableGesture(bool enable) {
    Eina_List *l;
    void *list_data;
    EINA_LIST_FOREACH(mPhotocams, l, list_data) {
        PhotocamWidget *photocam = static_cast<PhotocamWidget *> (list_data);
        assert(photocam);
        if (photocam) {
            photocam->Freeze(!enable);
        }
    }
}

void ImagesCarousel::EnableDoubleClick(bool enable) {
    Eina_List *l;
    void *list_data;
    EINA_LIST_FOREACH(mPhotocams, l, list_data) {
        PhotocamWidget *photocam = static_cast<PhotocamWidget *> (list_data);
        assert(photocam);
        if (photocam) {
            photocam->EnableDoubleClick(enable);
        }
    }
}

void ImagesCarousel::EnableZoom(bool enable) {
    Eina_List *l;
    void *list_data;
    EINA_LIST_FOREACH(mPhotocams, l, list_data) {
        PhotocamWidget *photocam = static_cast<PhotocamWidget *>(list_data);
        assert(photocam);
        photocam->EnableZoom(enable);
    }
}

void ImagesCarousel::StopScroll(bool stop) {
    if (stop) {
        elm_scroller_movement_block_set(mScroller, ELM_SCROLLER_MOVEMENT_BLOCK_HORIZONTAL);
    } else {
        elm_scroller_movement_block_set(mScroller, ELM_SCROLLER_MOVEMENT_NO_BLOCK);
    }
}

void ImagesCarousel::NormalizePhotocams() {
    Eina_List *l;
    void *list_data;
        EINA_LIST_FOREACH(mPhotocams, l, list_data) {
        PhotocamWidget *photocam = static_cast<PhotocamWidget *> (list_data);
        assert(photocam);
        if (photocam) {
            photocam->Normalize();
        }
    }
}

void ImagesCarousel::resize_cb(void *data, Evas *e, Evas_Object *obj, void *event_info) {
    ImagesCarousel *me = static_cast<ImagesCarousel *> (data);
    Evas_Coord w, h;

    evas_object_geometry_get(obj, NULL, NULL, &w, &h);

    Log::info_tag(LogTag, "Resize: %dX%d", w, h);

    elm_scroller_page_size_set(me->mScroller, w, h);
    elm_scroller_page_show(me->mScroller, me->mCurrentPage, 0);
}

PhotocamWidget *ImagesCarousel::GetPhotocamByPage(int page) {
    PhotocamWidget *ret = NULL;
    Eina_List *l;
    void *list_data;
    EINA_LIST_FOREACH(mPhotocams, l, list_data) {
        PhotocamWidget *photocam = static_cast<PhotocamWidget *> (list_data);
        assert(photocam);
        if (photocam) {
            if (photocam->GetId() == page) {
                ret = photocam;
                break;
            }
        }
    }
    return ret;
}

#ifdef _IMAGE_CAROUSEL_DEBUG
void ImagesCarousel::PrintPhotocams() {
    Eina_List *l;
    void *list_data;
    EINA_LIST_FOREACH(mPhotocams, l, list_data) {
        PhotocamWidget *photocam = static_cast<PhotocamWidget *> (list_data);
        Log::debug_tag(LogTag, "Photocam Id:%d", photocam->GetId());
    }
}
#endif

void ImagesCarousel::PageDownloaded(unsigned int pageNumber) {
    if (mObserver) {
        mObserver->PageDownloaded(pageNumber);
    }
}

ImageFile &ImagesCarousel::GetCurrentImageFile() const {
    CarouselImageDownloader *downloader = static_cast<CarouselImageDownloader *> (eina_list_nth(mDownloaders, mCurrentPage));
    assert(downloader);
    return downloader->GetImage();

};

/*
 * Class PhotocamWidget
 */
void PhotocamWidget::Normalize() {
    elm_photocam_zoom_mode_set(mPhotocam, ELM_PHOTOCAM_ZOOM_MODE_AUTO_FIT);
}

PhotocamWidget::PhotocamWidget(Evas_Object *parent, ImageFile &image, int id) : ImageFileDownloader(image), mId(id), mLayout(NULL), mParent(parent), mPhotocam(NULL),
        mZoomed(false), mIsDCEnabled(false), mSetImageFileJob(nullptr) {
    CreateLayout();
    if (mImage.GetStatus() == ImageFile::EDownloaded) {
        CreatePhotocam();
    } else {
        CreatePlaceholder();
        Download();
    }

}

PhotocamWidget::~PhotocamWidget() {
    EnableDoubleClick(false);
    evas_object_del(mLayout);
    if (mSetImageFileJob) {
        ecore_job_del(mSetImageFileJob);
    }
}

void PhotocamWidget::CreateLayout() {
    mLayout = elm_layout_add(mParent);
    elm_layout_file_set(mLayout, Application::mEdjPath, "photocam_layout");
    evas_object_size_hint_weight_set(mLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);
    Freeze(false);
    evas_object_show(mLayout);
}

void PhotocamWidget::CreatePhotocam() {
    mPhotocam = elm_photocam_add(mLayout);
    if (!mSetImageFileJob) {
        mSetImageFileJob = ecore_job_add(set_image_file, this);
    }

    elm_photocam_gesture_enabled_set(mPhotocam, EINA_TRUE);
    elm_photocam_zoom_mode_set(mPhotocam, ELM_PHOTOCAM_ZOOM_MODE_AUTO_FIT);

    evas_object_size_hint_weight_set(mPhotocam, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mPhotocam, EVAS_HINT_FILL, EVAS_HINT_FILL);
//AAA The code below is required for further development/bug fixing. Please don't remove.
//    evas_object_smart_callback_add(mPhotocam, "scroll,anim,start", scroll_anim_start_cb, this);
//    evas_object_smart_callback_add(mPhotocam, "scroll,anim,stop", scroll_anim_stop_cb, this);
//    evas_object_smart_callback_add(mPhotocam, "scroll", photocam_scroll_cb, this);
    EnableDoubleClick(true);

    elm_object_part_content_set(mLayout, "swallow.photocam", mPhotocam);

    evas_object_show(mPhotocam);
}

void PhotocamWidget::set_image_file(void *data) {
    PhotocamWidget *me = static_cast<PhotocamWidget *> (data);
    me->mSetImageFileJob = nullptr;
    elm_photocam_file_set(me->mPhotocam, me->mImage.GetPath());
}

void PhotocamWidget::CreatePlaceholder() {
    mPhotocam = elm_image_add(mLayout);
    elm_image_file_set(mPhotocam, ICON_DIR"/img_loading_placeholder.png", NULL);
    const static int placeholderSize = UIRes::GetInstance()->PHOTOCAM_PLACEHOLDER_IMAGE_SIZE;
    evas_object_size_hint_min_set(mPhotocam, placeholderSize, placeholderSize);
    evas_object_size_hint_max_set(mPhotocam, placeholderSize, placeholderSize);

    elm_object_part_content_set(mLayout, "swallow.photocam", mPhotocam);

    evas_object_show(mPhotocam);
}

void PhotocamWidget::ImageDownloaded(bool result) {
    if (result) {
        CreatePhotocam();
    }
    Unregister();
}

void PhotocamWidget::EnableDoubleClick(bool enable) {
    if (enable && !mIsDCEnabled) {
//AAA B2076: disable the feature for a while
        //evas_object_smart_callback_add(mPhotocam, "clicked,double", double_clicked_cb, this);
        mIsDCEnabled = true;
    } else if (!enable && mIsDCEnabled) {
//AAA B2076: disable the feature for a while
//        evas_object_smart_callback_del(mPhotocam, "clicked,double", double_clicked_cb);
        mIsDCEnabled = false;
    }
}

void PhotocamWidget::EnableZoom(bool enable) {
    elm_photocam_gesture_enabled_set(mPhotocam, enable);
    elm_photocam_zoom_mode_set(mPhotocam, ELM_PHOTOCAM_ZOOM_MODE_AUTO_FIT);
}

void PhotocamWidget::Freeze(bool freeze) {
    if (freeze) {
        elm_photocam_gesture_enabled_set (mPhotocam, EINA_FALSE);
        elm_photocam_paused_set(mPhotocam, EINA_TRUE);
        elm_scroller_movement_block_set(mPhotocam, ELM_SCROLLER_MOVEMENT_BLOCK_HORIZONTAL);
        elm_object_disabled_set(mPhotocam, EINA_TRUE);
    } else {
        elm_photocam_gesture_enabled_set (mPhotocam, EINA_TRUE);
        elm_photocam_paused_set(mPhotocam, EINA_FALSE);
        elm_scroller_movement_block_set(mPhotocam, ELM_SCROLLER_MOVEMENT_NO_BLOCK);
        elm_object_disabled_set(mPhotocam, EINA_FALSE);
    }
}

//Callbacks:
void PhotocamWidget::double_clicked_cb(void *data, Evas_Object *obj, void *event_info) {
    PhotocamWidget *me = static_cast<PhotocamWidget*> (data);
    if (me->mZoomed) {
        elm_photocam_zoom_mode_set(me->mPhotocam, ELM_PHOTOCAM_ZOOM_MODE_AUTO_FIT);
        Log::info_tag(LogTag, "double_clicked_cb: Zoomed");
    } else {
        elm_photocam_zoom_mode_set(me->mPhotocam, ELM_PHOTOCAM_ZOOM_MODE_MANUAL);
        elm_photocam_zoom_set(me->mPhotocam, 1.0);
        Log::info_tag(LogTag, "double_clicked_cb: Not zoomed");
    }
    me->mZoomed = !me->mZoomed;
}

//AAA The code below is required for further development/bug fixing. Please don't remove.
//void PhotocamWidget::scroll_anim_start_cb(void *data, Evas_Object *obj, void *event_info) {
//    Log::info_tag(LogTag, "ELM_SCROLLER_MOVEMENT_BLOCK_HORIZONTAL");
//}
//
//void PhotocamWidget::scroll_anim_stop_cb(void *data, Evas_Object *obj, void *event_info) {
//
//    Log::info_tag(LogTag, "ELM_SCROLLER_MOVEMENT_NO_BLOCK");
//}
//
//void PhotocamWidget::photocam_scroll_cb(void *data, Evas_Object *obj, void *event_info) {
//    int w, h;
//    elm_photocam_image_size_get (obj, &w, &h);
//    Log::info_tag(LogTag, "Zoom Changed: %dx%d", w, h);
//
//    double z = elm_photocam_zoom_get (obj);
//    Log::info_tag(LogTag, "Zoom: %f", z);
//    if (z < 1.0) {
//        elm_photocam_paused_set (obj, EINA_TRUE);
//        elm_photocam_zoom_mode_set(obj, ELM_PHOTOCAM_ZOOM_MODE_MANUAL);
//        elm_photocam_zoom_set (obj, 1.0);
//    //      elm_photocam_zoom_mode_set(obj, ELM_PHOTOCAM_ZOOM_MODE_AUTO_FIT_IN);
//    }
//}

