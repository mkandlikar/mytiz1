#include <assert.h>
#include <cstring>

#include "Common.h"
#include "EditCommentScreen.h"
#include "Utils.h"
#include "Log.h"
#include "WidgetFactory.h"
#include "CommentWidgetFactory.h"
#include "UIRes.h"
#include "FbRespondEditCommentMessage.h"
#include "CommentsProvider.h"
#include "FriendTagging.h"
#include "ScreenBase.h"
#include "CommentsAction.h"
#include "OperationManager.h"
#include "OperationPresenter.h"
#include "PresenterHelpers.h"
#include "SimpleCommentOperation.h"

#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "EDITCOMMENTS"

EditCommentScreen::EditCommentScreen(ActionAndData *data, const Evas_Object *dataArea, ActionAndData* selectedComment) : ScreenBase(nullptr), mOperation(nullptr)
{
    assert(data);
    assert(dataArea);

    mScreenId = ScreenBase::SID_EDIT_COMMENT;

    mComment = static_cast<Comment *>(data->mData);
    if(mComment) {
        mComment->AddRef();
    }

    mDataArea = dataArea;
    mTagging = nullptr;

    mCommentAction = new CommentsAction(this);
    mInActionAndData = data;

    mEditEntry = nullptr;
    mRootCommentForReply = selectedComment;

    mLayout = elm_layout_add(Application::GetInstance()->mNf);
    evas_object_size_hint_weight_set( mLayout, 1, 1 );
    elm_layout_file_set(mLayout, Application::mEdjPath, "edit_comment_screen_composer");
    evas_object_show(mLayout);

    CreateEntry(data);

    Evas_Object *avatar = elm_image_add(mLayout);
    elm_layout_content_set(mLayout, "comment_author_avatar", avatar);
    mInActionAndData->UpdateImageLayoutAsync(mComment->GetAvatar(), avatar);
    evas_object_show(avatar);

    if (mComment->mAttachment && mComment->mAttachment->mMedia && mComment->mAttachment->mMedia->mImage) {
        ImageSource *photo = static_cast<ImageSource*>(mComment->mAttachment->mMedia->mImage);
        Evas_Object *cmnt_photo = elm_image_add(mLayout);
        Evas_Coord cmnt_photo_width = 0;
        Evas_Coord cmnt_photo_height = 0;

        cmnt_photo_width = R->COMMENT_LANDSCAPE_PHOTO_LAYOUT_SIZE_W;

        // Keep original aspect ratio
        cmnt_photo_height = (Evas_Coord)((photo->mHeight * (cmnt_photo_width / (double)photo->mWidth))/2);
        evas_object_size_hint_min_set(cmnt_photo, cmnt_photo_width, cmnt_photo_height);
        evas_object_size_hint_max_set(cmnt_photo, cmnt_photo_width, cmnt_photo_height);
        evas_object_size_hint_weight_set(cmnt_photo, 1, 1);
        evas_object_size_hint_align_set(cmnt_photo, -1, -1);
        evas_object_show(cmnt_photo);

        elm_layout_content_set(mLayout, "photo", cmnt_photo);
        CommentWidgetFactory::CallUpdateImageLayoutAsync(mInActionAndData, mComment, cmnt_photo);
        evas_object_smart_callback_add(cmnt_photo, "clicked", ActionBase::on_photo_clicked_cb, mInActionAndData);
        evas_object_show(cmnt_photo);
     }

    elm_object_translatable_part_text_set(mLayout, "top_title_text", "IDS_EDIT_CMNT_TITLE");
    elm_object_translatable_part_text_set(mLayout, "update_btn_text", "IDS_UPDATE_CMNT");
    elm_object_translatable_part_text_set(mLayout, "cancel_btn_text", "IDS_CANCEL_UPDATING");

    elm_object_signal_callback_add(mLayout, "mouse,clicked,*", "top_*", back_btn_clicked_cb, this);
    elm_object_signal_callback_add(mLayout, "mouse,clicked,*", "cancel_*", back_btn_clicked_cb, this);
}

EditCommentScreen::~EditCommentScreen()
{
    if(mComment) {
        mComment->Release();
    }
    delete mCommentAction;
    OperationManager::GetInstance()->UnSubscribe(this);
    delete mTagging;
    delete[] mOriginalMessage;
}

void EditCommentScreen::Push() {
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(elm_naviframe_top_item_get(getNf()), EINA_FALSE, EINA_FALSE);
}

void EditCommentScreen::Update(AppEventId eventId, void * data) {
    Log::info(LOG_FACEBOOK_COMMENTING, "EditCommentScreen::Update start eventId = %d", eventId );
    switch(eventId){
    case eOPERATION_COMPLETE_EVENT:
    case eOPERATION_IN_ERROR_STATE_EVENT:
        if (data) {
            Sptr<Operation> oper(static_cast<Operation*>(data));
            Operation::OperationType operationType = oper->GetType();
            if (operationType == Operation::OT_Edit_CommentReply) {
                ProgressBarHide();
                Pop();
            }
        }
        break;
    default:
        break;
    }
}


void EditCommentScreen::CreateEntry(void *data)
{
    mEditEntry = elm_entry_add( mLayout );
    mTagging = new FriendTagging(mLayout, mEditEntry);
    if (mEditEntry)
    {
        elm_entry_prediction_allow_set( mEditEntry, EINA_FALSE );
        elm_object_part_content_set( mLayout, "entry", mEditEntry );
        elm_entry_scrollable_set( mEditEntry, EINA_TRUE );
        elm_entry_cnp_mode_set(mEditEntry, ELM_CNP_MODE_PLAINTEXT);
        evas_object_show(mEditEntry);

        char *fontStyle = NULL;
        fontStyle = WidgetFactory::WrapByFormat(SANS_REGULAR_BLACK_STYLE, R->COMMENT_MSG_TEXT_SIZE);
        if (fontStyle)
        {
            elm_entry_text_style_user_push(mEditEntry, fontStyle);
            delete[] fontStyle;
            fontStyle = NULL;
        }

        std::string taggedMessageStr = PresenterHelpers::AddFriendTags(mComment->GetMessage() ? mComment->GetMessage() : "",
                mComment->GetMessageTagsList(),
                PresenterHelpers::eComposer);
        mOriginalMessage = SAFE_STRDUP(taggedMessageStr.c_str());

        elm_entry_entry_set(mEditEntry, mOriginalMessage);

        elm_entry_cursor_end_set(mEditEntry);
        elm_entry_input_panel_enabled_set(mEditEntry, EINA_TRUE);
        elm_object_focus_allow_set(mEditEntry, EINA_TRUE);

        evas_object_smart_callback_add (mEditEntry, "changed,user", entry_text_changed, this);
    }
}

void EditCommentScreen::update_cmnt_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    assert(data);
    EditCommentScreen *screen = static_cast<EditCommentScreen *>(data);
    if (screen->mComment && screen->mEditEntry) {
        if (screen->mTagging && screen->mTagging->IsLongText(MAX_COMMENT_MESSAGE_LENGTH)) {
            return;
        }

        screen->ConfirmCallback(data, obj, NULL, NULL);

        elm_object_signal_callback_del(screen->mLayout, "mouse,clicked,*", "top_*", back_btn_clicked_cb);
        elm_object_signal_callback_del(screen->mLayout, "mouse,clicked,*", "cancel_*", back_btn_clicked_cb);
        elm_object_signal_callback_del(screen->mLayout, "mouse,clicked,*", "update_*", update_cmnt_cb);

        elm_entry_input_panel_hide(screen->mEditEntry);
    }
}

void EditCommentScreen::back_btn_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    EditCommentScreen *screen = static_cast<EditCommentScreen *>(data);
    screen->Pop();
}

void EditCommentScreen::entry_text_changed(void *data, Evas_Object *obj, void *event_info)
{
    EditCommentScreen *screen = static_cast<EditCommentScreen *>(data);

    if (screen) {
        char *newMessageUtf8 = elm_entry_markup_to_utf8(elm_entry_entry_get(obj));
        const char *updateButtonState = edje_object_part_state_get(elm_layout_edje_get(screen->mLayout), "update_btn", NULL);

        // Update is not allowed when:
        // 1) text is not changed;
        // 2) input contains only spaces, '\n' or '\t';
        // 3) input is empty and there are no attachments;
        if (strcmp(elm_entry_entry_get(obj), screen->mOriginalMessage)
                && (!Utils::isEmptyString(newMessageUtf8)
                    || (!strcmp(newMessageUtf8, "") && screen->mComment->mAttachment))) {
            // "add" call increases refs count, so let's ensure it's called only once
            if (!strcmp(updateButtonState, "default")) {
                elm_object_signal_emit(screen->mLayout, "updated", "entry");
                elm_object_signal_callback_add(screen->mLayout, "mouse,clicked,*", "update_*", update_cmnt_cb, screen);
            }
        } else {
            // signal emit is not required for "default" state
            if (!strcmp(updateButtonState, "active")) {
                elm_object_signal_emit(screen->mLayout, "empty", "entry");
                elm_object_signal_callback_del(screen->mLayout, "mouse,clicked,*", "update_*", update_cmnt_cb);
            }
        }

        free(newMessageUtf8);
    }
}

void EditCommentScreen::ConfirmCallback (void *data, Evas_Object *obj, const char *emission, const char *source)
{
    bundle* paramsBundle = CreateCommentBundle();

    Operation::OperationType operType = Operation::OT_Edit_CommentReply;

    const char* parentCommentId = nullptr;

    if(ActionAndData::IsAlive(mRootCommentForReply)) {
        assert(mRootCommentForReply->mData);
        Comment* comment = dynamic_cast<Comment*>(mRootCommentForReply->mData);
        if(comment) {
            parentCommentId = comment->GetId();// after all checks should contain root id only for replies
        }
    }

    const char* commentId = mComment->GetId();

    Sptr<Operation> operation =  MakeSptr<SimpleCommentOperation>(operType, parentCommentId, false, paramsBundle, commentId ? commentId : "");

    ReplaceEditedCommentWithPresenter(operation);

    operation->Subscribe(mInActionAndData);

    OperationManager::GetInstance()->AddOperation( operation );
    bundle_free(paramsBundle);

    OperationManager::GetInstance()->Subscribe(this);
    elm_object_signal_emit(mLayout, "empty", "entry");
    ProgressBarShow();
}

bundle* EditCommentScreen::CreateCommentBundle()
{
    bundle* paramsBundle;
    paramsBundle = bundle_create();

    if (mTagging) {
        char *textToPost = mTagging->GetStringToPost(false);
        char *escape = Utils::escape(textToPost);
        free(textToPost);
        char *originTextToPost = mTagging->GetStringToPost(true);
        bundle_add_str(paramsBundle, "origin_message", originTextToPost);
        free(originTextToPost);
        bundle_add_str(paramsBundle, "message", escape);
        free(escape);
    }

    const char *attachment_id = NULL;
    if ( mComment->mAttachment && mComment->mAttachment->mType &&
        !strcmp(mComment->mAttachment->mType,"photo")) {
        attachment_id = SAFE_STRDUP(mComment->mAttachment->mTarget->mId);
        bundle_add_str(paramsBundle, "attachment_id", attachment_id);
        free((void*) attachment_id);
    }

    return paramsBundle;
}

bool EditCommentScreen::HandleKeyPadHide()
{
    Log::info(LOG_FACEBOOK_TAGGING, "EditCommentScreen::HandleKeyPadHide");

    bool ret = true;
    if (mTagging && mTagging->HideFriendsPopup()) {
        elm_entry_cnp_mode_set(mEditEntry, ELM_CNP_MODE_PLAINTEXT);
        elm_entry_input_panel_show(mEditEntry);
    } else {
        ret = false;
    }
    return ret;
}

void EditCommentScreen::ReplaceEditedCommentWithPresenter(Sptr<Operation> operation) {
    Log::info("EditCommentScreen::ReplaceEditedCommentWithPresenter");
    assert(mComment);
    assert(mInActionAndData->mParentWidget);

    evas_object_hide(mInActionAndData->mParentWidget);
    Evas_Object* oldParentWidget = mInActionAndData->mParentWidget;

    OperationPresenter *oper = new OperationPresenter(operation, mComment);
    oper->SetId(const_cast<char*>(mInActionAndData->mData->GetId()));

    mInActionAndData->mData = oper;
    mInActionAndData->mParentWidget = CommentWidgetFactory::CreateCommentOperation(const_cast<Evas_Object*>(mDataArea), mInActionAndData, true, mInActionAndData->mParentWidget);

    if (!mInActionAndData->mParentWidget) {
        mInActionAndData->mData->Release();
        mInActionAndData->mData = mComment;//error case, replace presenter with comment back
        mInActionAndData->mParentWidget = oldParentWidget;
        evas_object_show(mInActionAndData->mParentWidget);
        Log::error("EditCommentScreen::ReplaceEditedCommentWithPresenter->can't replace comment");
    } else {
        mInActionAndData->mData->AddRef();
        AppEvents::Get().Notify(eREPLACE_GRAPH_OBJ_FOR_EDITED_COMMENT, mInActionAndData->mData);
        mInActionAndData->mData->Release();

        mComment->Release();
        mComment = nullptr;
        evas_object_del(oldParentWidget);
    }
}
