/*
 *  CommonScreen.cpp
 *
 *  Created on: June 26, 2015
 *      Author: Manjunath Raja
 */


#include <regex.h>

#include "LoginScreen.h"
#include "SignUpInProgressScreen.h"
#include "SignupRequest.h"


namespace CommonScreen {
    void LaunchSignIn() {
        Application::GetInstance()->RemoveAllScreensAndNfItemsFromNaviframe();//unused
        ScreenBase *NewScreen = new LoginScreen(NULL);
        Application::GetInstance()->AddScreen(NewScreen);
    }

    void StartProgressScreen() {
        ScreenBase *NewScreen = new SignUpInProgressScreen(NULL);
        Application::GetInstance()->AddScreen(NewScreen);
    }

    void StartSigningUp() {
        StartProgressScreen();
        SignupRequest::Singleton().InitAsync(SignupRequest::SIGNUP);
    }

    bool IsValidNumber(const char *Number) {
        regex_t RegexHandle;
        bool IsValidNo = false;
        const char *NoRegex = "[0-9]$";
        int RegexRetVal;

        if (regcomp(&RegexHandle, NoRegex, REG_EXTENDED)) {
            return false;
        }

        if (!(RegexRetVal = regexec(&RegexHandle, Number, 0, NULL, 0))) {
            IsValidNo = true;
        }
        else if (RegexRetVal == REG_NOMATCH){
        } else {
        }

        regfree(&RegexHandle);
        return IsValidNo;
    }
}


