#include "ConnectivityManager.h"
#include "FacebookSession.h"
#include "Log.h"
#include "MainScreen.h"
#include "NotificationObject.h"
#include "NotificationProvider.h"
#include "NotificationsScreen.h"
#include "PostScreen.h"
#include "PushService.h"
#include "Separator.h"
#include "Utils.h"
#include "WebViewScreen.h"
#include "WidgetFactory.h"

#include <badge.h>

const unsigned int NotificationsScreen::DEFAULT_WND_SIZE = 196;
GraphRequest* NotificationsScreen::mNotificationMarkAsRead = nullptr;

NotificationsScreen::NotificationsScreen(ScreenBase *parentScreen) :
    ScreenBase(parentScreen), TrackItemsProxyAdapter(nullptr, NotificationProvider::GetInstance())
{
    mScreenId = ScreenBase::SID_NOTIFICATION_SCREEN;

    mNotificationSeen = NULL;

    mNoNotifications = NULL;

    mObjectDownloader = nullptr;
    mClickedNotiLink = nullptr;

    mAction = new ActionBase(this);

    SetIdentifier("NOTIFICATIONS SCREEN");

    mLayout = WidgetFactory::CreateLayoutByGroup(getParentMainLayout(), "simple_wrapper");

    ApplyScroller(mLayout, true);
    elm_object_part_content_set(mLayout, "content", GetScroller());

    mPostponedLanguageChange = false;

    PushService *push = new PushService();
    push->connect();

    TrackItemsProxy::ProxySettings *settings = GetProxySettings();
    if (settings) {
        settings->wndSize = DEFAULT_WND_SIZE;
        SetProxySettings(settings);
    }

    AppEvents::Get().Subscribe(eNOTIFICATION_REDRAW, this);
    AppEvents::Get().Subscribe(eNOTIFICATION_UPDATE_BADGE, this);
    AppEvents::Get().Subscribe(eINTERNET_CONNECTION_CHANGED, this);
    AppEvents::Get().Subscribe(eLANGUAGE_CHANGED, this);
    AppEvents::Get().Subscribe(ePOST_DELETED, this);

    RequestData(true);
}

NotificationsScreen::~NotificationsScreen()
{
    FacebookSession::GetInstance()->ReleaseGraphRequest(mNotificationMarkAsRead, true);
    FacebookSession::GetInstance()->ReleaseGraphRequest(mNotificationSeen, true);

    delete mAction;

    if (mNoNotifications) {
        evas_object_del(mNoNotifications);
    }
    EraseWindowData();

    delete mObjectDownloader;
    free(mClickedNotiLink);
}

void NotificationsScreen::FetchEdgeItems()
{
    Log::info(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::FetchEdgeItems");
    if (ConnectivityManager::Singleton().IsConnected()) {
        Log::info(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::FetchEdgeItems->RequestData");
        DeleteNoResultItem();//delete 'no notifications' from screen and allow progressBar/Errors
        NotificationProvider::GetInstance()->EraseData();
        RequestData(true);
        RequestToRebuildAllItems();
        mPostponedLanguageChange = false;
    }
}

void* NotificationsScreen::CreateItem(void *dataForItem, bool isAddToEnd)
{
    ActionAndData *data = NULL;

    GraphObject *checkType = static_cast<GraphObject*>(dataForItem);
    switch (checkType->GetGraphObjectType()) {
    case GraphObject::GOT_NOTIFICATION: {
        NotificationObject *notification = static_cast<NotificationObject*>(dataForItem);
        if (notification) {
            data = new ActionAndData(notification, mAction);
            data->mParentWidget = CreateNotificationItem(data, GetDataArea(), isAddToEnd);
        }
        break;
    }
    case GraphObject::GOT_SEPARATOR: {
        Separator *separator = static_cast<Separator*>(dataForItem);
        data = new ActionAndData(separator, mAction);
        switch (separator->GetSeparatorType()) {
        case Separator::NEW_NOTI_SEPARATOR:
            data->mParentWidget = WidgetFactory::CreateSeparatorItem(GetDataArea(), isAddToEnd, "IDS_NOTIFICATIONS_NEW", true);
            break;
        case Separator::EARLIER_NOTI_SEPARATOR:
            data->mParentWidget = WidgetFactory::CreateSeparatorItem(GetDataArea(), isAddToEnd, "IDS_NOTIFICATIONS_EARLIER", true);
            break;
        default:
            assert(false);  //accidentally got a separator of an unsupported type
            break;
        }
        break;  //end of case GraphObject::GOT_SEPARATOR
    }
    default:
        break;
    }

    if (data && !data->mParentWidget) {
        Log::error(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::CreateItem() - UI content is not valid");
        delete data;
        data = nullptr;
    }

    return data;
}

void NotificationsScreen::SetDataAvailable(bool isAvailable)
{
    Log::info("NotificationsScreen::SetDataAvailable %d",isAvailable);
    if (!isAvailable && ConnectivityManager::Singleton().IsConnected()) {
        Log::info("NotificationsScreen::SetDataAvailable->NoResult");
        if (!mNoNotifications) {
            mNoNotifications = NoResultItem(GetDataArea(), true);
            ApplyProgressBar(false);
            SetShowConnectionErrorEnabled(false);
        }
    }
}

Evas_Object* NotificationsScreen::CreateNotificationItem(ActionAndData *actionData, Evas_Object *parent, bool isAddToEnd)
{
    NotificationObject *notification = static_cast<NotificationObject*>(actionData->mData);

    Evas_Object *content = NULL;

    if (notification) {
        content = WidgetFactory::CreateSimpleWrapper(parent);

        Evas_Object *list_item = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(content, "notification_item_type_1");

        if (notification->mUnread) {
            elm_object_signal_emit(list_item, "unread", "notification");
        }

        Evas_Object *avatar = elm_image_add(list_item);
        elm_object_part_content_set(list_item, "item_avatar", avatar);

        if (notification->mFromPictureUrl) {
            actionData->UpdateImageLayoutAsync(notification->mFromPictureUrl, avatar);
        } else {
            elm_image_file_set(avatar, ICON_DIR"/profiles/empty_profile_image.png", NULL);
        }

        evas_object_show(avatar);

        Evas_Object *box = elm_box_add(list_item);
        elm_object_part_content_set(list_item, "item_box", box);
        evas_object_size_hint_weight_set(box, 1, 1);
        evas_object_size_hint_align_set(box, -1, 0.5);
        evas_object_show(box);

        char *title_text = NULL;
        Evas_Object *title = elm_label_add(box);
        evas_object_size_hint_weight_set(title, 1, 1);
        evas_object_size_hint_align_set(title, -1, -1);
        elm_label_wrap_width_set(title, R->NOTIFICATION_TITLE_WRAP_SIZE);
        elm_label_line_wrap_set(title, ELM_WRAP_MIXED);
        title_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_000_FORMAT, R->NOTIFICATION_TITLE_FONT_SIZE, notification->mTaggedTitle.c_str());
        if (title_text) {
            elm_object_text_set(title, title_text);
            delete[] title_text;
        }
        elm_box_pack_end(box, title);
        evas_object_show(title);

        char *time_text = NULL;
        Evas_Object *time = elm_label_add(box);
        evas_object_size_hint_weight_set(time, 1, 1);
        evas_object_size_hint_align_set(time, -1, -1);
        elm_label_wrap_width_set(time, R->NOTIFICATION_TITLE_WRAP_SIZE);
        elm_label_line_wrap_set(time, ELM_WRAP_MIXED);
        time_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_000_FORMAT, R->NOTIFICATION_TITLE_FONT_SIZE, notification->mUpdatedTimeString);
        if (time_text) {
            HRSTC_TRNSLTD_TEXT_SET(time, time_text);
            delete[] time_text;
        }
        elm_box_pack_end(box, time);
        evas_object_show(time);

        Evas_Object *spacer = elm_box_add(box);
        evas_object_size_hint_weight_set(spacer, 10, 10);
        evas_object_size_hint_align_set(spacer, -1, -1);
        elm_box_pack_end(box, spacer);
        evas_object_show(spacer);

        actionData->mActionObj = list_item;

        elm_object_signal_callback_add(list_item, "mouse,clicked,*", "item_spacer", on_item_clicked_cb, actionData);

        if (isAddToEnd) {
            elm_box_pack_end(parent, content);
        } else {
            elm_box_pack_start(parent, content);
        }
        evas_object_show(content);
    }

    return content;
}

Evas_Object *NotificationsScreen::NoResultItem(Evas_Object* parent, bool isAddToEnd)
{
    Log::info(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::NoResultItem");

    Evas_Object *item = WidgetFactory::CreateLayoutByGroup(parent, "no_requests_box");
    if (isAddToEnd) {
        elm_box_pack_end(parent, item);
    } else {
        elm_box_pack_start(parent, item);
    }

    elm_object_translatable_part_text_set(item, "fr_rq_text", "IDS_NO_NOTIFICATIONS");

    return item;
}

void NotificationsScreen::on_item_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::info(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::on_item_clicked_cb");

    ActionAndData *actionData = static_cast<ActionAndData*>(user_data);
    if (actionData) {
        NotificationObject *notif = static_cast<NotificationObject*>(actionData->mData);
        if (notif) {
            NotificationsScreen *me = static_cast<NotificationsScreen*>(actionData->mAction->getScreen());
            Log::info(LOG_FACEBOOK_NOTIFICATION, "Notif appName: %s, appLink: %s", notif->mApplicationName, notif->mApplicationLink);
            assert(me);

            free(me->mClickedNotiLink);
            me->mClickedNotiLink = SAFE_STRDUP(notif->mLink);
            if (notif->mUnread) {
                if (mNotificationMarkAsRead) {
                    FacebookSession::GetInstance()->ReleaseGraphRequest(mNotificationMarkAsRead, true);
                }
                mNotificationMarkAsRead = FacebookSession::GetInstance()->MarkNotificationAsRead(notif->GetId(), on_notification_mark_as_read_completed, actionData);
                elm_object_signal_emit(actionData->mActionObj, "read", "notification");
            }

            switch (notif->mNotificationType) {
            case NotificationObject::NOTIF_FRIEND:
                Log::info(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::on_item_clicked_cb(NOTIF_FRIEND)");
                Utils::OpenProfileScreen(notif->mObjectId);
                break;
            case NotificationObject::NOTIF_LIKES:
            case NotificationObject::NOTIF_FEED_COMMENTS:
            case NotificationObject::NOTIF_WALL:
                Log::info(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::on_item_clicked_cb(NOTIF_WALL || NOTIF_FEED_COMMENTS || NOTIF_LIKES)");
                me->StartDownloader(notif->mObjectId);
                break;
            case NotificationObject::NOTIF_VIDEO:
                Log::info(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::on_item_clicked_cb(NOTIF_VIDEO)");
                if (notif->mObjectId) { // if mObjectId is NULL then do nothing. Sometimes we receive such broken notifications. See https://github.com/fbmp/fb4t/issues/3552.
                    std::string postId;
                    if (notif->mFromName && !strcmp(notif->mFromName, "Facebook")) {
                        //"Your video is ready..." notification is sent from Facebook.
                        //Need to use ID of the original sender instead of Facebook's ID.
                        postId = notif->GetId();
                        std::vector < std::string > splitResult = Utils::SplitString(postId, "_");
                        if (3 == splitResult.size() && splitResult[0] == "notif") {
                            postId = splitResult[1];
                        } else {
                            try_link_in_webview(me->mClickedNotiLink);
                            break;
                        }
                    } else {
                        postId = notif->mFromId;
                    }
                    postId.append("_");
                    postId.append(notif->mObjectId);
                    me->StartDownloader(postId.c_str());
                } else {
                    Log::error(LOG_FACEBOOK_NOTIFICATION, "Broken video notification with empty mObjectId is received!");
                }
                break;
            case NotificationObject::NOTIF_PHOTOS:
                Log::info(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::on_item_clicked_cb(NOTIF_PHOTOS)");
                if (notif->mObjectId) {
                    std::string postId = notif->mFromId;
                    postId.append("_");
                    postId.append(notif->mObjectId);
                    me->StartDownloader(postId.c_str());
                }
                break;
            default:
                Log::info(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::on_item_clicked_cb(mNotificationType=%d)", notif->mNotificationType);
                try_link_in_webview(me->mClickedNotiLink);
                break;
            }
        }
    }
}

void NotificationsScreen::try_link_in_webview(char *link)
{
    if (link) {
        if (Application::GetInstance()->GetTopScreenId() != SID_WEB_VIEW) {
            WebViewScreen::Launch(link, true);
        }
    }
}

void NotificationsScreen::on_notification_mark_as_read_completed(void* object, char* respond, int code)
{
    Log::debug(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::on_notification_mark_as_read_completed");
    FacebookSession::GetInstance()->ReleaseGraphRequest(mNotificationMarkAsRead);

    ActionAndData *actionData = static_cast<ActionAndData*>(object);

    if (ActionAndData::IsAlive(actionData)) {
        if (code == CURLE_OK) {
            Log::debug(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::on_notification_mark_as_read_completed->response: %s", respond);
            FbRespondBase *notifRespond = FbRespondBase::createFromJson(respond);
            if (notifRespond == NULL) {
                Log::error(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::on_notification_mark_as_read_completed->Error parsing http respond");
            } else {
                if (notifRespond->mError != NULL) {
                    Log::error(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::on_notification_mark_as_read_completed->Error sending add request, error = %s", notifRespond->mError->mMessage);
                    if (actionData && actionData->mActionObj) {
                        elm_object_item_signal_emit(actionData->mActionObj, "unread", "notification");
                    }
                } else if (notifRespond->mSuccess) {
                    //Do nothing
                }
            }
            delete notifRespond;
        } else {
            Log::error(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::on_notification_mark_as_read_completed->Error sending http request");
        }
    } else {
        Log::error(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::on_notification_mark_as_read_completed->ActionAndData is dead");
    }

    free(respond);
}

void NotificationsScreen::UpdateNotificationBadge()
{
    Log::info(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::UpdateNotificationBadge");

    MainScreen *screen = static_cast<MainScreen*>(GetParent());
    if (screen) {
        if ((screen->GetTabIndex() == MainScreen::eNotificationsTab) &&
            (NotificationProvider::GetInstance()->GetUnreadBadgeNum() != 0)) {
            mNotificationSeen = FacebookSession::GetInstance()->PostUnseenNotificationsCount(on_notification_mark_as_seen_completed, this);
            NotificationProvider::GetInstance()->SetUnreadBadgeNum(0);
        }
        int num = NotificationProvider::GetInstance()->GetUnreadBadgeNum();
        Log::debug(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::UpdateNotificationBadge = %d", num);
        screen->SetTabbarBadge(MainScreen::eNotificationsTab, num);
        badge_set_count(PACKAGE,num);
    }
}


void NotificationsScreen::on_notification_mark_as_seen_completed(void* object, char* respond, int code)
{
    Log::info(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::on_notification_mark_as_seen_completed");

    if (code == CURLE_OK) {
        Log::debug(LOG_FACEBOOK_NOTIFICATION, "response: %s", respond);
        FbRespondBase *notifRespond = FbRespondBase::createFromJson(respond);
        if (notifRespond == NULL) {
            Log::error(LOG_FACEBOOK_NOTIFICATION, "Error parsing http respond");
        } else {
            if (notifRespond->mError != NULL) {
                Log::error(LOG_FACEBOOK_NOTIFICATION, "Error sending add request, error = %s", notifRespond->mError->mMessage);
            } else if (notifRespond->mSuccess) {
                //Do nothing
            }
        }
        if (notifRespond) {
            delete notifRespond;
        }
    } else {
        Log::error(LOG_FACEBOOK_NOTIFICATION, "Error sending http request");
    }

    free(respond);

    if (object) {
        NotificationsScreen *me = static_cast<NotificationsScreen*>(object);
        FacebookSession::GetInstance()->ReleaseGraphRequest(me->mNotificationSeen, true);
    }
}

void NotificationsScreen::Update(AppEventId eventId, void *data)
{
    Log::info(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::Update");
    switch (eventId) {
    case eNOTIFICATION_REDRAW: {
        Log::info(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::Update = eNOTIFICATION_REDRAW");
        int num = NotificationProvider::GetInstance()->GetUnreadBadgeNum();
        NotificationProvider::GetInstance()->SetUnreadBadgeNum(++num);
        // Do not update badge beforehand! Do that later, when notification download completes.
        FetchNewNotifications();
        break;
    }
    case eREQUEST_COMPLETED_EVENT: {
        Log::info(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::Update = eREQUEST_COMPLETED_EVENT");
        UpdateNotificationBadge();
        TrackItemsProxy::Update(eventId, data);
        break;
    }
    case eNOTIFICATION_UPDATE_BADGE: {
        Log::info(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::Update = eNOTIFICATION_UPDATE_BADGE");
        UpdateNotificationBadge();
        break;
    }
    case eINTERNET_CONNECTION_CHANGED: {
        Log::info(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::Update = eINTERNET_CONNECTION_CHANGED");
        if (!ConnectivityManager::Singleton().IsConnected()) {
            DeleteNoResultItem();
            ShowConnectionError(false);
        } else {
            ShowProgressBarWithTimer(IsServiceWidgetOnBottom());
            FetchNewNotifications();
        }
        break;
    }
    case eLANGUAGE_CHANGED: {
        Log::info(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::Update = eLANGUAGE_CHANGED");
        mPostponedLanguageChange = true;
        FetchNewNotifications();
        break;
    }
    case ePOST_DELETED: {
        Log::info(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::Update =  ePOST_DELETED try to remove appropriate notifications");
        char *id = static_cast<char*>(data);
        NotificationProvider::GetInstance()->DeleteNotificationsByPostId(id);
        Redraw();
        break;
    }
    default:
        TrackItemsProxy::Update(eventId, data);
        break;
    }
}

void NotificationsScreen::RebuildAllItems(void) {
    Log::info(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::RebuildAllItems->ProviderSize= %u", NotificationProvider::GetInstance()->GetDataCount());
    bool isDataAvailable = NotificationProvider::GetInstance()->GetDataCount();
    SetDataAvailable(isDataAvailable);
    if (isDataAvailable) {
        if (IsEmpty()) {
            ShiftDown();
        } else {
            Redraw();
        }
    }
}

void NotificationsScreen::FetchNewNotifications(void) {
    Log::info(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::FetchNewNotifications");
    if (IsRealTopEdgeOfScroller() || mPostponedLanguageChange) {
        FetchEdgeItems();
    }
}

void NotificationsScreen::DeleteNoResultItem(void) {
    Log::info(LOG_FACEBOOK_NOTIFICATION, "NotificationsScreen::DeleteNoResultItem");
    if (mNoNotifications) {
        ApplyProgressBar(true);
        evas_object_del (mNoNotifications);
        mNoNotifications = nullptr;
        SetShowConnectionErrorEnabled(true);
    }
}

void NotificationsScreen::ProgressBarShow()
{
    ScreenBase::ProgressBarShow();
    //The progressbar appears hidden if current screen ID (NOTIFICATION) is not equal to the top screen ID (MAIN).
    //Hence force the visibility:
    evas_object_raise(mProgressBar);
}

void NotificationsScreen::StartDownloader(const char *objectId)
{
    if (!mObjectDownloader) {
        mObjectDownloader = new GraphObjectDownloader(objectId, this);
        mObjectDownloader->StartDownloading();
    }
}

void NotificationsScreen::GraphObjectDownloaderProgress(IGraphObjectDownloaderObserver::Result result) {
    switch (result) {
    case IGraphObjectDownloaderObserver::ENetworkErrorToBeResumed:
        ProgressBarHide();
        ShowErrorWidget(eCONNECTION_ERROR);
        break;
    case IGraphObjectDownloaderObserver::EDownloadingStarted:
        HideErrorWidget();
        ProgressBarShow();
        break;
    case IGraphObjectDownloaderObserver::EPostDownloaded:
    case IGraphObjectDownloaderObserver::EPhotoDownloaded:
        ProgressBarHide();
        if (mObjectDownloader) {
            PostScreen *postScreen = new PostScreen(nullptr, nullptr, mObjectDownloader->GetGraphObject());
            delete mObjectDownloader;
            mObjectDownloader = nullptr;
            Application::GetInstance()->AddScreen(postScreen);
        }
        break;
    case IGraphObjectDownloaderObserver::ENothingDownloaded:
        ProgressBarHide();
        if (mObjectDownloader) {
            delete mObjectDownloader;
            mObjectDownloader = nullptr;

            /**
             * The workaround for Bugzilla 2084 issue has not helped. We have a notification object which ID cannot be found on FB-server.
             * Show the notification in a webview.
             */
            try_link_in_webview(mClickedNotiLink);
        }
        break;
    default:
        break;
    }
}
