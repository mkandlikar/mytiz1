/*
 * MessagerScreen.cpp
 *
 *  Created on: Apr 6, 2015
 *      Author: RUINDMVA
 */
#include <app_manager.h>

#include "Common.h"
#include "MessagerScreen.h"
#include "Utils.h"
#include "WidgetFactory.h"

const int MessagerScreen::max_string_length = 1024;
MessagerScreen *MessagerScreen::msg_screen_instance = NULL;
//TODO: The UI part of the screen quite unusual in Z3 and Z1. Needs to be improved.

MessagerScreen::MessagerScreen(ScreenBase *parentScreen) : ScreenBase(parentScreen), mBtn(NULL), mTitle(NULL), mImage(NULL), mInfoString(NULL) {
    //This shall check whether the messenger application is installed or not
    //In case it is installed, it will show the app invoker screen else the installer screen
    int ret = check_Messenger_inst_status();
    if(ret == APP_MANAGER_ERROR_NONE) {
        Create_invoker_screen();
        messager_inst = true;
    } else {
        //Application Does not exist, create the installer Layout
        Create_installer_screen();
        messager_inst = false;
    }
    //Set the instance of the Messenger Screen;
    msg_screen_instance_set(this);
}

MessagerScreen::~MessagerScreen() {
}

void MessagerScreen::Create_installer_screen() {
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Creating Installer Screen");
    //The layout consists of 4 element - main title, main information string, Image and button
    //The following strings shall be taken for localization purposes.IDS_MESSENGER_INS_TITLE, IDS_MESSENGER_INS_MAIN, IDS_MESSENGER_INS_BTN
    mLayout =  elm_box_add(getParentMainLayout());
    evas_object_size_hint_weight_set(mLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);

    //Set the Title
    const char *localizedTitleTxt = i18n_get_text("IDS_MESSENGER_INS_TITLE");
    if (localizedTitleTxt) {
        char *formattedStr = WidgetFactory::WrapByFormat2(MESSENGER_SCRN_FORMAT, R->MESSENGER_TITLE_FONT_SIZE, localizedTitleTxt);
        mTitle = elm_label_add(mLayout);
        elm_object_text_set(mTitle, formattedStr );
        evas_object_size_hint_weight_set(mTitle, 0.1, 0.1);
        evas_object_size_hint_align_set(mTitle, 0.5, 0.3);
        delete []formattedStr;
        elm_box_pack_end(mLayout, mTitle);
        evas_object_show(mTitle);
    }

    //Set the main string information
    const char *localizedMainInfoTxt = i18n_get_text("IDS_MESSENGER_INS_MAIN");
    if (localizedMainInfoTxt)
    {
        char *formattedStr = WidgetFactory::WrapByFormat2(MESSENGER_SCRN_FORMAT, R->MESSENGER_MAININFO_FONT_SIZE, localizedMainInfoTxt);
        mInfoString = elm_label_add(mLayout);
        elm_label_line_wrap_set(mInfoString, ELM_WRAP_MIXED);
        elm_object_text_set(mInfoString, formattedStr );
        evas_object_size_hint_weight_set(mInfoString, 0.1, 0.1);
        evas_object_size_hint_align_set(mInfoString, 0.5, 0.0);
        evas_object_size_hint_max_set(mInfoString, 700, 100);
        evas_object_size_hint_min_set(mInfoString, 450, 100);
        delete []formattedStr;
        elm_box_pack_end(mLayout, mInfoString);
        evas_object_show(mInfoString);
    }

    //Set the image
    mImage = elm_image_add(mLayout);
    elm_image_file_set(mImage, ICON_DIR"/messaging/messanger_icon.jpg", NULL);
    evas_object_resize(mImage, 300, 200);
    evas_object_size_hint_weight_set(mImage, 0.3, 0.3);
    evas_object_size_hint_align_set(mImage, EVAS_HINT_FILL,EVAS_HINT_FILL);
    elm_box_pack_end(mLayout, mImage);
    evas_object_show(mImage);

    //Set the button
    const char *localizedBtnTxt = i18n_get_text("IDS_MESSENGER_INS_BTN");
    if (localizedBtnTxt) {
        char *formattedStr = WidgetFactory::WrapByFormat2(MESSENGER_SCRN_FORMAT, R->MESSENGER_BTN_FONT_SIZE, localizedBtnTxt);
        mBtn = elm_button_add(mLayout);
        elm_object_part_text_set(mBtn, "elm.text", formattedStr);
//        elm_object_style_set(mBtn,"hoversel_horizontal_entry/default");
        elm_object_style_set(mBtn,"default");
        elm_image_resizable_set(mBtn, EINA_TRUE, EINA_TRUE);
        evas_object_size_hint_max_set(mBtn, 700, 100);
        evas_object_size_hint_min_set(mBtn, 350, 70);
        evas_object_color_set(mBtn,78,106,163,255);
        evas_object_size_hint_weight_set(mBtn, 0.5, 0.5);
        evas_object_size_hint_align_set(mBtn, 0.5,0.2);
        delete []formattedStr;
        elm_box_pack_end(mLayout, mBtn);
        evas_object_show(mBtn);
    }

    //Add a smart call back function
    evas_object_smart_callback_add(mBtn, "clicked", trigger_msg_install, this);
    evas_object_show(mLayout);
}

void MessagerScreen::Create_invoker_screen() {
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Creating Invoker Screen");
    //The layout consists of 4 element - main title, main information string, Image and button
    //The following strings shall be taken for localization purposes.IDS_MESSENGER_INVK_TITLE, IDS_MESSENGER_INVK_MAIN, IDS_MESSENGER_INVK_BTN
    mLayout =  elm_box_add(getParentMainLayout());
    Evas_Object *layoutBg = elm_bg_add( mLayout );
    elm_bg_color_set(layoutBg, 255, 255, 255);
    evas_object_show(layoutBg);
    evas_object_size_hint_weight_set(mLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);

    //Set the Title
    const char *localizedTitleTxt = i18n_get_text("IDS_MESSENGER_INVK_TITLE");
    if (localizedTitleTxt)
    {
        char *formattedStr = WidgetFactory::WrapByFormat2(MESSENGER_SCRN_FORMAT, R->MESSENGER_TITLE_FONT_SIZE, localizedTitleTxt);
        mTitle = elm_label_add(mLayout);
        elm_object_text_set(mTitle, formattedStr );
        evas_object_size_hint_weight_set(mTitle, 0.1, 0.1);
        evas_object_size_hint_align_set(mTitle, 0.5, 0.3);
        delete []formattedStr;
    }

    //Set the main string information
    const char *localizedMainInfoTxt = i18n_get_text("IDS_MESSENGER_INVK_MAIN");
    if (localizedMainInfoTxt)
    {
        char *formattedStr = WidgetFactory::WrapByFormat2(MESSENGER_SCRN_FORMAT, R->MESSENGER_MAININFO_FONT_SIZE, localizedMainInfoTxt);
        mInfoString = elm_label_add(mLayout);
        elm_label_line_wrap_set(mInfoString, ELM_WRAP_WORD);
        elm_object_text_set(mInfoString, formattedStr );
        evas_object_size_hint_weight_set(mInfoString, 0.1, 0.1);
        evas_object_size_hint_align_set(mInfoString, 0.5, 0.0);
        delete []formattedStr;
    }

    //Set the image
    mImage = elm_image_add(mLayout);
    elm_image_file_set(mImage, ICON_DIR"/messaging/messanger_icon.jpg", NULL);
    evas_object_resize(mImage, 300, 200);
    evas_object_size_hint_weight_set(mImage, 0.3, 0.3);
    evas_object_size_hint_align_set(mImage, -1,-1);

    //Set the button
    mBtn = elm_button_add(mLayout);
    elm_object_translatable_text_set(mBtn, "IDS_MESSENGER_INVK_BTN");
    elm_object_style_set(mBtn,"hoversel_horizontal_entry/default");
    evas_object_color_set(mBtn,78,106,163,255);
    evas_object_size_hint_weight_set(mBtn, 0.5, 0.5);
    evas_object_size_hint_align_set(mBtn, 0.5,0.2);

    //Add a smart call back function
    evas_object_smart_callback_add(mBtn, "clicked", trigger_msg_invoke, this);

    //Pack the elements
    elm_box_pack_end(mLayout, mTitle);
    elm_box_pack_end(mLayout, mInfoString);
    elm_box_pack_end(mLayout, mImage);
    elm_box_pack_end(mLayout, mBtn);

    //Show the elements
    evas_object_show(mBtn);
    evas_object_show(mImage);
    evas_object_show(mInfoString);
    evas_object_show(mTitle);
    evas_object_show(mLayout);
}

//Callback for Installing Messenger, The following information shall be used
//App ID : org.tizen.tizenstore Operation ID : APP_CONTROL_OPERATION_VIEW Uri : tizenstore://ProductDetail/{a package Id}
void MessagerScreen::trigger_msg_install(void *data, Evas_Object *obj, void *EventInfo) {
    app_control_h service = NULL;
    int ret = -1;
    ret = app_control_create(&service);
    ret = app_control_set_app_id(service, "org.tizen.tizenstore");
    char l_app_name[max_string_length];
    Utils::Snprintf_s(l_app_name, max_string_length,"tizenstore://ProductDetail/%s",MESSENGER_APPLICATION_ID);
    ret = app_control_set_uri(service,l_app_name);
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Invoking Tizen store for %s", l_app_name);
    ret = app_control_set_operation(service, APP_CONTROL_OPERATION_VIEW);
    ret = app_control_send_launch_request(service, NULL, NULL);
}

//Callback for invoking Messenger
void MessagerScreen::trigger_msg_invoke(void *data, Evas_Object *obj, void *EventInfo) {
    app_control_h service = NULL;
    int ret = -1;
    ret = app_control_create(&service);
    ret = app_control_set_app_id(service, MESSENGER_APPLICATION_ID);
    ret = app_control_set_operation(service, APP_CONTROL_OPERATION_VIEW);
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Invoking Messenger application %s", MESSENGER_APPLICATION_ID);
    ret = app_control_send_launch_request(service, NULL, NULL);
}

//Check Installation status
int MessagerScreen::check_Messenger_inst_status() {
    app_info_h mesg_app_context = NULL;
    int ret = app_manager_get_app_info(MESSENGER_APPLICATION_ID, &mesg_app_context);
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "checking the messenger status %d", ret);
    if(APP_MANAGER_ERROR_NO_SUCH_APP == ret) {
        //Could not get the information of the Messenger application as it is not installed
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Messenger Application does not exist");
    } else if((APP_MANAGER_ERROR_NONE == ret)) {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Messenger Application Exists");
        app_info_destroy(mesg_app_context); //Destroy the application information to avoid memory leak
    } else {
        //Do nothing as some internal error is stopped from getting the status of the application
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Could Not get application status");
    }
    return ret;
}

//Callback to check the messenger installation status
void MessagerScreen::cb_check_Messenger_inst_status() {
    //Check whether there is an update to Messenger status, in case yes remake the layout else do nothing
    MessagerScreen * inst_scr = msg_screen_instance_get();
    if(inst_scr == NULL)
        return;
    int ret = inst_scr->check_Messenger_inst_status();
    if((ret == APP_MANAGER_ERROR_NONE) && (inst_scr->messager_inst == false)) {
        //Application Exists now, it did not earlier, hence now create the invoker Layout
        //First free the existing resource
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Messenger Has been installed redoing the layout");
        elm_box_clear(inst_scr->mLayout);
        evas_object_del(inst_scr->mLayout);

        //Now create the invoker Screen
        inst_scr->Create_invoker_screen();
        inst_scr->messager_inst = true;
    } else if((ret == APP_MANAGER_ERROR_NO_SUCH_APP) && (inst_scr->messager_inst == true)) {
        //Application Does not exist now, but it existed earlier
        //First free the existing resource, the following shall clear and delete the existing members packed into the box
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Messenger Has been removed redoing the layout");
        elm_box_clear(inst_scr->mLayout);
        evas_object_del(inst_scr->mLayout);

        //Now create the installer Screen
        inst_scr->Create_installer_screen();
        inst_scr->messager_inst = false;
    } else if(((ret == APP_MANAGER_ERROR_NONE) && (inst_scr->messager_inst == true)) || ((ret == APP_MANAGER_ERROR_NO_SUCH_APP) && (inst_scr->messager_inst == false))) {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "No change in Messenger Layout");
    } else {
        dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Not able to fetch data 0x%x", ret);
    }
}

void MessagerScreen::msg_screen_instance_set(MessagerScreen * inst_scr) {
    msg_screen_instance = inst_scr;
}

MessagerScreen *MessagerScreen::msg_screen_instance_get() {
    return msg_screen_instance;
}
