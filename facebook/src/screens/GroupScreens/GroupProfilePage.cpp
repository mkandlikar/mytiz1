#include "Common.h"
#include "ConnectivityManager.h"
#include "DataEventDescription.h"
#include "FbRespondGetGroup.h"
#include "GroupProfilePage.h"
#include "Log.h"
#include "OperationManager.h"
#include "OperationPresenter.h"
#include "Popup.h"
#include "Post.h"
#include "PostPresenter.h"
#include "ProfileWidgetFactory.h"
#include "SearchScreen.h"
#include "WidgetFactory.h"
#include "Utils.h"

GroupProfilePage::GroupProfilePage(const char *id, const char *name) :
        ScreenBase(NULL),
        TrackItemsProxyAdapter( NULL, mFeedProvider = new FeedProvider() ),
        mGroupHeaderWidget(nullptr),
        mGroupProfileLayout(nullptr),
        mGroupFeedWidget(nullptr)
{
    mScreenId = ScreenBase::SID_GROUP_PROFILE_PAGE;
    mId = SAFE_STRDUP(id);
    mName = SAFE_STRDUP(name);

    mGroupActionAndData = NULL;
    mAction = new FeedAction(this);

    mDetailedData = NULL;

    mProfileWidgetExist = false;
    mAllowFetchData = false;

    mFeedProvider->SetEntityId(mId);

    OperationManager::GetInstance()->Subscribe(mFeedProvider);
    OperationManager::GetInstance()->Subscribe(this);

    mLayout = WidgetFactory::CreateBaseScreenLayout(this, getParentMainLayout(), mName ? mName : "", true, true, false);

    ProgressBarShow();
    RequestDetailedData();

    SetIdentifier("GROUP_PROFILE");

    AppEvents::Get().Subscribe(eLIKE_COMMENT_EVENT, this);
    AppEvents::Get().Subscribe(ePOST_MESSAGE_EVENT, this);
    AppEvents::Get().Subscribe(ePOST_UPDATE_ABORTED, this);
    AppEvents::Get().Subscribe(eINTERNET_CONNECTION_CHANGED, this);
    AppEvents::Get().Subscribe(ePOST_NOT_AVAILABLE, this);
    AppEvents::Get().Subscribe(ePOST_DELETED, this);
    AppEvents::Get().Subscribe(ePOST_DELETION_CONFIRMED, this);

    ApplyProgressBar(false);
    SetShowConnectionErrorEnabled(false);
    SetOperationsComparator( TrackItemsProxyAdapter::operation_comparator );

    SetScrollerRegionTopOffset(R->USER_FEED_HEADER_REGION_H);
    // top edge region to automatically fetch items and show in progress shared posts
}

GroupProfilePage::~GroupProfilePage()
{
    free((void*) mId);
    free((void*) mName);

    delete mGroupActionAndData;

    if (mLayout) {
        ProgressBarHide();
    }

    FacebookSession::GetInstance()->ReleaseGraphRequest(mDetailedData);

    EraseWindowData();
    delete mAction;
    OperationManager::GetInstance()->UnSubscribe(this);
    OperationManager::GetInstance()->UnSubscribe(mFeedProvider);
    mFeedProvider->Release();
}

bool GroupProfilePage::HandleBackButton()
{
    bool ret = true;

    if (WidgetFactory::CloseConfirmDialogue()){
    } else if(WidgetFactory::CloseAlertPopup()) {
    } else if (WidgetFactory::CloseErrorDialogue()){
    } else if(PopupMenu::Close()) {
    } else {
        ret = false;
    }

    return ret;
}

void GroupProfilePage::FetchEdgeItems()
{
    Log::info(LOG_FACEBOOK_GROUP, "GroupProfilePage::FetchEdgeItems");
    if (IsGroupProfileWidgetShown() &&
        mFeedProvider->GetPresentersCount()==0) {
        mFeedProvider->ResetRequestTimer();
        if (GetProvider()->GetDataCount()==0) {
            Log::info(LOG_FACEBOOK_GROUP, "GroupProfilePage::FetchEdgeItems->no data in provider, descending order request");
            RequestData(true);
        }
        else {
            Log::info(LOG_FACEBOOK_GROUP, "GroupProfilePage::FetchEdgeItems->request only muted(new) items");
            RequestData(false,true);
        }
    }
}

void GroupProfilePage::Push()
{
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void GroupProfilePage::CreateBaseUI()
{
    ApplyScroller(mLayout, false);
    elm_object_part_content_set(mLayout, "content", GetScroller());

    mGroupProfileLayout = elm_layout_add(GetScroller());
    elm_layout_file_set(mGroupProfileLayout, Application::mEdjPath, "posts.area");
    elm_object_content_set(GetScroller(), mGroupProfileLayout);
    evas_object_show(mGroupProfileLayout);

    mGroupHeaderWidget = elm_box_add(mGroupProfileLayout);
    evas_object_show(mGroupHeaderWidget);
    elm_layout_content_set(mGroupProfileLayout, "static.data.area.swallow", mGroupHeaderWidget);

    mGroupFeedWidget = elm_box_add(mGroupProfileLayout);
    evas_object_show(mGroupFeedWidget);
    elm_layout_content_set(mGroupProfileLayout, "feed.area.swallow", mGroupFeedWidget);
    SetDataArea(mGroupFeedWidget);
}

void GroupProfilePage::RequestDetailedData()
{
    Log::info(LOG_FACEBOOK_GROUP, "GroupProfilePage::RequestData");
    if (mDetailedData == NULL) {
        mDetailedData = FacebookSession::GetInstance()->GetGroupDetails(mId, on_get_group_details_completed, this);
    }
}

void GroupProfilePage::on_get_group_details_completed(void* object, char* respond, int code)
{
    Log::info(LOG_FACEBOOK_GROUP, "GroupProfilePage::on_get_group_details_completed");

    GroupProfilePage *me = static_cast<GroupProfilePage*>(object);

    FacebookSession::GetInstance()->ReleaseGraphRequest(me->mDetailedData);

    if (code == CURLE_OK) {
        Log::info(LOG_FACEBOOK_GROUP, "response: %s", respond);
        FbRespondGetGroup * groupRespond = FbRespondGetGroup::createFromJson(respond);
        if (groupRespond == NULL) {
            Log::error(LOG_FACEBOOK_GROUP, "Error parsing http respond");
            me->ShowErrorWidget(eCONNECTION_ERROR);
        } else {
            if (groupRespond->mError) {
                Log::error(LOG_FACEBOOK_GROUP, "Error getting groups, error = %s", groupRespond->mError->mMessage);
                me->ShowErrorWidget(eNO_DATA_AVAILABLE);
            } else {
                me->CreateBaseUI();
                Group *group = static_cast<Group*>(eina_list_data_get(groupRespond->mGroupsList));
                if (group) {
                    me->mGroupActionAndData = new ActionAndData(group, me->mAction);
                    me->SetData(me->mGroupActionAndData);
                    groupRespond->RemoveDataFromList(group);
                    me->mProfileWidgetExist = true;
                    if(me->mAllowFetchData) {
                        me->ApplyProgressBar(true);
                        me->SetShowConnectionErrorEnabled(true);
                        me->RequestData(true);
                    }
                }
            }
        }
        delete groupRespond;
    } else {
        Log::error(LOG_FACEBOOK_GROUP, "Error sending http request");
        me->ShowErrorWidget(eCONNECTION_ERROR);
    }
    if (respond != NULL) {
        //Attention!! client should take care to free respond buffer
        free(respond);
    }
    me->ProgressBarHide();
}

void GroupProfilePage::SetData(ActionAndData *actionData)
{
    Log::info(LOG_FACEBOOK_GROUP, "GroupProfilePage::SetData");

    if (actionData && actionData->mData) {
        Group *group = static_cast<Group*>(actionData->mData);
        if (group) {
            if ( !mName && group->mName) {
                elm_object_part_text_set(mLayout, "header_text", group->mName);
            }

            ProfileWidgetFactory::CreateCoverItem(mGroupHeaderWidget, actionData);

            if (group->mMembershipState == Group::eMEMBER || group->mMembershipState == Group::eOWNER) { // For group members
                if (group->mPrivacy == Group::eSECRET) {
                    // Secret groups aren't sharable
                    ProfileWidgetFactory::Create3ActionBtsItem(mGroupHeaderWidget, actionData);
                } else {
                    // Public/Closed groups can be shared as well as other users can be invited/added to the group by you
                    ProfileWidgetFactory::Create4ActionBtsItem(mGroupHeaderWidget, actionData);
                }

                if (group->mMembershipState == Group::eOWNER && group->mMembersRequestCount > 0) {
                    Log::info(LOG_FACEBOOK_GROUP, "SetData == GroupOwner");
                    ProfileWidgetFactory::CreateSingleInfoField(mGroupHeaderWidget, ProfileWidgetFactory::FIELD_MEMBER_REQUEST, ICON_DIR "/Groups/group_gear2x.png", i18n_get_text("IDS_GROUP_PROFILE_MEMBER_REQUESTS"), NULL, actionData);
                }

                ProfileWidgetFactory::CreatePostAndPhotoItem(mGroupHeaderWidget, actionData);
                mAllowFetchData = true;
            } else { // Group non-members can Join/cancel a Join request
                if (group->mPrivacy == Group::eSECRET) {
                    // Nothing to be displayed in this scenario as secret groups are inaccessible(through Graph API) to non-group members
                    Log::warning(LOG_FACEBOOK_GROUP, "Unauthorised/Illegal access to Group (%s)  ", group->mName);
                } else {
                    // Members can't be added to Public/Closed groups by you
                    ProfileWidgetFactory::Create3ActionBtsItem(mGroupHeaderWidget, actionData);

                    /* This is UI for friends info in this group
                     ProfileWidgetFactory::CreateSingleInfoField(me->mGroupHeaderWidget, ICON_DIR "/Events/event_location.png", i18n_get_text("IDS_GROUP_PRIVACY_PUBLIC"), "Anyone can find the group and see posts and who's in it.");
                     */

                    if (group->mPrivacy == Group::eCLOSED) {
                        ProfileWidgetFactory::CreateSingleInfoField(mGroupHeaderWidget, ProfileWidgetFactory::FIELD_UNKNOWN, ICON_DIR "/Events/event_location.png", i18n_get_text("IDS_GROUP_PRIVACY_CLOSED"), "Anyone can find the group and see who's in it. Only members can see posts.", NULL);
                    } else if (group->mPrivacy == Group::eOPEN) {
                        ProfileWidgetFactory::CreateSingleInfoField(mGroupHeaderWidget, ProfileWidgetFactory::FIELD_UNKNOWN, ICON_DIR "/Events/event_location.png", i18n_get_text("IDS_GROUP_PRIVACY_PUBLIC"), "Anyone can find the group and see posts and who's in it.", NULL);
                        mAllowFetchData = true;
                    }
                }
            }
        }
    }
}

void* GroupProfilePage::CreateItem( void* dataForItem, bool isAddToEnd )
{
    ActionAndData *data = nullptr;

    if (dataForItem) { //in CreateItem there is no data
        GraphObject *graphObject = static_cast<GraphObject *>(dataForItem);
        if (graphObject->GetGraphObjectType() == GraphObject::GOT_POST) {
            UpdatePost(data, dataForItem, isAddToEnd);
        } else if (graphObject->GetGraphObjectType() == GraphObject::GOT_OPERATION_PRESENTER) {
            OperationPresenter *operation = static_cast<OperationPresenter *>(dataForItem);
            UpdatePresenter(data, operation, isAddToEnd);
        }
    }
    return data;
}

void* GroupProfilePage::UpdateItem( void* dataForItem, bool isAddToEnd )
{
    assert(dataForItem);
    ActionAndData *data = static_cast<ActionAndData*>(dataForItem);
    assert(data->mData);
    GraphObject *object = static_cast<GraphObject*>(data->mData);

    DeleteUIItem( dataForItem );

    if (object->GetGraphObjectType() == GraphObject::GOT_POST) {
        UpdatePost(data, data->mData, isAddToEnd);
    } else if (object->GetGraphObjectType() == GraphObject::GOT_OPERATION_PRESENTER) {
        OperationPresenter *operation = static_cast<OperationPresenter *>(data->mData);
        UpdatePresenter(data, operation, isAddToEnd);
    } else {
        delete data;
        data = nullptr;
    }
    return data;
}

void* GroupProfilePage::UpdateSingleItem( void* dataForItem, void* prevItemPtr )
{
    assert(dataForItem);
    ActionAndData *data = static_cast<ActionAndData*>(dataForItem);
    assert(data->mData);
    GraphObject *object = static_cast<GraphObject*>(data->mData);

    assert(prevItemPtr);

    DeleteUIItem( dataForItem );

    if (object->GetGraphObjectType() == GraphObject::GOT_POST) {
        UpdatePost(data, data->mData, false,  static_cast<Evas_Object*>(prevItemPtr));
    } else {
        delete data;
        data = nullptr;
    }
    return data;
}

//private method with common logic
void GroupProfilePage::UpdatePost(ActionAndData* &data, void* postPtr, bool isAddToEnd, Evas_Object* prevItem) {

    Evas_Object *postWidget = nullptr;
    Post *post = static_cast<Post*>(postPtr);

    Log::info("GroupProfilePage::UpdatePost()->%s item", data ? "Update" : "Create" );

    if( (!post->GetChildId()) ||
          Utils::IsMe(post->GetFromId().c_str()) || //post from me (could be source of shared post)
          Utils::IsMe(post->GetToId()) //post for me (could be source of shared post)
    ) {
        if(!data) {
            data = new ActionAndData(post, mAction);
        }
        if (post->GetParentId()) {
            postWidget = WidgetFactory::CreateAffectedPostBox(GetDataArea(), data);
        } else if (!post->GetPrivacyIcon().empty()) { // workaround in case of no privacy due to Github #804
            postWidget = WidgetFactory::CreateStatusPost(GetDataArea(), data);
        }
        if (postWidget) {
            data->mParentWidget = postWidget;
            if (prevItem) {
                elm_box_pack_after(GetDataArea(), postWidget, prevItem);
            } else if (isAddToEnd) {
                elm_box_pack_end(GetDataArea(), postWidget);
            } else {
                elm_box_pack_start(GetDataArea(), postWidget);
            }
            if (OperationManager::GetInstance()->GetActiveOperationById(post->GetId())) {
                WidgetFactory::SetPostActive(data, false);
            }

            RePackConnectionError();
        } else {
            Log::error(LOG_FACEBOOK_USER, "GroupProfilePage::UpdatePost->No post widget");
            delete data;
            data = nullptr;
        }
    }
}

//private method with common logic
void GroupProfilePage::UpdatePresenter(ActionAndData* &data, OperationPresenter *operation, bool isAddToEnd) {

    Evas_Object *postWidget = nullptr;

    Log::info("GroupProfilePage::UpdatePresenter()->%s item", data ? "Update" : "Create" );

    if(!data) {//in CreateItem there is no data
        data = new ActionAndData(operation, mAction);
    }
    postWidget = WidgetFactory::CreatePostOperation( GetDataArea(), data, isAddToEnd );

    if ( postWidget ) {
        WidgetFactory::UpdatePostOperation(data, nullptr);
        data->mParentWidget = postWidget;
    } else {
        delete data;
        data = nullptr;
    }

    if (isAddToEnd) {
        elm_box_pack_end(GetDataArea(), postWidget);
    } else {
        elm_box_pack_start(GetDataArea(), postWidget);
    }
    RePackConnectionError(true);
}

void GroupProfilePage::Update(AppEventId eventId, void * data)
{
    switch(eventId){
    case eLIKE_COMMENT_EVENT:
        Log::info(LOG_FACEBOOK_GROUP, "GroupProfilePage::Update, eLIKE_COMMENT_EVENT" );
        if (data) {
            ActionAndData * actionData = static_cast<ActionAndData *>(GetItemById(static_cast<char *>(data)));
            if (actionData) {
                WidgetFactory::RefreshPostItemFooter(actionData);
            }
        }
        break;
    case ePOST_MESSAGE_EVENT:
        Log::info(LOG_FACEBOOK_GROUP, "GroupProfilePage::Update, ePOST_MESSAGE_EVENT" );
        if (data) {
            ActionAndData * actionData = static_cast<ActionAndData *>(GetItemById(static_cast<char *>(data)));
            if (actionData) {
                WidgetFactory::SetPostActive(actionData, false);
            }

            if (!ConnectivityManager::Singleton().IsConnected()) {
                Utils::ShowToast(getMainLayout(), i18n_get_text("IDS_LOGIN_CONNECT_ERR"));
            }
        }
        break;
    case eDATA_CHANGES_EVENT:
        Log::info(LOG_FACEBOOK_GROUP, "GroupProfilePage::Update, eDATA_CHANGES_EVENT");
        if(data) {
            DataEventDescription *dataEvent = static_cast<DataEventDescription*>(data);
            if(GetProvider() == dataEvent->GetProvider() ) {
                TrackItemsProxy::Update(eventId, data);
            }
        }
        break;
    case ePOST_UPDATE_ABORTED:
        if (data) {
            ActionAndData *actionData = static_cast<ActionAndData *>(GetItemById(static_cast<char *>(data)));
            if (actionData) {
                WidgetFactory::SetPostActive(actionData, true);
            }
        }
        break;
    case eOPERATION_COMPLETE_EVENT:// event should comes from OperationManager
        Log::info(LOG_FACEBOOK_GROUP, "GroupProfilePage::Update, eOPERATION_COMPLETE_EVENT");
        if (data) {
            Sptr<Operation> oper(static_cast<Operation*>(data));
            if( oper->IsFeedOperation() ) {
                GroupProfilePage::FetchEdgeItems();
            }
        }
        break;
    case ePOST_DELETED:
    case ePOST_DELETION_CONFIRMED:
    case ePOST_NOT_AVAILABLE:
        if (data) {
            Log::info(LOG_FACEBOOK_GROUP, "BaseProfileScreen::Update, %s", eventId == ePOST_NOT_AVAILABLE ? "ePOST_NOT_AVAILABLE" : "ePOST_DELETED");
            FeedProvider* provider = dynamic_cast<FeedProvider*>(GetProvider());
            assert(provider);
            char *postId = static_cast<char*>(data);
            Post* post = provider->GetPostById(postId);
            if (post) {
                if (eventId != ePOST_DELETION_CONFIRMED) {
                    DestroyItemByData(post , TrackItemsProxyAdapter::items_comparator_Obj_vs_ActionAndData);
                }
                if (eventId != ePOST_DELETED) {
                    provider->DeleteItem(post);
                }
            }
        }
        break;
    case eINTERNET_CONNECTION_CHANGED:
        Log::info(LOG_FACEBOOK_GROUP, "GroupProfilePage::Update, eINTERNET_CONNECTION_CHANGED");
        if (ConnectivityManager::Singleton().IsConnected()) {  //connection has been restored
            if(!mProfileWidgetExist) {
                HideErrorWidget();
                ProgressBarShow();
                RequestDetailedData();
            } else if(mAllowFetchData){
                ShowProgressBarWithTimer(IsServiceWidgetOnBottom());
                GroupProfilePage::FetchEdgeItems();
            }
        } else if(mProfileWidgetExist) {
            SetShowConnectionErrorEnabled(true);
            ShowConnectionError(false);
        }
        break;
    default:
        TrackItemsProxy::Update(eventId, data);
        break;
    }
}

const char* GroupProfilePage::GetProfileId()
{
    return mId;
}

void GroupProfilePage::HideHeaderWidget() {
    Log::debug(LOG_FACEBOOK_GROUP, "GroupProfilePage::HideHeaderWidget");
    if (IsGroupProfileWidgetShown()) {
        Log::debug(LOG_FACEBOOK_GROUP, "GroupProfilePage::HideHeaderWidget->Unset group profile layout and compensate height");
        elm_layout_content_unset(mGroupProfileLayout, "static.data.area.swallow");
        evas_object_hide(mGroupHeaderWidget);
        Evas_Coord profileHeight = 0;
        evas_object_geometry_get(mGroupHeaderWidget, nullptr, nullptr, nullptr, &profileHeight);
        ChangeYCoordinate(profileHeight,false);
        SetScrollerRegionTopOffset(0);
    }
}

void GroupProfilePage::ShowHeaderWidget() {
    Log::debug(LOG_FACEBOOK_GROUP, "GroupProfilePage::ShowHeaderWidget" );
    if (!IsGroupProfileWidgetShown()) {
        Log::debug(LOG_FACEBOOK_GROUP, "GroupProfilePage::ShowHeaderWidget->set hidden group profile widget back");
        elm_layout_content_set(mGroupProfileLayout, "static.data.area.swallow",mGroupHeaderWidget);
        evas_object_show(mGroupHeaderWidget);
        Evas_Coord profileHeight = 0;
        evas_object_geometry_get(mGroupHeaderWidget, nullptr, nullptr, nullptr, &profileHeight);
        ChangeYCoordinate(profileHeight,true);
        SetScrollerRegionTopOffset(R->USER_FEED_HEADER_REGION_H);
    }
}

bool GroupProfilePage::IsGroupProfileWidgetShown() {
    return elm_layout_content_get(mGroupProfileLayout, "static.data.area.swallow");
}
