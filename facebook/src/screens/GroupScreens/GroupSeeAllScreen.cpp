#include "ConnectivityManager.h"
#include "Group.h"
#include "GroupProfilePage.h"
#include "GroupProvider.h"
#include "GroupSeeAllScreen.h"
#include "SearchScreen.h"
#include "WebViewScreen.h"
#include "WidgetFactory.h"

const unsigned int GroupSeeAllScreen::DEFAULT_ITEMS_WND_SIZE = 50;

GroupSeeAllScreen::GroupSeeAllScreen() :
        ScreenBase(NULL),
        TrackItemsProxyAdapter(NULL, GroupProvider::GetInstance())
{
    mAction = new FeedAction(this);

    mLayout = WidgetFactory::CreateBaseScreenLayout(this, getParentMainLayout(), i18n_get_text("IDS_SS_GROUPS"), true, true, false);

    ApplyScroller(mLayout, true);
    elm_object_part_content_set(mLayout, "content", GetScroller());

    elm_object_translatable_part_text_set(mLayout, "error_text", "IDS_NO_GROUPS");

    TrackItemsProxy::ProxySettings* settings = GetProxySettings();
    settings->initialWndSize = DEFAULT_ITEMS_WND_SIZE;
    SetProxySettings( settings );

    RequestData();
}

GroupSeeAllScreen::~GroupSeeAllScreen()
{
    delete mAction;
}

void GroupSeeAllScreen::Push()
{
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void* GroupSeeAllScreen::CreateItem( void* dataForItem, bool isAddToEnd )
{
    ActionAndData *data = NULL;
    GraphObject *graphObj = static_cast<GraphObject*>(dataForItem);
    if (graphObj) {
        switch (graphObj->GetGraphObjectType()) {
        case GraphObject::GOT_GROUP: {
            Group *group = static_cast<Group *>(dataForItem);
            if (group) {
                data = new ActionAndData(group, mAction);
                data->mParentWidget = GroupsGet(data, GetDataArea(), isAddToEnd);
            }
        }
            break;
        case GraphObject::GOT_SEPARATOR: {
            data = new ActionAndData(graphObj, mAction);
            data->mParentWidget = CreateSuggestedItem(data, GetDataArea(), isAddToEnd);
        }
            break;
        default:
            break;
        }
    }
    return data;
}

Evas_Object *GroupSeeAllScreen::GroupsGet(void *user_data, Evas_Object *obj, bool isAddToEnd)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    GroupSeeAllScreen *me = static_cast<GroupSeeAllScreen*>(action_data->mAction->getScreen());

    Evas_Object *content = WidgetFactory::CreateSimpleWrapper(obj);
    me->CreateGroupItem(content, action_data);

    if (isAddToEnd) {
        elm_box_pack_end(obj, content);
    } else {
        elm_box_pack_start(obj, content);
    }
    evas_object_show(content);

    return content;
}

void GroupSeeAllScreen::CreateGroupItem(Evas_Object* parent, ActionAndData *action_data)
{
    Group *item = static_cast<Group*>(action_data->mData);

    Evas_Object *list_item = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(parent, "see_all_groups_item");

    if (item->mIcon) {
        Evas_Object *avatar = elm_image_add(list_item);
        elm_object_part_content_set(list_item, "item_icon", avatar);
        action_data->UpdateImageLayoutAsync(item->mIcon, avatar, ActionAndData::EImage, Post::EFromPicturePath);
        evas_object_show(avatar);
    }

    elm_object_part_text_set(list_item, "item_name", item->mName);

    elm_object_signal_callback_add(list_item, "mouse,clicked,*", "item*", on_item_clicked_cb, action_data);
}

Evas_Object *GroupSeeAllScreen::CreateSuggestedItem(void *user_data, Evas_Object *obj, bool isAddToEnd)
{
    Evas_Object *content = WidgetFactory::CreateSimpleWrapper(obj);

    Evas_Object *list_item = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(content, "see_all_groups_item");

    Evas_Object *avatar = elm_image_add(list_item);
    elm_object_part_content_set(list_item, "item_icon", avatar);
    elm_image_file_set(avatar, ICON_SETTINGS_DIR"/suggested_groups.png", NULL);
    evas_object_show(avatar);

    elm_object_translatable_part_text_set(list_item, "item_name", "IDS_SETTING_SUGGESTED_GROUPS");

    elm_object_signal_callback_add(list_item, "mouse,clicked,*", "item*", on_suggested_clicked_cb, NULL);

    if (isAddToEnd) {
        elm_box_pack_end(obj, content);
    } else {
        elm_box_pack_start(obj, content);
    }
    evas_object_show(content);

    return content;
}

void GroupSeeAllScreen::on_item_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    Group *group = static_cast<Group*>(action_data->mData);

    GroupProfilePage *screen = new GroupProfilePage(group->GetId(), group->mName);
    Application::GetInstance()->AddScreen(screen);
}

void GroupSeeAllScreen::on_suggested_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    WebViewScreen::Launch(URL_SUGGESTED_GROUPS, true);
}

void GroupSeeAllScreen::SetDataAvailable(bool isAvailable)
{
    SetNoDataViewVisibility(!isAvailable);
}

void GroupSeeAllScreen::SetNoDataViewVisibility(bool isVisible)
{
    if (ConnectivityManager::Singleton().IsConnected()) {
        if (isVisible) {
            elm_object_signal_emit(mLayout, "error.show", "");
        } else {
            elm_object_signal_emit(mLayout, "error.hide", "");
        }
    }
}

