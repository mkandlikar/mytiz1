#include "AboutApp.h"
#include "Common.h"
#include "Config.h"
#include "ScreenBase.h"
#include "SearchScreen.h"
#include "WebViewScreen.h"

#include <string>

#define LAYOUT "AboutAppLayout"
#define FACEBOOK "Facebook"
#define FBINC FACEBOOK" Inc."
#define RIGHTS_FB_COLOR " <color=#7c90ba>"
#define RIGHTS_COLOR_END "</color> "

AboutApp::AboutApp(ScreenBase *ParentScreen) : ScreenBase(ParentScreen) {
    CreateView();
}

AboutApp::~AboutApp() {
}

void AboutApp::Push() {
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void AboutApp::CreateView() {
    mLayout = elm_layout_add(getParentMainLayout());
    elm_layout_file_set(mLayout, Application::mEdjPath, LAYOUT);
    evas_object_size_hint_weight_set(mLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_show(mLayout);

    elm_object_translatable_part_text_set(mLayout, "HeaderText", "IDS_ABOUT");
    elm_object_translatable_part_text_set(mLayout, "Terms", "IDS_TERMS_OF_SERVICE");
    elm_object_translatable_part_text_set(mLayout, "DataPolicy", "IDS_DATA_POLICY");

    elm_object_signal_callback_add(mLayout, "SignalHeaderBack", "SourceHeaderBack", (Edje_Signal_Cb)header_back_cb, this);
    elm_object_signal_callback_add(mLayout, "SearchClickSignal", "SearchClickSource", (Edje_Signal_Cb)search_cb, this);
    elm_object_signal_callback_add(mLayout, "TermsClickSignal", "TermsClickSource", (Edje_Signal_Cb)terms_cb, this);
    elm_object_signal_callback_add(mLayout, "DPolicyClickSignal", "DPolicyClickSource", (Edje_Signal_Cb)data_policy_cb, this);

    elm_object_translatable_part_text_set(mLayout, "Title", "IDS_FBFORT");
    elm_object_translatable_part_text_set(mLayout, "VersionTitle", "IDS_VERSION");

    char* Version = NULL;
    app_get_version(&Version);
    if (Version) {
        elm_object_part_text_set(mLayout, "Version", Version);
        free(Version);
    }

    std::string TmpStr;
    TmpStr.append(FACEBOOK).append(RIGHTS_FB_COLOR).append("%s").append(RIGHTS_COLOR_END);
    TmpStr.append(FACEBOOK).append(RIGHTS_FB_COLOR).append("%s").append(RIGHTS_COLOR_END);
    TmpStr.append(FBINC).append(RIGHTS_FB_COLOR).append("%s").append(RIGHTS_COLOR_END);

    FRMTD_TRNSLTD_PART_TXT_SET3(mLayout, "Rights", TmpStr.c_str(), "IDS_AND_THE", "IDS_LOGS_ARE_TRADEMARKS_OF", "IDS_RIGHTS_RESERVED");
}

void AboutApp::header_back_cb(void* Data, Evas_Object* Obj, void* EventInfo) {
    AboutApp *Me = static_cast<AboutApp*>(Data);
    Me->Pop();
}

void AboutApp::search_cb(void* Data, Evas_Object* Obj, void* EventInfo) {
    Application::GetInstance()->AddScreen(new SearchScreen(NULL));
}

void AboutApp::terms_cb(void* Data, Evas_Object* Obj, void* EventInfo) {
    std::string TermsWebUrl = URL_FB_MWEB;
    TermsWebUrl.append(URL_TERMS);
    WebViewScreen::Launch(TermsWebUrl.c_str());
}

void AboutApp::data_policy_cb(void* Data, Evas_Object* Obj, void* EventInfo) {
    std::string DPolicyWebUrl = URL_FB_MWEB;
    DPolicyWebUrl.append(URL_DATAPOLICY);
    WebViewScreen::Launch(DPolicyWebUrl.c_str());
}

