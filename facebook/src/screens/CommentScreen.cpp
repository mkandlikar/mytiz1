#include <stdlib.h>
#include <sstream>
#include <assert.h>

#include "ActionBase.h"
#include "AppEvents.h"
#include "CacheManager.h"
#include "CameraRollScreen.h"
#include "Comment.h"
#include "CommentsAction.h"
#include "CommentScreen.h"
#include "CommentsProvider.h"
#include "CommentWidgetFactory.h"
#include "Common.h"
#include "ConnectivityManager.h"
#include "ErrorHandling.h"
#include "FriendTagging.h"
#include "HomeScreen.h"
#include "LikeOperation.h"
#include "Log.h"
#include "OperationManager.h"
#include "OperationPresenter.h"
#include "Popup.h"
#include "PostComposerMediaData.h"
#include "PostScreen.h"
#include "RequestCompleteEvent.h"
#include "SelectedMediaList.h"
#include "SimpleCommentOperation.h"
#include "SoundNotificationPlayer.h"
#include "WidgetFactory.h"
#include "Utils.h"

const unsigned int CommentScreen::DEFAULT_ITEMS_WND_SIZE = 50;
const unsigned int CommentScreen::DEFAULT_INIT_ITEMS_SIZE = 25;
const unsigned int CommentScreen::DEFAULT_ITEMS_STEP_SIZE = 10;


CommentScreen::CommentScreen(ScreenBase *previousScreeen, ActionAndData *data, ScreenIdEnum screenId) :
        ScreenBase(nullptr),
        IUpdateMediaData(),
        TrackItemsProxyCommentsAdapter(nullptr, mCommentsProvider = new CommentsProvider()),
        mIsReadyForPost(false),
        mId(nullptr),
        mMediaDataPath(nullptr),
        mMediaDataThumbnailPath(nullptr),
        mCommentsAction(nullptr),
        mCommentLayout(nullptr),
        mCommentEntry(nullptr),
        mAttachedPhoto(nullptr),
        mTagging(nullptr),
        mInActionNData(nullptr),
        mSelectedActionNData(nullptr),
        mIsPostExistsRequest(nullptr),
        mTopLayout(nullptr),
        mBotLayout(nullptr),
        mIsDataDownloaded(false),
        mParentItemIsPost(false),
        mPreviousScreeen(nullptr)
{
    mInActionNData = data;
    mScreenId = screenId;
    mPreviousScreeen = previousScreeen;
    mPostExist = false;

    mCommentsAction = new CommentsAction(this);
    InitCommentScreen();

    mLayout = CommentWidgetFactory::CreateBaseLayout(getParentMainLayout());
    if (mScreenId == ScreenBase::SID_COMMENTS) {
        elm_object_translatable_part_text_set(mLayout, "error_text", "IDS_NO_COMMENTS");
        AppEvents::Get().Subscribe(eREPLIES_LIST_WAS_MODIFIED, this);
    } else {
        elm_object_translatable_part_text_set(mLayout, "error_text", "IDS_NO_REPLIES");
    }

    if (ConnectivityManager::Singleton().IsConnected()) {
        ProgressBarShow();
        mIsPostExistsRequest = FacebookSession::GetInstance()->IsObjectExists(mId, on_is_post_exists_completed, this);
    } else {
        ShowErrorWidget(ScreenBase::eCONNECTION_ERROR);
    }

    ApplyProgressBar(false);
    SetShowConnectionErrorEnabled(false);

    ProxySettings *settings = GetProxySettings();
    settings->wndSize = DEFAULT_ITEMS_WND_SIZE;
    settings->initialWndSize = DEFAULT_INIT_ITEMS_SIZE;
    settings->wndStep = DEFAULT_ITEMS_STEP_SIZE;//BaseCommentsProvider::REQUEST_COMMENT_LIMIT
    SetProxySettings( settings );

    AppEvents::Get().Subscribe(eREDRAW_EVENT, this);
    AppEvents::Get().Subscribe(eLIKE_COMMENT_EVENT, this);
    AppEvents::Get().Subscribe(ePAUSE_EVENT, this);
    AppEvents::Get().Subscribe(eINTERNET_CONNECTION_CHANGED, this);
    AppEvents::Get().Subscribe(eCOMMENT_WAS_DELETED, this);
    OperationManager::GetInstance()->Subscribe(this);

    Application::GetInstance()->SetScreenOrientationPortrait();
}

void CommentScreen::InitCommentScreen()
{
    if (!mInActionNData) {
        return;
    }
    assert(mInActionNData->mData);

    Post *post = dynamic_cast<Post*>(mInActionNData->mData);

    if (post) {
        Log::info("CommentScreen::InitCommentScreen->parentPostId = %s",post->GetId());
        mParentItemIsPost = true;
        OperationManager::GetInstance()->AddOperation(MakeSptr<LikeOperation<Post>>(post->GetId() ? post->GetId() : "",
                                                                                    Operation::OT_GetLikesSummary,
                                                                                    mInActionNData));
    }

    SetIdentifier(mScreenId == ScreenBase::SID_COMMENTS ? "COMMENT_SCREEN" : "REPLY_SCREEN");

    EnableUpdateParentCache(false);

    if(mInActionNData->mAction) {
        ScreenBase * rootScreen = mInActionNData->mAction->getScreen();

        if (rootScreen->GetScreenId() == ScreenBase::SID_IMAGE_CAROUSEL ||
            rootScreen->GetScreenId() == ScreenBase::SID_IMAGE_POST ) {
            Log::info("CommentScreen::InitCommentScreen->Screen = %s",
                 rootScreen->GetScreenId()==ScreenBase::SID_IMAGE_CAROUSEL ? "Carousel" : "ImagePost" );

            if (post) {
                Log::info("CommentScreen::InitCommentScreen->root is Post");
                mCommentsAction->SetParentId(post->GetId());
                mId = SAFE_STRDUP(post->GetId());
            } else {
                Photo* photo = dynamic_cast<Photo*> (mInActionNData->mData);
                if(photo) {
                    Log::info("CommentScreen::InitCommentScreen->root is Photo");
                    mCommentsAction->SetParentId(photo->GetId());
                } else {
                    Log::info("CommentScreen::InitCommentScreen->impossible type of graph object");
                }
            }
        } else {
            GraphObject * graphObject = mInActionNData->mData;
            if (mScreenId == ScreenBase::SID_REPLY) {// no matter what type of screen is root for replies
                mParentItemIsPost = false;// for replies parent item is always Comment
                Log::info("CommentScreen::InitCommentScreen->Set parent id = %s for Reply screen", graphObject->GetId());
            }

            Log::info("CommentScreen::InitCommentScreen->RootScreen %d", rootScreen->GetScreenId());

            if (post) {
                Log::info("CommentScreen::InitCommentScreen->RootObject type is Post");
            } else if (dynamic_cast<Comment*>(graphObject)) {
                Log::info("CommentScreen::InitCommentScreen->RootObject type is Comment");
            }
            mCommentsAction->SetParentId(graphObject->GetId());
        }
        mId = SAFE_STRDUP(mInActionNData->mData->GetId());
    }

    Log::info(LOG_FACEBOOK_COMMENTING, "CommentScreen, mId = %s %u", mId, mCommentsProvider);
    Log::info(LOG_FACEBOOK_COMMENTING, "CommentScreen, mCommentsProvider =  %u", GetProvider());
    mCommentsProvider->SetEntityId(mId);
    mCommentsProvider->SetReverseChronological(true);
}

CommentScreen::~CommentScreen()
{
    OperationManager::GetInstance()->UnSubscribe(this);

    if (mIsPostExistsRequest) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(mIsPostExistsRequest);
    }

    mCommentsProvider ->Release();

    delete mCommentsAction;

    TrackItemsProxy::EraseWindowData();

    free(mId);
    free(mMediaDataPath);

    evas_object_del(mCommentLayout); mCommentLayout = nullptr;
    evas_object_del(mAttachedPhoto); mAttachedPhoto = nullptr;

    if (mScreenId == ScreenBase::SID_COMMENTS) {
        EnableUpdateParentCache(true);
    }
    CommentWidgetFactory::remove_scroller_events(GetScroller());
    AppEvents::Get().Notify(eVIDEOPLAYER_RESUME_EVENT);
    SelectedMediaList::Clear();
}

void CommentScreen::Push()
{
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set( mNfItem, EINA_FALSE, EINA_FALSE );
}

void CommentScreen::on_is_post_exists_completed(void* object, char* respond, int code)
{
    Log::info(LOG_FACEBOOK_COMMENTING, "CommentScreen::on_is_post_exists_completed");

    CommentScreen *me = static_cast<CommentScreen *>(object);

    if (me) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(me->mIsPostExistsRequest);
        me->ProgressBarHide();

        if (code == CURLE_OK) {
            Log::debug(LOG_FACEBOOK_COMMENTING, "response: %s", respond);
            FbRespondBase * response = FbRespondBase::createFromJson(respond);
            if (response->mError) {
                me->ShowErrorWidget(ScreenBase::eSOMETHING_WRONG_ERROR);
            } else {
                me->mPostExist = true;
                me->CreateUI();
                me->ApplyProgressBar(true);
                me->RequestData(true);
            }
            delete response;
        } else {
            Log::error(LOG_FACEBOOK_COMMENTING, "Error sending http request");
            me->ShowErrorWidget(ScreenBase::eCONNECTION_ERROR);
        }
    }

    //Attention!! client should take care to free respond buffer
    free(respond);
}

void* CommentScreen::CreateItem( void* dataForItem, bool isAddToEnd )
{
    ActionAndData *data = nullptr;
    if (dataForItem) {
        GraphObject *graphObject = static_cast<GraphObject *>(dataForItem);
        if (graphObject->GetGraphObjectType() == GraphObject::GOT_COMMENT) {
            Log::info(LOG_FACEBOOK_OPERATION, "CommentScreen::CreateItem");
            if(FindItem(graphObject, items_comparator_Obj_vs_ActionAndData)){
                Log::error("CommentScreen::CreateItem->item has been already created");
                return nullptr;
            }
            Comment *comment = static_cast<Comment*>(dataForItem);
            data = new ActionAndData(comment, mCommentsAction);
            data->mParentWidget = CommentWidgetFactory::CommentsGet(data, GetDataArea(), isAddToEnd);
            if (!data->mParentWidget) {
                delete data;
                data = nullptr;
            }
            HideProgressBar();
        } else if (graphObject->GetGraphObjectType() == GraphObject::GOT_OPERATION_PRESENTER) {
            Log::info(LOG_FACEBOOK_OPERATION, "CommentScreen::CreateItem - operation");
            OperationPresenter *operation = static_cast<OperationPresenter *>(dataForItem);

            data = new ActionAndData(operation, mCommentsAction);
            data->mParentWidget = CommentWidgetFactory::CreateCommentOperation(GetDataArea(), data, isAddToEnd);
            if (!data->mParentWidget) {
                delete data;
                data = nullptr;
            }
            HideProgressBar();
            RePackConnectionError();
        }
    }

    return data;
}

void* CommentScreen::UpdateItem(void* dataForItem, bool isAddToEnd)
{
    ActionAndData *data = static_cast<ActionAndData*>(dataForItem);
    DeleteUIItem(dataForItem);

    if (data) {
        GraphObject *graphObject = static_cast<GraphObject *>(data->mData);
        if (graphObject->GetGraphObjectType() == GraphObject::GOT_COMMENT) {
            data->mParentWidget = CommentWidgetFactory::CommentsGet(data, GetDataArea(), isAddToEnd);
            if (!data->mParentWidget) {
                delete data;
                data = nullptr;
            }
        } else if (graphObject->GetGraphObjectType() == GraphObject::GOT_OPERATION_PRESENTER) {
            data->mParentWidget = CommentWidgetFactory::CreateCommentOperation(GetDataArea(), data, isAddToEnd);
            if (data->mParentWidget) {
                CommentWidgetFactory::UpdateCommentOperation(data);
            }
            else {
                delete data;
                data = nullptr;
            }
        }
    }

    return data;
}

void CommentScreen::SetDataAvailable( bool isAvailable )
{
    if (isAvailable) {
        Log::info(LOG_FACEBOOK_OPERATION, "CommentScreen::SetDataAvailable->HideNoItemsLayout");
        elm_object_signal_emit(mLayout, "hide", "error");
        ApplyProgressBar(true);
        SetShowConnectionErrorEnabled(true);

        if(!ConnectivityManager::Singleton().IsConnected()) {
            ShowConnectionError(IsServiceWidgetOnBottom());
        }
    } else if (IsEmpty()) {
        Log::info(LOG_FACEBOOK_OPERATION, "CommentScreen::SetDataAvailable->ShowNoItemsLayout");
        elm_object_signal_emit(mLayout, "show", "error");//show "no comments yet"
        HideConnectionError();//hide connection error if it is presented on screen
        ApplyProgressBar(false);
        SetShowConnectionErrorEnabled(false);
    }
}

void CommentScreen::RemoveMediaPath()
{
    free(mMediaDataPath);
    mMediaDataPath = nullptr;
    free(mMediaDataThumbnailPath);
    mMediaDataThumbnailPath = nullptr;
    SelectedMediaList::Clear();
}

void CommentScreen::CreateUI()
{
    Log::info(LOG_FACEBOOK_OPERATION, "CommentScreen::CreateUI");
    if (mScreenId == ScreenBase::SID_COMMENTS) {
        mTopLayout = CommentWidgetFactory::CreateTopLikeLayout(mLayout);
        CommentWidgetFactory::RefreshTopLikeLayout(mInActionNData, mTopLayout);
        elm_object_signal_callback_add(mTopLayout, "mouse,clicked,*", "like_btn", ActionBase::on_like_btn_clicked_cb, mInActionNData);
    } else {
        mTopLayout = CommentWidgetFactory::CreateTopBackLayout(mLayout);
        elm_object_signal_callback_add(mTopLayout, "mouse,clicked,*", "back_*", back_btn_cb, this);
    }
    mBotLayout = CommentWidgetFactory::CreateBotLayout(mLayout);

    ApplyScroller(mLayout, false);
    CommentWidgetFactory::add_scroller_events(GetScroller());
    ApplyProgressBar(false);
    CreateLayout();
    SetDataArea(mCommentLayout);

    mCommentEntry = CommentWidgetFactory::CreateEntry(mLayout, mScreenId);

    CommentWidgetFactory::RefreshBotLayout(mLayout, mBotLayout, false, false);

    elm_object_signal_callback_add(mBotLayout, "mouse,clicked,*", "photo_btn", on_add_photo_button_clicked_cb, this );
    elm_object_signal_callback_add(mBotLayout, "mouse,clicked,*", "send_btn", send_comment_cb, this);
    elm_object_signal_callback_add(mBotLayout, "mouse,clicked,*", "unattach_btn", on_unattach_btn_clicked_cb, this);

    evas_object_smart_callback_add(mCommentEntry, "changed", text_changed_cb, this);
    evas_object_smart_callback_add(mCommentEntry, "press", show_input_text_panel, this);

    mTagging = new FriendTagging(mLayout, mCommentEntry);
    mTagging->SetUpsideDownList(true);
}

void CommentScreen::CreateLayout()
{
    mCommentLayout = elm_box_add(GetScroller());
    if (mCommentLayout) {
        evas_object_size_hint_weight_set(mCommentLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
        evas_object_size_hint_align_set(mCommentLayout, -1, -1);

        elm_box_padding_set(mCommentLayout, 0, 0);
        elm_object_content_set(GetScroller(), mCommentLayout);
        elm_object_part_content_set(mLayout, "content", GetScroller());
    }
}

void CommentScreen::PostComment()
{
    bundle* paramsBundle;
    paramsBundle = bundle_create();
    if (mTagging) {
       if (mTagging->IsLongText(MAX_COMMENT_MESSAGE_LENGTH)) {
           bundle_free(paramsBundle);
           return;
        }
        char *originTextToPost = mTagging->GetStringToPost(true);
        bundle_add_str(paramsBundle, "origin_message", originTextToPost);
        free(originTextToPost);
        char *textToPost = mTagging->GetStringToPost(false);
        if (textToPost && *textToPost != '\0') {
            bundle_add_str(paramsBundle, "message", textToPost);
        }
        free(textToPost);
    }

    Operation::OperationType type;
    type = (mScreenId == ScreenBase::SID_REPLY) ? Operation::OT_Add_Reply : Operation::OT_Add_Comment;

    OperationManager::GetInstance()->AddOperation(MakeSptr<SimpleCommentOperation>(type, mId ? mId : "", mParentItemIsPost , paramsBundle, mMediaDataPath, mMediaDataThumbnailPath));
    SoundNotificationPlayer::PlayNotification(SoundNotificationPlayer::eSoundPostMain);

    bundle_free(paramsBundle);

    elm_entry_entry_set(mCommentEntry, "");
    elm_object_focus_set(mCommentEntry, EINA_FALSE);
    elm_entry_input_panel_hide(mCommentEntry);

    DeleteMedia();
}

void CommentScreen::DeleteMedia()
{
    if (SelectedMediaList::Count() == 1) {
        if (mAttachedPhoto) {
            evas_object_del(mAttachedPhoto);
            mAttachedPhoto = nullptr;
        }
        SelectedMediaList::Clear();
        EnableSendButton();
    }
    RemoveMediaPath();
}

void CommentScreen::UpdateMedia()
{
    Log::info(LOG_FACEBOOK_COMMENTING, "CommentScreen::UpdateMedia");

    Eina_List *mediaList = SelectedMediaList::Get();
    if (eina_list_count(mediaList)) {
        PostComposerMediaData * mediaData = static_cast<PostComposerMediaData *>(eina_list_data_get(mediaList));

        mAttachedPhoto = elm_image_add(mBotLayout);
        elm_object_part_content_set(mBotLayout, "attached_img", mAttachedPhoto);
        elm_image_file_set(mAttachedPhoto, mediaData->GetThumbnailPath(), nullptr);
        evas_object_show(mAttachedPhoto);

        free(mMediaDataPath);
        mMediaDataPath = SAFE_STRDUP(mediaData->GetFilePath());
        free(mMediaDataThumbnailPath);
        mMediaDataThumbnailPath = SAFE_STRDUP(mediaData->GetThumbnailPath());
        EnableSendButton();
    }
}

const char* CommentScreen::GetId() const
{
    return mId;
}

Evas_Object* CommentScreen::GetCommentEntry() const
{
    return mCommentEntry;
}

Evas_Object* CommentScreen::GetAttachedPhoto() const
{
    return mAttachedPhoto;
}

void CommentScreen::on_unattach_btn_clicked_cb(void *data, Evas_Object *obj, const char *emisson, const char *source)
{
    Log::info(LOG_FACEBOOK_COMMENTING, "CommentScreen::on_unattach_btn_clicked_cb");
    CommentScreen *screen = static_cast<CommentScreen*>(data);
    if (screen) {
        screen->DeleteMedia();
    }
}

void CommentScreen::on_add_photo_button_clicked_cb(void *data, Evas_Object *obj, const char *emisson, const char *source)
{
    Log::info(LOG_FACEBOOK_COMMENTING, "CommentScreen::on_add_photo_button_clicked_cb");
    if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_CAMERA_ROLL) {
        CommentScreen *screen = static_cast<CommentScreen *>(data);
        if (screen) {
            elm_entry_input_panel_hide(screen->mCommentEntry);
            CameraRollScreen* cameraRollScreen = new CameraRollScreen(eCAMERA_ROLL_COMMENT_MODE, screen);
            Application::GetInstance()->AddScreen(cameraRollScreen);
        }
    }
}

void CommentScreen::send_comment_cb(void *data, Evas_Object *obj, const char *emisson, const char *source)
{
    CommentScreen *screen = static_cast<CommentScreen*>(data);
    CHECK_RET_NRV(screen);
    if (screen->mIsReadyForPost) {
        if (screen->mTagging) {
            screen->mTagging->HideFriendsPopup();
        }
        // To prevent user to refresh data during PostComment operation.
        screen->CheckOffStageItemsBelow();
        screen->GetProvider()->UpdateTimerToCurrent();
        screen->PostComment();
    }
}

void CommentScreen::back_btn_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    CommentScreen *me = static_cast<CommentScreen*>(data);
    if (me) {
        if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_COMMENTS) {
            Log::info(LOG_FACEBOOK_OPERATION, "CommentScreen::back_btn_cb");
            me->Pop();
        }
    }
}

void CommentScreen::text_changed_cb(void *data, Evas_Object *obj, void *event_info) {
    CommentScreen *me = static_cast<CommentScreen *>(data);
    assert(me);
    me->EnableSendButton();
}

void CommentScreen::EnableSendButton() {
    char *CommentText = elm_entry_markup_to_utf8(elm_entry_entry_get(mCommentEntry));
    bool isTextExist = !Utils::isEmptyString(CommentText);
    free(CommentText);

    bool isPhotoExist = GetAttachedPhoto();
    mIsReadyForPost = isTextExist || isPhotoExist;

    CommentWidgetFactory::RefreshBotLayout(mLayout, mBotLayout, isTextExist, isPhotoExist);
}

void CommentScreen::show_input_text_panel(void *data, Evas_Object *obj, void *event_info)
{
    CommentScreen *screen = static_cast<CommentScreen *>(data);
    if (screen) {
        elm_entry_cnp_mode_set(screen->mCommentEntry, ELM_CNP_MODE_PLAINTEXT);
        elm_entry_input_panel_show(screen->mCommentEntry);
    }
}

bool CommentScreen::HandleBackButton()
{
    bool ret = true;
    if (PopupMenu::Close()) {
    } else if (CommentWidgetFactory::CloseCommentPopup()) {
    } else if (mTagging && mTagging->HideFriendsPopup()) {
    } else if (mIsDataDownloaded) {
        ret = false;
    } else {
        DeleteMedia(); // Clear the selected media if any
        ret = false;
    }
    return ret;
}

bool CommentScreen::HandleKeyPadHide()
{
    Log::info(LOG_FACEBOOK_TAGGING, "CommentScreen::HandleKeyPadHide");

    bool ret = true;
    if (mTagging && mTagging->HideFriendsPopup()) {
        Utils::SetKeyboardFocus(mCommentEntry, EINA_TRUE);
        elm_entry_input_panel_show(mCommentEntry);
    } else {
        ret = false;
    }
    return ret;
}

void CommentScreen::Update(AppEventId eventId, void * data)
{
    if (IS_OPERATION_EVENT(eventId)) {
        if (eventId == eOPERATION_REMOVED_EVENT && data) {
            Sptr<SimpleCommentOperation> oper(static_cast<SimpleCommentOperation*>(data));
            if (oper->GetType() == Operation::OT_Delete_Comment ||
                oper->GetType() == Operation::OT_Delete_Reply) {
                SetDataAvailable(GetProvider()->GetDataCount()>0);
            }
        }
    } else {
        switch (eventId) {
        case eREQUEST_COMPLETED_EVENT: {
            RequestCompleteEvent *eventData = static_cast<RequestCompleteEvent*>(data);
            if (eventData && eventData->mCurlCode == CURLE_OK) {
                mIsDataDownloaded = true;
            }
        }
            break;
        case eLIKE_COMMENT_EVENT: {
            CommentWidgetFactory::RefreshTopLikeLayout(mInActionNData, mTopLayout);
        }
            break;
        case ePAUSE_EVENT: {
            PopupMenu::Close();
            CommentWidgetFactory::CloseCommentPopup();
        }
            break;
        case eINTERNET_CONNECTION_CHANGED:
            if (ConnectivityManager::Singleton().IsConnected()) {  //connection has been restored
                if (!mPostExist) {
                    HideErrorWidget();
                    OnErrorWidgetRetryClickedCb();
                }
                else {
                    if ( eina_list_count(GetProvider()->GetOperationPresenters()) == 0 &&
                          !IsOffStageItemsBelow() ) { //fetch new comments if no items in progress and no comments below scroller's bottom
                        Log::info(LOG_FACEBOOK, "CommentScreen::Update comments provider contains %u items",GetProvider()->GetDataCount());
                        if ( GetProvider()->GetDataCount()==0 || IsServiceWidgetOnBottom() ) {
                            Log::info(LOG_FACEBOOK, "CommentScreen::Update->request new bottom comments");
                            GetProvider()->ResetRequestTimer();
                            RequestData(true);
                        }
                    }
                    HideConnectionError();
                }
            } else {
                ShowConnectionError(IsServiceWidgetOnBottom());
            }
            break;
        case eREPLIES_LIST_WAS_MODIFIED:
            if(data) {
                Comment* comment = static_cast<Comment*>(data);
                GraphObject* oldGraphObject = static_cast<GraphObject*>(GetProvider()->GetGraphObjectById(comment->GetId()));
                if(oldGraphObject) {
                    ActionAndData* actionData = static_cast<ActionAndData*>(FindItem(oldGraphObject, items_comparator_Obj_vs_ActionAndData));
                    bool isModified = mCommentsProvider->ReplaceCommentObject(comment);
                    if(ActionAndData::IsAlive(actionData) && isModified) {
                        actionData->ReSetData(comment);
                        evas_object_del(actionData->mReplyActionObject);
                        Evas_Object* content = elm_layout_content_get (actionData->mParentWidget, "content");
                        if(content) {
                            CommentWidgetFactory::ReplyItem(content, actionData);
                        }
                    } else {
                        Log::error("CommentScreen::Update->Can't refresh reply widget");
                    }
                }
            }
            break;
        case eCOMMENT_WAS_DELETED:
            if(data) {
                const char* id = static_cast<char*>(data);
                Comment* commentToDelete = static_cast<Comment*>(GetProvider()->GetGraphObjectById(id));
                if (commentToDelete) {
                    commentToDelete->AddRef();
                    DestroyItemByData(commentToDelete , TrackItemsProxyAdapter::items_comparator_Obj_vs_ActionAndData);
                    GetProvider()->DeleteItem(commentToDelete);
                    commentToDelete->Release();
                }
            }
            break;
        default:
            break;
        }
        TrackItemsProxy::Update(eventId, data);
    }
}

void CommentScreen::DeleteItem( void* item ) {
    if(mSelectedActionNData == item) {
        mSelectedActionNData = nullptr;
    }
    TrackItemsProxyAdapter::DeleteItem(item);
}

void CommentScreen::SetSelectedActionNData(ActionAndData *selectedActionNData)
{
    mSelectedActionNData = selectedActionNData;
}

ActionAndData *CommentScreen::GetSelectedActionNData()
{
    return mSelectedActionNData;
}

void CommentScreen::OnResume()
{
    elm_object_focus_set(mCommentEntry, EINA_FALSE);
    elm_entry_input_panel_hide(mCommentEntry);
    CommentWidgetFactory::add_scroller_events(GetScroller());
    ScreenBase::OnResume();
}

void CommentScreen::OnErrorWidgetRetryClickedCb()
{
    ProgressBarShow();
    mIsPostExistsRequest = FacebookSession::GetInstance()->IsObjectExists(mId, on_is_post_exists_completed, this);
}

void CommentScreen::EnableUpdateParentCache( bool isEnable )
{
    switch (mPreviousScreeen->GetScreenId()) {
    case ScreenBase::SID_HOME_SCREEN: {
        HomeScreen *screen = dynamic_cast<HomeScreen*>(mPreviousScreeen);
        if (screen) {
            screen->SetUpdateCacheEnable(isEnable);
            if (isEnable && ConnectivityManager::Singleton().IsConnected()) {
                screen->UpdateCachedData();
                screen->RequestData();
            }
        }
    }
        break;
    default:
        break;
    }
    // now, there is no reason to extend screens list for such action. Maybe in future, we add.
}
