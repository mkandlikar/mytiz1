#include "ActionBase.h"
#include "DataUploader.h"
#include "Log.h"
#include "Photo.h"
#include "PhotoGalleryProvider.h"
#include "PhotoGalleryScreenBase.h"
#include "Utils.h"


#include <cassert>

static const char* LogTag = "PhotoGalleryStrategyBase";


/*
 * Class AsyncImageLayoutUpdater
 */
PhotoGalleryScreenBase::AsyncImageLayoutUpdater::AsyncImageLayoutUpdater(AsyncImageUpdatersManager &manager, ImageFile &image, Evas_Object *layout) :
        ImageFileDownloader(image), mManager(manager), mLayout(layout) {
}

void PhotoGalleryScreenBase::AsyncImageLayoutUpdater::ImageDownloaded(bool result) {
    if (result && mLayout) {
        assert(GetImage().GetPath());
        evas_object_image_file_set(mLayout, GetImage().GetPath(), nullptr);
    }
    mManager.LayoutUpdated(this);
}


/*
 * Class AsyncImageUpdatersManager
 */
const unsigned int PhotoGalleryScreenBase::AsyncImageUpdatersManager::MAX_ACTIVE_IMAGE_DOWNLOADS = 10;


PhotoGalleryScreenBase::AsyncImageUpdatersManager::AsyncImageUpdatersManager() : mUpdaters(nullptr), mIsPaused(false) {
}

PhotoGalleryScreenBase::AsyncImageUpdatersManager::~AsyncImageUpdatersManager() {
    void *data;
    EINA_LIST_FREE(mUpdaters, data) {
        AsyncImageLayoutUpdater *updater = static_cast<AsyncImageLayoutUpdater*>(data);
        assert(updater);
        delete updater;
    }
}

void PhotoGalleryScreenBase::AsyncImageUpdatersManager::Resume() {
    mIsPaused = false;
    Continue();
}

void PhotoGalleryScreenBase::AsyncImageUpdatersManager::Continue() {
    static const int middleScreen = R->SCREEN_SIZE_HEIGHT/2;

    bool isNext = true;
    unsigned int activeDownloadsCount = 0;

    while (!mIsPaused && isNext && activeDownloadsCount < MAX_ACTIVE_IMAGE_DOWNLOADS) {
        AsyncImageLayoutUpdater *next = nullptr;
        //we must initialize lastOffsetY with big enough value.
        unsigned int lastOffsetY = R->SCREEN_SIZE_HEIGHT*100;
        activeDownloadsCount = 0;

        const Eina_List *list;
        const Eina_List *l_next;
        void *data;
        EINA_LIST_FOREACH_SAFE(mUpdaters, list, l_next, data) {
            AsyncImageLayoutUpdater *downloader = static_cast<AsyncImageLayoutUpdater*>(data);
            assert(downloader);
            if (ImageFile::EDownloading != downloader->GetImage().GetStatus()) {
                if (downloader->GetLayout()) {
                    Evas_Coord x, y;
                    evas_object_geometry_get(downloader->GetLayout(), &x, &y, NULL, NULL);
                    if (x >= 0 || -x < R->SCREEN_SIZE_WIDTH) { //if x < 0 and offset more than screen width then this is invisible layout.
                        unsigned int offsetY = abs(y - middleScreen);
                        if (offsetY < lastOffsetY) {
                            lastOffsetY = offsetY;
                            next = downloader;
                        }
                    }
                }
            } else { // if status is ImageFile::EDownloading
                // if we already have MAX_ACTIVE_IMAGE_DOWNLOADS then no more downloads.
                if (++activeDownloadsCount >= MAX_ACTIVE_IMAGE_DOWNLOADS) {
                    next = nullptr;
                    break;
                }
            }
        }
        if (next) {
            if (ImageFile::EDownloaded == next->GetImage().GetStatus()) {
                assert(next->GetImage().GetPath());
                assert(next->GetLayout());
                evas_object_image_file_set(next->GetLayout(), next->GetImage().GetPath(), nullptr);
                mUpdaters = eina_list_remove(mUpdaters, next);
                delete next;
            } else {
                next->Download();
            }
        } else { //nothing to download
            isNext = false;
        }
    }
}

void PhotoGalleryScreenBase::AsyncImageUpdatersManager::LayoutUpdated(AsyncImageLayoutUpdater *updater) {
    assert(updater);
    mUpdaters = eina_list_remove(mUpdaters, updater);
    delete updater;

    Continue();
}

void PhotoGalleryScreenBase::AsyncImageUpdatersManager::UpdateLayout(ImageFile *image, Evas_Object *layout) {
    assert(image);
    assert(layout);

    //if image is already downloaded then just update layout and return.
    if (ImageFile::EDownloaded == image->GetStatus()) {
        assert(image->GetPath());
        evas_object_image_file_set(layout, image->GetPath(), nullptr);
        return;
    }

    AsyncImageLayoutUpdater *newUpdater = new AsyncImageLayoutUpdater(*this, *image, layout);

    const char*newImageUrl = image->GetUrl();
    assert(newImageUrl);
    AsyncImageLayoutUpdater *existentUpdater = nullptr;

    const Eina_List *list;
    void *data;
    EINA_LIST_FOREACH(mUpdaters, list, data) {
        AsyncImageLayoutUpdater *updater = static_cast<AsyncImageLayoutUpdater*>(data);
        assert(updater);
        if (image == &(updater->GetImage())) {
            existentUpdater = updater;
        }
    }

//if downloader for this url is already existent then replace it with new one. This is needed because layout is re-created.
    if (existentUpdater) {
        delete existentUpdater;
        mUpdaters = eina_list_remove(mUpdaters, existentUpdater);
    }

    mUpdaters = eina_list_append(mUpdaters, newUpdater);
    Continue();
}


/*
 * Class GalleryItem
 */
GalleryItem::GalleryItem() : mActionAndDates(NULL), mLayout(NULL) {
}

GalleryItem::~GalleryItem() {
    void *listData;
    EINA_LIST_FREE(mActionAndDates, listData) {
        delete (static_cast<ActionAndData *> (listData));
    }
}

void GalleryItem::AppendActionAndData(ActionAndData *actionAndData) {
    mActionAndDates = eina_list_append(mActionAndDates, actionAndData);
}

Eina_List *GalleryItem::GetActionAndDates() const {
    return mActionAndDates;
}

Evas_Object *GalleryItem::GetLayout() const {
    return mLayout;
}

void GalleryItem::SetLayout(Evas_Object *layout) {
    mLayout = layout;
}


/*
 * Class PhotoGalleryStrategyBase
 */
void PhotoGalleryStrategyBase::SetScreen(PhotoGalleryScreenBase *screen) {
    assert(screen);
    mScreen = screen;
}


/*
 * Class PhotoGalleryScreenBase
 */
const unsigned int PhotoGalleryScreenBase::DEFAULT_ITEMS_WND_SIZE = 75;
const unsigned int PhotoGalleryScreenBase::DEFAULT_ITEMS_STEP_SIZE = 20;

PhotoGalleryScreenBase::PhotoGalleryScreenBase(ScreenBase *parentScreen, PhotoGalleryProvider* provider, PhotoGalleryStrategyBase *strategy) :
             ScreenBase(parentScreen), TrackItemsProxy(getParentMainLayout(), provider, go_item_comparator),
             mStrategy(strategy), mTopListLayout(NULL), mIsTopListLayoutSet(false), mBottomListLayout(NULL) {
    Log::info_tag(LogTag, "PhotoGalleryScreenBase: create Photo Gallery screen." );
    if (mStrategy) {
        mStrategy->SetScreen(this);
    }
    mAction = new ActionBase(this);
    ProxySettings *settings = GetProxySettings();
    assert(settings);
    settings->wndSize = DEFAULT_ITEMS_WND_SIZE;
    settings->wndStep = DEFAULT_ITEMS_STEP_SIZE;
    SetProxySettings(settings);

    mDataDownloader = new DataUploader(provider, on_download_completed, this);
    mDataDownloader->UpdateErase();
    provider->Subscribe(mDataDownloader);

    SetShowConnectionErrorEnabled(false);
}

PhotoGalleryScreenBase::~PhotoGalleryScreenBase() {
    Log::info_tag(LogTag, "PhotoGalleryScreenBase: destroy PhotoGalleryScreenBase screen." );

    delete mDataDownloader;

    delete mAction;

    delete mStrategy;
}

const char *PhotoGalleryScreenBase::SelectBestLayoutForItem(GalleryItem *item) {
    int imagesCount = eina_list_count(item->GetActionAndDates());
    if (imagesCount == 1) {
        return "gallery_photos_1";
    } else if (imagesCount == 2) {
        return "gallery_photos_2";
    } else if (imagesCount == 3) {
        ActionAndData *firstData = static_cast<ActionAndData *> (eina_list_nth(item->GetActionAndDates(), 0));
        Photo *firstPhoto = static_cast<Photo*> (firstData->mData);
        int hash = firstPhoto->GetHash();
        if (hash % 7 == 0) {
            return "gallery_photos_3_1";
        } else if (hash % 6 == 0) {
            return "gallery_photos_3_2";
        } else {
            return "gallery_photos_3_0";
        }
    }
    return NULL;
}

void PhotoGalleryScreenBase::SetTopListLayout(Evas_Object *topListLayout) {
    mTopListLayout = topListLayout;
}

void PhotoGalleryScreenBase::SetBottomListLayout(Evas_Object *bottomListLayout) {
    mBottomListLayout = bottomListLayout;
}

void* PhotoGalleryScreenBase::CreateItem(void* dataForItem, bool isAddToEnd) {
    GalleryItem *item = static_cast<GalleryItem *> (dataForItem);

    if (!item->GetActionAndDates() && mTopListLayout) {
//Workaround for mTopListLayout.
        item->SetLayout(mTopListLayout);
        evas_object_show(mTopListLayout);
    } else {
        Log::info_tag(LogTag, "Create item with %d photos", eina_list_count(item->GetActionAndDates()));
        Evas_Object *itemLayout = elm_layout_add(GetDataArea());
        elm_object_mirrored_automatic_set(itemLayout, EINA_FALSE);
        elm_object_mirrored_set(itemLayout, EINA_FALSE);

        item->SetLayout(itemLayout);

        const char *layout = SelectBestLayoutForItem(item);
        if (!layout) {
            Log::error_tag(LogTag, "Can't select layout! Probably number of photos in the item is wrong.");
            return NULL;
        }
        elm_layout_file_set(item->GetLayout(), Application::mEdjPath, layout);
        evas_object_show(item->GetLayout());

        int i = 0;
        Eina_List *list;
        void *data;
        EINA_LIST_FOREACH(item->GetActionAndDates(), list, data ) {
            ActionAndData *actionAndData = static_cast<ActionAndData*> (data);
            Photo *photo = static_cast<Photo*> (actionAndData->mData);
            Evas_Object *post_photo = evas_object_image_add(evas_object_evas_get(itemLayout));
            //ToDo: probably set a placeholder image while photos are downloading
            evas_object_size_hint_weight_set(post_photo, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
            evas_object_size_hint_align_set(post_photo, EVAS_HINT_FILL, EVAS_HINT_FILL);
            evas_object_show(post_photo);

            char part[32];
            Utils::Snprintf_s(part, 32, "swallow.photo_%d", i++);
            elm_object_part_content_set(item->GetLayout(), part, post_photo);
            elm_object_signal_callback_add(item->GetLayout(), "mouse,clicked,*", part, on_photo_clicked, actionAndData);

            int swallowW, swallowH;
            edje_object_part_geometry_get(elm_layout_edje_get(item->GetLayout()), part, NULL, NULL, &swallowW, &swallowH);

            Log::info_tag(LogTag, "PhotoGalleryScreenBase: Photo %s width %d height %d", photo->GetFileName(), swallowW, swallowH);
            evas_object_image_load_size_set(post_photo, swallowW, swallowH);
            Utils::CropImageToFill(post_photo, swallowW, swallowH, photo->GetWidth(), photo->GetHeight());
            //The following method shall download the optimum or the nearest to optimum picture for the device. The smaller or optimum image shall
            //help to reduce the time taken to download image and process it for scale up or down. The parameters for the function define the area
            //for which the image needs to be downloaded.
//            actionAndData->UpdateImageLayoutAsync(photo->GetOptimumResolutionPicture(swallowW,swallowH), post_photo, ActionAndData::EEvasImage);
            // Downloading two different resolution images. One is low resolution of around 100 pixels width & 150 pixels height.
            // Second image is High Resolution image, which has nearest resolution to the device resolution ( either 720 X 480, 1080 X 720 resolution )
            // The Heigh resolution image is used to display in the Image Carrousel screen, when Opened in Full Screen.
            // The Low Resolution picture is used as preview in the Uploads/PhotosOfYou/Albums Tab.

            const unsigned int resReducingFactor = 5;
            ImageFile *thumbnail = photo->GetOptimumResolutionPicture((Application::device_width)/resReducingFactor, (Application::device_height)/resReducingFactor);
            if (thumbnail) {
                mImageUpdatersManager.UpdateLayout(thumbnail, post_photo);
            }
        }
    }

    if (isAddToEnd) {
        elm_box_pack_end(GetDataArea(), item->GetLayout());
    } else {
        elm_box_pack_start(GetDataArea(), item->GetLayout());
    }

    return item;
}


void PhotoGalleryScreenBase::DeleteItem(void* itemData) {
    GalleryItem *item = static_cast<GalleryItem *> (itemData);
    elm_box_unpack(GetDataArea(), item->GetLayout());
    if (item->GetActionAndDates()) {
//Workaround for mTopListLayout.
        evas_object_del(item->GetLayout());
    } else {
        evas_object_hide(item->GetLayout());
    }
}

ItemSize* PhotoGalleryScreenBase::GetItemSize(void* itemData) {
    GalleryItem *item = static_cast<GalleryItem *> (itemData);
    Evas_Coord weight, height;
    evas_object_geometry_get(item->GetLayout(), NULL, NULL, &weight, &height);

    ActionAndData *firstData = static_cast<ActionAndData *> (eina_list_nth(item->GetActionAndDates(), 0));
    if (firstData) {
        Photo *firstPhoto = static_cast<Photo*> (firstData->mData);
        return new ItemSize(strdup(firstPhoto->GetId()), height, weight);
    } else {
//Workaround for mTopListLayout.
        return new ItemSize(strdup("mTopListLayout"), height, weight);
    }
}

void PhotoGalleryScreenBase::HideItem( void *itemData )
{
    GalleryItem *item = static_cast<GalleryItem *> (itemData);
    evas_object_hide( item->GetLayout() );
    elm_box_unpack( GetDataArea(), item->GetLayout() );
}

void PhotoGalleryScreenBase::ShowItem( void *itemData )
{
    GalleryItem *item = static_cast<GalleryItem *> (itemData);
    elm_box_pack_start( GetDataArea(), item->GetLayout() );
    evas_object_show( item->GetLayout() );
}

void PhotoGalleryScreenBase::DeleteAll_Prepare() {
    if(GetDataArea()) {
        if (mTopListLayout) {
            elm_box_unpack(GetDataArea(), mTopListLayout);
        }
        elm_box_clear(GetDataArea());
    }
}

ItemSize* PhotoGalleryScreenBase::SaveSize( void *item ) {
    ActionAndData *data = NULL;
    GalleryItem *galleryItem = static_cast<GalleryItem *> (item);
    if (galleryItem->GetActionAndDates()) {
        data = static_cast<ActionAndData *>(eina_list_nth(galleryItem->GetActionAndDates(), 0));
    }
    GraphObject *object = data ? static_cast<GraphObject*>(data->mData) : NULL;
    ItemSize *size = object ? static_cast<ItemSize *>(eina_hash_find( m_ItemSizeCache, object->GetId())) : NULL;
    if(!size){
        size = GetItemSize( item );
        if ( size ) {
            if (( size->height != 0 ) && ( size->weight != 0 ) ) {
                eina_hash_add( m_ItemSizeCache, size->key, size );
            } else {
                delete_cached_item_size(size);
                size = NULL;
            }
        }
    }
    return size;
}

ItemSize* PhotoGalleryScreenBase::GetSize( void *item ) {
    ItemSize *result = NULL;
    ActionAndData *data = NULL;
    GalleryItem *galleryItem = static_cast<GalleryItem *> (item);
    if (galleryItem->GetActionAndDates()) {
        data = static_cast<ActionAndData *>(eina_list_nth(galleryItem->GetActionAndDates(), 0));
    }
    if(data) {
        GraphObject *object = static_cast<GraphObject*>(data->mData);
        if(object) {
            result = static_cast<ItemSize* > ( eina_hash_find( m_ItemSizeCache, object->GetId() ) );
        }
    }
    return result;
}

void PhotoGalleryScreenBase::ArrangeItems(const Eina_List *photos, Eina_Array *items, void *userData) {
    PhotoGalleryScreenBase *me = static_cast<PhotoGalleryScreenBase *> (userData);
    GalleryItem *item = NULL;
    unsigned int photosCount = 0;
    unsigned int photosTotal = eina_list_count(photos);

//Workaround for mTopListLayout. If this is 1st portion of data (count == 0) then create fake item with empty list for the mTopListLayout.
    if (me->mTopListLayout) {
        Eina_Array* array = PhotoGalleryProvider::GetInstance()->GetData();
        if (eina_array_count(array) == 0) {
            item = new GalleryItem();
            eina_array_push(items, item);
        }
    }

//if we have 2 photos at the end then we need split them to 2 items (we must not have items with 2 photos)
    bool splitLast2Photos = (photosTotal % 3 == 2) && (photosTotal != 2);

    const Eina_List *list;
    void *data;
    EINA_LIST_FOREACH(photos, list, data) {
        Photo *photo = static_cast<Photo*>(data);
        if (photo) {
            ActionAndData *actionAndData = new ActionAndData(photo, me->mAction);
            //ToDo: It's very simple layouting. Probably we need to change it to more sophisticated later.
            //Create new item for each 3rd photo and for 2 last photos.
            if (photosCount % 3 == 0 || (splitLast2Photos && (photosCount == (photosTotal - 2) || photosCount == (photosTotal - 1)))) {
                    item = new GalleryItem();
                    eina_array_push(items, item);
            }
            item->AppendActionAndData(actionAndData);
            ++photosCount;
        }
    }
    Log::info_tag(LogTag, "PhotoGalleryScreenBase: there was handled %d photos and %d items created.",
            photosCount, eina_array_count(items));

}

void PhotoGalleryScreenBase::on_photo_clicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    Log::debug_tag(LogTag, "PhotoGalleryScreenBase::on_photo_clicked()");
    ActionAndData *actionAndData = static_cast<ActionAndData*>(data);
    assert(actionAndData);
    assert(actionAndData->mAction);
    PhotoGalleryScreenBase *me = static_cast<PhotoGalleryScreenBase*>(actionAndData->mAction->getScreen());
    assert(me);
    assert(actionAndData->mData);
    Photo *photo = static_cast<Photo*> (actionAndData->mData);
    assert(photo);
    if (me->mStrategy) {
        me->mStrategy->PhotoSelected(photo);
    }
}

void PhotoGalleryScreenBase::SetDataAvailable(bool isAvailable) {
    if (!isAvailable) {
//if we don't have any images then we should show mTopListLayout at least.
        //we need to prevent mTopListLayout to be set up each time
        if (mTopListLayout && !mIsTopListLayoutSet) {
            elm_object_content_unset(GetScroller());
            elm_object_content_set(GetScroller(), mTopListLayout);
            evas_object_show(mTopListLayout);
            mIsTopListLayoutSet = true;
        }
    //we need to prevent Data Area to be set up each time
    } else if (mIsTopListLayoutSet) {
        Evas_Object *obj = elm_object_content_unset(GetScroller());
        evas_object_hide(obj);
        elm_object_content_set(GetScroller(), GetDataArea());
        evas_object_show(GetDataArea());
        mIsTopListLayoutSet = false;
    }
}

void* PhotoGalleryScreenBase::GetDataForItem(void *item) {
    GalleryItem *galleryItem = static_cast<GalleryItem *> (item);
    ActionAndData *firstData = static_cast<ActionAndData *> (eina_list_nth(galleryItem->GetActionAndDates(), 0));
    return firstData ? firstData->mData : NULL;
}

bool PhotoGalleryScreenBase::go_item_comparator(void *itemToCompare, void *itemForCompare) {
    GalleryItem *galleryItem = static_cast<GalleryItem *> (itemForCompare);
    ActionAndData *firstData = static_cast<ActionAndData *> (eina_list_nth(galleryItem->GetActionAndDates(), 0));

    GraphObject *toCompare = static_cast<GraphObject*>(itemToCompare);
    GraphObject *forCompare = static_cast<GraphObject*> (firstData->mData);

    return ( toCompare->GetHash() == forCompare->GetHash() ) &&
            ( strcmp( toCompare->GetId(), forCompare->GetId() ) == 0 );
}

void PhotoGalleryScreenBase::on_download_completed(void* data) {
    assert(data);
    PhotoGalleryScreenBase *me = static_cast<PhotoGalleryScreenBase *> (data);
    delete me->mDataDownloader;
    me->mDataDownloader = NULL;
}

