#include "AlbumsScreen.h"
#include "CameraRollScreen.h"
#include "Common.h"
#include "Config.h"
#include "PhotoGalleryShowCarouselStrategy.h"
#include "PhotoGalleryUploadPhotoStrategy.h"
#include "PhotosOfYouScreen.h"
#include "UploadsScreen.h"
#include "UserPhotosScreen.h"
#include "jsonutilities.h"
#include "WidgetFactory.h"

#include <assert.h>


UserPhotosScreen::UserPhotosScreen(ScreenBase* parentScreen, const char* userProfileId, const char* userProfileName, UploadPhotoType targetUploadType, void *calBakObj4UsrProfilScr) : ScreenBase(parentScreen),
        mTabbarItems(),
        mTabbarSelectedItem(PHOTOS_OF_YOU_TAB) {
    mScreenId = ScreenBase::SID_USER_PHOTOS;
    Evas_Object *parent = getParentMainLayout();
    mScroller = NULL;
    mTabbar = NULL;

    mPhotosOfYouScreen = NULL;
    mUploadsScreen = NULL;
    mAlbumsScreen = NULL;

    mPhotosOfYouBox = NULL;
    mUploadsBox = NULL;
    mAlbumsBox = NULL;

    mUserProfileId = SAFE_STRDUP(userProfileId);
    mUserProfileName = SAFE_STRDUP(userProfileName);

    mTargetUploadType = targetUploadType;
    mCalBakObj4UsrProfilScr = calBakObj4UsrProfilScr;

    mLayout = CreateView(parent);
    UpdateAddPhotoBtn();

    AppEvents::Get().Subscribe(ePOST_SENDING_PHOTO_EVENT, this);
}

Evas_Object *UserPhotosScreen::CreateView(Evas_Object *parent)
{
    Evas_Object *layout = elm_layout_add(parent);
    elm_layout_file_set(layout, Application::mEdjPath, "user_photos_screen");
    evas_object_size_hint_weight_set(layout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(layout, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_show(layout);

    Evas_Object *box = elm_box_add(layout);
    evas_object_size_hint_weight_set(box, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(box, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_object_part_content_set(layout, "content", box);

    mTabbar = CreateTabbar(layout);
    evas_object_size_hint_weight_set(mTabbar, EVAS_HINT_EXPAND, 0);
    elm_object_part_content_set (layout, "tabbar", mTabbar);
    evas_object_size_hint_align_set(mTabbar, EVAS_HINT_FILL, EVAS_HINT_FILL);

    mScroller = CreateScroller(layout);
    elm_box_pack_end(box, mScroller);

    mNestedScreenContainer = elm_box_add(mScroller);
    elm_box_horizontal_set(mNestedScreenContainer, EINA_TRUE);
    evas_object_size_hint_weight_set(mNestedScreenContainer, 1, 1);
    evas_object_size_hint_align_set(mNestedScreenContainer, -1, 0);
    elm_object_content_set(mScroller, mNestedScreenContainer);
    evas_object_show(mNestedScreenContainer);

    OwnProfileScreen *ownProfileScreen = static_cast<OwnProfileScreen *> (mCalBakObj4UsrProfilScr);
    assert(ownProfileScreen || mTargetUploadType == UPLOAD_NONE);

    // Create Photos Of You Tab
    mPhotosOfYouScreen = new PhotosOfYouScreen(this, mUserProfileId, mTargetUploadType != UPLOAD_NONE ?
            static_cast<PhotoGalleryStrategyBase *> (new PhotoGalleryUploadPhotoStrategy(mTargetUploadType)) :
            static_cast<PhotoGalleryStrategyBase *> (new PhotoGalleryShowCarouselStrategy()));
    mPhotosOfYouBox = ((ScreenBase *)mPhotosOfYouScreen)->getMainLayout();
    elm_box_pack_end(mNestedScreenContainer, mPhotosOfYouBox);
    evas_object_show (mPhotosOfYouBox);

    // Create Uploads Tab
    mUploadsScreen = new UploadsScreen(this, mUserProfileId, mTargetUploadType != UPLOAD_NONE ?
            static_cast<PhotoGalleryStrategyBase *> (new PhotoGalleryUploadPhotoStrategy(mTargetUploadType)) :
            static_cast<PhotoGalleryStrategyBase *> (new PhotoGalleryShowCarouselStrategy()));
    mUploadsBox = ((ScreenBase *)mUploadsScreen)->getMainLayout();
    elm_box_pack_end(mNestedScreenContainer, mUploadsBox);
    evas_object_show (mUploadsBox);

    // Create Albums Tab
    mAlbumsScreen = new AlbumsScreen(this, mUserProfileId, mTargetUploadType != UPLOAD_NONE, mCalBakObj4UsrProfilScr);
    mAlbumsBox = ((ScreenBase *)mAlbumsScreen)->getMainLayout();
    elm_box_pack_end(mNestedScreenContainer, mAlbumsBox);
    evas_object_show (mAlbumsBox);

    Evas_Coord w, h;
    /* gets the object's position and size to build it correctly */
    evas_object_geometry_get(Application::GetInstance()->mWin, NULL, NULL, &w, &h);
    OnViewResize(w, h);

    // Set User Name
    if(mUserProfileName)
    {
        elm_object_part_text_set(layout, "title", mUserProfileName);
    }
    else
    {
        elm_object_part_text_set(layout, "title", "");
        mProfileNameGrpReq = FacebookSession::GetInstance()->GetUserProfileName( on_get_user_profile_name_completed, this);
    }

    elm_object_signal_callback_add(layout, "back.clicked", "back*", BackButtonClicked, this);
    elm_object_signal_callback_add(layout, "add.photos.clicked", "add.photos*", AddPhotoButtonClicked, this);

    return layout;
}

UserPhotosScreen::~UserPhotosScreen()
{
    free(mUserProfileId);
    free(mUserProfileName);
    if(mProfileNameGrpReq)
    {
        FacebookSession::GetInstance()->ReleaseGraphRequest(mProfileNameGrpReq);
        mProfileNameGrpReq = NULL;
    }
    for (int i = 0; i < MAX_TABS; ++i) {
        delete mTabbarItems[i];
    }
}

void UserPhotosScreen::on_get_user_profile_name_completed(void* object, char* respond, int code)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "UserPhotosScreen::on_get_user_profile_name_completed()\n");
    UserPhotosScreen *self = static_cast<UserPhotosScreen *>(object);
    FacebookSession::GetInstance()->ReleaseGraphRequest(self->mProfileNameGrpReq);
    self->mProfileNameGrpReq = NULL;
    if( CURLE_OK == code )
    {
        JsonObject * rootObject = NULL;
        JsonParser * parser = openJsonParser(respond, &rootObject);
        const char* profileName = rootObject ? json_object_get_string_member(rootObject, "name") : NULL;
        if(profileName)
        {
            elm_object_part_text_set(self->getMainLayout(), "title", profileName);
        }
        g_object_unref(parser);
    }
    free(respond);
}

void UserPhotosScreen::Push()
{
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

void UserPhotosScreen::BackButtonClicked(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    if(Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_USER_PHOTOS) {
        UserPhotosScreen *screen = static_cast<UserPhotosScreen *>(data);
        screen->Pop();
    }
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Back btn pressed on Photos Screen");
}

void UserPhotosScreen::AddPhotoButtonClicked(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "add photo btn pressed on Photos Screen");
    if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_CAMERA_ROLL) {
        CameraRollScreen *screen = new CameraRollScreen(eCAMERA_ROLL_ADD_PHOTO_FROM_USER_PHOTO_SCREEN);
        Application::GetInstance()->AddScreen(screen);
    }
}

void UserPhotosScreen::OnViewResize(Evas_Coord w, Evas_Coord h) {
    elm_scroller_page_size_set(mScroller, w, 0);
//https://www.dropbox.com/sh/ho4ftjneuykd3z6/AACo8jwO3AuobwvsSQE2hD7ia?dl=0&preview=Screen_20170328_192709.png
    elm_scroller_page_show(mScroller, mTabbarSelectedItem, 0);
    evas_object_size_hint_min_set(mPhotosOfYouBox, w, h);
    evas_object_size_hint_min_set(mUploadsBox, w, h);
    evas_object_size_hint_min_set(mAlbumsBox, w, h);
}

Evas_Object *UserPhotosScreen::CreateTabbar(Evas_Object *parent) {
    Evas_Object *scroller = elm_scroller_add(parent);
    elm_scroller_single_direction_set(scroller, ELM_SCROLLER_SINGLE_DIRECTION_HARD);
    elm_scroller_policy_set(scroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_OFF);
    evas_object_show(scroller);

    Evas_Object *data_area = elm_box_add(scroller);
    evas_object_size_hint_weight_set(data_area, 1, 1);
    evas_object_size_hint_align_set(data_area, -1, -1);
    elm_box_padding_set(data_area, 0, 0);
    elm_box_horizontal_set(data_area, EINA_TRUE);
    elm_object_content_set(scroller, data_area);
    evas_object_show(data_area);

    if (!Utils::IsMe(mUserProfileId)) {
        char *prof_name = SAFE_STRDUP(mUserProfileName);
        char *firstname = strtok(prof_name, " ");
        const char *textName = firstname ? firstname : prof_name;
        mTabbarItems[PHOTOS_OF_YOU_TAB] = new TabbarItem(data_area, this, PHOTOS_OF_YOU_TAB, "IDS_PHOTOS_OF", textName);
        free(prof_name);
    } else {
        mTabbarItems[PHOTOS_OF_YOU_TAB] = new TabbarItem(data_area, this, PHOTOS_OF_YOU_TAB, "IDS_PHOTOS_OF_YOU", NULL);
    }
    mTabbarItems[UPLOADS_TAB] = new TabbarItem(data_area, this, UPLOADS_TAB, "IDS_UPLOADS", NULL);
    mTabbarItems[ALBUMS_TAB] = new TabbarItem(data_area, this, ALBUMS_TAB, "IDS_ALBUMS", NULL);
    mTabbarItems[ALBUMS_TAB]->SetLast(true);

    mTabbarItems[mTabbarSelectedItem]->SetActive(true);

    return scroller;
}

Evas_Object *UserPhotosScreen::CreateScroller(Evas_Object *parent)
{
    Evas_Object *scroller = elm_scroller_add(parent);
    elm_scroller_policy_set(scroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_OFF);
    elm_scroller_page_size_set(scroller, R->SCREEN_SIZE_WIDTH, 0);
    elm_scroller_page_scroll_limit_set(scroller, 1, 0);
    elm_scroller_single_direction_set(scroller, ELM_SCROLLER_SINGLE_DIRECTION_HARD);
    evas_object_size_hint_weight_set(scroller, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(scroller, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_smart_callback_add(scroller, "scroll,anim,stop", anim_stop_scroll_cb, this);
    evas_object_show(scroller);

    return scroller;
}

void UserPhotosScreen::anim_stop_scroll_cb(void *data, Evas_Object *obj, void *event_info) {
    UserPhotosScreen *screen = static_cast<UserPhotosScreen *>(data);
    int page = 0;
    elm_scroller_current_page_get(screen->mScroller, &page, NULL);

    screen->DoAnimation(screen->mTabbarSelectedItem, page);
    screen->mTabbarSelectedItem = page;
    screen->UpdateAddPhotoBtn();
}

void UserPhotosScreen::DoAnimation(unsigned int wasChoosen, unsigned int isChoosen) {
    assert(wasChoosen < MAX_TABS && isChoosen < MAX_TABS);
    if (wasChoosen < MAX_TABS) {
        mTabbarItems[wasChoosen]->SetActive(false);
    }
    if (isChoosen < MAX_TABS) {
        mTabbarItems[isChoosen]->SetActive(true);
    }
}

void UserPhotosScreen::tabbar_cb(void *data, Evas_Object *obj, const char *emission, const char *source) {
    TabbarItem *ti = static_cast<TabbarItem *>(data);

    UserPhotosScreen* screen = static_cast<UserPhotosScreen* > (ti->GetScreen());

    elm_scroller_page_bring_in(screen->mScroller, ti->GetTabId(), 0);

    screen->DoAnimation(screen->mTabbarSelectedItem, ti->GetTabId());

    screen->mTabbarSelectedItem = ti->GetTabId();
    screen->UpdateAddPhotoBtn();
}

void UserPhotosScreen::UpdateAddPhotoBtn() {
    if (mTabbarSelectedItem == ALBUMS_TAB || mTargetUploadType != UPLOAD_NONE || !Utils::IsMe(mUserProfileId)) {
        elm_object_signal_emit(mLayout, "hide_photo_add_btn", "photo_add_btn");
    } else {
        elm_object_signal_emit(mLayout, Application::IsRTLLanguage() ? "show_photo_add_btn_rtl" : "show_photo_add_btn_ltr", "photo_add_btn");
    }

}
/*
 * Class UserPhotosScreen::TabbarItem
 */
UserPhotosScreen::TabbarItem::TabbarItem(Evas_Object *parent, ScreenBase *screen, int tabId, const char *text, const char *textName) : mScreen(screen),
        mTabId(tabId), mItem(NULL) {
    mItem = elm_layout_add(parent);
    evas_object_size_hint_weight_set(mItem, 1, 1);
    elm_layout_file_set(mItem, Application::mEdjPath, "tabbar_item_user_photos_screen");
    elm_box_pack_end(parent, mItem);
    evas_object_show(mItem);

    Evas_Object *label = elm_label_add(mItem);
    elm_object_part_content_set(mItem, "item_text", label);
    evas_object_size_hint_weight_set(label, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(label, EVAS_HINT_FILL, 0.5);
    if (textName) {
        FRMTD_TRNSLTD_TXT_SET3(label, SANS_9197a3_FORMAT, R->TABBAR_ITEM_FONT_SIZE, text, textName);
    } else {
        FRMTD_TRNSLTD_TXT_SET2(label, SANS_MEDIUM_9197a3_FORMAT, R->TABBAR_ITEM_FONT_SIZE, text);
    }
    evas_object_show(label);

    elm_object_signal_callback_add(mItem, "item.clicked", "", UserPhotosScreen::tabbar_cb, this);
}

void UserPhotosScreen::TabbarItem::SetActive(bool isActive) {
    if (isActive) {
        elm_object_signal_emit(mItem, "set.selected", "");
    } else {
        elm_object_signal_emit(mItem, "set.unselected", "");
    }
}

void UserPhotosScreen::TabbarItem::SetLast(bool isLast) {
    if (isLast) {
        elm_object_signal_emit(mItem, "set_last", "");
    } else {
//ToDo: Implement if it will be needed.
    }
}

ScreenBase *UserPhotosScreen::TabbarItem::GetScreen() {
    return mScreen;
}

int UserPhotosScreen::TabbarItem::GetTabId() {
    return mTabId;
}

void UserPhotosScreen::Update(AppEventId EventId, void* Data) {
    switch(EventId) {
    case ePOST_SENDING_PHOTO_EVENT:
        notification_status_message_post(i18n_get_text("IDS_MSG_PHOTO_POST_INITIATED"));
        break;
    default:
        break;
    }
}

bool UserPhotosScreen::HandleBackButton() {
    Log::info("UserPhotosScreen::HandleBackButton");
    return WidgetFactory::CloseAlertPopup();
}
