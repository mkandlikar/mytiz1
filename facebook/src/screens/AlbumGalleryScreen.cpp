#include "ActionBase.h"
#include "AlbumGalleryScreen.h"
#include "AlbumPresenter.h"
#include "AppEvents.h"
#include "CameraRollScreen.h"
#include "Config.h"
#include "FacebookSession.h"
#include "FbRespondPost.h"
#include "PhotoGalleryProvider.h"
#include "Popup.h"
#include "User.h"
#include "Utils.h"
#include "WidgetFactory.h"

#include <dlog.h>
#include <string>

#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "PhotoGallery"

/*
 * Class AlbumGalleryScreen
 */
AlbumGalleryScreen::AlbumGalleryScreen(Album *album, PhotoGalleryStrategyBase *strategy) :
    PhotoGalleryScreenBase(NULL, PhotoGalleryProvider::GetInstance(), strategy),
    mAlbum(album), mGalleryDetails(NULL), mRenameAlbumPopup(NULL),
        mRenameEntry(NULL), mRenameRequest(NULL), mDeleteRequest(NULL), mSubscriber(this), mConfirmationDialog(NULL) {
    dlog_print(DLOG_INFO, LOG_TAG, "AlbumGalleryScreen: create Photo Gallery screen." );
    mScreenId = ScreenBase::SID_ALBUM_GALLERY;
    assert(mAlbum);
    mAlbum->AddRef();

    mLayout = elm_layout_add(getParentMainLayout());
    elm_layout_file_set(mLayout, Application::mEdjPath, "album_gallery_screen");
    evas_object_size_hint_weight_set(mLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(mLayout, EVAS_HINT_FILL, EVAS_HINT_EXPAND);
    evas_object_show(mLayout);

    Evas_Object *actionBar = NULL;
//Show More option only for "normal" albums and only for user own albums.
    if ((mAlbum->mType && !strcmp(mAlbum->mType, "normal")) && (mAlbum->mFrom && mAlbum->mFrom->GetId() && Config::GetInstance().GetUserId() == mAlbum->mFrom->GetId())) {
        actionBar = WidgetFactory::CreateActionBarWithBackBtnText2Actions(mLayout, "IDS_ALBUMS_ALBUM", NULL, ICON_DIR"/more_white.png", this);
    } else {
        actionBar = WidgetFactory::CreateActionBarWithBackBtnText2Actions(mLayout, "IDS_ALBUMS_ALBUM", NULL, NULL, this);
    }
    elm_object_part_content_set(mLayout, "swallow.action_bar", actionBar);

    mGalleryCaption = CreateGalleryCaption();
    SetTopListLayout(mGalleryCaption);

    elm_object_part_content_set(mLayout, "swallow.scroller", GetScroller());

    PhotoGalleryProvider::GetInstance()->SetAlbumId(mAlbum->GetId(), this);

    RequestData();
}

Evas_Object *AlbumGalleryScreen::CreateGalleryCaption() {
    Evas_Object *galleryCaption = elm_layout_add(mLayout);
    elm_layout_file_set(galleryCaption, Application::mEdjPath, "album_gallery_caption");

    evas_object_hide(galleryCaption);

    mGalleryDetails = elm_layout_add(galleryCaption);
    elm_layout_file_set(mGalleryDetails, Application::mEdjPath, "album_gallery_details");
    UpdateAlbumCaption();
    elm_object_part_content_set(galleryCaption, "swallow.album_details", mGalleryDetails);

    if (mAlbum && mAlbum->mDescription) {
        Evas_Object *mAlbumNameEntry = elm_entry_add(mGalleryDetails);
        elm_entry_editable_set(mAlbumNameEntry, EINA_FALSE);
        elm_entry_context_menu_disabled_set(mAlbumNameEntry, EINA_TRUE);
        elm_entry_single_line_set(mAlbumNameEntry, EINA_FALSE);
        elm_entry_scrollable_set(mAlbumNameEntry, EINA_FALSE);
        std::string str = Utils::ReplaceString(mAlbum->mDescription, "\n", "<br/>");
        const char *formattedText = WidgetFactory::WrapByFormat(R->ALBUM_DETAILS_DESCRIPTION_TEXT_FORMAT, str.c_str());
        elm_entry_entry_set(mAlbumNameEntry, formattedText);
        delete[] formattedText;
        elm_layout_content_set(mGalleryDetails, "text.description", mAlbumNameEntry);

        elm_object_signal_emit(mGalleryDetails, "show_description", "");
    }

    if (!mAlbum->mCanUpload) {
        elm_object_signal_emit(galleryCaption, "hide_add_photos_btn", "");
    } else {
        Evas_Object *addPhotosBtn = elm_layout_add(galleryCaption);
        elm_layout_file_set(addPhotosBtn, Application::mEdjPath, "album_add_photos_button");
        elm_object_translatable_part_text_set(addPhotosBtn, "text.add_photos", "IDS_ALBUMS_ADD_PHOTOS");
        elm_object_part_content_set(galleryCaption, "swallow.add_photos_btn", addPhotosBtn);
        elm_object_signal_callback_add(addPhotosBtn, "mouse,clicked,*", "*", on_add_photos_btn_clicked, this);
    }
    return galleryCaption;
}

void AlbumGalleryScreen::UpdateAlbumCaption() {
    elm_object_part_text_set(mGalleryDetails, "text.name", mAlbum->mName);
    AlbumPresenter presenter(*mAlbum);

    std::stringstream str;
    str << presenter.GenPhotosNumber() << " %s " << mAlbum->mLocation;

    char * createdTime = strdup(mAlbum->mCreatedTime);
    DATE_TRNSLTD_PART_TEXT_SET(mGalleryDetails, "text.details", str.str().c_str(), Utils::ParseTimeToMilliseconds(createdTime));
    free(createdTime);

//ToDo: Implement support for privacy. It's not clear yet how to get privacy for albums

}

void AlbumGalleryScreen::Push() {
    ScreenBase::Push();
    // Remove standart blue header
    elm_naviframe_item_title_enabled_set(elm_naviframe_top_item_get(getNf()), EINA_FALSE, EINA_FALSE);
}

AlbumGalleryScreen::~AlbumGalleryScreen() {
    dlog_print(DLOG_INFO, LOG_TAG, "AlbumGalleryScreen: destroy AlbumGalleryScreen screen." );
    delete mConfirmationDialog;
    FacebookSession::GetInstance()->ReleaseGraphRequest(mRenameRequest);
    FacebookSession::GetInstance()->ReleaseGraphRequest(mDeleteRequest);
    mAlbum->Release();
}

void AlbumGalleryScreen::on_add_photos_btn_clicked (void *data, Evas_Object *obj, const char *emission, const char *source) {
    dlog_print(DLOG_INFO, LOG_TAG, "CLICKED: %s", source);
    if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_CAMERA_ROLL) {
        AlbumGalleryScreen *me = static_cast<AlbumGalleryScreen *>(data);
        CameraRollScreen* cameraRollScreen = new CameraRollScreen(eCAMERA_ROLL_ADD_PHOTO_TO_ALBUM, NULL, me->mAlbum);
        Application::GetInstance()->AddScreen(cameraRollScreen);
    }
}

void AlbumGalleryScreen::ShowAlbumRenamePopup() {
    dlog_print(DLOG_INFO, LOG_TAG, "ShowAlbumRenamePopup()");

    mRenameAlbumPopup = elm_popup_add(mLayout);

    elm_popup_align_set(mRenameAlbumPopup, ELM_NOTIFY_ALIGN_FILL, 0.5);

    evas_object_smart_callback_add(mRenameAlbumPopup, "block,clicked", menu_dismiss_cb, this);

    Evas_Object *renameLayout = elm_layout_add(mRenameAlbumPopup);

    elm_layout_file_set(renameLayout, Application::mEdjPath, "album_rename_popup");
    elm_object_content_set(mRenameAlbumPopup, renameLayout);

    elm_object_translatable_part_text_set(renameLayout, "top_text", "IDS_ALBUM_NEW_TITLE");
    elm_object_translatable_part_text_set(renameLayout, "text.cancel", "IDS_CANCEL");
    elm_object_translatable_part_text_set(renameLayout, "text.rename", "IDS_ALBUM_RENAME");


    mRenameEntry = elm_entry_add(renameLayout);
    elm_entry_prediction_allow_set(mRenameEntry, EINA_FALSE);
    elm_object_part_content_set(renameLayout, "swallow.rename", mRenameEntry);
    elm_entry_scrollable_set(mRenameEntry, EINA_TRUE);
    elm_entry_input_panel_return_key_disabled_set(mRenameEntry, EINA_TRUE);
    elm_entry_single_line_set(mRenameEntry, EINA_TRUE);
    elm_object_translatable_part_text_set(mRenameEntry, "elm.guide", "IDS_ALBUM_RENAME_GUIDED_HINT_TEXT");

    elm_entry_text_style_user_push(mRenameEntry, ScreenBase::R->RENAME_ALBUM_TEXT_STYLE );
    if (mAlbum->mName) {
        elm_object_part_text_set(mRenameEntry, "elm.text", mAlbum->mName);
    } else {
        elm_object_part_text_set(mRenameEntry, "elm.text", i18n_get_text("Untitled Album"));  //TODO: localization
    }
    elm_entry_cnp_mode_set(mRenameEntry, ELM_CNP_MODE_PLAINTEXT);

    elm_object_signal_callback_add(renameLayout, "mouse,clicked,*", "cancel", onCancelClicked, this);
    elm_object_signal_callback_add(renameLayout, "mouse,clicked,*", "rename", onRenameClicked, this);

    evas_object_show(renameLayout);

    evas_object_show(mRenameAlbumPopup);
}

void AlbumGalleryScreen::CloseAlbumRenamePopup() {
    dlog_print(DLOG_INFO, LOG_TAG, "CloseAlbumRenamePopup()");
    if (mRenameAlbumPopup) evas_object_del(mRenameAlbumPopup);
    mRenameAlbumPopup = NULL;
    mRenameEntry = NULL;
}

void AlbumGalleryScreen::onCancelClicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    dlog_print(DLOG_INFO, LOG_TAG, "onCancelClicked:%s:%s", emission, source);
    AlbumGalleryScreen *screen = static_cast<AlbumGalleryScreen *>(data);
    screen->CloseAlbumRenamePopup();
}

void AlbumGalleryScreen::onRenameClicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    dlog_print(DLOG_INFO, LOG_TAG, "onRenameClicked:%s:%s", emission, source);
    AlbumGalleryScreen *screen = static_cast<AlbumGalleryScreen *>(data);

    const char* renamedAlbumName = elm_object_part_text_get(screen->mRenameEntry, NULL);
    if( !Utils::isEmptyString(renamedAlbumName) )
    {
        screen->RenameAlbum(renamedAlbumName);
        screen->CloseAlbumRenamePopup();
    }
    else
    {
        notification_status_message_post(i18n_get_text("IDS_ALBUM_RENAME_EMPTY_EDIT_TEXT_NOTIF_MSG"));
    }
}

void AlbumGalleryScreen::menu_dismiss_cb(void *data, Evas_Object *obj, void *event_info){
    AlbumGalleryScreen *screen = static_cast<AlbumGalleryScreen *>(data);
    screen->CloseAlbumRenamePopup();
}

void AlbumGalleryScreen::SecondActionClicked() {
    ShowMoreMenu();
}

void AlbumGalleryScreen::ShowMoreMenu() {
//AAA ToDo:Don't show this popup for some types of albums. E.g. Mobile Uploads.
    const int menuItemsNumber = 2;
    static PopupMenu::PopupMenuItem more_menu_items[menuItemsNumber] =
    {
        {
            ICON_DIR "/edit.png",
            "IDS_ALBUM_RENAME",
            NULL,
            rename_selected_cb,
            NULL,
            0,
            false,
            false
        },
        {
            ICON_DIR "/delete.png",
            "IDS_DELETE",
            NULL,
            delete_selected_cb,
            NULL,
            1,
            true,
            false
        }
    };

    more_menu_items[0].data = this;
    more_menu_items[1].data = this;

    Evas_Coord y = ScreenBase::R->STATUS_BAR_SIZE_H + ScreenBase::R->ACTION_BAR_SIZE_H;
    PopupMenu::Show(menuItemsNumber, more_menu_items, mLayout, false, y);
}

void AlbumGalleryScreen::rename_selected_cb(void *data, Evas_Object *obj, void *event_info) {
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "rename_selected_cb");
    AlbumGalleryScreen *me = static_cast<AlbumGalleryScreen *>(data);
    me->ShowAlbumRenamePopup();
}

void AlbumGalleryScreen::delete_selected_cb(void *data, Evas_Object *obj, void *event_info) {
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "delete_selected_cb");
    AlbumGalleryScreen *me = static_cast<AlbumGalleryScreen *>(data);
    char *description = NULL;
    if (me->mAlbum && me->mAlbum->mName) {
        int size = strlen(i18n_get_text("IDS_DELETE_ALBUM_CONFIRMATION_TXT")) + strlen(me->mAlbum->mName) + 1;
        description = new char[size];
        Utils::Snprintf_s(description, size, i18n_get_text("IDS_DELETE_ALBUM_CONFIRMATION_TXT"), me->mAlbum->mName);
    } else {
        int size = strlen(i18n_get_text("IDS_DELETE_ALBUM_CONFIRMATION_TXT")) + 1;
        description = new char[size];
        Utils::Snprintf_s(description, size, i18n_get_text("IDS_DELETE_ALBUM_CONFIRMATION_TXT"), "");
    }
    me->mConfirmationDialog = ConfirmationDialog::CreateAndShow(me->mLayout, "IDS_DELETE_ALBUM_CONFIRMATION_CAPTION",
            description, "IDS_DELETE_ALBUM_CONFIRMATION_CANCEL",
            "IDS_DELETE_ALBUM_CONFIRMATION_DELETE", confirmation_dialog_cb, me);
    delete[] description;
}

void AlbumGalleryScreen::confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event) {
    dlog_print(DLOG_INFO, LOG_TAG_FACEBOOK, "confirmation_dialog_cb:%d", event);
    AlbumGalleryScreen *screen = static_cast<AlbumGalleryScreen *>(user_data);

    delete screen->mConfirmationDialog;
    screen->mConfirmationDialog = NULL;

    switch (event) {
    case ConfirmationDialog::ECDYesPressed:
        break;
    case ConfirmationDialog::ECDNoPressed:
        screen->DeleteAlbum();
        break;
    case ConfirmationDialog::ECDDismiss:
        break;
    default:
        break;
    }
}

void AlbumGalleryScreen::RenameAlbum(const char*newName) {
    if (newName && mAlbum->mName && strcmp(newName, mAlbum->mName)) {
        bundle *paramsBundle = bundle_create();
        bundle_add_str(paramsBundle, "name", newName);
        if (!mRenameRequest) {
            free((void *) mAlbum->mName);
            mAlbum->mName = strdup(newName);
            mRenameRequest = FacebookSession::GetInstance()->PostRenameAlbum(mAlbum->GetId(), paramsBundle, on_rename_album_completed, this);
        }
        bundle_free(paramsBundle);
    }
}

void AlbumGalleryScreen::on_rename_album_completed(void* object, char* response, int code) {
    dlog_print(DLOG_INFO, LOG_TAG, "on_rename_album_completed");

    AlbumGalleryScreen * screen = static_cast<AlbumGalleryScreen *>(object);
    FacebookSession::GetInstance()->ReleaseGraphRequest(screen->mRenameRequest);
    screen->mRenameRequest = NULL;
    if (code == CURLE_OK) {
        dlog_print(DLOG_INFO, LOG_TAG, "response: %s", response);
        FbRespondPost *postResponse = FbRespondPost::createFromJson(response);

        if (!postResponse) {
            dlog_print(DLOG_INFO, LOG_TAG, "Error parsing http response");
        } else {
            if (postResponse->mError) {
                dlog_print(DLOG_INFO, LOG_TAG, "Error renaming album, error = %s", postResponse->mError->mMessage);
            } else {
                dlog_print(DLOG_DEBUG, "Facebook", "on_rename_album_completed() successfully");
                screen->UpdateAlbumCaption();
                AppEvents::Get().Notify(eALBUM_RENAMED_EVENT, const_cast<char *> (screen->mAlbum->GetId()));
            }
            delete postResponse;
        }
    } else {
        dlog_print(DLOG_INFO, LOG_TAG, "Error sending http request");
    }

    //Attention!! client should take care to free respond buffer
    free(response);
}

void AlbumGalleryScreen::DeleteAlbum() {
    if (!mDeleteRequest) {
        mDeleteRequest = FacebookSession::GetInstance()->DeleteAlbum(mAlbum->GetId(), on_delete_album_completed, this);
    }
}

void AlbumGalleryScreen::on_delete_album_completed(void* object, char* response, int code) {
    dlog_print(DLOG_INFO, LOG_TAG, "on_delete_album_completed");

    AlbumGalleryScreen * screen = static_cast<AlbumGalleryScreen *>(object);
    FacebookSession::GetInstance()->ReleaseGraphRequest(screen->mDeleteRequest);
    screen->mDeleteRequest = NULL;
    if (code == CURLE_OK) {
        dlog_print(DLOG_INFO, LOG_TAG, "response: %s", response);
        FbRespondPost *postResponse = FbRespondPost::createFromJson(response);

        if (!postResponse) {
            dlog_print(DLOG_INFO, LOG_TAG, "Error parsing http response");
        } else {
            if (postResponse->mError) {
                dlog_print(DLOG_INFO, LOG_TAG, "Error deleting album, error = %s", postResponse->mError->mMessage);
            } else {
                dlog_print(DLOG_DEBUG, "Facebook", "on_delete_album_completed() successfully");
                AppEvents::Get().Notify(eALBUM_DELETED_EVENT, const_cast<char *> (screen->mAlbum->GetId()));
                screen->Pop();
            }
            delete postResponse;
        }
    } else {
        dlog_print(DLOG_INFO, LOG_TAG, "Error sending http request");
    }

    if (response != NULL) {
        //Attention!! client should take care to free respond buffer
        free(response);
    }
}

bool AlbumGalleryScreen::HandleBackButton(){
    dlog_print(DLOG_INFO, LOG_TAG, "BACK");
    bool ret = true;
    if (PopupMenu::Close()) {
    } else if (mRenameAlbumPopup) {
        CloseAlbumRenamePopup();
    } else if (mConfirmationDialog) {
        delete mConfirmationDialog;
        mConfirmationDialog = NULL;
    } else if (!mDeleteRequest && !mRenameRequest) {
        ret = false;
    }
    return ret;
}

void AlbumGalleryScreen::PhotoAdded() {
    if (mAlbum) {
        ++(mAlbum->mCount);
    }
    UpdateAlbumCaption();
}

void AlbumGalleryScreen::PhotosAdditionCompleted() {
    ReloadScreen();
}

void AlbumGalleryScreen::ReloadScreen() {
    EraseWindowData();
    PhotoGalleryProvider::GetInstance()->EraseData();
    PhotoGalleryProvider::GetInstance()->ResetRequestTimer();
    PhotoGalleryProvider::GetInstance()->SetAlbumId(mAlbum->GetId(), this);
    RequestData();
}


/*
 * Class AlbumGalleryScreen::AlbumGalleryEventsSubscriber
 */

AlbumGalleryScreen::AlbumGalleryEventsSubscriber::AlbumGalleryEventsSubscriber(AlbumGalleryScreen *screen) : mScreen(screen) {
    AppEvents::Get().Subscribe(eALBUM_NEW_PHOTO_ADDED_EVENT, this);
    AppEvents::Get().Subscribe(eALBUM_NEW_PHOTOS_ADDITION_COMPLETED_EVENT, this);
}

AlbumGalleryScreen::AlbumGalleryEventsSubscriber::~AlbumGalleryEventsSubscriber() {
}

void AlbumGalleryScreen::AlbumGalleryEventsSubscriber::Update(AppEventId eventId, void *data) {
    const char *albumId = static_cast<char *> (data);
    if (!albumId || strcmp(albumId, mScreen->mAlbum->GetId())) {
        return;
    }

    switch (eventId) {
    case eALBUM_NEW_PHOTO_ADDED_EVENT:
        mScreen->PhotoAdded();
        break;
    case eALBUM_NEW_PHOTOS_ADDITION_COMPLETED_EVENT:
        mScreen->PhotosAdditionCompleted();
        break;
    default:
        break;
    }
}


