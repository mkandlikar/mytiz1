#include <dlog.h>
#include "Common.h"
#include "Config.h"
#include "UserProfilePlacesScreen.h"
#include "UserProfilePlacesMore.h"
#include "WidgetFactory.h"

UIRes * UserProfilePlacesScreen::R = UIRes::GetInstance();

UserProfilePlacesScreen::UserProfilePlacesScreen(const char * profileId): ScreenBase(NULL)
{
	mProfileId = profileId ? strdup(profileId) : NULL;
    Evas_Object *parent = getParentMainLayout();

    mScroller = NULL;
    mLayout = CreatePlacesPage(parent);
}

UserProfilePlacesScreen::~UserProfilePlacesScreen()
{
	free(mProfileId);
}

void UserProfilePlacesScreen::Push() {
    ScreenBase::Push();
    // hide standard title of naviframe
    elm_naviframe_item_title_enabled_set(mNfItem, EINA_FALSE, EINA_FALSE);
}

Evas_Object *UserProfilePlacesScreen :: CreatePlacesPage(Evas_Object *parent)
{
    Evas_Object *layout = elm_layout_add(parent);
    elm_layout_file_set(layout, Application::mEdjPath, "user_profile_places_page");
    evas_object_size_hint_weight_set(layout, 1, 1);
    evas_object_size_hint_align_set(layout, -1, -1);
    evas_object_show(layout);

    elm_object_translatable_part_text_set(layout, "title", "IDS_PLACES");
    elm_object_signal_callback_add(layout, "back,clicked", "back_btn",
                (Edje_Signal_Cb)back_btn_cb, this);

    Evas_Object *box = elm_box_add(layout);
    evas_object_size_hint_weight_set(box, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(box, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_object_part_content_set(layout, "content", box);

    mScroller = CreateScroller(box);
    elm_box_pack_end(box, mScroller);

    mNestedScreenContainer = elm_box_add(mScroller);
    elm_box_horizontal_set(mNestedScreenContainer, EINA_TRUE);
    evas_object_size_hint_weight_set(mNestedScreenContainer, 1, 1);
    evas_object_size_hint_align_set(mNestedScreenContainer, -1, -1);
    elm_object_content_set(mScroller, mNestedScreenContainer);
    evas_object_show(mNestedScreenContainer);

    UserProfilePlacesMore* places_more = new UserProfilePlacesMore(this , mNestedScreenContainer , mProfileId);
    mViewRect1 = MinSet((static_cast<ScreenBase*>(places_more))->getMainLayout(), mNestedScreenContainer, 0, 0);

    Evas_Coord w, h;
    evas_object_geometry_get(Application::GetInstance()->mWin, NULL, NULL, &w, &h);
    OnViewResize(w, h);

    return layout;

}

void UserProfilePlacesScreen :: back_btn_cb(void *data , Evas_Object *obj, const char *emission, const char *source)
{
    UserProfilePlacesScreen *self = static_cast<UserProfilePlacesScreen*>(data);
    self->Pop();
}

Evas_Object *UserProfilePlacesScreen :: CreateScroller(Evas_Object *parent)
{
    Evas_Object *scroller = elm_scroller_add(parent);
    elm_scroller_policy_set(scroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_OFF);
    elm_scroller_page_size_set(scroller, 480, 0);
    elm_scroller_page_scroll_limit_set(scroller, 1, 0);
    elm_scroller_single_direction_set(scroller, ELM_SCROLLER_SINGLE_DIRECTION_HARD);
    evas_object_size_hint_weight_set(scroller, 1, 1);
    evas_object_size_hint_align_set(scroller, -1, -1);
    evas_object_show(scroller);

    return scroller;
}

void UserProfilePlacesScreen::OnViewResize(Evas_Coord w, Evas_Coord h)
{
    elm_scroller_page_size_set(mScroller, w, h);
    evas_object_size_hint_min_set(mViewRect1, R->FF_MAIN_RECT_SIZE_W, R->FF_MAIN_RECT_SIZE_H);
}
