#include "Common.h"
#include "GetMessengerScreen.h"
#include "Utils.h"
#include "WidgetFactory.h"

GetMessengerScreen::GetMessengerScreen() : ScreenBase(NULL)
{

    unsigned int textWidth = R->GET_MESSENGER_TEXT_W;

    mLayout = elm_layout_add(getParentMainLayout());
    evas_object_size_hint_weight_set(mLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    elm_layout_file_set(mLayout, Application::mEdjPath, "get_messenger_main");
    evas_object_show(mLayout);
    elm_object_translatable_part_text_set(mLayout, "title", "IDS_MESSENGER_TITLE");

    Evas_Object *scroller = elm_scroller_add(mLayout);
    elm_scroller_single_direction_set(scroller, ELM_SCROLLER_SINGLE_DIRECTION_HARD);
    elm_scroller_policy_set(scroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_AUTO);
    elm_object_part_content_set(mLayout, "content", scroller);
    evas_object_show(scroller);

    elm_object_signal_callback_add(mLayout, "back.clicked", "btn", (Edje_Signal_Cb) BackButtonClicked, this);

    Evas_Object *data_area = elm_box_add(scroller);
    evas_object_size_hint_weight_set(data_area, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(data_area, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_box_padding_set(data_area, 0, 0);
    elm_object_content_set(scroller, data_area);
    evas_object_show(data_area);

    Evas_Object *header_text = elm_layout_add(data_area);
    evas_object_size_hint_weight_set(header_text, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    elm_layout_file_set(header_text, Application::mEdjPath, "get_messenger_content_header_text");
    elm_box_pack_end(data_area, header_text);
    evas_object_show(header_text);

    elm_object_translatable_part_text_set(header_text, "header_text", "IDS_MESSENGER_SLOGAN");
    Evas_Object *header_logo = elm_layout_add(data_area);
    evas_object_size_hint_weight_set(header_logo, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    elm_layout_file_set(header_logo, Application::mEdjPath, "get_messenger_content_logo");
    elm_box_pack_end(data_area, header_logo);
    evas_object_show(header_logo);

    Evas_Object *btn = elm_layout_add(data_area);
    evas_object_size_hint_weight_set(btn, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    elm_layout_file_set(btn, Application::mEdjPath, "get_messenger_content_btn");
    elm_box_pack_end(data_area, btn);
    evas_object_show(btn);

    elm_object_translatable_part_text_set(btn, "btn_text", "IDS_GET_MESSENGER");
    elm_object_signal_callback_add(btn, "get.messenger", "btn", (Edje_Signal_Cb) GetMessengerClicked, this);

    Evas_Object *photo_videos = elm_layout_add(data_area);
    evas_object_size_hint_weight_set(photo_videos, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    elm_layout_file_set(photo_videos, Application::mEdjPath, "get_messenger_info_text");
    elm_box_pack_end(data_area, photo_videos);
    evas_object_show(photo_videos);

    Evas_Object *photo_icon = elm_image_add(photo_videos);
    elm_object_part_content_set(photo_videos, "icon", photo_icon);
    elm_image_file_set(photo_icon, ICON_DIR"/GetMessenger/feature_pic_photos.png", NULL);
    evas_object_show(photo_icon);

    char *photo_videos_text = NULL;
    Evas_Object *photo_videos_text_label = elm_label_add(photo_videos);
    elm_object_part_content_set(photo_videos, "text", photo_videos_text_label);
    evas_object_size_hint_weight_set(photo_videos_text_label, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(photo_videos_text_label, 0.0, 0.0);
    elm_label_wrap_width_set(photo_videos_text_label, textWidth);
    elm_label_line_wrap_set(photo_videos_text_label, ELM_WRAP_MIXED);

    photo_videos_text = WidgetFactory::WrapByFormat2(SANS_MEDIUM_9197a3_FORMAT, "27", i18n_get_text("IDS_MESSENGER_PHOTOS_AND_VIDEOS"));
    if (photo_videos_text) {
        elm_object_text_set(photo_videos_text_label, photo_videos_text);
        delete[] photo_videos_text;
    }
    evas_object_show(photo_videos_text_label);

    Evas_Object *calls = elm_layout_add(data_area);
    evas_object_size_hint_weight_set(calls, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    elm_layout_file_set(calls, Application::mEdjPath, "get_messenger_info_text");
    elm_box_pack_end(data_area, calls);
    evas_object_show(calls);

    Evas_Object *calls_icon = elm_image_add(calls);
    elm_object_part_content_set(calls, "icon", calls_icon);
    elm_image_file_set(calls_icon, ICON_DIR"/GetMessenger/feature_pic_phone.png", NULL);
    evas_object_show(calls_icon);

    char *calls_text = NULL;
    Evas_Object *calls_text_label = elm_label_add(calls);
    elm_object_part_content_set(calls, "text", calls_text_label);
    evas_object_size_hint_weight_set(calls_text_label, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(calls_text_label, 0.0, 0.0);
    elm_label_wrap_width_set(calls_text_label, textWidth);
    elm_label_line_wrap_set(calls_text_label, ELM_WRAP_MIXED);

    calls_text = WidgetFactory::WrapByFormat2(SANS_MEDIUM_9197a3_FORMAT, "27", i18n_get_text("IDS_MESSENGER_CALLS"));
    if (calls_text) {
        elm_object_text_set(calls_text_label, calls_text);
        delete[] calls_text;
    }
    evas_object_show(calls_text_label);

    Evas_Object *stickers = elm_layout_add(data_area);
    evas_object_size_hint_weight_set(stickers, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    elm_layout_file_set(stickers, Application::mEdjPath, "get_messenger_info_text");
    elm_box_pack_end(data_area, stickers);
    evas_object_show(stickers);

    Evas_Object *stickers_icon = elm_image_add(stickers);
    elm_object_part_content_set(stickers, "icon", stickers_icon);
    elm_image_file_set(stickers_icon, ICON_DIR"/GetMessenger/feature_pic_stikers.png", NULL);
    evas_object_show(stickers_icon);

    char *stickers_text = NULL;
    Evas_Object *stickers_text_label = elm_label_add(stickers);
    elm_object_part_content_set(stickers, "text", stickers_text_label);
    evas_object_size_hint_weight_set(stickers_text_label, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(stickers_text_label, 0.0, 0.0);
    elm_label_wrap_width_set(stickers_text_label, textWidth);
    elm_label_line_wrap_set(stickers_text_label, ELM_WRAP_MIXED);

    stickers_text = WidgetFactory::WrapByFormat2(SANS_MEDIUM_9197a3_FORMAT, "27", i18n_get_text("IDS_MESSENGER_STICKERS"));
    if (stickers_text) {
        elm_object_text_set(stickers_text_label, stickers_text);
        delete[] stickers_text;
    }
    evas_object_show(stickers_text_label);

    Evas_Object *voice = elm_layout_add(data_area);
    evas_object_size_hint_weight_set(voice, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    elm_layout_file_set(voice, Application::mEdjPath, "get_messenger_info_text");
    elm_box_pack_end(data_area, voice);
    evas_object_show(voice);

    Evas_Object *voice_icon = elm_image_add(voice);
    elm_object_part_content_set(voice, "icon", voice_icon);
    elm_image_file_set(voice_icon, ICON_DIR"/GetMessenger/feature_pic_voice.png", NULL);
    evas_object_show(voice_icon);

    char *voice_text = NULL;
    Evas_Object *voice_text_label = elm_label_add(voice);
    elm_object_part_content_set(voice, "text", voice_text_label);
    evas_object_size_hint_weight_set(voice_text_label, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(voice_text_label, 0.0, 0.0);
    elm_label_wrap_width_set(voice_text_label, textWidth);
    elm_label_line_wrap_set(voice_text_label, ELM_WRAP_MIXED);

    voice_text = WidgetFactory::WrapByFormat2(SANS_MEDIUM_9197a3_FORMAT, "27", i18n_get_text("IDS_MESSENGER_VOICE"));
    if (voice_text) {
        elm_object_text_set(voice_text_label, voice_text);
        delete[] voice_text;
    }
    evas_object_show(voice_text_label);

    Evas_Object *data_charges = elm_layout_add(data_area);
    evas_object_size_hint_weight_set(data_charges, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    elm_layout_file_set(data_charges, Application::mEdjPath, "get_messenger_data_charge");
    elm_box_pack_end(data_area, data_charges);
    evas_object_show(data_charges);

    elm_object_translatable_part_text_set(data_charges, "text", "IDS_MESSENGER_CHARGES");
}

void GetMessengerScreen::Push()
{
    ScreenBase::Push();
    elm_naviframe_item_title_enabled_set( mNfItem, EINA_FALSE, EINA_FALSE );
}

void GetMessengerScreen::BackButtonClicked(void *data, Evas_Object *obj, void *event_info)
{
    GetMessengerScreen *screen = static_cast<GetMessengerScreen*>(data);
    screen->Pop();
}

void GetMessengerScreen::GetMessengerClicked(void *data, Evas_Object *obj, void *event_info)
{
    app_control_h service = NULL;
    int ret = -1;
    ret = app_control_create(&service);
    ret = app_control_set_app_id(service, "org.tizen.tizenstore");
    char l_app_name[1024];
    Utils::Snprintf_s(l_app_name, 1024, "tizenstore://ProductDetail/%s", MESSENGER_APPLICATION_ID);
    ret = app_control_set_uri(service,l_app_name);

    Log::debug( "Invoking Tizen store for %s", l_app_name);
    ret = app_control_set_operation(service, APP_CONTROL_OPERATION_VIEW);
    ret = app_control_send_launch_request(service, NULL, NULL);
}

