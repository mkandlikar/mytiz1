#include <efl_extension_events.h>
#include <Elementary.h>
#include <Evas.h>

#include "Application.h"
#include "BaseObject.h"
#include "Popup.h"
#include "WidgetFactory.h"
#include "Log.h"

UIRes * PopupMenu::R = UIRes::GetInstance();

PopupMenu::PopupMenuItem *PopupMenu::mItems = nullptr;
int PopupMenu::mItemNum = 0;
int PopupMenu::mY = 0;
bool PopupMenu::mIsTopOffset = false;
Evas_Object * PopupMenu::mPopup = nullptr;

/**
 * @brief Shows the popup menu on application screen
 * @param itemNum - the number of menu items
 * @param items - items description
 * @param parent - parent container for popup menu
 * @param useOldPosition - indicate that should be used previous y position of popup menu
 * @param y - y coordinate where popup menu should appear
 * @param contextObject - optional param to control refs consistency
 */
void PopupMenu::Show(int itemNum, PopupMenuItem * items, Evas_Object * parent,
                     bool useOldPosition, Evas_Coord y, BaseObject *contextObject)
{
    if (mPopup) {
        menu_dismiss_cb();
    }

    if (items && itemNum > 0) {
        mPopup = elm_popup_add(parent);
        if (mPopup) {
            if (contextObject) {
                contextObject->AddRef();
                evas_object_data_set(mPopup, "ContextData", contextObject);
            }
            mItemNum = itemNum;
            mItems = items;

            elm_popup_align_set(mPopup, ELM_NOTIFY_ALIGN_FILL, 1.0);
            evas_object_size_hint_weight_set(mPopup, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);

            Evas_Object *content = elm_box_add(mPopup);
            evas_object_data_set(mPopup, "PopupContent", content);
            evas_object_size_hint_weight_set(content, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
            evas_object_size_hint_align_set(content, EVAS_HINT_FILL, EVAS_HINT_FILL);
            elm_box_padding_set(content, 0, 0);

            int menuHeight = 0;
            for (int i = 0; i < itemNum; i++) {
                WidgetFactory::CreateSelectorItemWithIcon(content,
                        items[i].itemSelected, items[i].itemIcon,
                        items[i].itemText, items[i].itemSubText,
                        items[i].itemLastItem, menu_item_sel_cb,
                        (void *) items[i].itemId);
                menuHeight += R->POPUP_WIDGET_MENU_MULTI_ITEM_HEIGHT;
            }

            evas_object_size_hint_min_set(content, R->SCREEN_SIZE_WIDTH, menuHeight);
            evas_object_size_hint_max_set(content, R->SCREEN_SIZE_WIDTH, menuHeight);

            if (useOldPosition) {
                y = mY;
            } else {
                mY = y;
            }

            if ((y + menuHeight > R->POPUP_WIDGET_MENU_ITEM_SCREEN_HEIGHT) || (useOldPosition && mIsTopOffset)) {
                y = y - menuHeight - 10 - R->POPUP_WIDGET_MENU_ITEM_OFFSET;
                mIsTopOffset = true;
            } else {
                y += R->POPUP_WIDGET_MENU_ITEM_OFFSET;
                mIsTopOffset = false;
            }

            // In current implementation x position is ignored
            evas_object_move(content, 0, y);
            evas_object_resize(content, R->SCREEN_SIZE_WIDTH, menuHeight);
            evas_object_show(content);
            evas_object_smart_callback_add(mPopup, "block,clicked", menu_dismiss_cb, NULL);
            evas_object_show(mPopup);
        }
    }
}

void PopupMenu::ShowMore(int itemNum, PopupMenuItem * items, Evas_Object * parent)
{
    Show(itemNum, items, parent, true, 0);
}

/**
 * @brief Closes popup menu
 * @return true if popup is closed
 */
bool PopupMenu::Close()
{
    bool res = mPopup;

    if (res) {
        // Content is blinking if it's removed with popup dismiss call, so let's remove it manually
        Evas_Object *content = static_cast<Evas_Object *>(evas_object_data_get(mPopup, "PopupContent"));
        if (content) {
            evas_object_del(content); content = nullptr;
            evas_object_data_del(mPopup, "PopupContent");
        }

        evas_object_smart_callback_add(mPopup, "dismissed", menu_dismiss_cb, NULL);
        elm_popup_dismiss(mPopup);
    }
    return res;
}

/**
 * @brief This callback is called to close popup menu
 * @param data [in] - user data passed in callback
 * @param obj [in] - object which generates click event
 * @param emission [in] - signal name
 * @param source [in] - signal source
 */
void PopupMenu::menu_dismiss_cb(void *data, Evas_Object *obj, void *event_info)
{
    BaseObject *contextObject = static_cast<BaseObject *>(evas_object_data_get(mPopup, "ContextData"));
    if (contextObject) {
        contextObject->Release();
    }

    evas_object_smart_callback_del(mPopup, "block,clicked", menu_dismiss_cb);
    evas_object_smart_callback_del(mPopup, "dismissed", menu_dismiss_cb);
    evas_object_del(mPopup); mPopup = nullptr;

    mItems = nullptr;
    mItemNum = -1;
}

/**
 * @brief This callback is called share menu item is selected
 * @param data [in] - user data passed in callback
 * @param obj [in] - object which generates click event
 * @param event_info [in] - event information
 */
void PopupMenu::menu_item_sel_cb(void *data, Evas_Object *obj, void *event_info)
{
    int index = (int) data;

    if (mItems && (index < mItemNum)) {
        BaseObject *contextObject = static_cast<BaseObject *>(evas_object_data_get(mPopup, "ContextData"));
        if (contextObject) {
            contextObject->AddRef();
        }

        PopupMenuItem item = mItems[index];
        Close();

        if (item.callback) {
            item.callback(item.data ? item.data : contextObject, obj, event_info);
        }

        if (contextObject) {
            contextObject->Release();
        }
    }
}

bool PopupMenu::IsPopupShown()
{
    return mPopup;
}
