#include "Application.h"
#include "PopupBase.h"

UIRes * PopupBase::R = UIRes::GetInstance();
PopupBase *PopupBase::mPopup = NULL;


PopupBase::PopupBase() : mPopupLayout(NULL), mSpacer(nullptr) {
}

PopupBase::~PopupBase(){
    if (mPopupLayout) {
        elm_object_signal_callback_del(mPopupLayout, "block,clicked", "*", dismiss_cb);
        evas_object_del(mPopupLayout); mPopupLayout = NULL;
    }
    mPopup = NULL;
}

Evas_Object *PopupBase::GetPopupLayout() {
    return mPopupLayout;
}

void PopupBase::Show(Evas_Object * parent, Evas_Coord y) {
    delete mPopup;
    mPopup = this;

    mPopupLayout = elm_layout_add(parent);
    elm_layout_file_set(mPopupLayout, Application::mEdjPath, "popup_base");
    elm_object_signal_callback_add(mPopupLayout, "block,clicked", "*", dismiss_cb, this);

    mSpacer = elm_box_add(mPopupLayout);
    evas_object_size_hint_align_set(mSpacer, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_size_hint_weight_set(mSpacer, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_min_set(mSpacer, R->SCREEN_SIZE_WIDTH, y);
    evas_object_size_hint_max_set(mSpacer, R->SCREEN_SIZE_WIDTH, y);
    elm_layout_content_set(mPopupLayout, "swallow.spacer", mSpacer);
    evas_object_show(mSpacer);

    Evas_Object *content = CreateContent();
    if (content) {
        elm_layout_content_set(mPopupLayout, "swallow.popup_content", content);
    }

    //this is a workaround for RTL mirroring. Otherwise the popup is displayed outside the screen.
    if (Application::IsRTLLanguage()) {
        evas_object_move(mPopupLayout, R->SCREEN_SIZE_WIDTH, 0);
    } else {
        evas_object_move(mPopupLayout, 0, 0);
    }
    evas_object_show(mPopupLayout);
}

void PopupBase::Move(Evas_Coord y) {
    if (mPopup && mPopup->mSpacer) {
        evas_object_size_hint_min_set(mPopup->mSpacer, R->SCREEN_SIZE_WIDTH, y);
        evas_object_size_hint_max_set(mPopup->mSpacer, R->SCREEN_SIZE_WIDTH, y);
    }
}

bool PopupBase::IsShown() {
    return mPopup;
}

void PopupBase::dismiss_cb(void *data, Evas_Object *obj, const char *emission, const char *source) {
    Close();
}

bool PopupBase::Close() {
    if (mPopup) {
        delete mPopup;
        mPopup = NULL;
        return true;
    } else {
        return false;
    }
}
