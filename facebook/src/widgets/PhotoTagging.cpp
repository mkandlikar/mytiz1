#include "Application.h"
#include "FacebookSession.h"
#include "LoggedInUserData.h"
#include "OwnFriendsProvider.h"
#include "PhotoTagging.h"
#include "WidgetFactory.h"

UIRes * PhotoTagging::R = UIRes::GetInstance();
/*
 * struct PhotoTagging::SearchItem
 */
PhotoTagging::SearchItem::SearchItem(const char* picturePath, const char * id, const char * name) {
    mPicturePath = SAFE_STRDUP(picturePath);
    mId = SAFE_STRDUP(id);
    mName = SAFE_STRDUP(name);
}

PhotoTagging::SearchItem::~SearchItem() {
    free(mPicturePath);
    free(mId);
    free(mName);
}
/*
 * struct PhotoTagging::PCData
 */
PhotoTagging::PTData::PTData(PhotoTagging *pt, SearchItem *item) : mPT(pt), mItem(item) { }
PhotoTagging::PTData::~PTData() { if (mItem) delete mItem; }

/*
 * Class PhotoTagging
 */
PhotoTagging::~PhotoTagging(){
    if (mTagLayout) {
        evas_object_del(mTagLayout);
    }
    FacebookSession::GetInstance()->ReleaseGraphRequest(mRequest);
    if (mPhotoTag)
        mPhotoTag->Release();
    evas_object_del(mTextBlockEmulator); mTextBlockEmulator = nullptr;
    evas_object_del(mEntryEmulator); mEntryEmulator = nullptr;
}

PhotoTagging::PhotoTagging(ActionAndData * actionAndData, Evas_Object * layout, PhotoTag * photoTag, ImageRegion * imageRegion, bool isEditView, Evas_Object * clipper, Evas_Object *above) :
        mParentLayout(layout), mTagLayout(nullptr), mEntryItem(nullptr), mEntry(nullptr), mImageRegion(imageRegion), mIsEditMode(isEditView), mParent(nullptr),
        mBufferString(""), mActionAndData(actionAndData), mClipper(clipper), mAboveLayout(above) {
    mX = mY = mH = mW = 0;
    photoTag->AddRef();
    mPhotoTag = photoTag;
    CreateEntryEmulator();
    CreateTag(mBufferString.c_str(), nullptr);
}

PhotoTagging::PhotoTagging(Evas_Object * layout, double xTap, double yTap, ImageRegion * imageRegion, PTInterface *parent, Evas_Object * clipper, Evas_Object * above) :
        mParentLayout(layout), mEntryItem(nullptr), mEntry(nullptr), mImageRegion(imageRegion), mParent(parent), mBufferString(""), mClipper(clipper), mAboveLayout(above) {
    mIsEditMode = true;
    mX = mY = mH = mW = 0;
    mTagText = nullptr;
    mTagLayout = nullptr;
    mPhotoTag = new PhotoTag(xTap, yTap);
    mRequest = nullptr;
    mActionAndData = nullptr;
    CreateEntryEmulator();
    evas_object_move(CreateSearchEntry(), R->PT_POPUP_X_POSITION, R->PT_POPUP_Y_POSITION);
}

void PhotoTagging::CreateEntryEmulator() {
    mEntryEmulator = elm_entry_add(mParentLayout);
    evas_object_size_hint_weight_set(mEntryEmulator, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    char *entryStyle = WidgetFactory::WrapByFormat(SANS_REGULAR_BLACK_STYLE, R->PT_TAG_FONT_SIZE);
    if (entryStyle) {
        elm_entry_text_style_user_push(mEntryEmulator, entryStyle);
        delete[] entryStyle;
    }
    mTextBlockEmulator = elm_entry_textblock_get(mEntryEmulator);
}

void PhotoTagging::GetX() {
    double photoTagX = mPhotoTag->mX;
    double minValue = (double)R->PT_IMAGE_BODY_OFFSET * 100 / R->SCREEN_SIZE_WIDTH;
    if (photoTagX < minValue) {
        photoTagX = minValue;
    } else if (photoTagX > 100 - minValue) {
        photoTagX = 100 - minValue;
    }
    mX = mImageRegion->x + (double) mImageRegion->width * photoTagX / 100;
}

void PhotoTagging::GetY() {
    mY = mImageRegion->y + (double) (mImageRegion->height * mPhotoTag->mY) / 100;
}

bool PhotoTagging::CreateTag(const char * tagName, const char * tagId){
    if (!mPhotoTag->mIsPhotoTag) return false;
    if (mPhotoTag->mTaggedUserName.empty()) {
        if (!Utils::isEmptyString(tagName)) {
            char *tagNameUtf8 = elm_entry_markup_to_utf8(tagName);
            mPhotoTag->mTaggedUserName = tagNameUtf8;
            free(tagNameUtf8);
            mPhotoTag->mTaggedUserId = tagId ? std::string(tagId) : "";
        } else {
            return false;
        }
    }
    char *taggedUserName = elm_entry_utf8_to_markup(mPhotoTag->mTaggedUserName.c_str());

    GetX();
    GetY();

    mTagLayout = elm_layout_add(mParentLayout);

    elm_layout_file_set(mTagLayout, Application::mEdjPath, "photo_tag");

    mTagText = elm_label_add(mTagLayout);
    elm_layout_content_set(mTagLayout, "content", mTagText);

    if (mActionAndData) {
        mRequest = FacebookSession::GetInstance()->GetPageDetails(mPhotoTag->mTaggedUserId.c_str(), on_get_tagged_user_details_completed, this);
        evas_object_smart_callback_add(mTagText, "anchor,clicked", ActionBase::on_anchor_clicked_cb, mActionAndData);
    }
    char *selectorText = WidgetFactory::WrapByFormat2("<font_size=%s color=#ffffff>%s</>", R->PT_TAG_FONT_SIZE, taggedUserName);
    if (selectorText) {
        elm_object_text_set(mTagText, selectorText);
        delete[] selectorText;
    }
    evas_object_show(mTagText);

    elm_entry_entry_set(mEntryEmulator, taggedUserName);

    evas_object_textblock_size_native_get(mTextBlockEmulator, &mW, &mH);
    mW -= 10; //Workaround: even empty string has 10 point length

    Evas_Object *leftOffset = evas_object_rectangle_add(mTagLayout);
    evas_object_color_set(leftOffset, 0, 0, 0, 0);
    elm_layout_content_set(mTagLayout, "left_offset", leftOffset);
    evas_object_show(leftOffset);

    CalculateTagLocation();
    evas_object_show(mTagLayout);
    free(taggedUserName);

    if (mClipper) {
        evas_object_clip_set(mTagLayout, mClipper);
    }

    if (mAboveLayout) {
        evas_object_stack_below(mTagLayout, mAboveLayout);
    }

    return true;
}

void PhotoTagging::on_delete_button_clicked(void *user_data, Evas_Object *obj, const char *emission, const char *source) {
    PhotoTagging * me = static_cast<PhotoTagging *>(user_data);
    me->mIsNeedDelete = true;
    me->Hide();
}

void PhotoTagging::CalculateTagLocation() {
    Log::debug("PhotoTagging::CalculateTagLocation()");
    int w = mW;
    int leftOffset = 0;
    w += 2*R->PT_IMAGE_BODY_TEXT_OFFSET;
    if(mIsEditMode){
       elm_object_signal_emit(mTagLayout, "set_delete_mode", "");
       elm_object_signal_callback_add(mTagLayout, "mouse,up,*", "delete_button", on_delete_button_clicked, this);
       w += R->PT_IMAGE_BODY_DELETE_WIDTH;
    }
    if (w > R->PT_IMAGE_BODY_MAX_WIDTH) {

        if (Application::IsRTLLanguage()) {
            leftOffset = R->SCREEN_SIZE_WIDTH - (mX + R->PT_IMAGE_BODY_OFFSET);
        } else {
            leftOffset = mX - R->PT_IMAGE_BODY_OFFSET;
        }

        elm_object_signal_emit(mTagLayout, "long_content", "content");
        elm_label_ellipsis_set(mTagText, true);
    } else {
        leftOffset = w / 2;
        int diffX = mX - w / 2;

        int diffX2 = R->SCREEN_SIZE_WIDTH - mX - w / 2;

        if (diffX < 0) {
            if (Application::IsRTLLanguage()) {
                leftOffset = w - mX;
            } else {
                leftOffset = mX;
            }
        } else if (diffX2 < 0) {
            if (Application::IsRTLLanguage()) {
                 leftOffset = R->SCREEN_SIZE_WIDTH - mX;
            } else {
                 leftOffset = mX - (R->SCREEN_SIZE_WIDTH - w);
            }
        }
    }

    evas_object_size_hint_min_set(elm_object_part_content_get(mTagLayout, "left_offset"), leftOffset, 2);
    evas_object_size_hint_max_set(elm_object_part_content_get(mTagLayout, "left_offset"), leftOffset, 2);
    evas_object_resize(mTagLayout, leftOffset, 2);

    evas_object_move(mTagLayout, mX , mY);
}

void PhotoTagging::on_get_tagged_user_details_completed(void *data, char* str, int code) {
    PhotoTagging *me = static_cast<PhotoTagging *>(data);
    if (me && code == CURLE_OK) {
        char *selector_text = nullptr;
        JsonObject * rootObject = nullptr;
        JsonParser * parser = openJsonParser(str, &rootObject);
        const char * category = json_object_get_string_member(rootObject, "category");
        char *taggedUserName = elm_entry_utf8_to_markup(me->mPhotoTag->mTaggedUserName.c_str());
        if (category) {
            selector_text = WidgetFactory::WrapByFormat3("<font_size=%s color=#ffffff><a href=l_%s>%s</a></>", R->PT_TAG_FONT_SIZE,
                    json_object_get_string_member(rootObject, "link"), taggedUserName);
        } else {
            if (!me->mPhotoTag->mTaggedUserId.empty()){
                selector_text = WidgetFactory::WrapByFormat3("<font_size=%s color=#ffffff><a href=u_%s>%s</a></>", R->PT_TAG_FONT_SIZE,
                    me->mPhotoTag->mTaggedUserId.c_str(), taggedUserName);
            } else {
                selector_text = WidgetFactory::WrapByFormat2("<font_size=%s color=#ffffff>%s</>", R->PT_TAG_FONT_SIZE,
                        taggedUserName);
            }
        }
        g_object_unref(parser);
        if (selector_text) {
            elm_object_text_set(me->mTagText, selector_text);
            delete[] selector_text;
        }
        free(taggedUserName);
    }
    FacebookSession::GetInstance()->ReleaseGraphRequest(me->mRequest);
}

void PhotoTagging::UpdateImageGeometry(ImageRegion *newImageRegion) {
    if (mTagLayout) {
        GetX();
        GetY();
        evas_object_move(mTagLayout, mX, mY);
    }
}

bool PhotoTagging::HidePTPopup() {
    bool ret = PhotoTaggingPopup::IsShown();
    if (ret) {
        PhotoTaggingPopup::Close();
        Log::debug("POPUP HIDDEN!");
    }
    return ret;
}

void PhotoTagging::OnPopupItemCb(SearchItem *item) {
    StartCreatingTag(item->mName, item->mId);
}

void PhotoTagging::StartCreatingTag(const char * tagName, const char * tagId) {
    mIsNeedCreate = CreateTag(tagName, tagId);
    ClosePopUp();
}

void PhotoTagging::ShowPopup( Eina_List * items) {
    PhotoTaggingPopup::Open(mParentLayout, this, true, items);
    PhotoTaggingPopup::Move(R->PT_POPUP_Y_POSITION + R->PT_POPUP_ITEM_Y_OFFSET);
}

void PhotoTagging::DoSearch() {
    Eina_List * itemsContaner = nullptr;
    if (!mBufferString.empty()) {
        Eina_Array *friendsArray = OwnFriendsProvider::GetInstance()->GetData();
        int friendsTotalCount = eina_array_count(friendsArray);
        bool comparisonFound = false;
        for (int k = 0; k < friendsTotalCount; k++) {
            Friend *ownFriend = static_cast<Friend*>(eina_array_data_get(friendsArray, k));
            comparisonFound = IsItemFound(ownFriend->mFirstName.c_str(), ownFriend->mLastName.c_str(), mBufferString.c_str()) ||
                              IsItemFound(ownFriend->mName.c_str(), "", mBufferString.c_str());
            if (comparisonFound) {
                itemsContaner = eina_list_append(itemsContaner,
                        new SearchItem(ownFriend->mPicturePath.c_str(), ownFriend->GetId(), ownFriend->mName.c_str()));
            }
        }
        if (LoggedInUserData::GetInstance().mFirstName && LoggedInUserData::GetInstance().mLastName) {
            comparisonFound = IsItemFound(LoggedInUserData::GetInstance().mFirstName, LoggedInUserData::GetInstance().mLastName, mBufferString.c_str()) ||
                              IsItemFound(LoggedInUserData::GetInstance().mName, "", mBufferString.c_str());
            if (comparisonFound) {
                itemsContaner = eina_list_append(itemsContaner,
                        new SearchItem(LoggedInUserData::GetInstance().mPicture->mUrl, LoggedInUserData::GetInstance().mId, LoggedInUserData::GetInstance().mName));
            }
        }
    }
    HidePTPopup();
    ShowPopup(itemsContaner);
}

bool PhotoTagging::IsItemFound(const char *firstName, const char *lastName, const char *tocompare) {
    int len = Utils::UStrLen(tocompare);
    return !Utils::UCompareN(tocompare, firstName, len, false) ||
           !Utils::UCompareN(tocompare, lastName, len, false);
}

Evas_Object * PhotoTagging::CreateSearchEntry() {
    mEntryItem = elm_layout_add(mParentLayout);
    evas_object_size_hint_weight_set(mEntryItem, 1, 1);
    elm_layout_file_set(mEntryItem, Application::mEdjPath, "photo_tag_entry_item");
    mEntry = elm_entry_add(mEntryItem);
    elm_entry_single_line_set(mEntry, EINA_TRUE);
    elm_layout_content_set(mEntryItem, "item_spacer", mEntry);
    elm_entry_scrollable_set(mEntry, EINA_TRUE);
    evas_object_size_hint_align_set(mEntry, EVAS_HINT_FILL, 0.0);
    evas_object_size_hint_weight_set(mEntry, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_entry_prediction_allow_set(mEntry, EINA_FALSE);
    elm_entry_cnp_mode_set(mEntry, ELM_CNP_MODE_PLAINTEXT);
    elm_entry_input_panel_return_key_type_set(mEntry, ELM_INPUT_PANEL_RETURN_KEY_TYPE_DONE);
    evas_object_event_callback_add(mEntry, EVAS_CALLBACK_KEY_DOWN, entry_key_down_cb, this);
    FRMTD_TRNSLTD_PART_TXT_SET1(mEntry, "elm.guide", "<font_size=30 color=#cccccc>%s</font_size>", "IDS_WHO_IS_THIS");
    elm_entry_text_style_user_push(mEntry, "DEFAULT='color=#FFF font_size=30'");
    evas_object_show(mEntry);
    mParent->EnableDismissCallback(true);


    elm_object_signal_callback_add(mEntryItem, "block,clicked", "*", menu_dismiss_cb, this);
    evas_object_smart_callback_add(mEntry, "changed", entry_changed_cb, this);
    evas_object_show(mEntryItem);
    DoSearch();
    elm_object_focus_set(mEntry, EINA_TRUE);
    elm_entry_input_panel_show(mEntry);
    return mEntryItem;
}

void PhotoTagging::entry_key_down_cb(void *data, Evas *e, Evas_Object *obj, void *event_info) {
    PhotoTagging *me = static_cast<PhotoTagging *>(data);
    Evas_Event_Key_Down *ev = static_cast<Evas_Event_Key_Down *> (event_info);
    if (me->mEntry && !strcmp(ev->keyname, "Return")) {
        me->StartCreatingTag(elm_entry_entry_get(me->mEntry), nullptr);
    }
}

void PhotoTagging::menu_dismiss_cb(void *data, Evas_Object *obj, const char *emission, const char *source) {
    PhotoTagging * me = static_cast<PhotoTagging *>(data);
    if (me) {
        me->ClosePopUp();
    }
}

void PhotoTagging::entry_changed_cb(void* data, Evas_Object* obj, void* event_info) {
    PhotoTagging *pt = static_cast<PhotoTagging*>(data);
    pt->mBufferString.clear();
    pt->mBufferString.append(elm_entry_entry_get(obj));
    pt->DoSearch();
}

bool PhotoTagging::ClosePopUp() {
    if (mParent && mEntryItem && mEntry) {
        evas_object_smart_callback_del(mEntry, "changed", entry_changed_cb);
        elm_object_signal_callback_del(mEntryItem, "block,clicked", "*", menu_dismiss_cb);
        mParent->EnableDismissCallback(false);
        evas_object_del(mEntry); mEntry = nullptr;
        evas_object_del(mEntryItem); mEntryItem = nullptr;
        HidePTPopup();
        mParent->FitToUserScreen();
        return true;
    } else {
        return false;
    }
}

/*
 * Class PhotoTagging::PhotoTaggingPopup
 */
PhotoTagging::PhotoTaggingPopup::PhotoTaggingPopup(PhotoTagging *tagging, bool upsideDown, Eina_List * items) :
        mItems(items), mPTagging(tagging), mHeight(0) {
}

void PhotoTagging::PhotoTaggingPopup::Open(Evas_Object *parent, PhotoTagging *tagging, bool upsideDown, Eina_List * items) {
    PhotoTaggingPopup *me = new PhotoTaggingPopup(tagging, upsideDown, items);
    me->Show(parent, 0);
    elm_object_signal_emit(me->GetPopupLayout(), "block_clicked_off", "popup_bg");
}

PhotoTagging::PhotoTaggingPopup::~PhotoTaggingPopup() { }
Evas_Object *PhotoTagging::PhotoTaggingPopup::WrapWithSize(Evas_Object *parent, Evas_Object *obj, Evas_Coord w, Evas_Coord h) {
    Evas_Object *rect;
    Evas_Object *layout = elm_layout_add(parent);
    elm_layout_file_set(layout, Application::mEdjPath, "fixed_size_layout");
    rect = evas_object_rectangle_add(evas_object_evas_get(layout));
    evas_object_color_set(rect, 0, 0, 0, 0);
    evas_object_size_hint_min_set(rect, w, h);
    evas_object_size_hint_max_set(rect, w, h);
    evas_object_size_hint_weight_set(rect, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(rect, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_layout_content_set(layout, "swallow.spacer", rect);
    elm_layout_content_set(layout, "swallow.scroller", obj);
    evas_object_show(rect);
    return layout;
}

Evas_Object *PhotoTagging::PhotoTaggingPopup::CreateContent() {
    Evas_Object *scroller = elm_scroller_add(GetPopupLayout());
    evas_object_size_hint_weight_set(scroller, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(scroller, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_scroller_propagate_events_set(scroller, EINA_FALSE);
    elm_scroller_single_direction_set(scroller, ELM_SCROLLER_SINGLE_DIRECTION_HARD);
    elm_scroller_policy_set(scroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_OFF);

    Evas_Object *box = elm_box_add(scroller);
    elm_object_content_set(scroller, box);
    evas_object_size_hint_weight_set(box, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(box, EVAS_HINT_FILL, EVAS_HINT_FILL);

    int size = eina_list_count(mItems);
    if (mPTagging->mBufferString.empty() || mPTagging->mBufferString[0] == ' ') {
        mHeight += R->PT_POPUP_ITEM_HEIGHT + 1;
        elm_box_pack_end(box,
                CreatePopupItem( new PTData(mPTagging,
                                 new SearchItem(LoggedInUserData::GetInstance().mPicture->mUrl, LoggedInUserData::GetInstance().mId,
                                     LoggedInUserData::GetInstance().mName)), box, LAST));
    } else if (size != 0) {
        Eina_List *l;
        void *data;
        int count = 1;
        EINA_LIST_FOREACH(mItems, l, data) {
            PopupItemType popuType = (count == size || count == mMaxItemsNum) ? LAST : MIDDLE;
            mHeight += R->PT_POPUP_ITEM_HEIGHT;
            elm_box_pack_end(box, CreatePopupItem(new PTData(mPTagging, static_cast<SearchItem *>(data)), box, popuType));
            count++;
            if (count > mMaxItemsNum) {
                break;
            }
        }
    } else if (!mPTagging->mBufferString.empty()) {
        mHeight += R->PT_POPUP_ITEM_HEIGHT;
        elm_box_pack_end(box, CreatePopupItem(new PTData(mPTagging, new SearchItem(nullptr, nullptr, mPTagging->mBufferString.c_str())), box, TEXT));
    }
    return WrapWithSize(GetPopupLayout(), scroller, R->PT_POPUP_WIDTH, mHeight);
}

Evas_Object *PhotoTagging::PhotoTaggingPopup::CreatePopupItem(PTData *data, Evas_Object *parent, PopupItemType type) {
    SearchItem *item = data->GetItem();
    Evas_Object *popupItem = elm_layout_add(parent);
    evas_object_size_hint_weight_set(popupItem, 1, 1);
    if(type == MIDDLE) {
        elm_layout_file_set(popupItem, Application::mEdjPath, "photo_tag_popup_item");
        elm_object_part_text_set(popupItem, "item_name", item->mName);
    } else if(type == LAST) {
        elm_layout_file_set(popupItem, Application::mEdjPath, "photo_last_tag_popup_item" );
        elm_object_part_text_set(popupItem, "item_name", item->mName);
    } else if(type == TEXT) {
        elm_layout_file_set(popupItem, Application::mEdjPath, "photo_tag_text_item");
        elm_object_translatable_part_text_set(popupItem, "item_name", "IDS_TEXT_TAG");
    }
    if (item->mPicturePath) {
        Evas_Object *avatar = elm_image_add(popupItem);
        elm_layout_content_set(popupItem, "item_icon", avatar);
        elm_image_file_set(avatar, item->mPicturePath, nullptr);
        evas_object_show(avatar);
    }
    evas_object_event_callback_add(popupItem, EVAS_CALLBACK_DEL, on_popup_item_delete_cb, data);
    elm_object_signal_callback_add(popupItem, "mouse,clicked,*", "item_*", on_popup_item_cb, data);
    evas_object_show(popupItem);
    return popupItem;
}

void PhotoTagging::PhotoTaggingPopup::on_popup_item_delete_cb(void *data, Evas *e, Evas_Object *obj, void *event_info) {
    delete static_cast<PTData*>(data);
}

void PhotoTagging::PhotoTaggingPopup::on_popup_item_cb(void *data, Evas_Object *obj, const char *emission, const char *source) {
    PTData *pc_data = static_cast<PTData*>(data);
    PhotoTagging *screen = static_cast<PhotoTagging*>(pc_data->GetPT());
    screen->OnPopupItemCb(pc_data->GetItem());
}
