#include "AddCropScreen.h"
#include "AddTextInputScreen.h"
#include "AddTextScreen.h"
#include "Application.h"
#include "ComposerPhotoWidget.h"
#include "ImagesCarouselScreen.h"
#include "Log.h"
#include "PostComposerScreen.h"
#include "PresenterHelpers.h"
#include "SelectedMediaList.h"
#include "VideoPlayerScreen.h"
#include "WidgetFactory.h"

static const char* LogTag = "COMPOSER_PHOTO_WIDGET";

ComposerPhotoWidget::ComposerPhotoWidget(ScreenMode mode, Evas_Object *layout, Evas_Object *boxLayout, IPhotoWidgetEvents *photoWidgetCb) {
    mMode = mode;
    mLayout = layout;
    mMediaBox = boxLayout;
    mActionAndData = NULL;
    mIPhotoWidgetCb = photoWidgetCb;

    CreateSelectedMedia();
}

ComposerPhotoWidget::~ComposerPhotoWidget() {
    Eina_List *mediaList = SelectedMediaList::Get();
    Eina_List *listItem;
    void *listData;
    EINA_LIST_FOREACH(mediaList, listItem, listData) {
        PostComposerMediaData *mediaData = static_cast<PostComposerMediaData *>(listData);
        mediaData->RemoveCaptionLayout();
    }

    delete mActionAndData;
}

void ComposerPhotoWidget::CreateSelectedMedia() {
    Eina_List *mediaList = SelectedMediaList::Get();
    Eina_List *listItem;
    void *listData;
    EINA_LIST_FOREACH(mediaList, listItem, listData) {
        PostComposerMediaData *mediaData = static_cast<PostComposerMediaData *>(listData);
        Evas_Object *wrapperBox = WidgetFactory::CreateWrapperByName(mMediaBox, "simple_wrapper");
        CreateMediaContent(wrapperBox, mediaData);
    }
}

bool ComposerPhotoWidget::IsDataValid(Eina_List *mediaList) {
    bool isDataValid = false;
    Eina_List *list = GetMediaItems();

    if (eina_list_count(list) == eina_list_count(mediaList)) {
        int index = 0;
        Eina_List *listItem;
        void *listData;
        EINA_LIST_FOREACH(list, listItem, listData) {
            Evas_Object *wrapperLayout = static_cast<Evas_Object*>(listData);
            Evas_Object *wrapperBox = elm_layout_content_get(wrapperLayout, "content");
            Eina_List *wrapperBoxChildList = elm_box_children_get(wrapperBox);
            Evas_Object *mediaLayout = static_cast<Evas_Object*>(eina_list_data_get(wrapperBoxChildList));
            eina_list_free(wrapperBoxChildList);
            Evas_Object *imageLayout = elm_layout_content_get(mediaLayout, "image");
            PostComposerMediaData *mediaData = static_cast<PostComposerMediaData *>(eina_list_nth(mediaList, index));
            const char *currentImagePath = nullptr;
            elm_image_file_get(imageLayout, &currentImagePath, nullptr);

            if (!mediaData || mediaData->GetImageLayout() != imageLayout || strcmp(mediaData->GetFilePath(), currentImagePath)) {
                // Data is not valid, stop the loop
                break;
            }
            index++;
        }

        isDataValid = (index == eina_list_count(mediaList));
    }

    eina_list_free(list);

    return isDataValid;
}

Eina_List* ComposerPhotoWidget::GetMediaItems() {
    Eina_List *list = elm_box_children_get(mMediaBox);
    Eina_List *l;
    Eina_List *l_next;
    void *data;

    EINA_LIST_FOREACH_SAFE(list, l, l_next, data) {
        Evas_Object *wrapperLayout = static_cast<Evas_Object*>(data);
        const char *groupName;
        edje_object_file_get(elm_layout_edje_get(wrapperLayout), nullptr, &groupName);
        if (strcmp(groupName, "simple_wrapper")) {
            list = eina_list_remove_list(list, l);
        }
    }
    return list;
}

void ComposerPhotoWidget::CreateMediaContent(Evas_Object *parent, PostComposerMediaData *mediaData) {
    Evas_Object *mediaLayout = WidgetFactory::CreateLayoutByGroup(parent, "post_composer_media_content");
    Evas_Object *img = elm_image_add(mediaLayout);
    elm_layout_content_set(mediaLayout, "image", img);
    elm_image_aspect_fixed_set(img, EINA_FALSE);
    elm_image_prescale_set(img, WidgetFactory::R->POST_COMPOSER_IMAGE_SIZE);
    if (mediaData->GetType() == MEDIA_CONTENT_TYPE_IMAGE) {
        if (mediaData->GetTempCropImagePath()) {
            elm_image_file_set(img, mediaData->GetTempCropImagePath(), nullptr);
        } else if (!mediaData->GetFilePath() && mediaData->GetSrcUrl()) {
            if (!mActionAndData) {
                mActionAndData = new ActionAndData(nullptr, nullptr);
            }
            mActionAndData->UpdateImageLayoutAsync(mediaData->GetSrcUrl(), img);
        } else {
            elm_image_file_set(img, mediaData->GetFilePath(), nullptr);
        }
    } else { //there are no full-size images for videos.
        elm_image_file_set(img, mediaData->GetThumbnailPath(), nullptr);
    }
    mediaData->SetImageLayout(img);
    // Calculate height of image
    int w, h;
    Evas_Object * imgEvas = elm_image_object_get(img);
    evas_object_image_size_get(imgEvas, &w, &h);
    evas_object_size_hint_min_set(img, WidgetFactory::R->POST_COMPOSER_IMAGE_SIZE, (w ? WidgetFactory::R->POST_COMPOSER_IMAGE_SIZE * h / w : 0));

    elm_box_pack_end(parent, mediaLayout);

    Evas_Object * imgGesture = elm_gesture_layer_add(mLayout);
    elm_gesture_layer_attach(imgGesture, img);

    PhotoWidgetUserData *photoWidgetUserData = new PhotoWidgetUserData(mIPhotoWidgetCb, mediaData);
    evas_object_event_callback_add(mediaLayout, EVAS_CALLBACK_DEL, on_media_layout_deleted, photoWidgetUserData);

    if (mediaData->GetType() == MEDIA_CONTENT_TYPE_IMAGE) {
        if (mMode == ePOST_COMPOSE) {
            // Open ImageCarousel on tap
            elm_gesture_layer_cb_set(imgGesture, ELM_GESTURE_N_TAPS, ELM_GESTURE_STATE_END, on_photo_clicked, mediaData);
            mediaData->SetImageObject(img);

#ifdef FEATURE_ADD_TEXT
             // Add extra text option
             Evas_Object *addTextBtn = elm_button_add(mediaLayout);
             elm_object_style_set(addTextBtn, Application::IsRTLLanguage() ? "post_composer_add_text_button_rtl" : "post_composer_add_text_button");
             elm_layout_content_set(mediaLayout, "add_text", addTextBtn);
             evas_object_smart_callback_add(addTextBtn, "clicked", on_add_text_button_clicked, mediaData);
             elm_object_signal_emit(mediaLayout, "show_add_text", "post_composer_screen");
#endif

             // Add crop option
            if (CheckIsAvailableToDecode(mediaData)) { //check if editing operations can be applied to this image
                Evas_Object *cropImageBtn = elm_button_add(mediaLayout);
                elm_object_style_set(cropImageBtn, "post_composer_crop_image_button");
                elm_layout_content_set(mediaLayout, "crop_image", cropImageBtn);
                evas_object_smart_callback_add(cropImageBtn, "clicked", on_crop_button_clicked, photoWidgetUserData);
                elm_object_signal_emit(mediaLayout, "show_crop_image", "post_composer_screen");
            }
        }

        Evas_Object *deleteBtn = elm_button_add(mediaLayout);
        elm_object_style_set(deleteBtn, "post_composer_delete_photo_button");
        elm_layout_content_set(mediaLayout, "delete", deleteBtn);
        evas_object_smart_callback_add(deleteBtn, "clicked", on_delete_media_button_clicked, photoWidgetUserData);

        CreatePhotoCaption(parent, mediaData);
    } else if (mediaData->GetType() == MEDIA_CONTENT_TYPE_VIDEO) {
        elm_gesture_layer_cb_set(imgGesture, ELM_GESTURE_N_TAPS, ELM_GESTURE_STATE_END, on_video_clicked, mediaData);

        Evas_Object *videoIndicatorBtn = elm_button_add(mediaLayout);
        elm_object_style_set(videoIndicatorBtn, "video_indicator");
        elm_layout_content_set(mediaLayout, "video_indicator", videoIndicatorBtn);
        elm_object_signal_emit(mediaLayout, "show_video_indicator", "post_composer_screen");

        Evas_Object *indicatorGesture = elm_gesture_layer_add(mLayout);
        elm_gesture_layer_attach(indicatorGesture, videoIndicatorBtn);
        elm_gesture_layer_cb_set(indicatorGesture, ELM_GESTURE_N_TAPS, ELM_GESTURE_STATE_END, on_video_clicked, mediaData);
    }
}

void ComposerPhotoWidget::CreatePhotoCaption(Evas_Object *parent, PostComposerMediaData *mediaData) {
    Evas_Object *captionLayout = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(parent, "post_composer_media_caption");
    mediaData->SetCaptionLayout(captionLayout);

    Evas_Object* caption = elm_entry_add(captionLayout);
    elm_layout_content_set(captionLayout, "caption", caption);
    elm_entry_prediction_allow_set(caption, EINA_FALSE);
    elm_entry_scrollable_set(caption, EINA_FALSE);
    elm_entry_cnp_mode_set(caption, ELM_CNP_MODE_PLAINTEXT);
    char *buf = WidgetFactory::WrapByFormat2(SANS_REGULAR_9298A4_FORMAT, WidgetFactory::R->POST_COMPOSER_TEXT_FONT_SIZE,
            i18n_get_text("IDS_ADD_A_CAPTION"));
    if (buf) {
        elm_object_part_text_set(caption, "elm.guide", buf);
        delete[] buf;
    }
    char *captStyle = WidgetFactory::WrapByFormat(SANS_REGULAR_BLACK_STYLE, WidgetFactory::R->POST_COMPOSER_TEXT_FONT_SIZE);
    if (captStyle) {
        elm_entry_text_style_user_push(caption, captStyle);
        delete[] captStyle;
    }

    PhotoWidgetUserData *photoWidgetUserData = new PhotoWidgetUserData(mIPhotoWidgetCb, mediaData);
    evas_object_smart_callback_add(caption, "changed", on_caption_text_changed, photoWidgetUserData);

    mediaData->CreateTagging();

    if (mediaData->GetCaption()) {
        std::string captionText = PresenterHelpers::AddFriendTags(mediaData->GetCaption(), mediaData->GetPhotoMessageTagsList(),
                PresenterHelpers::eComposer);
        elm_entry_entry_set(caption, captionText.c_str());
    }
}

void ComposerPhotoWidget::on_caption_text_changed(void *data, Evas_Object *obj, void *event_info) {
    PhotoWidgetUserData *widgetData = static_cast<PhotoWidgetUserData *>(data);

    if (widgetData && widgetData->mMediaData && widgetData->mPhotoWidgetEventsCb) {
        widgetData->mMediaData->UpdateCaption();
        widgetData->mPhotoWidgetEventsCb->CaptionTextChanged(widgetData->mMediaData);
    }
}

#ifdef FEATURE_ADD_TEXT
void ComposerPhotoWidget::on_add_text_button_clicked(void *data, Evas_Object *obj, void *event_info) {
    PostComposerMediaData *mediaData = static_cast<PostComposerMediaData *>(data);

    if (mediaData) {
        // If it's not enough memory then return
        if (mediaData->GetFileSize() * 2 > Utils::GetAvailableInternalMemorySize()) {
            notification_status_message_post(i18n_get_text("IDS_NOT_ENOUGH_SPACE"));
            return;
        }

        ScreenBase * addTextScreen = NULL;
        // If we have any text then add Text screen for view all created texts. Else add Text Input screen for create new one.
        if (eina_list_count(mediaData->GetTextList())) {
            if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_ADD_TEXT) {
                addTextScreen = new AddTextScreen(mediaData, eADD_TEXT_VIEW);
            }
        } else {
            if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_ADD_TEXT_INPUT) {
                addTextScreen = new AddTextInputScreen(mediaData);
            }
        }
        if (addTextScreen) {
            Application::GetInstance()->AddScreen(addTextScreen);
        }
    }
}
#endif

void ComposerPhotoWidget::on_crop_button_clicked(void *data, Evas_Object *obj, void *event_info) {
    Log::debug_tag(LogTag, "on_crop_button_clicked()");

    PhotoWidgetUserData *widgetData = static_cast<PhotoWidgetUserData *>(data);
    IPhotoWidgetEvents *photoWidgetEventsCb = static_cast<IPhotoWidgetEvents *>(widgetData->mPhotoWidgetEventsCb);
    photoWidgetEventsCb->CropImageButtonClick(widgetData->mMediaData);
}

bool ComposerPhotoWidget::CheckIsAvailableToDecode(PostComposerMediaData * mediaData) {
    Log::debug_tag(LogTag, "CheckIsAvailableToDecode %s", mediaData->GetOriginalPath());
    if (mediaData->GetDecodeStatus() == DECODE_STATUS_UNKNOWN) {
        unsigned char *img_buffer = NULL;
        int width = 0, height = 0;
        unsigned int size_decode = 0;
        DECODE_STATUS can_decode = UNABLE_TO_DECODE;

        //For decodeability check, we use decode_jpeg_with_downscale() which is much faster than image_util_decode_jpeg().
        int ret = image_util_decode_jpeg_with_downscale(mediaData->GetOriginalPath(), IMAGE_UTIL_COLORSPACE_RGB888, IMAGE_UTIL_DOWNSCALE_1_8,
                &img_buffer, &width, &height, &size_decode);
        Log::debug_tag(LogTag, "  decoded as JPG 1:8 -> ret=%d, %d*%d, size=%d KByte", ret, width, height, size_decode / 1024);
        if (ret == IMAGE_UTIL_ERROR_NONE) {
            int orientation = Utils::GetEXIFOrientation(mediaData->GetOriginalPath());
            if (orientation != (int) Utils::ORIENTATION_FLIP_HORIZONTAL && orientation != (int) Utils::ORIENTATION_FLIP_VERTICAL
                    && orientation != (int) Utils::ORIENTATION_TRANSPOSE && orientation != (int) Utils::ORIENTATION_TRANSVERSE
                    && width * height <= 14000000  //editing of huge images consumes too much memory
                            ) {
                can_decode = ABLE_DECODE_TO_JPEG;
            }
            free(img_buffer);
        } else if (Utils::HasEnding(mediaData->GetOriginalPath(), ".png")) {
            can_decode = ABLE_DECODE_TO_PNG;
        }
        mediaData->SetDecodeStatus(can_decode);
    }

    return ((mediaData->GetDecodeStatus() == ABLE_DECODE_TO_JPEG) ||
            (mediaData->GetDecodeStatus() == ABLE_DECODE_TO_PNG));
}

void ComposerPhotoWidget::on_delete_media_button_clicked(void *data, Evas_Object *obj, void *event_info) {
    Log::debug_tag(LogTag, "on_delete_media_button_clicked()");
    PhotoWidgetUserData *widgetData = static_cast<PhotoWidgetUserData *>(data);

    if (widgetData && widgetData->mPhotoWidgetEventsCb) {

        // mMediaBox - Contains array of Wrapper Layouts for each media
        //  \________________
        //  |Wrapper Layout |
        //  | \             |  - Wrapper for Media Layout
        //  |  Wrapper Box  |
        //  |___\___________|
        //       Media Layout  - post_composer_media_content group
        //        \
        //         obj - delete button

        Evas_Object * mediaWrapperLayout = elm_object_parent_widget_get(elm_object_parent_widget_get(elm_object_parent_widget_get(obj)));
        widgetData->mPhotoWidgetEventsCb->DeleteMediaButtonClick(widgetData->mMediaData, mediaWrapperLayout);
    }
}
void ComposerPhotoWidget::on_media_layout_deleted(void *data, Evas *e, Evas_Object *obj, void *event_info) {
    if (data) {
        delete static_cast<PhotoWidgetUserData *>(data);
    }
}

Evas_Event_Flags ComposerPhotoWidget::on_photo_clicked(void *data, void *event_info) {
    PostComposerMediaData *mediaData = static_cast<PostComposerMediaData *>(data);

    // in Carousel screen index starts with 0. So that why -1
    if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_IMAGE_CAROUSEL) {
        ImagesCarouselScreen *screen = new ImagesCarouselScreen(SelectedMediaList::Get(), mediaData->GetIndex() - 1,
                ImagesCarouselScreen::ePOST_COMPOSER_MODE);
        Application::GetInstance()->AddScreen(screen);
    }
    return EVAS_EVENT_FLAG_NONE;
}

Evas_Event_Flags ComposerPhotoWidget::on_video_clicked(void *data, void *event_info) {
    PostComposerMediaData *mediaData = static_cast<PostComposerMediaData *>(data);
    if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_VIDEO_PLAYER) {
        VideoPlayerScreen *screen = new VideoPlayerScreen(eVIDEO_PLAYER_POST_COMPOSER_MODE, mediaData);
        Application::GetInstance()->AddScreen(screen);
    }
    return EVAS_EVENT_FLAG_NONE;
}

PhotoWidgetUserData::PhotoWidgetUserData(
        IPhotoWidgetEvents *photoWidgetEventsCb,
        PostComposerMediaData *mediaData) {
    mPhotoWidgetEventsCb = photoWidgetEventsCb;
    mMediaData = mediaData;
}
