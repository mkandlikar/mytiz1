#include "Application.h"
#include "FacebookSession.h"
#include "PopupContextTip.h"

PopupContextTip::PopupContextTip(const char*text) : mContextTip(nullptr) {
    mText = SAFE_STRDUP(text);
}

PopupContextTip::~PopupContextTip() {
    elm_object_signal_callback_del(mContextTip, "mouse,clicked,*", "background", dismiss_cb);
    free((void *)mText);
}

void PopupContextTip::Open(Evas_Object *parent, const char*text) {
    Evas_Coord y, h;
    evas_object_geometry_get(parent, NULL, &y, NULL, &h);
    Open(parent, y + h, text);
}

void PopupContextTip::Open(Evas_Object *parent, Evas_Coord y, const char*text) {
    PopupContextTip *me = new PopupContextTip(text);
    me->Show(parent, y);
}

Evas_Object *PopupContextTip::CreateContent() {
    mContextTip = elm_layout_add(GetPopupLayout());
    elm_layout_file_set(mContextTip, Application::mEdjPath, "popup_context_tip");
    elm_object_part_text_set(mContextTip, "description", mText);
    elm_object_signal_callback_add(mContextTip, "mouse,clicked,*", "background", dismiss_cb, this);
    evas_object_show(mContextTip);
    return mContextTip;
}
