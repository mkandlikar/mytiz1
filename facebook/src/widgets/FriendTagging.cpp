#include "Application.h"
#include "FriendTagging.h"
#include "Log.h"
#include "LoggedInUserData.h"
#include "OwnFriendsProvider.h"
#include "PresenterHelpers.h"
#include "Utils.h"
#include "WidgetFactory.h"


/*
 * struct FriendTagging::PCData
 */
FriendTagging::PCData::PCData(const char* textToShow) {
    mFriend = nullptr;
    mScreen = nullptr;
    mTextToShow = SAFE_STRDUP(textToShow);
}

FriendTagging::PCData::~PCData() {
    if (mFriend) {
        mFriend->Release();
    }
    free (mTextToShow);
}

/*
 * class FriendTagging::TaggedFriendsParser
 */
const char *FriendTagging::TaggedFriendsParser::KBbeforeId = "<a rel=@[";
const char *FriendTagging::TaggedFriendsParser::KAfterId = " color=#5890ff>";
const char *FriendTagging::TaggedFriendsParser::KAfterName = "</a>";

const char *FriendTagging::TaggedFriendsParser::KBoldBeforeId = "<b><a href=u_";
const char *FriendTagging::TaggedFriendsParser::KBoldAfterId = ">";
const char *FriendTagging::TaggedFriendsParser::KBoldAfterName = "</a></b>";


FriendTagging::TaggedFriendsParser::TaggedFriendsParser() : mTaggedFriends(nullptr), mText(nullptr), mIsTextChanged(false), mIsPreeditActive(false), mBeforeId(nullptr), mAfterId(nullptr), mAfterName(nullptr) {
    SetMode(eComposer);
}

FriendTagging::TaggedFriendsParser::~TaggedFriendsParser() {
    free(mText);
    CleanList();
}

void FriendTagging::TaggedFriendsParser::SetText(const char*text, bool isPreeditActive) {
    Log::debug(LOG_FACEBOOK_TAGGING, "SetText:[%s]", text);
    mIsTextChanged = false;
    mIsPreeditActive = isPreeditActive;
    free(mText);
    mText = SAFE_STRDUP(text);
    CleanList();
}

void FriendTagging::TaggedFriendsParser::SetMode(Mode mode) {
    switch (mode) {
    case eComposer:
        mBeforeId = KBbeforeId;
        mAfterId = KAfterId;
        mAfterName = KAfterName;
        break;
    case eFeed:
        mBeforeId = KBoldBeforeId;
        mAfterId = KBoldAfterId;
        mAfterName = KBoldAfterName;
        break;
    default:
        break;
    }
}

char *FriendTagging::TaggedFriendsParser::GenVisibleName(Friend &fr) {
    int size = strlen(fr.mFirstName.c_str()) + strlen(fr.mLastName.c_str()) + 2;
    char *name = (char *) malloc(size);
    Utils::Snprintf_s(name, size, "%s %s", fr.mFirstName.c_str(), fr.mLastName.c_str());
    return name;
}

void FriendTagging::TaggedFriendsParser::Parse() {
    CleanList();
    if (*mText != '\0') {
        TaggedFriend *taggedFriend = ParseTaggedFriend(0);
        while (taggedFriend) {
            taggedFriend = ParseTaggedFriend(taggedFriend->mBeginOffset + taggedFriend->mLength);
        }
    }
}

const char *FriendTagging::TaggedFriendsParser::FindTagEnd(const char *begin) {
    const char *afterName = strstr(begin, mAfterName);
    int pairs = afterName ? 1 : 0; //we have at least 1 pair.
    const char *start = begin;
    while (pairs) {
        const char *tmpAfterName = strstr(start, mAfterName);
        const char *nextBegin = strstr(start, mBeforeId);

        if (nextBegin && tmpAfterName && (nextBegin < tmpAfterName)) {
            ++pairs;
            afterName = tmpAfterName;
            start = nextBegin + strlen(mBeforeId);
        } else if (tmpAfterName){
            --pairs;
            afterName = tmpAfterName;
            start = tmpAfterName + strlen(mAfterName);
        } else {
            afterName = nullptr;
            break;
        }
    }

    return afterName ? afterName : mText + strlen(mText);
}

const char *FriendTagging::TaggedFriendsParser::Addr(int offset) {
    return (offset == -1 ? nullptr : mText + offset);
}

int FriendTagging::TaggedFriendsParser::Offset(const char *address) {
    assert(!address || address >= mText);
    return (address ? address - mText : -1);
}

FriendTagging::TaggedFriend *FriendTagging::TaggedFriendsParser::ParseTaggedFriend(int beginOffset) {
    TaggedFriend *taggedFriend = nullptr;
    while (beginOffset != -1) {
        const char *tagEnd = strstr(Addr(beginOffset), mAfterName);
        Log::debug(LOG_FACEBOOK_TAGGING, "ParseTaggedFriend - begin:%s", Addr(beginOffset));
        beginOffset = Offset(strstr(Addr(beginOffset), mBeforeId));
        if (beginOffset == -1 || !tagEnd || beginOffset > Offset(tagEnd)) { //if there are no beginOffset or tagEnd at all or if there is tagEnd before beginOffset.
            break;
        }
        int idBeginOffset = beginOffset + strlen(mBeforeId);
        int afterIdOffset = Offset(strstr(Addr(idBeginOffset), mAfterId));
        if (afterIdOffset == -1) {
            break;
        }

        int idLen = afterIdOffset - idBeginOffset;
        //ToDo: check for idLen. If it's wrong then continue.
        char *id = (char *) malloc(idLen + 1);
        eina_strlcpy(id, Addr(idBeginOffset), idLen + 1);
        //check id
        Friend *fr = OwnFriendsProvider::GetInstance()->GetFriendById(id);
        bool isMe = false;
        if (!fr) {
            if (LoggedInUserData::GetInstance().mId && !strcmp(LoggedInUserData::GetInstance().mId, id)) {
                isMe = true;
                fr = FriendTagging::CreateLoggedUser();
            } else {
                free(id);
                beginOffset = afterIdOffset + strlen(mAfterId);
                continue;
            }
        }

        int nameBeginOffset = afterIdOffset + strlen(mAfterId);

        ParseTaggedFriend(nameBeginOffset); //There are may be nested tags inside the name tag.

        int afterNameOffset = Offset(FindTagEnd(Addr(nameBeginOffset))); //find after ParseTaggedFriend(), because it may be changed during parsing of nested friend.
        //tag is ok.
        int nameLen = afterNameOffset - nameBeginOffset;
        //ToDo: check for nameLen. If it's wrong then continue.
        char *name = (char *) malloc(nameLen + 1);
        eina_strlcpy(name, Addr(nameBeginOffset), nameLen + 1);

        taggedFriend = new TaggedFriend(fr);
        if (isMe) fr->Release();

        taggedFriend->mBeginOffset = beginOffset;
        taggedFriend->mLength = afterNameOffset + (afterNameOffset == strlen(mText) ? 0 : strlen(mAfterName)) - beginOffset;
        taggedFriend->mIdOffest = idBeginOffset - beginOffset;
        taggedFriend->mIdLen = idLen;
        taggedFriend->mNameOffset = nameBeginOffset - beginOffset;
        taggedFriend->mNameLen = nameLen;
        taggedFriend->mId = id;
        taggedFriend->mName = name;

        if (!mIsPreeditActive) {
            char *visibleName = TaggedFriendsParser::GenVisibleName(*fr);
            if (strcmp(taggedFriend->mName, visibleName)) {
                if (!strcmp(taggedFriend->mName + strlen(taggedFriend->mName) - strlen(visibleName), visibleName)) {
                    RemoveLeadingSymbolsFromTag(taggedFriend);
                } else {
                    RemoveTag(taggedFriend);
                    delete taggedFriend;
                    taggedFriend = nullptr;
                }
            }
            free(visibleName);
        }

        if (taggedFriend) {
            Log::debug(LOG_FACEBOOK_TAGGING, "id[%s], name:[%s]", taggedFriend->mId, taggedFriend->mName);
        } else {
            Log::debug(LOG_FACEBOOK_TAGGING, "Tag is removed");
        }
        break;
    }
    if (taggedFriend) {
        mTaggedFriends = eina_list_append(mTaggedFriends, taggedFriend);
    }
    Log::debug(LOG_FACEBOOK_TAGGING, "ParseTaggedFriend - end:[%s]", mText);
    return taggedFriend;
}

void FriendTagging::TaggedFriendsParser::RemoveTag(TaggedFriend *tag) {
    mIsTextChanged = true;
    char *oldText = mText;
    int size = strlen(oldText) - tag->mLength + tag->mNameLen + 1;
    mText = (char *) malloc(size);

    char *tagBegin = mText + tag->mBeginOffset;
    char *oldTagBegin = oldText + tag->mBeginOffset;
    eina_strlcpy(mText, oldText, tag->mBeginOffset + 1);
    eina_strlcpy(tagBegin, oldTagBegin + tag->mNameOffset, tag->mNameLen + 1);
    eina_strlcpy(tagBegin + tag->mNameLen, oldTagBegin + tag->mLength, size - tag->mBeginOffset - tag->mNameLen);

    free(oldText);
    RecalcTagOffsets(tag->mBeginOffset + tag->mNameLen, tag->mNameLen - tag->mLength);
}

void FriendTagging::TaggedFriendsParser::RemoveLeadingSymbolsFromTag(TaggedFriend *tag) {
    //Workaround which moves symbols outside a tag.
    //Transform
    //<a rel=@[123456789 color=#5890ff>abcUser Name</a>
    //to
    //abc<a rel=@[123456789 color=#5890ff>User Name</a>
    mIsTextChanged = true;
    char* tagBegin = mText + tag->mBeginOffset;
    char *name = TaggedFriendsParser::GenVisibleName(*(tag->mFriend));

    const char *actualNameBegin = tagBegin + tag->mNameOffset + tag->mNameLen - strlen(name);
    actualNameBegin = strstr(actualNameBegin, name);
    assert(actualNameBegin);

    int symbolsToMoveLen = actualNameBegin - (tagBegin + tag->mNameOffset);
    char *symbolsToMove = (char *) malloc(symbolsToMoveLen + 1);
    eina_strlcpy(symbolsToMove, tagBegin + tag->mNameOffset, symbolsToMoveLen + 1);

    memmove(tagBegin + symbolsToMoveLen, tagBegin, tag->mNameOffset);
    eina_strlcpy(tagBegin, symbolsToMove, sizeof(mText) - tag->mBeginOffset);

    tag->mBeginOffset += symbolsToMoveLen;
    tag->mNameLen -= symbolsToMoveLen;

    free(name);
    free(symbolsToMove);
}

void FriendTagging::TaggedFriendsParser::RecalcTagOffsets(int offset, int delta) {
    Eina_List * list;
    void *listData;
    EINA_LIST_FOREACH(mTaggedFriends, list, listData) {
        TaggedFriend *taggedFriend = static_cast<TaggedFriend*>(listData);
        if (taggedFriend->mBeginOffset > offset) {
            taggedFriend->mBeginOffset += delta;
        }
    }
}

void FriendTagging::TaggedFriendsParser::CleanList() {
    void *listData;
    EINA_LIST_FREE(mTaggedFriends, listData) {
        TaggedFriend *taggedFriend = static_cast<TaggedFriend *>(listData);
        delete taggedFriend;
    }
}


/*
 * class FriendTagging
 */
FriendTagging::FriendTagging(Evas_Object *parent, Evas_Object *entry) {
    Log::debug(LOG_FACEBOOK_TAGGING, "FriendTagging init:%p", this);
    mParentScreenLayout = parent;
    mMessageEntry = entry;
    if (mMessageEntry) {
        const char *text = elm_entry_entry_get(mMessageEntry);
        mParser.SetText(text, false);
        mParser.Parse();
        elm_entry_cnp_mode_set(mMessageEntry, ELM_CNP_MODE_PLAINTEXT);
    }
    mEntryEmulator = elm_entry_add(mParentScreenLayout);
    evas_object_hide(mEntryEmulator);
    mBufferString = "";
    mSearchType = SEARCH_TYPE_NONE;
    mUpsideDownList = false;
    mSearchAllowed = false;
    AddEntryCallbacks();
}

FriendTagging::~FriendTagging() {
    evas_object_del(mEntryEmulator);
    DeleteEntryCallbacks();
}

void FriendTagging::text_changed_cb(void *data, Evas_Object *obj, void *event_info) {
    FriendTagging *me = static_cast<FriendTagging *> (data);
    me->TextChanged();
}

void FriendTagging::AddEntryCallbacks() {
    if (mMessageEntry) {
        evas_object_smart_callback_add(mMessageEntry, "changed,user", text_changed_cb, this);
        evas_object_smart_callback_add(mMessageEntry, "preedit,changed", text_changed_cb, this);
    }
}

void FriendTagging::DeleteEntryCallbacks() {
    if (mMessageEntry) {
        evas_object_smart_callback_del(mMessageEntry, "changed,user", text_changed_cb);
        evas_object_smart_callback_del(mMessageEntry, "preedit,changed", text_changed_cb);
    }
}

bool FriendTagging::IsPreeditActive() {
    int cursorPosition = elm_entry_cursor_pos_get(mMessageEntry);
    Evas_Object *tb = elm_entry_textblock_get(mMessageEntry);
    Evas_Textblock_Cursor *cur1 = evas_object_textblock_cursor_new(tb);
    evas_textblock_cursor_pos_set(cur1, cursorPosition - 1);
    Evas_Textblock_Cursor *cur2 = evas_object_textblock_cursor_new(tb);
    evas_textblock_cursor_pos_set(cur2, cursorPosition);
    char *lastSymbol = evas_textblock_cursor_range_text_get(cur1, cur2, EVAS_TEXTBLOCK_TEXT_MARKUP);
    evas_textblock_cursor_free(cur1);
    evas_textblock_cursor_free(cur2);
    char *preedit = strstr(lastSymbol, "<preedit>");
    free(lastSymbol);
    return preedit;
}

char *FriendTagging::PreeditText() {
    int cursorPosition = elm_entry_cursor_pos_get(mMessageEntry);
    Evas_Object *tb = elm_entry_textblock_get(mMessageEntry);
    Evas_Textblock_Cursor *cur1 = evas_object_textblock_cursor_new(tb);
    evas_textblock_cursor_pos_set(cur1, cursorPosition - 1);
    Evas_Textblock_Cursor *cur2 = evas_object_textblock_cursor_new(tb);
    evas_textblock_cursor_pos_set(cur2, cursorPosition);
    char *lastSymbol = evas_textblock_cursor_range_text_get(cur1, cur2, EVAS_TEXTBLOCK_TEXT_MARKUP);
    evas_textblock_cursor_free(cur1);
    evas_textblock_cursor_free(cur2);
    char *preedit = strstr(lastSymbol, "<preedit>");
    preedit = preedit ? SAFE_STRDUP(preedit + strlen("<preedit>")) : nullptr;
    free(lastSymbol);
    return preedit;
}

char *FriendTagging::GetTextBeforeCursor(bool IsPreeditRequired) {
    int cursorPosition = elm_entry_cursor_pos_get(mMessageEntry);
    return GetTextByRange(0, cursorPosition, IsPreeditRequired);
}

char *FriendTagging::GetTextByRange(int begin, int end, bool IsPreeditRequired) {
    Evas_Object *tb = elm_entry_textblock_get(mMessageEntry);
    Evas_Textblock_Cursor *cur1 = evas_object_textblock_cursor_new(tb);
    evas_textblock_cursor_pos_set(cur1, begin);
    Evas_Textblock_Cursor *cur2 = evas_object_textblock_cursor_new(tb);
    evas_textblock_cursor_pos_set(cur2, end);

    char *res = evas_textblock_cursor_range_text_get(cur1, cur2, EVAS_TEXTBLOCK_TEXT_MARKUP);
    evas_textblock_cursor_free(cur1);
    evas_textblock_cursor_free(cur2);
    if (IsPreeditActive()) {
        char *endRes = strstr(res, "<preedit>");
        if (endRes) {
            if (IsPreeditRequired) {
                char *oldRes = res;
                int size = strlen(res) - strlen("<preedit>") + 1;
                res = (char *) malloc(size);
                eina_strlcpy(res, oldRes, (endRes - oldRes) + 1);
                eina_strlcat(res, endRes + strlen("<preedit>"), size);
                free(oldRes);
            } else {
                *endRes ='\0';
            }
        }
    }
    return res;
}

void FriendTagging::TagFriend(Friend *fr) {
    char *userName = TaggedFriendsParser::GenVisibleName(*fr);
    const char *textToReplace = nullptr;
    const char *oldText = elm_entry_entry_get(mMessageEntry);
    std::string newText = oldText;

    int cursorPosition = elm_entry_cursor_pos_get(mMessageEntry);
    char *textBeforeCursor = GetTextBeforeCursor(true);
    assert(mSearchType == SEARCH_TYPE_POSTPONED || mSearchType == SEARCH_TYPE_INSTANT);
    if (mSearchType == SEARCH_TYPE_INSTANT) {
        textToReplace = strrchr(textBeforeCursor, '@');
    } else { //mSearchType == SEARCH_TYPE_POSTPONED
        textToReplace = GetTextAfterDelim(textBeforeCursor);
    }
    assert(textToReplace);
    int tagOffset = textToReplace - textBeforeCursor;

    std::stringstream tag;
    tag << "<a rel=@[" << fr->GetId() << " color=#5890ff>" << userName << "</a>";
    newText.replace(tagOffset, strlen(textToReplace), tag.str().c_str());
    free(userName);

    elm_entry_entry_set(mMessageEntry, newText.c_str());

    int newCursorPosition = cursorPosition - GetPositionsNumber(textToReplace) + GetPositionsNumber(tag.str().c_str());
    elm_entry_cursor_pos_set(mMessageEntry, newCursorPosition);
    free(textBeforeCursor);
    CheckExistentTags();
}

void FriendTagging::TextChanged() {
    CheckExistentTags();
    LookForKeyword();
//the line below is for experiments. Please don't remove.
//    elm_entry_entry_set(mMessageEntry, "111<a rel=@[100012881534074 color=#589000>cty<a rel=@[100012881534074 color=#5890ff>Kuricova</a>222");
}

void FriendTagging::CheckExistentTags() {
    const char *text = elm_entry_entry_get(mMessageEntry);
    mParser.SetText(text, IsPreeditActive());
    mParser.Parse();

    if (mParser.IsTextChanged()) {
        int curPos = elm_entry_cursor_pos_get(mMessageEntry);
        elm_entry_entry_set(mMessageEntry, mParser.GetText());
        elm_entry_cursor_pos_set(mMessageEntry, curPos);
    }
}

void FriendTagging::LookForKeyword() {
    UpdateBufferString();
    mSearchAllowed = mBufferString.length();
    if (mSearchAllowed) {
        SearchUtility();
    } else {
        HideFriendsPopup();
    }
}

const char *FriendTagging::GetTextAfterDelim(const char *text) {
    const char *spacePos = strrchr(text, ' ');
    const char *textAfterSpace = spacePos ? spacePos + 1 : nullptr;
    //ToDo: AAA implement without std::string
    std::string strText = text;
    int brBegin = strText.rfind("<br/>");
    const char *textAfterBr = brBegin == std::string::npos ? nullptr : text + brBegin + 5; //5 is length of "<br/>".
    return (!textAfterBr && !textAfterSpace ? text : MAX(textAfterBr, textAfterSpace));
}

void FriendTagging::UpdateBufferString() {
    int cursorPosition = elm_entry_cursor_pos_get(mMessageEntry);
    char *textBeforeCursor = GetTextBeforeCursor(true);
    const char *atPosition = strrchr(textBeforeCursor, '@');
    const char *textAfterDelim = GetTextAfterDelim(textBeforeCursor);
    assert(textAfterDelim);

    mBufferString = "";

    if (atPosition > textAfterDelim - 1) {//search caused by @
        mBufferString = atPosition + 1;
        mSearchType = SEARCH_TYPE_INSTANT;
    } else { //looking at last 4 symbols.
        if (cursorPosition >= 4) {
            char *last4Symbols = GetTextByRange(cursorPosition - 4, cursorPosition, true);
            assert(last4Symbols);
            if (strlen(textAfterDelim) >= strlen(last4Symbols)) {
                mBufferString = textAfterDelim;
                mSearchType = SEARCH_TYPE_POSTPONED;
            }
            free(last4Symbols);
        }
    }

    free(textBeforeCursor);
}

Friend *FriendTagging::CreateLoggedUser() {
    Friend *fr = new Friend();
    fr->SetId(strdup(LoggedInUserData::GetInstance().mId));
    fr->mPicturePath = LoggedInUserData::GetInstance().mPicture->mUrl ? WidgetFactory::WrapByFormat("%s", LoggedInUserData::GetInstance().mPicture->mUrl) : "";
    fr->mName = LoggedInUserData::GetInstance().mName ? WidgetFactory::WrapByFormat("%s", LoggedInUserData::GetInstance().mName) : "";
    fr->mFirstName = (LoggedInUserData::GetInstance().mFirstName) ? (LoggedInUserData::GetInstance().mFirstName) : "";
    fr->mLastName = (LoggedInUserData::GetInstance().mLastName) ? (LoggedInUserData::GetInstance().mLastName) : "";
    return fr;
}

void FriendTagging::DoSearch()
{
    std::vector<Friend *> friendsContainer;

    if (!mBufferString.empty()) {
        Eina_Array *friendsArray = OwnFriendsProvider::GetInstance()->GetData();
        int friendsTotalCount = eina_array_count(friendsArray);
        bool comparisonFound = false;
        for (int k = 0; k < friendsTotalCount; k++) {
            Friend *ownFriend = static_cast<Friend*>(eina_array_data_get(friendsArray,k));
            comparisonFound = IsFriendFound(ownFriend->mFirstName.c_str(), ownFriend->mLastName.c_str(), mBufferString.c_str());
            if (comparisonFound) {
                friendsContainer.push_back(ownFriend);
                ownFriend->AddRef();
            }
        }
        if (LoggedInUserData::GetInstance().mFirstName && LoggedInUserData::GetInstance().mLastName) {
            comparisonFound = IsFriendFound(LoggedInUserData::GetInstance().mFirstName, LoggedInUserData::GetInstance().mLastName, mBufferString.c_str());
            if (comparisonFound) {
                Friend *fr = CreateLoggedUser();
                friendsContainer.push_back(fr);
            }
        }
        HideFriendsPopup(); // close previously opened popup if any.
        if (!friendsContainer.empty()) {
            ShowPopup(friendsContainer);
        }
    }
}

const unsigned int FriendTagging::TaggingPopup::mMaxItemsNum = 4;
bool FriendTagging::TaggingPopup::mIsShown = false;


FriendTagging::TaggingPopup::TaggingPopup(FriendTagging *tagging, std::vector<Friend*> const &friends, bool upsideDown) : mTagging(tagging), mFriends(friends), mUpsideDown(upsideDown), mHeight(0) {
}

void FriendTagging::TaggingPopup::Open(Evas_Object *parent, FriendTagging *tagging, std::vector<Friend*> const &friends, bool upsideDown) {
    TaggingPopup *me = new TaggingPopup(tagging, friends, upsideDown);
    me->Show(parent, 0);
    me->mIsShown = true;
}

FriendTagging::TaggingPopup::~TaggingPopup() {
    mIsShown = false;
}

Evas_Object *FriendTagging::TaggingPopup::WrapWithSize(Evas_Object *parent, Evas_Object *obj, Evas_Coord w, Evas_Coord h) {
    Evas_Object *rect;

    Evas_Object *layout = elm_layout_add(parent);
    elm_layout_file_set(layout, Application::mEdjPath, "fixed_size_layout");

    rect = evas_object_rectangle_add(evas_object_evas_get(layout));
    evas_object_size_hint_min_set(rect, w, h);
    evas_object_size_hint_max_set(rect, w, h);
    evas_object_size_hint_weight_set(rect, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(rect, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_layout_content_set(layout, "swallow.spacer", rect);
    elm_layout_content_set(layout, "swallow.scroller", obj);
    evas_object_show(rect);

    return layout;
}

Evas_Object *FriendTagging::TaggingPopup::CreateContent() {
    Evas_Object *layout = nullptr;
    Evas_Object *scroller = nullptr;
    Evas_Object *box = nullptr;
    int size = mFriends.size();

    scroller = elm_scroller_add(GetPopupLayout());
    evas_object_size_hint_weight_set(scroller, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(scroller, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_scroller_propagate_events_set(scroller, EINA_FALSE);
    elm_scroller_single_direction_set(scroller, ELM_SCROLLER_SINGLE_DIRECTION_HARD);
    elm_scroller_policy_set(scroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_OFF);

    box = elm_box_add(scroller);
    elm_object_content_set(scroller, box);
    evas_object_size_hint_weight_set(box, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(box, EVAS_HINT_FILL, EVAS_HINT_FILL);

    int w;
    for (int count = 0; count < size; ++count) {
        Friend *fr = mFriends.at(count);
        assert(fr);
        char *userName = FriendTagging::TaggedFriendsParser::GenVisibleName(*fr);
        PCData *pcData = new PCData(userName);
        free (userName);
        pcData->mScreen = mTagging;
        pcData->SetFriend(fr);

        Evas_Object *popupItem = CreatePopupItem(pcData, box);

        int h;
        edje_object_part_geometry_get(elm_layout_edje_get(popupItem), "item_spacer", nullptr, nullptr, &w, &h);
        if (count < mMaxItemsNum) {
            mHeight += h;
        }

        if (mUpsideDown) {
            elm_box_pack_start(box, popupItem);
        } else {
            elm_box_pack_end(box, popupItem);
        }
    }

    layout = WrapWithSize(GetPopupLayout(), scroller, w, mHeight);

    return layout;
}

Evas_Object *FriendTagging::TaggingPopup::CreatePopupItem(PCData *data, Evas_Object *parent) {
    Friend *user = data->GetFriend();

    Evas_Object *popup_item = elm_layout_add(parent);
    evas_object_size_hint_weight_set(popup_item, 1, 1);
    elm_layout_file_set(popup_item, Application::mEdjPath, mUpsideDown ? "comments_tag_friend_popup_item" : "tag_friend_popup_item");

    if (!user->mPicturePath.empty()) {
        Evas_Object *avatar = elm_image_add(popup_item);
        elm_layout_content_set(popup_item, "item_icon", avatar);
        elm_image_file_set(avatar, user->mPicturePath.c_str(), nullptr);
        evas_object_show(avatar);

    }

    elm_object_part_text_set(popup_item, "item_name", data->mTextToShow);

    evas_object_event_callback_add(popup_item, EVAS_CALLBACK_DEL, on_popup_item_delete_cb, data);
    elm_object_signal_callback_add(popup_item, "mouse,clicked,*", "item_*", on_popup_item_cb, data);

    evas_object_show(popup_item);
    return popup_item;
}

void FriendTagging::TaggingPopup::on_popup_item_delete_cb(void *data, Evas *e, Evas_Object *obj, void *event_info) {
    delete static_cast<PCData*>(data);
}

void FriendTagging::TaggingPopup::on_popup_item_cb(void *data, Evas_Object *obj, const char *emission, const char *source) {
    PCData *pc_data = static_cast<PCData*>(data);
    FriendTagging *screen = static_cast<FriendTagging*>(pc_data->mScreen);
    screen->OnPopupItemCb(pc_data->GetFriend());
}

void FriendTagging::ShowPopup(std::vector<Friend*> const &friendsVector) {
    int kx, ky, kw, kh;
    Ecore_IMF_Context *ctx = (Ecore_IMF_Context *) elm_entry_imf_context_get(mMessageEntry);
    ecore_imf_context_input_panel_geometry_get(ctx, &kx, &ky, &kw, &kh);

    Evas_Coord x, y, w, h;
    evas_object_geometry_get(mMessageEntry, &x, &y, &w, &h);

    Evas_Coord sx=0, sy=0, sw=0, sh=0;
    elm_scroller_region_get(mMessageEntry, &sx, &sy, &sw, &sh);

    Evas_Coord cx, cy, cw, ch;
    elm_entry_cursor_geometry_get(mMessageEntry, &cx, &cy, &cw, &ch);
    cy = cy > sy ? cy - sy : cy; // If the entry is scrolled then recalc cy relative to the scroller.
    TaggingPopup::Open(mParentScreenLayout, this, friendsVector, mUpsideDownList);
    Evas_Coord popupHeight = TaggingPopup::GetHeight();
    if (y + cy + ch + popupHeight > UIRes::GetInstance()->SCREEN_SIZE_HEIGHT - kh) { //show popup above cursor
        TaggingPopup::Move(y+cy-popupHeight);
    } else { //show popup under cursor
        TaggingPopup::Move(y+cy+ch);
    }
    Log::debug(LOG_FACEBOOK_TAGGING, "POPUP SHOWN!");
}

bool FriendTagging::HideFriendsPopup() {
    bool ret = TaggingPopup::IsShown();
    if (ret) {
        TaggingPopup::Close();
        Log::debug(LOG_FACEBOOK_TAGGING, "POPUP HIDDEN!");
    }
    return ret;
}

void FriendTagging::OnPopupItemCb(Friend *item) {
    TagFriend(item);
    HideFriendsPopup();
    mSearchType = SEARCH_TYPE_NONE;
    mSearchAllowed = false;
}

char *FriendTagging::GetStringToPost(bool originMsg) {
    Log::debug(LOG_FACEBOOK_TAGGING, "GetStringToPost()");
    char *textToPost = nullptr;
    const char *entryText = elm_entry_entry_get(mMessageEntry);
    assert(entryText);

    char *tmpUTF8 = elm_entry_markup_to_utf8(entryText);
    if (originMsg) {
        textToPost = tmpUTF8;
    } else {
        std::string tmp = PresenterHelpers::AddFriendTags(tmpUTF8, GetMessageTagsList(), PresenterHelpers::eServer).c_str();
        free((void *)tmpUTF8);
        //For server string must be in UTF8 format
        char *tmpTaggedUTF8 = elm_entry_markup_to_utf8(tmp.c_str());
        textToPost = tmpTaggedUTF8;
    }

    Log::debug(LOG_FACEBOOK_TAGGING, "Prototype for tagging: originMsg = %d; GetStringToPost = %s", originMsg, textToPost);
    return textToPost;
}

std::list<Tag> FriendTagging::GetMessageTagsList(const char* text) {
    mParser.SetText(text, false);
    mParser.Parse();
    return GetMessageTagsList();
}

std::list<Tag> FriendTagging::GetMessageTagsList() {
    std::list<Tag> messageTags;
    Eina_List * list;
    void *listData;
    const char *text = mParser.GetText();
    std::string markupText = text ? text : "";
    EINA_LIST_FOREACH(mParser.mTaggedFriends, list, listData) {
        TaggedFriend *taggedFriend = static_cast<TaggedFriend *>(listData);
        //To get tagged friend position in UTF8
        int offset = GetPositionsNumber(markupText.substr(0, taggedFriend->mBeginOffset).c_str());
        int length = GetPositionsNumber(taggedFriend->mName);
        Tag *tag = new Tag(*(taggedFriend->mFriend), offset, length);
        messageTags.push_back(*tag);
    }
    return std::move(messageTags);
}

unsigned int FriendTagging::GetPositionsNumber(const char *text) {
    elm_entry_entry_set(mEntryEmulator, text);
    elm_entry_cursor_end_set(mEntryEmulator);
    int pos = elm_entry_cursor_pos_get(mEntryEmulator);
    return pos;
}

char *FriendTagging::GetTextByRange(const char* text, int begin, int end) {
    elm_entry_entry_set(mEntryEmulator, text);
    Evas_Object *tb = elm_entry_textblock_get(mEntryEmulator);
    Evas_Textblock_Cursor *cur1 = evas_object_textblock_cursor_new(tb);
    evas_textblock_cursor_pos_set(cur1, begin);
    Evas_Textblock_Cursor *cur2 = evas_object_textblock_cursor_new(tb);
    evas_textblock_cursor_pos_set(cur2, end);

    char *res = evas_textblock_cursor_range_text_get(cur1, cur2, EVAS_TEXTBLOCK_TEXT_MARKUP);
    evas_textblock_cursor_free(cur1);
    evas_textblock_cursor_free(cur2);

    return res;
}

int FriendTagging::GetBytePos(const char *text, int start, int end) {
    Log::debug(LOG_FACEBOOK_TAGGING, "GetBytePos()");
    char *markupText = elm_entry_utf8_to_markup(text);
    char *offsetName = GetTextByRange(markupText, start, end);
    char *utf8OffsetName = elm_entry_markup_to_utf8(offsetName);
    free(offsetName);
    free(markupText);
    int ret = strlen(utf8OffsetName);
    free(utf8OffsetName);
    return ret;
}

void FriendTagging::SearchUtility() {
    if (mBufferString.length() > 3 && mSearchType == SEARCH_TYPE_POSTPONED ) {
        DoSearch();
        Log::debug("Prototype for Tagging: SEARCH_TYPE_POSTPONED mSearchAllowed for string = %s", mBufferString.c_str());
    } else if (mSearchType == SEARCH_TYPE_INSTANT && !mBufferString.empty()) {
        DoSearch();
        Log::debug("Prototype for Tagging: SEARCH_TYPE_INSTANT mSearchAllowed for string = %s", mBufferString.c_str());
    } else {
        HideFriendsPopup();
    }
}

bool FriendTagging::IsFriendFound(const char *firstName, const char *lastName, const char *tocompare) {
    int len = Utils::UStrLen(tocompare);
    return !Utils::UCompareN(tocompare, firstName, len, false) ||
           !Utils::UCompareN(tocompare, lastName, len, false);
}

bool FriendTagging::IsLongText(int maxLength) {
    bool ret = false;
    char *entryText = GetStringToPost(true);
    int messageLength = Utils::UStrLen(entryText);
    free(entryText);
    if (messageLength > maxLength) {
        char *message = WidgetFactory::WrapByFormat2(i18n_get_text("IDS_LONG_POST_DESC"), maxLength, messageLength);
        if (message) {
            WidgetFactory::ShowAlertPopup(message);
            delete[] message;
        }
        ret = true;
    }
    return ret;
}
