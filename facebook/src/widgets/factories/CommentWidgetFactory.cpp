#include "Config.h"
#include <Evas.h>
#include <sstream>

#include "CommentPresenter.h"
#include "CommentsProvider.h"
#include "CommentWidgetFactory.h"
#include "Common.h"
#include "Config.h"
#include "CSmartPtr.h"
#include "EditCommentScreen.h"
#include "ErrorHandling.h"
#include "FacebookSession.h"
#include "LikeOperation.h"
#include "Log.h"
#include "LoggedInUserData.h"
#include "OperationManager.h"
#include "OperationPresenter.h"
#include "Popup.h"
#include "PostPresenter.h" //TODO: remove, when Comment and OperationPresenter will inherit common interface
#include "PostScreen.h"
#include "ScreenBase.h"
#include "SimpleCommentOperation.h"
#include "WhoLikedScreen.h"
#include "WidgetFactory.h"

UIRes * CommentWidgetFactory::R = UIRes::GetInstance();

#define CONTEXT_MENU_CMNT_OWNER_ITEMS 4
#define CONTEXT_MENU_CMNT_OTHER_ITEMS 3
#define CONTEXT_MENU_THIRD_PARTY_ITEMS 2

ConfirmationDialog * CommentWidgetFactory::mConfirmationDialog = nullptr;
Ecore_Timer *CommentWidgetFactory::mLongPressedTimer = nullptr;

static int largeCommentHeight = 500;

CObjAndData::CObjAndData()
{
    mCData = NULL;
    mCLikeLabel = NULL;
    mCLikeIcon = NULL;
    mCLikeCount = NULL;
    mCLikeLabelDot = NULL;
    mCReplyLabelDot = NULL;
    mCActionBar = NULL;
}

CObjAndData::~CObjAndData()
{
    mCData = NULL;
    mCLikeLabel = NULL;
    mCLikeIcon = NULL;
    mCLikeCount = NULL;
    mCLikeLabelDot = NULL;
    mCReplyLabelDot = NULL;
    mCActionBar = NULL;
}

Evas_Object *CommentWidgetFactory::CreateBaseLayout(Evas_Object *parent)
{
    Evas_Object *layout = WidgetFactory::CreateLayoutByGroup(parent, "comment_base_layout");

    return layout;
}

Evas_Object *CommentWidgetFactory::CreateTopLikeLayout(Evas_Object *parent)
{
    Evas_Object *layout = WidgetFactory::CreateLayoutByGroup(parent, "comment_top_part_like");

    elm_object_part_content_set(parent, "top_part", layout);

    Evas_Object *title = elm_label_add(layout);
    elm_layout_content_set(layout, "push_text", title);
    elm_label_wrap_width_set(title, R->COMMENT_TOP_TOOLBAR_TEXTBOX_W);
    elm_label_line_wrap_set(title, ELM_WRAP_WORD);
    evas_object_show(title);

    return layout;
}

void CommentWidgetFactory::RefreshTopLikeLayout(ActionAndData *actionData, Evas_Object *layout)
{
    int likesCount = 0;
    bool myLike = false;
    const char *likeFromName = nullptr;
    const char *likeFromId = nullptr;

    if (actionData && actionData->mData) {
        switch (actionData->mData->GetGraphObjectType()) {
        case GraphObject::GOT_PHOTO: {
            Photo *photo = static_cast<Photo*>(actionData->mData);
            likesCount = photo->GetLikesCount();
            myLike = photo->GetHasLiked();
            likeFromId = photo->GetLikeFromId();
            likeFromName = photo->GetLikeFromName();
            break;
        }
        case GraphObject::GOT_POST: {
            Post *post = static_cast<Post *>(actionData->mData);
            likesCount = post->GetLikesCount();
            myLike = post->GetHasLiked();
            likeFromId = post->GetLikeFromId();
            likeFromName = post->GetLikeFromName();
            break;
        }
        default:
            break;
        }
    }

    if (likesCount > 0) {
        elm_object_signal_callback_add(layout, "mouse,clicked,*", "push_*", on_who_liked_clicked_cb, actionData);
        elm_object_signal_emit(layout, "activate", "who_liked_btn");

    } else {
        elm_object_signal_callback_del(layout, "mouse,clicked,*", "push_*", on_who_liked_clicked_cb);
        elm_object_signal_emit(layout, "deactivate", "who_liked_btn");
    }

    Log::info(LOG_FACEBOOK_COMMENTING, "CommentWidgetFactory::RefreshTopLikeLayout() - total count=%d, %s my like", likesCount, (myLike ? "including" : "without"));

    Evas_Object *title = elm_object_part_content_get(layout, "push_text");

    int otherLikesCount = 0;
    if (myLike) {
        otherLikesCount = likesCount - 1;
        elm_object_signal_emit(layout, "like", "like_btn");
    } else {
        otherLikesCount = likesCount;
        elm_object_signal_emit(layout, "unlike", "like_btn");
    }

    std::stringstream presenter;
    char *likeThis = WidgetFactory::WrapByFormat2(SANS_MEDIUM_000_FORMAT_BOLD, R->COMMENT_TOP_TITLE, "IDS_LIKE_THIS");
    char *youAnd = WidgetFactory::WrapByFormat2(SANS_MEDIUM_000_FORMAT_BOLD, R->COMMENT_TOP_TITLE, "IDS_YOU_AND");

    if (otherLikesCount == 0) {
        elm_object_signal_emit(layout, "hide", "push_area");
        elm_object_signal_emit(layout, "hide", "push_chevron");
        if (myLike) {
            char *youLikeThis = WidgetFactory::WrapByFormat2(SANS_MEDIUM_000_FORMAT_BOLD, R->COMMENT_TOP_TITLE, "IDS_YOU_LIKE_THIS");
            if (youLikeThis) {
                presenter << youLikeThis;
                delete[] youLikeThis;
            }
        } else {
            char *firstLikeThis = WidgetFactory::WrapByFormat2(SANS_MEDIUM_000_FORMAT, R->COMMENT_TOP_TITLE, "IDS_BE_THE_FIRST_TO_LIKE_THIS");
            if (firstLikeThis) {
                presenter << firstLikeThis;
                delete[] firstLikeThis;
            }
        }
    } else if (otherLikesCount == 1) {
        elm_object_signal_emit(layout, "hide", "push_area");
        elm_object_signal_emit(layout, "hide", "push_chevron");
        char *tmpLikeFromName = WidgetFactory::WrapByFormat2(SANS_MEDIUM_000_FORMAT_BOLD, R->COMMENT_TOP_TITLE, likeFromName);
        if (tmpLikeFromName) {
            if (myLike) {
                if (youAnd && likeThis) {
                    presenter << youAnd << " " << tmpLikeFromName << " " << likeThis;
                }
            } else {
                char *likes_this = WidgetFactory::WrapByFormat2(SANS_MEDIUM_000_FORMAT_BOLD, R->COMMENT_TOP_TITLE, "IDS_LIKES_THIS");
                if (likes_this) {
                    presenter << tmpLikeFromName << " " << likes_this;
                    delete[] likes_this;
                }
            }
            delete[] tmpLikeFromName;
        }
    } else if (otherLikesCount > 1) {
        elm_object_signal_emit(layout, "show", "push_area");
        elm_object_signal_emit(layout, (Application::IsRTLLanguage() ? "show,rtl" : "show,ltr"), "push_chevron");

        if (myLike) {
            std::string countString = Utils::FormatBigInteger(otherLikesCount);
            char *textCount = WidgetFactory::WrapByFormat2("%s %s", countString.c_str(), "IDS_NUM_OTHERS");
            if (textCount) {
                char *text = WidgetFactory::WrapByFormat2(SANS_MEDIUM_000_FORMAT_BOLD, R->COMMENT_TOP_TITLE, textCount);
                delete[] textCount;
                if (text && youAnd) {
                    presenter << youAnd << " " << text << " " << likeThis;
                }
                delete[] text;
            }
        } else {
            char *tmpLikeFromName = WidgetFactory::WrapByFormat2(SANS_MEDIUM_000_FORMAT_BOLD, R->COMMENT_TOP_TITLE, likeFromName);
            if (tmpLikeFromName) {
                std::string countString = Utils::FormatBigInteger(otherLikesCount - 1);
                char *textCount = WidgetFactory::WrapByFormat2("%s %s", countString.c_str(), (otherLikesCount == 2 ? "IDS_WHO_LIKE_THIS_NUM_OTHER" : "IDS_NUM_OTHERS"));
                if (textCount) {
                    char *text = WidgetFactory::WrapByFormat2(SANS_MEDIUM_000_FORMAT_BOLD, R->COMMENT_TOP_TITLE, textCount);
                    delete[] textCount;
                    if (text) {
                        char *And = WidgetFactory::WrapByFormat2(SANS_MEDIUM_000_FORMAT_BOLD, R->COMMENT_TOP_TITLE, "IDS_WHO_LIKE_THIS_AND");
                        presenter << tmpLikeFromName << " " << And << " " << text << " " << likeThis;
                        delete[] And;
                        delete[] text;
                    }
                }
                delete[] tmpLikeFromName;
            } else {
                std::string countString = Utils::FormatBigInteger(otherLikesCount);
                char *textCount = WidgetFactory::WrapByFormat2("%s %s", countString.c_str(), "IDS_PEOPLE");
                if (textCount) {
                    char *text = WidgetFactory::WrapByFormat2(SANS_MEDIUM_000_FORMAT_BOLD, R->COMMENT_TOP_TITLE, textCount);
                    delete[] textCount;
                    if (text) {
                        presenter << text << " " << likeThis;
                        delete[] text;
                    }
                }
            }
        }
    }
    delete[] youAnd;
    delete[] likeThis;
    HRSTC_TRNSLTD_TEXT_SET(title, presenter.str().c_str());
}

void CommentWidgetFactory::on_who_liked_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source) {
    ActionAndData *actionData = static_cast<ActionAndData*>(data);
    assert(actionData);
    if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_WHO_LIKED) {
        WhoLikedScreen* screen = new WhoLikedScreen(actionData->mData->GetId());
        Application::GetInstance()->AddScreen(screen);
    }
}

Evas_Object *CommentWidgetFactory::CreateBotLayout(Evas_Object *parent)
{
    Evas_Object *layout = WidgetFactory::CreateLayoutByGroup(parent, "comment_bot_part");

    elm_object_part_content_set(parent, "bot_part", layout);

    return layout;
}

Evas_Object *CommentWidgetFactory::CreateTopBackLayout(Evas_Object *parent)
{
    Evas_Object *layout = WidgetFactory::CreateLayoutByGroup(parent, "comment_top_part_back");

    elm_object_part_content_set(parent, "top_part", layout);

    elm_object_translatable_part_text_set(layout, "back_text", "IDS_REPLIES");

    return layout;
}

Evas_Object *CommentWidgetFactory::CreateEntry(Evas_Object *parent, int screenId)
{
    Evas_Object *commentEntry = elm_entry_add(parent);
    elm_entry_prediction_allow_set(commentEntry, EINA_FALSE);
    elm_object_part_content_set(parent, "entry", commentEntry);
    elm_entry_scrollable_set(commentEntry, EINA_TRUE);
    char *buf = NULL;
    buf = WidgetFactory::WrapByFormat2( SANS_REGULAR_9298A4_FORMAT, R->COMMENT_ENTRY_TEXT_SIZE, screenId == ScreenBase::SID_COMMENTS ? "IDS_WRITE_COMMENT" : "IDS_WRITE_REPLY");
    if (buf) {
        HRSTC_TRNSLTD_PART_TEXT_SET(commentEntry, "elm.guide", buf);
        delete[] buf;
    }
    elm_entry_text_style_user_push(commentEntry, COMMENT_TEXT_STYLE);
    elm_entry_cnp_mode_set(commentEntry, ELM_CNP_MODE_PLAINTEXT);

    evas_object_show(commentEntry);
    elm_object_focus_set(commentEntry, EINA_TRUE);
    elm_entry_input_panel_show(commentEntry);
    return commentEntry;
}

void CommentWidgetFactory::RefreshBotLayout(Evas_Object *parentLayout, Evas_Object *layout, bool isTextEntered, bool isPhotoAttached)
{
    elm_object_signal_emit(layout, isTextEntered || isPhotoAttached ?
            (Application::IsRTLLanguage() ? "active,rtl" : "active,ltr") :
            (Application::IsRTLLanguage() ? "disabled,rtl" : "disabled,ltr"), "send_btn");

    if (isPhotoAttached) {
        elm_object_signal_emit(parentLayout, "expand", "bot_part");
        elm_object_signal_emit(layout, "expand_spacer", "");
    } else {
        elm_object_signal_emit(layout, "minimize_spacer", "");
        elm_object_signal_emit(parentLayout, "minimize", "bot_part");
    }
}

void CommentWidgetFactory::reply_btn_cb(void *data, Evas_Object *obj, void *event_info)
{
    assert(data);
    ActionAndData *action_data = static_cast<ActionAndData*>(data);
    assert(action_data->mAction);

    Log::info(LOG_FACEBOOK_COMMENTING, "CommentWidgetFactory::reply_btn_cb");

    if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_REPLY) {
        ScreenBase* previousScreen =  Application::GetInstance()->GetTopScreen();
        CommentScreen* commentScreen = new CommentScreen(previousScreen, action_data, ScreenBase::SID_REPLY);
        Application::GetInstance()->AddScreen(commentScreen);

        ScreenBase* screen = action_data->mAction->getScreen();
        if (screen) {
            ScreenBase::ScreenIdEnum screenId = screen->GetScreenId();
            if ( screenId == ScreenBase::SID_COMMENTS ) {
                static_cast<CommentScreen *>(screen)->SetSelectedActionNData(action_data);
            } else if ( screenId == ScreenBase::SID_POST ) {
                static_cast<PostScreen *>(screen)->SetSelectedActionNData(action_data);
            }
        }
    }
}

void CommentWidgetFactory::reply_item_cb(void *data, Evas_Object *obj, void *event_info) {
    CommentWidgetFactory::reply_btn_cb(data, NULL, NULL);
}

void CommentWidgetFactory::reply_signal_cb(void *data, Evas_Object *obj, const char *emission, const char *source) {
    CommentWidgetFactory::reply_btn_cb(data, NULL, NULL);
}

Evas_Object *CommentWidgetFactory::CreateCommentAttachmentWrapper(Evas_Object* parent)
{
    Evas_Object *wrapper = elm_layout_add(parent);
    elm_layout_file_set(wrapper, Application::mEdjPath, "comment_attachment_wrapper");
    evas_object_size_hint_weight_set(wrapper, 1, 1);
    evas_object_size_hint_align_set(wrapper, -1, 0);
    elm_box_pack_end(parent, wrapper);
    evas_object_show(wrapper);

    Evas_Object *wrapperBox = elm_box_add(wrapper);
    evas_object_size_hint_weight_set(wrapperBox, 1, 1);
    evas_object_size_hint_align_set(wrapperBox, -1, 0);
    elm_layout_content_set(wrapper, "content", wrapperBox);
    elm_box_padding_set(wrapperBox, 0, 0);

    return wrapperBox;
}

Evas_Object *CommentWidgetFactory::CreateCommentPhotoAttachmentWrapper(Evas_Object* parent)
{
    Evas_Object *wrapper = elm_layout_add(parent);
    elm_layout_file_set(wrapper, Application::mEdjPath, "comment_photo_wrapper");
    evas_object_size_hint_weight_set(wrapper, 1, 1);
    evas_object_size_hint_align_set(wrapper, -1, 0);
    elm_box_pack_end(parent, wrapper);
    evas_object_show(wrapper);

    Evas_Object *wrapperBox = elm_box_add(wrapper);
    evas_object_size_hint_weight_set(wrapperBox, 1, 0);
    evas_object_size_hint_align_set(wrapperBox, -1, 0);
    elm_layout_content_set(wrapper, "content", wrapperBox);
    elm_box_padding_set(wrapperBox, 0, 0);

    return wrapperBox;
}

void CommentWidgetFactory::CallUpdateImageLayoutAsync(ActionAndData *action_data, Comment *comment, Evas_Object *picture) {
    if(picture && action_data && comment && comment->mAttachment){
        if(comment->mAttachment->mMedia
            && comment->mAttachment->mMedia->mImage
            && comment->mAttachment->mMedia->mImage->GetSrcUrl()) {
            action_data->UpdateImageLayoutAsync(comment->mAttachment->mMedia->mImage->GetSrcUrl(), picture,
                    ActionAndData::EImage, Comment::EPhotoPath);
        }
    }
}

void CommentWidgetFactory::AttachmentWithoutPictureItem(Evas_Object* parent, ActionAndData *action_data)
{
    Comment *comment = static_cast<Comment *>(action_data->mData);
    if(!comment || !comment->mAttachment) {
        return;
    }

    Evas_Object *item = elm_layout_add(parent);
    evas_object_size_hint_weight_set(item, 1, 1);
    evas_object_size_hint_align_set(item, -1, 0);
    elm_layout_file_set(item, Application::mEdjPath, "attachment_without_picture");

    elm_object_part_text_set(item, "link_title", comment->mAttachment->mTitle);
    elm_object_part_text_set(item, "link_description", comment->mAttachment->mDescription);
    if(comment->mAttachment->mTarget) {
        elm_object_part_text_set(item, "link", comment->mAttachment->mTarget->mUrl);
    }
    Evas_Object *gesture = elm_gesture_layer_add(item);
    elm_gesture_layer_attach(gesture, item);

    elm_gesture_layer_cb_set(gesture, ELM_GESTURE_N_TAPS, ELM_GESTURE_STATE_END, OnLinkGestureTap, action_data);

    elm_object_signal_callback_add(item, "mouse,down,*", "*", mouse_down, action_data);
    elm_object_signal_callback_add(item, "mouse,up,*", "*", mouse_up, action_data);

    elm_box_pack_end(parent, item);
    evas_object_show(item);
}

void CommentWidgetFactory::LinkAttachmentItem(Evas_Object* parent, ActionAndData *action_data)
{
    Evas_Object *item = elm_layout_add(parent);
    evas_object_size_hint_weight_set(item, 1, 1);
    evas_object_size_hint_align_set(item, -1, 0);
    elm_layout_file_set(item, Application::mEdjPath, "link_attachment_item");

    Evas_Object *link_picture = elm_image_add(item);
    elm_layout_content_set(item, "link_picture", link_picture);
    Comment *comment = static_cast<Comment *>((Comment *) action_data->mData);
    if(comment && comment->mAttachment){
        CallUpdateImageLayoutAsync(action_data, comment, link_picture);

        elm_object_part_text_set(item, "link_title", comment->mAttachment->mTitle);
        elm_object_part_text_set(item, "link_description", comment->mAttachment->mDescription);
        if(comment->mAttachment->mTarget) {
            elm_object_part_text_set(item, "link", comment->mAttachment->mTarget->mUrl);
        }
        Evas_Object *gesture = elm_gesture_layer_add(item);
        elm_gesture_layer_attach(gesture, item);

        elm_gesture_layer_cb_set(gesture, ELM_GESTURE_N_TAPS, ELM_GESTURE_STATE_END, OnLinkGestureTap, action_data);

        elm_object_signal_callback_add(item, "mouse,down,*", "*", mouse_down, action_data);
        elm_object_signal_callback_add(item, "mouse,up,*", "*", mouse_up, action_data);
    }
    evas_object_show(link_picture);

    elm_box_pack_end(parent, item);
    evas_object_show(item);
}

void CommentWidgetFactory::PageAttachmentItem(Evas_Object* parent, ActionAndData *action_data)
{
    Evas_Object *item = elm_layout_add(parent);
    evas_object_size_hint_weight_set(item, 1, 1);
    evas_object_size_hint_align_set(item, -1, 0);
    elm_layout_file_set(item, Application::mEdjPath, "page_attachment_item");

    Evas_Object *page_picture = elm_image_add(item);
    elm_layout_content_set(item, "page_avatar", page_picture);
    Comment *comment = static_cast<Comment *>((Comment *) action_data->mData);
    if(comment && comment->mAttachment){
        CallUpdateImageLayoutAsync(action_data, comment, page_picture);

        elm_object_part_text_set(item, "title", comment->mAttachment->mTitle);
        // TODO Add like btn
    }
    evas_object_show(page_picture);

    elm_box_pack_end(parent, item);
    evas_object_show(item);
}

void CommentWidgetFactory::PhotoAttachmentItem(Evas_Object* parent, ActionAndData *action_data)
{
    Evas_Object *item = elm_layout_add(parent);
    evas_object_size_hint_weight_set(item, 1, 1);
    evas_object_size_hint_align_set(item, -1, 0);

    Comment *comment = static_cast<Comment*>(action_data->mData);
    bool isSticker = comment->mAttachment->mType && !strcmp(comment->mAttachment->mType, "sticker");
    if (isSticker) {
        elm_layout_file_set(item, Application::mEdjPath, "photo_sticker_item");
    } else {
        elm_layout_file_set(item, Application::mEdjPath, "photo_attachment_item");
    }
    ImageSource *photo = static_cast<ImageSource*>(comment->mAttachment->mMedia->mImage);
    Evas_Object *cmnt_photo = elm_image_add(item);
    Evas_Coord cmnt_photo_width = 0;
    Evas_Coord cmnt_photo_height = 0;

    if (isSticker) {
        cmnt_photo_width = R->COMMENT_STICKER_LAYOUT_SIZE_W;
    } else {
        cmnt_photo_width = R->COMMENT_LANDSCAPE_PHOTO_LAYOUT_SIZE_W;
    }

    cmnt_photo_height = (Evas_Coord)(photo->GetHeight() * (cmnt_photo_width / (double)photo->GetWidth()));
    evas_object_size_hint_min_set(cmnt_photo, cmnt_photo_width, cmnt_photo_height);
    evas_object_size_hint_max_set(cmnt_photo, cmnt_photo_width, cmnt_photo_height);
    evas_object_size_hint_weight_set(cmnt_photo, 1, 1);
    evas_object_size_hint_align_set(cmnt_photo, -1, -1);
    evas_object_show(cmnt_photo);

    elm_layout_content_set(item, "photo", cmnt_photo);
    CallUpdateImageLayoutAsync(action_data, comment, cmnt_photo);

    Evas_Object *gesture = elm_gesture_layer_add(cmnt_photo);
    elm_gesture_layer_attach(gesture, cmnt_photo);

    if (!isSticker) {
        elm_gesture_layer_cb_set(gesture, ELM_GESTURE_N_TAPS, ELM_GESTURE_STATE_END, OnPhotoGestureTap, action_data);
    }

    elm_object_signal_callback_add(item, "mouse,down,*", "photo_layout", mouse_down, action_data);
    elm_object_signal_callback_add(item, "mouse,up,*", "photo_layout", mouse_up, action_data);

    evas_object_show(cmnt_photo);

    elm_box_pack_end(parent, item);
    evas_object_show(item);
}

Evas_Event_Flags CommentWidgetFactory::OnPhotoGestureTap(void *data, void *event_info)
{
    ActionAndData *action_data = static_cast<ActionAndData *>(data);
    StopLongpressTimer();
    ActionBase::on_photo_clicked_cb(action_data, NULL, event_info);
    return EVAS_EVENT_FLAG_NONE;
}

Evas_Event_Flags CommentWidgetFactory::OnLinkGestureTap(void *data, void *event_info)
{
    ActionAndData *action_data = static_cast<ActionAndData *>(data);
    StopLongpressTimer();
    ActionBase::on_link_clicked_cb(action_data, NULL, NULL, NULL);
    return EVAS_EVENT_FLAG_NONE;
}

void CommentWidgetFactory::OnAnchorTap(void *data, Evas_Object *obj, void *event_info)
{
    ActionAndData *action_data = static_cast<ActionAndData *>(data);
    assert(action_data);

    if (PopupMenu::IsPopupShown()) {
        if (action_data->mActionObj) {
            evas_object_smart_callback_add(action_data->mActionObj, "longpressed", OnAnchorTap, action_data);
            evas_object_smart_callback_add(action_data->mActionObj, "anchor,clicked", ActionBase::on_anchor_clicked_cb, action_data);
        }
    } else {
        Elm_Label_Anchor_Info *event = static_cast<Elm_Label_Anchor_Info*>(event_info);
        if (event && event->name) {
            StopLongpressTimer();
        }

        if (action_data->mActionObj) {
            evas_object_smart_callback_del(action_data->mActionObj, "longpressed", OnAnchorTap);
        }
        ActionBase::on_anchor_clicked_cb(action_data, NULL, event_info);
    }
}

void CommentWidgetFactory::VideoAttachmentItem(Evas_Object* parent, ActionAndData *action_data)
{
    Evas_Object *item = elm_layout_add(parent);
    evas_object_size_hint_weight_set(item, 1, 1);
    evas_object_size_hint_align_set(item, -1, 0);
    elm_layout_file_set(item, Application::mEdjPath, "video_attachment_item");

    Evas_Object *video_preview = elm_image_add(item);
    elm_layout_content_set(item, "preview", video_preview);
    CallUpdateImageLayoutAsync(action_data, (Comment *) action_data->mData, video_preview);
    evas_object_show(video_preview);
    Comment *comment = static_cast<Comment *>((Comment *) action_data->mData);
    if(comment && comment->mAttachment){
        elm_object_part_text_set(item, "title", comment->mAttachment->mTitle);

        elm_object_part_text_set(item, "description", comment->mAttachment->mDescription);

        // TODO No data from FB to this field
        if(comment->mAttachment->mTarget) {
            elm_object_part_text_set(item, "link", comment->mAttachment->mTarget->mUrl);
        }
    }

    elm_box_pack_end(parent, item);
    evas_object_show(item);
}

Evas_Object* CommentWidgetFactory::CreateCommentWrapper(Evas_Object* parent, Evas_Object** wrapWidget, bool isAddToEnd )
{
    Evas_Object *wrapper = elm_layout_add(parent);
    if(wrapWidget) {
        *wrapWidget = wrapper;
    }
    elm_layout_file_set(wrapper, Application::mEdjPath, "comment_wrapper");
    evas_object_size_hint_weight_set(wrapper, 1, 1);
    evas_object_size_hint_align_set(wrapper, -1, 0);
    Evas_Object *wrapperBox = elm_box_add(wrapper);
    evas_object_size_hint_weight_set(wrapperBox, 1, 1);
    evas_object_size_hint_align_set(wrapperBox, -1, 0);
    elm_layout_content_set(wrapper, "content", wrapperBox);
    elm_box_padding_set(wrapperBox, 0, 0);

    return wrapperBox;
}

void CommentWidgetFactory::CommentItem(Evas_Object* parent, ActionAndData *action_data)
{
    Comment *comment = static_cast<Comment*>(action_data->mData);
    assert(comment);
    if(!comment->GetFrom()) {
        return;
    }

    Evas_Object *item = elm_layout_add(parent);
    evas_object_size_hint_weight_set(item, 1, 1);
    evas_object_size_hint_align_set(item, -1, 0);
    elm_layout_file_set(item, Application::mEdjPath, "comment_item");

    elm_object_signal_callback_add(item, "mouse,down,*", "comment_*", mouse_down, action_data);
    elm_object_signal_callback_add(item, "mouse,up,*", "comment_*", mouse_up, action_data);

    Evas_Object *avatar = elm_image_add(item);
    elm_layout_content_set(item, "avatar", avatar);
    action_data->UpdateImageLayoutAsync(comment->GetAvatar(), avatar);
    evas_object_smart_callback_add(avatar, "clicked", ActionBase::on_avatar_clicked_cb, action_data);
    evas_object_show(avatar);

    Evas_Object *name = elm_label_add(item);
    elm_object_part_content_set(item, "name", name);
    char *userName = WidgetFactory::WrapByFormat2(COMMENT_AUTHOR_NAME, comment->GetFrom()->GetId(), comment->GetName() );
    if (userName) {
        elm_object_text_set(name, userName);
        delete[] userName;
    }
    evas_object_smart_callback_add(name, "anchor,clicked", ActionBase::on_anchor_clicked_cb, action_data);
    elm_label_wrap_width_set(name, R->COMMENT_MSG_WRAP_WIDTH);
    elm_label_line_wrap_set(name, ELM_WRAP_MIXED);
    evas_object_show(name);

    if (comment->GetMessage()) {
        Evas_Object *comment_text = elm_label_add(item);
        elm_object_part_content_set(item, "comment_text", comment_text);
        action_data->mActionObj = comment_text;

        CommentPresenter presenter(*comment);

        std::unique_ptr<char[]> msg_text(WidgetFactory::WrapByFormat2(SANS_REGULAR_000_FORMAT, R->COMMENT_MSG_TEXT_SIZE, presenter.GetCommentMessage().c_str()));
        if (msg_text) {
            elm_object_text_set(comment_text, msg_text.get());
        }
        elm_label_wrap_width_set(comment_text, R->COMMENT_MSG_WRAP_WIDTH);
        elm_label_line_wrap_set(comment_text, ELM_WRAP_MIXED);
        evas_object_show(comment_text);

        evas_object_smart_callback_add(action_data->mActionObj, "anchor,clicked", OnAnchorTap, action_data);
        evas_object_smart_callback_add(action_data->mActionObj, "longpressed", OnAnchorTap, action_data);
    }

    elm_box_pack_end(parent, item);
    evas_object_show(item);
}

void CommentWidgetFactory::mouse_down(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *actionAndData = static_cast<ActionAndData *>(data);
    assert(actionAndData);
    actionAndData->mObjectWidget = obj;
    if (mLongPressedTimer) {
        ecore_timer_del(mLongPressedTimer);
    }
    mLongPressedTimer = ecore_timer_add(1.0, longpressed_cmnt_cb, actionAndData);
}

void CommentWidgetFactory::mouse_up(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    StopLongpressTimer();
}

void CommentWidgetFactory::add_scroller_events(Evas_Object *scroller) {
    //First remove previously registered callback if any. If there is no registered callback then evas_object_smart_callback_del() do nothing.
    evas_object_smart_callback_del(scroller, "scroll", scroll);
    evas_object_smart_callback_add(scroller, "scroll", scroll, nullptr);
}

void CommentWidgetFactory::remove_scroller_events(Evas_Object *scroller) {
    evas_object_smart_callback_del(scroller, "scroll", scroll);
}

void CommentWidgetFactory::scroll(void *data, Evas_Object *obj, void *event_info) {
    StopLongpressTimer();
}

void CommentWidgetFactory::StopLongpressTimer() {
    if (mLongPressedTimer) {
        ecore_timer_del(mLongPressedTimer);
        mLongPressedTimer = nullptr;
    }
}

Eina_Bool CommentWidgetFactory::longpressed_cmnt_cb(void *data)
{
    assert(data);
    ActionAndData *action_data = static_cast<ActionAndData*>(data);

    if(!ActionAndData::IsAlive(action_data)) {
        Log::error("CommentWidgetFactory::longpressed_cmnt_cb->comment has been already deleted");
        return EINA_FALSE;
    }

    if (Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_COMMENTS ||
        Application::GetInstance()->GetTopScreenId() == ScreenBase::SID_REPLY) {
        CommentScreen *screen = static_cast<CommentScreen*>(action_data->mAction->getScreen());
        if (screen) {
            elm_entry_input_panel_hide(screen->GetCommentEntry());
        }
    }

    mLongPressedTimer = nullptr;

    if (action_data->mActionObj) {
        evas_object_smart_callback_del(action_data->mActionObj, "anchor,clicked", ActionBase::on_anchor_clicked_cb);
    }

    StopLongpressTimer();
    CommentWidgetFactory::ShowContextMenu(action_data, action_data->mObjectWidget, false);
    Log::debug(LOG_FACEBOOK_COMMENTING, "CommentWidgetFactory::longpressed_cmnt_cb");
    return EINA_FALSE;
}

void CommentWidgetFactory::ShowContextMenu(ActionAndData *action_data, Evas_Object *obj, bool useOldPosition)
{
    Comment *comment = static_cast<Comment*>(action_data->mData);

    Evas_Coord y = 0, h = 0;
    if(obj) {
        evas_object_geometry_get(obj, NULL, &y, NULL, &h);
        useOldPosition = h > largeCommentHeight;
        y += h/2;
    }

    bool userIsCmntOwner = false;
    if(action_data->mData && comment->mFrom->GetId()) {
        userIsCmntOwner = Config::GetInstance().GetUserId() == comment->mFrom->GetId();
    }

    int itemNum = 0;
    PopupMenu::PopupMenuItem *items= NULL;

    if (comment->GetMessage()) {  // "Copy" action is applicable to text messages only
        static PopupMenu::PopupMenuItem context_menu_owner_items[] = {
            { NULL, "IDS_DELETE_CMNT", NULL, on_delete_click, NULL, 0, false, false },
            { NULL, "IDS_COPY_CMNT", NULL, on_copy_click, NULL, 1, false, false },
            { NULL, "IDS_CANCEL", NULL, on_cancel_click, NULL, 2, false, false },
            { NULL, "IDS_EDIT_CMNT", NULL, on_edit_click, NULL, 3, true, false },
        };

        static PopupMenu::PopupMenuItem context_menu_other_items[] = {
            { NULL, "IDS_DELETE_CMNT", NULL, on_delete_click, NULL, 0, false, false },
            { NULL, "IDS_COPY_CMNT", NULL, on_copy_click, NULL, 1, false, false },
            { NULL, "IDS_CANCEL", NULL, on_cancel_click, NULL, 2, true, false },
        };

        static PopupMenu::PopupMenuItem context_menu_third_party_items[] = {
            { NULL, "IDS_COPY_CMNT", NULL, on_copy_click, NULL, 0, false, false },
            { NULL, "IDS_CANCEL", NULL, on_cancel_click, NULL, 1, true, false },
        };

        if (userIsCmntOwner) {
            itemNum = CONTEXT_MENU_CMNT_OWNER_ITEMS;
            items = context_menu_owner_items;
        } else {
            if (comment->mCanRemove) {
                itemNum = CONTEXT_MENU_CMNT_OTHER_ITEMS;
                items = context_menu_other_items;
            } else {
                itemNum = CONTEXT_MENU_THIRD_PARTY_ITEMS;
                items = context_menu_third_party_items;
            }
        }
        context_menu_owner_items[3].data = action_data;
    } else {
        static PopupMenu::PopupMenuItem context_menu_owner_items[] = {
            { NULL, "IDS_DELETE_CMNT", NULL, on_delete_click, NULL, 0, false, false },
            { NULL, "IDS_CANCEL", NULL, on_cancel_click, NULL, 1, false, false },
            { NULL, "IDS_EDIT_CMNT", NULL, on_edit_click, NULL, 2, true, false },
        };

        static PopupMenu::PopupMenuItem context_menu_other_items[] = {
            { NULL, "IDS_DELETE_CMNT", NULL, on_delete_click, NULL, 0, false, false },
            { NULL, "IDS_CANCEL", NULL, on_cancel_click, NULL, 1, true, false },
        };

        bool isSticker = false;
        if (comment && comment->mAttachment && comment->mAttachment->mType) {
            isSticker = !strcmp(comment->mAttachment->mType, "sticker");
        }

        if (userIsCmntOwner && !isSticker) {
            itemNum = CONTEXT_MENU_CMNT_OWNER_ITEMS - 1;
            items = context_menu_owner_items;
        } else {
            if (comment->mCanRemove) {
                itemNum = CONTEXT_MENU_CMNT_OTHER_ITEMS - 1;
                items = context_menu_other_items;
            }
        }
        context_menu_owner_items[2].data = action_data;
    }

    if (itemNum > 0) {
        PopupMenu::Show(itemNum, items, action_data->mAction->getScreen()->getParentMainLayout(),
                        useOldPosition, y, comment);
    }
}

void CommentWidgetFactory::on_cancel_click(void *data, Evas_Object *obj, void *user_data)
{
    CloseCommentPopup();
}

void CommentWidgetFactory::on_copy_click(void *data, Evas_Object *obj, void *user_data)
{
    ScreenBase *scr = Application::GetInstance()->GetTopScreen();
    if (data && scr) {
        Comment *comment = static_cast<Comment *>(data);
        ActionAndData *actionAndData = NULL;
        if (scr->GetScreenId() == ScreenBase::SID_COMMENTS || scr->GetScreenId() == ScreenBase::SID_REPLY) {
            actionAndData = static_cast<ActionAndData *>(static_cast<CommentScreen*>(scr)->GetItemById(comment->GetId()));
        } else if (scr->GetScreenId() == ScreenBase::SID_POST) {
            actionAndData = static_cast<ActionAndData *>(static_cast<PostScreen*>(scr)->GetItemById(comment->GetId()));
        }
        if (actionAndData) {
            elm_cnp_selection_set(actionAndData->mActionObj, ELM_SEL_TYPE_CLIPBOARD, ELM_SEL_FORMAT_TEXT, comment->GetMessage(), strlen(comment->GetMessage()));
        }
    }
}

void CommentWidgetFactory::on_edit_click(void *data, Evas_Object *obj, void *user_data)
{
    assert(data);
    ActionAndData *commentActionData = static_cast<ActionAndData *>(data);
    assert(commentActionData->mData);
    ScreenBase *scr = Application::GetInstance()->GetTopScreen();
    assert(scr);
    CommentScreen* commentScreen = dynamic_cast<CommentScreen*>(scr);
    if (commentScreen) {

        ActionAndData* actionData = nullptr;//to update root comment for edited reply
        CommentScreen* previousCommentScr = dynamic_cast<CommentScreen*>(commentScreen->GetPreviousScreen());
        if (previousCommentScr) {
            actionData = previousCommentScr->GetSelectedActionNData();
        } else {
            PostScreen* previousPostScr = dynamic_cast<PostScreen*>(commentScreen->GetPreviousScreen());
            if(previousPostScr) {
                actionData = previousPostScr->GetSelectedActionNData();
            }
        }

        Application::GetInstance()->AddScreen(new EditCommentScreen(commentActionData, commentScreen->GetDataArea(), actionData));
    } else {
        PostScreen* postScreen = dynamic_cast<PostScreen*>(scr);
        if (postScreen) {
            Application::GetInstance()->AddScreen(new EditCommentScreen(commentActionData, postScreen->GetDataArea()));
        } else {
            Log::error("CommentWidgetFactory::on_edit_click->wrong parent screen, can't cast");
        }
    }
}

void CommentWidgetFactory::on_delete_click(void *data, Evas_Object *obj, void *user_data)
{
    ScreenBase *scr = Application::GetInstance()->GetTopScreen();
    if (scr) {
        mConfirmationDialog = ConfirmationDialog::CreateAndShow(scr->getMainLayout(),
                                                                "IDS_SURE_TO_DELETE_TITLE",
                                                                "IDS_SURE_TO_DELETE",
                                                                "IDS_CANCEL",
                                                                "IDS_DELETE",
                                                                confirmation_dialog_cb,
                                                                data);
    }
}

bool CommentWidgetFactory::CloseCommentPopup() {
    bool ret = mConfirmationDialog;
    if (ret) {
        delete mConfirmationDialog;
        mConfirmationDialog = nullptr;
    }
    return ret;
}

void CommentWidgetFactory::confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event) {
    CloseCommentPopup();

    switch (event) {
    case ConfirmationDialog::ECDYesPressed:
        break;
    case ConfirmationDialog::ECDNoPressed:
        popup_approved_delete_cb(user_data, nullptr, nullptr);
        break;
    case ConfirmationDialog::ECDDismiss:
        break;
    default:
        break;
    }
}

//Delete cmnt approved
void CommentWidgetFactory::popup_approved_delete_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    ScreenBase *scr = Application::GetInstance()->GetTopScreen();
    if (user_data && scr) {
        Comment *comment = static_cast<Comment *>(user_data);
        ActionAndData *actionAndData = NULL;
        if (scr->GetScreenId() == ScreenBase::SID_COMMENTS || scr->GetScreenId() == ScreenBase::SID_REPLY) {
            actionAndData = static_cast<ActionAndData *>(static_cast<CommentScreen*>(scr)->GetItemById(comment->GetId()));
        } else if (scr->GetScreenId() == ScreenBase::SID_POST) {
            actionAndData = static_cast<ActionAndData *>(static_cast<PostScreen*>(scr)->GetItemById(comment->GetId()));
        }
        if (actionAndData) {
            ActionBase::on_ctxmenu_approved_delete_cmnt_clicked_cb(actionAndData, obj, event_info);
        }
    }
}

Evas_Object *CommentWidgetFactory::CreateDot(Evas_Object* parent)
{
    Evas_Object *dot = elm_label_add(parent);
    evas_object_size_hint_weight_set(dot, 0.5, 0.5);
    evas_object_size_hint_align_set(dot, 0, 0.5);
    elm_object_text_set(dot, "<color=#78839a>&#xb7;</color>");
    elm_box_pack_end(parent, dot);
    evas_object_show(dot);

    return dot;
}

void CommentWidgetFactory::ActionBar(Evas_Object* parent, ActionAndData *action_data)
{
    CObjAndData *c_data = new CObjAndData;
    c_data->mCData = action_data;

    Evas_Object *item = elm_layout_add(parent);
    evas_object_size_hint_weight_set(item, 1, 1);
    elm_layout_file_set(item, Application::mEdjPath, "comment_action_bar");
    evas_object_event_callback_add(item, EVAS_CALLBACK_DEL, on_action_bar_deleted, c_data);

    Comment *comment = static_cast<Comment*>(action_data->mData);

    Evas_Object *action_bar = elm_box_add(item);
    elm_box_padding_set(action_bar, 5, 0);
    elm_object_part_content_set(item, "spacer", action_bar);
    evas_object_size_hint_weight_set(action_bar, 1, 1);
    evas_object_size_hint_align_set(action_bar, 0, -1);
    elm_box_horizontal_set(action_bar, EINA_TRUE);
    elm_box_pack_end(item, action_bar);
    evas_object_show(action_bar);

    c_data->mCActionBar = action_bar;

    Evas_Object *date_label = elm_label_add(action_bar);
    evas_object_size_hint_weight_set(date_label, 0.5, 0.5);
    evas_object_size_hint_align_set(date_label, 0, 0.5);
    DATE_TRNSLTD_PART_TEXT_SET(date_label, nullptr, R->COMMENT_ACTIONBAR_WITH_TIME, comment->mCreatedTime);
    elm_box_pack_end(action_bar, date_label);
    evas_object_show(date_label);

    if (comment->mCanLike) {
        CreateDot(action_bar);
        char *like_text = NULL;
        Evas_Object *like_label = elm_label_add(action_bar);
        evas_object_size_hint_weight_set(like_label, 0.5, 0.5);
        evas_object_size_hint_align_set(like_label, 0, 0.5);
        if (comment->mUserLikes == false) {
            like_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_586c95_FORMAT, R->COMMENT_ACTIONBAR_TEXT_SIZE, "IDS_LIKE");
            evas_object_smart_callback_add(like_label, "clicked", like_btn_clicked, c_data);
        } else {
            like_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_586c95_FORMAT, R->COMMENT_ACTIONBAR_TEXT_SIZE, "IDS_UNLIKE");
            evas_object_smart_callback_add(like_label, "clicked", unlike_btn_clicked, c_data);
        }
        if (like_text) {
            HRSTC_TRNSLTD_TEXT_SET(like_label, like_text);
            delete[] like_text;
        }
        elm_box_pack_end(action_bar, like_label);
        evas_object_show(like_label);

        c_data->mCLikeLabel = like_label;
    }

    if (comment->mLikeCount)
    {
        Evas_Object *like_label_dot = CreateDot(action_bar);
        c_data->mCLikeLabelDot = like_label_dot;
        Evas_Object *like_icon = elm_image_add(action_bar);
        evas_object_size_hint_weight_set(like_icon, 1, 1);
        evas_object_size_hint_align_set(like_icon, 0, 0.5);
        evas_object_size_hint_min_set(like_icon, R->COMMENT_ACTIONBAR_LIKEICON_SIZE, R->COMMENT_ACTIONBAR_LIKEICON_SIZE);
        evas_object_size_hint_max_set(like_icon, R->COMMENT_ACTIONBAR_LIKEICON_SIZE, R->COMMENT_ACTIONBAR_LIKEICON_SIZE);
        elm_image_file_set(like_icon, ICON_COMMENTS_DIR"/post_likes_count.png", NULL);
        evas_object_smart_callback_add(like_icon, "clicked", who_liked_comment_clicked, comment);
        elm_box_pack_end(action_bar, like_icon);
        c_data->mCLikeIcon = like_icon;
        evas_object_show(like_icon);

        char *like_count_text = NULL;
        Evas_Object *like_count = elm_label_add(action_bar);
        evas_object_size_hint_weight_set(like_count, 0.5, 0.5);
        evas_object_size_hint_align_set(like_count, 0, 0.5);
        std::string likeCount(std::move(std::to_string(comment->mLikeCount)));
        like_count_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_586c95_FORMAT, R->COMMENT_ACTIONBAR_LIKE_COUNT_TEXT_SIZE, likeCount.c_str());
        if (like_count_text) {
            elm_object_text_set(like_count, like_count_text);
            delete[] like_count_text;
        }
        evas_object_smart_callback_add(like_count, "clicked", who_liked_comment_clicked, comment);
        elm_box_pack_end(action_bar, like_count);
        c_data->mCLikeCount = like_count;
        evas_object_show(like_count);
    }

    if (comment->mCanComment == TRUE) {
        c_data->mCReplyLabelDot = CreateDot(action_bar);
        char *reply_text = NULL;
        Evas_Object *reply_label = elm_label_add(action_bar);
        evas_object_size_hint_weight_set(reply_label, 0.5, 0.5);
        evas_object_size_hint_align_set(reply_label, 0, 0.5);
        reply_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_586c95_FORMAT, R->COMMENT_ACTIONBAR_TEXT_SIZE, "IDS_REPLY");
        if (reply_text) {
            HRSTC_TRNSLTD_TEXT_SET(reply_label, reply_text);
            delete[] reply_text;
        }
        evas_object_smart_callback_add(reply_label, "clicked", reply_item_cb, action_data);
        elm_box_pack_end(action_bar, reply_label);
        evas_object_show(reply_label);
    }

    Evas_Object *spacer = elm_box_add(action_bar);
    CHECK_RET_NRV(spacer);
    evas_object_size_hint_weight_set(spacer, 20, 20);
    evas_object_size_hint_align_set(spacer, -1, -1);
    elm_box_pack_end(action_bar, spacer);
    evas_object_show(spacer);

    elm_box_pack_end(parent, item);
    evas_object_show(item);
}

void CommentWidgetFactory::on_action_bar_deleted(void *data, Evas *e, Evas_Object *obj, void *event_info) {
    delete static_cast<CObjAndData *> (data);
}

Evas_Object *CommentWidgetFactory::CreateReplyWrapper(Evas_Object *parent, Evas_Object** wrapWidget, bool isAddToEnd)
{
    Evas_Object *wrapper = elm_layout_add(parent);
    if(wrapWidget) {
        *wrapWidget = wrapper;
    }
    elm_layout_file_set(wrapper, Application::mEdjPath, "reply_wrapper");
    evas_object_size_hint_weight_set(wrapper, 1, 1);
    evas_object_size_hint_align_set(wrapper, -1, 0);
    if ( isAddToEnd ) {
        elm_box_pack_end( parent, wrapper );
    } else {
        elm_box_pack_start( parent, wrapper );
    }
    evas_object_show(wrapper);

    Evas_Object *wrapperBox = elm_box_add(wrapper);
    evas_object_size_hint_weight_set(wrapperBox, 1, 1);
    evas_object_size_hint_align_set(wrapperBox, -1, 0);
    elm_layout_content_set(wrapper, "content", wrapperBox);
    elm_box_padding_set(wrapperBox, 0, 0);

    return wrapperBox;
}

//Widgets hierarchy for comment with replies (justFYI)
//1) TIP::mDataArea ( root elm_box) : Ptr is TIP::DataArea  (could be get using GetDataArea() )
//2) elm layout( loaded into "comment_wrapper") , packed on TIP::DataArea elm box : Ptr is mInActionAndData->mParentWidget
//3) internal (elm_box) set to ("content" field) of "comment_wrapper" layout : no Ptr in AnD
//4) internal elm_layout( loaded into "reply_wrapper" ), packed on "content" field of elm_box(3) :  no Ptr in AnD
//5) elm_box (set to "content" field of "reply_wrapper" elm_layout) : Ptr is mInActionAndData->mReplyActionObject

void CommentWidgetFactory::ReplyItem(Evas_Object* parent, ActionAndData *action_data, Comment *rootComment)
{
    Comment *comment = rootComment;
    if (!comment) {
        comment = static_cast<Comment*>(action_data->mData);
    }
    Evas_Object * reply_wrapper = CreateReplyWrapper(parent, NULL, true);
    action_data->mReplyActionObject = reply_wrapper;

    if (comment->mCommentsList) {
        if (comment->GetRepliesCount() > CommentsProvider::COMMENTS_DEFAULT_REPLY_LIMIT) {
            Evas_Object *itemHeader = elm_layout_add(reply_wrapper);
            evas_object_size_hint_weight_set(itemHeader, 1, 1);
            evas_object_size_hint_align_set(itemHeader, -1, 0);
            elm_layout_file_set(itemHeader, Application::mEdjPath, "reply_counter_line");

            CommentPresenter presenter(*comment);
            HRSTC_TRNSLTD_PART_TEXT_SET(itemHeader, "reply_count", presenter.GetRepliesCount().c_str());

            elm_box_pack_end(reply_wrapper, itemHeader);
            evas_object_show(itemHeader);
            elm_object_signal_callback_add(itemHeader, "got.a.reply.click", "reply_*", reply_signal_cb, action_data);
        }

        Eina_List * list;
        void * replyComment;
        Evas_Object * lastItem = NULL;
        EINA_LIST_FOREACH(comment->mCommentsList , list, replyComment) {
            assert(replyComment);
            Comment *reply = static_cast<Comment*>(replyComment);
            if(!reply->GetFrom()) {
                continue;
            }
            Evas_Object *item = elm_layout_add(reply_wrapper);
            evas_object_size_hint_weight_set(item, 1, 1);
            evas_object_size_hint_align_set(item, -1, 0);
            elm_layout_file_set(item, Application::mEdjPath, "reply_item");

            Evas_Object *avatar = elm_image_add(item);
            elm_layout_content_set(item, "reply_last_avatar", avatar);
            action_data->UpdateImageLayoutAsync(reply->GetAvatar(), avatar);
            evas_object_show(avatar);
            // TODO: we do not have action and data for comments replies
            // so click on avatar cannot be not handled now
            // evas_object_smart_callback_add(item, "clicked", ActionBase::on_comment_reply_avatar_clicked_cb,
            //                              <special_strcuture_to_hold_reply_author_id>);

            Evas_Object *user_name_comment = elm_label_add(item);
            elm_layout_content_set(item, "reply_last_name", user_name_comment);

            CommentPresenter presenter(*reply);
            HRSTC_TRNSLTD_TEXT_SET(user_name_comment, presenter.GetUserWithReply().c_str());
            elm_label_wrap_width_set(user_name_comment, R->COMMENT_REPLY_ITEM_MSG_WRAP_WIDTH);
            evas_object_smart_callback_add(user_name_comment, "clicked", reply_btn_cb, action_data);
            elm_label_line_wrap_set(user_name_comment, ELM_WRAP_NONE);
            evas_object_show(user_name_comment);

            if (lastItem) {
                elm_box_pack_before(reply_wrapper, item, lastItem);
            }
            else {
                elm_box_pack_end(reply_wrapper, item);
            }
            lastItem = item;
            evas_object_show(item);
            elm_object_signal_callback_add(item, "got.a.reply.click", "reply_*", reply_signal_cb, action_data);
        }
    }
}

Evas_Object *CommentWidgetFactory::CommentsGet(void *data, Evas_Object *parent, bool isAddToEnd )
{
    ActionAndData *action_data = static_cast<ActionAndData*>(data);
    Comment *comment = static_cast<Comment*>(action_data->mData);
    CHECK_RET(comment, NULL);

    Evas_Object *attachment;
    Evas_Object *wrapper = NULL;
    Evas_Object *content = CreateCommentWrapper(parent, &wrapper, isAddToEnd);
    CommentItem(content, action_data);
    if (comment->mAttachment) {
        const char* type = comment->mAttachment->mType;
        if (type) {
            if (!strcmp(type, "share")) {
                attachment = CreateCommentAttachmentWrapper(content);
                if (comment->mAttachment->mMedia && comment->mAttachment->mMedia->mImage && comment->mAttachment->mMedia->mImage->GetSrcUrl()) {
                    LinkAttachmentItem(attachment, action_data);
                } else {
                    AttachmentWithoutPictureItem(attachment, action_data);
                }
            } else if (!strcmp(type, "avatar")) {
                attachment = CommentWidgetFactory::CreateCommentAttachmentWrapper(content);
                PageAttachmentItem(attachment, action_data);
            } else if (!strcmp(type, "photo") || !strcmp(type, "sticker")) {
                attachment = CommentWidgetFactory::CreateCommentPhotoAttachmentWrapper(content);
                PhotoAttachmentItem(attachment, action_data);
            } else if (!strcmp(type, "video_share_youtube")) {
                attachment = CommentWidgetFactory::CreateCommentAttachmentWrapper(content);
                VideoAttachmentItem(attachment, action_data);
            }
        }
    }
    ActionBar(content, action_data);

    if (comment->GetRepliesCount()) {
        ReplyItem(content, action_data);
    }
    if (wrapper) {
        if (isAddToEnd) {
            elm_box_pack_end(parent, wrapper);
        } else {
            elm_box_pack_start(parent, wrapper);
        }
        evas_object_show(wrapper);
    }

    return wrapper;
}

void CommentWidgetFactory::who_liked_comment_clicked(void *data, Evas_Object *obj, void *event_info)
{
    Comment *comment = static_cast<Comment*>(data);

    if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_WHO_LIKED) {
        if (comment) {
            WhoLikedScreen* screen = new WhoLikedScreen(comment->GetId());
            Application::GetInstance()->AddScreen(screen);
        }
    }
}

void CommentWidgetFactory::like_btn_clicked(void *data, Evas_Object *obj, void *event_info)
{
    Log::info(LOG_FACEBOOK_COMMENTING, "CommentWidgetFactory::like_btn_clicked()");
    CObjAndData *action_data = static_cast<CObjAndData*>(data);
    Comment *comment = static_cast<Comment*>(action_data->mCData->mData);

    const char* commentId = comment->GetId();
    OperationManager::GetInstance()->AddOperation(MakeSptr<LikeOperation<Comment>>(commentId ? commentId : "", Operation::OT_Like));

    comment->SetHasLikedTrue();
    char *like_text = NULL;
    like_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_586c95_FORMAT, R->COMMENT_ACTIONBAR_TEXT_SIZE, "IDS_UNLIKE");
    evas_object_smart_callback_del(action_data->mCLikeLabel, "clicked", like_btn_clicked);
    evas_object_smart_callback_add(action_data->mCLikeLabel, "clicked", unlike_btn_clicked, action_data);
    if (like_text) {
        HRSTC_TRNSLTD_TEXT_SET(action_data->mCLikeLabel, like_text);
        delete[] like_text;
    }

    if (comment->mLikeCount == 0) {
        evas_object_del(action_data->mCLikeIcon);
        evas_object_del(action_data->mCLikeCount);
        evas_object_del(action_data->mCLikeLabelDot);
        action_data->mCLikeIcon = NULL;
        action_data->mCLikeCount = NULL;
        action_data->mCLikeLabelDot = NULL;
    } else if (comment->mLikeCount == 1) {
        action_data->mCLikeLabelDot = elm_label_add(action_data->mCActionBar);
        evas_object_size_hint_weight_set(action_data->mCLikeLabelDot, 0.5, 0.5);
        evas_object_size_hint_align_set(action_data->mCLikeLabelDot, 0, 0.5);
        elm_object_text_set(action_data->mCLikeLabelDot, "<color=#78839a>&#xb7;</color>");
        elm_box_pack_after(action_data->mCActionBar, action_data->mCLikeLabelDot, action_data->mCLikeLabel);
        evas_object_show(action_data->mCLikeLabelDot);

        Evas_Object *like_icon = elm_image_add(action_data->mCActionBar);
        evas_object_size_hint_weight_set(like_icon, 1, 1);
        evas_object_size_hint_align_set(like_icon, 0, 0.5);
        evas_object_size_hint_min_set(like_icon, R->COMMENT_ACTIONBAR_LIKEICON_SIZE, R->COMMENT_ACTIONBAR_LIKEICON_SIZE);
        evas_object_size_hint_max_set(like_icon, R->COMMENT_ACTIONBAR_LIKEICON_SIZE, R->COMMENT_ACTIONBAR_LIKEICON_SIZE);
        elm_image_file_set(like_icon, ICON_COMMENTS_DIR"/post_likes_count.png", NULL);
        elm_box_pack_after(action_data->mCActionBar, like_icon, action_data->mCLikeLabelDot);
        evas_object_smart_callback_add(like_icon, "clicked", who_liked_comment_clicked, comment);
        action_data->mCLikeIcon = like_icon;
        evas_object_show(like_icon);

        char *like_count_text = NULL;
        Evas_Object *like_count = elm_label_add(action_data->mCActionBar);
        evas_object_size_hint_weight_set(like_count, 0.5, 0.5);
        evas_object_size_hint_align_set(like_count, 0, 0.5);
        std::string likeCount(std::move(std::to_string(comment->mLikeCount)));
        like_count_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_586c95_FORMAT, R->COMMENT_ACTIONBAR_TEXT_SIZE, likeCount.c_str());
        if (like_count_text) {
            elm_object_text_set(like_count, like_count_text);
            delete[] like_count_text;
        }
        elm_box_pack_after(action_data->mCActionBar, like_count, like_icon);
        evas_object_smart_callback_add(like_count, "clicked", who_liked_comment_clicked, comment);
        action_data->mCLikeCount = like_count;
        evas_object_show(like_count);
    } else {
        char *like_count_text = NULL;
        std::string likeCount(std::move(std::to_string(comment->mLikeCount)));
        like_count_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_586c95_FORMAT, R->COMMENT_ACTIONBAR_TEXT_SIZE, likeCount.c_str());
        if (like_count_text) {
            elm_object_text_set(action_data->mCLikeCount, like_count_text);
            delete[] like_count_text;
        }
    }
}

void CommentWidgetFactory::unlike_btn_clicked(void *data, Evas_Object *obj, void *event_info)
{
    Log::info(LOG_FACEBOOK_COMMENTING, "CommentWidgetFactory::unlike_btn_clicked()");

    CObjAndData *action_data = static_cast<CObjAndData*>(data);
    if (action_data) {
        Comment *comment = static_cast<Comment*>(action_data->mCData->mData);

        if (comment) {
            const char* commentId = comment->GetId();
            OperationManager::GetInstance()->AddOperation(MakeSptr<LikeOperation<Comment>>(commentId ? commentId : "", Operation::OT_Unlike));

            char *like_text = NULL;
            comment->SetHasLikedFalse();
            like_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_586c95_FORMAT, R->COMMENT_ACTIONBAR_TEXT_SIZE, "IDS_LIKE");
            evas_object_smart_callback_del(action_data->mCLikeLabel, "clicked", unlike_btn_clicked);
            evas_object_smart_callback_add(action_data->mCLikeLabel, "clicked", like_btn_clicked, action_data);
            if (like_text) {
                HRSTC_TRNSLTD_TEXT_SET(action_data->mCLikeLabel, like_text);
                delete[] like_text;
            }

            if (comment->mLikeCount == 0) {
                evas_object_del(action_data->mCLikeIcon);
                evas_object_del(action_data->mCLikeCount);
                evas_object_del(action_data->mCLikeLabelDot);
                action_data->mCLikeIcon = NULL;
                action_data->mCLikeCount = NULL;
                action_data->mCLikeLabelDot = NULL;
            } else {
                char *like_count_text = NULL;
                std::string likeCount = std::to_string(comment->mLikeCount);
                like_count_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_586c95_FORMAT, R->COMMENT_ACTIONBAR_TEXT_SIZE, likeCount.c_str());
                if (like_count_text) {
                    elm_object_text_set(action_data->mCLikeCount, like_count_text);
                    delete[] like_count_text;
                }
            }
        }
    }
}

void CommentWidgetFactory::on_delete_unposted_click(void *data, Evas_Object *obj, void *event_info) {
    ActionAndData *actionData = static_cast<ActionAndData *>(data);
    if (actionData && actionData->mAction) {
        actionData->mAction->CancelPostingOperation(actionData);
    }
}

void CommentWidgetFactory::on_try_again_click(void *data, Evas_Object *obj, void *user_data) {
    ActionAndData *actionData = static_cast<ActionAndData *>(data);
    if (actionData && actionData->mAction) {
        actionData->mAction->RetryPostSend(actionData);
    }
}

void CommentWidgetFactory::ErrorPopupCb(void *user_data, Evas_Object *obj, const char *emission, const char *source) {
    ActionAndData *actionData = static_cast<ActionAndData *>(user_data);
    if (actionData) {
        ShowErrorPopup(actionData, obj, false);
    }
}

Evas_Object *CommentWidgetFactory::CreateCommentOperation(Evas_Object *parent, void *user_data, bool isAddToEnd, Evas_Object* insertAfter) {
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    Evas_Object* wrapWidget = NULL;

    if (action_data) {
        Evas_Object *content = CreateCommentWrapper( parent, &wrapWidget, isAddToEnd );

        AddLazyCommentItem(content, action_data);

        OperationPresenter* op = static_cast<OperationPresenter*>(action_data->mData);
        Sptr<Operation> operation(op->GetOperation());
        Sptr<SimpleCommentOperation> simpleCommentOperation(static_cast<SimpleCommentOperation*>(operation.Get()));

        if(simpleCommentOperation->GetPhotoFileName()) {
            Evas_Object *attachment = CommentWidgetFactory::CreateCommentPhotoAttachmentWrapper(content);
            LazyPhotoAttachmentItem(attachment, action_data);
        }

        Evas_Object *opLayout = elm_layout_add( content );
        elm_layout_file_set( opLayout, Application::mEdjPath, "comment_operation_presenter");
        elm_box_pack_end( content, opLayout );
        evas_object_show( opLayout );

        Evas_Object *spacer = elm_box_add(content);
        evas_object_size_hint_weight_set(spacer, 20, 20);
        evas_object_size_hint_align_set(spacer, -1, -1);
        elm_box_pack_end(content, spacer);
        evas_object_show(spacer);

        elm_object_translatable_part_text_set(opLayout, "operation_state", "IDS_SHARE_POSTING");
        action_data->mOperationProgress = opLayout;
        SetUploadErrorLayout(action_data);

        if ( insertAfter ){ // repack instead of the same item
            elm_box_pack_after( parent, wrapWidget, insertAfter );
        } else if ( isAddToEnd ) {
            elm_box_pack_end( parent, wrapWidget );
        } else {
            elm_box_pack_start( parent, wrapWidget );
        }
        if (action_data->mReplyActionObject && // redraw reply item in case of Edit only
            op->GetEditedComment()) {
            CommentWidgetFactory::ReplyItem(content, action_data, op->GetEditedComment());
        }
        evas_object_show(wrapWidget);
    }

    return wrapWidget;
}

void CommentWidgetFactory::UpdateCommentOperation(ActionAndData *action_data) {
    if (action_data && action_data->mData && (action_data->mData->GetGraphObjectType() == GraphObject::GOT_OPERATION_PRESENTER)
       && action_data->mOperationProgress) {
        OperationPresenter *op = static_cast<OperationPresenter*>(action_data->mData);
        Sptr<Operation> oper(op->GetOperation());
        if (oper) {
            if (oper->GetOperationResult() == Operation::OR_Error) {
                elm_layout_signal_emit(action_data->mInfoBoxWidget, "show.moreinfo", "");
                const char* text = (oper->GetType() == Operation::OT_Edit_CommentReply) ?
                    "IDS_EDIT_COMMENT_FAILED" : "IDS_POST_COMMENT_FAILED";
                elm_object_translatable_part_text_set(action_data->mOperationProgress, "operation_state", text);
            } else {
                elm_layout_signal_emit(action_data->mInfoBoxWidget, "hide.moreinfo", "");
                switch(oper->GetState()) {
                case Operation::OS_InProgress:
                    elm_object_translatable_part_text_set(action_data->mOperationProgress, "operation_state", "IDS_POST_COMMENT_PROGRESS");
                    break;
                case Operation::OS_Completed:
                    elm_object_translatable_part_text_set(action_data->mOperationProgress, "operation_state", "IDS_PROFILE_ME_UPLOAD_PROFILE_PICTURE");
                    break;
                case Operation::OS_NotStarted:
                case Operation::OS_Paused:
                case Operation::OS_Canceled:
                default:
                    elm_object_translatable_part_text_set(action_data->mOperationProgress, "operation_state", "IDS_SHARE_POSTING");
                    break;
                }
            }
        }
    }
}

void CommentWidgetFactory::ShowErrorPopup(ActionAndData *action_data, Evas_Object *obj, bool useOldPosition) {

    if(!ActionAndData::IsAlive(action_data)) {
        Log::error("CommentWidgetFactory::ShowErrorPopup->ActionAndData is dead");
        return;
    }
    OperationPresenter* op = dynamic_cast<OperationPresenter*>(action_data->mData);
    if(!op){
        Log::error("CommentWidgetFactory::ShowErrorPopup->wrong type of ActionAndData->mData");
        return;
    }

    int itemNum = 0;
    PopupMenu::PopupMenuItem *items = NULL;

    Evas_Coord y = 0, h = 0;
    if(obj) {
        evas_object_geometry_get(obj, NULL, &y, NULL, &h);
        useOldPosition = h > largeCommentHeight;
        y += h/2;
    }

    static PopupMenu::PopupMenuItem context_menu_edit_error_items[1] =
            {
                { NULL, "IDS_TRY_AGAIN", NULL, on_try_again_click, NULL, 0, false, false },
            };

    static PopupMenu::PopupMenuItem context_menu_post_error_items[2] =
            {
                { NULL, "IDS_DELETE_CMNT", NULL, on_delete_unposted_click, NULL, 0, false, false },
                { NULL, "IDS_TRY_AGAIN", NULL, on_try_again_click, NULL, 1, false, false },
            };
    if(op->GetEditedComment()) {
        itemNum = 1;
        items = context_menu_edit_error_items;
        context_menu_edit_error_items[0].data = action_data;
    } else {
        itemNum = 2;
        items = context_menu_post_error_items;
        context_menu_post_error_items[0].data = action_data;
        context_menu_post_error_items[1].data = action_data;
    }

    PopupMenu::Show(itemNum, items, action_data->mAction->getScreen()->getParentMainLayout(), useOldPosition, y, action_data->mData);
}

void CommentWidgetFactory::SetUploadErrorLayout(ActionAndData *action_data) {
    Log::info(LOG_FACEBOOK_OPERATION, "CommentWidgetFactory::SetUploadErrorLayout");
    Evas_Object *errorLayout = elm_layout_add(action_data->mOperationProgress);
    elm_layout_file_set(errorLayout, Application::mEdjPath, "comment_request_error");
    elm_layout_content_set(action_data->mOperationProgress, "elm.progressbar.swallow", errorLayout);

    Evas_Object* loadErrorIcon  = elm_image_add(errorLayout);
    elm_image_file_set(loadErrorIcon, ICON_DIR "/ic_lazyload_error.png", nullptr);
    elm_layout_content_set( errorLayout, "more_info_icon", loadErrorIcon );

    elm_object_signal_callback_add(errorLayout, "mouse,clicked,*", "more_info_icon", ErrorPopupCb, action_data);

    action_data->mInfoBoxWidget = errorLayout;
    elm_layout_signal_emit(action_data->mInfoBoxWidget, "hide.moreinfo", "");
}

void CommentWidgetFactory::AddLazyCommentItem(Evas_Object* parent, ActionAndData *action_data)
{
    Evas_Object *item = elm_layout_add(parent);
    evas_object_size_hint_weight_set(item, 1, 1);
    evas_object_size_hint_align_set(item, -1, 0);
    elm_layout_file_set(item, Application::mEdjPath, "comment_item");
    elm_box_pack_end(parent, item);
    evas_object_show(item);

    OperationPresenter* op = static_cast<OperationPresenter*>(action_data->mData);
    if(op->GetEditedComment()) {//edited comment
        Evas_Object *avatar = elm_image_add(item);
        elm_layout_content_set(item, "avatar", avatar);
        action_data->UpdateImageLayoutAsync(op->GetEditedComment()->GetAvatar(), avatar);
        evas_object_show(avatar);
    } else if (LoggedInUserData::GetInstance().mPicture) { //new comment
        action_data->UpdateImageLayoutAsync(LoggedInUserData::GetInstance().mPicture->mUrl, item, ActionAndData::EImage, Comment::EProfileAvatar);
    }

    char *userName = NULL;
    if (LoggedInUserData::GetInstance().mName != NULL) {
        userName = SAFE_STRDUP(LoggedInUserData::GetInstance().mName);
    } else {
        userName = SAFE_STRDUP("You");
    }

    Evas_Object *name = elm_label_add(item);
    elm_object_part_content_set(item, "name", name);
    char *userNameFormatted = WidgetFactory::WrapByFormat2(COMMENT_AUTHOR_NAME, Config::GetInstance().GetUserId().c_str(), userName );
    if (userNameFormatted) {
        elm_object_text_set(name, userNameFormatted);
        delete[] userNameFormatted;
    }
    free(userName);

    elm_label_wrap_width_set(name, R->COMMENT_MSG_WRAP_WIDTH);
    elm_label_line_wrap_set(name, ELM_WRAP_MIXED);
    evas_object_show(name);

    if (op->GetMessage() && !Utils::isEmptyString(op->GetMessage())) {
        Evas_Object *comment_text = elm_label_add(item);
        elm_object_part_content_set(item, "comment_text", comment_text);
        action_data->mActionObj = comment_text;

        PostPresenter presenter(*op); //TODO: replace with CommentPresenter
        char* msg_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_000_FORMAT, R->COMMENT_MSG_TEXT_SIZE, presenter.GetMessage().c_str());
        if (msg_text) {
            elm_object_text_set(comment_text, msg_text);
            delete[] msg_text;
        }
        elm_label_wrap_width_set(comment_text, R->COMMENT_MSG_WRAP_WIDTH);
        elm_label_line_wrap_set(comment_text, ELM_WRAP_MIXED);
        evas_object_show(comment_text);
    } else {
        // Empty widget is released with DeleteAll_Prepare call.
        action_data->mActionObj = NULL;
    }
}

void CommentWidgetFactory::LazyPhotoAttachmentItem(Evas_Object* parent, ActionAndData *action_data)
{
    Evas_Object *item = elm_layout_add(parent);
    evas_object_size_hint_weight_set(item, 1, 1);
    evas_object_size_hint_align_set(item, -1, 0);
    elm_layout_file_set(item, Application::mEdjPath, "photo_attachment_item");
    elm_box_pack_end(parent, item);
    evas_object_show(item);

    if(action_data && action_data->mData) {
        OperationPresenter* op = static_cast<OperationPresenter*>(action_data->mData);
        Sptr<Operation> operation(op->GetOperation());
        Sptr<SimpleCommentOperation> simpleCommentOperation(static_cast<SimpleCommentOperation*>(operation.Get()));

        if(simpleCommentOperation) {
            Evas_Object *cmntPhoto = elm_image_add(item);
            evas_object_size_hint_weight_set(cmntPhoto, 1, 1);
            evas_object_size_hint_align_set(cmntPhoto, -1, 0);
            evas_object_show(cmntPhoto);

            elm_layout_content_set(item, "photo", cmntPhoto);
            elm_image_file_set(cmntPhoto, simpleCommentOperation->GetPhotoFileThumbnailPath(), NULL);

            Evas_Coord cmntPhotoWidth = R->COMMENT_LANDSCAPE_PHOTO_LAYOUT_SIZE_W;
            Evas_Coord cmntPhotoHeight = 100;

            int w, h;
            elm_image_object_size_get(cmntPhoto, &w, &h);
            if (w != 0) {
                cmntPhotoHeight = cmntPhotoWidth * h / w;
            }
            Log::info(LOG_FACEBOOK_COMMENTING, "Image size: %dx%d, aspect:%f", cmntPhotoWidth, cmntPhotoHeight, (float)h/w);

            Evas_Object * o = edje_object_part_swallow_get(elm_layout_edje_get(item), "photo");
            evas_object_size_hint_min_set(o, cmntPhotoWidth, cmntPhotoHeight);
            evas_object_size_hint_max_set(o, cmntPhotoWidth, cmntPhotoHeight);

            evas_object_size_hint_min_set(cmntPhoto, cmntPhotoWidth, cmntPhotoHeight);
            evas_object_size_hint_max_set(cmntPhoto, cmntPhotoWidth, cmntPhotoHeight);

            evas_object_show(cmntPhoto);
        }
    }
}
