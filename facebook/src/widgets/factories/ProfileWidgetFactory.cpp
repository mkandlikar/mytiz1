#include "Application.h"
#include "Common.h"
#include "Config.h"
#include "ConnectivityManager.h"
#include "ErrorHandling.h"
#include "EventAllPostsScreen.h"
#include "EventCreatePage.h"
#include "EventGuestList.h"
#include "EventProfilePage.h"
#include "EventWidgetFactory.h"
#include "FbRespondGetGroups.h"
#include "GroupProfilePage.h"
#include "ImagesCarouselScreen.h"
#include "Log.h"
#include "MapViewScreen.h"
#include "Popup.h"
#include "PostComposerScreen.h"
#include "ProfileWidgetFactory.h"
#include "ScreenBase.h"
#include "User.h"
#include "WebViewScreen.h"
#include "WidgetFactory.h"

#include <stdlib.h>
#include <sstream>

UIRes * ProfileWidgetFactory::R = UIRes::GetInstance();

#ifdef EVENT_NATIVE_VIEW
static Evas_Object *delete_event_popup = NULL;
#endif

bool ProfileWidgetFactory::mAboutIsShown = false;

ProfileWidgetFactory::ProfileWidgetFactory()
{
    mJoinGroupRequest = NULL;
    mLeaveGroupRequest = NULL;
}

ProfileWidgetFactory::~ProfileWidgetFactory()
{
    FacebookSession::GetInstance()->ReleaseGraphRequest(mJoinGroupRequest);
    FacebookSession::GetInstance()->ReleaseGraphRequest(mLeaveGroupRequest);
}

Evas_Object* ProfileWidgetFactory::CreateProfileItemWrapper(Evas_Object* parent)
{
    Evas_Object *wrapper = elm_layout_add(parent);
    elm_layout_file_set(wrapper, Application::mEdjPath, "profile_wrapper");
    evas_object_size_hint_weight_set(wrapper, 1, 1);
    evas_object_size_hint_align_set(wrapper, -1, 0);
    elm_box_pack_end(parent, wrapper);
    evas_object_show(wrapper);

    Evas_Object *wrapperBox = elm_box_add(wrapper);
    evas_object_size_hint_weight_set(wrapperBox, 1, 0);
    evas_object_size_hint_align_set(wrapperBox, -1, 0);
    elm_layout_content_set(wrapper, "content", wrapperBox);
    elm_box_padding_set(wrapperBox, 0, 0);
    evas_object_show(wrapperBox);

    return wrapperBox;
}

void ProfileWidgetFactory::CreateSingleInfoField(Evas_Object *parent, InfoFieldID fieldId, const char *icon_path, const char *data_1, const char *data_2, ActionAndData *action_data)
{
    Evas_Object *field_item = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(parent, "profile_info_field");

    if (fieldId == FIELD_MEMBER_REQUEST) {
        if (action_data->mData->GetGraphObjectType() == GraphObject::GOT_GROUP) {
            Group *group = static_cast<Group*>(action_data->mData);
            if (group) {
                elm_object_signal_emit(field_item, "show_counter", "");
                std::stringstream temp_str;
                temp_str << (group->mMembersRequestCount);
                std::string str = temp_str.str();
                elm_object_part_text_set(field_item, "counter_text", str.c_str());
            }
        }
    }

    if (fieldId == FIELD_PLACE) {
        elm_object_signal_callback_add(field_item, "mouse,clicked,*", "spacer", on_event_place_ctxmenu_cb, action_data);
    } else if (fieldId == FIELD_TICKETS) {
        // open WebView cb
        elm_object_signal_callback_add(field_item, "mouse,clicked,*", "spacer", on_find_tickets_clicked_cb, action_data);
    } else if (fieldId == FIELD_INVITED) {
        // open UserProfile cb
    } else if (fieldId == FIELD_MEMBER_REQUEST) {
        elm_object_signal_callback_add(field_item, "mouse,clicked,*", "spacer", on_group_member_requests_clicked_cb, action_data);
    }

    Evas_Object *info_box = elm_box_add(field_item);
    elm_object_part_content_set(field_item, "text_field", info_box);
    evas_object_size_hint_weight_set(info_box, 1, 1);
    evas_object_size_hint_align_set(info_box, 0, -1);
    evas_object_show(info_box);

    Evas_Object *spacer = elm_box_add(info_box);
    evas_object_size_hint_weight_set(spacer, 10, 10);
    evas_object_size_hint_align_set(spacer, -1, -1);
    elm_box_pack_end(info_box, spacer);
    evas_object_show(spacer);

    Evas_Object *icon = elm_image_add(field_item);
    elm_object_part_content_set(field_item, "icon", icon);
    elm_image_file_set(icon, icon_path, NULL);
    evas_object_show(icon);

    bool isTime = ( fieldId == FIELD_DATE );

    if (data_1) {
        char *start_time_text = NULL;
        Evas_Object *start_time = elm_label_add(info_box);
        evas_object_size_hint_weight_set(start_time, 0.5, 0.5);
        evas_object_size_hint_align_set(start_time, 0, 0.5);
        elm_label_wrap_width_set(start_time, R->PROFILES_WF_INFOFIELD_WRAP_WIDTH);
        elm_label_line_wrap_set(start_time, ELM_WRAP_WORD);

        char * eventTime = NULL;
        int starts_in_hr = -1;

        if (isTime) {
            eventTime = Utils::GetEventDurationTime(data_1, data_2, &starts_in_hr);
        }
        start_time_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_9298A4_FORMAT,
                                                       R->PROFILES_WF_INFOFIELD_FIRST_FONT_SIZE,
                                                       isTime ? eventTime : data_1);
        if (start_time_text) {
            elm_object_text_set(start_time, start_time_text);
            delete[] start_time_text;
        }

        if (eventTime) {
            free((void*) eventTime);
            eventTime = NULL;
        }

        elm_box_pack_end(info_box, start_time);
        evas_object_show(start_time);

        if ((data_2 == NULL) && (starts_in_hr >= 0)) {
            int len;
            if (starts_in_hr == 0) {
                len = strlen(i18n_get_text("IDS_EVENT_STARTS_WITHIN_HOUR")) + 1;
            } else {
                len = strlen(i18n_get_text("IDS_EVENT_STARTS_IN")) + 4 + strlen(i18n_get_text("IDS_EVENT_HOURS")) + 1;
            }
            char * eventDetails = new char[len];
            if (eventDetails) {
                if (starts_in_hr == 0) {
                    Utils::Snprintf_s(eventDetails, len, "%s", i18n_get_text("IDS_EVENT_STARTS_WITHIN_HOUR"));
                } else {
                    Utils::Snprintf_s(eventDetails, len, "%s %d %s", i18n_get_text("IDS_EVENT_STARTS_IN"), starts_in_hr, i18n_get_text("IDS_EVENT_HOURS"));
                }
                start_time_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_9298A4_FORMAT,
                                                               R->PROFILES_WF_INFOFIELD_SECOND_FONT_SIZE,
                                                               eventDetails);
                delete[] eventDetails;

                if (start_time_text) {
                    Evas_Object *starts_in = elm_label_add(info_box);
                    evas_object_size_hint_weight_set(starts_in, 0.5, 0.5);
                    evas_object_size_hint_align_set(starts_in, 0, 0.5);
                    elm_label_wrap_width_set(starts_in, R->PROFILES_WF_INFOFIELD_WRAP_WIDTH);
                    elm_label_line_wrap_set(starts_in, ELM_WRAP_WORD);

                    elm_object_text_set(starts_in, start_time_text);
                    delete[] start_time_text;

                    elm_box_pack_end(info_box, starts_in);
                    evas_object_show(starts_in);
                }
            }
        }
    }

    if (data_2) {
        char *start_time_text = NULL;
        Evas_Object *start_time = elm_label_add(info_box);
        evas_object_size_hint_weight_set(start_time, 0.5, 0.5);
        evas_object_size_hint_align_set(start_time, 0, 0.5);
        elm_label_wrap_width_set(start_time, R->PROFILES_WF_INFOFIELD_WRAP_WIDTH);
        elm_label_line_wrap_set(start_time, ELM_WRAP_WORD);

        char * eventDetails = NULL;

        if (isTime) {
            eventDetails = Utils::GetEventDetailedDurationTime(data_1, data_2);
        }

        start_time_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_9298A4_FORMAT,
                                                       R->PROFILES_WF_INFOFIELD_SECOND_FONT_SIZE,
                                                       isTime ? eventDetails : data_2);
        if (start_time_text) {
            elm_object_text_set(start_time, start_time_text);
            delete[] start_time_text;
        }

        if (eventDetails) {
            free((void*) eventDetails);
            eventDetails = NULL;
        }

        elm_box_pack_end(info_box, start_time);
        evas_object_show(start_time);
    }

    Evas_Object *spacer2 = elm_box_add(info_box);
    evas_object_size_hint_weight_set(spacer2, 10, 10);
    evas_object_size_hint_align_set(spacer2, -1, -1);
    elm_box_pack_end(info_box, spacer2);
    evas_object_show(spacer2);
}

Evas_Object* ProfileWidgetFactory::CreateLoadingItemWrapper(Evas_Object* parent)
{
    Evas_Object *wrapper = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(parent, "post_wrapper");

    Evas_Object *wrapperBox = elm_box_add(wrapper);
    evas_object_size_hint_weight_set(wrapperBox, 1, 0);
    evas_object_size_hint_align_set(wrapperBox, -1, 0);
    elm_layout_content_set(wrapper, "content", wrapperBox);
    elm_box_padding_set(wrapperBox, 0, 0);

    CreateLoadingItem(wrapperBox, false, true);

    return wrapperBox;
}

Evas_Object *ProfileWidgetFactory::CreateLoadingItem(Evas_Object *parent, bool isWhiteBg, bool isAddToEnd)
{
    Evas_Object *layout = elm_layout_add(parent);
    elm_layout_file_set(layout, Application::mEdjPath, "progress_bar_item_wrapper");
    evas_object_size_hint_weight_set(layout, 1, 1);
    evas_object_size_hint_align_set(layout, -1, -1);
    if (isAddToEnd) {
        elm_box_pack_end(parent, layout);
    } else {
        elm_box_pack_start(parent, layout);
    }
    evas_object_show(layout);

    Evas_Object *progress_bar = elm_progressbar_add(layout);
    elm_object_part_content_set(layout, "progressbar", progress_bar);
    elm_object_style_set(progress_bar, "process_medium");
    evas_object_size_hint_align_set(progress_bar, EVAS_HINT_FILL, EVAS_HINT_FILL);
    evas_object_size_hint_weight_set(progress_bar, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    elm_progressbar_pulse_set(progress_bar, EINA_TRUE);
    elm_progressbar_pulse(progress_bar, EINA_TRUE);
    evas_object_resize(progress_bar, 100, 100);
    evas_object_move(progress_bar, R->SCREEN_BASE_PROGRESS_BAR_ITEM_OBJ_MOVE_X, R->SCREEN_BASE_PROGRESS_BAR_ITEM_OBJ_MOVE_Y);
    evas_object_raise(progress_bar);
    evas_object_show(progress_bar);

    if (isWhiteBg) {
        elm_object_signal_emit(layout, "bg.set", "white");
    }

    return layout;
}

#ifdef EVENT_NATIVE_VIEW
Evas_Object * ProfileWidgetFactory::CreateSimpleProfileStatistic(Evas_Object *box, ActionAndData *actionData)
{
    Evas_Object *stats_item = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(box, "simple_profile_stats");

    UpdateSimpleProfileStatistic(stats_item, static_cast<Event*>(actionData->mData));

    elm_object_translatable_part_text_set(stats_item, "part_one_desc", "IDS_EVENT_PROFILE_GOING");
    elm_object_translatable_part_text_set(stats_item, "part_two_desc", "IDS_EVENT_PROFILE_MAYBE");
    elm_object_translatable_part_text_set(stats_item, "part_three_desc", "IDS_EVENT_PROFILE_INVITED");

    elm_object_signal_callback_add(stats_item, "mouse,clicked,*", "part_one*", on_part_one_clicked_cb, actionData);
    elm_object_signal_callback_add(stats_item, "mouse,clicked,*", "part_two*", on_part_two_clicked_cb, actionData);
    elm_object_signal_callback_add(stats_item, "mouse,clicked,*", "part_three*", on_part_three_clicked_cb, actionData);

    return stats_item;
}

void ProfileWidgetFactory::UpdateSimpleProfileStatistic(Evas_Object *stats_item, Event *event)
{
    if (!stats_item || !event) {
        return;
    }

    if (event->mAttendingCount) {
        int attending = event->mAttendingCount;
        std::stringstream one_str;
        one_str << (attending);
        std::string str = one_str.str();
        const char* cstr2 = str.c_str();
        elm_object_part_text_set(stats_item, "part_one_count", cstr2);
    } else {
        elm_object_part_text_set(stats_item, "part_one_count", "0");
    }

    if (event->mMaybeCount) {
        int attending = event->mMaybeCount;
        std::stringstream two_str;
        two_str << (attending);
        std::string str = two_str.str();
        const char* cstr2 = str.c_str();
        elm_object_part_text_set(stats_item, "part_two_count", cstr2);
    } else {
        elm_object_part_text_set(stats_item, "part_two_count", "0");
    }

    if (event->mInvitedCount) {
        int attending = event->mInvitedCount;
        std::stringstream thr_str;
        thr_str << (attending);
        std::string str = thr_str.str();
        const char* cstr2 = str.c_str();
        elm_object_part_text_set(stats_item, "part_three_count", cstr2);
    } else {
        elm_object_part_text_set(stats_item, "part_three_count", "0");
    }
}
#endif

void ProfileWidgetFactory::CreateCoverItem(Evas_Object *parent, ActionAndData *action_data)
{
    char *cover = NULL;
    char *title = NULL;
    char *first_sub_field = NULL;
    char *second_sub_field = NULL;

    bool isNoData = false;

    if (action_data) {
        assert(action_data->mData);
        switch (action_data->mData->GetGraphObjectType()) {
#ifdef EVENT_NATIVE_VIEW
        case GraphObject::GOT_EVENT: {
            Event *data = static_cast<Event*>(action_data->mData);
            if (data) {
                User *user = static_cast<User*>(data->mOwner);

                if (data->mCover && data->mCover->mSource) {
                    cover = SAFE_STRDUP(data->mCover->mSource);
                }
                if (data->mName) {
                    title = SAFE_STRDUP(data->mName);
                }

                if (data->mPrivacyStatus) {
                    if (!strcmp(data->mPrivacyStatus, "public")) {
                        second_sub_field = strdup(i18n_get_text("IDS_PUBLIC"));
                    } else {
                        second_sub_field = strdup(i18n_get_text("IDS_EVENT_PROFILE_PRIVACY_SECRET"));
                    }
                }

                if (user->mName) {
                    char *name = WidgetFactory::WrapByFormat2("%s %s", i18n_get_text("IDS_EVENT_PROFILE_HOSTED_BY"), user->mName);
                    first_sub_field = SAFE_STRDUP(name);
                    delete[] name;
                }
            }
        }
        break;
#endif
        case GraphObject::GOT_GROUP: {
            Group *data = static_cast<Group*>(action_data->mData);
            if (data->mCover) {
                cover = strdup(data->mCover->mSource);
            }

            if (data->mName) {
                title = strdup(data->mName);
            }

            switch (data->mPrivacy) {
            case Group::eOPEN:
                first_sub_field = strdup(i18n_get_text("IDS_GROUP_PRIVACY_PUBLIC"));
                break;
            case Group::eCLOSED:
                first_sub_field = strdup(i18n_get_text("IDS_GROUP_PRIVACY_CLOSED"));
                break;
            case Group::eSECRET:
                first_sub_field = strdup(i18n_get_text("IDS_GROUP_PRIVACY_SECRET"));
                break;
            }

            if (data->mMembersCount) {
                second_sub_field = gen_members_number_text(data->mMembersCount);
            }
        }
        break;
        default:
            break;
        }
    } else {
        cover = SAFE_STRDUP(ICON_DIR"/No-Profile-Cover.png");
        title = SAFE_STRDUP(i18n_get_text("IDS_NO_DATA_LOADED"));
        first_sub_field = NULL;
        second_sub_field = NULL;
        isNoData = true;
    }

    Evas_Object *cover_item = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(parent, "profile_cover_item");

    if (cover && action_data) {
        action_data->UpdateImageLayoutAsync(cover, cover_item, ActionAndData::EImage, Post::EProfileCover);
        elm_object_signal_callback_add(cover_item, "mouse,clicked,*", "cover_*", on_cover_clicked_cb, action_data);
    }

    WidgetFactory::CreateFriendItemEmulator(cover_item, R->PROFILE_TEXT_STYLE, R->GROUP_NAME_ITEM_WIDTH);
    std::string strName = title;
    int lines = WidgetFactory::GetEntryEmulatorLines(strName, R->GROUP_NAME_ITEM_WIDTH);

    if (lines >= 2) {
        elm_object_signal_emit(cover_item, "2_lines", "");
    }

    Evas_Object *groupName = elm_object_part_content_get(cover_item, "title");
    if (!groupName) {
        groupName = elm_entry_add(cover_item);
        elm_entry_editable_set(groupName, EINA_FALSE);
        FRMTD_TRNSLTD_ENTRY_TXT(groupName, R->PROFILE_TEXT_LTR_STYLE, R->PROFILE_TEXT_RTL_STYLE);
        elm_object_part_content_set(cover_item, "title", groupName);
    }
    elm_entry_entry_set(groupName, strName.c_str());

    WidgetFactory::DeleteFriendItemEmulator();

    Evas_Object *info_box = elm_box_add(cover_item);
    elm_object_part_content_set(cover_item, "sub_title", info_box);
    evas_object_size_hint_weight_set(info_box, 1, 1);
    evas_object_size_hint_align_set(info_box, 0, -1);
    elm_box_horizontal_set(info_box, EINA_TRUE);
    evas_object_show(info_box);

    if (first_sub_field) {
        char *privacy_text = NULL;
        Evas_Object *privacy = elm_label_add(info_box);
        evas_object_size_hint_weight_set(privacy, 0.5, 0.5);
        evas_object_size_hint_align_set(privacy, 0, 0.5);
        privacy_text = WidgetFactory::WrapByFormat2(SANS_MEDIUM_fff_FORMAT, R->PROFILES_WF_COVER_ITEM_SUBTITLE_FONT_SIZE, first_sub_field);
        if (privacy_text) {
            elm_object_text_set(privacy, privacy_text);
            delete[] privacy_text;
        }
        elm_box_pack_end(info_box, privacy);
        evas_object_show(privacy);
    }

    if (!isNoData) {
        Evas_Object *dot = elm_label_add(info_box);
        evas_object_size_hint_weight_set(dot, 0.5, 0.5);
        evas_object_size_hint_align_set(dot, 0, 0.5);
        elm_object_text_set(dot, "<color=#fff>&#xb7;</color>");
        elm_box_pack_end(info_box, dot);
        evas_object_show(dot);
    }

    if (second_sub_field) {
        char *hosted_by_text = NULL;
        Evas_Object *hosted_by = elm_label_add(info_box);
        evas_object_size_hint_weight_set(hosted_by, 0.5, 0.5);
        evas_object_size_hint_align_set(hosted_by, 0, 0.5);
        hosted_by_text = WidgetFactory::WrapByFormat2(SANS_MEDIUM_fff_FORMAT, R->PROFILES_WF_COVER_ITEM_SUBTITLE_FONT_SIZE, second_sub_field);

        if (hosted_by_text) {
            elm_object_text_set(hosted_by, hosted_by_text);
            delete[] hosted_by_text;
        }

        elm_box_pack_end(info_box, hosted_by);
        evas_object_show(hosted_by);
    }

    Evas_Object *spacer = elm_box_add(info_box);
    evas_object_size_hint_weight_set(spacer, 10, 10);
    evas_object_size_hint_align_set(spacer, -1, -1);
    elm_box_pack_end(info_box, spacer);
    evas_object_show(spacer);

    free(cover);
    free(title);
    free(first_sub_field);
    free(second_sub_field);
}

#ifdef EVENT_NATIVE_VIEW
void ProfileWidgetFactory::CreateAboutProfileItem(Evas_Object *parent, ActionAndData *action_data)
{
    Event *event = static_cast<Event*>(action_data->mData);

    Evas_Object *content = CreateProfileItemWrapper(parent);

    Evas_Object *about_item = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(content, "about_profile_item");

    elm_object_translatable_part_text_set(about_item, "header", "IDS_ABOUT");

    char *text = NULL;
    Evas_Object *post_text = elm_label_add(about_item);
    elm_object_part_content_set(about_item, "content", post_text);
    elm_label_wrap_width_set(post_text, R->PROFILES_WF_ABOUT_WRAP_WIDTH);
    elm_label_line_wrap_set(post_text, ELM_WRAP_MIXED);
    std::string str;
    str = Utils::ReplaceString(event->CutDescription(), "\n", "<br/>");
    text = WidgetFactory::WrapByFormat2(SANS_REGULAR_000_FORMAT, R->PROFILES_WF_ABOUT_FONT_SIZE, str.c_str());
    if (text) {
        elm_object_text_set(post_text, text);
        delete[] text;
    }
    evas_object_show(post_text);

    elm_object_signal_callback_add(post_text, "mouse,clicked,*", "*", on_about_text_clicked, event);

    if (strlen(event->CutDescription()) < 125) {
        elm_object_signal_emit(about_item, "hide.pic", "gradient");
    }
}

void ProfileWidgetFactory::on_about_text_clicked(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    Event *event = static_cast<Event*>(data);
    std::string str;
    char *text = NULL;
    Evas_Object *parentWidget = elm_object_parent_widget_get(obj);
    if (mAboutIsShown) {
        str = Utils::ReplaceString(event->CutDescription(), "\n", "<br/>");
        text = WidgetFactory::WrapByFormat2(SANS_REGULAR_000_FORMAT, R->PROFILES_WF_ABOUT_FONT_SIZE, str.c_str());
        if (parentWidget) {
            elm_object_signal_emit(parentWidget, "show.pic", "gradient");
        }
        mAboutIsShown = false;
    } else {
        str = Utils::ReplaceString(event->mDescription, "\n", "<br/>");
        text = WidgetFactory::WrapByFormat2(SANS_REGULAR_000_FORMAT, R->PROFILES_WF_ABOUT_FONT_SIZE, str.c_str());
        if (parentWidget) {
            elm_object_signal_emit(parentWidget, "hide.pic", "gradient");
        }
        mAboutIsShown = true;
    }
    if (text) {
        elm_object_text_set(obj, text);
        delete[] text;
    }
}

Evas_Object * ProfileWidgetFactory::CreateViewAllPostsItem(Evas_Object *parent, const char *id)
{
    Evas_Object *content = CreateProfileItemWrapper(parent);

    Evas_Object *view_all_item = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(content, "view_all_posts_btn_item");

    elm_object_translatable_part_text_set(view_all_item, "item_text", "IDS_VIEW_ALL");

    void *void_id = (void *) id;

    elm_object_signal_callback_add(view_all_item, "mouse,clicked,*", "item_*", on_view_all_posts_clicked_cb, void_id);

    return content;
}
#endif

void ProfileWidgetFactory::CreatePostAndPhotoItem(Evas_Object *parent, ActionAndData *action_data)
{
    Evas_Object *content = CreateProfileItemWrapper(parent);

    Evas_Object *post_photo = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(content, "post_and_photo_btns");

    elm_object_translatable_part_text_set(post_photo, "post_btn_text", "IDS_POST");
    elm_object_translatable_part_text_set(post_photo, "photo_btn_text", "IDS_PROFILE_ME_POST_PHOTO");

    elm_object_signal_callback_add(post_photo, "mouse,clicked,*", "post_btn*", on_post_clicked_cb, action_data);
    elm_object_signal_callback_add(post_photo, "mouse,clicked,*", "photo_btn*", on_photo_post_clicked_cb, action_data);
}

void ProfileWidgetFactory::on_post_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(data);
    PostComposerScreen *screen = NULL;
    if(ActionAndData::IsAlive(action_data) &&
       Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_POST_COMPOSER_SCREEN) {
        assert(action_data->mData);
        switch (action_data->mData->GetGraphObjectType()) {
#ifdef EVENT_NATIVE_VIEW
        case GraphObject::GOT_EVENT: {
            Event *event = static_cast<Event*>(action_data->mData);
            screen = new PostComposerScreen(action_data->mAction->getScreen(), eADD_EVENT_POST,
                    event->GetId(), event->mName, event->mPrivacyStatus);
        }
            break;
#endif
        case GraphObject::GOT_GROUP: {
            Group *group = static_cast<Group*>(action_data->mData);
            screen = new PostComposerScreen(action_data->mAction->getScreen(), eADD_GROUP_POST,
                    group->GetId(), group->mName, group->GetPrivacyAsStr());
        }
            break;
        default:
            return;
        }
    }

    if (screen) {
        Application::GetInstance()->AddScreen(screen);
    }
}

void ProfileWidgetFactory::on_photo_post_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(data);
    if(ActionAndData::IsAlive(action_data) &&
       Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_CAMERA_ROLL) {
        assert(action_data->mData);
        Group *group = static_cast<Group*>(action_data->mData);

        //eADD_GROUP_POST, group)
        CameraRollScreen *screen = new CameraRollScreen(eCAMERA_ROLL_POST_FROM_GROUP_MODE, group);
        Application::GetInstance()->AddScreen(screen);
    }
}

void ProfileWidgetFactory::Create3ActionBtsItem(Evas_Object *parent, ActionAndData *action_data, bool isUsingSavedData)
{
    Evas_Object *mThreeBtnWidget = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(parent, "3_action_btns_item");

    if (action_data) {
        assert(action_data->mData);
        switch (action_data->mData->GetGraphObjectType()) {
#ifdef EVENT_NATIVE_VIEW
        case GraphObject::GOT_EVENT: {
            SetEvent3Actions(mThreeBtnWidget, action_data);
        }
        break;
#endif
        case GraphObject::GOT_GROUP: {
            SetGroup3Actions(mThreeBtnWidget, action_data);
        }
        break;
        default:
            break;
        }
    }

    if (isUsingSavedData) {
        elm_object_signal_emit(mThreeBtnWidget, "lost.connection", "*");
    }
}

void ProfileWidgetFactory::Create4ActionBtsItem(Evas_Object *parent, ActionAndData *action_data, bool isUsingSavedData)
{
    Evas_Object *mFourBtnWidget = WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(parent, "4_action_btns_item");

    if (action_data) {
        assert(action_data->mData);
        switch (action_data->mData->GetGraphObjectType()) {
#ifdef EVENT_NATIVE_VIEW
        case GraphObject::GOT_EVENT: {
            Event *event = static_cast<Event*>(action_data->mData);
            if (event->mRsvpStatus == Event::EVENT_RSVP_NOT_REPLIED) {
                SetEventInviteActions(mFourBtnWidget, action_data);
            } else {
                SetEvent4Actions(mFourBtnWidget, action_data, false);
            }
        }
        break;
#endif
        case GraphObject::GOT_GROUP: {
            SetGroup4Actions(mFourBtnWidget, action_data);
        }
        break;
        default:
            break;
        }
    }

    if (isUsingSavedData) {
        elm_object_signal_emit(mFourBtnWidget, "lost.connection", "*");
    }
}

#ifdef EVENT_NATIVE_VIEW
void ProfileWidgetFactory::SetEvent3Actions(Evas_Object *obj, ActionAndData *action_data)
{
    Event *event = static_cast<Event*>(action_data->mData);

    if (event && obj) {
        action_data->mActionObj = obj;
        /* Btn one */
        if (event->mRsvpStatus == Event::EVENT_RSVP_MAYBE) {
            elm_object_translatable_part_text_set(obj, "btn_one_text", "IDS_EVENT_PROFILE_MAYBE");
            elm_object_signal_emit(obj, "btn.one.icon.maybe", "event");
        } else if (event->mRsvpStatus == Event::EVENT_RSVP_ATTENDING) {
            elm_object_translatable_part_text_set(obj, "btn_one_text", "IDS_EVENT_PROFILE_GOING");
            elm_object_signal_emit(obj, "btn.one.icon.going", "event");
        } else {
            elm_object_translatable_part_text_set(obj, "btn_one_text", "IDS_EVENT_PROFILE_NOT_GOING");
            elm_object_signal_emit(obj, "btn.one.icon.notgoing", "event");
        }

        /* Btn two */
        elm_object_translatable_part_text_set(obj, "btn_two_text", "IDS_POST");
        elm_object_signal_emit(obj, "btn.two.icon", "event");

        /* Callbacks */
        elm_object_signal_callback_add(obj, "mouse,clicked,*", "btn_one*", on_join_clicked_cb, action_data);
        elm_object_signal_callback_add(obj, "mouse,clicked,*", "btn_two*", on_post_clicked_cb, action_data);
        elm_object_signal_callback_add(obj, "mouse,clicked,*", "btn_three*", on_event_ctxmenu_cb, action_data);

        Evas_Object *copyLabel = elm_label_add(obj);
        elm_object_text_set(copyLabel, event->GetLink());
        action_data->mEventObjLabel = copyLabel;
    }
}

void ProfileWidgetFactory::SetEvent4Actions(Evas_Object *obj, ActionAndData *action_data, bool isRefresh)
{
    Event *event = static_cast<Event*>(action_data->mData);
    bool isJoin = false;

    if (isRefresh) {
        elm_object_signal_callback_del(obj, "one.clicked", "btn", ActionBase::elm_on_event_going_btn_clicked_cb);
        elm_object_signal_callback_del(obj, "one.clicked", "btn", on_join_clicked_cb);
    }

    if (event && obj) {
        action_data->mActionObj = obj;

        if (Config::GetInstance().GetUserId() == event->mOwner->GetId()) {
            elm_object_translatable_part_text_set(obj, "btn_one_text", "IDS_EDIT");
            elm_object_signal_emit(obj, "btn.one.icon.edit", "event");
            if (!isRefresh) {
                elm_object_signal_callback_add(obj, "one.clicked", "btn", on_edit_clicked_cb, event);
            }
        } else {
            if (event->mRsvpStatus == Event::EVENT_RSVP_MAYBE) {
                elm_object_translatable_part_text_set(obj, "btn_one_text", "IDS_EVENT_PROFILE_MAYBE");
                elm_object_signal_emit(obj, "btn.one.icon.maybe.active", "event");
            } else if (event->mRsvpStatus == Event::EVENT_RSVP_ATTENDING) {
                elm_object_translatable_part_text_set(obj, "btn_one_text", "IDS_EVENT_PROFILE_GOING");
                elm_object_signal_emit(obj, "btn.one.icon.going.active", "event");
            } else if (event->mRsvpStatus == Event::EVENT_RSVP_DECLINED) {
                elm_object_translatable_part_text_set(obj, "btn_one_text", "IDS_EVENT_PROFILE_NOT_GOING");
                elm_object_signal_emit(obj, "btn.one.icon.not.going.active", "event");
            } else {
                elm_object_translatable_part_text_set(obj, "btn_one_text", "IDS_JOIN");
                elm_object_signal_emit(obj, "btn.one.icon.join", "event");
                isJoin = true;
            }

            if (isJoin) {
                elm_object_signal_callback_add(obj, "one.clicked", "btn", ActionBase::elm_on_event_going_btn_clicked_cb, action_data);
            } else {
                elm_object_signal_callback_add(obj, "one.clicked", "btn", on_join_clicked_cb, action_data);
            }
        }

        /* Btn two */
        elm_object_translatable_part_text_set(obj, "btn_two_text", "IDS_POST");
        elm_object_signal_emit(obj, "btn.two.icon", "event");

        /* Btn three */
        elm_object_translatable_part_text_set(obj, "btn_three_text", "IDS_INVITE");
        elm_object_signal_emit(obj, "btn.three.icon", "event");

        if (!isRefresh) {
            /* Callbacks */
            elm_object_signal_callback_add(obj, "two.clicked", "btn", on_post_clicked_cb, action_data);
            if (event->mPrivacyStatus && !strcmp(event->mPrivacyStatus, "private")) {
                elm_object_signal_callback_add(obj, "three.clicked", "btn", (Edje_Signal_Cb)on_invite_clicked_cb, action_data);
            } else {
                elm_object_signal_callback_add(obj, "three.clicked", "btn", on_event_invite_ctxmenu_cb, action_data);
            }
            elm_object_signal_callback_add(obj, "more.clicked", "btn", on_event_ctxmenu_cb, action_data);

            Evas_Object *copyLabel = elm_label_add(obj);
            elm_object_text_set(copyLabel, event->GetLink());
            action_data->mEventObjLabel = copyLabel;
        }
    }
}

void ProfileWidgetFactory::SetEventInviteActions(Evas_Object *obj, ActionAndData *action_data)
{
    action_data->mActionObj = obj;
    if(obj) {
        elm_object_translatable_part_text_set(obj, "btn_one_text", "IDS_JOIN");
        elm_object_signal_emit(obj, "btn.one.icon.going", "event");

        elm_object_translatable_part_text_set(obj, "btn_two_text", "IDS_EVENT_PROFILE_MAYBE");
        elm_object_signal_emit(obj, "btn.two.icon.maybe", "event");

        elm_object_translatable_part_text_set(obj, "btn_three_text", "IDS_EVENT_PROFILE_DECLINE");
        elm_object_signal_emit(obj, "btn.three.icon.notgoing", "event");

        elm_object_signal_callback_add(obj, "one.clicked", "btn", (Edje_Signal_Cb) ActionBase::on_event_going_btn_clicked_cb, action_data);
        elm_object_signal_callback_add(obj, "two.clicked", "btn", (Edje_Signal_Cb) ActionBase::on_event_maybe_btn_clicked_cb, action_data);
        elm_object_signal_callback_add(obj, "three.clicked", "btn", (Edje_Signal_Cb) ActionBase::on_event_not_going_btn_clicked_cb, action_data);
        elm_object_signal_callback_add(obj, "more.clicked", "btn", on_event_ctxmenu_cb, action_data);
    }
}
#endif

void ProfileWidgetFactory::SetGroup3Actions(Evas_Object *obj, ActionAndData *action_data)
{
    assert(obj);
    assert(action_data);
    assert(action_data->mData);
    Group *group = static_cast<Group *>(action_data->mData);
    if ( group->mMembershipState == Group::eMEMBER || group->mMembershipState == Group::eOWNER ) {
        /* Btn one */
        elm_object_translatable_part_text_set(obj, "btn_one_text", "IDS_JOINED");
        elm_object_signal_emit(obj, "active.one", "btn");
        /* Btn two */
        elm_object_translatable_part_text_set(obj, "btn_two_text", "IDS_ADD_MEMBER");
        elm_object_signal_callback_add(obj, "two.clicked", "btn", (Edje_Signal_Cb)on_invite_clicked_cb, action_data);
    } else if(group->mMembershipState == Group::eREQUESTED) {
        /* Btn one */
        elm_object_translatable_part_text_set(obj, "btn_one_text", "IDS_REQUESTED");
        /* Btn two */
        elm_object_translatable_part_text_set(obj, "btn_two_text", "IDS_SHARE");
        elm_object_signal_emit(obj, Application::IsRTLLanguage() ? "can.share.rtl" : "can.share.ltr", "btn");
        elm_object_signal_callback_add(obj, "two.clicked", "btn", (Edje_Signal_Cb)on_share_clicked_cb, action_data);
    } else {
        /* Btn one */
        elm_object_translatable_part_text_set(obj, "btn_one_text", "IDS_JOIN");
        /* Btn two */
        elm_object_translatable_part_text_set(obj, "btn_two_text", "IDS_SHARE");
        elm_object_signal_emit(obj, Application::IsRTLLanguage() ? "can.share.rtl" : "can.share.ltr", "btn");
        elm_object_signal_callback_add(obj, "two.clicked", "btn", (Edje_Signal_Cb)on_share_clicked_cb, action_data);
    }

    /* Callbacks */
    elm_object_signal_callback_add(obj, "one.clicked", "btn", on_join_clicked_cb, action_data);
    elm_object_signal_callback_add(obj, "more.clicked", "btn", on_group_ctxmenu_cb, action_data);
}

void ProfileWidgetFactory::SetGroup4Actions(Evas_Object *obj, ActionAndData *action_data)
{
    assert(obj);
    assert(action_data);
    assert(action_data->mData);
    Group *group = static_cast<Group*>(action_data->mData);
    if (group && group->mPrivacy == Group::eSECRET) {
        elm_object_signal_emit(obj, Application::IsRTLLanguage() ? "share.disabled.rtl" : "share.disabled.ltr", "btn");
    }

    if ( group->mMembershipState == Group::eMEMBER || group->mMembershipState == Group::eOWNER ) {
        elm_object_translatable_part_text_set(obj, "btn_one_text", "IDS_JOINED");
        elm_object_signal_emit(obj, "active.one", "btn");
    } else {
        elm_object_translatable_part_text_set(obj, "btn_one_text", "IDS_JOIN");
    }

    elm_object_translatable_part_text_set(obj, "btn_two_text", "IDS_ADD_MEMBER");
    elm_object_translatable_part_text_set(obj, "btn_three_text", "IDS_SHARE");

    elm_object_signal_callback_add(obj, "one.clicked", "btn", on_join_clicked_cb, action_data);
    elm_object_signal_callback_add(obj, "two.clicked", "btn", (Edje_Signal_Cb)on_invite_clicked_cb, action_data);
    elm_object_signal_callback_add(obj, "three.clicked", "btn", (Edje_Signal_Cb)on_share_clicked_cb, action_data);
    elm_object_signal_callback_add(obj, "more.clicked", "btn", on_group_ctxmenu_cb, action_data);
}

void ProfileWidgetFactory::on_join_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(data);
    if (ActionAndData::IsAlive(action_data)) {
        assert(action_data->mData);
        switch (action_data->mData->GetGraphObjectType()) {
#ifdef EVENT_NATIVE_VIEW
        case GraphObject::GOT_EVENT: {
            EventWidgetFactory::CreateEventJoinStatusPopup(action_data, obj);
        }
        break;
#endif
        case GraphObject::GOT_GROUP: {
            Group *group = static_cast<Group*>(action_data->mData);
            char targetUrl[MAX_REQ_WEBURL_LEN] = { 0, };
            if(group->mMembershipState == Group::eMEMBER) {
                Utils::Snprintf_s(targetUrl, MAX_REQ_WEBURL_LEN, URL_GROUP_LEAVE, group->GetId());
            } else { // From the Groupinfo web page user can also join/cancel join request
                Utils::Snprintf_s(targetUrl, MAX_REQ_WEBURL_LEN, URL_GROUP_VIEWINFO, group->GetId());
            }
            WebViewScreen::Launch(targetUrl);
        }
        break;
        default:
            break;
        }
    }
}

void ProfileWidgetFactory::on_invite_clicked_cb(void *data, Evas_Object *obj, void *event_info)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(data);
    char targetUrl[MAX_REQ_WEBURL_LEN] = { 0, };

    if (ActionAndData::IsAlive(action_data)) {
        assert(action_data->mData);
        switch (action_data->mData->GetGraphObjectType()) {
#ifdef EVENT_NATIVE_VIEW
        case GraphObject::GOT_EVENT: {
            EventProfilePage *screen = static_cast<EventProfilePage *> (action_data->mAction->getScreen());
            screen->InviteFriends();
        }
        break;
#endif
        case GraphObject::GOT_GROUP: {
            Group *group = static_cast<Group*>(action_data->mData);

            Utils::Snprintf_s(targetUrl, MAX_REQ_WEBURL_LEN, URL_GROUP_INVITE, group->GetId());
            WebViewScreen::Launch(targetUrl);
        }
        break;
        default:
            break;
        }
    }
}

void ProfileWidgetFactory::on_viewinfo_clicked_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    char targetUrl[MAX_REQ_WEBURL_LEN] = { 0, };

    if (ActionAndData::IsAlive(action_data)) {
        assert(action_data->mData);
        switch (action_data->mData->GetGraphObjectType()) {
        case GraphObject::GOT_GROUP: {
            Group *group = static_cast<Group*>(action_data->mData);

            Utils::Snprintf_s(targetUrl, MAX_REQ_WEBURL_LEN, URL_GROUP_VIEWINFO, group->GetId());
            WebViewScreen::Launch(targetUrl);
        }
        break;
        default:
            break;
        }
    }
}

void ProfileWidgetFactory::on_group_member_requests_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    char targetUrl[MAX_REQ_WEBURL_LEN] = { 0, };

    if (ActionAndData::IsAlive(action_data)) {
        assert(action_data->mData);
        switch (action_data->mData->GetGraphObjectType()) {
        case GraphObject::GOT_GROUP: {
            Group *group = static_cast<Group*>(action_data->mData);

            Utils::Snprintf_s(targetUrl, MAX_REQ_WEBURL_LEN, URL_GROUP_MEMBER_REQUEST, group->GetId());
            WebViewScreen::Launch(targetUrl);
        }
        break;
        default:
            break;
        }
    }
}

void ProfileWidgetFactory::on_share_clicked_cb(void *data, Evas_Object *obj, void *event_info)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(data);

    if (ActionAndData::IsAlive(action_data)) {
        assert(action_data->mData);
        switch (action_data->mData->GetGraphObjectType()) {
#ifdef EVENT_NATIVE_VIEW
        case GraphObject::GOT_EVENT: {
            Event *event = static_cast<Event*>(action_data->mData);
            PostComposerScreen* postComposer = new PostComposerScreen(action_data->mAction->getScreen(), eSHARE_EVENT, NULL, event);
            Application::GetInstance()->AddScreen(postComposer);
        }
        break;
#endif
        case GraphObject::GOT_GROUP: {
            Group *groupProf = static_cast<Group *> (action_data->mData);
            std::stringstream uri;
            uri << "https://www.facebook.com/groups/" << groupProf->GetId() << "/";
            PostComposerScreen* postComposer = new PostComposerScreen(action_data->mAction->getScreen(), eSHAREGROUP, groupProf);
            Application::GetInstance()->AddScreen(postComposer);
        }
        break;
        default:
            break;
        }
    }
}

void ProfileWidgetFactory::on_group_ctxmenu_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);

    if (!ActionAndData::IsAlive(action_data)) {
        Log::error("ProfileWidgetFactory::on_group_ctxmenu_cb->ActionAnd data is dead");
        return;
    }

    static PopupMenu::PopupMenuItem context_menu_items[1] =
    {
            {
                    NULL,
                    "IDS_VIEW_GROUP_INFO",
                    NULL,
                    on_viewinfo_clicked_cb,
                    NULL,
                    0,
                    true,
                    false
            },
            // Report Group was marked as "LOW PRIORITY" - https://github.com/fbmp/fb4t/issues/737
            //{
            //    ICON_DIR"/share_in_post.png",
            //    "Report Group",
            //    NULL,
            //    NULL,//on_share_with_post_clicked,
            //    NULL
            //},
    };

    context_menu_items[0].data = action_data;
    //context_menu_items[1].data = action_data;

    Evas_Coord y = 0, h = 0;

    evas_object_geometry_get(obj, NULL, &y, NULL, &h);
    y += h/2;

    assert(action_data->mAction);
    assert(action_data->mAction->getScreen());

    PopupMenu::Show(1, context_menu_items, action_data->mAction->getScreen()->getParentMainLayout(), false, y);
}

#ifdef EVENT_NATIVE_VIEW
void ProfileWidgetFactory::on_edit_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::info(LOG_FACEBOOK_EVENT, "ProfileWidgetFactory::on_edit_clicked_cb");

    if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_EVENT_CREATE_PAGE) {
        Event *event = static_cast<Event*>(user_data);
        if (event) {
            ScreenBase *event_edit_page = new EventCreatePage(event, true);
            Application::GetInstance()->AddScreen(event_edit_page);
        }
    }
}

void ProfileWidgetFactory::on_event_invite_ctxmenu_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::info("ProfileWidgetFactory::on_event_invite_ctxmenu_cb");
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    if (ActionAndData::IsAlive(action_data)) {
        static PopupMenu::PopupMenuItem context_menu_items[2] =
        {
                {
                        NULL,
                        "IDS_INVITE",
                        NULL,
                        on_invite_clicked_cb,
                        NULL,
                        0,
                        false,
                        false
                },
                {
                        NULL,
                        "IDS_SHARE",
                        NULL,
                        on_share_clicked_cb,
                        NULL,
                        1,
                        true,
                        false
                },
         };

        context_menu_items[0].data = action_data;
        context_menu_items[1].data = action_data;

        Evas_Coord y = 0, h = 0;

        evas_object_geometry_get(obj, NULL, &y, NULL, &h);
        y += h/2;

        assert(action_data->mAction);
        assert(action_data->mAction->getScreen());
        PopupMenu::Show(2, context_menu_items, action_data->mAction->getScreen()->getParentMainLayout(), false, y);
    } else {
        Log::error("ProfileWidgetFactory::on_event_invite_ctxmenu_cb->ActionAnd data is dead");
    }
}

void ProfileWidgetFactory::on_part_one_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_EVENT_GUEST) {
        ScreenBase *event_guest_list = new EventGuestList(EVENT_GOING_TAB);
        Application::GetInstance()->AddScreen(event_guest_list);
    }
}

void ProfileWidgetFactory::on_part_two_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{

    if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_EVENT_GUEST) {
        ScreenBase *event_guest_list = new EventGuestList(EVENT_MAYBE_TAB);
        Application::GetInstance()->AddScreen(event_guest_list);
    }
}

void ProfileWidgetFactory::on_part_three_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_EVENT_GUEST) {
        ScreenBase *event_guest_list = new EventGuestList(EVENT_INVITED_TAB);
        Application::GetInstance()->AddScreen(event_guest_list);
    }
}

void ProfileWidgetFactory::on_event_ctxmenu_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(data);
    if (!ActionAndData::IsAlive(action_data)) {
        Log::info(LOG_FACEBOOK_EVENT, "ProfileWidgetFactory::on_event_ctxmenu_cb->ActionAnd data is dead");
        return;
    }
    assert(action_data->mData);
    Event *event = static_cast<Event *>(action_data->mData);
    if (event) {
        if (Utils::IsMe(event->mOwner->GetId())) {
            const int menuItemsCount = 3;
            static PopupMenu::PopupMenuItem context_menu_items[menuItemsCount] = {
                    { NULL, "IDS_DELETE_EVENT", NULL, on_delete_click, NULL, 0, false, false },
                    { NULL, "IDS_COPY_EVENT_LINK", NULL, on_copy_click, NULL, 1, false, false },
                    { NULL, "IDS_PROFILE_CREATE_EVENT", NULL, on_create_event_click, NULL, 2, true, false },
            };

            context_menu_items[0].data = action_data;
            context_menu_items[1].data = action_data;
            context_menu_items[2].data = action_data;

            Evas_Coord y = 0, h = 0;

            evas_object_geometry_get(obj, NULL, &y, NULL, &h);
            y += h / 2;

            PopupMenu::Show(menuItemsCount, context_menu_items, action_data->mAction->getScreen()->getParentMainLayout(), false, y);
        }
        else
        {
            static PopupMenu::PopupMenuItem context_menu_items[2] = {
                { NULL, "IDS_COPY_EVENT_LINK", NULL, on_copy_click, NULL, 0, false, false },
                { NULL, "IDS_PROFILE_CREATE_EVENT", NULL, on_create_event_click, NULL, 1, true, false },
            };
            context_menu_items[0].data = action_data;
            context_menu_items[1].data = action_data;

            Evas_Coord y = 0, h = 0;

            evas_object_geometry_get(obj, NULL, &y, NULL, &h);
            y += h / 2;

            PopupMenu::Show(2, context_menu_items, action_data->mAction->getScreen()->getParentMainLayout(), false, y);
        }
    }
}

void ProfileWidgetFactory::on_copy_click(void *data, Evas_Object *obj, void *event_info)
{
    ActionAndData *action_data = static_cast<ActionAndData *>(data);
    Event *event = static_cast<Event *>(action_data->mData);
    elm_cnp_selection_set(action_data->mEventObjLabel, ELM_SEL_TYPE_CLIPBOARD, ELM_SEL_FORMAT_TEXT, event->GetLink(), strlen(event->GetLink()));
}

void ProfileWidgetFactory::on_create_event_click(void *data, Evas_Object *obj, void *event_info)
{
    Log::info(LOG_FACEBOOK_EVENT, "EventWidgetFactory::on_create_clicked");
    if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_EVENT_CREATE_PAGE) {
        EventCreatePage *event_create_screen = new EventCreatePage(NULL, false);
        Application::GetInstance()->AddScreen(event_create_screen);
    }
}

void ProfileWidgetFactory::on_delete_click(void *data, Evas_Object *obj, void *event_info)
{
    ActionAndData *action_data = static_cast<ActionAndData *>(data);

    delete_event_popup = elm_popup_add(action_data->mAction->getScreen()->getMainLayout());
    elm_popup_align_set(delete_event_popup, ELM_NOTIFY_ALIGN_FILL, 0.5);

    evas_object_smart_callback_add(delete_event_popup, "block,clicked", popup_cancel_delete_cb, NULL);
    Evas_Object *keepDiscard = elm_layout_add(delete_event_popup);
    elm_layout_file_set(keepDiscard, Application::mEdjPath, "post_composer_keep_discard_popup");
    elm_object_content_set(delete_event_popup, keepDiscard);

    elm_object_translatable_part_text_set(keepDiscard, "top_text", "IDS_ASK_DELETE_EVENT");
    elm_object_translatable_part_text_set(keepDiscard, "description", "IDS_DELETE_EVENT_DESCRIPTION");
    elm_object_translatable_part_text_set(keepDiscard, "text.keep", "IDS_NO");
    elm_object_translatable_part_text_set(keepDiscard, "text.discard", "IDS_YES");

    elm_object_signal_callback_add(keepDiscard, "mouse,clicked,*", "keep", (Edje_Signal_Cb)popup_cancel_delete_cb, NULL);
    elm_object_signal_callback_add(keepDiscard, "mouse,clicked,*", "discard", (Edje_Signal_Cb)popup_approved_delete_cb, action_data);

    evas_object_show(keepDiscard);
    evas_object_show(delete_event_popup);
}

void ProfileWidgetFactory::popup_approved_delete_cb(void *data, Evas_Object *obj, void *event_info)
{
    CloseEventPopup();

    ActionAndData *action_data = static_cast<ActionAndData*>(data);
    assert(action_data->mAction);
    action_data->mAction->OnCtxMenuApprovedDeleteEventClicked(data);
}

void ProfileWidgetFactory::popup_cancel_delete_cb(void *data, Evas_Object *obj, void *event_info)
{
    CloseEventPopup();
}

bool ProfileWidgetFactory::CloseEventPopup()
{
    bool result = false;

    if(delete_event_popup){
        evas_object_del(delete_event_popup);
        delete_event_popup = NULL;
        result = true;
    }
    return result;
}

void ProfileWidgetFactory::on_view_all_posts_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source)
{
    if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_EVENT_ALL_POSTS_SCREEN) {
        const char *id = static_cast<const char*>(data);
        EventAllPostsScreen* allPostsScreen = new EventAllPostsScreen(id);
        Application::GetInstance()->AddScreen(allPostsScreen);
    }
}
#endif

void ProfileWidgetFactory::TransparentWidget(ActionAndData *action_data) {
    if (action_data && action_data->mActionObj) {
        if (!ConnectivityManager::Singleton().IsConnected()) {
            elm_object_signal_emit(action_data->mActionObj, "lost.connection", "*");
        } else {
            elm_object_signal_emit(action_data->mActionObj, "recovered.connection", "*");
        }
    }
}

void ProfileWidgetFactory::on_cover_clicked_cb(void *data, Evas_Object *obj, const char *source, const char *emission)
{
    Log::info("ProfileWidgetFactory::on_cover_clicked_cb");
    ActionAndData *actionData = static_cast<ActionAndData*>(data);
    if (ActionAndData::IsAlive(actionData)) {
        switch (actionData->mData->GetGraphObjectType()) {
#ifdef EVENT_NATIVE_VIEW
        case GraphObject::GOT_EVENT: {
            Event *event = static_cast<Event*>(actionData->mData);
            if (event && event->mCover && event->mCover->mPhotoPath && event->mCover->GetId()) {
                ImagesCarouselScreen * carouselScreen = new ImagesCarouselScreen(event->mCover->mPhotoPath, event->mCover->GetId(), ImagesCarouselScreen::ePROFILE_COVER_MODE);
                Application::GetInstance()->AddScreen(carouselScreen);
            }
        }
        break;
#endif
        case GraphObject::GOT_GROUP: {
            Group *group = static_cast<Group*>(actionData->mData);
            if (group && group->mCover && group->mCover->mPhotoPath && group->mCover->GetId()) {
                ImagesCarouselScreen * carouselScreen = new ImagesCarouselScreen(group->mCover->mPhotoPath, group->mCover->GetId(), ImagesCarouselScreen::ePROFILE_COVER_MODE);
                Application::GetInstance()->AddScreen(carouselScreen);
            }
        }
        break;
        default:
            break;
        }
    }
}

void ProfileWidgetFactory::on_event_place_ctxmenu_cb(void *data, Evas_Object *obj, const char *emission, const char *source) {
    Log::info("ProfileWidgetFactory::on_event_place_ctxmenu_cb");
    ActionAndData *action_data = static_cast<ActionAndData*>(data);
    if (ActionAndData::IsAlive(action_data)) {
        assert(action_data->mData);
        Event *event = static_cast<Event *> (action_data->mData);
        const char *address = NULL;
        if (event && event->mPlace && event->mPlace->mLocation) {
            address = event->mPlace->mLocation->GetAddress().get();
        }

        static PopupMenu::PopupMenuItem context_menu_items[3] =
        {
                { NULL, NULL, NULL, on_view_page_selected_cb, NULL, 0, false, false },
                { NULL, NULL, NULL, on_open_maps_selected_cb, NULL, 1, false, false },
                { NULL, NULL, NULL, on_copy_location_selected_cb, NULL, 2, true, false }
        };

        context_menu_items[0].itemText = i18n_get_text("IDS_EVENT_LOCATION_VIEW_PAGE");
        context_menu_items[0].data = action_data;
        context_menu_items[1].itemText = i18n_get_text("IDS_EVENT_LOCATION_OPEN_MAPS");
        context_menu_items[1].data = action_data;
        context_menu_items[2].itemText = (address ? "IDS_EVENT_LOCATION_COPY_ADDRESS" : "IDS_EVENT_LOCATION_COPY_LOCATION");
        context_menu_items[2].data = action_data;
        delete[] address;

        Evas_Coord y = 0, h = 0;

        evas_object_geometry_get(obj, NULL, &y, NULL, &h);
        y += h/2;

        PopupMenu::Show(3, context_menu_items, action_data->mAction->getScreen()->getParentMainLayout(), false, y);
    }
}

void ProfileWidgetFactory::on_view_page_selected_cb(void *data, Evas_Object *obj, void *event_info) {
    if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_WEB_VIEW) {
        ActionAndData *action_data = static_cast<ActionAndData*>(data);
        if (action_data) {
            assert(action_data->mData);
            Event *event = static_cast<Event *> (action_data->mData);
            if (event && event->mPlace && event->mPlace->GetId()) {
                char url[MAX_REQ_WEBURL_LEN] = {0};
                Utils::Snprintf_s(url, MAX_REQ_WEBURL_LEN, "m.facebook.com/%s", event->mPlace->GetId());
                WebViewScreen::Launch(url, true, true);
           }
        }
    }
}

void ProfileWidgetFactory::on_open_maps_selected_cb(void *data, Evas_Object *obj, void *event_info) {
    if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_MAP_VIEW) {
        MapViewScreen* mapViewScreen = new MapViewScreen(static_cast<ActionAndData *> (data));
        Application::GetInstance()->AddScreen(mapViewScreen);
    }
}

void ProfileWidgetFactory::on_copy_location_selected_cb(void *data, Evas_Object *obj, void *event_info) {
    assert(data);
    ActionAndData *action_data = static_cast<ActionAndData *>(data);
    assert(action_data->mData);
    Event *event = static_cast<Event *>(action_data->mData);

    const char *textToCopy = NULL;
    const char *address = NULL;
    if (event->mPlace && event->mPlace->mLocation) {
        address = event->mPlace->mLocation->GetAddress().get();
        textToCopy = address;
    }
    if (!textToCopy && event->mPlace && event->mPlace->mName) {
        textToCopy = event->mPlace->mName;
    }
    if (textToCopy) {
        elm_cnp_selection_set(action_data->mEventObjLabel, ELM_SEL_TYPE_CLIPBOARD, ELM_SEL_FORMAT_TEXT, textToCopy, strlen(textToCopy));
        delete [] address;
    }
}

void ProfileWidgetFactory::on_find_tickets_clicked_cb(void *data, Evas_Object *obj, const char *emission, const char *source) {
    if(Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_WEB_VIEW) {
        ActionAndData *actionData = static_cast<ActionAndData *> (data);
        if (ActionAndData::IsAlive(actionData)) {
            assert(actionData->mData);
            Event *event = static_cast<Event *>(actionData->mData);
            if ( event->GetTicketURI() ) {
                WebViewScreen::Launch(event->GetTicketURI());
            }
        }
    }
}

char *ProfileWidgetFactory::gen_members_number_text(unsigned int number) {
    const char *members = number > 1 ? i18n_get_text("IDS_GROUP_PROFILE_MEMBERS") : i18n_get_text("IDS_GROUP_PROFILE_MEMBER");
    unsigned int len = strlen(members) + 32;
    char *text = (char*) malloc(len);
    Utils::Snprintf_s(text, len, "%d %s", number, members);
    return text;
}


