#include "ActionBase.h"
#include "Common.h"
#include "Config.h"
#include "ConnectivityManager.h"
#include "EditAction.h"
#include "ErrorHandling.h"
#include "Event.h"
#include "FriendOperation.h"
#include "Log.h"
#include "OperationPresenter.h"
#include "OwnFriendsProvider.h"
#include "ParserWrapper.h"
#include "Photo.h"
#include "PhotoPostOperation.h"
#include "PostPresenter.h"
#include "PresenterHelpers.h"
#include "SimplePostOperation.h"
#include "StoryAttachment.h"
#include "UIRes.h"
#include "Utils.h"
#include "ViewEditHistoryScreen.h"
#include "WidgetFactory.h"

#include <assert.h>
#include <metadata_extractor.h>
#include <string.h>


static Evas_Object *screen_popup = NULL;

#define CONTEXT_MENU_ITEMS_MAX 3
#define SHARE_MENU_ITEMS_NUM 2
#define CONTEXT_MENU_MORE_ITEMS_NUM 2
#define CONTEXT_MENU_OWNER_MORE_ITEMS_NUM_WITH_HISTORY 3 //turn to 4 when notifications work

UIRes * WidgetFactory::R = UIRes::GetInstance();
Evas_Object * WidgetFactory::mErrorPopup = nullptr;
Evas_Object * WidgetFactory::mEntryEmulator = nullptr;
ConfirmationDialog * WidgetFactory::mConfirmationDialog = nullptr;

PopupMenu::PopupMenuItem WidgetFactory::mMenuItemDelete = {ICON_DIR "/delete.png", "IDS_DELETE", NULL, popup_delete_post_cb, NULL, 0, false, false};
PopupMenu::PopupMenuItem WidgetFactory::mMenuItemEdit = {ICON_DIR "/edit.png", "IDS_EDIT_POST", NULL, ctxmenu_edit_post_option_clicked_cb, NULL, 0, false, false};
PopupMenu::PopupMenuItem WidgetFactory::mMenuItemMore = {ICON_DIR "/more_options.png", "IDS_MORE_OPTIONS", NULL, ctxmenu_more_options_owner_cb, NULL, 0, false, false};
PopupMenu::PopupMenuItem WidgetFactory::mMenuItemEditPrivacy = {ICON_DIR "/privacy_public_medium.png", "IDS_EDIT_PRIVACY", NULL, ctxmenu_edit_privacy_option_clicked_cb, NULL, 0, false, false};

PopupMenu::PopupMenuItem WidgetFactory::mMenuItemBack = {ICON_DIR "/back.png", "IDS_BACK_CTX_MENU", NULL, on_ctxmenu_back_item_cb, NULL, 0, false, false};
PopupMenu::PopupMenuItem WidgetFactory::mMenuItemViewEditHistory = {ICON_DIR "/edit_history.png", "IDS_VIEW_EDIT_HISTORY", NULL, ctxmenu_show_edit_history_clicked_cb, NULL, 0, false, false};


const char *LTRformatEllipsis = "DEFAULT='font_size=28 wrap=word ellipsis=1.0 align=left left_margin=0 right_margin=0 text_class=tizen'";
const char *RTLformatEllipsis = "DEFAULT='font_size=28 wrap=word ellipsis=1.0 align=right left_margin=0 right_margin=0 text_class=tizen'";

char* WidgetFactory::WrapByFormat(const char *formatPattern, const char *content)
{
    char *res = NULL;
    if (formatPattern && content && *formatPattern) {
        int len = strlen(formatPattern) + strlen(content) + 1;
        res = new char[len];
        if (res) {
            int len2 = Utils::Snprintf_s(res, len, formatPattern, content);
            res[len2] = '\0';
        }
    }
    return res;
}

char* WidgetFactory::WrapByFormat2(const char *formatPattern, const char *contentPart1, const char *contentPart2)
{
    char *res = NULL;
    if (formatPattern && contentPart1 && contentPart2 && *formatPattern) {
        int len = strlen(formatPattern) + strlen(contentPart1) + strlen(contentPart2) + 1;
        res = new char[len];
        if (res) {
            int len2 = Utils::Snprintf_s(res, len, formatPattern, contentPart1, contentPart2);
            res[len2] = '\0';
        }
    }
    return res;
}

char* WidgetFactory::WrapByFormat2(const char *formatPattern, int contentPart1, int contentPart2)
{
    char *res = NULL;
    if (formatPattern) {
        // 20 is for the number of digits in int in 64 bit system (for the future)
        int len = strlen(formatPattern) + 20 + 20 + 1;
        res = new char[len];
        if (res) {
            int len2 = Utils::Snprintf_s(res, len, formatPattern, contentPart1, contentPart2);
            res[len2] = '\0';
        }
    }
    return res;
}

char* WidgetFactory::WrapByFormat3(const char *formatPattern, const char *contentPart1,
        const char *contentPart2, const char *contentPart3)
{
    char *res = NULL;
    if (formatPattern && contentPart1 && contentPart2 && contentPart3 && *formatPattern) {
        int len = strlen(formatPattern) + strlen(contentPart1) + strlen(contentPart2) + strlen(contentPart3) + 1;
        res = new char[len];
        if (res) {
            int len2 = Utils::Snprintf_s(res, len, formatPattern, contentPart1, contentPart2, contentPart3);
            res[len2] = '\0';
        }
    }
    return res;
}

char* WidgetFactory::WrapByFormat4(const char *formatPattern, const char *contentPart1,
        const char *contentPart2, const char *contentPart3, const char *contentPart4) {
    char *res = NULL;
    if (formatPattern && contentPart1 && contentPart2 && contentPart3 && contentPart4 && *formatPattern) {
        int len = strlen(formatPattern) + strlen(contentPart1) + strlen(contentPart2) +
                  strlen(contentPart3) + strlen(contentPart4) + 1;
        res = new char[len];
        if (res) {
            int len2 = Utils::Snprintf_s(res, len, formatPattern, contentPart1, contentPart2, contentPart3, contentPart4);
            res[len2] = '\0';
        }
    }
    return res;
}

char* WidgetFactory::WrapByFormat5(const char *formatPattern, const char *contentPart1,
        const char *contentPart2, const char *contentPart3, const char *contentPart4, const char *contentPart5) {
    char *res = NULL;
    if (formatPattern && contentPart1 && contentPart2 && contentPart3 && contentPart4 && contentPart5 && *formatPattern) {
        int len = strlen(formatPattern) + strlen(contentPart1) + strlen(contentPart2) +
                  strlen(contentPart3) + strlen(contentPart4) + strlen(contentPart5) + 1;
        res = new char[len];
        if (res) {
            int len2 = Utils::Snprintf_s(res, len, formatPattern, contentPart1, contentPart2, contentPart3, contentPart4, contentPart5);
            res[len2] = '\0';
        }
    }
    return res;
}
/**
 * @brief This callback is called when Share button is clicked
 * @param user_data [in] - user data passed in callback
 * @param obj [in] - object which generates click event
 * @param event_info [in] - event information
 */
void WidgetFactory::on_share_btn_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    if(obj) {
        ActionAndData *action_data = static_cast<ActionAndData*>(user_data);

        static PopupMenu::PopupMenuItem share_menu_items[SHARE_MENU_ITEMS_NUM] =
        {
            {
                ICON_DIR"/reshare.png",
                "IDS_SHARE_NOW",
                "IDS_INSTANTLY_SHARE",
                on_share_now_clicked,
                NULL,
                0,
                false,
                false
            },
            {
                ICON_DIR"/share_in_post_grey.png",
                "IDS_WRITE_POST",
                NULL,
                on_share_with_post_clicked,
                NULL,
                1,
                true,
                false
            },
        };

        share_menu_items[0].data = action_data;
        share_menu_items[1].data = action_data;

        Evas_Coord y = 0, h = 0;

        evas_object_geometry_get(obj, NULL, &y, NULL, &h);
        y += h/2;

       PopupMenu::Show(SHARE_MENU_ITEMS_NUM, share_menu_items,
               action_data->mAction->getScreen()->getParentMainLayout(), false, y);
    }
}

/**
 * @brief This callback is called share menu item is selected
 * @param data [in] - user data passed in callback
 * @param obj [in] - object which generates click event
 * @param event_info [in] - event information
 */
void WidgetFactory::on_share_now_clicked(void *data, Evas_Object *obj, void *event_info)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(data);
    action_data->mAction->on_share_now_clicked(data, obj, event_info);
}

/**
 * @brief This callback is called share menu item is selected
 * @param data [in] - user data passed in callback
 * @param obj [in] - object which generates click event
 * @param event_info [in] - event information
 */
void WidgetFactory::on_share_with_post_clicked(void *data, Evas_Object *obj, void *event_info)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(data);
    action_data->mAction->on_share_with_post_clicked(data, obj, event_info);
}

/**
 * @brief This callback is called share menu item is selected
 * @param data [in] - user data passed in callback
 * @param obj [in] - object which generates click event
 * @param event_info [in] - event information
 */
void WidgetFactory::ctxmenu_edit_post_option_clicked_cb(void *data, Evas_Object *obj, void *event_info)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(data);
    action_data->mAction->on_ctxmenu_edit_post_option_clicked_cb(data, obj, event_info);
}

void WidgetFactory::ctxmenu_show_edit_history_clicked_cb(void *data, Evas_Object *obj, void *event_info)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(data);

    if (action_data && action_data->mData && action_data->mData->GetId()) {
        ViewEditHistoryScreen *screen = new ViewEditHistoryScreen(action_data->mData->GetId());
        if (screen) {
            Application::GetInstance()->AddScreen(screen);
        }
    }
}


/**
 * @brief This callback is called share menu item is selected
 * @param data [in] - user data passed in callback
 * @param obj [in] - object which generates click event
 * @param event_info [in] - event information
 */
void WidgetFactory::ctxmenu_edit_privacy_option_clicked_cb(void *data, Evas_Object *obj, void *event_info)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(data);
    action_data->mAction->on_ctxmenu_edit_privacy_option_clicked_cb(data, obj, event_info);
}

/**
 * @brief Show post item context menu
 * @param action_data [in] - action and data that belongs post item
 * @param obj [in] - object which is used for calculation y position of menu
 * @param useOldPosition [in] - indicate that should be used previous y position of popup menu
 */
void WidgetFactory::ShowContextMenu(ActionAndData *action_data, Evas_Object *obj, bool useOldPosition) {
    assert(action_data);

    static PopupMenu::PopupMenuItem context_menu_items[CONTEXT_MENU_ITEMS_MAX];

    Post * post = static_cast<Post*>(action_data->mData);
    assert(post);

    bool userIsPostOwner = Config::GetInstance().GetUserId() == post->GetFromId();
    bool undeletablePost = post->IsProfilePhotoPost() || post->IsCoverPhotoPost();

    Evas_Coord y = 0, h = 0;
    if(obj) {
        evas_object_geometry_get(obj, NULL, &y, NULL, &h);
        y += h/2;
    }

    if (userIsPostOwner) {
        int ctx_menu_items_num = 0;
        if (!undeletablePost) {
            SetMenuItem(context_menu_items, mMenuItemDelete, 0, 0, false, action_data);
            bool isNotPostPhotoToAlbumOrItIsButEdited = !post->IsPhotoAddedToAlbum() || post->IsEdited(); //For "Photo added to album" posts we need "delete" and "edit". But if the post is edited then we also need "more" for "view edit history".
            if (!post->IsPostWithTarget()) {
                if(isNotPostPhotoToAlbumOrItIsButEdited) {
                    ctx_menu_items_num = 3;
                    SetMenuItem(context_menu_items, mMenuItemEdit, 1, 1, false, action_data);
                    if(!post->IsPhotoAddedToAlbum()) {
                        if(post->IsEdited()) {
                            SetMenuItem(context_menu_items, mMenuItemMore, 2, 2, true, action_data);
                        } else {
                            SetMenuItem(context_menu_items, mMenuItemEditPrivacy, 2, 2, true, action_data);
                        }
                    } else {
                        SetMenuItem(context_menu_items, mMenuItemViewEditHistory, 2, 2, true, action_data);
                    }
                } else {
                    ctx_menu_items_num = 2;
                    SetMenuItem(context_menu_items, mMenuItemEdit, 1, 1, true, action_data);
                }
            } else {// groups and other users timelines
                SetMenuItem(context_menu_items, mMenuItemEdit, 1, 1, !post->IsEdited(), action_data);
                if(!post->IsEdited()) {
                    ctx_menu_items_num = 2;
                } else {
                    ctx_menu_items_num = 3;
                    SetMenuItem(context_menu_items, mMenuItemViewEditHistory, 2, 2, true, action_data);
                }
            }
        } else {
            SetMenuItem(context_menu_items, mMenuItemEdit, 0, 0, !post->IsEdited(), action_data);
            if(!post->IsEdited()) {
                ctx_menu_items_num = 1;
            } else {
                ctx_menu_items_num = 2;
                SetMenuItem(context_menu_items, mMenuItemViewEditHistory, 1, 1, true, action_data);
            }
        }
        PopupMenu::Show(ctx_menu_items_num, context_menu_items,
                action_data->mAction->getScreen()->getParentMainLayout(), useOldPosition, y);
    }
}

void WidgetFactory::ShowCtxtMenu(ActionAndData *action_data, Evas_Object *obj, bool useOldPosition) {
    assert(action_data);
    Post * post = static_cast<Post*>(action_data->mData);
    assert(post);

    const char *name = strdup(post->GetIsGroupPost() ? post->GetToName() : post->GetFromName().c_str());

    static PopupMenu::PopupMenuItem context_menu_items[] =
    {
        {
            ICON_DIR "/unfollow.png",
            "IDS_UNFOLLOW",
            NULL,
            NULL,
            NULL,
            0,
            true,
            false
        },
    };
    context_menu_items[0].callback = ActionBase::on_unfollow_user_clicked_cb;

    Evas_Coord y = 0, h = 0;
    if (obj) {
        evas_object_geometry_get(obj, NULL, &y, NULL, &h);
        y += h / 2;
    }

    int ctx_menu_items_num = sizeof(context_menu_items) / sizeof(context_menu_items[0]);
    context_menu_items[0].data = action_data;
    context_menu_items[0].itemSubText = name;

    PopupMenu::Show(ctx_menu_items_num, context_menu_items, action_data->mAction->getScreen()->getParentMainLayout(), useOldPosition, y);
    free((void*) name);
}


//Context menu clicked callback
void WidgetFactory::on_ctxmenu_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    WidgetFactory::ShowContextMenu(action_data, obj, false);
}

//Context back item callback function
void WidgetFactory::on_ctxmenu_back_item_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    WidgetFactory::ShowContextMenu(action_data, obj, true);
}

void WidgetFactory::on_context_menu_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    WidgetFactory::ShowCtxtMenu(action_data, obj, false);
}

//Context menu for more options
void WidgetFactory::ctxmenu_more_options_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);

    static PopupMenu::PopupMenuItem context_menu_more_items[CONTEXT_MENU_MORE_ITEMS_NUM] =
    {
        {
            ICON_DIR "back.png",
            "IDS_BACK_CTX_MENU",
            NULL,
            on_ctxmenu_back_item_cb,
            NULL,
            0,
            false,
            false
        },
        {
            ICON_DIR "/notifications.png",
            "IDS_TURN_OFF_NOTIFICATIONS",
            NULL,
            ctxmenu_item_select_cb,
            NULL,
            1,
            true,
            false
        },
    };

    context_menu_more_items[0].data = action_data;
    context_menu_more_items[1].data = action_data;

    PopupMenu::Close();
    PopupMenu::ShowMore(CONTEXT_MENU_MORE_ITEMS_NUM, context_menu_more_items,
            action_data->mAction->getScreen()->getParentMainLayout());
}

//Context menu for more options owner
void WidgetFactory::ctxmenu_more_options_owner_cb(void *user_data, Evas_Object *obj, void *event_info) {
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);

    if (action_data) {
        Post *post = static_cast<Post*>(action_data->mData);
        assert(post);

        static PopupMenu::PopupMenuItem context_menu_items[CONTEXT_MENU_ITEMS_MAX];

        SetMenuItem(context_menu_items, mMenuItemBack, 0, 0, false, action_data);
        SetMenuItem(context_menu_items, mMenuItemEditPrivacy, 1, 1, false, action_data);
        SetMenuItem(context_menu_items, mMenuItemViewEditHistory, 2, 2, true, action_data);
        PopupMenu::Close();
        PopupMenu::ShowMore(CONTEXT_MENU_OWNER_MORE_ITEMS_NUM_WITH_HISTORY, context_menu_items, action_data->mAction->getScreen()->getParentMainLayout());
    }
}

void WidgetFactory::SetMenuItem(PopupMenu::PopupMenuItem *items, PopupMenu::PopupMenuItem &item, int index, int id, bool isLast, ActionAndData *data) {
    items[index] = item;
    items[index].itemId = id;
    items[index].itemLastItem = isLast;
    items[index].data = data;
}

/**
 * @brief Closes popup menu
 * @return true if menu is closed
 */
bool WidgetFactory::close_popup_menu()
{
    return PopupMenu::Close();
}

//Temporary
void WidgetFactory::ctxmenu_item_select_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    //nothing here
}

//Delete post button clicked in context menu
void WidgetFactory::popup_delete_post_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    CHECK_RET_NRV(action_data);
    CHECK_RET_NRV(action_data->mAction);
    CHECK_RET_NRV(action_data->mAction->getScreen());
    ScreenBase *scr = action_data->mAction->getScreen();
    CHECK_RET_NRV(scr);
    if (ConnectivityManager::Singleton().IsConnected()) {
        mConfirmationDialog = ConfirmationDialog::CreateAndShow(scr->getMainLayout(),
                                                                "IDS_DELETE_POST_CONFIRMATION_TITLE",
                                                                "IDS_DELETE_POST_CONFIRMATION",
                                                                "IDS_CANCEL",
                                                                "IDS_DELETE",
                                                                confirmation_dialog_cb,
                                                                action_data);

    } else {
        Utils::ShowToast(scr->getMainLayout(), i18n_get_text("IDS_PROBLEM_DELETING_STORY"));
    }
}

void WidgetFactory::confirmation_dialog_cb(void *user_data, ConfirmationDialog::CDEventType event) {
    CloseConfirmDialogue();

    switch (event) {
    case ConfirmationDialog::ECDYesPressed:
        break;
    case ConfirmationDialog::ECDNoPressed:
        popup_approved_delete_cb(user_data, nullptr, nullptr, nullptr);
        break;
    case ConfirmationDialog::ECDDismiss:
        break;
    default:
        break;
    }
}

void WidgetFactory::ShowAlertPopup(const char* Message, const char* Title, Evas_Object* parent) {
    if(screen_popup)
    {
        CloseAlertPopup();
    }

    screen_popup = elm_popup_add(parent ? parent : Application::GetInstance()->mNf);
    elm_popup_align_set(screen_popup, ELM_NOTIFY_ALIGN_FILL, 0.5);

    evas_object_smart_callback_add(screen_popup, "block,clicked", popup_hide_cb, nullptr);
    Evas_Object *btnOk = elm_layout_add(screen_popup);
    if (Title && strlen(Title)) {
        elm_layout_file_set(btnOk, Application::mEdjPath, "screen_popup");
        elm_object_translatable_part_text_set(btnOk, "title_text_popup", Title);
    } else {
        elm_layout_file_set(btnOk, Application::mEdjPath, "screen_popup_nocaption");
    }

    elm_object_content_set(screen_popup, btnOk);

    elm_object_translatable_part_text_set(btnOk, "description_popup", Message);
    elm_object_translatable_part_text_set(btnOk, "btn_ok_text", "IDS_OK");
    elm_object_signal_callback_add(btnOk, "mouse,clicked,*", "*", popup_ok_cb, nullptr);

    evas_object_show(btnOk);
    evas_object_show(screen_popup);
}

void WidgetFactory::popup_hide_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    if (obj) {
        CloseAlertPopup();
    }
}

//Cancel deleting post
void WidgetFactory::popup_ok_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    if (obj) {
        CloseAlertPopup();
    }
}

 /**
  * @brief Closes confirm dialogue
  * @return true if dialogue is closed
  */
 bool WidgetFactory::CloseConfirmDialogue()
 {
    bool res = mConfirmationDialog;
    if (res) {
        delete mConfirmationDialog;
        mConfirmationDialog = nullptr;
    }
    return res;
 }

//Delete post approved
void WidgetFactory::popup_approved_delete_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    action_data->mAction->on_ctxmenu_approved_detete_post_clicked_cb(user_data, obj, emission, source);
}

//Creating title
std::string WidgetFactory::CreateTitle(PostItemTitleType titleContent, PresentableAsPost& post) {
    PostPresenter presenter(post);
    switch (titleContent) {
    case eNAME: {
        std::unique_ptr<char[]> wrappedName(WidgetFactory::WrapByFormat2(SANS_MEDIUM_000_FORMAT, R->NF_PH_TITLE_TEXT_SIZE, presenter.GetFromName().c_str()));
        return wrappedName ? wrappedName.get() : "";
    }
    case eSTORY:
        return presenter.GetStory();
    case eLOCATION:
        return presenter.GetLocationBox();
    case eVIA:
        return presenter.GetViaName();
    case eTo:
        return presenter.GetToStory();
    case eCOMPOSED_HEADER:
        std::unique_ptr<char[]> wrappedName(WidgetFactory::WrapByFormat2(SANS_MEDIUM_000_FORMAT, R->NF_PH_TITLE_TEXT_SIZE, presenter.ComposeHeaderFromNameTagsAndLocation().c_str()));
        return wrappedName ? wrappedName.get() : "";
    }
    return "";
}

Evas_Object *WidgetFactory::CreateDot(Evas_Object *parent, Evas_Object *afterWhat)
{
    Evas_Object *dot = elm_label_add(parent);
    evas_object_size_hint_weight_set(dot, 0.5, 0.5);
    evas_object_size_hint_align_set(dot, 0, 0.5);
    elm_object_text_set(dot, "<color=#78839a>&#xb7;</color>");
    elm_box_pack_after(parent, dot, afterWhat);
    evas_object_show(dot);
    return dot;
}

Evas_Object* WidgetFactory::CreatePrivacyImage(Evas_Object* parent, PresentableAsPost& post) {
    Evas_Object *privacy_icon = elm_image_add(parent);
    CHECK_RET(privacy_icon, nullptr);
    evas_object_size_hint_weight_set(privacy_icon, R->NF_PRIVACY_ICON_WEIGHT_BEFORE, R->NF_PRIVACY_ICON_WEIGHT_BEFORE);
    evas_object_size_hint_align_set(privacy_icon, 0, 0.5);
    evas_object_size_hint_min_set(privacy_icon, R->NF_PRIVACY_ICON_SIZE, R->NF_PRIVACY_ICON_SIZE);
    evas_object_size_hint_max_set(privacy_icon, R->NF_PRIVACY_ICON_SIZE, R->NF_PRIVACY_ICON_SIZE);
    if (!post.GetPrivacyIcon().empty()) { // workaround in case of no privacy due to Github #804
        ELM_IMAGE_FILE_SET(privacy_icon, post.GetPrivacyIcon().c_str(), NULL);
    }
    elm_box_pack_end(parent, privacy_icon);
    evas_object_show(privacy_icon);
    return privacy_icon;
}

Evas_Object *WidgetFactory::CreatePrivacySpacer(Evas_Object* parent)
{
    Evas_Object *spacer = elm_box_add(parent);
    if(spacer) {
        evas_object_size_hint_weight_set(spacer, R->NF_PRIVACY_ICON_WEIGHT_AFTER, R->NF_PRIVACY_ICON_WEIGHT_AFTER);
        evas_object_size_hint_align_set(spacer, -1, -1);
        elm_box_pack_end(parent, spacer);
        evas_object_show(spacer);
    }
    return spacer;
}

Evas_Object *WidgetFactory::CreatePostInfoBox(Evas_Object* parent) {
    Evas_Object* info_box = elm_box_add(parent);
    CHECK_RET(info_box, nullptr);
    elm_object_part_content_set(parent, "info", info_box);
    evas_object_size_hint_weight_set(info_box, 1, 1);
    evas_object_size_hint_align_set(info_box, 0, -1);
    elm_box_horizontal_set(info_box, EINA_TRUE);
    evas_object_show(info_box);
    return info_box;
}

Evas_Object *WidgetFactory::CreatePostInfoDate(Evas_Object* parent, double time)
{
    Evas_Object *info_date = elm_label_add(parent);
    if(info_date) {
        evas_object_size_hint_weight_set(info_date, 0.5, 0.5);
        evas_object_size_hint_align_set(info_date, 0, 0.5);
        DATE_TRNSLTD_PART_TEXT_SET(info_date, nullptr, R->NF_PH_DATE_TEXT_FORMAT, time);
        evas_object_show(info_date);
        elm_box_pack_end(parent, info_date);
    }
    return info_date;
}
/* This is not via in title, just for applications like Instagram */
Evas_Object *WidgetFactory::CreateAppLabel(Evas_Object* parent, PresentableAsPost& post, Evas_Object *afterWhat, int pxAvailableWidth) {
    Evas_Object *app_label = elm_label_add(parent);
    if (app_label) {
        evas_object_size_hint_weight_set(app_label, 0.5, 0.5);
        evas_object_size_hint_align_set(app_label, 0, 0.5);
        PostPresenter presenter(post);
        std::string appNameStr = presenter.GetApplicationName();
        const char *appName = appNameStr.c_str();
        if (*appName){
            const char *appText = WidgetFactory::WrapByFormat2(FEED_ITEM_DESCRIPTION_FORMAT_APP, i18n_get_text("IDS_VIA"), appName);
            elm_object_text_set(app_label, appText);
            delete[] appText;
        }
        elm_box_pack_after(parent, app_label, afterWhat);
        if (MeasureLabelWidth(app_label) > pxAvailableWidth) {
            elm_label_wrap_width_set(app_label, pxAvailableWidth);
            elm_label_ellipsis_set(app_label, EINA_TRUE);
            elm_label_line_wrap_set(app_label, ELM_WRAP_CHAR);
        }
        evas_object_show(app_label);
    }
    return app_label;
}

Evas_Object *WidgetFactory::CreateImplicitPlaceLabel(Evas_Object* parent, PresentableAsPost& post, Evas_Object *afterWhat, int pxAvailableWidth) {
    Evas_Object *imp_label = elm_label_add(parent);
    if (imp_label) {
        evas_object_size_hint_weight_set(imp_label, 0.5, 0.5);
        evas_object_size_hint_align_set(imp_label, 0, 0.5);
        std::string impPlaceNameStr = post.GetImplicitLocationName();
        const char *impPlaceName = impPlaceNameStr.c_str();
        if (*impPlaceName) {
            const char *impPlaceText = WidgetFactory::WrapByFormat(FEED_ITEM_DESCRIPTION_FORMAT, impPlaceName);
            elm_object_text_set(imp_label, impPlaceText);
            delete[] impPlaceText;
        }
        elm_box_pack_after(parent, imp_label, afterWhat);
        if (MeasureLabelWidth(imp_label) > pxAvailableWidth) {
            elm_label_wrap_width_set(imp_label, pxAvailableWidth);
            elm_label_ellipsis_set(imp_label, EINA_TRUE);
            elm_label_line_wrap_set(imp_label, ELM_WRAP_CHAR);
        }
        evas_object_show(imp_label);
    }
    return imp_label;
}

Evas_Object *WidgetFactory::CreateEditedLabel(Evas_Object* parent, Evas_Object *afterWhat) {
    Evas_Object *edited_label = elm_label_add(parent);
    if(edited_label) {
        evas_object_size_hint_weight_set(edited_label, 0.5, 0.5);
        evas_object_size_hint_align_set(edited_label, 0, 0.5);
        FRMTD_TRNSLTD_TXT_SET2(edited_label, SANS_MEDIUM_9197a3_FORMAT, R->NF_PH_DATE_TEXT_SIZE, "IDS_EDITED");
        elm_box_pack_after(parent, edited_label, afterWhat);
        evas_object_show(edited_label);
    }
    return edited_label;
}
Evas_Object *WidgetFactory::CreateStatusPost(Evas_Object *parent, void *user_data, RenderType renderType)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    Evas_Object* wrapWidget = NULL;

    WidgetFactory::CreatePostBox(parent, &wrapWidget, action_data, renderType);
    if (wrapWidget) {
        evas_object_show(wrapWidget);
    } else {
        Log::error("WidgetFactory::CreateStatusPost->Can't create widget");
    }
    return wrapWidget;
}

void WidgetFactory::SetPostActive(ActionAndData *actionAndData, bool active) {
    assert(actionAndData);
    if (actionAndData->mParentWidget) {
        if (active) {
            elm_layout_signal_emit(actionAndData->mParentWidget, "activate_post", "");
        } else {
            elm_layout_signal_emit(actionAndData->mParentWidget, "deactivate_post", "");
        }
    }
}


Evas_Object* WidgetFactory::CreatePostItemWrapper(Evas_Object* parent, Evas_Object** wrapWidget, RenderType renderType)
{
    Evas_Object *wrapper = elm_layout_add(parent);
    if(wrapWidget) {
        *wrapWidget = wrapper;
    }
    elm_layout_file_set(wrapper, Application::mEdjPath, "post_wrapper");
    evas_object_size_hint_weight_set(wrapper, 1, 1);
    evas_object_size_hint_align_set(wrapper, -1, 0);

    Evas_Object *wrapperBox = elm_box_add(wrapper);
    evas_object_size_hint_weight_set(wrapperBox, 1, 0);
    evas_object_size_hint_align_set(wrapperBox, -1, 0);
    elm_layout_content_set(wrapper, "content", wrapperBox);
    elm_box_padding_set(wrapperBox, 0, 0);

    return wrapperBox;
}

Evas_Object* WidgetFactory::CreatePostInPostItemWrapper(Evas_Object* parent)
{
    Evas_Object *wrapper = elm_layout_add(parent);
    Evas_Object *wrapperBox = NULL;
    if(wrapper) {
        elm_layout_file_set(wrapper, Application::mEdjPath, "post_in_post_wrapper");
        evas_object_size_hint_weight_set(wrapper, 1, 1);
        evas_object_size_hint_align_set(wrapper, -1, 0);
        elm_box_pack_end(parent, wrapper);
        evas_object_show(wrapper);

        wrapperBox = elm_box_add(wrapper);
        evas_object_size_hint_weight_set(wrapperBox, 1, 0);
        evas_object_size_hint_align_set(wrapperBox, -1, 0);
        elm_layout_content_set(wrapper, "content", wrapperBox);
        elm_box_padding_set(wrapperBox, 0, 0);
    }

    return wrapperBox;
}

Evas_Object* WidgetFactory::CreateGreyWrapper(Evas_Object* parent)
{
    Evas_Object *wrapper = elm_layout_add(parent);
    elm_layout_file_set(wrapper, Application::mEdjPath, "grey_wrapper");
    evas_object_size_hint_weight_set(wrapper, 1, 1);
    evas_object_size_hint_align_set(wrapper, -1, 0);
    elm_box_pack_end(parent, wrapper);
    evas_object_show(wrapper);

    Evas_Object *wrapperBox = elm_box_add(wrapper);
    elm_layout_content_set(wrapper, "content", wrapperBox);

    return wrapperBox;
}

Evas_Object* WidgetFactory::CreateCommentWrapper(Evas_Object* parent)
{
    Evas_Object *wrapper = elm_layout_add(parent);
    elm_layout_file_set(wrapper, Application::mEdjPath, "comment_box_wrapper");
    evas_object_size_hint_weight_set(wrapper, 1, 1);
    evas_object_size_hint_align_set(wrapper, -1, 0);
    elm_box_pack_end(parent, wrapper);
    evas_object_show(wrapper);

    Evas_Object *wrapperBox = elm_box_add(wrapper);
    elm_layout_content_set(wrapper, "content", wrapperBox);

    return wrapperBox;
}

void WidgetFactory::PostHeaderCustomization(Evas_Object *content, ActionAndData *action_data, bool addContextMenu)
{
    assert(content);
    if( !(action_data  && action_data->mData) ) {
        Log::error("WidgetFactory::PostHeaderCustomization: ActionAndData invalid, can't extract Post data");
        return;
    }
    if( !(action_data->mAction && action_data->mAction->getScreen()) )
    {
        Log::error("WidgetFactory::PostHeaderCustomization: ActionAndData->ActionBase invalid, can't extract Screen info");
        return;
    }

    Post* post = static_cast<Post*>(action_data->mData);

    switch (action_data->mAction->getScreen()->GetScreenId()) {
    case ScreenBase::SID_USER_PROFILE:
    case ScreenBase::SID_GROUP_PROFILE_PAGE:
    /*case ScreenBase::SID_EVENT_PROFILE_PAGE:
    case ScreenBase::SID_EVENT_ALL_POSTS_SCREEN:*/ {
        if(!post->GetStory().empty()) {
            const char* postStatusType = post->GetStatusType();
            if(postStatusType && !strcmp(postStatusType, "created_note")) {
                AddPostItemHeader(content, action_data, eNAME, addContextMenu);
            } else {
                AddPostItemHeader(content, action_data, eSTORY, addContextMenu);
            }
        } else {
            AddPostItemHeader(content, action_data, eCOMPOSED_HEADER, addContextMenu);//generate header with: name, tags, location
        }
    }
        break;
    // Please Add other Cases before this case, as this Case does not have Break Flow.
    case ScreenBase::SID_NOTIFICATION_SCREEN:
    {
        PostRelatedTo postRelatedTo = post->getPostRelatedTo();
        if( (EEvents == postRelatedTo) || (EGroups == postRelatedTo) ) {
            AddPostItemHeader(content, action_data, eTo, addContextMenu);
            // Intentionally moved break inside the If loop. So else part is covered by continuing to execute the Default case. Thus avoiding duplication of code.
            break;
        }
    }
    default: {
        if(!post->GetStory().empty()) {
            if (post->GetStatusType() && !strcmp(post->GetStatusType(), "created_note")) {
                AddPostItemHeader(content, action_data, eNAME, addContextMenu);
            } else {
                AddPostItemHeader(content, action_data, eSTORY, addContextMenu);
            }
        } else if (post->GetVia() && !post->GetToId()) {
            AddPostItemHeader(content, action_data, eVIA, addContextMenu);
        } else if (post->GetToId()) {
            if (post->IsPostWithTarget()) {
                AddPostItemHeader(content, action_data, eTo, addContextMenu);
            } else {
                AddPostItemHeader(content, action_data, eNAME, addContextMenu);
            }
        } else {
            AddPostItemHeader(content, action_data, eNAME, addContextMenu);
        }
    }
        break;
    }
}

void WidgetFactory::PostInPostHeaderCustomization(Evas_Object *content, ActionAndData *action_data)
{
    assert(content);
    if( !(action_data  && action_data->mData) ) {
        Log::error("WidgetFactory::PostInPostHeaderCustomization: ActionAndData invalid, can't extract Post data");
        return;
    }
    if( !(action_data->mAction && action_data->mAction->getScreen()) )
    {
        Log::error("WidgetFactory::PostInPostHeaderCustomization: ActionAndData->ActionBase invalid, can't extract Screen info");
        return;
    }

    Post* post = static_cast<Post*>(action_data->mData);

    switch (action_data->mAction->getScreen()->GetScreenId()) {
    case ScreenBase::SID_USER_PROFILE:
    case ScreenBase::SID_GROUP_PROFILE_PAGE:
    /*case ScreenBase::SID_EVENT_PROFILE_PAGE:
    case ScreenBase::SID_EVENT_ALL_POSTS_SCREEN:*/ {
        if (!post->GetStory().empty()) {
            if (post->GetStatusType() && !strcmp(post->GetStatusType(), "created_note")) {
                AddPostInPostItemHeader(content, action_data, eNAME, false);
            } else {
                AddPostInPostItemHeader(content, action_data, eSTORY, false);
            }
        } else {
            AddPostItemHeader(content, action_data, eCOMPOSED_HEADER, false);//generate header with: name, tags, location
        }
    }
        break;
    default: {
        if (!post->GetStory().empty()) {
            if (post->GetStatusType() && !strcmp(post->GetStatusType(), "created_note")) {
                AddPostInPostItemHeader(content, action_data, eNAME, false);
            } else {
                AddPostInPostItemHeader(content, action_data, eSTORY, false);
            }
        } else if (post->GetVia() && !post->GetToId()) {
            AddPostInPostItemHeader(content, action_data, eVIA, false);
        } else if (post->GetToId()) {
            if (post->IsPostWithTarget()) {
                AddPostInPostItemHeader(content, action_data, eTo, false);
            } else {
                AddPostInPostItemHeader(content, action_data, eNAME, false);
            }
        } else {
            AddPostInPostItemHeader(content, action_data, eNAME, false);
        }
    }
        break;
    }
}

void WidgetFactory::PostContentCustomization(Evas_Object *content, ActionAndData *action_data)
{
    Post *post = static_cast<Post*>(action_data->mData);
    if (post->GetAttachmentTargetId() && !action_data->IsNeedToDrawAttachment(post->GetAttachmentTargetId())) {
        return;
    }

    if (post->GetStatusType() && !strcmp(post->GetStatusType(), "shared_story")) {
        Evas_Object *box_post = CreateGreyWrapper(content);
        if (post->GetType() && !strcmp(post->GetType(), "event")) {
            CreateEventItemInfo(box_post, action_data);
        } else if (!post->GetParentId()) {
            CreateLinkItem(box_post, action_data);
        }
        post->SetIsSharedPost(true);
    } else if (post->GetType() && !strcmp(post->GetType(), "event")) {
        Evas_Object *box_event = CreateGreyWrapper(content);
        CreateEventItemInfo(box_event, action_data);
    } else if (!post->GetStory().empty() &&  post->GetStory().find("shared a group") != std::string::npos &&
               post->GetStoryTagsList().size() && post->GetStatusType() && !strcmp(post->GetStatusType(), "mobile_status_update") &&
               post->GetType() && !strcmp(post->GetType(), "status")) {

        Evas_Object *box_group = CreateGreyWrapper(content);
        AddSharedGroupCover(box_group, action_data);
        CreatePageItem(box_group, action_data);
        post->SetIsSharedPost(true);
    } else {
        if (post->GetFullPicture() && !post->GetParentId()) {//known cases
            if (post->GetStatusType() && !strcmp(post->GetStatusType(), "added_video")) {
                CreateVideoItem(content, action_data);
            } else if (post->GetStatusType() && !strcmp(post->GetStatusType(), "published_story")) {
                CreateLinkItem(CreateGreyWrapper(content), action_data);
            } else if (post->GetType() && !strcmp(post->GetType(), "video")) {
                CreateVideoItem(content, action_data);
            } else if (post->GetType() && !strcmp(post->GetType(), "link")) {
                CreateLinkItem(CreateGreyWrapper(content), action_data);
                post->SetIsSharedPost(true);
            } else {
                CreatePhotoItem(content, action_data);
            }
        } else if (post->GetPlace()) {
            Evas_Object *box_post = CreateGreyWrapper(content);
            CreateLocationItem(box_post, action_data);
            CreateLocationInfoItem(box_post, action_data);
        } else if (post->GetType() && (!strcmp(post->GetType(), "status") || !strcmp(post->GetType(), "link")) &&
                   !post->GetAttachmentTitle().empty()) {
            CreateLinkItem(CreateGreyWrapper(content), action_data);
        }
    }
}

void WidgetFactory::AddPostItemHeader(Evas_Object* parent,
                                      ActionAndData* action_data,
                                      PostItemTitleType titleContent,
                                      bool addContextMenu) {
    CHECK_RET_NRV(parent);
    CHECK_RET_NRV(action_data);
    CHECK_RET_NRV(action_data->mData);
    CHECK_RET_NRV(action_data->mData->GetGraphObjectType() == GraphObject::GOT_POST ||
                  action_data->mData->GetGraphObjectType() == GraphObject::GOT_OPERATION_PRESENTER);
    PresentableAsPost* post = static_cast<PresentableAsPost*>(action_data->mData);

    Evas_Object *item_header = elm_layout_add(parent);
    action_data->mHeader = item_header;
    elm_layout_file_set(item_header, Application::mEdjPath, "post_header");
    elm_box_pack_end(parent, item_header);
    evas_object_show(item_header);

    Post* thisPost = dynamic_cast<Post*>(action_data->mData);
    if(thisPost && thisPost->GetFromPicturePath()) {
        Utils::SetImageToEvasAndFill(item_header, "avatar", thisPost->GetFromPicturePath());
    } else if (!post->GetFromPicture().empty()) {
        Evas_Object *avatar = nullptr;
        avatar = elm_image_add(item_header);
        elm_object_part_content_set(item_header, "avatar", avatar);
        action_data->UpdateImageLayoutAsync(post->GetFromPicture().c_str(), avatar, ActionAndData::EImage, Post::EFromPicturePath);
        evas_object_smart_callback_add(avatar, "clicked", ActionBase::on_avatar_clicked_cb, action_data);
        evas_object_show(avatar);
    }

    Evas_Object *title = elm_label_add(item_header);
    elm_layout_content_set(item_header, "user_name", title);
    evas_object_smart_callback_add(item_header, "clicked", ActionBase::on_avatar_clicked_cb, action_data);
    TO_STORY_TRNSLTD_TEXT_SET(title, titleContent, *post);
    elm_label_wrap_width_set(title, R->NF_PH_TITLE_WRAP_WIDTH);
    evas_object_smart_callback_add(title, "anchor,clicked", ActionBase::on_anchor_clicked_cb, action_data);
    elm_label_line_wrap_set(title, ELM_WRAP_MIXED);
    evas_object_show(title);

    Evas_Object* info_box = CreatePostInfoBox(item_header);
    action_data->mInfoBoxWidget = info_box;
    RefreshPostInfoBox(*post, info_box, R->NF_PH_INFOBOX_WIDTH);

    // TODO find better solution when #1906 is answered
    std::string story(post->GetStory());
    bool createdAnEventPost = story.find("created event") != std::string::npos ||
                              story.find("created an event") != std::string::npos ||
                              story.find("created a private event") != std::string::npos;

    ScreenBase *screen = static_cast<ScreenBase*>(action_data->mAction->getScreen());

    if (screen->GetScreenId() != ScreenBase::SID_HOME_SCREEN) {
        if (!Utils::IsMe(post->GetFromId().c_str()) || !addContextMenu || createdAnEventPost || post->IsPhotoPost() ||
            (thisPost && thisPost->IsCreatedGroupPost())
           ) {
            elm_object_signal_emit(item_header, "context.hide", "context");
        }
    } else {
        if (!addContextMenu || (Utils::IsMe(post->GetFromId().c_str()) && post->IsPhotoPost())) {
            elm_object_signal_emit(item_header, "context.hide", "context");
        }
    }
    if(post->IsInteractive()) {
        if(Utils::IsMe(post->GetFromId().c_str())) {
            elm_object_signal_callback_add(item_header, "context.open", "", (Edje_Signal_Cb) on_ctxmenu_cb, action_data);
        } else {
            elm_object_signal_callback_add(item_header, "context.open", "", (Edje_Signal_Cb) on_context_menu_cb, action_data);
        }
        elm_object_signal_callback_add(item_header, "post.comment.open", "", action_data->mAction->on_comment_btn_clicked_cb, action_data);
    } else {
        elm_object_signal_emit(item_header, "opacity.true", "");
    }
}

void WidgetFactory::AddPostInPostItemHeader(Evas_Object* parent, ActionAndData *action_data,
        PostItemTitleType titleContent, bool addContextMenu)
{
    Post *post = static_cast<Post*> (action_data->mData);

    Evas_Object *item_header = elm_layout_add(parent);
    evas_object_size_hint_weight_set(item_header, 1, 1);
    evas_object_size_hint_align_set(item_header, 0, -1);
    elm_layout_file_set(item_header, Application::mEdjPath, "post_in_post_header");
    elm_box_pack_end(parent, item_header);
    evas_object_show(item_header);

    Evas_Object *avatar = elm_image_add(item_header);
    elm_object_part_content_set(item_header, "avatar", avatar);
    action_data->UpdateImageLayoutAsync(post->GetFromPictureLink(), avatar, ActionAndData::EImage, Post::EFromPicturePath);
    evas_object_smart_callback_add(avatar, "clicked", ActionBase::on_avatar_clicked_cb, action_data);
    evas_object_show(avatar);

    Evas_Object *title = elm_label_add(item_header);
    elm_layout_content_set(item_header, "user_name", title);
    TO_STORY_TRNSLTD_TEXT_SET(title, titleContent, *post);
    evas_object_smart_callback_add(title, "anchor,clicked", ActionBase::on_anchor_clicked_cb, action_data);
    elm_label_wrap_width_set(title, R->NF_PIPH_TITLE_WRAP_WIDTH);
    elm_label_line_wrap_set(title, ELM_WRAP_MIXED);
    evas_object_show(title);

    Evas_Object* info_box = CreatePostInfoBox(item_header);
    action_data->mInfoBoxWidget = info_box;
    RefreshPostInfoBox(*post, info_box, R->NF_PIPH_INFOBOX_WIDTH);

    //Check if this post belongs to the owner
    bool userIsPostOwner = Config::GetInstance().GetUserId() == post->GetFromId();

    //Check if this post belongs to friends of current user
    bool isPostOwnerFriend = OwnFriendsProvider::GetInstance()->IsFriend(post->GetFromId().c_str());

    if (userIsPostOwner || isPostOwnerFriend) {
        elm_object_signal_emit(item_header, "hide_add_friend", "add_friend");
    }

    elm_object_signal_callback_add(item_header, "post.comment.open", "", action_data->mAction->on_comment_btn_clicked_cb, action_data);
    //elm_object_signal_callback_add(item_header, "add.clicked", "btn", action_data->mAction->on_add_friend_clicked_cb, action_data);
}

void WidgetFactory::AddPostItemBumpHeader(Evas_Object* parent, ActionAndData *action_data,
        PostItemTitleType titleContent)
{
    if(parent) {
        Post *post = static_cast<Post*> (action_data->mData);

        Evas_Object *bump_header = elm_layout_add(parent);
        elm_layout_file_set(bump_header, Application::mEdjPath, "bump_header");
        evas_object_size_hint_weight_set(bump_header, 1, 1);
        elm_box_pack_end(parent, bump_header);
        evas_object_show(bump_header);

        if (Utils::IsMe(post->GetFromId().c_str()) && !post->IsPhotoPost()) {
            elm_object_signal_emit(bump_header, "context.show", "context");
        }

        elm_object_signal_callback_add(bump_header, "context.open", "", (Edje_Signal_Cb) on_ctxmenu_cb, action_data);

        Evas_Object *title = elm_label_add(bump_header);
        TO_STORY_TRNSLTD_TEXT_SET(title, titleContent, *post);
        evas_object_smart_callback_add(title, "anchor,clicked", ActionBase::on_anchor_clicked_cb, action_data);
        elm_label_wrap_width_set(title, R->NF_BH_TITLE_WRAP_WIDTH);
        elm_label_line_wrap_set(title, ELM_WRAP_MIXED);
        elm_layout_content_set(bump_header, "title", title);
        evas_object_show(title);
    }
}

//Creating stats footer for post
void WidgetFactory::AddPostStatsItemFooter(Evas_Object* parent, ActionAndData *action_data)
{
    if(parent) {
        Evas_Object *item_footer = elm_layout_add(parent);
        evas_object_size_hint_weight_set(item_footer, 1, 1);
        elm_layout_file_set(item_footer, Application::mEdjPath, "stats_footer");
        elm_box_pack_end(parent, item_footer);
        evas_object_show(item_footer);
        elm_object_signal_callback_add(item_footer, "stat.clicked", "stat", action_data->mAction->on_stats_footer_clicked_cb, action_data);

        action_data->mLikesCommentsWidget = item_footer;

        ecore_job_add(refresh_item_footer, action_data);
    }
}

void WidgetFactory::refresh_item_footer(void* data) {
    ActionAndData *actionAndData = static_cast<ActionAndData*>(data);
    if (ActionAndData::IsAlive(actionAndData)) { //will do nothing if actionAndData is already killed.
        RefreshPostItemFooter(actionAndData);
    } else {
        Log::error("WidgetFactory::refresh_item_footer: ActionAndData object isn't alive anymore.");
    }
}

int WidgetFactory::MeasureLabelWidth(Evas_Object *label) {
    static Evas_Object *emulatedEntry = nullptr;

    if (!label) {
        if (emulatedEntry) {
            evas_object_del(emulatedEntry);
            emulatedEntry = nullptr;
        }
        return -1;
    }

    if (!emulatedEntry) {
        emulatedEntry = elm_entry_add(elm_layout_edje_get(label));
        evas_object_hide(emulatedEntry);
        elm_entry_single_line_set(emulatedEntry, true);
        evas_object_size_hint_weight_set(emulatedEntry, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
        elm_entry_text_style_user_push(emulatedEntry, FEED_ITEM_DESCRIPTION_TEXT_STYLE);
    }

    const char *labelText = elm_object_text_get(label);
    elm_entry_entry_set(emulatedEntry, labelText);

    Evas_Coord w = 0, h = 0;
    evas_object_textblock_size_native_get(elm_entry_textblock_get(emulatedEntry), &w, &h);
    return w;
}

void WidgetFactory::RefreshPostInfoBox(PresentableAsPost& post, Evas_Object* infoBoxWidget, int pxAvailableWidth) {
    CHECK_RET_NRV(infoBoxWidget);
    elm_box_clear(infoBoxWidget);
    PostPresenter presenter(post);

    int pxDot = 0;  //width of a dot in pixels - measure it once and use up to three times

    // Date/time is always present in the post's infobox.
    Evas_Object *eoDateLabel = CreatePostInfoDate(infoBoxWidget, post.GetCreationTime());
    pxAvailableWidth -= MeasureLabelWidth(eoDateLabel);

    // Privacy information is always present in the post's infobox.
    Evas_Object *eoDotPrivacy = CreateDot(infoBoxWidget, eoDateLabel);
    if (eoDotPrivacy) {
        pxDot= MeasureLabelWidth(eoDotPrivacy);
    }
    CreatePrivacyImage(infoBoxWidget, post);
    CreatePrivacySpacer(infoBoxWidget);
    pxAvailableWidth -= (pxDot + R->NF_PRIVACY_ICON_SIZE);

    if (post.IsEdited()) {
        Evas_Object *eoEditedLabel = CreateEditedLabel(infoBoxWidget, eoDateLabel);
        if (eoEditedLabel) {
            pxAvailableWidth -= MeasureLabelWidth(eoEditedLabel);
        }
        CreateDot(infoBoxWidget, eoDateLabel);
        pxAvailableWidth -= pxDot;
    }

    if (!post.GetApplicationName().empty()) {
        CreateAppLabel(infoBoxWidget, post, eoDateLabel, pxAvailableWidth);
    } else if (post.GetStory().empty() && !post.GetImplicitLocationName().empty() && !post.IsEdited()) {
        pxAvailableWidth -= pxDot;
        CreateImplicitPlaceLabel(infoBoxWidget, post, eoDateLabel, pxAvailableWidth);
        CreateDot(infoBoxWidget, eoDateLabel);
    }

    MeasureLabelWidth(NULL);
}

void WidgetFactory::RefreshPostStatsItemFooter(ActionAndData *action_data)
{
    elm_object_signal_callback_del(action_data->mLikesCommentsWidget, "mouse,clicked,*", "rect_spacer", action_data->mAction->on_comment_btn_clicked_cb);
    CHECK_RET_NRV(action_data);
    if (action_data->mData && action_data->mData->GetGraphObjectType() == GraphObject::GOT_POST &&
        action_data->mLikesCommentsWidget) {
        Post * post = static_cast<Post *>(action_data->mData);
        if (post && (post->GetLikesCount() || post->GetCommentsCount())) {
            PostPresenter presenter(*post);
            HRSTC_TRNSLTD_PART_TEXT_SET(action_data->mLikesCommentsWidget, "stat_info", presenter.GetLikesAndCommentsStats().c_str());
            elm_object_signal_emit(action_data->mLikesCommentsWidget, "show", "widget_factory");
            elm_object_signal_callback_add(action_data->mLikesCommentsWidget, "mouse,clicked,*", "rect_spacer", action_data->mAction->on_comment_btn_clicked_cb, action_data);
        } else {
            elm_object_part_text_set(action_data->mLikesCommentsWidget, "stat_info", "");
            elm_object_signal_emit(action_data->mLikesCommentsWidget, "hide", "widget_factory");
        }
    }
}

//Creating stats footer for post
void WidgetFactory::AddPostBtnsItemFooter(Evas_Object* parent, ActionAndData *action_data)
{
    Evas_Object *item_footer = elm_layout_add(parent);
    if(item_footer) {
        Post * post = static_cast<Post *>(action_data->mData);
        evas_object_size_hint_weight_set(item_footer, 1, 1);

        if ( post && post->GetActionsAmount() > 1 ) {
            if (!post->GetCanShare()) {
                elm_layout_file_set(item_footer, Application::mEdjPath, "2btn_footer");
            } else {
                elm_layout_file_set(item_footer, Application::mEdjPath, "3btn_footer");
            }
        } else {
            elm_layout_file_set(item_footer, Application::mEdjPath, "1btn_footer");
        }
        elm_box_pack_end(parent, item_footer);
        evas_object_show(item_footer);

        elm_object_translatable_part_text_set(item_footer, "comment_text", "IDS_COMMENT");
        elm_object_translatable_part_text_set(item_footer, "share_text", "IDS_SHARE");
        elm_object_translatable_part_text_set(item_footer, "like_text", "IDS_LIKE");

        elm_object_signal_callback_add(item_footer, "comment.click", "comment_button", action_data->mAction->on_comment_btn_clicked_cb, action_data);
        elm_object_signal_callback_add(item_footer, "share.click", "share_button", on_share_btn_clicked_cb, action_data);
        elm_object_signal_callback_add(item_footer, "mouse,clicked,*", "like_button", action_data->mAction->on_like_btn_clicked_cb, action_data);

        action_data->mPostActionsBtns = item_footer;
        RefreshPostBtnsItemFooter(action_data);
    }
}

void WidgetFactory::RefreshPostBtnsItemFooter(ActionAndData *action_data)
{
    Post * post = static_cast<Post *>(action_data->mData);
    if (action_data->mPostActionsBtns) {
        if ( post && post->GetActionsAmount() > 1 ) {
            if (!post->GetCanShare()) {
                elm_layout_file_set(action_data->mPostActionsBtns, Application::mEdjPath, "2btn_footer");
            } else {
                elm_layout_file_set(action_data->mPostActionsBtns, Application::mEdjPath, "3btn_footer");
            }
            if ( post->GetHasLiked() ) {
                elm_object_signal_emit(action_data->mPostActionsBtns, "activate", "like_btn");
            }
            else {
                elm_object_signal_emit(action_data->mPostActionsBtns, "deactivate", "like_btn");
            }
        } else {
            elm_layout_file_set(action_data->mPostActionsBtns, Application::mEdjPath, "1btn_footer");
        }
    }
}

void WidgetFactory::RefreshPostItemFooter(ActionAndData *action_data)
{
    RefreshPostBtnsItemFooter(action_data);
    RefreshPostStatsItemFooter(action_data);
}

void WidgetFactory::CommentItem(Evas_Object* parent, ActionAndData *action_data)
{
    Post *post = static_cast<Post *>(action_data->mData);

    Evas_Object *item_comment = elm_layout_add(parent);
    if(item_comment) {
        evas_object_size_hint_weight_set(item_comment, 1, 1);
        evas_object_size_hint_align_set(item_comment, -1, 0);
        elm_layout_file_set(item_comment, Application::mEdjPath, "comment_box");
        elm_box_pack_end(parent, item_comment);
        evas_object_show(item_comment);

        Evas_Object *avatar = elm_image_add(item_comment);
        elm_object_part_content_set(item_comment, "avatar", avatar);
        // TODO Insert real data
        action_data->UpdateImageLayoutAsync(post->GetFromPictureLink(), avatar);
        evas_object_smart_callback_add(avatar, "clicked", ActionBase::on_avatar_clicked_cb, action_data);
        evas_object_show(avatar);

        // TODO Insert real data
        elm_object_part_text_set(item_comment, "name", "John Snow");

        // TODO Insert real data
        Evas_Object *comment_text = elm_label_add(item_comment);
        elm_object_part_content_set(item_comment, "text", comment_text);
        elm_label_wrap_width_set(comment_text, R->NF_COMMENT_MSG_WRAP_WIDTH);
        elm_label_line_wrap_set(comment_text, ELM_WRAP_WORD);
        char *text = WidgetFactory::WrapByFormat2(SANS_MEDIUM_666_FORMAT, R->NF_COMMENT_MSG_TEXT_SIZE, "long text example very very very long text insert here some real data, please. but later");
        if (text) {
            elm_object_text_set(comment_text, text);
            delete[] text;
        }
        evas_object_show(comment_text);
    }
}

//Creating post with text or photo or smthg else
Evas_Object *WidgetFactory::CreatePostBox(Evas_Object *parent, Evas_Object ** wrapWidget, void *user_data, RenderType renderType)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    CHECK_RET(action_data, nullptr);
    Post *post = static_cast<Post*>(action_data->mData);
    CHECK_RET(post, nullptr);

    Evas_Object *content = CreatePostItemWrapper(parent, wrapWidget, renderType);

    if(content) {
        PostHeaderCustomization(content, action_data);

        if(post->GetMessage()) {
            if (post->GetStatusType() && !strcmp(post->GetStatusType(), "created_note")) {
                //do not set message
            } else {
                PostPresenter presenter(*post);
                CreatePostText(content, action_data, presenter.CutMessage());
            }
        }

        PostContentCustomization(content, action_data);

        AddPostStatsItemFooter(content, action_data);
        AddPostBtnsItemFooter(content, action_data);

        // TODO Insert real rule to display comments under post
        int i = 0;
        if (i == 1) {
            Evas_Object *box_comment = CreateCommentWrapper(content);
            CommentItem(box_comment, action_data);
        }
    }

    return content;
}

void WidgetFactory::RefreshPostText(ActionAndData *action_data, const std::string& text) {
    CHECK_RET_NRV(action_data);
    CHECK_RET_NRV(action_data->mPostText);
    if(text.empty()) {
        evas_object_del(action_data->mPostText);
    } else {
        int textLen = text.length();
        int continueReadingFormatLen = strlen(FEED_ITEM_CUT_MESSAGE_FORMAT_1);
        bool isContinueReading = textLen >= continueReadingFormatLen && !strcmp(text.c_str() + textLen - continueReadingFormatLen, FEED_ITEM_CUT_MESSAGE_FORMAT_1);
        char *textCopy = strdup(text.c_str());
        if (isContinueReading) {
            *(textCopy + textLen - continueReadingFormatLen) = '\0';
        }
        std::stringstream messageTextStream;
        messageTextStream << "<font_size="<< R->NF_POST_MSG_TEXT_SIZE << " color=#000>" << textCopy << "</>";
        free(textCopy);
        if (isContinueReading) {
            std::stringstream format;
            format << "%s" << FEED_ITEM_CUT_MESSAGE_FORMAT_1;
            FRMTD_TRNSLTD_TXT_SET2(action_data->mPostText, format.str().c_str(), messageTextStream.str().c_str(), "IDS_CONTINUE_READING");
        } else {
            elm_object_text_set(action_data->mPostText, messageTextStream.str().c_str());
        }
    }
}
//Create post text
void WidgetFactory::CreatePostText(Evas_Object *parent, ActionAndData *action_data, const std::string& text)
{
    Evas_Object *text_wrapper = elm_layout_add(parent);
    if(text_wrapper && !text.empty()) {
        evas_object_size_hint_weight_set(text_wrapper, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
        evas_object_size_hint_align_set(text_wrapper, EVAS_HINT_FILL, 0);
        elm_layout_file_set(text_wrapper, Application::mEdjPath, "post_text");
        elm_box_pack_end(parent, text_wrapper);
        evas_object_show(text_wrapper);

        Evas_Object *post_text = elm_label_add(text_wrapper);
        if(post_text) {
            elm_object_part_content_set(text_wrapper, "rect_text_box", post_text);
            elm_label_wrap_width_set(post_text, R->NF_POST_MSG_WRAP_WIDTH);
            elm_label_line_wrap_set(post_text, ELM_WRAP_MIXED);
            action_data->mPostText = post_text;

            GraphObject *check = static_cast<GraphObject*>(action_data->mData);
            CHECK_RET_NRV(check);
            switch (check->GetGraphObjectType()) {
            case GraphObject::GOT_POST: {
                evas_object_smart_callback_add(post_text, "anchor,clicked", ActionBase::on_anchor_clicked_cb, action_data);
                elm_object_signal_callback_add(text_wrapper, "on_text_clicked", "", ActionBase::on_text_clicked_cb, action_data);
                elm_object_signal_callback_add(text_wrapper, "post.comment.open", "", ActionBase::on_comment_btn_clicked_cb, action_data);
                RefreshPostText(action_data, text);
            }
            break;
            case GraphObject::GOT_OPERATION_PRESENTER: {
                elm_object_signal_emit(text_wrapper, "opacity.true", "");
                PresentableAsPost* post = static_cast<PresentableAsPost*>(action_data->mData);
                PostPresenter presenter(*post);
                RefreshPostText(action_data, presenter.CutMessage());
            }
            break;
            case GraphObject::GOT_EDIT_ACTION: {
                EditAction *action = static_cast<EditAction*>(action_data->mData);
                const char *markupText = elm_entry_utf8_to_markup(action->mText);
                std::string message = markupText;
                PresenterHelpers::RemoveTaggedFriends(message);
                char *text = WidgetFactory::WrapByFormat2(SANS_REGULAR_000_FORMAT, R->NF_POST_MSG_TEXT_SIZE, message.c_str());
                free((void *)markupText);

                if(text) {
                    elm_object_text_set(post_text, text);
                    delete[] text;
                }
            }
            break;
            default:
                assert(false);
                return;
            }
            evas_object_show(post_text);
        }
    }
}

//Create post text
void WidgetFactory::CreatePostFullText(Evas_Object *parent, ActionAndData *action_data) {
    assert(action_data);
    assert(action_data->mData);
    PresentableAsPost *presentableAsPost = static_cast<PresentableAsPost*>(action_data->mData);
    PostPresenter presenter(*presentableAsPost);
    Post *post = dynamic_cast<Post*>(presentableAsPost);
    if (!post || post->GetIsGroupPost()) { //If this is Not a Post (e.g. OperationPresenter) Or this is a group post then show tags and location.
        CreatePostText(parent, action_data, presenter.ComposeFullTextMessageWithTagsAndLocation());
    } else { //If this is a non-group post
        CreatePostText(parent, action_data, presenter.GetMessage());
    }
}

//Create post text
void WidgetFactory::CreatePostInPostText(Evas_Object *parent, void *user_data)
{
    if(parent) {
        ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
        CHECK_RET_NRV(action_data);
        Evas_Object *text_wrapper = elm_layout_add(parent);
        evas_object_size_hint_weight_set(text_wrapper, 1, 1);
        evas_object_size_hint_align_set(text_wrapper, -1, 0);
        elm_layout_file_set(text_wrapper, Application::mEdjPath, "post_in_post_text");
        elm_box_pack_end(parent, text_wrapper);
        evas_object_show(text_wrapper);

        Evas_Object *post_text = elm_label_add(text_wrapper);
        if(post_text) {
            elm_object_part_content_set(text_wrapper, "rect_text_box", post_text);
            elm_label_wrap_width_set(post_text, R->NF_PIP_MSG_WRAP_WIDTH);
            elm_label_line_wrap_set(post_text, ELM_WRAP_MIXED);

            evas_object_smart_callback_add(post_text, "anchor,clicked", ActionBase::on_anchor_clicked_cb, action_data);
            elm_object_signal_callback_add(text_wrapper, "on_text_clicked", "", ActionBase::on_text_clicked_cb, action_data);
            elm_object_signal_callback_add(text_wrapper, "post.comment.open", "", ActionBase::on_comment_btn_clicked_cb, action_data);
            evas_object_show(post_text);

            action_data->mPostText = post_text;
        }
    }
}

//Create post item that was commented, shared, liked
void WidgetFactory::CreatePostInPostItem(Evas_Object *parent, ActionAndData *actionData, Evas_Object *fullTextPostinPostWrapper)
{
    assert(parent);
    if( !(actionData  && actionData->mData) ) {
        Log::error("WidgetFactory::CreatePostInPostItem: ActionAndData invalid, can't extract Post data");
        return;
    }

    Post *post = static_cast<Post*>(actionData->mData);

    Evas_Object *content = fullTextPostinPostWrapper;

    if (!content) {
        content = CreatePostInPostItemWrapper(parent);
    }

    if(content) {
        PostInPostHeaderCustomization(content, actionData);

        bool showContinueReading = false;
        if(post->GetParentId()) {
            showContinueReading = true;
        }

        if(post->GetMessage()) {
            if (post->GetStatusType() && !showContinueReading && !strcmp(post->GetStatusType(), "created_note")) {
                // do not set message
            } else {
                CreatePostInPostText(content, actionData);
                PostPresenter presenter(*post);
                if(fullTextPostinPostWrapper) {
                    RefreshPostText(actionData, presenter.ComposeFullTextMessageWithTagsAndLocation(showContinueReading));
                } else {
                    RefreshPostText(actionData, presenter.CutMessage(showContinueReading));
                }
            }
        } else if (showContinueReading) { //for posts without text but with shared sources
            CreatePostInPostText(content, actionData);
            PostPresenter presenter(*post);
            if(fullTextPostinPostWrapper) {// only ...Continue Reading should be composed
                RefreshPostText(actionData, presenter.GetMessage(showContinueReading));
            } else {
                RefreshPostText(actionData, presenter.CutMessage(showContinueReading));
            }
        }
        PostContentCustomization(content, actionData);
    }
}

void WidgetFactory::SelectBestGroupName(Eina_List *photos, char *edjeGroupName)
{
    // Total number of photos to be displayed, but not bigger than 5.
    unsigned int count = MIN(eina_list_count(photos), 5);
    // Kind of multi photo layout. Depends on portrait/landscape photos number.
    unsigned int kind;
    // Number of portrait photos within first 5 items.
    unsigned int portrait = 0;
    // Number of landscape photos within first 5 items.
    unsigned int landscape = 0;


    unsigned int i = 0;
    Eina_List *l;
    void *data;
    EINA_LIST_FOREACH(photos, l, data) {
        if (i++ == count) {
            // Do not count hidden photos.
            break;
        }
        Photo *photo = static_cast<Photo *>(data);
        if (photo->GetHeight() > photo->GetWidth()) {
            portrait++;
        } else if (photo->GetHeight() < photo->GetWidth()) {
            landscape++;
        }
    }

    switch (count) {
    case 2:
        kind = (portrait < landscape) ? 1 : 2;
        break;
    case 3:
        kind = (portrait <= landscape) ? 1 : 2;
        break;
    case 4:
        if (portrait == landscape) {
            kind = 1;
        } else {
            kind = (portrait < landscape) ? 2 : 3;
        }
        break;
    case 5:
        kind = (portrait <= landscape) ? 1 : 2;
        break;
    default:
        // Bad number of photos. Use default kind.
        kind = 1;
        break;
    }

    Utils::Snprintf_s(edjeGroupName, 25, "post_photos_%d_%d", count, kind);
}

void WidgetFactory::CreatePhotoItem(Evas_Object *parent, void *user_data)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    Post *post = static_cast<Post*>(action_data->mData);
    if(post) {
        Eina_List *photos = static_cast<Eina_List*>(post->GetPhotoList());

        unsigned int photos_count = photos ? eina_list_count(photos) : 0;
        int photosMaxCount = 5;

        Evas_Object *photo_wrapper = elm_layout_add(parent);
        if(photo_wrapper) {
            evas_object_size_hint_weight_set(photo_wrapper, 1, 1);
            evas_object_size_hint_align_set(photo_wrapper, -1, 0);

            if (photos_count > 1) {
                char edjeGroup[25];
                SelectBestGroupName(photos, edjeGroup);
                elm_layout_file_set(photo_wrapper, Application::mEdjPath, edjeGroup);
                Eina_List *l;
                void *listData;

                int i = 0;
                EINA_LIST_FOREACH(post->GetPhotoList(), l, listData) {
                    Photo *photo = static_cast<Photo *>(listData);
                    if(i < photosMaxCount) {
                        Evas_Object *post_photo = evas_object_image_add(evas_object_evas_get(photo_wrapper));
                        if(post_photo) {
                            evas_object_size_hint_weight_set(post_photo, 1, 1);
                            evas_object_size_hint_align_set(post_photo, -1, -1);
                            evas_object_show(post_photo);

                            char part[32];
                            Utils::Snprintf_s(part, 32, "swallow.photo_%d", i);
                            elm_object_part_content_set(photo_wrapper, part, post_photo);

                            int swallowW, swallowH;
                            edje_object_part_geometry_get(elm_layout_edje_get(photo_wrapper), part, NULL, NULL, &swallowW, &swallowH);
                            Utils::CropImageToFill(post_photo, swallowW, swallowH, photo->GetWidth(), photo->GetHeight());

                            action_data->UpdateImageLayoutAsync(photo->GetSrcUrl(), post_photo, ActionAndData::EEvasImage, Post::EPhotoPath, i);
                            // TODO: this exit should be checked when all layout for photo will be implemented
                        }
                    } else {
                        action_data->UpdateImageLayoutAsync(photo->GetSrcUrl(), NULL, ActionAndData::EEvasImage, Post::EPhotoPath, i);
                    }
                    if (i == photosMaxCount - 1 && photos_count > photosMaxCount) {
                        char *textExtraNum = Utils::GetRemaningPicturesNumber(photos_count);
                        if (textExtraNum) {
                            elm_object_signal_emit(photo_wrapper, "show_black_rect", "");
                            elm_object_part_text_set(photo_wrapper, "photo_text", textExtraNum);
                        }
                        delete[] textExtraNum;
                    }
                    ++i;
                }
            } else if (static_cast<Post *>(action_data->mData)->GetPhotoList() &&
                    eina_list_count(post->GetPhotoList()) == 1) {
                Photo *photo = static_cast<Photo*>(eina_list_nth(post->GetPhotoList(), 0));
                Evas_Object *post_photo = elm_image_add(photo_wrapper);
                if(post_photo) {
                    Evas_Coord post_photo_width = 0;
                    Evas_Coord post_photo_height = 0;

                    elm_image_prescale_set(post_photo,R->SCREEN_SIZE_WIDTH);

                    if (photo->GetWidth() > photo->GetHeight()) {
                        post_photo_width = R->NF_LANDSCAPE_PHOTO_LAYOUT_SIZE_W;
                        elm_layout_file_set(photo_wrapper, Application::mEdjPath, "post_landscape_photo");
                    } else {
                        post_photo_width = R->NF_SQUARE_PHOTO_LAYOUT_SIZE_W;
                        elm_layout_file_set(photo_wrapper, Application::mEdjPath, "post_square_photo");
                    }

                    // Keep original aspect ratio
                    post_photo_height = (Evas_Coord)(photo->GetHeight() * (post_photo_width / (double)photo->GetWidth()));

                    // Set all required hints for post_photo element
                    evas_object_size_hint_min_set(post_photo, post_photo_width, post_photo_height);
                    evas_object_size_hint_max_set(post_photo, post_photo_width, post_photo_height);
                    evas_object_size_hint_weight_set(post_photo, 1, 1);
                    evas_object_size_hint_align_set(post_photo, -1, -1);
                    evas_object_show(post_photo);

                    elm_layout_content_set(photo_wrapper, "photo_box", post_photo);
                    action_data->UpdateImageLayoutAsync(photo->GetSrcUrl(), post_photo, ActionAndData::EImage, Post::EPhotoPath, 0);
                }
            }
            if (photos_count > 0 && post->GetStatusType() && !strcmp(post->GetStatusType(), "added_photos")) {
                elm_object_signal_callback_add(photo_wrapper, "mouse,clicked,*", "*", action_data->mAction->on_photo_box_clicked_cb, action_data);
            }
            elm_box_pack_end(parent, photo_wrapper);
            evas_object_show(photo_wrapper);
        }
    }
}

//Create video item
void WidgetFactory::CreateVideoItem(Evas_Object *parent, void *user_data)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    Post *post = static_cast<Post*>(action_data->mData);

    int w = 0;
    int h = 0;
    int swallowW = 0;
    int swallowH = 0;
    bool isAttachmentPresented = false;
    const char * fileLink = NULL;

    Evas_Object *video_wrapper = elm_layout_add(parent);
    if(video_wrapper) {
        evas_object_size_hint_weight_set(video_wrapper, 1, 1);
        evas_object_size_hint_align_set(video_wrapper, -1, 0);

        if (eina_list_count(post->GetPhotoList()) && eina_list_nth(post->GetPhotoList(), 0)){
            Photo *photo = static_cast<Photo *> (eina_list_nth(post->GetPhotoList(), 0));

            isAttachmentPresented = true;
            w = photo->GetWidth();
            h = photo->GetHeight();
            fileLink = photo->GetSrcUrl();
        } else {
            w = R->NF_LANDSCAPE_PHOTO_LAYOUT_SIZE_W;
            h = R->NF_LANDSCAPE_PHOTO_LAYOUT_SIZE_H;
            fileLink = post->GetFullPicture();
        }

        if (w > h) {
            elm_layout_file_set(video_wrapper, Application::mEdjPath, "post_landscape_video");
        } else {
            elm_layout_file_set(video_wrapper, Application::mEdjPath, "post_portrait_video");
        }

        Evas_Object *post_photo = evas_object_image_add(evas_object_evas_get(video_wrapper));
        evas_object_size_hint_weight_set(post_photo, 1, 1);
        evas_object_size_hint_align_set(post_photo, -1, -1);
        evas_object_show(post_photo);


        elm_object_part_content_set(video_wrapper, "video_box", post_photo);


        if (edje_object_part_geometry_get(elm_layout_edje_get(video_wrapper), "video_box", NULL, NULL, &swallowW, &swallowH)){
            Utils::CropImageToFill(post_photo, swallowW, swallowH, w, h);
        }

        if(isAttachmentPresented){
            action_data->UpdateImageLayoutAsync(fileLink, post_photo, ActionAndData::EEvasImage, Post::EPhotoPath, 0);
        } else {
            action_data->UpdateImageLayoutAsync(fileLink, post_photo);
        }

        elm_object_signal_callback_add(video_wrapper, "mouse,clicked,*", "*", (Edje_Signal_Cb) action_data->mAction->on_video_post_clicked_cb, action_data);

        elm_box_pack_end(parent, video_wrapper);
        evas_object_show(video_wrapper);
    }
}

//Create location map module
void WidgetFactory::CreateLocationItem(Evas_Object *parent, void *user_data)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    Post *post = static_cast<Post*>(action_data->mData);

    Evas_Object *location_wrapper = elm_layout_add(parent);
    elm_layout_file_set(location_wrapper, Application::mEdjPath, "location_item");
    evas_object_size_hint_weight_set(location_wrapper, 1, 0);
    evas_object_size_hint_align_set(location_wrapper, -1, 0);
    elm_box_pack_end(parent, location_wrapper);
    evas_object_show(location_wrapper);

    Evas_Object *map = elm_map_add(location_wrapper);
    elm_map_user_agent_set(map, Utils::GetHTTPUserAgent());
    elm_map_paused_set(map, EINA_TRUE);
    elm_layout_content_set(location_wrapper, "map", map);
    elm_map_zoom_set(map, 16);

    Elm_Map_Overlay *overlay = elm_map_overlay_add(map, post->GetLongitude(), post->GetLatitude());
    elm_map_overlay_show(overlay);
    evas_object_show(map);

    Evas_Object *top_layer = elm_label_add(location_wrapper);
    elm_layout_content_set(location_wrapper, "map_link", top_layer);
    evas_object_show(top_layer);

    elm_object_signal_callback_add(location_wrapper, "map_clicked", "map_link", ActionBase::on_map_clicked_cb, action_data);
}

//Create location map module
void WidgetFactory::CreateLocationItemFullView(Evas_Object *parent, void *user_data)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);

    Evas_Object *location_wrapper = elm_layout_add(parent);
    elm_layout_file_set(location_wrapper, Application::mEdjPath, "location_item_full_view");
    evas_object_size_hint_weight_set(location_wrapper, 1, 0);
    evas_object_size_hint_align_set(location_wrapper, -1, 0);
    elm_box_pack_end(parent, location_wrapper);
    evas_object_show(location_wrapper);

    Evas_Object *map = elm_map_add(location_wrapper);
    elm_map_user_agent_set(map, Utils::GetHTTPUserAgent());
    elm_map_paused_set(map, EINA_TRUE);
    elm_layout_content_set(location_wrapper, "map_full_view", map);
    elm_map_zoom_set(map, 24);

    double longitude = 0;
    double latitude = 0;
    if (action_data->mData) {
        if (action_data->mData->GetGraphObjectType() == GraphObject::GOT_POST) {
            Post *post = static_cast<Post*> (action_data->mData);
            longitude = post->GetLongitude();
            latitude = post->GetLatitude();
        } else if (action_data->mData->GetGraphObjectType() == GraphObject::GOT_EVENT) {
            Event *event = static_cast<Event*> (action_data->mData);
            if (event->mPlace && event->mPlace->mLocation) {
                longitude = event->mPlace->mLocation->mLongitude;
                latitude = event->mPlace->mLocation->mLatitude;
            }
        }
    }
    Elm_Map_Overlay *overlay = elm_map_overlay_add(map, longitude, latitude);
    elm_map_overlay_show(overlay);
    evas_object_show(map);
}

//Create location info module
void WidgetFactory::CreateLocationInfoItem(Evas_Object *parent, void *user_data)
{
    ActionAndData* action_data = static_cast<ActionAndData*>(user_data);
    CHECK_RET_NRV(action_data);
    Post* post = static_cast<Post*>(action_data->mData);
    CHECK_RET_NRV(post);

    Evas_Object *location_wrapper_info = elm_layout_add(parent);
    elm_layout_file_set(location_wrapper_info, Application::mEdjPath, "location_and_group_item_info");
    evas_object_size_hint_weight_set(location_wrapper_info, EVAS_HINT_EXPAND, 0);
    evas_object_size_hint_align_set(location_wrapper_info, EVAS_HINT_FILL, 0);
    elm_box_pack_end(parent, location_wrapper_info);
    evas_object_show(location_wrapper_info);

    Evas_Object *place_avatar = elm_image_add(location_wrapper_info);
    elm_layout_content_set(location_wrapper_info, "place_avatar", place_avatar);

    if (post->GetPlacePictureLink()) {
        action_data->UpdateImageLayoutAsync(post->GetPlacePictureLink(), place_avatar);
    }
    evas_object_show(place_avatar);

    Evas_Object *info_spacer = elm_box_add(location_wrapper_info);
    elm_object_part_content_set(location_wrapper_info, "place_info", info_spacer);
    evas_object_size_hint_weight_set(info_spacer, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(info_spacer, EVAS_HINT_FILL, 0.5);
    elm_box_pack_end(location_wrapper_info, info_spacer);
    evas_object_show(info_spacer);

    Evas_Object *spacer1 = elm_box_add(info_spacer);
    evas_object_size_hint_weight_set(spacer1, 10, 10);
    evas_object_size_hint_align_set(spacer1, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_box_pack_end(info_spacer, spacer1);
    evas_object_show(spacer1);

    Evas_Object *place_title = elm_label_add(info_spacer);
    evas_object_size_hint_weight_set(place_title, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    evas_object_size_hint_align_set(place_title, EVAS_HINT_FILL, EVAS_HINT_FILL);

    PostPresenter presenter(*post);
    std::string placeTitleNameStr = presenter.GetLocationPlace();
    const char *placeTitleName = placeTitleNameStr.c_str();
    if (*placeTitleName) {
        const char *placeTitleText = WidgetFactory::WrapByFormat2(SANS_REGULAR_000_FORMAT, R->NF_LOC_INFO_TITLE_TEXT_SIZE, placeTitleName);
        elm_object_text_set(place_title, placeTitleText);
        delete[] placeTitleText;
    }
    elm_box_pack_end(info_spacer, place_title);
    elm_label_wrap_width_set(place_title, R->NF_LOC_INFO_WIDTH);
    elm_label_ellipsis_set(place_title, EINA_TRUE);
    elm_label_line_wrap_set(place_title, ELM_WRAP_CHAR);
    evas_object_show(place_title);

    std::string placeTypeNameStr = post->GetLocationType();
    if (!placeTypeNameStr.empty()) {
        Evas_Object *place_type = elm_label_add(info_spacer);
        evas_object_size_hint_weight_set(place_type, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
        evas_object_size_hint_align_set(place_type, EVAS_HINT_FILL, EVAS_HINT_FILL);

        std::stringstream placeTypeStream;
        placeTypeStream << "<font_size=" << R->NF_LOC_INFO_TYPE_TEXT_SIZE << " color=#9197a3>" << placeTypeNameStr.c_str();
        unsigned int checkinsNumber = post->GetCheckinsNumber();
        if (checkinsNumber) {
            placeTypeStream << ", " << checkinsNumber << " %s</>";
            FRMTD_TRNSLTD_TXT_SET1(place_type, placeTypeStream.str().c_str(), "IDS_CHECK_IN");
        } else {
            placeTypeStream << "</>";
            elm_object_text_set(place_type, placeTypeStream.str().c_str());
        }
        elm_box_pack_end(info_spacer, place_type);
        elm_label_wrap_width_set(place_type, R->NF_LOC_INFO_WIDTH);
        elm_label_ellipsis_set(place_type, EINA_TRUE);
        elm_label_line_wrap_set(place_type, ELM_WRAP_CHAR);
        evas_object_show(place_type);
    }

    Evas_Object *spacer2 = elm_box_add(info_spacer);
    evas_object_size_hint_weight_set(spacer2, 10, 10);
    evas_object_size_hint_align_set(spacer2, EVAS_HINT_FILL, EVAS_HINT_FILL);
    elm_box_pack_end(info_spacer, spacer2);
    evas_object_show(spacer2);

    elm_object_signal_callback_add(location_wrapper_info, "item.clicked", "place_*", ActionBase::on_link_clicked_cb, action_data);
}

//Create event module
void WidgetFactory::CreateEventItemInfo(Evas_Object *parent, void *user_data)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    Post *post = static_cast<Post*>(action_data->mData);

// In case of shared Event we get it's cover and needed size in Attachment field
    if (post->GetAttachmentSrc()) {
        Evas_Object *event_wrapper_cover = CreateLayoutByGroup(parent, "event_item_cover");
        evas_object_size_hint_align_set(event_wrapper_cover, -1, 0);
        elm_box_pack_end(parent, event_wrapper_cover);

        Evas_Object *event_cover = evas_object_image_add(evas_object_evas_get(event_wrapper_cover));
        evas_object_size_hint_weight_set(event_cover, 1, 1);
        evas_object_size_hint_align_set(event_cover, -1, -1);
        elm_object_part_content_set(event_wrapper_cover, "event_cover", event_cover);
        evas_object_show(event_cover);

        int swallowW = 0;
        int swallowH = 0;
        edje_object_part_geometry_get(elm_layout_edje_get(event_wrapper_cover), "event_cover", NULL, NULL, &swallowW, &swallowH);
        Utils::CropImageToFill(event_cover, swallowW, swallowH, post->GetAttachmentWidth(), post->GetAttachmentHeight());

        action_data->UpdateImageLayoutAsync( post->GetAttachmentSrc(), event_cover, ActionAndData::EEvasImage);

        elm_object_signal_callback_add(event_wrapper_cover, "mouse,clicked,*", "*", ActionBase::on_event_info_clicked_cb, action_data);
    }

    Evas_Object *event_wrapper = elm_layout_add(parent);
    elm_layout_file_set(event_wrapper, Application::mEdjPath, "event_item_info");
    evas_object_size_hint_weight_set(event_wrapper, 1, 1);
    evas_object_size_hint_align_set(event_wrapper, -1, 0);
    elm_box_pack_end(parent, event_wrapper);
    evas_object_show(event_wrapper);

    elm_object_signal_callback_add(event_wrapper, "mouse,clicked,*", "*", ActionBase::on_event_info_clicked_cb, action_data);

    std::string attachmentTitle(post->GetAttachmentTitle());
    if(!attachmentTitle.empty())
    {
        Evas_Object *event_name = elm_label_add(event_wrapper);
        elm_object_part_content_set(event_wrapper, "event_name", event_name);
        elm_label_wrap_width_set(event_name, R->NF_LOC_INFO_WRAP_WIDTH);
        elm_label_line_wrap_set(event_name, ELM_WRAP_WORD);
        std::unique_ptr<char[]> title(Utils::UTF8TextCutter(post->GetAttachmentTitle().c_str(), 30));
        std::unique_ptr<char[]> name(WidgetFactory::WrapByFormat2(SANS_REGULAR_000_FORMAT, R->NF_LOC_INFO_TITLE_TEXT_SIZE, title.get()));
        if (name){
            elm_object_text_set(event_name, name.get());
        }
    }
// Filling in the event info parts
    Evas_Object *event_date = elm_label_add(event_wrapper);
    elm_object_part_content_set(event_wrapper, "event_date", event_date);
    elm_label_wrap_width_set(event_date, UIRes::GetInstance()->NF_LOC_INFO_WRAP_WIDTH);
    elm_label_line_wrap_set(event_date, ELM_WRAP_WORD);
    action_data->UpdateLayoutAsync(ActionAndData::EText, event_date, Post::EEventInfo);

}

//Create page module
void WidgetFactory::CreatePageItem(Evas_Object *parent, void *user_data)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    Post *post = static_cast<Post*>(action_data->mData);

    Evas_Object *group_wrapper = elm_layout_add(parent);
    elm_layout_file_set(group_wrapper, Application::mEdjPath, "location_and_group_item_info");
    evas_object_size_hint_weight_set(group_wrapper, 1, 1);
    elm_box_pack_end(parent, group_wrapper);
    evas_object_show(group_wrapper);

    if (post->GetGroupName()) {
        elm_object_signal_emit(group_wrapper, "got.group", "item");
    }
    Evas_Object *info_spacer = elm_box_add(group_wrapper);
    elm_object_part_content_set(group_wrapper, "place_info", info_spacer);
    evas_object_size_hint_weight_set(info_spacer, 1, 1);
    evas_object_size_hint_align_set(info_spacer, -1, 0.5);
    elm_box_pack_end(group_wrapper, info_spacer);
    evas_object_show(info_spacer);

    Evas_Object *spacer1 = elm_box_add(info_spacer);
    evas_object_size_hint_weight_set(spacer1, 10, 10);
    evas_object_size_hint_align_set(spacer1, -1, -1);
    elm_box_pack_end(info_spacer, spacer1);
    evas_object_show(spacer1);

    Evas_Object *place_title = elm_label_add(info_spacer);
    evas_object_size_hint_weight_set(place_title, 1, 1);
    evas_object_size_hint_align_set(place_title, -1, -1);
    elm_label_wrap_width_set(place_title, R->NF_LOC_INFO_WRAP_WIDTH);
    elm_label_line_wrap_set(place_title, ELM_WRAP_MIXED);
    const char *place_text_cut = Utils::UTF8TextCutter(post->GetGroupName(), 30);
    char *place_text = WidgetFactory::WrapByFormat2(SANS_REGULAR_000_FORMAT, R->NF_LOC_INFO_TITLE_TEXT_SIZE, place_text_cut);
    delete[] place_text_cut;
    if (place_text) {
        elm_object_text_set(place_title, place_text);
        delete[] place_text;
    }
    elm_box_pack_end(info_spacer, place_title);
    evas_object_show(place_title);

    std::string attachmentDescription(post->GetAttachmentDescription());
    if(!attachmentDescription.empty()) {
        Evas_Object *place_type = elm_label_add(info_spacer);
        evas_object_size_hint_weight_set(place_type, 1, 1);
        evas_object_size_hint_align_set(place_type, -1, -1);
        elm_label_wrap_width_set(place_type, R->NF_LOC_INFO_WRAP_WIDTH);
        elm_label_line_wrap_set(place_type, ELM_WRAP_WORD);
        char *place_type_text = WidgetFactory::WrapByFormat2(SANS_MEDIUM_9197a3_FORMAT, R->NF_LOC_INFO_TYPE_TEXT_SIZE, attachmentDescription.c_str());
        if (place_type_text) {
            elm_object_text_set(place_type, place_type_text);
            delete[] place_type_text;
        }
        elm_box_pack_end(info_spacer, place_type);
        evas_object_show(place_type);
    }

    Evas_Object *spacer2 = elm_box_add(info_spacer);
    evas_object_size_hint_weight_set(spacer2, 10, 10);
    evas_object_size_hint_align_set(spacer2, -1, -1);
    elm_box_pack_end(info_spacer, spacer2);
    evas_object_show(spacer2);

    elm_object_signal_callback_add(group_wrapper, "mouse,clicked,*", "*", ActionBase::on_page_clicked_cb, action_data);
}

//Create link module
void WidgetFactory::CreateLinkItem(Evas_Object *parent, void *user_data)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    CHECK_RET_NRV(action_data);
    Post *post = static_cast<Post*>(action_data->mData);
    CHECK_RET_NRV(post);

    Evas_Object *link_wrapper = elm_layout_add(parent);
    // TODO Insert rule that calculate picture size and choose right layout type to display it
    // TODO Bug with link in "link_portrait" edje group
    // TODO If youtube link - link_lo_res_style
    //elm_layout_file_set(link_wrapper, mEdjPath, "link_portrait");
    elm_layout_file_set(link_wrapper, Application::mEdjPath, "link_lo_res");
    evas_object_size_hint_weight_set(link_wrapper, 1, 1);
    evas_object_size_hint_align_set(link_wrapper, -1, 0);
    elm_box_pack_end(parent, link_wrapper);
    evas_object_show(link_wrapper);

    Evas_Object *link_picture = elm_image_add(link_wrapper);
    elm_layout_content_set(link_wrapper, "link_picture", link_picture);
    if (post->GetFullPicture()) {
        action_data->UpdateImageLayoutAsync(post->GetFullPicture(), link_picture);
    } else if (post->GetAttachmentSrc()) {
        action_data->UpdateImageLayoutAsync(post->GetAttachmentSrc(), link_picture);
    } else {
        edje_object_signal_emit(elm_layout_edje_get(link_wrapper), "hide.link.picture", "link_picture");
    }
    evas_object_show(link_picture);

    Evas_Object *info_spacer = elm_box_add(link_wrapper);
    elm_object_part_content_set(link_wrapper, "link_info_box", info_spacer);
    evas_object_size_hint_weight_set(info_spacer, 1, 1);
    evas_object_size_hint_align_set(info_spacer, -1, 0.5);
    evas_object_show(info_spacer);

    PostPresenter presenter(*post);
    elm_object_part_text_set(link_wrapper, "link_text", presenter.GetLinkName().c_str());

    if(!presenter.GetPollOption().empty()) {
        elm_object_part_text_set(link_wrapper, "link_text_poll", presenter.GetPollOption().c_str());
    }

    if(!post->GetCaption().empty() && post->GetCaption() != post->GetAttachmentTitle()) {
        elm_object_part_text_set(link_wrapper, "link_text_caption", presenter.GetCaption().c_str());
    } else if (post->GetAttachmentDescription().size()) {
        elm_object_part_text_set(link_wrapper, "link_text_caption", presenter.GetPageDescription().c_str());
    }

    Evas_Object *spacer2 = elm_box_add(info_spacer);
    evas_object_size_hint_weight_set(spacer2, 10, 10);
    evas_object_size_hint_align_set(spacer2, -1, -1);
    elm_box_pack_end(info_spacer, spacer2);
    evas_object_show(spacer2);

    elm_object_signal_callback_add(link_wrapper, "link.click", "link_*", ActionBase::on_link_clicked_cb, action_data);
    if (post->GetType() && !strcmp(post->GetType(), "video"))
    {
        edje_object_signal_emit(elm_layout_edje_get(link_wrapper), "show", "play.icon");
    }
    evas_object_show(link_wrapper);
}

void WidgetFactory::CreateSharedPostItem(Evas_Object *content, ActionAndData *actionData)
{
    assert (content);
    if( !(actionData  && actionData->mData) ) {
        Log::error("WidgetFactory::CreateSharedPostItem: ActionAndData invalid, can't extract Post data");
        return;
    }

    Post *post = static_cast<Post*>(actionData->mData);

    if (post->GetMessage()) {
        PostPresenter presenter(*post);
        CreatePostText(content, actionData, presenter.CutMessage());
    }

    if (post->GetParentId()) {
        Post* sharedSource = post->GetParent();
        if (sharedSource) {
            ActionAndData* parentActionData = new ActionAndData(sharedSource, actionData->mAction, actionData);
            CreatePostInPostItem(content, parentActionData);
            post->SetIsSharedPost(true);
        } else {
            PostContentCustomization(content, actionData);
        }
    } else {
        PostContentCustomization(content, actionData);
    }
}

//Creating shared/commented/liked
Evas_Object *WidgetFactory::CreateAffectedPostBox(Evas_Object *parent, ActionAndData *actionData, RenderType renderType)
{
    Post *post = static_cast<Post*>(actionData->mData);

    Evas_Object *content = NULL;
    Evas_Object *wrapWidget = NULL;

    //accordingly to issue#36,
    //The status_type and type fields are returned when the user performs an action
    //via the composer (posting a status, uploading or directly sharing a photo/video).
    //Whereas for edge stories (liked, commented, shared to group etc.) only type field
    //is returned.
    bool isBumperNeeded = !post->GetStatusType();

    if(isBumperNeeded && ((post->GetType() && post->GetStory().find(" shared ") != std::string::npos) ||
            (post->GetIsGroupPost() && post->GetParentId()))) { //Shared to a group
        isBumperNeeded = false;
    }

    if (isBumperNeeded) {
        Post* postChild = post->GetParent();
        if (postChild) {
            ActionAndData* parentActionData = new ActionAndData(postChild, actionData->mAction, actionData);

            content = CreatePostItemWrapper(parent, &wrapWidget, renderType);

            AddPostItemBumpHeader(content, actionData);

            AddPostItemHeader(content, parentActionData, eNAME, false);

            CreateSharedPostItem(content, parentActionData);

            AddPostStatsItemFooter(content, parentActionData);
            AddPostBtnsItemFooter(content, parentActionData);
        } else {
            return NULL;
        }
    } else {
        content = CreatePostItemWrapper(parent, &wrapWidget, renderType);
        AddPostItemHeader(content, actionData, eSTORY);
        CreateSharedPostItem(content, actionData);

        AddPostStatsItemFooter(content, actionData);
        AddPostBtnsItemFooter(content, actionData);
    }
    if (wrapWidget) {
        evas_object_show(wrapWidget);
    } else {
        Log::error("WidgetFactory::CreateAffectedPostBox->Can't create widget");
    }
    return wrapWidget;
}

Evas_Object * WidgetFactory::CreateWrapperByName(Evas_Object * parent, const char * wrapperName) {

    Evas_Object *wrapper = elm_layout_add(parent);
    elm_layout_file_set(wrapper, Application::mEdjPath, wrapperName);
    evas_object_size_hint_weight_set(wrapper, 1, 1);
    evas_object_size_hint_align_set(wrapper, -1, 0);
    elm_box_pack_end(parent, wrapper);
    evas_object_show(wrapper);

    Evas_Object *wrapperBox = elm_box_add(wrapper);
    evas_object_size_hint_weight_set(wrapperBox, 1, 1);
    evas_object_size_hint_align_set(wrapperBox, -1, 0);
    elm_layout_content_set(wrapper, "content", wrapperBox);
    elm_box_padding_set(wrapperBox, 0, 0);

    return wrapperBox;
}

Evas_Object * WidgetFactory::CreateLayoutByGroupAtParentBoxEnd(Evas_Object * parent, const char * groupName)
{
    Evas_Object * contentLayout = CreateLayoutByGroup(parent, groupName);
    elm_box_pack_end(parent, contentLayout);

    return contentLayout;
}

Evas_Object * WidgetFactory::CreateLayoutByGroup(Evas_Object * parent, const char * groupName)
{
    Evas_Object *contentLayout = elm_layout_add(parent);
    evas_object_size_hint_weight_set(contentLayout, 1, 1);
    evas_object_size_hint_align_set(contentLayout, -1, -1);
    elm_layout_file_set(contentLayout, Application::mEdjPath, groupName);
    evas_object_show(contentLayout);
    return contentLayout;
}

Evas_Object * WidgetFactory::CreateGrayHeaderWithLogo(Evas_Object *parent, Evas_Object *textObject,
        const char *rightButtonText, ScreenBase *screen)
{
    return CreateHeaderWithBackBtnTextRightBtn(parent, true, "back_logo_grey",
            textObject, rightButtonText, screen);
}

Evas_Object * WidgetFactory::CreateGrayHeaderWithLogo(Evas_Object *parent, const char *headerText,
        const char *rightButtonText, ScreenBase *screen)
{
    return CreateHeaderWithBackBtnTextRightBtn(parent, true, "back_logo_grey",
            headerText, rightButtonText, screen);
}

Evas_Object * WidgetFactory::CreateGrayHeaderWithGroupLogo(Evas_Object *parent, const char *headerText,
        const char *rightButtonText, ScreenBase *screen)
{
    return CreateHeaderWithBackBtnTextRightBtn(parent, true, "back_group_logo_grey",
            headerText, rightButtonText, screen);
}

Evas_Object * WidgetFactory::CreateBlackHeaderWithLogo(Evas_Object *parent, Evas_Object *textObject,
        const char *rightButtonText, ScreenBase *screen)
{
    return CreateHeaderWithBackBtnTextRightBtn(parent, false, "back_logo_blue",
            textObject, rightButtonText, screen);
}

Evas_Object * WidgetFactory::CreateBlackHeaderWithLogo(Evas_Object *parent, const char *headerText,
        const char *rightButtonText, ScreenBase *screen)
{
    return CreateHeaderWithBackBtnTextRightBtn(parent, false, "back_logo_blue",
            headerText, rightButtonText, screen);
}

Evas_Object * WidgetFactory::CreateHeaderWithBackBtnTextRightBtn(Evas_Object *parent, bool isGrayHeader,
        const char * backButtonStyle, const char *headerText, const char * rightButtonText, ScreenBase *screen)
{
    // Create text object for header
    Evas_Object * textObject;
    if (isGrayHeader) {
        textObject = WidgetFactory::CreateLayoutByGroup(parent, "header_simple_black_text");
    }
    else {
        textObject = WidgetFactory::CreateLayoutByGroup(parent, "header_simple_white_text");
    }
    elm_object_translatable_part_text_set(textObject, "text", headerText);

    return CreateHeaderWithBackBtnTextRightBtn(parent, isGrayHeader, backButtonStyle, textObject, rightButtonText, screen);
}

Evas_Object * WidgetFactory::CreateHeaderWithBackBtnTextRightBtn(Evas_Object *parent, bool isGrayHeader,
        const char * backButtonStyle, Evas_Object *textObject, const char * rightButtonText, ScreenBase *screen)
{
    Evas_Object * header = CreateLayoutByGroup(parent, "header_backbtn_text_rightbtn");

    // Set color of header and divider
    Evas_Object * divider;
    Evas_Object * background = elm_bg_add(header);

    if (isGrayHeader) {
        divider = CreateLayoutByGroup(parent, "header_light_divider");
        elm_bg_color_set(background, 247, 248, 251);
    }
    else {
        divider = CreateLayoutByGroup(parent, "header_dark_divider");
        evas_object_color_set(background, 0, 0, 0, 204);
        elm_object_signal_emit(header, "hide_border", "header");
    }
    elm_layout_content_set(header, "divider", divider);
    elm_layout_content_set(header, "header", background);

    // Create back button
    Evas_Object *backButton = elm_button_add(header);
    ELM_BUTTON_STYLE_SET1(backButton, backButtonStyle);
    elm_layout_content_set(header, "back_button", backButton);
    evas_object_smart_callback_add(backButton, "clicked", screen->cancel_cb, screen);

    // Set header text
    elm_layout_content_set(header, "header_text", textObject);

    // Header should be defined before call of the EnableRightButton() method
    screen->SetHeaderWidget(header);
    screen->SetHeaderCancelBtnWidget(backButton);

    if (rightButtonText) {
    // Set right button text
        elm_object_translatable_part_text_set(header, "text", rightButtonText);
        elm_object_translatable_part_text_set(header, "right_button_text", rightButtonText);
        screen->EnableRightButton(true);
    } else {
        elm_object_signal_emit(header, "hide_right_button", "header");
        screen->EnableRightButton(false);
    }

    return header;
}

void WidgetFactory::SetHeaderText(Evas_Object * header, const char * text) {
    if (header && text) {
        Evas_Object * textObject = elm_layout_content_get(header, "header_text");
        if (textObject) {
            elm_object_part_text_set(textObject, "text", text);
        }
    }
}

void WidgetFactory::ShowErrorDialog(Evas_Object *layout, const char *message)
{
    if (mErrorPopup) {
       CloseErrorDialogue();
    }

    if(layout) {
        mErrorPopup = elm_popup_add(layout);
        evas_object_smart_callback_add(mErrorPopup, "block,clicked", hide_error_dialog_cb, NULL);
        elm_popup_align_set(mErrorPopup, ELM_NOTIFY_ALIGN_FILL, 0.5);

        Evas_Object *errorPopupLayout = elm_layout_add(mErrorPopup);
        evas_object_size_hint_weight_set(errorPopupLayout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
        evas_object_size_hint_align_set(errorPopupLayout, EVAS_HINT_FILL, EVAS_HINT_FILL);
        elm_layout_file_set(errorPopupLayout, Application::mEdjPath, "error_dialog_layout");
        elm_object_content_set(mErrorPopup, errorPopupLayout);
        evas_object_show(errorPopupLayout);

        elm_object_translatable_part_text_set(errorPopupLayout, "btn_text", "IDS_OK");

        elm_object_translatable_part_text_set(errorPopupLayout, "text", message);

        elm_object_signal_callback_add(errorPopupLayout, "ok.click", "btn", hide_error_dialog_cb, mErrorPopup);

        AppEvents::Get().Notify(eERROR_DIALOG_SHOW_EVENT, nullptr);
        evas_object_show(mErrorPopup);
    }
}

void WidgetFactory::hide_error_dialog_cb(void *user_data, Evas_Object *obj, void *event_info)
{
    if (obj) {
        CloseErrorDialogue();
    }
}

void WidgetFactory::hide_error_dialog_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    if (obj) {
        CloseErrorDialogue();
    }
}

void WidgetFactory::CreateSelectorItemWithIcon(Evas_Object *parent, bool isSelected, const char *icon_path, const char *text, const char *subtext, bool isLastItem, void (*callback) (void *, Evas_Object *, void *), void *callback_object)
{
    Evas_Object *item = elm_layout_add(parent);
    if(item) {
        if (icon_path) {
            elm_layout_file_set(item, Application::mEdjPath, "selector_popup_item_with_icon");
        } else {
            elm_layout_file_set(item, Application::mEdjPath, "selector_popup_item_with_no_icon");
        }
        evas_object_size_hint_weight_set(item, 1, 0);
        evas_object_size_hint_max_set(item, -1, 128);
        evas_object_size_hint_min_set(item, -1, 128);
        elm_box_pack_end(parent, item);
        evas_object_show(item);

        if (icon_path) {
            Evas_Object *avatar = elm_image_add(item);
            elm_object_part_content_set(item, "item_icon", avatar);
            ELM_IMAGE_FILE_SET(avatar, icon_path, NULL);
            evas_object_show(avatar);
        }

        elm_object_translatable_part_text_set(item, "item_title", text);

        if (subtext) {
            elm_object_translatable_part_text_set(item, "item_subtitle", subtext);
            if (isSelected) {
                elm_object_signal_emit(item, "title.selected.set", "default");
            }
        } else {
            elm_object_signal_emit(item, "center.set", "title");
            if (isSelected) {
                elm_object_signal_emit(item, "title.selected.set", "center");
            }
        }

        if (isLastItem) {
            elm_object_signal_emit(item, "hide.set", "breaker");
        }

        elm_object_signal_callback_add(item, "mouse,clicked,*", "item*", (Edje_Signal_Cb) callback, callback_object);
    }
}

/**
 * @brief Closes error dialogue
 * @return true if dialogue is closed
 */
bool WidgetFactory::CloseErrorDialogue()
{
    Log::debug("WidgetFactory::CloseErrorDialogue()");
    bool res = mErrorPopup;
    if (res) {
        evas_object_del(mErrorPopup);
        mErrorPopup = nullptr;
    }
    return res;
}

bool WidgetFactory::CloseAlertPopup()
{
    Log::debug("WidgetFactory::CloseAlertPopup()");
    bool res = screen_popup;
    if (res) {
        evas_object_del(screen_popup);
        screen_popup = nullptr;
    }
    return res;
}

Evas_Object * WidgetFactory::CreateActionBarWithBackBtnText2Actions(Evas_Object *parent,
        const char *text, const char *firstActionImageFile, const char *secondActionImageFile, ScreenBase *screen) {

    Evas_Object *actionBar = CreateLayoutByGroup(parent, "album_action_bar");
//set the action bar as a header widget of the screen
    screen->SetHeaderWidget(actionBar);


    if (text) {
        elm_object_translatable_part_text_set(actionBar, "text.caption", text);
    }

    if (firstActionImageFile) {
        Evas_Object *firstActionImage = elm_image_add(actionBar);
        elm_image_file_set(firstActionImage, firstActionImageFile, NULL);
        elm_layout_content_set(actionBar, "swallow.first_action_image", firstActionImage);
        evas_object_show(firstActionImage);
        screen->Enable1stActionBarAction(true);
    }

    if (secondActionImageFile) {
        Evas_Object *secondActionImage = elm_image_add(actionBar);
        elm_image_file_set(secondActionImage, secondActionImageFile, NULL);
        elm_layout_content_set(actionBar, "swallow.second_action_image", secondActionImage);
        evas_object_show(secondActionImage);
        screen->Enable2ndActionBarAction(true);
    }

    elm_object_signal_callback_add(actionBar, "mouse,clicked,*", "image.back", ScreenBase::on_back_btn_clicked, screen);

    screen->SetHeaderWidget(actionBar);

    return actionBar;
}

Evas_Object *WidgetFactory::CreatePostOperation(Evas_Object *parent, void *user_data, bool isAddToEnd)
{
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    Evas_Object* wrapWidget = NULL;

    if (action_data) {

        OperationPresenter* op = static_cast<OperationPresenter*>(action_data->mData);
        Sptr<Operation> oper(op->GetOperation());

        Evas_Object *content = CreatePostItemWrapper(parent, &wrapWidget);
        Evas_Object *opLayout = elm_layout_add( content );
        elm_layout_file_set( opLayout, Application::mEdjPath, "post_operation_presenter");
        elm_box_pack_end( content, opLayout );
        evas_object_show( opLayout );

        Evas_Object *progressBar  = elm_progressbar_add( opLayout );
        elm_object_style_set( progressBar, "pending_list" );
        elm_layout_content_set( opLayout, "progress_bar", progressBar );

        Evas_Object *bottomLine  = elm_bg_add( opLayout );
        elm_bg_color_set(bottomLine, 220, 220, 220);
        elm_layout_content_set( opLayout, "bottom_break_line", bottomLine );

        Evas_Object *leftLine  = elm_bg_add( opLayout );
        elm_bg_color_set(leftLine, 220, 220, 220);
        elm_layout_content_set( opLayout, "left_break_line", leftLine );

        Evas_Object *rightLine  = elm_bg_add( opLayout );
        elm_bg_color_set(rightLine, 220, 220, 220);
        elm_layout_content_set( opLayout, "right_break_line", rightLine );

        Evas_Object *percentLabel = elm_label_add(opLayout);
        evas_object_size_hint_align_set(percentLabel, 0.5, 0.5);
        elm_layout_content_set( opLayout, "left_counter", percentLabel );

        Evas_Object *leftIcon  = elm_image_add( opLayout );
        elm_image_file_set(leftIcon, ICON_DIR "/ic_lazyload_error.png", nullptr);
        elm_layout_content_set( opLayout, "left_icon", leftIcon );

        Evas_Object *rightIcon  = elm_image_add( opLayout );
        elm_image_file_set(rightIcon, ICON_DIR "/nf_post_more.png", nullptr);
        elm_layout_content_set( opLayout, "right_icon", rightIcon );

        elm_object_translatable_part_text_set(opLayout, "error_text", "IDS_ERROR_OCCURRED");

        AddLazyPostItemHeader(content, action_data);

        Post *post = static_cast<Post*>(action_data->mData);
        PostPresenter presenter(*post);
        CreatePostText(content, action_data, presenter.CutMessage());

        if (oper->GetType() == Operation::OT_Photo_Post_Create || oper->GetType() == Operation::OT_Album_Add_New_Photos) {
            CreateLazyPhotoItem(content, action_data);
        } else if (oper->GetType() == Operation::OT_Video_Post_Create) {
            CreateLazyVideoItem(content, action_data);
        } else if (oper->GetType() == Operation::OT_Share_Post_Create) {
            CreateLazySharedItem(content, action_data);
        }

        action_data->mOperationProgress = opLayout;
        if (oper->IsFeedOperation()) {
            RefreshItemHeader(false, action_data->mOperationProgress, action_data);
            UpdateProgressBar(action_data->mOperationProgress, 0.0);
        }
        if (wrapWidget) {
            evas_object_show(wrapWidget);
        } else {
            Log::error("WidgetFactory::CreatePostOperation->Can't create widget");
        }
    }

    return wrapWidget;
}

void WidgetFactory::show_cancel_posting_popup_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::info(LOG_FACEBOOK_OPERATION, "WidgetFactory::show_cancel_posting_popup_cb");
    if(obj) {
        ActionAndData *action_data = static_cast<ActionAndData*>(user_data);

        const int itemsNum = 1;
        static PopupMenu::PopupMenuItem popup_menu_items[itemsNum] =
        {
            {
                NULL,
                "IDS_CANCEL_UPLOAD",
                NULL,
                DeletePost,
                NULL,
                0,
                true,
                false
            },
        };

        popup_menu_items[0].data = action_data;

        Evas_Coord y = 0, h = 0;

        evas_object_geometry_get(obj, NULL, &y, NULL, &h);
        y += h/2;

       PopupMenu::Show(itemsNum, popup_menu_items, obj, false, y);
    }
}

void WidgetFactory::show_error_popup_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    assert(user_data);
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    assert(action_data->mData);
    OperationPresenter *op = static_cast<OperationPresenter*>(action_data->mData);
    Sptr<Operation> oper(op->GetOperation());
    if(oper) {
        if (oper->GetErrorCode() == 506) {
            ShowErrorDialog(Application::GetInstance()->mNf, "IDS_POST_FAILED_MORE_INFO_506");
        } else {
            ShowErrorDialog(Application::GetInstance()->mNf, "IDS_POST_FAILED_MORE_INFO_UNKNOWN");
        }
    }
}

void WidgetFactory::show_retry_delete_post_popup_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source)
{
    Log::info( LOG_FACEBOOK_OPERATION, "WidgetFactory::show_retry_delete_post_popup_cb: ");
    if(obj) {
        ActionAndData *action_data = static_cast<ActionAndData*>(user_data);

        const int itemsNum = 2;
        static PopupMenu::PopupMenuItem popup_menu_items[itemsNum] =
        {
            {
                NULL,
                "IDS_TAP_TO_RETRY",
                NULL,
                ActionBase::retry_post_send_cb,
                NULL,
                0,
                false,
                false
            },
            {
                NULL,
                "IDS_DELETE",
                NULL,
                DeletePost,
                NULL,
                1,
                true,
                false
            }
        };

        popup_menu_items[0].data = action_data;
        popup_menu_items[1].data = action_data;

        Evas_Coord y = 0, h = 0;

        evas_object_geometry_get(obj, NULL, &y, NULL, &h);
        y += h/2;

       PopupMenu::Show(itemsNum, popup_menu_items, obj, false, y);
    }
}

void WidgetFactory::RefreshItemHeader(bool showErrorHeader, Evas_Object *object, void *data)
{
    elm_object_signal_callback_del(object, "mouse,clicked,*", "right_icon",  show_retry_delete_post_popup_cb);
    elm_object_signal_callback_del(object, "mouse,clicked,*", "right_icon",  show_cancel_posting_popup_cb);
    elm_object_signal_callback_del(object, "mouse,clicked,*", "caption_rect",  show_error_popup_cb);
    if (showErrorHeader) {
        Log::info(LOG_FACEBOOK_OPERATION, "WidgetFactory::RefreshItemHeader: upload error!");
        elm_layout_signal_emit(object, "hide.progress", "*");
        elm_object_translatable_part_text_set(object, "error_text", "IDS_POST_UPLOAD_ERROR");
        elm_object_signal_callback_add(object, "mouse,clicked,*", "right_icon",  show_retry_delete_post_popup_cb, data);
        elm_object_signal_callback_add(object, "mouse,clicked,*", "caption_rect",  show_error_popup_cb, data);
    } else {
        Log::info(LOG_FACEBOOK_OPERATION, "WidgetFactory::RefreshItemHeader: show progress...");
        elm_layout_signal_emit(object, "show.progress", "*");
        elm_object_signal_callback_add(object, "mouse,clicked,*", "right_icon",  show_cancel_posting_popup_cb, data);
    }
}

void WidgetFactory::UpdateProgressBar(Evas_Object *object, double value)
{
    Evas_Object *progressBar = elm_layout_content_get(object, "progress_bar");
    elm_progressbar_value_set(progressBar, value);

    Evas_Object *leftCounter = elm_layout_content_get(object, "left_counter");
    int counter = (int)(value*100);
    std::string progressStr(std::to_string(counter));
    const char *text = WidgetFactory::WrapByFormat3(SANS_MEDIUM_3b5998_FORMAT, R->NF_PROGRESS_COUNTER_FONT_SIZE, progressStr.c_str(),"%");
    elm_object_text_set(leftCounter, text);
    delete[] text;
}

void WidgetFactory::DeletePost(void *user_data, Evas_Object *obj, void *EventInfo){
    ActionAndData *action_data = static_cast<ActionAndData*>(user_data);
    Log::info( LOG_FACEBOOK_OPERATION, "WidgetFactory::DeletePost");
    if(action_data && action_data->mAction) {
        Utils::ShowToast(action_data->mAction->getScreen()->getMainLayout(), i18n_get_text("IDS_POST_DELETED"));
        action_data->mAction->CancelPostingOperation(action_data);
    }
}

void WidgetFactory::UpdatePostOperation(ActionAndData *action_data, double *progress)
{
    if(action_data && action_data->mData && (action_data->mData->GetGraphObjectType() == GraphObject::GOT_OPERATION_PRESENTER)
       && action_data->mOperationProgress) {
        OperationPresenter *op = static_cast<OperationPresenter*>(action_data->mData);
        Sptr<Operation> oper(op->GetOperation());
        if(oper) {
            if( oper->GetOperationResult() == Operation::OR_Error) {
                if ( oper->IsFeedOperation() ) {
                    RefreshItemHeader(true, action_data->mOperationProgress, action_data);
                }
            } else if (oper->GetState() == Operation::OS_NotStarted) {
                if ( oper->IsFeedOperation() ) {
                    UpdateProgressBar(action_data->mOperationProgress, 0.0);
                    RefreshItemHeader(false, action_data->mOperationProgress, action_data);
                }
            } else if (oper->GetState() == Operation::OS_InProgress && progress ) {
                UpdateProgressBar(action_data->mOperationProgress, *progress);
            }
        }
    }
}

void WidgetFactory::AddLazyPostItemHeader(Evas_Object* parent, ActionAndData *action_data) {
    AddPostItemHeader(parent, action_data, eSTORY, false);
}

void WidgetFactory::CreateLazyPhotoItem(Evas_Object *parent, ActionAndData *action_data)
{
    OperationPresenter *op = static_cast<OperationPresenter*>(action_data->mData);
    Sptr<PhotoPostOperation> oper(static_cast<PhotoPostOperation*>(op->GetOperation().Get()));
    Eina_List *photos = static_cast<Eina_List*>(oper->GetPhotos());

    unsigned int photos_count = photos ? eina_list_count(photos) : 0;
    int photosMaxCount = 5;

    Evas_Object *photo_wrapper = elm_layout_add(parent);
    evas_object_size_hint_weight_set(photo_wrapper, 1, 1);
    evas_object_size_hint_align_set(photo_wrapper, -1, 0);

    if (photos_count > 1) {
        char edjeGroup[25];
        SelectBestGroupName(photos, edjeGroup);
        elm_layout_file_set(photo_wrapper, Application::mEdjPath, edjeGroup);
        Eina_List *l;
        void *listData;

        elm_object_signal_emit(photo_wrapper, "opacity.true", "");

        int i = 0;
        EINA_LIST_FOREACH(oper->GetPhotos(), l, listData) {
            PhotoPostOperation::PhotoPostDescription *photo = static_cast<PhotoPostOperation::PhotoPostDescription *>(listData);

            Evas_Object *post_photo = evas_object_image_add(evas_object_evas_get(photo_wrapper));
            evas_object_image_load_orientation_set(post_photo, EINA_TRUE);

            evas_object_size_hint_weight_set(post_photo, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
            evas_object_size_hint_align_set(post_photo, EVAS_HINT_FILL, EVAS_HINT_FILL);
            evas_object_show(post_photo);

            char part[32];
            Utils::Snprintf_s(part, 32, "swallow.photo_%d", i);
            elm_object_part_content_set(photo_wrapper, part, post_photo);

            evas_object_image_file_set(post_photo, photo->GetFilePath(), NULL);

            int swallowW = 0;
            int swallowH = 0;
            edje_object_part_geometry_get(elm_layout_edje_get(photo_wrapper), part, NULL, NULL, &swallowW, &swallowH);
            evas_object_image_load_size_set(post_photo,swallowW,swallowH);

            int imgW = 0;
            int imgH = 0;
            evas_object_image_size_get(post_photo, &imgW, &imgH);

            Utils::CropImageToFill(post_photo, swallowW, swallowH, imgW, imgH);
            ++i;
            if ( i >= photosMaxCount ) {
                break;
            }
        }
        if (photos_count > photosMaxCount) {
            char *textExtraNum = Utils::GetRemaningPicturesNumber(photos_count);
            if (textExtraNum) {
                elm_object_signal_emit(photo_wrapper, "show_black_rect", "");
                elm_object_part_text_set(photo_wrapper, "photo_text", textExtraNum);
            }
            delete[] textExtraNum;
        }
    } else if (eina_list_count(oper->GetPhotos()) == 1) {
        PhotoPostOperation::PhotoPostDescription *photo = static_cast<PhotoPostOperation::PhotoPostDescription*>(eina_list_nth(oper->GetPhotos(), 0));
        Evas_Object *post_photo = elm_image_add(photo_wrapper);
        elm_image_prescale_set(post_photo,R->SCREEN_SIZE_WIDTH);

        Evas_Coord post_photo_width = 0;
        Evas_Coord post_photo_height = 0;
        int imgW = 0;
        int imgH = 0;

        elm_image_file_set(post_photo, photo->GetFilePath(), NULL);
        elm_image_object_size_get(post_photo, &imgW, &imgH);

        if (imgW > imgH) {
            post_photo_width = R->NF_LANDSCAPE_PHOTO_LAYOUT_SIZE_W;
            elm_layout_file_set(photo_wrapper, Application::mEdjPath, "post_landscape_photo");
        } else {
            post_photo_width = R->NF_SQUARE_PHOTO_LAYOUT_SIZE_W;
            elm_layout_file_set(photo_wrapper, Application::mEdjPath, "post_square_photo");
        }

        elm_object_signal_emit(photo_wrapper, "opacity.true", "");

        // Keep original aspect ratio
        post_photo_height = (Evas_Coord)(imgH * (post_photo_width / (double)imgW));

        // Set all required hints for post_photo element
        evas_object_size_hint_min_set(post_photo, post_photo_width, post_photo_height);
        evas_object_size_hint_max_set(post_photo, post_photo_width, post_photo_height);
        evas_object_size_hint_weight_set(post_photo, 1, 1);
        evas_object_size_hint_align_set(post_photo, -1, -1);
        evas_object_show(post_photo);

        elm_layout_content_set(photo_wrapper, "photo_box", post_photo);
    }

    elm_box_pack_end(parent, photo_wrapper);
    evas_object_show(photo_wrapper);
}

void WidgetFactory::CreateLazyVideoItem(Evas_Object *parent, ActionAndData *action_data)
{
    OperationPresenter *op = static_cast<OperationPresenter*>(action_data->mData);
    Sptr<PhotoPostOperation> oper(static_cast<PhotoPostOperation*>(op->GetOperation().Get()));


    bool isPortrait = true;
    int swallowW = 0;
    int swallowH = 0;
    int w = 0;
    int h = 0;


    Evas_Object *video_wrapper = elm_layout_add(parent);
    evas_object_size_hint_weight_set(video_wrapper, 1, 1);
    evas_object_size_hint_align_set(video_wrapper, -1, 0);

    PhotoPostOperation::PhotoPostDescription *photo = static_cast<PhotoPostOperation::PhotoPostDescription*>(eina_list_nth(oper->GetPhotos(), 0));

    Evas_Object *post_photo = evas_object_image_add(evas_object_evas_get(video_wrapper));

    evas_object_size_hint_weight_set(post_photo, 1, 1);
    evas_object_size_hint_align_set(post_photo, -1, -1);
    evas_object_show(post_photo);

    metadata_extractor_h metadata;
    metadata_extractor_create(&metadata);
    metadata_extractor_set_path(metadata, photo->GetOriginalPath());


    if (metadata) {
        char *angle = nullptr;
        char *imgW = nullptr;
        char *imgH = nullptr;

        metadata_extractor_get_metadata(metadata, METADATA_ROTATE, &angle);
        metadata_extractor_get_metadata(metadata, METADATA_VIDEO_WIDTH, &imgW);
        metadata_extractor_get_metadata(metadata, METADATA_VIDEO_HEIGHT, &imgH);

        w = imgW? atoi(imgW) : 0;
        h = imgH? atoi(imgH) : 0;

        if (angle) {
            //Width value is always more than height value for video from device camera.
            //Orientation mode can be defined by rotation angle of the video
            isPortrait = !strcmp(angle, "90") || !strcmp(angle, "270");
            //For portrait orientation it's needed to invert width and height value
            if (isPortrait) {
                int tmp = w;
                w = h;
                h = tmp;
            }
        } else {
            isPortrait = h > w;
        }

        free(angle);
        free(imgW);
        free(imgH);

        metadata_extractor_destroy(metadata);
    }

    if (w == 0 || h == 0) return;
    // here we get the mode, in which video was filmed, portrait or landscape
    if (isPortrait) {
        elm_layout_file_set(video_wrapper, Application::mEdjPath, "post_portrait_video");
    } else {
        elm_layout_file_set(video_wrapper, Application::mEdjPath, "post_landscape_video");
    }

    evas_object_image_file_set(post_photo, photo->GetThumbnailPath(), NULL);
    elm_object_part_content_set(video_wrapper, "video_box", post_photo);

    if (edje_object_part_geometry_get(elm_layout_edje_get(video_wrapper), "video_box", NULL, NULL, &swallowW, &swallowH)){
        Utils::CropImageToFill(post_photo, swallowW, swallowH, w, h);
    }

    elm_object_signal_emit(video_wrapper, "opacity.true", "");

    elm_box_pack_end(parent, video_wrapper);
    evas_object_show(video_wrapper);
}

void WidgetFactory::CreateLazySharedItem(Evas_Object *parent, ActionAndData *action_data)
{
    OperationPresenter *op = static_cast<OperationPresenter*>(action_data->mData);
    Sptr<SimplePostOperation> oper(static_cast<SimplePostOperation*>(op->GetOperation().Get()));
    ActionAndData *sharedPost = oper->GetSharedActionAndPost();

    CreatePostInPostItem(parent, sharedPost);
    evas_object_show(parent);
}

void WidgetFactory::AddSharedGroupCover(Evas_Object* parent, ActionAndData *action_data)
{
    Evas_Object *cover_footer = elm_layout_add(parent);
    evas_object_size_hint_weight_set(cover_footer, 1, 1);
    elm_layout_file_set(cover_footer, Application::mEdjPath, "group_item_cover");
    elm_box_pack_end(parent, cover_footer);
    evas_object_show(cover_footer);

    action_data->UpdateLayoutAsync(ActionAndData::EImage, cover_footer, Post::EGroupCover);
}

Evas_Object* WidgetFactory::CreateSeparatorItem(Evas_Object* parent, bool isAddToEnd, const char * text, bool translatable)
{
    Evas_Object *item = elm_layout_add(parent);
    evas_object_size_hint_weight_set(item, 1, 1);
    evas_object_size_hint_align_set(item, -1, -1);
    elm_layout_file_set(item, Application::mEdjPath, "header_request_list");

    if (translatable) {
        elm_object_translatable_part_text_set(item, "text", text);
    } else {
        elm_object_part_text_set(item, "text", text);
    }

    if (isAddToEnd) {
        elm_box_pack_end(parent, item);
    } else {
        elm_box_pack_start(parent, item);
    }
    evas_object_show(item);

    return item;
}

Evas_Object *WidgetFactory::CreateConnectivityLostLayout(Evas_Object *parent, const char *part)
{
    Evas_Object *mError = elm_layout_add(parent);
    elm_object_part_content_set(parent, part, mError);
    elm_layout_file_set(mError, Application::mEdjPath, "connection_error_layout_widget");
    evas_object_size_hint_weight_set(mError, 1, 1);
    evas_object_show(mError);

    elm_object_translatable_part_text_set(mError, "con_lost_header", "IDS_SUP_IDS_SUP_NOCONNECT_HEADING");

    Evas_Object *refresh_box = elm_box_add(mError);
    elm_object_part_content_set(mError, "con_lost_box", refresh_box);
    evas_object_size_hint_weight_set(refresh_box, 1, 1);
    evas_object_size_hint_align_set(refresh_box, 0, -1);
    elm_box_horizontal_set(refresh_box, EINA_TRUE);
    evas_object_show(refresh_box);

    Evas_Object *spacer1 = elm_box_add(refresh_box);
    evas_object_size_hint_weight_set(spacer1, 10, 10);
    evas_object_size_hint_align_set(spacer1, -1, -1);
    elm_box_pack_end(refresh_box, spacer1);
    evas_object_show(spacer1);

    Evas_Object *refresh_icon = elm_image_add(refresh_box);
    evas_object_size_hint_weight_set(refresh_icon, 1, 1);
    evas_object_size_hint_align_set(refresh_icon, 0, 0.5);
    evas_object_size_hint_min_set(refresh_icon, 40, 40);
    evas_object_size_hint_max_set(refresh_icon, 40, 40);
    ELM_IMAGE_FILE_SET(refresh_icon, ICON_DIR"/ic_retry_connection.png", NULL);
    elm_box_pack_end(refresh_box, refresh_icon);
    evas_object_show(refresh_icon);

    Evas_Object *refresh_label = elm_label_add(refresh_box);
    evas_object_size_hint_weight_set(refresh_label, 0.5, 0.5);
    evas_object_size_hint_align_set(refresh_label, 0, 0.5);
    const char *refresh_text = WidgetFactory::WrapByFormat2(SANS_MEDIUM_BDC1C9_FORMAT, "32", i18n_get_text("IDS_TAP_TO_RETRY"));
    if (refresh_text) {
        elm_object_text_set(refresh_label, refresh_text);
        delete[] refresh_text;
    }
    elm_box_pack_end(refresh_box, refresh_label);
    evas_object_show(refresh_label);

    Evas_Object *spacer2 = elm_box_add(refresh_box);
    evas_object_size_hint_weight_set(spacer2, 10, 10);
    evas_object_size_hint_align_set(spacer2, -1, -1);
    elm_box_pack_end(refresh_box, spacer2);
    evas_object_show(spacer2);

    return mError;
}

Evas_Object* WidgetFactory::CreateErrorWidget(Evas_Object *parent, const char * title, const char * description, Edje_Signal_Cb func, void *user_data)
{
    Evas_Object * errorWidget = elm_layout_add( parent );
    elm_layout_file_set( errorWidget, Application::mEdjPath, "connection_error_widget_center" );

    Evas_Object *text_part1 = elm_label_add(errorWidget);
    elm_object_part_content_set(errorWidget, "text_part1", text_part1);
    FRMTD_TRNSLTD_TXT_SET2(text_part1, SANS_MEDIUM_CENTER_4e5665_FORMAT, UIRes::GetInstance()->SOMETHING_WRONG_TEXT_SIZE, title);
    elm_label_wrap_width_set(text_part1, UIRes::GetInstance()->SCREEN_SIZE_WIDTH);
    elm_label_line_wrap_set(text_part1, ELM_WRAP_WORD);

    elm_object_translatable_part_text_set(errorWidget, "text_part2", description);
    if (func) {
        elm_object_signal_callback_add(errorWidget, "widget.click", "image", func, user_data);
    }
    evas_object_move(errorWidget, R->CONNECTION_ERROR_WIDGET_MOVE_X, R->CONNECTION_ERROR_WIDGET_MOVE_Y);

    evas_object_show( errorWidget );

    return errorWidget;
}

Evas_Object *WidgetFactory::CreateBaseScreenLayout(ScreenBase *screen, Evas_Object *parent, const char *header_text, bool withBackButton, bool withSearchButton, bool isWhiteBg, bool hasBotBar, bool hasSearchBar)
{
    Evas_Object *layout = CreateLayoutByGroup(parent, "base_screen_layout");

    elm_object_part_text_set(layout, "header_text", header_text);

    if (withBackButton) {
        elm_object_signal_callback_add(layout, "mouse,clicked,*", "back_btn_area", ScreenBase::BackButtonClicked, screen);
    } else {
        elm_object_signal_emit(layout, "back.btn", "hide");
    }

    if (!withSearchButton) {
        elm_object_signal_emit(layout, "search.btn", "hide");
    }

    if (!isWhiteBg) {
        elm_object_signal_emit(layout, "gray.bg", "set");
    }

    if (hasBotBar) {
        elm_object_signal_emit(layout, "entry.show", "default");
    }

    if (hasSearchBar) {
        elm_object_signal_emit(layout, "show.set", "search_bar");
    }

    elm_object_translatable_part_text_set(layout, "error_text", "IDS_SUP_IDS_SUP_NOCONNECT_INFO");

    elm_object_signal_callback_add(layout, "mouse,clicked,*", "search_btn*", ScreenBase::SearchButtonClicked, screen);

    return layout;
}

void WidgetFactory::RefreshBaseScreenLayout(Evas_Object *layout, const char *header_text)
{
    if (layout) {
        if (header_text) {
            elm_object_part_text_set(layout, "header_text", header_text);
        }
    }
}

Evas_Object *WidgetFactory::CreateChooseScreenLayout(ScreenBase *screen, Evas_Object *parent, const char *header_text)
{
    Evas_Object *layout = CreateLayoutByGroup(parent, "choose_base_layout");

    elm_object_translatable_part_text_set(layout, "header_text", header_text);

    elm_object_signal_emit(layout, "search.btn", "hide");

    elm_object_translatable_part_text_set(layout, "error_text", "IDS_SUP_IDS_SUP_NOCONNECT_INFO");

    elm_object_signal_callback_add(layout, "mouse,clicked,*", "back_btn*", ScreenBase::BackButtonClicked, screen);
    elm_object_signal_callback_add(layout, "mouse,clicked,*", "search_btn*", ScreenBase::SearchButtonClicked, screen);

    return layout;
}

Evas_Object *WidgetFactory::CreateChooseScreenEntry(Evas_Object *parent)
{
    Evas_Object *searchEntry = elm_entry_add(parent);
    elm_entry_prediction_allow_set(searchEntry, EINA_FALSE);
    elm_object_part_content_set(parent, "entry_field", searchEntry);
    elm_entry_single_line_set(searchEntry, EINA_TRUE);
    elm_entry_scrollable_set(searchEntry, EINA_TRUE);
    elm_entry_input_panel_return_key_type_set(searchEntry, ELM_INPUT_PANEL_RETURN_KEY_TYPE_SEARCH);
    char *buf = WidgetFactory::WrapByFormat2(SANS_REGULAR_a0a3a7_FORMAT, "36", i18n_get_text("IDS_SEARCH"));
    if (buf) {
        elm_object_part_text_set(searchEntry, "elm.guide", buf);
        delete[] buf;
    }
    elm_entry_text_style_user_push(searchEntry, COMPOSER_SEARCH_TEXT_STYLE);
    elm_entry_cnp_mode_set(searchEntry, ELM_CNP_MODE_PLAINTEXT);
    evas_object_show(searchEntry);

    return searchEntry;
}

Evas_Object *WidgetFactory::CreateChooseScreenItem(Evas_Object *parent, ActionAndData *actionData)
{
    Evas_Object *list_item = NULL;
    if (actionData) {
        const char *picturePath = NULL;
        const char *name = NULL;
        GraphObject *check = static_cast<GraphObject*>(actionData->mData);
        if (check->GetGraphObjectType() == GraphObject::GOT_FRIEND) {
            Friend *item = static_cast<Friend*>(actionData->mData);
            picturePath = strdup(item->mPicturePath.c_str());
            name = strdup(item->mName.c_str());
        } else if (check->GetGraphObjectType() == GraphObject::GOT_GROUP) {
            Group *item = static_cast<Group*>(actionData->mData);
            picturePath = SAFE_STRDUP(item->mIcon);
            name = SAFE_STRDUP(item->mName);
        } else {
            Log::error("WidgetFactory::CreateChooseScreenItem = cound not create item");
            return NULL;
        }

        list_item = WidgetFactory::CreateLayoutByGroup(parent, "see_all_groups_item");

        if (picturePath) {
            Evas_Object *avatar = elm_image_add(list_item);
            elm_object_part_content_set(list_item, "item_icon", avatar);
            actionData->UpdateImageLayoutAsync(picturePath, avatar, ActionAndData::EImage, Post::EFromPicturePath);
            evas_object_show(avatar);
        }

        elm_object_part_text_set(list_item, "item_name", name);

        free((void *) picturePath);
        free((void *) name);
    }
    return list_item;
}

Evas_Object* WidgetFactory::CreateSimpleWrapper(Evas_Object* parent)
{

    Evas_Object *wrapperBox = elm_box_add(parent);
    evas_object_size_hint_weight_set(wrapperBox, 1, 1);
    evas_object_size_hint_align_set(wrapperBox, -1, 0);
    elm_box_padding_set(wrapperBox, 0, 0);

    return wrapperBox;
}

void WidgetFactory::CreateFriendItemEmulator(Evas_Object *parent, const char* format, int emulatorWidth)
{
    DeleteFriendItemEmulator();
    mEntryEmulator = elm_entry_add(parent);
    elm_entry_single_line_set(mEntryEmulator, false);
    evas_object_resize(mEntryEmulator, emulatorWidth, 1);
    evas_object_size_hint_weight_set(mEntryEmulator, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
    elm_entry_text_style_user_push(mEntryEmulator, format);
}

void WidgetFactory::DeleteFriendItemEmulator()
{
    if (mEntryEmulator) {
        evas_object_del(mEntryEmulator);
        mEntryEmulator = nullptr;
    }
}

int WidgetFactory::GetEntryEmulatorLines(std::string &strText, int itemWidth)
{
    int lines = 0;
    if (mEntryEmulator) {
        std::vector < std::string > splitResult = Utils::SplitString(strText.c_str(), " ");
        strText = "";
        Evas_Object* tb = elm_entry_textblock_get(mEntryEmulator);
        int w = 0;
        int splitResultCount = splitResult.size();
        for(int i = 0; i < splitResultCount; ++i) {
            elm_entry_entry_set(mEntryEmulator, splitResult[i].c_str());
            evas_object_textblock_size_native_get(tb, &w, nullptr);
            strText.append(splitResult[i].c_str());
            if (w >= itemWidth) {
                break;
            }
            if (i != splitResultCount - 1) {
                strText.append(" ");
            }
        }
        elm_entry_entry_set(mEntryEmulator, strText.c_str());
        lines = Utils::GetLinesNumber(mEntryEmulator);
    }
    return lines;
}

Evas_Object *WidgetFactory::CreateFriendItem(Evas_Object *parent, ActionAndData *actionData, bool isAddToEnd)
{
    CHECK_RET(actionData, nullptr);
    Friend *friendData = static_cast<Friend*>(actionData->mData);
    CHECK_RET(friendData, nullptr);
    if (FriendOperation::IsOperationStarted(friendData->GetId())) {
        return nullptr;
    }

    Evas_Object *list_item = WidgetFactory::CreateLayoutByGroup(parent, "ff_friends_list");
    actionData->mActionObj = list_item;

    Evas_Object *avatar = elm_image_add(list_item);
    elm_object_part_content_set(list_item, "item_avatar", avatar);
    actionData->UpdateImageLayoutAsync(friendData->GetPicturePath().c_str(), avatar);
    evas_object_show(avatar);

    elm_object_translatable_part_text_set(list_item, "friend_btn_text", "IDS_FFS_FRIENDS");
    elm_object_translatable_part_text_set(list_item, "add_friend_btn_text", "IDS_ADD_FRIEND");
    elm_object_translatable_part_text_set(list_item, "cancel_friend_btn_text", "IDS_CANCEL");

    Evas_Object *name_spacer = elm_box_add(list_item);
    elm_object_part_content_set(list_item, "item_name_spacer", name_spacer);
    evas_object_size_hint_weight_set(name_spacer, 1, 1);
    evas_object_size_hint_align_set(name_spacer, -1, 0.5);
    elm_box_pack_end(list_item, name_spacer);
    evas_object_show(name_spacer);

    Evas_Object *spacer1 = elm_box_add(name_spacer);
    evas_object_size_hint_weight_set(spacer1, 10, 10);
    evas_object_size_hint_align_set(spacer1, -1, -1);
    elm_box_pack_end(name_spacer, spacer1);
    evas_object_show(spacer1);

    if (!mEntryEmulator) {
        CreateFriendItemEmulator(parent, R->FF_ITEM_FRIEND_TEXT_STYLE, R->FF_SEARCH_RESULT_WRAP_WIDTH);
    }

    std::string strName = friendData->GetName().c_str();
    int lines = GetEntryEmulatorLines(strName, R->FF_SEARCH_RESULT_WRAP_WIDTH);

    if (lines == 2) {
        elm_object_signal_emit(list_item, "2_lines", "");
    } else if (lines >= 3) {
        elm_object_signal_emit(list_item, "3_lines", "");
    }

    Evas_Object *friendName = elm_entry_add(name_spacer);
    elm_entry_editable_set(friendName, EINA_FALSE);
    FRMTD_TRNSLTD_ENTRY_TXT(friendName, R->FF_ITEM_FRIEND_TEXT_LTR_STYLE, R->FF_ITEM_FRIEND_TEXT_RTL_STYLE);
    elm_object_part_content_set(list_item, "friend_name_text", friendName);
    elm_entry_entry_set(friendName, strName.c_str());

    int count = friendData->GetMutualFriendsCount();
    if (count > 0) {
        Evas_Object *mutuals = elm_label_add(name_spacer);
        std::string format = PresenterHelpers::CreateMutualFriendsFormat(count, R->FF_SEARCH_RESULT_MUTUAL_TEXT_SIZE);
        if (count == 1) {
           FRMTD_TRNSLTD_TXT_SET1(mutuals, format.c_str(), "IDS_SINGLE_MUTUAL_FRIEND");
        } else {
           FRMTD_TRNSLTD_TXT_SET1(mutuals, format.c_str(), "IDS_MUTUAL_FRIENDS");
        }
        elm_object_part_content_set(list_item, "mutual_friends", mutuals);
        evas_object_show(mutuals);
    }

    Evas_Object *spacer2 = elm_box_add(name_spacer);
    evas_object_size_hint_weight_set(spacer2, 10, 10);
    evas_object_size_hint_align_set(spacer2, -1, -1);
    elm_box_pack_end(name_spacer, spacer2);
    evas_object_show(spacer2);

    if (isAddToEnd) {
        elm_box_pack_end(parent, list_item);
    } else {
        elm_box_pack_start(parent, list_item);
    }
    evas_object_show(list_item);

    return list_item;
}

Evas_Object* WidgetFactory::CreateNoDataWidget(Evas_Object *parent, const char * title, const char * description)
{
    Evas_Object * noDataWidget = elm_layout_add( parent );
    elm_layout_file_set( noDataWidget, Application::mEdjPath, "no_data_widget_center" );

    Evas_Object *text_part1 = elm_label_add(noDataWidget);
    elm_object_part_content_set(noDataWidget, "text_part1", text_part1);
    char *text = WidgetFactory::WrapByFormat2(SANS_MEDIUM_CENTER_4e5665_FORMAT, UIRes::GetInstance()->SOMETHING_WRONG_TEXT_SIZE, title);
    if (text) {
        elm_object_text_set(text_part1, text);
        delete[] text;
    }
    elm_label_wrap_width_set(text_part1, UIRes::GetInstance()->SCREEN_SIZE_WIDTH);
    elm_label_line_wrap_set(text_part1, ELM_WRAP_WORD);

    elm_object_translatable_part_text_set(noDataWidget, "text_part2", description);

    evas_object_move(noDataWidget, R->CONNECTION_ERROR_WIDGET_MOVE_X, R->CONNECTION_ERROR_WIDGET_MOVE_Y);

    evas_object_show(noDataWidget);

    return noDataWidget;
}
