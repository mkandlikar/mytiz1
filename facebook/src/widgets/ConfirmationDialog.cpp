#include "Application.h"
#include "ConfirmationDialog.h"
#include "FacebookSession.h"
#include "Log.h"

#include <Elementary.h>


ConfirmationDialog::ConfirmationDialog(Evas_Object *parent, const char* caption, const char* description,
        const char* yesText, const char* noText, ConfirmationDialogCB callBack, void *userData) : mConfirmationDialogCb(callBack),
        mCbData(userData) {

    mConfirmationPopup = elm_popup_add(parent);
    elm_popup_align_set(mConfirmationPopup, ELM_NOTIFY_ALIGN_FILL, 0.5);

    evas_object_smart_callback_add(mConfirmationPopup, "block,clicked", dismiss_cb, this);

    Evas_Object *layout = elm_layout_add(mConfirmationPopup);
    if (!caption) {
        elm_layout_file_set(layout, Application::mEdjPath, "confirmation_dialog_nocaption");
        elm_object_content_set(mConfirmationPopup, layout);
    } else {
        elm_layout_file_set(layout, Application::mEdjPath, "confirmation_dialog");
        elm_object_content_set(mConfirmationPopup, layout);
        elm_object_translatable_part_text_set(layout, "top_text", caption);
    }
    elm_object_translatable_part_text_set(layout, "description", description);

    if (noText) {
        elm_object_translatable_part_text_set(layout, "text.discard", noText);
        elm_object_signal_callback_add(layout, "mouse,clicked,*", "discard", on_no_clicked, this);
    }

    if (yesText) {
        elm_object_translatable_part_text_set(layout, "text.keep", yesText);
        elm_object_signal_callback_add(layout, "mouse,clicked,*", "keep", on_yes_clicked, this);
    }

    evas_object_show(layout);

    evas_object_show(mConfirmationPopup);
}

ConfirmationDialog::~ConfirmationDialog() {
    Close();
}

ConfirmationDialog *ConfirmationDialog::CreateAndShow(Evas_Object *parent, const char* caption, const char* description,
        const char* yesText, const char* noText, ConfirmationDialogCB callBack, void *userData) {
    Log::info("ConfirmationDialog::CreateAndShow");
    AppEvents::Get().Notify(eCONFIRMATION_DIALOG_SHOW_EVENT, nullptr);
    return new ConfirmationDialog(parent, caption, description, yesText, noText, callBack, userData);
}

bool ConfirmationDialog::Close() {
    bool res = false;
    evas_object_hide(mConfirmationPopup);
    if (mConfirmationPopup) {
        evas_object_del(mConfirmationPopup);
        mConfirmationPopup = NULL;
        res = true;
    }
    return res;
}

void ConfirmationDialog::on_yes_clicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    ConfirmationDialog *me = static_cast<ConfirmationDialog *> (data);
    me->mConfirmationDialogCb(me->mCbData, ECDYesPressed);
}

void ConfirmationDialog::on_no_clicked(void *data, Evas_Object *obj, const char *emission, const char *source) {
    ConfirmationDialog *me = static_cast<ConfirmationDialog *> (data);
    me->mConfirmationDialogCb(me->mCbData, ECDNoPressed);
}

void ConfirmationDialog::dismiss_cb(void *data, Evas_Object *obj, void *event_info){
    ConfirmationDialog *me = static_cast<ConfirmationDialog *> (data);
    me->mConfirmationDialogCb(me->mCbData, ECDDismiss);
}
