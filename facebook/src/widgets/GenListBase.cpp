#include "GenListBase.h"

/*
 * Class GenListItemBase
 */

GenListItemBase::GenListItemBase(Elm_Genlist_Item_Class *itemClass) : mItem(NULL), mGenList(NULL), mIsSelectable(true), mItemClass(itemClass) {
    if (!mItemClass) {
        mItemClass = elm_genlist_item_class_new();
        mItemClass->item_style = "full";
        mItemClass->func.text_get = NULL;
        mItemClass->func.content_get = GenListItemBase::CreateLayout;
        mItemClass->func.state_get = NULL;
        mItemClass->func.del = NULL;
    }
}

void GenListItemBase::ShowItem() {
    elm_genlist_item_show(mItem, ELM_GENLIST_ITEM_SCROLLTO_NONE);
}


/*
 * Class GenListGroupItemBase
 */

GenListGroupItemBase::~GenListGroupItemBase() {
    void *listData;
    EINA_LIST_FREE(mItems, listData) {
        delete (GenListItemBase*) listData;
    }
}

int GenListGroupItemBase::compare(const void *data1, const void *data2) {
    GenListItemBase* item1 = (GenListItemBase*) data1;
    GenListItemBase* item2 = (GenListItemBase*) data2;
    return item1->Compare(item2);
}

Elm_Object_Item *GenListGroupItemBase::AddItem(GenListItemBase* item) {
    item->SetGenList(mGenList);
    return AddToGenList(item);
}

Elm_Object_Item *GenListGroupItemBase::AddToGenList(GenListItemBase* item) {
    mItems = eina_list_sorted_insert(mItems, compare, item);
    Eina_List *l;
    void *listData;
    Elm_Object_Item *insertAfterItem = NULL;
    if (mItem) {
        insertAfterItem = mItem;
    }
    EINA_LIST_FOREACH(mItems, l, listData) {
        GenListItemBase *currentItem = (GenListItemBase *) listData;
        if (currentItem == item) break;
        if (currentItem->GetLastItem()) {
            insertAfterItem = currentItem->GetLastItem();
        }
    }
    if (insertAfterItem) {
        item->InsertAfter(insertAfterItem);
    } else {
        item->Prepend();
    }
    return item->mItem;
}

void GenListGroupItemBase::RemoveItem(GenListItemBase* item) {
//AAA ToDo: implement support for removing entire sub-groups with sub-items
    item->Remove();
    mItems = eina_list_remove(mItems, item);
    delete item;
}

void GenListGroupItemBase::ApplyFilter() {
    Filter(NULL);
}

bool GenListGroupItemBase::Filter(Elm_Object_Item *insertAfter) {
    bool isGroupEmpty = true;
    Elm_Object_Item *prevItem = mItem;
    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(mItems, l, listData) {
        GenListItemBase *item = (GenListItemBase *) listData;
        if (item->Filter(prevItem)) { //then remove it form GenList
            if (item->mItem != NULL) {
                item->Remove();
            }
        } else { //else add it back to GenList
            if (!mIsRoot) {
                if (mItem == NULL) {
                    if (insertAfter) {
                        InsertAfter(insertAfter);
                    } else {
                        Prepend();
                    }
                    prevItem = mItem;
                }
            }
            if (item->mItem == NULL) {
                if (prevItem) {
                    item->InsertAfter(prevItem);
                } else {
                    item->Prepend();
                }
            }
            isGroupEmpty = false;
        }
        Elm_Object_Item *last = item->GetLastItem();
        if (last) prevItem = last;
    }
    return isGroupEmpty && mIsFilteredIfEmpty;
}

Elm_Object_Item *GenListGroupItemBase::GetLastItem() {
    GenListItemBase *last = NULL;
    if (mItem) {
        last = this;
    }
    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(mItems, l, listData) {
        GenListItemBase *currentItem = (GenListItemBase *) listData;
        if (currentItem->mItem) {
            last = currentItem;
        }
    }
    if (last) {
        return last->mItem;
    } else {
        return NULL;
    }
}

bool GenListGroupItemBase::IsGroupFiltered() {
    return (NULL == GetLastItem());
}

/*
 * Class GenList
 */
GenList::GenList(Evas_Object *parent) {
        mIsRoot = true;
        mGenList = elm_genlist_add(parent);
        evas_object_show(mGenList);
    }

Evas_Object *GenList::GetGenList() {
    return mGenList;
}

GenList::~GenList() {
    elm_genlist_clear(mGenList);
}

