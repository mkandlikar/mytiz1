#include "Application.h"
#include "ConnectivityManager.h"
#include "OwnProfileScreen.h"
#include "Popup.h"
#include "PresenterHelpers.h"
#include "ProfileScreen.h"
#include "SelectUserScreen.h"
#include "User.h"
#include "UserItemWidget.h"
#include "Utils.h"
#include "WidgetFactory.h"

#include <assert.h>

UIRes * UserItemWidget::R = UIRes::GetInstance();

void ItemWidgetBase::DestroyLayout() {
    if (mLayout) {
        evas_object_del(mLayout);
        mLayout = NULL;
    }
}

UserItemWidget::UserItemWidget(ActionAndData *actionAndData) : mAnD(actionAndData) {
}

UserItemWidget::~UserItemWidget() {
    delete mAnD;
    DestroyLayout();
}

Evas_Object *UserItemWidget::CreateLayout(Evas_Object* parent) {
    User *user = static_cast<User *> (mAnD->mData);
    assert(user);
    if (!user) {
        return nullptr;
    }

    mLayout = WidgetFactory::CreateLayoutByGroup(parent, "ff_friends_list");
    mAnD->mActionObj = mLayout;

    Evas_Object *avatar = elm_image_add(mLayout);
    elm_object_part_content_set(mLayout, "item_avatar", avatar);
    mAnD->UpdateImageLayoutAsync(user->mPicture->mUrl, avatar);
    evas_object_show(avatar);

    elm_object_translatable_part_text_set(mLayout, "friend_btn_text", "IDS_FFS_FRIENDS");
    elm_object_translatable_part_text_set(mLayout, "add_friend_btn_text", "IDS_ADD_FRIEND");
    elm_object_translatable_part_text_set(mLayout, "cancel_friend_btn_text", "IDS_CANCEL");

    Evas_Object *name_spacer = elm_box_add(mLayout);
    elm_object_part_content_set(mLayout, "item_name_spacer", name_spacer);
    evas_object_size_hint_weight_set(name_spacer, 1, 1);
    evas_object_size_hint_align_set(name_spacer, -1, 0.5);
    elm_box_pack_end(mLayout, name_spacer);
    evas_object_show(name_spacer);

    Evas_Object *spacer1 = elm_box_add(name_spacer);
    evas_object_size_hint_weight_set(spacer1, 10, 10);
    evas_object_size_hint_align_set(spacer1, -1, -1);
    elm_box_pack_end(name_spacer, spacer1);
    evas_object_show(spacer1);

    std::string strName = user->mName;
    int lines = WidgetFactory::GetEntryEmulatorLines(strName, R->FF_SEARCH_RESULT_WRAP_WIDTH);

    if (lines == 2) {
        elm_object_signal_emit(mLayout, "2_lines", "");
    } else if (lines >= 3) {
        elm_object_signal_emit(mLayout, "3_lines", "");
    }

    Evas_Object *userName = elm_entry_add(name_spacer);
    elm_entry_editable_set(userName, EINA_FALSE);
    FRMTD_TRNSLTD_ENTRY_TXT(userName, R->FF_ITEM_FRIEND_TEXT_LTR_STYLE, R->FF_ITEM_FRIEND_TEXT_RTL_STYLE);
    elm_object_part_content_set(mLayout, "friend_name_text", userName);
    elm_entry_entry_set(userName, strName.c_str());

    int count = user->mMutualFriendsCount;
    if (count > 0) {
        Evas_Object *mutuals = elm_label_add(name_spacer);
        std::string format = PresenterHelpers::CreateMutualFriendsFormat(count, R->FF_SEARCH_RESULT_MUTUAL_TEXT_SIZE);
        FRMTD_TRNSLTD_TXT_SET1(mutuals, format.c_str(), (count == 1 ? "IDS_SINGLE_MUTUAL_FRIEND" : "IDS_MUTUAL_FRIENDS"));
        elm_object_part_content_set(mLayout, "mutual_friends", mutuals);
        evas_object_show(mutuals);
    }

    Evas_Object *spacer2 = elm_box_add(name_spacer);
    evas_object_size_hint_weight_set(spacer2, 10, 10);
    evas_object_size_hint_align_set(spacer2, -1, -1);
    elm_box_pack_end(name_spacer, spacer2);
    evas_object_show(spacer2);

    elm_object_signal_callback_add(mLayout, "mouse,clicked,*", "friend_*", on_friends_button_clicked_cb, this);
    elm_object_signal_callback_add(mLayout, "mouse,clicked,*", "add_friend_*", on_add_friend_btn_clicked_cb, this);
    elm_object_signal_callback_add(mLayout, "mouse,clicked,*", "cancel_friend_*", on_cancel_friend_btn_clicked_cb, this);
    RefreshButton(user->mFriendshipStatus);
    elm_object_signal_callback_add(mLayout, "mouse,clicked,*", "item_*", on_item_clicked_cb, this);

    evas_object_show(mLayout);
    return mLayout;
}


void UserItemWidget::RefreshButton(Person::FriendshipStatus status) {
    elm_object_signal_emit(mAnD->mActionObj, (status == Person::eARE_FRIENDS ? "show.set" : "hide.set"), "friend");
    elm_object_signal_emit(mAnD->mActionObj, (status == Person::eCAN_REQUEST ? "show.set" : "hide.set"), "add");
    elm_object_signal_emit(mAnD->mActionObj, (status == Person::eOUTGOING_REQUEST ? "show.set" : "hide.set"), "cancel");
}

void UserItemWidget::on_add_friend_btn_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source) {
    UserItemWidget *me = static_cast<UserItemWidget *> (user_data);

    /* Animation */
    me->RefreshButton(Person::eOUTGOING_REQUEST);
    ActionBase::on_add_friend_btn_clicked_cb(me->mAnD, obj, emission, source);
}

void UserItemWidget::on_cancel_friend_btn_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source) {
    UserItemWidget *me = static_cast<UserItemWidget *> (user_data);

    /* Animation */
    me->RefreshButton(Person::eCAN_REQUEST);
    ActionBase::on_cancel_friend_btn_clicked_cb(me->mAnD, obj, emission, source);
}

void UserItemWidget::on_friends_button_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source) {
    UserItemWidget *me = static_cast<UserItemWidget*>(user_data);
    ActionAndData *actionAndData = me->mAnD;

    if (actionAndData && actionAndData->mData) {
        User *user = static_cast<User*>(actionAndData->mData);

        if (user->mFriendshipStatus == Person::eARE_FRIENDS) {
            static PopupMenu::PopupMenuItem context_menu_items[] = {
                { nullptr, "IDS_UNFRIEND", nullptr,
                  ActionBase::on_unfriend_friend_btn_clicked_cb, nullptr,
                  0, false, false
                },
                { nullptr, "IDS_BLOCK", nullptr,
                  ActionBase::on_block_friend_btn_clicked_cb, nullptr,
                  1, true, false
                },
            };
            context_menu_items[0].data = actionAndData;
            context_menu_items[1].data = actionAndData;

            Evas_Coord y = 0, h = 0;
            evas_object_geometry_get(obj, nullptr, &y, nullptr, &h);
            y += h / 2;
            ScreenBase *topScreen = Application::GetInstance()->GetTopScreen();
            PopupMenu::Show(2, context_menu_items, topScreen->getMainLayout(), false, y);
        }
    }
}

void UserItemWidget::on_item_clicked_cb(void *user_data, Evas_Object *obj, const char *emission, const char *source) {
    if (Application::GetInstance()->GetTopScreenId() != ScreenBase::SID_USER_PROFILE) {
        UserItemWidget *me = static_cast<UserItemWidget *> (user_data);
        ActionAndData *actionAndData = me->mAnD;
        if (actionAndData && actionAndData->mData) {
            Utils::OpenProfileScreen(actionAndData->mData->GetId());
        }
    }
}
