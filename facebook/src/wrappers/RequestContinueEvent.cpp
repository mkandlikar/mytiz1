/*
 * RequestContinueEvent.cpp
 *
 *  Created on: Dec 7, 2015
 *      Author: dvasin
 */

#ifndef REQUESTCONTINUEEVENT_CPP_
#define REQUESTCONTINUEEVENT_CPP_
#include <stdlib.h>
#include <string.h>
#include "Common.h"
#include "RequestContinueEvent.h"

RequestContinueEvent::RequestContinueEvent(const char *request, bool isDescendingOrder, bundle *param):
mRequest(NULL), mIsDescendingOrder(false), mParam(NULL) {
    mRequest = SAFE_STRDUP(request);
    mIsDescendingOrder = isDescendingOrder;
    if(param) {
        mParam = bundle_dup(param);
    }
}

RequestContinueEvent::~RequestContinueEvent() {
    free(mRequest);
    if(mParam) {
        bundle_free(mParam);
    }
}

#endif /* REQUESTCONTINUEEVENT_CPP_ */
