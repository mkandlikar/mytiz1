#include <stdlib.h>
#include <string.h>
#include "DataEventDescription.h"

DataEventDescription::DataEventDescription(AbstractDataProvider* provider, DataEventEnum event, const char *itemId, bool addedToEnd, Operation::OperationType operationType) {
    mProvider = provider;
    mEvent = event;
    mItemId = (itemId) ? strdup(itemId) : nullptr;
    mAddedToEnd = addedToEnd;
    mOperationType = operationType;
}

DataEventDescription::~DataEventDescription() {
    free(mItemId);
}

