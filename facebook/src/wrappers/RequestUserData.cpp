#include <dlog.h>
#include "RequestUserData.h"
#include "curlutilities.h"

RequestUserData::RequestUserData(const char *RequestUrl, const char *MultipartFileUploadName
        ,bundle *ParamsBundle, bundle *BodyParamsBundle, GraphRequest::HttpMethod requestHttpMethod
        ,Sptr<GraphRequest> Request, bool IsHeaderReq, const char *EtagsHeader):
        mParentRequest(Request) {
    Init();

    mRequestUrl = SAFE_STRDUP(RequestUrl);
    mMultipartFileUploadName = SAFE_STRDUP(MultipartFileUploadName);
    mRequestBuffer = GraphRequest::PrepareMemoryStructFromBundle(ParamsBundle);
    mBodyParamsBundle = (BodyParamsBundle != NULL) ? bundle_dup(BodyParamsBundle) : NULL;
    mResponseBuffer = (MemoryStruct *) calloc(1, sizeof(MemoryStruct));
    httpMethod = requestHttpMethod;
    mIsCancelled = false;
    mThrdEtagsHeader = EtagsHeader;
    mIsHeaderReq = IsHeaderReq;
}

RequestUserData::RequestUserData(const char *RequestUrl, const char *RequestPayload,
        RequestUserData_CB callbackFunc, void *callbackData):
        mParentRequest(nullptr) {
    Init();
    if((RequestUrl != NULL) && (RequestPayload != NULL)) {
        mRequestUrl = SAFE_STRDUP(RequestUrl);
        mRequestBuffer = (MemoryStruct *) calloc(1, sizeof(MemoryStruct));
        if(RequestPayload) {
            mRequestBuffer->memory = strdup(RequestPayload);
            mRequestBuffer->size = strlen(RequestPayload);
        }
        mResponseBuffer = (MemoryStruct *) calloc(1, sizeof(MemoryStruct));
        callbackObject = callbackData;
        callback = callbackFunc;
    }
}

RequestUserData::~RequestUserData() {
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "RequestUserData::~RequestUserData start");
    if (mParentRequest) {
        mParentRequest->mIsThreadCompleted = true;
        mParentRequest->mThreadData = NULL;
    }
    free((void*) mRequestUrl);
    mRequestUrl = NULL;
    free((void*) mMultipartFileUploadName);
    mMultipartFileUploadName = NULL;

    if (mRequestBuffer != NULL) {
        free(mRequestBuffer->memory);
        mRequestBuffer->memory = NULL;
        free(mRequestBuffer);
        mRequestBuffer = NULL;
    }
    if (mBodyParamsBundle != NULL) {
        bundle_free(mBodyParamsBundle);
        mBodyParamsBundle = NULL;
    }
    free(mResponseBuffer);
    mResponseBuffer = NULL;
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "RequestUserData::~RequestUserData end");
}

void RequestUserData::Init() {
    mRequestUrl = NULL;
    httpMethod = GraphRequest::GET;
    mIsCancelled = false;
    mMultipartFileUploadName = NULL;
    mRequestBuffer = NULL;
    mBodyParamsBundle = NULL;
    mIsHeaderReq = false;
    mThrdEtagsHeader = NULL;

    //out
    mResponseBuffer = NULL;
    curlCode = CURLE_OK;

    callback = NULL;
    callbackObject = NULL;
}
