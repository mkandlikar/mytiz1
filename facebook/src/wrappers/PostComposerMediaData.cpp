#include "Application.h"
#include "Common.h"
#include "Log.h"
#include "PostComposerMediaData.h"

PostComposerMediaData::PostComposerMediaData(const char * path, const char * thumbnailPath, media_content_type_e type, const char * mediaId,
        unsigned long long fileSize, bool app_ctrl, const char *imageUrl) : Photo(), mIndex(0), mCaption(NULL),
        mOriginalCaption(NULL), mCaptionLayout(NULL), mTextList(NULL), mImageObject(NULL), mGengridItem(NULL),
        mPhotoId(NULL)
{
    SetFilePath(path);
    mOriginalPath = path;
    mThumbnailPath = thumbnailPath;
    mOriginalThumbnailPath = thumbnailPath;
    mType = type;
    mMediaId = mediaId;
    mFileSize = fileSize;
    mMediaData_AppCtrl = app_ctrl;
    mAbleToDecode = DECODE_STATUS_UNKNOWN;
    mCropParams.saved = false;
    mHeight = 0;
    mWidth = 0;
    mTempCropImagePath = nullptr;
    mTagging = nullptr;
    mImageUrl = SAFE_STRDUP(imageUrl);
    mImageLayout = nullptr;
    mTextToCaption = nullptr;
}

PostComposerMediaData::PostComposerMediaData(const PostComposerMediaData & other) : Photo(other), mCaptionLayout(NULL), mTextList(NULL),
        mImageObject(NULL), mGengridItem(NULL), mImageLayout(NULL), mTagging(NULL) {
    mOriginalPath = SAFE_STRDUP(other.mOriginalPath);
    mThumbnailPath = SAFE_STRDUP(other.mThumbnailPath);
    mOriginalThumbnailPath = (other.mThumbnailPath == other.mOriginalThumbnailPath)
            ? mThumbnailPath
            : SAFE_STRDUP(other.mOriginalThumbnailPath);
    mType = other.mType;
    mIndex = other.mIndex;
    mCaption = SAFE_STRDUP(other.mCaption);
    mOriginalCaption = SAFE_STRDUP(other.mOriginalCaption);
    mMediaId = SAFE_STRDUP(other.mMediaId);
    mFileSize = other.mFileSize;
    mPhotoId = SAFE_STRDUP(other.mPhotoId);
    mMediaData_AppCtrl = other.mMediaData_AppCtrl;
    mAbleToDecode = other.mAbleToDecode;
    mHeight = other.mHeight;
    mWidth = other.mWidth;
    mImageUrl = SAFE_STRDUP(other.mImageUrl);
    mTextToCaption = SAFE_STRDUP(other.mTextToCaption);
    SetCropParams(other.mCropParams.x, other.mCropParams.y, other.mCropParams.w, other.mCropParams.h, other.mCropParams.orientation);
    mTempCropImagePath = SAFE_STRDUP(other.mTempCropImagePath);
}

int PostComposerMediaData::media_data_comparator(const void *data1, const void *data2) {
    const PostComposerMediaData *mediaData1 = static_cast<const PostComposerMediaData *> (data1);
    const PostComposerMediaData *mediaData2 = static_cast<const PostComposerMediaData *> (data2);
    return mediaData1 && mediaData2
            && (!strcmp(mediaData1->GetMediaId(), mediaData2->GetMediaId())
                    || !strcmp(mediaData1->GetOriginalPath(), mediaData2->GetOriginalPath())) // for cropped/rotated images
            ? 0 : -1;
}

void PostComposerMediaData::CreateTagging() {
    Evas_Object *caption = elm_object_part_content_get(mCaptionLayout, "caption");
    if (caption) {
        mTagging = new FriendTagging(mCaptionLayout, caption);
    }
}

void PostComposerMediaData::RemoveCaptionLayout() {
    DeleteTagging();
    if (mCaptionLayout) {
        evas_object_del(mCaptionLayout);
        mCaptionLayout = nullptr;
    }
}

void PostComposerMediaData::UpdateCaption() {
    Evas_Object *captionLayout = GetCaptionLayout();
    if (captionLayout && GetTagging()) {
        Evas_Object *captionEntry = elm_layout_content_get(captionLayout, "caption");
        const char *captionTxt = elm_entry_entry_get(captionEntry);
        char *entryText = GetTagging()->GetStringToPost(true);
        char *text = GetTagging()->GetStringToPost(false);
        SetTextToCaption(text);
        SetCaption(entryText);
        free(entryText);
        free(text);
        SetPhotoMessageTagsList(GetTagging()->GetMessageTagsList(captionTxt));
    }
}

bool PostComposerMediaData::IsCaptionChanged() {
    // Returns "true" when mOriginalCaption doesn't match mCaption, there are 3 possible cases:
    return ((!mOriginalCaption || *mOriginalCaption == '\0') && (mCaption && *mCaption != '\0'))     // caption was empty and then gets updated
            || ((mOriginalCaption && *mOriginalCaption != '\0') && (!mCaption || *mCaption == '\0')) // caption is removed
            || (mOriginalCaption && mCaption && strcmp(mOriginalCaption, mCaption));                 // caption is edited
}

PostComposerMediaData::~PostComposerMediaData() {
    if(mMediaData_AppCtrl) {//The operation was a result of app control and not through the user composing via the app
        Log::debug("Image posted %s", GetFilePath());
        free((void *)mThumbnailPath);
        // Hide the application, so that previous application is up
        // Application::GetInstance()->GoToBackground();
    } else {
        if (mOriginalPath && GetFilePath() && strcmp(mOriginalPath, GetFilePath())) {
            Utils::DeleteMediaFileFromDB(mMediaId);
           // file will be deleted when Composer is created next time
           // remove(mPath);
        }

        free((void *)mMediaId);
        free((void *)mOriginalPath);
        free((char *)mTempCropImagePath);
        if (mOriginalThumbnailPath != mThumbnailPath) {
            remove(mThumbnailPath);
            free((void *)mThumbnailPath);
        }
        free((void *)mOriginalThumbnailPath);
        free((void *)mCaption);
        free((void *)mOriginalCaption);
        free(mTextToCaption);
        free(mImageUrl);

        RemoveCaptionLayout();

        if (mTextList) {
            void * listData;
            EINA_LIST_FREE(mTextList, listData) {
                delete static_cast<PostComposerMediaText *>(listData);
            }
        }
        free((void *)mPhotoId);
    }
}

void PostComposerMediaData::DeleteTagging() {
    delete mTagging;
    mTagging = nullptr;
}

PostComposerMediaText::PostComposerMediaText(char * text, int colorR, int colorG, int colorB) :
        mPositionX(0), mPositionY(0), mWidth(0), mHeight(0), mOriginalWidth(0), mImagePath(NULL), mAngle(0.0)
{
    mText = text;
    mColorR = colorR;
    mColorG = colorG;
    mColorB = colorB;
}

PostComposerMediaText::~PostComposerMediaText()
{
    free(mText);
    remove(mImagePath);
    free(mImagePath);
}

void PostComposerMediaText::GetPosition(int *x, int *y)
{
    if (x)
        *x = mPositionX;

    if (y)
        *y = mPositionY;
}

void PostComposerMediaText::SetSize(int w, int h)
{
    if (!mOriginalWidth) {
        mOriginalWidth = w;
    }
    mWidth = w;
    mHeight = h;
}

void PostComposerMediaText::GetSize(int *w, int *h)
{
    if (w)
        *w = mWidth;

    if (h)
        *h = mHeight;
}

double PostComposerMediaText::GetZoomFactor()
{
    double result = 1.0;
    if (mOriginalWidth) {
        result = (double) mWidth / mOriginalWidth;
    }
    return result;
}

void PostComposerMediaText::GetGeometry(int *x, int *y, int *w, int *h)
{
    if (x)
        *x = mPositionX;

    if (y)
        *y = mPositionY;

    if (w)
        *w = mWidth;

    if (h)
        *h = mHeight;
}

void PostComposerMediaText::GetColor(int *r, int *g, int *b)
{
    if (r)
        *r = mColorR;

    if (g)
        *g = mColorG;

    if (b)
        *b = mColorB;
}
