/*
 * PlacesProvider.cpp
 *
 * Created: 18 SEP 2015
 * Author: Jeyaramakrishnan Sundar
 */

#include <sstream>
#include <dlog.h>
#include <stdlib.h>
#include "jsonutilities.h"

#include "CacheManager.h"
#include "Common.h"
#include "Config.h"
#include "PlacesProvider.h"
#include "ParserWrapper.h"
#include "Utils.h"
#include "FbResponseAboutPlaces.h"

const char* PlacesProvider::RADIUS = "distance=3000";
const char* PlacesProvider::PLACES_REQUEST_PART = "search?type=";

const double PlacesProvider::LAT_LONG_TUDE = 999.0f;

PlacesProvider::PlacesProvider()
{
	mLatitude = mLongitude = PlacesProvider::LAT_LONG_TUDE;

	m_Ids = eina_array_new( RESPONSE_CONTAINER_SIZE);
    SetPagingAlgorithm( ITERATORS_ALGORITHM );
}

void PlacesProvider::SetEntityId( const char *id )
{
    m_EntityId = id;
    EraseData();
}

PlacesProvider::~PlacesProvider(){
	mLatitude = PlacesProvider::LAT_LONG_TUDE;
	mLongitude = PlacesProvider::LAT_LONG_TUDE;
    EraseData();
    eina_array_free (m_Ids); m_Ids = NULL;
}
void PlacesProvider::ClearIds() {
    if ( m_Ids ) {
        unsigned int index = 0;
        void* item;
        Eina_Array_Iterator iterator;
        EINA_ARRAY_ITER_NEXT( m_Ids, index, item, iterator ) {
            free(item);
        }
        eina_array_clean( m_Ids );
    }
}

void PlacesProvider::CustomErase()
{
    ClearIds();
}

PlacesProvider* PlacesProvider::GetInstance()
{
    static PlacesProvider *place_provider = new PlacesProvider();
    return place_provider;
}

void PlacesProvider::DeleteItem( void* item )
{
    if (!eina_array_remove( GetData(), KeepItemById, item )) {
        dlog_print( DLOG_DEBUG, LOG_TAG_FACEBOOK, "PlacesProvider: cannot delete the referenced element" );
    }
}

UPLOAD_DATA_RESULT PlacesProvider::UploadData( bool isDescendingOrder )
{
    std::stringstream request;

    request << PLACES_REQUEST_PART << PLACE << '&'
    		<< "center=" << getLatitudeStr() <<',' << getLongitudeStr() << '&' << RADIUS
    		<< '&' << FIELDS << ID << ',' << NAME << ',' << LINK << ',' << "category" << ',' << LOCATION << ','
    		<< "picture.width(104).height(104).as(picture)" ;


    return RequestData( request.str().c_str(), isDescendingOrder );
}

unsigned int PlacesProvider::ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread )
{
    JsonObject *object = NULL;
    JsonParser *parser = openJsonParser( data, &object );
    ParserWrapper wr( parser );
    unsigned int itemsCount = NONE_ITEMS;
    if ( object ) {
    	FbResponseAboutPlaces * placesRespond = FbResponseAboutPlaces::createFromJson(data );
        if ( placesRespond->mListofPlaces ) {
            unsigned int countainerSize = GetLimit() > 0 ? GetLimit() : RESPONSE_CONTAINER_SIZE;
            Eina_Array *parsedItemsArray = eina_array_new( countainerSize );
            dlog_print( DLOG_DEBUG, LOG_TAG_FACEBOOK, "PlacesProvider:countainerSize = %d", countainerSize);
            if ( !parsedItemsArray || !m_Ids ) {
                dlog_print( DLOG_DEBUG, LOG_TAG_FACEBOOK, "PlacesProvider: cannot allocate a memory for a request array." );
                return NONE_ITEMS;
            }
            Eina_List *list;
            void *data;
            EINA_LIST_FOREACH( placesRespond->mListofPlaces, list, data ) {
                ++itemsCount;
                FbResponseAboutPlaces::PlacesList *pl = static_cast<FbResponseAboutPlaces::PlacesList*>(data);
                eina_array_push( parsedItemsArray, pl );
                placesRespond->RemoveDataFromList(pl);
            }
            AppendData( parsedItemsArray, isDescendingOrder );
            eina_array_free( parsedItemsArray );
        }
        delete placesRespond;
    }
    return itemsCount;
}

Eina_Array* PlacesProvider::GetIds() const
{
    return m_Ids;
}

void PlacesProvider::ReleaseItem( void* item ) {
    BaseObject *baseObject = static_cast<BaseObject*>(item);
    baseObject->Release();
}


const char * PlacesProvider :: getLatitudeStr()
{
	std::stringstream lat_str;
	lat_str << mLatitude;
	return lat_str.str().c_str();
}

const char * PlacesProvider :: getLongitudeStr()
{
	std::stringstream long_str;
	long_str << mLongitude;
	return long_str.str().c_str();
}

