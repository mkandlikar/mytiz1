#include "Common.h"
#include "FbRespondGetUserList.h"
#include "jsonutilities.h"
#include "Log.h"
#include "ParserWrapper.h"
#include "PeopleWhoLikedProvider.h"
#include "User.h"

#include <assert.h>
#include <sstream>

PeopleWhoLikedProvider::PeopleWhoLikedProvider() {
    m_EntityId = nullptr;
    SetPagingAlgorithm(ITERATORS_ALGORITHM);
    mProviderType = WHO_LIKED_PROVIDER;
    SetLimit(10);
}

PeopleWhoLikedProvider::~PeopleWhoLikedProvider() {
    EraseData();
    free(const_cast<char*>(m_EntityId));
}

void PeopleWhoLikedProvider::SetEntityId(const char *id) {
    m_EntityId = SAFE_STRDUP(id);
}

unsigned int PeopleWhoLikedProvider::ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread ) {
    JsonObject *object = nullptr;
    JsonParser *parser = openJsonParser( data, &object );
    ParserWrapper wr( parser );
    unsigned int itemsCount = NONE_ITEMS;
    if ( object ) {
        FbRespondGetUserList * wholikedResponse = new FbRespondGetUserList(object);
        if (wholikedResponse->mUserList) {
            unsigned int countainerSize = GetLimit() > 0 ? GetLimit() : RESPONSE_CONTAINER_SIZE;
            Eina_Array *parsedItemsArray = eina_array_new(countainerSize);
            if (!parsedItemsArray) {
                Log::debug("PeopleWhoLikedProvider: cannot allocate a memory for a request array.");
                return NONE_ITEMS;
            }
            Eina_List *list;
            void *data;
            EINA_LIST_FOREACH(wholikedResponse->mUserList, list, data) {
                ++itemsCount;
                User *user = static_cast<User*>(data);
                eina_array_push(parsedItemsArray, user);
                wholikedResponse->RemoveDataFromList(user);
            }
            Log::debug("PeopleWhoLikedProvider: there was handled %d items.", itemsCount);
            AppendData(parsedItemsArray, isDescendingOrder);
            eina_array_free(parsedItemsArray);
        }
        delete wholikedResponse;
    }
    return itemsCount;
}

UPLOAD_DATA_RESULT PeopleWhoLikedProvider::UploadData(bool isDescendingOrder) {
    if (!m_EntityId) {
        Log::debug("PeopleWhoLikedProvider: entity identifier was not specified." );
        assert( false );
        return NONE_DATA_COULD_BE_RESULT;
    }
    if (eina_array_count(GetData()) == GetItemsTotalCount()) {
        return NONE_DATA_COULD_BE_RESULT;
    }
    std::stringstream request;
    request << m_EntityId << '/' << LIKES << "?" << FIELDS << PICTURE << ',' << NAME << ',' << ID;

    return RequestData(request.str().c_str(), isDescendingOrder);
}

void PeopleWhoLikedProvider::DeleteItem(void* item) {
    if (eina_array_remove( GetData(), KeepItemById, item )) {
        Log::error("PeopleWhoLikedProvider: cannot delete the referenced element" );
    }
}

void PeopleWhoLikedProvider::ReleaseItem( void* item ) {
    BaseObject *baseObject = static_cast<BaseObject*>(item);
    baseObject->Release();
}

User *PeopleWhoLikedProvider::GetUserById(const char *id) {
    assert(id);

    Eina_Array *users = GetData();
    int usersCount = eina_array_count(users);

    for (int i = 0; i < usersCount; ++i) {
        User *user = static_cast<User*>(eina_array_data_get(users, i));
        assert(user && user->GetId());
        if (!strcmp(id, user->GetId())) {
            return user;
        }
    }
    return nullptr;
}

bool PeopleWhoLikedProvider::ReplaceUser(GraphObject *user) {
    bool ret = false;
    ret = ReplaceGraphObjectById(user->GetId(), user, GetData());
    user->Release();
    // if GraphObject(user) was not found - object should be released and will be deleted automatically,
    // but if GraphObject has been found - refCount should be decreased,
    //note: ParseData->AppendData does not call AddRef for every element,
    // it looks that we should check and exclude newObject->AddRef() from ReplaceGraphObjectById possibly!
    return ret;
}
