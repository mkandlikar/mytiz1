#include "HomeProvider.h"
#include <eina_array.h>
#include <sstream>
#include <dlog.h>
#include <vector>
#include <assert.h>
#include "jsonutilities.h"
#include "ParserWrapper.h"

#include "Common.h"
#include "FbRespondGetFeed.h"
#include "CacheManager.h"
#include "Post.h"
#include "OperationManager.h"
#include "Utils.h"

const unsigned int HomeProvider::LIMIT_BUFFER_SIZE = 32;
const unsigned int HomeProvider::REQUEST_HOME_ITEMS_LIMIT = 35;
const unsigned int HomeProvider::REQUEST_HOME_COMMENTS_LIMIT = 1;
const char *HomeProvider::HOME_MAIN_REQUEST_PART = "me/home";


HomeProvider::HomeProvider()
{
    SetLimit( REQUEST_HOME_ITEMS_LIMIT );
    SetCommentsLimit( REQUEST_HOME_COMMENTS_LIMIT );
    m_PostLowerBound = m_PostUpperBound = 0;
    OperationManager::GetInstance()->Subscribe(this);
    mProviderType = HOME_PROVIDER;
}

HomeProvider::~HomeProvider() {
    OperationManager::GetInstance()->UnSubscribe(this);
    EraseData();
}

HomeProvider* HomeProvider::GetInstance()
{
    static HomeProvider* homeProvider = new HomeProvider();
    return homeProvider;
}

void HomeProvider::Update(AppEventId eventId, void * data)
{
    switch (eventId) {
    case eOPERATION_ADDED_EVENT:
    case eOPERATION_REMOVED_EVENT:
        if (data) {
            Sptr<Operation> oper(static_cast<Operation*>(data));
            if (Operation::OD_Home == oper->GetDestination()) {
                PostProvider::Update(eventId, data);
            }
        }
        break;
    default:
        PostProvider::Update(eventId, data);
        break;
    }
}

std::string HomeProvider::GetMainRequestPart() {
    return std::string(HOME_MAIN_REQUEST_PART);
}
//
//   CACHE SECTION
//

bool HomeProvider::UpdateCacheData(FbRespondGetFeed * feedResponse) {
    return CacheManager::GetInstance().SetFeedData( feedResponse );
}

void HomeProvider::CachePostsSequence( Eina_Array *data, bool isDescendingOrder )
{
    Log::debug("HomeProvider::CachePostsSequence" );
    if ( m_PostLowerBound == 0 && m_PostUpperBound == 0 ) {
        const char *time = Utils::GetUnixTime();
        if ( time ) {
            CacheManager::GetInstance().CreateSessionData( time );
            free( (void*)time );
        }
    }
    unsigned int sessionId = CacheManager::GetInstance().GetSession();
    unsigned int count = ( data ? eina_array_count( data ) : 0 );
    if ( (sessionId > 0) && (count > 0) ) {
        std::vector<int> autoIds( count );
        Eina_Array_Iterator iterator;
        unsigned int index = 0;
        void* item;
        EINA_ARRAY_ITER_NEXT( data, index, item, iterator ) {
            GraphObject *post = static_cast<GraphObject*>(item);
            if ( post ) {
                unsigned int position = ( isDescendingOrder ? index : (count - index - 1) );
                autoIds[ position ] = CacheManager::GetInstance().GetPostAutoIdByPostId(post->GetId());
            }
        }
        for ( std::vector<int>::const_iterator it = autoIds.begin(); it != autoIds.end(); ++it ) {
            int dbPosition = isDescendingOrder ? ++m_PostUpperBound : --m_PostLowerBound;
            if ( *it != 0 ) { // auto id must not be equal to zero
                CacheManager::GetInstance().CreatePostPositons( dbPosition, *it, sessionId );
                // @todo remove this `dlog` after debug
                Log::debug("HomeProvider::CacheData() written position=%d, autoId=%d, session=%d", dbPosition, *it, sessionId );
            }
        }
    }
}

unsigned int HomeProvider::RetrieveCachedData()
{
    Log::debug("HomeProvider::RetrieveCachedData" );
    std::list<Order*> orderedPosts = RetrievePostIds();
    Eina_Array *array = eina_array_new( 256 );
    for ( std::list<Order*>::const_iterator it = orderedPosts.begin(); it != orderedPosts.end(); ++it ) {
        Post *post = CacheManager::GetInstance().SelectFeedDataByAutoId( (*it)->mPostAutoId );
        if ( post ) {
            Log::debug("HomeProvider::RetrieveCachedData -- name is: %s", post->GetName().c_str() );
            //
            // This assert is very often true. So, need to fix some logic in CacheManager
            //assert( post->GetAutoId() == (*it)->mPostAutoId );
            eina_array_push( array, post );
        }
    }
    if ( eina_array_count( array ) > 0 ) {
        AppendData( array, true );
    }
    eina_array_free( array );
    Log::debug("HomeProvider::RetrieveCachedData -- were parsed %d items from within the cache.", GetDataCount() );
    return GetDataCount();
}

std::list<Order*> HomeProvider::RetrievePostIds()
{
    Log::debug("HomeProvider::RetrievePostIds()" );
    int sessionId = CacheManager::GetInstance().GetSession();
    std::list<Order*> list;
    while ( sessionId > 0 ) {
        Eina_List *orderedPosts = CacheManager::GetInstance().SelectPostOrderDataBySessionId( sessionId );
        if ( orderedPosts ) {
            Eina_List *l;
            void *listData;
            EINA_LIST_FOREACH( orderedPosts, l, listData ) {
                Order *order = static_cast<Order*>(listData);
                if ( !isStdListContainOrder( list, order ) ) {
                    list.push_back( order );
                    if ( list.size() == GetLimit() ) {
                        return list;
                    }
                }
            }
        }
        --sessionId;
    }
    return list;
}

bool HomeProvider::IsNeedToHandleOperation(Sptr<Operation> oper) {
    return oper->GetDestination() == Operation::OD_Home;
}

void HomeProvider::MissCacheRetrieve( bool miss )
{
    mMissCacheRetrieve = miss;
}
