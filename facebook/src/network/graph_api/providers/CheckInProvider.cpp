#include "CheckInProvider.h"
#include "FbResponseSearchPlaces.h"
#include "ParserWrapper.h"
#include "Place.h"
#include "Utils.h"


#include <assert.h>
#include <eina_array.h>

#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "Check-In"

const char *CheckInProvider::CHECKIN_MAIN_REQUEST_PART = "/search?type=place&center=%05.2f,%05.2f&distance=%d&limit=%d";
const char *CheckInProvider::CHECKIN_MAIN_REQUEST_PART_WITH_STR = "/search?q=%s&type=place&center=%05.2f,%05.2f&limit=%d";
const unsigned int CheckInProvider::CHECKIN_REQUEST_LIMIT = 100;
const unsigned int CheckInProvider::CHECKIN_REQUEST_RADUIS = 1000;


CheckInProvider::CheckInProvider() :
        mLatitude(0.0f), mLongitude(0.0f), mSearchString(nullptr) {
    Log::info_tag(LOG_TAG, "CheckInProvider::CheckInProvider");
    SetPagingAlgorithm(ITERATORS_ALGORITHM);
}

CheckInProvider::~CheckInProvider() {
    EraseData();
    delete[] mSearchString;
}

CheckInProvider* CheckInProvider::GetInstance() {
    static CheckInProvider *checkInProvider = new CheckInProvider();
    return checkInProvider;
}

void CheckInProvider::SetSearchCriteria(double latitude, double longitude, const char *string) {
    EraseData();
    mLatitude = latitude;
    mLongitude = longitude;
    delete[] mSearchString;
    mSearchString = nullptr;
    if (string && strlen(string) > 0) {
        mSearchString = new char[strlen(string) + 1];
        eina_strlcpy(mSearchString, string, strlen(string) + 1);
    }
}

UPLOAD_DATA_RESULT CheckInProvider::UploadData(bool isDescendingOrder) {
    Log::info_tag(LOG_TAG, "CheckInProvider::UploadData");
    if ( eina_array_count( GetData() ) == GetItemsTotalCount() ) {
        // We cannot request more data
        return NONE_DATA_COULD_BE_RESULT;
    }

    char str[1024];
    if (mSearchString) {
        Utils::Snprintf_s(str, 1024, CHECKIN_MAIN_REQUEST_PART_WITH_STR, mSearchString, mLatitude, mLongitude, CHECKIN_REQUEST_LIMIT);
    } else {
        Utils::Snprintf_s(str, 1024, CHECKIN_MAIN_REQUEST_PART, mLatitude, mLongitude, CHECKIN_REQUEST_RADUIS, CHECKIN_REQUEST_LIMIT);
    }

    Log::info_tag(LOG_TAG, "UPLOAD:%s", str);
    return RequestData(str, isDescendingOrder );
}

unsigned int CheckInProvider::ParseData(char *data, bool isDescendingOrder, Ecore_Thread *thread) {
    Log::info_tag(LOG_TAG, "CheckInProvider::ParseData()" );
    unsigned int placesTotal = 0;
    JsonObject *object = nullptr;
    JsonParser *parser = openJsonParser(data, &object);
    ParserWrapper wr(parser);
    if (object) {
        FbResponseSearchPlaces *placesResponse = new FbResponseSearchPlaces( object );
        if (placesResponse->mPlacesList) {
            unsigned int containerSize = GetLimit() > 0 ? GetLimit() : RESPONSE_CONTAINER_SIZE;
            Eina_Array *parsedItemsArray = eina_array_new(containerSize);
            if (parsedItemsArray == nullptr) {
                Log::error_tag(LOG_TAG, "CheckInProvider: cannot allocate a memory for a request array.");
                return NONE_ITEMS;
            }
            placesTotal = eina_list_count(placesResponse->mPlacesList);

            Eina_List *list;
            void *data;
            EINA_LIST_FOREACH(placesResponse->mPlacesList, list, data) {
                Place *place = static_cast<Place*>(data);
                if (place) {
                    eina_array_push(parsedItemsArray, place);
                }
            }
            AppendData(parsedItemsArray, isDescendingOrder);
            placesResponse->mPlacesList = eina_list_free(placesResponse->mPlacesList);
            eina_array_free(parsedItemsArray);
            delete placesResponse;
        }
    }
    return placesTotal;
}

void CheckInProvider::DeleteItem(void* item) {
    if (!eina_array_remove(GetData(), KeepItemById, item)) {
        Log::debug_tag(LOG_TAG, "CheckInProvider: cannot delete the referenced element");
    }
}

void CheckInProvider::CustomErase() {
    mLatitude = 0.0f;
    mLongitude = 0.0f;
    delete []mSearchString;
    mSearchString = nullptr;
}

void CheckInProvider::ReleaseItem(void* item) {
    BaseObject *baseObject = static_cast<BaseObject*>(item);
    baseObject->Release();
}
