#include "jsonutilities.h"
#include "MutualFriendsProvider.h"
#include "ParserWrapper.h"
#include "Utils.h"

MutualFriendsProvider::MutualFriendsProvider(const char *id)
{
    mId = SAFE_STRDUP(id);
}

UPLOAD_DATA_RESULT MutualFriendsProvider::UploadData( bool isDescendingOrder )
{
    if ( GetDataCount() == GetItemsTotalCount() ) {
        return NONE_DATA_COULD_BE_RESULT;
    }

    std::stringstream request;
    request << mId << "?" << FIELDS;
    request << CONTEXT << '{' << MUTUAL_FRIENDS << '{' << ID << ',' << NAME << ',' <<  FIRST_NAME << ',' << LAST_NAME << ',' << BIRTHDAY << ','
            << "friendship_status" << ',' << PICTURE << ".width(200).height(200)" << ',' << CONTEXT << '{' << MUTUAL_FRIENDS << '}' << '}' << '}';

    return RequestData( request.str().c_str(), isDescendingOrder );
}

unsigned int MutualFriendsProvider::ParseData(char *data, bool isDescendingOrder, Ecore_Thread *thread) {
    JsonObject *object = NULL;
    JsonParser *parser = openJsonParser(data, &object);
    ParserWrapper wr(parser);
    unsigned int itemsCount = NONE_ITEMS;
    if (object) {
        Eina_List* mutualFriends = nullptr;
        JsonObject* context = json_object_get_object_member(object, "context");
        if (context) {
            JsonObject* mutual_friends = json_object_get_object_member(context, "mutual_friends");
            if (mutual_friends) {
                JsonArray* friends_array = json_object_get_array_member(mutual_friends, "data");
                if (friends_array) {
                    int len = json_array_get_length(friends_array);
                    for (int i = 0; i < len; i++) {
                        JsonObject* el = json_array_get_object_element(friends_array, i);
                        Friend* fr = new Friend(el);
                        mutualFriends = eina_list_append(mutualFriends, fr);
                    }
                }
            }
        }

        if (mutualFriends) {
            unsigned int countainerSize = GetLimit() > 0 ? GetLimit() : RESPONSE_CONTAINER_SIZE;
            Eina_Array *parsedItemsArray = eina_array_new(countainerSize);
            if (!parsedItemsArray) {
                Log::error("MutualFriendsProvider: cannot allocate a memory for a request array.");
                return NONE_ITEMS;
            }
            Eina_List *list;
            void *data;
            EINA_LIST_FOREACH( mutualFriends, list, data )
            {
                ++itemsCount;
                Friend* fr = static_cast<Friend*>(data);
                if (fr) {
                    eina_array_push(parsedItemsArray, fr);
                }
            }
            Log::debug("MutualFriendsProvider: there was handled %d items and %d friends parsed.", itemsCount, eina_array_count(parsedItemsArray));
            AppendData(parsedItemsArray, isDescendingOrder);
            eina_array_free(parsedItemsArray);
            if (mutualFriends)
                eina_list_free(mutualFriends);
        }
    }
    return itemsCount;
}
