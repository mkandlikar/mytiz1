#include "FbRespondGetNotifications.h"
#include "Log.h"
#include "NotificationProvider.h"
#include "ParserWrapper.h"
#include "Separator.h"
#include "Utils.h"

#include <Elementary.h>


NotificationProvider::NotificationProvider()
{
    SetPagingAlgorithm(ITERATORS_ALGORITHM);
    mUnreadBadgeNum = 0;
    mProviderType = NOTIFICATIONS_PROVIDER;
    mEarlierSeparatorCreated = false;
}

NotificationProvider::~NotificationProvider()
{
    EraseData();
}

NotificationProvider* NotificationProvider::GetInstance()
{
    static NotificationProvider *notificationProvider = new NotificationProvider();
    return notificationProvider;
}

void NotificationProvider::SetUnreadBadgeNum(int num)
{
    mUnreadBadgeNum = num;
}

bool NotificationProvider::IsNotificationNew(NotificationObject *notification)
{
    /* This function defines the criteria on which notifications are subdivided to "New" and "Earlier" groups.
     * Currently only one rule is applied: notifications are "New" if created or last updated not earlier than a day ago.
     */
    long int passedTime = (long int)Utils::GetUnixTime_inSec() - notification->mUpdatedTimeInSec;
    return (passedTime < (24*3600));
}

unsigned int NotificationProvider::ParseData(char *data, bool isDescendingOrder, Ecore_Thread *thread)
{
    JsonObject *object = NULL;
    JsonParser *parser = openJsonParser(data, &object);
    ParserWrapper wr(parser);

    unsigned int itemsCount = NONE_ITEMS;
    if (object) {
        std::unique_ptr<FbRespondGetNotifications> notifRespond(new FbRespondGetNotifications(object));
        if (notifRespond->mNotificationList) {
            unsigned int countainerSize = GetLimit() > 0 ? GetLimit() : RESPONSE_CONTAINER_SIZE;
            Eina_Array *parsedItemsArray = eina_array_new(countainerSize);
            if (parsedItemsArray == NULL) {
                Log::error(LOG_FACEBOOK_NOTIFICATION, "NotificationProvider: cannot allocate a memory for a request array.");
                return NONE_ITEMS;
            }
            Eina_List *list;
            void *data;
            EINA_LIST_FOREACH(notifRespond->mNotificationList, list, data) {
                NotificationObject *notif = static_cast<NotificationObject*>(data);

                if (itemsCount == 0 && GetDataCount() == 0 && IsNotificationNew(notif)) {
                    eina_array_push(parsedItemsArray, new Separator(Separator::NEW_NOTI_SEPARATOR));
                    ++itemsCount;
                }

                if (!mEarlierSeparatorCreated && !IsNotificationNew(notif)) {
                    eina_array_push(parsedItemsArray, new Separator(Separator::EARLIER_NOTI_SEPARATOR));
                    mEarlierSeparatorCreated = true;
                    ++itemsCount;
                }

                eina_array_push(parsedItemsArray, notif);
                ++itemsCount;
            }
            notifRespond->mNotificationList = eina_list_free(notifRespond->mNotificationList);

            mUnreadBadgeNum = notifRespond->mUnseenCount;
            Log::debug(LOG_FACEBOOK_NOTIFICATION, "NotificationProvider::ParseData() handled %d items", itemsCount);
            AppendData(parsedItemsArray, isDescendingOrder);
            eina_array_free(parsedItemsArray);
        }
    }
    return itemsCount;
}

UPLOAD_DATA_RESULT NotificationProvider::UploadData(bool isDescendingOrder)
{
    return RequestData("me/notifications?include_read=true&fields=created_time,application,message,from{id,name,picture.width(200).height(200)},id,object,title,to,unread,updated_time,link", isDescendingOrder);
}

void NotificationProvider::DeleteItem(void* item)
{
    GraphObject *itemAsGrObj = static_cast<GraphObject*>(item);
    if (GraphObject::GOT_SEPARATOR == itemAsGrObj->GetGraphObjectType()) {
        Separator *separator = static_cast<Separator*>(item);
        if (Separator::EARLIER_NOTI_SEPARATOR == separator->GetSeparatorType()) {
            mEarlierSeparatorCreated = false;
        }
    }

    if (!eina_array_remove(GetData(), KeepItemById, item)) {
        Log::warning("NotificationProvider: cannot delete the referenced element");
    }
}

void NotificationProvider::ReleaseItem(void* item)
{
    GraphObject *itemAsGrObj = static_cast<GraphObject*>(item);
    if (GraphObject::GOT_SEPARATOR == itemAsGrObj->GetGraphObjectType()) {
        Separator *separator = static_cast<Separator*>(item);
        if (Separator::EARLIER_NOTI_SEPARATOR == separator->GetSeparatorType()) {
            mEarlierSeparatorCreated = false;
        }
    }

    BaseObject *baseObject = static_cast<BaseObject*>(item);
    baseObject->Release();
}

void NotificationProvider::DeleteNotificationsByPostId(const char *id) {
    assert(id);

    Eina_Array *notifArray = GetData();
    int count = eina_array_count(notifArray);

    for (int i = 0; i < count; ++i) {
        GraphObject *obj = static_cast<GraphObject*>(eina_array_data_get(notifArray, i));
        assert(obj);
        NotificationObject *notification = dynamic_cast<NotificationObject*>(obj);
        if (notification && notification->mObjectId && !strcmp(id, notification->mObjectId)) {
            DeleteItem(notification);  // should delete all notifications related to the post!
        }
    }
}
