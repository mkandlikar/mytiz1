/*
 * SearchProvider.cpp
 *
 *  Created on: Aug 6, 2015
 *      Author: RUINMKOL
 */

#include <dlog.h>
#include <stdlib.h>
#include <sstream>

#include "Common.h"
#include "FbRespondGetSearchResults.h"
#include "FoundItem.h"
#include "jsonutilities.h"
#include "ParserWrapper.h"
#include "SearchProvider.h"
#include "Utils.h"

const char* SearchProvider::SEARCH_REQUEST_MAIN_PART = "method/ubersearch.get?format=json";
const char* SearchProvider::SEARCH_FILTER = "['user','group','page']";

const unsigned int SearchProvider::SEARCH_REQUEST_LIMIT = 50;

SearchProvider::SearchProvider()
{
    mSearchString = NULL;
    SetAPIType(AbstractDataProvider::REST_API);
    SetLimit(SEARCH_REQUEST_LIMIT);
}

SearchProvider::~SearchProvider()
{
    EraseData();
}

SearchProvider* SearchProvider::GetInstance()
{
    static SearchProvider *searchProvider = new SearchProvider();
    return searchProvider;
}

void SearchProvider::SetSearchString(const char * searchString)
{
    if ( searchString )
    {
        free((void *) mSearchString );
        mSearchString = Utils::escape(searchString);
    }
}

UPLOAD_DATA_RESULT SearchProvider::UploadData( bool isDescendingOrder )
{
    if ( eina_array_count( GetData() ) == GetItemsTotalCount() ) {
        //
        // We cannot request more data
        return NONE_DATA_COULD_BE_RESULT;
    }

    std::stringstream request;

    request << SEARCH_REQUEST_MAIN_PART
            << "&" << QUERY  << "=" << mSearchString
            << "&" << FILTER << "=" << SEARCH_FILTER;

    return RequestData( request.str().c_str(), isDescendingOrder );
}

void SearchProvider::DeleteItem( void* item )
{
    // @todo implement
}


unsigned int SearchProvider::ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread )
{
    unsigned int itemsCount = NONE_ITEMS;
    JsonParser *parser = openJsonParser( data, NULL);
    ParserWrapper wr( parser );
    JsonNode* rootNode = json_parser_get_root (parser);

    FbRespondGetSearchResults * searchRespond = rootNode != NULL ? new FbRespondGetSearchResults( rootNode ): NULL;

    if ((searchRespond) && ( searchRespond->mFoundItems )) {
        unsigned int countainerSize = GetLimit() > 0 ? GetLimit() : RESPONSE_CONTAINER_SIZE;
        Eina_Array *parsedItemsArray = eina_array_new( countainerSize );
        if ( parsedItemsArray == NULL ) {
            dlog_print( DLOG_DEBUG, LOG_TAG_FACEBOOK, "SearchProvider: cannot allocate a memory for a request array." );
            return NONE_ITEMS;
        }
        Eina_List *list;
        void *data;
        EINA_LIST_FOREACH( searchRespond->mFoundItems, list, data ) {
            ++itemsCount;
            FoundItem *foundItem = static_cast<FoundItem*>(data);
            if (strcmp(foundItem->mType, "app")) {
                eina_array_push( parsedItemsArray, foundItem );
            }
        }
        dlog_print( DLOG_DEBUG, LOG_TAG_FACEBOOK, "SearchProvider: there was handled %d friend requests.", itemsCount );
        AppendData( parsedItemsArray, isDescendingOrder );
        eina_array_free( parsedItemsArray );

    }
    return itemsCount;
}

void SearchProvider::CustomErase()
{
    free((void *) mSearchString);
    mSearchString = NULL;
}

void SearchProvider::ReleaseItem( void* item ) {
    BaseObject *baseObject = static_cast<BaseObject*>(item);
    baseObject->Release();
}

