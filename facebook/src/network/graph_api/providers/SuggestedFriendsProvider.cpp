#include <dlog.h>
#include <memory>
#include "ParserWrapper.h"

#include "Common.h"
#include "SuggestedFriendsProvider.h"
#include "FbRespondGetSuggestedFriends.h"
#include "SuggestedFriend.h"

#define REQUEST_SUGGESTED_FRIENDS "/me/orderedfriends"

SuggestedFriendsProvider::SuggestedFriendsProvider() {
    SetLimit(10);
}

SuggestedFriendsProvider::~SuggestedFriendsProvider() {
    EraseData();
}

SuggestedFriendsProvider* SuggestedFriendsProvider::GetInstance() {
    static SuggestedFriendsProvider *suggestedFriendsProvider = new SuggestedFriendsProvider();
    return suggestedFriendsProvider;
}

UPLOAD_DATA_RESULT SuggestedFriendsProvider::UploadData(bool isDescendingOrder) {
    if (GetDataCount() == GetItemsTotalCount()) {
        return NONE_DATA_COULD_BE_RESULT;
    }
    return RequestData(REQUEST_SUGGESTED_FRIENDS, isDescendingOrder);
}

unsigned int SuggestedFriendsProvider::ParseData(char *data, bool isDescendingOrder, Ecore_Thread *thread) {
    JsonObject *object = NULL;
    JsonParser *parser = openJsonParser(data, &object);
    ParserWrapper wr(parser);
    unsigned int itemsCount = NONE_ITEMS;
    if (object) {
        std::unique_ptr<FbRespondGetSuggestedFriends> suggestedResponse(new FbRespondGetSuggestedFriends(object));
        if (suggestedResponse->mSuggestedFriendsList) {
            unsigned int containerSize = GetLimit() > 0 ? GetLimit() : RESPONSE_CONTAINER_SIZE;
            Eina_Array *parsedItemsArray = eina_array_new(containerSize);
            if (parsedItemsArray == NULL) {
                dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "SuggestedFriendsProvider: cannot allocate a memory for a request array.");
                return NONE_ITEMS;
            }
            Eina_List *list;
            void *data;
            EINA_LIST_FOREACH(suggestedResponse->mSuggestedFriendsList, list, data) {
                ++itemsCount;
                SuggestedFriend *user = static_cast<SuggestedFriend*>(data);
                eina_array_push(parsedItemsArray, user);
            }
            suggestedResponse->mSuggestedFriendsList = eina_list_free(suggestedResponse->mSuggestedFriendsList);
            dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "SuggestedFriendsProvider: there was handled %d items.", itemsCount);
            AppendData(parsedItemsArray, isDescendingOrder);
            eina_array_free(parsedItemsArray);
        }
    }
    return itemsCount;
}

void SuggestedFriendsProvider::DeleteItem(void* item) {
    if (!eina_array_remove( GetData(), KeepItemById, item )) {
        dlog_print( DLOG_DEBUG, LOG_TAG_FACEBOOK, "SuggestedFriendsProvider: cannot delete the referenced element" );
    }
}

void SuggestedFriendsProvider::ReleaseItem(void* item) {
    BaseObject *baseObject = static_cast<BaseObject*>(item);
    baseObject->Release();
}
