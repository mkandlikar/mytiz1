/*
 * EventGuestListProvider.cpp
 *
 *  Created on: Nov 2, 2015
 *      Author: ruibezruch
 */
#include <assert.h>
#include <dlog.h>
#include <sstream>
#include <memory>
#include "ParserWrapper.h"

#include "Common.h"
#include "EventGuestListProvider.h"
#include "FbRespondGetUserList.h"
#include "User.h"
#include "Friend.h"
#include "Separator.h"
#include "Utils.h"

EventGuestListProvider::EventGuestListProvider(const char * requestEdge) :
    mListToSort(NULL),
    mFriendsList(NULL),
    mRequestEdge(SAFE_STRDUP(requestEdge))
{
    SetAPIVersion(API_VER_2_4);
    m_EntityId = NULL;
    isLoggedInUserHere = false;

    SetPagingAlgorithm(CURSOR_ALGORITHM);
    SetLimit(200);
}

EventGuestListProvider::~EventGuestListProvider() {
    EraseData();
    free((void *) m_EntityId);
    m_EntityId = NULL;
    mListToSort = eina_list_free(mListToSort);
    mFriendsList = eina_list_free(mFriendsList);
    free((void *)mRequestEdge);
    isLoggedInUserHere = false;
}

void EventGuestListProvider::SetEntityId(const char *id)
{
    if ((!m_EntityId && id) || (m_EntityId && id && strcmp(id, m_EntityId))) {
        EraseData();
        m_EntityId = SAFE_STRDUP(id);
    }
}

void EventGuestListProvider::CustomErase() {
    free((void *) m_EntityId);
    m_EntityId = NULL;
    isLoggedInUserHere = false;
    mListToSort = eina_list_free(mListToSort);
    mFriendsList = eina_list_free(mFriendsList);
}

unsigned int EventGuestListProvider::ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread )
{
    JsonObject *object = NULL;
    JsonParser *parser = openJsonParser( data, &object );
    ParserWrapper wr( parser );
    unsigned int itemsCount = NONE_ITEMS;
    if ( object ) {
        std::unique_ptr<FbRespondGetUserList> guestsResponse( new FbRespondGetUserList( object ) );
        if (guestsResponse->mUserList) {
            itemsCount = eina_list_count(guestsResponse->mUserList);

            mListToSort = eina_list_merge(mListToSort, guestsResponse->mUserList);
            guestsResponse->mUserList = NULL;

            dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "EventGuestListProvider: there was handled %d items.", itemsCount);
            if (!IsItLastPage(object)) {
                SwitchContinueRequest();
            }
            else {
                SortListByFriends();
                AppendData(mListToSort, isDescendingOrder);
                mListToSort = eina_list_free(mListToSort);
            }
        }
    }
    return itemsCount;
}

void EventGuestListProvider::SortListByFriends() {
    //current user must be 1st in the list

    mListToSort = eina_list_prepend(mListToSort, new Separator(Separator::OTHERS_SEPARATOR, i18n_get_text("IDS_EVENT_GUESTS_OTHERS")));

    bool thereAreFriends = false;
    Eina_List * listIndex, * listIndexNext;
    void * listData;
    EINA_LIST_REVERSE_FOREACH_SAFE(mListToSort, listIndex, listIndexNext, listData) {
        GraphObject * graphObject = static_cast<GraphObject *>(listData);
        if (graphObject->GetGraphObjectType() == GraphObject::GOT_SEPARATOR) {
            break;
        }

        User * user = static_cast<User *>(listData);

        if (Utils::IsMe(user->GetId())) {
            isLoggedInUserHere = true;
            mListToSort = eina_list_remove_list(mListToSort, listIndex);
        }
        else if (user->mFriendshipStatus == Person::eARE_FRIENDS) {
            thereAreFriends = true;
            mListToSort = eina_list_remove_list(mListToSort, listIndex);
            mListToSort = eina_list_prepend(mListToSort, user);
            mFriendsList = eina_list_append(mFriendsList, user);
        }
    }
    if (thereAreFriends) {
        mListToSort = eina_list_prepend(mListToSort, new Separator(Separator::FRIENDS_SEPARATOR, i18n_get_text("IDS_EVENT_GUESTS_FRIENDS")));
    }

    // Check if there are no others
    GraphObject * graphObject = static_cast<GraphObject *>(eina_list_last_data_get(mListToSort));
    if (graphObject->GetGraphObjectType() == GraphObject::GOT_SEPARATOR) {
        delete graphObject;
        mListToSort = eina_list_remove_list(mListToSort, eina_list_last(mListToSort));
    }
}

UPLOAD_DATA_RESULT EventGuestListProvider::UploadData(bool isDescendingOrder)
{
    if (!m_EntityId || !mRequestEdge) {
        dlog_print( DLOG_DEBUG, LOG_TAG_FACEBOOK, "EventGuestListProvider: entity identifier was not specified." );
        assert( false );
        return NONE_DATA_COULD_BE_RESULT;
    }
    if (GetDataCount() == GetItemsTotalCount()) {
        return NONE_DATA_COULD_BE_RESULT;
    }
    std::stringstream request;
    request << m_EntityId << mRequestEdge << "?fields=name,picture.width(100).height(100),friendship_status&summary=1";

    return RequestData(request.str().c_str(), isDescendingOrder);
}

void EventGuestListProvider::DeleteItem(void* item)
{
    if (!eina_array_remove( GetData(), KeepItemById, item )) {
        dlog_print( DLOG_DEBUG, LOG_TAG_FACEBOOK, "EventGuestListProvider: cannot delete the referenced element" );
    }
}

void EventGuestListProvider::ReleaseItem( void* item )
{
    BaseObject *baseObject = static_cast<BaseObject*>(item);
    baseObject->Release();
}


EventGoingListProvider::EventGoingListProvider() : EventGuestListProvider("/attending") { }

EventGoingListProvider * EventGoingListProvider::GetInstance()
{
    static EventGoingListProvider *provider = new EventGoingListProvider();
    return provider;
}

EventGoingListProvider::~EventGoingListProvider() { }


EventMaybeListProvider::EventMaybeListProvider() : EventGuestListProvider("/maybe") { }

EventMaybeListProvider * EventMaybeListProvider::GetInstance()
{
    static EventMaybeListProvider *provider = new EventMaybeListProvider();
    return provider;
}

EventMaybeListProvider::~EventMaybeListProvider() { }


EventNoReplyListProvider::EventNoReplyListProvider() : EventGuestListProvider("/noreply") { }

EventNoReplyListProvider * EventNoReplyListProvider::GetInstance()
{
    static EventNoReplyListProvider *provider = new EventNoReplyListProvider();
    return provider;
}

EventNoReplyListProvider::~EventNoReplyListProvider() { }


EventDeclinedListProvider::EventDeclinedListProvider() : EventGuestListProvider("/declined") { }

EventDeclinedListProvider * EventDeclinedListProvider::GetInstance()
{
    static EventDeclinedListProvider *provider = new EventDeclinedListProvider();
    return provider;
}

EventDeclinedListProvider::~EventDeclinedListProvider() { }
