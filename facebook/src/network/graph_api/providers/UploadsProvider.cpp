#include "UploadsProvider.h"

UploadsProvider* UploadsProvider::GetInstance()
{
    static UploadsProvider* uploadsProvider = new UploadsProvider();
    return uploadsProvider;
}

UploadsProvider::~UploadsProvider() {
    EraseData();
}
