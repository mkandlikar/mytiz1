#include <assert.h>
#include <sstream>
#include "jsonutilities.h"
#include "ParserWrapper.h"

#include "Common.h"
#include "Config.h"
#include "Event.h"
#include "EventBatchProvider.h"
#include "FbRespondGetEvents.h"
#include "FriendBirthDayProvider.h"
#include "Log.h"
#include "Separator.h"
#include "Utils.h"

const char * EventBatchProvider::SELECTOR_UPCOMING = "upcoming";
const char * EventBatchProvider::SELECTOR_INVITES = "invites";
const char * EventBatchProvider::SELECTOR_SAVED = "saved";
const char * EventBatchProvider::SELECTOR_PAST = "past";
const char * EventBatchProvider::SELECTOR_HOSTED = "hosted";

EventBatchProvider::EventBatchProvider()
{
    SetAPIVersion(API_VER_2_4);
    SetLimit(50);
    mSelectorId = strdup( SELECTOR_UPCOMING );
    mIsNeedSeparator = false;
}

EventBatchProvider::~EventBatchProvider()
{
    EraseData();

    if (mSelectorId) {
        free((void *) mSelectorId );
        mSelectorId = NULL;
    }
}

void EventBatchProvider::SetSelectorId(const char *text)
{
    if ( mSelectorId ) {
        free((void *) mSelectorId );
    }
    mSelectorId = strdup( text );
}

const char * EventBatchProvider::GetSelectorId()
{
    return mSelectorId;
}

EventBatchProvider* EventBatchProvider::GetInstance()
{
    static EventBatchProvider *eventBatchProvider = new EventBatchProvider();
    return eventBatchProvider;
}

unsigned int EventBatchProvider::ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread )
{
    JsonObject * object = NULL;
    JsonParser * parser = openJsonParser(data, &object);
    ParserWrapper wr( parser );
    unsigned int itemsCount = NONE_ITEMS;

    FbRespondGetEvents * eventRespond = new FbRespondGetEvents(object);

    if (eventRespond->mEventsList) {
        unsigned int countainerSize = GetLimit() > 0 ? GetLimit() : RESPONSE_CONTAINER_SIZE;
        Eina_Array *parsedItemsArray = eina_array_new(countainerSize);
        if (!parsedItemsArray) {
            Log::error("EventBatchProvider: cannot allocate a memory for a request array.");
            return itemsCount;
        }

        bool isItLast = IsItLastPage(object);
        struct tm currentTime = Utils::GetCurrentDateTime();

        Eina_List *list;
        void *data;
        if (mSelectorId && !strcmp(mSelectorId, SELECTOR_PAST)) {
            EINA_LIST_FOREACH(eventRespond->mEventsList, list, data) {
                Event *event = static_cast<Event*>(data);
                struct tm startTime;
                strptime(event->mStartTimeString, "%Y-%m-%dT%H:%M:%S", &startTime);
                if ((startTime.tm_year < currentTime.tm_year || (startTime.tm_year == currentTime.tm_year && startTime.tm_mon < currentTime.tm_mon) || (startTime.tm_year == currentTime.tm_year && startTime.tm_mon == currentTime.tm_mon && startTime.tm_mday < currentTime.tm_mday)) || (event->mEndTime != 0 && event->mEndTime < Utils::GetCurrentTime())) {
                    ++itemsCount;
                    eina_array_push(parsedItemsArray, event);
                }
                eventRespond->RemoveDataFromList(event);
            }
        } else if (mSelectorId && !strcmp(mSelectorId, SELECTOR_HOSTED)) {
            EINA_LIST_REVERSE_FOREACH(eventRespond->mEventsList, list, data) {
                Event *event = static_cast<Event*>(data);
                struct tm startTime;
                strptime(event->mStartTimeString, "%Y-%m-%dT%H:%M:%S", &startTime);
                if ((startTime.tm_year > currentTime.tm_year || (startTime.tm_year == currentTime.tm_year && startTime.tm_mon > currentTime.tm_mon) || (startTime.tm_year == currentTime.tm_year && startTime.tm_mon == currentTime.tm_mon && startTime.tm_mday >= currentTime.tm_mday)) || (event->mEndTime != 0 && event->mEndTime > Utils::GetCurrentTime())) {
                    if (event->mOwner && event->mOwner->GetId() && Config::GetInstance().GetUserId() == event->mOwner->GetId()) {
                        ++itemsCount;
                        eina_array_push(parsedItemsArray, event);
                    }
                }
                eventRespond->RemoveDataFromList(event);
            }
        } else {
            EINA_LIST_REVERSE_FOREACH(eventRespond->mEventsList, list, data) {
                Event *event = static_cast<Event*>(data);
                struct tm startTime;
                strptime(event->mStartTimeString, "%Y-%m-%dT%H:%M:%S", &startTime);
                if ((startTime.tm_year > currentTime.tm_year || (startTime.tm_year == currentTime.tm_year && startTime.tm_mon > currentTime.tm_mon) || (startTime.tm_year == currentTime.tm_year && startTime.tm_mon == currentTime.tm_mon && startTime.tm_mday >= currentTime.tm_mday)) || (event->mEndTime != 0 && event->mEndTime > Utils::GetCurrentTime())) {
                    ++itemsCount;
                    eina_array_push(parsedItemsArray, event);
                    if(!mIsNeedSeparator && itemsCount > 0){
                    	mIsNeedSeparator = true;
                    }
                }
                eventRespond->RemoveDataFromList(event);
            }
        }

        if(!isItLast){
            SwitchContinueRequest();
        }

        if(isItLast && mIsNeedSeparator && mSelectorId && !strcmp(mSelectorId, SELECTOR_UPCOMING) && FriendBirthDayProvider::GetInstance()->GetDataCount() > 0) {
            Separator *separator = new Separator( Separator::BDAY_ITEMS );
            eina_array_push(parsedItemsArray, separator);
            mIsNeedSeparator = false;
        }

        Log::debug("EventBatchProvider: there was handled %d items.", itemsCount);
        AppendData(parsedItemsArray, isDescendingOrder);
        eina_array_free(parsedItemsArray);
    }
    delete eventRespond;
    return itemsCount;
}

UPLOAD_DATA_RESULT EventBatchProvider::UploadData(bool isDescendingOrder)
{
    if (eina_array_count(GetData()) == GetItemsTotalCount()) {
        return NONE_DATA_COULD_BE_RESULT;
    }
    char buffer[MAX_REQ_WEBURL_LEN] = {0};

    if (!strcmp(mSelectorId, SELECTOR_UPCOMING)) {
        Utils::Snprintf_s(buffer, MAX_REQ_WEBURL_LEN, "me/events?fields=cover,can_guests_invite,guest_list_enabled,name,description,place,start_time,end_time,owner{id,name,picture.width(200).height(200)},rsvp_status,type,timezone,ticket_uri,updated_time,maybe_count,noreply_count,declined_count,attending_count");
    } else if (!strcmp(mSelectorId, SELECTOR_INVITES)) {
        Utils::Snprintf_s(buffer, MAX_REQ_WEBURL_LEN, "me/events/not_replied?fields=cover,can_guests_invite,guest_list_enabled,name,description,place,start_time,end_time,owner{id,name,picture.width(200).height(200)},rsvp_status,type,timezone,ticket_uri,updated_time,maybe_count,noreply_count,declined_count,attending_count");
    } else if (!strcmp(mSelectorId, SELECTOR_SAVED)) {
        Utils::Snprintf_s(buffer, MAX_REQ_WEBURL_LEN, "me/events/.saves?fields=cover,can_guests_invite,guest_list_enabled,name,description,place,start_time,end_time,owner{id,name,picture.width(200).height(200)},rsvp_status,type,timezone,ticket_uri,updated_time,maybe_count,noreply_count,declined_count,attending_count");
    } else if (!strcmp(mSelectorId, SELECTOR_HOSTED)) {
        Utils::Snprintf_s(buffer, MAX_REQ_WEBURL_LEN, "me/events/created?fields=cover,can_guests_invite,guest_list_enabled,name,description,place,start_time,end_time,owner{id,name,picture.width(200).height(200)},rsvp_status,type,timezone,ticket_uri,updated_time,maybe_count,noreply_count,declined_count,attending_count");
    } else {
        Utils::Snprintf_s(buffer, MAX_REQ_WEBURL_LEN, "me/events?fields=cover,name,guest_list_enabled,can_guests_invite,description,place,start_time,end_time,owner{id,name,picture.width(200).height(200)},rsvp_status,type,timezone,ticket_uri,updated_time,maybe_count,noreply_count,declined_count,attending_count");
    }

    return RequestData(buffer, isDescendingOrder);
}

void EventBatchProvider::DeleteItem(void* item)
{
    if (!eina_array_remove( GetData(), KeepItemById, item )) {
        Log::error("EventBatchProvider: cannot delete the referenced element" );
    }
}

void EventBatchProvider::ReleaseItem(void* item)
{
    BaseObject *baseObject = static_cast<BaseObject*>(item);
    baseObject->Release();
}

void EventBatchProvider::EventStatusSet(const void *object, Event::EventRsvpStatus status)
{
    for (unsigned int i = 0; i < GetDataCount(); ++i) {
        Event *event = static_cast<Event*>(eina_array_data_get(GetData(), i));
        if (event && idsComparator(object, event->GetId())) {
            event->mRsvpStatus = status;
            break;
        }
    }
}

void EventBatchProvider::ReSetDataByEvent(Event *event_updated)
{
    Log::info("EventBatchProvider::ReSetData");
    if (event_updated) {
        for (unsigned int i = 0; i < GetDataCount(); ++i) {
            Event *event = static_cast<Event*>(eina_array_data_get(GetData(), i));
            if (event && !strcmp(event->GetId(), event_updated->GetId())) {
                event->CopyEvent(*event_updated);
                break;
            }
        }
    }
}
