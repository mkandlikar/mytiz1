#include "PhotosOfYouProvider.h"

PhotosOfYouProvider* PhotosOfYouProvider::GetInstance()
{
    static PhotosOfYouProvider *photosOfYouProvider = new PhotosOfYouProvider();
    return photosOfYouProvider;
}
