/*
 * EventGuestListFactory.cpp
 *
 *  Created on: Nov 24, 2015
 *      Author: ruykurin
 */
#include "EventGuestListFactory.h"

#ifdef EVENT_NATIVE_VIEW
EventGuestListProvider * EventGuestListFactory::GetProvider(EventGuestListTabs type)
{
    EventGuestListProvider * provider = NULL;
    switch(type) {
    case EVENT_GOING_TAB:
        provider = EventGoingListProvider::GetInstance();
        break;
    case EVENT_MAYBE_TAB:
        provider = EventMaybeListProvider::GetInstance();
        break;
    case EVENT_INVITED_TAB:
        provider = EventNoReplyListProvider::GetInstance();
        break;
    case EVENT_DECLINED:
        provider = EventDeclinedListProvider::GetInstance();
        break;
    default:
        break;
    }
    return provider;
}

GraphObject * EventGuestListFactory::GetGraphObjectById(const char * id)
{
    GraphObject * graphObject = EventGuestListFactory::GetProvider(EVENT_GOING_TAB)->GetGraphObjectById(id);
    if (!graphObject) {
        graphObject = EventGuestListFactory::GetProvider(EVENT_MAYBE_TAB)->GetGraphObjectById(id);
    }
    if (!graphObject) {
        graphObject = EventGuestListFactory::GetProvider(EVENT_INVITED_TAB)->GetGraphObjectById(id);
    }
    if (!graphObject) {
        graphObject = EventGuestListFactory::GetProvider(EVENT_DECLINED)->GetGraphObjectById(id);
    }
    return graphObject;
}

Event::EventRsvpStatus EventGuestListFactory::GetLoggedInUserStatus()
{
    if (EventGoingListProvider::GetInstance()->isLoggedInUserHere) {
        return Event::EVENT_RSVP_ATTENDING;
    } else if (EventMaybeListProvider::GetInstance()->isLoggedInUserHere) {
        return Event::EVENT_RSVP_MAYBE;
    } else if (EventNoReplyListProvider::GetInstance()->isLoggedInUserHere) {
        return Event::EVENT_RSVP_NOT_REPLIED;
    } else if (EventDeclinedListProvider::GetInstance()->isLoggedInUserHere) {
        return Event::EVENT_RSVP_DECLINED;
    } else {
        return Event::EVENT_RSVP_NONE;
    }
}

#endif
