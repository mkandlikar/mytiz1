/*
 * NotificationAbstractDataProvider.cpp
 *
 *  Created on: Aug 28, 2015
 *      Author: ingsharm02
 */

#include "NotificationAbstractDataProvider.h"
#include "dlog.h"
#include "Common.h"
#include "RequestCompleteEvent.h"

NotificationAbstractDataProvider::NotificationAbstractDataProvider():AbstractDataProvider()
{
    mData = NULL;
    mHeader = NULL;
    mRetCode = 0;
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Notification Data Provider created");
}

NotificationAbstractDataProvider::~NotificationAbstractDataProvider()
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Notification Data Provider deleted");
}

NotificationAbstractDataProvider* NotificationAbstractDataProvider::GetInstance()
{
    static NotificationAbstractDataProvider *notifcationsProvider = new NotificationAbstractDataProvider();
    return notifcationsProvider;
}

void NotificationAbstractDataProvider::IncomingData(char* data, char * header, int curlCode, Ecore_Thread *thread )
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Notification data received in IncomingdatawithHeader");
    //Store the received data in the provider
    mData = data;
    mRetCode = curlCode;
    mHeader = header;
    RequestCompleteEvent *eventData = new RequestCompleteEvent();
    eventData->mCurlCode = curlCode;
    eventData->mResponseError = 0;
    Notify(eREQUEST_COMPLETED_EVENT, eventData, thread);
}

unsigned int NotificationAbstractDataProvider::ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread )
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Notification data Parsing");
    return NONE_ITEMS;
}

UPLOAD_DATA_RESULT NotificationAbstractDataProvider::UploadData( bool isDescendingOrder )
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Notification data Uploaded");
    return NONE_DATA_COULD_BE_RESULT;
}

void NotificationAbstractDataProvider::DeleteItem( void* item )
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Notification data deleted");
}

void NotificationAbstractDataProvider::ReleaseItem( void* item ) {
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "NotificationAbstractDataProvider::ReleaseItem");
}

GraphRequest* NotificationAbstractDataProvider::GetOldNotification( const char *request, bundle *param )
{

    GraphRequest *graphRequest = new GraphRequest( request,GraphRequest::GET, this, param );
    //We do not need to use Etags now as we have stored the etags for the latest notifications
    //Any changes to the latest shall refresh the whole list.
    //graphRequest->ExecuteAsyncExHeader();

    graphRequest->ExecuteAsyncEx(handleThreadMessage_cb);
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Request sent to fetch old notifications");
    return graphRequest;
}
