/*
 * SubscribersProvider.cpp
 *
 * Created: Aug 06, 2015
 * Author: ruinreku
 */

#include <efl_extension.h>
#include <sstream>
#include <dlog.h>
#include <stdlib.h>

#include "Common.h"
#include "Config.h"
#include "FbRespondGetSubscribers.h"
#include "UserShort.h"
#include "ParserWrapper.h"
#include "SubscribersProvider.h"
#include "Utils.h"
#include "jsonutilities.h"

const unsigned int SubscribersProvider::DEFAULT_MUTUAL_FRIENDS_LIMIT = 0;
const char* SubscribersProvider::SUBSCRIBERS_REQUEST_PART = "me/subscribers";

SubscribersProvider::SubscribersProvider()
{
	SetAPIVersion(API_VER_1_0);
    m_Ids = eina_array_new( RESPONSE_CONTAINER_SIZE);
    if ( !m_Ids ) {
        dlog_print( DLOG_ERROR, LOG_TAG_FACEBOOK, "SubscribersProvider: cannot allocate a memory for an array." );
    }
}

SubscribersProvider* SubscribersProvider::GetInstance()
{
    static SubscribersProvider *subscribersProvider = new SubscribersProvider();
    return subscribersProvider;
}

void SubscribersProvider::CustomErase()
{
    if ( m_Ids ) {
        eina_array_clean( m_Ids );
        m_Ids = NULL;
    }
}

void SubscribersProvider::DeleteItem( void* item )
{
    // @todo implement
}

UPLOAD_DATA_RESULT SubscribersProvider::UploadData( bool isDescendingOrder )
{
    if ( eina_array_count( GetData() ) == GetItemsTotalCount() ) {
        //
        // We cannot request more data
        return NONE_DATA_COULD_BE_RESULT;
    }

    std::stringstream request;

    request << SUBSCRIBERS_REQUEST_PART << "?" << FIELDS;

    request << ID << ',' << NAME;

    return RequestData( request.str().c_str(), isDescendingOrder );
}

unsigned int SubscribersProvider::ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread )
{
    JsonObject *object = NULL;
    JsonParser *parser = openJsonParser( data, &object );
    ParserWrapper wr( parser );

    unsigned int itemsCount = NONE_ITEMS;
    FbRespondGetSubscribers * subscribersRespond = new FbRespondGetSubscribers( object );

    if ( object && subscribersRespond->mSubscribersList ) {
        unsigned int countainerSize = GetLimit() > 0 ? GetLimit() : RESPONSE_CONTAINER_SIZE;
        Eina_Array *parsedItemsArray = eina_array_new( countainerSize );

        if ( !parsedItemsArray || !m_Ids ) {
            dlog_print( DLOG_DEBUG, LOG_TAG_FACEBOOK, "SubscribersProvider: cannot allocate a memory for a request array." );
            return NONE_ITEMS;
        }
        Eina_List *list;
        void *data;
        EINA_LIST_FOREACH( subscribersRespond->mSubscribersList, list, data ) {
            ++itemsCount;
            UserShort *subscriber = static_cast<UserShort*>(data);
            eina_array_push( parsedItemsArray, subscriber );
            if ( !Utils::FindElement((void *)subscriber->GetId(), m_Ids, SubscribersProvider::idsComparator) ) {
                eina_array_push( m_Ids, subscriber->GetId() );
            }
        }
        if ( !Utils::FindElement((void *)Config::GetInstance().GetUserId().c_str(), m_Ids, SubscribersProvider::idsComparator) ) {
            eina_array_push( m_Ids, Config::GetInstance().GetUserId().c_str() );
        }
        dlog_print( DLOG_DEBUG, LOG_TAG_FACEBOOK, "SubscribersProvider: there was handled %d items and %d subscribers parsed.",
            itemsCount, eina_array_count( parsedItemsArray ) );
        AppendData( parsedItemsArray, isDescendingOrder );
        eina_array_free( parsedItemsArray );
    }
    return itemsCount;
}

Eina_Array* SubscribersProvider::GetIds() const
{
    return m_Ids;
}

bool SubscribersProvider::idsComparator(const void * object, const void * id)
{
    return ( strcmp((char *) object, (char*) id) == 0 );
}

bool SubscribersProvider::comparator( void *forCompare, void *toCompare )
{
    UserShort *forCmp = static_cast<UserShort*>(forCompare);
    UserShort *toCmp = static_cast<UserShort*>(toCompare);
    if ( forCmp->GetHash() == toCmp->GetHash() &&
            strcmp( forCmp->GetId(), toCmp->GetId() ) == 0 ) {
        return true;
    } else {
        return false;
    }
}

void SubscribersProvider::ReleaseItem( void* item ) {
    BaseObject *baseObject = static_cast<BaseObject*>(item);
    baseObject->Release();
}
