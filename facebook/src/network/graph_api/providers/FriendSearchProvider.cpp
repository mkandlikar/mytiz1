#include "Common.h"
#include "FbRespondGetSearchResults.h"
#include "FoundItem.h"
#include "FriendSearchProvider.h"
#include "jsonutilities.h"
#include "Log.h"
#include "ParserWrapper.h"
#include "Utils.h"

#include <assert.h>
#include <sstream>

#define REQUEST_SEARCH_USER "method/ubersearch.get?format=json&query=%s&filter=user&limit=50"

FriendSearchProvider::FriendSearchProvider()
{
    SetAPIType(REST_API);
    mTextToFind = NULL;
}

FriendSearchProvider::~FriendSearchProvider() {
    EraseData();
    free(mTextToFind);
}

void FriendSearchProvider::SetFindText(const char *text)
{
    free(mTextToFind);
    mTextToFind = Utils::escape(text);
}

FriendSearchProvider* FriendSearchProvider::GetInstance()
{
    static FriendSearchProvider *friendsSearchProvider = new FriendSearchProvider();
    return friendsSearchProvider;
}

unsigned int FriendSearchProvider::ParseData(char *data, bool isDescendingOrder, Ecore_Thread *thread)
{
    JsonParser * parser = openJsonParser(data, NULL);
    //We use root node here, because respond can have array instead object as root element
    JsonNode* rootNode = json_parser_get_root(parser);
    ParserWrapper wr( parser );

    unsigned int itemsCount = NONE_ITEMS;
    FbRespondGetSearchResults * searchRespond = new FbRespondGetSearchResults(rootNode);
    if (searchRespond->mFoundItems) {
        unsigned int countainerSize = GetLimit() > 0 ? GetLimit() : RESPONSE_CONTAINER_SIZE;
        Eina_Array *parsedItemsArray = eina_array_new(countainerSize);
        if (parsedItemsArray == NULL) {
            Log::debug("FriendSearchProvider: cannot allocate a memory for a request array.");
            return NONE_ITEMS;
        }
        Eina_List *list;
        void *data;
        EINA_LIST_FOREACH(searchRespond->mFoundItems, list, data) {
            ++itemsCount;
            FoundItem *user = static_cast<FoundItem*>(data);
            if (user->mFriendshipStatus != Person::eARE_FRIENDS) {
                eina_array_push(parsedItemsArray, user);
            }
            searchRespond->RemoveDataFromList(user);
        }
        Log::debug("FriendSearchProvider: there was handled %d items.", itemsCount);
        AppendData(parsedItemsArray, isDescendingOrder);
        eina_array_free(parsedItemsArray);
        delete searchRespond;
    }
    return itemsCount;
}

UPLOAD_DATA_RESULT FriendSearchProvider::UploadData(bool isDescendingOrder)
{
    if (!mTextToFind) {
        Log::debug("FriendSearchProvider: entity identifier was not specified.");
        assert(false);
        return NONE_DATA_COULD_BE_RESULT;
    }
    if (eina_array_count(GetData()) == GetItemsTotalCount()) {
        return NONE_DATA_COULD_BE_RESULT;
    }
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, REQUEST_SEARCH_USER, mTextToFind);
    return RequestData(url, isDescendingOrder);
}

void FriendSearchProvider::DeleteItem(void* item)
{
    if (!eina_array_remove(GetData(), KeepItemById, item)) {
        Log::debug("FriendSearchProvider: cannot delete the referenced element" );
    } else {
        ReleaseItem(item);
    }
}

void FriendSearchProvider::ReleaseItem( void* item ) {
    BaseObject *baseObject = static_cast<BaseObject*>(item);
    baseObject->Release();
}

FoundItem *FriendSearchProvider::GetItemById(const char *id) {
    assert(id);

    Eina_Array *foundItems = GetData();
    for (int i = 0; i < eina_array_count(foundItems); i++) {
        FoundItem *item = static_cast<FoundItem*>(eina_array_data_get(foundItems, i));
        assert(item);
        assert(item->GetId());
        if (!strcmp(id, item->GetId())) {
            return item;
        }
    }
    return nullptr;
}
