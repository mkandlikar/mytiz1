#include "FbRespondGetUserList.h"
#include "Log.h"
#include "ParserWrapper.h"
#include "User.h"
#include "UsersByIdProvider.h"

#include <memory>
#include <sstream>


unsigned short UsersByIdProvider::MAX_REQUEST_ITEMS = 50;
unsigned short UsersByIdProvider::IDS_CONTAINER_SIZE_STEP = 16;


UsersByIdProvider::UsersByIdProvider() {
    m_Ids = eina_array_new( IDS_CONTAINER_SIZE_STEP );
    m_IdLastIndex = m_HandledItems = 0;
    SetPagingAlgorithm( MANUAL_ALGORITHM );
    SetRequestTimerEnabled( false );
}

UsersByIdProvider::~UsersByIdProvider() {
    EraseData();
    eina_array_free(m_Ids);
}

UsersByIdProvider* UsersByIdProvider::GetInstance() {
    static UsersByIdProvider *briefProvider = new UsersByIdProvider();
    return briefProvider;
}

void UsersByIdProvider::CustomErase() {
    unsigned int index = 0;
    void* item;
    Eina_Array_Iterator iterator;
    EINA_ARRAY_ITER_NEXT( m_Ids, index, item, iterator ) {
        free( item );
    }
    eina_array_clean(m_Ids);
    m_IdLastIndex = m_HandledItems = 0;
}

bool UsersByIdProvider::SetIds(Eina_List *ids) {
    if (eina_list_count( ids ) == 0 ) {
        return false;
    }
    EraseData();
    Eina_List *list;
    void *data;
    EINA_LIST_FOREACH(ids, list, data) {
        const char *id = static_cast<char*>(data) + 2; // removing "u_"
        if ( !eina_array_push(m_Ids, SAFE_STRDUP(id))) {
            Log::error( "UsersByIdProvider::SetIds, cannot push id [ %s ]", id);
        }
    }
    FillManualPagination();
    return true;
}

unsigned int UsersByIdProvider::ParseData(char *data, bool isDescendingOrder, Ecore_Thread *thread) {
    unsigned int items = NONE_ITEMS;
    JsonObject *object = NULL;
    JsonParser *parser = openJsonParser(data, &object);
    ParserWrapper wr(parser);
    JsonNode* rootNode = json_parser_get_root(parser);
    if (object && rootNode) {
        std::unique_ptr<FbRespondGetUserList> usersList(new FbRespondGetUserList( rootNode));
        if (usersList->mUserList) {
            unsigned int countainerSize = GetLimit() > 0 ? GetLimit() : RESPONSE_CONTAINER_SIZE;
            Eina_Array *parsedItemsArray = eina_array_new( countainerSize );
            if ( parsedItemsArray == NULL ) {
                Log::error( "UsersByIdProvider::ParseData cannot allocate a memory for a request array." );
                return NONE_ITEMS;
            }
            Eina_List *list;
            void *data;
            EINA_LIST_FOREACH (usersList->mUserList, list, data) {
                ++items;
                User *user = static_cast<User*>(data);
                if (!eina_array_push(parsedItemsArray, user)) {
                    Log::error( "UsersByIdProvider::ParseData, cannot push new user" );
                } else {
                    Log::info( "UsersByIdProvider::ParseData, id [ %s ], name [ %s ], friendship_status [ %d ]",
                            user->GetId(), user->mName, user->mFriendshipStatus);
                    usersList->RemoveDataFromList(user);
                }
            }
            if (eina_array_count(parsedItemsArray) == 0) {
                SwitchContinueRequest();
            } else {
                AppendData(parsedItemsArray, isDescendingOrder);
            }
            eina_array_free(parsedItemsArray);
        }
    } else {
        Log::error("UsersByIdProvider::ParseData, Can't parse data!");
    }
    m_HandledItems += items;
    return items;
}

UPLOAD_DATA_RESULT UsersByIdProvider::UploadData(bool isDescendingOrder) {
    Log::info( "UsersByIdProvider::UploadData(), isDescendingOrder [ %s ]", (isDescendingOrder ? "true" : "false") );
    int idsCount = eina_array_count( m_Ids );
    if (m_HandledItems >= idsCount) {
        Log::info( "UsersByIdProvider::UploadData(), the data is over" );
        return NONE_DATA_COULD_BE_RESULT;
    }
    std::stringstream request;
    request << '?' << FIELDS << ID << ','<< NAME << ',' << PICTURE << ',' << CONTEXT << ',' << "friendship_status";

    return RequestData( request.str().c_str(), isDescendingOrder );
}

std::string UsersByIdProvider::PrepareIds(bool isDescendingOrder) {
    UNUSED(isDescendingOrder);

    unsigned int idsCount = eina_array_count( m_Ids );
    std::stringstream ids;
    //Notice, FB-server rejects requests containing more than 50 items.
    for ( unsigned short step = 1; (m_IdLastIndex < idsCount && step <= MAX_REQUEST_ITEMS ); ++step, ++m_IdLastIndex) {
        if (!ids.str().empty()) {
            ids << ',';
        }
        ids << static_cast<const char*>(eina_array_data_get( m_Ids, m_IdLastIndex ));
    }
    return ids.str();
}

void UsersByIdProvider::DeleteItem(void *item) {
    if (!eina_array_remove(GetData(), KeepItemByPointer, item)) {
        Log::error( "UsersByIdProvider: cannot delete the referenced element [ %s ]", static_cast<char*>( item ) );
    }
}

void UsersByIdProvider::ReleaseItem(void* item) {
    BaseObject *baseObject = static_cast<BaseObject*>(item);
    baseObject->Release();
}

void UsersByIdProvider::FillManualPagination(bool isDescendigOrder) {
    std::string ids = PrepareIds( isDescendigOrder );
    free( m_Pagination.tail );
    m_Pagination.tail = NULL;
    if( ids.length() ) {
        m_Pagination.tail = strdup( ids.c_str() );
    }
}

bool UsersByIdProvider::GetPagingAttributes(std::stringstream &finalRequest, bool isDescendingOrder, bool isExtraParams) {
    bool result = false;
    if (m_Pagination.tail) {
        result = AbstractDataProvider::GetPagingAttributes(finalRequest, isDescendingOrder, isExtraParams);
    }
    return result;
}
