#include "Album.h"
#include "AlbumsProvider.h"
#include "AlbumsScreen.h"
#include "Common.h"
#include "ErrorHandling.h"
#include "FbRespondGetAlbums.h"
#include "jsonutilities.h"
#include "Log.h"
#include "ParserWrapper.h"

#include <eina_array.h>
#include <memory>
#include <sstream>

AlbumsProvider::AlbumsProvider() : mAction(nullptr) {
    Log::info("AlbumsProvider::AlbumsProvider");
}

AlbumsProvider::~AlbumsProvider() {
}

AlbumsProvider* AlbumsProvider::GetInstance() {
    static AlbumsProvider* albumsProvider = new AlbumsProvider();
    return albumsProvider;
}

void AlbumsProvider::SetUserId(const char* userId, ActionBase* action) {
    Log::info("AlbumsProvider::SetUserId: userId(%s), action(%p)", userId, action);
    EraseData();
    mUserId = userId ? userId : "";
    mAction = action;
}

unsigned int AlbumsProvider::ParseData(char* data, bool isDescendingOrder, Ecore_Thread* thread) {
    Log::info("AlbumsProvider::ParseData()");
    CHECK_RET(!mUserId.empty(), 0u);
    CHECK_RET(mAction, 0u);
    unsigned int albumsTotal(0u);
    JsonObject* object = nullptr;
    ParserWrapper wr(openJsonParser(data, &object));
    if (object) {
        std::unique_ptr<FbRespondGetAlbums> albumsResponse(new FbRespondGetAlbums(object));
        unsigned int countainerSize = GetLimit() > 0 ? GetLimit() : RESPONSE_CONTAINER_SIZE;
        Eina_Array* parsedItemsArray = eina_array_new(countainerSize);
        CHECK_RET(parsedItemsArray, NONE_ITEMS);
        albumsTotal = eina_list_count(albumsResponse->mAlbumsList);
        AlbumsScreen::ArrangeItems(albumsResponse->mAlbumsList, parsedItemsArray, mUserId.c_str(), mAction);
        AppendData(parsedItemsArray, isDescendingOrder);
        albumsResponse->mAlbumsList = eina_list_free(albumsResponse->mAlbumsList);
        eina_array_free(parsedItemsArray);
    }
    return albumsTotal;
}

void AlbumsProvider::CustomErase() {
    AlbumsProviderBase::CustomErase();
    mAction = nullptr;
}
