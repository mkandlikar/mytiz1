/*
 * FriendRequestsProvider.cpp
 *
 * Created on: Jul 23, 2015
 * Author: Dmitry.Shcherbakov@SymphonyTeleca.com
 */

#include <sstream>
#include <dlog.h>
#include "jsonutilities.h"
#include "ParserWrapper.h"

#include "FbRespondGetFriendRequests.h"
#include "FriendRequestsProvider.h"
#include "FriendRequest.h"
#include "Common.h"
#include "Utils.h"

const char* FriendRequestsProvider::FRIEND_REQUESTS_MAIN_REQUEST_PART = "me/friendrequests";

FriendRequestsProvider::FriendRequestsProvider()
{
    SetAPIVersion( API_VER_1_0 );
    m_Ids = eina_array_new( RESPONSE_CONTAINER_SIZE);
    if ( !m_Ids ) {
        dlog_print( DLOG_ERROR, LOG_TAG_FACEBOOK, "FriendRequestsProvider: cannot allocate a memory for an array." );
    }
}

FriendRequestsProvider::~FriendRequestsProvider(){
    EraseData();
    ClearIds();
    eina_array_free (m_Ids); m_Ids = NULL;
}

FriendRequestsProvider* FriendRequestsProvider::GetInstance()
{
    static FriendRequestsProvider *friendRequestsProvider = new FriendRequestsProvider();
    return friendRequestsProvider;
}

void FriendRequestsProvider::ClearIds() {
    if ( m_Ids ) {
        unsigned int index = 0;
        void* item;
        Eina_Array_Iterator iterator;
        EINA_ARRAY_ITER_NEXT( m_Ids, index, item, iterator ) {
            free(item);
        }
        eina_array_clean( m_Ids );
    }
}

void FriendRequestsProvider::CustomErase() {
    ClearIds();
}

unsigned int FriendRequestsProvider::ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread )
{
    JsonObject *object = NULL;
    JsonParser *parser = openJsonParser( data, &object );
    ParserWrapper wr( parser );
    unsigned int itemsCount = NONE_ITEMS;
    if ( object ) {
        FbRespondGetFriendRequests * friendReqResponse = new FbRespondGetFriendRequests( object );
        if ( friendReqResponse->mRequestsList ) {
            unsigned int countainerSize = GetLimit() > 0 ? GetLimit() : RESPONSE_CONTAINER_SIZE;
            Eina_Array *parsedItemsArray = eina_array_new( countainerSize );
            if ( parsedItemsArray == NULL ) {
                dlog_print( DLOG_DEBUG, LOG_TAG_FACEBOOK, "FriendRequestsProvider: cannot allocate a memory for a request array." );
                return NONE_ITEMS;
            }
            Eina_List *list;
            void *data;
            EINA_LIST_FOREACH( friendReqResponse->mRequestsList, list, data ) {
                ++itemsCount;
                FriendRequest *friendReq = static_cast<FriendRequest*>(data);
                eina_array_push( parsedItemsArray, friendReq );
                friendReqResponse->RemoveDataFromList(friendReq);
                if ( NO_ITEMS_FOUND == Utils::FindElement((void *)friendReq->mFrom->GetId(), m_Ids, idsComparator) ) {
                    eina_array_push( m_Ids, strdup(friendReq->mFrom->GetId()) );
                }
            }
            dlog_print( DLOG_DEBUG, LOG_TAG_FACEBOOK, "FriendRequestsProvider: there was handled %d friend requests.", itemsCount );
            AppendData( parsedItemsArray, isDescendingOrder );
            eina_array_free( parsedItemsArray );
        }
        delete friendReqResponse;
    }
    return itemsCount;
}

UPLOAD_DATA_RESULT FriendRequestsProvider::UploadData( bool isDescendingOrder )
{
    if ( eina_array_count( GetData() ) == GetItemsTotalCount() ) {
        //
        // We cannot request more data
        return NONE_DATA_COULD_BE_RESULT;
    }
    std::stringstream request;

    request << FRIEND_REQUESTS_MAIN_REQUEST_PART << "?" << FIELDS;
    request << FROM << '{' << NAME << ',' << ID << ',' << PICTURE << '}';

    return RequestData( request.str().c_str(), isDescendingOrder );
}

void FriendRequestsProvider::ReleaseItem( void* item ) {
    FriendRequest *friendReq = static_cast<FriendRequest*>(item);
    delete friendReq;
}

Eina_Array* FriendRequestsProvider::GetIds() const {
    return m_Ids;
}

bool FriendRequestsProvider::IsRequest(const void * object){
    bool res = false;
    int arraySize = 0;

    if(GetIds()){
        arraySize = eina_array_count(GetIds());
    }

    for(unsigned int i = 0; i< arraySize; ++i)
    {
        const void *id = eina_array_data_get(GetIds(),i);
        if(idsComparator(object, id)){
            res = true;
            break;
        }
    }
    return res;
}
