#include "Comment.h"
#include "CommentsProvider.h"
#include "DataEventDescription.h"
#include "Log.h"
#include "SimpleCommentOperation.h"

const unsigned int CommentsProvider::COMMENTS_DEFAULT_REPLY_LIMIT = 1;

CommentsProvider::CommentsProvider() {
    SetReplyCount(COMMENTS_DEFAULT_REPLY_LIMIT);
    Log::info("CommentsProvider::CommentsProvider" );
    AppEvents::Get().Subscribe(eREPLACE_GRAPH_OBJ_FOR_EDITED_COMMENT, this);
}

CommentsProvider::~CommentsProvider() {
    Log::info("CommentsProvider::~CommentsProvider" );
}

void CommentsProvider::Update(AppEventId eventId, void * data) {
    Log::debug(LOG_FACEBOOK_OPERATION, "CommentsProvider::Update start eventId = %d", eventId );
    switch(eventId){
    case eOPERATION_ADDED_EVENT:
        if (data) {
            Sptr<Operation> oper(static_cast<Operation*>(data));
            if(oper->GetType() == Operation::OT_Add_Comment ||
               oper->GetType() == Operation::OT_Add_Reply) {
                Sptr<SimpleCommentOperation> scop(static_cast<SimpleCommentOperation*>(oper.Get()));
                if(scop->GetParentId() && m_EntityId && !strcmp(scop->GetParentId(), m_EntityId)) {
                    AddOperation(oper);
                }
            }
        }
        break;
    case eOPERATION_REMOVED_EVENT:
        if (data) {
            Sptr<Operation> oper(static_cast<Operation*>(data));
            switch (oper->GetType()) {
            case Operation::OT_Add_Comment:
            case Operation::OT_Add_Reply: {
                Sptr<SimpleCommentOperation> scop(static_cast<SimpleCommentOperation*>(oper.Get()));
                if(scop->GetParentId() && m_EntityId && !strcmp(scop->GetParentId(), m_EntityId)) {

                    if (scop->GetOperationResult() == Operation::OR_Success && scop->GetComment()) {
                        RemoveOperation(oper);

                        if(!IsContainItem( scop->GetComment() )) {
                            scop->GetComment()->AddRef();
                            AppendData(scop->GetComment(), IsReverseChronological());
                        
                            DataEventDescription dataEvent(this,
                                DataEventDescription::eDE_ITEM_ADDED,
                                scop->GetComment()->GetId(),
                                IsReverseChronological());
                            AppEvents::Get().Notify(eDATA_CHANGES_EVENT, &dataEvent);
                        } else {
                            Log::error("CommentsProvider::Update->Item is in provider already" );
                        }
                    }
                }
            }
                break;
            case Operation::OT_Edit_CommentReply:
            {
                Sptr<SimpleCommentOperation> scop(static_cast<SimpleCommentOperation*>(oper.Get()));
                if (scop->GetComment() && scop->GetOperationResult() == Operation::OR_Success) {
                    if (ReplaceGraphObjectById(scop->GetComment()->GetId(), scop->GetComment(), GetData()) ) {
                        DataEventDescription dataEvent(this,
                            DataEventDescription::eDE_COMMENT_EDITED,
                            scop->GetComment()->GetId(),
                            true);
                        AppEvents::Get().Notify(eDATA_CHANGES_EVENT, &dataEvent);
                    } else {
                        Log::error("CommentsProvider::Update->can't replace old comment with new" );
                    }
                }
            }
            default: { }
            }
        }
        break;
    case eREPLACE_GRAPH_OBJ_FOR_EDITED_COMMENT:
        if(data) {
            GraphObject *op = static_cast<GraphObject*>(data);
            if (!ReplaceGraphObjectById(op->GetId(), op, GetData()) ) {
                Log::error("CommentsProvider::Update->can't replace edited comment with presenter" );
            }
        }
        break;
    default:
        break;
    }
    Log::info(LOG_FACEBOOK_OPERATION, "CommentsProvider::Update end" );
}

bool CommentsProvider::ReplaceCommentObject(Comment *comment) {
    return ReplaceGraphObjectById(comment->GetId(), comment, GetData());
}
