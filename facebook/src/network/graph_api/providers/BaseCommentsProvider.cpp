#include <assert.h>
#include <sstream>
#include "jsonutilities.h"

#include "BaseCommentsProvider.h"
#include "Common.h"
#include "Comment.h"
#include "FbRespondGetCommentsList.h"
#include "Log.h"
#include "ParserWrapper.h"
#include "OperationManager.h"

const unsigned int BaseCommentsProvider::DEFAULT_REPLAY_LIMIT = 1;
const unsigned int BaseCommentsProvider::DEFAULT_REPLAY_SUMMARY_COUNT = 1;

const unsigned int BaseCommentsProvider::DEFAULT_SUMMARY_COUNT = 1;
const unsigned int BaseCommentsProvider::REQUEST_COMMENT_LIMIT = 25;

BaseCommentsProvider::BaseCommentsProvider()
{
    m_EntityId = NULL;
    m_SummaryCount = DEFAULT_SUMMARY_COUNT;
    m_RepliesLimit = DEFAULT_REPLAY_LIMIT;
    m_RepliesSummaryCount = DEFAULT_REPLAY_SUMMARY_COUNT;
    SetLimit( REQUEST_COMMENT_LIMIT );
    SetPagingAlgorithm( CURSOR_ALGORITHM );
    OperationManager::GetInstance()->Subscribe(this);
    mProviderType = COMMENTS_PROVIDER;
}

BaseCommentsProvider::~BaseCommentsProvider(){
    OperationManager::GetInstance()->UnSubscribe(this);
    EraseData();
}

void BaseCommentsProvider::SetReplyCount( unsigned int count ) {
    m_RepliesLimit = count;
}

void BaseCommentsProvider::SetEntityId( const char *id )
{
    m_EntityId = id;
}

unsigned int BaseCommentsProvider::ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread )
{
    JsonObject *object = NULL;
    JsonParser *parser = openJsonParser( data, &object );
    ParserWrapper wr( parser );
    unsigned int itemsCount = NONE_ITEMS;
    if ( object ) {
        FbRespondGetCommentsList * commentsResponse = new FbRespondGetCommentsList( object );
        if ( commentsResponse->GetCommentsList() ) {
            unsigned int countainerSize = GetLimit() > 0 ? GetLimit() : RESPONSE_CONTAINER_SIZE;
            Eina_Array *parsedItemsArray = eina_array_new( countainerSize );
            if (!parsedItemsArray) {
                Log::error("BaseCommentsProvider: cannot allocate a memory for a request array." );
                return NONE_ITEMS;
            }
            Eina_List *list;
            void *data;
            if ( IsReverseChronological() ) {
                EINA_LIST_FOREACH( commentsResponse->GetCommentsList(), list, data ) {
                    ++itemsCount;
                    Comment *comment = static_cast<Comment*>(data);

                    if( !IsContainItem( comment ) ) {
                        eina_array_push( parsedItemsArray, comment );
                        commentsResponse->RemoveDataFromList(comment);
                    }
                }
                isDescendingOrder = !isDescendingOrder;
            } else {
                EINA_LIST_REVERSE_FOREACH( commentsResponse->GetCommentsList(), list, data ) {
                    ++itemsCount;
                    Comment *comment = static_cast<Comment*>(data);

                    if( !IsContainItem( comment ) ) {
                        eina_array_push( parsedItemsArray, comment );
                        commentsResponse->RemoveDataFromList(comment);
                    }
                }
            }
            Log::debug("BaseCommentsProvider: there was handled %d items.", itemsCount );
            AppendData( parsedItemsArray, isDescendingOrder );
            eina_array_free( parsedItemsArray );
        }
        delete commentsResponse;
    }
    return itemsCount;
}

UPLOAD_DATA_RESULT BaseCommentsProvider::UploadData( bool isDescendingOrder )
{
    if ( !m_EntityId ) {
        Log::error("BaseCommentsProvider: entity identifier was not specified." );
        return NONE_DATA_COULD_BE_RESULT;
    }
    if ( eina_array_count( GetData() ) == GetItemsTotalCount() && !isDescendingOrder) {
        //
        // We cannot request more data
        return NONE_DATA_COULD_BE_RESULT;
    }

    std::stringstream request;

    request << m_EntityId << '/' << COMMENTS << "?" << FILTER
            << '=' << TOPLEVEL << '&' << SUMMARY << '=' << m_SummaryCount << '&' << FIELDS << CAN_COMMENT << ','
            << CAN_LIKE << ',' << CAN_REMOVE << ',' << MESSAGE << ',' << MESSAGE_TAGS << ',' << FROM << '{' << ID << ',' << NAME << ',' << PICTURE << "},"
            << ID << ',' << USER_LIKES << ',' << CREATED_TIME << ',' << COMMENT_COUNT << ',' << ATTACHMENT << ','
            << LIKE_COUNT << ',' << IS_HIDDEN << ',';
    //
    // replay fields
    request << COMMENTS << '.' << LIMIT << '(' << m_RepliesLimit << ")." << SUMMARY << '(' << m_RepliesSummaryCount
            << ")." << ORDER << '(' << REVERSE_CHRONOLOGICAL << "){" << CAN_COMMENT << ',' << CAN_REMOVE << ',' << CAN_LIKE << ','
            << MESSAGE << ',' << FROM << '{' << ID << ',' << NAME << ',' << PICTURE << "}," << ID << ','
            << USER_LIKES << ',' << CREATED_TIME << ',' << COMMENT_COUNT << ',' << ATTACHMENT << ','
            << LIKE_COUNT << ',' << IS_HIDDEN << '}';

    return RequestData( request.str().c_str(), isDescendingOrder );
}

void BaseCommentsProvider::ReleaseItem( void* item ) {
    BaseObject *baseObject = static_cast<BaseObject*>(item);
    baseObject->Release();
}

void BaseCommentsProvider::DeleteItem( void* item ) {
    if ( !eina_array_remove( GetData(), KeepItemById, item ) ) {
        Log::error("BaseCommentsProvider: cannot delete the referenced element" );
    }
}

