#include "CacheManager.h"
#include "Config.h"
#include "ErrorHandling.h"
#include "Friend.h"
#include "Log.h"
#include "MutexLocker.h"
#include "OwnFriendsProvider.h"
#include "ParserWrapper.h"
#include "Utils.h"

OwnFriendsProvider::OwnFriendsProvider(): m_Ids(RESPONSE_CONTAINER_SIZE) {
}

OwnFriendsProvider::~OwnFriendsProvider() {
    ClearIds();
}

OwnFriendsProvider* OwnFriendsProvider::GetInstance()
{
    static OwnFriendsProvider *chooseFriendProvider = new OwnFriendsProvider();
    return chooseFriendProvider;
}

void OwnFriendsProvider::ClearIds() {
    MutexLocker locker(&m_IdsMutex);
    m_Ids.clear();
}

UPLOAD_DATA_RESULT OwnFriendsProvider::UploadData( bool isDescendingOrder )
{
    if ( GetDataCount() == GetItemsTotalCount() ) {
        //
        // We cannot request more data
        return NONE_DATA_COULD_BE_RESULT;
    }

    if ( IsCachedDataShouldBeSet() ) {
        if ( RetrieveCachedData() ) {
            SetDataCached(true);
            return ONLY_CACHED_DATA_RESULT;
        }
    }

    std::stringstream request;

    request << FRIENDS_REQUEST_PART << "?" << FIELDS;

#ifdef FEATURE_MUTUAL_FRIENDS
    request << ID << ',' << NAME << ',' <<  FIRST_NAME << ',' << LAST_NAME << ',' << "email" << ',' << "mobile_phone" << ',' << BIRTHDAY << ',' << "friendship_status" << ',' << PICTURE << ".width(200).height(200)"
        << ',' << CONTEXT << '{' << MUTUAL_FRIENDS << '.' << LIMIT << '(' << m_MutualFriendsLimit << ")}";
#else
    request << ID << ',' << NAME << ',' <<  FIRST_NAME << ',' << LAST_NAME << ',' << "email" << ',' << "mobile_phone" << ',' << BIRTHDAY << ',' << "friendship_status" << ',' << PICTURE << ".width(200).height(200)";
#endif

    return RequestData( request.str().c_str(), isDescendingOrder );
}

unsigned int OwnFriendsProvider::ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread )
{
    MutexLocker locker(&m_IdsMutex);
    JsonObject *object = NULL;
    JsonParser *parser = openJsonParser( data, &object );
    ParserWrapper wr( parser );
    Log::info("OwnFriendsProvider::ParseData");
    unsigned int itemsCount = NONE_ITEMS;
    if ( object ) {
        FbRespondGetFriends *friendsRespond = new FbRespondGetFriends(object);
        bool isLastPage = IsItLastPage(object);
        if ( friendsRespond->mFriendsList ) {
            unsigned int countainerSize = GetLimit() > 0 ? GetLimit() : RESPONSE_CONTAINER_SIZE;
            Eina_Array *parsedItemsArray = eina_array_new( countainerSize );
            if ( !parsedItemsArray) {
                Log::error("OwnFriendsProvider::ParseData->cannot allocate memory for a request array");
                return NONE_ITEMS;
            }
            if (IsDataCached()) {
                SetDataCached(false, thread);
                CleanData();
            }
            CacheManager::GetInstance().SetFriendsData(friendsRespond);

            Eina_List *list;
            Eina_List *l_next;
            void *data;
            EINA_LIST_FOREACH_SAFE(friendsRespond->mFriendsList, l_next, list, data) {
                ++itemsCount;
                Friend *fr = static_cast<Friend*>(data);
                if(fr) {
                    eina_array_push( parsedItemsArray, fr );
                    friendsRespond->RemoveDataFromList(fr);
                    const char *frId = fr->GetId();
                    bool isAlreadySaved = false;
                    if(frId) {
                        for(std::vector<std::string>::iterator it = m_Ids.begin(); it != m_Ids.end(); it++) {
                            if(*it == frId) {
                                isAlreadySaved = true;
                                break;
                            }
                        }
                        if (!isAlreadySaved) {
                            m_Ids.push_back(frId);
                        }
                    }
                }
            }

            bool isSelfAlreadySaved = false;
            std::string selfId(Config::GetInstance().GetUserId());
            for(std::vector<std::string>::iterator it = m_Ids.begin(); it != m_Ids.end(); it++) {
                if(*it == selfId) {
                    isSelfAlreadySaved = true;
                    break;
                }
            }
            if(!isSelfAlreadySaved) {
                m_Ids.push_back(selfId);
            }

            Log::debug("OwnFriendsProvider: handled %d items and parsed %d friends", itemsCount, eina_array_count(parsedItemsArray));
            AppendData(parsedItemsArray, isDescendingOrder);
            AppendToFilteredDataArray();
            eina_array_free(parsedItemsArray);
            if (isLastPage) {
                SortData();
            }
        }
        delete friendsRespond;
    }
    return itemsCount;
}

bool OwnFriendsProvider::IsCachedDataShouldBeSet() {
    bool ret = !mMissCacheRetrieve && AbstractDataProvider::GetDataCount() == 0;
    //GetDataCount() could returns filtered data count
    mMissCacheRetrieve = false;
    return ret;
}

bool OwnFriendsProvider::RetrieveCachedData()
{
    Log::info("OwnFriendsProvider::RetrieveCachedData()");

    Eina_List *friendsList = CacheManager::GetInstance().SelectFriendsData();

    Eina_Array *array = eina_array_new(256);

    Eina_List *list;
    void *data;
    EINA_LIST_FOREACH(friendsList, list, data) {
        Friend *fr = static_cast<Friend*>(data);
        eina_array_push(array, fr);
    }

    if (eina_array_count(array) > 0 ) {
        AppendData(array, true);
    }
    eina_array_free(array);
    return AbstractDataProvider::GetDataCount();
}

bool OwnFriendsProvider::IsFriend(const void * object)
{
    assert(object);
    bool res = false;
    const char* friendId = static_cast<const char*>(object);
    for(std::vector<std::string>::iterator it = m_Ids.begin(); it != m_Ids.end(); it++) {
        if(*it == friendId) {
            res = true;
            break;
        }
    }
    return res;
}

std::list<Friend*> OwnFriendsProvider::GetDataCopy() {
    MutexLocker(&this->m_Mutex);
    std::list<Friend*> dataCopy;
    unsigned int i(0);
    void* item(nullptr);
    Eina_Array_Iterator iter;
    EINA_ARRAY_ITER_NEXT(GetData(), i, item, iter) {
        Friend* fbFriend = static_cast<Friend*>(item);
        CHECK_CONT(fbFriend);
        fbFriend->AddRef();
        dataCopy.push_back(fbFriend);
    }
    return dataCopy;
}

void OwnFriendsProvider::DeleteItemById(const char * id) {
    Friend * user_fr = GetFriendById(id);
    if(user_fr){
        DeleteItem(user_fr);
    }
}
