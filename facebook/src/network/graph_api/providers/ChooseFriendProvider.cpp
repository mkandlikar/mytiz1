#include "Common.h"
#include "ChooseFriendProvider.h"
#include "Friend.h"
#include "OwnFriendsProvider.h"
#include "Log.h"
#include "Separator.h"
#include "Utils.h"

#include <assert.h>
#include <sstream>
#include <utils_i18n.h>

ChooseFriendProvider::ChooseFriendProvider()
{
    mTextToFind = NULL;
    mFilteredData = eina_array_new(BASE_CONTAINER_SIZE);
}

ChooseFriendProvider::~ChooseFriendProvider()
{
    EraseData();
    eina_array_free(mFilteredData); mFilteredData = NULL;
}

void ChooseFriendProvider::CustomErase()
{
    if (mFilteredData) {
        eina_array_clean(mFilteredData);
    }
    free((void *) mTextToFind); mTextToFind = NULL;
    Log::info("ChooseFriendProvider::CustomErase");
}

void ChooseFriendProvider::FilterData()
{
    Log::info("ChooseFriendProvider::FilterData");

    if (eina_array_count(mFilteredData) > 0) {
        Log::debug("ChooseFriendProvider::FilterDataFREE");
        eina_array_clean(mFilteredData);
    }

    if (mTextToFind) {
        int len = AbstractDataProvider::GetDataCount();
        for (int i = 0; i < len; i++) {
            GraphObject *check = static_cast<GraphObject*>(eina_array_data_get(AbstractDataProvider::GetData(), i));
            if (check->GetGraphObjectType() == GraphObject::GOT_FRIEND) {
                Friend *friends = static_cast<Friend*>(check);
                if (friends) {
                    if (Utils::UAreAllWordsSubstringsOfAnyWord(friends->mName.c_str(), mTextToFind, false)) {
                        eina_array_push(mFilteredData, friends);
                    }
                }
            }
        }
    }
}

void ChooseFriendProvider::SetFindText(const char *text)
{
    Log::info("ChooseFriendProvider::SetFindText");
    if (mTextToFind) {
        free((void *) mTextToFind);
    }
    mTextToFind = SAFE_STRDUP(text);
    FilterData();
}

Eina_Array* ChooseFriendProvider::GetData() const
{
    Eina_Array* ret = nullptr;
    if (mTextToFind && (*mTextToFind != '\0')) {
        ret = mFilteredData;
    } else {
        ret = AbstractDataProvider::GetData();
    }
    Log::info("ChooseFriendProvider::GetData() (%d)", eina_array_count(ret));
    return ret;
}

unsigned int ChooseFriendProvider::GetDataCount() const
{
    return eina_array_count(GetData());
}

ChooseFriendProvider* ChooseFriendProvider::GetInstance()
{
    static ChooseFriendProvider *chooseFriendProvider = new ChooseFriendProvider();
    return chooseFriendProvider;
}

unsigned int ChooseFriendProvider::ParseData(char *data, bool isDescendingOrder, Ecore_Thread *thread )
{
    return NONE_ITEMS;
}

UPLOAD_DATA_RESULT ChooseFriendProvider::UploadData(bool isDescendingOrder)
{
    if (AbstractDataProvider::GetDataCount() == 0) {
        CreateFriendsData(isDescendingOrder);
    }
    return NONE_DATA_COULD_BE_RESULT;
}

void ChooseFriendProvider::CreateFriendsData(bool isDescendingOrder)
{
    AbstractDataProvider *provider = OwnFriendsProvider::GetInstance();
    int count = provider->AbstractDataProvider::GetDataCount();

    Eina_Array *parsedItemsArray = eina_array_new(RESPONSE_CONTAINER_SIZE);
    Eina_Array *sortedItemsArray = eina_array_new(RESPONSE_CONTAINER_SIZE);

    for (int i = 0; i < count; ++i) {
        eina_array_push(parsedItemsArray, eina_array_data_get(provider->AbstractDataProvider::GetData(), i));
    }

    if (count != 1) {
        for (int i = 1; i < count; ++i) {
            for (int j = i; j > 0; --j) {
                Friend *friend_1 = static_cast<Friend*>(eina_array_data_get(parsedItemsArray, j));
                Friend *friend_2 = static_cast<Friend*>(eina_array_data_get(parsedItemsArray, j-1));
                if (friend_1 && friend_2) {
                    if (strcmp(friend_2->mName.c_str(), friend_1->mName.c_str()) > 0) {
                        eina_array_data_set(parsedItemsArray, j-1, friend_1);
                        eina_array_data_set(parsedItemsArray, j, friend_2);
                    }
                }
            }
        }

        for (int k = 0; k < count; ++k)
        {
            Friend *friend_1 = static_cast<Friend*>(eina_array_data_get(parsedItemsArray, k));
            if (friend_1) {
                if (k < 1) {
                    const char * firstNameSymbol = GetFirstNameSymbol(friend_1->mName.c_str());
                    Separator *separator = new Separator(Separator::FRIENDS_SEPARATOR, firstNameSymbol);
                    delete [] firstNameSymbol;
                    eina_array_push(sortedItemsArray, separator);
                    eina_array_push(sortedItemsArray, friend_1);
                    friend_1->AddRef();
                } else {
                    Friend *friend_0 = static_cast<Friend*>(eina_array_data_get(parsedItemsArray, k-1));
                    if (friend_0) {
                        if (strncmp(friend_1->mName.c_str(), friend_0->mName.c_str(), 1)) {
                            const char * firstNameSymbol = GetFirstNameSymbol(friend_1->mName.c_str());
                            Separator *separator = new Separator(Separator::FRIENDS_SEPARATOR, firstNameSymbol);
                            delete [] firstNameSymbol;
                            eina_array_push(sortedItemsArray, separator);
                        }
                        eina_array_push(sortedItemsArray, friend_1);
                        friend_1->AddRef();
                    }
                }
            }
        }
    } else {
        Friend *friend_1 = static_cast<Friend*>(eina_array_data_get(parsedItemsArray, 0));
        if (friend_1) {
            Separator *separator = new Separator(Separator::FRIENDS_SEPARATOR, GetFirstNameSymbol(friend_1->mName.c_str()));
            eina_array_push(sortedItemsArray, separator);
            eina_array_push(sortedItemsArray, friend_1);
            friend_1->AddRef();
        }
    }

    AppendData(sortedItemsArray, isDescendingOrder);

    eina_array_free(parsedItemsArray);
    eina_array_free(sortedItemsArray);
}

void ChooseFriendProvider::DeleteItem(void* item)
{
    if (!eina_array_remove(GetData(), KeepItemById, item)) {
        Log::error("ChooseFriendProvider::DeleteItem() - cannot delete the referenced element");
    }
}

char *ChooseFriendProvider::GetFirstNameSymbol(const char *name)
{
    i18n_uchar *uName = new i18n_uchar[strlen(name) + 1];
    i18n_ustring_copy_ua(uName, name);
    uName[1] = 0;

    char *firstSym = new char [16];
    i18n_ustring_copy_au(firstSym, uName);
    delete [] uName;
    return firstSym;
}
