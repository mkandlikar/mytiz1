#include <assert.h>
#include <sstream>
#include <memory>
#include <dlog.h>
#include "jsonutilities.h"
#include "ParserWrapper.h"

#include <curl/curl.h>

#include <cstring>
#include "Common.h"
#include "FriendRequestsBatchProvider.h"
#include "CacheManager.h"
#include "FbRespondGetPeopleYouMayKnow.h"
#include "FbRespondGetFriendRequestsBatch.h"
#include "MutexLocker.h"
#include "User.h"
#include "RequestCompleteEvent.h"
#include "Separator.h"

const char* FriendRequestsBatchProvider::REST_PEOPLE_YOU_MAY_KNOW_REQUEST = "method/friendfinder.pymk?format=JSON&flow=FRIENDS_TAB&pic_size=120";

#ifdef FEATURE_MUTUAL_FRIENDS
const char* FriendRequestsBatchProvider::BATCH_REQUEST_FRIENDS_REQUEST = "[{'method':'GET','name':'r1','relative_url':'v1.0/me/friendrequests%3Flimit=30%26after=', 'omit_response_on_success': false},{'method':'GET','name':'r2','relative_url':'v2.4/?fields=id,first_name,last_name,name,picture.width(200).height(200),context.fields(mutual_friends.limit(0))%26ids={result=r1:$.data.*.from.id}'}]";
#else
const char* FriendRequestsBatchProvider::BATCH_REQUEST_FRIENDS_REQUEST = "[{'method':'GET','name':'r1','relative_url':'v1.0/me/friendrequests%3Flimit=30%26after=', 'omit_response_on_success': false},{'method':'GET','name':'r2','relative_url':'v2.4/?fields=id,first_name,last_name,name,picture.width(200).height(200)%26ids={result=r1:$.data.*.from.id}'}]";
#endif

Eina_Bool KeepByCharId( void *forCompare, void *toCompare ) {
    if ( !forCompare || !toCompare ) {
        assert( false );
        return EINA_FALSE;
    }
    Eina_Bool result = ( !(strcmp( (const char*)forCompare, (const char*)toCompare ) == 0 ) );
    if ( !result ) {
        free( forCompare );
    }
    return result;
}

FriendRequestsBatchProvider::FriendRequestsBatchProvider()
{
    mPYMK_Ids = eina_array_new( RESPONSE_CONTAINER_SIZE );
    mFR_Ids = eina_array_new( RESPONSE_CONTAINER_SIZE );
    mFriendRequestsArray = eina_array_new(RESPONSE_CONTAINER_SIZE);
    mPYMKArray = eina_array_new(RESPONSE_CONTAINER_SIZE);
    mFR_Header = eina_array_new( 2 );
    mPYMK_Header =  eina_array_new( 1 );
    mUnreadBadgeNum = 0;
    mRequestsTotalCount = 0;
    mBothRequestsWereDone = false;
    mProviderType = FRIENDS_REQUESTS_BATCH_PROVIDER;
    SetFriendsRequest();
    mDeleteInvalidFRs = false;
    mParsedItemsList = nullptr;
    eina_lock_new( &m_IdsMutex );
}

FriendRequestsBatchProvider::~FriendRequestsBatchProvider() {
    EraseData();
    eina_array_free( mPYMK_Ids );
    eina_array_free( mFR_Ids );
    eina_array_free( mFriendRequestsArray );
    eina_array_free( mPYMKArray );
    eina_array_free( mFR_Header );
    eina_array_free( mPYMK_Header );
    mParsedItemsList = eina_list_free( mParsedItemsList );
    eina_lock_free( &m_IdsMutex );
}

FriendRequestsBatchProvider* FriendRequestsBatchProvider::GetInstance()
{
    static FriendRequestsBatchProvider *friendRequestsBatchProvider = new FriendRequestsBatchProvider();
    return friendRequestsBatchProvider;
}

unsigned int FriendRequestsBatchProvider::ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread )
{
    MutexLocker locker( &m_IdsMutex );
    JsonObject *object = NULL;
    JsonParser *parser = openJsonParser(data, &object);
    //We use root node here, because respond can have array instead object as root element
    JsonNode* rootNode = json_parser_get_root(parser);
    ParserWrapper wr(parser);
    unsigned int itemsCount = NONE_ITEMS;
    switch ( m_CurrentRequest )
    {
        case FRIENDS_REQUEST:
        {
            Log::info("FriendRequestsBatchProvider::ParseData, FRIENDS_REQUEST");

            if(mDeleteInvalidFRs) {
                //part of provider related to FriendsRequests and mFR_Ids array will be fully updated in this case
                ClearFRIds();
                RemoveData(mParsedItemsList);

                mParsedItemsList = eina_list_free(mParsedItemsList);

                if ( mFriendRequestsArray ) {
                    eina_array_clean ( mFriendRequestsArray );
                }
                mDeleteInvalidFRs = false;
            }

            std::unique_ptr<FbRespondGetFriendRequestsBatch> requestRespond( new FbRespondGetFriendRequestsBatch( rootNode ) );
           // bool isItLast = IsItLastPage( object );// Page always last and first((

            Eina_List* listToMerge = nullptr;
            Eina_List *list;
            Eina_List *l_next;
            void *data;
            EINA_LIST_FOREACH_SAFE( requestRespond->mUsersList, list, l_next, data ) {
                User *user = static_cast<User*>(data);
                if (user && !IsContainItem( user ) ) {
                    user->SetGraphObjectType( GraphObject::GOT_FRIEND_REQUEST );
                    mParsedItemsList = eina_list_append(mParsedItemsList, user);
                    listToMerge = eina_list_append(listToMerge, user);
                    eina_array_push(mFriendRequestsArray, user);
                    requestRespond->RemoveDataFromList(user);
                    ++itemsCount;
                    if(user->GetId()) {
                        char *user_id = strdup(user->GetId());
                        if(!eina_array_push( mFR_Ids, user_id )) {
                            free(user_id);
                        }
                    }
                }
            }
            if(requestRespond->mRespondGetFriendRequests && requestRespond->mRespondGetFriendRequests->mSummary) {
                SetRequestsTotalCount (requestRespond->mRespondGetFriendRequests->mSummary->mTotalCount);
            }
            Log::info("FriendRequestsBatchProvider::ParseData,[ FRIENDS_REQUEST ]: there was handled %d items.", itemsCount);
            AppendData( listToMerge, false );
            AppendNewFriendRequestsHeaderItems();
            listToMerge = eina_list_free( listToMerge );

            if(!mBothRequestsWereDone) {
                Log::info("FriendRequestsBatchProvider::ParseData->Perform PYMK request");
                SetPeopleYouMayKnowRequest();
            } else {
                Log::info("FriendRequestsBatchProvider::ParseData->redraw friend requests only");
                AppEvents::Get().Notify(eADD_NEW_FRIEND_REQUESTS);// will redraw only friend requests
            }
            break;
        }
        case PEOPLE_YOU_MAY_KNOW_REQUEST:
        {
            Log::info("FriendRequestsBatchProvider::ParseData->PEOPLE_YOU_MAY_KNOW_REQUEST");
            FbRespondGetPeopleYouMayKnow *respond = FbRespondGetPeopleYouMayKnow::createFromJson(data);
            if ( !respond ) {
                break;
            }
            CacheManager::GetInstance().SetFriendableUserData(respond);
            std::unique_ptr<FbRespondGetPeopleYouMayKnow> peopleRespond( respond );
            if ( peopleRespond->mPeople ) {
                peopleRespond->mPeople = SetPeopleYouMayKnow(peopleRespond->mPeople, true);
            }
            mBothRequestsWereDone = true;
            AppEvents::Get().Notify(eREQUEST_FOR_ERASE_EVENT);
            break;
        }
        default:
            assert( false );
    }
    return itemsCount;
}

unsigned int FriendRequestsBatchProvider::RetrieveCachedData() {
    Log::info( "FriendRequestsBatchProvider::RetrieveCachedData" );
    AppendNewFriendRequestsHeaderItems();
    unsigned int cachedItemsCount = 0;
    if( eina_array_count(mPYMKArray) == 0)  {
        LoadPeopleYouMayKnowFromCache();
        cachedItemsCount = eina_array_count(mPYMKArray);
    }
    return cachedItemsCount;
}

UPLOAD_DATA_RESULT FriendRequestsBatchProvider::UploadData(bool isDescendingOrder)
{
	Log::info( "FriendRequestsBatchProvider::UploadData->isDescendingOrder %s", isDescendingOrder? "true":"false" );
    if ( !isDescendingOrder ) {
        SetFriendsRequest();
        isDescendingOrder = true;
    }

    if ( GetDataCount() == 0 && !mMissCacheRetrive ) {
        bool isCached = RetrieveCachedData() > 0;
        if ( isCached ) {
            SetDataCached( isCached );
            return ONLY_CACHED_DATA_RESULT;
        }
    }
    mMissCacheRetrive = false;

    switch ( m_CurrentRequest )
    {
        case FRIENDS_REQUEST:
        {
            bundle* paramsBundle;
            paramsBundle = bundle_create();
            bundle_add_str(paramsBundle, "batch", BATCH_REQUEST_FRIENDS_REQUEST);
            ResetRequestTimer();
            UPLOAD_DATA_RESULT result = RequestData( "", isDescendingOrder, paramsBundle );
            bundle_free(paramsBundle);
            return result;
        }
        case PEOPLE_YOU_MAY_KNOW_REQUEST:
        {
            if ( !mBothRequestsWereDone ) {
                return RequestData( REST_PEOPLE_YOU_MAY_KNOW_REQUEST, isDescendingOrder );
            }
        }
        break;
        default:
            assert( false );
    }
    return NONE_DATA_COULD_BE_RESULT;
}

void FriendRequestsBatchProvider::DeleteItem(void* item)
{
    GraphObject *object = static_cast<GraphObject*>(item);
    //
    // firstly remove and appropriate id
    switch ( object->GetGraphObjectType() )
    {
        case GraphObject::GOT_FRIEND_REQUEST:
        {
            Log::info( "FriendRequestsBatchProvider::DeleteItem->GOT_FRIEND_REQUEST");
            if ( eina_array_count( mFR_Ids ) > 0 ) {
                if ( !eina_array_remove( mFR_Ids, KeepByCharId, (void*)object->GetId() ) ) {
                    Log::error( "FriendRequestsBatchProvider::DeleteItem cannot delete the referenced from internal friends reqs." );
                    return;
                }
            }
        }
        break;
        case GraphObject::GOT_PEOPLE_YOU_MAY_KNOW:
        {
            Log::info( "FriendRequestsBatchProvider::DeleteItem->GOT_PEOPLE_YOU_MAY_KNOW");
            if ( eina_array_count( mPYMK_Ids ) > 0 ) {
                if ( !eina_array_remove( mPYMK_Ids, KeepByCharId, (void*)object->GetId() ) ) {
                    Log::error( "FriendRequestsBatchProvider::DeleteItem cannot delete the referenced from internal People You May Know." );
                    return;
                }
            }
        }
        break;
        default:
        {
            // There could be another types, like Separator,
            // but we are not interesting for such type
        }
    }
    //
    // Secondary remove a reference from m_Data
    if ( !eina_array_remove( GetData(), KeepItemById, item ) ) {
        Log::error("FriendRequestsBatchProvider:DeleteItem->cannot delete the referenced element" );
    }
}

void FriendRequestsBatchProvider::ReleaseItem( void* item )
{
    BaseObject *baseObject = static_cast<BaseObject*>(item);
    baseObject->Release();
}

Eina_List *FriendRequestsBatchProvider::SetPeopleYouMayKnow(Eina_List *peopleList, bool isDescendingOrder)
{
    Log::info("FriendRequestsBatchProvider::SetPeopleYouMayKnow" );
    for (unsigned int i = 0; i < eina_array_count(mPYMKArray); ++i) {
    //this code covers the case where we replace cached data with new data
    //In case of PRT Pymk data will be automatically erased with FriendRequestsBatchProvider::CustomErase->ClearPYMKIds() && AbstractDataProvider::EraseData->CleanData()
        void *user = eina_array_data_get(mPYMKArray, i);
        if(user) {
            DeleteItem(user);// method removes from both containers at once
            static_cast<BaseObject*>(user)->Release();
        }
    }
    eina_array_clean(mPYMKArray);

    if ( peopleList ) {
        UpdatePeopleYouMayKnowHeader();

        Eina_List *list;
        Eina_List *l_next;
        void *data;
        EINA_LIST_FOREACH_SAFE(peopleList, list, l_next, data) {
            FbRespondGetPeopleYouMayKnow::FriendableUser *user = static_cast<FbRespondGetPeopleYouMayKnow::FriendableUser*>(data);
            if (user) {
                eina_array_push(mPYMKArray, user);
                peopleList = eina_list_remove(peopleList, user);
                if(user->GetId()) {
                    const char *user_id = strdup(user->GetId());
                    if(!eina_array_push( mPYMK_Ids, user_id )) {
                        free((void*)user_id);
                    }
                }
            }
        }
        AppendData( mPYMK_Header, isDescendingOrder );
        AppendData( mPYMKArray, isDescendingOrder );
    } else {
        Log::error("FriendRequestsBatchProvider: cannot allocate a memory for a request array.");
    }
    return peopleList;
}

void FriendRequestsBatchProvider::LoadPeopleYouMayKnowFromCache() {
    Eina_List *list = CacheManager::GetInstance().SelectFriendableUserData(50);
    SetPeopleYouMayKnow(list, true);
}

void FriendRequestsBatchProvider::SetFriendsRequest()
{
    CleanPaging();
    SetAPIType( AbstractDataProvider::GRAPH_API );
    SetHttpRequestType( GraphRequest::POST );
    SetPagingAlgorithm( ITERATORS_ALGORITHM );
    m_CurrentRequest = FRIENDS_REQUEST;
}

void FriendRequestsBatchProvider::SetPeopleYouMayKnowRequest()
{
    SetAPIType( AbstractDataProvider::REST_API );
    SetHttpRequestType( GraphRequest::GET );
    CleanPaging();
    SaveLastRequest( REST_PEOPLE_YOU_MAY_KNOW_REQUEST, true );
    m_CurrentRequest = PEOPLE_YOU_MAY_KNOW_REQUEST;
    SwitchContinueRequest();
}

void FriendRequestsBatchProvider::CustomErase()
{
    Log::info("FriendRequestsBatchProvider::CustomErase");
    ClearAllIds();

    if ( mFriendRequestsArray ) {
        eina_array_clean(mFriendRequestsArray);
    }
    if ( mPYMKArray ) {
        eina_array_clean(mPYMKArray);
    }
    if ( mFR_Header ) {
        eina_array_clean(mFR_Header);
    }
    if ( mPYMK_Header ) {
        eina_array_clean(mPYMK_Header);
    }

    mParsedItemsList = eina_list_free( mParsedItemsList );

    mFriendRequests_Separator = nullptr;
    mNoNewFriends_Separator = nullptr;
    mPeopleYouMayKnow_Separator = nullptr;

    mUnreadBadgeNum = 0;
    mRequestsTotalCount = 0;

    mBothRequestsWereDone = false;
    mDeleteInvalidFRs = false;
 }

void FriendRequestsBatchProvider::ClearAllIds()
{
    MutexLocker locker( &m_IdsMutex );
    ClearPYMKIds();
    ClearFRIds();
}
 
void FriendRequestsBatchProvider::ClearFRIds() {
    Log::info("FriendRequestsBatchProvider::ClearFRIds");
    if ( mFR_Ids ) {
        unsigned int index = 0;
        void* item;
        Eina_Array_Iterator iterator;
        EINA_ARRAY_ITER_NEXT( mFR_Ids, index, item, iterator ) {
            free(item);
        }
        eina_array_clean( mFR_Ids );
    }
}

void FriendRequestsBatchProvider::ClearPYMKIds() {
    Log::info("FriendRequestsBatchProvider::ClearPYMKIds");
    if ( mPYMK_Ids ) {
        unsigned int index = 0;
        void* item;
        Eina_Array_Iterator iterator;
        EINA_ARRAY_ITER_NEXT( mPYMK_Ids, index, item, iterator ) {
            free(item);
        }
        eina_array_clean( mPYMK_Ids );
    }
}

Eina_Array* FriendRequestsBatchProvider::GetPYMK_Ids() const {
    return mPYMK_Ids;
}

Eina_Array* FriendRequestsBatchProvider::GetFR_Ids() const {
    return mFR_Ids;
}

GraphObject* FriendRequestsBatchProvider::GetGraphObjectById(const void *userId) {
    if (userId) {
        int arraySize = GetDataCount();

        for (int i = 0; i < arraySize; ++i)
        {
            GraphObject *object = static_cast<GraphObject*>(eina_array_data_get(GetData(), i));

            if (object && object->GetId()) {
                if (idsComparator(userId, object->GetId())) {
                    return object;
                }
            }
        }
    }
    return nullptr;
}

FbRespondGetPeopleYouMayKnow::FriendableUser *FriendRequestsBatchProvider::GetPymkDataById(const void *userId) {
    return dynamic_cast<FbRespondGetPeopleYouMayKnow::FriendableUser*>(GetGraphObjectById(userId));
}

User *FriendRequestsBatchProvider::GetFrDataById(const void *userId) {
    return dynamic_cast<User*>(GetGraphObjectById(userId));
}

void FriendRequestsBatchProvider::RequestSendSuccessSet(const void *object)
{
    for (unsigned int i = 0; i < eina_array_count(mPYMKArray); ++i) {
        FbRespondGetPeopleYouMayKnow::FriendableUser *user = static_cast<FbRespondGetPeopleYouMayKnow::FriendableUser*>(eina_array_data_get(mPYMKArray, i));
        if (user && idsComparator(object, user->GetId())) {
            user->mFriendRequestStatus = eUSER_REQUEST_SENT;
            break;
        }
    }
}

void FriendRequestsBatchProvider::FriendConfirmedSuccessSet(const void *object)
{
    for (unsigned int i = 0; i < eina_array_count(mFriendRequestsArray); ++i)
    {
        User *user = static_cast<User*>(eina_array_data_get(mFriendRequestsArray, i));
        if (user && idsComparator(object, user->GetId())) {
            user->mFriendRequestStatus = eUSER_REQUEST_CONFIRMED;
            break;
        }
    }
}

void FriendRequestsBatchProvider::FriendDeletedSuccessSet(const void *object)
{
    for (unsigned int i = 0; i < eina_array_count(mFriendRequestsArray); ++i)
    {
        User *user = static_cast<User*>(eina_array_data_get(mFriendRequestsArray, i));
        if (user && idsComparator(object, user->GetId())) {
            user->mFriendRequestStatus = eUSER_REQUEST_DELETED;
            break;
        }
    }
}

void FriendRequestsBatchProvider::RequestCancelSuccessSet(const void *object)
{
    Log::info("FriendRequestsBatchProvider::RequestCancelSuccessSet");
    for (unsigned int i = 0; i < eina_array_count(mPYMKArray); ++i)
    {
        FbRespondGetPeopleYouMayKnow::FriendableUser *user = static_cast<FbRespondGetPeopleYouMayKnow::FriendableUser*>(eina_array_data_get(mPYMKArray, i));
        if (user && idsComparator(object, user->GetId())) {
            user->mFriendRequestStatus = eUSER_REQUEST_CANCELLED;
        }
    }
}

void FriendRequestsBatchProvider::AppendNewFriendRequestsHeaderItems( )
{
    UpdateFriendRequestsHeader();
    AppendData(mFR_Header,false);
}

void FriendRequestsBatchProvider::UpdateFriendRequestsHeader( )
{
    Log::info("FriendRequestsBatchProvider::UpdateFriendRequestsHeader");
    if (!mFriendRequests_Separator && !mNoNewFriends_Separator) {
        mFriendRequests_Separator = new Separator( Separator::FRIENDS_SEPARATOR );
        if ( !eina_array_push( mFR_Header, mFriendRequests_Separator) ) {
            Log::error("FriendRequestsBatchProvider::UpdateFriendRequestsHeader: cannot append FRIENDS_SEPARATOR");
        }
        mNoNewFriends_Separator = new Separator( Separator::FRIENDS_NOREQUEST_SEPARATOR );
    }
    else {
        DeleteItem( mFriendRequests_Separator ); // remove from provider if already exist
        if(eina_array_count( mFR_Header )==2) {
            DeleteItem( mNoNewFriends_Separator ); // remove from provider if already exist
        }
    }

    if ( eina_array_count( mFR_Ids )==0 && eina_array_count( mFR_Header )==1 ) {
        Log::info("FriendRequestsBatchProvider::UpdateFriendRequestsHeader->AppendHeader");
        if ( !eina_array_push( mFR_Header, mNoNewFriends_Separator) ) { //append again
            Log::error("FriendRequestsBatchProvider::UpdateFriendRequestsHeader->cannot push into header array");
        }
    } else if (eina_array_count( mFR_Ids )>0 && eina_array_count( mFR_Header )==2 ) {
        Log::info("FriendRequestsBatchProvider::UpdateFriendRequestsHeader->RemoveHeader");
        if ( !eina_array_remove( mFR_Header, KeepItemById, mNoNewFriends_Separator ) ) {
            Log::error("FriendRequestsBatchProvider::UpdateFriendRequestsHeader->cannot delete from Header" );
        }
    }
}

void FriendRequestsBatchProvider::UpdatePeopleYouMayKnowHeader( )
{
    Log::info ("FriendRequestsBatchProvider::UpdatePeopleYouMayKnowHeader");
    if (!mPeopleYouMayKnow_Separator ) {
        mPeopleYouMayKnow_Separator = new Separator( Separator::PEOPLE_YOU_MAY_KNOW_SEPARATOR );
        if ( !eina_array_push( mPYMK_Header, mPeopleYouMayKnow_Separator) ) { //append again
            Log::error("FriendRequestsBatchProvider::UpdatePeopleYouMayKnowHeader: cannot append PEOPLE_YOU_MAY_KNOW_SEPARATOR");
        }
    }
    else {
        DeleteItem( mPeopleYouMayKnow_Separator ); // remove from provider if already exist
    }
}

void FriendRequestsBatchProvider::SetRequestsTotalCount(int count)
{
    Log::info ("FriendRequestsBatchProvider::SetRequestsTotalCount %d", count);
    if (count>=0) {
        if (mRequestsTotalCount != count) {
            Log::info ("FriendRequestsBatchProvider::SetRequestsTotalCount->Send event to update ");
            mRequestsTotalCount = count;
            AppEvents::Get().Notify(eUPDATE_FIND_FRIENDS_REQUESTS_COUNT);
        }
    }
}
