#include "Album.h"
#include "ErrorHandling.h"
#include "FbRespondGetAlbums.h"
#include "Log.h"
#include "ParserWrapper.h"
#include "SelectAlbumProvider.h"
#include <memory>

SelectAlbumProvider* SelectAlbumProvider::GetInstance()
{
    static SelectAlbumProvider* selectAlbumProvider = new SelectAlbumProvider();
    return selectAlbumProvider;
}
SelectAlbumProvider::~SelectAlbumProvider() {
}

void SelectAlbumProvider::SetUserId(const char* userId) {
    EraseData();
    mUserId = userId ? userId : "";
}

unsigned int SelectAlbumProvider::ParseData(char *data, bool isDescendingOrder, Ecore_Thread *thread) {
    Log::info("SelectAlbumProvider::ParseData()");
    JsonObject* object = nullptr;
    ParserWrapper wr(openJsonParser(data, &object));
    unsigned int itemsCount = NONE_ITEMS;
    if (object) {
        std::unique_ptr<FbRespondGetAlbums> albumsResponse(new FbRespondGetAlbums(object));
        if (albumsResponse->mAlbumsList) {
            unsigned int countainerSize = GetLimit() > 0 ? GetLimit() : RESPONSE_CONTAINER_SIZE;
            Eina_Array* parsedItemsArray = eina_array_new(countainerSize);
            CHECK_RET(parsedItemsArray, NONE_ITEMS);
            Eina_List* list;
            void* data;
            EINA_LIST_FOREACH(albumsResponse->mAlbumsList, list, data ) {
                Album *album = static_cast<Album*>(data);
                //we must show only albums of type "normal".
                if (album && album->mType && !strcmp(album->mType, "normal")) {
                    eina_array_push(parsedItemsArray, album);
                    ++itemsCount;
                }
            }
            AppendData(parsedItemsArray, isDescendingOrder);
            albumsResponse->mAlbumsList = eina_list_free(albumsResponse->mAlbumsList);
            eina_array_free(parsedItemsArray);
        }
    }
    return itemsCount;
}
