#include <stdlib.h>
#include <assert.h>
#include "AbstractDataProvider.h"
#include "AppEvents.h"
#include "ParserWrapper.h"
#include "Log.h"

#include <curl/curl.h>

#include "dlog.h"
#include "Common.h"
#include "DataEventDescription.h"
#include "jsonutilities.h"
#include "Config.h"
#include "FacebookSession.h"
#include "RequestCompleteEvent.h"
#include "RequestContinueEvent.h"
#include "LogoutRequest.h"
#include "MutexLocker.h"
#include "OperationPresenter.h"
#include "Utils.h"

const char* AbstractDataProvider::ID = "id";
const char* AbstractDataProvider::URL = "url";
const char* AbstractDataProvider::CREATED_TIME = "created_time";
const char* AbstractDataProvider::ACTIONS = "actions";
const char* AbstractDataProvider::CAPTION = "caption";
const char* AbstractDataProvider::DESCRIPTION = "description";
const char* AbstractDataProvider::DESCRIPTION_TAGS = "description_tags";
const char* AbstractDataProvider::FULL_PICTURE = "full_picture";
const char* AbstractDataProvider::LINK = "link";
const char* AbstractDataProvider::MESSAGE = "message";
const char* AbstractDataProvider::MESSAGE_TAGS = "message_tags";
const char* AbstractDataProvider::DATA = "data";
const char* AbstractDataProvider::NAME = "name";
const char* AbstractDataProvider::FIRST_NAME = "first_name";
const char* AbstractDataProvider::LAST_NAME = "last_name";
const char* AbstractDataProvider::OBJECT_ID = "object_id";
const char* AbstractDataProvider::PARENT_ID = "parent_id";
const char* AbstractDataProvider::PRIVACY = "privacy";
const char* AbstractDataProvider::SOURCE = "source";
const char* AbstractDataProvider::STATUS_TYPE = "status_type";
const char* AbstractDataProvider::STORY = "story";
const char* AbstractDataProvider::STORY_TAGS = "story_tags";
const char* AbstractDataProvider::TYPE = "type";
const char* AbstractDataProvider::UPDATED_TIME = "updated_time";
const char* AbstractDataProvider::PICTURE = "picture";
const char* AbstractDataProvider::COVER = "cover";
const char* AbstractDataProvider::RELATIONSHIP_STATUS = "relationship_status";
const char* AbstractDataProvider::WORK = "work";
const char* AbstractDataProvider::EDUCATION = "education";
const char* AbstractDataProvider::SCHOOL = "school";
const char* AbstractDataProvider::BIRTHDAY = "birthday";
const char* AbstractDataProvider::EMPLOYER = "employer";
const char* AbstractDataProvider::POSITION = "position";
const char* AbstractDataProvider::FROM = "from";
const char* AbstractDataProvider::TO = "to";
const char* AbstractDataProvider::TO_PROFILE_TYPE = "profile_type";
const char* AbstractDataProvider::PLACE = "place";
const char* AbstractDataProvider::LOCATION = "location";
const char* AbstractDataProvider::CATEGORY_LIST = "category_list";
const char* AbstractDataProvider::CHECKINS = "checkins";
const char* AbstractDataProvider::LIKES = "likes";
const char* AbstractDataProvider::LIMIT = "limit";
const char* AbstractDataProvider::COMMENTS = "comments";
const char* AbstractDataProvider::ATTACHMENT = "attachment";
const char* AbstractDataProvider::ATTACHMENTS = "attachments";
const char* AbstractDataProvider::SUBATTACHMENTS = "subattachments";
const char* AbstractDataProvider::MEDIA = "media";
const char* AbstractDataProvider::TARGET = "target";
const char* AbstractDataProvider::TITLE = "title";
const char* AbstractDataProvider::CONTEXT = "context";
const char* AbstractDataProvider::MUTUAL_FRIENDS = "mutual_friends";
const char* AbstractDataProvider::APPLICATION = "application";
const char* AbstractDataProvider::VIA = "via";
const char* AbstractDataProvider::ORDER = "order";
const char* AbstractDataProvider::REVERSE_CHRONOLOGICAL = "reverse_chronological";
const char* AbstractDataProvider::FILTER = "filter";
const char* AbstractDataProvider::TOPLEVEL = "toplevel";
const char* AbstractDataProvider::CAN_COMMENT = "can_comment";
const char* AbstractDataProvider::CAN_LIKE = "can_like";
const char* AbstractDataProvider::CAN_REMOVE = "can_remove";
const char* AbstractDataProvider::USER_LIKES = "user_likes";
const char* AbstractDataProvider::COMMENT_COUNT = "comment_count";
const char* AbstractDataProvider::LIKE_COUNT = "like_count";
const char* AbstractDataProvider::IS_HIDDEN = "is_hidden";
const char* AbstractDataProvider::EDIT_ACTIONS = "edit_actions";
const char* AbstractDataProvider::TEXT = "text";
const char* AbstractDataProvider::EDIT_TIME = "edit_time";
const char* AbstractDataProvider::QUERY = "query";
const char* AbstractDataProvider::LOCALE = "locale";
const char* AbstractDataProvider::ATTACHMENTS_LIMIT = ".limit(42)";
const char* AbstractDataProvider::CITY = "city";
const char* AbstractDataProvider::COUNTRY = "country";
const char* AbstractDataProvider::WITH_TAGS = "with_tags";
const char* AbstractDataProvider::IMPLICIT_PLACE = "implicit_place";

const char* AbstractDataProvider::GRAPH_API_VERSION_2_4 = "v2.4";
const char* AbstractDataProvider::GRAPH_API_VERSION_2_3 = "v2.3";
const char* AbstractDataProvider::GRAPH_API_VERSION_1_0 = "v1.0";

const int AbstractDataProvider::UNDEFINED_VALUE = -1;

const char* AbstractDataProvider::API_GRAPH_ADDRESS = "https://graph.facebook.com";
const char* AbstractDataProvider::API_REST_ADDRESS = "https://api.facebook.com";

const char* AbstractDataProvider::ACCESS_TOKEN = "access_token=";
const char* AbstractDataProvider::LIMIT_FIELD = "limit=";
const char* AbstractDataProvider::FIELDS = "fields=";

const char* AbstractDataProvider::NEXT = "next";
const char* AbstractDataProvider::PREV = "previous";

const char* AbstractDataProvider::BEFORE = "before";
const char* AbstractDataProvider::AFTER = "after";

const char* AbstractDataProvider::IDS = "ids";

const char* AbstractDataProvider::ERROR = "error";
const char* AbstractDataProvider::PAGING = "paging";
const char* AbstractDataProvider::CURSORS = "cursors";
const char* AbstractDataProvider::SUMMARY = "summary";
const char* AbstractDataProvider::CODE = "code";

const char* AbstractDataProvider::TOTAL_COUNT = "total_count";

const unsigned int AbstractDataProvider::DEFAULT_LIMITS_LIMIT = 0;
const unsigned int AbstractDataProvider::RESPONSE_CONTAINER_SIZE = 32;
const unsigned int AbstractDataProvider::NONE_ITEMS = 0;
const unsigned int AbstractDataProvider::LIMIT_BUFFER_SIZE = 32;
const unsigned int AbstractDataProvider::BASE_CONTAINER_SIZE = 256;
//
// @todo Need to specify the real limit for fb requests. There is requirement for
//       GET request type ( if GET request is too long, we need to use POST request
//       type ).
const unsigned int AbstractDataProvider::MAX_REQUEST_LENGTH = 2048;
const unsigned int AbstractDataProvider::MAX_REQUEST_DELAY = 120; // seconds

/**
 * Data wrapper for subscriber notification
 */
class Notifier {
public:
    Notifier();
    virtual ~Notifier();
    bool isDropCahedData;
    AppEventId eventId;
    BaseObject *eventData;
    AbstractDataProvider* source;
};


Notifier::Notifier() {
    eventId = eUNDEFINED_EVENT;
    isDropCahedData = false;
    eventData = NULL;
    source = NULL;
}

Notifier::~Notifier() {
    if(eventData) {
        eventData->Release();
    }
    source = NULL;
}

ThreadMessage::ThreadMessage(EThreadMsgType type, void* data):mType(type), mData(data) {
}

/**
 * This is a callback for a subscriber for notify in UI thread
 */
void AbstractDataProvider::notifySelf( void *notif ) {
    if ( notif ) {
        Notifier *notifier = static_cast<Notifier*>(notif);
        if(notifier->source) {
            AbstractDataProvider *provider = static_cast<AbstractDataProvider*>(notifier->source);
            provider->ResetNotifySelfJob();
            provider->UpdateInternal( notifier->eventId, notifier->eventData );
        }
        delete notifier;
    }
}

/**
 * This is a callback for a subscriber for notify in UI thread
 */
void AbstractDataProvider::notifyUISubscribers( void *notif ) {
    if ( notif ) {
        Notifier *notifier = static_cast<Notifier*>(notif);
        if(notifier->source) {
            AbstractDataProvider *provider = static_cast<AbstractDataProvider*>(notifier->source);
            provider->ResetNotifyJob();
            Eina_List *l;
            void *data;
            EINA_LIST_FOREACH( provider->m_Subscribers, l, data ) {
                Subscriber *subscriber = static_cast<Subscriber*>(data);
                if(provider->IsSubscribed(subscriber)) {
                    subscriber->Update( notifier->eventId, notifier->eventData );
                }
            }
        }
        delete notifier;
    }
}

/**
 * This is a callback for a subscriber for erase notify in UI thread
 */
void AbstractDataProvider::notifyUIErase( void *notif )
{
    if ( notif ) {
        Notifier *notifier = static_cast<Notifier*>(notif);
        if(notifier->source) {
            AbstractDataProvider *provider = static_cast<AbstractDataProvider*>(notifier->source);
            provider->ResetNotifyJob();
            Eina_List *l;
            void *data;
            EINA_LIST_FOREACH( provider->m_Subscribers, l, data ) {
                Subscriber *subscriber = static_cast<Subscriber*>(data);
                if(provider->IsSubscribed(subscriber)) {
                    subscriber->UpdateErase();
                }
            }
        }
        delete notifier;
    }
}

void AbstractDataProvider::handleThreadMessage_cb( void *data, Ecore_Thread *thread, void *ThreadMsgObj )
{
    if (ThreadMsgObj) {
        ThreadMessage* ThreadMsg = static_cast<ThreadMessage*>(ThreadMsgObj);
        if((ThreadMsg->GetRefCount() > 1) && (ThreadMsg->mType != eUnKnown)) {
            Notifier *notifier = static_cast<Notifier*>(ThreadMsg->mData);
            Ecore_Job *notifyJob = NULL;
            switch (ThreadMsg->mType) {
            case eNotify:
            {
                if (notifier){
                    if(notifier->eventId == eREQUEST_CONTINUE_EVENT) {
                        Ecore_Job *notifySelfJob = ecore_job_add(notifySelf, ThreadMsg->mData);
                        if(notifySelfJob && notifier && notifier->source) {
                            notifier->source->SetNotifySelfJob(notifySelfJob);
                        }
                    } else {
                        if(notifier->isDropCahedData) {
                            notifyJob = ecore_job_add(notifyUIErase, ThreadMsg->mData);
                        }
                        if(NULL == notifyJob) {
                            // event is not handled yet
                            notifyJob = ecore_job_add(notifyUISubscribers, ThreadMsg->mData);
                        }
                    }
                }
                break;
            }
            case eInValidAccessToken:
                notifyJob = ecore_job_add(LogoutRequest::RedirectToLogin, nullptr);
                break;
            default:
                break;
            }

            if(notifyJob && notifier && notifier->source) {
                notifier->source->SetNotifyJob(notifyJob);
            }

            ThreadMsg->mType = eUnKnown;
            ThreadMsg->mData = NULL;
        }
        ThreadMsg->Release();
    }
}

AbstractDataProvider::AbstractDataProvider()
{
    m_Pagination.head = NULL;
    m_Pagination.tail = NULL;
    m_Pagination.isPagingEnabled = true;
    m_Pagination.isHeadRequired = true;
    m_LastRequests.headRequest = NULL;
    m_LastRequests.tailRequest = NULL;
    m_LastRequests.isLastOrderDescending = true;
    m_LastRequests.isBatchRequest = false;
    m_HttpRequestType = GraphRequest::GET;
    m_ItemsTotalCount = UNDEFINED_VALUE;
    m_ItemsLimit = DEFAULT_LIMITS_LIMIT;
    m_ItemsResponseCounter = NONE_ITEMS;
    m_Request = NULL;
    m_APIVersion = API_VER_LATEST;
    m_APIType = GRAPH_API;
    m_PagingAlgorithm = CURSOR_ALGORITHM;
    m_Subscribers = NULL;
    m_CommentsLimit = UNDEFINED_VALUE;
    m_LikesLimit = UNDEFINED_VALUE;
    m_IsLikesSummary = true;
    m_IsCommentsSummary = true;
    m_IsReverseChronological = false;
    m_IsDataCached = false;
    m_IsContinueRequest = false;
    m_IsRequestTimerEnabled = true;
    m_LastRequests.lastRequestTime_secs = 0;

    m_PageIdettifier_Prev[ CURSOR_ALGORITHM ] = BEFORE ;
    m_PageIdettifier_Prev[ ITERATORS_ALGORITHM ] = PREV;
    m_PageIdettifier_Prev[ MANUAL_ALGORITHM ] = IDS;

    m_PageIdettifier_Next[ CURSOR_ALGORITHM ] = AFTER;
    m_PageIdettifier_Next[ ITERATORS_ALGORITHM ] = NEXT;
    m_PageIdettifier_Next[ MANUAL_ALGORITHM ] = IDS;

    m_Data = eina_array_new( BASE_CONTAINER_SIZE );
    if ( m_Data == NULL ) {
        Log::error( "AbstractDataProvider: cannot allocate a memory during initialization." );
    }
    m_SkipHeaderPagingEnabled = false;
    m_SkipHeaderPaging = false;
    m_Etag = NULL;
    mOperationPresenters = NULL;
    mThreadMsgObj = new ThreadMessage();
    mThreadMsgObj->mType = eUnKnown;
    mThreadMsgObj->mData = NULL;
    mNotifyJob = NULL;
    mNotifySelfJob = NULL;
    eina_lock_new( &m_Mutex );
    mProviderType = UNDEFINED_TYPE;
}

AbstractDataProvider::~AbstractDataProvider()
{
    dlog_print( DLOG_DEBUG, "Facebook_oper", "AbstractDataProvider::~AbstractDataProvider start" );
    CleanRequest();
    MutexLocker locker( &m_Mutex );
    if(mNotifyJob) {
        ecore_job_del(mNotifyJob);
        mNotifyJob = NULL;
    }

    if(mNotifySelfJob) {
        ecore_job_del(mNotifySelfJob);
        mNotifySelfJob = NULL;
    }

    DeleteAllOperations();

    m_Subscribers = eina_list_free( m_Subscribers );

    if ( m_Data ) {
        eina_array_free( m_Data );
        m_Data = NULL;
    }

    if (mThreadMsgObj) {
        mThreadMsgObj->mType = eUnKnown;
        mThreadMsgObj->mData = NULL;
        mThreadMsgObj->Release();
    }
    eina_lock_free( &m_Mutex );
    dlog_print( DLOG_DEBUG, "Facebook_oper", "AbstractDataProvider::~AbstractDataProvider end" );
}

void AbstractDataProvider::CleanPaging() {
    free( m_LastRequests.headRequest );
    free( m_LastRequests.tailRequest );
    m_LastRequests.headRequest = NULL;
    m_LastRequests.tailRequest = NULL;

    free( m_Pagination.head );
    free( m_Pagination.tail );
    m_Pagination.head = NULL;
    m_Pagination.tail = NULL;

    m_Pagination.isPagingEnabled = true;
    m_Pagination.isHeadRequired = true;
    m_LastRequests.isLastOrderDescending = true;
    free(m_Etag); m_Etag = NULL;
}

void AbstractDataProvider::CleanData() {
    if (m_Data) {
        void* item;
        unsigned int index;
        Eina_Array_Iterator iterator;
        EINA_ARRAY_ITER_NEXT( m_Data, index, item, iterator ) {
            ReleaseItem(item);
        }
        eina_array_clean( m_Data );
    }
}


void AbstractDataProvider::EraseData()
{
    CleanRequest();
    MutexLocker locker( &m_Mutex );
    Log::debug( "AbstractDataProvider: erase data." );
    CleanData();
    CleanPaging();
    CustomErase();
    ResetRequestTimer();
    m_ItemsTotalCount = UNDEFINED_VALUE;
}

void AbstractDataProvider::CleanRequest()
{
    if ( m_Request ) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(m_Request);
        m_Request = NULL;
    }
}

void AbstractDataProvider::Subscribe( const Subscriber *subscriber )
{
    if ( eina_list_data_find( m_Subscribers, subscriber ) == NULL ) {
        m_Subscribers = eina_list_append( m_Subscribers, subscriber );
    }
}

void AbstractDataProvider::Unsubscribe( const Subscriber *subscriber )
{
    if(m_Subscribers) {
        m_Subscribers = eina_list_remove( m_Subscribers, subscriber );
    }
}

bool AbstractDataProvider::IsSubscribed( const Subscriber *subscriber ) const {
    return (eina_list_data_find( m_Subscribers, subscriber ) != NULL);
}

void AbstractDataProvider::Notify(AppEventId eventId, BaseObject * data, Ecore_Thread *thread )
{
    if(thread && ecore_thread_check(thread)) {
        Log::warning( "AbstractDataProvider::Notify, thread is cancelled" );
        return;
    }
    Notifier *notifier = new Notifier;
    notifier->isDropCahedData = false;
    notifier->eventId = eventId;
    notifier->eventData = data;
    notifier->source = this;

    if(thread) {
        mThreadMsgObj->AddRef();
        mThreadMsgObj->mType = eNotify;
        mThreadMsgObj->mData = notifier;
        if ( !ecore_thread_feedback( thread, (void*)mThreadMsgObj ) ) {
            mThreadMsgObj->Release();
            Log::error( "[ thread ] : cannot pass a message to a main thread to notify." );
        }
    } else {
        mNotifyJob = ecore_job_add(notifyUISubscribers, notifier);
    }
}

void AbstractDataProvider::NotifyErase( Ecore_Thread *thread )
{
    if(thread && ecore_thread_check(thread)) {
        Log::warning( "AbstractDataProvider::NotifyErase, thread is cancelled" );
        return;
    }
    Notifier *notifier = new Notifier();
    notifier->isDropCahedData = true;
    notifier->source = this;

    if(thread) {
        mThreadMsgObj->AddRef();
        mThreadMsgObj->mType = eNotify;
        mThreadMsgObj->mData = notifier;
        if ( !ecore_thread_feedback( thread, (void*)mThreadMsgObj ) ) {
            mThreadMsgObj->Release();
            Log::error( "[ thread ] : cannot pass a message to a main thread to notify erase." );
        }
    } else {
        mNotifyJob = ecore_job_add(notifyUIErase, notifier);
    }
}

void AbstractDataProvider::IncomingData( char* data, char* header, int curlCode, Ecore_Thread *thread )
{
    AddRef();
    Log::debug( "AbstractDataProvider::IncomingData start" );
    MutexLocker locker( &m_Mutex );
    Log::info( "AbstractDataProvider::IncomingData after mutex" );
    RequestCompleteEvent *eventData = new RequestCompleteEvent();
    eventData->mCurlCode = curlCode;
    bool LoginRequired = false;

    if(header) {
        if(m_SkipHeaderPagingEnabled && m_SkipHeaderPaging && Utils::IsHeader_Etag(header)) {
            std::string etag = Utils::ExtractEtag(header);
            if(etag.length() > 0) {
                Log::debug( "Etag-FT: etag = %s", etag.c_str() );
                SetEtag(etag.c_str());
            }
        }
        free(header); header = NULL;
    }

    m_SkipHeaderPaging = false;

    if ( curlCode == CURLE_OK ) {
        JsonObject *object = NULL;
        JsonParser *parser = openJsonParser( data, &object );
        ParserWrapper wr( parser );

        //
        // @todo Implement algorithm to handle fb permissions feature for each
        //       request. Maybe it could be request specific.
        //

        int responseError = GetResponseError( object );
        eventData->mResponseError = responseError;
        if ( responseError == RESPONSE_OK ) {
            bool descendingOrder = m_LastRequests.isLastOrderDescending;
            Log::debug( "IncomingData: response ok");
            //
            // common response data
            ParsePagination( object, descendingOrder );
            ParseSummary( object );
            //
            // custom response data
            int numParsedItems = ParseData( data, descendingOrder, thread);
            descendingOrder = m_LastRequests.isLastOrderDescending;
            m_LastRequests.isEmptyResponce = (numParsedItems == 0);
            if(m_SkipHeaderPagingEnabled
               && m_LastRequests.isEmptyResponce
               && (!m_LastRequests.isSkipHeaderPaging)
               && (!descendingOrder)) {
                m_IsContinueRequest = true;
                m_SkipHeaderPaging = true;
            }
            m_LastRequests.isSkipHeaderPaging = m_SkipHeaderPaging;
            if ( m_IsContinueRequest ) {
                free( data );
                char *request = descendingOrder ? m_LastRequests.tailRequest: m_LastRequests.headRequest;
                CleanRequest();
                if ( request ) {
                    RequestContinueEvent *eData = new RequestContinueEvent(std::string( request ).c_str(), descendingOrder);
                    Log::debug( "AbstractDataProvider::IncomingData Notify eREQUEST_CONTINUE_EVENT start" );
                    Notify(eREQUEST_CONTINUE_EVENT, eData, thread);
                    Log::debug( "AbstractDataProvider::IncomingData Notify eREQUEST_CONTINUE_EVENT end" );
                }
                m_IsContinueRequest = false;
                Log::info( "AbstractDataProvider::IncomingData end (1)" );
                Release();
                return;
            }
        } else if ( responseError == DEPRICATED_API ) {
            Log::error( "IncomingData: there was used deprecated API." );
            // we might to send this request for GRAPH_API_VERSION_1_0
            assert( false );
        } else if ( responseError == INCORRECT_FB_REQUEST_API ) {
            Log::error( "IncomingData: wrongly formed request was used." );
            // use it to debug
            //assert( false );
            //Login is required if we receive this error from server
            LoginRequired = true;
        } else if (FB_ERROR_INVALID_ACCESS_TOKEN == responseError) {
            Log::error( "IncomingData: Invalid Access Token" );
            LoginRequired = true;
        } else {
            Log::error( "IncomingData: Other error, Code = %d", responseError );
        }
    }

    Log::debug( "AbstractDataProvider::IncomingData CleanRequest start" );
    CleanRequest();
    Log::debug( "AbstractDataProvider::IncomingData CleanRequest end" );
    if (!LoginRequired) {
        Log::debug( "AbstractDataProvider::IncomingData Notify eREQUEST_COMPLETED_EVENT start" );
        Notify(eREQUEST_COMPLETED_EVENT, eventData, thread);
        Log::debug( "AbstractDataProvider::IncomingData Notify eREQUEST_COMPLETED_EVENT end" );
        eventData = NULL;
    } else {
        delete eventData;
        if(thread && !ecore_thread_check(thread)) {
            mThreadMsgObj->AddRef();
            mThreadMsgObj->mType = eInValidAccessToken;
            if (!ecore_thread_feedback(thread, (void*)mThreadMsgObj)) {
                mThreadMsgObj->Release();
                Log::error( "Thread: Failed to communicate invalid access token message to main thread." );
            }
        }
    }

    free(data);
    Log::debug( "AbstractDataProvider::IncomingData end" );
    Release();
}

void AbstractDataProvider::ParsePagination( JsonObject *object, bool isDescendigOrder )
{
    if ( m_LastRequests.isBatchRequest ) {
        object = json_object_get_object_member( object, "body" );
    }

    if(m_PagingAlgorithm == MANUAL_ALGORITHM) {
        FillManualPagination(isDescendigOrder);
        return;
    }

    JsonObject *paging = json_object_get_object_member( object, PAGING );

    if ( paging ) {
        char *next = GetStringFromJson( paging, NEXT );
        char *prev = GetStringFromJson( paging, PREV );

        if ( !next && !prev && m_PagingAlgorithm == ITERATORS_ALGORITHM ) {
            //
            // It could be due to the requested data went into a single response.
            free( m_Pagination.head );
            free( m_Pagination.tail );
            m_Pagination.head = NULL;
            m_Pagination.tail = NULL;
            m_Pagination.isHeadRequired = false;
            m_Pagination.isPagingEnabled = false;
            Log::debug( "ParsePagination: received the data without paging info next/previous." );
            return;
        }

        switch( m_PagingAlgorithm )
        {
            case CURSOR_ALGORITHM:
            {
                JsonObject *cursors = json_object_get_object_member( paging, CURSORS );
                if ( cursors ) {
                    //
                    // If cursor tag is present (not null), tags before/after will always present.
                    // It is not possible if 'before' is present and 'after' is absent, and vice versa.
                    //
                    char *before = GetStringFromJson( cursors, BEFORE );
                    char *after = GetStringFromJson( cursors, AFTER );
                    if ( isDescendigOrder ) {
                        if ( after ) {
                            free( m_Pagination.tail );
                            m_Pagination.tail = strdup( after );
                        }
                        if ( !m_Pagination.head ) {
                            m_Pagination.head = strdup( before );
                        }
                    } else {
                        if ( before ) {
                            free( m_Pagination.head );
                            m_Pagination.head = strdup( before );
                        }
                        if ( !m_Pagination.tail ) {
                            m_Pagination.tail = strdup( after );
                        }
                    }
                }
                break;
            }
            case ITERATORS_ALGORITHM:
            {
                if ( isDescendigOrder ) {
                    if ( next ) {
                        free( m_Pagination.tail );
                        m_Pagination.tail = strdup( next );
                        if ( !prev ) {
                            // it is not iterable case to head
                            m_Pagination.isHeadRequired = false;
                        }
                    } else if ( prev ) {
                        // the last page has been reached
                        free( m_Pagination.tail );
                        m_Pagination.tail = NULL;
                    }
                    if ( m_Pagination.isHeadRequired && !m_Pagination.head && prev ) {
                        //
                        // It might be on the first response
                        m_Pagination.head = strdup( prev );
                    }
                } else {
                    if ( prev ) {
                        free( m_Pagination.head );
                        m_Pagination.head = strdup( prev );
                    }
                }
                break;
            }
            default:
            {
                // undefined paging algorithm
                assert( false );
            }
        }
        free( prev );
        free( next );
    } else if ( !( m_Pagination.head || m_Pagination.tail ) ) {
        m_Pagination.isPagingEnabled = false;
    }
}

void AbstractDataProvider::ParseSummary( JsonObject *object )
{
    JsonObject *summary = json_object_get_object_member( object, SUMMARY );
    if ( summary ) {
        if (json_object_has_member(summary, TOTAL_COUNT)) {
            m_ItemsTotalCount = (int)GetGint64FromJson( summary, TOTAL_COUNT );
        }
    }
}

int AbstractDataProvider::GetResponseError( JsonObject *object ) const
{
    JsonObject * error = json_object_get_object_member( object, ERROR );

    if( error ) {
        char *errorMsg = GetStringFromJson( error, MESSAGE );
        char *errorType = GetStringFromJson( error, TYPE );
        gint64 errorCode = GetGint64FromJson( error, CODE );
        Log::error( "Response from Facebook returned an error: Message: %s Type: %s Code: %d",
                errorMsg, errorType, (int)errorCode );
        free( errorMsg );
        free( errorType );

        return (int)errorCode;
    }
    return (int)RESPONSE_OK;
}

void AbstractDataProvider::AppendLocale(std::stringstream &request)
{
	if(request){
		request << '&' << LOCALE << '=' << Utils::GetLocale();
	}
}

UPLOAD_DATA_RESULT AbstractDataProvider::RequestData( const char *request, bool isDescendingOrder, bundle *param )
{

    bool isHead = ( isDescendingOrder == IsReverseChronological() ) || ( !m_LastRequests.headRequest && !m_LastRequests.tailRequest );
    if ( isHead && m_LastRequests.lastRequestTime_secs != 0 ) {
        int currentTime_secs = Utils::GetCurrentTimeMicroSecs() / 1000000;
        if ( (currentTime_secs - m_LastRequests.lastRequestTime_secs) < MAX_REQUEST_DELAY ) {
            Log::debug( "DETECTED DELAY" );
            return TOO_EARLY_TO_REQUEST_RESULT;
        }
    }

    if ((m_PagingAlgorithm != MANUAL_ALGORITHM)
        && !m_Pagination.isPagingEnabled ) {
        return NONE_DATA_COULD_BE_RESULT;
    }

    if ( m_Request ) {
        if ( isDescendingOrder == m_LastRequests.isLastOrderDescending ) {
            return ONLY_REQUEST_SENT_RESULT;
        }
        CleanRequest();
    }

    if ( IsReverseChronological() && ( m_Pagination.tail || m_Pagination.head ) ) {
        isDescendingOrder = !isDescendingOrder;
    }
    std::stringstream finalRequest;

    bool isBatchRequest = ( param && ( !request || !strlen( request ) ) );

    if (    m_PagingAlgorithm == ITERATORS_ALGORITHM && !isBatchRequest &&
            ( (isDescendingOrder && m_Pagination.tail) || (!isDescendingOrder && m_Pagination.head && !m_SkipHeaderPaging) ) ) {
        //
        // ITERATORS_ALGORITHM has cached paging offsets which are stored in paging attributes, received
        // from a server. So, in a such case we do not need to form a request again, just copy an appropriate
        // paging attribute.
        //
        finalRequest << (isDescendingOrder ? m_Pagination.tail : m_Pagination.head);
    } else {
        //
        // Specify API type
        if ( m_APIType == GRAPH_API ) {
            finalRequest << API_GRAPH_ADDRESS << "/";
            //
            // Specify version
            if ( m_APIVersion == API_VER_2_4 ) {
                finalRequest << GRAPH_API_VERSION_2_4 << ((isBatchRequest && GetLimit() == 0) ? "" : "/");
            } else if ( m_APIVersion == API_VER_2_3 ) {
                finalRequest << GRAPH_API_VERSION_2_3 << ((isBatchRequest && GetLimit() == 0) ? "" : "/");
            } else if ( m_APIVersion ==  API_VER_1_0 ) {
                finalRequest << GRAPH_API_VERSION_1_0 << ((isBatchRequest && GetLimit() == 0) ? "" : "/");
            } else {
                //
                // API_VER_UNVERSIONED
            }
        } else if ( m_APIType == REST_API ) {
            finalRequest << API_REST_ADDRESS << "/";
        } else {
            // it must not happen
            assert( false );
        }

        bool isExtraParams = strstr( request, "?" ) != NULL;
        finalRequest << request;

        //
        // Fill reverse chronological order
        if ( IsReverseChronological() ) {
            finalRequest << ( isExtraParams ? "&" : "?" ) << ORDER << '=' << REVERSE_CHRONOLOGICAL;
            isExtraParams = true;
        }
        //
        // Fill limit attribute
        if ( GetLimit() > 0 ) {
            finalRequest << ( isExtraParams ? "&" : "?" ) << LIMIT_FIELD << GetLimit();
            isExtraParams = true;
        }

        //
        // Fill pagination attribute
        if (!m_LastRequests.isBatchRequest) {
            if ( !GetPagingAttributes( finalRequest, isDescendingOrder, isExtraParams ) ) {
                return NONE_DATA_COULD_BE_RESULT;
            }
        }

        isExtraParams = strstr( finalRequest.str().c_str(), "?" ) != NULL;

        finalRequest << ( isExtraParams ? "&" : "?" ) << ACCESS_TOKEN << Config::GetInstance().GetAccessToken();
        AppendLocale(finalRequest);
    }

    if ( finalRequest.gcount() >= MAX_REQUEST_LENGTH && m_HttpRequestType == GraphRequest::GET ) {
        Log::error( "AbstractDataProvider: the length for GET request has been exceeded. "
                "Use POST request within the scope of fb recommendations for such case." );
        return NONE_DATA_COULD_BE_RESULT;
    }
    Log::debug( "AbstractDataProvider: url =%s\n",  finalRequest.str().c_str() );
    m_Request = SendRequest( finalRequest.str().c_str(), param );
    SaveLastRequest( request, isDescendingOrder );
    return ONLY_REQUEST_SENT_RESULT;
}

bool AbstractDataProvider::GetPagingAttributes( std::stringstream &finalRequest, bool isDescendingOrder, bool isExtraParams )
{
    if (isDescendingOrder) {
        if (m_Pagination.tail) {
            finalRequest << (isExtraParams ? '&' : '?') << m_PageIdettifier_Next[m_PagingAlgorithm] << "=" << m_Pagination.tail;
        } else if (m_Pagination.head) {
            // we are on the last page
            return false;
        } else if ( !m_Pagination.isHeadRequired ) {
            return false;
        } else {
            Log::debug( "AbstractDataProvider: absent data for tail." );
        }
    } else {
        if (m_Pagination.head) {
            if(!m_SkipHeaderPaging) {
                finalRequest << (isExtraParams ? '&' : '?') << m_PageIdettifier_Prev[m_PagingAlgorithm] << "=" << m_Pagination.head;
            }
        } else if (m_Pagination.tail) {
            // we are on the first page
            return false;
        } else if ( !m_Pagination.isHeadRequired ) {
            return false;
        } else {
            Log::debug( "AbstractDataProvider: absent data for head." );
        }
    }
    return true;
}

bool AbstractDataProvider::IsItLastPage( JsonObject *object ) const
{
    if ( m_LastRequests.isBatchRequest ) {
        object = json_object_get_object_member( object, "body" );
    }

    JsonObject *paging = json_object_get_object_member( object, PAGING );
    if ( paging ) {
        return GetStringFromJson( paging, NEXT ) == NULL;
    }
    return true;
}

bool AbstractDataProvider::IsItFirstPage( JsonObject *object ) const
{
    if ( m_LastRequests.isBatchRequest ) {
        object = json_object_get_object_member( object, "body" );
    }

    JsonObject *paging = json_object_get_object_member( object, PAGING );
    if ( paging ) {
        return GetStringFromJson( paging, PREV ) == NULL;
    } else {
        return true;
    }
}

void AbstractDataProvider::SaveLastRequest( const char *request, bool isDescendingOrder )
{
    m_LastRequests.isLastOrderDescending = isDescendingOrder;
    if ( isDescendingOrder ) {
        free( m_LastRequests.tailRequest );
        m_LastRequests.tailRequest = strdup( request );
    } else {
        free( m_LastRequests.headRequest );
        m_LastRequests.headRequest = strdup( request );
    }
}

void AbstractDataProvider::SetEtag(const char *etag) {
    if(m_Etag){
        free(m_Etag); m_Etag = NULL;
    }
    m_Etag = (etag) ? strdup(etag) : NULL;
}

void AbstractDataProvider::SetSkipHeaderPagingEnabled(bool isEnable) {
    m_SkipHeaderPagingEnabled = isEnable;
}

void AbstractDataProvider::AppendData( const Eina_Array *array, bool isToEnd )
{
    unsigned int newItemsCount = eina_array_count( array );
    assert( array );
    assert( m_Data );
    if ( newItemsCount > 0 ) {
        unsigned int index = 0;
        void* item;
        Eina_Array_Iterator iterator;
        if ( isToEnd ) {
            EINA_ARRAY_ITER_NEXT( array, index, item, iterator ) {
                if ( !eina_array_push( m_Data, item ) ) {
                    Log::error( "AbstractDataProvider: cannot append new items to the end." );
                }
            }
        } else {
            Eina_Array *arrayForMerge = eina_array_new( BASE_CONTAINER_SIZE );
            if ( arrayForMerge == NULL ) {
                Log::error( "AbstractDataProvider: cannot allocate a memory for a new array." );
                return;
            }
            //
            // copy new items
            EINA_ARRAY_ITER_NEXT( array, index, item, iterator ) {
                if ( !eina_array_push( arrayForMerge, item ) ) {
                    Log::error( "AbstractDataProvider: cannot pre-append new items to the begin." );
                }
            }
            //
            // copy cached items
            EINA_ARRAY_ITER_NEXT( m_Data, index, item, iterator ) {
                if ( !eina_array_push( arrayForMerge, item ) ) {
                    Log::error( "AbstractDataProvider: cannot post-append new items to the begin." );
                }
            }
            eina_array_free( m_Data );
            m_Data = arrayForMerge;
        }
    }
}

void AbstractDataProvider::AppendData( Eina_List *list, bool isToEnd ) {
    assert( m_Data );
    if ( list ) {
        unsigned int index = 0;
        void* item;
        Eina_Array_Iterator iterator;
        Eina_List *l, *l_next;
        void *itemData;
        if ( isToEnd ) {
            EINA_LIST_FOREACH_SAFE( list, l, l_next, itemData ) {
                if ( !eina_array_push( m_Data, itemData ) ) {
                    Log::error( "AbstractDataProvider: cannot append new items to the end." );
                }
            }
        } else {
            Eina_Array *arrayForMerge = eina_array_new( BASE_CONTAINER_SIZE );
            if ( arrayForMerge == NULL ) {
                Log::error( "AbstractDataProvider: cannot allocate a memory for a new array." );
                return;
            }
            //
            // copy new items
            EINA_LIST_FOREACH_SAFE( list, l, l_next, itemData ) {
                if ( !eina_array_push( arrayForMerge, itemData ) ) {
                    Log::error( "AbstractDataProvider: cannot pre-append new items to the begin." );
                }
            }
            //
            // copy cached items
            EINA_ARRAY_ITER_NEXT( m_Data, index, item, iterator ) {
                if ( !eina_array_push( arrayForMerge, item ) ) {
                    Log::error( "AbstractDataProvider: cannot post-append new items to the begin." );
                }
            }
            eina_array_free( m_Data );
            m_Data = arrayForMerge;
        }
    }
}

void AbstractDataProvider::AppendData( void *item, bool isToEnd )
{
    assert(item);
    BaseObject *baseItem = static_cast<BaseObject *> (item);
    baseItem->AddRef();
    assert( m_Data );
    if ( isToEnd ) {
        if ( !eina_array_push( m_Data, item ) ) {
            baseItem->Release();
            Log::error( "AbstractDataProvider: cannot append new items to the end." );
        }
    } else {
        Eina_Array *arrayForMerge = eina_array_new( BASE_CONTAINER_SIZE );
        if ( arrayForMerge == NULL ) {
            baseItem->Release();
            Log::error( "AbstractDataProvider: cannot allocate a memory for a new array." );
            return;
        }
        //
        // copy new items
        if ( !eina_array_push( arrayForMerge, item ) ) {
            baseItem->Release();
            Log::error( "AbstractDataProvider: cannot pre-append new items to the begin." );
        }
        //
        // copy cached items
        unsigned int index = 0;
        void* item;
        Eina_Array_Iterator iterator;
        EINA_ARRAY_ITER_NEXT( m_Data, index, item, iterator ) {
            if ( !eina_array_push( arrayForMerge, item ) ) {
                Log::error( "AbstractDataProvider: cannot post-append new items to the begin." );
            }
        }
        eina_array_free( m_Data );
        m_Data = arrayForMerge;
    }
}

void AbstractDataProvider::RemoveData(Eina_List *list) {
    assert( m_Data );
    if ( list ) {
        Eina_List *l, *l_next;
        void *itemData;
        EINA_LIST_FOREACH_SAFE( list, l, l_next, itemData ) {
            if ( !eina_array_remove( m_Data, RemoveComparator, itemData ) ) {
                Log::error( "AbstractDataProvider::RemoveData: cannot remove element" );
            }
        }
    }
}

//
// If both Posts are equal, 'EINA_FALSE' will return, otherwise 'EINA_TRUE'
Eina_Bool AbstractDataProvider::RemoveComparator( void *forCompare, void *toCompare ) {
    if ( forCompare == toCompare ) {
        return EINA_FALSE;
    } else {
        return EINA_TRUE;
    }
}

GraphRequest* AbstractDataProvider::SendRequest( const char *request, bundle *param )
{
    Log::debug( "Etag-FT: send request = %s, etag = %s", request, ( m_SkipHeaderPaging ? m_Etag : NULL ) );
    GraphRequest *graphRequest = new GraphRequest( request, m_HttpRequestType, this, param, m_SkipHeaderPaging ? m_Etag : NULL);
    graphRequest->ExecuteAsyncExHeader(handleThreadMessage_cb);

    bool isHead = ( !m_LastRequests.isLastOrderDescending || ( !m_LastRequests.headRequest && !m_LastRequests.tailRequest ) );
    if ( isHead && m_IsRequestTimerEnabled ) {
        m_LastRequests.lastRequestTime_secs = Utils::GetCurrentTimeMicroSecs() / 1000000;
    }

    return graphRequest;
}

void AbstractDataProvider::SetHttpRequestType( GraphRequest::HttpMethod httpRequestType )
{
    m_HttpRequestType = httpRequestType;
}

void AbstractDataProvider::SetPagingAlgorithm( PAGIN_ALGORITHM algorithm )
{
    m_PagingAlgorithm = algorithm;
}

void AbstractDataProvider::SetAPIVersion( FB_API_VERSION version )
{
    m_APIVersion = version;
}

void AbstractDataProvider::SetAPIType( API_TYPE type )
{
    m_APIType = type;
}

void AbstractDataProvider::SetLimit( unsigned int itemsLimit )
{
    m_ItemsLimit = itemsLimit;
}

unsigned int AbstractDataProvider::GetLimit() const
{
    return m_ItemsLimit;
}

int AbstractDataProvider::GetItemsTotalCount() const
{
    return m_ItemsTotalCount;
}

void AbstractDataProvider::SetReverseChronological( bool isReverseChronological )
{
    m_IsReverseChronological = isReverseChronological;
}

bool AbstractDataProvider::IsReverseChronological() const
{
    return m_IsReverseChronological;
}

Eina_Array* AbstractDataProvider::GetData() const
{
    return m_Data;
}

unsigned int AbstractDataProvider::GetDataCount() const
{
    unsigned int res = 0;
    if(m_Data) {
        res = eina_array_count( m_Data );
    }
    return res;
}

void AbstractDataProvider::SetCommentsLimit( int limit )
{
    m_CommentsLimit = limit;
}

int AbstractDataProvider::GetCommentsLimit() const
{
    return m_CommentsLimit;
}

void AbstractDataProvider::SetCommentsSummary( bool isSummary )
{
    m_IsCommentsSummary = isSummary;
}

bool AbstractDataProvider::IsCommentsSummary() const
{
    return m_IsCommentsSummary;
}

void AbstractDataProvider::SetLikesLimit( int limit )
{
    m_LikesLimit = limit;
}

int AbstractDataProvider::GetLikesLimit() const
{
    return m_LikesLimit;
}

void AbstractDataProvider::SetLikesSummary( bool isSummary )
{
    m_IsLikesSummary = isSummary;
}

bool AbstractDataProvider::IsLikesSummary() const
{
    return m_IsLikesSummary;
}

void AbstractDataProvider::SetBatchRequest( bool isBatchRequest )
{
    m_LastRequests.isBatchRequest = isBatchRequest;
}

void AbstractDataProvider::SetDataCached( bool isCached, Ecore_Thread *thread )
{
    if ( m_IsDataCached && !isCached ) {
        CleanRequest();
        NotifyErase( thread );
    }
    m_IsDataCached = isCached;
}

bool AbstractDataProvider::IsDataCached() const
{
    return m_IsDataCached;
}

Eina_Bool AbstractDataProvider::KeepItemByPointer(void *forCompare, void *toCompare ) {
    if (forCompare == toCompare) {
        return EINA_FALSE;
    } else {
        return EINA_TRUE;
    }
}

Eina_Bool AbstractDataProvider::KeepItemById(void *forCompare, void *toCompare ) {
    GraphObject *forCmp = static_cast<GraphObject*>(forCompare);
    GraphObject *toCmp = static_cast<GraphObject*>(toCompare);
    if ((forCmp->GetHash() == toCmp->GetHash()) && (strcmp( forCmp->GetId(), toCmp->GetId() ) == 0 )) {
        return EINA_FALSE;
    } else {
        return EINA_TRUE;
    }
}

bool AbstractDataProvider::idsComparator(const void * object, const void * id) {
    return (object && id && strcmp((char *) object, (char*) id) == 0 );
}

Eina_List* AbstractDataProvider::SearchItem(const char *condition)
{
    return NULL;
}

void AbstractDataProvider::SwitchContinueRequest()
{
    m_IsContinueRequest = true;
}

bool AbstractDataProvider::IsContainItem( void *forCompare, Eina_Array *arrayForSearch )
{
    Eina_Array *array = ( arrayForSearch ? arrayForSearch : m_Data );
    unsigned int index = 0;
    void* item;
    Eina_Array_Iterator iterator;
    EINA_ARRAY_ITER_NEXT( array, index, item, iterator ) {
        if ( !KeepItemById( forCompare, item ) ) {
            return true;
        }
    }
    return false;
}


void AbstractDataProvider::SetData(Eina_Array * tmp_Data)
{
    m_Data = tmp_Data;
}
void AbstractDataProvider::SetTotalDataCount(int i)
{
    m_ItemsTotalCount = i;
}

void AbstractDataProvider::ResetRequestTimer()
{
    m_LastRequests.lastRequestTime_secs = 0;
}

void AbstractDataProvider::UpdateTimerToCurrent()
{
    m_LastRequests.lastRequestTime_secs = Utils::GetCurrentTimeMicroSecs() / 1000000;
}

GraphObject * AbstractDataProvider::GetGraphObjectById(const char * id)
{
    GraphObject *ret = NULL;
    assert(id);
    if (GetData() && GetDataCount()) {
        GraphObject *data = NULL;
        for (unsigned int i = 0; i < GetDataCount(); ++i) {
            data = static_cast<GraphObject*>(eina_array_data_get(GetData(), i));
            if (idsComparator(data->GetId(), id)) {
                ret = data;
                break;
            }
        }
    }
    return ret;
}

void AbstractDataProvider::SetRequestTimerEnabled( bool isEnabled )
{
    m_IsRequestTimerEnabled = isEnabled;
}

bool AbstractDataProvider::IsRequestTimerEnabled() const
{
    return m_IsRequestTimerEnabled;
}


void AbstractDataProvider::DeleteAllOperations() {
    if(mOperationPresenters) {
        Eina_List *l, *l_next;
        void *itemData;
        EINA_LIST_FOREACH_SAFE( mOperationPresenters, l, l_next, itemData ) {
            OperationPresenter *op = static_cast<OperationPresenter *>(itemData);
            if(op) {
                op->Release();
            }
        }
        mOperationPresenters = eina_list_free( mOperationPresenters );
    }
    mOperations.clear();
}

void AbstractDataProvider::Update(AppEventId eventId, void * data) {
}

void AbstractDataProvider::UpdateInternal(AppEventId eventId, void * data) {
    if(eventId == eREQUEST_CONTINUE_EVENT) {
        RequestContinueEvent *eData = static_cast<RequestContinueEvent *>(data);
        if(eData) {
            RequestData( eData->mRequest, eData->mIsDescendingOrder, eData->mParam );
        }
    }
}

void AbstractDataProvider::AddOperation(Sptr<Operation> operation) {
    dlog_print( DLOG_DEBUG, "Facebook_oper", "AbstractDataProvider::AddOperation start" );
    if(operation) {
        bool isFound(false);
        for(auto operationIterator = mOperations.begin(); operationIterator != mOperations.end(); operationIterator++) {
            if(*operationIterator == operation) {
                isFound = true;
                break;
            }
        }
        if(!isFound) {
            mOperations.push_back(operation);
            OperationPresenter* op = new OperationPresenter(operation);
            mOperationPresenters = eina_list_append( mOperationPresenters, op );

            AppendData(op, m_IsReverseChronological);
            DataEventDescription dataEvent(this,
                    DataEventDescription::eDE_ITEM_ADDED,
                    op->GetId(),
                    m_IsReverseChronological,
                    operation->GetType());
            AppEvents::Get().Notify(eDATA_CHANGES_EVENT, &dataEvent);
        }
    }
    dlog_print( DLOG_DEBUG, "Facebook_oper", "AbstractDataProvider::AddOperation end" );
}

void AbstractDataProvider::RemoveOperation(Sptr<Operation> operation) {
    dlog_print( DLOG_DEBUG, "Facebook_oper", "AbstractDataProvider::RemoveOperation start" );

    mOperations.remove(operation);

    Eina_List *l, *l_next;
    void *itemData;
    EINA_LIST_FOREACH_SAFE( mOperationPresenters, l, l_next, itemData ) {
        OperationPresenter *op = static_cast<OperationPresenter *>(itemData);
        if(op && (op->GetOperation() == operation)) {
            DeleteItem(op);
            op->Release(); // because of DeleteItem(op);
            DataEventDescription dataEvent(this,
                    DataEventDescription::eDE_ITEM_REMOVED,
                    op->GetId());
            AppEvents::Get().Notify(eDATA_CHANGES_EVENT, &dataEvent);
            mOperationPresenters = eina_list_remove(mOperationPresenters, op);
            op->Release(); // because of mOperationPresenters = eina_list_remove(mOperationPresenters, op);
            break;
        }
    }

    operation->UnSubscribe(this);
    dlog_print( DLOG_DEBUG, "Facebook_oper", "AbstractDataProvider::RemoveOperation end" );
}

bool AbstractDataProvider::IsRequestAlive() const
{
    return m_Request != NULL || m_IsContinueRequest;
}

void AbstractDataProvider::ResetNotifyJob() {
    mNotifyJob = NULL;
}

void AbstractDataProvider::SetNotifyJob(Ecore_Job *notifyJob) {
    mNotifyJob = notifyJob;
}

void AbstractDataProvider::ResetNotifySelfJob() {
    mNotifySelfJob = NULL;
}

void AbstractDataProvider::SetNotifySelfJob(Ecore_Job *notifyJob) {
    mNotifySelfJob = notifyJob;
}

unsigned int AbstractDataProvider::GetPresentersCount() const
{
    return eina_list_count( mOperationPresenters );
}

bool AbstractDataProvider::ReplaceGraphObjectById(const char *id, GraphObject *newObject, Eina_Array* dataArray) {
    assert(id);
    assert(newObject);
    bool ret = false;
    if (dataArray) {
        GraphObject *data = nullptr;
        for (unsigned int i = 0; i < eina_array_count(dataArray); ++i) {
            data = static_cast<GraphObject*>(eina_array_data_get(dataArray, i));
            assert(data);
            assert(data->GetId());
            if (idsComparator(data->GetId(), id)) {
                data->Release();
                newObject->AddRef();
                eina_array_data_set(dataArray, i, newObject);
                ret = true;
                break;
            }
        }
    }
    return ret;
}

