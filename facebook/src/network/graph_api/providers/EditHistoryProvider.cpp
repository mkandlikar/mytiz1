#include <assert.h>

#include "EditAction.h"
#include "EditHistoryProvider.h"
#include "jsonutilities.h"
#include "FbRespondGetEditActions.h"
#include "Log.h"
#include "ParserWrapper.h"
#include "WidgetFactory.h"

const char* EditHistoryProvider::REQUEST_MAIN_PART = "%s?fields=edit_actions{edit_time,editor,text}";

EditHistoryProvider::EditHistoryProvider()
{
    mId = NULL;
    SetPagingAlgorithm(CURSOR_ALGORITHM);
}

EditHistoryProvider::~EditHistoryProvider()
{
    EraseData();
}

void EditHistoryProvider::CustomErase()
{
    free((void *) mId); mId = NULL;
}

void EditHistoryProvider::SetId(const char *id)
{
    EraseData();

    mId = SAFE_STRDUP(id);
}

EditHistoryProvider* EditHistoryProvider::GetInstance()
{
    static EditHistoryProvider *editHistoryProvider = new EditHistoryProvider();
    return editHistoryProvider;
}

unsigned int EditHistoryProvider::ParseData(char *data, bool isDescendingOrder, Ecore_Thread *thread)
{
    JsonObject * object = NULL;
    JsonParser * parser = openJsonParser(data, &object);
    ParserWrapper wr( parser );

    unsigned int itemsCount = NONE_ITEMS;
    FbRespondGetEditActions * actionRespond = new FbRespondGetEditActions(object);
    if (actionRespond->mEditActionsList) {
        unsigned int countainerSize = GetLimit() > 0 ? GetLimit() : RESPONSE_CONTAINER_SIZE;
        Eina_Array *parsedItemsArray = eina_array_new(countainerSize);
        if (parsedItemsArray == NULL) {
            Log::error("EditHistoryProvider: cannot allocate a memory for a request array.");
            return NONE_ITEMS;
        }
        Eina_List *list;
        void *data;
        EINA_LIST_FOREACH(actionRespond->mEditActionsList, list, data) {
            ++itemsCount;
            EditAction *action = static_cast<EditAction*>(data);
            if (action->mText) {
                eina_array_push(parsedItemsArray, action);
            }
            actionRespond->RemoveDataFromList(action);
        }
        Log::debug("EditHistoryProvider: there was handled %d items.", itemsCount);
        AppendData(parsedItemsArray, isDescendingOrder);
        eina_array_free(parsedItemsArray);
    }
    delete actionRespond;

    return itemsCount;
}

UPLOAD_DATA_RESULT EditHistoryProvider::UploadData(bool isDescendingOrder)
{
    if (!mId) {
        Log::error("EditHistoryProvider: entity identifier was not specified.");
        assert(false);
        return NONE_DATA_COULD_BE_RESULT;
    }

    if (GetDataCount() == GetItemsTotalCount()) {
        return NONE_DATA_COULD_BE_RESULT;
    }

    char *url = WidgetFactory::WrapByFormat(REQUEST_MAIN_PART, mId);
    UPLOAD_DATA_RESULT res = RequestData(url, isDescendingOrder);
    delete []url;

    return res;
}

void EditHistoryProvider::DeleteItem(void* item)
{
    if (!eina_array_remove(GetData(), KeepItemById, item)) {
        Log::warning("EditHistoryProvider: cannot delete the referenced element");
    }
}

void EditHistoryProvider::ReleaseItem(void* item)
{
    BaseObject *baseObject = static_cast<BaseObject*>(item);
    baseObject->Release();
}

