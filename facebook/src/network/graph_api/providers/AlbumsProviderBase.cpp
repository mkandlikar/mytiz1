#include "AlbumsProviderBase.h"

const char* AlbumsProviderBase::ALBUMS_MAIN_REQUEST_PART = "/albums";
const char* AlbumsProviderBase::ALBUMS_FIELDS_LIST = "name,description,can_upload,count,picture,type,created_time,privacy,from,location";

AlbumsProviderBase::AlbumsProviderBase() {
    Log::info("AlbumsProviderBase::AlbumsProviderBase");
    SetPagingAlgorithm(ITERATORS_ALGORITHM);
}

AlbumsProviderBase::~AlbumsProviderBase() {
    EraseData();
}

UPLOAD_DATA_RESULT AlbumsProviderBase::UploadData(bool isDescendingOrder) {
    CHECK_RET(!mUserId.empty(), NONE_DATA_COULD_BE_RESULT);
    Log::info("AlbumsProviderBase::UploadData");
    if (eina_array_count( GetData() ) == GetItemsTotalCount()) {
        // We cannot request more data
        return NONE_DATA_COULD_BE_RESULT;
    }

    std::stringstream request;
    request << mUserId << ALBUMS_MAIN_REQUEST_PART << "?" << FIELDS << ALBUMS_FIELDS_LIST;
    Log::info("AlbumsProviderBase UPLOAD:%s", request.str().c_str());
    return RequestData(request.str().c_str(), isDescendingOrder);
}

void AlbumsProviderBase::DeleteItem(void* item) {
    Log::info("AlbumsProviderBase::DeleteItem");
    if (!eina_array_remove(GetData(), KeepItemByPointer, item)) {
        dlog_print( DLOG_DEBUG, LOG_TAG_FACEBOOK, "AlbumsProviderBase: cannot delete the referenced element" );
    }
}

void AlbumsProviderBase::CustomErase() {
    Log::info("AlbumsProviderBase::CustomErase");
    mUserId.clear();
}

void AlbumsProviderBase::ReleaseItem(void* item) {
    Log::info("AlbumsProviderBase::ReleaseItem");
    BaseObject* object = static_cast<BaseObject*>(item);
    CHECK_RET_NRV(object);
    object->Release();
}
