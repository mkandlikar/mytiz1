#include <assert.h>
#include <sstream>

#include "ParserWrapper.h"
#include "jsonutilities.h"
#include "FriendsList.h"
#include "Friend.h"
#include "GroupFriendsProvider.h"
#include "Utils.h"
#include "Log.h"


const char* GroupFriendsProvider::REQUEST_MAIN_PART = "%s/members?fields=id,name,picture";

GroupFriendsProvider::GroupFriendsProvider() : mGroupId(NULL) {
    SetPagingAlgorithm(ITERATORS_ALGORITHM);
}

GroupFriendsProvider::~GroupFriendsProvider() {
    EraseData();
}

void GroupFriendsProvider::SetGroupId(const char *groupId) {
    assert(groupId);
    if (groupId) {
        if (!mGroupId || strcmp(mGroupId, groupId)) {
            if (mGroupId) {
                EraseData();
                free((void*) mGroupId);
            }
            mGroupId = SAFE_STRDUP(groupId);
        }
    }
}

GroupFriendsProvider* GroupFriendsProvider::GetInstance() {
    static GroupFriendsProvider *groupFriendsProvider = new GroupFriendsProvider();
    return groupFriendsProvider;
}

void GroupFriendsProvider::CustomErase() {
    free((void*) mGroupId);
    mGroupId = NULL;
}

unsigned int GroupFriendsProvider::ParseData(char *response, bool isDescendingOrder, Ecore_Thread *thread) {
    Log::debug("GroupFriendsProvider::ParseData()");
    JsonObject *object = NULL;
    JsonParser *parser = openJsonParser(response, &object);
    ParserWrapper wr(parser);
    unsigned int itemsCount = NONE_ITEMS;
    FriendsList *friends = new FriendsList(object);
    unsigned int countainerSize = GetLimit() > 0 ? GetLimit() : RESPONSE_CONTAINER_SIZE;
    Eina_Array *parsedItemsArray = eina_array_new(countainerSize);
    if (parsedItemsArray == NULL) {
        Log::error("GroupFriendsProvider: cannot allocate a memory for a request array.");
        return NONE_ITEMS;
    }
    Eina_List *list;
    void *data;
    EINA_LIST_FOREACH(friends->GetList(), list, data) {
        ++itemsCount;
        Friend *fr = static_cast<Friend*>(data);
        fr->AddRef();
        eina_array_push(parsedItemsArray, fr);
    }
    AppendData(parsedItemsArray, isDescendingOrder);
    eina_array_free(parsedItemsArray);
    delete friends;
    return itemsCount;
}

UPLOAD_DATA_RESULT GroupFriendsProvider::UploadData(bool isDescendingOrder) {
    Log::debug("GroupFriendsProvider::UploadData()");
    if (eina_array_count(GetData()) == GetItemsTotalCount()) {
        return NONE_DATA_COULD_BE_RESULT;
    }

    int size = strlen(REQUEST_MAIN_PART) + strlen(mGroupId) + 1;
    char *url = new char[size];
    Utils::Snprintf_s(url, size, REQUEST_MAIN_PART, mGroupId);
    UPLOAD_DATA_RESULT res = RequestData(url, isDescendingOrder);
    delete []url;
    return res;
}

void GroupFriendsProvider::DeleteItem(void* item) {
    if (!eina_array_remove(GetData(), KeepItemById, item)) {
        Log::error("GroupFriendsProvider: cannot delete the referenced element" );
    }
}

void GroupFriendsProvider::ReleaseItem( void* item ) {
    BaseObject *baseObject = static_cast<BaseObject*>(item);
    baseObject->Release();
}

