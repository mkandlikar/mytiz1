#include <efl_extension.h>
#include <sstream>
#include <dlog.h>
#include <stdlib.h>

#include "Common.h"
#include "Config.h"
#include "FbRespondGetUsersDetailInfo.h"
#include "Friend.h"
#include "ParserWrapper.h"
#include "UserDetailInfo.h"
#include "UsersDetailInfoProvider.h"
#include "Utils.h"
#include "jsonutilities.h"

const unsigned int UsersDetailInfoProvider::REQUEST_USERS_DETAIL_INFO_LIMIT = 10;
const unsigned int UsersDetailInfoProvider::DEFAULT_MUTUAL_FRIENDS_LIMIT = 2;

UsersDetailInfoProvider::UsersDetailInfoProvider()
{
    SetLimit( REQUEST_USERS_DETAIL_INFO_LIMIT );
    SetMutualFriendsLimit( DEFAULT_MUTUAL_FRIENDS_LIMIT );
    SetPagingAlgorithm( MANUAL_ALGORITHM );
    m_Ids = eina_array_new( RESPONSE_CONTAINER_SIZE );
    if (!m_Ids) {
        Log::error("UsersDetailInfoProvider: cannot allocate a memory for an array." );
    }
    SetRequestTimerEnabled(false);
    mLastRequestInDescendingOrder = false;
}

UsersDetailInfoProvider::~UsersDetailInfoProvider(){
    ClearIds();
    eina_array_free (m_Ids);// m_Ids = NULL;
    EraseData();
}

UsersDetailInfoProvider* UsersDetailInfoProvider::GetInstance()
{
    static UsersDetailInfoProvider *usersDetailInfoProvider = new UsersDetailInfoProvider();
    return usersDetailInfoProvider;
}

void UsersDetailInfoProvider::CustomErase()
{
}

void UsersDetailInfoProvider::SetMutualFriendsLimit( unsigned int limit )
{
    m_MutualFriendsLimit = limit;
}

void UsersDetailInfoProvider::DeleteItem( void* item )
{
    if (!eina_array_remove(GetData(), KeepItemByPointer, item)) {
        Log::error("UsersDetailInfoProvider::DeleteItem Error" );
    } else {
        ReleaseItem(item);
    }
}

bool UsersDetailInfoProvider::CheckPaginationInfo( bool isDescendingOrder )
{
    if(isDescendingOrder) {
        if(!m_Pagination.tail) {
            return false;
        }
    }
    else{
        if (!m_Pagination.head) {
            return false;
        }
    }
    return true;
}

UPLOAD_DATA_RESULT UsersDetailInfoProvider::UploadData( bool isDescendingOrder )
{
    Log::debug("UsersDetailInfoProvider::UploadData->%d", isDescendingOrder );

    if  (!CheckPaginationInfo(isDescendingOrder)) {
        return NONE_DATA_COULD_BE_RESULT;
    }

    std::stringstream request;

    request << "?" << FIELDS;

#ifdef FEATURE_MUTUAL_FRIENDS
    request << ID << ',' << NAME << ',' << PICTURE << ',' << COVER << ','
            << RELATIONSHIP_STATUS << ',' << BIRTHDAY << ','
            << EDUCATION << '{' << SCHOOL << '{' << NAME << ',' << PICTURE << "}}" << ','
            << LOCATION << '{' << ID << ',' << NAME << ',' << PICTURE << '}' << ','
            << WORK << '{' << EMPLOYER << '{' << ID << ',' << NAME << ',' << PICTURE << '}' << ','
            << POSITION << '}' << ',' << CONTEXT << '{'
            << MUTUAL_FRIENDS << '.' << LIMIT << '(' << m_MutualFriendsLimit << ')'
            << '{' << ID << ',' << NAME << ',' << PICTURE << "}}";
#else
    request << ID << ',' << NAME << ',' << PICTURE << ',' << COVER << ','
            << RELATIONSHIP_STATUS << ',' << BIRTHDAY << ','
            << EDUCATION << '{' << SCHOOL << '{' << NAME << ',' << PICTURE << "}}" << ','
            << LOCATION << '{' << ID << ',' << NAME << ',' << PICTURE << '}' << ','
            << WORK << '{' << EMPLOYER << '{' << ID << ',' << NAME << ',' << PICTURE << '}' << ','
            << POSITION << '}';
#endif

    Log::debug("UsersDetailInfoProvider::UploadData->request.str()=%s", request.str().c_str());
    return RequestData( request.str().c_str(), isDescendingOrder );
}

void UsersDetailInfoProvider::FillManualPagination(bool isDescendigOrder) {
    std::string st = PrepareIdsPage(isDescendigOrder);
    if ( isDescendigOrder ) {
        free( m_Pagination.tail );
        m_Pagination.tail = NULL;
        if(st.length()) {
            m_Pagination.tail = strdup( st.c_str() );
        }
    } else {
        free( m_Pagination.head );
        m_Pagination.head = NULL;
        if(st.length()) {
            m_Pagination.head = strdup( st.c_str() );
        }
    }
}

unsigned int UsersDetailInfoProvider::ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread )
{
    JsonObject *object = NULL;
    JsonParser *parser = openJsonParser( data, &object );
    ParserWrapper wr( parser );
    JsonNode* rootNode = json_parser_get_root (parser);

    unsigned int itemsCount = NONE_ITEMS;
    FbRespondGetUsersDetailInfo * userDetailInfoRespond = new FbRespondGetUsersDetailInfo( rootNode );

    if ( object && userDetailInfoRespond->mUsersDetailInfoList ) {
        unsigned int countainerSize = GetLimit() > 0 ? GetLimit() : RESPONSE_CONTAINER_SIZE;
        Eina_Array *parsedItemsArray = eina_array_new( countainerSize );

        if ( !parsedItemsArray || !m_Ids ) {
            Log::debug("UsersDetailInfoProvider: cannot allocate a memory for a request array." );
            return NONE_ITEMS;
        }
        Eina_List *list;
        Eina_List *l_next;
        void *data;
        EINA_LIST_FOREACH_SAFE( userDetailInfoRespond->mUsersDetailInfoList, list, l_next ,data ) {
            ++itemsCount;
            UserDetailInfo *user = static_cast<UserDetailInfo*>(data);
            if ( NO_ITEMS_FOUND == Utils::FindElement((void *)user->GetId(), m_Ids, UsersDetailInfoProvider::idsComparator) ) {
                // looks like that we have to ignore it
            } else {
                eina_array_push( parsedItemsArray, user );
                userDetailInfoRespond->RemoveDataFromList(user);
            }
        }
        Log::debug("UsersDetailInfoProvider: there was handled %d items and %d people you may know parsed.",
            itemsCount, eina_array_count( parsedItemsArray ) );
        mLastRequestInDescendingOrder = isDescendingOrder;
        AppendData( parsedItemsArray, isDescendingOrder );
        eina_array_free( parsedItemsArray );
    }
    delete userDetailInfoRespond;
    return itemsCount;
}

Eina_Array* UsersDetailInfoProvider::GetIds() const
{
    return m_Ids;
}

void UsersDetailInfoProvider::SetPositionById(const char *firstId) {
    int index = 0;
    unsigned int size = eina_array_count(m_Ids);
    Log::debug("UsersDetailInfoProvider::SetPositionById %d", size);
    for (unsigned int i = 0; i < size; i++){
        char *id = static_cast<char *>(eina_array_data_get(m_Ids, i));
        if(id && (strcmp(id, firstId) == 0)) {
            index = i;
            break;
        }
    }

    // initial paging filling
    std::stringstream ids_chain;

    int nextIndex = 0;

    nextIndex = index + REQUEST_USERS_DETAIL_INFO_LIMIT;

    if( nextIndex > size ) {
        nextIndex = size;
    }

    m_IdsTailIndex = nextIndex;
    m_IdsHeadIndex = index;

    for ( unsigned int i = index; i < nextIndex; ++i ) {
        char *item = static_cast<char *>(eina_array_data_get(m_Ids, i));
        ids_chain << item << ( (i + 1) < nextIndex ? "," : "" );
        Log::debug("UsersDetailInfoProvider::SetPositionById_End item->%s", item);
    }

    free( m_Pagination.tail );
    m_Pagination.tail = NULL;
    if(ids_chain.str().length()) {
        m_Pagination.tail = strdup( ids_chain.str().c_str() );
    }

    if(index == 0) return;//miss head pagination in this case

    int prevIndex = 0;

    prevIndex = index - REQUEST_USERS_DETAIL_INFO_LIMIT;

    if( prevIndex < 0 ) {
        prevIndex = 0;
    }
    m_IdsHeadIndex = prevIndex;

    ids_chain.str("");
    for ( unsigned int i = prevIndex; i < index ; ++i ) {
        char *item = static_cast<char *>(eina_array_data_get(m_Ids, i));
        ids_chain << item << ( (i + 1) < index ? "," : "" );
        Log::debug("UsersDetailInfoProvider::SetPositionById_Begin item->%s", item);
    }

    free( m_Pagination.head );
    m_Pagination.head = NULL;
    if(ids_chain.str().length()) {
        m_Pagination.head = strdup( ids_chain.str().c_str() );
    }
}

/**
 * @brief Set a part of all ids in carousel to show users mini-profile in it.
 * New ids are set each next time
 */
std::string UsersDetailInfoProvider::PrepareIdsPage(bool isDescendigOrder) {
    std::stringstream ids_chain;

    int size = eina_array_count(m_Ids);
    int prevIndex = 0;
    int nextIndex = 0;
    if(isDescendigOrder) {
        prevIndex = m_IdsTailIndex;
        nextIndex = m_IdsTailIndex + REQUEST_USERS_DETAIL_INFO_LIMIT;
        if(nextIndex > size) {
            nextIndex = size;
        }
        m_IdsTailIndex = nextIndex;
    } else {
        prevIndex = m_IdsHeadIndex - REQUEST_USERS_DETAIL_INFO_LIMIT;
        nextIndex = m_IdsHeadIndex;
        if(prevIndex < 0) {
            prevIndex = 0;
        }
        m_IdsHeadIndex = prevIndex;
    }
    for ( unsigned int i = prevIndex; i < nextIndex; ++i ) {
        char *item = static_cast<char *>(eina_array_data_get(m_Ids, i));
        ids_chain << item << ( (i + 1) < nextIndex ? "," : "" );
    }
    Log::debug("UsersDetailInfoProvider::PrepareIdsPage ids_chain.str= %s",ids_chain.str().c_str() );
    return ids_chain.str();
}

/**
 * @brief Set all ids in carousel to show users mini-profile in it.
 */
void UsersDetailInfoProvider::SetIds(const Eina_Array * ids) {

    ClearIds();

    unsigned int index = 0;
    void* item;
    Eina_Array_Iterator iterator;
    // copy new items
    EINA_ARRAY_ITER_NEXT( ids, index, item, iterator ) {
        if(item) {
            char *id = strdup((char*)item);
            if ( !eina_array_push( m_Ids, id ) ) {
                free(id);
                Log::error("UsersDetailInfoProvider::SetIds cannot copy new items." );
            }
        }
    }
}

bool UsersDetailInfoProvider::idsComparator(const void * object, const void * id)
{
    return ( strcmp((char *) object, (char*) id) == 0 );
}

void UsersDetailInfoProvider::ReleaseItem( void* item ) {
    BaseObject *baseObject = static_cast<BaseObject*>(item);
    baseObject->Release();
}

void UsersDetailInfoProvider::ClearIds() {
    if ( m_Ids ) {
        unsigned int index = 0;
        void* item;
        Eina_Array_Iterator iterator;
        EINA_ARRAY_ITER_NEXT( m_Ids, index, item, iterator ) {
            free(item);
        }
        eina_array_clean( m_Ids );
    }
}

UserDetailInfo* UsersDetailInfoProvider::GetUsersDetailInfoObjectById(const char *userId) {
    return dynamic_cast<UserDetailInfo *>(GetGraphObjectById(userId));
}
