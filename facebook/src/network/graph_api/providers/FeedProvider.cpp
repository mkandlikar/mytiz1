#include "CacheManager.h"
#include "FeedProvider.h"
#include "Group.h"
#include "Utils.h"

#include <dlog.h>


const unsigned int FeedProvider::LIMIT_BUFFER_SIZE = 32;
const unsigned int FeedProvider::REQUEST_FEEDS_LIMIT = 30;
const unsigned int FeedProvider::REQUEST_FEED_COMMENTS_LIMIT = 1;
const char* FeedProvider::FEED_MAIN_REQUEST_PART = "feed";
const char* FeedProvider::FEED_DEFAULT_ID = "me";


FeedProvider::FeedProvider()
{
    mEntityId = nullptr;
    mGraphObj = nullptr;
    SetLimit( REQUEST_FEEDS_LIMIT );
    SetCommentsLimit( REQUEST_FEED_COMMENTS_LIMIT );
    SetPagingAlgorithm( ITERATORS_ALGORITHM );
    mProviderType = FEED_PROVIDER;
}

FeedProvider::~FeedProvider() {
    Log::info("FeedProvider::~FeedProvider");
    EraseData();
    if (mGraphObj) {
        mGraphObj->Release();
    }
}


void FeedProvider::Update(AppEventId eventId, void * data)
{
    switch (eventId) {
    case eOPERATION_ADDED_EVENT:
    case eOPERATION_REMOVED_EVENT:
        if (mEntityId) {
            Sptr<Operation> oper(static_cast<Operation*>(data));
            if (Operation::OD_Home == oper->GetDestination() || (oper->GetUserId() == mEntityId || ( oper->GetUserId() == "me" && Utils::IsMe(mEntityId)))) {
                PostProvider::Update(eventId, data);
            }
        }
        break;
    default:
        PostProvider::Update(eventId, data);
        break;
    }
}

void FeedProvider::SetEntityId( const char *id )
{
    mEntityId = id;
    if (mGraphObj) {
        mGraphObj->Release();
    }
    mGraphObj = Utils::FindGraphObjectInProvidersById(mEntityId);
    if (mGraphObj) {
        mGraphObj->AddRef();
    }
}

std::string FeedProvider::GetMainRequestPart() {
    return std::string((mEntityId ? mEntityId : FEED_DEFAULT_ID)).append("/").append(FEED_MAIN_REQUEST_PART);
}

unsigned int FeedProvider::RetrieveCachedData()
{
    return 0;
}

Post* FeedProvider::GetPostByIdFromCache(const char* id) {
    // This will address the scenario, where user has shared the post from homefeed/newsfeed.
    // In other words, the post has already been dowloaded by news feed request.
    return id ? CacheManager::GetInstance().SelectFeedDataById( id ) : nullptr;
}

bool FeedProvider::IsNeedToHandleOperation(Sptr<Operation> oper) {
    bool res = false;
    if ((oper->GetDestinationId() && mEntityId && !strcmp(oper->GetDestinationId(), mEntityId)) || (oper->GetDestination() == Operation::OD_Home) || (oper->GetDestination() == Operation::OD_Group)) {
        res = true;
    }
    return res;
}

unsigned int FeedProvider::ParseData(char *data, bool isDescendingOrder, Ecore_Thread *thread) {
    unsigned int ret = PostProvider::ParseData(data, isDescendingOrder, thread);
    if (mGraphObj) {
        if (mGraphObj->GetGraphObjectType() == GraphObject::GOT_GROUP) {
            Group *group = static_cast<Group *> (mGraphObj);
            if (group->GetPrivacyAsStr() && strcmp(group->GetPrivacyAsStr(), "Open")) {
//This is a workaround. For some posts from non-Open groups Graph API states that these posts can be shared. But actualy those posts can't be shared.
//All the posts from non-Open groups can NOT be shared. So, we need to set mCanShare to false for all posts from non-Public groups.
                unsigned int index;
                void* item;
                Eina_Array_Iterator iterator;
                EINA_ARRAY_ITER_NEXT(GetData(), index, item, iterator) {
                    Post *post = static_cast<Post *>(item);
                    assert(post);
                    post->SetCanShare(false);
                }

            }
        }
    }
    return ret;
}
