#include "CacheManager.h"
#include "DataEventDescription.h"
#include "FbRespondGetFeed.h"
#include "Log.h"
#include "ParserWrapper.h"
#include "Post.h"
#include "PostProvider.h"
#include "Utils.h"

#include <cassert>
#include <eina_array.h>
#include <eina_list.h>


const unsigned int PostProvider::LIMIT_BUFFER_SIZE = 32;
const unsigned int PostProvider::REQUEST_HOME_ITEMS_LIMIT = 35;
const unsigned int PostProvider::REQUEST_HOME_LIKES_LIMIT = 2;
const unsigned int PostProvider::REQUEST_HOME_COMMENTS_LIMIT = 1;
const char *PostProvider::MAIN_REQUEST_PART = "me/home";


bool PostProvider::isStdListContainOrder( std::list<Order*> &list, Order *element )
{
    for ( std::list<Order*>::const_iterator it = list.begin(); it != list.end(); ++it ) {
        Order *order = *it;
        if ( order->mPostAutoId == element->mPostAutoId ) {
            return true;
        }
    }
    return false;
}

PostProvider::PostProvider()
{
    SetSkipHeaderPagingEnabled(true);
    SetLimit( REQUEST_HOME_ITEMS_LIMIT );
    SetLikesLimit( REQUEST_HOME_LIKES_LIMIT );
    SetCommentsLimit( REQUEST_HOME_COMMENTS_LIMIT );
    m_PostLowerBound = m_PostUpperBound = 0;
    mPostLoaderList = nullptr;
    mSourcePosts = eina_array_new(BASE_CONTAINER_SIZE);;
    AppEvents::Get().Subscribe(ePOST_RELOAD_EVENT, this);
    AppEvents::Get().Subscribe(eUPDATE_POST_LIKES_SUMMARY, this);
    AppEvents::Get().Subscribe(eUPDATED_PROFILE_PICTURE, this);
    mMissCacheRetrieve = false;
}

PostProvider::~PostProvider() {
    EraseData();
    eina_array_free(mSourcePosts);
}

void PostProvider::CustomErase()
{
    m_PostLowerBound = m_PostUpperBound = 0;
    if (mPostLoaderList) {
        void * listData;
        EINA_LIST_FREE(mPostLoaderList, listData) {
            delete static_cast<PostLoader*>(listData);
        }
        mPostLoaderList = nullptr;
    }

    if(mSourcePosts) {
        unsigned int index = 0;
        void* item;
        Eina_Array_Iterator iterator;
        EINA_ARRAY_ITER_NEXT( mSourcePosts, index, item, iterator ) {
            assert(item);
            ReleaseItem(item);
        }// release all items from additional sources array
        eina_array_clean(mSourcePosts);
    }
}

Post* PostProvider::GetPostById(const char* id) {
    Post* post = GetPostById(id, GetData());
    if (!post) {
        post = GetPostById(id, mSourcePosts);
    }
    return post;
}

Eina_List* PostProvider::GetPostsByAlbumId(const char* albumId) {
    Eina_List* posts = GetPostsByAlbumId(albumId, GetData());
    Eina_List* sourcesPosts = GetPostsByAlbumId(albumId, mSourcePosts);
    Eina_List* result = eina_list_merge(posts, sourcesPosts);
    return result;
}

std::string PostProvider::GetMainRequestPart() {
    return std::string(MAIN_REQUEST_PART);
}

Post* PostProvider::GetPostByIdFromCache(const char* id) {
    return NULL;
}

void PostProvider::RemoveCompletedPostLoader() {
    Eina_List *l;
    Eina_List *l_next;
    void *data;

    EINA_LIST_FOREACH_SAFE(mPostLoaderList, l, l_next, data){
        PostLoader *postLoader = static_cast<PostLoader*>(data);
        if (postLoader && postLoader->GetProcessed()) {
            mPostLoaderList = eina_list_remove_list(mPostLoaderList, l);
            delete static_cast<PostLoader*>(data);
        }
    }
}

void PostProvider::LoadComplete(PostLoader *loader, int errorCode) {
    assert(loader);
    // at this place we have to:
    // - link parent and child posts
    // - add post to provider
    // - add post to CacheManager
    // - notify clients that data is updated

    Post* childPost = nullptr;
    if(errorCode == CURLE_OK) {
        Post* parentPost = loader->getPost();
        if(parentPost) {
            childPost = GetPostById( loader->getPostId(), GetData() );
            if(childPost) {
                Post* parentPostExist = GetPostById( loader->getParentPostId(), GetData() ); //get from provider
                if(parentPostExist) { // post already exist in provider
                    Log::info("PostProvider::LoadComplete->parentPostExist %s",loader->getParentPostId());
                    childPost->SetParent(parentPostExist);
                } else {
                    Post* parentExistInPostsSources = GetPostById( loader->getParentPostId(), mSourcePosts );
                    if (parentExistInPostsSources) { // post already exist in additional post sources array
                        Log::info("PostProvider::LoadComplete->parentPostExistInSources %s",loader->getParentPostId());
                        childPost->SetParent(parentExistInPostsSources);
                    } else {
                        // add post because it was not in provider yet
                        Log::info("PostProvider::LoadComplete->pushToSourceList %s",loader->getParentPostId());
                        parentPost->AddRef();
                        childPost->SetParent(parentPost);
                        eina_array_push( mSourcePosts, parentPost );
                    }
                }
            } else {
                Log::error("PostProvider::LoadComplete->NoChildPost");
            }
        } else {
            Log::error("PostProvider::LoadComplete->NoParentPost");
        }

    }
    loader->SetProcessed(true);
    // - remove postLoader from the list and destroy it.
    RemoveCompletedPostLoader();
    // we don't need to notify client when each loader is completed because it causes entire window re-creation.
    // we need to notify the client only once when all the loaders are completed. This was a root cause of B2212.
    if (childPost) {
        Log::debug("PostProvider::Update->LoadComplete:child %s", childPost->GetId());
        DataEventDescription dataEvent(this,
                DataEventDescription::eDE_POST_UPDATED,
                childPost->GetId());
        AppEvents::Get().Notify(eDATA_CHANGES_EVENT, &dataEvent);
    }
}

UPLOAD_DATA_RESULT PostProvider::UploadData( bool isDescendingOrder )
{
    if ( eina_array_count( GetData() ) == GetItemsTotalCount() ) {
        //
        // We cannot request more data
        return NONE_DATA_COULD_BE_RESULT;
    }

    std::stringstream request;

    if ( IsCachedDataShouldBeSet() ) {
        bool isCached = RetrieveCachedData() > 0;
        if ( isCached ) {
            SetDataCached( isCached );
            return ONLY_CACHED_DATA_RESULT;
        }
    }

    request << GetMainRequestPart() << "?" << FIELDS;

    GenPostRequestFields(request, GetLikesLimit(), IsLikesSummary(), GetCommentsLimit(), IsCommentsSummary());

    return RequestData( request.str().c_str(), isDescendingOrder );
}

void PostProvider::GenPostRequestFields(std::stringstream &request, int likesLimit, bool isLikesSummary, int commentsLimit,
        bool isCommentsSummary)
{
    request << ID << ',' << CREATED_TIME << ',' << WITH_TAGS << "," << EDIT_ACTIONS << '{' << TEXT << ',' << EDIT_TIME << "},"<< ACTIONS << ',' << CAPTION << ',' << DESCRIPTION << ',' << FULL_PICTURE << ','
            << LINK << ',' << MESSAGE << ',' << MESSAGE_TAGS << ',' << NAME << ',' << OBJECT_ID << ',' << PARENT_ID << ',' << PRIVACY << ','
            << SOURCE << ',' << STATUS_TYPE << ',' << STORY << ',' << STORY_TAGS << ',' << TYPE << ',' << UPDATED_TIME << ','
            << FROM << '{' << NAME << ',' << ID << ',' << PICTURE << "}," << TO << '{' << NAME <<',' << ID << ',' << TO_PROFILE_TYPE << "},"
            << VIA << '{' << NAME <<',' << ID << "}," << APPLICATION << "{" << LINK << ',' << NAME << ',' << ID << "},"
            << PLACE << '{' << ID << ',' << NAME << ',' << LOCATION
            << ',' << PICTURE << ',' << CATEGORY_LIST << ',' << CHECKINS << "}," << IMPLICIT_PLACE << '{' << NAME << ',' << ID << "},"
            << LIKES;

    if ( likesLimit != UNDEFINED_VALUE ) {
        char limitBuffer[LIMIT_BUFFER_SIZE] = {0};
        Utils::Snprintf_s( limitBuffer, LIMIT_BUFFER_SIZE, "%d", likesLimit );
        request << '.' << LIMIT << '(' << limitBuffer << ')';
    }

    request << '.' << SUMMARY << '(' << ( isLikesSummary ? "true" : "false" ) << ")" << '{' << ID << ',' << NAME << "}," << COMMENTS;

    if ( commentsLimit != UNDEFINED_VALUE ) {
        char limitBuffer[LIMIT_BUFFER_SIZE] = {0};
        Utils::Snprintf_s( limitBuffer, LIMIT_BUFFER_SIZE, "%d", commentsLimit );
        request << '.' << LIMIT << '(' << limitBuffer << ')';
    }

    request << '.' << SUMMARY << '(' << ( isCommentsSummary ? "true" : "false" ) << ")," << ATTACHMENTS << ATTACHMENTS_LIMIT
            << '{' << DESCRIPTION << ',' << MEDIA << ',' << TARGET << '{' << ID << ',' << URL << "}," << TITLE << ',' << TYPE << "," << URL << ',' << DESCRIPTION_TAGS << ','
            << SUBATTACHMENTS << ATTACHMENTS_LIMIT << '{' << MEDIA << ',' << TARGET << ',' << TITLE << ',' << TYPE << ',' << DESCRIPTION << ',' << DESCRIPTION_TAGS << "}}";
}

bool PostProvider::UpdateCacheData(FbRespondGetFeed * feedResponse) {
    return false;
}

unsigned int PostProvider::ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread )
{
    JsonObject *object = NULL;
    JsonParser *parser = openJsonParser( data, &object );
    ParserWrapper wr( parser );
    unsigned int itemsCount = NONE_ITEMS;
    Log::debug("PostProvider::ParseData %d", isDescendingOrder );
    if ( object ) {
        FbRespondGetFeed * postResponse = new FbRespondGetFeed( object );
        if ( postResponse->GetPostsList() ) {
            unsigned int countainerSize = GetLimit() > 0 ? GetLimit() : RESPONSE_CONTAINER_SIZE;
            Eina_Array *parsedItemsArray = eina_array_new( countainerSize );
            if ( parsedItemsArray == NULL ) {
                Log::debug("PostProvider:: cannot allocate a memory for a request array.");
                return NONE_ITEMS;
            }
            Eina_List *list;
            void *data;
            if ( IsDataCached() ) {
                SetDataCached( false, thread );
                CleanData();
            }
            UpdateCacheData(postResponse);
            EINA_LIST_FOREACH( postResponse->GetPostsList(), list, data ) {
                ++itemsCount;
                Post *post = static_cast<Post*>(data);
                if ( post ) {
                    if(!IsContainItem( post )) {
                        const char* parentId = post->GetParentId();
                        if ( parentId ) {
                            Post* parent_post = GetPostById( parentId, GetData() );
                            if(!parent_post) {
                                // parent post was not found in already stored posts
                                // try to find parent in new ones
                                parent_post = GetPostById( parentId, postResponse->GetPostsList() );
                            }
                            if(!parent_post) {
                                parent_post = GetPostById( parentId, parsedItemsArray );
                            }
                            if(!parent_post) {
                                parent_post = GetPostById( parentId, mSourcePosts );
                            }
                            if ( parent_post ) {
                                post->SetParent(parent_post);
                            } else {
                                // we should request parent post  from server
                                PostLoader *postLoader = new PostLoader(post->GetId(), parentId);
                                mPostLoaderList = eina_list_append(mPostLoaderList, postLoader);
                                postLoader->startLoad(this);
                                Log::debug("PostProvider::ParseData->parent post is not found, parentId = %s", parentId);
                            }
                        }
                        eina_array_push( parsedItemsArray, post );
                        postResponse->RemoveDataFromList(post);
                    }
                }
            }
            Log::debug("PostProvider:: there was handled %d items and %d feed parsed.",
                    itemsCount, eina_array_count( parsedItemsArray ) );
            if(eina_array_count( parsedItemsArray ) > 0) {
                if(!isDescendingOrder) {
                    // remove operations
                    RemoveData(GetOperationPresenters());
                }
                AppendData( parsedItemsArray, isDescendingOrder );
                if(!isDescendingOrder) {
                    // add operations
                    AppendData(GetOperationPresenters(), false);
                }
                CachePostsSequence( parsedItemsArray, isDescendingOrder );
            }
            eina_array_free( parsedItemsArray );
        }
        delete postResponse;
    }
    return itemsCount;
}

Post* PostProvider::GetPostById(const char* id, Eina_List * postsList)
{
    if ( id && postsList ) {
        Eina_List *l;
        void *listData;
        EINA_LIST_FOREACH( postsList, l, listData ) {
            Post *post = static_cast<Post*>(listData);
            assert(post);
            if(strcmp(id, post->GetId()) == 0) {
                return post;
            }
        }
    }
    return NULL;
}

Post* PostProvider::GetPostById(const char* id, Eina_Array * postsArray)
{
    if ( id && postsArray ) {
        unsigned int index = 0;
        void* item;
        Eina_Array_Iterator iterator;
        EINA_ARRAY_ITER_NEXT( postsArray, index, item, iterator ) {
            Post *post = static_cast<Post*>(item);
            assert(post);
            if(strcmp(id, post->GetId()) == 0) {
                return post;
            }
        }
    }
    return NULL;
}

Eina_List* PostProvider::GetPostsByAlbumId(const char* albumId, Eina_Array *postsArray) {
    Eina_List *res = nullptr;
    if (albumId && postsArray) {
        unsigned int index = 0;
        void* item;
        Eina_Array_Iterator iterator;
        EINA_ARRAY_ITER_NEXT(postsArray, index, item, iterator) {
            Post *post = static_cast<Post*>(item);
            assert(post);
            if (post->IsPhotoAddedToAlbum()) {
                Album *album = post->GetAlbum();
                if (album) {
                    if(!strcmp(albumId, album->GetId())) {
                        res = eina_list_append(res, post);
                    }
                }
            }
        }
    }
    return res;
}

void PostProvider::UpdateAllChildrenPostsByParent(Post *updatedPost)
{
    assert(updatedPost);
    unsigned int index = 0;
    void* item;
    Eina_Array_Iterator iterator;
    EINA_ARRAY_ITER_NEXT( GetData(), index, item, iterator ) {
        Post *post = static_cast<Post*>(item);
        assert(post);
        if( post->GetParentId() && strcmp (updatedPost->GetId(), post->GetParentId()) == 0) {
            post->SetParent(updatedPost);
// just update all posts where parent has been reloaded
            DataEventDescription dataEvent(this,
                DataEventDescription::eDE_POST_UPDATED,
                post->GetId(),
                IsReverseChronological());
            AppEvents::Get().Notify(eDATA_CHANGES_EVENT, &dataEvent);
        }
    }
}

void PostProvider::UpdateAvatarForAllMyPosts(const char* path)
{
    assert(path);
    unsigned int index = 0;
    void* item;
    Eina_Array_Iterator iterator;
    EINA_ARRAY_ITER_NEXT( GetData(), index, item, iterator ) {
        Post *post = static_cast<Post*>(item);
        assert(post);
        bool eventToUpdate = false;
        if( Utils::IsMe( post->GetFromId().c_str()) ) {
            post->SetFromPicturePath(path);
            eventToUpdate = true;
        }
        if( post->GetParent() && Utils::IsMe( post->GetParent()->GetFromId().c_str()) ) {
            post->GetParent()->SetFromPicturePath(path);
            eventToUpdate = true;
        }
        if(eventToUpdate) {
            assert(post->GetId());
            DataEventDescription dataEvent(this,
                DataEventDescription::eDE_POST_UPDATED,
                post->GetId(),
                IsReverseChronological());
            AppEvents::Get().Notify(eDATA_CHANGES_EVENT, &dataEvent);
        }
    }
}

void PostProvider::DeleteItem( void* item )
{
    if ( !eina_array_remove( GetData(), KeepItemById, item ) ) {
        Log::error("PostProvider::DeleteItem cannot delete the referenced element" );
    }
}

void PostProvider::CachePostsSequence( Eina_Array *data, bool isDescendingOrder ) {

}

unsigned int PostProvider::RetrieveCachedData() {
    return 0;
}

std::list<Order*> PostProvider::RetrievePostIds() {
    Log::debug("PostProvider::RetrievePostIds()" );
    std::list<Order*> list;
    return list;
}

void PostProvider::ReleaseItem( void* item ) {
    BaseObject *baseObject = static_cast<BaseObject*>(item);
    baseObject->Release();
}

void PostProvider::Update(AppEventId eventId, void * data) {
    Log::debug("PostProvider::Update start eventId = %d", eventId );
    switch(eventId){
    case eOPERATION_ADDED_EVENT:
        if (data) {
            Sptr<Operation> oper(static_cast<Operation*>(data));
            if (IsNeedToHandleOperation(oper)) {
                Operation::OperationType operationType = oper->GetType();
                if(  (operationType == Operation::OT_Post_Create)
                   ||(operationType == Operation::OT_Share_Post_Create)
                   ||(operationType == Operation::OT_Photo_Post_Create)
                   ||(operationType == Operation::OT_Video_Post_Create)
                   ||(operationType == Operation::OT_Album_Add_New_Photos))
                {
                    AddOperation(oper);
                }
            }
        }
        break;
    case eOPERATION_REMOVED_EVENT:
        if (data) {
            Sptr<Operation> oper(static_cast<Operation*>(data));
            if (IsNeedToHandleOperation(oper)) {
                Operation::OperationType operationType = oper->GetType();
                if(  (operationType == Operation::OT_Post_Create)
                   ||(operationType == Operation::OT_Share_Post_Create)
                   ||(operationType == Operation::OT_Photo_Post_Create)
                   ||(operationType == Operation::OT_Video_Post_Create)
                   ||(operationType == Operation::OT_Album_Add_New_Photos)){
                    if(oper->GetOperationResult() == Operation::OR_Success ||
                       oper->GetState() == Operation::OS_Canceled
                      ) {
                        RemoveOperation(oper);
                    }
                }
            }
        }
        break;
    case ePOST_RELOAD_EVENT:
        if (data) {
            Post *post = static_cast<Post *>(data);
            if ( ReplaceGraphObjectById(post->GetId(), post, GetData()) ||
                 ReplaceGraphObjectById(post->GetId(), post, mSourcePosts)
                ) {
                if (post->GetParentId()) {
                    Post *parentPost = GetPostById(post->GetParentId());
                    if (parentPost) {
                        post->SetParent(parentPost);
                    }
                }
                DataEventDescription dataEvent(this,
                    DataEventDescription::eDE_POST_UPDATED,
                    post->GetId(),
                    IsReverseChronological());
                AppEvents::Get().Notify(eDATA_CHANGES_EVENT, &dataEvent);

                UpdateAllChildrenPostsByParent(post);
            }
        }
        break;
    case eUPDATE_POST_LIKES_SUMMARY:
        if(data) {
            Post *post = static_cast<Post *>(data);
            Post *foundPost = GetPostById(post->GetId());
            if ( foundPost && post != foundPost ) {// to do not update the same post, only copies in NF

                foundPost->CopyLikesSummary(*post);

                DataEventDescription dataEvent(this,
                     DataEventDescription::eDE_POST_UPDATED,
                     post->GetId(),
                     IsReverseChronological());
                AppEvents::Get().Notify(eDATA_CHANGES_EVENT, &dataEvent);
            }
        }
        break;
    case eUPDATED_PROFILE_PICTURE:
        if(data) {
            const char *path = static_cast<char*>(data);
            UpdateAvatarForAllMyPosts(path);
        }
        break;
    default:
        break;
    }
}

bool PostProvider::IsNeedToHandleOperation(Sptr<Operation> oper) {
    return false;
}

bool PostProvider::IsCachedDataShouldBeSet() {
    bool ret = !mMissCacheRetrieve && AbstractDataProvider::GetDataCount() == 0;
    mMissCacheRetrieve = false;
    return ret;
}
