#include "jsonutilities.h"
#include "ParserWrapper.h"
#include "Common.h"
#include "Photo.h"
#include "FbRespondGetPhotos.h"

#include "PhotoGalleryProvider.h"
#include "PhotoGalleryScreenBase.h"

#include <eina_array.h>
#include <sstream>
#include <dlog.h>
#include <assert.h>

#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "PhotoGallery"

const char *PhotoGalleryProvider::UPLOADED = "?type=uploaded";
const char *PhotoGalleryProvider::TAGGED = "?type=tagged";
const char *PhotoGalleryProvider::PHOTOS_MAIN_REQUEST_PART = "/photos";
const char *PhotoGalleryProvider::PHOTOS_FIELDS_LIST = "name,name_tags,tags{x,y,created_time,id,name,tagging_user},link,height,width,picture,album,from{id},images,likes.limit(2).summary(true),comments.limit(0).summary(true)";

PhotoGalleryProvider::PhotoGalleryProvider() :
    mMode(EUndefinedMode),
    mUserPhotosType(EUndefinedType),
    mId(NULL),
    mUserData(NULL) {

    dlog_print(DLOG_INFO, LOG_TAG, "PhotoGalleryProvider::PhotoGalleryProvider");
    SetPagingAlgorithm(ITERATORS_ALGORITHM);
}

PhotoGalleryProvider::~PhotoGalleryProvider() {
    EraseData();
}

void PhotoGalleryProvider::CustomErase() {
    mMode = EUndefinedMode;
    mUserPhotosType = EUndefinedType;
    delete []mId;
    mId = NULL;
    mUserData = NULL;
}

PhotoGalleryProvider* PhotoGalleryProvider::GetInstance() {
    static PhotoGalleryProvider *photoGalleryProvider = new PhotoGalleryProvider();
    return photoGalleryProvider;
}

void PhotoGalleryProvider::SetAlbumId(const char *id, void* userData) {
    assert(id); //It doesn't make sense to set NULL value
    EraseData();
    if (id) {
// @note each time, when we reassign id, we need to clear prev data.
        mMode = EAlbum;
        delete []mId;
//In case of EAlbum mode mId means AlbumId
        mId = new char[strlen(id) + 1];
        eina_strlcpy(mId, id, strlen(id) + 1);
        mUserData = userData;
    }
}

void PhotoGalleryProvider::SetUserId(const char *id, UserPhotosType photosType, void* userData) {
    assert(id); //It doesn't make sense to set NULL value
    EraseData();
    if (id) {
// @note each time, when we reassign id, we need to clear prev data.
        mMode = EUserPhotos;
        mUserPhotosType = photosType;
        delete []mId;
//In case of EUserPhotos mode mId means UserId
        mId = new char[strlen(id) + 1];
        eina_strlcpy(mId, id, strlen(id) + 1);
        mUserData = userData;
    }
}

UPLOAD_DATA_RESULT PhotoGalleryProvider::UploadData( bool isDescendingOrder ) {
    dlog_print(DLOG_INFO, LOG_TAG, "PhotoGalleryProvider::UploadData");
    if ( eina_array_count( GetData() ) == GetItemsTotalCount() ) {
        // We cannot request more data
        return NONE_DATA_COULD_BE_RESULT;
    }

    std::stringstream request;
    if (mMode == EUserPhotos) {
        const char *type = NULL;
        if (mUserPhotosType == ETagged) {
            type = TAGGED;
        } else if (mUserPhotosType == EUploaded) {
            type = UPLOADED;
        }
        request << mId << PHOTOS_MAIN_REQUEST_PART << (type ? type : "") << (type ? "&" : "?") << FIELDS << PHOTOS_FIELDS_LIST;
    } else if (mMode == EAlbum) {
        request << mId << PHOTOS_MAIN_REQUEST_PART << "?" << FIELDS << PHOTOS_FIELDS_LIST;
    }
    if (!request.str().empty()) {
        dlog_print(DLOG_INFO, LOG_TAG, "SEND:%s", request.str().c_str());
        return RequestData( request.str().c_str(), isDescendingOrder );
    } else {
        dlog_print(DLOG_ERROR, LOG_TAG, "ATTEMPT TO SEND REQUEST WITH WRONG PARAMS");
        return NONE_DATA_COULD_BE_RESULT;
    }
}

unsigned int PhotoGalleryProvider::ParseData(char *data, bool isDescendingOrder, Ecore_Thread *thread) {
    dlog_print(DLOG_INFO, LOG_TAG, "PhotoGalleryProvider::ParseData()" );
    unsigned int photosTotal = 0;
    JsonObject *object = NULL;
    JsonParser *parser = openJsonParser(data, &object);
    ParserWrapper wr(parser);
    if (object) {
        FbRespondGetPhotos * photosResponse = new FbRespondGetPhotos(object);
        if (photosResponse->GetPhotosList()) {
            unsigned int countainerSize = GetLimit() > 0 ? GetLimit() : RESPONSE_CONTAINER_SIZE;
            Eina_Array *parsedItemsArray = eina_array_new( countainerSize );
            if ( parsedItemsArray == NULL ) {
                dlog_print(DLOG_ERROR, LOG_TAG, "PhotoGalleryProvider: cannot allocate a memory for a request array." );
                return NONE_ITEMS;
            }
            photosTotal = eina_list_count(photosResponse->GetPhotosList());
//ToDo: This is a temporary solution. Ideally, DataProvder should not know about screen. It will be redesigned later.
            PhotoGalleryScreenBase::ArrangeItems(photosResponse->GetPhotosList(), parsedItemsArray, mUserData);
            AppendData(parsedItemsArray, isDescendingOrder);
            eina_array_free( parsedItemsArray );
            delete photosResponse;
        }
    }
    return photosTotal;
}

void PhotoGalleryProvider::DeleteItem( void* item ) {
    if (!eina_array_remove(GetData(), KeepItemByPointer, item)) {
        dlog_print( DLOG_DEBUG, LOG_TAG_FACEBOOK, "PhotoGalleryProvider: cannot delete the referenced element" );
    }
}


void PhotoGalleryProvider::ReleaseItem( void* item ) {
    GalleryItem *galleryItem = static_cast<GalleryItem*>(item);
    delete galleryItem;
}
