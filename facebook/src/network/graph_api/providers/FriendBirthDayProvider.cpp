/*
 * FriendBirthDayProvider.cpp
 *
 *  Created on: Oct 30, 2015
 *      Author: ruibezruch
 */
#include <dlog.h>
#include <app.h>
#include <assert.h>

#include "Common.h"
#include "Friend.h"
#include "OwnFriendsProvider.h"
#include "FriendBirthDayProvider.h"
#include "Utils.h"

FriendBirthDayProvider::FriendBirthDayProvider()
{
    // TODO Auto-generated constructor stub

}

FriendBirthDayProvider::~FriendBirthDayProvider()
{
    EraseData();
}

FriendBirthDayProvider* FriendBirthDayProvider::GetInstance()
{
    static FriendBirthDayProvider *friendBdayProvider = new FriendBirthDayProvider();
    return friendBdayProvider;
}

unsigned int FriendBirthDayProvider::ParseData(char *data, bool isDescendingOrder, Ecore_Thread *thread )
{
    return NONE_ITEMS;
}

UPLOAD_DATA_RESULT FriendBirthDayProvider::UploadData(bool isDescendingOrder)
{
    if ( GetDataCount() == 0 ) {
        CreateBDayData(isDescendingOrder);
    }
    return NONE_DATA_COULD_BE_RESULT;
}

void FriendBirthDayProvider::CreateBDayData(bool isDescendingOrder)
{
    int count = OwnFriendsProvider::GetInstance()->GetDataCount();

    Eina_Array *parsedItemsArray = NULL;
    if (count == 0) {
        return;
    }
    parsedItemsArray = eina_array_new(count);
    Eina_List *friends = NULL;
    Eina_List *friendsFuture = NULL;

    for (int i = 0; i < count; ++i) {
        Friend *fr = static_cast<Friend*>(eina_array_data_get(OwnFriendsProvider::GetInstance()->GetData(), i));
        assert(fr);
        if (!fr->mBirthday.empty()) {

            const char *mB1 = strdup(fr->mBirthday.c_str());

            struct tm timeinfo_1;
            strptime(mB1, "%m/%d", &timeinfo_1);

            struct tm currentTime = Utils::GetCurrentDateTime();

            tm timeStruct = {};
            timeStruct.tm_year = currentTime.tm_year;
            timeStruct.tm_mon = timeinfo_1.tm_mon;
            timeStruct.tm_mday = timeinfo_1.tm_mday;
            mktime(&timeStruct);

            free((void *) mB1);

            if (timeStruct.tm_yday >= currentTime.tm_yday) {
                friends = eina_list_append(friends, fr);
            } else {
                fr->isBdayNextYear = true;
                friendsFuture = eina_list_append(friendsFuture, fr);
            }
            fr->AddRef();
        }
    }

    friends = eina_list_sort(friends, 0, sort_cb);

    Eina_List *l;
    void *listData;

    EINA_LIST_FOREACH(friends, l, listData)
    {
        eina_array_push(parsedItemsArray, listData);
    }

    friendsFuture = eina_list_sort(friendsFuture, 0, sort_cb);

    Eina_List *l2;
    void *listData2;

    EINA_LIST_FOREACH(friendsFuture, l2, listData2)
    {
        eina_array_push(parsedItemsArray, listData2);
    }

    AppendData(parsedItemsArray, isDescendingOrder);

    eina_array_free(parsedItemsArray);
    eina_list_free(friends);
    eina_list_free(friendsFuture);
}

int FriendBirthDayProvider::sort_cb(const void *d1, const void *d2)
{
    const Friend *fr1 = static_cast<const Friend *>(d1);
    const Friend *fr2 = static_cast<const Friend *>(d2);
    return strcmp(fr1->mBirthday.c_str(), fr2->mBirthday.c_str());
}

void FriendBirthDayProvider::DeleteItem(void* item)
{
    if (!eina_array_remove(GetData(), AbstractDataProvider::KeepItemById, item)) {
        dlog_print( DLOG_DEBUG, LOG_TAG_FACEBOOK, "FriendBirthDayProvider: cannot delete the referenced element" );
    }
}

void FriendBirthDayProvider::ReleaseItem( void* item ) {
    BaseObject *baseObject = static_cast<BaseObject*>(item);
    baseObject->Release();
}
