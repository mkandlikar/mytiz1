#include "FbRespondGetGroups.h"
#include "Group.h"
#include "GroupProvider.h"
#include "ParserWrapper.h"
#include "Separator.h"
#include "Utils.h"

GroupProvider::GroupProvider()
{
    mTextToFind = nullptr;
    mFilteredData = eina_array_new(BASE_CONTAINER_SIZE);
    SetPagingAlgorithm(CURSOR_ALGORITHM);
    mIds = eina_array_new( RESPONSE_CONTAINER_SIZE);
    if (!mIds) {
        Log::error("GroupProvider: cannot allocate a memory for an array." );
    }
    mIsNeedSeparator = false;
    mGroupsDataRequestSuccessful = false;
}

void GroupProvider::ClearIds() {
    if (mIds) {
        void *item;
        Eina_Array_Iterator iterator;
        unsigned int i;
        EINA_ARRAY_ITER_NEXT(mIds, i, item, iterator) {
            free(item);
        }
        eina_array_clean(mIds);
    }
}

GroupProvider::~GroupProvider()
{
    EraseData();
    eina_array_free(mFilteredData);
    ClearIds();
    eina_array_free(mIds);
}

void GroupProvider::CustomErase()
{
    if(mFilteredData) {
        eina_array_clean(mFilteredData);
    }
    free(mTextToFind); mTextToFind = nullptr;
    Log::debug("GroupProvider::CustomErase");
    ClearIds();
    mGroupsDataRequestSuccessful = false;
}

void GroupProvider::FilterData()
{
    Log::debug("GroupProvider::FilterData");

    if (eina_array_count(mFilteredData) > 0) {
        eina_array_clean(mFilteredData);
    }

    if (mTextToFind) {
        int len = AbstractDataProvider::GetDataCount();
        for (int i = 0; i < len; i++) {
            GraphObject *check = static_cast<GraphObject*>(eina_array_data_get(AbstractDataProvider::GetData(), i));
            if (check->GetGraphObjectType() == GraphObject::GOT_GROUP) {
                Group *group = static_cast<Group*>(check);
                if (group && group->mName) {
                    if (Utils::UAreAllWordsSubstringsOfAnyWord(group->mName, mTextToFind, false)) {
                        eina_array_push(mFilteredData, group);
                    }
                }
            }
        }
    }
}

void GroupProvider::SetFindText(const char *text)
{
    Log::debug("GroupProvider::SetFindText");

    free(mTextToFind);
    mTextToFind = Utils::escape(text);
    FilterData();
}

Eina_Array* GroupProvider::GetData() const
{
    if (!mTextToFind || *mTextToFind == '\0') { //if there is no filter
        Log::debug("GroupProvider::FilterDataFULLRETURN");
        return AbstractDataProvider::GetData();
    } else {
        Log::debug("GroupProvider::FilterDataRETURN");
        return mFilteredData;
    }
}

unsigned int GroupProvider::GetDataCount() const
{
    return eina_array_count(GetData());
}

GroupProvider* GroupProvider::GetInstance()
{
    static GroupProvider *groupProvider = new GroupProvider();
    return groupProvider;
}

unsigned int GroupProvider::ParseData(char *data, bool isDescendingOrder, Ecore_Thread *thread)
{
    JsonObject *object = nullptr;
    JsonParser *parser = openJsonParser(data, &object);
    ParserWrapper wr(parser);
    unsigned int itemsCount = NONE_ITEMS;
    FbRespondGetGroups * groupsRespond = new FbRespondGetGroups(object);
    if (groupsRespond->mGroupsList) {
        unsigned int countainerSize = GetLimit() > 0 ? GetLimit() : RESPONSE_CONTAINER_SIZE;
        Eina_Array *parsedItemsArray = eina_array_new(countainerSize);
        if (parsedItemsArray == nullptr) {
            Log::error("GroupProvider: cannot allocate a memory for a request array.");
            return NONE_ITEMS;
        }
        Eina_List *list;
        void *data;
        EINA_LIST_FOREACH(groupsRespond->mGroupsList, list, data) {
            ++itemsCount;
            Group *group = static_cast<Group*>(data);
            eina_array_push(parsedItemsArray, group);
            if (NO_ITEMS_FOUND == Utils::FindElement((void *)group->GetId(), mIds, GroupProvider::idsComparator)) {
                eina_array_push(mIds, strdup(group->GetId()));
                if(!mIsNeedSeparator && itemsCount > 0){
                    mIsNeedSeparator = true;
                }
            }
            groupsRespond->RemoveDataFromList(group);
        }

        if (IsItLastPage(object) && mIsNeedSeparator) {
            Separator *separator = new Separator(Separator::OTHERS_SEPARATOR);
            eina_array_push(parsedItemsArray, separator);
            mIsNeedSeparator = false;
        }

        Log::debug("GroupProvider: there was handled %d items.", itemsCount);
        AppendData(parsedItemsArray, isDescendingOrder);
        FilterData();
        eina_array_free(parsedItemsArray);
    }
    mGroupsDataRequestSuccessful = true;
    delete groupsRespond;
    return itemsCount;
}

UPLOAD_DATA_RESULT GroupProvider::UploadData(bool isDescendingOrder)
{
    if (eina_array_count(GetData()) == GetItemsTotalCount()) {
        return NONE_DATA_COULD_BE_RESULT;
    }

    return RequestData("me/groups?fields=cover,name,icon,privacy,membership_state,picture", isDescendingOrder);
}

void GroupProvider::DeleteItem(void* item)
{
    if (!eina_array_remove(GetData(), KeepItemById, item)) {
        Log::debug("GroupProvider: cannot delete the referenced element" );
    }
}

Eina_Array* GroupProvider::GetIds() const
{
    return mIds;
}

void GroupProvider::ReleaseItem(void* item) {
    BaseObject *baseObject = static_cast<BaseObject*>(item);
    baseObject->Release();
}

bool GroupProvider::IsGroup(const char * object)
{
    bool res = false;

    if (mIds) {
        for (unsigned int i = 0; i < eina_array_count(mIds); ++i) {
            const void *id = eina_array_data_get(mIds, i);

            if (idsComparator(object, id)) {
                res = true;
                break;
            }
        }
    }
    return res;
}
