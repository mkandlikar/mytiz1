#include "CacheManager.h"
#include "FbRespondGetFriends.h"
#include "FriendsProvider.h"
#include "Friend.h"
#include "Log.h"
#include "ParserWrapper.h"
#include "Utils.h"

const unsigned int FriendsProvider::REQUEST_FRIENDS_LIMIT = 1000;
const unsigned int FriendsProvider::DEFAULT_MUTUAL_FRIENDS_LIMIT = 0;
const char* FriendsProvider::FRIENDS_REQUEST_PART = "me/friends";

FriendsProvider::FriendsProvider(const char *id)
{
    SetLimit( REQUEST_FRIENDS_LIMIT );
    SetMutualFriendsLimit( DEFAULT_MUTUAL_FRIENDS_LIMIT );
    SetPagingAlgorithm(CURSOR_ALGORITHM);
    mTextToFind = nullptr;
    mFilteredData = eina_array_new(BASE_CONTAINER_SIZE);
    mSortedData = eina_array_new(BASE_CONTAINER_SIZE);
    mAlphabeticalOrder = false;
    mId = SAFE_STRDUP(id);
    mProviderType = FRIENDS_PROVIDER;
    mMissCacheRetrieve = false;
    mLastIndex = 0;
}

FriendsProvider::~FriendsProvider()
{
    Log::info("FriendsProvider::~FriendsProvider");
    EraseData();
    free(mId);
    eina_array_free(mFilteredData);
    eina_array_free(mSortedData);
}

void FriendsProvider::FilterData(bool appendToFilteredDataArray)
{
    Log::info("FriendsProvider::FilterData");

    if (!appendToFilteredDataArray) {
        mLastIndex = 0;
        if(eina_array_count(mFilteredData) > 0) {
            Log::info("FriendsProvider::FilterData->clean filtered array");
            eina_array_clean(mFilteredData);
        }
    }

    if (mTextToFind) {
        unsigned int len = AbstractDataProvider::GetDataCount();
        for (; mLastIndex < len; mLastIndex++)
        {
            Friend *friends = static_cast<Friend*>(eina_array_data_get(AbstractDataProvider::GetData(), mLastIndex));

            if (friends) {
                if (Utils::UAreAllWordsSubstringsOfAnyWord(friends->mName.c_str(), mTextToFind, false)) {
                    eina_array_push(mFilteredData, friends);
                }
            }
        }
    }
    if(!appendToFilteredDataArray) {
        mFilteredData = SortFriendsArray(mFilteredData);
    }
}

void FriendsProvider::SortData()
{
    Log::info("FriendsProvider::SortData");

    int count = AbstractDataProvider::GetDataCount();

    if (eina_array_count(mSortedData) > 0) {
        Log::info("FriendsProvider::SortDataFREE");
        eina_array_clean(mSortedData);
    }

    for (int i = 0; i < count; ++i) {
        eina_array_push(mSortedData, eina_array_data_get(AbstractDataProvider::GetData(), i));
    }
    mSortedData = SortFriendsArray(mSortedData);
}

void FriendsProvider::SetFindText(const char *text)
{
    Log::info("FriendsProvider::SetFindText");

    free(mTextToFind);
    mTextToFind = SAFE_STRDUP(text);
    FilterData();
}

void FriendsProvider::MissCacheRetrieve( bool miss )
{
    mMissCacheRetrieve = miss;
}

void FriendsProvider::ResetFilter() {
    SetFindText("");
}

void FriendsProvider::AppendToFilteredDataArray() {
   if(mTextToFind && (*mTextToFind != '\0')) {
       FilterData(true);//if data is loading with not empty applied filter
   }
}

void FriendsProvider::SetAlphabeticalOrder(bool isAlphabetical)
{
    Log::info("FriendsProvider::SetAlphabeticalOrder");

    mAlphabeticalOrder = isAlphabetical;

    //if (mAlphabeticalOrder) {
    //    SortData();
    //}
}

Eina_Array* FriendsProvider::GetData() const
{
    if (mTextToFind && *mTextToFind) { //if there is a filter
        return mFilteredData;
    } else if(mAlphabeticalOrder){
        return mSortedData;
    } else {
        return AbstractDataProvider::GetData();
    }
}

unsigned int FriendsProvider::GetDataCount() const
{
    return eina_array_count(GetData());
}

void FriendsProvider::CustomErase()
{
    Log::info("FriendsProvider::CustomErase");

    if (mFilteredData) {
        eina_array_clean(mFilteredData);
    }
    if (mSortedData) {
        eina_array_clean(mSortedData);
    }
    mAlphabeticalOrder = false;
    free(mTextToFind);
    mTextToFind = nullptr;
}

void FriendsProvider::SetMutualFriendsLimit( unsigned int limit )
{
    m_MutualFriendsLimit = limit;
}

void FriendsProvider::DeleteItem( void* item )
{
    if ( !eina_array_remove( AbstractDataProvider::GetData(), KeepItemById, item )) {
        Log::error("FriendsProvider::DeleteItem() - cannot delete the referenced element");
    } else {
        if ( !eina_array_remove( mFilteredData, KeepItemById, item ) ) {
            Log::error("FriendsProvider::DeleteItem() - cannot delete the referenced element from filtered data");
        }

        if ( !eina_array_remove( mSortedData, KeepItemById, item ) ) {
            Log::error("FriendsProvider::DeleteItem() - cannot delete the referenced element from sorted data");
        }

        const char *id = (static_cast<GraphObject*>( item ))->GetId();
        CacheManager::GetInstance().DeleteFriend( id );
        ReleaseItem( item );
    }
}

Eina_Bool FriendsProvider::friends_array_comparator(const void *container, void *data, void *fdata)
{
    Log::warning("FriendsProvider::friends_array_comparator");
    Friend * user = static_cast<Friend *>(data);
    char* requestedUserId = static_cast<char*>(fdata);
    if(strcmp(user->GetId(),requestedUserId) == 0 ){
        Log::info("FriendsProvider::friends_array_comparator requested userId %s is presented in array", requestedUserId);
        return false;
    }
    return true;
}

bool FriendsProvider::IsFriendInFriendsArray(const char* userId)
{
    return !eina_array_foreach ( AbstractDataProvider::GetData(), friends_array_comparator, const_cast<char*>(userId) );
}

void FriendsProvider::AddItem(User *item)
{
    Log::info("FriendsProvider::AddItem");

    if (item) {

        if (IsFriendInFriendsArray(item->GetId())) return;

        Friend *newFriend = new Friend();
        newFriend->mName = item->mName ? item->mName : "";
        newFriend->mFirstName = item->mFirstName ? item->mFirstName : "";
        newFriend->mLastName = item->mLastName ? item->mLastName : "";
        newFriend->SetId(SAFE_STRDUP(item->GetId()));
        newFriend->mMutualFriendsCount = item->mMutualFriendsCount;
        newFriend->mPicturePath = item->mPicture ? ( item->mPicture->mUrl ? item->mPicture->mUrl : "") : "";
        newFriend->mFriendshipStatus = item->mFriendshipStatus;

        CacheManager::GetInstance().AddFriend(newFriend);
        eina_array_push(AbstractDataProvider::GetData(), newFriend);
        SortData();

    } else {
        Log::error("FriendsProvider::AddItem - user is not valid");
    }
}

UPLOAD_DATA_RESULT FriendsProvider::UploadData( bool isDescendingOrder )
{
    if ( GetDataCount() == GetItemsTotalCount() ) {
        //
        // We cannot request more data
        return NONE_DATA_COULD_BE_RESULT;
    }

    std::stringstream request;

    request << mId << "/friends" << "?" << FIELDS;

#ifdef FEATURE_MUTUAL_FRIENDS
    request << ID << ',' << NAME << ',' <<  FIRST_NAME << ',' << LAST_NAME << ',' << BIRTHDAY << ',' << "friendship_status" << ',' << PICTURE << ".width(200).height(200)"
        << ',' << CONTEXT << '{' << MUTUAL_FRIENDS << '.' << LIMIT << '(' << m_MutualFriendsLimit << ")}";
#else
    request << ID << ',' << NAME << ',' <<  FIRST_NAME << ',' << LAST_NAME << ',' << BIRTHDAY << ',' << "friendship_status" << ',' << PICTURE << ".width(200).height(200)";
#endif

    return RequestData( request.str().c_str(), isDescendingOrder );
}

unsigned int FriendsProvider::ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread )
{
    JsonObject *object = nullptr;
    JsonParser *parser = openJsonParser( data, &object );
    ParserWrapper wr( parser );
    unsigned int itemsCount = NONE_ITEMS;
    if ( object ) {
        FbRespondGetFriends *friendsRespond = new FbRespondGetFriends(object);
        if ( friendsRespond->mFriendsList ) {
            unsigned int countainerSize = GetLimit() > 0 ? GetLimit() : RESPONSE_CONTAINER_SIZE;
            Eina_Array *parsedItemsArray = eina_array_new( countainerSize );
            if ( !parsedItemsArray) {
                Log::error("FriendsProvider: cannot allocate a memory for a request array.");
                return NONE_ITEMS;
            }
            Eina_List *list;
            void *data;
            EINA_LIST_FOREACH( friendsRespond->mFriendsList, list, data ) {
                ++itemsCount;
                Friend *fr = static_cast<Friend*>(data);
                if (fr) {
                    eina_array_push(parsedItemsArray, fr);
                    friendsRespond->RemoveDataFromList(fr);
                }
            }
            Log::debug("FriendsProvider: there was handled %d items and %d friends parsed.", itemsCount, eina_array_count(parsedItemsArray));
            AppendData(parsedItemsArray, isDescendingOrder);
            AppendToFilteredDataArray();
            eina_array_free(parsedItemsArray);
        }
        delete friendsRespond;
    }
    return itemsCount;
}

void FriendsProvider::ReleaseItem( void* item )
{
    BaseObject *baseObject = static_cast<BaseObject*>(item);
    baseObject->Release();
}

Eina_Array * FriendsProvider::SortFriendsArray( Eina_Array * friendsArray){

    for (int i = 1; i < eina_array_count(friendsArray); ++i) {
        for (int j = i; j > 0; --j) {
            Friend *friend_1 = static_cast<Friend*>(eina_array_data_get(friendsArray, j));
            Friend *friend_2 = static_cast<Friend*>(eina_array_data_get(friendsArray, j - 1));
            if (friend_1 && friend_2) {
                if (Utils::UCompare(friend_1->GetName().c_str(), friend_2->GetName().c_str(), true) < 0) {
                    eina_array_data_set(friendsArray, j - 1, friend_1);
                    eina_array_data_set(friendsArray, j, friend_2);
                }
            }
        }
    }
    return friendsArray;
}

Friend *FriendsProvider::GetFriendById(const char *id) {
    assert(id);

    Friend * res = nullptr;
    Eina_Array *friends = GetData();
    int friendsCount = eina_array_count(friends);

    for (int i = 0; i < friendsCount; ++i) {
        Friend *fr = static_cast<Friend*>(eina_array_data_get(friends, i));
        assert(fr && fr->GetId());
        if (!strcmp(id, fr->GetId())) {
            res = fr;
            break;
        }
    }
    return res;
}

