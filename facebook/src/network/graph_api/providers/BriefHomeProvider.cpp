#include "BriefHomeProvider.h"
#include "Log.h"
#include <sstream>
#include <memory>
#include "Common.h"
#include "jsonutilities.h"
#include "ParserWrapper.h"
#include "FbRespondBriefPostInfo.h"
#include "BriefPostInfo.h"
#include "HomeProvider.h"

unsigned short BriefHomeProvider::MAX_REQUEST_ITEMS = 50;
unsigned short BriefHomeProvider::IDS_CONTAINER_SIZE_STEP = 256;

BriefHomeProvider::BriefHomeProvider()
{
    m_Ids = eina_array_new( IDS_CONTAINER_SIZE_STEP );
    m_IdLastIndex = m_HandledItems = 0;
    SetPagingAlgorithm( MANUAL_ALGORITHM );
    SetRequestTimerEnabled( false );
}

BriefHomeProvider::~BriefHomeProvider()
{
    EraseData();
    eina_array_free( m_Ids );
}

BriefHomeProvider* BriefHomeProvider::GetInstance()
{
    static BriefHomeProvider *briefProvider = new BriefHomeProvider();
    return briefProvider;
}

void BriefHomeProvider::CustomErase()
{
    unsigned int index = 0;
    void* item;
    Eina_Array_Iterator iterator;
    EINA_ARRAY_ITER_NEXT( m_Ids, index, item, iterator ) {
        free( item );
    }
    eina_array_clean( m_Ids );
    m_IdLastIndex = m_HandledItems = 0;
}

bool BriefHomeProvider::SetIds( const Eina_Array *ids )
{
    if (eina_array_count( ids ) == 0 ) {
        //
        // @IMPROVEMENT Need to add hash calculation of the entire array. If previous array hash
        //              is equal to current hash value, the ids are the same and we must not to
        //              re-request data for these ids.
        //
        return false;
    }
    EraseData();
    unsigned int index = 0;
    void* item;
    Eina_Array_Iterator iterator;
    EINA_ARRAY_ITER_NEXT( ids, index, item, iterator ) {
        if ( !eina_array_push( m_Ids, item) ) {
            Log::error( "BriefHomeProvider::SetIds, cannot push id [ %s ]", static_cast<char*>(item) );
        }
    }
    FillManualPagination();
    return true;
}

unsigned int BriefHomeProvider::ParseData( char *data, bool isDescendingOrder, Ecore_Thread *thread )
{
    unsigned int items = NONE_ITEMS;
    JsonObject *object = NULL;
    JsonParser *parser = openJsonParser( data, &object );
    ParserWrapper wr( parser );
    JsonNode* rootNode = json_parser_get_root( parser );
    if ( object ) {
        std::unique_ptr<FbRespondBriefPostInfo> briefInfo( new FbRespondBriefPostInfo( rootNode ) );
        if ( briefInfo->GetBriefPostInfoList() ) {
            unsigned int countainerSize = GetLimit() > 0 ? GetLimit() : RESPONSE_CONTAINER_SIZE;
            Eina_Array *parsedItemsArray = eina_array_new( countainerSize );
            if ( parsedItemsArray == NULL ) {
                Log::error( "BriefHomeProvider::ParseData cannot allocate a memory for a request array." );
                return NONE_ITEMS;
            }
            Eina_List *list;
            void *data;
            EINA_LIST_FOREACH( briefInfo->GetBriefPostInfoList(), list, data ) {
                ++items;
                BriefPostInfo *info = static_cast<BriefPostInfo*>( data );
                Post *post = HomeProvider::GetInstance()->GetPostById( info->GetId() );
                if ( IsPostChanged( post, info ) ) {
                    if ( !eina_array_push( parsedItemsArray, info ) ) {
                        Log::error( "BriefHomeProvider::ParseData, cannot push new brief info" );
                    } else {
                        Log::info( "BriefHomeProvider::ParseData, id [ %s ], comm_count [ %d ], likes_count [ %d ]",
                                info->GetId(), info->GetCommentsCount(), info->GetLikesCount() );
                        post->BriefUpdate(info->GetUpdatedTime(), info->GetCommentsCount(), info->GetLikesCount(),
                                info->GetHasLiked(), info->GetLikeFromId(), info->GetLikeFromName());
                        briefInfo->RemoveDataFromList( info );
                    }
                }
            }
            if ( eina_array_count( parsedItemsArray ) == 0 ) {
                SwitchContinueRequest();
            } else {
                AppendData( parsedItemsArray, isDescendingOrder );
            }
            eina_array_free( parsedItemsArray );
        }
    }
    m_HandledItems += items;
    return items;
}

UPLOAD_DATA_RESULT BriefHomeProvider::UploadData( bool isDescendingOrder )
{
    Log::info( "BriefHomeProvider::UploadData(), isDescendingOrder [ %s ]", (isDescendingOrder ? "true" : "false") );
    int idsCount = eina_array_count( m_Ids );
    if ( m_HandledItems >= idsCount ) {
        Log::info( "BriefHomeProvider::UploadData(), the data is over" );
        return NONE_DATA_COULD_BE_RESULT;
    }
    std::stringstream request;
    request << '?' << FIELDS << ID << ','<< UPDATED_TIME << ','
            << LIKES << '.' << LIMIT << "(2)" << '.' << SUMMARY << "(true)" << '{' << ID << ',' << NAME << "},"
            << COMMENTS << '.' << LIMIT << "(1)" << '.' << SUMMARY << "(true)";

    return RequestData( request.str().c_str(), isDescendingOrder );
}

std::string BriefHomeProvider::PrepareIds( bool isDescendingOrder )
{
    UNUSED( isDescendingOrder );

    unsigned int idsCount = eina_array_count( m_Ids );
    std::stringstream ids;
    for ( unsigned short step = 0; ( m_IdLastIndex < idsCount && step <= MAX_REQUEST_ITEMS ); ++step, ++m_IdLastIndex ) {
        if ( !ids.str().empty() ) {
            ids << ',';
        }
        ids << static_cast<const char*>(eina_array_data_get( m_Ids, m_IdLastIndex ));
    }
    return ids.str();
}

void BriefHomeProvider::DeleteItem( void *item )
{
    if ( !eina_array_remove(GetData(), KeepItemByPointer, item) ) {
        Log::error( "BriefHomeProvider: cannot delete the referenced element [ %s ]", static_cast<char*>( item ) );
    }
}

void BriefHomeProvider::ReleaseItem( void* item ) {
    BaseObject *baseObject = static_cast<BaseObject*>(item);
    baseObject->Release();
}

void BriefHomeProvider::FillManualPagination( bool isDescendigOrder )
{
    std::string ids = PrepareIds( isDescendigOrder );
    //
    // We are not interesting in the descending/ascending order.
    // Just go forward through the ids chain.
    free( m_Pagination.tail );
    m_Pagination.tail = NULL;
    if( ids.length() ) {
        m_Pagination.tail = strdup( ids.c_str() );
    }
}

bool BriefHomeProvider::IsPostChanged( Post *post, BriefPostInfo *info )
{
    //
    // We must not update info for Operations
    if ( post && post->GetGraphObjectType() != GraphObject::GOT_OPERATION_PRESENTER ) {
#ifdef TEST_CHANGED_POSTS_LOGS
        Log::info( "Stored post: id [ %s ], time [ %f ], coms [ %d ], likes [ %d ]",
                post->GetId(), post->GetUpdatedTime(), post->GetCommentsCount(), post->GetLikesCount() );
        Log::info( "Brief info: id [ %s ], time [ %f ], coms [ %d ], likes [ %d ]",
                info->GetId(), info->GetUpdatedTime(), info->GetCommentsCount(), info->GetLikesCount() );
#endif
        return (post->GetCommentsCount() != info->GetCommentsCount() ||
                post->GetLikesCount() != info->GetLikesCount() ||
                post->GetHasLiked() != info->GetHasLiked() ||
                (post->GetLikeFromId() && info->GetLikeFromId() && strcmp(post->GetLikeFromId(), info->GetLikeFromId())) ||
                (post->GetLikeFromName() && info->GetLikeFromName() && strcmp(post->GetLikeFromName(), info->GetLikeFromName())));
    }
    return false;
}

bool BriefHomeProvider::GetPagingAttributes( std::stringstream &finalRequest, bool isDescendingOrder, bool isExtraParams )
{
    bool result = false;
    if (m_Pagination.tail) {
        result = AbstractDataProvider::GetPagingAttributes(finalRequest, isDescendingOrder, isExtraParams);
    }
    return result;
}
