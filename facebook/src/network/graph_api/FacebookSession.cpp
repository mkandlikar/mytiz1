#include "Common.h"
#include "Config.h"
#include "curlutilities.h"
#include "FacebookSession.h"
#include "GraphRequest.h"
#include "HomeProvider.h"
#include "Log.h"
#include "MutexLocker.h"
#include "PostComposerMediaData.h"
#include "Utils.h"

#include <assert.h>
#include <curl/easy.h>
#include <ewk_view.h>
#include <sstream>
#include <system_info.h>


// Add permissions for access token here in scope field
#define URL_LOGIN "https://www.facebook.com/dialog/oauth?client_id=775504625879081&display=popup&redirect_uri=https://www.facebook.com/connect/login_success.html&response_type=token&scope=public_profile,email,user_friends,read_stream"
#define URL_GRAPH "https://graph.facebook.com"
#define PARAM_DELIMITER "?"
#define ACCESS_TOKEN_PARAM "access_token="

#ifdef FEATURE_MUTUAL_FRIENDS
#define REQUEST_MY_FRIENDS "/me/friends?fields=id,name,picture,context{mutual_friends.limit(0)}"
#else
#define REQUEST_MY_FRIENDS "/me/friends?fields=id,name,picture"
#endif

#define REQUEST_ACTIONS_AVAILABLE "?fields=actions"
#define REQUEST_LIKES "/likes?fields=picture,name"
#define REQUEST_LIKES_SUMMARY "?fields=likes.limit(2).summary(true)"
#define REQUEST_COMMENTS "/comments"
#define REQUEST_GET_COMMENTS "/comments?order=reverse_chronological&filter=toplevel&summary=1&limit=10&fields="
#define REQUEST_GET_COMMENTS_FIELDS "can_comment,can_like,can_remove,message,message_tags,from{id,name,picture},id,user_likes,created_time,comment_count,attachment,like_count,is_hidden"
#define REQUEST_GET_COMMENTS_REPLIES_FORMAT ",comments.limit(1).summary(1).order(reverse_chronological){%s}"
#define REQUEST_FRIEND_DETAILS "?fields=work,education,cover,picture"
#define REQUEST_SHARE "/sharedposts"
#define REQUEST_EVENT_DETAILS "?fields=start_time,end_time,place,cover,owner{id}"
#define REQUEST_IMAGE_DETAILS "/%s?fields=likes.limit(2).summary(true),comments.limit(0).summary(true),name,tags{x,y,created_time,id,name,tagging_user},name_tags,from{id},height,width,can_tag"
#define REQUEST_INFO "?id="
#define REQUEST_FEED "feed"
#define REQUEST_POST_SET_PRIVACY_SETTING "/me/privacy_setting"
#define REQUEST_POST_PRIVACY_OPTIONS "/me/privacy_options"
#define REQUEST_INCOMING_FRIEND_REQUESTS "/me/friendrequests?fields=from{name,id,picture}"
#define REQUEST_INCOMING_FRIEND_REQUESTS_COUNTS "/me/friendrequests?limit=0"
#define REQUEST_SEEN_FRIEND_REQUESTS "/me/friendrequests?seen=true"
#define REQUEST_FRIENDS "/me/friends?uid="
#define REQUEST_FRIENDLISTS "/me/friendlists"
#define REQUEST_EDIT_MEMBERS "/members?member="
#define REQUEST_SEARCH "/method/ubersearch.get?format=json&query=%s&filter=%s&limit=50"
#define REQUEST_PHOTOS "photos"
#define REQUEST_VIDEOS "videos"
#define REQUEST_CONFIRM_FRIEND_REQUEST "/method/friends.confirm?v=1.0&format=JSON&uid=%s&confirm=%d"
#define REQUEST_PEOPLE_YOU_MAY_KNOW "/method/friendfinder.pymk?format=JSON&flow=%s&pic_size=%d"
#define REQUEST_CANCEL_OUTGOING_REQUEST "/method/friends.cancel.request?format=json&uid=%s"
#define REQUEST_PRIVACY_OPTIONS "/me/privacy_options?fields=id,description,type,icon_src,is_currently_selected"
#define REQUEST_PROF_COVER_PIC "fields=id,name,first_name,last_name,friendship_status,subscribe_status,cover,picture,work{employer{id,picture.width(200)}},albums{id,name,type,cover_photo{id,likes.summary(1),comments.summary(1)}}"
#define REQUEST_PROF_FRIEND_PIC "/friends?fields=id,name,picture.width(192).height(128)&limit=1"
#define REQUEST_PROF_FIRSTPHOTO_PIC "/photos?type=uploaded&fields=id,images&limit=1"
#define REQUEST_PROF_PICTUREID "/albums?fields=type,cover_photo{id,likes.summary(1),comments.summary(1)}"
#define REQUEST_PROF_BLOCKEDLIST "/me/blocked?fields=name,block_time,block_type&limit=50"
#define REQUEST_PROF_BLOCKUNBLOCK "/me/blocked?uid={%s}"
#define REQUEST_PROF_POKEUSER "/%s/pokes"
#define REQUEST_PROF_NAME "/me?fields=name"
#define REQUEST_PROF_FULL_PICTURE "fields=picture.width(720)"
#define REQUEST_UNSEEN_NOTIF_COUNT "/me/notifications?limit=0&summary=1"
#define REQUEST_POST_UNSEEN_NOTIF_COUNT "/me/notifications?seen=true"
#define REQUEST_PHOTO_DETAILS "?fields=name,link,height,width,picture,album,from,images,likes.limit(2).summary(true),comments.limit(0).summary(true)"
#define REQUEST_UP_ABOUT_PIC "/me?fields=work{employer{id,picture}}"
#define REQUEST_UP_ABOUT_OVERVIEW "fields=work,education,location,hometown,relationship_status"
#define REQUEST_UP_PLACES_OVERVIEW "/tagged_places?limit=2&fields=id,name,created_time,place{id,name,location{city,country},picture}"
#define REQUEST_UPAM_MORE "fields=name,work{employer{id,name,picture},location,position,start_date,end_date}"
#define REQUEST_PROFILE_ACTIVITY_FEED "/feed?filter=nf&limit=20&fields=id,from{name,id,picture},created_time,actions,caption,description,full_picture,link,message,message_tags,name,object_id,privacy,source,status_type,story,type,updated_time,likes.limit(1).summary(true),comments.limit(1).summary(true)"
#define REQUEST_UP_PHOTOS "/photos/tagged?fields=id,picture,name,height,width,images"
#define REQUEST_EVENTS "/me/events"
#define REQUEST_EVENTS_SAVES "/me/events.saves"
#define REQUEST_EVENTS_SINCE "/me/events?since="
#define REQUEST_EVENTS_UNTIL "/me/events?until="
#define REQUEST_EVENT_LAST_POST "/%s/feed?limit=1"
#define REQUEST_INVITES_EVENTS "/me/events/not_replied"
#define REQUEST_INVITED "/invited?"
#define REQUEST_SEARCH_EVENTS "/search/?type=event%26q="
#define REQUEST_FIELDS "?fields="
#define REQUEST_GROUPS "/me/groups"
#define REQUEST_GROUPS_DETAILS "?fields=cover,name,privacy"
#define REQUEST_ALBUMS "/me/albums"
#define REQUEST_ALBUM_DETAILS "/%s?fields=id,name,privacy,type,description"
#define REQUEST_ALBUM_DETAILS_FOR_PHOTO "/%s?fields=album{type,name,privacy,description,from,created_time,count,can_upload,location}" //this requesets album's details by given photo id
#define REQUEST_MOBILE_ZERO_CAMPAIN "method/mobile.zero.campaign"
#define REQUEST_FETCH_NOTIFICATIONS "/me/notifications?include_read=true&fields=created_time,from{id,name,picture.width(200).height(200)},id,object,title,to,unread,updated_time,link&limit=25"
#define REQUEST_EVENT_PROFILE "/%s?fields=id,name,guest_list_enabled,cover,description,place,start_time,declined_count,noreply_count,end_time,owner{id,name,picture.width(200).height(200)},attending_count,maybe_count,can_guests_invite,type,ticket_uri"
#define REQUEST_SPECIFIC_STORY "/%s?fields=id,created_time,actions,caption,description,full_picture,link,message,message_tags,name,object_id,parent_id,privacy,source,status_type,story,story_tags,type,updated_time,from{name,id,picture},place{id,name,location,picture,category_list},likes.summary(true){id,name},comments.summary(true),attachments"
#define REQUEST_OBJECT_TYPE "/%s?metadata=1&fields=metadata.fields(type),name,id,link"
#define REQUEST_OBJECT_TYPE_WITHOUT_LINK "/%s?metadata=1&fields=metadata.fields(type),name,id"
#define REQUEST_NOTIF_IMAGE_DETAILS "/%s?fields=source,height,width,likes.limit(0).summary(true),comments.limit(0).summary(true),name,id"
#define REQUEST_POST_NOTIF_READ "/%s?unread=false"
#define REQUEST_GET_POST_DETAILS "/%s?fields=id,created_time,edit_actions{text,edit_time},actions,caption,description,full_picture,link,message,message_tags,name,object_id,parent_id,privacy,source,status_type,story,story_tags,type,with_tags,updated_time,from{name,id,picture},to{name,id,profile_type},via{name,id},application{link,name,id},place{id,name,location,picture,category_list},likes.limit(2).summary(true){id,name},comments.limit(1).summary(true),attachments.limit(30){media,target{id,url},type,subattachments.limit(30){media,target,type,description,description_tags},description,description_tags,title}"
#define REQUEST_GET_PHOTO_DETAILS "/%s?fields=album,created_time,from{name,picture},height,width,icon,id,images,link,likes.limit(1).summary(true){id,name},comments.limit(1).summary(true),tags{created_time,id,name,tagging_user,x,y},can_tag"
#define REQUEST_GET_USER_DETAILS "/%s?fields=friendship_status,name,id,picture,last_name,first_name,context"
#define REQUEST_GATEKEEPER_CONFIG "/method/mobile.gatekeepers"

//in batch request '&' is changed to '%26' symbol, because it should be url-encoded
#ifdef FEATURE_MUTUAL_FRIENDS
#define BATCH_REQUEST_FRIENDS_REQUEST "[{'method':'GET','name':'r1','relative_url':'v1.0/me/friendrequests', 'omit_response_on_success': false},{'method':'GET','name':'r2','relative_url':'v2.4/?fields=id,name,picture,context.fields(mutual_friends.limit(0))%26ids={result=r1:$.data.*.from.id}'}]"
#else
#define BATCH_REQUEST_FRIENDS_REQUEST "[{'method':'GET','name':'r1','relative_url':'v1.0/me/friendrequests', 'omit_response_on_success': false},{'method':'GET','name':'r2','relative_url':'v2.4/?fields=id,name,picture%26ids={result=r1:$.data.*.from.id}'}]"
#endif

#define BATCH_REQUEST_START_EVENTS_REQUEST "[{'method':'GET','name':'r1','relative_url':'v2.4"
#define BATCH_REQUEST_END_EVENTS_REQUEST "', 'omit_response_on_success': false},{'method':'GET','name':'r2','relative_url':'v2.4/?fields=id,name,cover,description,place,start_time%26ids={result=r1:$.data.*.id}'}]"

#define BATCH_REQUEST_EVENTS_SAVES_REQUEST "[{'method':'GET','name':'r1','relative_url':'v2.4/me/events.saves', 'omit_response_on_success': false},{'method':'GET','name':'r2','relative_url':'v2.4/?fields=id,name,cover,description,place,start_time%26ids={result=r1:$.data.*.data.event.id}'}]"
#define BATCH_REQUEST_INVITES_EVENTS_REQUEST "[{'method':'GET','name':'r1','relative_url':'v2.4/me/events/not_replied', 'omit_response_on_success': false},{'method':'GET','name':'r2','relative_url':'v2.4/?fields=id,name,cover,description,place,start_time%26ids={result=r1:$.data.*.id}'}]"
#define BATCH_REQUEST_HOSTING_EVENTS_REQUEST "[{'method':'GET','name':'r1','relative_url':'v2.4/me/events/created', 'omit_response_on_success': false},{'method':'GET','name':'r2','relative_url':'v2.4/?fields=id,name,cover,description,place,start_time%26ids={result=r1:$.data.*.id}'}]"

#define BATCH_REQUEST_GET_POST_DETAILS_REQUEST "[{'method':'GET','name':'r1','relative_url':'v2.4/me/home?limit=%d%s"
#define END_BATCH_REQUEST_GET_POST_DETAILS "', 'omit_response_on_success': false},{'method':'GET','name':'r2','relative_url':'v2.4/?fields=id,created_time,edit_actions{text,edit_time},actions,caption,description,full_picture,link,message,message_tags,name,object_id,parent_id,privacy,source,status_type,story,story_tags,type,updated_time,from{name,id,picture},to,via{name,id},application{link,name,id},place{id,name,location,picture,category_list},likes.limit(1).summary(true){id,name},comments.limit(1).summary(true),attachments.limit(30){media,subattachments.limit(30){media,target,description,description_tags},description,description_tags}%26ids={result=r1:$.data.*.id}'}]"

#define REQUEST_BIRTHDAY_EVENTS "/me/friends?limit=3&fields=id,name,picture,birthday"
#define REQUEST_GET_EVENT_ACTIONS_COUNT "?fields=attending_count,maybe_count,noreply_count,declined_count"

#define REQUEST_GET_GROUPS_LIST "/me/groups?fields=name,icon"
#define REQUEST_GROUP_ACTION "/%s/members?member=%s"

#define REQUEST_MY_AVATAR "/me?fields=id,name,picture.width(%d).height(%d)"

#define REQUEST_IS_OBJECT_EXISTS "/%s?fields=id"

#define MAX_PRIVACY_LEN 256

#define REQUEST_POST_PHOTO_GRAPH_VERSION "v2.4/"

#define REST_APP_KEY_KEY "api_key"
#define REST_COORDS_KEY "coords"
#define REST_FORMAT_KEY "format"
#define REST_FORMAT_JSON "JSON"
#define REST_METHOD_KEY "method"
#define REST_METHOD_SET_LAST_LOCATION "places.setLastLocation"
#define REST_SESSION_KEY_KEY "session_key"
#define REST_UID_KEY "uid"
#define REST_VERSION_KEY "v"
#define REST_VERSION "1.0"
#define REST_SIG_KEY "sig"
#define REST_COORDS_FORMAT "{\"latitude\":%07.4f,\"longitude\":%07.4f,\"timestamp\":%ld}"
#define RESTSERVER_URL "/restserver.php"


Eina_Lock FacebookSession::m_Mutex;
bool FacebookSession::mIsSessionClosed = true;

const char* FacebookSession::REQUEST_USER_BASE_INFO = "?fields=name,id,first_name,last_name,friendship_status,subscribe_status,cover,picture.width(720),work{employer{id,picture.width(200)}}";

FacebookSession * FacebookSession::GetInstance()
{
    static FacebookSession *facebookSession = new FacebookSession();
    return facebookSession;
}

/////////////////////////////////////////////////////////////////////////////////////
//  Construction region
/////////////////////////////////////////////////////////////////////////////////////
FacebookSession::FacebookSession():mGraphRequestList(NULL)
{
    mIsSessionClosed = false;
    mLoginCallback = NULL;
    mLoginCallbackObject = NULL;
    eina_lock_new( &m_Mutex );
}

FacebookSession::~FacebookSession()
{
    DeleteAllGraphRequests();
    eina_lock_free( &m_Mutex );
    mIsSessionClosed = true;
}

/////////////////////////////////////////////////////////////////////////////////////
//  API region
/////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief Destroy GraphRequest object
 * @param[in] objID - reference on GraphRequest object ID
 * @param[in] waitForComplete - indicate that request should complete
 * @return FBE_NONE if object is destroyed without error, otherwise error code
 */
FacebookSession::FacebookSessionError FacebookSession::ReleaseGraphRequest(GraphRequest* &objID, bool waitForComplete) {
    Log::debug("FacebookSession::ReleaseGraphRequest start");
    FacebookSessionError errorCode = FBE_OBJECT_IS_NULL;
    if(objID) {
        MutexLocker locker( &m_Mutex );
        // we have to find request
        GraphRequest* obj = FacebookSession::GetInstance()->GetGraphRequestById(objID);
        if(obj) {
            obj->UnSubscribeAll();
            obj->RemoveCallbacks();
            errorCode = RemoveGraphRequest(obj);

            if((obj->GetStatus() == GraphRequest::GRS_IN_PROGRESS) && (!waitForComplete)) {
                obj->Cancel();
            }
            obj->Release();
            objID = NULL;
        } else {
            errorCode = FBE_OBJECT_NOT_FOUND;
        }
    }
    Log::debug("FacebookSession::ReleaseGraphRequest end");
    return errorCode;
}

/**
 * @brief Add GraphRequest object to internal list
 * @param obj - GraphRequest object
 * @return error code FBE_NONE if operation done without error, otherwise error code
 */
FacebookSession::FacebookSessionError FacebookSession::AddGraphRequest(GraphRequest *obj) {
    FacebookSessionError errorCode = FBE_NONE;
    if(obj) {
        if ( eina_list_data_find( mGraphRequestList, obj ) == NULL ) {
            mGraphRequestList = eina_list_append( mGraphRequestList, obj );
            Log::error("AddGraphRequest: mStatus=%d mGraphPath=%s", obj->GetStatus(), obj->GetGraphPath() ? obj->GetGraphPath():"");
            PrintAllGraphRequests();
        } else {
            errorCode = FBE_OBJECT_ALREADY_EXISTS;
        }
    } else {
        errorCode = FBE_OBJECT_IS_NULL;
    }
    return errorCode;
}

/**
 * @brief Remove GraphRequest object from internal list
 * @param[in] objID - ID of GraphRequest object
 * @return pointer on GraphRequest object if it is found otherwise NULL
 */
GraphRequest *FacebookSession::GetGraphRequestById(const GraphRequest *objId) {
    GraphRequest *res = static_cast<GraphRequest *>(eina_list_data_find( mGraphRequestList, objId ));
    return res;
}

/**
 * @brief Remove GraphRequest object from internal list
 * @param[in] objID - ID of GraphRequest object
 * @return error code FBE_NONE if operation done without error, otherwise error code
 */
FacebookSession::FacebookSessionError FacebookSession::RemoveGraphRequest(const GraphRequest *objID) {
    FacebookSessionError errorCode = FBE_NONE;
    if (mIsSessionClosed) return errorCode;
    FacebookSession *me = GetInstance();
    if(objID) {
        if ( eina_list_data_find(me->mGraphRequestList, objID ) != NULL ) {
            me->mGraphRequestList = eina_list_remove(me->mGraphRequestList, objID );
        } else {
            errorCode = FBE_OBJECT_NOT_FOUND;
        }
    } else {
        errorCode = FBE_OBJECT_IS_NULL;
    }
    return errorCode;
}

/**
 * @brief Delete all GraphRequest objects from internal list
 */
void FacebookSession::DeleteAllGraphRequests() {
    void *listData;
    Log::error("DeleteAllGraphRequests");
    EINA_LIST_FREE(mGraphRequestList, listData) {
        GraphRequest *graphRequest = static_cast<GraphRequest *>(listData);
        Log::error("Delete GraphRequest mStatus=%d mGraphPath=%s", graphRequest->GetStatus(), graphRequest->GetGraphPath() ? graphRequest->GetGraphPath():"");
        graphRequest->Cancel();
        graphRequest->Release();
    }
    mGraphRequestList = NULL;
}

/**
 * @brief Prints all GraphRequest objects handled by FacebookSession
 */
void FacebookSession::PrintAllGraphRequests() {
    int index = 0;
    Eina_List *l;
    void *listData;
    Log::error("PrintAllGraphRequests");
    EINA_LIST_FOREACH(mGraphRequestList, l, listData) {
        GraphRequest *graphRequest = static_cast<GraphRequest *>(listData);
        Log::error("PrintAllGraphRequests: GraphRequest(%d) mStatus=%d mGraphPath=%s", index, graphRequest->GetStatus(), graphRequest->GetGraphPath() ? graphRequest->GetGraphPath():"");
        index++;
    }
}

/**
 * @brief Build privacy value string from enum
 * @param privacy - privacy enum
 * @return built value
 */
char * FacebookSession::BuildPrivacyValue(PrivacyTypes privacy, Eina_List *allowList, Eina_List *denyList)
{
    const int max_id_len = 30;
    const char *privacy_names[] = {
            PRIVACY_EVERYONE,
            PRIVACY_ALL_FRIENDS,
            PRIVACY_FRIENDS_OF_FRIENDS,
            PRIVACY_CUSTOM,
            PRIVACY_SELF
    };

    char * allowArray = NULL;
    if (allowList) {
        const char * allow_key = ",'allow':'";
        int size = max_id_len * eina_list_count(allowList) + strlen(allow_key) + 1;
        allowArray = (char *) malloc(size);
        eina_strlcpy(allowArray, allow_key, size);

        bool populated = false;
        Eina_List *l;
        void *listData;
        EINA_LIST_FOREACH(allowList, l, listData) {
            if (populated) {
                eina_strlcat(allowArray, ",", size);
            }
            eina_strlcat(allowArray, (const char *)listData, size);
            populated = true;
        }
        eina_strlcat(allowArray, "'", size);
    }

    char * denyArray = NULL;
    if (denyList) {
        const char *deny_key = ",'deny':'";
        int size = max_id_len * eina_list_count(denyList) + strlen(deny_key) + 1;
        denyArray = (char *) malloc(size);
        eina_strlcpy(denyArray, deny_key, size);

        bool populated = false;
        Eina_List *l;
        void *listData;
        EINA_LIST_FOREACH(denyList, l, listData) {
            if (populated) {
                eina_strlcat(denyArray, ",", size);
            }
            eina_strlcat(denyArray, (const char *)listData, size);
            populated = true;
        }
        eina_strlcat(denyArray, "'", size);
    }

    const char * privacy_string =  privacy_names[(int)privacy] != NULL ? privacy_names[(int)privacy] : "";

    char valueBuf[MAX_PRIVACY_LEN];
    memset(valueBuf, 0, sizeof(valueBuf));
    Utils::Snprintf_s(valueBuf, MAX_PRIVACY_LEN, "'value': '%s'", privacy_string);

    int size = 2 + strlen(valueBuf) + (allowArray ? strlen(allowArray) : 0) + (denyArray ? strlen(denyArray) : 0) + 1;
    char *ret = (char *) malloc(size);
    Utils::Snprintf_s(ret, size, "{%s%s%s}", valueBuf, allowArray ? allowArray : "", denyArray ? denyArray : "");
    Log::debug("FacebookSession::BuildPrivacyValue = %s", ret);

    free(allowArray);
    free(denyArray);

    return ret;
}

/**
 * @brief This method should be invoked for login to facebook.
 * It initiates redirect to facebook login page, and subscribe for login floe redirections
 * @param[in] ewk_view_login - web view for displaying facebook login page
 * @param[in] login_cb - callback to notify client about login statuses
 * @param[in] cb_object - The user data to be passed to the callback function
 */
void FacebookSession::Login(Evas_Object *ewk_view_login, void (*login_cb)(ScreenBase *, LoginStatus), ScreenBase * cb_object)
{
    Log::debug("FacebookSession::Login");
    mLoginCallbackObject = cb_object;
    mLoginCallback = login_cb;
    //Set the User Agent
    ewk_view_user_agent_set (ewk_view_login, Utils::GetHTTPUserAgent());
    ewk_view_url_set(ewk_view_login, URL_LOGIN);
    evas_object_smart_callback_add(ewk_view_login, "url,changed", on_url_changed, this);
}

GraphRequest * FacebookSession::GetActionsAvailable(const char * object_id, void (*callback) (void *, char*, int), void * callback_object)
{
    GraphRequest * request = NULL;

    if(object_id) {
        Log::debug("FacebookSession::GetComments, object_id = %s", object_id);
        char url[MAX_URL_LEN] = {0};

        Utils::Snprintf_s(url, MAX_URL_LEN, "/%s%s", object_id, REQUEST_ACTIONS_AVAILABLE);
        request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object);
        request->ExecuteAsync();
    }
    return request;
}

/**
 * @brief This method should be invoked to request friends from facebook
 * @param[in] callback - callback to notify client when request is completed
 * @param[in] callback_object - user data to pass in callback
 * @return[in] a pointer to async thread
 */
GraphRequest * FacebookSession::GetFriends(void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetFriendsList");

    GraphRequest * request = new GraphRequest(REQUEST_MY_FRIENDS, NULL, GraphRequest::GET, callback, callback_object);
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to post feed to facebook
 * @param params - params bundle
 * @param[in] callback - callback to notify client when request is completed
 * @param[in] callback_object - user data to pass in callback
 * @return
 */
GraphRequest * FacebookSession::PostFeed(bundle * params, void (*callback) (void *, char*, int), void * callback_object, const char *userId )
{
    Log::debug("FacebookSession::PostFeed");

    char url[SHORT_BUFFER_SIZE] = {0};
    Utils::Snprintf_s( url, SHORT_BUFFER_SIZE, "/%s/%s", userId, REQUEST_FEED );
    GraphRequest * request = new GraphRequest( url, params, GraphRequest::POST, callback, callback_object );
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to post feed to facebook
 * @param params - params bundle
 * @param[in] callback - callback to notify client when request is completed
 * @param[in] callback_object - user data to pass in callback
 * @return
 */
GraphRequest * FacebookSession::PostFeedWithPhotos(bundle * params, Eina_List *photos, void (*callback) (void *, char*, int), void * callback_object, const char *userId )
{
    Log::debug("FacebookSession::PostFeedWithPhotos");

    GraphRequest * request = NULL;

    std::stringstream photosDescription;

    std::string postParam = "format=json&is_photo_container=1&";
    if(params) {
        postParam += GraphRequest::PrepareParamsFromBundle(params);
    }

    postParam = Utils::ReplaceString(postParam ,"\"", "\\\"");

    photosDescription << "[{ \"method\":\"POST\","
            "\"body\":\""<< postParam <<"\","
            "\"name\":\"photo_0\","
            "\"omit_response_on_success\":false,"
            "\"relative_url\": \""<< REQUEST_POST_PHOTO_GRAPH_VERSION <<
            "/" << userId << "/"<< REQUEST_FEED <<"\" }, ";

    char *photosArrayJSON = CreateJSONArrayPhotosForPost(photos, "{result=photo_0:$.id}", false);

    photosDescription << photosArrayJSON << "]";

    free(photosArrayJSON);

    Log::debug("FacebookSession::PostFeedWithPhotos = photosDescription %s", photosDescription.str().c_str());
    bundle* batchBundle = bundle_create();
    bundle_add_str(batchBundle, "batch", photosDescription.str().c_str());

    request = new GraphRequest( "", NULL, GraphRequest::POST_MULTIPART, callback, callback_object, NULL, GraphRequest::GRAPH, batchBundle );
    if(request) {
        request->ExecuteAsync();
    }
    bundle_free(batchBundle);
    return request;
}

GraphRequest *FacebookSession::AddPhotosToPost(bundle *params, Eina_List *photos, void (*callback) (void *, char*, int), void * callback_object, const char *userId, const char *PhostId) {
    Log::debug("FacebookSession::RePostFeedWithPhotos");

    std::stringstream photosDescription;
    photosDescription << "[";

    char *photosArrayJSON = CreateJSONArrayPhotosForPost(photos, PhostId, false);
    photosDescription << photosArrayJSON << ']';
    free(photosArrayJSON);

    bundle* batchBundle = bundle_create();
    bundle_add_str(batchBundle, "batch", photosDescription.str().c_str());

    GraphRequest *request = new GraphRequest("", nullptr, GraphRequest::POST_MULTIPART, callback, callback_object, nullptr, GraphRequest::GRAPH, batchBundle);
    if(request) {
        request->ExecuteAsync();
    }
    bundle_free(batchBundle);
    return request;
}

char *FacebookSession::CreateJSONArrayPhotosForPost(Eina_List *list, const char *postId, bool withDependencies) {
    std::stringstream photosDescription;

        int photoRequestIndex = 1;
        Eina_List * listItem;
        void * listData;
        EINA_LIST_FOREACH(list, listItem, listData) {
            PhotoPostOperation::PhotoPostDescription *photoDecription = static_cast<PhotoPostOperation::PhotoPostDescription *>(listData);
            if (photoRequestIndex > 1) {
                photosDescription << ",";
            }
            char *photoJSON = CreateJSONPhotoForPost(photoDecription, postId, withDependencies ? photoRequestIndex : 0);
            photosDescription << photoJSON;
            free(photoJSON);
            ++photoRequestIndex;
        }

    return strdup(photosDescription.str().c_str());
}

char *FacebookSession::CreateJSONPhotoForPost(PhotoPostOperation::PhotoPostDescription *photoDecription, const char *postId, unsigned int number) {
    std::stringstream photosDescription;

    assert(photoDecription);
    assert(photoDecription->GetPhotoId());
    photosDescription << "{\"method\":\"POST\","
            "\"body\":\"published=1&target_post=" << postId;
    if(photoDecription->GetCaption()) {
        char *escape = Utils::escape(photoDecription->GetCaption());
        photosDescription << "&name=" << escape << "";
        free(escape);
    }
    photosDescription << "\",";
            if (number > 0) {
                photosDescription << "\"name\":\"photo_"<< number << "\","
                "\"depends_on\":\"photo_"<< (number - 1) <<"\",";
            }
            photosDescription << "\"omit_response_on_success\":false,"
            "\"relative_url\": \"" << REQUEST_POST_PHOTO_GRAPH_VERSION <<
            photoDecription->GetPhotoId() << "\"";
    photosDescription << "}";

    return strdup(photosDescription.str().c_str());
}

GraphRequest * FacebookSession::UpdatePost(bundle * params, void (*callback) (void *, char*, int), void * callback_object, const char* id)
{
    Log::debug("FacebookSession::UpdatePost");

    char url[SHORT_BUFFER_SIZE] = {0};
    Utils::Snprintf_s( url, SHORT_BUFFER_SIZE, "/%s", id );
    GraphRequest * request = new GraphRequest( url, params, GraphRequest::POST, callback, callback_object );
    request->ExecuteAsync();
    return request;
}

GraphRequest * FacebookSession::SetDefaultPrivacyOption(const char *privacyOptionId, void (*callback) (void *, char*, int), void * callback_object) {
    Log::debug("FacebookSession::SetDefaultPrivacyOption");
    bundle* params = bundle_create();
    bundle_add_str(params, "setting", "COMPOSER");
    bundle_add_str(params, "privacy", privacyOptionId);

    char url[SHORT_BUFFER_SIZE] = {0};
    Utils::Snprintf_s( url, SHORT_BUFFER_SIZE, REQUEST_POST_SET_PRIVACY_SETTING);
    GraphRequest * request = new GraphRequest(url, params, GraphRequest::POST, callback, callback_object );
    request->ExecuteAsync();
    bundle_free(params);

    return request;
}

GraphRequest * FacebookSession::SetCustomPrivacyOptionAsDefault(const char *privacyOption, void (*callback) (void *, char*, int), void * callback_object) {
    Log::debug("FacebookSession::SetDefaultPrivacyOption");
    bundle* params = bundle_create();
    bundle_add_str(params, "privacy_option", privacyOption);
    bundle_add_str(params, "privacy_field", "current_composer");

    char url[SHORT_BUFFER_SIZE] = {0};
    Utils::Snprintf_s( url, SHORT_BUFFER_SIZE, REQUEST_POST_PRIVACY_OPTIONS);
    GraphRequest * request = new GraphRequest(url, params, GraphRequest::POST, callback, callback_object );
    request->ExecuteAsync();
    bundle_free(params);

    return request;
}

GraphRequest * FacebookSession::UpdateComment(bundle * params, void (*callback) (void *, char*, int), void * callback_object, const char* id)
{
    Log::debug("FacebookSession::UpdateComment");

    GraphRequest * request = NULL;

    char url[SHORT_BUFFER_SIZE] = {0};
    Utils::Snprintf_s(url, SHORT_BUFFER_SIZE, "/%s", id);
    request = new GraphRequest( url, params, GraphRequest::POST, callback, callback_object );
    request->ExecuteAsync();
    return request;
}

GraphRequest * FacebookSession::ReloadComment(const char* id, void (*callback) (void *, char*, int), void * callback_object)
{
    GraphRequest * request = NULL;

    if(id) {
        Log::debug("FacebookSession::ReloadComment, id = %s", id);
        char url[MAX_URL_LEN] = {0};
        char replies[MAX_URL_LEN] = {0};

        Utils::Snprintf_s(replies, MAX_URL_LEN, REQUEST_GET_COMMENTS_REPLIES_FORMAT, REQUEST_GET_COMMENTS_FIELDS);

        Utils::Snprintf_s(url, MAX_URL_LEN, "/%s?fields=%s%s", id, REQUEST_GET_COMMENTS_FIELDS, replies);
        request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object);
        request->ExecuteAsync();
    }

    return request;
}

/**
 * @brief This method should be invoked to send likes requests to facebook
 * @param object_id - object to which like request is sent
 * @param httpMethod - the type of http sending method
 * @param[in] callback - callback to notify client when request is completed
 * @param[in] callback_object - user data to pass in callback
 * @return[in] a pointer to async thread
 */
GraphRequest * FacebookSession::DoLike(const char * object_id, GraphRequest::HttpMethod httpMethod, void (*callback) (void *, char*, int), void * callback_object)
{
    GraphRequest * request = NULL;

    if(object_id) {
        Log::debug("FacebookSession::DoLike, object_id = %s, httpMethod =%i", object_id, httpMethod);
        char url[MAX_URL_LEN] = {0};
        Utils::Snprintf_s(url, MAX_URL_LEN, "/%s%s", object_id, REQUEST_LIKES);
        request = new GraphRequest(url, NULL, httpMethod, callback, callback_object);
        request->ExecuteAsync();
    }
    return request;
}

GraphRequest* FacebookSession::DoLike(const std::string& objectId, GraphRequest::HttpMethod httpMethod, std::function<void(const std::string&, CURLcode)> callback) {
    GraphRequest * request = nullptr;
    if (!objectId.empty()) {
        Log::debug("FacebookSession::DoLike, object_id = %s, httpMethod =%i\n", objectId.c_str(), httpMethod);
        std::string url("/");
        url.append(objectId).append(REQUEST_LIKES);
        request = new GraphRequest(url, nullptr, httpMethod, callback);
        request->ExecuteAsync();
    }
    return request;
}

GraphRequest* FacebookSession::Like(const std::string& objectId, bool liked, std::function<void(const std::string&, CURLcode)> callback) {
    return DoLike(objectId, liked ? GraphRequest::POST : GraphRequest::DELETE, callback);
}

/**
 * @brief This method should be invoked to fetch likes summary data from facebook
 * @param[in] callback - callback to notify client when request is completed
 * @param[in] callback_object - user data to pass in callback
 * @return[in] a pointer to async thread
 */
GraphRequest * FacebookSession::GetLikesSummary(const std::string& objectId, std::function<void(const std::string&, CURLcode)> callback)
{
    GraphRequest * request = nullptr;
    if(!objectId.empty()) {
        Log::debug("FacebookSession::GetLikesSummary, objectId = %s", objectId.c_str());
        std::string url("/");
        url.append(objectId).append(REQUEST_LIKES_SUMMARY);
        request = new GraphRequest(url, nullptr, GraphRequest::GET, callback);
        request->ExecuteAsync();
    }
    return request;
}


/**
 * @brief This method should be invoked to post comment to facebook
 * @param object_id - object to add comment
 * @param params - parameters bundle
 * @param[in] callback - callback to notify client when request is completed
 * @param[in] callback_object - user data to pass in callback
 * @param[in] photoFileName - the name of photo file to be attached; NULL if no photo should be attached
 * @return
 */
GraphRequest * FacebookSession::PostComment(const char * object_id, bundle * params, void (*callback) (void *, char*, int), void * callback_object, const char * photoFileName)
{
    GraphRequest * request = NULL;

    if(object_id) {
        Log::debug("FacebookSession::PostComment, object_id = %s", object_id);
        char url[MAX_URL_LEN] = {0};
        Utils::Snprintf_s(url, MAX_URL_LEN, "/%s%s", object_id, REQUEST_COMMENTS);
        if(photoFileName) {
            Log::debug("FacebookSession::PostComment: photoFileName = %s", object_id, photoFileName);
            request = new GraphRequest(url, NULL, GraphRequest::POST_MULTIPART, callback, callback_object, NULL, GraphRequest::GRAPH, params, photoFileName);
        } else {
            request = new GraphRequest(url, params, GraphRequest::POST, callback, callback_object );
        }
        request->ExecuteAsync();
    }
    return request;
}

/**
 * @brief This method should be invoked to get comments for facebook object
 * @param object_id - object to publish comment
 * @param callback - callback to notify client when request is completed
 * @param callback_object -user data to pass in callback
 * @return
 */
GraphRequest * FacebookSession::GetComments(const char * object_id, void (*callback) (void *, char*, int), void * callback_object)
{
    GraphRequest * request = NULL;

    if(object_id) {
        Log::debug("FacebookSession::GetComments, object_id = %s", object_id);
        char url[MAX_URL_LEN] = {0};
        char replies[MAX_URL_LEN] = {0};

        Utils::Snprintf_s(replies, MAX_URL_LEN, REQUEST_GET_COMMENTS_REPLIES_FORMAT, REQUEST_GET_COMMENTS_FIELDS);

        Utils::Snprintf_s(url, MAX_URL_LEN, "/%s%s%s%s", object_id, REQUEST_GET_COMMENTS,REQUEST_GET_COMMENTS_FIELDS, replies);
        request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object);
        request->ExecuteAsync();
    }
    return request;
}

/**
 * @brief These methods should be invoked to request UserProfile from facebook
 * @param[in] object_id - object id
 * @param[in] callback - callback to notify client when request is completed
 * @param[in] callback_object - user data to pass in callback
 * @return[in] a pointer to async thread
 */
GraphRequest * FacebookSession::GetUserProfileAndCoverPic(const char * object_id, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetUserProfile");
    char graphPath[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(graphPath, MAX_URL_LEN, "/%s?%s", object_id ? object_id : "me", REQUEST_PROF_COVER_PIC);

    GraphRequest * request = new GraphRequest(graphPath, NULL, GraphRequest::GET, callback, callback_object);
    request->ExecuteAsync();
    return request;
}

GraphRequest * FacebookSession::GetUserFullPicture(const char* object_id, void (*callback) (void*, char*, int), void* callback_object)
{
    Log::debug("FacebookSession::GetUserFullPicture()");
    char graphPath[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(graphPath, MAX_URL_LEN, "/%s?%s", object_id ? object_id : "me", REQUEST_PROF_FULL_PICTURE);
    GraphRequest * request = new GraphRequest(graphPath, NULL, GraphRequest::GET, callback, callback_object);
    request->ExecuteAsync();
    return request;
}

GraphRequest * FacebookSession::GetUserProfileName(void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetUserProfileName()");
    GraphRequest * request = new GraphRequest(REQUEST_PROF_NAME, NULL, GraphRequest::GET, callback, callback_object);
    request->ExecuteAsync();
    return request;
}

GraphRequest * FacebookSession::GetUnseenNotificationsCount(void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetUnseenNotificationsCount()");
    GraphRequest * request = new GraphRequest(REQUEST_UNSEEN_NOTIF_COUNT, NULL, GraphRequest::GET, callback, callback_object);
    request->ExecuteAsync();
    return request;
}

GraphRequest * FacebookSession::PostUnseenNotificationsCount(void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::PostUnseenNotificationsCount()");
    GraphRequest * request = new GraphRequest(REQUEST_POST_UNSEEN_NOTIF_COUNT, NULL, GraphRequest::POST, callback, callback_object);
    request->ExecuteAsync();
    return request;
}

GraphRequest * FacebookSession::GetNotificationPhotoDetails(const char* photoObjId, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetNotificationPhotoDetails()");
    char graphPath[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(graphPath, MAX_URL_LEN, "/%s%s", photoObjId, REQUEST_PHOTO_DETAILS);
    GraphRequest * request = new GraphRequest(graphPath, NULL, GraphRequest::GET, callback, callback_object);
    request->ExecuteAsync();
    return request;
}

GraphRequest * FacebookSession::GetUserFirstPhoto(const char * object_id, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetUserFirstPhoto");
    char graphPath[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(graphPath, MAX_URL_LEN, "/%s%s", object_id ? object_id : "me", REQUEST_PROF_FIRSTPHOTO_PIC);
    GraphRequest * request = new GraphRequest(graphPath, NULL, GraphRequest::GET, callback, callback_object);
    request->ExecuteAsync();
    return request;
}

GraphRequest * FacebookSession::GetUserFriendPic(const char * object_id, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetUserFriendPic");
    char graphPath[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(graphPath, MAX_URL_LEN, "/%s%s", object_id ? object_id : "me", REQUEST_PROF_FRIEND_PIC);

    GraphRequest * request = new GraphRequest(graphPath, NULL, GraphRequest::GET, callback, callback_object);
    request->ExecuteAsync();
    return request;
}
GraphRequest * FacebookSession::GetAnyUserProfilepic(void (*callback) (void *, char*, int), void * callback_object, const char * Req)
{
    Log::debug("FacebookSession::GetAnyUserProfile");
    GraphRequest * request = new GraphRequest(Req, NULL, GraphRequest::GET, callback, callback_object);
    request->ExecuteAsync();
    return request;
}

GraphRequest* FacebookSession::GetUserProfileAboutPic(void (*CallBack)(void *, char*, int), void* CallBackObject) {
    GraphRequest * request = new GraphRequest(REQUEST_UP_ABOUT_PIC, NULL, GraphRequest::GET, CallBack, CallBackObject);
    request->ExecuteAsync();
    return request;
}

GraphRequest* FacebookSession::GetUPAboutOverview(const char *object_id, void (*CallBack)(void *, char*, int), void* CallBackObject) {
    char graphPath[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(graphPath, MAX_URL_LEN, "/%s?%s", object_id ? object_id : "me", REQUEST_UP_ABOUT_OVERVIEW);
    GraphRequest * request = new GraphRequest(graphPath, NULL, GraphRequest::GET, CallBack, CallBackObject);
    request->ExecuteAsync();
    return request;
}

GraphRequest* FacebookSession::GetUPPlacesOverview(const char *object_id, void (*CallBack)(void *, char*, int), void* CallBackObject) {
    char graphPath[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(graphPath, MAX_URL_LEN, "/%s%s", object_id ? object_id : "me", REQUEST_UP_PLACES_OVERVIEW);
    GraphRequest * request = new GraphRequest(graphPath, NULL, GraphRequest::GET, CallBack, CallBackObject);
    request->ExecuteAsync();
    return request;
}

GraphRequest* FacebookSession::GetUPPhotos(const char* userProfileId, void (*CallBack)(void *, char*, int), void* CallBackObject) {
    char graphPath[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(graphPath,MAX_URL_LEN,"/%s%s", userProfileId ? userProfileId : "me", REQUEST_UP_PHOTOS);
    Log::debug("FacebookSession::GetUPPhotos url :: %s", graphPath);
    GraphRequest * request = new GraphRequest(graphPath, NULL, GraphRequest::GET, CallBack, CallBackObject);
    request->ExecuteAsync();
    return request;
}

GraphRequest* FacebookSession::GetUPPictureId(const char* userProfileId, void (*CallBack)(void *, char*, int), void* CallBackObject) {
    char graphPath[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(graphPath,MAX_URL_LEN,"/%s%s", userProfileId ? userProfileId : "me", REQUEST_PROF_PICTUREID);
    Log::debug("FacebookSession::GetUPPictureId url :: %s", graphPath);
    GraphRequest * request = new GraphRequest(graphPath, NULL, GraphRequest::GET, CallBack, CallBackObject);
    request->ExecuteAsync();
    return request;
}

GraphRequest* FacebookSession::GetUPAboutMore(const char* UserId, void (*CallBack)(void *, char*, int), void* CallBackObject) {
    std::string Url;
    Url.append("/");
    Url.append(UserId ? UserId : "me");
    Url.append("?");
    Url.append(REQUEST_UPAM_MORE);

    GraphRequest* NewRequest = new GraphRequest(Url.c_str(), NULL, GraphRequest::GET, CallBack, CallBackObject);
    NewRequest->ExecuteAsync();
    return NewRequest;
}
/**
 * @brief This method should be invoked to get the user activity feed from facebook
 * @param[in] object_id - object id
 * @param[in] callback - callback to notify client when request is completed
 * @param[in] callback_object - user data to pass in callback
 * @return started request
 */
GraphRequest *FacebookSession::GetProfileActivityFeed(const char *object_id, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetProfileActivityFeed");
    char graphPath[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(graphPath, MAX_URL_LEN, "/%s/%s", object_id ? object_id : "me", REQUEST_PROFILE_ACTIVITY_FEED);

    GraphRequest * request = new GraphRequest(graphPath, NULL, GraphRequest::GET, callback, callback_object);
    request->ExecuteAsync();
    return request;
}

GraphRequest *FacebookSession::GetFriendDetails(const char *object_id, void (*callback) (void *, char*, int), void * callback_object) {
    GraphRequest * request = NULL;

    if(object_id) {
        Log::debug("FacebookSession::GetFriendDetails, object_id = %s", object_id);
        char url[MAX_URL_LEN] = {0};

        Utils::Snprintf_s(url, MAX_URL_LEN, "/%s%s", object_id, REQUEST_FRIEND_DETAILS);
        request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object);
        request->ExecuteAsync();
    }
    return request;
}

/**
 * @brief This method should be invoked to share some facebook object
 * @param params - parameters bundle
 * @param[in] callback - callback to notify client when request is completed
 * @param[in] callback_object - user data to pass in callback
 * @return
 */
GraphRequest * FacebookSession::Share(bundle * params, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::Share");
    char url[MAX_URL_LEN] = {0};

    Utils::Snprintf_s(url, MAX_URL_LEN, "%s", REQUEST_SHARE);
    GraphRequest * request = new GraphRequest(url, params, GraphRequest::POST, callback, callback_object);
    request->ExecuteAsync();
    return request;
}

GraphRequest *FacebookSession::GetEventDetails(const char *object_id, GraphRequest::HttpMethod httpMethod, void (*callback) (void *, char*, int), void * callback_object) {
    GraphRequest * request = NULL;

    if(object_id) {
        Log::debug("FacebookSession::GetEventDetails, object_id = %s", object_id);
        char url[MAX_URL_LEN] = {0};
        Utils::Snprintf_s(url, MAX_URL_LEN, "/%s%s", object_id, REQUEST_EVENT_DETAILS);
        url[MAX_URL_LEN-1]=0;

        request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object);
        request->ExecuteAsync();
    }
    return request;
}

Eina_List *FacebookSession::GetImageDetails(Eina_List *imageIds, void (*callback) (void *, char*, int), void * callback_object) {
    Log::debug("FacebookSession::GetImageDetails()");
    char url[MAX_URL_LEN] = {0};

    Eina_List *requests = NULL;

    Eina_List *l;
    void *list_data;
    EINA_LIST_FOREACH(imageIds, l, list_data) {
        const char *objectId = (const char *) list_data;
        Log::debug("FacebookSession::GetImageDetails() for image Id:%s", objectId);
        Utils::Snprintf_s(url, MAX_URL_LEN, REQUEST_IMAGE_DETAILS, objectId);
        GraphRequest *request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object);
        request->ExecuteAsync();
        requests = eina_list_append(requests, request);
    }
    return requests;
}

/**
 * @brief This method should be invoked to get info about some facebook object
 * @param id - object id or web url
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::GetInfo(const char * id, void (*callback) (void *, char*, int), void * callback_object)
{
    GraphRequest * request = NULL;

    if(id) {
        Log::debug("FacebookSession::GetInfo, object_id = %s", id);
        char url[MAX_URL_LEN] = {0};

        Utils::Snprintf_s(url, MAX_URL_LEN, "%s%s", REQUEST_INFO, id);

        request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object);
        request->ExecuteAsync();
    }
    return request;
}

/**
 * @brief This method should be invoked to get list of icoming friend requests
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::GetIncomingFriendRequestsCounts(void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetIncomingFriendRequestsCounts");
    char url[MAX_URL_LEN] = {0};

    Utils::Snprintf_s(url, MAX_URL_LEN, "%s", REQUEST_INCOMING_FRIEND_REQUESTS_COUNTS);

    GraphRequest * request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object, "/v1.0");
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to send friend requests to facebook
 * @param object_id - object to which like request is sent
 * @param httpMethod - the type of http sending method
 * @param[in] callback - callback to notify client when request is completed
 * @param[in] callback_object - user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::DoFriends(const char * object_id, GraphRequest::HttpMethod httpMethod, void (*callback) (void *, char*, int), void * callback_object)
{
    GraphRequest * request = NULL;

    if(object_id) {
        Log::debug("FacebookSession::DoFriends, object_id = %s, httpMethod =%i", object_id, httpMethod);
        char url[MAX_URL_LEN] = {0};
        Utils::Snprintf_s(url, MAX_URL_LEN, "%s%s", REQUEST_FRIENDS, object_id);
        request = new GraphRequest(url, NULL, httpMethod, callback, callback_object);
        request->ExecuteAsync();
    }
    return request;
}

GraphRequest * FacebookSession::FollowUser(const char * object_id, void (*callback) (void *, char*, int), void * callback_object)
{
    GraphRequest * request = NULL;

    if(object_id) {
        Log::debug("FacebookSession::FollowUser, object_id = %s", object_id);
        char url[MAX_URL_LEN] = {0};
        Utils::Snprintf_s(url, MAX_URL_LEN, "/%s%s", object_id, "/subscribers"); //NOTE: FB response to this request differ for different API version
        request = new GraphRequest(url, NULL, GraphRequest::POST, callback, callback_object, "/v2.4");
        request->ExecuteAsync();
    }
    return request;
}

GraphRequest * FacebookSession::UnFollowUser(const char * object_id, void (*callback) (void *, char*, int), void * callback_object)
{
    GraphRequest * request = NULL;

    if(object_id) {
        Log::debug("FacebookSession::UnFollowUser, object_id = %s", object_id);
        char url[MAX_URL_LEN] = {0};
        Utils::Snprintf_s(url, MAX_URL_LEN, "/%s%s", object_id, "/subscribers"); //NOTE: FB response to this request differ for different API version
        request = new GraphRequest(url, NULL, GraphRequest::DELETE, callback, callback_object, "/v2.4");
        request->ExecuteAsync();
    }
    return request;
}
GraphRequest * FacebookSession::BlockUser(const char * object_id, void (*callback) (void *, char*, int), void * callback_object)
{
    GraphRequest * request = NULL;

    if(object_id) {
        Log::debug("FacebookSession::BlockUser, object_id = %s", object_id);
        char url[MAX_URL_LEN] = {0};
        Utils::Snprintf_s(url, MAX_URL_LEN, "%s%s", "/me/blocked?uid=", object_id);
        request = new GraphRequest(url, NULL, GraphRequest::POST, callback, callback_object);
        request->ExecuteAsync();
    }
    return request;
}

GraphRequest * FacebookSession::UnblockUser(const char * object_id, void (*callback) (void *, char*, int), void * callback_object)
{
    GraphRequest * request = NULL;

    if(object_id) {
        Log::debug("FacebookSession::UnblockUser, object_id = %s", object_id);
        char url[MAX_URL_LEN] = {0};
        Utils::Snprintf_s(url, MAX_URL_LEN, "%s%s", "/me/blocked?uid=", object_id);
        request = new GraphRequest(url, NULL, GraphRequest::DELETE, callback, callback_object);
        request->ExecuteAsync();
    }
    return request;
}

GraphRequest * FacebookSession::PokeUser(const char * object_id, void (*callback) (void *, char*, int), void * callback_object)
{
    GraphRequest * request = NULL;
    if(object_id) {
        Log::debug("FacebookSession::PokeUser, object_id = %s", object_id);
        char url[MAX_URL_LEN] = {0};
        Utils::Snprintf_s(url, MAX_URL_LEN, "/%s/pokes", object_id);
        request = new GraphRequest(url, NULL, GraphRequest::POST, callback, callback_object);
        request->ExecuteAsync();
    }
    return request;
}

/**
 * @brief This method should be invoked to send friend requests to facebook
 * @param[in] callback - callback to notify client when request is completed
 * @param[in] callback_object - user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::SendFriendRequest(const char * object_id, void (*callback) (void *, char*, int), void * callback_object)
{
    return DoFriends(object_id, GraphRequest::POST, callback, callback_object);
}

GraphRequest * FacebookSession::CancelOutgoingFriendRequest(const char * object_id, void (*callback) (void *, char*, int), void * callback_object)
{
    GraphRequest * request = NULL;

    if(object_id) {
        Log::debug("FacebookSession::CancelOutgoingFriendRequest");
        char url[MAX_URL_LEN] = {0,};
        Utils::Snprintf_s(url, MAX_URL_LEN, REQUEST_CANCEL_OUTGOING_REQUEST, object_id);
        request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object, NULL, GraphRequest::REST);
        request->ExecuteAsync();
    }
    return request;
}

/**
 * @brief This method should be invoked to fetch likes from facebook
 * @param[in] callback - callback to notify client when request is completed
 * @param[in] callback_object - user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::Unfriend(const char * object_id, void (*callback) (void *, char*, int), void * callback_object)
{
    return DoFriends(object_id, GraphRequest::DELETE, callback, callback_object);
}

/**
 * @brief This method should be invoked to get list of friendlists
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::GetFriendlists(void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetFriendlists");
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, "%s", REQUEST_FRIENDLISTS);

    GraphRequest * request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object);
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to edit friendlist
 * @param list_id - friend list to me modified
 * @param member_id - user id to be added/deleted
 * @param httpMethod - the type of http sending method. POST - to add member, DELETE - to delete member
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::EditFriendlistMembers(const char * list_id, const char * member_id, GraphRequest::HttpMethod httpMethod, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::EditFriendlistsMembers");
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, "%s%s%s", list_id, REQUEST_EDIT_MEMBERS, member_id);

    GraphRequest * request = new GraphRequest(url, NULL, httpMethod, callback, callback_object);
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to get photos
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::GetPhotos(void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetPhotos");
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, REQUEST_PHOTOS);

    GraphRequest * request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object);
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to post new photo
 * @param imageFileName - the name of image file that should be uploaded
 * @param params - parameters bundle
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @param id- user id or album id.
 * @return started request
 */
GraphRequest * FacebookSession::PostPhoto(const char * imageFileName, bundle * params, void (*callback) (void *, char*, int), void * callback_object, const char *id )
{
    Log::debug("FacebookSession::PostPhoto");
    char url[SHORT_BUFFER_SIZE] = {0};
    Utils::Snprintf_s( url, SHORT_BUFFER_SIZE, "/%s/%s", id, REQUEST_PHOTOS );

    GraphRequest * request = new GraphRequest( url, NULL, GraphRequest::POST_MULTIPART, callback, callback_object, NULL, GraphRequest::GRAPH, params, imageFileName );
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to post new video
 * @param imageFileName - the name of image file that should be uploaded
 * @param params - parameters bundle
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @param id- user id or album id.
 * @return started request
 */
GraphRequest * FacebookSession::PostVideo(const char * imageFileName, bundle * params, void (*callback) (void *, char*, int), void * callback_object, const char *id )
{
    Log::debug("FacebookSession::PostVideo");
    char url[SHORT_BUFFER_SIZE] = {0};
    Utils::Snprintf_s( url, SHORT_BUFFER_SIZE, "/%s/%s", id, REQUEST_VIDEOS );

    GraphRequest * request = new GraphRequest( url, NULL, GraphRequest::POST_MULTIPART, callback, callback_object, NULL, GraphRequest::GRAPH_VIDEO, params, imageFileName );
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to post new photo for cover image
 * @param imageFileName - the name of image file that should be uploaded
 * @param params - parameters bundle
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::PostCoverPhoto(const char * imageFileName, bundle * params, void (*callback) (void *, char*, int), void * callback_object, const char *userId )
{
    Log::debug("FacebookSession::PostPhoto");
    char url[SHORT_BUFFER_SIZE] = {0};
    Utils::Snprintf_s( url, SHORT_BUFFER_SIZE, "/%s/%s", userId, "cover" );

    GraphRequest * request = new GraphRequest( url, params, GraphRequest::POST_MULTIPART, callback, callback_object, NULL, GraphRequest::GRAPH, NULL, imageFileName );
    request->ExecuteAsync();
    return request;
}

GraphRequest * FacebookSession::PostProfilePicture(const char * imageFileName, bundle * params, void (*callback) (void *, char*, int), void * callback_object, const char *userId )
{
    Log::debug("FacebookSession::Post Profile Picture");
    char url[SHORT_BUFFER_SIZE] = {0};
    Utils::Snprintf_s( url, SHORT_BUFFER_SIZE, "/%s/%s", userId, "picture" );

    Log::debug("Profile pic = %s", imageFileName);
    GraphRequest * request = new GraphRequest( url, params, GraphRequest::POST_MULTIPART, callback, callback_object, NULL, GraphRequest::GRAPH, NULL, imageFileName );
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to confirm or reject friend request
 * @param userId - the user id who sent request
 * @param isConfirm - indicates whether you want confirm or reject friend request
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::ConfirmFriendRequest(const char * userId, bool isConfirm, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::info("FacebookSession::Confirm(Delete)FriendRequest");
    char url[MAX_URL_LEN] = {0};
    //0 - Hide the request; 1 - Confirm the request; 2 - Reject the request
    Utils::Snprintf_s(url, MAX_URL_LEN, REQUEST_CONFIRM_FRIEND_REQUEST, userId, isConfirm ? 1 : 2);

    GraphRequest * request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object, NULL, GraphRequest::REST);
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to get list of people you may know
 * @param flowType - application context, for which people are requested.Following types are supported:
 *      NEWS_FEED_MEGAPHONE,
 *      FRIENDS_TAB,
 *      FRIEND_BROWSER,
 *      NEW_ACCOUNT_NUX,
 *      INTERSTITIAL,
 *      BOOKMARKS,
 *      SEARCH_BOX,
 *      FRIENDS_CENTER,
 *      CONTINUOUS_SYNC,
 *      FRIEND_REQUEST_TAB,
 *      ACCEPT_FRIEND_REQUEST,
 *      FIND_FRIENDS,
 *      NOTIFICATION,
 *      NUX
 * @param picSize - the size of user picture
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::GetPeopleYouMayKnow(const char * flowType, int picSize, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetPeopleYouMayKnow");
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, REQUEST_PEOPLE_YOU_MAY_KNOW, flowType, picSize);

    GraphRequest * request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object, NULL, GraphRequest::REST);
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to get list of privacy options for PostComposerScreen
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::GetPrivacyOptions(void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetPrivacyOptions");
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, REQUEST_PRIVACY_OPTIONS);

    GraphRequest * request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object);
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to get events
 * @param time - date and time value (e.g. 2015-05-14T12:51:02+0300)
 * @param since - true in case of events posted since the passed time (false in case of old events)
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::GetEvents(const char * time, bool since, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetEvents");
    char url[MAX_URL_LEN] = {0};

    if (time) {
        Utils::Snprintf_s(url, MAX_URL_LEN, "/%s%s", (since ? REQUEST_EVENTS_SINCE : REQUEST_EVENTS_UNTIL), time);
    } else {
        Utils::Snprintf_s(url, MAX_URL_LEN, REQUEST_EVENTS);
    }

    GraphRequest * request = new GraphRequest( url, NULL, GraphRequest::GET, callback, callback_object );
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to get events
 * @param time - date and time value (e.g. 2015-05-14T12:51:02+0300)
 * @param since - true in case of events posted since the passed time (false in case of old events)
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::GetEventsRequestsBatch(const char * time, bool since, void (*callback) (void *, char*, int), void * callback_object)
{
    char url[MAX_URL_LEN] = {0};

    if (time) {

        CURL *curl = curl_easy_init();
        char *output = NULL;
        if(curl) {
            output = curl_easy_escape(curl, time, 0);
        }

        Utils::Snprintf_s(url, MAX_URL_LEN, "%s%s%s%s", BATCH_REQUEST_START_EVENTS_REQUEST, (since ? REQUEST_EVENTS_SINCE : REQUEST_EVENTS_UNTIL),
                output, BATCH_REQUEST_END_EVENTS_REQUEST);

        curl_free(output);
        curl_easy_cleanup(curl);
    } else {
        Utils::Snprintf_s(url, MAX_URL_LEN, "%s%s%s", BATCH_REQUEST_START_EVENTS_REQUEST, REQUEST_EVENTS, BATCH_REQUEST_END_EVENTS_REQUEST);
    }

    return DoBatchRequest(url, callback, callback_object);
}

/**
 * @brief This method should be invoked to get events saves
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::GetEventsSavesRequestBatch(void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetEventsSavesRequestBatch");
    return DoBatchRequest(BATCH_REQUEST_EVENTS_SAVES_REQUEST, callback, callback_object);
}

/**
 * @brief This method should be invoked to get invites events
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::GetInvitesEventsRequestBatch(void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetInvitesEventsRequestBatch");
    return DoBatchRequest(BATCH_REQUEST_INVITES_EVENTS_REQUEST, callback, callback_object);
}

/**
 * @brief This method should be invoked to get hosting events
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::GetHostingEventsRequestBatch(void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetInvitesEventsRequestBatch");
    return DoBatchRequest(BATCH_REQUEST_HOSTING_EVENTS_REQUEST, callback, callback_object);
}

/**
 * @brief This method should be invoked to post a new event to facebook
 * @param params - params bundle
 * e.g.
 *      bundle* paramsBundle;
 *      paramsBundle = bundle_create();
 *      bundle_add_str(paramsBundle, "name", "NewEvent10072015-10-22");
 *      bundle_add_str(paramsBundle, "start_time", "2016-01-02T10:00:00Z");
 *      bundle_add_str(paramsBundle, "end_time", "2016-01-02T11:00:00Z");
 *      //bundle_add_str(paramsBundle, "location", "Kremlin");//used if no location_id found for the event
 *      bundle_add_str(paramsBundle, "location_id", "213636511990328");
 *      bundle_add_str(paramsBundle, "country", "Russia");
 *      bundle_add_str(paramsBundle, "description", "First successful event");
 *      bundle_add_str(paramsBundle, "type", "public");//value should be clarified, this is privacy_type field
 *      bundle_add_str(paramsBundle, "can_guests_invite_friends", "true");
 * @param[in] callback - callback to notify client when request is completed
 * @param[in] callback_object - user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::PostCreateNewEvent(bundle * params, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::PostCreateNewEvent");
    if ( !params ) {
        Log::debug("FacebookSession::PostCreateNewEvent: NULL arg has been passed");
        return NULL;
    }

    GraphRequest * request = new GraphRequest( REQUEST_EVENTS, params, GraphRequest::POST, callback, callback_object );
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to edit a facebook event
 * @param params - params bundle
 * e.g.
 *      bundle_add_str(paramsBundle, "name", "NewEvent10072015-11-31");
 *      bundle_add_str(paramsBundle, "id", "1471636316463640");
 * @param[in] callback - callback to notify client when request is completed
 * @param[in] callback_object - user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::PostEditEvent(const char * object_id, bundle * params, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::PostEditEvent");
    if ( !object_id || !params ) {
        Log::debug("FacebookSession::PostEditEvent: NULL arg has been passed");
        return NULL;
    }

    char url[MAX_URL_LEN] = {0};

    Utils::Snprintf_s(url, MAX_URL_LEN, "/%s?", object_id);

    GraphRequest * request = new GraphRequest( url, params, GraphRequest::POST, callback, callback_object );
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to invite friends to event
 * @param object_id - object - id of event
 * @param params - params bundle
 * e.g.
 *      bundle_add_str(paramsBundle, "users", "100000392273904, 100003295217975");
 * @param[in] callback - callback to notify client when request is completed
 * @param[in] callback_object - user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::PostInviteFriendsToEvent(const char * object_id, bundle * params, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::PostInviteFriendsToEvent");
    if ( !object_id || !params ) {
        Log::debug("FacebookSession::PostInviteFriendsToEvent: NULL arg has been passed");
        return NULL;
    }
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, "/%s%s", object_id, REQUEST_INVITED);

    GraphRequest * request = new GraphRequest( url, params, GraphRequest::POST, callback, callback_object );
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to delete an event
 * @param object_id - object - id of event
 * @param[in] callback - callback to notify client when request is completed
 * @param[in] callback_object - user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::DeleteEvent(const char * object_id, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::DeleteEvent");
    if ( !object_id ) {
        Log::debug("FacebookSession::DeleteEvent: NULL arg has been passed");
        return NULL;
    }
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, "/%s", object_id);

    GraphRequest * request = new GraphRequest(url, NULL, GraphRequest::DELETE, callback, callback_object);
    request->ExecuteAsync();
    return request;
}


GraphRequest * FacebookSession::GetLastFeedPost(const char * object_id, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetLastFeedPost");
    if (!object_id) {
        Log::debug("FacebookSession::GetLastFeedPost: NULL arg has been passed");
        assert( false );
        return NULL;
    }

    std::stringstream url;
    url << "/" << object_id << "/feed?limit=1&fields=";
    HomeProvider::GetInstance()->GenPostRequestFields(url);

    GraphRequest * request = new GraphRequest(url.str().c_str(), NULL, GraphRequest::GET, callback, callback_object, "/v2.4");
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to get event containing the search string
 * @param searchString - string which should be part of event's name
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::SearchEvents(const char * searchString, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::SearchEvents");
    if ( !searchString ) {
        Log::debug("FacebookSession::SearchEvents: NULL arg has been passed");
        return NULL;
    }
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, "%s%s%s%s", BATCH_REQUEST_START_EVENTS_REQUEST, REQUEST_SEARCH_EVENTS, searchString, BATCH_REQUEST_END_EVENTS_REQUEST);

    return DoBatchRequest(url, callback, callback_object);
}

/**
 * @brief This method should be invoked to get birthday events
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::GetBirthdayEvents(void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetBirthdayEvents");
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, "%s", REQUEST_BIRTHDAY_EVENTS);

    GraphRequest * request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object);
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to get event actions count
 * @param eventId - string which should be part of event's name
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::GetEventActionsCount(const char * eventid, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetEventActionsCount");
    if (!eventid) {
        Log::debug("FacebookSession::GetEventActionsCount: NULL arg has been passed");
        return NULL;
    }
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, "/%s%s", eventid, REQUEST_GET_EVENT_ACTIONS_COUNT);

    Log::debug("FacebookSession::GetEventActionsCount, url is: %s", url);

    GraphRequest * request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object);
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to change a status of event on which you are invited (not own event)
 * @param object_id - event object to be modified
 * @param status - the new status of event
 *     declined
 *     attending
 *     maybe
 * @param[in] callback - callback to notify client when request is completed
 * @param[in] callback_object - user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::ChangeEventStatus(const char * object_id, const char * status, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::ChangeEventStatus, object_id = %s, status =%s", object_id, status);
    if ( !object_id || !status ) {
        Log::debug("FacebookSession::ChangeEventStatus: NULL arg has been passed");
        return NULL;
    }
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, "/%s/%s", object_id, status);

    GraphRequest * request = new GraphRequest( url, NULL, GraphRequest::POST, callback, callback_object );
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to get the status count of the event
 * @param object_id - event id
 * @param type - the required status count of the event
 *     declined_count
 *     attending_count
 *     maybe_count
 *     noreply_count
 * @param[in] callback - callback to notify client when request is completed
 * @param[in] callback_object - user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::GetStatusCountOfEvent(const char * object_id, const char * statusCount, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::getStatusCountOfEvent, object_id = %s, statusCount =%s", object_id, statusCount);
    if ( !object_id || !statusCount ) {
        Log::debug("FacebookSession::GetStatusCountOfEvent: NULL arg has been passed");
        return NULL;
    }
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, "/%s/%s%s", object_id, REQUEST_FIELDS, statusCount);

    GraphRequest * request = new GraphRequest( url, NULL, GraphRequest::GET, callback, callback_object );
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to get groups
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::GetGroups(void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetGroups");
    GraphRequest * request = new GraphRequest( REQUEST_GROUPS, NULL, GraphRequest::GET, callback, callback_object );
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to get groups list
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::GetGroupsList(void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("#### FacebookSession::GetGroupsList");
    GraphRequest * request = new GraphRequest( REQUEST_GET_GROUPS_LIST, NULL, GraphRequest::GET, callback, callback_object );
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to get group details information
 * @param group_id - id of group
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::GetGroupDetails(const char * groupId, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetGroupDetails");
    if ( !groupId ) {
        Log::debug("FacebookSession::GetGroupDetails: NULL arg has been passed");
        return NULL;
    }
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, "/%s?fields=cover,description,name,owner,privacy,member_request_count,membership_state,members.limit(0).summary(true),picture", groupId);
    Log::debug(url);

    GraphRequest * request = new GraphRequest( url, NULL, GraphRequest::GET, callback, callback_object );
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to join to a group
 * @param group_id - id of group
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::JoinToGroup(const char * group_id, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::JoinToGroup");
    if ( !group_id ) {
        Log::debug("FacebookSession::JoinToGroup: NULL arg has been passed");
        return NULL;
    }
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, REQUEST_GROUP_ACTION, group_id, Config::GetInstance().GetUserId().c_str());

    GraphRequest * request = new GraphRequest( url, NULL, GraphRequest::POST, callback, callback_object );
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to cancel join request or leave a group
 * @param group_id - id of group
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::CancelGroup(const char * group_id, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::CancelGroup");
    if ( !group_id ) {
        Log::debug("FacebookSession::CancelGroup: NULL arg has been passed");
        return NULL;
    }
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, REQUEST_GROUP_ACTION, group_id, Config::GetInstance().GetUserId().c_str());

    GraphRequest * request = new GraphRequest( url, NULL, GraphRequest::DELETE, callback, callback_object );
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to get albums
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::GetAlbums(void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetAlbums");
    GraphRequest * request = new GraphRequest( REQUEST_ALBUMS, NULL, GraphRequest::GET, callback, callback_object );
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to delete an album
 * @param object_id - object - id of album
 * @param[in] callback - callback to notify client when request is completed
 * @param[in] callback_object - user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::DeleteAlbum(const char * album_id, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::DeleteAlbum");
    if ( !album_id ) {
        Log::debug("FacebookSession::DeleteAlbum: NULL arg has been passed");
        return NULL;
    }
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, "/%s", album_id);

    GraphRequest * request = new GraphRequest( url, NULL, GraphRequest::DELETE, callback, callback_object );
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to rename album
 * @param params - params bundle
 * e.g.
 *      bundle* paramsBundle;
 *      paramsBundle = bundle_create();
 *      bundle_add_str(paramsBundle, "name", "NewAlbumName");
 * @param[in] callback - callback to notify client when request is completed
 * @param[in] callback_object - user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::PostRenameAlbum(const char * album_id, bundle * params, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::PostRenameAlbum");
    if ( !album_id || !params ) {
        Log::debug("FacebookSession::PostRenameAlbum: NULL arg has been passed");
        return NULL;
    }
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, "/%s", album_id);

    GraphRequest * request = new GraphRequest( url, params, GraphRequest::POST, callback, callback_object );
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to create an album
 * @param params - params bundle
 * e.g.
 *      bundle* paramsBundle;
 *      paramsBundle = bundle_create();
 *      bundle_add_str(paramsBundle, "name", "NewAlbumName");
 * @param[in] callback - callback to notify client when request is completed
 * @param[in] callback_object - user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::PostCreateAlbum(bundle * params, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::PostCreateAlbum");
    if ( !params ) {
        Log::debug("FacebookSession::PostCreateAlbum: NULL arg has been passed");
        return NULL;
    }
    GraphRequest * request = new GraphRequest( REQUEST_ALBUMS, params, GraphRequest::POST, callback, callback_object );
    request->ExecuteAsync();
    return request;
}

GraphRequest *FacebookSession::GetAlbumDetails(const char *albumId, void (*callback) (void *, char*, int), void * callback_object) {
    Log::debug("FacebookSession::GetAlbumDetails()");
    char url[MAX_URL_LEN] = {0};

    Log::debug("FacebookSession::GetAlbumDetails() for Album Id:%s", albumId);
    Utils::Snprintf_s(url, MAX_URL_LEN, REQUEST_ALBUM_DETAILS, albumId);
    GraphRequest *request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object);
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to add a photo to the album
 * @param params - params bundle
 * e.g.
 *      bundle* paramsBundle;
 *      paramsBundle = bundle_create();
 *      bundle_add_str(paramsBundle, "url", "http://moisad.ua/images/stories/virtuemart/product/aspirin%C2%AE-rose3.jpg");
 *      bundle_add_str(paramsBundle, "caption", "Flowers_in_garden");
 * @param[in] callback - callback to notify client when request is completed
 * @param[in] callback_object - user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::PostAddPhotoToAlbum(const char * album_id, bundle * params, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::PostAddPhotoToAlbum");
    if ( !album_id || !params ) {
        Log::debug("FacebookSession::PostAddPhotoToAlbum: NULL arg has been passed");
        return NULL;
    }
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, "/%s/%s", album_id, REQUEST_PHOTOS);

    GraphRequest * request = new GraphRequest( url, params, GraphRequest::POST, callback, callback_object );
    request->ExecuteAsync();
    return request;
}

GraphRequest *FacebookSession::GetAlbumDetailsForPhoto(const char *photoId, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetAlbumDetailsForPhoto");
    char url[MAX_URL_LEN] = {0};

    Utils::Snprintf_s(url, MAX_URL_LEN, REQUEST_ALBUM_DETAILS_FOR_PHOTO, photoId);

    GraphRequest * request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object, NULL);
    request->ExecuteAsync();
    return request;
}

/** CURRENTLY THIS METHOD DOESN'T WORK EVEN THE RESPONSE RETURNS TRUE
 * @brief This method should be invoked to edit photo caption
 * @param params - params bundle
 * e.g.
 *      bundle* paramsBundle;
 *      paramsBundle = bundle_create();
 *      bundle_add_str(paramsBundle, "name", "NewCaption");//tried with caption field also
 * @param[in] callback - callback to notify client when request is completed
 * @param[in] callback_object - user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::PostEditPhotoCaption(const char * photo_id, bundle * params, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::PostEditPhotoCaption");
    if ( !photo_id || !params ) {
        Log::debug("FacebookSession::PostEditPhotoCaption: NULL arg has been passed");
        return NULL;
    }
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, "/%s", photo_id);

    GraphRequest * request = new GraphRequest( url, params, GraphRequest::POST, callback, callback_object );
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to delete a photo
 * @param photo_id - object - id of photo
 * @param[in] callback - callback to notify client when request is completed
 * @param[in] callback_object - user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::DeletePhoto(const char * photo_id, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::DeletePhoto");
    if ( !photo_id ) {
        Log::debug("FacebookSession::DeletePhoto: NULL arg has been passed");
        return nullptr;
    }
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, "/%s", photo_id);

    GraphRequest * request = new GraphRequest( url, nullptr, GraphRequest::DELETE, callback, callback_object );
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to delete own post
 * @param post_id - id of post
 * @param[in] callback - callback to notify client when request is completed
 * @param[in] callback_object - user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::DeletePost(const char * post_id, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::DeletePost");
    if ( !post_id ) {
        Log::debug("FacebookSession::DeletePost: NULL arg has been passed");
        return NULL;
    }
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, "/%s", post_id);

    GraphRequest * request = new GraphRequest( url, NULL, GraphRequest::DELETE, callback, callback_object );
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to delete own comment
 * @param comment_id - id of comment
 * @param[in] callback - callback to notify client when request is completed
 * @param[in] callback_object - user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::DeleteComment(const char * comment_id, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::DeleteComment");
    if ( !comment_id ) {
        Log::debug("FacebookSession::DeleteComment: NULL arg has been passed");
        return NULL;
    }
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, "/%s", comment_id);

    GraphRequest * request = new GraphRequest( url, NULL, GraphRequest::DELETE, callback, callback_object );
    request->ExecuteAsync();
    return request;
}

GraphRequest * FacebookSession::MarkNotificationRead(void (*callback) (void *, char*, int), void * callback_object, const char * ntf_id)
{
    Log::debug("FacebookSession::MarkNotificationRead");
    if ( !ntf_id ) {
            Log::debug("FacebookSession::MarkNotificationRead: NULL arg has been passed");
            return NULL;
        }

    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, REQUEST_POST_NOTIF_READ,ntf_id );
    GraphRequest * request = new GraphRequest( url, NULL, GraphRequest::POST, callback, callback_object );
    request->ExecuteAsync();
    return request;
}

GraphRequest * FacebookSession::MarkNotificationAsRead(const char * object_id, void (*callback) (void *, char*, int), void * callback_object)
{
    GraphRequest * request = NULL;

    if (object_id) {
        Log::debug(LOG_FACEBOOK_NOTIFICATION, "FacebookSession::MarkNotificationAsRead, object_id = %s", object_id);
        char url[MAX_URL_LEN] = { 0 };
        Utils::Snprintf_s(url, MAX_URL_LEN, REQUEST_POST_NOTIF_READ, object_id);
        request = new GraphRequest(url, NULL, GraphRequest::POST, callback, callback_object);
        request->ExecuteAsync();
    }

    return request;
}

/**
 * @brief This method should be invoked to send batch request
 * @param batch - the json array with batch requests
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */

GraphRequest * FacebookSession::DoBatchRequest(const char * batch, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::DoBatchRequest");

    bundle* paramsBundle;
    paramsBundle = bundle_create();
    bundle_add_str(paramsBundle, "batch", batch);

    GraphRequest * request = new GraphRequest("", paramsBundle, GraphRequest::POST, callback, callback_object);
    request->ExecuteAsync();

    bundle_free(paramsBundle);
    return request;
}

/**
 * @brief This method should be invoked to get friends requests via batch request
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::GetFriendRequestsBatch(void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetBatchFriendRequests");
    return DoBatchRequest(BATCH_REQUEST_FRIENDS_REQUEST, callback, callback_object);
}

/**
 * @brief This method should be invoked to get quick search results
 * @param searchString - string to be found
 * @param filter.Following filters are supported:
 *            ['user','group','page', 'app']
 *            ['group']
 *            ['user']
 *            ['page']
 *            ['app']
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::GetSearchResults(const char * searchString, const char * filter, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetSearchResults");
    char url[MAX_URL_LEN] = {0};

    CURL *curl = curl_easy_init();
    char *output = NULL;
    if(curl) {
      output = curl_easy_escape(curl, searchString, 0);
    }

    Utils::Snprintf_s(url, MAX_URL_LEN, REQUEST_SEARCH, output, filter);
    curl_free(output);
    curl_easy_cleanup(curl);

    GraphRequest * request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object, NULL, GraphRequest::REST);
    request->ExecuteAsync();
    return request;
}

GraphRequest * FacebookSession::GetEventProfileInfo(const char * id, void (*callback) (void *, char*, int), void * callback_object)
{
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, REQUEST_EVENT_PROFILE, id);
    GraphRequest * request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object, "/v2.4", GraphRequest::GRAPH);
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to get mobile zero campain info
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::GetMobileZeroCampain(void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetMobileZeroCampain");
    GraphRequest * request = new GraphRequest( REQUEST_MOBILE_ZERO_CAMPAIN, NULL, GraphRequest::GET, callback, callback_object, NULL, GraphRequest::REST);
    request->ExecuteAsync();
    return request;
}
/////////////////////////////////////////////////////////////////////////////////////
//  Internal Callbacks region
/////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief This is internal callback which is called during login procedure when url redirection event happens
 * @param[in] data - The user data to be passed to the callback function
 * @param[in] obj - object which generates event
 * @param[in] event_info - event information
 */
void FacebookSession::on_url_changed(void *data, Evas_Object *obj, void *event_info)
{
    const char * url = ewk_view_url_get(obj);
    FacebookSession* fb_session = reinterpret_cast<FacebookSession*>(data);
    LoginStatus status = fb_session->ProceedUrl(url);
    fb_session->mLoginCallback(fb_session->mLoginCallbackObject, status);
}

/////////////////////////////////////////////////////////////////////////////////////
//  Helpers region
/////////////////////////////////////////////////////////////////////////////////////
/**
 * @brief This method examines redirection url for access_token presence
 * If token is found, saves it to mAccessToken
 * @param[in] url - redirection url
 * @return - login status (LoginOk if access_token is found)
 */
FacebookSession::LoginStatus FacebookSession::ProceedUrl(const char * url)
{
    Log::debug("ProceedUrl = %s", url);
    LoginStatus res = LoginInProgress;

    const char * buf = strdup(url);

    const char* token_string = "access_token";
    const char* expires_in = "expires_in";
    const char* error_message = "error_message";

    const char *token_begin = strstr(buf, token_string);
    const char *token_end = strstr(buf, expires_in);


    if (token_begin)
    {
        Log::debug("Access Token found");
        token_begin += (strlen(token_string) + 1);

        if (token_end) {
            Config::GetInstance().SetAccessToken(token_begin);

            Log::debug("token = %s", Config::GetInstance().GetAccessToken().c_str());

            res = LoginOk;
        }
    }
    else
    {
        //This is a quick fix to handle error state during login.
        //Should be reworked more carefully
        const char *error_message_found = strstr(buf, error_message);
        if (error_message_found)
        {
            res = LoginFailed;
            Log::debug("Login Failed, error_message =  %s ", error_message_found);
        }
    }

    free((void*) buf);
    return res;
}

GraphRequest * FacebookSession::PostSeenRequests()
{
    Log::debug("FacebookSession::PostSeenRequests");

    GraphRequest * request = new GraphRequest(REQUEST_SEEN_FRIEND_REQUESTS, NULL, GraphRequest::POST, NULL, NULL, "/v1.0");
    request->ExecuteAsync();
    return request;
}

GraphRequest * FacebookSession::GetSpecificStory(void (*callback) (void *, char*, int), void * callback_object, const char * id)
{
    Log::debug("FacebookSession::GetSpecificStory");
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, REQUEST_SPECIFIC_STORY, id);
    GraphRequest * request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object, NULL);
    request->ExecuteAsync();
    return request;
}

GraphRequest * FacebookSession::GetObjectType(void (*callback) (void *, char*, int), void * callback_object, const char * id, bool isLinkNeeded)
{
    Log::debug("FacebookSession::GetObjectType");
    char url[MAX_URL_LEN] = {0};
    if(isLinkNeeded)
    {
        Utils::Snprintf_s(url, MAX_URL_LEN, REQUEST_OBJECT_TYPE, id);
    }
    else
    {
        Utils::Snprintf_s(url, MAX_URL_LEN, REQUEST_OBJECT_TYPE_WITHOUT_LINK, id);
    }
    GraphRequest * request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object, NULL);
    request->ExecuteAsync();
    return request;
}

GraphRequest *FacebookSession::GetPostDetails(const char *postId, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetPostDetails");
    char url[MAX_URL_LEN] = {0};

    Utils::Snprintf_s(url, MAX_URL_LEN, REQUEST_GET_POST_DETAILS, postId);

    GraphRequest * request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object, NULL);
    request->ExecuteAsync();
    return request;
}

GraphRequest *FacebookSession::GetPhotoDetails(const char *photoId, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetPhotoDetails");
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, REQUEST_GET_PHOTO_DETAILS, photoId);
    GraphRequest * request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object, NULL);
    request->ExecuteAsync();
    return request;
}

GraphRequest *FacebookSession::GetUserDetails(const char *userId, void (*callback) (void *, char*, int), void * callback_object)
{
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, REQUEST_GET_USER_DETAILS, userId);
    GraphRequest * request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object, NULL);
    request->ExecuteAsync();
    return request;
}

/**
 * @brief This method should be invoked to Post Details via Batch request
 * @param callback - callback to notify client when request is completed
 * @param callback_object- user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::GetPostsDetailsBatch(void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetPostsDetailsBatch");

    char url[MAX_URL_LEN] = {0};

    Utils::Snprintf_s(url, MAX_URL_LEN, BATCH_REQUEST_GET_POST_DETAILS_REQUEST, REQUEST_HOME_ITEMS_LIMIT, END_BATCH_REQUEST_GET_POST_DETAILS);

    return DoBatchRequest(url, callback, callback_object);
}

GraphRequest * FacebookSession::PostSetLastLocation(double latitude, double longitude, time_t timestamp, int accuracy, void (*callback) (void *, char*, int), void * callback_object) {
    Log::debug("LocationManager", "FacebookSession::PostSetLastLocation");

    GraphRequest *request = NULL;

    if (Config::GetInstance().GetAccessToken().empty()) {
        //we are not registered yet.
        return request;
    }

    bundle* paramsBundle = bundle_create();
    bundle_add_str(paramsBundle, REST_APP_KEY_KEY, Config::GetApplicationId());
    bundle_add_str(paramsBundle, REST_VERSION_KEY, REST_VERSION);
    bundle_add_str(paramsBundle, REST_FORMAT_KEY, REST_FORMAT_JSON);
    bundle_add_str(paramsBundle, REST_METHOD_KEY, REST_METHOD_SET_LAST_LOCATION);
    bundle_add_str(paramsBundle, REST_UID_KEY, Config::GetInstance().GetUserId().c_str());
    char coords[SHORT_BUFFER_SIZE] = {0,};
    Utils::Snprintf_s(coords, SHORT_BUFFER_SIZE, REST_COORDS_FORMAT, latitude, longitude, timestamp);

    bundle_add_str(paramsBundle, REST_COORDS_KEY, coords);

    char sigBuf[MAX_URL_LEN];
    int len = 0;
    char *sig = NULL;

    len = Utils::Snprintf_s(sigBuf, MAX_URL_LEN, "%s=%s%s=%s%s=%s%s=%s%s=%s%s=%s%s=%s%s",
            REST_APP_KEY_KEY, Config::GetApplicationId(),
            REST_COORDS_KEY, coords,
            REST_FORMAT_KEY, REST_FORMAT_JSON,
            REST_METHOD_KEY, REST_METHOD_SET_LAST_LOCATION,
            REST_SESSION_KEY_KEY, Config::GetInstance().GetSessionKey().c_str(),
            REST_UID_KEY, Config::GetInstance().GetUserId().c_str(),
            REST_VERSION_KEY, REST_VERSION,
            Config::GetInstance().GetSessionSecret().c_str());

    sig = Utils::str2md5(sigBuf, len);
    bundle_add_str(paramsBundle, REST_SIG_KEY, sig);
    free(sig);

    request = new GraphRequest(RESTSERVER_URL, paramsBundle, GraphRequest::POST, callback, callback_object, NULL, GraphRequest::REST);
    request->ExecuteAsync();
    bundle_free(paramsBundle);

    return request;
}

GraphRequest * FacebookSession::PostLinkPreview(const char *link, void (*callback) (void *, char*, int), void * callback_object) {
    Log::debug("FacebookSession::PostLinkPreview");

    GraphRequest *request = NULL;

    if (Config::GetInstance().GetAccessToken().empty()) {
        //we are not registered yet.
        return request;
    }

    bundle* paramsBundle = bundle_create();
    bundle_add_str(paramsBundle, "client_country_code", "US");
    bundle_add_str(paramsBundle, "method", "links.preview");
    bundle_add_str(paramsBundle, "fb_api_req_friendly_name", "links.preview");
    bundle_add_str(paramsBundle, "fb_api_caller_class", "com.facebook.tizen");
    bundle_add_str(paramsBundle, "format", "json");
    bundle_add_str(paramsBundle, "url", link);

    request = new GraphRequest("/method/links.preview", paramsBundle, GraphRequest::POST, callback, callback_object, NULL, GraphRequest::REST);
    request->ExecuteAsync();
    bundle_free(paramsBundle);

    return request;
}

GraphRequest * FacebookSession::GetUserProfileBaseInfo(const char *id, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug(LOG_FACEBOOK_USER, "FacebookSession::GetUserProfileInfo");

    std::stringstream url;
    url << "/" << id << REQUEST_USER_BASE_INFO;
    GraphRequest * request = new GraphRequest(url.str().c_str(), NULL, GraphRequest::GET, callback, callback_object);
    request->ExecuteAsync();
    return request;
}

GraphRequest * FacebookSession::GetMyAvatar(int width, int height, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetMyAvatar");
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, REQUEST_MY_AVATAR, width, height);
    GraphRequest * request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object);
    request->ExecuteAsync();
    return request;
}

GraphRequest *FacebookSession::IsObjectExists(const char *id, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::IsObjectExists");
    char url[MAX_URL_LEN] = {0};

    Utils::Snprintf_s(url, MAX_URL_LEN, REQUEST_IS_OBJECT_EXISTS, id);

    GraphRequest * request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object, NULL);
    request->ExecuteAsync();
    return request;
}

GraphRequest *FacebookSession::GetPageDetails(const char *id, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::GetPageDetails");
    char url[MAX_URL_LEN] = {0};
    Utils::Snprintf_s(url, MAX_URL_LEN, "/%s?fields=link,category", id);
    GraphRequest * request = new GraphRequest(url, NULL, GraphRequest::GET, callback, callback_object, NULL);
    request->ExecuteAsync();
    return request;
}

GraphRequest *FacebookSession::CreatePhotoTags(const char * id, Eina_List * tagList, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::CreatePhotoTags");
    std::stringstream url;
    url << "/" << id << "/tags";
    std::stringstream list;
    Eina_List *l;
    void *listData;
    list << "[";
    EINA_LIST_FOREACH(tagList, l, listData) {
        PhotoTag * tag = static_cast<PhotoTag*>(listData);
        if(!tag->mTaggedUserId.empty()){
            list << "{\"tag_uid\":\"" << tag->mTaggedUserId ;
        } else {
            list << "{\"tag_text\":\"" << Utils::ReplaceString(tag->mTaggedUserName ,"\"", "\\\"");
        }
        list << "\",\"x\":" << tag->mX << ",\"y\":" << tag->mY << ",},";
    }
    list << "]";
    bundle *param;
    param = bundle_create();
    char *escape = Utils::escape(list.str().c_str());
    bundle_add_str(param, "tags", escape);
    free(escape);
    GraphRequest * request = new GraphRequest( url.str().c_str(), param, GraphRequest::POST, callback, callback_object );
    request->ExecuteAsync();
    bundle_free(param);
    return request;
}

GraphRequest *FacebookSession::DeletePhotoTags(const char * id, Eina_List * tagList, void (*callback) (void *, char*, int), void * callback_object)
{
    Log::debug("FacebookSession::DeletePhotoTags");
    std::stringstream url;
    url << "/" << id << "/tags?tags=[";
    Eina_List *l; void *listData;
    EINA_LIST_FOREACH(tagList, l, listData) {
        PhotoTag * tag = static_cast<PhotoTag*>(listData);
        if (!tag->mTaggedUserId.empty()) {
            url << "{\"tag_uid\":\"" << tag->mTaggedUserId << "\"";
        } else {
            url << "{\"tag_text\":\"" << Utils::ReplaceString(tag->mTaggedUserName ,"\"", "\\\"") << "\",\"x\":" << tag->mX << ",\"y\":" << tag->mY;
        }
        url << ",},";
    }
    url << "]";
    GraphRequest * request = new GraphRequest( url.str().c_str(), nullptr, GraphRequest::DELETE, callback, callback_object );
    request->ExecuteAsync();

    return request;
}

GraphRequest * FacebookSession::UpdatePostWithPhotos(bundle * params, Eina_List * mediaList, void (*callback) (void *, char*, int), void * callback_object, const char *postId )
{
    Log::debug("FacebookSession::UpdatePostWithPhotos");

    GraphRequest * request = NULL;
    std::stringstream photosDescription;

    photosDescription << "[";
    if (params) {
        std::string postParam = GraphRequest::PrepareParamsFromBundle(params);
        postParam = Utils::ReplaceString(postParam, "\"", "\\\"");

        photosDescription << "{ \"method\":\"POST\","
                "\"body\":\"" << postParam << "\","
                "\"relative_url\": \"" << REQUEST_POST_PHOTO_GRAPH_VERSION <<
                postId << "\" }";
    }

    int mediaCount = eina_list_count(mediaList);
    if (mediaCount) {
        int photoIndex = 0;
        if(params) {
            photosDescription << ",";
        }
        Eina_List * listItem;
        void * listData;
        EINA_LIST_FOREACH(mediaList, listItem, listData) {
            PostComposerMediaData * mediaData = static_cast<PostComposerMediaData *>(listData);
            if(mediaData && mediaData->GetMediaId()) {
                photosDescription << "{ \"method\":\"POST\",";
                if(mediaData->GetTextToCaption()) {
                    char *escape = Utils::escape(mediaData->GetTextToCaption());
                    photosDescription << "\"body\":\"&name=" << escape << "";
                    free(escape);
                }
                photosDescription << "\","
                        "\"relative_url\": \"" << REQUEST_POST_PHOTO_GRAPH_VERSION <<
                        mediaData->GetMediaId() << "\" }";

                photoIndex++;
                if(photoIndex < mediaCount) {
                    photosDescription << ",";
                }
            }
        }
    }
    photosDescription << "]";

    Log::debug("FacebookSession::UpdatePostWithPhotos = photosDescription %s", photosDescription.str().c_str());
    bundle* batchBundle = bundle_create();
    bundle_add_str(batchBundle, "batch", photosDescription.str().c_str());

    request = new GraphRequest( "", NULL, GraphRequest::POST_MULTIPART, callback, callback_object, NULL, GraphRequest::GRAPH, batchBundle );
    if(request) {
        request->ExecuteAsync();
    }
    bundle_free(batchBundle);
    return request;
}

/**
 * @brief This method is used to read feature flags from GK server
 * @param[in] gkNames - names of gk flags
 * @param[in] callback - callback to notify client when request is completed
 * @param[in] callback_object - user data to pass in callback
 * @return started request
 */
GraphRequest * FacebookSession::GetFeatureState(const char* gkNames, void (*callback) (void*, char*, int), void* callback_object) {
    Log::debug("FacebookSession::GetFeatureState");

    GraphRequest* request = nullptr;

    if (Config::GetInstance().GetAccessToken().empty()) {
        // we are not registered yet
        return request;
    }

    bundle* params = bundle_create();
    bundle_add_str(params, REST_APP_KEY_KEY, Config::GetApplicationId());
    bundle_add_str(params, REST_FORMAT_KEY, REST_FORMAT_JSON);
    bundle_add_str(params, REST_VERSION_KEY, REST_VERSION);
    bundle_add_str(params, "query", gkNames);
    char* queryHash = Utils::ComputeHash(gkNames);
    bundle_add_str(params, "query_hash", queryHash);

    char* duid = nullptr;
    char* hashId = nullptr;
    if (SYSTEM_INFO_ERROR_NONE == system_info_get_platform_string("http://tizen.org/system/tizenid", &duid)) {
        hashId = Utils::ComputeHash(duid);
        bundle_add_str(params, "hash_id", hashId);
    }

    char url[SHORT_BUFFER_SIZE] = {0,};
    Utils::Snprintf_s(url, SHORT_BUFFER_SIZE, REQUEST_GATEKEEPER_CONFIG);
    request = new GraphRequest(url, params, GraphRequest::POST, callback, callback_object, nullptr, GraphRequest::REST);
    request->ExecuteAsync();

    bundle_free(params);
    free(queryHash);
    free(hashId);
    free(duid);

    return request;
}
