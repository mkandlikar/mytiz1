#include <efl_extension.h>

#include "BriefPostInfo.h"
#include "FbRespondBriefPostInfo.h"
#include "Log.h"
#include "ParserWrapper.h"
#include "jsonutilities.h"

FbRespondBriefPostInfo::FbRespondBriefPostInfo( JsonNode* node ) : FbRespondBase( node )
{
    Log::info("FbRespondBriefPostInfo::FbRespondBriefPostInfo()");
    JsonObject * responseNode = json_node_get_object(node);
    m_BriefPostInfoList = NULL;
    if (responseNode) {
        GList *keysList = json_object_get_members(responseNode);
        guint size = json_object_get_size(responseNode);
        for (int j = 0; j < size; ++j, keysList = g_list_next(keysList)) {
            gchar *keyName = (gchar *) keysList->data;
            JsonObject* member = json_object_get_object_member(responseNode, keyName);
            if (member) {
                BriefPostInfo *info = new BriefPostInfo(member);
                m_BriefPostInfoList = eina_list_append(m_BriefPostInfoList, info);
            }
        }
        if (keysList) {
            g_list_free(keysList);
        }
    }
}

FbRespondBriefPostInfo::~FbRespondBriefPostInfo()
{
    Log::info("FbRespondBriefPostInfo::~FbRespondBriefPostInfo()");
    if (m_BriefPostInfoList) {
        void * listData;
        EINA_LIST_FREE( m_BriefPostInfoList, listData )
        {
            static_cast<BriefPostInfo*>(listData)->Release();
        }
    }
}

Eina_List* FbRespondBriefPostInfo::GetBriefPostInfoList()
{
    return m_BriefPostInfoList;
}

FbRespondBriefPostInfo* FbRespondBriefPostInfo::createFromJson(char* data)
{
    JsonObject *object = NULL;
    JsonParser *parser = openJsonParser( data, &object );
    ParserWrapper wr( parser );
    JsonNode* rootNode = json_parser_get_root( parser );
    return rootNode ? new FbRespondBriefPostInfo( rootNode ) : NULL;
}

Eina_List* FbRespondBriefPostInfo::RemoveDataFromList(void *data)
{
    m_BriefPostInfoList = eina_list_remove(m_BriefPostInfoList, data);
    return m_BriefPostInfoList;
}
