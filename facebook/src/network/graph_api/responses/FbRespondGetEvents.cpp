#include <efl_extension.h>

#include "Common.h"
#include "Event.h"
#include "FbRespondGetEvents.h"
#include "jsonutilities.h"
#include <json-glib/json-glib.h>

/**
 * @brief Construct FbRespondGetEvents from JsonObject
 * @param[in] object - Json Object, from which FbRespondGetEvents will be constructed
 */
FbRespondGetEvents::FbRespondGetEvents(JsonObject* object) : FbRespondBase(object)
{
    JsonArray * events_array = json_object_get_array_member(object, DATA);
    mEventsList = nullptr;
    if (events_array) {
        int len = json_array_get_length(events_array);
        for (int i = 0; i < len; i++) {
            JsonObject* el = json_array_get_object_element(events_array, i);
            if (el) {
                Event* eventEl = new Event(el);
                if (eventEl) {
                    mEventsList = eina_list_append(mEventsList, eventEl);
                }
            }
        }
    }
}

/**
 * @brief Destruction
 */
FbRespondGetEvents::~FbRespondGetEvents()
{
    if (mEventsList) {
        void * listData;
        EINA_LIST_FREE(mEventsList, listData)
        {
            static_cast<Event*>(listData)->Release();
        }
    }
}

/**
 * @brief Creates FbRespondGetEvents object from JSON formatted text
 * @param data - JSON formatted text
 * @return - created FbRespondGetEvents object
 */
FbRespondGetEvents* FbRespondGetEvents::createFromJson(char* data)
{
    FbRespondGetEvents * res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject ? new FbRespondGetEvents(rootObject) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}

Eina_List* FbRespondGetEvents::RemoveDataFromList(void *data)
{
    mEventsList = eina_list_remove(mEventsList, data);
    return mEventsList;
}
