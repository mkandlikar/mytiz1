#include <efl_extension.h>
#include <json-glib/json-glib.h>
#include <stdlib.h>

#include "Common.h"
#include "FbRespondGetSubscribers.h"
#include "ParserWrapper.h"
#include "UserShort.h"
#include "jsonutilities.h"
/**
 * @brief Construct FbRespondGetSubscribers from JsonObject
 * @param object - Json Object, from which FbRespondGetSubscribers will be constructed
 */
FbRespondGetSubscribers::FbRespondGetSubscribers(JsonObject* object) : FbRespondBase(object)
{
    JsonArray * subscribers_array = json_object_get_array_member(object, "data");
    mSubscribersList = nullptr;
    if (subscribers_array) {
        int len = json_array_get_length(subscribers_array);

        for (int i = 0; i < len; i++) {
            JsonObject* el = json_array_get_object_element(subscribers_array, i);
            UserShort* subscriberEl = new UserShort(el);
            mSubscribersList = eina_list_append(mSubscribersList, subscriberEl);
        }
    }
}

/**
 * @brief Destructor
 */
FbRespondGetSubscribers::~FbRespondGetSubscribers()
{
    if (mSubscribersList) {
        void * listData;
        EINA_LIST_FREE(mSubscribersList, listData)
        {
            static_cast<UserShort*>(listData)->Release();
        }
    }
}

/**
 * @brief Creates FbRespondGetFriends object fron JSON formatted text
 * @param[in] data - JSON formatted text
 * @return - created FbRespondGetFriends object
 */
FbRespondGetSubscribers* FbRespondGetSubscribers::createFromJson(char* data)
{
    FbRespondGetSubscribers * res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);
    ParserWrapper wr(parser);
    res = rootObject ? new FbRespondGetSubscribers(rootObject) : nullptr;

    return res;
}

