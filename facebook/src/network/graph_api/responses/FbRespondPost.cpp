#include <malloc.h>
#include <stddef.h>

#include "Common.h"
#include "FbRespondPost.h"
#include "jsonutilities.h"

/**
 * @brief Construct FbRespondPost object from JsonObject
 * @param[in] object - Json Object, from which FbRespondBase will be constructed
 */
FbRespondPost::FbRespondPost(JsonObject * object) : FbRespondBase(object)
{
    mId = GetStringFromJson(object, "id");
}

/**
 * @brief Destructor
 */
FbRespondPost::~FbRespondPost()
{
    free((void*) mId);
}

/**
 * @brief Creates FbRespondPost object fron JSON formatted text
 * @param[in] data - JSON formatted text
 * @return - created FbRespondBase object
 */
FbRespondPost* FbRespondPost::createFromJson(const char* data)
{
    FbRespondPost * res = nullptr;

    JsonObject* rootObject = nullptr;
    JsonParser* parser = openJsonParser(data, &rootObject);
    res = rootObject ? new FbRespondPost(rootObject) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}

FbResponsePhotoPost::FbResponsePhotoPost(JsonObject * object) : FbRespondPost(object), mNotSentPhotos(nullptr) {
}

FbResponsePhotoPost::~FbResponsePhotoPost() {
    mNotSentPhotos = eina_list_free(mNotSentPhotos);
}

FbResponsePhotoPost* FbResponsePhotoPost::createFromJson(const char* data) {
    FbResponsePhotoPost *res = nullptr;
    JsonArray* rootArray = nullptr;

    if (!data) {
        return res;
    }

    JsonParser *jsonParser = json_parser_new ();
    GError *error = nullptr;

    if (jsonParser && json_parser_load_from_data(jsonParser, data, -1, &error)) {
        JsonNode* root = json_parser_get_root(jsonParser);
        if(root) {
            rootArray = json_node_get_array(root);
        }
    } else {
        g_error_free (error);
    }

    if (rootArray) {
        int len = json_array_get_length(rootArray);
        for (int i = 0; i < len; ++i) {
            JsonObject* el = json_array_get_object_element(rootArray, i);
            if (el && i == 0) {
                const char *body = json_object_get_string_member(el, "body");
                if (body) {
                    JsonObject* rootObject = nullptr;
                    JsonParser* parser = openJsonParser(body, &rootObject);
                    res = rootObject ? new FbResponsePhotoPost(rootObject) : nullptr;
                    g_object_unref(parser);
                }
                if (!res) {
                    break;
                }
            } else if (i == 0) {
                break;
            } else if (i > 0 && el) {
                const char *body = json_object_get_string_member(el, "body");
                if (body) {
                    JsonObject* rootObject = nullptr;
                    JsonParser* parser = openJsonParser(body, &rootObject);
                    FbResponsePhotoPost *responseBody = rootObject ? new FbResponsePhotoPost(rootObject) : nullptr;
                    g_object_unref(parser);
                    if (!responseBody->mSuccess) {
                        res->mNotSentPhotos = eina_list_append(res->mNotSentPhotos, (void *)(i - 1)); //-1 because the first el in the array is Post itself.
                    }
                    delete responseBody;
                }
            } else if (i > 0 && json_array_get_null_element(rootArray, i)) {
                res->mNotSentPhotos = eina_list_append(res->mNotSentPhotos, (void *)(i - 1));
            }
        }
    }

    g_object_unref(jsonParser);
    return res;
}
