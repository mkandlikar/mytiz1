#include <efl_extension.h>

#include "Common.h"
#include "Event.h"
#include "FbRespondGetEvent.h"
#include "jsonutilities.h"
#include <json-glib/json-glib.h>

/**
 * @brief Construct FbRespondGetEvents from JsonObject
 * @param[in] object - Json Object, from which FbRespondGetEvents will be constructed
 */
FbRespondGetEvent::FbRespondGetEvent(JsonObject* object) : FbRespondBase(object)
{
    mEventsList = nullptr;
    Event* eventEl = new Event(object);
    mEventsList = eina_list_append(mEventsList, eventEl);
}

/**
 * @brief Destruction
 */
FbRespondGetEvent::~FbRespondGetEvent()
{
    if ( mEventsList ) {
        void * listData;
        EINA_LIST_FREE(mEventsList, listData) {
            static_cast<Event*>(listData)->Release();
        }
    }
}

/**
 * @brief Creates FbRespondGetEvents object from JSON formatted text
 * @param data - JSON formatted text
 * @return - created FbRespondGetEvents object
 */
FbRespondGetEvent* FbRespondGetEvent::createFromJson(char* data)
{
    FbRespondGetEvent * res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject ? new FbRespondGetEvent(rootObject) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}

Eina_List* FbRespondGetEvent::RemoveDataFromList(void *data)
{
    mEventsList = eina_list_remove(mEventsList, data);
    return mEventsList;
}
