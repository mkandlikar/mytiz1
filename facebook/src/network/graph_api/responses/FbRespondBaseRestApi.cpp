#include <malloc.h>

#include "Common.h"
#include "FbRespondBaseRestApi.h"
#include "jsonutilities.h"
#include "Log.h"
#include "ParserWrapper.h"

/**
 * @brief Construct FbRespondBaseRestApi object from JsonNode
 * @param rootNode - JsonNode, from which FbRespondBaseRestApi will be constructed
 */
FbRespondBaseRestApi::FbRespondBaseRestApi(JsonNode* rootNode)
{
    JsonObject * object = json_node_get_object(rootNode);

    mErrorCode = 0;
    mErrorMessage = nullptr;
    if (object) {
        mErrorCode = json_object_get_int_member(object, "error_code");
        mErrorMessage = GetStringFromJson(object, "error_msg");
    }
}

/**
 * @brief Destructor
 */
FbRespondBaseRestApi::~FbRespondBaseRestApi()
{
    free((void*) mErrorMessage);
}

/**
 * @brief Creates FbRespondGetFindFriend object fron JSON formatted text
 * @param data - JSON formatted text
 * @return - created FbRespondGetFindFriend object
 */
FbRespondBaseRestApi* FbRespondBaseRestApi::createFromJson(const char* data)
{
    Log::info("FbRespondBaseRestApi::createFromJson");
    FbRespondBaseRestApi * res = NULL;

    JsonParser * parser = openJsonParser(data, nullptr);
    ParserWrapper wr( parser );
    //We use root node here, because REST API respond can have array instead object as root element
    JsonNode* rootNode = json_parser_get_root(parser);
    res = rootNode ? new FbRespondBaseRestApi(rootNode) : nullptr;
    return res;
}

