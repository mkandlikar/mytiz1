#include <efl_extension.h>

#include "Common.h"
#include "FbRespondGetPhotos.h"
#include "Photo.h"
#include "jsonutilities.h"

/**
 * @brief Construct FbRespondGetPhotos from JsonObject
 * @param[in] object - Json Object, from which FbRespondGetPhotos will be constructed
 */
FbRespondGetPhotos::FbRespondGetPhotos(JsonObject* object):FbRespondBase(object)
{
    JsonArray * photos_array = json_object_get_array_member(object, "data");
    mPhotosList = nullptr;
    if (photos_array) {
        int len = json_array_get_length(photos_array);

        for (int i = 0; i < len; i++) {
            JsonObject* el = json_array_get_object_element(photos_array, i);
            if (el) {
                Photo* photoEl = new Photo(el);
                mPhotosList = eina_list_append(mPhotosList, photoEl);
            }
        }
    }
}

/**
 * @brief Destruction
 */
FbRespondGetPhotos::~FbRespondGetPhotos()
{
    if (mPhotosList) {
        void * listData;
        EINA_LIST_FREE( mPhotosList, listData )
        {
            static_cast<Photo*>(listData)->Release();
        }
    }
}

/**
 * @brief Creates FbRespondGetPhotos object fron JSON formatted text
 * @param data - JSON formatted text
 * @return - created FbRespondGetPhotos object
 */
FbRespondGetPhotos* FbRespondGetPhotos::createFromJson(char* data)
{
    FbRespondGetPhotos * res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject ? new FbRespondGetPhotos(rootObject) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}

