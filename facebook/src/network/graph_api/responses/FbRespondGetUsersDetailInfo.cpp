#include <efl_extension.h>

#include "Common.h"
#include "FbRespondGetUsersDetailInfo.h"
#include "ParserWrapper.h"
#include "UserDetailInfo.h"
#include "jsonutilities.h"

/**
 * @brief Construct FbRespondGetUsersDetailInfo from JsonObject
 * @param[in] object - Json Object, from which FbRespondGetUsersDetailInfo will be constructed
 */
FbRespondGetUsersDetailInfo::FbRespondGetUsersDetailInfo(JsonNode* rootNode):FbRespondBase(rootNode)
{
    mUsersDetailInfoList = nullptr;
    JsonObject * response_node = json_node_get_object(rootNode);

    if (response_node) {
        GList * keysList = nullptr;
        guint size;
        size = json_object_get_size(response_node);
        keysList = json_object_get_members(response_node);
        gchar *keyName;

        for (int j = 0; j < size; j++) {
            keyName = (gchar *) keysList->data;
            JsonObject* member = json_object_get_object_member(response_node, keyName);

            if (member) {
                UserDetailInfo* user_detail_info = new UserDetailInfo(member);
                mUsersDetailInfoList = eina_list_append(mUsersDetailInfoList, user_detail_info);
            }
            keysList = g_list_next(keysList);
        }

        if (keysList) {
            g_list_free(keysList);
        }
    }
}

/**
 * @brief Destruction
 */
FbRespondGetUsersDetailInfo::~FbRespondGetUsersDetailInfo()
{
    if (mUsersDetailInfoList) {
        void * listData;
        EINA_LIST_FREE( mUsersDetailInfoList, listData )
        {
            static_cast<UserDetailInfo*>(listData)->Release();
        }
    }
}

/**
 * @brief Creates FbRespondGetUsersDetailInfo object from JSON formatted text
 * @param data - JSON formatted text
 * @return - created FbRespondGetUsersDetailInfo object
 */
FbRespondGetUsersDetailInfo* FbRespondGetUsersDetailInfo::createFromJson(char* data)
{
    FbRespondGetUsersDetailInfo * res = nullptr;

    JsonObject *object = nullptr;
    JsonParser *parser = openJsonParser(data, &object);
    ParserWrapper wr(parser);
    //We use root node here, because respond can have array instead object as root element
    JsonNode* rootNode = json_parser_get_root(parser);
    res = rootNode ? new FbRespondGetUsersDetailInfo(rootNode) : nullptr;

    return res;
}

Eina_List* FbRespondGetUsersDetailInfo::RemoveDataFromList(void *data)
{
    mUsersDetailInfoList = eina_list_remove(mUsersDetailInfoList, data);
    return mUsersDetailInfoList;
}
