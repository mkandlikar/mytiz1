/*
 * FbResponseAboutPlaces.cpp
 *
 *  Created on: SEP 17, 2015
 *      Author: Jeyaramakrishnan Sundar
 */

#include <malloc.h>
#include "Common.h"
#include "dlog.h"
#include "FbResponseAboutPlaces.h"
#include "jsonutilities.h"
#include "Utils.h"

FbResponseAboutPlaces::FbResponseAboutPlaces(JsonObject* root_object) : FbRespondBase(root_object)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "FbResponseAboutPlaces::FbResponseAboutPlaces()\n");

    JsonArray * lists_array = json_object_get_array_member(root_object, "data");
    mListofPlaces = NULL;
    if (lists_array != NULL)
    {
        int len = json_array_get_length(lists_array);

        for (int i = 0; i < len; i++)
        {
            JsonObject* el = json_array_get_object_element(lists_array, i);
            PlacesList* requestEl = new PlacesList(el);
            mListofPlaces = eina_list_append(mListofPlaces, requestEl);
        }
    }
}


FbResponseAboutPlaces::~FbResponseAboutPlaces()
{
    if ( mListofPlaces ) {
        void * listData;
        EINA_LIST_FREE( mListofPlaces, listData ) {
            delete static_cast<PlacesList*>(listData);
        }
    }
}

FbResponseAboutPlaces* FbResponseAboutPlaces::createFromJson(const char* data)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "FbRespondGetFriendLists::createFromJson\n");
    FbResponseAboutPlaces * res = NULL;

    JsonObject * rootObject = NULL;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject != NULL ? new FbResponseAboutPlaces(rootObject): NULL;
    g_object_unref(parser);
    return res;
}

FbResponseAboutPlaces::PlacesList::PlacesList(JsonObject * object)
{
	const gchar *recv_string = NULL;

	GraphObject::SetId( GetStringFromJson(object, "id") );
	created_time = GetStringFromJson(object, "category");
	place_name = GetStringFromJson(object, "name");
	recv_string = json_object_get_string_member(object,"link");
	mWebLinkToOpen = SAFE_STRDUP(recv_string);

	JsonObject* loc = json_object_get_object_member(object, "location");
	if(loc){
		city_name = GetStringFromJson(loc, "city");
		country = GetStringFromJson(loc, "country");
		mLatitude = json_object_get_double_member(loc, "latitude");
		mLongitude = json_object_get_double_member(loc, "longitude");

	}
	JsonObject* pic = json_object_get_object_member(object, "picture");
	if(pic){
		JsonObject* JsonObjData = json_object_get_object_member(pic, "data");
		mPicSquareWithLogo = GetStringFromJson(JsonObjData, "url");
	}
	dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "place_name = %s" , place_name);
	dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "city_name  = %s" , city_name);
	dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "mPicSquareWithLogo = %s" , mPicSquareWithLogo);
	dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "mLatitude = %f" , mLatitude);
	dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "mLongitude = %f", mLongitude);

//	if(recv_string)
//		delete recv_string;
}

Eina_List* FbResponseAboutPlaces::RemoveDataFromList(void *data)
{
	mListofPlaces = eina_list_remove(mListofPlaces, data);
    return mListofPlaces;
}

FbResponseAboutPlaces::PlacesList::~PlacesList()
{
	free((void*)place_name);
	free((void*)country);
    free((void*)created_time);
    free((void*)city_name);
    free((void*)mPicSquareWithLogo);
    free((void*)mWebLinkToOpen);
}
