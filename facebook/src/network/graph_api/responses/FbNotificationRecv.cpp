#include "Common.h"
#include "FbNotificationRecv.h"
#include "jsonutilities.h"
#include "Log.h"
#include "Utils.h"

FbNotificationRecv::FbNotificationRecv(JsonObject * rootNode) : FbRespondBase(rootNode),
        mMessengerNotif(false),
        mAuthorId(nullptr),
        mObjectId(nullptr),
        mSessionId(nullptr),
        mNtfType(nullptr),
        mNtfDisplayMessage(nullptr),
        mNtfId(nullptr),
        mUserId(nullptr),
        mWebLinkToOpen(nullptr)
{
    Log::info(LOG_FACEBOOK_NOTIFICATION, "FbNotificationRecv::FbNotificationRecv()");
    //Get the parameter containing the received notification details
    JsonObject * param = json_object_get_object_member(rootNode, "p");
    if (param) {
        //Get the relevant individual notification details
        mAuthorId = GetStringFromJson(param, "a");
        mObjectId = GetStringFromJson(param, "o");
        mSessionId = GetStringFromJson(param, "s");
        mNtfType = GetStringFromJson(param, "t");
        mNtfId = GetStringFromJson(param, "i");
        mUserId = GetStringFromJson(param, "u");
        const gchar *recvString = GetStringFromJson(param, "p");
        if (recvString && mObjectId) {
            size_t len = strlen(mObjectId) + strlen(recvString) + 2;
            char buf[len];
            Utils::Snprintf_s(buf, len, "%s_%s", mObjectId, recvString);
            free((char*) mObjectId); mObjectId = NULL; //free old allocated for obj memory
            mObjectId = SAFE_STRDUP(buf);
        }

        if (mNtfType) {
            //mNtfType : S => Comments , O => Poked , U => Friend Request
            //The received notification Id

            JsonObject *aps = json_object_get_object_member(rootNode, "aps");
            if (aps) {
                mNtfDisplayMessage = GetStringFromJson(param, "alert");
            }

            if (!strcmp("O", mNtfType)) {
                char msgBuf[MAX_REQ_WEBURL_LEN] = { 0, };
                Utils::Snprintf_s(msgBuf, MAX_REQ_WEBURL_LEN, "%s", URL_POKES_PAGE);
                mWebLinkToOpen = SAFE_STRDUP(msgBuf);
            }
            if (mAuthorId && mObjectId && mSessionId && mUserId && strlen(mNtfType) > 1) {
                mMessengerNotif = true;
            }

            Log::debug(LOG_FACEBOOK_NOTIFICATION, "FbNotificationRecv: type = %s graph_obj = %s display message = %s", mNtfType, mObjectId, mNtfDisplayMessage);
        }
    }
}

FbNotificationRecv::~FbNotificationRecv()
{
    free((void *) mObjectId);
    free((void *) mNtfType);
    free((void *) mNtfDisplayMessage);
    free((void *) mNtfId);
    free((void *) mWebLinkToOpen);
}


FbNotificationRecv* FbNotificationRecv::createFromJson(const char* data)
{
    //Two types of notification are there - Asynchronous which come via the Push service and the Synchronous which come
    //when we send a graph request. This function caters to async notifications which are received one by one by push service.
    Log::info(LOG_FACEBOOK_NOTIFICATION, "FbNotificationRecv::createFromJson ASYNC_NOTIF");
    FbNotificationRecv * res = NULL;
    JsonParser * parser = openJsonParser(data, NULL);

    //We use root node here, because REST API respond can have array instead object as root element
    JsonNode* rootNode = parser ? json_parser_get_root(parser) : NULL;
    JsonObject *rootobj = rootNode ? json_node_get_object(rootNode) : NULL;
    //Create the individual notification and specify the notification type as async
    res = rootobj ? new FbNotificationRecv(rootobj) : NULL;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}
