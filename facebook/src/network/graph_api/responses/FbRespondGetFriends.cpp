/*
 * GetFriendsListRespond.cpp
 *
 *  Created on: Apr 13, 2015
 *      Author: RUINMKOL
 */

#include <efl_extension.h>
#include <FbRespondGetFriends.h>
#include <json-glib/json-glib.h>

#include "Common.h"
#include "Friend.h"
#include "Summary.h"
#include "jsonutilities.h"

/**
 * @brief Construct FbRespondGetFriends from JsonObject
 * @param object - Json Object, from which FbRespondGetFriends will be constructed
 */
FbRespondGetFriends::FbRespondGetFriends(JsonObject* object) : FbRespondBase(object)
{
    JsonObject* summary = json_object_get_object_member(object, "summary");
    mSummary = summary ? new Summary(summary) : nullptr;

    JsonArray * friends_array = json_object_get_array_member(object, "data");
    mFriendsList = nullptr;
    if (friends_array) {
        int len = json_array_get_length(friends_array);

        for (int i = 0; i < len; i++) {
            JsonObject* el = json_array_get_object_element(friends_array, i);
            if (el) {
                Friend* friendEl = new Friend(el);
                if (friendEl) {
                    mFriendsList = eina_list_append(mFriendsList, friendEl);
                }
            }
        }
    }
}

/**
 * @brief Destructor
 */
FbRespondGetFriends::~FbRespondGetFriends()
{
    if (mFriendsList) {
        void * listData;
        EINA_LIST_FREE(mFriendsList, listData)
        {
            static_cast<Friend*>(listData)->Release();
        }
    }

    delete mSummary;
}

/**
 * @brief Creates FbRespondGetFriends object fron JSON formatted text
 * @param[in] data - JSON formatted text
 * @return - created FbRespondGetFriends object
 */
FbRespondGetFriends* FbRespondGetFriends::createFromJson(char* data)
{
    FbRespondGetFriends * res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject ? new FbRespondGetFriends(rootObject): nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}

Eina_List* FbRespondGetFriends::RemoveDataFromList(void *data)
{
    mFriendsList = eina_list_remove(mFriendsList, data);
    return mFriendsList;
}
