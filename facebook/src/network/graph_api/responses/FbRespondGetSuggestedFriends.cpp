#include <efl_extension.h>

#include "Common.h"
#include "FbRespondGetSuggestedFriends.h"
#include "SuggestedFriend.h"
#include "jsonutilities.h"

/**
 * @brief Construct FbRespondGetSuggestedFriends from JsonObject
 * @param[in] object - Json Object, from which FbRespondGetSuggestedFriends will be constructed
 */
FbRespondGetSuggestedFriends::FbRespondGetSuggestedFriends(JsonObject* object):FbRespondBase(object)
{
    JsonArray * suggested_friends_array = json_object_get_array_member(object, DATA);
    mSuggestedFriendsList = nullptr;
    if (suggested_friends_array) {
        int len = json_array_get_length(suggested_friends_array);

        for (int i = 0; i < len; i++) {
            JsonObject* el = json_array_get_object_element(suggested_friends_array, i);
            SuggestedFriend* suggestedFriendEl = new SuggestedFriend(el);
            mSuggestedFriendsList = eina_list_append(mSuggestedFriendsList, suggestedFriendEl);
        }
    }
}

/**
 * @brief Destruction
 */
FbRespondGetSuggestedFriends::~FbRespondGetSuggestedFriends()
{
    if (mSuggestedFriendsList) {
        void * listData;
        EINA_LIST_FREE( mSuggestedFriendsList, listData )
        {
            static_cast<SuggestedFriend*>(listData)->Release();
        }
    }
}

/**
 * @brief Creates FbRespondGetSuggestedFriends object from JSON formatted text
 * @param data - JSON formatted text
 * @return - created FbRespondGetSuggestedFriends object
 */
FbRespondGetSuggestedFriends* FbRespondGetSuggestedFriends::createFromJson(char* data)
{
    FbRespondGetSuggestedFriends * res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject ? new FbRespondGetSuggestedFriends(rootObject) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}

