#include "Common.h"
#include "FbRespondGetPeopleYouMayKnow.h"
#include "jsonutilities.h"
#include "MutualFriendsList.h"
#include "Queries.h"
#include "Utils.h"

#include <malloc.h>

/**
 * @brief Construct FbRespondGetPeopleYouMayKnow from JsonNode
 * @param[in] object - Json Object, from which FbRespondGetPeopleYouMayKnow will be constructed
 */
FbRespondGetPeopleYouMayKnow::FbRespondGetPeopleYouMayKnow(JsonNode * rootNode) : FbRespondBaseRestApi(rootNode)
{
    //We use root node here instead JsonObject, because REST API respond can have array instead object as root element
    JsonObject * object = json_node_get_object(rootNode);

    JsonArray * friendable_array = json_object_get_array_member(object, "friendable");
    mPeople = nullptr;
    if (friendable_array) {
        int len = json_array_get_length(friendable_array);

        for (int i = 0; i < len; i++) {
            JsonObject* el = json_array_get_object_element(friendable_array, i);
            FriendableUser* friendEl = new FriendableUser(el);
            mPeople = eina_list_append(mPeople, friendEl);
        }
    }
}

/**
 * @brief Destruction
 */
FbRespondGetPeopleYouMayKnow::~FbRespondGetPeopleYouMayKnow()
{
    if (mPeople) {
        void * listData;
        EINA_LIST_FREE( mPeople, listData )
        {
            static_cast<FriendableUser*>(listData)->Release();
        }
    }
}

/**
 * @brief Creates FbRespondGetPeopleYouMayKnow object fron JSON formatted text
 * @param data - JSON formatted text
 * @return - created FbRespondGetPeopleYouMayKnow object
 */
FbRespondGetPeopleYouMayKnow* FbRespondGetPeopleYouMayKnow::createFromJson(const char* data)
{
    FbRespondGetPeopleYouMayKnow * res = nullptr;

    JsonParser * parser = openJsonParser(data, nullptr);

    //We use root node here, because REST API respond can have array instead object as root element
    JsonNode* rootNode = json_parser_get_root (parser);

    res = rootNode ? new FbRespondGetPeopleYouMayKnow(rootNode) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}

/**
 * @brief Construct FriendableUser from JsonObject
 * @param[in] object - Json Object, from which FriendableUser will be constructed
 */
FbRespondGetPeopleYouMayKnow::FriendableUser::FriendableUser(JsonObject * object)
{
    mGraphObjectType = GraphObject::GOT_PEOPLE_YOU_MAY_KNOW;
    char *id = GetStringFromJson(object, "uid");
    SetId(id);
    mName = GetStringFromJson(object, "name");
    mUserName = GetStringFromJson(object, "username");
    mPicSquareWithLogo = GetStringFromJson(object, "pic_square_with_logo");
    mMutualFriends = json_object_get_int_member(object, "mutual_friends");
    if (mMutualFriends) {
        MutualFriendsList::GetInstance()->AddUsersMutualFriends(id, mMutualFriends);
    }
}


FbRespondGetPeopleYouMayKnow::FriendableUser::FriendableUser(char **name, char **value)
{
    mGraphObjectType = GraphObject::GOT_PEOPLE_YOU_MAY_KNOW;
    char *id = Utils::GetStringOrNull(value[Queries::SelectFriendableUser::ID]);
    GraphObject::SetId(id);
    mName = Utils::GetStringOrNull(value[Queries::SelectFriendableUser::NAME]);
    mUserName = Utils::GetStringOrNull(value[Queries::SelectFriendableUser::USER_NAME]);
    mPicSquareWithLogo = Utils::GetStringOrNull(value[Queries::SelectFriendableUser::PIC_SQUARE_WITH_LOGO]);
    mMutualFriends = atoi(value[Queries::SelectFriendableUser::MUTUAL_FRIENDS]);
    if (mMutualFriends) {
        MutualFriendsList::GetInstance()->AddUsersMutualFriends(id, mMutualFriends);
    }
}

/**
 * @brief Destruction
 */
FbRespondGetPeopleYouMayKnow::FriendableUser::~FriendableUser()
{
    free((void*) mName);
    free((void*) mUserName);
    free((void*) mPicSquareWithLogo);
}

Eina_List* FbRespondGetPeopleYouMayKnow::RemoveDataFromList(void *data)
{
    mPeople = eina_list_remove(mPeople, data);
    return mPeople;
}
