#include <efl_extension.h>

#include "Album.h"
#include "Common.h"
#include "FbRespondGetAlbums.h"
#include "jsonutilities.h"

/**
 * @brief Construct FbRespondGetAlbums from JsonObject
 * @param[in] object - Json Object, from which FbRespondGetAlbums will be constructed
 */
FbRespondGetAlbums::FbRespondGetAlbums(JsonObject* object) : FbRespondBase(object)
{
    JsonArray * albums_array = json_object_get_array_member(object, DATA);
    mAlbumsList = nullptr;
    if (albums_array) {
        int len = json_array_get_length(albums_array);
        for (int i = 0; i < len; i++) {
            JsonObject* el = json_array_get_object_element(albums_array, i);
            if (el) {
                Album* albumEl = new Album(el);
                if (albumEl) {
                    mAlbumsList = eina_list_append(mAlbumsList, albumEl);
                }
            }
        }
    }
}

/**
 * @brief Destruction
 */
FbRespondGetAlbums::~FbRespondGetAlbums()
{
    if (mAlbumsList) {
        void * listData;
        EINA_LIST_FREE(mAlbumsList, listData) {
            static_cast<Album*>(listData)->Release();
        }
    }
}

/**
 * @brief Creates FbRespondGetAlbums object from JSON formatted text
 * @param data - JSON formatted text
 * @return - created FbRespondGetAlbums object
 */
FbRespondGetAlbums* FbRespondGetAlbums::createFromJson(char* data)
{
    FbRespondGetAlbums * res = NULL;

    JsonObject * rootObject = NULL;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject != NULL ? new FbRespondGetAlbums(rootObject): NULL;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}
