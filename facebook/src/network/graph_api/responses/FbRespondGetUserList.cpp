#include <efl_extension.h>
#include "FbRespondGetUserList.h"
#include "User.h"
#include "Common.h"
#include "jsonutilities.h"


FbRespondGetUserList::FbRespondGetUserList( JsonNode* node ) : FbRespondBase( node )
{
    JsonObject * responseNode = json_node_get_object(node);
    mUserList = nullptr;
    if (responseNode) {
        GList *keysList = json_object_get_members(responseNode);
        guint size = json_object_get_size(responseNode);
        for (int j = 0; j < size; ++j, keysList = g_list_next(keysList)) {
            gchar *keyName = (gchar *) keysList->data;
            JsonObject* member = json_object_get_object_member(responseNode, keyName);
            if (member) {
                User *user = new User(member);
                mUserList = eina_list_append(mUserList, user);
            }
        }
        if (keysList) {
            g_list_free(keysList);
        }
    }
}

FbRespondGetUserList::FbRespondGetUserList(JsonObject* object) : FbRespondBase(object)
{
    JsonArray * friends_array = json_object_get_array_member(object, "data");
    mUserList = nullptr;
    if (friends_array) {
        int len = json_array_get_length(friends_array);

        for (int i = 0; i < len; i++) {
            JsonObject* el = json_array_get_object_element(friends_array, i);
            User* friendEl = new User(el);
            mUserList = eina_list_append(mUserList, friendEl);
        }
    }
}

FbRespondGetUserList::~FbRespondGetUserList()
{
    if (mUserList) {
        void * listData;
        EINA_LIST_FREE( mUserList, listData )
        {
            static_cast<User*>(listData)->Release();
        }
    }
}

FbRespondGetUserList* FbRespondGetUserList::createFromJson(char* data)
{
    FbRespondGetUserList * res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject ? new FbRespondGetUserList(rootObject) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}

Eina_List* FbRespondGetUserList::RemoveDataFromList(void *data)
{
    mUserList = eina_list_remove(mUserList, data);
    return mUserList;
}
