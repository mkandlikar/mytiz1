/*
 * FbRespondGetFindFriend.cpp
 *
 *  Created on: Jun 29, 2015
 *      Author: RUINMKOL
 */
#include <efl_extension.h>
#include "Common.h"
#include "dlog.h"
#include "FbRespondGetSearchResults.h"
#include "FoundItem.h"
#include "jsonutilities.h"

/**
 * @brief Construct FbRespondGetFindFriend from JsonNode
 * @param[in] object - Json Object, from which FbRespondGetFindFriend will be constructed
 */
FbRespondGetSearchResults::FbRespondGetSearchResults(JsonNode* rootNode) : FbRespondBaseRestApi(rootNode)
{
    //We use root node here instead JsonObject, because REST API respond can have array instead object as root element
    JsonArray * friends_array = json_node_get_array(rootNode);
    mFoundItems = nullptr;
    if (friends_array) {
        int len = json_array_get_length(friends_array);

        for (int i = 0; i < len; i++) {
            JsonObject* el = json_array_get_object_element(friends_array, i);
            FoundItem* friendEl = new FoundItem(el);
            mFoundItems = eina_list_append(mFoundItems, friendEl);
        }
    }
}

/**
 * @brief Destructor
 */
FbRespondGetSearchResults::~FbRespondGetSearchResults()
{
    if (mFoundItems) {
        void * listData;
        EINA_LIST_FREE( mFoundItems, listData )
        {
            static_cast<FoundItem*>(listData)->Release();
        }
    }
}

/**
 * @brief Creates FbRespondGetFindFriend object fron JSON formatted text
 * @param data - JSON formatted text
 * @return - created FbRespondGetFindFriend object
 */
FbRespondGetSearchResults* FbRespondGetSearchResults::createFromJson(const char* data)
{
    FbRespondGetSearchResults * res = nullptr;

    JsonParser * parser = openJsonParser(data, nullptr);

    //We use root node here, because REST API respond can have array instead object as root element
    JsonNode* rootNode = json_parser_get_root (parser);

    res = rootNode ? new FbRespondGetSearchResults(rootNode) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}

Eina_List* FbRespondGetSearchResults::RemoveDataFromList(void *data)
{
    mFoundItems = eina_list_remove(mFoundItems, data);
    return mFoundItems;
}
