/*
 *  FbResponseAboutMore.cpp
 *
 *  Created on: July 16, 2015
 *      Author: Manjunath Raja
 */

#include <app.h>
#include <dlog.h>
#include <json-glib/json-glib.h>

#include "Common.h"
#include "FbResponseAboutMore.h"
#include "jsonutilities.h"


FbResponseAboutMore::FbResponseAboutMore(JsonObject* JsonRoot) :
    FbRespondBase(JsonRoot),
    mWorkList(NULL) {

    ListItemWork* NewListItemWork = NULL;
    const char* TmpPtr = NULL;

    if (JsonRoot) {
        TmpPtr = json_object_get_string_member(JsonRoot, "name");
        if (TmpPtr)
            mUserName = TmpPtr;

        NewListItemWork = new ListItemWork();
        NewListItemWork->mHeading = "Work";//TODO: i18n_get_text("IDS_WORK");
        mWorkList = eina_list_append(mWorkList, NewListItemWork);

        NewListItemWork = new ListItemWork();
        NewListItemWork->mAddWork = "Add Work";//TODO: i18n_get_text("IDS_ADD_WORK");
        NewListItemWork->mImage = "about_info_thumnail_plus.png";
        mWorkList = eina_list_append(mWorkList, NewListItemWork);

        JsonArray* JsonArrayWork = json_object_get_array_member(JsonRoot, "work");
        if (JsonArrayWork) {
            int JsonArrayWorkLen = json_array_get_length(JsonArrayWork);
            int Idx = 0;
            while (Idx < JsonArrayWorkLen) {
                JsonObject* JsonObjWork = json_array_get_object_element(JsonArrayWork, Idx++);
                if (JsonObjWork) {
                    NewListItemWork = new ListItemWork();
                    JsonObject* JsonObjEmployer = json_object_get_object_member(JsonObjWork, "employer");
                    if (JsonObjEmployer) {
                        TmpPtr = json_object_get_string_member(JsonObjEmployer, "name");
                        if (TmpPtr)
                            NewListItemWork->mName = TmpPtr;
                        JsonObject* JsonObjPicture = json_object_get_object_member(JsonObjEmployer, "picture");
                        if (JsonObjPicture) {
                            JsonObject* JsonObjData = json_object_get_object_member(JsonObjPicture, "data");
                            TmpPtr = json_object_get_string_member(JsonObjData, "url");
                            if (TmpPtr)
                                NewListItemWork->mImage = TmpPtr;
                        }
                    }
                    JsonObject* JsonObjPosition = json_object_get_object_member(JsonObjWork, "position");
                    if (JsonObjPosition) {
                        TmpPtr = json_object_get_string_member(JsonObjPosition, "name");
                        if (TmpPtr)
                            NewListItemWork->mDesignation = TmpPtr;
                    }
                    JsonObject* JsonObjLocation = json_object_get_object_member(JsonObjWork, "location");
                    if (JsonObjLocation) {
                        TmpPtr = json_object_get_string_member(JsonObjLocation, "name");
                        if (TmpPtr)
                            NewListItemWork->mLocation = TmpPtr;
                    }
                    TmpPtr = json_object_get_string_member(JsonObjWork, "start_date");
                    if (TmpPtr)
                        NewListItemWork->mDuration = TmpPtr;
                    NewListItemWork->mDuration.append("-");
                    TmpPtr = json_object_get_string_member(JsonObjWork, "end_date");
                    if (TmpPtr)
                        NewListItemWork->mDuration.append(TmpPtr);
                    else
                        NewListItemWork->mDuration.append("Present");//TODO: i18n_get_text("IDS_PRESENT"));

                    mWorkList = eina_list_append(mWorkList, NewListItemWork);
                }
            }
        }
    }
}

FbResponseAboutMore::~FbResponseAboutMore() {
    if (mWorkList) {
        void* ListItemWorkData;
        EINA_LIST_FREE(mWorkList, ListItemWorkData) {
            delete static_cast<ListItemWork*>(ListItemWorkData);
        }
    }
}

FbResponseAboutMore* FbResponseAboutMore::Parse(char* Respond) {
    JsonObject* JsonRoot;
    JsonParser* JsonParser = openJsonParser(Respond, &JsonRoot);
    FbResponseAboutMore* Parsed = (JsonRoot ? new FbResponseAboutMore(JsonRoot): NULL);
    g_object_unref(JsonParser);
    return Parsed;
}
