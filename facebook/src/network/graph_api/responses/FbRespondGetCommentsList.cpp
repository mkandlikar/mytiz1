#include <efl_extension.h>
#include <json-glib/json-glib.h>

#include "Comment.h"
#include "Common.h"
#include "FbRespondGetCommentsList.h"
#include "Log.h"
#include "jsonutilities.h"

/**
* @brief Construct FbRespondGetCommentsList from JsonObject
* @param[in] object - Json Object, from which FbRespondGetCommentsList will be constructed
*/
FbRespondGetCommentsList::FbRespondGetCommentsList(JsonObject* object):FbRespondBase(object)
{
    Log::info("FbRespondGetCommentsList::FbRespondGetCommentsList()");
    JsonObject* summary = json_object_get_object_member(object, "summary");
    mSummary = summary ? new FbRespondGetCommentsList::Summary(summary) : nullptr;

    JsonArray * comments_array = json_object_get_array_member(object, "data");
    mCommentsList = nullptr;
    if (comments_array) {
        int len = json_array_get_length(comments_array);

        for (int i = 0; i < len; i++) {
            JsonObject* el = json_array_get_object_element(comments_array, i);
            Comment* commentEl = new Comment(el);
            //Comments have reverse order
            mCommentsList = eina_list_prepend(mCommentsList, commentEl);
        }
    }
}

/**
 * @brief Destructor
 */
FbRespondGetCommentsList::~FbRespondGetCommentsList()
{
    if (mCommentsList) {
        void * listData;
        EINA_LIST_FREE(mCommentsList, listData)
        {
            static_cast<Comment*>(listData)->Release();
        }
    }

    delete mSummary;
}

Eina_List* FbRespondGetCommentsList::GetCommentsList()
{
    return mCommentsList;
}

/**
 * @brief Creates FbRespondGetCommentsList object fron JSON formatted text
 * @param data - JSON formatted text
 * @return - created FbRespondGetCommentsList object
 */
FbRespondGetCommentsList* FbRespondGetCommentsList::createFromJson(char* data)
{
    Log::info("FbRespondGetCommentsList::createFromJson");
    FbRespondGetCommentsList * res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject ? new FbRespondGetCommentsList(rootObject) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}

Eina_List* FbRespondGetCommentsList::RemoveDataFromList(void *data)
{
	mCommentsList = eina_list_remove(mCommentsList, data);
    return mCommentsList;
}

/**
 * @brief Construct Summary object from JsonObject
 * @param object - Json Object, from which Summary will be constructed
 */
FbRespondGetCommentsList::Summary::Summary(JsonObject * object)
{
    mTotalCount = json_object_get_int_member(object, "total_count");
    mOrder = GetStringFromJson(object, "order");
}

/**
 * @brief Destructor
 */
FbRespondGetCommentsList::Summary::~Summary()
{
    free((void *) mOrder);
}
