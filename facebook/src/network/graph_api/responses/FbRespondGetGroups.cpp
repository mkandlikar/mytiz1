#include <efl_extension.h>

#include "Common.h"
#include "FbRespondGetGroups.h"
#include "Group.h"
#include "jsonutilities.h"

/**
 * @brief Construct FbRespondGetGroups from JsonObject
 * @param[in] object - Json Object, from which FbRespondGetGroups will be constructed
 */
FbRespondGetGroups::FbRespondGetGroups(JsonObject* object):FbRespondBase(object)
{
    JsonArray * groups_array = json_object_get_array_member(object, DATA);
    mGroupsList = nullptr;
    if (groups_array) {
        int len = json_array_get_length(groups_array);

        for (int i = 0; i < len; i++) {
            JsonObject* el = json_array_get_object_element(groups_array, i);
            Group* groupEl = new Group(el);
            mGroupsList = eina_list_append(mGroupsList, groupEl);
        }
    }
}

/**
 * @brief Destruction
 */
FbRespondGetGroups::~FbRespondGetGroups()
{
    if ( mGroupsList ) {
        void * listData;
        EINA_LIST_FREE( mGroupsList, listData ) {
            static_cast<Group*>(listData)->Release();
        }
    }
}

/**
 * @brief Creates FbRespondGetGroups object from JSON formatted text
 * @param data - JSON formatted text
 * @return - created FbRespondGetGroups object
 */
FbRespondGetGroups* FbRespondGetGroups::createFromJson(char* data)
{
    FbRespondGetGroups * res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject ? new FbRespondGetGroups(rootObject) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}

Eina_List* FbRespondGetGroups::RemoveDataFromList(void *data)
{
    mGroupsList = eina_list_remove(mGroupsList, data);
    return mGroupsList;
}
