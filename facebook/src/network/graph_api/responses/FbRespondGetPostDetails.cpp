#include "FbRespondGetPostDetails.h"
#include "jsonutilities.h"

FbRespondGetPostDetails::FbRespondGetPostDetails(JsonObject* object) : FbRespondBase(object)
{
    mPost = mError ? nullptr : new Post(object);
}
FbRespondGetPostDetails::~FbRespondGetPostDetails()
{
    if (mPost) {
        mPost->Release();
    }
}

FbRespondGetPostDetails* FbRespondGetPostDetails::createFromJson(char* data)
{
    FbRespondGetPostDetails * res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject ? new FbRespondGetPostDetails(rootObject) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}
