#include <malloc.h>

#include "Common.h"
#include "CSmartPtr.h"
#include "FbResponseEventDetails.h"
#include "ParserWrapper.h"
#include "jsonutilities.h"
#include "Utils.h"

/**
 * @brief Construct FbResponseEventDetails object from JsonObject
 * @param[in] object - Json Object, from which FbRespondBase will be constructed
 */
FbResponseEventDetails::FbResponseEventDetails(JsonObject * object): FbRespondBase(object)
{
    //Init
	mStartTime = 0;
    mId = nullptr;
    mCoverUrl = nullptr;
    mLocation = nullptr;
    mOwnerId = nullptr;
    mCoverUrl = nullptr;

    c_unique_ptr<char> startTime(GetStringFromJson(object, "start_time"));
    c_unique_ptr<char> timeShiftFromString(Utils::GetTimeShift(startTime.get()));
    c_unique_ptr<char> currentTimeShift(Utils::GetCurrentTimeShift());
    if (startTime && timeShiftFromString && currentTimeShift) {
        mNeedTimeShift = strcmp(timeShiftFromString.get(), currentTimeShift.get());
        mStartTime = Utils::ConvertTimeToGMT(startTime.get(), mNeedTimeShift);
    } else {
        mStartTime = 0.0;
    }

    JsonObject *place = json_object_get_object_member(object, "place");
    if (place) {
        mLocation = GetStringFromJson(place, "name");
    }

    JsonObject *owner = json_object_get_object_member(object, "owner");
    if (owner) {
        mOwnerId = GetStringFromJson(owner, "id");
    }

    JsonObject *cover = json_object_get_object_member(object, "cover");
    if (cover) {
        mCoverUrl = GetStringFromJson(cover, "source");
    }

    mId = GetStringFromJson(object, "id");
}

/**
 * @brief Destructor
 */
FbResponseEventDetails::~FbResponseEventDetails()
{
    free((void*) mId);
    free((void*) mLocation);
    free((void*) mCoverUrl);
    free((void*) mOwnerId);
}

/**
 * @brief Creates FbResponseEventDetails object fron JSON formatted text
 * @param[in] data - JSON formatted text
 * @return - created FbRespondBase object
 */
FbResponseEventDetails* FbResponseEventDetails::createFromJson(char* data)
{
    FbResponseEventDetails * res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);
    ParserWrapper wr( parser );
    res = rootObject ? new FbResponseEventDetails(rootObject) : nullptr;
    return res;
}
