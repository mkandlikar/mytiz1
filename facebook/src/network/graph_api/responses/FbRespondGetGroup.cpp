#include <efl_extension.h>

#include "Common.h"
#include "FbRespondGetGroup.h"
#include "Group.h"
#include "jsonutilities.h"

/**
 * @brief Construct FbRespondGetGroup from JsonObject
 * @param[in] object - Json Object, from which FbRespondGetGroup will be constructed
 */
FbRespondGetGroup::FbRespondGetGroup(JsonObject* object) : FbRespondBase(object)
{
    mGroupsList = nullptr;
    Group* groupEl = new Group(object);
    mGroupsList = eina_list_append(mGroupsList, groupEl);
}

/**
 * @brief Destruction
 */
FbRespondGetGroup::~FbRespondGetGroup()
{
    if (mGroupsList) {
        void * listData;
        EINA_LIST_FREE( mGroupsList, listData )
        {
            static_cast<Group*>(listData)->Release();
        }
    }
}

/**
 * @brief Creates FbRespondGetGroups object from JSON formatted text
 * @param data - JSON formatted text
 * @return - created FbRespondGetGroups object
 */
FbRespondGetGroup* FbRespondGetGroup::createFromJson(char* data)
{
    FbRespondGetGroup * res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject ? new FbRespondGetGroup(rootObject) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}

Eina_List* FbRespondGetGroup::RemoveDataFromList(void *data)
{
    mGroupsList = eina_list_remove(mGroupsList, data);
    return mGroupsList;
}
