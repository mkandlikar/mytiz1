/*
 * FbRespondGetMobileZeroCampain.cpp
 *
 *  Created on: Aug 20, 2015
 *      Author: ruinmkol
 */

#include <dlog.h>
#include <Elementary.h>

#include "Common.h"
#include "FbRespondGetMobileZeroCampain.h"
#include "jsonutilities.h"

FbRespondGetMobileZeroCampain::FbRespondGetMobileZeroCampain(JsonNode* rootNode): FbRespondBaseRestApi(rootNode)
{
    //We use root node here instead JsonObject, because REST API respond can have array instead object as root element
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "FbRespondGetMobileZeroCampain::FbRespondGetMobileZeroCampain()\n");

    JsonObject * object = json_node_get_object(rootNode);

    mId = json_object_get_int_member(object, "id");
    mStatus = GetStringFromJson(object, "status");
    mTtl = json_object_get_int_member(object, "ttl");
    mIpPool = GetStringFromJson(object, "ip_pool");

    JsonArray * rewriteRulesArray = json_object_get_array_member(object, "rewrite_rules");
    mRewriteRules = NULL;
    if (rewriteRulesArray != NULL)
    {
        int len = json_array_get_length(rewriteRulesArray);

        for (int i = 0; i < len; i++)
        {
            JsonObject* el = json_array_get_object_element(rewriteRulesArray, i);
            RewriteRules* rules = new RewriteRules(el);
            mRewriteRules = eina_list_append(mRewriteRules, rules);
        }
    }

    JsonArray * featuresArray = json_object_get_array_member(object, "enabled_ui_features");
    mEnabledUiFeatures = NULL;
    if (featuresArray != NULL)
    {
        int len = json_array_get_length(featuresArray);

        for (int i = 0; i < len; i++)
        {
            JsonObject* el = json_array_get_object_element(featuresArray, i);
            //TODO: get string and put to array
            mEnabledUiFeatures = eina_list_append(mEnabledUiFeatures, "");
        }
    }

    mCarrierName = GetStringFromJson(object, "carrier_name");
}

FbRespondGetMobileZeroCampain::~FbRespondGetMobileZeroCampain()
{
    free((void*) mStatus);
    free((void*) mIpPool);
    free((void*) mCarrierName);

    if ( mRewriteRules ) {
        void * listData;
        EINA_LIST_FREE( mRewriteRules, listData ) {
            delete static_cast<RewriteRules*>(listData);
        }
    }

    if ( mEnabledUiFeatures ) {
        void * listData;
        //TODO: delete strings here
        //EINA_LIST_FREE( mEnabledUiFeatures, listData ) {
        //    delete static_cast<FriendableUser*>(listData);
        //}
    }
}

/**
 * @brief Construct FriendableUser from JsonObject
 * @param[in] object - Json Object, from which FriendableUser will be constructed
 */
FbRespondGetMobileZeroCampain::RewriteRules::RewriteRules(JsonObject * object)
{
    mMatcher = GetStringFromJson(object, "matcher");
    mReplacer = GetStringFromJson(object, "replacer");
    mMode = GetStringFromJson(object, "mode");
}

/**
 * @brief Destruction
 */
FbRespondGetMobileZeroCampain::RewriteRules::~RewriteRules()
{
    free((void*) mMatcher);
    free((void*) mReplacer);
    free((void*) mMode);
}

