#include <efl_extension.h>

#include "Common.h"
#include "FbResponseSearchPlaces.h"
#include "Place.h"
#include "jsonutilities.h"

/**
 * @brief Construct FbResponseSearchPlaces from JsonObject
 * @param[in] object - Json Object, from which FbResponseSearchPlaces will be constructed
 */
FbResponseSearchPlaces::FbResponseSearchPlaces(JsonObject* object):FbRespondBase(object)
{
    JsonArray * places_array = json_object_get_array_member(object, DATA);
    mPlacesList = nullptr;
    if (places_array) {
        int len = json_array_get_length(places_array);
        for (int i = 0; i < len; i++) {
            JsonObject* el = json_array_get_object_element(places_array, i);
            Place* placeEl = new Place(el);
            mPlacesList = eina_list_append(mPlacesList, placeEl);
        }
    }
}

/**
 * @brief Destruction
 */
FbResponseSearchPlaces::~FbResponseSearchPlaces()
{
    if (mPlacesList) {
        void * listData;
        EINA_LIST_FREE(mPlacesList, listData) {
            static_cast<Place*>(listData)->Release();
        }
    }
}

/**
 * @brief Creates FbResponseSearchPlaces object from JSON formatted text
 * @param data - JSON formatted text
 * @return - created FbResponseSearchPlaces object
 */
FbResponseSearchPlaces* FbResponseSearchPlaces::createFromJson(char* data)
{
    FbResponseSearchPlaces *res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject ? new FbResponseSearchPlaces(rootObject) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}
