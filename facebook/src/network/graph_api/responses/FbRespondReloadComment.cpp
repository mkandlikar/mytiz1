#include <malloc.h>
#include <stddef.h>
#include "jsonutilities.h"

#include "Common.h"
#include "FbRespondBase.h"
#include "FbRespondReloadComment.h"
#include "Comment.h"

/**
 * @brief Construct FbRespondReloadComment object from JsonObject
 * @param[in] object - Json Object, from which FbRespondBase will be constructed
 */
FbRespondReloadComment::FbRespondReloadComment(JsonObject * object) : FbRespondBase(object)
{
    mComment = new Comment(object);
}

/**
 * @brief Destructor
 */
FbRespondReloadComment::~FbRespondReloadComment()
{
    if (mComment) {
        mComment->Release();
    }
}


/**
 * @brief Creates FbRespondReloadComment object fron JSON formatted text
 * @param[in] data - JSON formatted text
 * @return - created FbRespondBase object
 */
FbRespondReloadComment* FbRespondReloadComment::createFromJson(char* data)
{
    FbRespondReloadComment * res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject ? new FbRespondReloadComment(rootObject) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}
