#include "FbResponseImageDetails.h"
#include "jsonutilities.h"
#include "Log.h"
#include "Photo.h"

FbResponseImageDetails::FbResponseImageDetails(JsonObject * object): FbRespondBase(object),
    mPhoto(nullptr)
{
    mPhoto = new Photo(object);
}

FbResponseImageDetails::~FbResponseImageDetails()
{
    if (mPhoto) {
        mPhoto->Release();
    }
}

/**
 * @brief Creates FbResponseImageDetails object fron JSON formatted text
 * @param[in] data - JSON formatted text
 * @return - created FbRespondBase object
 */
FbResponseImageDetails* FbResponseImageDetails::createFromJson(char* data)
{
    Log::info("FbResponseImageDetails::createFromJson");
    FbResponseImageDetails * res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject ? new FbResponseImageDetails(rootObject) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}
