#include <malloc.h>
#include <stddef.h>

#include "Common.h"
#include "FbRespondBase.h"
#include "FbRespondEditCommentMessage.h"
#include "Log.h"
#include "jsonutilities.h"

/**
 * @brief Construct FbRespondEditPostMessage object from JsonObject
 * @param[in] object - Json Object, from which FbRespondBase will be constructed
 */
FbRespondEditCommentMessage::FbRespondEditCommentMessage(JsonObject * object): FbRespondBase(object) {
    mIsSuccess = false;
    //result
    mIsSuccess = json_object_get_boolean_member(object, "success");
    Log::debug("FbRespondEditPostMessage, mIsSuccess = %d", mIsSuccess);
}

/**
 * @brief Destructor
 */
FbRespondEditCommentMessage::~FbRespondEditCommentMessage()
{

}


/**
 * @brief Creates FbRespondEditPostMessage object fron JSON formatted text
 * @param[in] data - JSON formatted text
 * @return - created FbRespondBase object
 */
FbRespondEditCommentMessage* FbRespondEditCommentMessage::createFromJson(char* data)
{
    FbRespondEditCommentMessage * res = NULL;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject ? new FbRespondEditCommentMessage(rootObject) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}
