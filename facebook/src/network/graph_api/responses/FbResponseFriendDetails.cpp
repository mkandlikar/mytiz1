#include <malloc.h>
#include "jsonutilities.h"

#include "Common.h"
#include "FbResponseFriendDetails.h"

/**
 * @brief Construct FbResponseFriendDetails object from JsonObject
 * @param[in] object - Json Object, from which FbRespondBase will be constructed
 */
FbResponseFriendDetails::FbResponseFriendDetails(JsonObject * object): FbRespondBase(object)
{
    mId = nullptr;
    mEmployer = nullptr;
    mLocation = nullptr;
    mPosition = nullptr;
    mEducation = nullptr;
    mCoverUrl = nullptr;
    mPictureUrl = nullptr;

    JsonArray * work = json_object_get_array_member(object, "work");
    if (work) {
        JsonObject *el = json_array_get_object_element(work, 0);
        if (el) {
            JsonObject *employer = json_object_get_object_member(el, "employer");
            if (employer) {
                mEmployer = GetStringFromJson(employer, "name");
            }
            JsonObject *location = json_object_get_object_member(el, "location");
            if (location) {
                mLocation = GetStringFromJson(location, "name");
            }
            JsonObject *position = json_object_get_object_member(el, "position");
            if (position) {
                mPosition = GetStringFromJson(location, "name");
            }
        }
    }

    JsonArray * education = json_object_get_array_member(object, "education");
    if (education) {
        JsonObject *el = json_array_get_object_element(education, 0);
        if (el) {
            JsonObject *school = json_object_get_object_member(el, "school");
            if (school) {
                mEducation = GetStringFromJson(school, "name");
            }
        }
    }

    JsonObject *cover = json_object_get_object_member(object, "cover");
    if (cover) {
        mCoverUrl = GetStringFromJson(cover, "source");
    }

    JsonObject *picture = json_object_get_object_member(object, "picture");
    if (picture) {
        JsonObject *pictureData = json_object_get_object_member(picture, "data");
        if (pictureData) {
            mPictureUrl = GetStringFromJson(pictureData, "url");
        }
    }
    mId = GetStringFromJson(object, "id");
}

/**
 * @brief Destructor
 */
FbResponseFriendDetails::~FbResponseFriendDetails()
{
    free((void*) mId);
    free((void*) mEmployer);
    free((void*) mLocation);
    free((void*) mPosition);
    free((void*) mEducation);
    free((void*) mCoverUrl);
    free((void*) mPictureUrl);
}

/**
 * @brief Creates FbResponseFriendDetails object fron JSON formatted text
 * @param[in] data - JSON formatted text
 * @return - created FbRespondBase object
 */
FbResponseFriendDetails* FbResponseFriendDetails::createFromJson(char* data)
{
    FbResponseFriendDetails * res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject ? new FbResponseFriendDetails(rootObject) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}
