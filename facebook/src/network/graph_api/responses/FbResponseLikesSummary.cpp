#include <malloc.h>

#include "FbResponseLikesSummary.h"
#include "jsonutilities.h"
#include "Log.h"
#include "Summary.h"
#include "Utils.h"

FbResponseLikesSummary::FbResponseLikesSummary(JsonObject * object): FbRespondBase(object), mId(nullptr), mLikesSummaryObj(nullptr),
    mOtherWhoLikedId(nullptr), mOtherWhoLikedName(nullptr) {
    if (!mError) {
        mId = GetStringFromJson(object, "id");
        if(mId) { // parse in case of non empty field
            JsonObject *likes = json_object_get_object_member(object, "likes");
            if (likes) {
                JsonObject *summary = json_object_get_object_member(likes, "summary");
                if (summary) {
                    mLikesSummaryObj = new LikesSummary(summary);
                }
                JsonArray *who_liked_array = json_object_get_array_member(likes, "data");
                if (who_liked_array) {
                    int len = json_array_get_length(who_liked_array);
                    Log::info("FbResponseLikesSummary::FbResponseLikesSummary->Parse who liked array, length(max=2) = %d",len);
                    for (int i = 0; i < len; i++) {
                        JsonObject *element = json_array_get_object_element(who_liked_array, i);
                        if (element) {
                            const char *id = GetStringFromJson(element, "id");
                            Log::info("FbResponseLikesSummary::FbResponseLikesSummary->Parse who liked array, id= %s",id);
                            if(Utils::IsMe(id)) {
                                continue;
                            } else {
                                mOtherWhoLikedId = const_cast<char*>(id);
                                mOtherWhoLikedName = GetStringFromJson(element, "name");
                                Log::info("FbResponseLikesSummary::FbResponseLikesSummary->Parse who liked array, user name= %s",mOtherWhoLikedName);
                                break;
                            }
                        }
                    }
                }
            } else {
                Log::error("FbResponseLikesSummary::FbResponseLikesSummary->can't extract 'likes' field");
            }
        } else {
            Log::error("FbResponseLikesSummary::FbResponseLikesSummary->can't extract object id");
        }
    }
}

FbResponseLikesSummary::~FbResponseLikesSummary() {
    free(mId);
    free(mOtherWhoLikedId);
    free(mOtherWhoLikedName);
    delete mLikesSummaryObj;
}

FbResponseLikesSummary* FbResponseLikesSummary::createFromJson (const char* data) {
    Log::info("FbResponseLikesSummary::createFromJson");

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);

    FbResponseLikesSummary * res = rootObject ? new FbResponseLikesSummary(rootObject): nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}
