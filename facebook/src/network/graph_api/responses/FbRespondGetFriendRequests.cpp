#include <json-glib/json-glib.h>

#include "Common.h"
#include "FbRespondGetFriendRequests.h"
#include "FriendRequest.h"
#include "jsonutilities.h"

/**
 * @brief Construct FbRespondGetFriendRequests from JsonObject
 * @param[in] object - Json Object, from which FbRespondGetFriendRequests will be constructed
 */
FbRespondGetFriendRequests::FbRespondGetFriendRequests(JsonObject* object):FbRespondBase(object)
{
    JsonObject* summary = json_object_get_object_member(object, "summary");
    mSummary = summary ? new FbRespondGetFriendRequests::Summary(summary) : nullptr;

    JsonArray * requests_array = json_object_get_array_member(object, "data");
    mRequestsList = nullptr;
    if (requests_array) {
        int len = json_array_get_length(requests_array);

        for (int i = 0; i < len; i++) {
            JsonObject* el = json_array_get_object_element(requests_array, i);
            FriendRequest* requestEl = new FriendRequest(el);
            mRequestsList = eina_list_append(mRequestsList, requestEl);
        }
    }
}

/**
 * @brief Destructor
 */
FbRespondGetFriendRequests::~FbRespondGetFriendRequests()
{
    if ( mRequestsList ) {
        void * listData;
        EINA_LIST_FREE( mRequestsList, listData ) {
            delete static_cast<FriendRequest*>(listData);
        }
    }

    delete mSummary;
}

/**
 * @brief Creates FbRespondGetFriendRequests object fron JSON formatted text
 * @param[in] data - JSON formatted text
 * @return - created FbRespondGetFriendRequests object
 */
FbRespondGetFriendRequests* FbRespondGetFriendRequests::createFromJson(char* data)
{
    FbRespondGetFriendRequests * res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject ? new FbRespondGetFriendRequests(rootObject) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}

/**
 * @brief Construct Summary from JsonObject
 * @param[in] object - Json Object, from which Summary will be constructed
 */
FbRespondGetFriendRequests::Summary::Summary(JsonObject * object): mTotalCount(0),
        mUnreadCount(0), mUpdatedTime(NULL)
{
    if(object) {
        mTotalCount = json_object_get_int_member(object, "total_count");
        mUnreadCount = json_object_get_int_member(object, "unread_count");
        mUpdatedTime = GetStringFromJson(object, "updated_time");
    }
}

/**
 * @brief Destructor
 */
FbRespondGetFriendRequests::Summary::~Summary()
{
    free((void *) mUpdatedTime);
}

Eina_List* FbRespondGetFriendRequests::RemoveDataFromList(void *data)
{
    mRequestsList = eina_list_remove(mRequestsList, data);
    return mRequestsList;
}

