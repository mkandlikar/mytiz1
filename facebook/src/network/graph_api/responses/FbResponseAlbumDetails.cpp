#include "Album.h"
#include "Common.h"
#include "FbResponseAlbumDetails.h"
#include "jsonutilities.h"

#include <malloc.h>

/**
 * @brief Construct FbResponseAlbumDetails object from JsonObject
 * @param[in] object - Json Object, from which FbRespondBase will be constructed
 */
FbResponseAlbumDetails::FbResponseAlbumDetails(JsonObject * object): FbRespondBase(object), mAlbum(nullptr) {
    mAlbum = new Album(object);
}

/**
 * @brief Destructor
 */
FbResponseAlbumDetails::~FbResponseAlbumDetails()
{
    if (mAlbum) {
        mAlbum->Release();
    }
}

/**
 * @brief Creates FbResponseAlbumDetails object fron JSON formatted text
 * @param[in] data - JSON formatted text
 * @return - created FbRespondBase object
 */
FbResponseAlbumDetails* FbResponseAlbumDetails::createFromJson(char* data)
{
    FbResponseAlbumDetails * res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject ? new FbResponseAlbumDetails(rootObject) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}
