#include <efl_extension.h>

#include "Common.h"
#include "FbRespondProfileFeed.h"
#include "jsonutilities.h"
#include "Post.h"

/**
 * @brief Construct FbRespondProfileFeed from JsonObject
 * @param object - Json Object, from which FbRespondProfileFeed will be constructed
 */
FbRespondProfileFeed::FbRespondProfileFeed(JsonObject* object) : FbRespondBase(object)
{
    JsonArray * feed_array = json_object_get_array_member(object, "data");
    mFeedList = nullptr;
    if (feed_array) {
        int len = json_array_get_length(feed_array);
        for (int i = 0; i < len; i++) {
            JsonObject *el = json_array_get_object_element(feed_array, i);
            Post *post = new Post(el);
            mFeedList = eina_list_append(mFeedList, post);
        }
    }
}

FbRespondProfileFeed::~FbRespondProfileFeed()
{
    if (mFeedList) {
        void * listData;
        EINA_LIST_FREE( mFeedList, listData )
        {
            static_cast<Post*>(listData)->Release();
        }
        mFeedList = nullptr;
    }
}

/**
 * @brief Creates FbRespondProfileFeed object from JSON formatted text
 * @param[in] data - JSON formatted text
 * @return - created FbRespondProfileFeed object
 */
FbRespondProfileFeed* FbRespondProfileFeed::createFromJson(char* data)
{
    FbRespondProfileFeed * res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject ? new FbRespondProfileFeed(rootObject) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}
