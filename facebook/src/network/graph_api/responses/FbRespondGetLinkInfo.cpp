#include <json-glib/json-glib.h>

#include "Common.h"
#include "FbRespondGetLinkInfo.h"
#include "jsonutilities.h"

FbRespondGetLinkInfo::FbRespondGetLinkInfo(JsonObject* object):FbRespondBase(object)
{
    mUrl = new Url(object);
}

FbRespondGetLinkInfo::~FbRespondGetLinkInfo()
{
    mUrl->Release();
}

FbRespondGetLinkInfo* FbRespondGetLinkInfo::createFromJson(char* data)
{
    FbRespondGetLinkInfo * res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject ? new FbRespondGetLinkInfo(rootObject) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}
