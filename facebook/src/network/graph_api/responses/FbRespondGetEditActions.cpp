#include "EditAction.h"
#include "FbRespondGetEditActions.h"
#include "jsonutilities.h"
#include "Log.h"

FbRespondGetEditActions::FbRespondGetEditActions(JsonObject* object) : FbRespondBase(object)
{
    mEditActionsList = nullptr;
    Log::info("FbRespondGetEditActions::FbRespondGetEditActions()");

    JsonObject * action_member = json_object_get_object_member(object, "edit_actions");
    if (action_member) {
        JsonArray * actions_array = json_object_get_array_member(action_member, DATA);
        if (actions_array) {
            int len = json_array_get_length(actions_array);
            for (int i = 0; i < len; i++) {
                JsonObject* el = json_array_get_object_element(actions_array, i);
                if (el) {
                    EditAction* eventEl = new EditAction(el);
                    if (eventEl) {
                        mEditActionsList = eina_list_append(mEditActionsList, eventEl);
                    }
                }
            }
        }
    }
}

FbRespondGetEditActions::~FbRespondGetEditActions()
{
    if (mEditActionsList) {
        void * listData;
        EINA_LIST_FREE(mEditActionsList, listData)
        {
            static_cast<EditAction*>(listData)->Release();
        }
    }
}

FbRespondGetEditActions* FbRespondGetEditActions::createFromJson(char* data)
{
    Log::info("FbRespondGetEditActions::createFromJson");
    FbRespondGetEditActions * res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject ? new FbRespondGetEditActions(rootObject) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}

Eina_List* FbRespondGetEditActions::RemoveDataFromList(void *data)
{
    mEditActionsList = eina_list_remove(mEditActionsList, data);
    return mEditActionsList;
}

