#include "Common.h"
#include "FbRespondPostObjectId.h"
#include "jsonutilities.h"

#include <malloc.h>
#include <stddef.h>

FbRespondPostObjectId::FbRespondPostObjectId(JsonObject * object) : FbRespondBase(object) {
    JsonObject *album = json_object_get_object_member(object, "album");
    mAlbum = album ? new Album(album) : nullptr;
}

FbRespondPostObjectId::~FbRespondPostObjectId()
{
    if (mAlbum) {
        mAlbum->Release();
    }
}

FbRespondPostObjectId* FbRespondPostObjectId::createFromJson(const char* data)
{
    JsonObject* rootObject = nullptr;
    JsonParser* parser = openJsonParser(data, &rootObject);
    FbRespondPostObjectId *res = rootObject ? new FbRespondPostObjectId(rootObject) : nullptr;

    g_object_unref(parser);
    return res;
}
