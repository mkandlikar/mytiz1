#include <json-glib/json-glib.h>
#include <efl_extension.h>

#include "Common.h"
#include "jsonutilities.h"
#include "FbRespondUserProfileData.h"
#include "Log.h"
#include "UserProfileData.h"
#include "ParserWrapper.h"

FbRespondUserProfileData::FbRespondUserProfileData(JsonObject* object) : FbRespondBase(object)
{
    Log::info(LOG_FACEBOOK_USER, "FbRespondUserProfileData::FbRespondUserProfileData()");
    mDataList = nullptr;
    UserProfileData *data = new UserProfileData(object);
    mDataList = eina_list_append(mDataList, data);
}

FbRespondUserProfileData::~FbRespondUserProfileData()
{
    if (mDataList) {
        void * listData;
        EINA_LIST_FREE(mDataList, listData)
        {
            static_cast<UserProfileData*>(listData)->Release();
        }
    }
}

FbRespondUserProfileData* FbRespondUserProfileData::createFromJson(char* data)
{
    Log::info("FbRespondUserProfileData::createFromJson");
    FbRespondUserProfileData *res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);
    ParserWrapper wr(parser);

    res = rootObject ? new FbRespondUserProfileData(rootObject) : nullptr;

    return res;
}

Eina_List* FbRespondUserProfileData::RemoveDataFromList(void *data)
{
    mDataList = eina_list_remove(mDataList, data);
    return mDataList;
}
