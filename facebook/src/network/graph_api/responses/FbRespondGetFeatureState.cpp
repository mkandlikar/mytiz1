#include "FbRespondGetFeatureState.h"
#include "jsonutilities.h"
#include "Common.h"
#include "Log.h"
#include "Utils.h"

/**
 * @brief Constructs FbRespondGetFeatureState from JSON root node
 * @param[in] object - JSON object to parse out
 */
FbRespondGetFeatureState::FbRespondGetFeatureState(JsonNode* rootNode) : FbRespondBaseRestApi(rootNode) {
    JsonObject* object = json_node_get_object(rootNode);
    char* result = GetStringFromJson(object, "result");
    char* checkSum = GetStringFromJson(object, "hash");
    char* calculatedCheckSum = Utils::ComputeHash(result);

    mConfig = checkSum && calculatedCheckSum && !strcmp(checkSum, calculatedCheckSum) ? SAFE_STRDUP(result) : nullptr;

    free(result);
    free(checkSum);
    free(calculatedCheckSum);
}

/**
 * @brief Destruction
 */
FbRespondGetFeatureState::~FbRespondGetFeatureState() {
    free((void*) mConfig);
}

/**
 * @brief Creates FbRespondGetFeatureState based on passed JSON string
 * @param data - JSON formatted string
 * @return - created FbRespondGetFeatureState object
 */
FbRespondGetFeatureState* FbRespondGetFeatureState::createFromJson(const char* data) {
    FbRespondGetFeatureState* result = nullptr;
    JsonParser* parser = openJsonParser(data, nullptr);
    JsonNode* rootNode = json_parser_get_root(parser);

    // REST API response can have an array instead of object in the root
    result = rootNode ? new FbRespondGetFeatureState(rootNode) : nullptr;
    // Parser object should be unreferenced after parsing is completed
    g_object_unref(parser);

    return result;
}
