#include <efl_extension.h>

#include "Common.h"
#include "CSmartPtr.h"
#include "FbRespondGetFriendRequestsBatch.h"
#include "FriendRequest.h"
#include "jsonutilities.h"

/**
 * @brief Construct FbRespondGetFriendRequests from JsonObject
 * @param[in] object - Json Object, from which FbRespondGetFriendRequests will be constructed
 */
FbRespondGetFriendRequestsBatch::FbRespondGetFriendRequestsBatch(JsonNode* rootNode):FbRespondBase(rootNode)
{
    mUsersList = nullptr;
    mRespondGetFriendRequests = nullptr;
    if (!mError) {
        JsonArray * responses_array = json_node_get_array(rootNode);
        if (responses_array) {
            int len = json_array_get_length(responses_array);
            //get first element, which contains response for batch Request1
            JsonObject* el0 = json_array_get_object_element(responses_array, 0);

            if (el0) {
                c_unique_ptr<char> bodyStr(GetStringFromJson(el0, "body"));
                mRespondGetFriendRequests = FbRespondGetFriendRequests::createFromJson(bodyStr.get());
            }

            if (mRespondGetFriendRequests && mRespondGetFriendRequests->mSummary && mRespondGetFriendRequests->mSummary->mTotalCount > 0) {
                // get second element, which contains response for batch Request2
                JsonObject* el = json_array_get_object_element(responses_array, 1);

                if (el) {
                    c_unique_ptr<char> bodyStr(GetStringFromJson(el, "body"));
                    if (bodyStr) {
                        JsonObject * bodyObject;
                        JsonParser * parser = openJsonParser(bodyStr.get(), &bodyObject);

                        GList * keysList = nullptr;
                        guint size;

                        size = json_object_get_size(bodyObject);
                        keysList = json_object_get_members(bodyObject);

                        gchar *keyName;

                        for (int j = 0; j < size; j++) {
                            keyName = (gchar *) keysList->data;

                            JsonObject* member = json_object_get_object_member(bodyObject, keyName);
                            if (member) {
                                User* user = new User(member);
                                mUsersList = eina_list_append(mUsersList, user);

                            }
                            keysList = g_list_next(keysList);
                        }

                        if (keysList) {
                            g_list_free(keysList);
                        }

                        g_object_unref(parser);
                    }
                }
            }
        }
    }
}

/**
 * @brief Destructor
 */
FbRespondGetFriendRequestsBatch::~FbRespondGetFriendRequestsBatch()
{
    if (mUsersList)
    {
        void * listData;
        EINA_LIST_FREE(mUsersList, listData) {
            static_cast<User*>(listData)->Release();
        }
    }

    delete mRespondGetFriendRequests;
}

/**
 * @brief Creates FbRespondGetFriendRequests object fron JSON formatted text
 * @param[in] data - JSON formatted text
 * @return - created FbRespondGetFriendRequests object
 */
FbRespondGetFriendRequestsBatch* FbRespondGetFriendRequestsBatch::createFromJson(char* data)
{
    FbRespondGetFriendRequestsBatch * res = nullptr;

    JsonParser * parser = openJsonParser(data, nullptr);

    //We use root node here, because REST API respond can have array instead object as root element
    JsonNode* rootNode = json_parser_get_root (parser);

    res = rootNode ? new FbRespondGetFriendRequestsBatch(rootNode) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}

Eina_List* FbRespondGetFriendRequestsBatch::RemoveDataFromList(void *data)
{
    mUsersList = eina_list_remove(mUsersList, data);
    return mUsersList;
}
