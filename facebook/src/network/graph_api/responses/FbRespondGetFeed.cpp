#include <efl_extension.h>
#include <json-glib/json-glib.h>

#include "Common.h"
#include "FbRespondGetFeed.h"
#include "jsonutilities.h"
#include "Post.h"

/**
 * @brief Construct FbRespondGetFriends from JsonObject
 * @param object - Json Object, from which FbRespondGetFriends will be constructed
 */
FbRespondGetFeed::FbRespondGetFeed(JsonObject* object) : FbRespondBase(object)
{
    JsonArray * feed_array = json_object_get_array_member(object, "data");
    mPostsList = nullptr;
    if (feed_array) {
        int len = json_array_get_length(feed_array);
        for (int i = 0; i < len; i++) {
            JsonObject *el = json_array_get_object_element(feed_array, i);
            if (el) {
                Post *post = new Post(el);
                mPostsList = eina_list_append(mPostsList, post);
            }
        }
    }
}

/**
 * @brief Destructor
 */
FbRespondGetFeed::~FbRespondGetFeed()
{
    if ( mPostsList ) {
        void * listData;
        EINA_LIST_FREE(mPostsList, listData) {
            static_cast<Post*>(listData)->Release();
        }
    }

}

/**
 * @brief Creates FbRespondGetFeed object from JSON formatted text
 * @param[in] data - JSON formatted text
 * @return - created FbRespondGetFeed object
 */
FbRespondGetFeed* FbRespondGetFeed::createFromJson(const char* data)
{
    FbRespondGetFeed * res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject ? new FbRespondGetFeed(rootObject) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}

Eina_List* FbRespondGetFeed::GetPostsList() const
{
    return mPostsList;
}

Eina_List* FbRespondGetFeed::RemoveDataFromList(void *data)
{
    mPostsList = eina_list_remove(mPostsList, data);
    return mPostsList;
}
