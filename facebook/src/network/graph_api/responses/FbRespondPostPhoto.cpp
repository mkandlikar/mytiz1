#include <efl_extension.h>

#include "Common.h"
#include "FbRespondPostPhoto.h"
#include "jsonutilities.h"

/**
 * @brief Construct FbRespondPostPhoto from JsonObject
 * @param[in] object - Json Object, from which FbRespondPostPhoto will be constructed
 */
FbRespondPostPhoto::FbRespondPostPhoto(JsonObject* object) : FbRespondBase(object)
    ,mId(NULL), mPostId(NULL)
{
    mId = GetStringFromJson(object, "id");
    mPostId = GetStringFromJson(object, "post_id");
}

/**
 * @brief Destruction
 */
FbRespondPostPhoto::~FbRespondPostPhoto()
{
    free(mId);
    free(mPostId);
}

/**
 * @brief Creates FbRespondPostPhoto object fron JSON formatted text
 * @param data - JSON formatted text
 * @return - created FbRespondPostPhoto object
 */
FbRespondPostPhoto* FbRespondPostPhoto::createFromJson(char* data)
{
    FbRespondPostPhoto * res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject ? new FbRespondPostPhoto(rootObject) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}

