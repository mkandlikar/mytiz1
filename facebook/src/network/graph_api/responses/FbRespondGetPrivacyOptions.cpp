/*
 * FbRespondGetPrivacyOptions.cpp
 *
 *  Created on: Jul 7, 2015
 *      Author: RUINMKOL
 */
#include <efl_extension.h>

#include "FbRespondGetPrivacyOptions.h"
#include "Common.h"
#include "jsonutilities.h"

/**
 * @brief Construct FbRespondGetPrivacyOptions from JsonObject
 * @param[in] object - Json Object, from which FbRespondGetPrivacyOptions will be constructed
 */
FbRespondGetPrivacyOptions::FbRespondGetPrivacyOptions(JsonObject* object):FbRespondBase(object)
{
    JsonArray * options_array = json_object_get_array_member(object, "data");
    mPrivacyOptionsList = nullptr;
    if (options_array) {
        int len = json_array_get_length(options_array);

        for (int i = 0; i < len; i++) {
            JsonObject* el = json_array_get_object_element(options_array, i);
            PrivacyOption* option = new PrivacyOption(el);
            mPrivacyOptionsList = eina_list_append(mPrivacyOptionsList, option);
        }
    }
}

/**
 * @brief Destruction
 */
FbRespondGetPrivacyOptions::~FbRespondGetPrivacyOptions()
{
    if (mPrivacyOptionsList) {
        void * listData;
        EINA_LIST_FREE( mPrivacyOptionsList, listData )
        {
            delete static_cast<PrivacyOption*>(listData);
        }
    }
}

/**
 * @brief Creates FbRespondGetPrivacyOptions object fron JSON formatted text
 * @param data - JSON formatted text
 * @return - created FbRespondGetPrivacyOptions object
 */
FbRespondGetPrivacyOptions* FbRespondGetPrivacyOptions::createFromJson(const char* data)
{
    FbRespondGetPrivacyOptions * res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject ? new FbRespondGetPrivacyOptions(rootObject) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}

/**
 * @brief Construct PrivacyOption from JsonObject
 * @param[in] object - Json Object, from which PrivacyOption will be constructed
 */
FbRespondGetPrivacyOptions::PrivacyOption::PrivacyOption(JsonObject * object)
{
    mId = GetStringFromJson(object, "id");
    mDescription = GetStringFromJson(object, "description");
    mType = GetStringFromJson(object, "type");
    mIconSrc = GetStringFromJson(object, "icon_src");
    mIsCurrentlySelected = json_object_get_boolean_member(object, "is_currently_selected");
}

/**
 * @brief Destruction
 */
FbRespondGetPrivacyOptions::PrivacyOption::~PrivacyOption()
{
    free((void*) mId);
    free((void*) mDescription);
    free((void*) mType);
    free((void*) mIconSrc);
}
