#include "Common.h"
#include "FbRespondBase.h"
#include "Log.h"
#include "jsonutilities.h"

const char* FbRespondBase::DATA = "data";
/**
 * @brief Construct FbRespondBase object from JsonObject
 * @param[in] object - Json Object, from which FbRespondBase will be constructed
 */
FbRespondBase::FbRespondBase(JsonObject * object)
{
    Init(object);
}

/**
 * @brief Construct FbRespondBase object from JsonNode
 * @param object - JsonNode, from which FbRespondBase will be constructed
 */
FbRespondBase::FbRespondBase(JsonNode* rootNode)
{
    JsonObject * object = json_node_get_object(rootNode);
    Init(object);
}

void FbRespondBase::Init(JsonObject * object)
{
    mError = nullptr;
    mSuccess = false;

    if (object) {
        JsonObject * error = json_object_get_object_member(object, "error");
        mError = error ? new Error(error) : nullptr;

        gboolean success = json_object_get_boolean_member(object, "success");
        mSuccess = success > 0;
    }
}

/**
 * @brief Destructor
 */
FbRespondBase::~FbRespondBase()
{
    delete mError;
}


/**
 * @brief Creates FbRespondBase object fron JSON formatted text
 * @param[in] data - JSON formatted text
 * @return - created FbRespondBase object
 */
FbRespondBase* FbRespondBase::createFromJson(const char* data)
{
    Log::info("FbRespondBase::createFromJson");
    FbRespondBase * res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject ? new FbRespondBase(rootObject): nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}
