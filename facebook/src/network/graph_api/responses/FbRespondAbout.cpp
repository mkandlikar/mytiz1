#include <app.h>
#include <json-glib/json-glib.h>

#include "Common.h"
#include "FbRespondAbout.h"
#include "jsonutilities.h"


FbRespondAbout::FbRespondAbout(JsonObject* JsonRoot) :
    FbRespondBase(JsonRoot),
    mOverviewList(nullptr), mAboutPicUrl(nullptr)
{
    const char* Heading = nullptr;
    const char* Info = nullptr;
    const char* Tmp = nullptr;
    ListItem* NewListItem = nullptr;
    JsonArray* JsonArrayWork = json_object_get_array_member(JsonRoot, "work");
    if (JsonArrayWork) {
        JsonObject* JsonObjWork = json_array_get_object_element(JsonArrayWork, 0);
        if (JsonObjWork) {
            JsonObject* JsonObjEmployer = json_object_get_object_member(JsonObjWork, "employer");
            if (JsonObjEmployer) {
                JsonObject* JsonObjPosition = json_object_get_object_member(JsonObjWork, "position");
                if (JsonObjPosition)
                    Heading = json_object_get_string_member(JsonObjPosition, "name");
                Info = json_object_get_string_member(JsonObjEmployer, "name");
                if (Heading && Info) {
                    NewListItem = new ListItem();
                    NewListItem->mHeading = Heading;
                    NewListItem->mInfo = Info;
                    NewListItem->mImage = "about_work.png";
                    NewListItem->mAt = true;

                    JsonObject* JsonObjPastWork = json_array_get_object_element(JsonArrayWork, 1);
                    if (JsonObjPastWork) {
                        JsonObject* JsonObjPastEmployer = json_object_get_object_member(JsonObjPastWork, "employer");
                        if (JsonObjPastEmployer) {
                            Tmp = json_object_get_string_member(JsonObjPastEmployer, "name");
                            if (Tmp) {
                                NewListItem->mPast = i18n_get_text("IDS_PAST");
                                NewListItem->mPast.append(": ");
                                NewListItem->mPast.append(Tmp);
                            }
                        }
                    }
                    mOverviewList = eina_list_append(mOverviewList, NewListItem);
                }
                JsonObject* JsonObjPicture = json_object_get_object_member(JsonObjEmployer, "picture");
                if (JsonObjPicture) {
                    JsonObject* JsonObjData = json_object_get_object_member(JsonObjPicture, "data");
                    Tmp = json_object_get_string_member(JsonObjData, "url");
                    if (Tmp)
                        mAboutPicUrl = strdup(Tmp);
                }
            }
        }
    }

    Heading = Info = Tmp = NULL;
    JsonArray* JsonArrayEducation = json_object_get_array_member(JsonRoot, "education");
    if (JsonArrayEducation) {
        JsonObject* JsonObjEducation;
        if (json_array_get_length(JsonArrayEducation) < 2) {
            JsonObjEducation = json_array_get_object_element(JsonArrayEducation, 0);
        }
        else {
            JsonObject* JsonObjPastEducation = json_array_get_object_element(JsonArrayEducation, 0);
            if (JsonObjPastEducation) {
                JsonObject* JsonObjPastSchool = json_object_get_object_member(JsonObjPastEducation, "school");
                if (JsonObjPastSchool)
                    Tmp = json_object_get_string_member(JsonObjPastSchool, "name");
            }
            JsonObjEducation = json_array_get_object_element(JsonArrayEducation, 1);
        }
        if (JsonObjEducation) {
            JsonArray* JsonArrayConcentration = json_object_get_array_member(JsonObjEducation, "concentration");
            if (JsonArrayConcentration) {
                JsonObject* JsonObjConcentration = json_array_get_object_element(JsonArrayConcentration, 0);
                if (JsonObjConcentration)
                    Heading = json_object_get_string_member(JsonObjConcentration, "name");
            }
            JsonObject* JsonObjSchool = json_object_get_object_member(JsonObjEducation, "school");
            if (JsonObjSchool)
                Info = json_object_get_string_member(JsonObjSchool, "name");
        }
        if (Heading && Info) {
            NewListItem = new ListItem();
            NewListItem->mHeading = i18n_get_text("IDS_STUDIED");
            NewListItem->mHeading.append(" ");
            NewListItem->mHeading.append(Heading);
            NewListItem->mInfo = Info;
            if (Tmp) {
                NewListItem->mPast = i18n_get_text("IDS_PAST");
                NewListItem->mPast.append(": ");
                NewListItem->mPast.append(Tmp);
            }
            NewListItem->mImage = "about_education.png";
            NewListItem->mAt = true;
            mOverviewList = eina_list_append(mOverviewList, NewListItem);
        }
    }

    Heading = Info = Tmp = NULL;
    JsonObject* JsonObjLocation = json_object_get_object_member(JsonRoot, "location");
    if (JsonObjLocation) {
        Info = json_object_get_string_member(JsonObjLocation, "name");
        if (Info) {
            NewListItem = new ListItem();
            NewListItem->mHeading = i18n_get_text("IDS_LIVESIN");
            NewListItem->mHeading.append(" ");
            NewListItem->mInfo = Info;
            NewListItem->mImage = "about_lives.png";
            mOverviewList = eina_list_append(mOverviewList, NewListItem);
        }
    }

    Heading = Info = NULL;
    JsonObject* JsonObjHomeTown = json_object_get_object_member(JsonRoot, "hometown");
    if (JsonObjHomeTown) {
        Info = json_object_get_string_member(JsonObjHomeTown, "name");
        if (Info) {
            NewListItem = new ListItem();
            NewListItem->mHeading = i18n_get_text("IDS_FROM");
            NewListItem->mHeading.append(" ");
            NewListItem->mInfo = Info;
            NewListItem->mImage = "about_location.png";
            mOverviewList = eina_list_append(mOverviewList, NewListItem);
        }
    }

    Heading = Info = Tmp = NULL;
    Tmp = json_object_get_string_member(JsonRoot, "relationship_status");
    if (Tmp) {
        NewListItem = new ListItem();
        NewListItem->mHeading = Tmp;
        NewListItem->mImage = "Relationship.png";
        mOverviewList = eina_list_append(mOverviewList, NewListItem);
    }

    NewListItem = new ListItem();
    //TODO: Translations
    NewListItem->mHeading = "More about you";//i18n_get_text("IDS_MORE_HEADING");
    NewListItem->mInfo = "Birthday, family and more.";//i18n_get_text("IDS_MORE_INFO");
    NewListItem->mMore = true;
    mOverviewList = eina_list_append(mOverviewList, NewListItem);
}

FbRespondAbout::~FbRespondAbout() {
    free(mAboutPicUrl);
    if (mOverviewList) {
        void* ListItemData;
        EINA_LIST_FREE(mOverviewList, ListItemData) {
            delete static_cast<ListItem*>(ListItemData);
        }
    }
}

FbRespondAbout* FbRespondAbout::Parse(char* Respond) {
    JsonObject* JsonRoot;
    JsonParser* JsonParser = openJsonParser(Respond, &JsonRoot);
    FbRespondAbout* Parsed = (JsonRoot ? new FbRespondAbout(JsonRoot): NULL);
    g_object_unref(JsonParser);
    return Parsed;
}
