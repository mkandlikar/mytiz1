#include <malloc.h>

#include "Common.h"
#include "FbResponseGroupDetails.h"
#include "jsonutilities.h"
#include "Utils.h"

/**
 * @brief Construct FbResponseGroupDetails object from JsonObject
 * @param[in] object - Json Object, from which FbRespondBase will be constructed
 */
FbResponseGroupDetails::FbResponseGroupDetails(JsonObject * object) : FbRespondBase(object)
{
    mCoverUrl = nullptr;
    JsonObject *cover = json_object_get_object_member(object, "cover");
    if (cover) {
        mCoverUrl = GetStringFromJson(cover, "source");
    }
}

/**
 * @brief Destructor
 */
FbResponseGroupDetails::~FbResponseGroupDetails()
{

    free((void*) mCoverUrl);

}

/**
 * @brief Creates FbResponseGroupDetails object fron JSON formatted text
 * @param[in] data - JSON formatted text
 * @return - created FbRespondBase object
 */
FbResponseGroupDetails* FbResponseGroupDetails::createFromJson(char* data)
{
    FbResponseGroupDetails * res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject ? new FbResponseGroupDetails(rootObject) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);
    return res;
}




