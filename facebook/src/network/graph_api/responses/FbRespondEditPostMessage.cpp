#include <string>

#include "Common.h"
#include "CSmartPtr.h"
#include "FbRespondBase.h"
#include "FbRespondEditPostMessage.h"
#include "Log.h"
#include "jsonutilities.h"
#include "ParserWrapper.h"

/**
 * @brief Construct FbRespondEditPostMessage object from JsonObject
 * @param[in] object - Json Object, from which FbRespondBase will be constructed
 */
FbRespondEditPostMessage::FbRespondEditPostMessage(JsonObject * object): FbRespondBase(object)
{
	mIsSuccess = false;
    //result
	mIsSuccess = json_object_get_boolean_member(object, "success");
    Log::debug("FbRespondEditPostMessage, mIsSuccess = %d", mIsSuccess);

    JsonArray *actionsArray = json_object_get_array_member(object, "actions");
    if (actionsArray) {
        int length = json_array_get_length(actionsArray);
        for (int i = 0; i < length; ++i) {
            JsonObject *action = json_array_get_object_element(actionsArray, i);
            if (action) {
                c_unique_ptr<char> actionName(GetStringFromJson(action, "name"));
                if (actionName && !strcmp(actionName.get(), "Share")) {
                    mCanShare = true;
                    break;
                } else {
                    mCanShare = false;
                }
            }
        }
    }
}

/**
 * @brief Destructor
 */
FbRespondEditPostMessage::~FbRespondEditPostMessage()
{
    //
}


/**
 * @brief Creates FbRespondEditPostMessage object fron JSON formatted text
 * @param[in] data - JSON formatted text
 * @return - created FbRespondBase object
 */
FbRespondEditPostMessage* FbRespondEditPostMessage::createFromJson(char* data)
{
    Log::info("FbRespondEditPostMessage::createFromJson");
    FbRespondEditPostMessage * res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);
    ParserWrapper wr( parser );
    res = rootObject ? new FbRespondEditPostMessage(rootObject) : nullptr;
    return res;
}
