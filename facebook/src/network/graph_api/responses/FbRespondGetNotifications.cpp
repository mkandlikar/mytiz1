#include <efl_extension.h>
#include "jsonutilities.h"
#include <json-glib/json-glib.h>

#include "FbRespondGetNotifications.h"
#include "Log.h"
#include "NotificationObject.h"

FbRespondGetNotifications::FbRespondGetNotifications(JsonObject *object) : FbRespondBase(object)
{
    Log::info(LOG_FACEBOOK_NOTIFICATION, "FbRespondGetNotifications::FbRespondGetNotifications");
    JsonArray *notification_array = json_object_get_array_member(object, "data");
    mNotificationList = nullptr;
    if (notification_array) {
        int len = json_array_get_length(notification_array);
        for (int i = 0; i < len; ++i) {
            JsonObject* el = json_array_get_object_element(notification_array, i);
            if (el) {
                NotificationObject* notif = new NotificationObject(el);
                if (notif) {
                    mNotificationList = eina_list_append(mNotificationList, notif);
                }
            }
        }
    }

    mUnseenCount = 0;
    JsonObject *notificationSummary= json_object_get_object_member(object, "summary");
    if (notificationSummary) {
        mUnseenCount = json_object_get_int_member(notificationSummary, "unseen_count");
    }
}

FbRespondGetNotifications::~FbRespondGetNotifications()
{
    if (mNotificationList) {
        void * listData;
        EINA_LIST_FREE(mNotificationList, listData)
        {
            static_cast<NotificationObject*>(listData)->Release();
        }
    }
}

FbRespondGetNotifications* FbRespondGetNotifications::createFromJson(char* data)
{
    Log::info(LOG_FACEBOOK_NOTIFICATION, "FbRespondGetNotifications::createFromJson");
    FbRespondGetNotifications * res = nullptr;

    JsonObject * rootObject = nullptr;
    JsonParser * parser = openJsonParser(data, &rootObject);

    res = rootObject ? new FbRespondGetNotifications(rootObject) : nullptr;

    //Attention!! parser object should be unreferenced after parsing is completed
    g_object_unref(parser);

    return res;
}

Eina_List* FbRespondGetNotifications::RemoveDataFromList(void *data)
{
    mNotificationList = eina_list_remove(mNotificationList, data);
    return mNotificationList;
}

