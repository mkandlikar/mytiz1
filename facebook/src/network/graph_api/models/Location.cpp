#include <malloc.h>
#include <string.h>

#include "Common.h"
#include "jsonutilities.h"
#include "Location.h"
#include "WidgetFactory.h"
#include <memory>

/**
 * @brief Construct Location object from JsonObject
 * @param[in] object - Json Object, from which Location will be constructed
 */
Location::Location(JsonObject * object)
{
    mCity = GetStringFromJson(object, "city");
    mCountry = GetStringFromJson(object, "country");

    mLatitude = json_object_get_double_member(object, "latitude");

    mLocatedIn = GetStringFromJson(object, "located_in");
    mLongitude = json_object_get_double_member(object, "longitude");
    mName = GetStringFromJson(object, "name");
    mRegion = GetStringFromJson(object, "region");
    mState = GetStringFromJson(object, "state");
    mStreet= GetStringFromJson(object, "street");
    mZip = GetStringFromJson(object, "zip");

}

Location::Location(const Location & other)
{
    mCity = SAFE_STRDUP(other.mCity);
    mCountry = SAFE_STRDUP(other.mCountry);
    mLocatedIn = SAFE_STRDUP(other.mLocatedIn);
    mName = SAFE_STRDUP(other.mName);
    mRegion = SAFE_STRDUP(other.mRegion);
    mState = SAFE_STRDUP(other.mState);
    mStreet = SAFE_STRDUP(other.mStreet);
    mZip = SAFE_STRDUP(other.mZip);

    mLatitude = other.mLatitude;
    mLongitude = other.mLongitude;
}

/**
 * @brief Destruction
 */
Location::~Location()
{
    free ((void*) mCity);
    free ((void*) mCountry);
    free ((void*) mLocatedIn);
    free ((void*) mName);
    free ((void*) mRegion);
    free ((void*) mState);
    free ((void*) mStreet);
    free ((void*) mZip);
}

std::unique_ptr<char[]> Location::GetAddress()
{
    std::unique_ptr<char[]> address(WidgetFactory::WrapByFormat4("%s, %s, %s, %s", mStreet, mCity, mCountry, mZip));
    return std::move(address);
}

