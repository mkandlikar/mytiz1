/*
 * Place.cpp
 *
 *  Created on: Jun 29, 2015
 *      Author: RUINMKOL
 */

#include "Common.h"
#include "Place.h"
#include "jsonutilities.h"

/**
 * @brief Construct Place object from JsonObject
 * @param object - Json Object, from which Place will be constructed
 */
Place::Place(JsonObject * object)
{
    SetId( GetStringFromJson(object, "id") );

    mLocation = nullptr;
    JsonObject *locationObject = json_object_get_object_member(object, "location");
    if (locationObject)
    {
        mLocation = new Location(locationObject);
    }

    mName = GetStringFromJson(object, "name");
    mCategory = GetStringFromJson(object, "category");
}

Place::Place(const Place & other)
{
    SetId(SAFE_STRDUP(other.GetId()));

    mLocation = other.mLocation ? new Location(*other.mLocation) : nullptr;
    mName = SAFE_STRDUP(other.mName);
    mCategory = SAFE_STRDUP(other.mCategory);
}

/**
 * @brief Destruction
 */

Place::~Place()
{
    delete mLocation;
    free((void*) mName);
    free((void*) mCategory);
}

