#include "Common.h"
#include "Friend.h"
#include "jsonutilities.h"
#include "MutualFriendsList.h"
#include "Queries.h"
#include "Utils.h"

/**
 * @brief Construct Friend object from JsonObject
 * @param[in] object - Json Object, from which Friend will be constructed
 */
Friend::Friend(JsonObject* object) :
    Person(object)
{
    char *id = GetStringFromJson(object, "id");
    GraphObject::SetId(id);
    mDownloadImageReqId = -1;
    const char* name = json_object_get_string_member(object, "name");
    mName = name ? name : "";

    JsonObject *picture = json_object_get_object_member(object, "picture");
    if (picture) {
        picture = json_object_get_object_member(picture, "data");
        const char* pictureUrl = json_object_get_string_member(picture, "url");
        mPicturePath = pictureUrl ? pictureUrl : "";
    }

#ifdef FEATURE_MUTUAL_FRIENDS
    mMutualFriendsCount = 0;
    JsonObject* context = json_object_get_object_member(object, "context");
    if (context) {
        JsonObject* mutual_friends = json_object_get_object_member(context, "mutual_friends");
        if (mutual_friends) {
            JsonObject* summary = json_object_get_object_member(mutual_friends, "summary");
            if (summary) {
                mMutualFriendsCount = json_object_get_int_member(summary, "total_count");
            }
        } else {
            mMutualFriendsCount = MutualFriendsList::GetInstance()->GetUsersMutualFriends(id);
        }
    } else {
        mMutualFriendsCount = MutualFriendsList::GetInstance()->GetUsersMutualFriends(id);
    }
#else
    mMutualFriendsCount = MutualFriendsList::GetInstance()->GetUsersMutualFriends(id);
#endif

    const char* birthday = json_object_get_string_member(object, "birthday");
    mBirthday = birthday ? birthday : "";

    const char* firstName = json_object_get_string_member(object, "first_name");
    mFirstName = firstName ? firstName : "";
    const char* lastName = json_object_get_string_member(object, "last_name");
    mLastName = lastName ? lastName : "";

    if (!mBirthday.empty()) {
        char* turningInfo = Utils::GetTurningInfo(mBirthday.c_str());
        mTurningInfo = turningInfo ? turningInfo : "";
        free(turningInfo);
    }

    const char* email = json_object_get_string_member(object, "email");
    mEmail = email ? email : "";
    const char* phone = json_object_get_string_member(object, "mobile_phone");
    mPhone = phone ? phone : "";

    mGraphObjectType = GOT_FRIEND;

    isBdayNextYear = false;
}

Friend::Friend(Friend&& friendObj):
    isBdayNextYear(friendObj.isBdayNextYear),
    mName(std::move(friendObj.mName)),
    mMutualFriendsCount(friendObj.mMutualFriendsCount),
    mPicturePath(std::move(friendObj.mPicturePath)),
    mBirthday(std::move(friendObj.mBirthday)),
    mTurningInfo(std::move(friendObj.mTurningInfo)),
    mFirstName(std::move(friendObj.mFirstName)),
    mLastName(std::move(friendObj.mLastName)),
    mEmail(std::move(friendObj.mEmail)),
    mPhone(std::move(friendObj.mPhone)),
    mDownloadImageReqId(friendObj.mDownloadImageReqId) {
    GraphObject::SetId(SAFE_STRDUP(friendObj.GetId()));
    mGraphObjectType = friendObj.mGraphObjectType,
    friendObj.mDownloadImageReqId = -1;
    friendObj.mMutualFriendsCount = 0;
    friendObj.isBdayNextYear = false;
    mFriendshipStatus = friendObj.mFriendshipStatus;
}

Friend::Friend(const Friend& friendObj) :
    isBdayNextYear(friendObj.isBdayNextYear),
    mName(friendObj.mName),
    mMutualFriendsCount(friendObj.mMutualFriendsCount),
    mPicturePath(friendObj.mPicturePath),
    mBirthday(friendObj.mBirthday),
    mTurningInfo(friendObj.mTurningInfo),
    mFirstName(friendObj.mFirstName),
    mLastName(friendObj.mLastName),
    mEmail(friendObj.mEmail),
    mPhone(friendObj.mPhone),
    mDownloadImageReqId(-1) {
    GraphObject::SetId(SAFE_STRDUP(friendObj.GetId()));
    mGraphObjectType = friendObj.mGraphObjectType;
    mFriendshipStatus= friendObj.mFriendshipStatus;
}

/**
 * @brief Destructor
 */
Friend::~Friend() {
}

Friend::Friend(char **name, char **value):
    isBdayNextYear(false),
    mDownloadImageReqId(-1) {
    mGraphObjectType = GOT_FRIEND;
    CHECK_RET_NRV(value);
    GraphObject::SetId( Utils::GetStringOrNull(value[Queries::SelectNFriends::ID]) );
    char* userName = Utils::GetStringOrNull(value[Queries::SelectNFriends::NAME]);
    mName = userName ? userName : "";
    free(userName);
    char* picturePath = Utils::GetStringOrNull(value[Queries::SelectNFriends::PICTURE_PATH]);
    mPicturePath = picturePath ? picturePath : "";
    free(picturePath);
    mMutualFriendsCount = atoi(value[Queries::SelectNFriends::MUTUAL_FRIENDS]);
    char* firstName = Utils::GetStringOrNull(value[Queries::SelectNFriends::FIRST_NAME]);
    mFirstName = firstName ? firstName : "";
    free(firstName);
    char* lastName = Utils::GetStringOrNull(value[Queries::SelectNFriends::LAST_NAME]);
    mLastName = lastName ? lastName : "";
    free(lastName);
    char* birthDay = Utils::GetStringOrNull(value[Queries::SelectNFriends::BIRTHDAY]);
    mBirthday = birthDay ? birthDay : "";
    free(birthDay);
    char* email = Utils::GetStringOrNull(value[Queries::SelectNFriends::EMAIL]);
    mEmail = email ? email : "";
    free(email);
    char* phoneNumber = Utils::GetStringOrNull(value[Queries::SelectNFriends::MOBILE_PHONE]);
    mPhone = phoneNumber ? phoneNumber : "";
    free(phoneNumber);
}

Friend::Friend() :
    isBdayNextYear(false),
    mMutualFriendsCount(0),
    mDownloadImageReqId(-1) {
    mGraphObjectType = GOT_FRIEND;
}
