#include "Common.h"
#include "ErrorHandling.h"
#include "Friend.h"
#include "i18n.h"
#include "LoggedInUserData.h"
#include "OperationPresenter.h"
#include "PhotoPostOperation.h"
#include "Utils.h"

#include <sstream>

struct OperationPresenter::TagPlace {
    TagPlace(size_t offset, size_t length): mOffset(offset), mLength(length) {}
    size_t mOffset;
    size_t mLength;
};

OperationPresenter::OperationPresenter(Sptr<Operation> oper, Comment* comment) :
    mPrivacyIconMapping({
        {PRIVACY_EVERYONE,                     ICON_FEED_PRIVACY_DIR "/nf_privacy_public.png" },
        {PRIVACY_ALL_FRIENDS,                  ICON_FEED_PRIVACY_DIR "/nf_privacy_friends.png"},
        {PRIVACY_SOME_FRIENDS,                 ICON_FEED_PRIVACY_DIR "/nf_privacy_friends.png"},
        {PRIVACY_FRIENDS_OF_FRIENDS,           ICON_FEED_PRIVACY_DIR "/nf_privacy_friends.png"},
        {PRIVACY_SELF,                         ICON_FEED_PRIVACY_DIR "/nf_privacy_private.png"},
        {PRIVACY_CUSTOM,                       ICON_FEED_PRIVACY_DIR "/nf_privacy_custom.png" },
        {PRIVACY_CUSTOM_MANUAL_DENIED,         ICON_FEED_PRIVACY_DIR "/nf_privacy_custom.png" },
        {PRIVACY_CUSTOM_MANUAL_ALLOWED,        ICON_FEED_PRIVACY_DIR "/nf_privacy_custom.png" },
        {PRIVACY_FRIENDLIST,                   ICON_FEED_PRIVACY_DIR "/nf_privacy_friends.png"},
        {PRIVACY_FRIENDS_EXCEPT_ACQUAINTANCES, ICON_FEED_PRIVACY_DIR "/nf_privacy_friends.png"},
        {PRIVACY_GROUP_OPEN,                   ICON_FEED_PRIVACY_DIR "/nf_privacy_public.png" },
        {PRIVACY_GROUP_CLOSED,                 POST_COMPOSER_ICON_DIR "/privacy_group_small.png"},
        {PRIVACY_GROUP_SECRET,                 POST_COMPOSER_ICON_DIR "/privacy_group_small.png"}
    }),
    mOperation(oper) {
    mGraphObjectType = GOT_OPERATION_PRESENTER;
    mEditedComment = comment;
    if(mEditedComment) {
        mEditedComment->AddRef();
    }
}

OperationPresenter::~OperationPresenter() {
    if(mEditedComment) {
        mEditedComment->Release();
    }
}

bool OperationPresenter::IsInteractive() {
    return false;
}

//From
std::string OperationPresenter::GetFromId(){
    return LoggedInUserData::GetInstance().mId ? LoggedInUserData::GetInstance().mId : "me";
}

std::string OperationPresenter::GetFromName() {
    return LoggedInUserData::GetInstance().mName ? LoggedInUserData::GetInstance().mName : i18n::Localize("IDS_POST_PREVIEW_YOUR_NAME");
}

std::string OperationPresenter::GetFromPicture() {
    return LoggedInUserData::GetInstance().mPicture ? (LoggedInUserData::GetInstance().mPicture->mUrl ? LoggedInUserData::GetInstance().mPicture->mUrl : "") : "";
}
//Via
std::string OperationPresenter::GetViaId() {
    return "";
}

std::string OperationPresenter::GetViaName() {
    return "";
}
//With
std::string OperationPresenter::GetWithTagsId() {
    return "";
}

std::string OperationPresenter::GetWithTagsName() {
    return "";
}
unsigned int OperationPresenter::GetWithTagsCount() {
    return 0;
}
//Location
std::string OperationPresenter::GetLocationId(){
    return "";
}

std::string OperationPresenter::GetLocationName() {
    CHECK_RET(mOperation, "");
    char* locationName = nullptr;
    bundle* operationBundle = mOperation->GetBundle();
    CHECK_RET(operationBundle, "");
    bundle_get_str(operationBundle, "place_name", &locationName);
    return locationName ? locationName : "";
}

const char* OperationPresenter::GetAlbumName() {
    CHECK_RET(mOperation,"");
    char* albumName = nullptr;
    bundle* operationBundle = mOperation->GetBundle();
    CHECK_RET(operationBundle, "");
    bundle_get_str(operationBundle, "album_name", &albumName);
    return albumName;
}

std::string OperationPresenter::GetLocationType() {
    return "";
}

unsigned int OperationPresenter::GetCheckinsNumber() {
    return 0;
}

std::string OperationPresenter::GetImplicitLocationName() {
    return "";
}

//Content
std::string OperationPresenter::GetName() {
    return "";
}

std::string OperationPresenter::GetStory() {
    CHECK_RET(mOperation, "");
    std::stringstream storyStream;
    std::stringstream albumSubstring;
    storyStream << GetFromName();
    switch(mOperation->GetType()){
        case Operation::OT_Post_Create:
            break;
        case Operation::OT_Album_Add_New_Photos:
        {
            const char *albumName = GetAlbumName();
            albumSubstring << " " << i18n::Localize("IDS_TO_THE_ALBUM") << " " << (albumName ? albumName : "");
        }
        //there is no break here - fall-through!
        case Operation::OT_Photo_Post_Create:
        {
            Sptr<PhotoPostOperation> photoOperation(dynamic_cast<PhotoPostOperation*>(mOperation.Get()));
            if(!photoOperation) break;
            Eina_List* photos = photoOperation->GetPhotos();
            if(!photos) break;
            unsigned int photoCount(eina_list_count(photos));
            if(!photoCount) break;
            storyStream << " " << (photoCount == 1 ? i18n::Localize("IDS_POST_PREVIEW_ADDED_SINGLE_PHOTO") : i18n::Localize("IDS_POST_PREVIEW_ADDED_MULTIPLE_PHOTOS", photoCount)) << albumSubstring.str();
            break;
        }
        case Operation::OT_Video_Post_Create: {
            storyStream << " " << i18n::Localize("IDS_POST_PREVIEW_ADDED_SINGLE_VIDEO");
            break;
        default:
            break;
        }
    }

    std::list<Friend> taggedFriends(std::move(mOperation->GetTaggedFriends()));
    bool areFriendsAdded = false;
    if(taggedFriends.size() > 0) {
        areFriendsAdded = true;
        storyStream << i18n::Localize("IDS_POST_PREVIEW_TAGGED_FRIEND", taggedFriends.begin()->GetName());
        if(taggedFriends.size() >= 2) {
            storyStream << " " << i18n::Localize("IDS_POST_PREVIEW_TAGGED_FRIENDS_AND")
                        << " " << (taggedFriends.size() == 2 ? (++taggedFriends.begin())->GetName() :
                                                        i18n::Localize("IDS_POST_PREVIEW_TAGGED_FRIENDS_OTHERS", taggedFriends.size() - 1));
        }
    }

    std::string locationName(GetLocationName());
    if(!locationName.empty()) {
        storyStream << " " << i18n::Localize(areFriendsAdded ? "IDS_POST_PREVIEW_ADDED_LOCATION" : "IDS_POST_PREVIEW_ADDED_LOCATION_ONLY", locationName);
    }
    storyStream << ".";
    return storyStream.str();
}

std::list<Tag> OperationPresenter::GetStoryTagsList() {
    std::list<Friend> friends(std::move(mOperation->GetTaggedFriends()));
    std::list<Tag> tags;
    std::string message(std::move(GetStory()));
    auto hiddenTags(0u);
    for(auto friendIterator = friends.begin(); friendIterator != friends.end(); friendIterator++) {
        auto tagPlace(GetTagPlace(message, friendIterator->mName));
        if(tagPlace.mOffset == std::string::npos) {
            hiddenTags++;
            continue;
        }
        tags.push_back(Tag(*friendIterator, tagPlace.mOffset, tagPlace.mLength));
    }
    // adding others
    if(hiddenTags) {
        std::string others(i18n::Localize("IDS_POST_PREVIEW_TAGGED_FRIENDS_OTHERS", hiddenTags));
        auto tagPlace(GetTagPlace(message, others));
        tags.push_back(Tag("", others, "user", tagPlace.mOffset, tagPlace.mLength));
    }
    // adding place
    std::string locationName(std::move(GetLocationName()));
    if(!locationName.empty()) {
        auto tagPlace(GetTagPlace(message, locationName));
        tags.push_back(Tag("", locationName, "page", tagPlace.mOffset, tagPlace.mLength));
    }
    // adding self
    auto fromName(GetFromName());
    auto selfTagPlace(GetTagPlace(message, fromName));
    tags.push_back(Tag(GetFromId(), fromName, "user", selfTagPlace.mOffset, selfTagPlace.mLength));
    return std::move(tags);
}

const char *OperationPresenter::GetMessage() {
    assert(mOperation);
    return mOperation->GetMessage();
}

std::list<Tag> OperationPresenter::GetMessageTagsList() {
    return {};
}

std::string OperationPresenter::GetCaption() {
    return "";
}

unsigned int OperationPresenter::GetLikesCount() {
    return 0;
}

unsigned int OperationPresenter::GetCommentsCount() {
    return 0;
}

std::string OperationPresenter::GetApplicationName() {
    return "";
}

double OperationPresenter::GetCreationTime() {
    return Utils::GetCurrentTime();
}

std::list<Friend> OperationPresenter::GetPostFriendsList() {
    return {};
}

bool OperationPresenter::IsEdited() {
    return false;
}

std::string OperationPresenter::GetPrivacyIcon() {
    CHECK_RET(mOperation, "");
    auto iconIterator = mPrivacyIconMapping.find(mOperation->GetPrivacyType());
    return iconIterator != mPrivacyIconMapping.end() ? iconIterator->second : "";
}
//Attachment
std::string OperationPresenter::GetAttachmentTitle() {
    return "";
}
std::string OperationPresenter::GetAttachmentTargetUrl() {
    return "";
}
std::string OperationPresenter::GetAttachmentDescription() {
    return "";
}
std::list<Tag> OperationPresenter::GetSubattachmentList() {
    return {};
}

OperationPresenter::TagPlace OperationPresenter::GetTagPlace(const std::string& message, const std::string& tagName) {
    i18n::u16string uMessage(i18n::StdStringToU16String(message));
    i18n::u16string uTagName(i18n::StdStringToU16String(tagName));
    return TagPlace(uMessage.find(uTagName), uTagName.length());
}

bool OperationPresenter::IsPhotoPost(){
    return (mOperation->GetType() == Operation::OT_Album_Add_New_Photos);
}
