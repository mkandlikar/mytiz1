#include "FriendRequest.h"
#include "jsonutilities.h"

/**
 * @brief Construct FriendRequest object from JsonObject
 * @param object - Json Object, from which FriendRequest will be constructed
 */
FriendRequest::FriendRequest(JsonObject * object)
{
    mFrom = nullptr;
    JsonObject * from_object = json_object_get_object_member(object, "from");
    if (from_object) {
        mFrom = new User(from_object);
    }

    mTo = nullptr;
    JsonObject * to_object = json_object_get_object_member(object, "to");
    if (to_object) {
        mTo = new User(to_object);
    }

    mCreatedTime = GetStringFromJson(object, "created_time");

    mMessage = GetStringFromJson(object, "message");

    mUnread = json_object_get_boolean_member(object, "unread");
}

/**
 * @brief Destructor
 */
FriendRequest::~FriendRequest()
{
    if (mFrom) {
        mFrom->Release();
    }
    if (mTo) {
        mTo->Release();
    }
    free((void*) mMessage);
    free((void*) mCreatedTime);
}

