#include "jsonutilities.h"
#include "Log.h"
#include "NotificationObject.h"
#include "Utils.h"

#include <sstream>

NotificationObject::NotificationObject(JsonObject *object)
{
    Init();

    SetId(GetStringFromJson(object, "id"));

    JsonObject *application = json_object_get_object_member(object, "application");

    if (application) {
        mApplicationCategory = GetStringFromJson(application, "category");
        mApplicationLink = GetStringFromJson(application, "link");
        mApplicationName = GetStringFromJson(application, "name");
        mApplicationId = GetStringFromJson(application, "id");
    }

    if (mApplicationName) {
        if (!strcmp(mApplicationName, "Groups")) {
            mNotificationType = NOTIF_GROUP;
        } else if (!strcmp(mApplicationName, "Friends")) {
            mNotificationType = NOTIF_FRIEND;
        } else if (!strcmp(mApplicationName, "Videos")) {
            mNotificationType = NOTIF_VIDEO;
        } else if (strstr(mApplicationName, "Feed Comments")) {  //strcmp() fails in RTL language
            mNotificationType = NOTIF_FEED_COMMENTS;
        } else if (!strcmp(mApplicationName, "Wall")) {
            mNotificationType = NOTIF_WALL;
        } else if (!strcmp(mApplicationName, "Likes")) {
            mNotificationType = NOTIF_LIKES;
        } else if (!strcmp(mApplicationName, "Gifts")) {
            mNotificationType = NOTIF_GIFTS;
        } else if (!strcmp(mApplicationName, "Photos")) {
            mNotificationType = NOTIF_PHOTOS;
        } else if (!strcmp(mApplicationName, "Facebook for Tizen")) {
            mNotificationType = NOTIF_FB4T;
        } else if (!strcmp(mApplicationName, "Links")) {
            mNotificationType = NOTIF_LINKS;
        } else if (!strcmp(mApplicationName, "Pages")) {
            mNotificationType = NOTIF_PAGES;
        } else {
            mNotificationType = NOTIF_UNKNOWN;
            Log::debug(LOG_FACEBOOK_NOTIFICATION, "NotificationObject: Unknown notification type");
        }
    }

    if ((mNotificationType == NOTIF_NONE || mNotificationType == NOTIF_UNKNOWN) && mApplicationLink) {
        if (!strcmp(mApplicationLink, "/?sk=ff")) {
            mNotificationType = NOTIF_FRIEND;
        } else if (!strcmp(mApplicationLink, "/videos/")) {
            mNotificationType = NOTIF_VIDEO;
        } else if (!strcmp(mApplicationLink, "/gifts")) {
            mNotificationType = NOTIF_GIFTS;
        } else if (strstr(mApplicationLink, "sk=photos")) {
            mNotificationType = NOTIF_PHOTOS;
        }
    }
    Log::debug("NotificationObject::constructor, mNotificationType=%d", mNotificationType);

    mCreatedTime = GetStringFromJson(object, "created_time");

    JsonObject *from = json_object_get_object_member(object, "from");
    if (from) {
        mFromName = GetStringFromJson(from, "name");
        mFromId = GetStringFromJson(from, "id");
        JsonObject *pictureObject = json_object_get_object_member(from, "picture");
        if (pictureObject) {
            JsonObject * data_object = json_object_get_object_member(pictureObject, "data");
            if (data_object) {
                mFromPictureUrl = GetStringFromJson(data_object, "url");
            }
        }
    }

    mLink = GetStringFromJson(object, "link");

    JsonObject *objectNotif = json_object_get_object_member(object, "object");

    if (objectNotif) {
        mObjectCreatedTime = GetStringFromJson(objectNotif, "created_time");
        mObjectStory = GetStringFromJson(objectNotif, "story");
        mObjectId = GetStringFromJson(objectNotif, "id");
        mObjectName = GetStringFromJson(objectNotif, "name");
    }

    mTitle = GetStringFromJson(object, "title");

    if (mTitle) {
        mTaggedTitle = mTitle;
        std::stringstream str;
        if (mFromName) {
            str << "<b>" << mFromName << "</b>";
            mTaggedTitle = Utils::ReplaceString(mTaggedTitle, mFromName, str.str(), 0, TRUE);
        }

        if (mObjectName) {
            str.str("");
            str << "<b>" << mObjectName << "</b>";
            mTaggedTitle = Utils::ReplaceString(mTaggedTitle, mObjectName, str.str());
        }
    }

    mUnread = json_object_get_boolean_member(object, "unread");

    mUpdatedTime = GetStringFromJson(object, "updated_time");
    if (mUpdatedTime) {
        bool needTimeShift = Utils::IsTimeShiftNeeded(mUpdatedTime);
        mUpdatedTimeInSec = Utils::ConvertTimeToGMT(mUpdatedTime, needTimeShift) / 1000;

        mUpdatedTimeString = Utils::CreateTimeWithShift(mUpdatedTime, true);
    }

    SetGraphObjectType(GraphObject::GOT_NOTIFICATION);
}

NotificationObject::NotificationObject(const NotificationObject &other)
{
    SetId(SAFE_STRDUP(other.GetId()));

    mApplicationCategory = SAFE_STRDUP(other.mApplicationCategory);
    mApplicationLink = SAFE_STRDUP(other.mApplicationLink);
    mApplicationName = SAFE_STRDUP(other.mApplicationName);
    mApplicationId = SAFE_STRDUP(other.mApplicationId);

    mNotificationType = other.mNotificationType;

    mCreatedTime = SAFE_STRDUP(other.mCreatedTime);

    mFromName = SAFE_STRDUP(other.mFromName);
    mFromId = SAFE_STRDUP(other.mFromId);
    mFromPictureUrl = SAFE_STRDUP(other.mFromPictureUrl);

    mLink = SAFE_STRDUP(other.mLink);

    mObjectCreatedTime = SAFE_STRDUP(other.mObjectCreatedTime);
    mObjectStory = SAFE_STRDUP(other.mObjectStory);
    mObjectId = SAFE_STRDUP(other.mObjectId);
    mObjectName = SAFE_STRDUP(other.mObjectName);

    mTitle = SAFE_STRDUP(other.mTitle);

    mUnread = other.mUnread;

    mUpdatedTime = SAFE_STRDUP(other.mUpdatedTime);

    mUpdatedTimeString = SAFE_STRDUP(other.mUpdatedTimeString);
    mUpdatedTimeInSec = other.mUpdatedTimeInSec;

    mGraphObjectType = other.mGraphObjectType;
}

void NotificationObject::Init()
{
    mApplicationCategory = nullptr;
    mApplicationLink = nullptr;
    mApplicationName = nullptr;
    mApplicationId = nullptr;

    mNotificationType = NOTIF_NONE;

    mCreatedTime = nullptr;

    mFromName = nullptr;
    mFromId = nullptr;
    mFromPictureUrl = nullptr;

    mLink = nullptr;

    mObjectCreatedTime = nullptr;
    mObjectStory = nullptr;
    mObjectId = nullptr;
    mObjectName = nullptr;

    mTitle = nullptr;

    mUnread = false;

    mUpdatedTime = nullptr;
    mUpdatedTimeString = nullptr;
    mUpdatedTimeInSec = 0;
}

NotificationObject::~NotificationObject()
{
    free((void *) mApplicationCategory);
    free((void *) mApplicationLink);
    free((void *) mApplicationName);
    free((void *) mApplicationId);

    free(mCreatedTime);

    free((void *) mFromName);
    free((void *) mFromId);
    free((void *) mFromPictureUrl);

    free((void *) mLink);

    free((void *) mObjectCreatedTime);
    free((void *) mObjectStory);
    free((void *) mObjectId);
    free((void *) mObjectName);

    free((void *) mTitle);

    free(mUpdatedTime);

    free((void *) mUpdatedTimeString);
}
