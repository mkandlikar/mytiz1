#include <assert.h>

#include "Common.h"
#include "CSmartPtr.h"
#include "BriefPostInfo.h"
#include "jsonutilities.h"
#include "Log.h"
#include "Utils.h"

BriefPostInfo::BriefPostInfo( JsonObject *object ) : GraphObject( GOT_UNKNOWN )
{
    m_CommentsCount = m_LikesCount = 0;
    m_HasLiked = false;
    m_UpdatedTime = 0.0;
    m_LikeFromId = nullptr;
    m_LikeFromName = nullptr;

    SetId( GetStringFromJson( object, "id" ) );

    c_unique_ptr<char> updatedTime(GetStringFromJson(object, "updated_time"));
    if (updatedTime) {
        m_UpdatedTime = Utils::ParseTimeToMilliseconds(updatedTime.get());
    }
    JsonObject *likes = json_object_get_object_member(object, "likes");
    if (likes) {
        JsonObject * summary = json_object_get_object_member(likes, "summary");
        if (summary) {
            m_LikesCount = json_object_get_int_member(summary, "total_count");
            m_HasLiked = json_object_get_boolean_member(summary, "has_liked");
        }
        JsonArray *likesData = json_object_get_array_member(likes, "data");
        if (likesData) {
            for (int i = 0; i < 2; ++i) {
                JsonObject *person = json_array_get_object_element(likesData, i);
                if (person) {
                    c_unique_ptr<char> id(GetStringFromJson(person, "id"));
                    if (id && !Utils::IsMe(id.get())) {
                        m_LikeFromId = SAFE_STRDUP(id.get());
                        m_LikeFromName = GetStringFromJson(person, "name");
                        break;
                    }
                }
            }
        }
    }
    JsonObject *comments = json_object_get_object_member(object, "comments");
    if (comments) {
        comments = json_object_get_object_member(comments, "summary");
        if (comments) {
            m_CommentsCount = json_object_get_int_member(comments, "total_count");
        }
    }
}

BriefPostInfo::~BriefPostInfo()
{
    free((void *) m_LikeFromId);
    free((void *) m_LikeFromName);
}

unsigned int BriefPostInfo::GetCommentsCount() const
{
    return m_CommentsCount;
}

unsigned int BriefPostInfo::GetLikesCount() const
{
    return m_LikesCount;
}

double BriefPostInfo::GetUpdatedTime() const
{
    return m_UpdatedTime;
}

bool BriefPostInfo::GetHasLiked() const
{
    return m_HasLiked;
}

const char * BriefPostInfo::GetLikeFromId() const
{
    return m_LikeFromId;
}

const char * BriefPostInfo::GetLikeFromName() const
{
    return m_LikeFromName;
}
