/*
 * StoryAttachment.cpp
 *
 *  Created on: May 24, 2015
 *      Author: RUINMKOL
 */

#include <stdlib.h>
#include <string.h>

#include "Common.h"
#include "jsonutilities.h"
#include "StoryAttachment.h"

ImageSource::ImageSource(JsonObject * object) : Photo()
{
    mHeight = json_object_get_int_member(object, "height");
    mWidth = json_object_get_int_member(object, "width");
    SetSrcUrl(json_object_get_string_member(object, "src"));
}

ImageSource::ImageSource(const ImageSource & other) : Photo(other)
{
    mHeight = other.mHeight;
    mWidth = other.mWidth;
}

ImageSource::~ImageSource() {}

StoryAttachmentTarget::StoryAttachmentTarget(JsonObject * object)
{
    mId = GetStringFromJson(object, "id");
    mUrl = GetStringFromJson(object, "url");
}

StoryAttachmentTarget::StoryAttachmentTarget(const StoryAttachmentTarget & other)
{
    mId = SAFE_STRDUP(other.mId);
    mUrl = SAFE_STRDUP(other.mUrl);
}

StoryAttachmentTarget::~StoryAttachmentTarget()
{
    free((void *) mId);
    free((void *) mUrl);
}

StoryAttachmentMedia::StoryAttachmentMedia(JsonObject * object)
{
    JsonObject * image = json_object_get_object_member(object, "image");
    mImage = image != NULL ? new ImageSource(image): NULL;
}

StoryAttachmentMedia::StoryAttachmentMedia(const StoryAttachmentMedia & other)
{
    mImage = other.mImage ? new ImageSource(*other.mImage) : NULL;
}

StoryAttachmentMedia::~StoryAttachmentMedia()
{
    delete mImage;
}



StoryAttachment::StoryAttachment(JsonObject * object)
{
    mDescription = GetStringFromJson(object, "description");

    JsonObject * media = json_object_get_object_member(object, "media");
    mMedia = media != NULL ? new StoryAttachmentMedia(media): NULL;

    JsonObject * target = json_object_get_object_member(object, "target");
    mTarget = target != NULL ? new StoryAttachmentTarget(target): NULL;

    mTitle = GetStringFromJson(object, "title");
    mType = GetStringFromJson(object, "type");
    mUrl = GetStringFromJson(object, "url");

}

StoryAttachment::StoryAttachment(const StoryAttachment & other)
{
    mDescription = SAFE_STRDUP(other.mDescription);
    mMedia = other.mMedia ? new StoryAttachmentMedia(*other.mMedia) : NULL;
    mTarget = other.mTarget ? new StoryAttachmentTarget(*other.mTarget) : NULL;
    mTitle = SAFE_STRDUP(other.mTitle);
    mType = SAFE_STRDUP(other.mType);
    mUrl = SAFE_STRDUP(other.mUrl);
}

StoryAttachment::~StoryAttachment()
{
    free((void *) mDescription);

    delete mMedia;
    delete mTarget;

    free((void *) mTitle);
    free((void *) mType);
    free((void *) mUrl);
}

