#include "Common.h"
#include "jsonutilities.h"
#include "Log.h"
#include "MutualFriendsList.h"
#include "User.h"

User::User(JsonObject * object):
    Person(object)
{
    char *id = GetStringFromJson(object, "id");
    SetId(id);

    mName = GetStringFromJson(object, "name");
    mFirstName = GetStringFromJson(object, "first_name");
    mLastName = GetStringFromJson(object, "last_name");
    JsonObject * pictureObject = json_object_get_object_member(object, "picture");
    mPicture = pictureObject ? new ProfilePictureSource(pictureObject) : nullptr;

#ifdef FEATURE_MUTUAL_FRIENDS
    mMutualFriendsCount = 0;
    JsonObject * context = json_object_get_object_member(object, "context");
    if (context) {
        JsonObject * mutual_friends = json_object_get_object_member(context, "mutual_friends");
        if (mutual_friends) {
            JsonObject * summary = json_object_get_object_member(mutual_friends, "summary");
            if (summary) {
                mMutualFriendsCount = json_object_get_int_member(summary, "total_count");
            }
        } else {
            mMutualFriendsCount = MutualFriendsList::GetInstance()->GetUsersMutualFriends(id);
        }
    } else {
        mMutualFriendsCount = MutualFriendsList::GetInstance()->GetUsersMutualFriends(id);
    }
#else
    mMutualFriendsCount = MutualFriendsList::GetInstance()->GetUsersMutualFriends(id);
#endif

    mGraphObjectType = GOT_USER;

    mFriendRequestStatus = eUSER_REQUEST_UNKNOWN;
}

User::User(const User & other)
{
    SetId(SAFE_STRDUP(other.GetId()));
    mName = SAFE_STRDUP(other.mName);
    mFirstName = SAFE_STRDUP(other.mFirstName);
    mLastName = SAFE_STRDUP(other.mLastName);
    mPicture = other.mPicture ? new ProfilePictureSource(*other.mPicture) : nullptr;
    mMutualFriendsCount = other.mMutualFriendsCount;
    mGraphObjectType = other.mGraphObjectType;
    mFriendRequestStatus = other.mFriendRequestStatus;
    mFriendshipStatus = other.mFriendshipStatus;
}

User::~User()
{
    free((void *) mName);
    free((void *) mFirstName);
    free((void *) mLastName);

    delete mPicture;
}
