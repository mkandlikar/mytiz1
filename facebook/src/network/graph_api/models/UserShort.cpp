/*
 * UserShort.cpp
 *
 *  Created on: Aug 5, 2015
 *      Author: RUINREKU
 */

#include <stdlib.h>
#include <string.h>
#include "jsonutilities.h"

#include "UserShort.h"

UserShort::UserShort(JsonObject * object)
{
    SetId( GetStringFromJson(object, "id") );
    if(object) {
        const gchar *name = json_object_get_string_member(object, "name");
        SetName(name ? name : "");

        JsonObject *picture = json_object_get_object_member(object, "picture");
        JsonObject *pictureData = json_object_get_object_member(picture, "data");
        const gchar *url = json_object_get_string_member(pictureData, "url");
        SetPictureUrl(url ? url : "");
    }
}

UserShort::~UserShort()
{
    free((void*) mName);
    free((void*) mPictureUrl);
}
