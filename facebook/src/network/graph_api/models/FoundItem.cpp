#include "Common.h"
#include "FoundItem.h"
#include "jsonutilities.h"

#include <malloc.h>

/**
 * @brief Construct FoundItem object from JsonObject
 * @param object - Json Object, from which FoundItem will be constructed
 */
FoundItem::FoundItem(JsonObject * object) :
    Person (object)
{
    SetId(GetStringFromJson(object, "uid"));
    mType = GetStringFromJson(object, "type");
    mPath = GetStringFromJson(object, "path");
    mText = GetStringFromJson(object, "text");
    mCategory = GetStringFromJson(object, "category");
    mSubText = GetStringFromJson(object, "subtext");
    mPhoto = GetStringFromJson(object, "photo");
    mGraphObjectType = GraphObject::GOT_FOUNDITEM;
}

/**
 * @brief Destructor
 */
FoundItem::~FoundItem()
{
    free((void*) mType);
    free((void*) mPath);
    free((void*) mText);
    free((void*) mCategory);
    free((void*) mSubText);
    free((void*) mPhoto);
}

