#include "Common.h"
#include "ErrorHandling.h"
#include "Tag.h"

#include <cassert>

Tag::Tag(): mOffset(0), mLength(0){
}

Tag::Tag(const std::string& id, const std::string& name, const std::string& type, int offset, int length) :
    mId(id), mName(name), mType(type), mOffset(offset), mLength(length) {
}

Tag::Tag(const Tag& other):
    mId(other.mId),
    mName(other.mName),
    mType(other.mType),
    mOffset(other.mOffset),
    mLength(other.mLength) {
}

Tag::Tag(Tag&& other):
    mId(std::move(other.mId)),
    mName(std::move(other.mName)),
    mType(std::move(other.mType)),
    mOffset(other.mOffset),
    mLength(other.mLength) {
    other.mOffset = 0;
    other.mLength = 0;
}

Tag::Tag(Friend& fr, int offset, int length):
    mId(fr.GetId() ? fr.GetId() : ""),
    mName(fr.mName),
    mType("user"),
    mOffset(offset),
    mLength(length) {
}

Tag::~Tag() {
}

void Tag::operator=(const Tag& other) {
    mId = other.mId;
    mName = other.mName;
    mType = other.mType;
    mOffset = other.mOffset;
    mLength = other.mLength;
}

bool Tag::operator<(const Tag& other)
{
  return mOffset < other.mOffset;
}

void Tag::parse_each_json_object_cb(JsonObject *object, const gchar *memberName, JsonNode *memberNode, gpointer userData) {
    assert(object);
    assert(userData);
    CHECK_RET_NRV(memberName);
    tags_json_array_to_eina_list(json_object_get_array_member(object, memberName), static_cast<Eina_List**>(userData));
}

void Tag::tags_json_array_to_eina_list(JsonArray* memberArray, Eina_List** list) {
    assert(list);
    CHECK_RET_NRV(memberArray);
    for (unsigned int i = 0u; i < json_array_get_length(memberArray); i++) {
        JsonObject* memberObject = json_array_get_object_element(memberArray, i);
        if(!memberObject) continue;
        const char *id = json_object_get_string_member(memberObject, "id");
        const char *name = json_object_get_string_member(memberObject, "name");
        const char *type = json_object_get_string_member(memberObject, "type");
        *list = eina_list_append(*list, new Tag(id ? id : "",
                                                name ? name : "",
                                                type ? type : "",
                                                json_object_get_int_member(memberObject, "offset"),
                                                json_object_get_int_member(memberObject, "length")));
    }
}

