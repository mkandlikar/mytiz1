#include "Common.h"
#include "CoverPhoto.h"
#include "jsonutilities.h"

/**
 * @brief Construct CoverPhoto object from JsonObject
 * @param[in] object - Json Object, from which CoverPhoto will be constructed
 */
CoverPhoto::CoverPhoto(JsonObject * object)
{
    SetId( GetStringFromJson(object, "id") );
    mOffsetX = json_object_get_double_member(object, "offset_x");
    mOffsetY = json_object_get_double_member(object, "offset_y");
    mSource = GetStringFromJson(object, "source");
    mPhotoPath = NULL;
}

CoverPhoto::CoverPhoto(const CoverPhoto & other)
{
    SetId(SAFE_STRDUP(other.GetId()));
    mOffsetX = other.mOffsetX;
    mOffsetY = other.mOffsetY;
    mSource = SAFE_STRDUP(other.mSource);
    mPhotoPath = SAFE_STRDUP(other.mPhotoPath);
}

/**
 * @brief Destruction
 */
CoverPhoto::~CoverPhoto()
{
    free((void*) mSource);
    free((void*) mPhotoPath);
}
