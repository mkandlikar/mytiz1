#include "Separator.h"

Separator::Separator( SeparatorType type, const char *custom_text) : GraphObject( GraphObject::GOT_SEPARATOR )
{
    m_SeparatorType = type;
    mCustomText = nullptr;
    if (custom_text) {
        mCustomText = SAFE_STRDUP(custom_text);
    }
}

Separator::~Separator()
{
   free(mCustomText);
}

Separator::SeparatorType Separator::GetSeparatorType() const
{
    return m_SeparatorType;
}
