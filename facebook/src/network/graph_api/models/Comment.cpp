#include <app.h>
#include <efl_extension.h>
#include <string>
#include <string.h>

#include "Common.h"
#include "Config.h"
#include "Comment.h"
#include "GraphObject.h"
#include "jsonutilities.h"
#include "Log.h"
#include "Post.h"
#include "Queries.h"
#include "StoryAttachment.h"
#include "Utils.h"
#include "CSmartPtr.h"

/**
* @brief Construction
* @param object - json object with data
*/
Comment::Comment(JsonObject * object)
{
    mGraphObjectType = GraphObject::GOT_COMMENT;
    JsonObject *attach = json_object_get_object_member(object, "attachment");
    mAttachment = attach ? new StoryAttachment(attach) : nullptr;

    SetId( GetStringFromJson(object, "id") );
    mCanComment = json_object_get_boolean_member(object, "can_comment");
    mCanRemove = json_object_get_boolean_member(object, "can_remove");
    mCanHide = json_object_get_boolean_member(object, "can_hide");
    mCanLike = json_object_get_boolean_member(object, "can_like");
    mCommentCount = json_object_get_int_member(object, "comment_count");
    c_unique_ptr<char> createdTime(GetStringFromJson(object, "created_time"));
    mCreatedTime = Utils::ParseTimeToMilliseconds(createdTime.get());

    JsonObject *user = json_object_get_object_member(object, "from");
    mFrom = user ? new User(user) : nullptr;

    mLikeCount = json_object_get_int_member(object, "like_count");
    mMessage = GetStringFromJson(object, "message");

    JsonArray* memberArray = json_object_get_array_member(object, "message_tags");
    if(memberArray) {
        int len = json_array_get_length(memberArray);
        for (int i = 0; i < len; i++) {
            JsonObject* memberObject = json_array_get_object_element(memberArray, i);
            if(memberObject) {
                const char* id = json_object_get_string_member(memberObject, "id");
                const char* name = json_object_get_string_member(memberObject, "name");
                const char* type = json_object_get_string_member(memberObject, "type");
                mMessageTagsList.push_back(Tag(id ? id : "",
                                               name ? name : "",
                                               type ? type : "",
                                               json_object_get_int_member(memberObject, "offset"),
                                               json_object_get_int_member(memberObject, "length")));
            }
        }
    }

    mUserLikes = json_object_get_boolean_member(object, "user_likes");

    mCommentsList = NULL;
    JsonObject * comments = json_object_get_object_member(object, "comments");
    if (comments)
    {
        JsonArray * comments_array = json_object_get_array_member(comments, "data");
        if (comments_array)
        {
            int len = json_array_get_length(comments_array);

            for (int i = 0; i < len; i++)
            {
                JsonObject* el = json_array_get_object_element(comments_array, i);
                Comment* commentEl = new Comment(el);
                mCommentsList = eina_list_append(mCommentsList, commentEl);
            }
        }
    }
}

Comment::Comment(const Comment & other)
{
    SetId(SAFE_STRDUP(other.GetId()));
    mGraphObjectType = GraphObject::GOT_COMMENT;

    mAttachment = NULL;
    mFrom = NULL;
    mMessage = NULL;
    mCommentsList = NULL;

    copy(other);
}

/**
 * @brief destruction
 */
Comment::~Comment()
{
    free ((void*) mMessage);
    delete mFrom;
    delete mAttachment;

    FreeCommentsList();

    FreeMessageTagsList();
}

void Comment::copy(const Comment & other) {
    delete mAttachment;
    mAttachment = other.mAttachment ? new StoryAttachment(*other.mAttachment) : NULL;

    mCanComment = other.mCanComment;
    mCanRemove = other.mCanRemove;
    mCanHide = other.mCanHide;
    mCanLike = other.mCanLike;
    mCommentCount = other.mCommentCount;
    mCreatedTime = other.mCreatedTime;

    delete mFrom;
    mFrom = other.mFrom ? new User(*other.mFrom) : NULL;

    mLikeCount = other.mLikeCount;

    free((void*)mMessage);
    mMessage = SAFE_STRDUP(other.mMessage);

    mUserLikes = other.mUserLikes;

    FreeCommentsList();
    if (other.mCommentsList) {
        Eina_List * listIndex;
        void * listData;
        EINA_LIST_FOREACH(other.mCommentsList, listIndex, listData) {
            Comment * comment = static_cast<Comment *>(listData);
            mCommentsList = eina_list_append(mCommentsList, new Comment(*comment));
        }
    }

    mMessageTagsList = other.mMessageTagsList;
}

void Comment::SetHasLikedTrue()
{
    if (mUserLikes == false) {
        mUserLikes = true;
        mLikeCount = mLikeCount + 1;
    }
}

void Comment::SetHasLikedFalse()
{
    if (mUserLikes == true) {
        mUserLikes = false;
        mLikeCount = mLikeCount - 1;
    }
}

std::list<Tag> Comment::GetMessageTagsList() {
    return mMessageTagsList;
}

void Comment::FreeCommentsList()
{
    if (mCommentsList) {
        void * listData;
        EINA_LIST_FREE(mCommentsList, listData) {
            delete static_cast<Comment *>(listData);
        }
    }
    mCommentsList = NULL;
}

void Comment::FreeMessageTagsList() {
    mMessageTagsList.clear();
}

void Comment::SetMessage(const char *message)
{
    free((void *)mMessage);
    mMessage = NULL;

    if(message) {
        mMessage = SAFE_STRDUP(message);
    }
}

void Comment::SetAttachement(const char *attachement) {
    if (mAttachment->mTarget->mId){
        free((void *)mAttachment->mTarget->mId);
        mAttachment->mTarget->mId = NULL;

        if(attachement)
        {
            mAttachment->mTarget->mId = SAFE_STRDUP(attachement);
        }
    }
}

void Comment::SetStringForDataType(const char *str, int dataType, int index) {
    if (dataType == EPhotoPath) {
        mAttachment->mMedia->mImage->SetFilePath(str);
    }
}
