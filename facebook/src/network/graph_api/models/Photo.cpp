#include "Application.h"
#include "Common.h"
#include "CSmartPtr.h"
#include "Config.h"
#include "Event.h"
#include "jsonutilities.h"
#include "Log.h"
#include "Photo.h"
#include "Summary.h"
#include "User.h"

#include <cassert>


#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "Photo"


/*
 * Class ImageFileDownloader
 */

ImageFileDownloader::ImageFileDownloader(ImageFile &image) : mImage(image) {
    mImage.AddRef();
    Register();
}

ImageFileDownloader::~ImageFileDownloader()
{
    Unregister();
    mImage.Release();
}
/*
 * Class PhotoTag
 */
PhotoTag::PhotoTag(JsonObject * object) : mIsPhotoTag(true) {
    const char* createdTime = json_object_get_string_member(object, "created_time");
    mCreatedTime = createdTime ? createdTime : "";
    const char* taggedUserId = json_object_get_string_member(object, "id");
    mTaggedUserId = taggedUserId ? taggedUserId : "";
    const char*  taggedUserName = json_object_get_string_member(object, "name");
    mTaggedUserName = taggedUserName ? taggedUserName : "";
    JsonObject *TagAuthor = json_object_get_object_member(object, "tagging_user");
    if (TagAuthor) {
        const char* tagAuthorName = json_object_get_string_member(TagAuthor, "name");
        mTagAuthorName = tagAuthorName ? tagAuthorName : "";
        const char* tagAuthorId = json_object_get_string_member(TagAuthor, "id");
        mTagAuthorId = tagAuthorId ? tagAuthorId : "";
    }
    if (json_object_has_member(object, "x")) {
        mX = json_object_get_double_member(object, "x");
    } else {
        mIsPhotoTag = false;
    }
    if (json_object_has_member(object, "y")) {
        mY = json_object_get_double_member(object, "y");
    } else {
        mIsPhotoTag = false;
    }
}

PhotoTag::PhotoTag(double x, double y) : mIsPhotoTag(true), mX(x), mY(y) {}

PhotoTag::~PhotoTag() {
}
/*
 * Class ImageFile
 */
ImageFile::ImageFile(const char *url, const char *path, unsigned int height, unsigned int width) :
        mHeight(height), mWidth(width), mRequestID(0), mDownloaders(nullptr)
{
    mUrl = SAFE_STRDUP(url);
    mPath = SAFE_STRDUP(path);
}

ImageFile::~ImageFile() {
    free((void *) mUrl);
    free((void *) mPath);
    StopDownload();
    mDownloaders = eina_list_free(mDownloaders);
}

ImageFile::Status ImageFile::GetStatus() {
    if (IsImageDownloaded()) {
        return EDownloaded;
    } else if (mRequestID) {
        return EDownloading;
    } else {
        return ENotDownloaded;
    }
}

void ImageFile::SetUrl(const char* url) {
    free((char*) mUrl);
    mUrl = SAFE_STRDUP(url);
}

void ImageFile::SetPath(const char* path)
{
    free((char*) mPath);
    mPath = SAFE_STRDUP(path);
}

bool ImageFile::IsImageDownloaded() {
    bool res = false;
    //we assume that file is already downloaded if its path is set.
    if (mPath) {
        res = true;
    } else if (mUrl) {
        const char *path = Utils::GetImagePathFromUrl(mUrl);
        if (path) {
            FILE *fp = fopen(path, "r");
            if (fp) {
                fclose(fp);
                SetPath(path);
                res = true;
            }
            free((void *) path);
        }
    }
    return res;
}

void ImageFile::Download() {
    if (!mRequestID && !IsImageDownloaded() && mUrl) {
        Application::GetInstance()->mDataService->DownloadImage(this, mUrl, mRequestID);
    }
}

void ImageFile::StopDownload()
{
    if (mRequestID) {
        Cancel(mRequestID);
        mRequestID = 0;
        DownloadComplete(false);
    }
}

void ImageFile::AddDownloader(ImageFileDownloader *downloader) {
    assert(downloader);
    mDownloaders = eina_list_append(mDownloaders, downloader);
}

void ImageFile::RemoveDownloader(ImageFileDownloader *downloader) {
    assert(downloader);
    mDownloaders = eina_list_remove(mDownloaders, downloader);
    if (eina_list_count(mDownloaders) == 0) {
        StopDownload();
    }
}

void ImageFile::DownloadComplete(bool result) {
    Eina_List *list;
    Eina_List *next;
    void *data;
    EINA_LIST_FOREACH_SAFE (mDownloaders, list, next, data) {
        ImageFileDownloader *downloader = static_cast<ImageFileDownloader *>(data);
        assert(downloader);
        downloader->ImageDownloaded(result);
    }
}

void ImageFile::HandleDownloadImageResponse(ErrorCode error, MessageId requestID, const char* url, const char* fileName)
{
    if (error == EBPErrNone) {
        assert(requestID == mRequestID);
        assert(!strcmp(url, mUrl));
        SetPath(fileName);
    }
    mRequestID = 0;
    DownloadComplete(error == EBPErrNone);
}


/**
 * @brief Construct Photo object from JsonObject
 * @param object - Json Object, from which Photo will be constructed
 */
Photo::Photo(JsonObject * object)
{
    Init();
    SetId( GetStringFromJson(object, "id") );

    mAlbum = nullptr;
    if (object) {
        JsonObject *albumObject = json_object_get_object_member(object, "album");
        if (albumObject) {
            mAlbum = new Album(albumObject);
        }
    }

    c_unique_ptr<char> createdTime(GetStringFromJson(object, "created_time"));
    mCreateTimeInMilliseconds = Utils::ParseTimeToMilliseconds(createdTime.get());

    mFrom = nullptr;
    mFromId = nullptr;
    if (object) {
        JsonObject *from = json_object_get_object_member(object, "from");
        if (from) {
            //TODO: user or page
            mFrom = new User(from);
            mFromId = GetStringFromJson(from, "id");
        }
    }

    mHeight = json_object_get_int_member(object, "height");
    mWidth = json_object_get_int_member(object, "width");

    mIcon = GetStringFromJson(object, "icon");

    mImages = nullptr;
    if (object) {
        JsonArray *imagesArray = json_object_get_array_member(object, "images");
        if (imagesArray) {
            int length = json_array_get_length(imagesArray);
            for (int i = 0; i < length; ++i) {
                JsonObject *imageObj = json_array_get_object_element(imagesArray, i);
                if (imageObj) {
                    c_unique_ptr<char> source(GetStringFromJson(imageObj, "source"));
                    if (source) {
                        ImageFile *image = new ImageFile(source.get(), nullptr, mHeight, mWidth);
                        mImages = eina_list_append(mImages, image);
                    }
                }
            }
        }
    }

    mLink = GetStringFromJson(object, "link");

    mName = GetStringFromJson(object, "name");

    JsonObject *nameTags = json_object_get_object_member(object, "name_tags");
    if (nameTags) {
        json_object_foreach_member(nameTags, Tag::parse_each_json_object_cb, &mPhotoMessageTagsList);
    }

    mPageStoryId = GetStringFromJson(object, "page_story_id");
    mUpdatedTime = GetStringFromJson(object, "updated_time");

    mEvent = nullptr;
    if (object) {
        JsonObject *eventObject = json_object_get_object_member(object, "event");
        if (eventObject) {
            mEvent = new Event(eventObject);
        }
    }

    mPlace = nullptr;
    if (object) {
        JsonObject *placeObject = json_object_get_object_member(object, "place");
        if (placeObject) {
            mPlace = new Place(placeObject);
        }
    }

    mBackdatedTime = GetStringFromJson(object, "backdated_time");
    mBackdatedTimeGranularity = json_object_get_int_member(object, "backdated_time_granularity");

    SetSrcUrl(GetStringFromJson(object, "picture"));

    mLikeFromId = nullptr;
    mLikeFromName = nullptr;

    if (object) {
        JsonObject *likes = json_object_get_object_member(object, "likes");
        if (likes) {
            JsonObject *summary = json_object_get_object_member(likes, "summary");
            if (summary) {
                mLikesCount = json_object_get_int_member(summary, "total_count");
                mCanLike = json_object_get_boolean_member(summary, "can_like");
                mHasLiked = json_object_get_boolean_member(summary, "has_liked");
            }
            JsonArray *likesData = json_object_get_array_member(likes, "data");
            if (likesData) {
                JsonObject *person = json_array_get_object_element(likesData, 0);
                int length = json_array_get_length(likesData);
                if (person) {
                    mLikeFromId = GetStringFromJson(person, "id");
                    mLikeFromName = GetStringFromJson(person, "name");
                }
                if ((length > 1) && mLikeFromId && Config::GetInstance().GetUserId() == mLikeFromId) {
                    person = json_array_get_object_element(likesData, 1);
                    if (person) {
                        mLikeFromId = GetStringFromJson(person, "id");
                        mLikeFromName = GetStringFromJson(person, "name");
                    }
                }
            }
        }
    }

// comments
    mCommentsCount = 0;
    mCanComment = false;
    if (object) {
        JsonObject *comments = json_object_get_object_member(object, "comments");
        if (comments) {
            JsonObject *summary = json_object_get_object_member(comments, "summary");
            if (summary) {
                mCommentsCount = json_object_get_int_member(summary, "total_count");
                mCanComment = json_object_get_boolean_member(summary, "can_comment");
            }
        }
    }

    mPhotoTags = nullptr;
    if (object) {
        JsonObject *tagsObj = json_object_get_object_member(object, "tags");
        if (tagsObj) {
            JsonArray *dataTagArray = json_object_get_array_member(tagsObj, "data");
            if (dataTagArray) {
                int length = json_array_get_length(dataTagArray);
                for (int i = 0; i < length; ++i) {
                    JsonObject *tagObj = json_array_get_object_element(dataTagArray, i);
                    if (tagObj) {
                        mPhotoTags = eina_list_append(mPhotoTags, new PhotoTag(tagObj));
                    }
                }
            }
        }
    }
    mCanTag = json_object_get_boolean_member(object, "can_tag");
}

Photo::Photo() : GraphObject()
{
    Init();
}

Photo::Photo(const Photo & other)
{
    Init();
    mPostImage = other.mPostImage;
    if (mPostImage) {
        mPostImage->AddRef();
    }
    SetId(SAFE_STRDUP(other.GetId()));
    mAlbum = other.mAlbum ? new Album(*other.mAlbum) : nullptr;

    mCreateTimeInMilliseconds = other.mCreateTimeInMilliseconds;

    //TODO: user or page
    mFrom = other.mFrom ? new User(*static_cast<User *>(other.mFrom)) : nullptr;

    mIcon = SAFE_STRDUP(other.mIcon);
    mLink = SAFE_STRDUP(other.mLink);
    SetName(other.mName);
    mPageStoryId = SAFE_STRDUP(other.mPageStoryId);
    mUpdatedTime = SAFE_STRDUP(other.mUpdatedTime);

    mEvent = other.mEvent ? new Event(*other.mEvent) : nullptr;
    mPlace = other.mPlace ? new Place(*other.mPlace) : nullptr;

    mBackdatedTime = SAFE_STRDUP(other.mBackdatedTime);

    mHeight = other.mHeight;
    mWidth = other.mWidth;
    mBackdatedTimeGranularity = other.mBackdatedTimeGranularity;
    mCommentsCount = other.mCommentsCount;
    mCanComment = other.mCanComment;

    if (other.mImages) {
        Eina_List * listIndex;
        void * listData;
        EINA_LIST_FOREACH(other.mImages, listIndex, listData) {
            ImageFile *imageFile = static_cast<ImageFile *>(listData);
            mImages = eina_list_append(mImages, imageFile);
            imageFile->AddRef();
        }
    }

    mLikesCount = other.mLikesCount;
    mCanLike = other.mCanLike;
    mHasLiked = other.mHasLiked;
    mLikeFromId = SAFE_STRDUP(other.mLikeFromId);
    mLikeFromName = SAFE_STRDUP(other.mLikeFromName);
    mFromId = SAFE_STRDUP(other.mFromId);

    Eina_List* list = nullptr;
    void * listData;
    EINA_LIST_FOREACH(other.mPhotoTags, list, listData) {
        PhotoTag * tag = static_cast<PhotoTag*>(listData);
        tag->AddRef();
        mPhotoTags = eina_list_append(mPhotoTags, tag);
    }
    mCanTag = other.mCanTag;
    SetPhotoPost(other.mPhotoPost);
}

/**
 * @brief Destruction
 */
Photo::~Photo()
{
    if (mAlbum) {
        mAlbum->Release();
    }

    if (mFrom) {
        mFrom->Release();
    }

    free((void*) mIcon);
    free((void*) mLink);
    free((void*) mName);
    free((void*) mPageStoryId);
    free((void*) mUpdatedTime);

    if (mEvent) {
        mEvent->Release();
    }
    if (mPlace) {
        mPlace->Release();
    }

    free((void*) mBackdatedTime);

    if (mPostImage) {
        mPostImage->Release();
    }

    void *listData;
    if (mImages) {
        EINA_LIST_FREE(mImages, listData) {
            static_cast<ImageFile*>(listData)->Release();
        }
    }

    if ( mPhotoMessageTagsList ) {
        EINA_LIST_FREE( mPhotoMessageTagsList, listData ) {
            delete static_cast<Tag*>(listData);
        }
    }


    EINA_LIST_FREE(mPhotoTags, listData) {
        PhotoTag * photoTag = static_cast<PhotoTag*>(listData);
        if (photoTag)
            photoTag->Release();
    }

    free((void*) mLikeFromId);
    free((void*) mLikeFromName);
    free((void*) mFromId);
    if (mPhotoPost) {
        mPhotoPost->Release();
    }
}

void Photo::SetPhotoPost(Post *photoPost) {
    if (mPhotoPost) {
        mPhotoPost->Release();
    }
    mPhotoPost = photoPost;
    if (mPhotoPost) {
        mPhotoPost->AddRef();
    }
}

void Photo::Init()
{
    mPostImage = nullptr;
    mGraphObjectType = GraphObject::GOT_PHOTO;
    mAlbum = nullptr;
    mCreateTimeInMilliseconds = 0;
    mFrom = nullptr;
    mHeight = 0;
    mIcon = nullptr;
    mImages = nullptr;
    mLink = nullptr;
    mName = nullptr;
    mPageStoryId = nullptr;
    mUpdatedTime = nullptr;
    mWidth = 0;
    mEvent = nullptr;
    mPlace = nullptr;
    mBackdatedTime = nullptr;
    mBackdatedTimeGranularity = 0;
    mCommentsCount = 0;
    mCanComment = false;
    mLikesCount = 0;
    mCanLike = false;
    mHasLiked = false;
    mLikeFromId = nullptr;
    mLikeFromName = nullptr;
    mFromId = nullptr;
    mPhotoMessageTagsList = nullptr;
    mPhotoTags = nullptr;
    mPhotoPost = nullptr;
}

void Photo::SetName(const char *name)
{
    free((void *) mName);
    mName = SAFE_STRDUP(name);
}

void Photo::UpdatePhotoDetails(Photo *other)
{
    if (this != other) {
        SetName(other->mName);
        SetPhotoMessageTagsList(other->GetPhotoMessageTagsList());

        if (mPhotoPost) {
            mPhotoPost->SetCanLike(other->GetCanLike());
            mPhotoPost->SetHasLiked(other->GetHasLiked());
            mPhotoPost->SetLikesCount(other->GetLikesCount());
            mPhotoPost->SetCommentsCount(other->GetCommentsCount());
            mPhotoPost->SetCanComment(other->GetCanComment());
        } else {
            mCanLike = other->mCanLike;
            mHasLiked = other->mHasLiked;
            mLikesCount = other->mLikesCount;
            mCommentsCount = other->mCommentsCount;
            mCanComment = other->mCanComment;
        }

        SetLikeFromId(SAFE_STRDUP(other->mLikeFromId));
        SetLikeFromName(SAFE_STRDUP(other->mLikeFromName));
        SetPhotoMessageTagsList(other->mPhotoMessageTagsList);
        mWidth = other->mWidth;
        mHeight = other->mHeight;
        const char *newFromId = SAFE_STRDUP(other->mFromId);
        free((void *) mFromId);
        mFromId = newFromId;
        Eina_List* list = nullptr;
        void * listData;
        EINA_LIST_FREE( mPhotoTags, listData ) {
            if (static_cast<PhotoTag*>(listData))
                static_cast<PhotoTag*>(listData)->Release();
        }
        mPhotoTags = nullptr;
        EINA_LIST_FOREACH(other->mPhotoTags, list, listData) {
            PhotoTag * tag = static_cast<PhotoTag*>(listData);
            tag->AddRef();
            mPhotoTags = eina_list_append(mPhotoTags, tag);
        }
        mCanTag = other->mCanTag;
    }
}

void Photo::ToggleLike() {
    if (mPhotoPost) {
        mPhotoPost->ToggleLike();
    } else {
        if ( mHasLiked ) {
            --mLikesCount;
        } else {
            ++mLikesCount;
        }
        mHasLiked = !mHasLiked;
    }
}

void Photo::SetFilePath(const char * filePath) {
    if (!mPostImage) {
        mPostImage = new ImageFile(nullptr, filePath);
    } else {
        mPostImage->SetPath(filePath);
    }
}

void Photo::SetSrcUrl(const char *src) {
    if (!mPostImage) {
        mPostImage = new ImageFile(src, nullptr);
    } else {
        mPostImage->SetUrl(src);
    }
}

//AAA ToDo: It seems that this function doesn't work properly. Let's change the algorithm later.
ImageFile *Photo::GetOptimumResolutionPicture(int width, int height) {
//The following algorithm is used to get the optimum resolution image. Generally nImages list shall contain the image paths for the various resolution
//of the images. Facebook generally has a predefined set of resolutions for an image. The set may include 600*600, 480*480*, 130*130 and a number of
//rectangular resolutions as well for landscape and portrait modes. To get the optimum resolution image we shall select the image using the following
//algorithm. The idea behind the algorithm is to avoid scaling up of images which causes blurring.
// 1. If the image is Landscape (meaning width>height) then select the image with the height nearest (minimum bigger) to the height of the target object. This means that
// the quality of the image shall not be degraded
//2. If the image is portrait (meaning height>width then select the image with the width nearest (minimum bigger) to the width of the target object.
//3. For square shape images select the image with closest width+height

//Also if the nImages list does not exist then just return the usual picture path
    Log::debug("Photo::GetOptimumResolutionPicture W=%d, H=%d", width, height );

    if(!mImages || !eina_list_count(mImages)) { //THe second is just a defensive check to avoid any potential erroneous message received.
        Log::debug("GetOptimumResolutionPicture == GetPicture()" );
        return mPostImage;
    }
//Start the normal processing by Getting the type of the photo
    if(mWidth > mHeight) {
        //Image is Landscape, so try to match the height. we also need to handle the edge cases where in the first image
        //is smaller then the target path and similarly when even the last image is bigger then the target path. This case
        //is extremely rare infact should not arise because FB server returns images which are bigger then the device screens
        //But still is catered to in a simplistic manner. This can be further optimized but there does not seem to be a
        //use case for the same.
        ImageFile * imageFileFirst = static_cast<ImageFile *>(eina_list_nth(mImages, 0));
        if(imageFileFirst->GetHeight() < height) {
            //This is the larget images in the set, return it
            Log::debug("GetOptimumResolutionPicture L == Position 0 W = %d, H = %d",imageFileFirst->GetWidth(),imageFileFirst->GetHeight() );
            return imageFileFirst;
        }
        //Start checking from the next image
        for (int i = 1; i < eina_list_count(mImages); i++) {
            ImageFile * imageFile = static_cast<ImageFile *>(eina_list_nth(mImages, i));
            if(imageFile->GetHeight() < height) {
             //This is the first smaller images in the set, return its previous image then, this would also take care of the
                //scenario wherein the sizes match
                imageFileFirst = static_cast<ImageFile *>(eina_list_nth(mImages, i-1));
                Log::debug("GetOptimumResolutionPicture L == Position %d W = %d, H = %d", i-1,imageFileFirst->GetWidth(),imageFileFirst->GetHeight() );
                return imageFileFirst;
            }

        }
        //This section would only be hit if the target size is still smaller then even the smallest image, in this case just
        //return the last image in the set.
        imageFileFirst = static_cast<ImageFile *>(eina_list_nth(mImages, eina_list_count(mImages)-1));
        Log::debug("GetOptimumResolutionPicture L == Position Last W = %d, H = %d",imageFileFirst->GetWidth(),imageFileFirst->GetHeight() );
        return imageFileFirst;
    } else {
        //Image is Portrait, so try to match the width instead, rest of the logic/comments are similar to as above. This would also
        //take care of the scenario when we are talking about square picture. Currently there is not a use case to have some
        //some special handling of the same
        ImageFile * imageFileFirst = static_cast<ImageFile *>(eina_list_nth(mImages, 0));
        if(imageFileFirst->GetWidth() < width) {
            //This is the larget images in the set, return it
            Log::debug("GetOptimumResolutionPicture P == Position 0 W = %d, H = %d",imageFileFirst->GetWidth(),imageFileFirst->GetHeight() );
            return imageFileFirst;
        }
        //Start checking from the next image
        for(int i = 1; i < eina_list_count(mImages); i++) {
            ImageFile * imageFile = static_cast<ImageFile *>(eina_list_nth(mImages, i));
            if(imageFile->GetWidth() < width) {
             //This is the first smaller images in the set, return its previous image then, this would also take care of the
                //scenario wherein the sizes match
                imageFileFirst = static_cast<ImageFile *>(eina_list_nth(mImages, i-1));
                Log::debug("GetOptimumResolutionPicture P == Position %d W = %d, H = %d", i-1,imageFileFirst->GetWidth(),imageFileFirst->GetHeight() );
                return imageFileFirst;
            }

        }
        //This section would only be hit if the target size is still smaller then even the smallest image, in this case just
        //return the last image in the set.
        imageFileFirst = static_cast<ImageFile *>(eina_list_nth(mImages, eina_list_count(mImages)-1));
        Log::debug("GetOptimumResolutionPicture P == Position Last W = %d, H = %d",imageFileFirst->GetWidth(),imageFileFirst->GetHeight() );
        return imageFileFirst;
    }
}

ImageFile *Photo::GetMaxResDownloadedImageFile() {
    ImageFile *resultImageFile = nullptr;

    if(!mImages || !eina_list_count(mImages)) {
        if (mPostImage && mPostImage->GetStatus() == ImageFile::EDownloaded) {
            resultImageFile = mPostImage;
        }
    } else {
        Eina_List *l;
        void *listData;
        EINA_LIST_FOREACH(mImages, l, listData) {
            ImageFile *imageFile = static_cast<ImageFile *> (listData);
            assert(imageFile);
            if (imageFile->GetStatus() == ImageFile::EDownloaded) {
                if (!resultImageFile || imageFile->GetHeight() > resultImageFile->GetHeight()) {
                    resultImageFile = imageFile;
                }
            }
        }
    }

    return resultImageFile;
}

void Photo::SetLikeFromId(const char * likeFromId) {
    if (mPhotoPost) {
        mPhotoPost->SetLikeFromId(likeFromId);
    } else {
        free((void *)mLikeFromId);
        mLikeFromId = likeFromId;
    }
}

void Photo::SetLikeFromName(const char * likeFromName) {
    if (mPhotoPost) {
        mPhotoPost->SetLikeFromName(likeFromName);
    } else {
        free((void *) mLikeFromName);
        mLikeFromName = likeFromName;
    }
}

void Photo::SetPhotoMessageTagsList(std::list<Tag> messageTagsList) {
    void * listData;
    EINA_LIST_FREE(mPhotoMessageTagsList, listData) {
        delete static_cast<Tag*>(listData);
    }

    for(auto tagIt = messageTagsList.begin(); tagIt != messageTagsList.end(); tagIt++) {
        mPhotoMessageTagsList = eina_list_append(mPhotoMessageTagsList, new Tag(*tagIt));
    }
}

void Photo::SetPhotoMessageTagsList(Eina_List *messageTagsList) {
    void * listData;
    EINA_LIST_FREE(mPhotoMessageTagsList, listData) {
        delete static_cast<Tag*>(listData);
    }

    Eina_List* list = nullptr;
    void* data = nullptr;
    EINA_LIST_FOREACH(messageTagsList, list, data){
        Tag* tag = static_cast<Tag*>(data);
        mPhotoMessageTagsList = eina_list_append(mPhotoMessageTagsList, new Tag(*tag));
    }
}

std::list<Tag> Photo::GetPhotoMessageTagsList() {
    Eina_List* list = nullptr;
    void* data = nullptr;
    std::list<Tag> messageTags;
    EINA_LIST_FOREACH(mPhotoMessageTagsList, list, data) {
        Tag* tag = static_cast<Tag*>(data);
        CHECK_CONT(tag);
        messageTags.push_back(*tag);
    }
    return messageTags;
}
