#include "Friend.h"
#include "FriendsList.h"
#include "jsonutilities.h"

FriendsList::FriendsList(JsonObject *object) {
    mList = NULL;

    JsonArray *membersArray = json_object_get_array_member(object, "data");
    if (membersArray) {
        int len = json_array_get_length(membersArray);
        for (int i = 0; i < len; ++i) {
            JsonObject *member = json_array_get_object_element(membersArray, i);
            if(member) {
                Friend *fr = new Friend(member);
                mList = eina_list_append(mList, fr);
            }
        }
    }
}

FriendsList::~FriendsList() {
    void * listData;
    EINA_LIST_FREE(mList, listData) {
        (static_cast<Friend*>(listData))->Release();
    }
}
