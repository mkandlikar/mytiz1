#include <json-glib/json-glib.h>

#include "Album.h"
#include "Common.h"
#include "ErrorHandling.h"
#include "Event.h"
#include "GraphObject.h"
#include "Place.h"
#include "User.h"
#include "jsonutilities.h"

Picture::Picture(JsonObject * object) {
    mUrl = NULL;
    if (object) {
        JsonObject * picdata = json_object_get_object_member(object, "data");
        if (picdata) {
            mIsSilhouette = json_object_get_boolean_member(picdata, "is_silhouette");
            mUrl = GetStringFromJson(picdata, "url");
        }
    }
}

Picture::Picture(const Picture & other)
{
    mIsSilhouette = other.mIsSilhouette;
    mUrl = SAFE_STRDUP(other.mUrl);
}

Picture::~Picture()
{
    free((void*) mUrl);
}

/**
 * @brief Construct Album object from JsonObject
 * @param[in] object - Json Object, from which Error will be constructed
 */
Album::Album(JsonObject * object) {
    CHECK_RET_NRV(object);
    mGraphObjectType = GraphObject::GOT_ALBUM;
    SetId( GetStringFromJson(object, "id") );

    mCanUpload = json_object_get_boolean_member(object, "can_upload");
    mCount = json_object_get_int_member(object, "count");

    mCoverPhoto = GetStringFromJson(object, "cover_photo");
    mCreatedTime = GetStringFromJson(object, "created_time");
    mDescription = GetStringFromJson(object, "description");

    mEvent = NULL;
    JsonObject *eventObject = json_object_get_object_member(object, "event");
    if (eventObject != NULL)
    {
        mEvent = new Event(eventObject);
    }

    mFrom = NULL;
    JsonObject *fromObject = json_object_get_object_member(object, "from");
    if (fromObject != NULL)
    {
        mFrom = new User(fromObject);
    }

    mLink = GetStringFromJson(object, "link");
    mLocation = GetStringFromJson(object, "location");
    mName = GetStringFromJson(object, "name");
    mType = GetStringFromJson(object, "type");

    mPlace = NULL;
    JsonObject *placeObject = json_object_get_object_member(object, "place");
    if (placeObject != NULL)
    {
        mPlace = new Place(placeObject);
    }


    mPrivacy = GetStringFromJson(object, "privacy");

    //TODO: implement if needed
    //    mType
    //    enum{app, cover, profile, mobile, wall, normal, album}

    mUpdatedTime = GetStringFromJson(object, "updated_time");

    mPicture = NULL;
    JsonObject *picture = json_object_get_object_member(object, "picture");
    if (picture != NULL) {
        mPicture = new Picture(picture);
    }

}

Album::Album(const Album & other)
{
    mGraphObjectType = GraphObject::GOT_ALBUM;
    SetId(SAFE_STRDUP(other.GetId()));

    mCanUpload = other.mCanUpload;
    mCount = other.mCount;

    mCoverPhoto = SAFE_STRDUP(other.mCoverPhoto);
    mCreatedTime = SAFE_STRDUP(other.mCreatedTime);
    mDescription = SAFE_STRDUP(other.mDescription);

    mEvent = other.mEvent ? new Event(*other.mEvent) : NULL;
    mFrom = other.mFrom ? new User(*other.mFrom) : NULL;

    mLink = SAFE_STRDUP(other.mLink);
    mLocation = SAFE_STRDUP(other.mLocation);
    mName = SAFE_STRDUP(other.mName);
    mType = SAFE_STRDUP(other.mType);

    mPlace = other.mPlace ? new Place(*other.mPlace) : NULL;

    mPrivacy = SAFE_STRDUP(other.mPrivacy);

    mUpdatedTime = SAFE_STRDUP(other.mUpdatedTime);

    mPicture = other.mPicture ? new Picture(*other.mPicture) : NULL;
}

/**
 * @brief Destruction
 */
Album::~Album()
{
    free((void*) mCoverPhoto);
    free((void*) mCreatedTime);
    free((void*) mDescription);

    if (mEvent) {
        mEvent->Release();
    }
    if (mFrom) {
        mFrom->Release();
    }

    free((void*) mLink);
    free((void*) mLocation);
    free((void*) mName);
    free((char*) mType);

    if (mPlace) {
        mPlace->Release();
    }

    free((void*) mPrivacy);
    free((void*) mUpdatedTime);

    delete mPicture;
}

