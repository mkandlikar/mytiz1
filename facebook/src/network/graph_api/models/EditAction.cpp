#include "EditAction.h"
#include "jsonutilities.h"
#include "Utils.h"
#include "CSmartPtr.h"

EditAction::EditAction(JsonObject *object)
{
    mText = GetStringFromJson(object, "text");

    c_unique_ptr<char> time(GetStringFromJson(object, "edit_time"));
    c_unique_ptr<char> timeShiftFromString(Utils::GetTimeShift(time.get()));
    c_unique_ptr<char> currentTimeShift(Utils::GetCurrentTimeShift());

    bool needTimeShift = false;

    if (timeShiftFromString && currentTimeShift) {
        needTimeShift = strcmp(timeShiftFromString.get(), currentTimeShift.get());
    }

    double timeDouble = Utils::ConvertTimeToGMT(time.get(), needTimeShift);

    mEditTime = Utils::PresentTimeFromMillisecondsGeneral(timeDouble, needTimeShift);

    SetGraphObjectType(GraphObject::GOT_EDIT_ACTION);
}

EditAction::EditAction(const EditAction & other)
{
    mText = SAFE_STRDUP(other.mText);

    mEditTime = SAFE_STRDUP(other.mEditTime);
}

EditAction::~EditAction()
{
    free((void *) mText); mText = NULL;
    free((void *) mEditTime); mEditTime = NULL;
}

const char *EditAction::GetMessage() {
    return mText;
}

