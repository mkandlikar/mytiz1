#include <stdlib.h>
#include <string.h>

#include "Common.h"
#include "jsonutilities.h"
#include "ProfilePictureSource.h"

ProfilePictureSource::ProfilePictureSource(JsonObject * object)
{
    mHeight = 0;
    mWidth = 0;
    mIsSilhouette = TRUE;
    mUrl = nullptr;
    JsonObject * data_object = json_object_get_object_member(object, "data");
    if (data_object)
    {
        mHeight = json_object_get_int_member(data_object, "height");
        mWidth = json_object_get_int_member(data_object, "width");
        mIsSilhouette = json_object_get_boolean_member(data_object, "is_silhouette");
        mUrl = GetStringFromJson(data_object, "url");
    }
}

ProfilePictureSource::ProfilePictureSource(const ProfilePictureSource & other)
{
    mHeight = other.mHeight;
    mWidth = other.mWidth;
    mIsSilhouette = other.mIsSilhouette;
    mUrl = SAFE_STRDUP(other.mUrl);
}

ProfilePictureSource::~ProfilePictureSource()
{
    free((void *) mUrl);
}

