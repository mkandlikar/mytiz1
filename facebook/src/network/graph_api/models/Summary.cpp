#include "Summary.h"
#include <stdlib.h>

#include "Common.h"

/**
 * @brief Construct Summary object from JsonObject
 * @param object - Json Object, from which Summary will be constructed
 */
Summary::Summary(JsonObject * object): mTotalCount(0)
{
    if (object) {
        mTotalCount = json_object_get_int_member(object, "total_count");
    }
}

Summary::Summary()
{
    mTotalCount = 0;
}

/**
 * @brief Destructor
 */
Summary::~Summary()
{
}

/**
 * @brief Construct Summary object from JsonObject
 * @param object - Json Object, from which Summary will be constructed
 */
LikesSummary::LikesSummary(JsonObject * object) : Summary(object)
{
    mCanLike = json_object_get_boolean_member(object, "can_like");
    mHasLiked = json_object_get_boolean_member(object, "has_liked");
}

LikesSummary::LikesSummary() : Summary()
{
    mCanLike = false;
    mHasLiked = false;
}

LikesSummary::LikesSummary(const LikesSummary & other) : Summary()
{
    mCanLike = other.mCanLike;
    mHasLiked = other.mHasLiked;
    mTotalCount = other.mTotalCount;
}

/**
 * @brief Destructor
 */
LikesSummary::~LikesSummary()
{
}


