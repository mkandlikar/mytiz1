#include "Common.h"
#include "CSmartPtr.h"
#include "Group.h"
#include "jsonutilities.h"
#include "Friend.h"
#include "User.h"
#include "Utils.h"

/**
 * @brief Construct Group object from JsonObject
 * @param[in] object - Json Object, from which Group will be constructed
 */
Group::Group(JsonObject * object)
{
    SetId( GetStringFromJson(object, "id") );
    mGraphObjectType = GraphObject::GOT_GROUP;

    mCover = nullptr;
    JsonObject *coverObject = json_object_get_object_member(object, "cover");
    if (coverObject) {
        mCover = new CoverPhoto(coverObject);
    }
    mPictureUrl = nullptr;
    JsonObject *pictureObject = json_object_get_object_member(object, "picture");
    if (pictureObject) {
        JsonObject *dataObject = json_object_get_object_member(pictureObject, "data");
        if (dataObject) {
            mPictureUrl = GetStringFromJson(dataObject, "url");
        }
    }

    mOwner = nullptr;
    JsonObject *ownerObject = json_object_get_object_member(object, "owner");
    if (ownerObject)
    {
        //TODO: it is possible User/Page/Group, implement later if needed
        mOwner = new User(ownerObject);
    }

    mDescription = GetStringFromJson(object, "description");

    mIcon = GetStringFromJson(object, "icon");

    mMembershipState = eNOT_AVAILABLE;
    c_unique_ptr<char> membershipState(GetStringFromJson(object, "membership_state"));
    if (membershipState) {
        if (!strcmp(membershipState.get(), "MEMBER")) {
            if (mOwner && Utils::IsMe(mOwner->GetId())) {
                mMembershipState = eOWNER;
            } else {
                mMembershipState = eMEMBER;
            }
        } else if (!strcmp(membershipState.get(), "CAN_REQUEST")) {
            mMembershipState = eCAN_REQUEST;
        } else if (!strcmp(membershipState.get(), "REQUESTED")) {
            mMembershipState = eREQUESTED;
        }
    }

    mName = GetStringFromJson(object, "name");

    mPrivacy = eCLOSED;
    c_unique_ptr<char> privacy(GetStringFromJson(object, "privacy"));
    if (privacy) {
        if (!strcmp(privacy.get(), "OPEN")) {
            mPrivacy = eOPEN;
        } else if (!strcmp(privacy.get(), "SECRET")) {
            mPrivacy = eSECRET;
        }
    }

    mMembersCount = 0;
    JsonObject *members = json_object_get_object_member(object, "members");
    if (members) {
        JsonObject *summary = json_object_get_object_member(members, "summary");
        if (summary) {
            mMembersCount = json_object_get_int_member(summary, "total_count");
        }
    }

    mMembersRequestCount = json_object_get_int_member(object, "member_request_count");
}

Group::Group(const Group & other)
{
    SetId(SAFE_STRDUP(other.GetId()));
    mGraphObjectType = GraphObject::GOT_GROUP;

    mCover = other.mCover ? new CoverPhoto(*other.mCover) : nullptr;
    mPictureUrl = SAFE_STRDUP(other.mPictureUrl);

    mOwner = other.mOwner ? new User(*static_cast<User *>(other.mOwner)) : nullptr;

    mDescription = SAFE_STRDUP(other.mDescription);
    mIcon = SAFE_STRDUP(other.mIcon);

    mName = SAFE_STRDUP(other.mName);

    mPrivacy = other.mPrivacy;
    mMembersCount = other.mMembersCount;
    mMembersRequestCount = other.mMembersRequestCount;
    mMembershipState = other.mMembershipState;
}

/**
 * @brief Destruction
 */
Group::~Group()
{
    if (mCover) {
        mCover->Release();
    }
    if (mOwner) {
        mOwner->Release();
    }

    free ((void*) mDescription);
    free ((void*) mIcon);
    free ((void*) mName);
    free ((void*) mMembersCount);
    free (mPictureUrl);
}

const char * Group::GetPrivacyAsStr() const
{
    switch (mPrivacy) {
    case eCLOSED:
        return "Closed";
    case eOPEN:
        return "Open";
    case eSECRET:
        return "Secret";
    }
}

void Group::SetStringForDataType(const char *str, int dataType, int index)
{
    if (str && mCover) {
        if (mCover->mPhotoPath) {
            free((void*) mCover->mPhotoPath); mCover->mPhotoPath = nullptr;
        }
        mCover->mPhotoPath = SAFE_STRDUP(str);
    }
}
