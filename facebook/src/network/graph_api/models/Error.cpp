#include <memory>
#include <stdlib.h>
#include <string.h>

#include "Common.h"
#include "CSmartPtr.h"
#include "Error.h"
#include "jsonutilities.h"

/**
 * @brief Construct Error object from JsonObject
 * @param[in] object - Json Object, from which Error will be constructed
 */
Error::Error(JsonObject * object)
{
    mCode = json_object_get_int_member(object, "code");
    mErrorSubcode = json_object_get_int_member(object, "error_subcode");
    mMessage = GetStringFromJson(object, "message");
    c_unique_ptr<char> type(GetStringFromJson(object, "type"));
    mType = eUnknown;
    if (type) {
        if (!strcmp(type.get(), "OAuthException")) {
            mType = eOAuthException;
        } else if (!strcmp(type.get(), "GraphMethodException")) {
            mType = eGraphMethodException;
        }
    }
}

/**
 * @brief Destructor
 */
Error::~Error()
{
    free((void *)mMessage);
}
