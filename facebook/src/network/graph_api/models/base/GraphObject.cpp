#include "FacebookSession.h"
#include "GraphObject.h"
#include "Utils.h"

#include <efl_extension.h>

GraphObject::GraphObject( GraphObjectTypeEnum type ) : mGraphObjectType( type ), mActiveAsyncRequests(nullptr)
{
    mId = nullptr;
    mHash = 0;
    if ( type == GOT_SEPARATOR ) {
        SetId( 0 );
    }
}

GraphObject::~GraphObject()
{
    free( (void*) mId);
    void * listData;
    EINA_LIST_FREE( mActiveAsyncRequests, listData ) {
        delete static_cast<AsyncRequestData*>(listData);
    }
}

bool GraphObject::operator==(const GraphObject& otherObject) const {
    return (mHash == otherObject.mHash && !strcmp(mId, otherObject.mId));
}

int GraphObject::GetGraphObjectType() const {
    return mGraphObjectType;
}

void GraphObject::SetGraphObjectType( GraphObjectTypeEnum type ) {
    mGraphObjectType = type;
}

GraphObject::AsyncRequestData* GraphObject::GetActiveAsyncRequest(int reqType) {
    if(mActiveAsyncRequests) {
        Eina_List *list;
        void *listData;
        EINA_LIST_FOREACH (mActiveAsyncRequests, list, listData) {
            AsyncRequestData *req = static_cast<AsyncRequestData*>(listData);
            if (reqType == req->mAsyncRequestType) {
                return req;
            }
        }
    }
    return NULL;
}

bool GraphObject::GetAsync(int dataType, void* data, AsyncClientCallback callBack) {
    int reqType = GetAsyncRequestTypeForData(dataType);
    if (reqType == -1) return false;
    AsyncRequestData* req = GetActiveAsyncRequest(reqType);
    GraphRequest* res = NULL;
    if (!req) {
        req = new AsyncRequestData(reqType, this);
        mActiveAsyncRequests = eina_list_append(mActiveAsyncRequests, req);
        res = SendAsyncRequest(reqType, req, AsyncRequestCb);
        req->SetGraphRequest(res);
    }
    req->AddAsyncClient(dataType, callBack, data);
    //ToDo: probably handle some network error conditions here. Let's keep it as it is.
    return true;
}

void GraphObject::AsyncRequestCb(void* data, char* respData, int code) {
    AsyncRequestData * req = (AsyncRequestData*) data;
    GraphObject * me = (GraphObject *) req->mGraphObject;
    bool r = me->ParseAsyncResponse(req->mAsyncRequestType, respData, code);
    req->NotifyClients(r);
    me->mActiveAsyncRequests = eina_list_remove(me->mActiveAsyncRequests, req);
    delete req;
}

void GraphObject::CancelAsyncReq(void *clientData) {
    if(mActiveAsyncRequests) {
        Eina_List *list;
        void *listData;
        EINA_LIST_FOREACH (mActiveAsyncRequests, list, listData) {
            AsyncRequestData *req = (AsyncRequestData *) listData;
            req->Cancel(clientData);
        }
    }
}

//class GraphObject::AsyncRequestData
GraphObject::AsyncRequestData::AsyncRequestData(int reqType, GraphObject* me) : mGraphRequest(NULL), mAsyncRequestType(reqType),
        mAsyncClients(NULL), mGraphObject(me) {
    if(mGraphObject) {
        mGraphObject->AddRef();
    }
}

GraphObject::AsyncRequestData::~AsyncRequestData() {
    FacebookSession::GetInstance()->ReleaseGraphRequest(mGraphRequest);
    if(mGraphObject) {
        mGraphObject->Release();
    }
}

void GraphObject::AsyncRequestData::AddAsyncClient(int dataType, AsyncClientCallback callBack, void* data) {
    if (callBack) {
        AsyncClientData* client = new AsyncClientData();
        client->mData = data;
        client->mCallBack = callBack;
        client->mDataType = dataType;
        mAsyncClients = eina_list_append(mAsyncClients, client);
    }
}

void GraphObject::AsyncRequestData::NotifyClients(bool result) {
    if(mAsyncClients) {
        Eina_List *list;
        void *listData;
        EINA_LIST_FOREACH (mAsyncClients, list, listData) {
            AsyncClientData *client = static_cast<AsyncClientData *>(listData);
            if(client) {
                if (!result || !mGraphObject) {
                    client->mCallBack(client->mData, NULL);
                } else {
                    client->mCallBack(client->mData, mGraphObject->GetStringForDataType(client->mDataType));
                }
                mAsyncClients = eina_list_remove(mAsyncClients, client);
                delete client;
            }
        }
        eina_list_free(mAsyncClients);
        mAsyncClients = NULL;
    }

}

bool GraphObject::AsyncRequestData::Cancel(void *clientData) {
    Eina_List *list;
    Eina_List *l_next;
    void *listData;
    bool res = false;
    if(mAsyncClients) {
        EINA_LIST_FOREACH_SAFE (mAsyncClients, list, l_next, listData) {
            AsyncClientData *client = (AsyncClientData *) listData;
            if (client->mData == clientData) {
                mAsyncClients = eina_list_remove(mAsyncClients, client);
                delete client;
                res = true;
                break;
            }
        }
    }
    return res;
}

void GraphObject::AsyncRequestData::SetGraphRequest(GraphRequest *graphRequest) {
    mGraphRequest = graphRequest;
}
GraphRequest *GraphObject::AsyncRequestData::GetGraphRequest() {
    return mGraphRequest;
}

void GraphObject::SetId( char *id )
{
    if ( id == NULL || strlen( id ) == 0 ) {
        // ULLONG_MAX == 18446744073709551615, so there is
        // no reason to allocate more bytes for id string
        id = (char*)malloc( sizeof(char)*22 );
        Utils::Snprintf_s( id, sizeof(char)*22, "%d", rand() );
    }
    free((void *)mId);
    mId = id;
    mHash = eina_hash_superfast( id, ( strlen( id ) + 1 ) );
}

const char* GraphObject::GetId() const
{
    return mId;
}

int GraphObject::GetHash() const
{
    return mHash;
}
