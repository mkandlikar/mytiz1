#include "BaseObject.h"

BaseObject::BaseObject():mRefCount(1) {

};

BaseObject::~BaseObject() {

};

unsigned int BaseObject::AddRef() {
    mRefCount++;
    return mRefCount;
};

unsigned int BaseObject::Release() {
    if(mRefCount > 0) {
        mRefCount--;
    } else {
        // it means that something going wrong
        mRefCount = 0;
    }
    if (0 == mRefCount) {
        delete this;
        return 0;
    }
    return mRefCount;
};
