#include "Common.h"
#include "CSmartPtr.h"
#include "jsonutilities.h"
#include "Post.h"
#include "UserProfileData.h"

UserProfileData::UserProfileData(JsonObject *object) :
    Person(object)
{
    SetId(GetStringFromJson(object, "id"));

    mName = GetStringFromJson(object, "name");
    mFirstName = GetStringFromJson(object, "first_name");
    mLastName = GetStringFromJson(object, "last_name");

    mCover = NULL;
    JsonObject *coverObject = json_object_get_object_member(object, "cover");
    if (coverObject) {
        mCover = new CoverPhoto(coverObject);
    }

    mPicture = NULL;
    JsonObject *smallPicObject = json_object_get_object_member(object, "picture");
    if (smallPicObject) {
        mPicture = new ProfilePictureSource(smallPicObject);
    }

    mSubscribeStatus = SUBSCRIBE_UNKNOWN;
    c_unique_ptr<char> subscribe(GetStringFromJson(object, "subscribe_status"));
    if (subscribe) {
        if (!strcmp(subscribe.get(), "CANNOT_SUBSCRIBE")) {
            mSubscribeStatus = CANNOT_SUBSCRIBE;
        } else if (!strcmp(subscribe.get(), "IS_SUBSCRIBED")) {
            mSubscribeStatus = IS_SUBSCRIBED;
        } else if (!strcmp(subscribe.get(), "CAN_SUBSCRIBE")) {
            mSubscribeStatus = CAN_SUBSCRIBE;
        }
    }

    mAboutPhotoUrl = nullptr;
    mAboutPhotoPath = nullptr;

    mFirstPhotoUrl = nullptr;
    mFirstPhotoPath = nullptr;

    mFirstFriendUrl = nullptr;
    mFirstFriendPath = nullptr;

    mUserAvatarId = nullptr;
    mUserAvatarPath = nullptr;

    SetAboutPhotoUrl(object);
}

UserProfileData::UserProfileData(const UserProfileData &other)
{
    SetId(SAFE_STRDUP(other.GetId()));

    SetProfileDataVariables(other);

    mCover = other.mCover ? new CoverPhoto(*other.mCover) : nullptr;
    mPicture = other.mPicture ? new ProfilePictureSource(*other.mPicture) : nullptr;
}

UserProfileData::~UserProfileData()
{
    if (mCover) {
        mCover->Release();
    }
    delete mPicture;

    free((void *) mName);
    free((void *) mFirstName);
    free((void *) mLastName);

    free((void *) mFirstPhotoUrl);
    free((void *) mFirstPhotoPath);

    free((void *) mFirstFriendUrl);
    free((void *) mFirstFriendPath);

    free((void *) mUserAvatarId);
    free((void *) mUserAvatarPath);

    free((void *) mAboutPhotoUrl);
    free((void *) mAboutPhotoPath);
}

void UserProfileData::CopyProfileData(const UserProfileData &other)
{
    if (mName) {
        free((void *) mName); mName = NULL;
    }

    if (mLastName) {
        free((void *) mLastName); mLastName = NULL;
    }

    if (mFirstName) {
        free((void *) mFirstName); mFirstName = NULL;
    }

    if (mFirstPhotoUrl) {
        free((void *) mFirstPhotoUrl); mFirstPhotoUrl = NULL;
    }

    if (mFirstPhotoPath) {
        free((void *) mFirstPhotoPath); mFirstPhotoPath = NULL;
    }

    if (mFirstFriendUrl) {
        free((void *) mFirstFriendUrl); mFirstFriendUrl = NULL;
    }

    if (mFirstFriendPath) {
        free((void *) mFirstFriendPath); mFirstFriendPath = NULL;
    }

    if (mUserAvatarId) {
        free((void *) mUserAvatarId); mUserAvatarId = NULL;
    }

    if (mUserAvatarPath) {
        free((void *) mUserAvatarPath); mUserAvatarPath = NULL;
    }

    if (mAboutPhotoUrl) {
        free((void *) mAboutPhotoUrl); mAboutPhotoUrl = NULL;
    }

    if (mAboutPhotoPath) {
        free((void *) mAboutPhotoPath); mAboutPhotoPath = NULL;
    }

    SetProfileDataVariables(other);
}

void UserProfileData::SetProfileDataVariables(const UserProfileData &other)
{
    mName = SAFE_STRDUP(other.mName);
    mFirstName = SAFE_STRDUP(other.mFirstName);
    mLastName = SAFE_STRDUP(other.mLastName);

    mFriendshipStatus = other.mFriendshipStatus;
    mSubscribeStatus = other.mSubscribeStatus;

    mFirstPhotoUrl = SAFE_STRDUP(other.mFirstPhotoUrl);
    mFirstPhotoPath = SAFE_STRDUP(other.mFirstPhotoPath);

    mFirstFriendUrl = SAFE_STRDUP(other.mFirstFriendUrl);
    mFirstFriendPath = SAFE_STRDUP(other.mFirstFriendPath);

    mUserAvatarId = SAFE_STRDUP(other.mUserAvatarId);
    mUserAvatarPath = SAFE_STRDUP(other.mUserAvatarPath);

    mAboutPhotoUrl = SAFE_STRDUP(other.mAboutPhotoUrl);
    mAboutPhotoPath = SAFE_STRDUP(other.mAboutPhotoPath);
}

void UserProfileData::SetStringForDataType(const char *str, int dataType, int index)
{
    if (str) {
        if (dataType == Post::EProfileCover) {
            if (mCover) {
                if (mCover->mPhotoPath) {
                    free((void*) mCover->mPhotoPath);
                }
                mCover->mPhotoPath = SAFE_STRDUP(str);
            }
        } else if (dataType == Post::EProfileAvatar) {
            if (mUserAvatarPath) {
                free((void *) mUserAvatarPath);
            }
            mUserAvatarPath = SAFE_STRDUP(str);
        } else if (dataType == Post::EProfileFirstFriend) {
            if (mFirstFriendPath) {
                free((void *) mFirstFriendPath);
            }
            mFirstFriendPath = SAFE_STRDUP(str);
        } else if (dataType == Post::EProfileFirstPhoto) {
            if (mFirstPhotoPath) {
                free((void *) mFirstPhotoPath);
            }
            mFirstPhotoPath = SAFE_STRDUP(str);
        } else if (dataType == Post::EProfileAboutInfo) {
            if (mAboutPhotoPath) {
                free((void *) mAboutPhotoPath);
            }
            mAboutPhotoPath = SAFE_STRDUP(str);
        }
    }
}

void UserProfileData::SetFirstPhotoData(JsonObject *object)
{
    JsonArray *arrayParentData = json_object_get_array_member(object, "data");
    if (arrayParentData) {
        JsonObject *objFirst = json_array_get_object_element(arrayParentData, 0);
        if (objFirst) {
            JsonArray *imageList = json_object_get_array_member(objFirst, "images");
            if (imageList) {
                JsonObject *objElem = json_array_get_object_element(imageList, json_array_get_length(imageList) - 1);
                if (objElem) {
                    mFirstPhotoUrl = GetStringFromJson(objElem, "source");
                }
            }
        }
    }
}

void UserProfileData::SetFirstFriendData(JsonObject *object)
{
    JsonArray *arrayParentData = json_object_get_array_member(object, "data");
    if (arrayParentData) {
        JsonObject *objFirst = json_array_get_object_element(arrayParentData, 0);
        if (objFirst) {
            JsonObject *imageObj = json_object_get_object_member(objFirst, "picture");
            if (imageObj) {
                JsonObject *dataObj = json_object_get_object_member(imageObj, "data");
                if (dataObj) {
                    mFirstFriendUrl = GetStringFromJson(dataObj, "url");
                }
            }
        }
    }
}

void UserProfileData::SetUserAvatarData(JsonObject *object)
{
    JsonArray *arrayParentData = json_object_get_array_member(object, "data");
    int arrLength = json_array_get_length(arrayParentData);
    if (arrayParentData) {
        for (int index = 0; index < arrLength; ++index) {
            JsonObject *element = json_array_get_object_element(arrayParentData, index);
            if (element) {
                c_unique_ptr<char> typeStr(GetStringFromJson(element, "type"));
                if (typeStr && !strcmp("profile", typeStr.get())) {
                    JsonObject *userAvatarObj = json_object_get_object_member(element, "cover_photo");
                    if (userAvatarObj) {
                        mUserAvatarId = GetStringFromJson(userAvatarObj, "id");
                    }
                }
            }
        }
    }
}

void UserProfileData::SetAboutPhotoUrl(JsonObject *obj)
{
    JsonArray *arrayWork = json_object_get_array_member(obj, "work");
    if (arrayWork) {
        JsonObject *objWork = json_array_get_object_element(arrayWork, 0);
        if (objWork) {
            JsonObject *objEmployer = json_object_get_object_member(objWork, "employer");
            if (objEmployer) {
                JsonObject* objEmpPic = json_object_get_object_member(objEmployer, "picture");
                if (objEmpPic) {
                    JsonObject* objPicData = json_object_get_object_member(objEmpPic, "data");
                    if (objPicData) {
                        mAboutPhotoUrl = GetStringFromJson(objPicData, "url");
                    }
                }
            }
        }
    }
}
