#include "SuggestedFriend.h"
#include "jsonutilities.h"

/**
 * @brief Construct SuggestedFriend object from JsonObject
 * @param[in] object - Json Object, from which Friend will be constructed
 */
SuggestedFriend::SuggestedFriend(JsonObject * object)
{
    GraphObject::SetId( GetStringFromJson(object, "id") );
    mName = GetStringFromJson(object, "name");
}

/**
 * @brief Destructor
 */
SuggestedFriend::~SuggestedFriend()
{
    free ((void*) mName);
}
