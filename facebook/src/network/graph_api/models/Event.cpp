#include <string.h>

#include "Common.h"
#include "CSmartPtr.h"
#include "Event.h"
#include "jsonutilities.h"
#include "User.h"
#include "Utils.h"

#define EVENT_LINK "https://www.facebook.com/events/"
/**
 * @brief Construct Event object from JsonObject
 * @param[in] object - Json Object, from which Event will be constructed
 */
Event::Event(JsonObject * object)
{
    SetId( GetStringFromJson(object, "id") );

    mCover = NULL;
    JsonObject *coverObject = json_object_get_object_member(object, "cover");
    if (coverObject)
    {
        mCover = new CoverPhoto(coverObject);
    }

    mDescription = GetStringFromJson(object, "description");

    mIsDateOnly = json_object_get_boolean_member(object, "is_date_only");

    mCanGuestsInvite = json_object_get_boolean_member(object, "can_guests_invite");

    mGuestListsEnabled = json_object_get_boolean_member(object, "guest_list_enabled");

    mName = GetStringFromJson(object, "name");

    mOwner = nullptr;
    JsonObject *ownerObject = json_object_get_object_member(object, "owner");
    if (ownerObject) {
        //TODO: it is possible User/Page/Group, implement later if needed
        mOwner = new User(ownerObject);
    }

    mParentGroup = NULL;
    JsonObject *parentObject = json_object_get_object_member(object, "parent_group");
    if (parentObject) {
        mParentGroup = new Group(parentObject);
    }

    mPlace = NULL;
    JsonObject *placeObject = json_object_get_object_member(object, "place");
    if (placeObject) {
        mPlace = new Place(placeObject);
    }

    mPrivacyStatus = GetStringFromJson(object, "type");
    mPrivacy = GetStringFromJson(object, "privacy");

    c_unique_ptr<char> startTime(GetStringFromJson(object, "start_time"));
    c_unique_ptr<char> timeShiftFromString(Utils::GetTimeShift(startTime.get()));
    c_unique_ptr<char> currentTimeShift(Utils::GetCurrentTimeShift());

    if (startTime && timeShiftFromString && currentTimeShift) {
        mNeedTimeShift = strcmp(timeShiftFromString.get(), currentTimeShift.get());
    }

    mStartTime = Utils::ConvertTimeToGMT(startTime.get(), mNeedTimeShift);
    mStartTimeString = Utils::PresentTimeFromMillisecondsFullFormat(mStartTime, mNeedTimeShift);

    c_unique_ptr<char> endTime(GetStringFromJson(object, "end_time"));
    mEndTime = Utils::ConvertTimeToGMT(endTime.get(), mNeedTimeShift);
    if (mEndTime != 0) {
        mEndTimeString = Utils::PresentTimeFromMillisecondsFullFormat(mEndTime, mNeedTimeShift);
    } else {
        mEndTimeString = nullptr;
    }

    mTicketUri = GetStringFromJson(object, "ticket_uri");
    mTimezone = GetStringFromJson(object, "timezone");
    mUpdatedTime = GetStringFromJson(object, "updated_time");

    mAttendingCount = GetGint64FromJson(object, "attending_count");
    mMaybeCount = GetGint64FromJson(object, "maybe_count");
    mInvitedCount = GetGint64FromJson(object, "noreply_count");
    mDeclinedCount = GetGint64FromJson(object, "declined_count");

    mStartTimePresenter = nullptr;
    mEndTimePresenter = nullptr;
    mCuttedDescription = nullptr;
    mGraphObjectType = GraphObject::GOT_EVENT;

    isInvited = false;

    mLinkPresenter = nullptr;

    mRsvpStatus = EVENT_RSVP_NONE;
    c_unique_ptr<char> rsvpStatus(GetStringFromJson(object, "rsvp_status"));
    if (rsvpStatus) {
        if (!strcmp(rsvpStatus.get(), "attending")) {
            mRsvpStatus = EVENT_RSVP_ATTENDING;
        } else if (!strcmp(rsvpStatus.get(), "unsure")) {
            mRsvpStatus = EVENT_RSVP_MAYBE;
        } else if (!strcmp(rsvpStatus.get(), "declined")) {
            mRsvpStatus = EVENT_RSVP_DECLINED;
        } else if (!strcmp(rsvpStatus.get(), "not_replied")) {
            mRsvpStatus = EVENT_RSVP_NOT_REPLIED;
        }
    }
}

Event::Event(const Event & other)
{
    SetId(SAFE_STRDUP(other.GetId()));

    SetEventVariables(other);

    mCover = other.mCover ? new CoverPhoto(*other.mCover) : nullptr;

    //TODO: it is possible User/Page/Group, implement later if needed
    mOwner = other.mOwner ? new User(*static_cast<User *>(other.mOwner)) : nullptr;
    mParentGroup = other.mParentGroup ? new Group(*other.mParentGroup) : nullptr;

    mTicketUri = SAFE_STRDUP(other.mTicketUri);
    mTimezone = SAFE_STRDUP(other.mTimezone);
    mUpdatedTime = SAFE_STRDUP(other.mUpdatedTime);
}

/**
 * @brief Destruction
 */
Event::~Event()
{
    if (mCover) {
        mCover->Release();
    }

    free((void*)mDescription);
    free((void*)mName);

    if (mOwner) {
        mOwner->Release();
    }

    if (mParentGroup) {
        mParentGroup->Release();
    }

    if (mPlace) {
        mPlace->Release();
    }

    free((void*)mPrivacyStatus);
    free((void*)mTicketUri);
    free((void*)mTimezone);
    free((void*)mUpdatedTime);
    free((void*)mPrivacy);

    free((void*)mStartTimeString);
    free((void*)mEndTimeString);

    free((void*) mStartTimePresenter);
    free((void*) mEndTimePresenter);
    free((void*) mCuttedDescription);

    free((void *) mLinkPresenter); mLinkPresenter = NULL;
}

void Event::CopyEvent(const Event &other)
{
    if (mDescription) {
        free((void *) mDescription); mDescription = NULL;
    }
    if (mName) {
        free((void *) mName); mName = NULL;
    }
    if (mPlace) {
        delete mPlace;
    }
    if (mPrivacyStatus) {
        free((void *) mPrivacyStatus); mPrivacyStatus = NULL;
    }
    if (mPrivacy) {
        free((void *) mPrivacy); mPrivacy = NULL;
    }
    if (mStartTimePresenter) {
        free((void *) mStartTimePresenter); mStartTimePresenter = NULL;
    }
    if (mEndTimePresenter) {
        free((void *) mEndTimePresenter); mEndTimePresenter = NULL;
    }
    if (mCuttedDescription) {
        free((void *) mCuttedDescription); mCuttedDescription = NULL;
    }
    if (mLinkPresenter) {
        free((void *) mLinkPresenter); mLinkPresenter = NULL;
    }

    SetEventVariables(other);
}

void Event::SetEventVariables(const Event &other)
{
    mDescription = SAFE_STRDUP(other.mDescription);
    mIsDateOnly = other.mIsDateOnly;
    mCanGuestsInvite = other.mCanGuestsInvite;
    mGuestListsEnabled = other.mGuestListsEnabled;

    mName = SAFE_STRDUP(other.mName);

    mPlace = other.mPlace ? new Place(*other.mPlace) : nullptr;

    mPrivacyStatus = SAFE_STRDUP(other.mPrivacyStatus);

    mPrivacy = SAFE_STRDUP(other.mPrivacy);

    mRsvpStatus = other.mRsvpStatus;

    mStartTime = other.mStartTime;
    mStartTimeString = other.mStartTimeString;
    mEndTime = other.mEndTime;
    mEndTimeString = other.mEndTimeString;

    mStartTimePresenter = SAFE_STRDUP(other.mStartTimePresenter);
    mEndTimePresenter = SAFE_STRDUP(other.mEndTimePresenter);

    mCuttedDescription = SAFE_STRDUP(other.mCuttedDescription);

    mAttendingCount = other.mAttendingCount;
    mMaybeCount = other.mMaybeCount;
    mInvitedCount = other.mInvitedCount;
    mDeclinedCount = other.mDeclinedCount;

    isInvited = other.isInvited;

    mLinkPresenter = SAFE_STRDUP(other.mLinkPresenter);
}

const char *Event::GetStartTime()
{
    if ( ! mStartTimePresenter ) {
        mStartTimePresenter = Utils::PresentTimeFromMillisecondsFullFormat(mStartTime, mNeedTimeShift);
    }
    return mStartTimePresenter;
}

const char *Event::GetEndTime()
{
    if ( ! mEndTimePresenter ) {
        mEndTimePresenter = Utils::PresentTimeFromMillisecondsFullFormat(mEndTime, mNeedTimeShift);
    }
    return mEndTimePresenter;
}

const char *Event::CutDescription()
{
    if (mCuttedDescription != NULL) {
        return mCuttedDescription;
    }
    const char * description = mDescription;
    int count = strlen(description);
    std::string str1 = description;

    if (count > 660) {
        mCuttedDescription = (char*) new char[str1.length() + 1];
        if (mCuttedDescription) {
            eina_strlcpy(mCuttedDescription, str1.c_str(), 660);
        }
    }
    else {
        mCuttedDescription = Utils::getCopy(str1.c_str());
    }
    return mCuttedDescription;
}

const char *Event::GetLink()
{
    if (GetId()) {
        std::string link = EVENT_LINK;
        link.append(GetId());
        link.append("/");
        free((void *) mLinkPresenter);
        mLinkPresenter = SAFE_STRDUP(link.c_str());
    }
    return mLinkPresenter;
}

void Event::SetStringForDataType(const char *str, int dataType, int index)
{
    if (str && mCover) {
        if (mCover->mPhotoPath) {
            free((void*) mCover->mPhotoPath);
            mCover->mPhotoPath = nullptr;
        }
        mCover->mPhotoPath = SAFE_STRDUP(str);
    }
}
