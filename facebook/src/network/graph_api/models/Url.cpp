#include "Common.h"
#include "Url.h"
#include "jsonutilities.h"

Url::Url(JsonObject * object)
{
    mShare = nullptr;
    JsonObject * share_object = json_object_get_object_member(object, "share");
    if (share_object) {
        mShare = new Share(share_object);
    }
    SetId(GetStringFromJson(object, "id"));

}

Url::~Url()
{
    delete mShare;
}

Share::Share(JsonObject * object)
{
    mCommentCount = json_object_get_int_member(object, "comment_count");
    mShareCount = json_object_get_int_member(object, "share_count");
}
