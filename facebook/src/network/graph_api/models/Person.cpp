#include "FacebookSession.h"
#include "FriendsAction.h"
#include "Person.h"
#include "Utils.h"

#include "jsonutilities.h"

Person::Person() {
    mGraphRequestTracker = new GraphRequestTracker(this);
}

Person::Person(JsonObject *object) {
    char *friendshipStatus = GetStringFromJson(object, "friendship_status");
    if (friendshipStatus) {
        if (!Utils::UCompare (friendshipStatus, "ARE_FRIENDS", false)) {
            mFriendshipStatus = eARE_FRIENDS;
        } else if (!Utils::UCompare(friendshipStatus, "CAN_REQUEST", false )) {
            mFriendshipStatus = eCAN_REQUEST;
        } else if (!Utils::UCompare(friendshipStatus, "CANNOT_REQUEST", false)) {
            mFriendshipStatus = eCANNOT_REQUEST;
        } else if (!Utils::UCompare(friendshipStatus, "OUTGOING_REQUEST", false)) {
            mFriendshipStatus = eOUTGOING_REQUEST;
        } else if (!Utils::UCompare(friendshipStatus, "INCOMING_REQUEST", false)) {
            mFriendshipStatus = eINCOMING_REQUEST;
        }
    }
    if (mFriendshipStatus != eCANNOT_REQUEST) {
        mGraphRequestTracker = new GraphRequestTracker(this);
    }
    free(friendshipStatus);
}

Person::~Person() {
    delete mGraphRequestTracker;
}

Person::GraphRequestTracker::GraphRequestTracker(Person *parent):
    mPerson (parent) {
    assert(parent);
}

Person::GraphRequestTracker::~GraphRequestTracker() {
    ReleaseRequests();
    if (mRequestTimer) {
        ecore_timer_del(mRequestTimer);
        mPerson->Release();
    }
}

void Person::GraphRequestTracker::RestartRequestTimer() {// Note: only for FR in MainMenu
    if (!mRequestTimer) {
        mPerson->AddRef();
        double delay = mUseBigDelay ? 2.0 : 0.5;
        Log::info("Person::GraphRequestTracker::RestartRequestTimer  with %f delay",delay);
        mRequestTimer = ecore_timer_add(delay, on_request_timer_cb, this);
    } else {
        ecore_timer_reset(mRequestTimer);
        Log::info("Person::GraphRequestTracker::RestartRequestTimer->Reset");
    }
}

Eina_Bool Person::GraphRequestTracker::on_request_timer_cb(void *data) {
    GraphRequestTracker *me = static_cast<GraphRequestTracker*>(data);
    if (me->mRequestTimer) {
        Log::info("Person::GraphRequestTracker::on_request_timer_cb");
        ecore_timer_del(me->mRequestTimer);
        me->mRequestTimer = nullptr;
        me->mUseBigDelay = false;
        me->RepeatAddCancelRequest();
        me->mPerson->Release();
    }
    return false;
}

void Person::GraphRequestTracker::RepeatAddCancelRequest() {
    ReleaseRequests();
    if (mPerson->mFriendshipStatus == eOUTGOING_REQUEST) {
        SendFriendRequest();
    } else if (mPerson->mFriendshipStatus == eCAN_REQUEST) {
        CancelOutgoingFriendRequest();
        mUseBigDelay = true;// useful trick to postpone Add after Cancel...
        //FB server processes requests slowly : otherwise 520 error occurs and state is not changed
    }
}

void Person::GraphRequestTracker::SendFriendRequest() {
    Log::info("Person::GraphRequestTracker::SendFriend");
    mAddFriendRequest = FacebookSession::GetInstance()->SendFriendRequest(mPerson->GetId(), FriendsAction::on_add_friend_completed, mPerson);
}

void Person::GraphRequestTracker::CancelOutgoingFriendRequest() {
    Log::info("Person::GraphRequestTracker::Cancel");
    mCancelFriendRequest = FacebookSession::GetInstance()->CancelOutgoingFriendRequest(mPerson->GetId(), FriendsAction::on_cancel_friend_completed, mPerson);
}

void Person::GraphRequestTracker::ReleaseRequests() {
    FacebookSession::GetInstance()->ReleaseGraphRequest(mAddFriendRequest, true);
    FacebookSession::GetInstance()->ReleaseGraphRequest(mCancelFriendRequest, true);
}
