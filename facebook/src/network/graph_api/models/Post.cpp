#include "Application.h"
#include "Common.h"
#include "CSmartPtr.h"
#include "FacebookSession.h"
#include "FbResponseEventDetails.h"
#include "FbResponseFriendDetails.h"
#include "FbResponseGroupDetails.h"
#include "GraphRequest.h"
#include "jsonutilities.h"
#include "Log.h"
#include "Photo.h"
#include "Post.h"
#include "PrivacyOptionsData.h"
#include "Queries.h"
#include "Utils.h"

#include <app.h>
#include <assert.h>
#include <openssl/md5.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <utils_i18n.h>

Post::Place::Place()
{
    mId = nullptr;
    mName = nullptr;
    mCity = nullptr;
    mCountry = nullptr;
    mLatitude = 0.0;
    mLongitude = 0.0;
    mStreet = nullptr;
    mCheckins = 0;
}

Post::Place::Place(const Post::Place& other) {
    mId = SAFE_STRDUP(other.mId);
    mName = SAFE_STRDUP(other.mName);
    mCity = SAFE_STRDUP(other.mCity);
    mCountry = SAFE_STRDUP(other.mCountry);
    mLatitude = other.mLatitude;
    mLongitude = other.mLongitude;
    mStreet = SAFE_STRDUP(other.mStreet);
    mCheckins = other.mCheckins;
}


Post::Place::~Place()
{
    free(mId);
    free(mName);
    free(mCity);
    free(mCountry);
    free(mStreet);
}

Privacy::Privacy()
{
    mValue = nullptr;
    mDescription = nullptr;
    mFriends = nullptr;
    mAllow = nullptr;
    mDeny = nullptr;
}
Privacy::~Privacy()
{
    free(mValue);
    free(mDescription);
    free(mFriends);
    free(mAllow);
    free(mDeny);
}

void Privacy::Copy(Privacy *src) {
    if(src && (src != this)) {
        SetValue(src->mValue);
        SetDescription(src->mDescription);
        SetFriends(src->mFriends);
        SetAllow(src->mAllow);
        SetDeny(src->mDeny);
    }
}

void Privacy::SetValue(const char *str)
{
    if (mValue) {
        free(mValue);
        mValue = NULL;
    }

    if (str && *str != '\0') {
        mValue = strdup(str);
    }
}

void Privacy::SetDescription(const char *str) {
    if(mDescription) {
      free(mDescription);
      mDescription = nullptr;

    }
    if (str && *str != '\0') {
        mDescription = strdup(str);
    }
}

void Privacy::SetFriends(const char *str) {
    if(mFriends) {
      free(mFriends);
      mFriends = nullptr;
    }

    if (str && *str != '\0') {
        mFriends = strdup(str);
    }

}

void Privacy::SetAllow(const char *str) {
    if(mAllow) {
      free(mAllow);
      mAllow = nullptr;
    }

    if (str && *str != '\0') {
        mAllow = strdup(str);
    }
}

void Privacy::SetDeny(const char *str) {
    if(mDeny) {
      free(mDeny);
      mDeny = nullptr;
    }

    if (str && *str != '\0') {
        mDeny = strdup(str);
    }
}

bool Privacy::CompareWithOption(PrivacyOption* option) {
    assert(option);
    const char *description = option->GetDescription();

    bool ret = ((mDescription && description && !strcmp(description, mDescription)) ||
                (!strcmp(option->GetType(), "ALL_FRIENDS") && !strcmp(mValue, "ALL_FRIENDS") && !GetDeny() && !GetAllow()));
    return ret;
}

NewFriend::NewFriend() : mId(nullptr), mName(nullptr), mAvatar(nullptr), mCareer(nullptr),
    mMutualFriends(nullptr), mWork(nullptr),  mEducation(nullptr), mCover(nullptr)
{
}

NewFriend::~NewFriend() {
    free ((char*) mId);
    free ((char*) mName);
    free ((char*) mAvatar);
    free ((char*) mCareer);
    free ((char*) mMutualFriends);
    free ((char*) mWork);
    free ((char*) mEducation);
    free ((char*) mCover);
}

ApplicationField::ApplicationField()
{
    mAppLink = nullptr;
    mAppName = nullptr;
    mAppId = nullptr;
}

ApplicationField::~ApplicationField()
{
    free((void *) mAppLink);
    free((void *) mAppName);
    free((void *) mAppId);
}

Via::Via()
{
    mName = nullptr;
    mId = nullptr;
}

Via::~Via()
{
    free((void *) mName);
    free((void *) mId);
}

Post::Post(JsonObject *object) {
    Init();
    assert(object);
    SetId( GetStringFromJson(object, "id") );
    mCaption = GetStringFromJson(object, "caption");
    mDescription = GetStringFromJson(object, "description");
    mFullPicture = GetStringFromJson(object, "full_picture");
    mLink = GetStringFromJson(object, "link");
    mMessage = GetStringFromJson(object, "message");
    mName = GetStringFromJson(object, "name");
    mObjectId = GetStringFromJson(object, "object_id");
    mParentId = GetStringFromJson(object, "parent_id");
    mSource = GetStringFromJson(object, "source");
    mStatusType = GetStringFromJson(object, "status_type");
    mStory = GetStringFromJson(object, "story");
    mType = GetStringFromJson(object, "type");

    JsonObject *edit_actions = json_object_get_object_member(object, "edit_actions");
    mEdited = edit_actions;

    char *createdTime = GetStringFromJson(object, "created_time");
    mCreatedTime = Utils::ParseTimeToMilliseconds(createdTime);

    char *updatedTime = GetStringFromJson(object, "updated_time");
    if (createdTime && updatedTime && !strcmp(createdTime, updatedTime)) {
        mUpdatedTime = mCreatedTime;
    } else {
        mUpdatedTime = Utils::ParseTimeToMilliseconds(updatedTime);
    }
    free (createdTime);
    free (updatedTime);

    JsonObject *from = json_object_get_object_member(object, "from");
    if (from) {
        mFromName = GetStringFromJson(from, "name");
        mFromId = GetStringFromJson(from, "id");
        from = json_object_get_object_member(from, "picture");
        from = json_object_get_object_member(from, "data");
        mFromPictureLink = GetStringFromJson(from, "url");
    }

    JsonObject *toFriend = json_object_get_object_member(object, "to");
    if (toFriend) {
        JsonArray *to_data_list = json_object_get_array_member(toFriend, "data");
        JsonObject *to_data = json_array_get_object_element(to_data_list, 0);
        mToName = GetStringFromJson(to_data, "name");
        mToId = GetStringFromJson(to_data, "id");
        mToProfileType = GetStringFromJson(to_data, "profile_type");
        if (mToProfileType) {
            mIsGroupPost = !strcmp(mToProfileType, "group") && IsTrulyGroupPost();
        }
    } else {
        mToName = NULL;
    }

    JsonObject *place = json_object_get_object_member(object, "place");
    if (place) {
        mPlace = ParsePlace(place);
        mLongitude = mPlace->mLongitude;
        mLatitude = mPlace->mLatitude;
        mCheckinsNumber = mPlace->mCheckins;

        JsonArray *category_list = json_object_get_array_member(place, "category_list");
        JsonObject *category = json_array_get_object_element(category_list, 0);
        mLocationType = GetStringFromJson(category, "name");
    } else {
        mPlace = nullptr;
    }

    JsonObject *implicitPlace = json_object_get_object_member(object, "implicit_place");
    if (implicitPlace) {
        mImplicitPlace = ParsePlace(implicitPlace);
    }

    mPlacePictureLink = ParcePlacePicture(place);

    JsonObject *privacy = json_object_get_object_member(object, "privacy");
    if (privacy) {
        mPrivacy = new Privacy();
        mPrivacy->SetValue(json_object_get_string_member(privacy, "value"));
        mPrivacy->SetDescription(json_object_get_string_member(privacy, "description"));
        mPrivacy->SetFriends(json_object_get_string_member(privacy, "friends"));
        mPrivacy->SetAllow(json_object_get_string_member(privacy, "allow"));
        mPrivacy->SetDeny(json_object_get_string_member(privacy, "deny"));
    } else {
        mPrivacy = nullptr;
    }

    JsonObject *application = json_object_get_object_member(object, "application");
    if (application) {
        mApplication = new ApplicationField();
        mApplication->mAppLink = GetStringFromJson(application, "link");
        mApplication->mAppName = GetStringFromJson(application, "name");
        mApplication->mAppId = GetStringFromJson(application, "id");
    } else {
        mApplication = nullptr;
    }

    JsonObject *via = json_object_get_object_member(object, "via");
    if (via) {
        mVia = new Via();
        mVia->mName = GetStringFromJson(via, "name");
        mVia->mId = GetStringFromJson(via, "id");
    } else {
        mVia = nullptr;
    }

    mActionsAmount = 0;

    JsonObject *likes = json_object_get_object_member(object, "likes");
    if (likes) {
        JsonObject * summary = json_object_get_object_member(likes, "summary");
        if (summary) {
            mLikesCount = json_object_get_int_member(summary, "total_count");
            mCanLike = json_object_get_boolean_member(summary, "can_like");
            mHasLiked = json_object_get_boolean_member(summary, "has_liked");
            if (mCanLike) {
                ++mActionsAmount;
            }
        }
        JsonArray *likesData = json_object_get_array_member(likes, "data");
        if (likesData) {
            for (int i = 0; i < 2; ++i) {
                JsonObject *person = json_array_get_object_element(likesData, i);
                if (person) {
                    c_unique_ptr<char> id(GetStringFromJson(person, "id"));
                    if (id && !Utils::IsMe(id.get())) {
                        mLikeFromId = SAFE_STRDUP(id.get());
                        mLikeFromName = GetStringFromJson(person, "name");
                        break;
                    }
                }
            }
        }
    }

    JsonObject *comments = json_object_get_object_member(object, "comments");
    if (comments) {
        comments = json_object_get_object_member(comments, "summary");
        if (comments) {
            mCommentsCount = json_object_get_int_member(comments, "total_count");
            mCanComment = json_object_get_boolean_member(comments, "can_comment");
            if (mCanComment) {
                ++mActionsAmount;
            }
        }
    }

    JsonArray *actionsArray = json_object_get_array_member(object, "actions");
    if (actionsArray) {
        int length = json_array_get_length(actionsArray);
        for (int i = 0; i < length; ++i) {
            JsonObject *action = json_array_get_object_element(actionsArray, i);
            if(action) {
                c_unique_ptr<char> actionName(GetStringFromJson(action, "name"));
                if (actionName && !strcmp(actionName.get(), "Share")) {
                    mCanShare = true;
                    ++mActionsAmount;
                    break;
                }
            }
        }
    }

    Eina_List *withTagsList = nullptr;
    JsonArray *withTagsArray = nullptr;
    //Parser for the expected "with_tags" format - if it fails, there is an alternative variant below.
    JsonObject *withTags = json_object_get_object_member(object, "with_tags");
    if (withTags) {
        withTagsArray = json_object_get_array_member(withTags, "data");
        if (withTagsArray) {
            mWithTagsCount = json_array_get_length(withTagsArray);
            for (unsigned int i = 0u; i < mWithTagsCount; ++i) {
                JsonObject* tag = json_array_get_object_element(withTagsArray, i);
                if(!tag) continue;
                const char* id = json_object_get_string_member(tag, "id");
                const char* name = json_object_get_string_member(tag, "name");
                if (i == 0) {
                    mWithTagsId = SAFE_STRDUP(id);
                    mWithTagsName = SAFE_STRDUP(name);
                }
                withTagsList = eina_list_append(withTagsList, new Tag(id ? id : "",
                                                        name ? name : "",
                                                        "with_tags",
                                                        0,
                                                        0));
            }
        }
    } else {
        //Parser for an alternative "with_tags" format - Facebook utilized it some time in place of the old format above.
        withTagsArray = json_object_get_array_member(object, "with_tags");
        if (withTagsArray) {
            mWithTagsCount = json_array_get_length(withTagsArray);
            for (unsigned int i = 0; i < mWithTagsCount; ++i) {
                gint64 tagId = json_array_get_int_element(withTagsArray, i);
                if(!tagId) continue;
                char tmpId[25];
                Utils::Snprintf_s(tmpId, 25, "%lld", tagId);
                if (i == 0) {
                    mWithTagsId = SAFE_STRDUP(tmpId);
                    //This is a workaround because in the new "with_tags" format there is no Name field. So, we have only id and try to find name in providers. This works not for all Ids.
                    //This is used when there is no story and we generate story manually. E.g. for groups' feeds.
                    Friend *fr = dynamic_cast<Friend *> (Utils::FindGraphObjectInProvidersById(mWithTagsId));
                    if (fr) {
                        mWithTagsName = SAFE_STRDUP(fr->GetName().c_str());
                    }
                }
                withTagsList = eina_list_append(withTagsList, new Tag(tmpId, "", "with_tags", 0, 0));
            }
        }
    }

    mPhotoList = nullptr;
    mSubattachmentList = nullptr;
    JsonObject *attachments = json_object_get_object_member(object, "attachments");
    if (attachments) {
        JsonArray *attachments_array = json_object_get_array_member(attachments, "data");
        if(attachments_array) {
            JsonObject *attachmentsDataObject = json_array_get_object_element(attachments_array, 0);
            if (attachmentsDataObject) {
                mAttachmentDescription = GetStringFromJson(attachmentsDataObject, "description");
                mAttachmentTitle = GetStringFromJson(attachmentsDataObject, "title");
                mAttachmentType = GetStringFromJson(attachmentsDataObject, "type");
    // picking up image info
                JsonObject * mediaImage = json_object_get_object_member( attachmentsDataObject, "media");
                if (mediaImage) {
                    mediaImage = json_object_get_object_member(mediaImage, "image");
                    if (mediaImage) {
                        mAttachmentSrc = GetStringFromJson(mediaImage, "src");
                        mAttachmentHeight = json_object_get_int_member(mediaImage, "height");
                        mAttachmentWidth = json_object_get_int_member(mediaImage, "width");
                    }
                }
    // picking up title of page and its id
                JsonObject *target = json_object_get_object_member( attachmentsDataObject, "target");
                if (target) {
                    mAttachTargetId = GetStringFromJson(target, "id");
                    mAttachTargetUrl = GetStringFromJson(target, "url");
                }
            }
            JsonObject *subAttachments = json_array_get_object_element(attachments_array, 0);
            if(subAttachments) {
                subAttachments = json_object_get_object_member(subAttachments, "subattachments");
                if (subAttachments) {
                    attachments_array = json_object_get_array_member(subAttachments, "data");
                }
            }
        }
        if (attachments_array) {
            mPhotoCount = json_array_get_length(attachments_array);

            Photo *photo = nullptr;
            bool isVideoPost = false;
            for (int i = 0; i < mPhotoCount; ++i) {
                JsonObject *mediaAndTarget = json_array_get_object_element(attachments_array, i);
                if (mediaAndTarget) {
                    Tag *subattachment_tag = new Tag();
                    const char* type = json_object_get_string_member(mediaAndTarget, "type");
                    subattachment_tag->mType = type ? type : "";
                    const char* name = json_object_get_string_member(mediaAndTarget, "title");
                    subattachment_tag->mName = name ? name : "";
                    mSubattachmentList = eina_list_append(mSubattachmentList, subattachment_tag);
                    JsonObject *media = json_object_get_object_member(mediaAndTarget, "media");
                    isVideoPost = type && !strcmp(type, "video_inline");
                    if (media && type && strcmp(type, "map")) {
                        JsonObject *image = json_object_get_object_member(media, "image");
                        if (image) {
                            photo = new Photo();
                            photo->SetHeight(json_object_get_int_member(image, "height"));
                            photo->SetWidth(json_object_get_int_member(image, "width"));
                            photo->SetSrcUrl(GetStringFromJson(image, "src"));
                            JsonObject *target = json_object_get_object_member(mediaAndTarget, "target");
                            if (target) {
                                photo->SetId(GetStringFromJson(target, "id"));
                            }
                            const char* description = json_object_get_string_member(mediaAndTarget, "description");
                            photo->SetName(description);
                            JsonArray *descriptionTags = json_object_get_array_member(mediaAndTarget, "description_tags");
                            if(descriptionTags) {
                                Eina_List *tags = nullptr;
                                Tag::tags_json_array_to_eina_list(descriptionTags, &tags);
                                photo->SetPhotoMessageTagsList(tags);
                                eina_list_free(tags);
                            }
                            mPhotoList = eina_list_append(mPhotoList, photo);
                        }
                    }
                }
            }
            if (!isVideoPost && eina_list_count(mPhotoList) == 1 && !mParentId) { //if there is only one photo in the post then we consider it as a PhotoPost. Also, ignore posts with mParentId because they are shared posts.
                photo->SetPhotoPost(this);
            }
        }
    }

    mMessageTagsList = nullptr;
    JsonObject *tags = json_object_get_object_member(object, "message_tags");
    if (tags) {
        json_object_foreach_member(tags, Tag::parse_each_json_object_cb, &mMessageTagsList);
    }

    mStoryTagsList = nullptr;
    tags = json_object_get_object_member(object, "story_tags");
    if (tags) {
        json_object_foreach_member(tags, Tag::parse_each_json_object_cb, &mStoryTagsList);
    }

    if (mStoryTagsList) {
        Tag *group_tag = static_cast<Tag*>(eina_list_nth(mStoryTagsList, 1));
        if (group_tag && group_tag->mType == "group") {
            mGroupName = elm_entry_utf8_to_markup(group_tag->mName.c_str());
            mGroupId = strdup(group_tag->mId.c_str());
        }
    }

    if (mStory && strstr(mStory, "are now friends.")) { //AAA ToDo: this is a temporary solution while FB isn't answer our question.
        if (mStoryTagsList) {
            Tag *tag = static_cast<Tag*>(eina_list_nth(mStoryTagsList, 1));
            if (tag) {
                if (tag->mId.empty()) {
                    mNewFriend = new NewFriend();
                    mNewFriend->mId = strdup(tag->mId.c_str());
                    //    GetAsync(FacebookSession* session, EFriendWork, NULL, NULL); //AAA Not sure that it may be useful. We need to pass FacebookSession to the constructor.
                }
            }
        }
    }

    if (mPrivacy){
        mPrivacyIcon = GetPrivacyIcon(mPrivacy);
    } else {
        mPrivacyIcon = nullptr;
    }

    mTaggedFriendsList = nullptr;
    if (withTagsList) {
        AppendTaggedFriendsToList(withTagsList);
    }

    if(mPlace){
        mLocationPlace = SAFE_STRDUP(mPlace->mName);
    } else {
        mLocationPlace = nullptr;
    }

    //Update image list
    mImagesList = nullptr;

    if (mFullPicture) {
        ImagesListUpdate(mFullPicture);
    }

    if (mFromPictureLink) {
        ImagesListUpdate(mFromPictureLink);
    }

    if (mPlacePictureLink) {
        ImagesListUpdate(mPlacePictureLink);
    }
}

Post::Post(char **name, char **value)
{
    Init();

    mAutoId = atoi(value[Queries::SelectNPosts::AUTO_ID]);
    SetId( Utils::GetStringOrNull(value[Queries::SelectNPosts::ID]) );
    mCaption = Utils::GetStringOrNull(value[Queries::SelectNPosts::CAPTION]);
    mDescription = Utils::GetStringOrNull(value[Queries::SelectNPosts::DESCRIPTION]);
    mFromName = Utils::GetStringOrNull(value[Queries::SelectNPosts::FROM_NAME]);
    mFromId = Utils::GetStringOrNull(value[Queries::SelectNPosts::FROM_ID]);
    mToName = Utils::GetStringOrNull(value[Queries::SelectNPosts::TO_NAME]);
    mToId = Utils::GetStringOrNull(value[Queries::SelectNPosts::TO_ID]);
    mToProfileType = Utils::GetStringOrNull(value[Queries::SelectNPosts::TO_PROFILE_TYPE]);
    mFromPictureLink = Utils::GetStringOrNull(value[Queries::SelectNPosts::FROM_PICTURE]);
    mFullPicture = Utils::GetStringOrNull(value[Queries::SelectNPosts::FULL_PICTURE]);
    mLink = Utils::GetStringOrNull(value[Queries::SelectNPosts::LINK]);
    mMessage = Utils::GetStringOrNull(value[Queries::SelectNPosts::MESSAGE]);
    mObjectId = Utils::GetStringOrNull(value[Queries::SelectNPosts::OBJECT_ID]);
    mParentId= Utils::GetStringOrNull(value[Queries::SelectNPosts::PARENT_ID]);
    mSource = Utils::GetStringOrNull(value[Queries::SelectNPosts::SOURCE]);
    mStatusType = Utils::GetStringOrNull(value[Queries::SelectNPosts::STATUS_TYPE]);
    mStory = Utils::GetStringOrNull(value[Queries::SelectNPosts::STORY]);
    mType = Utils::GetStringOrNull(value[Queries::SelectNPosts::TYPE]);
    mCreatedTime = atof(value[Queries::SelectNPosts::CREATED_TIME]);
    mUpdatedTime = atof(value[Queries::SelectNPosts::UPDATED_TIME]);
    mEdited = mCreatedTime != mUpdatedTime;
    mLikesCount = atoi(value[Queries::SelectNPosts::LIKES_COUNT]);
    mCanLike = (bool) atoi(value[Queries::SelectNPosts::CAN_LIKE]);
    mHasLiked = (bool) atoi(value[Queries::SelectNPosts::HAS_LIKED]);
    mCommentsCount = atoi(value[Queries::SelectNPosts::COMMENTS_COUNT]);
    mCanComment = (bool) atoi(value[Queries::SelectNPosts::CAN_COMMENT]);
    mCanShare = (bool) atoi(value[Queries::SelectNPosts::CAN_SHARE]);
    mPhotoCount = atoi(value[Queries::SelectNPosts::PHOTO_COUNT]);

    if (mToProfileType) {
        mIsGroupPost = !strcmp(mToProfileType, "group") && IsTrulyGroupPost();
    }

    mActionsAmount = 0;
    if (mCanComment) ++mActionsAmount;
    if (mCanLike) ++mActionsAmount;
    if (mCanShare) ++mActionsAmount;

    //Place
    if (!strcmp(value[Queries::SelectNPosts::PLACE_ID], "NULL")) {
        mPlace = nullptr;
    } else {
        mPlace = new Place();
        mPlace->mCity = Utils::GetStringOrNull(value[Queries::SelectNPosts::PLACE_CITY]);
        mPlace->mCountry = Utils::GetStringOrNull(value[Queries::SelectNPosts::PLACE_COUNTRY]);
        mPlace->mId = Utils::GetStringOrNull(value[Queries::SelectNPosts::PLACE_ID]);
        mPlace->mName = Utils::GetStringOrNull(value[Queries::SelectNPosts::PLACE_NAME]);
        mPlace->mStreet = Utils::GetStringOrNull(value[Queries::SelectNPosts::PLACE_STREET]);
        mPlace->mLatitude = atof(value[Queries::SelectNPosts::PLACE_LATITUDE]);
        mPlace->mLongitude = atof(value[Queries::SelectNPosts::PLACE_LONGITUDE]);
        mPlace->mCheckins = atoi(value[Queries::SelectNPosts::PLACE_CHECKINS]);
    }


    //ImplicitPlace
    if (!strcmp(value[Queries::SelectNPosts::IMPLICIT_PLACE_ID], "NULL")) {
        mImplicitPlace = nullptr;
    }
    else {
        mImplicitPlace = new Place();
        mImplicitPlace->mCity = Utils::GetStringOrNull(value[Queries::SelectNPosts::IMPLICIT_PLACE_CITY]);
        mImplicitPlace->mCountry = Utils::GetStringOrNull(value[Queries::SelectNPosts::IMPLICIT_PLACE_COUNTRY]);
        mImplicitPlace->mId = Utils::GetStringOrNull(value[Queries::SelectNPosts::IMPLICIT_PLACE_ID]);
        mImplicitPlace->mName = Utils::GetStringOrNull(value[Queries::SelectNPosts::IMPLICIT_PLACE_NAME]);
        mImplicitPlace->mStreet = Utils::GetStringOrNull(value[Queries::SelectNPosts::IMPLICIT_PLACE_STREET]);
        mImplicitPlace->mLatitude = atof(value[Queries::SelectNPosts::IMPLICIT_PLACE_LATITUDE]);
        mImplicitPlace->mLongitude = atof(value[Queries::SelectNPosts::IMPLICIT_PLACE_LONGITUDE]);
    }

    //Privacy
    if (!strcmp(value[Queries::SelectNPosts::PRIVACY_VALUE], "NULL")) {
        mPrivacy = NULL;
    }
    else {
        mPrivacy = new Privacy();
        char *val = Utils::GetStringOrNull(value[Queries::SelectNPosts::PRIVACY_ALLOW]);
        mPrivacy->SetAllow(val);
        free(val);
        val = Utils::GetStringOrNull(value[Queries::SelectNPosts::PRIVACY_DENY]);
        mPrivacy->SetDeny(val);
        free(val);
        val = Utils::GetStringOrNull(value[Queries::SelectNPosts::PRIVACY_DESCRIPTION]);
        mPrivacy->SetDescription(val);
        free(val);
        val = Utils::GetStringOrNull(value[Queries::SelectNPosts::PRIVACY_FRIENDS]);
        mPrivacy->SetFriends(val);
        free(val);
        val = Utils::GetStringOrNull(value[Queries::SelectNPosts::PRIVACY_VALUE]);
        mPrivacy->SetValue(val);
        free(val);
    }

    if(mPrivacy){
        mPrivacyIcon = GetPrivacyIcon(mPrivacy);
    } else {
        mPrivacyIcon = nullptr;
    }

    JsonObject *object = nullptr;
    JsonParser *parser = nullptr;

    //Message tags
    mMessageTagsList = nullptr;
    if (strcmp(value[Queries::SelectNPosts::MESSAGE_TAGS], "NULL")) {
        parser = openJsonParser(value[Queries::SelectNPosts::MESSAGE_TAGS], &object);
        if (object) {
            object = json_object_get_object_member(object, "tags");
            if(object) json_object_foreach_member(object, Tag::parse_each_json_object_cb, &mMessageTagsList);
        }
        //Attention!! parser object should be unreferenced after parsing is completed
        g_object_unref(parser);
    }

    //Story tags
    mStoryTagsList = nullptr;
    if (strcmp(value[Queries::SelectNPosts::STORY_TAGS], "NULL")) {
        parser = openJsonParser(value[Queries::SelectNPosts::STORY_TAGS], &object);
        if (object) {
            object = json_object_get_object_member(object, "tags");
            if(object) json_object_foreach_member(object, Tag::parse_each_json_object_cb, &mStoryTagsList);
        }
        //Attention!! parser object should be unreferenced after parsing is completed
        g_object_unref(parser);
    }

    mTaggedFriendsList = nullptr;
    if (mStoryTagsList) {
        AppendTaggedFriendsToList(mStoryTagsList);
    }


    if (mPlace) {
        mLocationPlace = SAFE_STRDUP(mPlace->mName);
    } else {
        mLocationPlace = nullptr;
    }

    mAddedSthg = Utils::GetStringOrNull(value[Queries::SelectNPosts::ADDED_STHG]);
    mChildId = Utils::GetStringOrNull(value[Queries::SelectNPosts::CHILD_ID]);
    mComment = Utils::GetStringOrNull(value[Queries::SelectNPosts::COMMENT]);
    mCommunityAvatar = Utils::GetStringOrNull(value[Queries::SelectNPosts::COMMUTITY_AVATAR]);
    mCommunityName = Utils::GetStringOrNull(value[Queries::SelectNPosts::COMMUTITY_NAME]);
    mCommunitySubscribers = Utils::GetStringOrNull(value[Queries::SelectNPosts::COMMUTITY_SUBSCRIBERS]);
    mCommunityType = Utils::GetStringOrNull(value[Queries::SelectNPosts::COMMUTITY_TYPE]);
    mCommunityWallpaper = Utils::GetStringOrNull(value[Queries::SelectNPosts::COMMUTITY_WALLPAPER]);
    mEventCover = Utils::GetStringOrNull(value[Queries::SelectNPosts::EVENT_COVER]);
    mEventInfo = Utils::GetStringOrNull(value[Queries::SelectNPosts::EVENT_INFO]);
    mFromPicturePath = Utils::GetStringOrNull(value[Queries::SelectNPosts::FROM_PICTURE_PATH]);
    mLocationAvatarPath = Utils::GetStringOrNull(value[Queries::SelectNPosts::LOCATION_AVATAR_PATH]);
    mLocationType = Utils::GetStringOrNull(value[Queries::SelectNPosts::PLACE_TYPE]);
    mLocationVisitors = Utils::GetStringOrNull(value[Queries::SelectNPosts::PLACE_VISITORS]);
    mName = Utils::GetStringOrNull(value[Queries::SelectNPosts::NAME]);
    mPlacePictureLink = Utils::GetStringOrNull(value[Queries::SelectNPosts::PLACE_PICTURE]);
    mSongArtist = Utils::GetStringOrNull(value[Queries::SelectNPosts::SONG_ARTIST]);
    mSongName = Utils::GetStringOrNull(value[Queries::SelectNPosts::SONG_NAME]);
    mGroupName = Utils::GetStringOrNull(value[Queries::SelectNPosts::GROUP_NAME]);
    mGroupId = Utils::GetStringOrNull(value[Queries::SelectNPosts::GROUP_ID]);
    mLikeFromName = Utils::GetStringOrNull(value[Queries::SelectNPosts::LIKE_FROM_NAME]);
    mLikeFromId = Utils::GetStringOrNull(value[Queries::SelectNPosts::LIKE_FROM_ID]);
    mAttachmentDescription = Utils::GetStringOrNull(value[Queries::SelectNPosts::ATTACHMENT_DESCRIPTION]);
    mAttachmentSrc = Utils::GetStringOrNull(value[Queries::SelectNPosts::ATTACHMENT_SRC]);
    mAttachmentHeight = atoi(value[Queries::SelectNPosts::ATTACHMENT_HEIGHT]);
    mAttachmentWidth = atoi(value[Queries::SelectNPosts::ATTACHMENT_WIDTH]);
    mAttachTargetId = Utils::GetStringOrNull(value[Queries::SelectNPosts::ATTACHMENT_TARGET_ID]);
    mAttachTargetUrl = Utils::GetStringOrNull(value[Queries::SelectNPosts::ATTACHMENT_TARGET_URL]);
    mAttachmentTitle = Utils::GetStringOrNull(value[Queries::SelectNPosts::ATTACHMENT_TARGET_TITLE]);

    mWithTagsCount = atoi(value[Queries::SelectNPosts::WITH_TAGS_COUNT]);
    mWithTagsId = Utils::GetStringOrNull(value[Queries::SelectNPosts::WITH_TAGS_ID]);
    mWithTagsName = Utils::GetStringOrNull(value[Queries::SelectNPosts::WITH_TAGS_NAME]);

    //New Friend
    if (!strcmp(value[Queries::SelectNPosts::NEW_FRIEND_ID], "NULL")) {
        mNewFriend = nullptr;
    }
    else {
        mNewFriend = new NewFriend();
        mNewFriend->mId = Utils::GetStringOrNull(value[Queries::SelectNPosts::NEW_FRIEND_ID]);
        mNewFriend->mAvatar = Utils::GetStringOrNull(value[Queries::SelectNPosts::NEW_FRIEND_AVATAR]);
        mNewFriend->mCareer = Utils::GetStringOrNull(value[Queries::SelectNPosts::NEW_FRIEND_CAREER]);
        mNewFriend->mMutualFriends = Utils::GetStringOrNull(value[Queries::SelectNPosts::NEW_FRIEND_MUTAL_FRIENDS]);
        mNewFriend->mWork = Utils::GetStringOrNull(value[Queries::SelectNPosts::NEW_FRIEND_WORK]);
        mNewFriend->mEducation = Utils::GetStringOrNull(value[Queries::SelectNPosts::NEW_FRIEND_EDUCATION]);
        mNewFriend->mCover = Utils::GetStringOrNull(value[Queries::SelectNPosts::NEW_FRIEND_COVER]);
    }

    if (!strcmp(value[Queries::SelectNPosts::VIA_ID], "NULL")) {
        mVia = nullptr;
    }
    else {
        mVia = new Via();
        mVia->mId = Utils::GetStringOrNull(value[Queries::SelectNPosts::VIA_ID]);
        mVia->mName = Utils::GetStringOrNull(value[Queries::SelectNPosts::VIA_NAME]);
    }
}

void Post::Init() {
    mGraphObjectType = GraphObject::GOT_POST; // indicate that type of object is Post
    mChildId = nullptr;
    mParentPost = nullptr;

    mApplication = nullptr;
    mCaption = nullptr;
    mDescription = nullptr;
    mEdited = false;
    mEditText = nullptr;
    mEditTime = nullptr;
    mEventCover = nullptr;
    mEventInfo = nullptr;
    mToName = nullptr;
    mToId = nullptr;
    mToProfileType = nullptr;
    mFromName = nullptr;
    mFromId = nullptr;
    mFromPictureLink = nullptr;
    mPlacePictureLink = nullptr;
    mFromPicturePath = nullptr;
    mFullPicture = nullptr;
    mFullPicturePath = nullptr;
    mLink = nullptr;
    mMessage = nullptr;
    mMessageTagsList = nullptr;
    mObjectId = nullptr;
    mParentId = nullptr;
    mPlace = nullptr;
    mImplicitPlace = nullptr;
    mPrivacy = nullptr;
    mSource = nullptr;
    mStatusType = nullptr;
    mStory = nullptr;
    mStoryTagsList = nullptr;
    mType = nullptr;
    mCreatedTime = 0;
    mUpdatedTime = 0;
    mLikesCount = 0;
    mCanLike = false;
    mHasLiked = false;
    mCommentsCount = 0;
    mCanComment = false;
    mCanShare = false;
    mPhotoCount = 0;
    mImagesCount = 0;
    mAutoId = 0;
    mLongitude = 0.0;
    mCheckinsNumber = 0;
    mLatitude = 0.0;
    mPhotoList = nullptr;
    mImagesList = nullptr;
    mTaggedFriendsList = nullptr;
    mSubattachmentList = nullptr;
    mPrivacyIcon = nullptr;
    mLocationAvatarPath = nullptr;

    mIsFriendInfoRequested = false;
    mIsEventInfoRequested = false;
    mIsGroupInfoRequested = false;

    mSongName = nullptr;
    mSongArtist = nullptr;

    mCommunityName = nullptr;
    mCommunityAvatar = nullptr;
    mCommunityType = nullptr;
    mCommunitySubscribers = nullptr;
    mCommunityWallpaper = nullptr;

    mComment = nullptr;

    mLocationPlace = nullptr;
    mLocationType = nullptr;
    mLocationVisitors = nullptr;

    mAddedSthg = nullptr;

    mName = nullptr;
    mNewFriend = nullptr;

    mGroupName = nullptr;
    mGroupId = nullptr;
    mGroupCover = nullptr;

    mVia = nullptr;
    mIsSharedPost = false;

    mLikeFromName = nullptr;
    mLikeFromId = nullptr;

    mAttachmentDescription = nullptr;
    mAttachmentSrc = nullptr;
    mAttachmentHeight = 0;
    mAttachmentWidth = 0;
    mAttachTargetId = nullptr;
    mAttachTargetUrl = nullptr;
    mAttachmentTitle = nullptr;
    mAttachmentType = nullptr;
    mAlbum = nullptr;

    mWithTagsCount = 0;
    mWithTagsId = nullptr;
    mWithTagsName = nullptr;

    mIsGroupPost = false;
    mProfilePictureStory = nullptr;

    mPostRelatedTo = EUnKnown;
}

Post::~Post()
{
    if(mParentPost) {
        mParentPost->Release();
    }
    // free can take NULL value as argument
    free((void *) mChildId);
    free((void *) mCaption);
    free((void *) mDescription);
    free((void *) mEventCover);
    free((void *) mEventInfo);
    free((void *) mToName);
    free((void *) mToId);
    free((void *) mToProfileType);
    free((void *) mFromName);
    free((void *) mFromId);
    free((void *) mFromPictureLink);
    free((void *) mPlacePictureLink);
    free((void *) mFromPicturePath);
    free((void *) mFullPicture);
    free((void *) mFullPicturePath);
    free((void *) mLink);
    free((void *) mMessage);
    free((void *) mName);
    free((void *) mObjectId);
    free((void *) mParentId);
    free((void *) mEditText);
    free((void *) mEditTime);
    delete mApplication;
    delete mPlace;
    delete mImplicitPlace;
    delete mPrivacy;
    delete mVia;
    free((void *) mSource);
    free((void *) mStatusType);
    free((void *) mStory);
    free((void *) mType);
    free((void *) mGroupName);
    free((void *) mGroupId);
    free((void *) mGroupCover);
    free((void *) mLikeFromName);
    free((void *) mLikeFromId);

    free((void *) mAttachmentDescription );
    free((void *) mAttachmentSrc);
    free((void *) mAttachTargetId );
    free((void *) mAttachTargetUrl );
    free((void *) mAttachmentTitle);
    free((void *) mAttachmentType);
    free((void *) mProfilePictureStory);

    void * listData;
    EINA_LIST_FREE( mMessageTagsList, listData ) {
        delete static_cast<Tag*>(listData);
    }

    EINA_LIST_FREE( mStoryTagsList, listData ) {
        delete static_cast<Tag*>(listData);
    }

    EINA_LIST_FREE( mPhotoList, listData ) {
        static_cast<Photo*>(listData)->Release();
    }

    EINA_LIST_FREE(mImagesList, listData) {
        static_cast<Photo*>(listData)->Release();
    }

    CleanTaggedFriends();

    EINA_LIST_FREE( mSubattachmentList, listData ) {
        delete static_cast<Tag*>(listData);
    }

    free((void *) mPrivacyIcon);

    free((void *) mSongName);
    free((void *) mSongArtist);

    free((void *) mCommunityAvatar);
    free((void *) mCommunityName);
    free((void *) mCommunityType);
    free((void *) mCommunitySubscribers);
    free((void *) mCommunityWallpaper);

    free((void *) mComment);

    free((void *) mLocationPlace);
    free((void *) mLocationType);
    free((void *) mLocationVisitors);

    free((void *) mWithTagsId);
    free((void *) mWithTagsName);

    free((void *) mAddedSthg);

    delete mNewFriend;

    if (mAlbum) {
        mAlbum->Release();
    }
}


void Post::copy(Post &srcPost) {
    free((void *) mMessage);
    mMessage = SAFE_STRDUP(srcPost.mMessage);

    void * listData;
    if ( mMessageTagsList ) {
        EINA_LIST_FREE( mMessageTagsList, listData ) {
            delete static_cast<Tag*>(listData);
        }
        mMessageTagsList = nullptr;
    }

    if (srcPost.mMessageTagsList) {
        Eina_List * list_item;
        void * list_data;
        EINA_LIST_FOREACH(srcPost.mMessageTagsList, list_item, list_data) {
            Tag * tag = static_cast<Tag *>(list_data);
            mMessageTagsList = eina_list_append(mMessageTagsList, new Tag(*tag));
        }
    }

    mCanShare = srcPost.mCanShare;
}

char * Post::GetPrivacyIcon(Privacy *privacy)
{
    if (privacy && privacy->GetValue()) {
        if (!strcmp(privacy->GetValue(), PRIVACY_EVERYONE)) {
            return SAFE_STRDUP(ICON_FEED_PRIVACY_DIR "/nf_privacy_public.png");
        } else if (!strcmp(privacy->GetValue(), PRIVACY_ALL_FRIENDS)) {
            return SAFE_STRDUP(ICON_FEED_PRIVACY_DIR "/nf_privacy_friends.png");
        } else if (!strcmp(privacy->GetValue(), PRIVACY_FRIENDS_OF_FRIENDS)) {
            return SAFE_STRDUP(ICON_FEED_PRIVACY_DIR "/nf_privacy_friends.png");
        } else if (!strcmp(privacy->GetValue(), PRIVACY_SELF)) {
            return SAFE_STRDUP(ICON_FEED_PRIVACY_DIR "/nf_privacy_private.png");
        } else if (!strcmp(privacy->GetValue(), PRIVACY_CUSTOM)) {
            if (privacy->GetDeny()) {
                if (!strcmp(privacy->GetFriends(), PRIVACY_ALL_FRIENDS)) {  //for posts with "Friends except..." privacy
                    return SAFE_STRDUP(ICON_FEED_PRIVACY_DIR "/nf_privacy_custom.png");
                } else {
                    return SAFE_STRDUP(ICON_FEED_PRIVACY_DIR "/nf_privacy_close_friends.png");
                }
            } else if (privacy->GetAllow()) {
                if (!strcmp(privacy->GetFriends(), PRIVACY_SOME_FRIENDS)) {  //for posts with "Specific Friends" privacy
                    return SAFE_STRDUP(ICON_FEED_PRIVACY_DIR "/nf_privacy_custom.png");
                } else if (strstr(privacy->GetDescription(), "Area")) {
                      return SAFE_STRDUP(ICON_FEED_PRIVACY_DIR "/nf_privacy_location.png");
                  } else if (!strcmp(privacy->GetDescription(), "Acquaintances")) {
                      return SAFE_STRDUP(ICON_FEED_PRIVACY_DIR "/nf_privacy_aquaintances.png");
                  } else {
                      return SAFE_STRDUP(ICON_FEED_PRIVACY_DIR "/nf_privacy_work.png");
                  }
            } else if (privacy->GetDescription()) {
                if(strstr(privacy->GetDescription(), "friends")) {
                    return SAFE_STRDUP(ICON_FEED_PRIVACY_DIR "/nf_privacy_aquaintances.png");
                }
            } else if (mIsGroupPost) {
                return SAFE_STRDUP(POST_COMPOSER_ICON_DIR "/privacy_group_small.png");
            } else {
                return SAFE_STRDUP(ICON_EVENT_DIR "/ic_private_ivent.png");
            }
        }
    }

    return nullptr;
}

const char* Post::GetNewFriendId() const {
    if (mNewFriend) {
        return mNewFriend->mId;
    }
    return nullptr;
}

const char* Post::GetNewFriendMutualFriends() const {
    if (mNewFriend) {
        return mNewFriend->mMutualFriends;
    }
    return nullptr;
}

const char* Post::GetNewFriendAvatar() const {
    if (mNewFriend) {
        return mNewFriend->mAvatar;
    }
    return nullptr;
}

const char* Post::GetEventInfo() const {
    return mEventInfo;
}

const char* Post::GetEventCover() const {
    return mEventCover;
}

const char* Post::GetNewFriendCareer() const {
    if (mNewFriend) {
        return mNewFriend->mCareer;
    }
    return nullptr;
}

const char* Post::GetNewFriendWork() const {
    if (mNewFriend) {
        return mNewFriend->mWork;
    }
    return nullptr;
}

const char* Post::GetNewFriendEducation() const {
    if (mNewFriend) {
        return mNewFriend->mEducation;
    }
    return nullptr;
}

const char* Post::GetNewFriendCover() const {
    if (mNewFriend) {
        return mNewFriend->mCover;
    }
    return nullptr;
}

void Post::BriefUpdate( double updatedTime, int commentsCount, int likesCount, bool hasLiked, const char * likeFromId, const char * likeFromName )
{
    mUpdatedTime = updatedTime;
    mCommentsCount = commentsCount;
    mLikesCount = likesCount;
    mHasLiked = hasLiked;

    free((void *) mLikeFromId);
    mLikeFromId = SAFE_STRDUP(likeFromId);
    free((void *) mLikeFromName);
    mLikeFromName = SAFE_STRDUP(likeFromName);
}

bool Post::ParseFriendDetailsResponse(char* data, int code)
{
    bool res = false;
    if (code == CURLE_OK) {
        Log::debug("ParseFriendDetailsResponse: data: %s", data);
        FbResponseFriendDetails *friendsDetails = FbResponseFriendDetails::createFromJson(data);
        if (!friendsDetails) {
            Log::error("ParseFriendDetailsResponse:Error parsing http respond");
        } else {
            if (friendsDetails->mError && friendsDetails->mError->mMessage) {
                Log::error("ParseFriendDetailsResponse:Error getting friend details, error = %s", friendsDetails->mError->mMessage);
            } else {
    //AAA ToDo:                CacheManager::GetInstance().SetFriendDetails(friendsDetails);
                if(mNewFriend) {
                    delete mNewFriend;
                    mNewFriend = nullptr;
                }
                mNewFriend = new NewFriend();
                if (!mNewFriend) {
                    mNewFriend->mWork = (friendsDetails->mEmployer) ? strdup(friendsDetails->mEmployer) : NULL;
                    mNewFriend->mEducation = (friendsDetails->mEducation) ? strdup(friendsDetails->mEducation) : NULL;
                    mNewFriend->mAvatar = (friendsDetails->mPictureUrl) ? strdup(friendsDetails->mPictureUrl) : NULL;
                    mNewFriend->mCover = (friendsDetails->mCoverUrl) ? strdup(friendsDetails->mCoverUrl) : NULL;
                }
                mIsFriendInfoRequested = true;
                res = true;
            }
            delete friendsDetails;
        }
    } else {
        Log::error("ParseFriendDetailsResponse: Error sending http request");
    }

    free(data);
    return res;
}

bool Post::ParseEventDetailsResponse(char* data, int code)
{
    bool res = false;
    if (code == CURLE_OK) {
        FbResponseEventDetails * eventsDetails = FbResponseEventDetails::createFromJson(data);

        if (!eventsDetails) {
            Log::debug("ParseEventDetailsResponse: Error parsing http respond");
        } else {
            if (eventsDetails->mError && eventsDetails->mError->mMessage) {
                Log::error("ParseEventDetailsResponse: Error getting friend details, error = %s", eventsDetails->mError->mMessage);
            }
            mEventCover = SAFE_STRDUP(eventsDetails->mCoverUrl);

            if (mEventCover) {
                ImagesListUpdate(mEventCover);
            }

            // Parse an event info. Start time + location + "You are the event host" if it's true
            c_unique_ptr<char> startTime(Utils::PresentTimeFromMillisecondsGeneral(eventsDetails->mStartTime, eventsDetails->mNeedTimeShift));
            std::string eventInfo(startTime.get());
            if (eventsDetails->mLocation) {
                std::unique_ptr<char[]> location(Utils::UTF8TextCutter(eventsDetails->mLocation, 30));
                if (location){
                    eventInfo = eventInfo + "<br/>" + location.get();
                }
            }
            if( Utils::IsMe(eventsDetails->mOwnerId) ) {
                eventInfo = eventInfo + "<br/>" + i18n_get_text("IDS_YOU_ARE_EVENT_HOST");
            }
            mEventInfo = SAFE_STRDUP(eventInfo.c_str());

            delete eventsDetails;
            mIsEventInfoRequested = true;
            res = true;
        }
    } else {
        Log::error("ParseEventDetailsResponse: Error sending http request");
    }

    free(data);
    return res;
}

bool Post::ParseGroupDetailsResponse(char* data, int code)
{
    bool res = false;
    if (code == CURLE_OK) {
        FbResponseEventDetails * groupDetails = FbResponseEventDetails::createFromJson(data);
        if (!groupDetails) {
            Log::error("ParseGroupDetailsResponse: Error parsing http respond");
        } else {
            if (groupDetails->mError && groupDetails->mError->mMessage) {
                Log::error("ParseGroupDetailsResponse: Error getting friend details, error = %s", groupDetails->mError->mMessage);
            }
            mGroupCover = SAFE_STRDUP(groupDetails->mCoverUrl);

            if(mGroupCover) {
                ImagesListUpdate(mGroupCover);
            }

            delete groupDetails;
            mIsGroupInfoRequested = true;
            res = true;
        }
    } else {
        Log::error("ParseGroupDetailsResponse: Error sending http request");
    }
    free(data);
    return res;
}

int Post::GetAsyncRequestTypeForData(int dataType)
{
    switch(dataType) {
    case EFriendWork:
    case EFriendEducation:
    case EFriendCover:
    case EFriendPicture:
        return EFriendDetails;
        break;
    case EEventInfo:
    case EEventCover:
        return EEventDetails;
        break;
    case EGroupCover:
        return EGroupDetails;
        break;
    default:
        break;
    }
    return EUnknownType;
}

void Post::SetPhotoPath(int index, const char* path) {
    Photo *photo = static_cast<Photo *>(eina_list_nth(mPhotoList, index));
    if (photo) {
        photo->SetFilePath(path);
    }
}

const char* Post::GetPhotoPath(int index) const {
    Photo *photo = static_cast<Photo *>(eina_list_nth(mPhotoList, index));
    if (photo) {
        return photo->GetFilePath();
    } else {
        return NULL;
    }
}


bool Post::isDataReady(int dataType, int index) {
    AsyncReqType reqType = static_cast<AsyncReqType>(GetAsyncRequestTypeForData(dataType));
    bool result = false;
    switch(reqType) {
    case EFriendDetails:
        result = mIsFriendInfoRequested;
        break;
    case EEventDetails:
        result = mIsEventInfoRequested;
        break;
    case EGroupDetails:
        result = mIsGroupInfoRequested;
        break;
    default:
        switch(dataType) {
        case EPhotoPath:
            result = GetPhotoPath(index);
            break;
        case EFromPicturePath:
            result = mFromPicturePath;
            break;
        case EFullPicturePath:
            result = mFullPicturePath;
            break;
        case EGroupCover:
            result = mGroupCover;
            break;
        default:
            result = GraphObject::isDataReady(dataType);
            break;
        }
    }
    return result;
}

GraphRequest* Post::SendAsyncRequest( int reqType, void* data, AsyncRequestCallback callback) {
    GraphRequest *res = NULL;
    switch(reqType) {
    case EFriendDetails:
        if (GetNewFriendId()) {
            res = FacebookSession::GetInstance()->GetFriendDetails(GetNewFriendId(), callback, data);
        }
        break;
    case EEventDetails:
        res = FacebookSession::GetInstance()->GetEventDetails(mObjectId, GraphRequest::GET, callback, data);
        break;
    case EGroupDetails:
        res = FacebookSession::GetInstance()->GetGroupDetails(mGroupId, callback, data);
        break;
    default:
        break;
    }
    return res;
}

bool Post::ParseAsyncResponse(int reqType, char* data, int code) {
    bool result = false;
    switch(reqType) {
    case EFriendDetails:
        result = ParseFriendDetailsResponse(data, code);
        break;
    case EEventDetails:
        result = ParseEventDetailsResponse(data, code);
        break;
    case EGroupDetails:
        result = ParseGroupDetailsResponse(data, code);
        break;
    default:
        result = false;
        break;
    }
    return result;
}

const char *Post::GetStringForDataType(int dataType, int index) const {
    const char *result = NULL;
    switch(dataType) {
    case EFriendWork:
        result = GetNewFriendWork();
        break;
    case EFriendEducation:
        result = GetNewFriendEducation();
        break;
    case EFriendCover:
        result = GetNewFriendCover();
        break;
    case EFriendPicture:
        result = GetNewFriendAvatar();
        break;
    case EEventInfo:
        result = GetEventInfo();
        break;
    case EEventCover:
        result = mFullPicture;
        break;
    case EPhotoPath:
        result = GetPhotoPath(index);
        break;
    case EFromPicturePath:
        result = mFromPicturePath;
        break;
    case EFullPicturePath:
        return mFullPicturePath;
        break;
    case EGroupCover:
        result = mGroupCover;
        break;
    default:
        result = nullptr;
        break;
    }
    return result;
}

void Post::SetStringForDataType(const char *str, int dataType, int index) {
    if(str) {
        switch(dataType) {
        case EPhotoPath:
            SetPhotoPath(index, str);
            break;
        case EFromPicturePath:
            mFromPicturePath = SAFE_STRDUP(str);
            break;
        case EFullPicturePath:
            mFullPicturePath = SAFE_STRDUP(str);
            break;
        case EGroupCover:
            mGroupCover = SAFE_STRDUP(str);
            break;
        default:
            break;
        }
    }
}


Post::Place *Post::ParsePlace(JsonObject *object)
{
    Place *place = new Place();
    if (place) {
        place->mId = GetStringFromJson(object, "id");
        place->mName = GetStringFromJson(object, "name");
        place->mCheckins = json_object_get_int_member(object, "checkins");

        JsonObject *location = json_object_get_object_member(object, "location");
        place->mCity = GetStringFromJson(location, "city");
        place->mCountry = GetStringFromJson(location, "country");
        place->mLatitude = json_object_get_double_member(location, "latitude");
        place->mLongitude = json_object_get_double_member(location, "longitude");
        place->mStreet = GetStringFromJson(object, "street");
    }
    return place;
}

const char* Post::ParcePlacePicture (JsonObject *object)
{
    const char *place_picture = nullptr;

    JsonObject *place_pic = json_object_get_object_member(object, "picture");
    if (place_pic) {
        place_pic = json_object_get_object_member( place_pic, "data");
        place_picture = GetStringFromJson( place_pic, "url");
    }

    return place_picture;
}

void Post::AppendTaggedFriendsToList(Eina_List *tags) {
    Eina_List *tagsListIndex;
    void *tagsListData;

    EINA_LIST_FOREACH(tags, tagsListIndex, tagsListData) {
        Tag *tagsData = static_cast<Tag*>(tagsListData);
        if (mFromName && tagsData->mName != mFromName && (tagsData->mType == "user" || tagsData->mType == "with_tags")) {
            Friend *postFriendsData = new Friend();
            postFriendsData->SetId(strdup(tagsData->mId.c_str()));
            postFriendsData->mName = tagsData->mName.c_str();
            mTaggedFriendsList = eina_list_append(mTaggedFriendsList, postFriendsData);
        }
    }
}

void Post::CleanTaggedFriends() {
    void *listData;
    EINA_LIST_FREE(mTaggedFriendsList, listData) {
        static_cast<Friend*>(listData)->Release();
    }
}

void Post::ImagesListUpdate(const char *url)
{
    if(url) {
        Photo *photo = new Photo();
        photo->SetHeight(50); //TODO create GetHight() function
        photo->SetWidth(50); //TODO create GetWhight() function
        photo->SetSrcUrl(url);
//ToDo, code below looks like a workaround because actually image isn't downloaded yet, but we just know its Url.
        c_unique_ptr<char> imagePath(Utils::GetImagePathFromUrl(url));
        photo->SetFilePath(imagePath.get());

        mImagesList = eina_list_append(mImagesList, photo);
        mImagesCount++;
    }
}

void Post::SetChildId(const char *id) {
    free((void *)mChildId);
    mChildId = SAFE_STRDUP(id);
}

void Post::SetParent(Post *post)
{
    if (post && (mParentPost != post)) {
        if (mParentPost && mParentPost->GetHash() != post->GetHash() && strcmp(mParentPost->GetId(), post->GetId()) != 0) {
            Log::info("Post::SetParent: redefinition of ParentPost ");
        }
        if (mParentPost) {
            mParentPost->SetChildId(NULL);
            mParentPost->Release();
            mParentPost = NULL;
        }
        mParentPost = post;
        mParentPost->AddRef();
        post->SetChildId(GetId());
    }
}

void Post::ToggleLike()
{
    if ( mHasLiked ) {
        --mLikesCount;
    }
    else {
        ++mLikesCount;
    }
    mHasLiked = !mHasLiked;
}

void Post::SetMessage(const char *message)
{
    free((void *)mMessage);
    mMessage = SAFE_STRDUP(message);
}

void Post::SetLikeFromName(const char * likeFromName)
{
    free((void *) mLikeFromName);
    mLikeFromName = likeFromName;
}

void Post::SetLikeFromId(const char * likeFromId)
{
    free((void *) mLikeFromId);
    mLikeFromId = likeFromId;
}

bool Post::IsPostWithTarget()
{
    bool result = true;
    if (!mToId) {
        result = false;
    } else if (mToProfileType && !strcmp(mToProfileType, "group") && !mIsGroupPost) {
        result = false;
    } else if (mMessageTagsList) {
        Eina_List * list_item;
        void * list_data;
        EINA_LIST_FOREACH(mMessageTagsList, list_item, list_data) {
            Tag * tag = static_cast<Tag *>(list_data);
            if (tag->mId == mToId) {
                result = false;
                break;
            }
        }
    }
    return result;
}

//PresentableAsPost
bool Post::IsInteractive() {
    return true;
}
//From
std::string Post::GetFromId() {
    return mFromId ? mFromId : "";
}
std::string Post::GetFromName() {
    return mFromName ? mFromName : "";
}
std::string Post::GetFromPicture() {
    return mFromPictureLink ? mFromPictureLink : "";
}
//Via
std::string Post::GetViaId() {
    return (mVia && mVia->mId) ? mVia->mId : "";
}
std::string Post::GetViaName() {
    return (mVia && mVia->mName) ? mVia->mName : "";
}
//With
std::string Post::GetWithTagsId() {
    return mWithTagsId ? mWithTagsId : "";
}
std::string Post::GetWithTagsName() {
    return mWithTagsName ? mWithTagsName : "";
}
unsigned int Post::GetWithTagsCount() {
    return mWithTagsCount;
}
//Location
std::string Post::GetLocationId() {
    return (mPlace && mPlace->mId) ? mPlace->mId : "";
}
std::string Post::GetLocationName() {
    return mLocationPlace ? mLocationPlace : "";
}
std::string Post::GetLocationType() {
    return mLocationType ? mLocationType : "";
}
unsigned int Post::GetCheckinsNumber() {
    return mCheckinsNumber;
}
std::string Post::GetImplicitLocationName() {
    return (mImplicitPlace && mImplicitPlace->mName) ? mImplicitPlace->mName : "";
}
//Content
std::string Post::GetName() {
    return mName ? mName : "";
}
std::string Post::GetStory() {
    const char *story = mProfilePictureStory ? mProfilePictureStory : mStory;
    return story ? story : "";
}

void Post::SetProfilePictureStory(const char *story) {
    free((void *) mProfilePictureStory);
    mProfilePictureStory = SAFE_STRDUP(story);
}

//For feed
std::list<Tag> Post::GetStoryTagsList() {
    Eina_List* list = nullptr;
    void* data = nullptr;
    std::list<Tag> storyTags;
    EINA_LIST_FOREACH(mStoryTagsList, list, data){
        Tag* tag = static_cast<Tag*>(data);
        CHECK_CONT(tag);
        storyTags.push_back(*tag);
    }
    return storyTags;
}
const char *Post::GetMessage() {
    return mMessage;
}

void Post::SetMessageTagsList(std::list<Tag> messageTagsList) {
    void * listData;
    EINA_LIST_FREE(mMessageTagsList, listData) {
        delete static_cast<Tag*>(listData);
    }

    for(auto tagIt = messageTagsList.begin(); tagIt != messageTagsList.end(); tagIt++) {
        mMessageTagsList = eina_list_append(mMessageTagsList, new Tag(*tagIt));
    }
}

std::list<Tag> Post::GetMessageTagsList() {
    Eina_List* list = nullptr;
    void* data = nullptr;
    std::list<Tag> messageTags;
    EINA_LIST_FOREACH(mMessageTagsList, list, data){
        Tag* tag = static_cast<Tag*>(data);
        CHECK_CONT(tag);
        messageTags.push_back(*tag);
    }
    return messageTags;
}

std::string Post::GetCaption() {
    return mCaption ? mCaption : "";
}
unsigned int Post::GetLikesCount() {
    return mLikesCount;
}
unsigned int Post::GetCommentsCount() {
    return mCommentsCount;
}
std::string Post::GetApplicationName() {
    return (mApplication && mApplication->mAppName) ? mApplication->mAppName : "";
}
double Post::GetCreationTime() {
    return mCreatedTime;
}
std::list<Friend> Post::GetPostFriendsList() {
    std::list<Friend> friendList;
    return friendList;
}
bool Post::IsEdited() {
    return mEdited;
}
std::string Post::GetPrivacyIcon() {
    return mPrivacyIcon ? mPrivacyIcon : "";
}
//Attachment
std::string Post::GetAttachmentTitle() {
    return mAttachmentTitle ? mAttachmentTitle : "";
}
std::string Post::GetAttachmentType() {
    return mAttachmentType ? mAttachmentType : "";
}
std::string Post::GetAttachmentTargetUrl() {
    return mAttachTargetUrl ? mAttachTargetUrl : "";
}
std::string Post::GetAttachmentDescription() {
    return mAttachmentDescription ? mAttachmentDescription : "";
}
std::list<Tag> Post::GetSubattachmentList() {
    Eina_List* list = nullptr;
    void* data = nullptr;
    std::list<Tag> subattachments;
    EINA_LIST_FOREACH(mSubattachmentList, list, data){
        Tag* tag = static_cast<Tag*>(data);
        CHECK_CONT(tag);
        subattachments.push_back(*tag);
    }
    return subattachments;
}

bool Post::IsCoverPhotoPost(){
    return (GetAttachmentType() == "cover_photo" && !GetParentId());
}

bool Post::IsProfilePhotoPost(){
    return (GetAlbumPostType() && (!strcmp(GetAlbumPostType(), "profile")));
}

bool Post::IsPhotoPost(){
    return (GetType() && !strcmp(GetType(), "photo") && GetObjectId() && !GetParentId() && !mIsGroupPost);
}

bool Post::IsPostEditable() {
    bool isEditableAlbum = GetAlbumPostType() && (!strcmp(GetAlbumPostType(), "mobile") || !strcmp(GetAlbumPostType(), "wall") || !strcmp(GetAlbumPostType(), "profile") || !strcmp(GetAlbumPostType(), "cover"));

    bool isEditableAddPhotoToAlbum = IsPhotoAddedToAlbum() && GetPhotoListCount() == 1;
    return (isEditableAlbum || isEditableAddPhotoToAlbum);
}

bool Post::IsPhotoAddedToAlbum() {
    return GetAlbumPostType() && !strcmp(GetAlbumPostType(), "normal") && IsPhotoPost();
}

bool Post::IsTrulyGroupPost() {
//workaround for G316. This solution works for a story in English.
//If the story is in another language, the solution will not work. We can resolve
//the problem with using API v2.11, but we don't use it now because there
//is a risk of breaking down the NF.
    return (!mParentId || GetStory().empty() || GetStory().find("to the group") != std::string::npos);
}

bool Post::IsSharedLink() const {
    return (!GetParent() && GetType() && !strcmp(GetType(), "link") && GetStatusType() && !strcmp(GetStatusType(), "shared_story"));
}

bool Post::IsSharedGroup() const {
    return (!GetParent() && mAttachmentType && !strcmp(mAttachmentType, "group"));
}

void Post::CopyLikesSummary(Post &sourcePost) {
    mLikesCount = sourcePost.GetLikesCount();
    mCanLike  = sourcePost.GetCanLike();
    mHasLiked = sourcePost.GetHasLiked();
}

void Post::SetStoryTagsList(std::list<Tag> storyTags) {
    void * listData;
    EINA_LIST_FREE( mStoryTagsList, listData ) {
        delete static_cast<Tag*>(listData);
    }
    for (auto tagIt = storyTags.begin(); tagIt != storyTags.end(); tagIt++) {
        mStoryTagsList = eina_list_append(mStoryTagsList, new Tag(*tagIt));
    }
}
