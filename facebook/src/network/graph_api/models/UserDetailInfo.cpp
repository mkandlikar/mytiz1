#include <dlog.h>
#include <json-glib/json-glib.h>
#include <string.h>

#include "AbstractDataProvider.h"
#include "Common.h"
#include "UserDetailInfo.h"
#include "jsonutilities.h"

/**
 * @brief Construct UserDetailInfo object from JsonObject
 * @param[in] object - Json Object, from which UserDetailInfo will be constructed
 */
UserDetailInfo::UserDetailInfo(JsonObject * object)
{
    Init();
    if(object) {
        const gchar *name = json_object_get_string_member(object, AbstractDataProvider::NAME);
        SetName(name ? name : "");

        const gchar *birthday = json_object_get_string_member(object, AbstractDataProvider::BIRTHDAY);
        SetBirthday(birthday ? birthday : "");

        const gchar *relationshipStatus = json_object_get_string_member(object, AbstractDataProvider::RELATIONSHIP_STATUS);
        SetRelationshipStatus(relationshipStatus ? relationshipStatus : "");

        JsonArray * work = json_object_get_array_member(object, AbstractDataProvider::WORK);
        JsonObject *el = json_array_get_object_element(work, 0);

        if(el) {
            JsonObject *employer = json_object_get_object_member(el, AbstractDataProvider::EMPLOYER);
            const gchar *employerName = json_object_get_string_member(employer, AbstractDataProvider::NAME);
            SetEmployer(employerName ? employerName : "");
            JsonObject *workPicture = json_object_get_object_member(employer, AbstractDataProvider::PICTURE);
            JsonObject *workPictureData = json_object_get_object_member(workPicture, AbstractDataProvider::DATA);
            const gchar *workUrl = json_object_get_string_member(workPictureData, AbstractDataProvider::URL);
            SetWorkIconUrl(workUrl ? workUrl : "");

            JsonObject *position = json_object_get_object_member(el, AbstractDataProvider::POSITION);
            const gchar *positionName = json_object_get_string_member(position, AbstractDataProvider::NAME);
            SetPosition(positionName ? positionName : "");
        }

        JsonObject *location = json_object_get_object_member(object, AbstractDataProvider::LOCATION);
        const gchar *locationName = json_object_get_string_member(location, AbstractDataProvider::NAME);
        SetLocation(locationName ? locationName : "");
        JsonObject *locationPicture = json_object_get_object_member(location, AbstractDataProvider::PICTURE);
        JsonObject *locationPictureData = json_object_get_object_member(locationPicture, AbstractDataProvider::DATA );
        const gchar *locationUrl = json_object_get_string_member(locationPictureData, AbstractDataProvider::URL);
        SetLocationIconUrl(locationUrl ? locationUrl : "");

        JsonArray * education = json_object_get_array_member(object, AbstractDataProvider::EDUCATION);
        el = json_array_get_object_element(education, 0);
        if(el) {
            JsonObject *school = json_object_get_object_member(el, AbstractDataProvider::SCHOOL);
            const gchar *schoolName = json_object_get_string_member(school, AbstractDataProvider::NAME);
            SetEducation(schoolName ? schoolName : "");
            JsonObject *educationPicture = json_object_get_object_member(school, AbstractDataProvider::PICTURE);
            JsonObject *educationPictureData = json_object_get_object_member(educationPicture, AbstractDataProvider::DATA);
            const gchar *educationUrl = json_object_get_string_member(educationPictureData, AbstractDataProvider::URL);
            SetEducationUrl(educationUrl ? educationUrl : "");
        }

        JsonObject *cover = json_object_get_object_member(object, AbstractDataProvider::COVER);
        if(cover) {
            const gchar *source = json_object_get_string_member(cover, AbstractDataProvider::SOURCE);
            SetCoverUrl(source ? source : "");
        }

        JsonObject *picture = json_object_get_object_member(object, AbstractDataProvider::PICTURE);
        if(picture) {
            JsonObject *pictureData = json_object_get_object_member(picture, AbstractDataProvider::DATA);
            const gchar *url = json_object_get_string_member(pictureData, AbstractDataProvider::URL);
            SetPictureUrl(url ? url : "");
        }

        const gchar *id = json_object_get_string_member(object, AbstractDataProvider::ID);
        SetId(id ? strdup(id) : NULL);

        mMutualFriendsCount = 0;
#ifdef FEATURE_MUTUAL_FRIENDS
        JsonObject * context = json_object_get_object_member(object, "context");
        if ( context )
        {
            JsonObject * mutual_friends = json_object_get_object_member(context, "mutual_friends");
            if (mutual_friends != NULL)
            {
                JsonObject * summary = json_object_get_object_member(mutual_friends, "summary");
                if (summary != NULL)
                {
                    mMutualFriendsCount = json_object_get_int_member(summary, "total_count");
                }

                JsonArray *mutualData = json_object_get_array_member(mutual_friends, AbstractDataProvider::DATA);
                if(mutualData) {
                    JsonObject *el = json_array_get_object_element(mutualData, 0);
                    if(el) {
                        JsonObject *picture = json_object_get_object_member(el, AbstractDataProvider::PICTURE);
                        const gchar *name = json_object_get_string_member(el, AbstractDataProvider::NAME);
                        SetFirstMutualFriendName(name ? name : "");
                        JsonObject *pictureData = json_object_get_object_member(picture, AbstractDataProvider::DATA);
                        const gchar *url = json_object_get_string_member(pictureData, AbstractDataProvider::URL);
                        SetMutualFriendAvatar(url ? url : "");

                        if ( mMutualFriendsCount > 1 ) {
                            JsonArray *mutualData = json_object_get_array_member(mutual_friends, AbstractDataProvider::DATA);
                            if(mutualData) {
                                JsonObject *el = json_array_get_object_element(mutualData, 1);
                                if(el) {
                                    const gchar *name = json_object_get_string_member(el, AbstractDataProvider::NAME);
                                    SetSecondMutualFriendName(name ? name : "");
                                }
                            }
                        }
                    }
                }
            }
        }
#endif
    }

    mGraphObjectType = GraphObject::GOT_USERPROFILEDETAIL;

    isClickable = true;
}

/**
 * @brief Destructor
 */
UserDetailInfo::~UserDetailInfo()
{
    free((void*) mName);
    free((void*) mCoverUrl);
    free((void*) mPictureUrl);
    free((void*) mBirthday);
    free((void*) mEducation);
    free((void*) mEducationIconUrl);
    free((void*) mEmployer);
    free((void*) mWorkIconUrl);
    free((void*) mLocation);
    free((void*) mLocationIconUrl);
    free((void*) mPosition);
    free((void*) mRelationshipStatus);
    free((void*) mMutualFriendAvatar);
    free((void*) mFirstMutualFriendName);
    free((void*) mSecondMutualFriendName);
}

void UserDetailInfo::Init() {
    mName = NULL;
    mCoverUrl = NULL;
    mPictureUrl = NULL;
    mBirthday = NULL;
    mEducation = NULL;
    mEducationIconUrl = NULL;
    mEmployer = NULL;
    mWorkIconUrl = NULL;
    mLocation = NULL;
    mLocationIconUrl = NULL;
    mPosition = NULL;
    mRelationshipStatus = NULL;
    mMutualFriendAvatar = NULL;
    mFirstMutualFriendName = NULL;
    mSecondMutualFriendName = NULL;
    mMutualFriendsCount = 0;
}

