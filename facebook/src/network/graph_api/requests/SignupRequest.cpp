#include <stdlib.h>
#include <string.h>

#include "Application.h"
#include "Common.h"
#include "Config.h"
#include "ConnectivityManager.h"
#include "dlog.h"
#include "Ecore.h"
#include "MainScreen.h"
#include "SignupRequest.h"
#include "curlutilities.h"
#include "jsonutilities.h"
#include "Utils.h"

#define BASE_URL "https://graph.facebook.com"
#define ACCESS_TOKEN "access_token="
#define API_KEY "api_key="
#define METHOD "method=POST"
#define FORMAT "format=json"
#define EMAIL "email="
#define FNAME "firstname="
#define LNAME "lastname="
#define PSSWD "password="
#define BDATE "birthday="
#define GENDER "gender="

#define CONFIRM_BASE_URL "https://api.facebook.com"
#define CONFIRM_URL "/method/user.confirmcontactpoint"
#define SENCODE_URL "/method/user.sendconfirmationcode"
#define CONTACT_TYPE "contactpoint_type="
#define NCONTACT_POINT "normalized_contactpoint="
#define CODE "code="
#define SOURCE "source=UNKNOWN"

#define MAX_REQUEST_LEN 2048
/*
typedef struct ThreadDataStruct {
    const char *RequestUrl;
    struct MemoryStruct* RequestPayload;
    struct MemoryStruct* Response;
    void *Data;
    void (*CallBack)(void *Data, char* Response, int CurlCode);
    int CurlCode;
} ThreadData;
*/

SignupRequest::SignupRequest() {
    mIsGenderFemale = false;
    mIsConfirmPending = false;
}//SignupRequest

SignupRequest::~SignupRequest() {
    //TODO
}//~SignupRequest

void SignupRequest::SetData(EDataType DataType, std::string Data) {
    if (Data.empty()) return;
    switch (DataType) {
        case EMAILADDRESS:
            mEmailAddress = Data;
            break;
        case MOBILENUMBER:
            mMobileNumber = Data;
            break;
        case FIRSTNAME:
            mFirstName = Data;
            break;
        case LASTNAME:
            mLastName = Data;
            break;
        case PASSWORD:
            mPassword = Data;
            break;
        case BIRTHDAY:
            mBirthDay = Data;
            break;
        case CONFIRMCODE:
            mConfirmCode = Data;
            break;
        case ISGENDERFEMALE:
            mIsGenderFemale = !Data.compare("true") ? true : false;
            break;
    }
}

std::string SignupRequest::GetData(EDataType DataType) {
    std::string Data;
    switch (DataType) {
    case EMAILADDRESS:
        Data = mEmailAddress;
        break;
    case MOBILENUMBER:
        Data = mMobileNumber;
        break;
    default:
        Data = "";
        break;
    }
    return Data;
}

bool SignupRequest::IsConfirmPending() {
    return mIsConfirmPending;
}

Ecore_Thread *SignupRequest::InitAsync(ERequestType RequestType) {
    char RequestUrl[MAX_REQUEST_LEN] = {0};
    char RequestPayload[MAX_REQUEST_LEN] = {0};
    bool IsEmailContact = false;
    void *Data = NULL;
    void (*ResponseCallBack)(void *Data, char* Response, int CurlCode) = NULL;


    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK,
                "SignUp User Given Data:"
                "Email Addresss(%s), Mobile Number (%s) First Name(%s), Last Name(%s), Password(%s), BirthDay(%s) and Gender(%s)",
                mEmailAddress.c_str(), mMobileNumber.c_str(), mFirstName.c_str(), mLastName.c_str(), mPassword.c_str(),
                mBirthDay.c_str(), mIsGenderFemale ? "Female" : "Male");

    if (!ConnectivityManager::Singleton().IsConnected()) {
        Application::GetInstance()->RemoveTopScreen();
        Utils::ShowToast(Application::GetInstance()->mNf, i18n_get_text("IDS_SUP_IDS_SUP_NOCONNECT_INFO"));
        return NULL;
    }

    if (mEmailAddress.length() > 0) {
        IsEmailContact = true;
    }

    if (SIGNUP == RequestType) {
        Utils::Snprintf_s(RequestUrl, sizeof(RequestUrl), "%s/%s/Users", BASE_URL, Config::GetApplicationId());
        Utils::Snprintf_s(RequestPayload, sizeof(RequestPayload), "%s&%s%s|%s&%s&%s%s&%s%s&%s%s&%s%s&%s%s&%s%s",
                METHOD, ACCESS_TOKEN, Config::GetApplicationId(), Config::GetClientToken(), FORMAT, EMAIL, mEmailAddress.c_str(), FNAME, mFirstName.c_str(),
                LNAME, mLastName.c_str(), PSSWD, mPassword.c_str(), BDATE, mBirthDay.c_str(), GENDER, mIsGenderFemale ? "F" : "M");
        Data = this;
        ResponseCallBack = SignUpCallback;
    } else if (CONFIRM == RequestType) {
        Utils::Snprintf_s(RequestUrl, sizeof(RequestUrl), "%s%s", CONFIRM_BASE_URL, CONFIRM_URL);
        Utils::Snprintf_s(RequestPayload, sizeof(RequestPayload), "%s&%s%s&%s&%s%s&%s%s&%s&%s%s",
                METHOD, API_KEY, Config::GetApplicationId(), FORMAT, CONTACT_TYPE, (IsEmailContact ? "email" : "phone"), NCONTACT_POINT,
                (IsEmailContact ? mEmailAddress.c_str() : mMobileNumber.c_str()), SOURCE, CODE, mConfirmCode.c_str());
        ResponseCallBack = ConfirmCallback;
    } else if (SENDCODE == RequestType) {
        Utils::Snprintf_s(RequestUrl, sizeof(RequestUrl), "%s%s", CONFIRM_BASE_URL, SENCODE_URL);
        Utils::Snprintf_s(RequestPayload, sizeof(RequestPayload), "%s&%s%s&%s&%s%s&%s%s",
                METHOD, API_KEY, Config::GetApplicationId(), FORMAT, CONTACT_TYPE, (IsEmailContact ? "email" : "phone"), NCONTACT_POINT,
                (IsEmailContact ? mEmailAddress.c_str() : mMobileNumber.c_str()));
        ResponseCallBack = SendCodeCallback;
    } else {
        //TODO
    }

    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "SignUp Request Url <%s>\n Request Payload <%s>", RequestUrl, RequestPayload);

    RequestUserData *TData = new RequestUserData(RequestUrl, RequestPayload, ResponseCallBack, Data);

    Ecore_Thread *NewThread = ecore_thread_run(ThreadRunCallback, ThreadEndCallback, ThreadCancelCallback, TData);
    return NewThread;
}

void SignupRequest::ProcessResponse(ERequestType RequestType, const char* Response) {
    JsonObject* JsonRoot = NULL;
    JsonParser* JsonParser = openJsonParser(Response, &JsonRoot);
    if (JsonRoot) {
        const char* ErrMsg = NULL;
        if (SIGNUP == RequestType) {
            JsonObject* JsonObjError = json_object_get_object_member(JsonRoot, "error");
            if (JsonObjError) {
                ErrMsg = json_object_get_string_member(JsonObjError, "message");
            }
        } else if ((CONFIRM == RequestType) || (SENDCODE == RequestType)) {
            ErrMsg = json_object_get_string_member(JsonRoot, "error_msg");
        }
        if (ErrMsg) {
            Utils::ShowToast(Application::GetInstance()->mNf, ErrMsg);
        }
    }
    g_object_unref(JsonParser);
}

void SignupRequest::SignUpCallback(void *Data, char *Response, int CurlCode) {
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "SignUp Response Code: %d, Response: %s", CurlCode, Response);
    SignupRequest *Self = static_cast<SignupRequest *>(Data);
    bool Success = false;
    if (CURLE_OK == CurlCode) {
        std::string ServerResponse = Response;
        if (!ServerResponse.compare("true")) {
            Self->mIsConfirmPending = true;
            Application::GetInstance()->RemoveAllScreensAndNfItemsFromNaviframe();//unused
            ScreenBase *NewScreen = new MainScreen(NULL);
            Application::GetInstance()->AddScreen(NewScreen);
            Success = true;
        }
    }

    if (!Success) {
        Application::GetInstance()->RemoveTopScreen();
        Self->ProcessResponse(SIGNUP, Response);
    }
}

void SignupRequest::ConfirmCallback(void *Data, char *Response, int CurlCode) {
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "SignUp Confirm Response Code: %d, Response: %s", CurlCode, Response);
    SignupRequest *Me = static_cast<SignupRequest *>(Data);
    Me->ProcessResponse(SignupRequest::CONFIRM, Response);
}

void SignupRequest::SendCodeCallback(void* Data, char* Response, int CurlCode) {
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "SignUp Send Code Response Code: %d, Response: %s", CurlCode, Response);
    SignupRequest *Me = static_cast<SignupRequest *>(Data);
    Me->ProcessResponse(SignupRequest::SENDCODE, Response);
}

void SignupRequest::ThreadRunCallback(void *data, Ecore_Thread *thread) {
    RequestUserData *TData = static_cast<RequestUserData *>(data);
    CURLcode CurlCode = SendHttpPost(TData);
    TData->curlCode = CurlCode;
}

void SignupRequest::ThreadEndCallback(void *data, Ecore_Thread *thread) {
    RequestUserData *TData = static_cast<RequestUserData *>(data);

    if (TData->callback) {
        TData->callback(TData->callbackObject, TData->mResponseBuffer->memory, TData->curlCode);
    }
    CleanupThreadData(data);
}

void SignupRequest::ThreadCancelCallback(void *data, Ecore_Thread *thread) {
    CleanupThreadData(data);
}

void SignupRequest::CleanupThreadData(void *data) {
    RequestUserData *TData = static_cast<RequestUserData *>(data);

    delete TData;
}
