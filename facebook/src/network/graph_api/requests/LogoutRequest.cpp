#include "CacheManager.h"
#include "Common.h"
#include "Config.h"
#include "curlutilities.h"
#include "FacebookSession.h"
#include "LoggedInUserData.h"
#include "LoginScreen.h"
#include "LogoutRequest.h"
#include "TrayNotificationsManager.h"
#include "Utils.h"

#include <app_preference.h>
#include <badge.h>

#define URL_LOGOUT "https://api.facebook.com/method/auth.logout"
#define LOGOUT_APP_ID "api_key="
#define LOGOUT_FORMAT "format=JSON"
#define LOGOUT_METHOD "method=auth.logout"
#define LOGOUT_REASON "reason=UserLoggedOut"
#define LOGOUT_SESSION_KEY "session_key="
#define LOGOUT_SIGNATURE "sig="
#define LOGOUT_VERSION "v=1.0"

#define MAX_REQUEST_LEN 2048

LogoutRequest::LogoutRequest(void (*callback)(void *, char*, int), void * callback_object) {
    mCallback = callback;
    mCallbackObject = callback_object;
    mThread = NULL;
    mThreadData = NULL;
}

LogoutRequest::~LogoutRequest() {
}

void LogoutRequest::DoLogout() {
    ExecuteAsync();
}

/**
 * @brief This method run a request to facebook in async thread
 * @return pointer to started async thread
 */
Ecore_Thread * LogoutRequest::ExecuteAsync() {
    Log::debug("LogoutRequest::ExecuteAsync()");
    //build request
    char requestBuf[MAX_REQUEST_LEN];
    char sigBuf[MAX_REQUEST_LEN];
    int len = 0;
    char *sig = NULL;

    memset(requestBuf, 0, sizeof(requestBuf));
    memset(sigBuf, 0, sizeof(sigBuf));

    len = Utils::Snprintf_s(sigBuf, MAX_REQUEST_LEN, "%s%s%s%s%s%s%s%s%s", LOGOUT_APP_ID, Config::GetApplicationId(), LOGOUT_FORMAT, LOGOUT_METHOD,
                  LOGOUT_REASON, LOGOUT_SESSION_KEY, Config::GetInstance().GetSessionKey().c_str(), LOGOUT_VERSION,
                  Config::GetInstance().GetSessionSecret().c_str());
    Log::debug("Signature Input =%s", sigBuf);

    sig = Utils::str2md5(sigBuf, len);
    Log::debug("MD5 HASH = %s", sig);

    Utils::Snprintf_s(requestBuf, MAX_REQUEST_LEN, "%s%s&%s&%s&%s&%s%s&%s%s&%s", LOGOUT_APP_ID, Config::GetApplicationId(), LOGOUT_FORMAT, LOGOUT_METHOD,
            LOGOUT_REASON, LOGOUT_SESSION_KEY, Config::GetInstance().GetSessionKey().c_str(), LOGOUT_SIGNATURE, sig, LOGOUT_VERSION);
    free(sig);
    Log::debug("Full Request Url = %s", requestBuf);

    RequestUserData *ud = new RequestUserData(URL_LOGOUT, requestBuf, mCallback, mCallbackObject);
    mThreadData = ud;

    //Run a thread
    mThread = ecore_thread_run(thread_run_cb, thread_end_cb, thread_cancel_cb, ud);

    return mThread;
}

/**
 * @brief  - This method runs in async thread
 * @param data[in] - user data passed to thread
 * @param thread[in] - thread
 */

void LogoutRequest::thread_run_cb(void *data, Ecore_Thread *thread) {
    RequestUserData *ud = static_cast<RequestUserData *>(data);
    CURLcode code = SendHttpPost(ud);
    ud->curlCode = code;
}

/**
 * @brief - This callback is called when async thread is finished
 * @param data[in] -user data
 * @param thread[in] - thread
 */
void LogoutRequest::thread_end_cb(void *data, Ecore_Thread *thread) {
    //Sice thread_end_cb() is called from main loop thread,
    //we can access EAPI without considering any syncronization.
    RequestUserData *ud = static_cast<RequestUserData *>(data);

    int curlCode = ud->curlCode;
    char * respond = ud->mResponseBuffer->memory;
    void * object = ud->callbackObject;
    RequestUserData_CB callback = ud->callback;
    delete ud;

    if (callback != NULL) {
        callback(object, respond, curlCode);
    }
}

/**
 * @brief This callback is called when async thread should be cancelled
 * @param data[in] - user data
 * @param thread[in] - thread
 */
void LogoutRequest::thread_cancel_cb(void *data, Ecore_Thread *thread) {
    RequestUserData* TData = static_cast<RequestUserData*>(data);

    Log::debug("LogoutRequest: Cleaning cancelled thread...");
    delete TData;
}

void LogoutRequest::Cancel() {
    Log::debug("LogoutRequest: Cancelling in progress thread...");
    if (mThread) {
        if (mThreadData) {
            mThreadData->mIsCancelled = true;
        }
        ecore_thread_cancel(mThread);
        mThread = NULL;
    }
}

void LogoutRequest::ClearAllUserData() {
    Log::debug("LogoutRequest::ClearAllUserData()");

    push_service_deregister(PushService::pushConn, PushServiceCb, NULL);
    PushService::disconnect();

    account_query_account_by_package_name(account_remove_cb, PACKAGE, nullptr);

    // Destroy the Icon Badge Display
    int ret = badge_remove(PACKAGE);
    Log::debug("badge_remove() is %s. Return Value is %d.", (BADGE_ERROR_NONE == ret) ? "Success" : "Failure", ret);
    FacebookSession::GetInstance()->DeleteAllGraphRequests();
    CacheManager::GetInstance().DeleteAccountData(Config::GetInstance().GetUserName().c_str());
    Config::GetInstance().SetAccessToken(NULL);
    Config::GetInstance().SetSessionKey(NULL);
    Config::GetInstance().SetSessionSecret(NULL);
    Application::GetInstance()->RemoveAllScreensAndNfItemsFromNaviframe();
    Application::GetInstance()->EraseAllProviderData();
    TrayNotificationsManager::GetInstance()->DeleteAllNotiNotifications();
    TrayNotificationsManager::GetInstance()->DeleteAllOngoingNotifications();

    LoggedInUserData::GetInstance().FreeData();
}

bool LogoutRequest::account_remove_cb(account_h account, void* data) {
    int account_id = 0;
    int ret = 0;
    ret = account_get_account_id(account, &account_id);
    Log::debug("account_remove_cb :: ACCOUNT ID = %d", account_id);
    if (ret == ACCOUNT_ERROR_NONE) {
        ret = account_delete_from_db_by_id(account_id);
    } else {
        Log::debug("account_delete_from_db_by_id() Failed = %d", ret);
    }
    return false;
}

void LogoutRequest::PushServiceCb(push_service_result_e Result, const char *Message, void *Data) {
}

void LogoutRequest::RedirectToLogin(void* ThreadData) {
    Log::debug("LogoutRequest::RedirectToLogin");
    if (Config::GetInstance().IsLogin()) {
        ClearAllUserData();

        LoginScreen* NewLoginScreen = new LoginScreen(NULL);
        Application::GetInstance()->AddScreen(NewLoginScreen);

        const char* userName = Config::GetInstance().GetUserName().c_str();
        preference_set_string(KEY_USER_LOGIN, userName);
        NewLoginScreen->set_login_field(userName);

        Config::GetInstance().SetUserName("");

        Application::GetInstance()->mDataService->ClearUserData();
    }
}
