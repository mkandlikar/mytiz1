#include "Common.h"
#include "Config.h"
#include "curlutilities.h"
#include "Log.h"
#include "LoginRequest.h"
#include "Utils.h"
#include "WebViewScreen.h"

#include <Ecore.h>
#include <string.h>

#define URL_LOGIN "https://api.facebook.com/method/auth.login"
#define URL_LOGIN_SMS "https://graph.facebook.com/"
#define ACCESS_TOKEN "access_token="
#define APP_ID "api_key="
#define EMAIL "email="
#define FORMAT "format=JSON"
#define METHOD "method=facebook.auth.login"
#define PASSWORD "password="
#define SIGNATURE "sig="
#define VERSION "v=1.0"
#define USERID "userid="
#define FIRSTFACTOR "first_factor="
#define TWOFACTOR "twofactor_code="
#define CREDENTIALSTYPE2FA "credentials_type=two_factor"
#define CREDENTIALSTYPEPSWD "credentials_type=password"
#define TWOFACSMS "twofacsms"

#define MAX_REQUEST_LEN 2048

LoginRequest::LoginRequest(void (*callback)(void *, char*, int),
        void * callback_object, const char *username, const char *password, const char *firstFactor, const char *secondFactor) {
    mUsername = SAFE_STRDUP(username);
    mPassword = SAFE_STRDUP(password);
    mFirstFactorCode = SAFE_STRDUP(firstFactor);
    mSecondFactorCode = SAFE_STRDUP(secondFactor);
    mCallback = callback;
    mCallbackObject = callback_object;
    mThread = NULL;
    mThreadData = NULL;
}

void LoginRequest::DoLogin() {
    ExecuteAsync();
}

LoginRequest::~LoginRequest() {
    // TODO Auto-generated destructor stub
    free(mUsername);
    free(mPassword);
    free(mFirstFactorCode);
    free(mSecondFactorCode);
}

/**
 * @brief This method run a request to facebook in async thread
 * @return pointer to started async thread
 */
Ecore_Thread * LoginRequest::ExecuteAsync() {
    //build request
    char requestBuf[MAX_REQUEST_LEN] = {0,};
    char sigBuf[MAX_REQUEST_LEN] = {0,};
    int len = 0;
    char *sig = NULL;
    RequestUserData *ud = nullptr;

    if (mFirstFactorCode && mSecondFactorCode) {
        len = Utils::Snprintf_s(sigBuf, MAX_REQUEST_LEN, "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s", APP_ID, Config::GetApplicationId(), CREDENTIALSTYPE2FA, EMAIL, mUsername, FIRSTFACTOR, mFirstFactorCode,
                FORMAT, METHOD, PASSWORD, mPassword, TWOFACTOR, mSecondFactorCode, USERID, Config::GetInstance().GetUserId().c_str(), VERSION, Config::GetClientToken());
        sig = Utils::str2md5(sigBuf, len);
        Utils::Snprintf_s(requestBuf, MAX_REQUEST_LEN, "%s%s&%s&%s%s&%s%s&%s&%s&%s%s&%s%s&%s%s&%s%s&%s", APP_ID, Config::GetApplicationId(), CREDENTIALSTYPE2FA, EMAIL, mUsername, FIRSTFACTOR, mFirstFactorCode,
                FORMAT, METHOD, PASSWORD, WebViewScreen::encode_url(mPassword), TWOFACTOR, WebViewScreen::encode_url(mSecondFactorCode),  SIGNATURE, sig, USERID, Config::GetInstance().GetUserId().c_str(), VERSION);
    } else {
		len = Utils::Snprintf_s(sigBuf, MAX_REQUEST_LEN, "%s%s%s%s%s%s%s%s%s%s%s", APP_ID, Config::GetApplicationId(), CREDENTIALSTYPEPSWD, EMAIL, mUsername,
                FORMAT, METHOD, PASSWORD, mPassword, VERSION, Config::GetClientToken());
		sig = Utils::str2md5(sigBuf, len);
		Utils::Snprintf_s(requestBuf, MAX_REQUEST_LEN, "%s%s&%s&%s%s&%s&%s&%s%s&%s%s&%s", APP_ID, Config::GetApplicationId(), CREDENTIALSTYPEPSWD, EMAIL, mUsername,
                FORMAT, METHOD, PASSWORD, WebViewScreen::encode_url(mPassword), SIGNATURE, sig, VERSION);
    }
    ud = new RequestUserData(URL_LOGIN, requestBuf, mCallback, mCallbackObject);
    free (sig);
    mThreadData = ud;
    //Run a thread
    mThread = ecore_thread_run(thread_run_cb, thread_end_cb,
            thread_cancel_cb, ud);
    return mThread;
}

Ecore_Thread * LoginRequest::ResendLoginCodeExecuteAsync() {
    //build request
    char requestBuf[MAX_REQUEST_LEN] = {0,};
    std::string token;
    token.append(Config::GetApplicationId()).append("|").append(Config::GetClientToken());
    Utils::Snprintf_s(requestBuf, MAX_REQUEST_LEN, "%s%s&%s%s", FIRSTFACTOR, mFirstFactorCode, ACCESS_TOKEN, token.c_str());

    std::string path = URL_LOGIN_SMS;
    path.append(Config::GetInstance().GetUserId()).append("/").append(TWOFACSMS);

    mThreadData = new RequestUserData(path.c_str(), requestBuf, mCallback, mCallbackObject);
    //Run a thread
    mThread = ecore_thread_run(thread_run_cb, thread_end_cb, thread_cancel_cb, mThreadData);
    return mThread;
}
/**
 * @brief  - This method runs in async thread
 * @param data[in] - user data passed to thread
 * @param thread[in] - thread
 */

void LoginRequest::thread_run_cb(void *data, Ecore_Thread *thread) {
    RequestUserData *ud = static_cast<RequestUserData *>(data);
    CURLcode code;
    /* if (ud->httpMethod == GET)
     {
     code = SendHttpGet(ud);
     }
     else if (ud->httpMethod == POST)
     {*/
    code = SendHttpPost(ud);
    /* }
     else if (ud->httpMethod == DELETE)
     {
     //TODO: implement sending DELETE request
     }*/
    ud->curlCode = code;
}

/**
 * @brief - This callback is called when async thread is finished
 * @param data[in] -user data
 * @param thread[in] - thread
 */
void LoginRequest::thread_end_cb(void *data, Ecore_Thread *thread) {
    //Since thread_end_cb() is called from main loop thread,
    //we can access EAPI without considering any syncronization.
    RequestUserData *ud = static_cast<RequestUserData *>(data);

    int curlCode = ud->curlCode;
    char * respond = ud->mResponseBuffer->memory;
    void * object = ud->callbackObject;
    RequestUserData_CB callback = ud->callback;

    delete ud;

    if (callback != NULL) {
        callback(object, respond, curlCode);
    }
}

/**
 * @brief This callback is called when async thread should be cancelled
 * @param data[in] - user data
 * @param thread[in] - thread
 */
void LoginRequest::thread_cancel_cb(void *data, Ecore_Thread *thread) {
    RequestUserData* TData = static_cast<RequestUserData*>(data);

    Log::debug("LoginRequest: Cleaning cancelled thread...");

    delete TData;
}

void LoginRequest::Cancel() {
    Log::debug("LoginRequest: Cancelling in progress thread...");
    if (mThread){
        if (mThreadData) {
            mThreadData->mIsCancelled = true;
        }
        ecore_thread_cancel(mThread);
        mThread = NULL;
    }
}
