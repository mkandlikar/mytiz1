#include "AbstractDataProvider.h"
#include "Config.h"
#include "FacebookSession.h"
#include "GraphRequest.h"
#include "MutexLocker.h"
#include "RequestUserData.h"
#include "Utils.h"

#include <dlog.h>
#include <string.h>

#define URL_PREFIX_GRAPH "https://graph.facebook.com"
#define URL_PREFIX_GRAPH_VIDEO "https://graph-video.facebook.com"
#define URL_PREFIX_API "https://api.facebook.com"
#define GRAPH_VERSION "/v2.4"
#define PARAM_DELIMITER "?"
#define PARAM_SEPARATOR "&"
#define ACCESS_TOKEN_PARAM "access_token="
#define MAX_REQUEST_LEN 2048
#define MAX_GRAPH_VERSION_LEN 10
#define MAX_URL_PREFIX 40
#define LOCALE "&locale="


/**
 * @brief Construct an instance of GraphRequest object
 * @param accessToken[in] - This is a facebook access token
 * @param graphPath[in] - This is facebook graph request
 * @param parameters[in] - Parameters for facebook graph request
 * @param httpMethod[in] - The type of Http request
 * @param callback[in] - This is callback to notify client when request is completed
 * @param callback_object[in] - data passed to callback
 */
GraphRequest::GraphRequest(const char * graphPath, bundle * parameters, HttpMethod httpMethod,
        void (*callback) (void *, char*, int ), void* callback_object,
        const char * graphVersion, ServerType serverType,
        bundle * bodyParamsBundle,
        const char * multipartFileUploadName, const char *eTagsHeader) : mStatus(GRS_NOT_STARTED), mIsCallbackInProgress(false)
{
    eina_lock_new( &m_Mutex );
    mGraphPath = strdup(graphPath);
    mHttpMethod = httpMethod;
    mCallback = callback;
    mCallbackObject = callback_object;
    mParamsBundle = parameters;
    mBodyParamsBundle = bodyParamsBundle;
    mGraphVersion = graphVersion != nullptr ? strdup(graphVersion) : nullptr;
    mServerType = serverType;
    mMultipartFileUploadName = multipartFileUploadName != nullptr ? strdup(multipartFileUploadName) : nullptr;
    m_Provider = nullptr;
    mEtagsHeader = (eTagsHeader) ? strdup(eTagsHeader) : nullptr;

    FacebookSession::GetInstance()->AddGraphRequest(this);
}

GraphRequest::GraphRequest(const std::string& graphPath,
                           bundle* parameters,
                           HttpMethod httpMethod,
                           std::function<void(const std::string&, CURLcode)> callback,
                           const std::string& graphVersion,
                           ServerType serverType,
                           bundle* bodyParamsBundle,
                           const std::string& multipartFileUploadName,
                           const std::string& eTagsHeader):
    mCallbackFunctionObject(callback) {
    eina_lock_new( &m_Mutex );
    mGraphPath = strdup(graphPath.c_str());
    mHttpMethod = httpMethod;
    mCallback = nullptr;
    mCallbackObject = nullptr;
    mParamsBundle = parameters;
    mBodyParamsBundle = bodyParamsBundle;
    mGraphVersion = !graphVersion.empty() ? strdup(graphVersion.c_str()) : nullptr;
    mServerType = serverType;
    mMultipartFileUploadName = !multipartFileUploadName.empty() ? strdup(multipartFileUploadName.c_str()) : nullptr;
    m_Provider = nullptr;
    mEtagsHeader = (!eTagsHeader.empty()) ? strdup(eTagsHeader.c_str()) : nullptr;

    FacebookSession::GetInstance()->AddGraphRequest(this);
}

GraphRequest::GraphRequest(const char * graphPath,
        HttpMethod httpMethod, AbstractDataProvider *provider,
        bundle *parameters, const char *eTagsHeader ) : mStatus(GRS_NOT_STARTED), mIsCallbackInProgress(false)
{
    eina_lock_new( &m_Mutex );
    mGraphPath = strdup(graphPath);
    mHttpMethod = httpMethod;
    m_Provider = provider;
    mParamsBundle = parameters;

    // unused for this flow
    mBodyParamsBundle = NULL;
    mGraphVersion = NULL;
    mCallbackObject = NULL;
    mCallback = NULL;
    mServerType = GRAPH;
    mMultipartFileUploadName= NULL;
    mEtagsHeader = (eTagsHeader) ? strdup(eTagsHeader):NULL;

    FacebookSession::GetInstance()->AddGraphRequest(this);
}

/**
 * @brief Destruction
 */
GraphRequest::~GraphRequest()
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "GraphRequest::~GraphRequest start");
    FacebookSession::RemoveGraphRequest(this);
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "mGraphPath = %s", mGraphPath);
    Cancel();

    free((void *) mGraphPath);
    free((void *) mGraphVersion);
    free((void *) mMultipartFileUploadName);
    free(mEtagsHeader);
    eina_lock_free( &m_Mutex );
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "GraphRequest::~GraphRequest end");
}

/**
 * @brief This method run a request to facebook in async thread
 * @return pointer to started async thread
 */
void GraphRequest::ExecuteAsync()
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "GraphRequest::ExecuteAsync");

    //build request
    char requestBuf[MAX_REQUEST_LEN] = {0,};
    const char *res = strstr(mGraphPath, PARAM_DELIMITER);

    char graphVersion[MAX_GRAPH_VERSION_LEN] = {0,};
    if (mServerType != REST)
    {
        Utils::Snprintf_s(graphVersion, MAX_GRAPH_VERSION_LEN, "%s", mGraphVersion != NULL ? mGraphVersion : GRAPH_VERSION);
    }

    char url_prefix[MAX_URL_PREFIX];
    memset(url_prefix, 0, sizeof(url_prefix));

    switch(mServerType){
    case GRAPH:
        Utils::Snprintf_s(url_prefix, MAX_URL_PREFIX, URL_PREFIX_GRAPH);
        break;
    case GRAPH_VIDEO:
        Utils::Snprintf_s(url_prefix, MAX_URL_PREFIX, URL_PREFIX_GRAPH_VIDEO);
        break;
    case REST:
        Utils::Snprintf_s(url_prefix, MAX_URL_PREFIX, URL_PREFIX_API);
        break;
    }

    if (res) {
        Utils::Snprintf_s(requestBuf, MAX_REQUEST_LEN, "%s%s%s%s%s%s%s%s", url_prefix, graphVersion, mGraphPath
                ,PARAM_SEPARATOR, ACCESS_TOKEN_PARAM, Config::GetInstance().GetAccessToken().c_str(), LOCALE,Utils::GetLocale());
    }
    else {
        Utils::Snprintf_s(requestBuf, MAX_REQUEST_LEN, "%s%s%s%s%s%s%s%s", url_prefix, graphVersion, mGraphPath
                ,PARAM_DELIMITER, ACCESS_TOKEN_PARAM, Config::GetInstance().GetAccessToken().c_str(), LOCALE,Utils::GetLocale());
    }

    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "Full Request Url =%s\n", requestBuf);

    mThreadData = new RequestUserData(requestBuf, mMultipartFileUploadName
            ,mParamsBundle, mBodyParamsBundle, mHttpMethod
            ,this, (mEtagsHeader != NULL), mEtagsHeader);
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "GraphRequest::ExecuteAsync ecore_thread_available_get = %d  ecore_thread_max_get = %d"
            ,ecore_thread_available_get(), ecore_thread_max_get());

    //Run a thread
    mThread = ecore_thread_feedback_run( thread_run_cb, handleThreadProgress_cb, thread_end_cb, thread_cancel_cb, mThreadData, EINA_FALSE );
}

void GraphRequest::ExecuteAsyncEx( void (*handleNofify_cb) (void *data, Ecore_Thread *thread, void *notif ) )
{
    mThreadData = new RequestUserData(mGraphPath, mMultipartFileUploadName
            ,mParamsBundle, mBodyParamsBundle, mHttpMethod
            ,this, (mEtagsHeader != NULL), mEtagsHeader);

    mIsProgressFunctionUsed = true;
    //Run the thread
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "GraphRequest::ExecuteAsyncEx ecore_thread_available_get = %d",ecore_thread_available_get());

    mThread = ecore_thread_feedback_run( thread_run_ex_cb, handleNofify_cb, thread_end_ex_cb, thread_cancel_cb, mThreadData, EINA_FALSE );
}

void GraphRequest::ExecuteAsyncExHeader( void (*handleNofify_cb) (void *data, Ecore_Thread *thread, void *notif ) )
{
    mThreadData = new RequestUserData(mGraphPath, mMultipartFileUploadName
            ,mParamsBundle, mBodyParamsBundle, mHttpMethod
            ,this, true, mEtagsHeader);

    mIsProgressFunctionUsed = true;
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "GraphRequest::ExecuteAsyncExHeader ecore_thread_available_get = %d  ecore_thread_max_get = %d"
            ,ecore_thread_available_get(), ecore_thread_max_get());

    //Run the thread
    mThread = ecore_thread_feedback_run( thread_run_ex_cb, handleNofify_cb, thread_end_ex_cb, thread_cancel_cb, mThreadData, EINA_FALSE );
}

/**
 * @brief Invokes when request is completed
 * @param[in] response - body of HTTP response
 * @param[in] curlCode - error code from Curl library
 */
void GraphRequest::OnComplete(char* response, CURLcode curlCode, Ecore_Thread *thread)
{
    SetStatus(GRS_COMPLETED);
    mThread = NULL;
    mIsCallbackInProgress = true;
    if ( m_Provider ) {
        m_Provider->IncomingData( response, NULL, curlCode, thread );
    } else if (mCallback != NULL) {
        mCallback( mCallbackObject, response, curlCode );
    } else if (mCallbackFunctionObject) {
        mCallbackFunctionObject(response ? response : "", curlCode);
        free(response);
    }
    mIsCallbackInProgress = false;
}

void GraphRequest::OnCompletewithHeader(char* response,char *header, int curlCode, Ecore_Thread *thread)
{
    SetStatus(GRS_COMPLETED);
    mThread = NULL;
    mIsCallbackInProgress = true;
    if ( m_Provider ) {
        m_Provider->IncomingData( response, header, curlCode, thread );
    }
    mIsCallbackInProgress = false;
}

void GraphRequest::thread_end_cb(void *data, Ecore_Thread *thread)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "GraphRequest::thread_end_cb start");
    RequestUserData *ud = static_cast<RequestUserData *>(data);

    if(ud->mParentRequest) {
        Sptr<GraphRequest> request = ud->mParentRequest;
        if(request) {
            if(ud->mIsHeaderReq == false) {
                request->OnComplete(ud->mResponseBuffer->memory, ud->curlCode, NULL);
            }
            else {
                request->OnCompletewithHeader(ud->mResponseBuffer->memory, ud->mResponseBuffer->header, ud->curlCode, NULL);
            }
            request->mIsThreadCompleted = true;
        } else {
            if(ud->mResponseBuffer) {
                free((void*) ud->mResponseBuffer->memory);
                ud->mResponseBuffer->memory = NULL;
            }
        }
    }
    delete ud;
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "GraphRequest::thread_end_cb completed");
}

void GraphRequest::thread_end_ex_cb(void *data, Ecore_Thread *thread)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "GraphRequest::thread_end_ex_cb start");
    RequestUserData *requestUserData = static_cast<RequestUserData*>(data);
    if(requestUserData) {
        if(requestUserData->mParentRequest) {
            requestUserData->mParentRequest->mIsThreadCompleted = true;
        }
        delete requestUserData;
    }
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "GraphRequest::thread_end_ex_cb end");
}

/**
 * @brief Cancel request execution
 */
void GraphRequest::Cancel()
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "GraphRequest::Cancel start");
    UnSubscribeAll();
    RemoveCallbacks();
    if(!mIsThreadCompleted) {
        if (mThread != NULL) {
            if (mThreadData != NULL) {
                dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "GraphRequest::Cancel mThreadData->isCancelled");
                mThreadData->mIsCancelled = true;
                //mThread Data will be  deallocated in thread_end_cb or thread_cancel_cb in cleanup() method
            }
            dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "GraphRequest::Cancel ecore_thread_cancel");
            // TODO: check return value, if it is EINA_TRUE thread is cancelled immediately, otherwise it is in running state
            ecore_thread_cancel(mThread);
            mThread = NULL;
        } else {
            SetStatus(GRS_CANCELED);
        }
    }
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "GraphRequest::Cancel end");
}

/**
 * @brief Returns request status
 * @return object status
 */
GraphRequest::GraphRequestStatus GraphRequest::GetStatus() const {
    return mStatus;
};

void GraphRequest::SetStatus(GraphRequestStatus status) {
    mStatus = status;
}

/**
 * @brief Remove all callbacks linked with the object
 */
void GraphRequest::RemoveCallbacks() {
    mCallbackObject = NULL;
    mCallback = NULL;
    m_Provider = NULL;
}

/**
 * @brief Returns flag that indicate that object invokes callback functions
 */
bool GraphRequest::GetIsCallbackInProgress() const {
    return mIsCallbackInProgress;
};

/**
 * @brief Returns request graph path
 */
const char* GraphRequest::GetGraphPath() const {
    return mGraphPath;
}

void GraphRequest::Subscribe(const Subscriber *subscriber) {
    assert(subscriber);
    if (!eina_list_data_find(mSubscribers, subscriber)) {
        mSubscribers = eina_list_append(mSubscribers, subscriber);
    }
}

void GraphRequest::UnSubscribe(const Subscriber *subscriber) {
    mSubscribers = eina_list_remove(mSubscribers, subscriber);
}

void GraphRequest::UnSubscribeAll() {
    mSubscribers = eina_list_free(mSubscribers);
}

void GraphRequest::Notify(AppEventId eventId, void * data) {
    Eina_List *l;
    void *itemData;
    EINA_LIST_FOREACH(mSubscribers, l, itemData) {
        Subscriber *subscriber = static_cast<Subscriber *>(itemData);
        if(subscriber) {
            subscriber->Update(eventId, data);
        }
    }
}

void GraphRequest::OnProgress(long int dltotal, long int dlnow, long int ultotal, long int ulnow) {
    mDltotal = dltotal;
    mDlnow = dlnow;
    mUltotal = ultotal;
    mUlnow = ulnow;
    if(!mIsProgressFunctionUsed) {
        if(mThread && !mIsThreadCompleted) {
            if ( !ecore_thread_feedback( mThread, (void*)this ) ) {
                dlog_print( DLOG_ERROR, LOG_TAG_FACEBOOK, "OnProgress [ thread ] : cannot pass a message to a main thread to notify." );
            }
        }
    }
}

double GraphRequest::GetProgress() {
    double total = 1;
    double cur = 0;
    if(mUltotal > 0) {
        total = mUltotal;
        cur = mUlnow;
    }
    double progress = cur / total;
    if(progress > 1.0) {
        progress = 1.0;
    }
    return progress;
}

bool GraphRequest::IsProgressFunctionUsed() {
    return mIsProgressFunctionUsed;
}

unsigned int GraphRequest::AddRef() {
    return BaseObject::AddRef();
};

unsigned int GraphRequest::Release() {
    MutexLocker locker( &m_Mutex );
    return BaseObject::Release();
};

/**
 * @brief  - This method prepare parameter string from bundle
 * @param params[in] - bundle with parameters
 * @return string with parameters
 */
char *GraphRequest::PrepareParamsFromBundle(bundle * params) {
    char* res = NULL;
    struct MemoryStruct* concatenatedParams = PrepareMemoryStructFromBundle(params);

    if ( concatenatedParams != NULL ) {
        res = concatenatedParams->memory;
        free(concatenatedParams);
    }
    return res;
}

/**
 * @brief  - This method prepare MemoryStruct from bandle
 * @param params[in] - bandle with parameters
 * @return MemoryStruct object
 */
MemoryStruct* GraphRequest::PrepareMemoryStructFromBundle(bundle * params) {
    struct MemoryStruct* concatenatedParams = NULL;
    if ( params != NULL )
    {
        bundle_iterator_t iterator = &iterate_bundle_foreach;
        concatenatedParams = (MemoryStruct *) calloc(1, sizeof(MemoryStruct));
        concatenatedParams->memory = NULL;
        concatenatedParams->size = 0;
        bundle_foreach( params, iterator, concatenatedParams );
    }
    return concatenatedParams;
}

/**
 * @brief  - This method runs in async thread
 * @param data[in] - user data passed to thread
 * @param thread[in] - thread
 */
void GraphRequest::thread_run_cb(void *data, Ecore_Thread *thread)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "GraphRequest::thread_run_cb");
    RequestUserData *ud = static_cast<RequestUserData *>(data);
    if(ud->mParentRequest) {
        ud->mParentRequest->SetStatus(GRS_IN_PROGRESS);
    }

    CURLcode code = CURLE_OK;
    if (ud->httpMethod == GET) {
        code = SendHttpGet(ud);
    }
    else if (ud->httpMethod == POST) {
        code = (CURLcode)SendHttpPost(ud);
    }
    else if (ud->httpMethod == POST_MULTIPART) {
        code = (CURLcode)SendHttpPostMultiPart(ud);
    }
    else if (ud->httpMethod == DELETE) {
        code = SendHttpDelete(ud);
    }
    ud->curlCode = code;
}

void GraphRequest::thread_run_ex_cb(void *data, Ecore_Thread *thread)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "GraphRequest::thread_run_ex_cb");
    RequestUserData *ud = static_cast<RequestUserData *>(data);
    if(ud->mParentRequest) {
        ud->mParentRequest->SetStatus(GRS_IN_PROGRESS);
    }

    CURLcode code = CURLE_OK;
    if (ud->httpMethod == GET) {
        code = SendHttpGet(ud);
    }
    else if (ud->httpMethod == POST) {
        code = (CURLcode)SendHttpPost(ud);
    }
    else if (ud->httpMethod == POST_MULTIPART) {
        code = (CURLcode)SendHttpPostMultiPart(ud);
    }
    else if (ud->httpMethod == DELETE) {
        code = SendHttpDelete(ud);
    }
    ud->curlCode = code;

    // In case  thread must be canceled, cancel this
    // loop co-operatively (canceling is co-operative)
    if (!ecore_thread_check(thread)) {
        if(ud->mParentRequest && !ud->mIsCancelled ) {
            Sptr<GraphRequest> request = ud->mParentRequest;
            if(ud->mIsHeaderReq ) {
                request->OnCompletewithHeader(ud->mResponseBuffer->memory, ud->mResponseBuffer->header, ud->curlCode, thread);
            } else {
                request->OnComplete(ud->mResponseBuffer->memory, ud->curlCode, thread);
            }
        }
    } else {
        if(ud->mResponseBuffer) {
            free((void*) ud->mResponseBuffer->memory);
            ud->mResponseBuffer->memory = NULL;
        }
    }
}

/**
 * @brief This callback is called when async thread should be cancelled
 * @param data[in] - user data
 * @param thread[in] - thread
 */
void GraphRequest::thread_cancel_cb(void *data, Ecore_Thread *thread)
{
    dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "GraphRequest::thread_cancel_cb");
    RequestUserData *ud = static_cast<RequestUserData *>(data);
    if(ud) {
        if(ud->mParentRequest) {
            ud->mParentRequest->SetStatus(GRS_CANCELED);
            ud->mParentRequest->mIsThreadCompleted = true;
        }
        if(ud->mResponseBuffer) {
            free((void*) ud->mResponseBuffer->memory);
            ud->mResponseBuffer->memory = NULL;
        }
    }
    delete ud;
}

/**
 * @brief This is callback for bundle_foreach function. It append bundle item to parameters list
 * @param key - bundle item key
 * @param type - bundle item typr
 * @param kv - bundle item value
 * @param user_data - user data passed to callback
 */
void GraphRequest::iterate_bundle_foreach(const char *key, const int type, const bundle_keyval_t *kv, void *user_data)
{
    /* We remove bundle parametr "origin_message",
     * because this is just a temporary local parametr for Lazy-Posting feature.
     * We should not send it to Facebook server, because some times it corrupts request.
     */
    if (key && strcmp(key, "origin_message")) {
        void *ptr = NULL;
        unsigned int size = 0;
        MemoryStruct* concatenatedParams = (MemoryStruct*) user_data;

        if (type == BUNDLE_TYPE_STR) {
            bundle_keyval_get_basic_val((bundle_keyval_t *) kv, &ptr, &size);
            int keyLen = strlen(key);

            size_t appendLen = keyLen + 1 + sizeof(char) * size + 1;
            char* appendBuff = (char*) malloc(appendLen);

//       std::string msg = Utils::ReplaceString((char *)ptr, "&", "%26");
//       Utils::Snprintf_s(appendBuff, keyLen + 1 + size + 1, "%s=%s", key, msg.c_str());
            Utils::Snprintf_s(appendBuff, keyLen + 1 + size + 1, "%s=%s", key, ((char*) ptr));

            if (concatenatedParams->memory == NULL) {
                concatenatedParams->memory = appendBuff;
                concatenatedParams->size = appendLen;
            } else {
                size_t newBufferLen = concatenatedParams->size + 1 + appendLen;
                concatenatedParams->memory = (char*) realloc(concatenatedParams->memory, newBufferLen);
                concatenatedParams->size = newBufferLen;
                eina_strlcat(concatenatedParams->memory, "&", newBufferLen);
                eina_strlcat(concatenatedParams->memory, appendBuff, newBufferLen);
                free(appendBuff);
            }

            dlog_print(DLOG_DEBUG, LOG_TAG_FACEBOOK, "newBuf = %s", concatenatedParams->memory);
        }
    }
}

void GraphRequest::handleThreadProgress_cb( void *data, Ecore_Thread *thread, void *ThreadMsgObj )
{
    if (ThreadMsgObj) {
        GraphRequest* graphRequest = static_cast<GraphRequest*>(ThreadMsgObj);
        AppEvents::Get().Notify(eGRAPHREQUEST_PROGRESS, graphRequest);
    }
}
