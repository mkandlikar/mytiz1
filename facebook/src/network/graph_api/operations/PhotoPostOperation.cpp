#include <app.h>
#include <notification.h>

#include "AlbumsScreen.h"
#include "Common.h"
#include "ConnectivityManager.h"
#include "CSmartPtr.h"
#include "FacebookSession.h"
#include "FbRespondPost.h"
#include "FbRespondPostPhoto.h"
#include "Log.h"
#include "OperationManager.h"
#include "PhotoPostOperation.h"
#include "PostComposerMediaData.h"
#include "PresenterHelpers.h"
#include "SoundNotificationPlayer.h"
#include "TrayNotificationsManager.h"

static constexpr char PPOP_LOG_TAG[] = "Facebook::PhotoPostOperation";

PhotoPostOperation::PhotoPostOperation(OperationType opType,
                                       const std::string& userId,
                                       bundle* paramsBundle,
                                       Eina_List *mediaList,
                                       const char* msgUTF8,
                                       const char* privacy,
                                       const std::list<Friend>& taggedFriends,
                                       const std::string& privacyType,
                                       const PCPlace* place,
                                       OperationDestination opDestination):
    Operation(taggedFriends, privacyType, place),
    mRequest(nullptr),
    mIsSubscribedToProgress(false) {
    Log::debug_tag(PPOP_LOG_TAG, "PhotoPostOperation::PhotoPostOperation");
    mOpType = opType;
    mOpDestination = opDestination;
    mStepCount = 1;
    mUserId = userId;
    mParamsBundle = bundle_dup(paramsBundle);
    mPhotoDescriptionList = nullptr;
    mPrevProgress = 0.0;
    mOngoingNotification = nullptr;
    mUploadedPostId = nullptr;
    mSentPhotos = 0;
    mOngoingNotificationJob = nullptr;
    if(mediaList) {
        // prepare photo descriptions
        Eina_List * listItem;
        void * listData;
        EINA_LIST_FOREACH(mediaList, listItem, listData) {
            PostComposerMediaData * mediaContent = static_cast<PostComposerMediaData *>(listData);
            bundle* mediaParamsBundle = bundle_create();

            if (mOpType == OT_Photo_Post_Create) {
                bundle_add_str(mediaParamsBundle, "published", "0");
                bundle_add_str(mediaParamsBundle, "no_story", "true");
            } else if (mOpType == OT_Video_Post_Create) {
                if(mediaContent->GetType() == MEDIA_CONTENT_TYPE_VIDEO) {
                    bundle_add_str(mediaParamsBundle, "no_story", "false");
                    if(msgUTF8) {
                        bundle_add_str(mediaParamsBundle, "description", msgUTF8);
                    }
                    //generate privacy
                    if(privacy) {
                        bundle_add_str(mediaParamsBundle, "privacy", privacy);
                    }
                }
            } else if (mOpType == OT_Album_Add_New_Photos) {
                const char *caption = nullptr;
                if (eina_list_count(mediaList) == 1) {
                    caption = msgUTF8;
                } else if (mediaContent->GetCaption()) {
                    caption = mediaContent->GetCaption();
                }
                if (caption) {
                    bundle_add_str(mediaParamsBundle, "caption", caption);
                }
                char *place = nullptr;
                bundle_get_str(mParamsBundle, "place", &place);
                if (place) {
                    bundle_add_str(mediaParamsBundle, "place", place);
                }
            }

            if (mediaContent->GetTagging()) {
                char *textToPost = mediaContent->GetTagging()->GetStringToPost(false);
                mediaContent->SetCaption(textToPost);
                free(textToPost);
            }

            PhotoPostDescription *desc = new PhotoPostDescription(mediaContent->GetFilePath()
                                                                    ,userId.c_str()
                                                                    ,mediaContent->GetCaption()
                                                                    ,mediaParamsBundle
                                                                    ,mediaContent->GetThumbnailPath()
                                                                    ,mediaContent->GetOriginalPath(), this);
            mPhotoDescriptionList = eina_list_append( mPhotoDescriptionList, desc );
        }

        switch (mOpType) {
        case OT_Photo_Post_Create:
            mStepCount = 2;
            mOngoingNotification = TrayNotificationsManager::GetInstance()->CreateOngoingNotification();
            break;
        case OT_Video_Post_Create:
            mStepCount = 1;
            mOngoingNotification = TrayNotificationsManager::GetInstance()->CreateOngoingNotification();
            if(!mIsSubscribedToProgress) {
                AppEvents::Get().Subscribe(eGRAPHREQUEST_PROGRESS, this);
                mIsSubscribedToProgress = true;
            }
            break;
        case OT_Album_Add_New_Photos:
            mStepCount = 1;
            mOngoingNotification = TrayNotificationsManager::GetInstance()->CreateOngoingNotification();
            break;
        default:
            break;
        }
    }
}

PhotoPostOperation::~PhotoPostOperation() {
    Log::debug_tag(PPOP_LOG_TAG, "PhotoPostOperation::~PhotoPostOperation");
    bundle_free(mParamsBundle);
    GraphRequest* requestRawPtr = mRequest.Get();
    if (requestRawPtr) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(requestRawPtr);
    }

    void *itemData;
    EINA_LIST_FREE(mPhotoDescriptionList, itemData) {
        PhotoPostDescription *description = static_cast<PhotoPostDescription *>(itemData);
        assert(description);
        GraphRequest* request = description->GetRequest();
        if (request) {
            FacebookSession::GetInstance()->ReleaseGraphRequest(request);
        }
        delete description;
    }

    free(mUploadedPostId);

    if (mOngoingNotificationJob) {
        ecore_job_del(mOngoingNotificationJob);
    }
}

void PhotoPostOperation::CurrentStepComplete() {
    Operation::CurrentStepComplete();
}

bool PhotoPostOperation::DoNextStep() {
    mStepError = mStepError || mRequestError;
    if(mStepError) {
        //After a network error we are going to redo the last step instead of doing the next step. So, don't want to increase step count.
        return DoStep();
    } else {
        return Operation::DoNextStep();
    }
}

bundle* PhotoPostOperation::GetBundle() {
    return mParamsBundle;
}

Eina_List* PhotoPostOperation::GetPhotos() {
    return mPhotoDescriptionList;
}

bool PhotoPostOperation::CurrentStepExecute() {
    bool ret = false;
    if ((mCurrentStep < mStepCount) || ((mCurrentStep == mStepCount) && (mOpType == OT_Album_Add_New_Photos))) {
        mOpState = OS_InProgress; // it's needed to update progress, otherwise the event below is ignored.
        UpdateProgress(true);

        switch (mOpType) {
        case OT_Album_Add_New_Photos:
        case OT_Photo_Post_Create:
            UploadNextPhoto();
            break;
        default:
            break;
        }
        ret = true;
    } else if (mCurrentStep == mStepCount) {
        GraphRequest* requestRawPtr = mRequest.Get();
        if (requestRawPtr) {
            FacebookSession::GetInstance()->ReleaseGraphRequest(requestRawPtr);
        }
        mRequest = nullptr;
        switch (mOpType) {
        case OT_Photo_Post_Create:
            UpdateProgress(true);
            mRequest = FacebookSession::GetInstance()->PostFeedWithPhotos(mParamsBundle, mPhotoDescriptionList, on_post_feed_request_completed, this, mUserId.c_str());
            if (mRequest) {
                mRequest->Subscribe(this);
            }
            ret = true;
            break;
        case OT_Video_Post_Create: {
            PhotoPostDescription *photo = GetPhotoDescriptionByIndex(0);
            assert(photo);
            photo->SetRequest(FacebookSession::GetInstance()->PostVideo(photo->GetFilePath(), mParamsBundle, on_photo_upload_request_completed, photo, mUserId.c_str()));
            if (photo->GetRequest()) {
                photo->GetRequest()->Subscribe(this);
                ret = true;
            }
        }
            break;
        default:
            break;
        }
    }
    return ret;
}

void PhotoPostOperation::UploadNextPhoto() {
    Eina_List *l;
    void *itemData;
    EINA_LIST_FOREACH(mPhotoDescriptionList, l, itemData) {
        PhotoPostDescription *photo = static_cast<PhotoPostDescription *>(itemData);
        assert(photo);

        if (!photo->GetPhotoId()) {
            photo->SetRequest(FacebookSession::GetInstance()->PostPhoto(photo->GetFilePath(), photo->GetBundle(), on_photo_upload_request_completed, photo, mUserId.c_str()));
            break;
        }
    }
}

void PhotoPostOperation::on_photo_upload_request_completed(void* object, char* responseData, int code) {
    Log::debug_tag(PPOP_LOG_TAG, "PhotoPostOperation::on_photo_upload_request_completed");
    c_unique_ptr<char> responseText(responseData);
    PhotoPostDescription *photo = static_cast<PhotoPostDescription*>(object);
    PhotoPostOperation *oper = photo->GetOperation();
    assert(oper);

    GraphRequest* requestRawPtr = photo->GetRequest();
    if (requestRawPtr) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(requestRawPtr);
        photo->SetRequest(nullptr);
    }
    oper->mErrorCode = static_cast<CURLcode>(code);
    if(oper->IsFailedBecauseOfGoingOffline()) {
        if (oper->mErrorCode == CURLE_RECV_ERROR && oper->GetType() == OT_Album_Add_New_Photos) { //This means that the request is received by the FB server, but the FB4T app doesn't receive response.
//Usually, this means that the Photo is fully uploaded on the server side and we can consider the operation as completed (only for OT_Album_Add_New_Photos mode, becaues we don't need PhotoId). The post will appear on the next feed update.
            Log::debug_tag(PPOP_LOG_TAG, "PhotoPostOperation::on_photo_upload_request_completed connection error CURLE_RECV_ERROR. Most probably photo is uploaded");
            oper->mErrorCode = CURLE_OK;
            photo->SetPhotoId("999"); //set non-NULL fake photoId, because we have not received real photoId in case of CURLE_RECV_ERROR. But we don't need it for OT_Album_Add_New_Photos mode. Fake Id is enough.

            AppEvents::Get().Notify(eALBUM_NEW_PHOTO_ADDED_EVENT, const_cast<char *> (oper->mUserId.c_str()));
            ++oper->mSentPhotos;
            oper->UpdateProgress(false);

            if (oper->mSentPhotos == eina_list_count(oper->mPhotoDescriptionList)) {
                oper->CurrentStepComplete();
            } else {
                oper->UploadNextPhoto();
            }
        } else {
            Log::error_tag(PPOP_LOG_TAG, "PhotoPostOperation::on_post_feed_request_completed connection error, error code: `%d`", code);
            oper->mRequestError = true;
        }
    } else {
        switch(code) {
            case CURLE_OK: {
                if (responseText) {
                   Log::debug_tag(PPOP_LOG_TAG, "response: %s", responseText.get());
                   std::unique_ptr<FbRespondPostPhoto> response(FbRespondPostPhoto::createFromJson(responseText.get()));
                   if (response) {
                       if (response->mError) {
                           Log::error_tag(PPOP_LOG_TAG, "error = %s", response->mError->mMessage);
                           ShowErrorPopup(response->mError);
                           oper->Cancel();
                       } else {
                           // store photo id
                           if (photo) {
                               photo->SetPhotoId(response->mId);
                           }
                           if (oper->GetType() == OT_Album_Add_New_Photos) {
                               AppEvents::Get().Notify(eALBUM_NEW_PHOTO_ADDED_EVENT, const_cast<char *> (oper->mUserId.c_str()));
                           }

                           ++oper->mSentPhotos;
                           oper->UpdateProgress(false);

                           if (oper->mSentPhotos == eina_list_count(oper->mPhotoDescriptionList)) {
                               oper->CurrentStepComplete();
                           } else {
                               oper->UploadNextPhoto();
                           }
                       }
                   } else {
                       Log::error(LOG_FACEBOOK_OPERATION, "Http respond is corrupted");
                       oper->mRequestError = true;
                   }
               } else {
                   Log::error(LOG_FACEBOOK_OPERATION, "Error parsing http respond");
                   oper->mRequestError = true;
               }
                break;
            }
            case CURLE_FILE_COULDNT_READ_FILE:
                Log::debug_tag(PPOP_LOG_TAG, "PhotoPostOperation::on_photo_upload_request_completed file was removed before upload");
                oper->mRequestError = true;
                oper->Cancel();
                break;
            default:
                oper->mRequestError = true;
                break;
        }
    }
    if (oper->mRequestError) {
        oper->CurrentStepComplete();
    }
}

void PhotoPostOperation::UpdateProgress(bool isForced) {
    unsigned int totalPhotos = eina_list_count(mPhotoDescriptionList);

    if (GetType() != OT_Video_Post_Create) {
        int percent = mSentPhotos * 100 / (totalPhotos + (mOpType == OT_Photo_Post_Create ? 1 : 0)); //for OT_Photo_Post_Create one more step is required to send post body.
        double progress = (double)percent/100;
        if(isForced || progress > mPrevProgress) {
            if (progress <= 1.0) {
                Notify(eOPERATION_STEP_PROGRESS_EVENT, &progress);
                mPrevProgress = progress;
                if (!mOngoingNotificationJob) {
                    mOngoingNotificationJob = ecore_job_add(update_ongoing_notification, this);
                }
            }
        }
    }
}

void PhotoPostOperation::on_post_feed_request_completed(void* object, char* responseData, int code) {
    Log::debug_tag(PPOP_LOG_TAG, "PhotoPostOperation::on_post_feed_request_completed, curl code: `%d`, response: `%s`", code, responseData);
    Sptr<PhotoPostOperation> oper(static_cast<PhotoPostOperation*>(object));

    if (oper) {
        bool isComplete = true;
        oper->mErrorCode = static_cast<CURLcode>(code);
        if(oper->mErrorCode == CURLE_OK) {
            if (responseData) {
                FbResponsePhotoPost *postRespond = FbResponsePhotoPost::createFromJson(responseData);
                if (postRespond) {
                    if (!postRespond->mError) {
                        free(oper->mUploadedPostId);
                        oper->mUploadedPostId = SAFE_STRDUP(postRespond->mId);
                        Log::info(LOG_FACEBOOK_OPERATION, "Share id = %s", postRespond->mId);

                        Eina_List *resendList = nullptr;
                        if (postRespond->GetNotSentPhotos()) {
                            Eina_List *l;
                            void *itemData;
                            EINA_LIST_FOREACH(postRespond->GetNotSentPhotos(), l, itemData) {
                                int notSentPhotoIndex = (int)(itemData);
                                PhotoPostDescription *photo = static_cast<PhotoPostDescription *> (eina_list_nth(oper->mPhotoDescriptionList, notSentPhotoIndex));
                                assert(photo);
                                resendList = eina_list_append(resendList, photo);
                            }
                            FacebookSession::GetInstance()->AddPhotosToPost(nullptr, resendList, on_post_feed_request_completed, object, nullptr, oper->mUploadedPostId);
                            resendList = eina_list_free(resendList);
                            isComplete = false;
                        }
                    } else {
                        Log::error(LOG_FACEBOOK_OPERATION, "error = %s", postRespond->mError->mMessage);
                        oper->mRequestError = true;
                        ShowErrorPopup(postRespond->mError);
                        oper->Cancel();
                    }
                    delete postRespond;
                } else {
                    Log::error(LOG_FACEBOOK_OPERATION, "postRespond is NULL.");
                }
            } else {
                Log::error(LOG_FACEBOOK_OPERATION, "Http respond is corrupted");
                oper->mRequestError = true;
            }
        } else if(oper->IsFailedBecauseOfGoingOffline()) {
            if (oper->mErrorCode == CURLE_RECV_ERROR) { //This means that the request is received by the FB server, but the FB4T app doesn't receive response.
//Usually, this means that the Post is fully created on the server side and we can consider the operation as completed. The post will appear on the next feed update.
//However, sometimes it's possible that the post isn't created yet, but, unfortunately, we can't handle such cases.
                Log::debug_tag(PPOP_LOG_TAG, "PhotoPostOperation::on_post_feed_request_completed connection error CURLE_RECV_ERROR. Most probably post is created");
                oper->mErrorCode = CURLE_OK;
            } else {
                Log::error_tag(PPOP_LOG_TAG, "PhotoPostOperation::on_post_feed_request_completed connection error, error code: `%d`", code);
                isComplete = false;
            }
        } else {
            Log::error(LOG_FACEBOOK_OPERATION, "Error sending http request");
            oper->mRequestError = true;
        }
        GraphRequest* requestRawPtr = oper->mRequest.Get();
        if (requestRawPtr) {
            FacebookSession::GetInstance()->ReleaseGraphRequest(requestRawPtr);
            oper->mRequest = nullptr;
        }
        if (isComplete) {
            oper->CurrentStepComplete();
        }
    }

    free(responseData);
}

PhotoPostOperation::PhotoPostDescription *PhotoPostOperation::GetPhotoDescriptionByIndex(unsigned int index) {
    return (PhotoPostOperation::PhotoPostDescription *)eina_list_nth(mPhotoDescriptionList, index);
}

void PhotoPostOperation::OnOperationComplete() {
    Log::info(LOG_FACEBOOK_OPERATION, "PhotoPostOperation::OnOperationComplete->Is Success=(%d), Error Code = (%d) ", !mRequestError, mErrorCode);
    Log::info(LOG_FACEBOOK_OPERATION, "PhotoPostOperation::OnOperationComplete->OperationType=(%d)", (int)mOpType);
    if(mIsSubscribedToProgress) {
        AppEvents::Get().Unsubscribe(eGRAPHREQUEST_PROGRESS, this);
        mIsSubscribedToProgress = false;
    }
    mOperationResult = (mRequestError || IsArtificiallyFailed()) ? OR_Error : OR_Success;

    if(IsFailedBecauseOfGoingOffline()) {// for mArtificialErrorCode this condition is ignored
        PauseOperation();
    }
    switch (mOpType) {
    case OT_Album_Add_New_Photos:
        AppEvents::Get().Notify(eALBUM_NEW_PHOTOS_ADDITION_COMPLETED_EVENT, const_cast<char *> (mUserId.c_str()));
        //no break here - fall through!
    case OT_Photo_Post_Create:
    case OT_Video_Post_Create:
        TrayNotificationsManager::GetInstance()->DeleteOngoingNotification(mOngoingNotification);
        break;
    default:
        break;
    }
}

bool PhotoPostOperation::Cancel() {
    Log::info(LOG_FACEBOOK_OPERATION, "PhotoPostOperation::Cancel()");

    Eina_List *l;
    void *itemData;
    EINA_LIST_FOREACH(mPhotoDescriptionList, l, itemData) {
        PhotoPostDescription *photo = static_cast<PhotoPostDescription *>(itemData);
        assert(photo);
        GraphRequest* request = photo->GetRequest();
        if (request) {
            FacebookSession::GetInstance()->ReleaseGraphRequest(request);
            photo->SetRequest(nullptr);
        }
    }

    if (mRequest) {
        GraphRequest* requestRawPtr = mRequest.Get();
        FacebookSession::GetInstance()->ReleaseGraphRequest(requestRawPtr);
        mRequest = nullptr;
    }

    mSentPhotos = 0;

    Operation::Cancel();
    if(mIsSubscribedToProgress) {
        AppEvents::Get().Unsubscribe(eGRAPHREQUEST_PROGRESS, this);
        mIsSubscribedToProgress = false;
    }
    TrayNotificationsManager::GetInstance()->DeleteOngoingNotification(mOngoingNotification);
    return true;
}

const char* PhotoPostOperation::GetDestinationId() {
    return mUserId.c_str();
}

PhotoPostOperation::PhotoPostDescription::PhotoPostDescription(const char * imageFileName, const char * id, const char * caption, bundle * paramsBundle, const char *thumbnailPath, const char *originalPath, PhotoPostOperation *operation) {
    mImageFileName = SAFE_STRDUP(imageFileName);
    mId = SAFE_STRDUP(id);
    mParams = bundle_dup(paramsBundle);
    mPhotoId = NULL;
    mCaption = SAFE_STRDUP(caption);
    mThumbnailPath = SAFE_STRDUP(thumbnailPath);
    mOriginalPath = SAFE_STRDUP(originalPath);
    mOperation = operation;
    mRequest = nullptr;
}

PhotoPostOperation::PhotoPostDescription::~PhotoPostDescription() {
    free(mImageFileName);
    free(mId);
    bundle_free(mParams);
    free(mPhotoId);
    free(mCaption);
    free(mThumbnailPath);
    free(mOriginalPath);
}

void PhotoPostOperation::PhotoPostDescription::SetPhotoId(const char *photoId) {
    mPhotoId = SAFE_STRDUP(photoId);
}

void PhotoPostOperation::Update(AppEventId eventId, void * data) {
    switch(eventId) {
        case eGRAPHREQUEST_PROGRESS: { // this is used only for OT_Video_Post_Create now!
            GraphRequest *request = static_cast<GraphRequest *>(data);

            PhotoPostDescription *photo = GetPhotoDescriptionByIndex(0);
            assert(photo);

            if(request == photo->GetRequest()) {
                int stepCount = GetStepCount();
                int curStep = (GetCurrentStep() > 0) ? GetCurrentStep() - 1 : 0;

                if (mOpType == OT_Photo_Post_Create) {
                    stepCount = (GetStepCount() > 1) ? GetStepCount() - 1: 1;
                }

                int percent = (curStep + request->GetProgress() )*100 / stepCount;

                double progress = (double)percent/100;
                if(progress > mPrevProgress) {
                    if (progress <= 1.0) {
                        Notify(eOPERATION_STEP_PROGRESS_EVENT, &progress);
                        mPrevProgress = progress;
                        if (!mOngoingNotificationJob) {
                            mOngoingNotificationJob = ecore_job_add(update_ongoing_notification, this);
                        }
                    }
                }
            }
            break;
        }
        default:
            break;
    }
}

void PhotoPostOperation::update_ongoing_notification(void * data)
{
    Sptr<PhotoPostOperation> me(static_cast<PhotoPostOperation*>(data));
    assert (me);
    me->mOngoingNotificationJob = nullptr;
    TrayNotificationsManager::GetInstance()->UpdateOngoingNotification(me->mOngoingNotification, me->mPrevProgress);
}

void PhotoPostOperation::PauseOperation() {
    Operation::PauseOperation();
    TrayNotificationsManager::GetInstance()->DeleteOngoingNotification(mOngoingNotification);
}

bool PhotoPostOperation::ReStart() {
    return DoStep();
}

void PhotoPostOperation::ResetOperation() {
    OperationState opState = mOpState;
    int currentStep = mCurrentStep;
    Operation::ResetOperation();
    if (opState != OS_NotStarted) { //Keep current state if operation is in progress.
        mCurrentStep = currentStep;
        mOpState = OS_InProgress;
    } else {
        mPrevProgress = 0.0;
    }
    if(!mOngoingNotification) {
        mOngoingNotification = TrayNotificationsManager::GetInstance()->CreateOngoingNotification();
    }
    if(OT_Video_Post_Create == mOpType && !mIsSubscribedToProgress) {
        AppEvents::Get().Subscribe(eGRAPHREQUEST_PROGRESS, this);
        mIsSubscribedToProgress = true;
    }
}

bool PhotoPostOperation::IsFeedOperation(){

    if( GetType() == Operation::OT_Photo_Post_Create || GetType() == Operation::OT_Album_Add_New_Photos ||
        GetType() == Operation::OT_Video_Post_Create )
    {
        if( GetDestination() == Operation::OD_Home ||
            GetDestination() == Operation::OD_Group ||
            GetDestination() == Operation::OD_Friend_Timeline ) {
            return true;
        }
    }
    return false;
}

void PhotoPostOperation::PrepareToBeRemoved() {
    if (mRequestError) {
        TrayNotificationsManager::GetInstance()->CreateNotiNotification(i18n_get_text("IDS_POST_UPLOAD_ERROR_NOTIFICATION"), nullptr);
    } else if (mOpDestination == OD_Friend_Timeline) {
        notification_status_message_post(i18n_get_text("IDS_POST_WAS_SUCCESSFUL"));
    } else {
        app_control_h app_control;
        if (APP_CONTROL_ERROR_NONE != app_control_create(&app_control)) {
            app_control = nullptr;
        } else {
            app_control_set_app_id(app_control, PACKAGE);
            app_control_set_operation(app_control, APP_CONTROL_CUSTOM_OPERATION_PHOTO_VIDEO_UPLOAD);
            app_control_add_extra_data(app_control, NOTIF_POST_ID, mUploadedPostId);
        }
        TrayNotificationsManager::GetInstance()->CreateNotiNotification(i18n_get_text("IDS_POST_UPLOAD_SUCCESS_NOTIFICATION"), app_control);
        if (app_control) {
            app_control_destroy(app_control);
        }
    }
}

void PhotoPostOperation::PutInErrorState() {
    if(mRequest) {
        mRequest->Cancel();
        GraphRequest* requestRawPtr = mRequest.Get();
        if (requestRawPtr) {
            FacebookSession::GetInstance()->ReleaseGraphRequest(requestRawPtr);
            mRequest = nullptr;
        }
    }
    Operation::PutInErrorState();
}
