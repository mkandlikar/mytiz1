#include "ActionBase.h"
#include "Application.h"
#include "CacheManager.h"
#include "FacebookSession.h"
#include "FbRespondEditPostMessage.h"
#include "FbRespondGetPostDetails.h"
#include "FbRespondPost.h"
#include "Log.h"
#include "OperationManager.h"
#include "PrivacyOptionsData.h"
#include "ScreenBase.h"
#include "SimplePostOperation.h"
#include "SoundNotificationPlayer.h"
#include "Utils.h"

static constexpr char SPOP_LOG_TAG[] = "Facebook::SimplePostOperation";

SimplePostOperation::SimplePostOperation(OperationType opType,
                                         const std::string& userId,
                                         bundle* paramsBundle,
                                         const std::list<Friend>& taggedFriends,
                                         const std::string& privacyType,
                                         const PCPlace* place,
                                         OperationDestination opDestination,
                                         Post * sharedPost,
                                         const char *coverOrProfilePhotoId):
     Operation(taggedFriends, privacyType, place),
     mIsSubscribedToRequestProgress(false),
     mRequest(nullptr) {
    Log::debug_tag(SPOP_LOG_TAG, "SimplePostOperation::SimplePostOperation");
    //OT_Share_WEB_Link is a some kind of a fake op.type. Its behavior is exactly the same as OT_Post_Create,
    // except that it must show messages when post sending is started and completed. So, map this op.type to OT_Post_Create.
    if (opType == OT_Share_WEB_Link) {
        mOpType = OT_Post_Create;
        mIsShareWebLink = true;
    } else {
        mOpType = opType;
        mIsShareWebLink = false;
    }

    mOpDestination = opDestination;
    mStepCount = 1;
    mUserId = userId;
    mParamsBundle = paramsBundle ? bundle_dup(paramsBundle) : NULL;
    mSharedActionAndPost = new ActionAndData(sharedPost, nullptr);
    mPostDetailsResponse = nullptr;
    mCoverOrProfilePhotoId = SAFE_STRDUP(coverOrProfilePhotoId);
}

SimplePostOperation::~SimplePostOperation() {
    Log::debug_tag(SPOP_LOG_TAG, "SimplePostOperation::~SimplePostOperation start");
    if (mParamsBundle) {
        bundle_free(mParamsBundle);
    }

    delete mSharedActionAndPost;
    GraphRequest* requestRawPtr = mRequest.Get();
    FacebookSession::GetInstance()->ReleaseGraphRequest(requestRawPtr);
    AppEvents::Get().Unsubscribe(eGRAPHREQUEST_PROGRESS, this);
    Log::debug_tag(SPOP_LOG_TAG, "SimplePostOperation::~SimplePostOperation end");
    delete mPostDetailsResponse;
    free(mCoverOrProfilePhotoId);
}

bool SimplePostOperation::CurrentStepExecute() {
    Log::debug_tag(SPOP_LOG_TAG, "SimplePostOperation::CurrentStepExecute");
    bool ret = false;
    if(mCurrentStep == 1) {
        if(mRequest) {
            Log::debug_tag(SPOP_LOG_TAG, "SimplePostOperation::CurrentStepExecute unsubscribe");
            mRequest->UnSubscribe(this);
            GraphRequest* requestRawPtr = mRequest.Get();
            FacebookSession::GetInstance()->ReleaseGraphRequest(requestRawPtr);
            mRequest = nullptr;
        }
        switch(mOpType) {
            case OT_Post_Create: {
                if(!mIsSubscribedToRequestProgress) {
                    AppEvents::Get().Subscribe(eGRAPHREQUEST_PROGRESS, this);
                    mIsSubscribedToRequestProgress = true;
                }
            }
            case OT_Share_WEB_Link:
                mRequest = FacebookSession::GetInstance()->PostFeed(mParamsBundle, on_post_feed_request_completed, this, mUserId.c_str());
                ret = mRequest;
                break;
            case OT_Share_Post_Create:
                if(!mIsSubscribedToRequestProgress) {
                    AppEvents::Get().Subscribe(eGRAPHREQUEST_PROGRESS, this);
                    mIsSubscribedToRequestProgress = true;
                }
                mRequest = FacebookSession::GetInstance()->Share(mParamsBundle, on_post_feed_request_completed, this);
                ret = mRequest;
                break;
            case OT_Album_Create:
                mRequest = FacebookSession::GetInstance()->PostCreateAlbum(mParamsBundle, on_post_feed_request_completed, this);
                ret = mRequest;
                break;
            case OT_Delete_Post:
                mRequest = FacebookSession::GetInstance()->DeletePost(mUserId.c_str(), on_post_delete_request_completed, this);
                ret = mRequest;
                break;
            case OT_Reload_Post:
                mRequest = FacebookSession::GetInstance()->GetPostDetails(mUserId.c_str(), on_post_reload_request_completed, this);
                ret = mRequest;
                break;
            case OT_Set_Custom_Privacy_Option_As_Default:
                mRequest = FacebookSession::GetInstance()->SetCustomPrivacyOptionAsDefault(mUserId.c_str(), on_set_default_privacy_completed, this);
                ret = mRequest;
                break;
            case OT_Set_Privacy_Option_As_Default:
                mRequest = FacebookSession::GetInstance()->SetDefaultPrivacyOption(mUserId.c_str(), on_set_default_privacy_completed, this);
                ret = mRequest;
                break;
            default:
                ret = DoCurrentStepExecute();
                break;
        }
        if(mRequest) {
            Log::debug_tag(SPOP_LOG_TAG, "SimplePostOperation::CurrentStepExecute subscribe");
            mRequest->Subscribe(this);
        }
    } else if (mCurrentStep == 2) {
        if(mRequest) {
            Log::debug_tag(SPOP_LOG_TAG, "SimplePostOperation::CurrentStepExecute unsubscribe");
            mRequest->UnSubscribe(this);
            GraphRequest* requestRawPtr = mRequest.Get();
            FacebookSession::GetInstance()->ReleaseGraphRequest(requestRawPtr);
            mRequest = nullptr;
        }
        ret = DoCurrentStepExecute();
        if(mRequest) {
            Log::debug_tag(SPOP_LOG_TAG, "SimplePostOperation::CurrentStepExecute subscribe");
            mRequest->Subscribe(this);
        }
    }
    return ret;
}

void SimplePostOperation::OnOperationComplete() {
    Log::debug_tag(SPOP_LOG_TAG, "SimplePostOperation::OnOperationComplete, mRequestError: `%d`, mErrorCode: `%d`", mRequestError, mErrorCode);

    mOperationResult = (mRequestError || IsArtificiallyFailed()) ? OR_Error : OR_Success;

    bool isFailedBecauseOfGoingOffline = IsFailedBecauseOfGoingOffline();
    if (isFailedBecauseOfGoingOffline) {// for mArtificialErrorCode this condition is ignored
        Log::debug_tag(SPOP_LOG_TAG, "SimplePostOperation::OnOperationComplete, operation was paused");
        PauseOperation(); // Note: mOperationResult becomes OR_Unknown.
    }

    Log::debug_tag(SPOP_LOG_TAG, "SimplePostOperation::OnOperationComplete, mOperationResult: `%d`", mOperationResult);
    switch(mOpType) {
        case OT_Reload_Post:
            if (mOperationResult == OR_Success && mPostDetailsResponse && mPostDetailsResponse->mPost) {
                AppEvents::Get().Notify(ePOST_RELOAD_EVENT, mPostDetailsResponse->mPost);
                CacheManager::GetInstance().UpdateFeedDataByPost(mPostDetailsResponse->mPost);
            } else if (!isFailedBecauseOfGoingOffline && !IsArtificiallyFailed()) { //operation is completed with unrecoverable error.
                if (mPostDetailsResponse && mPostDetailsResponse->mError && mPostDetailsResponse->mError->mCode == 100 && mPostDetailsResponse->mError->mErrorSubcode == 33) {
                    Utils::ShowToast(Application::GetInstance()->GetTopScreen()->getMainLayout(), i18n_get_text("IDS_POST_IS_NOT_AVAILABLE"));
                }
                AppEvents::Get().Notify(ePOST_UPDATE_ABORTED, (void *)mUserId.c_str());
            }
            delete mPostDetailsResponse;
            mPostDetailsResponse = nullptr;
        break;
        case OT_Album_Create:
            AppEvents::Get().Notify(eALBUM_CREATED_EVENT, NULL);
        break;
        case OT_Post_Create:
            if (mOperationResult == OR_Success) {
                if (mOpDestination == OD_Friend_Timeline) {
                    notification_status_message_post(i18n_get_text("IDS_POST_WAS_SUCCESSFUL"));
                } else if (mIsShareWebLink) {
                    notification_status_message_post(i18n_get_text("IDS_MSG_POST_SHARED"));
                }
            }
            if(mErrorCode == CURLE_OK) {
                Log::debug_tag(SPOP_LOG_TAG, "SimplePostOperation::OnOperationComplete unsubscribe");
                AppEvents::Get().Unsubscribe(eGRAPHREQUEST_PROGRESS, this);
            }
        break;
        case OT_Share_Post_Create:
            if (IsFeedOperation()) {
                if (mOperationResult == OR_Success) {
                    notification_status_message_post(i18n_get_text("IDS_MSG_POST_SHARED"));
                }
                if(mErrorCode == CURLE_OK) {
                    Log::debug_tag(SPOP_LOG_TAG, "SimplePostOperation::OnOperationComplete unsubscribe shared operation");
                    AppEvents::Get().Unsubscribe(eGRAPHREQUEST_PROGRESS, this);
                }
            }
            break;
        case OT_Delete_Post:
            if (IsArtificiallyFailed()) {
                Cancel();
            }
            break;
        default:
            break;
    }
}

void SimplePostOperation::on_post_feed_request_completed(void* object, char* respond, int code) {
    Log::debug_tag(SPOP_LOG_TAG, "SimplePostOperation::on_post_feed_request_completed, code: `%d`", code);

    Sptr<SimplePostOperation> oper(static_cast<SimplePostOperation*>(object));
    if (oper) {
        oper->mErrorCode = static_cast<CURLcode>(code);
        if (code == CURLE_OK) {
            if (respond) {
                Log::debug(LOG_FACEBOOK_OPERATION, "response: %s", respond);
                FbRespondPost * postRespond = FbRespondPost::createFromJson(respond);
                if (postRespond) {
                    if (postRespond->mError) {
                        Log::error(LOG_FACEBOOK_OPERATION, "error = %s", postRespond->mError->mMessage);
                        oper->mRequestError = true;
                        ShowErrorPopup(postRespond->mError);
                        oper->Cancel();
                    } else {
                        SoundNotificationPlayer::PlayNotification(SoundNotificationPlayer::eSoundPostMain);
                        Log::debug(LOG_FACEBOOK_OPERATION, "Share id = %s", postRespond->mId);
                    }
                    delete postRespond;
                } else {
                    Log::error(LOG_FACEBOOK_OPERATION, "Error parsing http respond");
                }
            } else {
                Log::error(LOG_FACEBOOK_OPERATION, "Http respond is corrupted");
                oper->mRequestError = true;
            }
        } else {
            Log::error(LOG_FACEBOOK_OPERATION, "Error sending http request");
            oper->mRequestError = true;
        }

        GraphRequest* requestRawPtr = oper->mRequest.Get();
        FacebookSession::GetInstance()->ReleaseGraphRequest(requestRawPtr);
        oper->CurrentStepComplete();
    }

    //Attention!! client should take care to free respond buffer
    free(respond);
}

void SimplePostOperation::on_post_edit_request_completed(void* object, char* respond, int code) {
    Log::debug_tag(SPOP_LOG_TAG, "SimplePostOperation::on_post_edit_request_completed");

    Sptr<SimplePostOperation> oper(static_cast<SimplePostOperation*>(object));
    if (oper) {
        oper->mErrorCode = static_cast<CURLcode>(code);
        if (code == CURLE_OK) {
            if (respond) {
                Log::debug(LOG_FACEBOOK_OPERATION, "SimplePostOperation::on_post_edit_request_completed->response: %s", respond);
                FbRespondEditPostMessage * postRespond = FbRespondEditPostMessage::createFromJson(respond);
                if (postRespond) {
                    if (postRespond->mError != NULL) {
                        Log::error(LOG_FACEBOOK_OPERATION, "SimplePostOperation::on_post_edit_request_completed->error = %s", postRespond->mError->mMessage);
                        oper->mRequestError = true;
                        if (postRespond->mError->mType == Error::eOAuthException && postRespond->mError->mCode == 100) {
                            Utils::ShowToast(Application::GetInstance()->GetTopScreen()->getMainLayout(), i18n_get_text("IDS_POST_IS_NOT_AVAILABLE"));
                            AppEvents::Get().Notify(ePOST_NOT_AVAILABLE, const_cast<char*>(oper->mUserId.c_str()));
                        }
                    } else {
                        Log::info(LOG_FACEBOOK_OPERATION, "SimplePostOperation::on_post_edit_request_completed->ReloadPost");
                        OperationManager::GetInstance()->AddOperation(MakeSptr<SimplePostOperation>(OT_Reload_Post, oper->mUserId, nullptr, std::list<Friend>(), "", nullptr));
                    }
                    delete postRespond;

                } else {
                    Log::error(LOG_FACEBOOK_OPERATION, "SimplePostOperation::on_post_edit_request_completed->Error parsing http respond");
                    oper->mRequestError = true;
                }
            } else {
                Log::error(LOG_FACEBOOK_OPERATION, "SimplePostOperation::on_post_edit_request_completed->Http respond is corrupted");
                oper->mRequestError = true;
            }
        } else {
            Log::error(LOG_FACEBOOK_OPERATION, "SimplePostOperation::on_post_edit_request_completed->Error sending http request");
        }

        GraphRequest* requestRawPtr = oper->mRequest.Get();
        FacebookSession::GetInstance()->ReleaseGraphRequest(requestRawPtr);
        oper->CurrentStepComplete();
    }

    free(respond);
}

bundle* SimplePostOperation::GetBundle() {
    return mParamsBundle;
}

const char * SimplePostOperation::GetDestinationId() {
    return mUserId.c_str();
}

void SimplePostOperation::on_post_delete_request_completed(void* object, char* respond, int code) {
    Log::debug_tag(SPOP_LOG_TAG, "SimplePostOperation::on_post_delete_request_completed");

    Sptr<SimplePostOperation> oper(static_cast<SimplePostOperation*>(object));
    if (oper) {
        oper->mErrorCode = static_cast<CURLcode>(code);
        GraphRequest* requestRawPtr = oper->mRequest.Get();
        FacebookSession::GetInstance()->ReleaseGraphRequest(requestRawPtr);
        oper->CurrentStepComplete();
    }

    if (code == CURLE_OK && strstr(respond, "<") == 0) {
        Log::debug(LOG_FACEBOOK_OPERATION, "SimplePostOperation::on_post_delete_request_completed->response: %s", respond);
        FbRespondBase * postRespond = FbRespondBase::createFromJson(respond);

        if (!postRespond) {
            Log::error(LOG_FACEBOOK_OPERATION, "SimplePostOperation::on_post_delete_request_completed->Error parsing http respond");
        } else {
            if (postRespond->mError) {
                Log::error(LOG_FACEBOOK_OPERATION, "SimplePostOperation::on_post_delete_request_completed->error message = %s", postRespond->mError->mMessage);
            } else {
                Log::debug(LOG_FACEBOOK_OPERATION, "SimplePostOperation::on_post_delete_request_completed->response = %d", postRespond->mSuccess);
                Utils::ShowToast(Application::GetInstance()->GetTopScreen()->getMainLayout(), i18n_get_text("IDS_POST_DELETED"));
                AppEvents::Get().Notify(ePOST_DELETION_CONFIRMED,const_cast<char*>(oper->mUserId.c_str()));
            }
            delete postRespond;
        }
    } else {
        AppEvents::Get().Notify(ePOST_DELETION_CONFIRMED,const_cast<char*>(oper->mUserId.c_str()));
        // we suppose that post has been already deleted (because of request has been sent on server)
        oper->Cancel();
        Log::error(LOG_FACEBOOK_OPERATION, "SimplePostOperation::on_post_delete_request_completed->Error sending http request");
    }

    //Attention!! client should take care to free respond buffer
    free(respond);
}

void SimplePostOperation::on_post_reload_request_completed(void* object, char* response, int code) {
    Log::debug_tag(SPOP_LOG_TAG, "SimplePostOperation::on_post_reload_request_completed");

    Sptr<SimplePostOperation> oper(static_cast<SimplePostOperation *>(object));
    if (oper) {
        oper->mErrorCode = static_cast<CURLcode>(code);
        GraphRequest* requestRawPtr = oper->mRequest.Get();
        FacebookSession::GetInstance()->ReleaseGraphRequest(requestRawPtr);
        oper->CurrentStepComplete();
    }

    if (code == CURLE_OK) {
        Log::debug(LOG_FACEBOOK_OPERATION, "response: %s", response);
        oper->mPostDetailsResponse = FbRespondGetPostDetails::createFromJson(response);
        if (oper->mPostDetailsResponse) {
            if (!oper->mPostDetailsResponse->mError) {
                Log::debug(LOG_FACEBOOK_OPERATION, "on_post_reload_request_completed() is successfully completed.");
                Post *post = static_cast<Post*>(oper->mSharedActionAndPost->mData);
                if(post && post->IsProfilePhotoPost()) {
                    oper->mPostDetailsResponse->mPost->SetProfilePictureStory(post->GetStory().c_str());
                    oper->mPostDetailsResponse->mPost->SetStoryTagsList(post->GetStoryTagsList());
                }
            } else {
                oper->mRequestError = true;
                Log::error(LOG_FACEBOOK_OPERATION, "Error returned by FB server:code[%d], message:[%s]", oper->mPostDetailsResponse->mError->mCode, oper->mPostDetailsResponse->mError->mMessage);
            }
        } else {
            oper->mRequestError = true;
            Log::error(LOG_FACEBOOK_OPERATION, "Error parsing http respond");
        }
    } else {
        Log::error(LOG_FACEBOOK_OPERATION, "Error sending http request");
    }

    //Attention!! client should take care to free response buffer
    free(response);
}

void SimplePostOperation::on_set_default_privacy_completed(void* object, char* response, int code) {
    Log::debug_tag(SPOP_LOG_TAG, "SimplePostOperation::on_set_default_privacy_completed");

    if (code == CURLE_OK) {
        Log::debug(LOG_FACEBOOK_OPERATION, "response: %s", response);
        FbRespondBase *setPrivacyResponse = FbRespondBase::createFromJson(response);
        if (setPrivacyResponse == NULL) {
            Log::error(LOG_FACEBOOK_OPERATION, "Error parsing http response");
        } else {
            if (setPrivacyResponse->mError != NULL) {
                Log::error(LOG_FACEBOOK_OPERATION, "Error posting default privacy, error = %s", setPrivacyResponse->mError->mMessage);
            } else {
                Log::debug(LOG_FACEBOOK_OPERATION, "Success = %d", setPrivacyResponse->mSuccess);
                // On success update privacy options with a new values.
                Application::GetInstance()->mPrivacyOptionsData->GetPrivacyOptionsFromFB(NULL);
            }
            delete setPrivacyResponse;
        }
    } else {
        Log::error(LOG_FACEBOOK_OPERATION, "Error sending http request");
    }

    Sptr<SimplePostOperation> oper(static_cast<SimplePostOperation*>(object));
    if (oper) {
        oper->mErrorCode = static_cast<CURLcode>(code);
        GraphRequest* requestRawPtr = oper->mRequest.Get();
        FacebookSession::GetInstance()->ReleaseGraphRequest(requestRawPtr);
        oper->CurrentStepComplete();
    }

    //Attention!! client should take care to free respond buffer
    free(response);
}

bool SimplePostOperation::Start() {
    Log::debug_tag(SPOP_LOG_TAG, "SimplePostOperation::Start");
    bool res = Operation::Start();
    if(res && ((mOpType == OT_Post_Create && mIsShareWebLink) || mOpType == OT_Share_Post_Create)) { //For example, (mOpType == OT_Post_Create && mIsShareWebLink) is true when we share link from Web browser.
        notification_status_message_post(i18n_get_text("IDS_SHARE_POSTING"));
    }
    return res;
}

void SimplePostOperation::Update(AppEventId eventId, void * data) {
    Log::debug_tag(SPOP_LOG_TAG, "SimplePostOperation::Update");
    if(eGRAPHREQUEST_PROGRESS == eventId) {
        Sptr<GraphRequest> request = static_cast<GraphRequest*>(data);
        Log::debug_tag(SPOP_LOG_TAG, "SimplePostOperation::Update");
        if(request == mRequest) {
            int percent = request->GetProgress()*100;
            double progress = (double)percent/100;
            if(progress > mPrevProgress) {
                Notify(eOPERATION_STEP_PROGRESS_EVENT, &progress);
                mPrevProgress = progress;
            }
        }
    }
}

bool SimplePostOperation::Cancel() {
    Log::debug("SimplePostOperation::Cancel");

    if (mRequest) {
        GraphRequest* requestRawPtr = mRequest.Get();
        FacebookSession::GetInstance()->ReleaseGraphRequest(requestRawPtr);
    }
    Operation::Cancel();
    AppEvents::Get().Unsubscribe(eGRAPHREQUEST_PROGRESS, this);

    if(mOpType == OT_Delete_Post ) {
        Utils::ShowToast(Application::GetInstance()->GetTopScreen()->getMainLayout(), i18n_get_text("IDS_PROBLEM_DELETING_STORY"));
    }
    return true;
}

void SimplePostOperation::ResetOperation() {
    Log::debug_tag(SPOP_LOG_TAG, "SimplePostOperation::ResetOperation");
    Operation::ResetOperation();
    mPrevProgress = 0.0;
}

bool SimplePostOperation::IsFeedOperation() {
    return (GetType() == Operation::OT_Post_Create || GetType() == OT_Share_Post_Create) &&
           (GetDestination() == Operation::OD_Home || GetDestination() == Operation::OD_Group || GetDestination() == Operation::OD_Friend_Timeline);
}

void SimplePostOperation::PutInErrorState() {
    if(mRequest) {
        mRequest->Cancel();
        GraphRequest* requestRawPtr = mRequest.Get();
        FacebookSession::GetInstance()->ReleaseGraphRequest(requestRawPtr);
        mRequest = nullptr;
    }
    Operation::PutInErrorState();
}

ActionAndData * SimplePostOperation::GetSharedActionAndPost() {
    return mSharedActionAndPost;
}
