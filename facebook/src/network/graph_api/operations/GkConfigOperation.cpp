#include "Application.h"
#include "FacebookSession.h"
#include "GkConfigOperation.h"
#include "FbRespondGetFeatureState.h"
#include "Log.h"
#include "Utils.h"

#include <app_preference.h>

#define APP_GK_CONFIG "fb4t_feature_config_graphql_enabled"
#define GK_CONFIG_GRAPHQL_INDEX 0
#define PREFERENCE_KEY_GRAPHQL "IS_GRAPHQL_ALLOWED"

static constexpr char GKOP_LOG_TAG[] = "Facebook::GkConfigOperation";

/*
 * This operation is designed to work with GateKeeper service.
 * It allows to read app configuration and notify subscribers.
 */
GkConfigOperation::GkConfigOperation(OperationType opType) : Operation({}, "") {
    mUserId = "";
    mRequest = nullptr;
    mOpType = opType;
    mStepCount = 1;
}

GkConfigOperation::~GkConfigOperation() {
    FacebookSession::GetInstance()->ReleaseGraphRequest(mRequest);
    Log::debug_tag(GKOP_LOG_TAG, "GkConfigOperation::~GkConfigOperation end");
}

bool GkConfigOperation::CurrentStepExecute() {
    if (mRequest) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(mRequest);
    }

    switch (mOpType) {
    case OT_Get_App_Config: {
            mRequest = FacebookSession::GetInstance()->GetFeatureState(APP_GK_CONFIG, on_get_app_config_completed, this);
            Log::debug_tag(GKOP_LOG_TAG, "GkConfigOperation::CurrentStepExecute() submitted new GetFeatureState Graph request");
        }
        break;
    default:
        break;
    }
    return mRequest;
}

void GkConfigOperation::on_get_app_config_completed(void* object, char* response, int code) {
    assert(object);
    GkConfigOperation *me = static_cast<GkConfigOperation *>(object);
    FacebookSession::GetInstance()->ReleaseGraphRequest(me->mRequest);

    me->mRequest = nullptr;
    me->mErrorCode = static_cast<CURLcode>(code);

    if (CURLE_OK == code) {
        Log::debug_tag(GKOP_LOG_TAG,"GkConfigOperation::on_get_app_config_completed->response: %s", response);

        FbRespondGetFeatureState* respondObj = FbRespondGetFeatureState::createFromJson(response);
        if (!respondObj) {
            Log::error("GkConfigOperation::on_get_app_config_completed->Can't parse response.");
        } else {
            if (respondObj->mErrorMessage) {
                Log::info("GkConfigOperation::on_get_app_config_completed->error_code = %d", respondObj->mErrorCode);
                if (respondObj->mErrorCode == 101
                        || respondObj->mErrorCode == 104
                        || respondObj->mErrorCode == 7501) {
                    Log::debug_tag(GKOP_LOG_TAG,"GkConfigOperation::on_get_app_config_completed->Invalid request params.");
                } else if (respondObj->mErrorCode == 100
                        || respondObj->mErrorCode == 7500) {
                    Log::debug_tag(GKOP_LOG_TAG,"GkConfigOperation::on_get_app_config_completed->Incomplete request.");
                } else if (respondObj->mErrorCode == 12) {
                    Log::debug_tag(GKOP_LOG_TAG,"GkConfigOperation::on_get_app_config_completed->API is deprecated.");
                } else {
                    Log::debug_tag(GKOP_LOG_TAG,"GkConfigOperation::on_get_app_config_completed->Unhandled error occurred.");
                }
                AppEvents::Get().Notify(eCONFIG_UPDATE_ERROR_EVENT);
            } else {
                const char* config = respondObj->getConfig();
                preference_set_boolean(PREFERENCE_KEY_GRAPHQL, config[GK_CONFIG_GRAPHQL_INDEX] == '1');
                AppEvents::Get().Notify(eCONFIG_UPDATE_COMPLETED_EVENT);
            }
        }
        delete respondObj;
        me->CurrentStepComplete();
    } else {
        me->PutInErrorState();
        Log::error_tag(GKOP_LOG_TAG, "GkConfigOperation::on_get_app_config_completed->Http error occurred, curl code = %d", code);
    }

    free(response);
}
