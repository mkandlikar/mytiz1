#include "Application.h"
#include "FacebookSession.h"
#include "FriendOperation.h"
#include "FriendRequestsBatchProvider.h"
#include "Log.h"
#include "OwnFriendsProvider.h"
#include "ScreenBase.h"
#include "Utils.h"

static constexpr char FOP_LOG_TAG[] = "Facebook::FriendOperation";

Eina_List *FriendOperation::mFriendOperationsList = nullptr;

/*
 * Class FriendOperation
 */
FriendOperation::FriendOperation(OperationType opType, const char *userId, const char *postId) : Operation({}, "") {
    mOpType = opType;
    mStepCount = 1;
    mUserId = userId ? userId : "";
    mRequest = nullptr;
    mFriendOperationsList = eina_list_append(mFriendOperationsList, this);
    mFriendRequestConfirmed = false;
    mUpdateUnfriendedUserStatus = false;
    mPostId = postId ? postId : "";
}

FriendOperation::~FriendOperation() {
    if (mRequest) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(mRequest);
    }
    mFriendOperationsList = eina_list_remove(mFriendOperationsList, this);
    SendPostponedEvent();
}

void FriendOperation::SendPostponedEvent() {
// send event to update UserFriends screen after operation has been removed from operation list
    if(mFriendRequestConfirmed) {// postponed events for user friends screen
        AppEvents::Get().Notify(eFRIEND_REQUEST_CONFIRMED, const_cast<char*>(mUserId.c_str()));
    }
    if(mUpdateUnfriendedUserStatus) {
        AppEvents::Get().Notify(eUPDATE_UNFRIENDED_USER_STATUS, const_cast<char*>(mUserId.c_str()));
    }
}

bool FriendOperation::IsOperationStarted(const char *userId) {
    bool result = eina_list_search_unsorted_list(mFriendOperationsList, friend_operations_comparator, userId);
    return result;
}

int FriendOperation::friend_operations_comparator(const void * object, const void * id) {
    const FriendOperation *oper = static_cast<const FriendOperation *>(object);
    if (oper && id && !strcmp(oper->mUserId.c_str() , (const char*)id)) {
        return 0;
    } else {
        return -1;
    }
}

bool FriendOperation::CurrentStepExecute() {
    if (mRequest) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(mRequest);
    }

    switch (mOpType) {
    case OT_Unfriend: {
            mRequest = FacebookSession::GetInstance()->Unfriend(mUserId.c_str(), on_unfriend_friend_completed, this);
            Log::debug_tag(FOP_LOG_TAG, "FriendOperation::CurrentStepExecute() submitted new Unfriend Graph request");
        }
        break;
    case OT_Block: {
            mRequest = FacebookSession::GetInstance()->BlockUser(mUserId.c_str(), on_block_completed, this);
            Log::debug_tag(FOP_LOG_TAG, "FriendOperation::CurrentStepExecute() submitted new BlockUser Graph request");
        }
        break;
    case OT_ConfirmFriend: {
            mRequest = FacebookSession::GetInstance()->ConfirmFriendRequest(mUserId.c_str(), true, on_confirm_friend_completed, this);
            Log::debug_tag(FOP_LOG_TAG, "FriendOperation::CurrentStepExecute() submitted new ConfirmFriend(true) Graph request");
        }
        break;
    case OT_NotConfirmFriend: {
            mRequest = FacebookSession::GetInstance()->ConfirmFriendRequest(mUserId.c_str(), false, on_confirm_friend_completed, this);
            Log::debug_tag(FOP_LOG_TAG, "FriendOperation::CurrentStepExecute() submitted new ConfirmFriend(false) Graph request");
        }
        break;
    case OT_Unfollow: {
            mRequest = FacebookSession::GetInstance()->UnFollowUser(mUserId.c_str(), on_unfollow_completed, this);
            Log::debug_tag(FOP_LOG_TAG, "FriendOperation::CurrentStepExecute() submitted new UnFollowUser Graph request");
        }
        break;
    case OT_Follow: {
            mRequest = FacebookSession::GetInstance()->FollowUser(mUserId.c_str(), on_follow_completed, this);
            Log::debug_tag(FOP_LOG_TAG, "FriendOperation::CurrentStepExecute() submitted new FollowUser Graph request");
        }
        break;
    default:
        break;
    }
    return mRequest;
}

void FriendOperation::on_unfriend_friend_completed(void* object, char* response, int code)
{
    Log::debug_tag(FOP_LOG_TAG, "FriendOperation::on_unfriend_friend_completed()");
    FriendOperation *me = static_cast<FriendOperation *>(object);
    assert(me);

    FacebookSession::GetInstance()->ReleaseGraphRequest(me->mRequest);
    me->mRequest = nullptr;

    me->mErrorCode = static_cast<CURLcode>(code);
    if (CURLE_OK == code) {
        Log::debug_tag(FOP_LOG_TAG, "FriendOperation::on_unfriend_friend_completed()->response: %s", response);

        FbRespondBase * unfriendResponse = FbRespondBase::createFromJson(response);
        if(unfriendResponse) {
            if (unfriendResponse->mError && unfriendResponse->mError->mCode != 100) {
                Log::debug_tag(FOP_LOG_TAG, "FriendOperation::on_unfriend_friend_completed()->Failed to unfriend, errorMsg: '%s'", unfriendResponse->mError->mMessage);
                AppEvents::Get().Notify(eUNFRIEND_FRIEND_ERROR, nullptr);//comes to ProfileScreen
                Utils::ShowToast(Application::GetInstance()->GetTopScreen()->getMainLayout(), i18n_get_text("IDS_SOMETHING_WENT_WRONG"));
             } else {
                // error 100 comes if user was already unfriended outside - this case will be considered as success
                Friend *friends = OwnFriendsProvider::GetInstance()->GetFriendById(me->mUserId.c_str());
                if (friends) {
                    Log::debug_tag(FOP_LOG_TAG, "FriendOperation::on_unfriend_friend_completed()->Successfully unfriended");
                    AppEvents::Get().Notify(eFRIEND_UNFRIENDED, friends);// for own friends list
                    OwnFriendsProvider::GetInstance()->DeleteItem(friends);
                }
            }
            me->mUpdateUnfriendedUserStatus = true;//postponed event to update status in or friends lists
            delete unfriendResponse;
        }
        me->CurrentStepComplete();
    } else {
        me->Cancel();
        AppEvents::Get().Notify(eUNFRIEND_FRIEND_ERROR, nullptr);
        Utils::ShowToast(Application::GetInstance()->GetTopScreen()->getMainLayout(), i18n_get_text("IDS_SOMETHING_WENT_WRONG"));
        Log::error_tag(FOP_LOG_TAG, "FriendOperation::on_unfriend_friend_completed()->Error sending http request, curl code=%d", code);
    }

    //Attention!! client should take care to free response buffer
    free(response);
}

void FriendOperation::on_block_completed(void* object, char* response, int code)
{
    Log::debug_tag(FOP_LOG_TAG, "FriendOperation::on_block_completed()");
    FriendOperation *me = static_cast<FriendOperation *>(object);
    assert(me);

    FacebookSession::GetInstance()->ReleaseGraphRequest(me->mRequest);
    me->mRequest = nullptr;

    me->mErrorCode = static_cast<CURLcode>(code);
    if (CURLE_OK == code) {
        Log::debug_tag(FOP_LOG_TAG, "FriendOperation::on_block_completed()->response: %s", response);

        FbRespondBase * blockResponse = FbRespondBase::createFromJson(response);
        if(blockResponse) {
            if (blockResponse->mError) {
                Log::debug_tag(FOP_LOG_TAG, "FriendOperation::on_block_completed()->Failed to block, errorMsg: '%s'", blockResponse->mError->mMessage);
                AppEvents::Get().Notify(eBLOCK_FRIEND_ERROR, nullptr);
                if (3802 == blockResponse->mError->mCode) {
                    WidgetFactory::ShowErrorDialog(Application::GetInstance()->GetTopScreen()->getMainLayout(), "IDS_CANNOT_REBLOCK_USER");
                } else {
                    Utils::ShowToast(Application::GetInstance()->GetTopScreen()->getMainLayout(), i18n_get_text("IDS_SOMETHING_WENT_WRONG"));
                }
            } else {
                Log::debug_tag(FOP_LOG_TAG, "FriendOperation::on_block_completed()->Success");
                Friend *friends = OwnFriendsProvider::GetInstance()->GetFriendById(me->mUserId.c_str());
                if (friends) {
                    Log::debug_tag(FOP_LOG_TAG, "FriendOperation::on_block_completed()->Successfully blocked friend");
                    OwnFriendsProvider::GetInstance()->DeleteItem(friends);
                    AppEvents::Get().Notify(eDELETE_BLOCKED_FRIEND_FROM_MY_LIST, friends);
                }
                AppEvents::Get().Notify(ePYMK_ITEM_DELETE_EVENT, const_cast<char*>(me->mUserId.c_str()));//to delete from PYMK list and FriendRequests/PYMK carousels
                AppEvents::Get().Notify(eFR_ITEM_DELETE_EVENT, const_cast<char*>(me->mUserId.c_str()));//to delete from FriendRequests list
                AppEvents::Get().Notify(eDELETE_BLOCKED_FRIEND_FROM_OTHER_LIST, const_cast<char*>(me->mUserId.c_str()));// even user not in provider
                AppEvents::Get().Notify(eREMOVE_TOP_BLOCKED_SCREEN, nullptr);
            }
            delete blockResponse;
        }
        me->CurrentStepComplete();
    } else {
        me->Cancel();
        Utils::ShowToast(Application::GetInstance()->GetTopScreen()->getMainLayout(), i18n_get_text("IDS_SOMETHING_WENT_WRONG"));
        AppEvents::Get().Notify(eBLOCK_FRIEND_ERROR, nullptr);
        Log::error_tag(FOP_LOG_TAG, "FriendOperation::on_block_completed->Error sending http request, curl code=%d", code);
    }
    //Attention!! client should take care to free response buffer
    free(response);
}

void FriendOperation::on_confirm_friend_completed(void* object, char* response, int code)
{
    Log::debug_tag(FOP_LOG_TAG, "FriendOperation::on_confirm_friend_completed()");
    FriendOperation *me = static_cast<FriendOperation *>(object);
    assert(me);

    FacebookSession::GetInstance()->ReleaseGraphRequest(me->mRequest);
    me->mRequest = nullptr;

    me->mErrorCode = static_cast<CURLcode>(code);
    if (CURLE_OK == code) {
        Log::debug_tag(FOP_LOG_TAG,"FriendOperation::on_confirm_friend_completed->response: %s", response);

        FbRespondBaseRestApi * confirmRespond = FbRespondBaseRestApi::createFromJson(response);
        if (!confirmRespond) {
            Log::error("FriendOperation::on_confirm_friend_completed->Error parsing http respond");
        } else {
            int error_code = confirmRespond->mErrorCode;
            Log::info("FriendOperation::on_confirm_friend_completed->error_code = %d", error_code);
            User *user = FriendRequestsBatchProvider::GetInstance()->GetFrDataById(me->mUserId.c_str());
            if (confirmRespond->mErrorMessage) {
                if (error_code == 536) {
                    Log::info("FriendOperation::on_confirm_friend_completed->error 536");
                    AppEvents::Get().Notify(eFRIEND_REQUEST_CONFIRM_DELETE_ERROR, const_cast<char*>(me->mUserId.c_str()));
                    WidgetFactory::ShowErrorDialog(Application::GetInstance()->GetTopScreen()->getMainLayout(), "IDS_FRIENDS_ERROR_536");
                } else if (error_code == 522) {
                    if (user) {
                        user->mFriendshipStatus = Person::eARE_FRIENDS;
                        OwnFriendsProvider::GetInstance()->AddItem(user);
                        AppEvents::Get().Notify(eFR_ITEM_DELETE_EVENT, const_cast<char*>(me->mUserId.c_str()));
                    }
                    me->mFriendRequestConfirmed = true;
                    Log::info("FriendOperation::on_confirm_friend_completed->error 522");
                } else { // try to set to default state with "confirm/delete" buttons
                    if (user) {
                        user->mFriendRequestStatus = eUSER_REQUEST_UNKNOWN;
                        AppEvents::Get().Notify(eFR_STATUS_CHANGE_EVENT, user);
                    }
                    Log::info("FriendOperation::on_confirm_friend_completed->other error");
                }
            } else {
                if (me->mOpType == Operation::OT_ConfirmFriend) {
                    if (user) {
                        user->mFriendshipStatus = Person::eARE_FRIENDS;
                        OwnFriendsProvider::GetInstance()->AddItem(user);
                    }
                    me->mFriendRequestConfirmed = true;//postponed event
                }
                AppEvents::Get().Notify(eFR_ITEM_DELETE_EVENT, const_cast<char*>(me->mUserId.c_str()));// delete from every screen
            }
        }
        delete confirmRespond;
        me->CurrentStepComplete();
    } else {
        me->PutInErrorState();
        Log::error_tag(FOP_LOG_TAG, "FriendOperation::on_confirm_friend_completed->Error sending http request, curl code=%d", code);
    }

    //Attention!! client should take care to free response buffer
    free(response);
}

void FriendOperation::on_unfollow_completed(void* object, char* respond, int code)
{
    assert(object);
    FriendOperation *me = static_cast<FriendOperation *>(object);
    FacebookSession::GetInstance()->ReleaseGraphRequest(me->mRequest);
    me->mRequest = nullptr;

    me->mErrorCode = static_cast<CURLcode>(code);

    if (code == CURLE_OK) {
        Log::debug("response: %s", respond);
        FbRespondBase *unfollowRespond = FbRespondBase::createFromJson(respond);

        if (unfollowRespond) {
            if (unfollowRespond->mError) {
                Log::debug_tag(FOP_LOG_TAG, "FriendOperation::on_unfollow_completed()->Failed to unfollow, errorMsg: '%s'", unfollowRespond->mError->mMessage);
            } else {
                AppEvents::Get().Notify(eUNFOLLOW_COMPLETED, const_cast<char*>(me->mPostId.c_str()));
            }
        }
        me->CurrentStepComplete();
        delete unfollowRespond;
    } else {
        me->PutInErrorState();
    }
    free(respond);
}

void FriendOperation::on_follow_completed(void* object, char* respond, int code)
{
    assert(object);
    FriendOperation *me = static_cast<FriendOperation *>(object);
    FacebookSession::GetInstance()->ReleaseGraphRequest(me->mRequest);
    me->mRequest = nullptr;

    me->mErrorCode = static_cast<CURLcode>(code);
    if (code == CURLE_OK) {
        FbRespondBase *followRespond = FbRespondBase::createFromJson(respond);
        if (followRespond) {
            if (followRespond->mError) {
                Log::debug_tag(FOP_LOG_TAG, "FriendOperation::on_follow_completed()->Failed to follow, errorMsg: '%s'", followRespond->mError->mMessage);
            } else {
                AppEvents::Get().Notify(eFOLLOW_COMPLETED, const_cast<char*>(me->mPostId.c_str()));
            }
        }
        me->CurrentStepComplete();
        delete followRespond;
    } else {
        me->PutInErrorState();
    }
    free(respond);
}
