#include "PhotoTagOperation.h"
#include "FacebookSession.h"
#include "PhotoTagging.h"

/*
 * Class PhotoTagOperation
 */
Eina_List *PhotoTagOperation::mPhotoTagOperationsList = nullptr;
PhotoTagOperation::PhotoTagOperation(OperationType opType, const char *photoId, Eina_List * tags) :
        Operation( { }, ""), mPhotoTagList(tags), mPhotoId(photoId), mOpType(opType), mRequest(nullptr) {
    mPhotoTagOperationsList = eina_list_append(mPhotoTagOperationsList, this);
    mStepCount = 1;
}
PhotoTagOperation::~PhotoTagOperation() {
    if (mRequest) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(mRequest);
    }
    mPhotoTagOperationsList = eina_list_remove(mPhotoTagOperationsList, this);
    void *listData;
    EINA_LIST_FREE(mPhotoTagList, listData) {
        delete static_cast<PhotoTagging*>(listData);
    }
}
bool PhotoTagOperation::CurrentStepExecute() {
    if (mRequest) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(mRequest);
    }
    switch (mOpType) {
        case OT_DeletePhotoTag: {
            mRequest = FacebookSession::GetInstance()->DeletePhotoTags(mPhotoId, mPhotoTagList, on_delete_tag_completed, this);
        }
            break;
        case OT_CreatePhotoTag: {
            mRequest =FacebookSession::GetInstance()->CreatePhotoTags(mPhotoId, mPhotoTagList, on_create_tag_completed, this);
        }
            break;
        default:
            break;
    }
    return mRequest;
}
void PhotoTagOperation::on_create_tag_completed(void* object, char* response, int code) {
    PhotoTagOperation *me = static_cast<PhotoTagOperation *>(object);
    assert(me);
    FacebookSession::GetInstance()->ReleaseGraphRequest(me->mRequest);
    me->mRequest = nullptr;
    me->mErrorCode = static_cast<CURLcode>(code);
    if (CURLE_OK == code) {
        Log::debug("PhotoTagOperation::on_create_tag_completed->response: %s", response);

        FbRespondBase * resp = FbRespondBase::createFromJson(response);
        if (resp) {
            if (resp->mError) {
                Log::debug("Failed create tag, error: '%s'", resp->mError->mMessage);
            } else if (resp->mSuccess) {
                Log::debug("PhotoTagOperation::on_create_tag_completed->Successfully ");
            }
            delete resp;
        }
        me->CurrentStepComplete();
    } else {
        me->PutInErrorState();
        Log::error("PhotoTagOperation::on_create_tag_completed->Error sending http request, curl code=%d", code);
    }
    free(response);
}
void PhotoTagOperation::on_delete_tag_completed(void* object, char* response, int code) {
    PhotoTagOperation *me = static_cast<PhotoTagOperation *>(object);
    assert(me);
    FacebookSession::GetInstance()->ReleaseGraphRequest(me->mRequest);
    me->mRequest = nullptr;
    me->mErrorCode = static_cast<CURLcode>(code);
    if (CURLE_OK == code) {
        Log::debug("PhotoTagOperation::on_delete_tag_completed->response: %s", response);
        FbRespondBase * resp = FbRespondBase::createFromJson(response);
        if (resp) {
            if (resp->mError) {
                Log::debug("Failed delete tag, error: '%s'", resp->mError->mMessage);
            } else if (resp->mSuccess) {
                Log::debug("PhotoTagOperation::on_delete_tag_completed->Successfully ");
            }
            delete resp;
        }
        me->CurrentStepComplete();
    } else {
        me->PutInErrorState();
        Log::error("PhotoTagOperation::on_delete_tag_completed->Error sending http request, curl code=%d", code);
    }
    free(response);
}
