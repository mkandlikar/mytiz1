#include "Application.h"
#include "CacheManager.h"
#include "FacebookSession.h"
#include "FbRespondPost.h"
#include "Log.h"
#include "OperationManager.h"
#include "PostUpdateOperation.h"
#include "PostComposerMediaData.h"
#include "ScreenBase.h"
#include "SimplePostOperation.h"

static constexpr char PUOP_LOG_TAG[] = "Facebook::PostUpdateOperation";

PostUpdateOperation::PostUpdateOperation(OperationType opType,
                                         const std::string& postId,
                                         bundle* paramsBundle,
                                         const std::list<Friend>& taggedFriends,
                                         const std::string& privacyType,
                                         const PCPlace* place,
                                         const char *coverOrProfilePhotoId,
                                         Eina_List *mediaList,
                                         Eina_List *mediaListToDelete,
                                         Post *post):
      Operation(taggedFriends, privacyType, place),
      mRequest(nullptr),
      mMediaList(nullptr),
      mMediaListToDelete(nullptr) {

    Log::debug_tag(PUOP_LOG_TAG, "PostUpdateOperation::PostUpdateOperation");
    mOpType = opType;

    mStepCount = 2;
    mPostId = postId;
    mParamsBundle = paramsBundle ? bundle_dup(paramsBundle) : NULL;
    mCoverOrProfilePhotoId = SAFE_STRDUP(coverOrProfilePhotoId);

    if (mediaList) {
        Eina_List *listItem;
        void *listData;
        EINA_LIST_FOREACH(mediaList, listItem, listData) {
            PostComposerMediaData *media = static_cast<PostComposerMediaData *>(listData);
            PostComposerMediaData *mediaData = new PostComposerMediaData(*media);
            mMediaList = eina_list_append(mMediaList, mediaData);
        }
    }

    if (mediaListToDelete) {
        Eina_List *listItem;
        void *listData;
        EINA_LIST_FOREACH(mediaListToDelete, listItem, listData) {
            PostComposerMediaData *media = static_cast<PostComposerMediaData *>(listData);
            PostComposerMediaData *mediaData = new PostComposerMediaData(*media);
            mMediaListToDelete = eina_list_append(mMediaListToDelete, mediaData);
        }
    }

    mPost = post;
}

PostUpdateOperation::~PostUpdateOperation() {
    Log::debug_tag(PUOP_LOG_TAG, "PostUpdateOperation::~PostUpdateOperation start");
    if (mParamsBundle) {
        bundle_free(mParamsBundle);
    }
    if (mRequest) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(mRequest);
        mRequest = nullptr;
    }
    free(mCoverOrProfilePhotoId);

    void * listData;
    EINA_LIST_FREE(mMediaList, listData) {
        (static_cast<PostComposerMediaData *>(listData))->Release();
    }

    EINA_LIST_FREE(mMediaListToDelete, listData) {
        (static_cast<PostComposerMediaData *>(listData))->Release();
    }

    Log::debug_tag(PUOP_LOG_TAG, "PostUpdateOperation::~PostUpdateOperation end");
}

bool PostUpdateOperation::CurrentStepExecute() {
    Log::debug_tag(PUOP_LOG_TAG, "PostUpdateOperation::CurrentStepExecute with %d", mCurrentStep);

    bool retVal = true;
    AppEvents::Get().Notify(ePOST_MESSAGE_EVENT, (void *) mPostId.c_str());

    if (mCurrentStep == 1) {
        if (eina_list_count(mMediaListToDelete) > 0) {
            DeleteNextPhoto();
        } else {
            CurrentStepComplete();
        }
    } else if (mCurrentStep == 2) {
        if (mParamsBundle || eina_list_count(mMediaList) > 0) {
            retVal = UpdatePostMsgAndPhotoCaptions();
        } else {
            CurrentStepComplete();
        }
    }

    return retVal;
}

void PostUpdateOperation::OnOperationComplete() {
    Log::debug_tag(PUOP_LOG_TAG, "PostUpdateOperation::OnOperationComplete, mRequestError: `%d`, mErrorCode: `%d`", mRequestError, mErrorCode);

    mOperationResult = (mRequestError || IsArtificiallyFailed()) ? OR_Error : OR_Success;

    bool isFailedBecauseOfGoingOffline = IsFailedBecauseOfGoingOffline();
    if (isFailedBecauseOfGoingOffline) {
        Log::debug_tag(PUOP_LOG_TAG, "PostUpdateOperation::OnOperationComplete, operation was paused");
        PauseOperation();
    }

    Log::debug_tag(PUOP_LOG_TAG, "PostUpdateOperation::OnOperationComplete, mOperationResult: `%d`", mOperationResult);

    // TODO move to on_post_with_photos_update_request_completed ?
    if (mOperationResult == OR_Success) {
        CacheManager::GetInstance().UpdateFeedDataById(mPostId.c_str());
    }

    if (mIsPostReloadRequired) {
        Log::info(LOG_FACEBOOK_OPERATION, "PostUpdateOperation::OnOperationComplete->ReloadPost");
        OperationManager::GetInstance()->AddOperation(MakeSptr<SimplePostOperation>(OT_Reload_Post, mPostId, nullptr, std::list<Friend>(), "", nullptr, Operation::OD_None, mPost, nullptr));
    } else {
        if (mOperationResult == OR_Success || (!isFailedBecauseOfGoingOffline && !IsArtificiallyFailed())) {
            // It needs to make the post active again
            AppEvents::Get().Notify(ePOST_UPDATE_ABORTED, (void *)mPostId.c_str());
        }
    }
}

void PostUpdateOperation::on_post_with_photos_update_request_completed(void* object, char* response, int code) {
    Log::debug_tag(PUOP_LOG_TAG, "PostUpdateOperation::on_post_with_photos_update_request_completed");

    Sptr<PostUpdateOperation> oper(static_cast<PostUpdateOperation*>(object));
    if (oper) {
        oper->mErrorCode = static_cast<CURLcode>(code);
        if (code == CURLE_OK) {
            if (response) {
                Log::debug(LOG_FACEBOOK_OPERATION, "PostUpdateOperation::on_post_with_photos_update_request_completed->response: %s", response);
                FbRespondPost * postRespond = FbResponsePhotoPost::createFromJson(response);
                if (postRespond) {
                    if (postRespond->mError != NULL) {
                        Log::error(LOG_FACEBOOK_OPERATION, "PostUpdateOperation::on_post_with_photos_update_request_completed->error = %s", postRespond->mError->mMessage);
                        oper->mRequestError = true;
                        if (postRespond->mError->mType == Error::eOAuthException && postRespond->mError->mCode == 100) {
                            Utils::ShowToast(Application::GetInstance()->GetTopScreen()->getMainLayout(), i18n_get_text("IDS_POST_IS_NOT_AVAILABLE"));
                            AppEvents::Get().Notify(ePOST_NOT_AVAILABLE, const_cast<char*>(oper->mPostId.c_str()));
                        }
                    } else {
                        // Post update is required if it's requested on previous step or post message is changed
                        oper->mIsPostReloadRequired = oper->mIsPostReloadRequired || (oper->mParamsBundle != nullptr);
                    }
                    delete postRespond;
                } else {
                    Log::error(LOG_FACEBOOK_OPERATION, "PostUpdateOperation::on_post_with_photos_update_request_completed->Error parsing http response");
                    oper->mRequestError = true;
                }
            } else {
                Log::error(LOG_FACEBOOK_OPERATION, "PostUpdateOperation::on_post_with_photos_update_request_completed->Http response is corrupted");
                oper->mRequestError = true;
            }
        } else {
            Log::error(LOG_FACEBOOK_OPERATION, "PostUpdateOperation::on_post_with_photos_update_request_completed->Error sending http request");
        }

        if (oper->mRequest) {
            FacebookSession::GetInstance()->ReleaseGraphRequest(oper->mRequest);
            oper->mRequest = nullptr;
        }
        oper->CurrentStepComplete();
    }
    free(response);
}

bool PostUpdateOperation::UpdatePostMsgAndPhotoCaptions() {
    if (mRequest) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(mRequest);
        mRequest = nullptr;
    }

    const char *entryId = mCoverOrProfilePhotoId ? mCoverOrProfilePhotoId : mPostId.c_str();
    mRequest = FacebookSession::GetInstance()->UpdatePostWithPhotos(mParamsBundle, mMediaList,
            on_post_with_photos_update_request_completed, this, entryId);
    return mRequest;
}

void PostUpdateOperation::DeleteNextPhoto() {
    PostComposerMediaData *mediaData = static_cast<PostComposerMediaData *>(eina_list_data_get(mMediaListToDelete));
    assert(mediaData);

    if (mRequest) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(mRequest);
        mRequest = nullptr;
    }

    mMediaListToDelete = eina_list_remove(mMediaListToDelete, mediaData);
    mRequest = FacebookSession::GetInstance()->DeletePhoto(mediaData->GetMediaId(), on_photo_delete_request_completed, this);
    mediaData->Release();
}

void PostUpdateOperation::on_photo_delete_request_completed(void *object, char *response, int code) {
    Log::debug_tag(PUOP_LOG_TAG, "PostUpdateOperation::on_photo_delete_request_completed");

    Sptr<PostUpdateOperation> oper(static_cast<PostUpdateOperation*>(object));
    if (oper) {
        oper->mErrorCode = static_cast<CURLcode>(code);

        if (code == CURLE_OK) {
            // At least one photo has been successfully deleted, so let's reload post once process is completed
            oper->mIsPostReloadRequired = true;
        } else {
            Utils::ShowToast(Application::GetInstance()->GetTopScreen()->getMainLayout(), i18n_get_text("IDS_DELETE_PHOTO_FAILED"));
            Log::error(LOG_FACEBOOK_OPERATION, "PostUpdateOperation::on_photo_delete_request_completed->Error sending http request");
        }

        if (oper->mRequest) {
            FacebookSession::GetInstance()->ReleaseGraphRequest(oper->mRequest);
            oper->mRequest = nullptr;
        }

        if (!oper->IsFailedBecauseOfGoingOffline() && eina_list_count(oper->mMediaListToDelete) > 0) {
            oper->DeleteNextPhoto();
        } else {
            oper->CurrentStepComplete();
        }
    }
    free(response);
}
