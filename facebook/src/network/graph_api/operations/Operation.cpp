#include <app_i18n.h>
#include <notification.h>

#include "Log.h"
#include "Operation.h"
#include "Utils.h"

const std::set<CURLcode> Operation::mOfflineErrorCodes({CURLE_COULDNT_RESOLVE_HOST,
                                                        CURLE_SEND_ERROR,
                                                        CURLE_RECV_ERROR,
                                                        CURLE_COULDNT_CONNECT,
                                                        CURLE_COULDNT_RESOLVE_HOST,
                                                        CURLE_WRITE_ERROR,
                                                        CURLE_OPERATION_TIMEDOUT,
                                                        CURLE_HTTP_POST_ERROR,
                                                        CURLE_SSL_CONNECT_ERROR,
                                                        CURLE_AGAIN,
                                                        CURLE_NO_CONNECTION_AVAILABLE,
                                                        CURLE_GOT_NOTHING});
const CURLcode Operation::mArtificialErrorCode(CURLE_OBSOLETE20);


Operation::Operation(const std::list<Friend>& taggedFriends, const std::string& privacyType, const PCPlace* place):
    mTaggedFriends(taggedFriends),
    mPrivacyType(privacyType),
    mPlace(place ? *place : PCPlace()),
    mOpType(OT_None),
    mOpState(OS_NotStarted),
    mOpDestination(OD_None),
    mOperationResult(OR_Unknown),
    mCurrentStep(0),
    mStepCount(0),
    mSubscribers(nullptr),
    mStepCompleteJob(nullptr),
    mBundle(nullptr),
    mErrorCode(CURLE_OK),
    mStepError(false),
    mRequestError(false) {
}

Operation::~Operation() {
    if(mStepCompleteJob) {
        ecore_job_del(mStepCompleteJob);
        mStepCompleteJob = nullptr;
    }
    mSubscribers = eina_list_free(mSubscribers);
}

Operation::OperationType Operation::GetType() {
    return mOpType;
}

Operation::OperationDestination Operation::GetDestination() {
    return mOpDestination;
}

Operation::OperationState Operation::GetState() {
    return mOpState;
}

Operation::OperationResult Operation::GetOperationResult() {
    return mOperationResult;
}

void Operation::Subscribe(const Subscriber *subscriber) {
    if ( eina_list_data_find( mSubscribers, subscriber ) == nullptr ) {
        mSubscribers = eina_list_append( mSubscribers, subscriber );
    }
}

void Operation::UnSubscribe(const Subscriber *subscriber) {
    mSubscribers = eina_list_remove( mSubscribers, subscriber );
}

bool Operation::Start(){
    bool res = false;
    if(mCurrentStep < mStepCount) {
        res = DoNextStep();
        if(res) {
            mOpState = OS_InProgress;
            Notify(eOPERATION_START_EVENT, this);
            if (mOpType == OT_Post_Create || mOpType == OT_Photo_Post_Create || mOpType == OT_Video_Post_Create) {
                if (mOpDestination == OD_Friend_Timeline) {
                    notification_status_message_post(i18n_get_text("IDS_SHARE_POSTING"));
                }
            }
        }
    }
    return res;
}

bool Operation::ReStart() {
    ResetOperation();
    return Start();
}

bool Operation::Suspend() {
    bool res = true;
    // TODO: paused
    mOpState = OS_Paused;
    return res;
}

bool Operation::Resume() {
    bool res = DoStep();
    return res;
}

bool Operation::Cancel() {
    bool res = true;
    // TODO: cancel
    mOpState = OS_Canceled;
    Notify(eOPERATION_CANCELED_EVENT, this);
    return res;
}

bool Operation::isCompleted() {
    return (mCurrentStep > mStepCount || mStepError);
}

bool Operation::isCanceled() {
    return (mOpState == OS_Canceled);
}

bool Operation::DoStep() {
    bool res = false;
    if(mCurrentStep > mStepCount || mStepError) {
        mOpState = OS_Completed;
        AddRef();
        OnOperationComplete();
        Notify(eOPERATION_COMPLETE_EVENT, this);
        Release();
    } else {
        res = CurrentStepExecute();
    }
    return res;
}

bool Operation::DoNextStep() {
    bool res = false;
    if(mCurrentStep <= mStepCount) {
        mCurrentStep++;
        res = DoStep();
    }
    return res;
}

void Operation::step_complete_cb(void* data) {
    Log::info(LOG_FACEBOOK_OPERATION, "Operation::step_complete_cb");
    Sptr<Operation> object(static_cast<Operation*>(data));
    if(object) {
        object->mStepCompleteJob = nullptr;
        object->DoNextStep();
    }
}

void Operation::ShowErrorPopup(Error *error) {
    if (error) {
        if (error->mType == Error::eOAuthException || error->mErrorSubcode == 1607035) { //1607035 means "Timeline Posts Are Off". This happens if we share post on timeline of friend who doesn't allow people to post on their timeline.
            switch (error->mCode) {
            case 120:
                notification_status_message_post(i18n_get_text("IDS_ALBUM_IS_NOT_AVAILABLE"));
                break;
            case 200:
            case 240:
                notification_status_message_post(i18n_get_text("IDS_POST_UPLOAD_ERROR_RESTRICTED_TIMELINE"));
                break;
            default:
                break;
            }
        } else if (error->mCode == 6000 && error->mErrorSubcode == 1363025) { //This means: "There was a problem uploading your video file. Please try again with another file."
            if (error->mMessage) {
                notification_status_message_post(error->mMessage);
            }
        }
    }

}

void Operation::CurrentStepComplete() {
    Notify(eOPERATION_STEP_COMPLETE_EVENT, this);
    if(mStepCompleteJob) {
        ecore_job_del(mStepCompleteJob);
        mStepCompleteJob = nullptr;
    }
    mStepCompleteJob = ecore_job_add(step_complete_cb, this);
}

int Operation::GetCurrentStep() {
    return mCurrentStep;
}

int Operation::GetStepCount() {
    return mStepCount;
}

bool Operation::Save(bundle *param) {
    bool res = true;
    return res;
}

bool Operation::Load(bundle *param) {
    bool res = true;
    return res;
}

void Operation::OnOperationComplete() {
    mOperationResult = mStepError ? OR_Error : OR_Success;
    return;
}

void Operation::Notify(AppEventId eventId, void * data) {
    Eina_List *l;
    void *itemData;

    Eina_List *originalList = eina_list_clone(mSubscribers);
    EINA_LIST_FOREACH(originalList, l, itemData) {
        void *foundData = eina_list_data_find(mSubscribers, itemData);
        Subscriber *subscriber = static_cast<Subscriber *>(foundData);
        if (subscriber) {
            subscriber->Update(eventId, data);
        }
    }
     eina_list_free(originalList);
}

void Operation::Update(AppEventId eventId, void * data) {
}

bundle *Operation::GetBundle()
{
    return mBundle;
}

const char * Operation::GetDestinationId()
{
    return nullptr;
}

void Operation::ResetOperation() {
    if(mOpState != OS_NotStarted) {
        Log::info(LOG_FACEBOOK_OPERATION, "Operation::ResetOperation");
        PauseOperation();
        mOpState = OS_NotStarted;
        mCurrentStep = 0;
        mRequestError = false;
        Notify(eOPERATION_RETRY_SEND_POST_EVENT, this);
    } else {
        Log::error(LOG_FACEBOOK_OPERATION, "Operation::ResetOperation->operation already reset");
    }
}

void Operation::PauseOperation() {
    Log::info(LOG_FACEBOOK_OPERATION, "Operation::PauseOperation");
    mOpState = OS_Paused;
    mStepError = false;
    mOperationResult = OR_Unknown;
}

const char *Operation::GetMessage() {
    bundle* operationBundle = GetBundle();
    CHECK_RET(operationBundle, "");
    char* msg = nullptr;
    bundle_get_str(operationBundle, "origin_message", &msg);
    return msg;
}

void Operation::PutInErrorState() {
    Log::info(LOG_FACEBOOK_OPERATION, "Operation::PutInErrorState");
    mErrorCode = mArtificialErrorCode;
    mOpState = Operation::OS_Completed;
    mOperationResult = Operation::OR_Error;
    mStepError = true;
    CurrentStepComplete();
}

bool Operation::IsFailedBecauseOfGoingOffline() {
    auto codeIterator = mOfflineErrorCodes.find(mErrorCode);
    return codeIterator != mOfflineErrorCodes.end();
}
