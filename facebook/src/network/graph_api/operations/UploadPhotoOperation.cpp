#include "Application.h"
#include "FacebookSession.h"
#include "Log.h"
#include "OperationManager.h"
#include "ParserWrapper.h"
#include "Photo.h"
#include "ScreenBase.h"
#include "UploadPhotoOperation.h"
#include "Utils.h"

static constexpr char UPOP_LOG_TAG[] = "Facebook::UploadPhotoOperation";


/*
 * Class UploadPhotoOperation
 */
UploadPhotoOperation::UploadPhotoOperation(PhotoType type, ImageFile &image, bool showNotification) : SimplePostOperation(Operation::OT_Upload_Profile_Photo,
        "", // user Id
        nullptr, // bundle
        std::list<Friend> (),
        "", // privacy type
        nullptr // place
        ),
        mType(type),
        mImageFile(image),
        mImageDownloader(nullptr),
        mShowNotification(showNotification) {
    mStepCount = 2;
    mImageFile.AddRef();
}

UploadPhotoOperation::~UploadPhotoOperation() {
    delete mImageDownloader;
    mImageFile.Release();
}

bool UploadPhotoOperation::DoCurrentStepExecute() {

    if(mCurrentStep == 1) {
        mImageDownloader = new ImageDownloader(*this, mImageFile);
        switch(mImageDownloader->GetImage().GetStatus()) {
        case ImageFile::EDownloaded:
            ImageReady();
            break;
        case ImageFile::EDownloading:
            break;
        case ImageFile::ENotDownloaded:
            mImageDownloader->Download();
            break;
        }

        return true;
    } else if (mCurrentStep == 2) {
        if(mShowNotification) {
            notification_status_message_post(i18n_get_text("IDS_PROFILE_ME_UPLOAD_PROFILE_PICTURE"));
        }
        if (ECoverPhoto == mType) {
            mRequest = FacebookSession::GetInstance()->PostCoverPhoto(mImageFile.GetPath(), nullptr, on_upload_photo_completed, this);
        } else {
            mRequest = FacebookSession::GetInstance()->PostProfilePicture(mImageFile.GetPath(), nullptr, on_upload_photo_completed, this);
        }
        return mRequest;
    }
    return false;
}

void UploadPhotoOperation::ImageReady() {
    Log::debug_tag(UPOP_LOG_TAG, "UploadPhotoOperation::ImageReady()");
    assert (mImageDownloader);
    delete mImageDownloader;
    mImageDownloader = nullptr;

    if (mImageFile.GetStatus() == ImageFile::EDownloaded) {
        Log::debug_tag(UPOP_LOG_TAG, "UploadPhotoOperation::ImageReady(photo): path is %s", mImageFile.GetPath());

        // Check the update Type in user profile. It can be Profile picture or Cover Picture update.
        switch(mType)
        {
            case ECoverPhoto:
            {
                Log::debug_tag(UPOP_LOG_TAG, "UploadPhotoOperation::ImageReady() and Opened From Upload Cover Photo");
            }
            break;

            case EUserPhoto:
                Log::debug_tag(UPOP_LOG_TAG, "UploadPhotoOperation::ImageReady() and Opened From Upload Profile Photo");
            break;
            default:
                assert(false);
                break;
        }
        CurrentStepComplete();
    } else {
        PutInErrorState();
    }
}

void UploadPhotoOperation::on_upload_photo_completed(void* object, char* response, int code)
{
    Log::debug_tag(UPOP_LOG_TAG, "UploadPhotoOperation::on_upload_photo_completed");
    UploadPhotoOperation *me = static_cast<UploadPhotoOperation *>(object);
    assert(me);

    GraphRequest* requestRawPtr = me->mRequest.Get();
    FacebookSession::GetInstance()->ReleaseGraphRequest(requestRawPtr);
    me->mRequest = nullptr;

    me->mErrorCode = static_cast<CURLcode>(code);
    if (CURLE_OK == code) {
        if (me->ParseUploadPhotoResponse(response)) {
            Log::debug_tag(UPOP_LOG_TAG, "Upload succeed with response: %s", response);
            AppEventId eventId = eUNDEFINED_EVENT;
            char *popupMsg = nullptr;
            if (me->mType == ECoverPhoto) {
                popupMsg = i18n_get_text("IDS_PROFILE_ME_UPLOAD_COVER_PICTURE_COMPLETED");
                eventId = eUPDATED_COVER_PICTURE;
            } else if (me->mType == EUserPhoto) {
                popupMsg = i18n_get_text("IDS_PROFILE_ME_UPLOAD_PROFILE_PICTURE_COMPLETED");
                eventId = eUPDATED_PROFILE_PICTURE;
            } else {
                assert(false);
            }
            AppEvents::Get().Notify(eventId, (void *)me->mImageFile.GetPath());
            Utils::ShowToast(Application::GetInstance()->GetTopScreen()->getMainLayout(), popupMsg);
        }
        me->CurrentStepComplete();
    } else {
        me->PutInErrorState();
        Log::error_tag(UPOP_LOG_TAG, "Error in uploading your photo: Error code(%d)", code);
        Log::error_tag(UPOP_LOG_TAG, "Upload failed with response: %s", response);
    }

    //Attention!! client should take care to free respond buffer
    free(response);
}

bool UploadPhotoOperation::ParseUploadPhotoResponse(char *response) {
    JsonObject *rootObject = nullptr;
    JsonParser *parser = openJsonParser(response, &rootObject);
    ParserWrapper autoDestroyer(parser);

    JsonObject *error = json_object_get_object_member(rootObject, "error");
    if (error) {
        char *msg = GetStringFromJson(error, "message");
        Log::error_tag(UPOP_LOG_TAG, "Parsed Error Message = %s", msg);

        int code = GetGint64FromJson(error, "code");
        Log::error_tag(UPOP_LOG_TAG, "Parsed Error Code = %d", code);

        if(code && msg) {
            std::string str = msg;
            str.erase(0, str.find_first_of(')')+1);
            str.erase(0, str.find_first_not_of(' '));
            Log::error("Final Error Message = %s", str.c_str());
            WidgetFactory::ShowAlertPopup(str.c_str(), "IDS_PROFILE_ME_UPLOAD_PROFILE_PICTURE");
        }
        free(msg);
        return false;
    } else {
        return true;
    }
}

/*
 * Class UploadPhotoOperation::ImageDownloader
 */
UploadPhotoOperation::ImageDownloader::ImageDownloader(UploadPhotoOperation &operation, ImageFile &image) :
        ImageFileDownloader(image), mOperation(operation) {

}

void UploadPhotoOperation::ImageDownloader::ImageDownloaded(bool result) {
    mOperation.ImageReady();
}
