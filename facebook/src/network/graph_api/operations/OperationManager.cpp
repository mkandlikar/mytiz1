#include "OperationManager.h"

#include "AppEvents.h"
#include "ConnectivityManager.h"
#include "Log.h"
#include "TrayNotificationsManager.h"

static constexpr char OPM_LOG_TAG[] = "Facebook::OperationManager";

OperationManager::OperationManager():
        mSubscribers(nullptr),
        mCurrentOperation(nullptr),
        mOperationIsWaitingForConnection(false),
        mTimer(std::chrono::seconds(10), std::bind(std::mem_fn<void(void)>(&OperationManager::OnTimer), this)) {
    Log::debug_tag(OPM_LOG_TAG, "OperationManager::OperationManager");
    AppEvents::Get().Subscribe(eINTERNET_CONNECTION_CHANGED, this);
}

OperationManager::~OperationManager() {
    Log::debug_tag(OPM_LOG_TAG, "OperationManager::~OperationManager");
    if ( mSubscribers ) {
        eina_list_free( mSubscribers );
        mSubscribers = nullptr;
    }
    CancelAll();
    DeleteAll();
}

OperationManager* OperationManager::GetInstance() {
    static OperationManager* opMng = new OperationManager();
    return opMng;
}

void OperationManager::Subscribe(const Subscriber *subscriber) {
    if ( eina_list_data_find( mSubscribers, subscriber ) == nullptr) {
        mSubscribers = eina_list_append( mSubscribers, subscriber );
    }
}

void OperationManager::UnSubscribe(const Subscriber *subscriber) {
    mSubscribers = eina_list_remove( mSubscribers, subscriber );
}

void OperationManager::Update(AppEventId eventId, void* data) {
    Log::debug_tag(OPM_LOG_TAG, "OperationManager::Update");

    switch(eventId) {
        case eINTERNET_CONNECTION_CHANGED: {
            Log::debug_tag(OPM_LOG_TAG, "OperationManager::Update eINTERNET_CONNECTION_CHANGED");
            if(ConnectivityManager::Singleton().IsConnected() && mOperationIsWaitingForConnection) {
                Log::debug_tag(OPM_LOG_TAG, "OperationManager::Update eINTERNET_CONNECTION_CHANGED re-starting operation");
                mOperationIsWaitingForConnection = false;
                ResetAllOperations();
                mTimer.Stop();
                if(mCurrentOperation) {
                    Restart();
                } else {
                    DoNextOperation();
                }
            } else if(!ConnectivityManager::Singleton().IsConnected() && mCurrentOperation && !mOperationIsWaitingForConnection) {
                Log::debug_tag(OPM_LOG_TAG, "OperationManager::Update eINTERNET_CONNECTION_CHANGED stopping operation");
                mOperationIsWaitingForConnection = true;
                PutAllOperationsInErrorState();
                ShowUploadErrorNotification();
            }
            break;
        }
        case eOPERATION_START_EVENT: {
            Sptr<Operation> oper(static_cast<Operation*>(data));
            Log::debug_tag(OPM_LOG_TAG, "OperationManager::Update eOPERATION_START_EVENT");
            if(!oper) break;
            Log::debug( LOG_FACEBOOK_OPERATION, "OperationManager::Update - eOPERATION_START_EVENT" );
            Notify(eventId, oper.Get());
            break;
        }
        case eOPERATION_COMPLETE_EVENT: {
            Sptr<Operation> oper(static_cast<Operation*>(data));
            Log::debug_tag(OPM_LOG_TAG, "OperationManager::Update eOPERATION_COMPLETE_EVENT");
            if(!oper) break;
            if(oper->IsFailedBecauseOfGoingOffline()) {
                Log::debug_tag(OPM_LOG_TAG, "OperationManager::Update eOPERATION_COMPLETE_EVENT connection lost, operation exempt, code : `%d`", oper->GetErrorCode());
                mCurrentOperation->PauseOperation();
                mOperationIsWaitingForConnection = true;
                if(ConnectivityManager::Singleton().IsConnected() && !mTimer.IsSet()) {
                    mTimer.Start(3);
                } else if(mTimer.IsSet()) {
                    mTimer.Thaw();
                    mTimer.Reset();
                } else if(!ConnectivityManager::Singleton().IsConnected()) {
                    PutAllOperationsInErrorState();
                    ShowUploadErrorNotification();
                }
                break;
            } else if(oper->IsArtificiallyFailed()) { // we setup error our self
                if(mTimer.IsSet()) {
                    mTimer.Thaw();
                    mTimer.Reset();
                }
                mOperationIsWaitingForConnection = true;
            } else {
                Log::debug_tag(OPM_LOG_TAG, "OperationManager::Update eOPERATION_COMPLETE_EVENT execution finished");
                mCurrentOperation->PrepareToBeRemoved();
                RemoveOperation(mCurrentOperation);
                mCurrentOperation = nullptr;
                mTimer.Stop();
                Notify(eventId, oper.Get());
                DoNextOperation();
            }
            if (mOperationIsWaitingForConnection) {
                Notify(eOPERATION_IN_ERROR_STATE_EVENT, oper.Get());
            }
            break;
        }
        case eOPERATION_CANCELED_EVENT: {
            Sptr<Operation> oper(static_cast<Operation*>(data));
            Log::debug_tag(OPM_LOG_TAG, "OperationManager::Update eOPERATION_CANCELED_EVENT");
            if(!oper) break;
            Log::debug( LOG_FACEBOOK_OPERATION, "OperationManager::Update - eOPERATION_CANCELED_EVENT" );
            if(oper == mCurrentOperation) {
                mCurrentOperation = nullptr;
                RemoveOperation(oper);
                DoNextOperation();
            } else {
                RemoveOperation(oper);
            }
            break;
        }
        case eOPERATION_STEP_COMPLETE_EVENT: {
            Log::debug_tag(OPM_LOG_TAG, "OperationManager::Update eOPERATION_STEP_COMPLETE_EVENT");
            // ignore
            break;
        }
        default:
            break;
    }
}

void OperationManager::AddOperation(Sptr<Operation> operation) {
    Log::debug_tag(OPM_LOG_TAG, "OperationManager::AddOperation, operation: %p", operation.Get());
    if(operation) {
        Log::debug_tag(OPM_LOG_TAG, "OperationManager::AddOperation, operation reference count: %d", operation->GetRefCount());
        for(auto it = mOperations.begin(); it != mOperations.end(); it++) {
            if(*it == operation) return;
        }
        operation->Subscribe(this);
        Log::debug_tag(OPM_LOG_TAG, "OperationManager::AddOperation: adding operation in queue");
        mOperations.push_back(operation);
        Notify(eOPERATION_ADDED_EVENT, operation.Get());
        if(mOperationIsWaitingForConnection) {
            operation->PutInErrorState();
        } else {
            Start();
        }
    }
}

void OperationManager::RemoveOperation(Sptr<Operation> operation) {
    if(operation) {
        for(auto it = mOperations.begin(); it != mOperations.end(); it++) {
            if(*it == operation) {
                Log::debug_tag(OPM_LOG_TAG, "OperationManager::RemoveOperation %p", operation.Get() );
                mOperations.erase(it);
                Notify(eOPERATION_REMOVED_EVENT, operation.Get());
                operation->UnSubscribe(this);
                return;
            }
        }
    }
}

Operation *OperationManager::GetOperationByUserId(const char *id) {
    assert(id);
    Operation *res = nullptr;
    for (auto it = mOperations.begin(); it != mOperations.end(); it++) {
        Operation *op = (*it).Get();
        if (op) {
            if (op->GetUserId() == id) {
                res = op;
                break;
            }
        }
    }
    return res;
}

Operation *OperationManager::GetActiveOperationById(const char *id) {
    Operation *op = GetOperationByUserId(id);
    if (op && (op->GetType() == Operation::OT_Post_Update || op->GetType() == Operation::OT_Reload_Post) && op->GetState() != Operation::OS_Completed) {
        return op;
    } else {
        return nullptr;
    }
}

void OperationManager::RestartOperationAfterError(Sptr<Operation> operation) {
    if(!mOperationIsWaitingForConnection) {
        return;
    }
    if(operation) {
        Log::debug_tag(OPM_LOG_TAG, "OperationManager::RestartOperationAfterError" );
        if(operation == mCurrentOperation) {
            if(mTimer.IsSet()) {
                mTimer.Thaw();
                mTimer.Reset();
            }
            mTimer.Start(1);
            Restart();
            return;
        } else {
            for(auto it = mOperations.begin(); it != mOperations.end(); it++) {
                if(*it == operation) {
                    mOperations.emplace_front(operation);
                    mOperations.erase(it);
                    mCurrentOperation = operation;

                    if(mTimer.IsSet()) {
                        mTimer.Thaw();
                        mTimer.Reset();
                    }
                    mTimer.Start(1);
                    Restart();
                    break;
                }
            }
        }
    }
}

bool OperationManager::Start(){
    bool res = DoNextOperation();
    return res;
}

bool OperationManager::Restart() {
    bool started(false);
    if(mCurrentOperation) {
        mOperationIsWaitingForConnection = false;
        started = mCurrentOperation->ReStart();
    }
    return started;
}

bool OperationManager::Suspend() {
    bool res = true;
    return res;
}

bool OperationManager::Resume() {
    bool res = true;
    return res;
}

void OperationManager::CancelAll() {
    while(!mOperations.empty()) {
        (*mOperations.begin())->Cancel();
    }
    mCurrentOperation = nullptr;
}

void OperationManager::DeleteAll() {
    for(auto it = mOperations.begin(); it != mOperations.end(); it++) {
        (*it)->UnSubscribe(this);
    }
    mOperations.clear();
    mCurrentOperation = nullptr;
}

bool OperationManager::DoNextOperation() {
    bool res = true;

    if(!mCurrentOperation && !mOperations.empty()) {
        mCurrentOperation = *(mOperations.begin());
        mCurrentOperation->Start();
    }
    return res;
}

Sptr<Operation> OperationManager::GetCurrentOperation() {
    return mCurrentOperation;
}

bool OperationManager::Save() {
    bool res = true;
    return res;
}

bool OperationManager::Load() {
    bool res = true;
    return res;
}

void OperationManager::PutAllOperationsInErrorState() {
    std::list<Sptr<Operation>>::iterator iter;
    for (iter = mOperations.begin(); iter != mOperations.end(); iter++) {
       (*iter)->PutInErrorState();
    }
}

void OperationManager::ResetAllOperations() {
    std::list<Sptr<Operation>>::iterator iter;
    for (iter = mOperations.begin(); iter != mOperations.end(); iter++) {
       (*iter)->ResetOperation();
    }
}

void OperationManager::OnTimer() {
    Log::debug_tag(OPM_LOG_TAG, "OperationManager::OnTimer");
    if(!mOperationIsWaitingForConnection || !ConnectivityManager::Singleton().IsConnected()) {
        mTimer.Stop();
        if (mCurrentOperation) {
            PutAllOperationsInErrorState();
            mOperationIsWaitingForConnection = true;
            TrayNotificationsManager::GetInstance()->CreateNotiNotification(i18n_get_text("IDS_POST_UPLOAD_ERROR_NOTIFICATION"), nullptr);
        }
    }
    if(mCurrentOperation && ConnectivityManager::Singleton().IsConnected()) {
        Log::debug_tag(OPM_LOG_TAG, "OperationManager::OnTimer: trying to launch operation again");
        mOperationIsWaitingForConnection = false;
        if(mTimer.GetLeftRepeats()) {
            mTimer.Freeze();
            Restart();
        } else {
            PutAllOperationsInErrorState();
            mOperationIsWaitingForConnection = true;
            TrayNotificationsManager::GetInstance()->CreateNotiNotification(i18n_get_text("IDS_POST_UPLOAD_ERROR_NOTIFICATION"), nullptr);
        }
    }
}

void OperationManager::Notify(AppEventId eventId, void * data) {
    Eina_List *l, *l_next;
    void *itemData;
    EINA_LIST_FOREACH_SAFE( mSubscribers, l, l_next, itemData ) {
        Subscriber *subscriber = static_cast<Subscriber *>(itemData);
        if(subscriber) {
            subscriber->Update(eventId, data);
        }
    }
}

void OperationManager::ShowUploadErrorNotification() const {
    if (mCurrentOperation->IsErrorNotificationRequired()) {
        TrayNotificationsManager::GetInstance()->CreateNotiNotification(i18n_get_text("IDS_POST_UPLOAD_ERROR_NOTIFICATION"), nullptr);
    }
}
