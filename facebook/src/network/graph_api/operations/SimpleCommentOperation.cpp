#include <app_i18n.h>
#include <dlog.h>
#include <notification_status.h>

#include "Common.h"
#include "FacebookSession.h"
#include "FbRespondEditCommentMessage.h"
#include "FbRespondReloadComment.h"
#include "FbRespondPost.h"
#include "OperationManager.h"
#include "SimpleCommentOperation.h"
#include "SimplePostOperation.h"

SimpleCommentOperation::SimpleCommentOperation(OperationType opType, const char* parentObjectId, bool isPost, bundle* paramsBundle, const char* commentId ):
    Operation({}, "") {
    Log::info("SimpleCommentOperation::SimpleCommentOperation->%s", parentObjectId);
    mOpType = opType;
    mCommentId = SAFE_STRDUP(commentId);
    mParentId = SAFE_STRDUP(parentObjectId);
    mPhotoFileName = nullptr;
    mPhotoFileThumbnailPath = nullptr;
    mParamsBundle = bundle_dup(paramsBundle);
    mRequest = nullptr;
    mComment = nullptr;
    mIsReloadInProgress = false;

    mParentObjectIsPost = isPost; // true for Post, false for Comment

    Init();
}

SimpleCommentOperation::SimpleCommentOperation(OperationType opType, const char* parentObjectId, bool isPost, bundle* paramsBundle, const char * photoFileName,  const char * photoFileThumbnailPath):
    Operation({}, "") {
    Log::info("SimpleCommentOperation::SimpleCommentOperation->with photo file");
    mOpType = opType;
    mCommentId = nullptr;
    mParentId = SAFE_STRDUP(parentObjectId);
    mPhotoFileName = SAFE_STRDUP(photoFileName);
    mPhotoFileThumbnailPath = SAFE_STRDUP(photoFileThumbnailPath);
    mParamsBundle = bundle_dup(paramsBundle);
    mRequest = nullptr;
    mComment = nullptr;
    mIsReloadInProgress = false;

    mParentObjectIsPost = isPost; // true for Post, false for Comment

    Init();
}


SimpleCommentOperation::~SimpleCommentOperation() {
    Log::info("SimpleCommentOperation::~SimpleCommentOperation");
    FacebookSession::GetInstance()->ReleaseGraphRequest(mRequest);
    free(mCommentId);
    free(mParentId);
    free(mPhotoFileName);
    bundle_free(mParamsBundle);
    if (mComment) {
        mComment->Release();
    }
}

void SimpleCommentOperation::Init()
{
    switch(mOpType) {
        case OT_Add_Comment:
        case OT_Add_Reply:
        case OT_Edit_CommentReply:
            mStepCount = 2;
            break;
        default:
            mStepCount = 1;
    }
    Log::info("SimpleCommentOperation::Init->mStepCount = %d",mStepCount );
}

bool SimpleCommentOperation::CurrentStepExecute() {
    bool res = false;
    if(mRequest) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(mRequest);
    }

    if(mCurrentStep == 1) {
        switch(mOpType) {
            case OT_Add_Comment:
            case OT_Add_Reply:
                Log::info("SimpleCommentOperation::CurrentStepExecute->OT_Add");
                mRequest = FacebookSession::GetInstance()->PostComment(mParentId, mParamsBundle, on_comment_add_request_completed, this, mPhotoFileName);
                break;
            case OT_Edit_CommentReply:
                Log::info("SimpleCommentOperation::CurrentStepExecute->OT_Edit");
                mRequest = FacebookSession::GetInstance()->UpdateComment(mParamsBundle, on_comment_edit_request_completed, this, mCommentId);
                break;
            case OT_Delete_Comment:
            case OT_Delete_Reply:
                Log::info("SimpleCommentOperation::CurrentStepExecute->OT_Delete");
                mRequest = FacebookSession::GetInstance()->DeleteComment(mCommentId, on_comment_delete_request_completed, this);
                break;
            case OT_Reload_Comment:
                mRequest = FacebookSession::GetInstance()->ReloadComment(mCommentId, on_comment_reload_request_completed, this);
                break;
            default: { }
        }
    }
    else if (mCurrentStep == 2 && ! mStepError) {
        switch(mOpType) {
            case OT_Edit_CommentReply:
            case OT_Add_Comment:
            case OT_Add_Reply:
                Log::info("SimpleCommentOperation::CurrentStepExecute:Step2->OT_Add:ReloadComment");
                mIsReloadInProgress = true;
                mRequest = FacebookSession::GetInstance()->ReloadComment(mCommentId, on_comment_reload_request_completed, this);
                break;
            default: { }
        }
    }

    if(mRequest) {
        mRequest->Subscribe(this);
    }
    res = (mRequest != nullptr);
    return res;
}

void SimpleCommentOperation::on_comment_add_request_completed(void* object, char* respond, int code) {
    Log::info("SimpleCommentOperation::on_comment_add_request_completed->code is = %d", code);

    Sptr<SimpleCommentOperation> oper(static_cast<SimpleCommentOperation*>(object));

    oper->mErrorCode = static_cast<CURLcode>(code);
    if (code == CURLE_OK) {
        FbRespondPost * cmntRespond = FbRespondPost::createFromJson(respond);
        if (cmntRespond) {
            if (cmntRespond->mError != nullptr) {
                Log::error("SimpleCommentOperation->Error editing comment, error = %s", cmntRespond->mError->mMessage);
                notification_status_message_post(i18n_get_text("IDS_POST_IS_NOT_AVAILABLE"));
                if (oper) {
                    oper->mStepError = true;
                }
            }
            else {
                Log::info("SimpleCommentOperation->Result response = %d", cmntRespond->mSuccess);
                if (oper) {
                    if (oper->mCommentId) {
                        free(oper->mCommentId);
                        oper->mCommentId = nullptr;
                    }
                    oper->mCommentId = SAFE_STRDUP(cmntRespond->mId);

                    if(oper->mParentId) {
                        if (oper->mParentObjectIsPost ) {
                            Log::info("SimpleCommentOperation:on_comment_add_request_completed->Reload root post = %s after adding", oper->mParentId);
                            OperationManager::GetInstance()->AddOperation(MakeSptr<SimplePostOperation>(OT_Reload_Post, oper->mParentId, nullptr, std::list<Friend>(), "", nullptr));
                        } else {
                            Log::info("SimpleCommentOperation:on_comment_add_request_completed->Reload photo details for = %s after adding", oper->mParentId);
                            AppEvents::Get().Notify(ePHOTO_COMMENTS_LIST_WAS_MODIFIED, oper->mParentId);
                        }
                    }
                }
            }
            delete cmntRespond;
        } else {
            Log::error("SimpleCommentOperation::on_comment_add_request_completed->Error parsing http respond");
        }
    }
    else {
        Log::error("SimpleCommentOperation::on_comment_add_request_completed->Error sending http request");
        if (oper) {
            oper->mStepError = true;
        }
    }

    if (respond != nullptr) {
        //Attention!! client should take care to free respond buffer
        free(respond);
    }

    if(oper) {
        Log::error("SimpleCommentOperation::on_comment_add_request_completed!");
        FacebookSession::GetInstance()->ReleaseGraphRequest(oper->mRequest);
        if(code == CURLE_RECV_ERROR) {
            // finish operation without confirmation and reload
            oper->Notify(eOPERATION_CANCELED_EVENT, oper.Get());// remove from operation manager queue
            oper->OnNetworkErrorAfterAdded();
        }
        else {
            oper->CurrentStepComplete();
        }
    }
}

void SimpleCommentOperation::on_comment_edit_request_completed(void* object, char* respond, int code) {
    Log::info("SimpleCommentOperation::on_comment_edit_request_completed");

    Sptr<SimpleCommentOperation> oper(static_cast<SimpleCommentOperation*>(object));

    oper->mErrorCode = static_cast<CURLcode>(code);
    if (code == CURLE_OK) {
        Log::info("SimpleCommentOperation::on_comment_edit_request_completed, respond = %s", respond);

        FbRespondEditCommentMessage * cmntRespond = FbRespondEditCommentMessage::createFromJson(respond);

        if (cmntRespond) {
            if (cmntRespond->mError != nullptr) {
                Log::error( "SimpleCommentOperation->Error editing comment, error = %s", cmntRespond->mError->mMessage);
                notification_status_message_post(i18n_get_text("IDS_POST_IS_NOT_AVAILABLE"));
                if (oper) {
                    oper->mStepError = true;
                }
            }
            else {
                Log::info("SimpleCommentOperation->Result response = %d", cmntRespond->mIsSuccess);
            }
            delete cmntRespond;
        } else {
            Log::error("SimpleCommentOperation::on_comment_edit_request_completed->Error parsing http respond");
        }
    }
    else {
        Log::error("SimpleCommentOperation::on_comment_edit_request_completed->Error sending http requestd");
        if (oper) {
            oper->mStepError = true;
        }
    }

    if (respond) {
        //Attention!! client should take care to free respond buffer
        free(respond);
    }

    if(oper) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(oper->mRequest);
        oper->CurrentStepComplete();
    }
}


void SimpleCommentOperation::on_comment_reload_request_completed(void* object, char* respond, int code) {
    Log::info("SimpleCommentOperation::on_comment_reload_request_completed");

    Sptr<SimpleCommentOperation> oper(static_cast<SimpleCommentOperation*>(object));
    oper->mErrorCode = static_cast<CURLcode>(code);
    if (code == CURLE_OK) {
        Log::info("SimpleCommentOperation::on_comment_reload_request_completed, response: %s", respond);

        FbRespondReloadComment *cmntRespond= FbRespondReloadComment::createFromJson(respond);
        if (cmntRespond) {
            if (cmntRespond->mError) {
                Log::error("SimpleCommentOperation->Error reloading comment, error = %s", cmntRespond->mError->mMessage);
                if (oper) {
                    oper->mStepError = true;
                }
            }
            else {
                if (cmntRespond->mComment) {
                    cmntRespond->mComment->AddRef();
                    oper->mComment = cmntRespond->mComment;

                    if ( oper->mParentId &&
                        !oper->mParentObjectIsPost &&
                         (oper->GetType() == Operation::OT_Add_Reply || oper->GetType() == Operation::OT_Edit_CommentReply)
                       ) {
                        Log::info("SimpleCommentOperation:on_comment_reload_request_completed->Reload root comment = %s after reply adding", oper->mParentId);
                        OperationManager::GetInstance()->AddOperation(MakeSptr<SimpleCommentOperation>(OT_Reload_Comment, nullptr, oper->mParentObjectIsPost, nullptr, oper->mParentId));
                    } else if (oper->GetType() == Operation::OT_Reload_Comment) {
                        Log::info("SimpleCommentOperation:on_comment_reload_request_completed->Update reply widget for comment = %s", oper->mParentId);
                        AppEvents::Get().Notify(eREPLIES_LIST_WAS_MODIFIED, oper->mComment);
                    }
                }
            }
            delete cmntRespond;
        } else {
            Log::error("SimpleCommentOperation::on_comment_reload_request_completed->Error parsing http respond");
        }
    }
    else {
        Log::error("SimpleCommentOperation::on_comment_reload_request_completed->Error sending http request");
        if (oper) {
            oper->mStepError = true;
        }
    }

    if (respond != nullptr) {
        //Attention!! client should take care to free respond buffer
        free(respond);
    }

    if(oper) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(oper->mRequest);

        if(oper->mStepError){
            if(oper->IsReloadError()) {
                Log::error("SimpleCommentOperation::on_comment_reload_request_completed->added but can't reload");
                oper->OnNetworkErrorAfterAdded();
            }
        }
        else {
            oper->CurrentStepComplete();
        }
    }
}

void SimpleCommentOperation::OnNetworkErrorAfterAdded() {
    Log::info("SimpleCommentOperation::OnNetworkErrorAfterAdded");
    mOperationResult = OR_Success;
    mOpState = OS_Completed;
    Notify(eOPERATION_STEP_COMPLETE_EVENT, this);
}

void SimpleCommentOperation::on_comment_delete_request_completed(void* object, char* respond, int code) {
    Log::info("SimpleCommentOperation::on_comment_delete_request_completed");

    Sptr<SimpleCommentOperation> oper(static_cast<SimpleCommentOperation*>(object));
    if(oper) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(oper->mRequest);
        Log::error("SimpleCommentOperation::on_comment_delete_request_completed!");
        oper->CurrentStepComplete();
    }
    oper->mErrorCode = static_cast<CURLcode>(code);
    if (code == CURLE_OK) {
        Log::info("SimpleCommentOperation::on_comment_delete_request_completed, respond = %s", respond);

        FbRespondEditCommentMessage * cmntRespond = FbRespondEditCommentMessage::createFromJson(respond);

        if (cmntRespond) {
            if (cmntRespond->mError != nullptr) {
                Log::error("SimpleCommentOperation::on_comment_delete_request_completed->Error editing message, error = %s", cmntRespond->mError->mMessage);
            } else {
                Log::info("SimpleCommentOperation::on_comment_delete_request_completed->Result response = %d", cmntRespond->mIsSuccess );

                AppEvents::Get().Notify(eCOMMENT_WAS_DELETED, oper->mCommentId);
                if(oper->mParentId) {
                    if(oper->mParentObjectIsPost) {
                        Log::info("SimpleCommentOperation::on_comment_delete_request_completed->Reload root post  = %s after reply deletion", oper->mParentId);
                        OperationManager::GetInstance()->AddOperation(MakeSptr<SimplePostOperation>(OT_Reload_Post, oper->mParentId, nullptr, std::list<Friend>(), "", nullptr));
                    } else {
                        if( oper->GetType() == Operation::OT_Delete_Reply ) {
                            Log::info("SimpleCommentOperation::on_comment_delete_request_completed->Reload root comment = %s after reply deletion", oper->mParentId);
                            OperationManager::GetInstance()->AddOperation(MakeSptr<SimpleCommentOperation>(OT_Reload_Comment, nullptr, oper->mParentObjectIsPost , nullptr, oper->mParentId));
                        } else if (oper->GetType() == Operation::OT_Delete_Comment) {
                            Log::info("SimpleCommentOperation::on_comment_delete_request_completed->Get new photo details for %s", oper->mParentId);
                            AppEvents::Get().Notify(ePHOTO_COMMENTS_LIST_WAS_MODIFIED, oper->mParentId);
                        }
                    }
                }
            }
            delete cmntRespond;
        } else {
            Log::info("SimpleCommentOperation->Error parsing http respond" );
        }
    }
    else {
        Log::info("SimpleCommentOperation->Error sending http request");
    }

    if (respond != nullptr) {
        //Attention!! client should take care to free respond buffer
        free(respond);
    }
}

bool SimpleCommentOperation::IsReloadError() {
    if(mIsReloadInProgress) {
        return mOpType == OT_Add_Comment || mOpType == OT_Add_Reply ;
    }
    return false;
}

void SimpleCommentOperation::ResetOperation() {
    Log::info("SimpleCommentOperation::ResetOperation");
    if(!mIsReloadInProgress) {
        Operation::ResetOperation();
    } else {
        mCurrentStep = 1;
        mStepError = false;
    }
}

void SimpleCommentOperation::PutInErrorState() {
    Log::info("SimpleCommentOperation::PutInErrorState");
    if(!mIsReloadInProgress) {
        Operation::PutInErrorState();
    }
}

void SimpleCommentOperation::OnOperationComplete() {
    Operation::OnOperationComplete();
    Log::info("SimpleCommentOperation::OnOperationComplete, mStepError: `%d`, mErrorCode: `%d` OR %d", mStepError, mErrorCode, mOperationResult);

    if(IsFailedBecauseOfGoingOffline()) {
        PauseOperation();
    }
}

bundle* SimpleCommentOperation::GetBundle() {
    return mParamsBundle;
}

const char* SimpleCommentOperation::GetPhotoFileName() {
    return mPhotoFileName;
}

const char * SimpleCommentOperation::GetPhotoFileThumbnailPath(){
    return mPhotoFileThumbnailPath;
}
const char* SimpleCommentOperation::GetParentId() {
    return mParentId;
}

Comment * SimpleCommentOperation::GetComment()
{
    return mComment;
}

const char * SimpleCommentOperation::GetCommentId() {
    return mCommentId;
}

bool SimpleCommentOperation::Cancel() {
    Log::info("SimpleCommentOperation::Cancel");
    if (mRequest) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(mRequest);
    }
    Operation::Cancel();
    return true;
}

