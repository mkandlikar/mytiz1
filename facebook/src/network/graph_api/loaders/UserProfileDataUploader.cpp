#include "Config.h"
#include "ErrorHandling.h"
#include "FacebookSession.h"
#include "FbRespondUserProfileData.h"
#include "jsonutilities.h"
#include "Log.h"
#include "LoggedInUserData.h"
#include "ParserWrapper.h"
#include "UserProfileData.h"
#include "UserProfileDataUploader.h"
#include "Utils.h"

UserProfileDataUploader::UserProfileDataUploader(const char *id, profile_data_uploaded done_cb, void *parent)
{
    Log::info(LOG_FACEBOOK_USER, "UserProfileDataUploader constructed");
    mParent = parent;
    mBasicDataUploader = NULL;
    mFirstFriendUploader = NULL;
    mFirstPhotoUploader = NULL;
    mAvatarIdUploader = NULL;
    mUserData = NULL;
    mId = SAFE_STRDUP(id);
    mUploadingDone = done_cb;
}

UserProfileDataUploader::~UserProfileDataUploader()
{
    FacebookSession::GetInstance()->ReleaseGraphRequest(mBasicDataUploader);
    FacebookSession::GetInstance()->ReleaseGraphRequest(mFirstFriendUploader);
    FacebookSession::GetInstance()->ReleaseGraphRequest(mFirstPhotoUploader);
    FacebookSession::GetInstance()->ReleaseGraphRequest(mAvatarIdUploader);

    if (mUserData) {
        mUserData->Release();
    }

    free((void *) mId); mId = NULL;
}

void UserProfileDataUploader::StartUploading()
{
    Log::info(LOG_FACEBOOK_USER, "UserProfileDataUploader::StartUploading");

    mBasicDataUploader = FacebookSession::GetInstance()->GetUserProfileBaseInfo(mId, on_user_profile_info_received_cb, this);
}

void UserProfileDataUploader::on_user_profile_info_received_cb(void* object, char* respond, int code)
{
    Log::info(LOG_FACEBOOK_USER, "UserProfileDataUploader::on_user_profile_info_received_cb Code = %d  Response = %s",code, (respond?respond:"") );

    UserProfileDataUploader *uploader = static_cast<UserProfileDataUploader*>(object);
    CHECK_RET_NRV(uploader);

    FacebookSession::GetInstance()->ReleaseGraphRequest(uploader->mBasicDataUploader);

    if (code == CURLE_OK) {
        Log::info(LOG_FACEBOOK_USER, "response: %s", respond);
        FbRespondUserProfileData *profileData = FbRespondUserProfileData::createFromJson(respond);
        if (profileData == NULL) {
            Log::error(LOG_FACEBOOK_USER, "Error parsing http respond");
        } else {
            if (profileData->mError != NULL) {
                Log::error(LOG_FACEBOOK_USER, "Error getting logged in user data, error = %s", profileData->mError->mMessage);
                if (uploader && uploader->mParent) {
                    uploader->mUploadingDone(uploader->mParent, code);
                }
            } else {
                uploader->mUserData = new UserProfileData(*static_cast<UserProfileData*>(eina_list_data_get(profileData->mDataList)));
                uploader->mFirstPhotoUploader = FacebookSession::GetInstance()->GetUserFirstPhoto(uploader->mId, on_first_photo_received_cb, uploader);
            }
            delete profileData;
        }
    } else {
        if (uploader && uploader->mParent) {
            uploader->mUploadingDone(uploader->mParent, code);
        }
        Log::error(LOG_FACEBOOK_USER, "Error sending http request");
    }
    free(respond);
}

void UserProfileDataUploader::on_first_photo_received_cb(void* object, char* respond, int code)
{
    Log::info(LOG_FACEBOOK_USER, "UserProfileDataUploader::on_first_photo_received_cb");

    UserProfileDataUploader *uploader = static_cast<UserProfileDataUploader*>(object);

    CHECK_RET_NRV(uploader);

    FacebookSession::GetInstance()->ReleaseGraphRequest(uploader->mFirstPhotoUploader);

    if (code == CURLE_OK) {
        Log::info(LOG_FACEBOOK_USER, "response: %s", respond);
        JsonObject * rootObject = NULL;
        JsonParser * parser = openJsonParser(respond, &rootObject);
        ParserWrapper autoDestroyer(parser);
        if (rootObject == NULL) {
            Log::error(LOG_FACEBOOK_USER, "Error parsing http respond");
        } else {
            if (uploader->mUserData) {
                uploader->mUserData->SetFirstPhotoData(rootObject);
            }
        }
    } else {
        Log::error(LOG_FACEBOOK_USER, "Error sending http request");
    }
    free(respond);
    uploader->mFirstFriendUploader = FacebookSession::GetInstance()->GetUserFriendPic(uploader->mId, on_first_friend_received_cb, uploader);
}

void UserProfileDataUploader::on_first_friend_received_cb(void* object, char* respond, int code)
{
    Log::info(LOG_FACEBOOK_USER, "UserProfileDataUploader::on_first_friend_received_cb");

    UserProfileDataUploader *uploader = static_cast<UserProfileDataUploader*>(object);

    CHECK_RET_NRV(uploader);

    FacebookSession::GetInstance()->ReleaseGraphRequest(uploader->mFirstFriendUploader);

    if (code == CURLE_OK) {
        Log::info(LOG_FACEBOOK_USER, "response: %s", respond);
        JsonObject * rootObject = NULL;
        JsonParser * parser = openJsonParser(respond, &rootObject);
        ParserWrapper autoDestroyer(parser);
        if (rootObject == NULL) {
            Log::error(LOG_FACEBOOK_USER, "Error parsing http respond");
        } else {
            if (uploader->mUserData) {
                uploader->mUserData->SetFirstFriendData(rootObject);
            }
        }
    } else {
        Log::error(LOG_FACEBOOK_USER, "Error sending http request");
    }
    free(respond);
    uploader->mAvatarIdUploader = FacebookSession::GetInstance()->GetUPPictureId(uploader->mId, on_avatar_id_received_cb, uploader);
}

void UserProfileDataUploader::on_avatar_id_received_cb(void* object, char* respond, int code)
{
    Log::info(LOG_FACEBOOK_USER, "UserProfileDataUploader::on_avatar_id_received_cb");

    UserProfileDataUploader *uploader = static_cast<UserProfileDataUploader*>(object);

    CHECK_RET_NRV(uploader);

    FacebookSession::GetInstance()->ReleaseGraphRequest(uploader->mAvatarIdUploader);

    if (code == CURLE_OK) {
        Log::info(LOG_FACEBOOK_USER, "response: %s", respond);
        JsonObject * rootObject = NULL;
        JsonParser * parser = openJsonParser(respond, &rootObject);
        ParserWrapper autoDestroyer(parser);
        if (rootObject == NULL) {
            Log::error(LOG_FACEBOOK_USER, "Error parsing http respond");
        } else {
            if (uploader->mUserData) {
                uploader->mUserData->SetUserAvatarData(rootObject);
            }
        }
    } else {
        Log::error(LOG_FACEBOOK_USER, "Error sending http request");
    }
    free(respond);

    if (Utils::IsMe(uploader->mId)) {
        LoggedInUserData::GetInstance().SetData(*uploader->mUserData);
    }

    if (uploader->mParent) {
        uploader->mUploadingDone(uploader->mParent, code);
    }
    AppEvents::Get().Notify(eOWN_PROFILE_DATA_UPDATED);
}
