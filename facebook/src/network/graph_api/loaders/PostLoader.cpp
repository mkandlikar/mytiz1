#include "FbRespondGetPostDetails.h"
#include "Log.h"
#include "PostLoader.h"

#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "PostLoader"

PostLoader::PostLoader(const char *id, const char *parent_id) {
    mId = SAFE_STRDUP(id);
    mParentId = SAFE_STRDUP(parent_id);
    mPost = NULL;
    mSubscriber = NULL;
    mPostRequest = NULL;
    mProcessed = false;
}

PostLoader::~PostLoader() {
    FacebookSession::GetInstance()->ReleaseGraphRequest(mPostRequest);
    free(mId);
    free(mParentId);
    if(mPost) {
        mPost->Release();
    }
}

bool PostLoader::startLoad(PostLoaderSubscriber *subscriber) {
    bool res = true;
    mSubscriber = subscriber;
    mPostRequest = FacebookSession::GetInstance()->GetPostDetails(mParentId, on_post_request_completed, this);
    return res;
}

void PostLoader::on_post_request_completed(void *object, char *response, int code) {
    Log::info_tag(LOG_TAG, "PostLoader::on_post_request_completed");
    PostLoader * postLoader = static_cast<PostLoader *>(object);
    if(postLoader) {
        if (code == CURLE_OK) {
            Log::info_tag(LOG_TAG, "response: %s", response);
            FbRespondGetPostDetails * fbResponse = FbRespondGetPostDetails::createFromJson(response);
            if (!fbResponse) {
                Log::error_tag(LOG_TAG, "Error parsing http response");
            } else {
                //prepare Post
                postLoader->mPost = fbResponse->mPost;
                if(postLoader->mPost) {
                    postLoader->mPost->AddRef();
                }
            }
            delete fbResponse;
        } else {
            Log::error_tag(LOG_TAG, "Error sending http request");
        }

        FacebookSession::GetInstance()->ReleaseGraphRequest(postLoader->mPostRequest);

        // notify subscriber
        if(postLoader->mSubscriber) {
            postLoader->mSubscriber->LoadComplete(postLoader, code );
        }
    }
    free(response);    //Attention!! client should take care of freeing the response buffer
}

Post *PostLoader::getPost() {
    return mPost;
}
