#include "ConnectivityManager.h"
#include "FacebookSession.h"
#include "FbRespondGetPostDetails.h"
#include "FbRespondPostObjectId.h"
#include "FbResponseImageDetails.h"
#include "GraphObjectDownloader.h"
#include "Photo.h"
#include "Log.h"

#include <cassert>

static const char* LogTag = "GraphObjectDownloader";

GraphObjectDownloader::GraphObjectDownloader(const char *id, IGraphObjectDownloaderObserver *observer) : mObject(nullptr), mObserver(observer),
        mRequest(nullptr), mState(ENotStarted) {
    mId = SAFE_STRDUP(id);
    mActionAndData = nullptr;
    AppEvents::Get().Subscribe(eINTERNET_CONNECTION_CHANGED, this);
}

GraphObjectDownloader::GraphObjectDownloader(const char *id, IGraphObjectDownloaderObserver *observer, ActionAndData *data) : mObject(nullptr), mObserver(observer),
        mRequest(nullptr), mState(EAlbumDownloaded) {
    mId = SAFE_STRDUP(id);
    mActionAndData = data;
    AppEvents::Get().Subscribe(eINTERNET_CONNECTION_CHANGED, this);
}

GraphObjectDownloader::~GraphObjectDownloader() {
    free(mId);
    if (mObject) {
        mObject->Release();
    }
    if (mRequest) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(mRequest, true);
    }
}

void GraphObjectDownloader::StartDownloading() {
    Log::info_tag(LogTag, "GraphObjectDownloader::StartDownloading");
    if (!mRequest) {
        switch (mState) {
        case ENotStarted:
            mRequest = FacebookSession::GetInstance()->GetPostDetails(mId, on_post_request_completed, this);
            mObserver->GraphObjectDownloaderProgress(IGraphObjectDownloaderObserver::EDownloadingStarted);
            break;
        case EPostDownloaded:
            mRequest = FacebookSession::GetInstance()->GetPhotoDetails(mId, on_photo_request_completed, this);
            mObserver->GraphObjectDownloaderProgress(IGraphObjectDownloaderObserver::EDownloadingStarted);
            break;
        case EPhotoDownloaded:
            break;
        case EAlbumDownloaded:
            mRequest = FacebookSession::GetInstance()->GetAlbumDetailsForPhoto(mId, on_get_object_id_details_completed, this);
            break;
        default:
            break;
        }
    }
}

void GraphObjectDownloader::on_post_request_completed(void* object, char* respond, int code) {
    Log::info_tag(LogTag, "GraphObjectDownloader::on_post_request_completed");
    GraphObjectDownloader *me = static_cast<GraphObjectDownloader*>(object);
    assert(me);
    FacebookSession::GetInstance()->ReleaseGraphRequest(me->mRequest, true);
    me->mRequest = nullptr;

    if (code == CURLE_OK) {
        me->mState = EPostDownloaded;
        Log::debug_tag(LogTag, "GraphObjectDownloader: response: %s", respond);
        FbRespondGetPostDetails * fbRespond = FbRespondGetPostDetails::createFromJson(respond);
        if (fbRespond) {
            if (fbRespond->mError) {
                Log::error_tag(LogTag, "GraphObjectDownloader: Error message: %s", fbRespond->mError->mMessage);
                /**
                 * Small workaround in case of Bugzilla 2084 issue. When we have notification
                 * "%username% likes your photo" we got ID of a PHOTO - not POST!.
                 * So we must do another request to get photo information and SetData. If we still got error
                 * than we must show error on the screen (elm_object_signal_emit(postScreen->mLayout, "error.show", "");) Discussed with Egor
                 */
                me->mRequest = FacebookSession::GetInstance()->GetPhotoDetails(me->mId, on_photo_request_completed, me);
            } else {
                if (fbRespond->mPost) {
                    me->mObject = fbRespond->mPost;
                    me->mObject->AddRef();
                    me->mObserver->GraphObjectDownloaderProgress(IGraphObjectDownloaderObserver::EPostDownloaded);
                } else {
                    //AAA ToDo: This is the 1st step of refactoring and the current logic isn't changed, but we need to handle this case properly in the future.
                    Log::error_tag(LogTag, "GraphObjectDownloader: Parsing error.");
                }
            }
            delete fbRespond;
        } else {
            //AAA ToDo: This is the 1st step of refactoring and the current logic isn't changed, but we need to handle this case properly in the future.
            Log::error_tag(LogTag, "GraphObjectDownloader: Error parsing http respond");
        }
    } else {
        Log::error_tag(LogTag, "GraphObjectDownloader: Error sending http request");
        me->mObserver->GraphObjectDownloaderProgress(IGraphObjectDownloaderObserver::ENetworkErrorToBeResumed);
    }

    free(respond);
}

void GraphObjectDownloader::on_photo_request_completed(void* object, char* respond, int code) {
    Log::info_tag(LogTag, "GraphObjectDownloader::on_photo_request_completed");
    GraphObjectDownloader *me = static_cast<GraphObjectDownloader*>(object);
    assert(me);
    FacebookSession::GetInstance()->ReleaseGraphRequest(me->mRequest, true);
    me->mRequest = nullptr;

    if (code == CURLE_OK) {
        me->mState = EPhotoDownloaded;
        Log::debug_tag(LogTag, "GraphObjectDownloader: response: %s", respond);
        FbResponseImageDetails * fbRespond = FbResponseImageDetails::createFromJson(respond);
        if (fbRespond) {
            if (fbRespond->mError) {
                Log::error_tag(LogTag, "GraphObjectDownloader: Error message: %s", fbRespond->mError->mMessage);
                me->mObserver->GraphObjectDownloaderProgress(IGraphObjectDownloaderObserver::ENothingDownloaded);
            } else {
                if (fbRespond->mPhoto) {
                    me->mObject = fbRespond->mPhoto;
                    me->mObject->AddRef();
                    me->mObserver->GraphObjectDownloaderProgress(IGraphObjectDownloaderObserver::EPhotoDownloaded);
                } else {
                    //AAA ToDo: This is the 1st step of refactoring and the current logic isn't changed, but we need to handle this case properly in the future.
                    Log::error_tag(LogTag, "GraphObjectDownloader: Parsing error.");
                }
            }
            delete fbRespond;
        } else {
            //AAA ToDo: This is the 1st step of refactoring and the current logic isn't changed, but we need to handle this case properly in the future.
            Log::error_tag(LogTag, "GraphObjectDownloader: Error parsing http respond");
        }
    } else {
        Log::error_tag(LogTag, "GraphObjectDownloader: Error sending http request");
        me->mObserver->GraphObjectDownloaderProgress(IGraphObjectDownloaderObserver::ENetworkErrorToBeResumed);
    }

    free(respond);
}

void GraphObjectDownloader::on_get_object_id_details_completed(void *data, char* str, int code)
{
    GraphObjectDownloader *me = static_cast<GraphObjectDownloader*>(data);
    if (me) {
        FacebookSession::GetInstance()->ReleaseGraphRequest(me->mRequest);
        if (code == CURLE_OK) {
            Log::debug_tag(LogTag, "GraphObjectDownloader: response: %s", str);
            if (str) {
                FbRespondPostObjectId *fbRespond = FbRespondPostObjectId::createFromJson(str);
                if (fbRespond) {
                    if (fbRespond->mError) {
                        Log::error_tag(LogTag, "GraphObjectDownloader: Error message: %s", fbRespond->mError->mMessage);
                    } else {
                        if (fbRespond->mAlbum) {
                            me->mObject = fbRespond->mAlbum;
                            me->mObject->AddRef();
                            me->mObserver->DownloaderProgress(IGraphObjectDownloaderObserver::EAlbumDownloaded, me);
                        }
                    }
                    delete fbRespond;
                } else {
                    Log::error_tag(LogTag, "GraphObjectDownloader: Error parsing http respond");
                }
            }
        } else {
            Log::error_tag(LogTag, "GraphObjectDownloader: Error sending http request");
        }
    }
    free(str);
}

void GraphObjectDownloader::Update(AppEventId eventId, void * data) {
    Log::info_tag(LogTag, "GraphObjectDownloader::Update:%d", eventId);
    switch (eventId) {
    case eINTERNET_CONNECTION_CHANGED:
        if (ConnectivityManager::Singleton().IsConnected()) {  //connection has been restored
            StartDownloading();
        }
        break;
    default:
        break;
    }
}

