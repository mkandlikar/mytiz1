#include "CacheManager.h"
#include "Common.h"
#include "DataTables.h"
#include "FacebookSession.h"
#include "FbRespondGetPrivacyOptions.h"
#include "Log.h"
#include "PrivacyOptionsData.h"

#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "POSTCOMPOSER"

const char *PrivacyOption::KBigIconPostfix = "_big.png";
const char *PrivacyOption::KSmallIconPostfix = "_small.png";


/*
 * Class PostComposerScreen::PrivacyOptionsData
*/

PrivacyOptionsData::PrivacyOptionsData() : mOptions(nullptr), mObservers(nullptr),
        mGetPrivacyOptionsRequest(nullptr) {
    GetPrivacyOptionsFromDB();
}

PrivacyOptionsData::~PrivacyOptionsData() {
    void *listData;
    if(mOptions) {
        EINA_LIST_FREE(mOptions, listData) {
            PrivacyOption *po = (PrivacyOption *) listData;
            delete po;
        }
    }
    eina_list_free(mObservers);
    FacebookSession::GetInstance()->ReleaseGraphRequest(mGetPrivacyOptionsRequest);
}

void PrivacyOptionsData::GetPrivacyOptionsFromFB(IPrivacyOptionsObserver *observer) {
    if (observer && !eina_list_data_find(mObservers, observer)) {
        mObservers = eina_list_append(mObservers, observer);
    }
    StartGetPrivacyOptionsRequest();
}

void PrivacyOptionsData::GetPrivacyOptionsFromDB()
{
    std::string response = CacheManager::GetInstance().GetSettingsValue(Tables::Settings::ValueName::LAST_PRIVACY_OPTIONS_JSON.c_str());
    ParsePrivacyOptionsFromJson(response.c_str());
}

void PrivacyOptionsData::RemoveObserver(IPrivacyOptionsObserver *observer) {
    if (observer) {
        mObservers = eina_list_remove(mObservers, observer);
    }
}

void PrivacyOptionsData::StartGetPrivacyOptionsRequest() {
    if (mGetPrivacyOptionsRequest == nullptr) {
        mGetPrivacyOptionsRequest = FacebookSession::GetInstance()->GetPrivacyOptions(
                on_get_privacy_options_completed, this);
    }
}

void PrivacyOptionsData::on_get_privacy_options_completed(void* object, char* respond, int code) {
    Log::debug_tag(LOG_TAG, "on_get_privacy_options_completed");

    PrivacyOptionsData *me = static_cast<PrivacyOptionsData *>(object);

    FacebookSession::GetInstance()->ReleaseGraphRequest(me->mGetPrivacyOptionsRequest);

    if (code == CURLE_OK) {
        Log::debug_tag(LOG_TAG, "response: %s", respond);
        CacheManager::GetInstance().SetSettingsValue(Tables::Settings::ValueName::LAST_PRIVACY_OPTIONS_JSON.c_str(), respond);
        me->ParsePrivacyOptionsFromJson(respond);
    } else {
        Log::error_tag(LOG_TAG, "Error sending http request");
    }

    if (respond != nullptr) {
        //Attention!! client should take care to free respond buffer
        free(respond);
    }
}

void PrivacyOptionsData::ParsePrivacyOptionsFromJson(const char* json)
{
    FbRespondGetPrivacyOptions *privacyOptionsResponse = FbRespondGetPrivacyOptions::createFromJson(json);

    if (privacyOptionsResponse == nullptr) {
        Log::error_tag(LOG_TAG, "Error parsing http respond");
    } else {
        if (privacyOptionsResponse->mError != nullptr) {
            Log::error_tag(LOG_TAG, "Error sending get privacy options, error = %s", privacyOptionsResponse->mError->mMessage);
        } else {
            UpdateOptions(privacyOptionsResponse->mPrivacyOptionsList);
            Eina_List *l;
            void *listData;
            EINA_LIST_FOREACH(mObservers, l, listData) {
                IPrivacyOptionsObserver *observer = static_cast<IPrivacyOptionsObserver *> (listData);
                observer->UpdatePrivacyOptions(mOptions);
            }
            mObservers = eina_list_free(mObservers);
        }
        delete privacyOptionsResponse;
    }
}

void PrivacyOptionsData::UpdateOptions(Eina_List *newOptions) {
    PrivacyOption *option = nullptr;

    Eina_List *l;
    void *listData;
    EINA_LIST_FOREACH(mOptions, l, listData) {
        PrivacyOption *po = static_cast<PrivacyOption *> (listData);
        delete po;
    }
    mOptions = eina_list_free(mOptions);

    EINA_LIST_FOREACH(newOptions, l, listData) {
        FbRespondGetPrivacyOptions::PrivacyOption *newOp = static_cast<FbRespondGetPrivacyOptions::PrivacyOption *> (listData);
        Log::debug_tag(LOG_TAG, "newOp->mType: %s", newOp->mType);
        option = new PrivacyOption(newOp->mId, newOp->mDescription, newOp->mType, newOp->mIconSrc, newOp->mIsCurrentlySelected);
        mOptions = eina_list_append(mOptions, option);
    }
}


/*
 * Class PrivacyOption
 */
PrivacyOption::PrivacyOption(const char *id, const char *description, const char *type, const char *icon, bool isCurrentlySelected) :
        mId(nullptr),
        mDescription(nullptr),
        mType(nullptr),
        mIcon(nullptr),
        mIsCurrentlySelected(isCurrentlySelected) {

    if (id) {
        mId = new char[strlen(id) + 1];
        eina_strlcpy(mId, id, strlen(id) + 1);
    }
    if (description) {
        mDescription = new char[strlen(description) + 1];
        eina_strlcpy(mDescription, description, strlen(description) + 1);
    }
    if (type) {
        mType = new char[strlen(type) + 1];
        eina_strlcpy(mType, type, strlen(type) + 1);
    }
    if (icon) {
        mIcon = new char[strlen(icon) + 1];
        eina_strlcpy(mIcon, icon, strlen(icon) + 1);
    }
}

PrivacyOption::~PrivacyOption(){
    delete [] mId;
    delete [] mDescription;
    delete [] mType;
    delete [] mIcon;
}

