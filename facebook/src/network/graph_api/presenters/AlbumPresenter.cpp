#include "AlbumPresenter.h"
#include <app_i18n.h>
#include <sstream>

AlbumPresenter::AlbumPresenter(Album& album): mAlbum(album) {
    mAlbum.AddRef();
}

AlbumPresenter::~AlbumPresenter() {
    mAlbum.Release();
}

std::string AlbumPresenter::GenPhotosNumber() {
    std::stringstream photoCountTextStream;
    photoCountTextStream << mAlbum.mCount << " " << (mAlbum.mCount == 1 ? "IDS_ALBUMS_PHOTO" : "IDS_ALBUMS_PHOTOS");
    return std::move(photoCountTextStream.str());
}
