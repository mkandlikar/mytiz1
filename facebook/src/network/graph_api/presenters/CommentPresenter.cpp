#include "CommentPresenter.h"
#include "PresenterHelpers.h"
#include "UIRes.h"
#include "WidgetFactory.h"
#include <app_i18n.h>
#include <memory>
#include <sstream>

CommentPresenter::CommentPresenter(Comment& comment): mComment(comment) {
    mComment.AddRef();
}

CommentPresenter::~CommentPresenter() {
    mComment.Release();
}

std::string CommentPresenter::GetCommentMessage() {
    return std::move(PresenterHelpers::AddFriendTags(mComment.GetMessage() ? mComment.GetMessage() : "", mComment.GetMessageTagsList(), PresenterHelpers::eFeed));
}

std::string CommentPresenter::GetUserWithReply() {
    std::string replyContent;
    if(mComment.GetFrom()) {
       if(mComment.GetMessage()) {
           replyContent = mComment.GetMessage();
           replyContent = Utils::ReplaceString(replyContent, "&", "&amp;");
           replyContent = Utils::ReplaceString(replyContent, "\n", " ");

           std::unique_ptr<char[]> cuttedReplyContent(Utils::UTF8TextCutter(replyContent.c_str(), UIRes::GetInstance()->REPLY_READ_MORE_LIMIT));
           if (cuttedReplyContent) {
               replyContent = cuttedReplyContent.get();
           }
       } else {
           if(mComment.mAttachment && !strcmp(mComment.mAttachment->mType, "photo" )) {
               replyContent = "IDS_REPLIED_WITH_PHOTO";
           } else if( mComment.mAttachment && !strcmp(mComment.mAttachment->mType, "sticker" )) {
               replyContent = "IDS_REPLIED_WITH_STICKER";
           } else if(mComment.GetFrom()->mName && strlen(mComment.GetFrom()->mName) > 25) {
               replyContent = "IDS_REPLIED";
           }
       }

       std::unique_ptr<char[]> wrappedReply(WidgetFactory::WrapByFormat5(COMMENT_ITEM_REPLY_ITEM_FORMAT_5,
                                                                         UIRes::GetInstance()->COMMENT_REPLY_ITEM_FONT_SIZE,
                                                                         mComment.GetFrom()->GetId(),
                                                                         mComment.GetFrom()->mName,
                                                                         UIRes::GetInstance()->COMMENT_REPLY_ITEM_FONT_SIZE,
                                                                         replyContent.c_str()));
       if(wrappedReply) replyContent = wrappedReply.get();
    }
    return std::move(replyContent);
}

std::string CommentPresenter::GetRepliesCount() {
    int repliesCount(mComment.GetRepliesCount() - 1);
    std::stringstream repliesCountTextStream;
    if(repliesCount > 0) {
        repliesCountTextStream << "IDS_VIEW" << " " << repliesCount << " " << (repliesCount == 1 ? "IDS_COMMENT_REPLY" : "IDS_COMMENT_REPLIES");
    }
    return std::move(repliesCountTextStream.str());
}
