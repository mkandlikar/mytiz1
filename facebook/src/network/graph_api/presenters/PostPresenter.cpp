#include "Application.h"
#include "PostPresenter.h"
#include "Common.h"
#include "CSmartPtr.h"
#include "ErrorHandling.h"
#include "PresenterHelpers.h"
#include "UIRes.h"
#include "Utils.h"
#include "WidgetFactory.h"
#include <app_i18n.h>
#include <memory>
#include <sstream>

PostPresenter::PostPresenter(PresentableAsPost& post): mPost(post) {
    mPost.AddRef();
}

PostPresenter::~PostPresenter() {
    mPost.Release();
}

std::string PostPresenter::GetApplicationName() {
    return mPost.GetApplicationName();
}

std::string PostPresenter::GetFromName() {
    std::unique_ptr<char[]> fromNameWrappedText(WidgetFactory::WrapByFormat2(FEED_ITEM_TITLE_NAME_FORMAT_2, mPost.GetFromId().c_str(), mPost.GetFromName().c_str()));
    return fromNameWrappedText ? fromNameWrappedText.get() : "";
}

std::string PostPresenter::GetCaption() {
    return mPost.GetCaption();
}

std::string PostPresenter::GetCommentsCount() {
    std::string countString = Utils::FormatBigInteger(mPost.GetCommentsCount());
    std::stringstream commentsCountTextStream;
    commentsCountTextStream << countString << " " << (mPost.GetCommentsCount() == 1 ? "IDS_STATUS_COMMENT" : "IDS_STATUS_COMMENT_PLURAL");
    return commentsCountTextStream.str();
}

std::string PostPresenter::GetImageComments() {
    std::string countString = Utils::FormatBigInteger(mPost.GetCommentsCount());
    std::stringstream imageCommentsCountTextStream;
    imageCommentsCountTextStream << countString << " " << (mPost.GetCommentsCount() == 1 ? "IDS_POST_IMAGE_COMMENT" : "IDS_POST_IMAGE_COMMENTS");
    return imageCommentsCountTextStream.str();
}

std::string PostPresenter::GetFriendLocationList() {
    std::string strFinal;
    std::list<Friend> friendList(mPost.GetPostFriendsList());
    size_t listLength(friendList.size());
    for(auto friendIt = friendList.begin(); friendIt != friendList.end(); friendIt++) {
        std::string tagFriend1(PrepareFriendTag(*friendIt));
        if(listLength == 1) {
            strFinal += tagFriend1;
        } else if(listLength == 2) {
            friendIt++;
            std::string tagFriend2(PrepareFriendTag(*friendIt));
            std::unique_ptr<char[]> tagFormat(WidgetFactory::WrapByFormat2(i18n_get_text("IDS_FRIEND1_AND_FRIEND2"), tagFriend1.c_str(), tagFriend2.c_str()));
            if(tagFormat) strFinal += tagFormat.get();
        } else {
            std::unique_ptr<char[]> tagFormat(WidgetFactory::WrapByFormat2(i18n_get_text("IDS_FRIEND1_AND_NUM_OTHER"), tagFriend1.c_str(), std::to_string(listLength - 1).c_str()));
            if(tagFormat) strFinal += tagFormat.get();
        }
    }
    return strFinal;
}

std::string PostPresenter::PrepareFriendTag(const Friend& friendDesc) {
    std::unique_ptr<char[]> wrappedFriend(WidgetFactory::WrapByFormat3(FEED_ITEM_TAG_FORMAT_3, "u", friendDesc.GetId(), friendDesc.mName.c_str()));
    return wrappedFriend ? wrappedFriend.get() : "";
}

std::string PostPresenter::GetLikesCount() {
    std::string countString = Utils::FormatBigInteger(mPost.GetLikesCount());
    std::stringstream likesCountTextStream;
    likesCountTextStream << countString << " " << (mPost.GetLikesCount() == 1 ? "IDS_STATUS_LIKE" : "IDS_STATUS_LIKE_PLURAL");
    return likesCountTextStream.str();
}

std::string PostPresenter::GetImageLikes() {
    std::string countString = Utils::FormatBigInteger(mPost.GetLikesCount());
    std::stringstream imagesLikesCountTextStream;
    imagesLikesCountTextStream << countString << " " << (mPost.GetLikesCount() == 1 ? "IDS_POST_IMAGE_LIKE" : "IDS_POST_IMAGE_LIKES");
    return imagesLikesCountTextStream.str();
}

std::string PostPresenter::GetLikesAndCommentsStats() {
    std::stringstream postLikesAndCommentsTextStream;
    postLikesAndCommentsTextStream << (mPost.GetLikesCount() > 0 ? GetLikesCount() : "")
                                   << (mPost.GetLikesCount() > 0 ? " " : "")
                                   << (mPost.GetCommentsCount() > 0 ? GetCommentsCount() : "");
    return postLikesAndCommentsTextStream.str();
}

std::string PostPresenter::GetImageLikesAndComments() {
    std::stringstream imageLikesAndCommentsTextStream;
    imageLikesAndCommentsTextStream << (mPost.GetLikesCount() > 0 ? GetImageLikes() : "")
                                    << (mPost.GetLikesCount() > 0 ? " " : "")
                                    << (mPost.GetCommentsCount() > 0 ? GetImageComments() : "");
    return imageLikesAndCommentsTextStream.str();
}

std::string PostPresenter::GetLocationBox() {
    std::string taggedFriendsList(GetFriendLocationList());
    if(!taggedFriendsList.empty()) {
        std::unique_ptr<char[]> wrappedLocationWithFriends(WidgetFactory::WrapByFormat5(FEED_ITEM_TITLE_LOCATION_FORMAT_5,
                                                                                        GetFromName().c_str(),
                                                                                        i18n_get_text("IDS_WITH"),
                                                                                        taggedFriendsList.c_str(),
                                                                                        i18n_get_text("IDS_IN"),
                                                                                        GetLocationPlace().c_str()));
        if(wrappedLocationWithFriends) return wrappedLocationWithFriends.get();
    } else {
        std::unique_ptr<char[]> wrappedLocation(WidgetFactory::WrapByFormat4(FEED_ITEM_TITLE_LOCATION_FORMAT_4,
                                                                             GetFromName().c_str(),
                                                                             i18n_get_text("IDS_IN"),
                                                                             mPost.GetLocationId().c_str(),
                                                                             GetLocationPlace().c_str()));
        if(wrappedLocation) return wrappedLocation.get();
    }
    return "";
}

std::string PostPresenter::GetLocationPlace() {
    return mPost.GetLocationName();
}

std::string PostPresenter::GetLocationType() {
    std::unique_ptr<char[]> wrappedLocationType(WidgetFactory::WrapByFormat3(PLACE_INFO_ITEM_FORMAT_3,
                                                                             mPost.GetLocationType().c_str(),
                                                                             std::to_string(mPost.GetCheckinsNumber()).c_str(),
                                                                             i18n_get_text("IDS_CHECK_IN")));
    return wrappedLocationType ? wrappedLocationType.get() : "";
}

std::string PostPresenter::GetMessage(bool createContinueReading) {
    std::string message = mPost.GetMessage() ? mPost.GetMessage() : "";
    if(createContinueReading) {
        AppendContinueReading(message);
    }
    return PresenterHelpers::AddFriendTags(message, mPost.GetMessageTagsList(), PresenterHelpers::eFeed);
}

std::string PostPresenter::GetMultipleLikeBox() {
    std::unique_ptr<char[]> wrappedMultipleLikeBox(WidgetFactory::WrapByFormat2(FEED_ITEM_MULTIPLE_LIKE_FORMAT_2, GetFromName().c_str(), i18n_get_text("IDS_LIKES")));
    return wrappedMultipleLikeBox ? wrappedMultipleLikeBox.get() : "";
}

std::string PostPresenter::GetStory() {
    std::string story(mPost.GetStory());
    if(story.empty()) {
        story = GetFromName();
    } else {
        Utils::FoundAndReplaceText(story, "Page", "page" ); //workaround, somehow "page" in story turns into "Page" on device
        story = PresenterHelpers::AddFriendTags(story, mPost.GetStoryTagsList(), PresenterHelpers::eFeed);
        const char *albumName = mPost.GetAlbumName();
        if (mPost.IsPhotoAddedToAlbum() && albumName && (story.at(story.length() - 1) == '.')) {
            std::stringstream toAlbum;
            toAlbum << " " << "IDS_TO_THE_ALBUM" << " " << albumName;
            std::string toAlbumAnchor = PresenterHelpers::PutAnchorInText(toAlbum.str(), 'a', albumName, "", 0, true);
            story.insert(story.length() - 1, toAlbumAnchor);
        }
        std::unique_ptr<char[]> wrappedStory(WidgetFactory::WrapByFormat2(SANS_MEDIUM_000_FORMAT, UIRes::GetInstance()->NF_PH_TITLE_TEXT_SIZE, story.c_str()));
        story = wrappedStory ? wrappedStory.get() : "";
    }
    return story;
}

std::string PostPresenter::CutMessage(bool createContinueReading) {
    std::string message = mPost.GetMessage()? mPost.GetMessage(): "";
    unsigned int linesNumber = Utils::GetNLineLength (message, UIRes::GetInstance()->MESSAGE_READ_MORE_LINES_LIMIT);

    int messageLength = linesNumber;
    if (linesNumber == 0 || linesNumber > UIRes::GetInstance()->MESSAGE_READ_MORE_LIMIT) {
        messageLength = UIRes::GetInstance()->MESSAGE_READ_MORE_LIMIT;
    }
    if (Utils::UStrLen(message.c_str()) > messageLength || createContinueReading) {
        std::unique_ptr<char[]> cuttedMessage(Utils::UTF8TextCutter(message.c_str(), messageLength, false));
        if(cuttedMessage) {
           message = cuttedMessage.get();
           if(linesNumber == 0) {
              message.append( message.length() ? "\n" : " ");
           }
           message.append(FEED_ITEM_CUT_MESSAGE_FORMAT_1);
       } else {
           message = FEED_ITEM_CUT_MESSAGE_FORMAT_1;
       }
    }
    return PresenterHelpers::AddFriendTags(message, mPost.GetMessageTagsList(), PresenterHelpers::eFeed);
}

std::string PostPresenter::ComposeHeaderFromNameTagsAndLocation() {
    std::stringstream composedHeader;
    composedHeader << GetFromName() << PresenterHelpers::AddFriendTags(" ", mPost.GetMessageTagsList(), PresenterHelpers::eFeed);
    ComposeWithTagsAndLocationSubstr(composedHeader);
    return composedHeader.str();
}

std::string PostPresenter::ComposeFullTextMessageWithTagsAndLocation(bool createContinueReading) {
    std::stringstream withTagsAndLocation;
    ComposeWithTagsAndLocationSubstr(withTagsAndLocation);
    std::string message = GetMessage();

    if(!withTagsAndLocation.str().empty()) {
         message.append (" - ");
         message.append(withTagsAndLocation.str());
    }

    if(createContinueReading) {
        message.append("<br/>");
        AppendContinueReading(message);
    }
    return message;
}

std::string PostPresenter::GetLinkName() {
    if(mPost.GetName().empty() && mPost.GetAttachmentTitle().empty()) return "";
    std::string link(!mPost.GetName().empty() ? mPost.GetName() : mPost.GetAttachmentTitle());
    return Utils::ReplaceString(link, "\n", "<br/>");
}

std::string PostPresenter::GetPollOption() {
    std::string text;
    std::list<Tag> subAttachmentList(mPost.GetSubattachmentList());
    for(auto tagIt = subAttachmentList.begin(); tagIt != subAttachmentList.end(); tagIt++) {
        if (tagIt->mType == "option") {
            text = tagIt->mName;
            break;
        }
    }

    return text;
}

std::string PostPresenter::GetViaName() {
    std::unique_ptr<char[]> wrappedViaName(WidgetFactory::WrapByFormat5(FEED_ITEM_TITLE_VIA,
                                                                        mPost.GetFromId().c_str(),
                                                                        mPost.GetFromName().c_str(),
                                                                        i18n_get_text("IDS_VIA"),
                                                                        mPost.GetViaId().c_str(),
                                                                        mPost.GetViaName().c_str()));
    return wrappedViaName ? wrappedViaName.get() : "";
}

std::string PostPresenter::GetToStory() {
    std::string fromName(mPost.GetFromName());
    Utils::FoundAndReplaceText(fromName, "&", "&amp;");
    std::string toName(std::move(mPost.GetToName()));
    Utils::FoundAndReplaceText(toName, "&", "&amp;");
    char *arrow = NULL;
    bool isRTL = Application::IsRTLLanguage();
    if (Application::GetInstance()->GetPlatformVersion() < 3) {
        arrow = WidgetFactory::WrapByFormat("%s", " \u25B6 ");
    } else {
        arrow = WidgetFactory::WrapByFormat2(SANS_REGULAR_000_FORMAT, "27", isRTL?" \u25C4 ":" \u25BA ");
    }
    std::unique_ptr<char[]> wrappedStory( WidgetFactory::WrapByFormat5(FEED_ITEM_TITLE_TO_FRIEND_FEED,
                                                                       isRTL ? mPost.GetToId() : mPost.GetFromId().c_str(),
                                                                       isRTL ? toName.c_str() : fromName.c_str(),
                                                                       arrow,
                                                                       isRTL ? mPost.GetFromId().c_str() : mPost.GetToId(),
                                                                       isRTL ? fromName.c_str() : toName.c_str()));
    delete[] arrow;
    return wrappedStory ? wrappedStory.get() : "";
}

std::string PostPresenter::GetPageDescription() {
    return mPost.GetAttachmentDescription();
}

void PostPresenter::ComposeWithTagsAndLocationSubstr(std::stringstream& composedSubstr) {
    if(!mPost.GetLocationName().empty() && !mPost.GetAttachmentTargetUrl().empty()) {
        composedSubstr << i18n_get_text("IDS_AT") << " <b><a href=d_" << mPost.GetAttachmentTargetUrl() << ">" << mPost.GetLocationName() << "</a></b>";
    }
    if (mPost.GetWithTagsCount() > 0 && !mPost.GetWithTagsId().empty() && !mPost.GetWithTagsName().empty()) {
        composedSubstr << " " << i18n_get_text("IDS_WITH") << " <b><a href=u_" << mPost.GetWithTagsId() << ">" << mPost.GetWithTagsName() << "</a></b>";
        if (mPost.GetWithTagsCount() > 1) {
            composedSubstr << "<b> " << i18n_get_text("IDS_AND") << " " << mPost.GetWithTagsCount() - 1 << " " << i18n_get_text("IDS_OTHERS") << "</b>";
        }
    }
}

void PostPresenter::AppendContinueReading(std::string& message) {
    std::unique_ptr<char[]> reading(WidgetFactory::WrapByFormat(FEED_ITEM_CUT_MESSAGE_FORMAT_1, i18n_get_text("IDS_CONTINUE_READING")));
    if(reading) {
        message.append(reading.get());
    }
}
