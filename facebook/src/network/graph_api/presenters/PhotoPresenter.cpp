#include "Log.h"
#include "PhotoPresenter.h"
#include "PresenterHelpers.h"
#include "Utils.h"

#include <app_i18n.h>
#include <sstream>

PhotoPresenter::PhotoPresenter(Photo& photo): mPhoto(photo) {
    mPhoto.AddRef();
}

PhotoPresenter::~PhotoPresenter() {
    mPhoto.Release();
}

std::string PhotoPresenter::GetName(PresenterHelpers::Mode mode) {
    return std::move(PresenterHelpers::AddFriendTags(mPhoto.GetName() ? mPhoto.GetName() : "", mPhoto.GetPhotoMessageTagsList(), mode));
}

std::string PhotoPresenter::GetLikesCount() {
    std::string countString = Utils::FormatBigInteger(mPhoto.GetLikesCount());
    std::stringstream likesCountTextStream;
    likesCountTextStream << countString << " " << (mPhoto.GetLikesCount() == 1 ? "IDS_POST_IMAGE_LIKE" : "IDS_POST_IMAGE_LIKES");
    return std::move(likesCountTextStream.str());
}

std::string PhotoPresenter::GetCommentsCount() {
    std::string countString = Utils::FormatBigInteger(mPhoto.GetCommentsCount());
    std::stringstream commentsCountTextStream;
    commentsCountTextStream << countString << " " << (mPhoto.GetCommentsCount() == 1 ? "IDS_POST_IMAGE_COMMENT" : "IDS_POST_IMAGE_COMMENTS");
    return std::move(commentsCountTextStream.str());
}

std::string PhotoPresenter::GetLikesAndCommentsCount() {
    std::stringstream likesAndCommentTextStream;
    likesAndCommentTextStream << (mPhoto.GetLikesCount() > 0 ? GetLikesCount() : "")
                              << (mPhoto.GetLikesCount() > 0 ? " " : "")
                              << (mPhoto.GetCommentsCount() > 0 ? GetCommentsCount() : "");
    return std::move(likesAndCommentTextStream.str());
}
