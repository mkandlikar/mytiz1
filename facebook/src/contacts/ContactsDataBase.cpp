#include "ContactsDataBase.h"
#include "ErrorHandling.h"
#include "MutexLocker.h"

namespace Contacts {
    unsigned int DataBase::mDbInstanceCount = 0;
    Mutex DataBase::mMutex;
    DataBase::DataBase() {
        MutexLocker lock(&mMutex);
        if(mDbInstanceCount == 0) CHECK_CONTACTS_OPERATION(contacts_connect());
        mDbInstanceCount++;
    }

    DataBase::~DataBase() {
        MutexLocker lock(&mMutex);
        mDbInstanceCount--;
        if(mDbInstanceCount == 0) {
            CHECK_CONTACTS_OPERATION(contacts_disconnect());
        }
    }

    int DataBase::insertRecord(Record& record) {
        int id = 0;
        Log::info(LOG_CONTACT_SYNC, "DataBase::insertRecord");
        CHECK_CONTACTS_OPERATION(contacts_db_insert_record(record.mRecordHandle, &id));
        return id;
    }

    Record DataBase::getRecordById(const char* viewUri, int recordId) {
        CHECK_RET(viewUri, Record());
        Record record(viewUri);
        CHECK_CONTACTS_OPERATION(contacts_db_get_record(viewUri, recordId, &record.mRecordHandle));
        return record;
    }

    Record DataBase::getRecordByField(const char* viewUri, int fieldId, int fieldValue) {
        CHECK_RET(viewUri, Record());
        contacts_list_h queryResultlist = NULL;
        contacts_query_h filterQuery = NULL;
        CHECK_CONTACTS_OPERATION(contacts_query_create(viewUri, &filterQuery));
        contacts_filter_h contactFilter = NULL;
        CHECK_CONTACTS_OPERATION(contacts_filter_create(viewUri, &contactFilter));
        CHECK_CONTACTS_OPERATION(contacts_filter_add_int(contactFilter, fieldId, CONTACTS_MATCH_EQUAL, fieldValue));
        CHECK_CONTACTS_OPERATION(contacts_query_set_filter(filterQuery, contactFilter));
        CHECK_CONTACTS_OPERATION(contacts_db_get_records_with_query(filterQuery, 0, 1, &queryResultlist));
        contacts_record_h contactRecord = NULL;
        CHECK_CONTACTS_OPERATION(contacts_list_get_current_record_p(queryResultlist, &contactRecord));
        Record record(contactRecord, viewUri);
        CHECK_CONTACTS_OPERATION(contacts_filter_destroy(contactFilter));
        CHECK_CONTACTS_OPERATION(contacts_query_destroy(filterQuery));
        CHECK_CONTACTS_OPERATION(contacts_list_destroy(queryResultlist, true));

        return record;
    }

    Record DataBase::getRecordByField(const char* viewUri, int fieldId, const char* fieldValue) {
        CHECK_RET(viewUri, Record());
        contacts_list_h queryResultlist = NULL;
        contacts_query_h filterQuery = NULL;
        CHECK_CONTACTS_OPERATION(contacts_query_create(viewUri, &filterQuery));
        contacts_filter_h contactFilter = NULL;
        CHECK_CONTACTS_OPERATION(contacts_filter_create(viewUri, &contactFilter));
        CHECK_CONTACTS_OPERATION(contacts_filter_add_str(contactFilter, fieldId, CONTACTS_MATCH_EXACTLY, fieldValue));
        CHECK_CONTACTS_OPERATION(contacts_query_set_filter(filterQuery, contactFilter));
        CHECK_CONTACTS_OPERATION(contacts_db_get_records_with_query(filterQuery, 0, 1, &queryResultlist));
        contacts_record_h contactRecord = NULL;
        CHECK_CONTACTS_OPERATION(contacts_list_get_current_record_p(queryResultlist, &contactRecord));
        Record record(contactRecord, viewUri);
        CHECK_CONTACTS_OPERATION(contacts_filter_destroy(contactFilter));
        CHECK_CONTACTS_OPERATION(contacts_query_destroy(filterQuery));
        CHECK_CONTACTS_OPERATION(contacts_list_destroy(queryResultlist, true));

        return record;
    }

    Records DataBase::getRecordsByFilter(const Filter& filter) {
        contacts_query_h filterQuery = NULL;
        CHECK_CONTACTS_OPERATION(contacts_query_create(filter.mViewUri.c_str(), &filterQuery));
        CHECK_CONTACTS_OPERATION(contacts_query_set_filter(filterQuery, filter.mFilterHandle));
        contacts_list_h queryResultlist = NULL;
        CHECK_CONTACTS_OPERATION(contacts_db_get_records_with_query(filterQuery, 0, 0, &queryResultlist));
        int count = 0;
        CHECK_CONTACTS_OPERATION(contacts_list_get_count(queryResultlist, &count));
        Records recordList;
        for(contacts_record_h contactRecord = NULL;
            contacts_list_get_current_record_p(queryResultlist, &contactRecord) == CONTACTS_ERROR_NONE && recordList.size() < count;
            contacts_list_next(queryResultlist)) {
            recordList.push_back(Record(contactRecord, filter.mViewUri.c_str()));
        }
        CHECK_CONTACTS_OPERATION(contacts_query_destroy(filterQuery));
        CHECK_CONTACTS_OPERATION(contacts_list_destroy(queryResultlist, true));
        return recordList;
    }

    void DataBase::updateRecord(const Record& record) {
        CHECK_CONTACTS_OPERATION(contacts_db_update_record(record.mRecordHandle));
    }

    void DataBase::removeRecord(const Record& record, int recordId) {
        CHECK_CONTACTS_OPERATION(contacts_db_delete_record(record.mViewUri.c_str(), recordId));
    }
}
