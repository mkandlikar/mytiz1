/*
 * ContactsDataBaseFilter.cpp
 *
 *  Created on: Jun 27, 2016
 *      Author: ruebasargi
 */

#include "ContactsDataBaseFilter.h"
#include "ContactsException.h"

namespace Contacts {
    Filter::Filter(const char* viewUri): mFilterHandle(NULL), mViewUri(viewUri ? viewUri : "") {
        CHECK_CONTACTS_OPERATION(contacts_filter_create(mViewUri.c_str(), &mFilterHandle));
    }
    Filter::~Filter() {
        CHECK_CONTACTS_OPERATION(contacts_filter_destroy(mFilterHandle));
    }
    void Filter::addStringValue(unsigned int propertyId, contacts_match_str_flag_e matchFlag, const char* matchValue) {
        CHECK_RET_NRV(matchValue);
        CHECK_CONTACTS_OPERATION(contacts_filter_add_str(mFilterHandle, propertyId, matchFlag, matchValue));
    }
    void Filter::addIntegerValue(unsigned int propertyId, contacts_match_int_flag_e matchFlag, int matchValue) {
        CHECK_CONTACTS_OPERATION(contacts_filter_add_int(mFilterHandle, propertyId, matchFlag, matchValue));
    }
    void Filter::addOperator(contacts_filter_operator_e filterOperator) {
        CHECK_CONTACTS_OPERATION(contacts_filter_add_operator(mFilterHandle, filterOperator));
    }
}
