/*
 * ContactsException.cpp
 *
 *  Created on: Jul 4, 2016
 *      Author: ruebasargi
 */

#include "ContactsException.h"
#include "Log.h"
#include <sstream>

namespace Contacts {

    DataBaseException::DataBaseException(const std::string& operation, int errorCode, const std::string& fileName, int lineNumber) {
        std::stringstream messageStream;
        messageStream << "Contacts data base operation `"
                      << operation
                      << "` returned "
                      << getErrorString(errorCode)
                      << " in file: `"
                      << fileName
                      << "`, line number: "
                      << lineNumber;
        mErrorDescription = messageStream.str();
    }

    const char* DataBaseException::what() const throw() {
       return mErrorDescription.c_str();
    }

    std::string DataBaseException::getErrorString(int code) {
        static ErrorMapping errorMapping = initErrorCodeTable();
        ErrorMapping::iterator it = errorMapping.find(code);
        if(it == errorMapping.end()) {
            Log::error("DataBaseException::getErrorString() received unknow error code `%d`, please add it in error code table", code);
            return "";
        }
        return it->second;
    }

    DataBaseException::ErrorMapping DataBaseException::initErrorCodeTable() {
        DataBaseException::ErrorMapping errorMapping;
        errorMapping.insert(std::pair<int, std::string>(CONTACTS_ERROR_NONE, "CONTACTS_ERROR_NONE"));
        errorMapping.insert(std::pair<int, std::string>(CONTACTS_ERROR_OUT_OF_MEMORY, "CONTACTS_ERROR_OUT_OF_MEMORY"));
        errorMapping.insert(std::pair<int, std::string>(CONTACTS_ERROR_INVALID_PARAMETER, "CONTACTS_ERROR_INVALID_PARAMETER"));
        errorMapping.insert(std::pair<int, std::string>(CONTACTS_ERROR_FILE_NO_SPACE, "CONTACTS_ERROR_FILE_NO_SPACE"));
        errorMapping.insert(std::pair<int, std::string>(CONTACTS_ERROR_NO_DATA, "CONTACTS_ERROR_NO_DATA"));
        errorMapping.insert(std::pair<int, std::string>(CONTACTS_ERROR_PERMISSION_DENIED, "CONTACTS_ERROR_PERMISSION_DENIED"));
        errorMapping.insert(std::pair<int, std::string>(CONTACTS_ERROR_NOT_SUPPORTED, "CONTACTS_ERROR_NOT_SUPPORTED"));
        errorMapping.insert(std::pair<int, std::string>(CONTACTS_ERROR_DB, "CONTACTS_ERROR_DB"));
        errorMapping.insert(std::pair<int, std::string>(CONTACTS_ERROR_IPC, "CONTACTS_ERROR_IPC"));
        return errorMapping;
    }
}
