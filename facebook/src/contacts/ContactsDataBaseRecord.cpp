/*
 * ContactsDataBaseRecord.cpp
 *
 *  Created on: Jun 27, 2016
 *      Author: ruebasargi
 */

#include "ContactsDataBaseRecord.h"
#include "ContactsException.h"

namespace Contacts {
    Record::Record(): mRecordHandle(NULL), mViewUri("") {
    }

    Record::Record(const char* viewUri): mRecordHandle(NULL), mViewUri(viewUri ? viewUri : "") {
        CHECK_CONTACTS_OPERATION(contacts_record_create(viewUri, &mRecordHandle));
    }

    Record::Record(const Record& record): mRecordHandle(NULL), mViewUri(record.mViewUri) {
        CHECK_CONTACTS_OPERATION(contacts_record_clone(record.mRecordHandle, &mRecordHandle));
    }

    Record::Record(const contacts_record_h& recordHandle, const char* viewUri):
        mRecordHandle(NULL), mViewUri(viewUri ? viewUri : "") {
        CHECK_CONTACTS_OPERATION(contacts_record_clone(recordHandle, &mRecordHandle));
    }

    Record::~Record() {
        CHECK_CONTACTS_OPERATION(contacts_record_destroy(mRecordHandle, false));
    }

    void Record::operator=(const Record& record) {
        if(mRecordHandle) {
            CHECK_CONTACTS_OPERATION(contacts_record_destroy(mRecordHandle, false));
        }
        CHECK_CONTACTS_OPERATION(contacts_record_clone(record.mRecordHandle, &mRecordHandle));
        mViewUri = record.mViewUri;
    }

    void Record::setFieldValue(unsigned int propertyId, int value) {
        CHECK_CONTACTS_OPERATION(contacts_record_set_int(mRecordHandle, propertyId, value));
    }

    void Record::setFieldValue(unsigned int propertyId, const std::string& value) {
        CHECK_CONTACTS_OPERATION(contacts_record_set_str(mRecordHandle, propertyId, value.c_str()));
    }

    int Record::getIntegerValue(unsigned int propertyId) {
        int value = 0;
        CHECK_CONTACTS_OPERATION(contacts_record_get_int(mRecordHandle, propertyId, &value));
        return value;
    }

    std::string Record::getStringValue(unsigned int propertyId) {
        char* str = NULL;
        CHECK_CONTACTS_OPERATION(contacts_record_get_str_p(mRecordHandle, propertyId, &str));
        return str;
    }

    void Record::addChildRecord(unsigned int propertyId, const Record& childRecord) {
        CHECK_CONTACTS_OPERATION(contacts_record_add_child_record(mRecordHandle, propertyId, childRecord.mRecordHandle));
    }
}
