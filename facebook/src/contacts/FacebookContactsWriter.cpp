#include <sstream>
#include "Friend.h"
#include "FacebookContactsWriter.h"
#include "ContactsDataBase.h"
#include "app_control.h"
#include "Common.h"
#include "Utils.h"
#include "ErrorHandling.h"


namespace Contacts
{

FacebookContactsWriter::FacebookContactsWriter(int accountId, const std::string& addressBookName) :
        mAccountId(accountId), mAddressBookId(0), mAddressBookName(addressBookName) {
    mAddressBookId = getTheAddressBookId();
}

FacebookContactsWriter::FacebookContactsWriter() :
        mAccountId(0), mAddressBookId(0) {
}

int FacebookContactsWriter::writeFacebookContact(const Friend& contact) {
    CHECK_RET(mAccountId, 0);
    CHECK_RET(!mAddressBookName.empty(), 0);
    CHECK_RET(contact.GetId(), 0);

    if (!mAddressBookId) {
        mAddressBookId = createTheAddressBook();
    }

    CHECK_RET(mAddressBookId, 0);

    Log::info(LOG_CONTACT_SYNC, "FacebookContactsWriter::writeFacebookContact: Friend - Frist name - %s", contact.mFirstName.c_str());
    Log::info(LOG_CONTACT_SYNC, "FacebookContactsWriter::writeFacebookContact: Friend - Last name - %s", contact.mLastName.c_str());
    DataBase db;
    Record contactRecord(_contacts_contact._uri);
    contactRecord.setFieldValue(_contacts_contact.address_book_id, mAddressBookId);
    if (contact.GetId())
        contactRecord.setFieldValue(_contacts_contact.uid, contact.GetId());

    Record contactNameRecord(_contacts_name._uri);
    contactNameRecord.setFieldValue(_contacts_name.first, contact.mFirstName);
    contactNameRecord.setFieldValue(_contacts_name.last, contact.mLastName);
    contactRecord.addChildRecord(_contacts_contact.name, contactNameRecord);

    Record contactEmailRecord(_contacts_email._uri);
    contactEmailRecord.setFieldValue(_contacts_email.email, contact.mEmail);
    contactRecord.addChildRecord(_contacts_contact.email, contactEmailRecord);

    Record contactPhoneNumberRecord(_contacts_number._uri);
    contactPhoneNumberRecord.setFieldValue(_contacts_number.number, contact.mPhone);
    contactRecord.addChildRecord(_contacts_contact.number, contactPhoneNumberRecord);

    Record contactDetailsAppRecord(_contacts_profile._uri);
    contactDetailsAppRecord.setFieldValue(_contacts_profile.app_id, PACKAGE);
    contactDetailsAppRecord.setFieldValue(_contacts_profile.service_operation, APP_CONTROL_OPERATION_MAIN);
    contactDetailsAppRecord.setFieldValue(_contacts_profile.uri, contact.GetId());
    contactDetailsAppRecord.setFieldValue(_contacts_profile.extra_data, generateExtraDataForProfileRecord(contact.GetId()));
    contactRecord.addChildRecord(_contacts_contact.profile, contactDetailsAppRecord);

    Record birthdayRecord(_contacts_event._uri);
    birthdayRecord.setFieldValue(_contacts_event.calendar_type, CONTACTS_EVENT_CALENDAR_TYPE_GREGORIAN);
    birthdayRecord.setFieldValue(_contacts_event.type, CONTACTS_EVENT_TYPE_BIRTH);
    birthdayRecord.setFieldValue(_contacts_event.date, getBirthdayDateForDb(contact.mBirthday));
    contactRecord.addChildRecord(_contacts_contact.event, birthdayRecord);

    return db.insertRecord(contactRecord);
}

void FacebookContactsWriter::updateFacebookContactWithImage(int contactId, const std::string& imagePath, const std::string& uid) {
    CHECK_RET_NRV(contactId);
    CHECK_RET_NRV(!imagePath.empty());
    CHECK_RET_NRV(!uid.empty());

    Contacts::DataBase db;
    Contacts::Record contactRecord(db.getRecordByField(_contacts_contact._uri, _contacts_contact.id, contactId));

    Contacts::Record pictureRecord(_contacts_image._uri);
    pictureRecord.setFieldValue(_contacts_image.path, imagePath);
    contactRecord.addChildRecord(_contacts_contact.image, pictureRecord);

    //This is a workaround. I don't know why, but Tizen contact data base looses data about Facebook profile record,
    //so we're writing this data one more time.
    Contacts::Record profileRecord(_contacts_profile._uri);
    profileRecord.setFieldValue(_contacts_profile.app_id, PACKAGE);
    profileRecord.setFieldValue(_contacts_profile.service_operation, APP_CONTROL_OPERATION_MAIN);
    profileRecord.setFieldValue(_contacts_profile.uri, uid);
    profileRecord.setFieldValue(_contacts_profile.extra_data, generateExtraDataForProfileRecord(uid));
    contactRecord.addChildRecord(_contacts_contact.profile, profileRecord);

    db.updateRecord(contactRecord);
}

int FacebookContactsWriter::getTheAddressBookId() {
    int theAddressBookId = 0;
    DataBase db;
    Filter filter(_contacts_address_book._uri);
    filter.addStringValue(_contacts_address_book.name, CONTACTS_MATCH_EXACTLY, mAddressBookName.c_str());
    filter.addOperator(CONTACTS_FILTER_OPERATOR_AND);
    filter.addIntegerValue(_contacts_address_book.account_id, CONTACTS_MATCH_EQUAL, mAccountId);
    Records addressBookRecords = db.getRecordsByFilter(filter);
    if (addressBookRecords.size()) {
        theAddressBookId = addressBookRecords[0].getIntegerValue(_contacts_address_book.id);
    }
    return theAddressBookId;
}

int FacebookContactsWriter::createTheAddressBook() {
    Log::info(LOG_CONTACT_SYNC, "FacebookContactsWriter: New Address Book Create");
    DataBase db;
    Record addressBookRecord(_contacts_address_book._uri);
    addressBookRecord.setFieldValue(_contacts_address_book.account_id, mAccountId);
    addressBookRecord.setFieldValue(_contacts_address_book.mode, CONTACTS_ADDRESS_BOOK_MODE_READONLY);
    addressBookRecord.setFieldValue(_contacts_address_book.name, mAddressBookName.c_str());
    return db.insertRecord(addressBookRecord);
}

void FacebookContactsWriter::clearTheAddressBook() {
    Log::info(LOG_CONTACT_SYNC, "FacebookContactsWriter: Clear Address Book");
    CHECK_RET_NRV(mAddressBookId);

    DataBase db;
    Filter filter(_contacts_contact._uri);
    filter.addIntegerValue(_contacts_contact.address_book_id, CONTACTS_MATCH_EQUAL, mAddressBookId);
    Records records = db.getRecordsByFilter(filter);
    for (Records::iterator it = records.begin(); it != records.end(); it++) {
        db.removeRecord(*it, it->getIntegerValue(_contacts_contact.id));
    }
}

int FacebookContactsWriter::getBirthdayDateForDb(const std::string& fbBirthdayDate) {
    //Facebook keeps date in string in the following format: mm/dd/yyyy
    //Tizen contact data base keeps it in integer number in the following format: yyyymmdd
    //So, this it transformation of date from facebook format into Tizen.
    int date = -1;
    std::vector < std::string > splitResult = Utils::SplitString(fbBirthdayDate, "/");
    CHECK_RET(3 == splitResult.size(), date);
    std::stringstream ss;
    ss << splitResult[2] << splitResult[0] << splitResult[1];
    ss >> date;
    return date;
}

std::string FacebookContactsWriter::generateExtraDataForProfileRecord(const std::string& uid) {
    //Tizen contact data base keeps extra information in string in the following format:
    //key:value,key:value
    //The string is base64 encoded. So, here we create string containing extra data about contact uid.
    std::string dataToEncode("uid:");
    dataToEncode += uid;
    gchar* tmp = g_base64_encode((const guchar *) dataToEncode.c_str(), dataToEncode.length());
    std::string result(tmp);
    g_free(tmp);
    return result;
}
}
