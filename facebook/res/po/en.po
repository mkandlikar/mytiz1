msgid ""
msgstr ""

"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "IDS_GEN_NETWORK_ERROR"
msgstr "Check your network connection and try again."

msgid "IDS_NOTIF_MSG_SHARE_PHOTO"
msgstr "Your photo will be uploaded. Check progress in the notifications window."

msgid "IDS_NOTIF_MSG_SHARE_VIDEO"
msgstr "Your video will be uploaded. Check progress in the notifications window."

msgid "IDS_NOTIF_MSG_MULTI_SHARE_PHOTOS"
msgstr "Your photos will be uploaded. Check progress in the notifications window."

msgid "IDS_PROFILE_UPLOAD_PHOTO"
msgstr "Upload Photo"

msgid "IDS_PROFILE_CHOOSE_PHOTO"
msgstr "Choose from Photos"

msgid "IDS_PROFILE_VIEW_PHOTO"
msgstr "View Photo"

msgid "IDS_PROFILE_ME_ADD_COVERPHOTO"
msgstr "Add Cover Photo"

msgid "IDS_PROFILE_MORE"
msgstr "More"

msgid "IDS_PROFILE_ME_EDITPROFPIC_LABEL"
msgstr "Edit Profile Picture"

msgid "IDS_PROFILE_ME_CHANGECOVER_LABEL"
msgstr "Change Cover"

msgid "IDS_PROFILE_ME_VIEWPRIVACYSHTCUT_LABEL"
msgstr "View Privacy Shortcuts"

msgid "IDS_PROFILE_ME_UPDATEINFO"
msgstr "Update Info"

msgid "IDS_PROFILE_ME_POST_STATUS"
msgstr "Status"

msgid "IDS_PROFILE_ME_POST_PHOTO"
msgstr "Photo"

msgid "IDS_PROFILE_ME_UPLOAD_PROFILE_PICTURE"
msgstr "Uploading..."

msgid "IDS_PROFILE_ME_UPLOAD_PROFILE_PICTURE_COMPLETED"
msgstr "Profile Picture Updated."

msgid "IDS_PROFILE_ME_UPLOAD_COVER_PICTURE_COMPLETED"
msgstr "Cover Picture Updated."

msgid "IDS_PROFILE_FRIEND_FRIENDED"
msgstr "Friends"

msgid "IDS_PROFILE_FRIEND_FOLLOWED"
msgstr "Followed"

msgid "IDS_PROFILE_FRIEND_FOLLOW"
msgstr "Follow"

msgid "IDS_PROFILE_FRIEND_MESSAGE"
msgstr "Message"

msgid "IDS_PROFILE_FRIEND_POST_STATUS"
msgstr "Write Post"

msgid "IDS_PROFILE_FRIEND_POST_PHOTO"
msgstr "Share Photo"

msgid "IDS_PROFILE_OTHER_ADDFRIEND"
msgstr "Add Friend"

msgid "IDS_PROFILE_CONTENT_NOT_AVAILABLE"
msgstr "Content not available"

msgid "IDS_PROFILE_POKE"
msgstr "Poke"

msgid "IDS_POKE_ERROR_RESPONSE"
msgstr "has not responded to your last poke."

msgid "IDS_POKE_ERROR_SUCCESS"
msgstr "You have poked"

msgid "IDS_PHOTO_SAVE"
msgstr "Save Photo"

msgid "IDS_PHOTO_SAVE_SUCCESS"
msgstr "Photo saved to this device"

msgid "IDS_PHOTO_SAVE_ERROR"
msgstr "Error saving the photo!"

msgid "IDS_GEN_ERROR_LOADING_CONTENT"
msgstr "Error Loading Content."

msgid "IDS_LIKE"
msgstr "Like"

msgid "IDS_COMMENT"
msgstr "Comment"

msgid "IDS_SHARE"
msgstr "Share"

msgid "IDS_SEARCH"
msgstr "Search"

msgid "IDS_REPLY"
msgstr "Reply"

msgid "IDS_WRITE_COMMENT"
msgstr "Write a comment..."

msgid "IDS_JUST_NOW"
msgstr "Just now"

msgid "IDS_MINS"
msgstr "mins"

msgid "IDS_HOURS"
msgstr "hours ago"

msgid "IDS_1_MIN"
msgstr "1 min"

msgid "IDS_1_HOUR"
msgstr "1 hr"

msgid "IDS_CONFIRM_FRIEND_REQUEST"
msgstr "Confirm"

msgid "IDS_DELETE_FRIEND_REQUEST"
msgstr "Delete"

msgid "IDS_ADD_FRIEND"
msgstr "Add Friend"

msgid "IDS_BE_THE_FIRST_TO_LIKE_THIS"
msgstr "Be the first to like this"

msgid "IDS_YOU_AND"
msgstr "You and"

msgid "IDS_LIKE_THIS"
msgstr "like this"

msgid "IDS_YOU"
msgstr "You"

msgid "IDS_PEOPLE"
msgstr " people"

msgid "IDS_OTHERS"
msgstr " others"

msgid "IDS_PPL_WHO_LIKE_THIS"
msgstr "People who like this"

msgid "IDS_IN"
msgstr "in"

msgid "IDS_AND"
msgstr "and"

msgid "IDS_VIEW"
msgstr "View "

msgid "IDS_COMMENT_REPLIES"
msgstr " previous replies"

msgid "IDS_REPLIED"
msgstr " replied"

msgid "IDS_REPLIES"
msgstr "Replies"

msgid "IDS_SUP_EMAIL_HEADING"
msgstr "Enter Your Email Address"

msgid "IDS_SUP_EMAIL_INFO"
msgstr "You'll use this email when you log in and if you ever need to reset your password."

msgid "IDS_CONTINUE"
msgstr "Continue"

msgid "IDS_SIGNIN"
msgstr "Sign In"

msgid "IDS_SUP_USE_MOBILE_NO"
msgstr "Sign Up With Mobile Number"

msgid "IDS_SUP_EMAIL"
msgstr "Email Address"

msgid "IDS_SUP_NAME_HEADING"
msgstr "What's Your Name?"

msgid "IDS_SUP_NAME_INFO"
msgstr "Using your real name makes it easier for friends to recognize you."

msgid "IDS_SUP_FIRST_NAME"
msgstr "First Name"

msgid "IDS_SUP_LAST_NAME"
msgstr "Last Name"

msgid "IDS_SUP_PSWD_HEADING"
msgstr "Create a Password"

msgid "IDS_SUP_PSWD_INFO"
msgstr "Enter a combination of at least six numbers, letters and punctuation marks (like ! and &)."

msgid "IDS_PASSWORD"
msgstr "Password"

msgid "IDS_SIGNUP"
msgstr "Sign Up for Facebook"

msgid "IDS_SUP_BDATE_HEADING"
msgstr "What's Your Birthday?"

msgid "IDS_SUP_BDATE_INFO"
msgstr "You can decide later if you want to hide your birthday from your profile. Learn More."

msgid "IDS_SUP_SELECT_BDATE"
msgstr "Select Birthday"

msgid "IDS_SUP_GENDER_HEADING"
msgstr "What's Your Gender?"

msgid "IDS_SUP_GENDER_INFO"
msgstr "Providing your gender creates the best Facebook experience for you."

msgid "IDS_SUP_GENDER_FEMALE"
msgstr "Female"

msgid "IDS_SUP_GENDER_MALE"
msgstr "Male"

msgid "IDS_WHATS_ON_YOUR_MIND"
msgstr "What's on your mind?"

msgid "IDS_ADD_A_CAPTION"
msgstr "Add a caption…"

msgid "IDS_SAY_SOMETHING_ABOUT_THIS_PHOTO"
msgstr "Say something about this photo"

msgid "IDS_SAY_SOMETHING_ABOUT_THESE_PHOTOS"
msgstr "Say something about these photos"

msgid "IDS_SAY_SOMETHING_ABOUT_THIS_VIDEO"
msgstr "Say something about this video"

msgid "IDS_MAY_NOT_SELECT_MORE_MAX_PHOTOS"
msgstr "You may not select more than %d photos."

msgid "IDS_NOT_POSSIBLE_TO_SELECT_PHOTO_AND_VIDEO"
msgstr "It is not possible to select photos and videos at the same time."

msgid "IDS_NOT_POSSIBLE_TO_SELECT_SMALL_VIDEO_HEIGHT"
msgstr "The height of the video you tried to upload is too short. The minimum height for a video is 120. Please upload a taller video."

msgid "IDS_SOUND_POLICY_ERROR"
msgstr "Unable to play video during call"

msgid "IDS_UPPERCASE_DONE"
msgstr "DONE"

msgid "IDS_ADD_PHOTOS"
msgstr "Add Photos"

msgid "IDS_NO_COMMENTS"
msgstr "No Comments Yet"

msgid "IDS_SUP_MOBILE_HEADING"
msgstr "Enter Your Mobile Number"

msgid "IDS_SUP_MOBILE_INFO"
msgstr "You'll use this number when you log in and if you ever need to reset your password."

msgid "IDS_SUP_MOBILE_ENTER"
msgstr "Mobile Number"

msgid "IDS_SUP_USE_EMAIL"
msgstr "Sign Up With Email Address"

msgid "IDS_SUP_EMAIL_INVALID"
msgstr "Please enter a valid email address or use your mobile phone number."

msgid "IDS_SUP_MOBILE_INVALID"
msgstr "Please enter a valid mobile phone number or use your email address."

msgid "IDS_SHARE_NOW"
msgstr "Share Now"

msgid "IDS_INSTANTLY_SHARE"
msgstr "Instantly share this post with Friends"

msgid "IDS_WRITE_POST"
msgstr "Write Post"

msgid "IDS_SHARE_IN_NEW_POST"
msgstr "Share in New Post"

msgid "IDS_COPY_LINK"
msgstr "Copy Link"

msgid "IDS_OPEN_IN_BROWSER"
msgstr "Open in Browser"

msgid "IDS_SAVE_LINK"
msgstr "Save Link"

msgid "IDS_SUP_CONFIRM_CODE_ENTER"
msgstr "Confirmation Code"

msgid "IDS_CONFIRM"
msgstr "Confirm"

msgid "IDS_SEND_CODE_AGAIN"
msgstr "Send Code Again"

msgid "IDS_CHANGE_EMAIL_ADDRESS"
msgstr "Change Email Address"

msgid "IDS_CONFIRM_BY_MOBILE_NO"
msgstr "Confirm by Phone Number"

msgid "IDS_SUP_CONFIRM_EMAIL_INFO"
msgstr "Let us know this email belongs to you. Enter the code in the email sent to"

msgid "IDS_ADD_TEXT"
msgstr "Add Text"

msgid "IDS_DONE"
msgstr "Done"

msgid "IDS_CROP"
msgstr "Crop"

msgid "IDS_FIND_FRIENDS"
msgstr "Find Friends"

msgid "IDS_FFS_NOFRIENDS"
msgstr "No friends to show"

msgid "IDS_FFS_SEARCH"
msgstr "Search"

msgid "IDS_FFS_REQUESTS"
msgstr "Requests"

msgid "IDS_FFS_CONTACTS"
msgstr "Contacts"

msgid "IDS_FFS_FRIENDS"
msgstr "Friends"

msgid "IDS_FFS_FRIEND_REQUESTS"
msgstr "Friend Requests"

msgid "IDS_SUP_NAME_ERROR"
msgstr "Please enter your first and last name."

msgid "IDS_SUP_PSWD_ERROR"
msgstr "Your password must have at least 6 characters."

msgid "IDS_SUP_BDATE_ERROR"
msgstr "Please select a valid birthday."

msgid "IDS_SEARCH_FIELD"
msgstr "Search by Name or Email"

msgid "IDS_RESPOND"
msgstr "Respond"

msgid "IDS_FOLLOW"
msgstr "Follow"

msgid "IDS_MESSAGE"
msgstr "Message"

msgid "IDS_VIEW_PROFILE"
msgstr "View Profile"

msgid "IDS_WITH"
msgstr "with"

msgid "IDS_SHARED"
msgstr "shared"

msgid "IDS_SOMEONE_POST"
msgstr "'s post"

msgid "IDS_UNFOLLOW"
msgstr "Unfollow"

msgid "IDS_ADDED"
msgstr "Added"

msgid "IDS_LIKES"
msgstr "likes"

msgid "IDS_STATUS_COMMENT"
msgstr "comment"

msgid "IDS_STATUS_COMMENT_PLURAL"
msgstr "comments"

msgid "IDS_STATUS_LIKE"
msgstr "like"

msgid "IDS_STATUS_LIKE_PLURAL"
msgstr "likes"

msgid "IDS_FF_CONTACTS_TITLE"
msgstr "Facebook is Better With Friends"

msgid "IDS_FF_CONTACTS_DESCRIP"
msgstr "See who's on Facebook by continuously uploading your address book. Then choose who you want to add as friends"

msgid "IDS_FF_CONTACTS_LEARN_MORE"
msgstr "Info about your contacts in your address book, including names, phone numbers and nicknames, will be sent to Facebook to help you and others find friends faster, and to help us provide a better service. You can turn this off in Settings and manage or delete contact information you share with Facebook. Learn more."

msgid "IDS_SUP_IDS_SUP_NOCONNECT_HEADING"
msgstr "No Internet Connection"

msgid "IDS_SUP_IDS_SUP_NOCONNECT_INFO"
msgstr "Please check your internet connection and try again."

msgid "IDS_TRYAGAIN"
msgstr "Try Again"

msgid "IDS_PHOTOS_OF_YOU"
msgstr "Photos of You"

msgid "IDS_UPLOADS"
msgstr "Uploads"

msgid "IDS_ALBUMS"
msgstr "Albums"

msgid "IDS_SYNCED"
msgstr "Synced"

msgid "IDS_NO_PHOTO"
msgstr "No photos to show."

msgid "IDS_YOU_FRIENDS"
msgstr "You are now friends"

msgid "IDS_REQUEST_REMOVED"
msgstr "Request removed"

msgid "IDS_NO_REQUESTS"
msgstr "No New Friends Requests"

msgid "IDS_PPL_U_MAY_KNOW"
msgstr "People You May Know"

msgid "IDS_CANCEL"
msgstr "Cancel"

msgid "IDS_LOGIN"
msgstr "LOG IN"

msgid "IDS_LOGIN_CONTINUE"
msgstr "CONTINUE"

msgid "IDS_RESEND_CODE"
msgstr "Resend Code"

msgid "IDS_LOGIN_CODE"
msgstr "Login Code"

msgid "IDS_LOGIN_UID"
msgstr "Email or Phone"

msgid "IDS_LOGIN_HELP"
msgstr "Need help?"

msgid "IDS_HAVING_TROUBLE"
msgstr "Having Trouble?"

msgid "IDS_LOGIN_FAILED_TITLE"
msgstr "Login Failed"

msgid "IDS_LOGIN_CONNECT_ERR"
msgstr "Please check your Internet connection and try again."

msgid "IDS_OK"
msgstr "Ok"

msgid "IDS_FORGOT_PSWD"
msgstr "Forgot Password?"

msgid "IDS_HELP_CENTER"
msgstr "Help Center"

msgid "IDS_WRITE_STATUS"
msgstr "STATUS"

msgid "IDS_PHOTO_STATUS"
msgstr "PHOTO"

msgid "IDS_CHECK_IN_STATUS"
msgstr "CHECK IN"

msgid "IDS_CAMERA_ROLL"
msgstr "Camera Roll"

msgid "IDS_SUP_JOIN_FB"
msgstr "Join Facebook"

msgid "IDS_SUP_WC_NEXT"
msgstr "Next"

msgid "IDS_ABOUT_YOU"
msgstr "About You"

msgid "IDS_SUP_PSWD_TITLE"
msgstr "Protect Your Account"

msgid "IDS_UNFRIEND"
msgstr "Unfriend"

msgid "IDS_EDIT_FRIEND_LIST"
msgstr "Edit Friends List"

msgid "IDS_CONTINUE_READING"
msgstr "...Continue Reading"

msgid "IDS_CREATING_USER"
msgstr "Creating Account"

msgid "IDS_POST_IMAGE_LIKE"
msgstr "Like"

msgid "IDS_POST_IMAGE_LIKES"
msgstr "Likes"

msgid "IDS_POST_IMAGE_COMMENT"
msgstr "Comment"

msgid "IDS_POST_IMAGE_COMMENTS"
msgstr "Comments"

msgid "IDS_NO_RESULTS_FOUND"
msgstr "No Results Found."

msgid "IDS_BACK"
msgstr "Back"

msgid "IDS_ABOUT"
msgstr "About"

msgid "IDS_DELETE"
msgstr "Delete"

msgid "IDS_EDIT_POST"
msgstr "Edit Post"

msgid "IDS_TURN_OFF_NOTIFICATIONS"
msgstr "Turn Off Notification"

msgid "IDS_TURN_ON_NOTIFICATIONS"
msgstr "Turn On Notification"

msgid "IDS_HIDE_POST"
msgstr "Hide Post"

msgid "IDS_MORE_OPTIONS"
msgstr "More Options"

msgid "IDS_BACK_CTX_MENU"
msgstr "Back"

msgid "IDS_UNFOLLOW_SUB_TEXT"
msgstr "You won't see posts from"

msgid "IDS_HIDE_POST_SUB_TEXT"
msgstr "Hide this post from News Feed"

msgid "IDS_CAP_PEOPLE"
msgstr "People"

msgid "IDS_CHOOSE_FRIEND"
msgstr "Choose Friend"

msgid "IDS_TAG_FRIENDS"
msgstr "Tag Friends"

msgid "IDS_SHARE_WITH"
msgstr "Share With"

msgid "IDS_MORE"
msgstr "More..."

msgid "IDS_CAP_TAP_TO_CHANGE"
msgstr "TAP TO CHANGE"

msgid "IDS_IN_A_GROUP"
msgstr "In a Group"

msgid "IDS_ON_A_FRIENDS_TIMELINE"
msgstr "On a Friend's Timeline"

msgid "IDS_SHARE_TO_FACEBOOK"
msgstr "Share to Facebook"

msgid "IDS_POST_TO_FACEBOOK"
msgstr "Post to Facebook"

msgid "IDS_ALL_FRIENDS"
msgstr "All Friends"

msgid "IDS_DISCARD_POST"
msgstr "Discard Post?"

msgid "IDS_DISCARD_POST_DESC"
msgstr "If you go back now, your draft will be discarded."

msgid "IDS_DISCARD_ALBUM"
msgstr "Discard Album?"

msgid "IDS_DISCARD_ALBUM_DESC"
msgstr "If you go back now, your album will be discarded."

msgid "IDS_DISCARD"
msgstr "Discard"

msgid "IDS_KEEP"
msgstr "Keep"

msgid "IDS_LONG_POST_DESC"
msgstr "A text can't be longer than %d characters, but this one is %d characters long. Please make it shorter and try again."

msgid "IDS_ON_TIMELINE"
msgstr "On %s's Timeline"

msgid "IDS_S_FRIENDS"
msgstr "'s Friends"

msgid "IDS_POST"
msgstr "Post"

msgid "IDS_SAVE"
msgstr "Save"

msgid "IDS_EDIT_PRIVACY"
msgstr "Edit Privacy"

msgid "IDS_INCORRECT_EMAIL"
msgstr "Incorrect Email"

msgid "IDS_INCORRECT_PSWD"
msgstr "Incorrect Password"

msgid "IDS_AUTH_ERROR"
msgstr "Authentication Error"

msgid "IDS_AUTH_TOO_MANY_SMS"
msgstr "Too Many SMS Requests"

msgid "IDS_LOGIN_EMAIL_ERR_MSG"
msgstr "The email you entered doesn't appear to belong to an account. Please check your email address and try again."

msgid "IDS_LOGIN_PSWD_ERR_MSG"
msgstr "The password you entered is incorrect. Please try again."

msgid "IDS_AUTH_ERR_MSG"
msgstr "The code you entered is incorrect. Please try again."

msgid "IDS_AUTH_TOO_MANY_SMS_ERR_MSG"
msgstr "You have requested too many text message security codes recently. Please try again later."

msgid "IDS_LOGIN_APPROVAL_TITLE"
msgstr "Login Code Required"

msgid "IDS_LOGIN_APPROVAL_ERR_MSG"
msgstr "Enter the code we sent you by text message to log in."

msgid "IDS_LOGIN_ACCOUNT_CONFIRMATION_CAPTION"
msgstr "Confirmation Required"

msgid "IDS_LOGIN_ACCOUNT_CONFIRMATION_MESSAGE"
msgstr "You must confirm your e-mail address on www.facebook.com"

msgid "IDS_TRY_AGAIN_LATER"
msgstr "Please try again later"

msgid "IDS_LOGIN_TRYING_TOO_OFTEN"
msgstr "You are trying too often. Please try again later."

msgid "IDS_LOGIN_UNKNOWN_ERROR"
msgstr "An error occurred during sign in. Please try again later."

msgid "IDS_POST_DELETED"
msgstr "Post deleted"

msgid "IDS_NOT_ENOUGH_SPACE"
msgstr "There is not enough space on your device"

msgid "IDS_UPPERCASE_SELECT"
msgstr "SELECT"

msgid "IDS_PREVIEW"
msgstr "Preview"

msgid "IDS_1_VIDEO_SELECTED"
msgstr "1 Video Selected"

msgid "IDS_STUDIED"
msgstr "Studied"

msgid "IDS_LIVESIN"
msgstr "Lives in"

msgid "IDS_FROM"
msgstr "From"

msgid "IDS_AT"
msgstr "at"

msgid "IDS_SUGGESTIONS"
msgstr "Suggestions"

msgid "IDS_PAST"
msgstr "Past"

msgid "IDS_UNLIKE"
msgstr "Unlike"

msgid "IDS_VIA"
msgstr "via"

msgid "IDS_OTHER"
msgstr "other"

msgid "IDS_SINGLE_MUTUAL_FRIEND"
msgstr "mutual friend"

msgid "IDS_MUTUAL_FRIENDS"
msgstr "mutual friends"

msgid "IDS_MUTUAL_FRIEND"
msgstr "mutual friend"

msgid "IDS_WRITE_REPLY"
msgstr "Write a reply..."

msgid "IDS_NO_REPLIES"
msgstr "No Replies Yet"

msgid "IDS_EDITED"
msgstr "Edited"

msgid "IDS_SS_ALL"
msgstr "All"

msgid "IDS_SS_PEOPLE"
msgstr "People"

msgid "IDS_SS_PAGES"
msgstr "Pages"

msgid "IDS_SS_GROUPS"
msgstr "Groups"

msgid "IDS_SS_APPS"
msgstr "Apps"

msgid "IDS_POST_COMP_TO"
msgstr "To:"

msgid "IDS_POST_COMP_FRIENDS_EXCEPT_ONE"
msgstr "Friends; Except: %s"

msgid "IDS_INCLUDING"
msgstr "including"

msgid "IDS_FOLLOWS_YOUR_POSTS"
msgstr "Follows your public posts"

msgid "IDS_ALSO_LIVES_IN"
msgstr "Also lives in"

msgid "IDS_BORN_ON"
msgstr "Born on"

msgid "IDS_LEARN_MORE"
msgstr "Learn More."

msgid "IDS_FRIENDS_EXCEPT"
msgstr "Friends Except..."

msgid "IDS_FRIENDS_SPECIFIC"
msgstr "Specific Friends"

msgid "IDS_PUBLIC"
msgstr "Public"

msgid "IDS_ONLY_ME"
msgstr "Only Me"

msgid "IDS_ANYONE_ON_FACEBOOK"
msgstr "Anyone on or off Facebook"

msgid "IDS_YOUR_FRIENDS_ON_FACEBOOK"
msgstr "Your friends on Facebook"

msgid "IDS_SETTING_VIEW_YOUR_PROFILE"
msgstr "View your profile"

msgid "IDS_UP_SEARCH_FRND"
msgstr "Search your friend list"

msgid "IDS_UP_FRNDS_ALL"
msgstr "All"

msgid "IDS_UP_FRIENDS"
msgstr "Friends"

msgid "IDS_UP_PHOTOS"
msgstr "Photos"

msgid "IDS_PHOTOS_NO_PHOTOS"
msgstr "No photos to show."

msgid "IDS_VIEW_ALL"
msgstr "View All"

msgid "IDS_FRIEND1_AND_FRIEND2"
msgstr "%s with %s"

msgid "IDS_FRIEND1_AND_NUM_OTHER"
msgstr "%s with %s and %s other"

msgid "IDS_YOU_LIKE_THIS"
msgstr "You like this"

msgid "IDS_YOU_AND_NUM_OTHERS_LIKE_THIS"
msgstr "You and %s others like this"

msgid "IDS_NUM_OTHERS"
msgstr "others"

msgid "IDS_NUM_OTHER"
msgstr "other"

msgid "IDS_IN_CITY"
msgstr "in"

msgid "IDS_SETTING_FAVORITES"
msgstr "Favorites"

msgid "IDS_SETTING_FIND_FRIENDS"
msgstr "Find Friends"

msgid "IDS_SETTING_NEAR_BY_PLACES"
msgstr "Nearby Places"

msgid "IDS_SETTING_EVENTS"
msgstr "Events"

msgid "IDS_SETTING_APPS"
msgstr "Apps"

msgid "IDS_SETTING_CHAT"
msgstr "Chat"

msgid "IDS_SETTING_LIKE_PAGES"
msgstr "Like Pages"

msgid "IDS_SETTING_SAVED"
msgstr "Saved"

msgid "IDS_SETTING_FRIENDS"
msgstr "Friends"

msgid "IDS_SETTING_PHOTOS"
msgstr "Photos"

msgid "IDS_SETTING_POKES"
msgstr "Pokes"

msgid "IDS_SETTING_GROUPS"
msgstr "Groups"

msgid "IDS_SETTING_CREATE_GROUP"
msgstr "Create Group"

msgid "IDS_SETTING_PAGES"
msgstr "Pages"

msgid "IDS_SETTING_CREATE_PAGE"
msgstr "Create Page"

msgid "IDS_SETTING_FEEDS"
msgstr "Feeds"

msgid "IDS_SETTING_MOST_RECENT"
msgstr "Most Recent"

msgid "IDS_SETTING_CLOSE_FRIENDS"
msgstr "Close Friends"

msgid "IDS_SETTING_FAMILY"
msgstr "Family"

msgid "IDS_SETTING_HELP_N_SETTINGS"
msgstr "Help&Settings"

msgid "IDS_SETTING_APP_SETTINGS"
msgstr "App Settings"

msgid "IDS_SETTING_NEWS_FEED_PREF"
msgstr "News Feed Preferences"

msgid "IDS_SETTING_ACCOUNT_SETTINGS"
msgstr "Account Settings"

msgid "IDS_SETTING_HELP_CENTER"
msgstr "Help Center"

msgid "IDS_SETTING_PRIVACY_SHORTCUTS"
msgstr "Privacy Shortcuts"

msgid "IDS_SETTING_TERMS_N_POLICIES"
msgstr "Terms&Policies"

msgid "IDS_SETTING_REPORT_A_PROBLEM"
msgstr "Report a Problem"

msgid "IDS_SETTING_ABOUT"
msgstr "About"

msgid "IDS_SETTING_MOBILE_DATA"
msgstr "Mobile Data"

msgid "IDS_SETTING_LOG_OUT"
msgstr "Log Out"

msgid "IDS_EVENT_BIRTHDAY_TURNING"
msgstr "Turning %d years old"

msgid "IDS_EVENT_STARTS_WITHIN_HOUR"
msgstr "Starts within an hour"

msgid "IDS_EVENT_STARTS_IN"
msgstr "Starts in about"

msgid "IDS_EVENT_HOURS"
msgstr "hours"

msgid "IDS_MESSENGER_INVK_TITLE"
msgstr "Messaging"

msgid "IDS_MESSENGER_INVK_MAIN"
msgstr "Messenger or Chat"

msgid "IDS_MESSENGER_INVK_BTN"
msgstr "Messenger"

msgid "IDS_MESSENGER_INS_TITLE"
msgstr "Get App"

msgid "IDS_MESSENGER_INS_MAIN"
msgstr "You need to have the Messenger app installed to use this feature.<br>Please download it."

msgid "IDS_MESSENGER_INS_BTN"
msgstr "Download Messenger"

msgid "IDS_CANT_CONNECT"
msgstr "Can't connect"

msgid "IDS_TAP_TO_RETRY"
msgstr "Tap to retry"

msgid "IDS_DELETE_POST_CONFIRMATION_TITLE"
msgstr "Delete post?"

msgid "IDS_DELETE_POST_CONFIRMATION"
msgstr "Are you sure you want to delete this post?"

msgid "IDS_ALBUMS_PHOTO"
msgstr "photo"

msgid "IDS_ALBUMS_PHOTOS"
msgstr "photos"

msgid "IDS_ALBUMS_ALBUM"
msgstr "Album"

msgid "IDS_ALBUMS_ADD_PHOTOS"
msgstr "Add Photos"

msgid "IDS_ALBUMS_CREATE_ALBUM"
msgstr "Create Album"

msgid "IDS_ALBUMS_UNTITLED_ALBUM"
msgstr "Untitled Album"

msgid "IDS_ALBUMS_ADD_DESCRIPTION"
msgstr "Add a description..."

msgid "IDS_ALBUMS_CREATE"
msgstr "Create"

msgid "IDS_SETTING_SUGGESTED_GROUPS"
msgstr "Suggested Groups"

msgid "IDS_SETTING_SEE_ALL"
msgstr "See All"

msgid "IDS_REQUEST_SENT"
msgstr "Request sent"

msgid "IDS_CHOOSE_GROUP"
msgstr "Choose Group"

msgid "IDS_REQUEST_CANCELLED"
msgstr "Request cancelled"

msgid "IDS_ALBUM_RENAME"
msgstr "Rename"

msgid "IDS_ALBUM_RENAME_GUIDED_HINT_TEXT"
msgstr "Album Title"

msgid "IDS_ALBUM_RENAME_EMPTY_EDIT_TEXT_NOTIF_MSG"
msgstr "Please enter the album name"

msgid "IDS_ALBUM_NEW_TITLE"
msgstr "New Album Title"

msgid "IDS_SHARE_POSTING"
msgstr "Posting..."

msgid "IDS_SETTING_LOG_OUT_NOW"
msgstr "Log out now?"

msgid "IDS_POST_TO_GROUP"
msgstr "Post to group %s"

msgid "IDS_1_PHOTO_SELECTED"
msgstr "1 Photo Selected"

msgid "IDS_INDEX_OF_TOTAL_SELECTED"
msgstr "%d of %d Selected"

msgid "IDS_TAP_TO_TAG"
msgstr "Tap to tag"

msgid "IDS_SELECT_ALBUM"
msgstr "Select Album"

msgid "IDS_EXCEPT"
msgstr "Except:"

msgid "IDS_FRIENDS"
msgstr "Friends:"

msgid "IDS_TO"
msgstr "To:"

msgid "IDS_WRITE_SOMETHING"
msgstr "Write something..."

msgid "IDS_MESSENGER_TITLE"
msgstr "Messages"

msgid "IDS_MESSENGER_HEADING"
msgstr "Get Started With Messenger"

msgid "IDS_MESSENGER_INFO"
msgstr "Messenger is just as fast as texting, but with much more. You can even take photos and videos directly in the app."

msgid "IDS_FIND_FRIENDS_BTN"
msgstr "FIND FRIENDS"

msgid "IDS_BLOCK"
msgstr "Block"

msgid "IDS_UNBLOCK"
msgstr "Unblock"

msgid "IDS_LOADING"
msgstr "Loading..."

msgid "IDS_FBFORT"
msgstr "Facebook for Tizen"

msgid "IDS_VERSION"
msgstr "Version"

msgid "IDS_AND_THE"
msgstr "and the"

msgid "IDS_LOGS_ARE_TRADEMARKS_OF"
msgstr "logos are trademarks of"

msgid "IDS_RIGHTS_RESERVED"
msgstr "All rights reserved."

msgid "IDS_CANCEL_REQUEST"
msgstr "Cancel Request"

msgid "IDS_LIKES_THIS"
msgstr "likes this"

msgid "IDS_EDIT_CMNT"
msgstr "Edit"

msgid "IDS_EDIT_CMNT_TITLE"
msgstr "Edit"

msgid "IDS_UPDATE_CMNT"
msgstr "Update"

msgid "IDS_CANCEL_UPDATING"
msgstr "Cancel"

msgid "IDS_DELETE_CMNT"
msgstr "Delete"

msgid "IDS_COPY_CMNT"
msgstr "Copy"

msgid "IDS_SURE_TO_DELETE_TITLE"
msgstr "Delete comment?"

msgid "IDS_SURE_TO_DELETE"
msgstr "Are you sure you want to delete this comment?"

msgid "IDS_REPLIED_WITH_PHOTO"
msgstr "replied with a photo"

msgid "IDS_REPLIED_WITH_STICKER"
msgstr "replied with a sticker"

msgid "IDS_REPLIED_WITH_LINK"
msgstr "replied with a link"

msgid "IDS_GROUP_PROFILE_MEMBER"
msgstr "Member"

msgid "IDS_GROUP_PROFILE_MEMBERS"
msgstr "Members"

msgid "IDS_GROUP_PROFILE_MEMBER_REQUESTS"
msgstr "Member Requests"

msgid "IDS_NO_DATA_LOADED"
msgstr "No Data Loaded"

msgid "IDS_GROUP_PRIVACY_PUBLIC"
msgstr "Public Group"

msgid "IDS_GROUP_PRIVACY_CLOSED"
msgstr "Closed Group"

msgid "IDS_GROUP_PRIVACY_SECRET"
msgstr "Secret Group"

msgid "IDS_MAIN_CREATE_EVENT"
msgstr "CREATE"

msgid "IDS_PROFILE_CREATE_EVENT"
msgstr "Create Event"

msgid "IDS_NO_UPCOMING_EVENTS"
msgstr "No events coming up."

msgid "IDS_NO_INVITES_EVENTS"
msgstr "You're all caught up."

msgid "IDS_NO_SAVED_EVENTS"
msgstr "Save events and decide if you want to go later."

msgid "IDS_NO_HOSTING_EVENTS"
msgstr "Have friends over or plan a night out."

msgid "IDS_NO_PAST_EVENTS"
msgstr "Events you've attended in the past will show up here."

msgid "IDS_UPCOMING_EVENTS"
msgstr "Upcoming"

msgid "IDS_INVITES_EVENTS"
msgstr "Invites"

msgid "IDS_SAVED_EVENTS"
msgstr "Saved"

msgid "IDS_HOSTING_EVENTS"
msgstr "Hosting"

msgid "IDS_PAST_EVENTS"
msgstr "Past Events"

msgid "IDS_NEARBY_EVENTS"
msgstr "Nearby Events"

msgid "IDS_EVENTS_TODAY"
msgstr "Today at"

msgid "IDS_EVENTS_TOMORROW"
msgstr "Tomorrow at"

msgid "IDS_EVENTS_THIS_WEEK"
msgstr "This Week"

msgid "IDS_EVENTS_LATER"
msgstr "Later"

msgid "IDS_EVENT_TYPE_HOSTED"
msgstr "You're hosting"

msgid "IDS_EVENT_TYPE_GOING"
msgstr "You're going"

msgid "IDS_EVENT_TYPE_MIGHT_GO"
msgstr "You might go"

msgid "IDS_EVENT_TYPE_SAVED"
msgstr "You saved this"

msgid "IDS_EVENTS_BIRTHDAYS"
msgstr "Birthdays"

msgid "IDS_EVENTS_BIRTHDAYS_YEARS"
msgstr "%d years old"

msgid "IDS_EVENTS_BIRTHDAYS_JAN"
msgstr "Jan"

msgid "IDS_EVENTS_BIRTHDAYS_FEB"
msgstr "Feb"

msgid "IDS_EVENTS_BIRTHDAYS_MAR"
msgstr "Mar"

msgid "IDS_EVENTS_BIRTHDAYS_APR"
msgstr "Apr"

msgid "IDS_EVENTS_BIRTHDAYS_MAY"
msgstr "May"

msgid "IDS_EVENTS_BIRTHDAYS_JUNE"
msgstr "June"

msgid "IDS_EVENTS_BIRTHDAYS_JULY"
msgstr "July"

msgid "IDS_EVENTS_BIRTHDAYS_AUG"
msgstr "Aug"

msgid "IDS_EVENTS_BIRTHDAYS_SEPT"
msgstr "Sept"

msgid "IDS_EVENTS_BIRTHDAYS_OCT"
msgstr "Oct"

msgid "IDS_EVENTS_BIRTHDAYS_NOV"
msgstr "Nov"

msgid "IDS_EVENTS_BIRTHDAYS_DEC"
msgstr "Dec"

msgid "IDS_COMMENT_REPLY"
msgstr "previous reply"

msgid "IDS_CHECK_IN"
msgstr "people checked in here"

msgid "IDS_POSTS"
msgstr "Posts"

msgid "IDS_CHECK_IN_SERVICE_OFF"
msgstr "To use this feature, location services must be on."

msgid "IDS_CHECK_IN_DATA_NOT_AVAILABLE"
msgstr "Location data isn't available. Please try again later."

msgid "IDS_CHECK_IN_AT"
msgstr "at"

msgid "IDS_POPUP_TITLE1"
msgstr "Turn on Location Services"

msgid "IDS_POPUP_TITLE2"
msgstr "Problem Loading Places"

msgid "IDS_POPUP_MSG1"
msgstr "To use this feature, location services must be on."

msgid "IDS_POPUP_MSG2"
msgstr "Check your network connection and try again"

msgid "IDS_POPUP_MSG3"
msgstr "Location service not available"

msgid "IDS_POPUP_SKIP"
msgstr "SKIP"

msgid "IDS_POPUP_SETTINGS"
msgstr "SETTINGS"

msgid "IDS_EVENTS_CREATE_EVENT_NAME"
msgstr "Event name"

msgid "IDS_PRIVATE"
msgstr "Private"

msgid "IDS_PRIVATE_SUBTEXT"
msgstr "Visible only to people that are invited"

msgid "IDS_PUBLIC_SUBTEXT"
msgstr "Visible to anyone on or off Facebook"

msgid "IDS_PRIVATE_INVITE_FRIENDS"
msgstr "Guests can invite friends"

msgid "IDS_PRIVATE_EVENT_DESCRIPTION"
msgstr "You're creating a Private Event"

msgid "IDS_PUBLIC_EVENT_DESCRIPTION"
msgstr "You're creating a Public Event"

msgid "IDS_PRIVATE_EVENT_GUESTS_ON"
msgstr "Only people invited by hosts or guests can see this"

msgid "IDS_PRIVATE_EVENT_GUESTS_OFF"
msgstr "Only people invited by hosts can see this"

msgid "IDS_PUBLIC_EVENT_CREATE"
msgstr "Anyone on or off Facebook can see this"

msgid "IDS_EVENT_TODAY_TEXT"
msgstr "Today"

msgid "IDS_EVENT_TOMORROW_TEXT"
msgstr "Tomorrow"

msgid "IDS_EVENTS_NEXT_WEEK"
msgstr "Next Week"

msgid "IDS_EVENT_SUNDAY_TEXT"
msgstr "Sunday"

msgid "IDS_EVENT_MONDAY_TEXT"
msgstr "Monday"

msgid "IDS_EVENT_TUESTDAY_TEXT"
msgstr "Tuesday"

msgid "IDS_EVENT_WEDNESDAY_TEXT"
msgstr "Wednesday"

msgid "IDS_EVENT_THURSDAY_TEXT"
msgstr "Thursday"

msgid "IDS_EVENT_FRIDAY_TEXT"
msgstr "Friday"

msgid "IDS_EVENT_SATURDAY_TEXT"
msgstr "Saturday"

msgid "IDS_EVENT_OPTIONAL_TEXT"
msgstr "OPTIONAL"

msgid "IDS_EVENT_LOCATION_TEXT"
msgstr "Location"

msgid "IDS_EVENT_DETAILS_TEXT"
msgstr "Details"

msgid "IDS_EVENT_TIME_CREATION_ERROR"
msgstr "An event cannot last for more than 4 months."

msgid "IDS_EVENT_END_TIME_CREATION_ERROR"
msgstr "The event cannot end before it starts. End time was automatically changed"

msgid "IDS_EVENT_CREATION_ERROR_NO_NAME"
msgstr "Your event needs a name."

msgid "IDS_EVENT_PROFILE_GOING"
msgstr "Going"

msgid "IDS_EVENT_PROFILE_NOT_GOING"
msgstr "Not Going"

msgid "IDS_EVENT_PROFILE_MAYBE"
msgstr "Maybe"

msgid "IDS_EVENT_PROFILE_DECLINE"
msgstr "Decline"

msgid "IDS_EVENT_PROFILE_INVITED"
msgstr "Invited"

msgid "IDS_EVENT_PROFILE_PRIVACY_SECRET"
msgstr "GUESTS AND FRIENDS"

msgid "IDS_EVENT_PROFILE_HOSTED_BY"
msgstr "HOSTED BY"

msgid "IDS_EVENT_PROFILE_INVITE_FRIENDS"
msgstr "Invite Friends"

msgid "IDS_INVITE"
msgstr "Invite"

msgid "IDS_EDIT"
msgstr "Edit"

msgid "IDS_JOIN"
msgstr "Join"

msgid "IDS_JOINED"
msgstr "Joined"

msgid "IDS_REQUESTED"
msgstr "Requested"

msgid "IDS_COPY_EVENT_LINK"
msgstr "Copy Link"

msgid "IDS_EVENT_SUNDAY_SHORT"
msgstr "Sun"

msgid "IDS_EVENT_MONDAY_SHORT"
msgstr "Mon"

msgid "IDS_EVENT_TUESTDAY_SHORT"
msgstr "Tue"

msgid "IDS_EVENT_WEDNESDAY_SHORT"
msgstr "Wed"

msgid "IDS_EVENT_THURSDAY_SHORT"
msgstr "Thu"

msgid "IDS_EVENT_FRIDAY_SHORT"
msgstr "Fri"

msgid "IDS_EVENT_SATURDAY_SHORT"
msgstr "Sat"

msgid "IDS_EVENT_DURATION_SEPARATOR"
msgstr "to"

msgid "IDS_EVENT_CONFIRMATION"
msgstr "Are you sure?"

msgid "IDS_EVENT_CREATION_DESC"
msgstr "Your event won't be saved."

msgid "IDS_DELETE_EVENT"
msgstr "Delete Event"

msgid "IDS_DELETING_EVENT_PROCESS"
msgstr "Deleting"

msgid "IDS_ASK_DELETE_EVENT"
msgstr "Delete Event?"

msgid "IDS_DELETE_EVENT_DESCRIPTION"
msgstr "Are you sure you want to delete this event? This cannot be undone."

msgid "IDS_NO"
msgstr "No"

msgid "IDS_YES"
msgstr "Yes"

msgid "IDS_CURL_ERRORMSG_CACERT"
msgstr "Security Exception - Check Date and Time settings."

msgid "IDS_COULD_NOT_FIND_YOUR_LOCATION"
msgstr "Couldn't Find Your Location"

msgid "IDS_IMAGE_DOWNLOAD_FAILED"
msgstr "Image Download Failed"

msgid "IDS_NO_NOTIFICATIONS"
msgstr "No notifications"

msgid "IDS_NOTIFICATIONS_NEW"
msgstr "New"

msgid "IDS_NOTIFICATIONS_EARLIER"
msgstr "Earlier"

msgid "IDS_POST_UPLOAD_ERROR"
msgstr "We could not post this. Tap for more info"

msgid "IDS_POST_UPLOAD_ERROR_RESTRICTED_TIMELINE"
msgstr "We could not post this. The target user has restricted their timeline from publishing."

msgid "IDS_POST_UPLOAD_ERROR_NOTIFICATION"
msgstr "Facebook upload failed"

msgid "IDS_POST_UPLOAD_SUCCESS_NOTIFICATION"
msgstr "The upload has been successfully completed."

msgid "IDS_YOU_ARE_EVENT_HOST"
msgstr "You are the host of this event"

msgid "IDS_PROBLEM_DELETING_STORY"
msgstr "There was a problem deleting the story. Please try again later."

msgid "IDS_YOU_CANT_ADD_THIS_FRIEND"
msgstr "You can't add this friend"

msgid "IDS_YOU_CANT_ADD_THIS_FRIEND_UPDATE"
msgstr "You can't add this friend, please update list"

msgid "IDS_FRIENDS_ERROR_520"
msgstr "There is already a pending request"

msgid "IDS_FRIENDS_ERROR_522"
msgstr "You are already friends"

msgid "IDS_FRIENDS_ERROR_528"
msgstr "The user already has too many requests"

msgid "IDS_FRIENDS_ERROR_536"
msgstr "Unable to locate friend request"

msgid "IDS_FRIENDS_ERROR_UNKNOWN"
msgstr "Unknown error"

msgid "IDS_EVENT_IS_LOCKED"
msgstr "Event is locked and can no longer be modified"

msgid "IDS_VIDEO_TITLE_CANNOT_PLAY"
msgstr "Cannot play video"

msgid "IDS_VIDEO_MSG_NO_INTERNET"
msgstr "Check the strength of your connection and try again."

msgid "IDS_POST_FAILED_MORE_INFO_TITLE"
msgstr "Unable to post"

msgid "IDS_POST_FAILED_MORE_INFO"
msgstr "We weren't able to post your update"

msgid "IDS_POST_FAILED_MORE_INFO_506"
msgstr "This status update is identical to the last one you posted. Try posting something different, or delete your previous update."

msgid "IDS_POST_FAILED_MORE_INFO_UNKNOWN"
msgstr "The status cannot be posted at this time. Please try again later"

msgid "IDS_NO_FRIENDS_FOUND"
msgstr "No friends found"

msgid "IDS_NO_GROUPS"
msgstr "No groups"

msgid "IDS_EVENT_GUESTS_FRIENDS"
msgstr "Friends"

msgid "IDS_EVENT_GUESTS_OTHERS"
msgstr "Others"

msgid "IDS_FACEBOOK"
msgstr "Facebook"

msgid "IDS_AC_INFO_TITLE"
msgstr "Remove Account?"

msgid "IDS_AC_INFO_MSG"
msgstr "Removing this account will delete all of its messages, contacts and other data from the phone!"

msgid "IDS_REMOVE_ACCOUNT"
msgstr "REMOVE ACCOUNT"

msgid "IDS_CAPS_CANCEL"
msgstr "CANCEL"

msgid "IDS_EVENT_GUEST_TAB_GOING_NONE"
msgstr "None of your friends are going"

msgid "IDS_EVENT_GUEST_TAB_MAYBE_NONE"
msgstr "None of your friends have said they may be going"

msgid "IDS_EVENT_GUEST_TAB_INVITED_NONE"
msgstr "None of your friends have been invited"

msgid "IDS_ON_EVENT_CREATE_CONNECTION_LOST"
msgstr "Sorry, something went wrong while creating your event. Please try again."

msgid "IDS_CREATE_ALBUM_MSG_NO_INTERNET"
msgstr "The album cannot be created at this time.Please try again later."

msgid "IDS_DELETE_ALBUM_CONFIRMATION_CAPTION"
msgstr "Delete Album?"

msgid "IDS_DELETE_ALBUM_CONFIRMATION_TXT"
msgstr "Are you sure you want to delete the album \"%s\"? Photos in this album will also be deleted."

msgid "IDS_DELETE_ALBUM_CONFIRMATION_CANCEL"
msgstr "Cancel"

msgid "IDS_DELETE_ALBUM_CONFIRMATION_DELETE"
msgstr "Delete"

msgid "IDS_SOMETHING_WENT_WRONG"
msgstr "Something went wrong. Please try again."

msgid "IDS_EVENT_NOT_AVAILABLE"
msgstr "This event is not available"

msgid "IDS_POST_IS_NOT_AVAILABLE"
msgstr "We're sorry, this post is no longer available. It may have been removed."

msgid "IDS_POST_COMPOSER_LINK_PREVIEW_PLACEHOLDER"
msgstr "A preview will be added after you post this to Facebook."

msgid "IDS_EVENT_LOCATION_VIEW_PAGE"
msgstr "View Page"

msgid "IDS_EVENT_LOCATION_OPEN_MAPS"
msgstr "Open in Maps"

msgid "IDS_EVENT_LOCATION_COPY_LOCATION"
msgstr "Copy Location"

msgid "IDS_EVENT_LOCATION_COPY_ADDRESS"
msgstr "Copy Address"

msgid "IDS_POST_WAS_SUCCESSFUL"
msgstr "Your post was successful."

msgid "IDS_PHOTOS_OF"
msgstr "Photos of"

msgid "IDS_MSG_PHOTO_POST_INITIATED"
msgstr "Your photo will be posted."

msgid "IDS_MSG_ALBUM_CREATED"
msgstr "The album was successfully created."

msgid "IDS_MSG_ALBUM_DELETED"
msgstr "The album was successfully deleted."

msgid "IDS_ALBUM_IS_NOT_AVAILABLE"
msgstr "We're sorry, this album is no longer available. It may have been removed."

msgid "IDS_MSG_POST_SHARED"
msgstr "You shared this post."

msgid "IDS_GROUP_POST_TIP_PRIVACY_PUBLIC"
msgstr "This is a public group, so your post will be visible to anyone on Facebook."

msgid "IDS_GROUP_POST_TIP_PRIVACY_CLOSED"
msgstr "This post will be visible to members of the group."

msgid "IDS_TICKETS"
msgstr "Tickets Available"

msgid "IDS_GROUP_POST_TIP_PRIVACY_FRIENDS_TIMELINE"
msgstr "controls who can see this."

msgid "IDS_EDIT_HISTORY"
msgstr "Edit History"

msgid "IDS_VIEW_EDIT_HISTORY"
msgstr "View Edit History"

msgid "IDS_CONFIRM_SHOW_POST_FOR_TAGGED"
msgstr "%s will be able to see this post because you've tagged them. Do you still want to post this?"

msgid "IDS_FIRST_FRIEND_AND_SECOND"
msgstr "%s and %s"

msgid "IDS_FRIEND_AND_OTHERS"
msgstr "%s and %s others"

msgid "IDS_EDIT_CAPTION"
msgstr "Edit Caption"

msgid "IDS_EDIT_CAPTION_HINT"
msgstr "Write photo caption..."

msgid "IDS_SAVE_CHANGES"
msgstr "Save changes"

msgid "IDS_CLEAR_CAPTION_BOX"
msgstr "Clear"

msgid "IDS_LEAVE_CAPTION"
msgstr "Leave Caption?"

msgid "IDS_YOUR_CHANGES_WONT_BE_SAVED"
msgstr "Your changes won't be saved"

msgid "IDS_STAY"
msgstr "Stay"

msgid "IDS_LEAVE"
msgstr "Leave"

msgid "IDS_DELETE_PHOTO"
msgstr "Delete Photo"

msgid "IDS_DELETE_PHOTO_CONFIRMATION_CAPTION"
msgstr "Delete photo"

msgid "IDS_DELETE_PHOTO_CONFIRMATION_TEXT"
msgstr "Do you want to delete this photo?"

msgid "IDS_DELETE_PHOTO_FAILED"
msgstr "Cannot delete the photo at this time. Please try again later."

msgid "IDS_CAMERA_UNAVAILABLE"
msgstr "Unable to connect to the camera"

msgid "IDS_TRY_AGAIN"
msgstr "Try Again"

msgid "IDS_POST_COMMENT_WAIT"
msgstr "Waiting"

msgid "IDS_POST_COMMENT_PROGRESS"
msgstr "In progress"

msgid "IDS_POST_COMMENT_PAUSE"
msgstr "Paused"

msgid "IDS_POST_COMMENT_COMPLETED"
msgstr "Completed"

msgid "IDS_POST_COMMENT_CANCELED"
msgstr "Canceled"

msgid "IDS_POST_COMMENT_FAILED"
msgstr "Failed to post"

msgid "IDS_UPLOAD_IN_PROGRESS"
msgstr "Some upload is in progress. Try again later"

msgid "IDS_CANCEL_UPLOAD"
msgstr "Cancel Upload"

msgid "IDS_ADD_MEMBER"
msgstr "Add Member"

msgid "IDS_MAX_CHARACTERS_REACHED"
msgstr "Maximum number of characters reached."

msgid "IDS_MAX_EXCEPTED_FRIENDS_LIMIT_REACHED_CAPTION"
msgstr "Maximum Limit Reached"

msgid "IDS_MAX_SPECIFIC_FRIENDS_LIMIT_REACHED_CAPTION"
msgstr "Maximum Limit Reached"

msgid "IDS_MAX_EXCEPTED_FRIENDS_LIMIT_REACHED_MESSAGE"
msgstr "You can remove up to 25 people."

msgid "IDS_MAX_SPECIFIC_FRIENDS_LIMIT_REACHED_MESSAGE"
msgstr "You can specify up to 25 people."

msgid "IDS_POST_PREVIEW_YOUR_NAME"
msgstr "You"

msgid "IDS_POST_PREVIEW_ADDED_SINGLE_PHOTO"
msgstr " added a new photo"

msgid "IDS_POST_PREVIEW_ADDED_MULTIPLE_PHOTOS"
msgstr " added %s new photos"

msgid "IDS_POST_PREVIEW_ADDED_SINGLE_VIDEO"
msgstr " added a new video"

msgid "IDS_POST_PREVIEW_TAGGED_FRIEND"
msgstr " - with %s"

msgid "IDS_POST_PREVIEW_TAGGED_FRIENDS_AND"
msgstr " and "

msgid "IDS_POST_PREVIEW_TAGGED_FRIENDS_OTHERS"
msgstr "%s others"

msgid "IDS_POST_PREVIEW_ADDED_LOCATION_ONLY"
msgstr " - at %s"

msgid "IDS_POST_PREVIEW_ADDED_LOCATION"
msgstr " at %s"

msgid "IDS_DOWNLOAD_DOCUMENT_FILE"
msgstr "Starting download document"

msgid "IDS_REQUEST_IS_NOT_VALID"
msgstr "Sorry, this request is not valid"

msgid "IDS_FRIENDS_ARE_WAITING_CAPTION"
msgstr "Your Friends Are Waiting"

msgid "IDS_FRIENDS_ARE_WAITING_MESSAGE"
msgstr "The friend finder is better than ever. Try it"

msgid "IDS_FRIENDS_ARE_WAITING_BUTTON"
msgstr "Find Friends"

msgid "IDS_NEWS_FEED_IS_UP_TO_DATE" 
msgstr "News Feed is up to date"

msgid "IDS_TO_THE_ALBUM" 
msgstr "to the album"

msgid "IDS_HINT_SEARCH_FOR_PLACES"
msgstr "Search for places"

msgid "IDS_SORRY"
msgstr "Sorry"

msgid "IDS_CONTENT_ISNT_AVAILABLE"
msgstr "This content isn't available"

msgid "IDS_LOADING_DATA"
msgstr "Loading data. Try again later"

msgid "IDS_MESSENGER_SLOGAN"
msgstr "Messenger is a Better Way to Text"

msgid "IDS_GET_MESSENGER"
msgstr "Get Messenger"

msgid "IDS_MESSENGER_PHOTOS_AND_VIDEOS"
msgstr "Photos and Videos: Snap or shoot right from app and send instantly"

msgid "IDS_MESSENGER_CALLS"
msgstr "Calls: Make free calls, even to friends and family across the world"

msgid "IDS_MESSENGER_STICKERS"
msgstr "Stickers: Choose from cute and crazy stickers to say what you're doing or feeling"

msgid "IDS_MESSENGER_VOICE"
msgstr "Voice: Just start talking to record message when you're on the go"

msgid "IDS_MESSENGER_CHARGES"
msgstr "Standard data charges apply"

msgid "IDS_NEW_STORIES"
msgstr "New Stories"

msgid "IDS_PLACES"
msgstr "Places"

msgid "IDS_ERROR_OCCURRED"
msgstr "Error occurred"

msgid "IDS_TERMS_OF_SERVICE"
msgstr "Terms of Service"

msgid "IDS_DATA_POLICY"
msgstr "Data Policy"

msgid "IDS_AT_T"
msgstr "at"

msgid "IDS_VIEW_GROUP_INFO"
msgstr "View Group Info"

msgid "IDS_OPEN"
msgstr "Open"

msgid "IDS_CLOSED"
msgstr "Closed"

msgid "IDS_SECRET"
msgstr "Secret"

msgid "IDS_SYNC_NOW_AIS"
msgstr "Sync now"

msgid "IDS_REMOVE_ACCOUNT_AIS"
msgstr "Remove Account"

msgid "IDS_MUTUAL_FRIENDS_TITLE"
msgstr "Mutual Friends"

msgid "IDS_UNFRIEND_CD_MESSAGE"
msgstr "You'll need to send a friend request to become friends again with %s."

msgid "IDS_UNFRIEND_CD_TITLE"
msgstr "Unfriend %s?"

msgid "IDS_EDIT_PRIVACY_KEEP_DISCARD_TEXT"
msgstr "If you go back now, your draft will be discarded."

msgid "IDS_PROFILE_POPUP_BLOCK_MESSAGE"
msgstr "%s won't be able to see you or contact you on Facebook."

msgid "IDS_PROFILE_POPUP_BLOCK_TITLE"
msgstr "Block %s?"

msgid "IDS_TAG_MEMBERS"
msgstr "Tag Members"

msgid "IDS_WHO_LIKE_THIS_AND"
msgstr "and"

msgid "IDS_WHO_LIKE_THIS_NUM_OTHER"
msgstr "other"

msgid "IDS_WHO_IS_THIS"
msgstr "Who is this?"

msgid "IDS_TEXT_TAG"
msgstr "text tag"

msgid "IDS_TAG_PHOTO"
msgstr "Tag Photo"

msgid "IDS_EDIT_PHOTO_CAPTION"
msgstr "Edit"

msgid "IDS_DISCARD_CAPTIONS"
msgstr "Discard Captions?"

msgid "IDS_DISCARD_CHANGES"
msgstr "Discard Changes?"

msgid "IDS_DISCARD_CHANGES_DESC"
msgstr "If you go back now, your edit will be discarded."

msgid "IDS_BLOCK_USER"
msgstr "Blocking this user"

msgid "IDS_TAP_PHOTO_LABEL"
msgstr "Tap the photo to start tagging"

msgid "IDS_CANNOT_REBLOCK_USER"
msgstr "User cannot reblock target so soon after unblocking target"

msgid "IDS_UNFOLLOWED_FRIEND"
msgstr "Unfollowed "

msgid "IDS_UNDO"
msgstr "Undo"

msgid "IDS_ONE_OTHER_LIKE_THIS"
msgstr "1 other like this"

msgid "IDS_NUM_OTHERS_LIKE_THIS"
msgstr "others like this"

msgid "IDS_ACCOUNT_DISABLED"
msgstr "Account Disabled"

msgid "IDS_ACCOUNT_HAS_BEEN_DISABLED"
msgstr "Your account has been disabled. If you have any questions or concerns, you can visit our Help Center."

msgid "IDS_ACCOUNT_DISABLED_LEARN_MORE"
msgstr "Learn More"

msgid "IDS_EDIT_COMMENT_FAILED"
msgstr "Failed to update"
